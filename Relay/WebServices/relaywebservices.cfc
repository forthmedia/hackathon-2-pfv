<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent hint="Relay Web Service Functions" output="false">
<!---
File name:			relaywebservices.cfm
Author:				AJC
Date started:		2007-08-09

Description:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2008-10-01 WAB   altered get_person_Usage query after a blockage
2008-12-04 	NYB 	CR-SNY662 - added whether Terms & Conditions were accepted.  Passed as boolean called TsAndCsAccepted based on flag 'Sony1TCAgree'
2009-11-25  WAB		LID 2876 Problem when username is numeric.  Function authenticateWebServiceUser doesn't know whether it is dealing with a personid or a username
2010-02-01  SSS     p-sny089 All http links need to be https
2010-05-24	NJH		LID 3286: Sony relayWebServices Error - use isoEncode function to encode &'s in xml.
2010-06-30	NJH		P-FNL069 - changed relay_login to use new login/security functions.
2010-06-17  SSS     p-sny096 released locationID to location node
2012/05/08 	IH 		Case 428085 URL decode the xml file
2014-01-31	WAB 	removed references to et cfm, can always just use / or /?
2016/09/01		NJH JIRA PROD2016-1190 - replaced old matching/dataload code with call to new matching functions and call to relayEntity.

--->


	<cfprocessingdirective pageencoding="utf-8">

	<!--- NJH 2010-06-29 P-FNL069 - use authenticateUserV2 function --->

	<cffunction name="RELAY_LOGIN" access="remote" returntype="xml" output="false" hint="Authentication Services for RelayWS">
		<cfargument name="relayusername" type="String" required="true">
		<cfargument name="relaypassword" type="String" required="true">

		<cfset var CallMainTask = "">
		<cfset var receivedParameter = "">
		<cfset var EmailCheck = "">
		<cfset var relayWSXMLResponse = "">

				<cfquery name="EmailCheck"  datasource = "#application.sitedatasource#">
				SELECT count(*) AS 'CheckCount1' FROM person
				WHERE 1=1 AND Email  =  <cf_queryparam value="#arguments.relayusername#" CFSQLTYPE="CF_SQL_VARCHAR" >
				</cfquery>

				<cfif EmailCheck.CheckCount1 GTE 2>

					<cfscript>
						receivedParameter=arraynew(2);
						receivedParameter[1][1]="relayusername";
						receivedParameter[1][2]="#arguments.relayusername#";
						receivedParameter[2][1]="relaypassword";
						receivedParameter[2][2]="#arguments.relaypassword#";

						relayWSXMLResponse = build_XML_Response(
							wsMethod="RELAY_LOGIN",
							wsResponseID="403",
							wsResponseText="Duplicate Email",
							receivedParameter=receivedParameter,
							relayContent="Duplicate Email");

						CallMainTask = relayWSXMLResponse;
					</cfscript>

				<cfelse>

					<cfset CallMainTask = RELAY_LOGINMAIN(relayusername = relayusername, relaypassword = relaypassword)>

				</cfif>

		<cfreturn CallMainTask>

	</cffunction>

	<cffunction name="RELAY_LOGINMAIN" access="private" returntype="xml" output="false" hint="Authentication Services for RelayWS">
		<cfargument name="relayusername" type="String" required="true">
		<cfargument name="relaypassword" type="String" required="true">

		<cfscript>
			var relayWSXMLResponse="";
			var authenticationResult = application.com.login.authenticateUserV2(urldecode(arguments.relayUsername,"utf-8"),arguments.relayPassword);
			var personAuthenticated = authenticationResult.userQuery;
			var personNotAuthenticated = ''; //authenticationResult.failedUserQuery;
			var userXML = "this would normally be user xml";
			var statusID = 0;
			var statusText = "";
			var userDetailXML = "";
			var qryUserDetails = "";
			receivedParameter=arraynew(2);
			receivedParameter[1][1]="relayusername";
			receivedParameter[1][2]="#arguments.relayusername#";
			receivedParameter[2][1]="relaypassword";
			receivedParameter[2][2]="#arguments.relaypassword#";
		</cfscript>

		<cfif personAuthenticated.recordCount gt 0>
				<cfset statusID = 200>

				<cfset qryUserDetails = getUserDetails(personID=personAuthenticated.personID)>
				<!--- sets any usergroups set by profile --->
				<cfset application.com.login.addPersonToUserGroupsByProfile(personID=personAuthenticated.personID)>

				<cfquery name="qry_get_recordRights" datasource="#application.siteDataSource#">
					select * from RightsGroup where personID =  <cf_queryparam value="#personAuthenticated.personID#" CFSQLTYPE="CF_SQL_INTEGER" >  and UserGroupID IN (1277,1279)
				</cfquery>

				<cfif (application.com.flag.checkBooleanFlagByID(entityid=qryUserDetails.organisationID,flagID="sony1_orgApproved") and application.com.flag.checkBooleanFlagByID(entityid=personAuthenticated.personID,flagID="sony1_perApproved"))
					 or (application.com.flag.checkBooleanFlagByID(entityid=qryUserDetails.organisationID,flagID="dns_orgApproved") and application.com.flag.checkBooleanFlagByID(entityid=personAuthenticated.personID,flagID="dns_perApproved"))
					 or (qry_get_recordRights.recordcount gt 0)>

					<cfset userDetailXML = GET_USERDETAILS(personID=personAuthenticated.personID)>
					<cfoutput>
						<cfsavecontent variable="userXML">
							#userDetailXML#
						</cfsavecontent>
					</cfoutput>

					<!---  WAB 2008-10-01
					noticed a block on the db.
					This cfquery had a maxrows = 1 on it but  SQL still has to bring back a whole record set, so replaced with TOP 1
					--->
					<cfquery name="get_person_Usage" datasource="#application.sitedatasource#" >
						select top 1 loginDate from Usage
						where personID =  <cf_queryparam value="#personAuthenticated.personID#" CFSQLTYPE="CF_SQL_INTEGER" >
						order by loginDate Desc
					</cfquery>

					<cfif get_person_Usage.recordcount neq 0>
						<cfset statusText = "#dateformat(createodbcdatetime(get_person_Usage.loginDate),"DD/MM/YYYY")# #timeformat(createodbcdatetime(get_person_Usage.loginDate),"HH:MM")#">
					<cfelse>
						<cfset statusText = "#dateformat(createodbcdatetime(now()),"DD/MM/YYYY")# #dateformat(createodbcdatetime(now()),"HH:MM")#">
					</cfif>
					<cfset application.com.login.insertUsage(personID=personAuthenticated.personID,app="Web Service Login")>
				<cfelse>
					<cfset statusText = "Incorrect Details.">
					<cfset statusID = 404>
					<cfoutput>
						<cfsavecontent variable="userXML">
						</cfsavecontent>
					</cfoutput>
				</cfif>

		<cfelseif authenticationResult.failedUserQuery.recordcount >
			<!--- This is the case where the username was correct but something else was wrong --->
			<cfset personNotAuthenticated = authenticationResult.failedUserQuery>
			<cfset statusid = personNotAuthenticated.LoginStatus>
			<cfswitch expression="#statusid#">
				<cfcase value="401"><cfset statusText = personNotAuthenticated.loginAttemptsLeft></cfcase>
				<cfcase value="404"><cfset statusText = "Incorrect Details"></cfcase>
				<cfcase value="423"><cfset statusText = personNotAuthenticated.loginLastLock></cfcase>
			</cfswitch>

			<cfoutput>
				<cfsavecontent variable="userXML">
				</cfsavecontent>
			</cfoutput>

		<cfelse>
			<!--- This is the case where the username was  wrong --->
				<cfset statusText = "Incorrect Details.">
				<cfset statusID = 404>
				<cfoutput>
					<cfsavecontent variable="userXML">
					</cfsavecontent>
				</cfoutput>

		</cfif>

		<cfscript>
			relayWSXMLResponse = build_XML_Response(
				wsMethod="RELAY_LOGIN",
				wsResponseID="#statusID#",
				wsResponseText="#statusText#",
				receivedParameter=receivedParameter,
				relayContent="#userXML#");
		</cfscript>

		<cfreturn relayWSXMLResponse>
	</cffunction>

	<cffunction name="RELAY_REGISTRATION" access="remote" returntype="xml" output="false" hint="Registration Services for RelayWS">
		<cfargument name="locale" type="String" required="true">
		<cfset var relayWSXMLResponse="">
		<!--- <cfset var relayWSiframe="">
		<cfset var relayWSXMLResponse="">
		<cfset var countryCode=listgetat(locale,2,"_")>
		<cfset var languageCode=listgetat(locale,1,"_")> --->

		<cfscript>
			relayWSXMLResponse=RELAY_GETCONTENT(relayusername='',personid=0,locale=arguments.locale,etid='sony1RegistrationStart');
		</cfscript>
		<cfset relayWSXMLResponse.relayResponse[1].relayTag[1].xmlAttributes.method="RELAY_REGISTRATION">

		<cfreturn relayWSXMLResponse>
	</cffunction>

	<cffunction name="RELAY_COMPANY_ADMIN" access="remote" returntype="xml" output="false" hint="Company Admin Iframe">
		<cfargument name="personID" type="numeric" required="false">
		<cfargument name="relayusername" type="String" required="false">
		<cfargument name="locale" type="String" required="true">

		<cfset var relayWSXMLResponse="">

		<cfscript>
			relayWSXMLResponse=RELAY_GETCONTENT(personID=arguments.personID,relayusername=arguments.relayusername,locale=arguments.locale,etid='companyAdmin');
		</cfscript>
		<cfset relayWSXMLResponse.relayResponse[1].relayTag[1].xmlAttributes.method="RELAY_COMPANY_ADMIN">

		<cfreturn relayWSXMLResponse>

	</cffunction>

	<cffunction name="RELAY_RESENDPASSWORD" access="remote" returntype="xml" output="false" hint="Forgotten Password Iframe">
		<!--- <cfargument name="relayusername" type="String" required="false"> --->
		<cfargument name="locale" type="String" required="true">

		<cfset var relayWSXMLResponse="">
		<!--- <cfset var relayWSiframe="">
		<cfset var relayWSXMLResponse="">
		<cfset var countryCode=listgetat(locale,2,"_")>
		<cfset var languageCode=listgetat(locale,1,"_")> --->

		<cfscript>
			relayWSXMLResponse=RELAY_GETCONTENT(relayusername='',personid=0,locale=arguments.locale,etid='Resendsony1PW');
		</cfscript>
		<cfset relayWSXMLResponse.relayResponse[1].relayTag[1].xmlAttributes.method="RELAY_RESENDPASSWORD">

		<cfreturn relayWSXMLResponse>
	</cffunction>

	<cffunction name="RELAY_CHANGEPASSWORD" access="remote" returntype="xml" output="false" hint="Change Password Iframe">
		<!--- <cfargument name="relayusername" type="String" required="false"> --->
		<cfargument name="personID" type="numeric" required="false">
		<cfargument name="relayusername" type="String" required="false">
		<cfargument name="locale" type="String" required="true">

		<cfset var relayWSXMLResponse="">
		<!--- <cfset var relayWSiframe="">
		<cfset var relayWSXMLResponse="">
		<cfset var countryCode=listgetat(locale,2,"_")>
		<cfset var languageCode=listgetat(locale,1,"_")> --->

		<cfscript>
			relayWSXMLResponse=RELAY_GETCONTENT(relayusername=arguments.relayusername,personid=arguments.personID,locale=arguments.locale,etid='ChangePW');
		</cfscript>
		<cfset relayWSXMLResponse.relayResponse[1].relayTag[1].xmlAttributes.method="RELAY_CHANGEPASSWORD">

		<cfreturn relayWSXMLResponse>
	</cffunction>

	<cffunction name="RELAY_GET_ATTRS_FOR_ACC_BG" access="remote" returntype="xml" output="false" hint="Gets Attributes for Company BG">
		<cfargument name="accID" type="String" required="false">
		<cfargument name="BG" type="String" required="false">

		<cfscript>
			var relayWSXMLResponse="";
			var qry_get_orgID="";
			var receivedParameter=arraynew(2);
			var qry_get_org_bgflags="";
			receivedParameter[1][1]="accID";
			receivedParameter[1][2]="#arguments.accID#";
			receivedParameter[2][1]="BG";
			receivedParameter[2][2]="#arguments.BG#";
		</cfscript>

		<!--- <cftry> --->
		<cfquery name="qry_get_orgID" datasource="#application.sitedatasource#">
			select organisationID from organisationAccountBG
			where account =  <cf_queryparam value="#arguments.accID#" CFSQLTYPE="CF_SQL_VARCHAR" >  and businessGroupID IN (select businessgroupID from businessgroup where businessgroupkey =  <cf_queryparam value="#arguments.bg#" CFSQLTYPE="CF_SQL_VARCHAR" >  and businessgrouptypeID=2)
			and organisationID NOT IN (select entityID from booleanflagdata where flagID IN (select flagID from flag where flagtextID='orgDNSisInternal'))
		</cfquery>
		<cfif qry_get_orgID.recordcount neq 0>
			<cfquery name="qry_get_org_bgflags" datasource="#application.siteDataSource#">
				select flagGroupid, flagtypeID, replace(flagGroupTextid,'#BG#_','') as flagGroupTextid, dbo.booleanflaglist (flagGroupid,#qry_get_orgID.organisationID#,default) as value
				from flaggroup where flaggroupid IN (select fg.flaggroupID from flaggroup pfg inner join flaggroup fg on fg.parentflaggroupid = pfg.flaggroupid where pfg.flaggroupTextID =  <cf_queryparam value="#BG#_attributes" CFSQLTYPE="CF_SQL_VARCHAR" > )
			</cfquery>

			<cfquery name="qry_get_org_type" datasource="#application.siteDataSource#">
				SELECT case when isinternalbfd.flagid IS NULL THEN
					case when isresellerbfd.flagID IS NOT NULL AND isdealerbfd.flagID IS NOT NULL THEN
						'Hybrid'
					ELSE
						case when isresellerbfd.flagID IS NOT NULL THEN
							'Reseller'
						ELSE
							case when isdealerbfd.flagID IS NOT NULL THEN
								'Dealer'
							ELSE
								'Invalid'
							END
						END
					END
				ELSE 'Internal'
				END as orgType
				from organisation o
				LEFT JOIN BooleanFlagData isinternalbfd on isinternalbfd.entityID = o.organisationid AND isinternalbfd.flagID IN (select flagID from flag where flagtextID='orgDNSisInternal')
				LEFT JOIN BooleanFlagData isresellerbfd on isresellerbfd.entityID = o.organisationid AND isresellerbfd.flagID IN (select flagID from flag where flagtextID='sony1_orgApproved')
				LEFT JOIN BooleanFlagData isdealerbfd on isdealerbfd.entityID = o.organisationid AND isdealerbfd.flagID IN (select flagID from flag where flagtextID='dns_OrgApproved')
				WHERE o.organisationID =  <cf_queryparam value="#qry_get_orgID.organisationID#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfquery>

			<cfquery name="get_directDebitEnabled" datasource="#application.siteDataSource#">
				select distinct(oabg.account) as account from organisationAccountBG oabg
				where oabg.organisationID =  <cf_queryparam value="#qry_get_orgID.organisationID#" CFSQLTYPE="CF_SQL_INTEGER" >  and oabg.businessGroupID IN (select businessgroupID from businessgroup where businessgroupkey =  <cf_queryparam value="#arguments.bg#" CFSQLTYPE="CF_SQL_VARCHAR" >  and businessgrouptypeID=2)
				and oabg.directdebit=1
				and oabg.organisationID not IN (
				select entityID from BooleanFlagData isinternalbfd where isinternalbfd.entityID = oabg.organisationid AND isinternalbfd.flagID IN (select flagID from flag where flagtextID='orgDNSisInternal')
				)
			</cfquery>

			<cfoutput><cfsavecontent variable="accBGXML">
				<businessgroup name="#arguments.BG#" type="#qry_get_org_type.orgType#">
					<account>#arguments.accID#</account>
					<cfif qry_get_org_bgflags.recordcount is 0 and get_directDebitEnabled.recordCount is 0>
						<attributes />
					<cfelse>
						<attributes>
							<cfif get_directDebitEnabled.recordCount neq 0>
								<cfset directDebitEnabled = "">
								<cfloop query="get_directDebitEnabled">
									<cfset directDebitEnabled = listappend(directDebitEnabled,account)>
								</cfloop>
								<attribute name="DirectDebitEnabled" value="#directDebitEnabled#" />
							</cfif>
							<cfloop query="qry_get_org_bgflags">
								<cfif value is "" and flagtypeID is 2>
									<cfset fgValue = "False">
								<cfelseif value is "" and flagtypeID is 3>
									<cfset fgValue = "None">
								<cfelseif value is not "" and flagtypeID is 2>
									<cfset fgValue = "True">
								<cfelse>
									<cfset fgValue = value>
								</cfif>
								<attribute name="#flagGroupTextid#" value="#fgValue#" />
							</cfloop>
						</attributes>
					</cfif>
				</businessgroup>
			</cfsavecontent></cfoutput>
			<cfset statusID=200>
			<cfset statusText="Successful">
		<cfelse>
			<cfset statusID=404>
			<cfset statusText="Account and Business Group Combination Not Found">
			<cfset accBGXML="">
		</cfif>
			<!--- <cfcatch>
				<cfset statusID=400>
				<cfset statusText="Error compiling response, please contact support">
				<cfset accBGXML="">
			</cfcatch>
		</cftry> --->

		<cfscript>
			relayWSXMLResponse = build_XML_Response(
				wsMethod="RELAY_GET_ATTRS_FOR_ACC_BG_RG",
				wsResponseID="#statusID#",
				wsResponseText="#statusText#",
				receivedParameter=receivedParameter,
				relayContent="#accBGXML#");
		</cfscript>

		<cfreturn relayWSXMLResponse>
	</cffunction>

	<cffunction name="RELAY_UPDATEPROFILE" access="remote" returntype="xml" output="false" hint="Updates a User Profile via Iframe">
		<cfargument name="personID" type="numeric" required="false">
		<cfargument name="relayusername" type="String" required="false">
		<cfargument name="locale" type="String" required="true">
		<!--- <cfset var countryCode=listgetat(locale,2,"_")>
		<cfset var languageCode=listgetat(locale,1,"_")>
		<cfset var relayWSiframe=""> --->
		<cfset var relayWSXMLResponse="">

		<cfscript>
			relayWSXMLResponse=RELAY_GETCONTENT(personID=arguments.personID,relayusername=arguments.relayusername,locale=arguments.locale,etid='sony1profile');
		</cfscript>
		<cfset relayWSXMLResponse.relayResponse[1].relayTag[1].xmlAttributes.method="RELAY_UPDATEPROFILE">

		<cfreturn relayWSXMLResponse>
	</cffunction>

	<cffunction name="RELAY_CJMTEMPLATEHOME" access="remote" returntype="xml" output="false" hint="Updates a User Profile via Iframe">
		<cfargument name="personID" type="numeric" required="false">
		<cfargument name="relayusername" type="String" required="false">
		<cfargument name="locale" type="String" required="true">
		<!--- <cfset var countryCode=listgetat(locale,2,"_")>
		<cfset var languageCode=listgetat(locale,1,"_")>
		<cfset var relayWSiframe=""> --->
		<cfset var relayWSXMLResponse="">

		<cfscript>
			relayWSXMLResponse=RELAY_GETCONTENT(personID=arguments.personID,relayusername=arguments.relayusername,locale=arguments.locale,etid='sony1cjm');
		</cfscript>
		<cfset relayWSXMLResponse.relayResponse[1].relayTag[1].xmlAttributes.method="RELAY_CJMTEMPLATEHOME">

		<cfreturn relayWSXMLResponse>
	</cffunction>

	<cffunction name="RELAY_SETRELAYFLAG" access="remote" returntype="xml" output="false" hint="Sets a relay flag against an entity related to person">
		<cfargument name="personID" type="numeric" required="false" default="0">
		<cfargument name="relayusername" type="String" required="false">
		<cfargument name="flagTextIDs" type="String" required="true">
		<cfargument name="justUnsetFlag" type="String" required="false" default="false">
		<cfscript>
			var relayWSXMLResponse="";
			var statusID = 0;
			var statusText = "";
			var userDetailXML = "";
			var qryUserDetails = "";
			var errorFlagTextIDs = "";
			var directDebitEnabled = "";
			var entityID = "";
			receivedParameter=arraynew(2);
			receivedParameter[1][1]="personID";
			receivedParameter[1][2]="#arguments.personID#";
			receivedParameter[2][1]="relayusername";
			receivedParameter[2][2]="#arguments.relayusername#";
			receivedParameter[3][1]="flagTextIDs";
			receivedParameter[3][2]="#arguments.flagTextIDs#";
			receivedParameter[4][1]="justUnsetFlag";
			receivedParameter[4][2]="#arguments.justUnsetFlag#";
		</cfscript>
		<cfif arguments.relayusername is not "">
			<cfquery name="get_personID" datasource="#application.sitedatasource#">
				select personID from person where username =  <cf_queryparam value="#arguments.relayusername#" CFSQLTYPE="CF_SQL_VARCHAR" >
			</cfquery>
			<cfif get_personID.recordcount neq 0>
				<cfset entityID = get_personID.personID>
			</cfif>
		</cfif>
		<cfif arguments.personID is not 0>
			<cfset entityID = arguments.personID>
		</cfif>
		<cfif entityID is not "">
		<cfif listlen(arguments.flagTextIDs) gt 0>
			<cfscript>
				listlength = listLen(arguments.flagTextIDs,"-");
				for(i=1; i lte listlength; i=i+1) {
					theFlagID = listGetAt(arguments.flagTextIDs,i,"-") ;
					if (application.com.flag.doesflagexist(theFlagID)) {
						//
					} else {
						status = 404;
						listappend(errorFlagTextIDs,theFlagID);
					}
				}
				if (statusID eq 0)
				{
					for(j=1; j lte listlength; j=j+1) {
						theFlagID = listGetAt(arguments.flagTextIDs,j,"-") ;
						y = application.com.flag.unsetBooleanFlag(entityID=entityID,flagID=theFlagID);
						if (justUnsetFlag eq false or justUnsetFlag eq ""){
							x = application.com.flag.setBooleanFlag(entityID=entityID,flagTextID=theFlagID);
						}

					}
					statusID = 200;
					statusText = "Success";
				}
				else
				{
					statusText = "#errorFlagTextIDs#";
				}
			</cfscript>
		<cfelse>
			<cfset statusID = 400>
			<cfset statusText  = "Bad Request">
		</cfif>
		<cfelse>
			<cfset statusID = 404>
			<cfset statusText  = "Person Not Found">
		</cfif>
		<cfscript>
			relayWSXMLResponse = build_XML_Response(
				wsMethod="RELAY_SETRELAYFLAG",
				wsResponseID="#statusID#",
				wsResponseText="#statusText#",
				receivedParameter=receivedParameter,
				relayContent="");
		</cfscript>

		<cfreturn relayWSXMLResponse>
	</cffunction>

	<cffunction name="RELAY_MDF" access="remote" returntype="xml" output="false" hint="Updates a User Profile via Iframe">
		<cfargument name="personID" type="numeric" required="false" default="0">
		<cfargument name="relayusername" type="String" required="false">
		<cfargument name="locale" type="String" required="true">
		<!--- <cfset var countryCode=listgetat(locale,2,"_")>
		<cfset var languageCode=listgetat(locale,1,"_")>
		<cfset var relayWSiframe=""> --->
		<cfset var relayWSXMLResponse="">

		<cfscript>
			relayWSXMLResponse=RELAY_GETCONTENT(personID=arguments.personID,relayusername=arguments.relayusername,locale=arguments.locale,etid='sony1mdf');
		</cfscript>
		<cfset relayWSXMLResponse.relayResponse[1].relayTag[1].xmlAttributes.method="RELAY_MDF">

		<cfreturn relayWSXMLResponse>
	</cffunction>

	<cffunction name="RELAY_UNSUBSCRIBE" access="remote" returntype="xml" output="false" hint="Updates a User Profile via Iframe">
		<cfargument name="personID" type="numeric" required="false" default="0">
		<cfargument name="relayusername" type="String" required="false">
		<cfargument name="locale" type="String" required="true">
		<!--- <cfset var countryCode=listgetat(locale,2,"_")>
		<cfset var languageCode=listgetat(locale,1,"_")>
		<cfset var relayWSiframe=""> --->
		<cfset var relayWSXMLResponse="">

		<cfscript>
			relayWSXMLResponse=RELAY_GETCONTENT(personID=arguments.personID,relayusername=arguments.relayusername,locale=arguments.locale,etid='sony1unsubscribe');
		</cfscript>
		<cfset relayWSXMLResponse.relayResponse[1].relayTag[1].xmlAttributes.method="RELAY_UNSUBSCRIBE">

		<cfreturn relayWSXMLResponse>
	</cffunction>

	<cffunction name="RELAY_EVENTS" access="remote" returntype="xml" output="false" hint="Shows Events a Person as Rights to">
		<cfargument name="personID" type="numeric" required="false" default="0">
		<cfargument name="relayusername" type="String" required="false">
		<cfargument name="locale" type="String" required="true">
		<!--- <cfset var countryCode=listgetat(locale,2,"_")>
		<cfset var languageCode=listgetat(locale,1,"_")>
		<cfset var relayWSiframe=""> --->
		<cfset var relayWSXMLResponse="">

		<cfscript>
			relayWSXMLResponse=RELAY_GETCONTENT(personID=arguments.personID,relayusername=arguments.relayusername,locale=arguments.locale,etid='sony1events');
		</cfscript>
		<cfset relayWSXMLResponse.relayResponse[1].relayTag[1].xmlAttributes.method="RELAY_EVENTS">

		<cfreturn relayWSXMLResponse>
	</cffunction>

	<cffunction name="RELAY_SHOWADVERT" access="remote" returntype="xml" output="false" hint="Shows Events a Person as Rights to">
		<cfargument name="locale" type="String" required="true">
		<cfargument name="elementBranchRootID" type="String" required="true">
		<cfargument name="advertMethod" type="String" required="true">
		<cfargument name="startPosition" type="String" required="true">
		<cfargument name="bannerElementIDList" type="String" required="false">


		<cfscript>
			var relayWSXMLResponse="";
			var statusID="";
			var statusText="";
			var advertCode="";
			var preadvertCode="";
			var siteDomain = CGI.SERVER_NAME;
			var relayContent = "";
			var advertWidth="207";
			var advertHeight="117";
			receivedParameter=arraynew(2);
			receivedParameter[1][1]="locale";
			receivedParameter[1][2]="#arguments.locale#";
			receivedParameter[2][1]="elementBranchRootID";
			receivedParameter[2][2]="#arguments.elementBranchRootID#";
			receivedParameter[3][1]="advertMethod";
			receivedParameter[3][2]="#arguments.advertMethod#";
			receivedParameter[4][1]="startPosition";
			receivedParameter[4][2]="#arguments.startPosition#";
			receivedParameter[5][1]="bannerElementIDList";
			receivedParameter[5][2]="#arguments.bannerElementIDList#";
		</cfscript>

		<cfif startPosition is "">
			<cfset startPosition = 0>
		</cfif>

		<cftry>
			<cfset statusID = 200>
			<cfset statusText="Success">
			<cftry>
				<cfset countryCode=listgetat(locale,2,"_")>
				<cfset languageCode=listgetat(locale,1,"_")>
				<cfcatch>
					<cfset countryCode="xxxxx">
					<cfset languageCode="xxxxx">
				</cfcatch>
			</cftry>

			<!--- <cfquery name="qry_get_country" datasource="#application.sitedatasource#">
				select countryID from country where isocode = '#countrycode#'
			</cfquery>

			<cfquery name="qry_get_language" datasource="#application.sitedatasource#">
				select languageID from language where isocode = '#languagecode#'
			</cfquery> --->

			<!--- <cfscript>
				application.com.relayCurrentUser.refresh();
				application.com.login.externalUserLoginProcess(404);
				application.com.relaycurrentuser.setlanguage (language=languageCode);
				application.com.relaycurrentuser.setshowcontentforcountryID(countryID=qry_get_country.countryID);
				request.currentElementTree = application.com.relayElementTree.getTreeForCurrentUser(topElementID=request.currentsite.elementTreeDef.TopID );
				debugInfo.languageid = request.relaycurrentuser.languageid ;
				debugInfo.countryid = request.relaycurrentuser.Content.ShowForCountryID ;
			</cfscript> --->

			<!--- <cfsavecontent variable="advertCode">
				<cfinclude template="\relay\RelayTagTemplates\showAdvert.cfm">
			</cfsavecontent>
			<cfsavecontent variable="preadvertCode">
				<SCRIPT type="text/javascript" src="https://sonydns1ext.staging.relayware.com/javascript/AC_RunActiveContent.js"></script>
			</cfsavecontent> --->

			<!--- <cfsavecontent variable="advertCode"><cf_executeRelayTag ElementID= "" parameters = "suppressJSInclude=true,SITEDOMAIN=#SITEDOMAIN#"><cfoutput>#advertCode#</cfoutput></cf_executeRelayTag></cfsavecontent>
 --->
			<!--- <cfset relayContent = "<![CDATA[#preadvertCode##replace(advertCode,chr(9),"")#]]>"> --->

			<cfsavecontent variable="advertCode">
				<!--- <cfinclude template="../application .cfm"> --->
				<cfinclude template="relaywebservicesINI.cfm">
				<cfset sitedomain = request.ws_iFrameURL>
				<!--- NJH 2008-08-18 CR-SNY651 - change from http to https --->
				<cfoutput><cfset iFrameURL = "https://#request.ws_iFrameURL#/?etid=#arguments.elementBranchRootID#&elementBranchRootID=#arguments.elementBranchRootID#&advertMethod=#arguments.advertMethod#&startPosition=#arguments.startPosition#&bannerElementIDList=#arguments.bannerElementIDList#">
				<![CDATA[<iframe src="#iFrameURL#" name="relayIFrame" id="relayIFrame" width="#advertwidth#" height="#advertheight#" marginwidth="0" marginheight="0" hspace="0" vspace="0" scrolling="no" frameborder="0" style="border: 0px solid ##FFFFFF; width: #advertwidth#px; height: #advertheight#px;"></iframe>]]></cfoutput>
			</cfsavecontent>


			<cfcatch>
				<cfset statusID = 400>
				<cfset statusText="Bad Request">
			</cfcatch>
		</cftry>

		<cfscript>
			relayWSXMLResponse = build_XML_Response(
			wsMethod="RELAY_SHOWADVERT",
			wsResponseID="#statusID#",
			wsResponseText="#statusText#",
			receivedParameter=receivedParameter,
			relayContent="#advertCode#");
		</cfscript>

		<cfreturn relayWSXMLResponse>

	</cffunction>

	<cffunction name="RELAY_GETCONTENT" access="remote" returntype="xml" output="false" hint="Get Relayware Content">
		<cfargument name="personID" type="numeric" required="false" default="0">  <!--- WAB 2009-11-25  LID 2876  now set a default of blank and test    --->
		<cfargument name="relayusername" type="String" required="false" default="">  <!--- WAB 2009-11-25 LID 2876  now set a default of blank and test    --->
		<cfargument name="locale" type="String" required="true">
		<cfargument name="etid" type="String" required="true">

		<cfscript>
			var countryCode="";
			var languageCode="";
			var relayWSiframe="";
			var relayWSXMLResponse="";
			var isWebServiceCall = true;
			var userAuthenticated = false;
			var userLogin = "";
			if ((not isDefined("arguments.personID") or arguments.personID eq 0) and (not isDefined("arguments.relayusername") or arguments.relayusername eq "")) {
				arguments.personID = "404";
			}
			receivedParameter=arraynew(2);
			receivedParameter[1][1]="personID";
			receivedParameter[1][2]="#arguments.personID#";
			receivedParameter[2][1]="relayusername";
			receivedParameter[2][2]="#arguments.relayusername#";
			receivedParameter[3][1]="locale";
			receivedParameter[3][2]="#arguments.locale#";
			receivedParameter[4][1]="etid";
			receivedParameter[4][2]="#arguments.etid#";

			debugInfo = structNew() ;
		</cfscript>

		<cftry>
			<cfset countryCode=listgetat(locale,2,"_")>
			<cfset languageCode=listgetat(locale,1,"_")>
			<cfcatch>
				<cfset countryCode="xxxxx">
				<cfset languageCode="xxxxx">
			</cfcatch>
		</cftry>

		<!---
		2009-11-25 WAB LID 2876  Both arguments now passed through to authenticateWebServiceUser so don't need this bit
		<cfif isDefined("arguments.personID") and arguments.personID neq 0>
			<cfset userLogin = arguments.personID>
		</cfif>

		<cfif isDefined("arguments.relayusername") and arguments.relayusername neq "">
			<cfset userLogin = arguments.relayusername>
		</cfif>
		 --->

		<cfquery name="qry_get_country" datasource="#application.sitedatasource#">
			select countryID from country where isocode =  <cf_queryparam value="#countrycode#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</cfquery>

		<cfquery name="qry_get_language" datasource="#application.sitedatasource#">
			select languageID from language where isocode =  <cf_queryparam value="#languagecode#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</cfquery>

		<cfif qry_get_country.recordcount neq 0 and qry_get_language.recordcount neq 0>

		<!---
			2009-11-25 WAB LID 2876 change authenticateWebServiceUser so is passed personid and relayusername  (one of which will be blank)
			<cfset userAuthenticated = authenticateWebServiceUser(userLogin=userLogin)>
		--->
		<cfset userAuthenticated = authenticateWebServiceUser(personid = arguments.personID, relayusername = arguments.relayusername )>

			<cfset wsResponseText = "">
			<cfif userAuthenticated.StatusID neq 200>
				<cfif userAuthenticated.StatusID is 400>
					<cfset wsResponseText = "Bad Request">
				<cfelse>
					<cfset wsResponseText = "Username Invalid">
				</cfif>

			<cfelse>

				<cfquery name="qry_isValidLocale" datasource="#application.sitedatasource#">
					select * from sonyDivineLocale
					where locale =  <cf_queryparam value="#arguments.locale#" CFSQLTYPE="CF_SQL_VARCHAR" >
				</cfquery>

				<cfif qry_isValidLocale.recordcount is 0>
					<cfset userAuthenticated.statusID = 404>
					<cfset failureinfo = application.com.relayElementTree.whyCantCurrentUserSeeThisElement(topElementID=request.currentsite.elementTreeDef.TopID,elementid=arguments.etid)	>
					<cfset debugInfo["failureInfo"] = failureinfo>
					<cfset wsResponseText = "Content Not Found">

				<cfelse>

					<cfscript>
						application.com.relayCurrentUser.refresh();
						application.com.login.externalUserLoginProcess(userAuthenticated.userDetail.PersonID);
						application.com.relaycurrentuser.setshowcontentforcountryID(countryID=qry_get_country.countryID);
						application.com.relaycurrentuser.setlanguage (language=languageCode);
						getElements = application.com.relayElementTree.getTreeForCurrentUser(topElementID=request.currentsite.elementTreeDef.TopID );
						debugInfo.languageid = request.relaycurrentuser.languageid ;
						debugInfo.countryid = request.relaycurrentuser.Content.ShowForCountryID ;
						debugInfo.elements = valuelist(getelements.node);
					</cfscript>

					<cfif userAuthenticated.statusID eq 200>
						<cfset wsResponseText = "Success">
						<cfsavecontent variable="relayWSiframe">
							<!--- <cfinclude template="../application .cfm"> --->
							<cfinclude template="relaywebservicesINI.cfm">
							<cfset sitedomain = request.ws_iFrameURL>
							<cfset encryptedQueryString = URLEncodedFormat(replace(application.com.encryption.EncryptStringAES(StringToEncrypt="locale=#arguments.locale#&personID=#userAuthenticated.userDetail.PersonID#",PassPhrase=request.wsPassPhrase),"+","||","ALL"))>
							<cfset iFrameURL = "https://#request.ws_iFrameURL#/WebServices/wsRelayTag.cfm?eString=#encryptedQueryString#&loadURL=/&etid=#arguments.etid#&tagArguments=true">
							<cfoutput><![CDATA[<iframe frameborder="0" style="border: 0px solid ##FFFFFF; width: 538px; height: 500px;" name="relayIFrame" id="relayIFrame" width="538" height="500" src="#iFrameURL#"></iframe>]]></cfoutput>
						</cfsavecontent>
					</cfif>
				</cfif>
			</cfif>
		<cfelse>
			<cfset userAuthenticated = structnew()>
			<cfset userAuthenticated.statusID = 403>
			<cfset wsResponseText = "Locale Invalid">
		</cfif>

		<cfscript>
			relayWSXMLResponse = build_XML_Response(
				wsMethod="RELAY_GETCONTENT",
				wsResponseID="#userAuthenticated.statusID#",
				wsResponseText= wsResponseText,
				receivedParameter=receivedParameter,
				relayContent="#relayWSiframe#");
		</cfscript>

		<cfreturn relayWSXMLResponse>
	</cffunction>

	<cffunction name="GET_USERDETAILS" access="remote" returntype="xml" output="false" hint="Get Relayware Content">
		<cfargument name="personID" type="numeric" required="false">
		<cfargument name="relayusername" type="String" required="false">
		<cfscript>
			var relayWSXMLResponse="";
			var qry_get_per_BG = "";
			var qry_get_org_Reseller_BG = "";
			var qry_get_per_UGs = "";
			var currentBGKey = "";
			var qry_get_per_bgflags = "";
			var qry_get_per_resellerbgflags = "";
			var qry_get_org_bgflags = "";
			var get_directDebitEnabled = "";
			var fgValue = "";
			var userDetails = getUserDetails(personID=arguments.personID);
		</cfscript>
		<cfquery name="qry_get_per_BG" datasource="#application.siteDataSource#">
			SELECT     * from personAccountBG pabg
			INNER JOIN person p on pabg.personID = p.personID
			INNER JOIN businessgroup bg On pabg.businessgroupID = bg.businessgroupID
			INNER JOIN booleanflagdata bfd on p.organisationID = bfd.entityID and bfd.flagID IN (select flagID from flag where flagtextID='dns_orgApproved')
			INNER JOIN booleanflagdata bfd2 on p.personID = bfd2.entityID and bfd2.flagID IN (select flagID from flag where flagtextID='dns_perApproved')
			WHERE     pabg.personID=#arguments.personID#
			order by  bg.businessgroupkey
		</cfquery>
		<cfquery name="qry_get_org_Reseller_BG" datasource="#application.siteDataSource#">
			SELECT     * from organisationAccountBG oabg
			INNER JOIN businessgroup bg On oabg.businessgroupID = bg.businessgroupID
			inner join booleanflagdata bfd on oabg.organisationID = bfd.entityID and bfd.flagID IN (select flagID from flag where flagtextID='sony1_orgApproved')
			WHERE     oabg.organisationid =  <cf_queryparam value="#userDetails.organisationID#" CFSQLTYPE="CF_SQL_INTEGER" >  and bg.businessgrouptypeID = 1
		</cfquery>
		<cfquery name="qry_get_per_UGs" datasource="#application.siteDataSource#">
			select Name as ug from rightsGroup rg
			inner join usergroup ug on rg.usergroupid = ug.usergroupID
			where rg.personID = #arguments.personID#
		</cfquery>
		<!--- 2012-12-14 - RMB - P-SNY117 - Remove the DealerNet SSO section from the Login web service - START <cfquery name="qry_get_per_sso" datasource="#application.siteDataSource#">
			select fg.name as ssoname,fg.formattingparameters,fg.flaggroupID,tfd.entityID from TextFlagData tfd inner join flag f on tfd.flagID = f.flagID
			inner join flaggroup fg on f.flaggroupID = fg.flaggroupID and parentFlaggroupID=1577
			where f.name='enabled' and tfd.data = 'true' and entityID = #arguments.personID#
		</cfquery> - 2012-12-14 - RMB - P-SNY117 - Remove the DealerNet SSO section from the Login web service - END --->

		<!--- 2012-09-06 - P-SNY118 - Start --->
		<cfset JobFunction = application.com.flag.getFlagGroupDataForPerson(flaggroupID='JobFunction', PersonID=arguments.personID)>
		<cfset NoCommsTypeALL = application.com.flag.getFlagDataForPerson(flagID='NoCommsTypeALL', PersonID=arguments.personID)>
		<!--- 2012-09-06 - P-SNY118 - End --->

		<cfif userDetails.recordcount gt 0>

				<cfsavecontent variable="relayWSXMLResponse">
					<cfoutput><person username="#userDetails.username#" personID="#arguments.personID#">
						<profile>
							<organisation organisationID="#userDetails.organisationID#" type="#userDetails.orgSystemType#">
								<cfif userDetails.orgtype is ""><type /><cfelse><type>#userDetails.orgType#</type></cfif>
								<cfif userDetails.organisationName is not ""><organisationName><![CDATA[#userDetails.organisationName#]]></organisationName><cfelse><organisationName /></cfif>
								<cfif userDetails.AKA is not ""><alsoKnowAs><![CDATA[#userDetails.AKA#]]></alsoKnowAs><cfelse><alsoKnowAs /></cfif>
								<cfif userDetails.VATNumber is not ""><VATNumber>#userDetails.VATNumber#</VATNumber><cfelse><VATNumber /></cfif>
								<cfif userDetails.orgCountryISO is not ""><countryISO>#userDetails.orgCountryISO#</countryISO><cfelse><countryISO /></cfif>
							</organisation>
							<location locationID="#userDetails.locationID#">
								<cfif userDetails.siteName is not ""><sitename><![CDATA[#userDetails.siteName#]]></sitename><cfelse><sitename /></cfif>
								<cfif userDetails.address1 is not ""><address1><![CDATA[#userDetails.address1#]]></address1><cfelse><address1 /></cfif>
								<cfif userDetails.address2 is not ""><address2><![CDATA[#userDetails.address2#]]></address2><cfelse><address2 /></cfif>
								<cfif userDetails.address3 is not ""><address3><![CDATA[#userDetails.address3#]]></address3><cfelse><address3 /></cfif>
								<cfif userDetails.address4 is not ""><address4><![CDATA[#userDetails.address4#]]></address4><cfelse><address4 /></cfif>
								<cfif userDetails.address5 is not ""><address5><![CDATA[#userDetails.address5#]]></address5><cfelse><address5 /></cfif>
								<cfif userDetails.postalCode is not ""><postalCode>#userDetails.postalCode#</postalCode><cfelse><postalCode /></cfif>
								<cfif userDetails.countryISO is not ""><country>#userDetails.countryISO#</country><cfelse><country /></cfif>
							</location>
							<cfif userDetails.salutation is not ""><salutation><![CDATA[#userDetails.salutation#]]></salutation><cfelse><salutation /></cfif>
							<cfif userDetails.firstName is not ""><firstName><![CDATA[#userDetails.firstName#]]></firstName><cfelse><firstName /></cfif>
							<cfif userDetails.lastName is not ""><lastName><![CDATA[#userDetails.lastName#]]></lastName><cfelse><lastName /></cfif>
							<cfif userDetails.email is not ""><email><![CDATA[#userDetails.email#]]></email><cfelse><email /></cfif>
							<cfif userDetails.siteName is not ""><officePhone><![CDATA[#userDetails.siteName#]]></officePhone><cfelse><officePhone /></cfif>
							<cfif userDetails.mobilePhone is not ""><mobilePhone><![CDATA[#userDetails.mobilePhone#]]></mobilePhone><cfelse><mobilePhone /></cfif>
							<cfif userDetails.faxPhone is not "">	<faxPhone><![CDATA[#userDetails.faxPhone#]]></faxPhone><cfelse><faxPhone /></cfif>
							<cfif userDetails.jobDesc is not ""><jobDescription><![CDATA[#userDetails.jobDesc#]]></jobDescription><cfelse><jobDescription /></cfif>
							<cfif userDetails.locale is not ""><locale>#userDetails.locale#</locale><cfelse><locale /></cfif>
							<cfif userDetails.countryISO is not ""><country>#userDetails.countryISO#</country><cfelse><country /></cfif>
							<cfif userDetails.locationCountryID is not ""><CountryID>#userDetails.locationCountryID#</CountryID><cfelse><CountryID /></cfif>
		<!--- 2012-09-06 - P-SNY118 - Start --->
						<cfif JobFunction.recordcount is 0>
						<JobFunctions />
						<cfelse>
						<JobFunctions>
							<cfloop query="JobFunction"><#flagtextid#>true</#flagtextid#></cfloop>
						</JobFunctions>
						</cfif>

						<cfif JobFunction.recordcount is 0>
						<NoCommsTypeALL>false</NoCommsTypeALL>
						<cfelse>
						<NoCommsTypeALL>true</NoCommsTypeALL>
						</cfif>
		<!--- 2012-09-06 - P-SNY118 - End --->

						</profile>
						</cfoutput>
						<cfif qry_get_per_UGs.recordcount is 0>
						<groups />
						<cfelse>
						<groups>
							<cfoutput query="qry_get_per_UGs"><group><![CDATA[#ug#]]></group></cfoutput>
						</groups>
						</cfif>

						<!--- 2012-12-14 - RMB - P-SNY117 - Remove the DealerNet SSO section from the Login web service - START - <cfif qry_get_per_sso.recordcount is 0>
						<sso />
						<cfelse>
						<sso>
						<cfoutput query="qry_get_per_sso">
						<cfif listlen(formattingparameters) gte 2>
							<cfset ssoURL = replace(listfirst(formattingparameters),"ssoURL=","","All")>
							<cfset ssotype = replace(listgetAt(formattingparameters,2),"ssotype=","","All")>
							<cfquery name="qry_get_per_ssoflags" datasource="#application.siteDataSource#">
								select f.name, tfd.data from TextFlagData tfd inner join flag f on tfd.flagID = f.flagID and f.FlaggroupID =  <cf_queryparam value="#flaggroupID#" CFSQLTYPE="CF_SQL_INTEGER" >
								where not f.name='enabled' and tfd.entityID = #arguments.personID#
							</cfquery>
							<link name="#ssoname#" url="#ssoURL#" type="POST">
								<attributes>
									<cfif qry_get_per_ssoflags.recordcount gt 0>
										<cfloop query="qry_get_per_ssoflags">
											<attribute name="#name#" value="#data#"/>
										</cfloop>
									</cfif>
									<cfif ssotype is 2>
										<attribute name="sso_1_locale" value="${context.locale}"/>
									</cfif>
								</attributes>
							</link>
						</cfif>
						</cfoutput>
						</sso>
						</cfif> 2012-12-14 - RMB - P-SNY117 - Remove the DealerNet SSO section from the Login web service - END --->

						<cfoutput>
						<attributes>
							<attribute name="systemAdmin" value="#userDetails.sysAdmin#"/>
							<attribute name="businessGroupAdmin" value="#userDetails.BGAdmin#"/>
							<attribute name="secondLanguage" value="#userDetails.secondLanguage#"/>
							<attribute name="displayPrice" value="#userDetails.DisplayPrice#"/>
							<attribute name="displaypricedefault" value="#userDetails.DisplayPriceDefault#"/>
							<attribute name="changeDealer" value="#userDetails.changedealer#"/>
							<attribute name="forcePassword" value="#userDetails.passwordChange#"/>
							<attribute name="companyAdmin" value="#userDetails.companyAdmin#"/>
							<!--- NYB 2008-12-04 - CR-SNY662 -> --->
							<attribute name="TsAndCsAccepted" value="#userDetails.TsAndCsAccepted#"/>
							<!--- <- CR-SNY662 --->
						</attributes>
						</cfoutput>
						<cfif qry_get_per_BG.recordcount is 0 and qry_get_org_Reseller_BG.recordcount is 0>
							<businessgroups />
						<cfelse>
							<businessgroups>
								<cfif qry_get_per_BG.recordcount gt 0>
								<cfoutput query="qry_get_per_BG" group="businessgroupID">
								<cfset currentBGKey = businessgroupkey>
								<businessgroup name="#businessgroupkey#" type="<cfif businessgrouptypeID eq 2>D<cfelse>R</cfif>">
									<cfif account gt 0>
									<accounts>
										<cfoutput><account>#account#</account></cfoutput>
									</accounts>
									<cfelse>
										<accounts/>
									</cfif>
									<cfquery name="qry_get_per_bgflags" datasource="#application.siteDataSource#">
										SELECT     replace(f.flagtextID,'#currentBGKey#_','') as flagname, f.flagID, fg.flaggroupID, bfd.created from flag f
										INNER JOIN flaggroup fg ON f.flaggroupID=fg.flaggroupID
										INNER JOIN booleanflagdata bfd ON f.flagID=bfd.flagID and entityID=#arguments.personID#
										WHERE     fg.flaggroupTextID =  <cf_queryparam value="#currentBGKey#_roles" CFSQLTYPE="CF_SQL_VARCHAR" >
									</cfquery>
									<cfif  qry_get_per_bgflags.recordcount is 0>
									<roles />
									<cfelse>
									<roles>
										<cfloop query="qry_get_per_bgflags">
											<role>#flagname#</role>
										</cfloop>
									</roles>
									</cfif>
									<cfquery name="qry_get_org_bgflags" datasource="#application.siteDataSource#">
										select flagGroupid, flagtypeID, replace(flagGroupTextid,'#currentBGKey#_','') as flagGroupTextid, dbo.booleanflaglist (flagGroupid,#userDetails.organisationID#,default) as value
										from flaggroup where flaggroupid IN (select fg.flaggroupID from flaggroup pfg inner join flaggroup fg on fg.parentflaggroupid = pfg.flaggroupid where pfg.flaggroupTextID =  <cf_queryparam value="#currentBGKey#_attributes" CFSQLTYPE="CF_SQL_VARCHAR" > )
									</cfquery>
									<cfquery name="get_directDebitEnabled" datasource="#application.siteDataSource#">
										select distinct(oabg.account) as account from organisationAccountBG oabg
										inner join personAccountBG pabg ON pabg.account = oabg.account
										where oabg.organisationID =  <cf_queryparam value="#userDetails.organisationID#" CFSQLTYPE="CF_SQL_INTEGER" >  and oabg.businessGroupID =  <cf_queryparam value="#businessgroupID#" CFSQLTYPE="CF_SQL_INTEGER" >
										and personID = #personID#
										and directdebit=1
									</cfquery>
									<cfif qry_get_org_bgflags.recordcount is 0 and get_directDebitEnabled.recordCount is 0>
									<attributes />
									<cfelse>
										<attributes>
											<cfif get_directDebitEnabled.recordCount neq 0>
											<cfset directDebitEnabled = "">
											<cfloop query="get_directDebitEnabled">
												<cfset directDebitEnabled = listappend(directDebitEnabled,account)>
											</cfloop>
											<attribute name="DirectDebitEnabled" value="#directDebitEnabled#" />
											</cfif>
											<cfloop query="qry_get_org_bgflags">
												<cfif value is "" and flagtypeID is 2>
													<cfset fgValue = "False">
												<cfelseif value is "" and flagtypeID is 3>
													<cfset fgValue = "None">
												<cfelseif value is not "" and flagtypeID is 2>
													<cfset fgValue = "True">
												<cfelse>
													<cfset fgValue = value>
												</cfif>
												<attribute name="#flagGroupTextid#" value="#fgValue#" />
											</cfloop>
										</attributes>
									</cfif>
								</businessgroup>
								</cfoutput>
								</cfif>
								<cfif qry_get_org_Reseller_BG.recordcount gt 0>
									<cfoutput query="qry_get_org_Reseller_BG" group="businessgroupkey">
								<cfset currentBGKey = businessgroupkey>
								<businessgroup name="#businessgroupkey#" type="<cfif businessgrouptypeID eq 2>d<cfelse>r</cfif>">
									<cfif account gt 0>
									<accounts>
										<cfoutput><account>#account#</account></cfoutput>
									</accounts>
									<cfelse>
										<accounts/>
									</cfif>
									<cfquery name="qry_get_per_bgflags" datasource="#application.siteDataSource#">
										SELECT     replace(f.flagtextID,'#currentBGKey#_','') as flagname, f.flagID, fg.flaggroupID, bfd.created from flag f
										INNER JOIN flaggroup fg ON f.flaggroupID=fg.flaggroupID
										INNER JOIN booleanflagdata bfd ON f.flagID=bfd.flagID and entityID=#arguments.personID#
										WHERE     fg.flaggroupTextID =  <cf_queryparam value="#currentBGKey#_roles" CFSQLTYPE="CF_SQL_VARCHAR" >
									</cfquery>
									<cfif  qry_get_per_bgflags.recordcount is 0>
									<roles />
									<cfelse>
									<roles>
										<cfloop query="qry_get_per_bgflags">
											<role>#flagname#</role>
										</cfloop>
									</roles>
									</cfif>
									<cfquery name="qry_get_per_resellerbgflags" datasource="#application.siteDataSource#">
										select flagGroupid, flagtypeID, replace(flagGroupTextid,'#currentBGKey#_','') as flagGroupTextid, dbo.booleanflaglist (flagGroupid,#userDetails.organisationID#,default) as value
										from flaggroup where flaggroupid IN (select fg.flaggroupID from flaggroup pfg inner join flaggroup fg on fg.parentflaggroupid = pfg.flaggroupid where pfg.flaggroupTextID =  <cf_queryparam value="#currentBGKey#_attributes" CFSQLTYPE="CF_SQL_VARCHAR" > )
									</cfquery>
									<cfif qry_get_per_resellerbgflags.recordcount is 0>
									<attributes />
									<cfelse>
										<attributes>
											<cfloop query="qry_get_per_resellerbgflags">
												<cfif value is "" and flagtypeID is 2>
													<cfset fgValue = "False">
												<cfelseif value is "" and flagtypeID is 3>
													<cfset fgValue = "None">
												<cfelseif value is not "" and flagtypeID is 2>
													<cfset fgValue = "True">
												<cfelse>
													<cfset fgValue = value>
												</cfif>
												<attribute name="#flagGroupTextid#" value="#fgValue#" />
											</cfloop>
										</attributes>
									</cfif>
								</businessgroup>
								</cfoutput>
								</cfif>
							</businessgroups>
						</cfif>
					</person>
				</cfsavecontent>

		</cfif>

		<!--- <cfscript>
			relayWSXMLResponse = build_XML_Response(
				wsMethod="GET_USERDETAILS",
				wsResponseID="#userAuthenticated.statusID#",
				wsResponseText="",
				receivedParameter=receivedParameter,
				relayContent="#userXML#");
		</cfscript> --->
		<cfreturn relayWSXMLResponse>
	</cffunction>

	<cffunction name="build_XML_Response" access="private" output="false" returntype="xml">
		<cfargument name="wsMethod" type="String" required="true">
		<cfargument name="wsResponseID" type="String" required="true">
		<cfargument name="wsResponseText" type="String" required="false">
		<cfargument name="receivedParameter" type="Array" required="false">
		<cfargument name="relayContent" type="string" required="false">

		<cfset var receivedParameters = "">
		<cfset var receivedParameterNo = "">
		<cfset var relayWSXMLResponse = "">
		<cfset var i = "">
		<!--- has a receivedParameter value been passed --->
		<!--- NJH 2010-05-24 LID 3286: Sony relayWebServices Error - use ISOEncode to encode &'s, etc --->
		<cfoutput>
			<cfsavecontent variable="receivedParameters">
		<cfif isArray(receivedParameter)>
			<cfset receivedParameterNo = arraylen(receivedParameter)>
			<cfif receivedParameterNo gt 0>
				<receivedParameters>
				<cfloop index="i" from="1" to="#receivedParameterNo#" step="1">
					<receivedParameter name="#receivedParameter[i][1]#" value="#xmlformat(receivedParameter[i][2])#"></receivedParameter>
				</cfloop>
				</receivedParameters>
			<cfelse>
				<receivedParameters />
			</cfif>
		<cfelse>
			<receivedParameters />
		</cfif>
		</cfsavecontent>
		</cfoutput>

		<!--- WAB 2012-03- rejigged to remove excess white space (probably not important - actuall the leading whitespace which causes problems) --->
		<cfsaveContent variable="relayWSXMLResponseXML"><cfoutput><?xml version='1.0' encoding='utf-8'?>
					<relayResponse>
						<relayTag method="#htmleditformat(wsMethod)#">
							<status responseid="#htmleditformat(wsResponseID)#"><![CDATA[#htmleditformat(wsResponseText)#]]></status>
							#receivedParameters#
							<cfif relayContent is "">
							<content />
							<cfelse>
							<content>
								#relayContent#
							</content>
							</cfif>
						</relayTag>
					</relayResponse>
				</cfoutput>
		</cfsaveContent>

		<cfxml casesensitive="false" variable="relayWSXMLResponse"><cfoutput>#application.com.regExp.removeWhiteSpace(relayWSXMLResponseXML)#</cfoutput></cfxml>
		<cfreturn relayWSXMLResponse>
	</cffunction>

<!---
		2009-11-25  WAB
		Was problem with this function if the username was numeric.
		It tried to match against personid rather than the username
		Therefore changed whole function so that it took arguments personid and relayusername so that we always know which has been passed
--->


	<cffunction access="private" name="authenticateWebServiceUser" output="false" returntype="any">
		<!--- 		<cfargument name="userLogin" required="true"> --->
		<cfargument name="personID" type="numeric" required="false" default="0">
		<cfargument name="relayusername" type="String" required="false" default="">
		<cfargument name="datasource" default="#application.sitedatasource#">
		<cfset var authenticationDetail = structNew()>
		<cfset var userDetail = "">

		<!--- 	WAB 2009-11-25 moved into middle of query in the if statement!
		<cfset authenticationDetail.userLogin = arguments.userLogin>
		--->


		<cfquery name="userDetail" datasource="#datasource#">
			select	*
			from	person
			where
					<cfif arguments.relayusername is not "">
						<cfset authenticationDetail.userLogin = arguments.relayusername>
						username =  <cf_queryparam value="#arguments.relayusername#" CFSQLTYPE="CF_SQL_VARCHAR" >
					<cfelseif arguments.personid is not 0>
						<cfset authenticationDetail.userLogin = arguments.personid>
							personID = #arguments.personid#
					<cfelse>
						1 = 0   <!--- need this so that if neither is passed in then the authentication fails --->
					</cfif>
			<!---
				<!--- if it is numeric, then check it as a personID, if it is not, then check it as a username --->
				<cfif isNumeric(userLogin)>
					personID = #arguments.userLogin#
				<cfelse>
					username = N'#arguments.userLogin#'
				</cfif>
			 --->


		</cfquery>

		<cfset authenticationDetail.userDetail = userDetail>

		<cfif userDetail.recordcount eq 0>
			<cfset authenticationDetail.statusID = 403>
		<cfelseif userDetail.recordcount gt 1>
			<cfset authenticationDetail.statusID = 400>
		<cfelse>
			<cfset authenticationDetail.statusID = 200>
		</cfif>

		<cfreturn authenticationDetail>

	</cffunction>

	<cffunction name="getUserDetails" access="private" output="false" returntype="query">
		<cfargument name="personID" type="numeric" required="true">

		<cfset var userDetails = "">

		<cfquery name="userDetails" datasource="#application.sitedatasource#">
			select	*, c.isoCode as countryISO, oc.isoCode as orgcountryISO, c.countryID AS 'locationCountryID', <!--- lg.isoCode as languageISO, --->
			case when isinternalbfd.flagid IS NULL THEN
			'External'
			ELSE 'Internal'
			END as orgSystemType,

			case when isresellerbfd.flagID IS NOT NULL AND isdealerbfd.flagID IS NOT NULL THEN
				'Hybrid'
			ELSE
				case when isresellerbfd.flagID IS NOT NULL THEN
					'Reseller'
				ELSE
					case when isdealerbfd.flagID IS NOT NULL THEN
						'Dealer'
					ELSE
						''
					END
				END
			END as orgType,
			case when tfdSecondLanguage.Data IS NOT NULL THEN
				tfdSecondLanguage.Data
			ELSE
				'en'
			END as secondLanguage,
			case when changedealerbfd.flagID IS NOT NULL THEN
				'True'
			ELSE
				'False'
			END as changeDealer,
			case when perDisplayPricebfd.flagID IS NOT NULL THEN
				'True'
			ELSE
				'False'
			END as DisplayPrice,
			case when PerdisplayPriceDefaultbfd.flagID IS NOT NULL THEN
				'True'
			ELSE
				'False'
			END as DisplayPriceDefault,
			case when ifdcompanyAdmin.flagID IS NOT NULL THEN
				'True'
			ELSE
				'False'
			END as companyAdmin,
			case when PerChangePasswordbfd.flagID IS NOT NULL THEN
				'True'
			ELSE
				'False'
			END as passwordChange,
			case when isSysAdmin.personID IS NOT NULL THEN
				'True'
			ELSE
				'False'
			END as sysAdmin,
			case when isBGAdmin.personID IS NOT NULL THEN
				'True'
			ELSE
				'False'
			END as BGAdmin,
			<!--- NYB 2008-12-04 - CR-SNY662 -> --->
			case when TermsAndConditions.flagID IS NOT NULL THEN
				'True'
			ELSE
				'False'
			END as TsAndCsAccepted,
			<!--- <- CR-SNY662 --->
			tfdDNSLocaleAtt.data as locale
			from	person p inner join
					location l on p.locationid = l.locationid inner join
					organisation o on p.organisationid = o.organisationid inner join
					country c on c.countryID = l.countryID inner join
					country oc on oc.countryID = o.countryID<!--- inner join
					 language lg on lower(lg.language) = lower(p.language) --->
					LEFT JOIN BooleanFlagData isinternalbfd on isinternalbfd.entityID = o.organisationid AND isinternalbfd.flagID IN (select flagID from flag where flagtextID='orgDNSisInternal')
					LEFT JOIN BooleanFlagData isresellerbfd on isresellerbfd.entityID = o.organisationid AND isresellerbfd.flagID IN (select flagID from flag where flagtextID='sony1_orgApproved')
					LEFT JOIN RightsGroup isSysAdmin on isSysAdmin.personID = p.personID AND isSysAdmin.UserGroupID = 1279
					LEFT JOIN RightsGroup isBGAdmin on isBGAdmin.personID = p.personID AND isBGAdmin.UserGroupID = 1277
					LEFT JOIN BooleanFlagData isdealerbfd on isdealerbfd.entityID = o.organisationid AND isdealerbfd.flagID IN (select flagID from flag where flagtextID='dns_OrgApproved')
					LEFT JOIN BooleanFlagData changedealerbfd on changedealerbfd.entityID = p.personid AND changedealerbfd.flagID IN (select flagID from flag where flagtextID='perDNSchangeDealer')
					LEFT JOIN BooleanFlagData perDisplayPricebfd on perDisplayPricebfd.entityID = p.personid AND perDisplayPricebfd.flagID IN (select flagID from flag where flagtextID='perDisplayPrice')
					LEFT JOIN BooleanFlagData PerdisplayPriceDefaultbfd on PerdisplayPriceDefaultbfd.entityID = p.personid AND PerdisplayPriceDefaultbfd.flagID IN (select flagID from flag where flagtextID='PerdisplayPriceDefault')
					LEFT JOIN BooleanFlagData PerChangePasswordbfd on PerChangePasswordbfd.entityID = p.personid AND PerChangePasswordbfd.flagID IN (select flagID from flag where flagtextID='PerChangePassword')
					<!--- NYB 2008-12-04 - CR-SNY662 -> --->
					LEFT JOIN BooleanFlagData TermsAndConditions on TermsAndConditions.entityID = p.personid AND TermsAndConditions.flagID IN (select flagID from flag where flagtextID='Sony1TCAgree')
					<!--- <- CR-SNY662 --->
					LEFT JOIN TextFlagData tfdSecondLanguage on tfdSecondLanguage.entityID = p.personid AND tfdSecondLanguage.flagID IN (select flagID from flag where flagtextID='perSecondLanguage')
					LEFT JOIN TextFlagData tfdDNSLocaleAtt on tfdDNSLocaleAtt.entityID = p.personid AND tfdDNSLocaleAtt.flagID IN (select flagID from flag where flagtextID='DNSLocaleAtt')
					LEFT JOIN integerFlagData ifdcompanyAdmin on ifdcompanyAdmin.entityID = o.organisationid AND ifdcompanyAdmin.flagID IN (select flagID from flag where flagtextID='OrgCompanyAdmin') AND ifdcompanyAdmin.data = p.personid
			where	p.personid = #arguments.personID#
		</cfquery>

		<cfreturn userDetails>
	</cffunction>

	<cffunction name="RELAY_FORGOTTENPASSWORD" access="remote" output="false" returntype="XML" hint="Authentication Services for RelayWS">
		<cfargument name="paramUsernameOrEmail" type="string" required="true">

	    <cfset var checkRequestValue = "#paramUsernameOrEmail#">
	    <cfset var checkRequestType = application.com.email.isValidEmail(emailAddress = checkRequestValue)>
		<cfset var byTypeResponse = "">
		<cfset var byUserResponse = "">
		<cfset var ResponseXML = "">
		<cfset var userXML = "">

		<cfscript>
			forgottenStruct = structNew();
			forgottenStruct.Request.UserRequestType = '' ;
			forgottenStruct.Request.UserRequestValue = '#checkRequestValue#';
			forgottenStruct.Request.UsersFound = '' ;
			forgottenStruct.Request.RequestStatusCode = '' ;
			forgottenStruct.Request.RequestStatusText = '' ;
			forgottenStruct.Request.ValidUserEmail = '' ;
			forgottenStruct.Request.personId = '' ;
			forgottenStruct.Request.firstName = '' ;
			forgottenStruct.Request.lastName = '' ;
			forgottenStruct.Request.locale = '' ;
		</cfscript>

			<cfif checkRequestType>

				<cfscript>
					forgottenStruct.Request.UserRequestType = 'Email' ;
				</cfscript>

			<cfelse>

				<cfscript>
					forgottenStruct.Request.UserRequestType = 'UserName' ;
				</cfscript>

			</cfif>

			<cfquery name="checkPersonByType"  datasource = "#application.sitedatasource#">
			SELECT count(*) AS 'CheckCount1' FROM person
			WHERE 1=1
			<cfif #forgottenStruct.request.UserRequestType# NEQ ''>
				AND #forgottenStruct.request.UserRequestType# = N'#forgottenStruct.request.UserRequestValue#'
			</cfif>
			</cfquery>

			<cfset byTypeResponse = checkPersonByType.CheckCount1>

				<cfscript>
					forgottenStruct.Request.UsersFound = '#byTypeResponse#' ;
				</cfscript>


				<cfif byTypeResponse LTE 0>

					<cfscript>
						forgottenStruct.Request.RequestStatusCode = '404' ;
						forgottenStruct.Request.RequestStatusText = 'No Record Found' ;
					</cfscript>


				<cfelseif byTypeResponse GTE 2>

					<cfscript>
						forgottenStruct.Request.RequestStatusCode = '409' ;
						forgottenStruct.Request.RequestStatusText = 'Duplicate Records Found' ;
					</cfscript>

				<cfelse>

					<cfquery name="checkPersonByType"  datasource = "#application.sitedatasource#">
					SELECT personId, email, firstName, lastName,
					tfdDNSLocaleAtt.data as locale
					FROM person p
					LEFT JOIN TextFlagData tfdDNSLocaleAtt on tfdDNSLocaleAtt.entityID = p.personid AND tfdDNSLocaleAtt.flagID IN (select flagID from flag where flagtextID='DNSLocaleAtt')
					WHERE 1=1
					<cfif #forgottenStruct.request.UserRequestType# NEQ ''>
						AND p.#forgottenStruct.request.UserRequestType# = N'#forgottenStruct.request.UserRequestValue#'
					</cfif>
					</cfquery>

					<cfset DoCheck = application.com.login.isPersonAValidUserOnGivenSite(personid = checkPersonByType.personId, domain=CGI.HTTP_HOST)>

					<cfif DoCheck.ISVALIDUSER>
						<cfset byUserResponse = "#checkPersonByType.email#">
					<cfelse>
						<cfset byUserResponse = "">
					</cfif>

						<cfif byUserResponse IS ''>

							<cfscript>
								forgottenStruct.Request.RequestStatusCode = '401' ;
								forgottenStruct.Request.RequestStatusText = 'Unauthorized User' ;
							</cfscript>

						<cfelse>

							<cfif forgottenStruct.Request.UserRequestType EQ 'UserName'>

								<cfquery name="checkPersonByEmail"  datasource = "#application.sitedatasource#">
								SELECT count(*) AS 'CheckCount1' FROM person
								WHERE 1=1
									AND Email  =  <cf_queryparam value="#byUserResponse#" CFSQLTYPE="CF_SQL_VARCHAR" >
								</cfquery>

								<cfif checkPersonByEmail.CheckCount1 GTE 2>

									<cfscript>
										forgottenStruct.Request.RequestStatusCode = '409' ;
										forgottenStruct.Request.RequestStatusText = 'VALID USER with Duplicate Email Records Found' ;
										forgottenStruct.Request.ValidUserEmail = byUserResponse ;
										forgottenStruct.Request.personId = checkPersonByType.personId ;
										forgottenStruct.Request.firstName = checkPersonByType.firstName ;
										forgottenStruct.Request.lastName = checkPersonByType.lastName ;
										forgottenStruct.Request.locale = checkPersonByType.locale ;
									</cfscript>

								<cfelse>

									<cfscript>
										forgottenStruct.Request.RequestStatusCode = '200' ;
										forgottenStruct.Request.RequestStatusText = 'Successful' ;
										forgottenStruct.Request.ValidUserEmail = byUserResponse ;
										forgottenStruct.Request.personId = checkPersonByType.personId ;
										forgottenStruct.Request.firstName = checkPersonByType.firstName ;
										forgottenStruct.Request.lastName = checkPersonByType.lastName ;
										forgottenStruct.Request.locale = checkPersonByType.locale ;
									</cfscript>

								</cfif>


							<cfelse>

								<cfscript>
									forgottenStruct.Request.RequestStatusCode = '200' ;
									forgottenStruct.Request.RequestStatusText = 'Successful' ;
									forgottenStruct.Request.ValidUserEmail = byUserResponse ;
									forgottenStruct.Request.personId = checkPersonByType.personId ;
									forgottenStruct.Request.firstName = checkPersonByType.firstName ;
									forgottenStruct.Request.lastName = checkPersonByType.lastName ;
									forgottenStruct.Request.locale = checkPersonByType.locale ;
								</cfscript>

							</cfif>

						</cfif>

				</cfif>

		<cfoutput>
		<cfsavecontent variable="ResponseXML">
			<Response>
				<UsersFound>#forgottenStruct.Request.UsersFound#</UsersFound>
				<RequestStatusCode>#forgottenStruct.Request.RequestStatusCode#</RequestStatusCode>
				<RequestStatusText><![CDATA[#forgottenStruct.Request.RequestStatusText#]]></RequestStatusText>
				<ValidPersonId>#forgottenStruct.Request.personId#</ValidPersonId>
				<ValidFirstName><![CDATA[#forgottenStruct.Request.firstName#]]></ValidFirstName>
				<ValidLastName><![CDATA[#forgottenStruct.Request.lastName#]]></ValidLastName>
				<ValidUserEmail><![CDATA[#forgottenStruct.Request.ValidUserEmail#]]></ValidUserEmail>
				<ValidLocale>#forgottenStruct.Request.locale#</ValidLocale>
			</Response>
		</cfsavecontent>
		</cfoutput>

		<cfscript>
			relayWSXMLResponse = build_XML_Response(
				wsMethod="RELAY_FORGOTTENPASSWORD",
				wsResponseID="#forgottenStruct.Request.RequestStatusCode#",
				wsResponseText="#forgottenStruct.Request.RequestStatusText#",
				receivedParameter=build_ReceivedParameters(argumentCollection=arguments),
				relayContent="#ResponseXML#");
		</cfscript>

		<cfreturn relayWSXMLResponse>

	</cffunction>

	<cffunction name="RELAY_VALIDATEPASSWORD" access="remote" output="false" returntype="XML" hint="Authentication Services for RelayWS">
		<cfargument name="paramPersonID" type="numeric" required="true">
		<cfargument name="paramPassword" type="string" required="true">

		<cfset var GETUSERDETAILS = "">
		<cfset var userXML = "">
		<cfset var userDetailXML = "">
		<cfset var DoCheck = "">
		<cfset var DoPwCheck = "">
		<cfset var PasswordChangeStatus = "">
		<cfset var LoginStatus = "">

		<cfset var checkPersonById = "">
		<cfset var checkPersonByEmail = "">
		<cfscript>
			var forgottenStruct = structNew();
			forgottenStruct.Request.UserRequestPersonID = #paramPersonID#;
			forgottenStruct.Request.UserRequestPassword = '#paramPassword#';
			forgottenStruct.Request.RequestStatusCode = '406' ;
			forgottenStruct.Request.RequestStatusText = 'Not Acceptable' ;
			forgottenStruct.Request.ValidUserStatus = 'false' ;
			forgottenStruct.Request.ValidUserEmail = 'false' ;
			forgottenStruct.Request.ValidUserDetails = 'false' ;
			forgottenStruct.Request.ValidPasswordStatus = 'false' ;
			forgottenStruct.Request.ValidPasswordText = 'PASSWORD NOT CHANGED' ;
		</cfscript>

			<cfquery name="checkPersonById"  datasource = "#application.sitedatasource#">
			SELECT personId, email, userName, password FROM person
			WHERE personId = #paramPersonID#
			</cfquery>

			<cfif checkPersonById.recordcount LTE 0>

					<cfscript>
						forgottenStruct.Request.RequestStatusCode = '404' ;
						forgottenStruct.Request.RequestStatusText = 'Unknown User ID' ;
					</cfscript>

			<cfelse>

			<cfset DoCheck = application.com.login.isPersonAValidUserOnGivenSite(personid = checkPersonById.personId, domain=CGI.HTTP_HOST)>

				<cfif NOT DoCheck.ISVALIDUSER>

					<cfscript>
						forgottenStruct.Request.RequestStatusCode = '401' ;
						forgottenStruct.Request.RequestStatusText = 'Unauthorized User' ;
					</cfscript>

				<cfelse>

					<cfscript>
						forgottenStruct.Request.ValidUserStatus = DoCheck.ISVALIDUSER ;
					</cfscript>

								<cfquery name="checkPersonByEmail"  datasource = "#application.sitedatasource#">
								SELECT count(*) AS 'CheckCount1' FROM person
								WHERE 1=1
									AND Email  =  <cf_queryparam value="#checkPersonById.email#" CFSQLTYPE="CF_SQL_VARCHAR" >
								</cfquery>

								<cfif checkPersonByEmail.CheckCount1 GTE 2>

									<cfscript>
										forgottenStruct.Request.RequestStatusCode = '409' ;
										forgottenStruct.Request.RequestStatusText = 'VALID USER with Duplicate Email Records Found' ;
										forgottenStruct.Request.ValidUserEmail = 'false' ;
									</cfscript>

								<cfelse>

									<cfscript>
										forgottenStruct.Request.ValidUserEmail = '#checkPersonById.email#' ;
									</cfscript>

								</cfif>

				</cfif>

			</cfif>

			<cfif forgottenStruct.Request.ValidUserStatus>

				<cfset GETUSERDETAILS = GET_USERDETAILS(personID=paramPersonID)>

				<cfoutput>
					<cfsavecontent variable="userDetailXML">
						#GETUSERDETAILS#
					</cfsavecontent>
				</cfoutput>



				<cfset DoPwCheck = application.com.login.checkPasswordComplexity(password=paramPassword).isoK>

				<cfscript>
				forgottenStruct.Request.ValidUserDetails = GETUSERDETAILS ;
				forgottenStruct.Request.ValidPasswordStatus = DoPwCheck ;
				</cfscript>

				<cfif forgottenStruct.Request.ValidPasswordStatus>

					<cfset PasswordChangeStatus = application.com.login.changeUserPassword(userName=checkPersonById.userName, currentPassword =checkPersonById.password, newPassword1=paramPassword, newPassword2=paramPassword, passwordEncrypted='true')>

					<cfscript>
					forgottenStruct.Request.ValidPasswordText = PasswordChangeStatus.MESSAGE ;
					</cfscript>

					<cfif PasswordChangeStatus.MESSAGE EQ ''>
						<cfset LoginStatus = RELAY_LOGINMAIN(relayusername=checkPersonById.userName, relaypassword=paramPassword)>

							<cfscript>
							forgottenStruct.Request.RequestStatusCode = '200' ;
							forgottenStruct.Request.RequestStatusText = 'Successful' ;
							forgottenStruct.Request.LoginStatus = LoginStatus ;
							</cfscript>

					</cfif>

				</cfif>

			</cfif>

		<cfoutput>
		<cfsavecontent variable="ResponseXML">
			<Response>
				<UserRequestPersonID>#forgottenStruct.Request.UserRequestPersonID#</UserRequestPersonID>
				<UserRequestPassword>#forgottenStruct.Request.UserRequestPassword#</UserRequestPassword>
				<RequestStatusCode>#forgottenStruct.Request.RequestStatusCode#</RequestStatusCode>
				<RequestStatusText>#forgottenStruct.Request.RequestStatusText#</RequestStatusText>
				<ValidUserStatus>#forgottenStruct.Request.ValidUserStatus#</ValidUserStatus>
				<ValidUserEmail>#forgottenStruct.Request.ValidUserEmail#</ValidUserEmail>
				<ValidPasswordStatus>#forgottenStruct.Request.ValidPasswordStatus#</ValidPasswordStatus>
				<ValidPasswordText>#forgottenStruct.Request.ValidPasswordText#</ValidPasswordText>
				<UserDetails>
					#userDetailXML#
				</UserDetails>
			</Response>
		</cfsavecontent>
		</cfoutput>

		<cfscript>
			relayWSXMLResponse = build_XML_Response(
				wsMethod="RELAY_VALIDATEPASSWORD",
				wsResponseID="#forgottenStruct.Request.RequestStatusCode#",
				wsResponseText="#forgottenStruct.Request.RequestStatusText#",
				receivedParameter=build_ReceivedParameters(argumentCollection=arguments),
				relayContent="#ResponseXML#");
		</cfscript>

		<cfreturn relayWSXMLResponse>

	</cffunction>

	<cffunction name="RELAY_PORTALREGISTRATION" access="remote" output="false" returntype="XML" hint="PORTAL REGISTRATION Services for RelayWS">
		<cfargument name="paramRequest" type="string" required="true">

		<!--- 2012/05/08 IH Case 428085 URL decode the xml file --->
		<cfset var XMLRequest = "#XmlParse(urlDecode(ARGUMENTS.PARAMREQUEST))#">

		<cfscript>
			var paramStruct = structNew();
			var responseStruct = structNew();
			var DebugStruct = structNew();
			var personDetails = structNew();

			var countryidlookupfromisocodestr = application.countryidlookupfromisocodestr;
			var languageisocodelookupstr = application.languageisocodelookupstr;
			var languageLookupFromIDstr = application.languageLookupFromIDstr;
			var languagefindkey="";

			var wsResponseID="400";
			var wsResponseText="NOT RUN";
			var PasswordCheck="";
			var EmailCheck01="";
			var EmailCheck02="";
			var VATnumberCheck="";

			var UserCountry="";
			var UserLanguage="";
			var UseCompanyCountry="";

		    var xmlSalutation = XmlSearch(XMLRequest, "/registrationDetails/userDetails/salutation");
		    var xmlFirstName = XmlSearch(XMLRequest, "/registrationDetails/userDetails/firstName");
		    var xmlLastName = XmlSearch(XMLRequest, "/registrationDetails/userDetails/lastName");
		    var xmlEmail = XmlSearch(XMLRequest, "/registrationDetails/userDetails/email");
		    var xmlDirectLine = XmlSearch(XMLRequest, "/registrationDetails/userDetails/telephoneNumber");
		    var xmlCountry = XmlSearch(XMLRequest, "/registrationDetails/userDetails/userCountry");
		    var xmlLanguage = XmlSearch(XMLRequest, "/registrationDetails/userDetails/language");
		    var xmlJobFunctions = XmlSearch(XMLRequest, "/registrationDetails/userDetails/job");
		    var xmlPassword = XmlSearch(XMLRequest, "/registrationDetails/userDetails/password");
		    var xmlNoCommsTypeALL = XmlSearch(XMLRequest, "/registrationDetails/userDetails/subscribeNewsletters");
		    var xmlSony1Iagreechoice = XmlSearch(XMLRequest, "/registrationDetails/userDetails/agreement");
		    var xmlSony1Iagree = XmlSearch(XMLRequest, "/registrationDetails/userDetails/agreement");

		    var xmlSAPAccountNumber = XmlSearch(XMLRequest, "/registrationDetails/companyDetails/sapAccountNumber");
		    var xmlCompanyName = XmlSearch(XMLRequest, "/registrationDetails/companyDetails/companyName");
		    var xmlCompanyAddress1 = XmlSearch(XMLRequest, "/registrationDetails/companyDetails/address1");
		    var xmlCompanyAddress2 = XmlSearch(XMLRequest, "/registrationDetails/companyDetails/address2");
		    var xmlCompanyAddress3 = XmlSearch(XMLRequest, "/registrationDetails/companyDetails/address3");
		    var xmlCity = XmlSearch(XMLRequest, "/registrationDetails/companyDetails/city");
		    var xmlPostCode = XmlSearch(XMLRequest, "/registrationDetails/companyDetails/postalCode");
		    var xmlCompanyCountry = XmlSearch(XMLRequest, "/registrationDetails/companyDetails/companyCountry");
		    var xmlWebAddress = XmlSearch(XMLRequest, "/registrationDetails/companyDetails/webAddress");
		    var xmlBusinessType = XmlSearch(XMLRequest, "/registrationDetails/companyDetails/department");
		    var xmlTCIagreeSony1 = XmlSearch(XMLRequest, "/registrationDetails/companyDetails/b2bProgramRules");
		    var xmlVATnumber = XmlSearch(XMLRequest, "/registrationDetails/companyDetails/VATnumber");

			var CompanyCountryDetails="";
			var orgMatchKey="";
			var updateExistingOrgs="";
			var checkOrg="";
			var orgMatchReplace="";

			var updateExistingLocs="";
			var checkLoc="";
			var thisLocID="";
			var thisLocAction="";
			var locMatchKeyList="";
			var locMatchName="";

			var personInsResult="";
			var thisPerAction="";
			var thisPersonID="";

			var GetFlagIDS="";
			var SetUpFlag="";

			paramStruct.Salutation="";
			paramStruct.FirstName="";
			paramStruct.LastName="";
			paramStruct.Email="";
			paramStruct.DirectLine="";
			paramStruct.Password="";
			paramStruct.Language="";
			paramStruct.Sony1Iagreechoice="false";
			paramStruct.Sony1Iagree="false";
			paramStruct.NoCommsTypeALL="false";
			paramStruct.TCIagreeSony1="false";
			paramStruct.JobFunctions="";
			paramStruct.SAPAccountNumber="";
			paramStruct.CompanyName="";
			paramStruct.CompanyAddress1="";
			paramStruct.CompanyAddress2="";
			paramStruct.CompanyAddress3="";
			paramStruct.City="";
			paramStruct.PostCode="";
			paramStruct.CompanyCountry="";
			paramStruct.Country="";
			paramStruct.WebAddress="";
			paramStruct.BusinessType="";
			paramStruct.VATnumber="";

			responseStruct.MainStatus="400";
			responseStruct.MainStatusText="NOT RUN";
			responseStruct.PasswordStatus="";
			responseStruct.PasswordStatusText="";
			responseStruct.EmailStatus="";
			responseStruct.EmailStatusText="";
			responseStruct.FirstNameStatus="";
			responseStruct.FirstNameStatusText="";
			responseStruct.LastNameStatus="";
			responseStruct.LastNameStatusText="";
			responseStruct.CompanyAddress1Status="";
			responseStruct.CompanyAddress1StatusText="";
			responseStruct.CompanyNameStatus="";
			responseStruct.CompanyNameStatusText="";
			responseStruct.UserCountryStatus="";
			responseStruct.UserCountryStatusText="";
			responseStruct.UserCountryId="";
			responseStruct.UserLanguageStatus="";
			responseStruct.UserLanguageStatusText="";
			responseStruct.UserLanguageID="";
			responseStruct.UserCompanyCountryStatus="";
			responseStruct.UserCompanyCountryStatusText="";
			responseStruct.UserCompanyCountryId="";
			responseStruct.UserVATnumberStatus="";
			responseStruct.UserVATnumberStatusText="";
			responseStruct.orgMatchKey="";
			responseStruct.locMatchName="";

			responseStruct.thisOrgID="";
			responseStruct.thisOrgAction="";
			responseStruct.commitIt="";
			responseStruct.message="";

			responseStruct.thisLocID="";
			responseStruct.thisLocAction="";

			responseStruct.thisPerAction="";
			responseStruct.thisPersonID="";
			responseStruct.sapAdded="";
			responseStruct.sapAddedResponce="";
			responseStruct.sapAddedStatus="";
		</cfscript>

		<cfscript>
		    for (i = 1; i LTE ArrayLen(xmlSalutation); i = i + 1)
		    paramStruct.Salutation = xmlSalutation[i].XmlText;

		    for (i = 1; i LTE ArrayLen(xmlFirstName); i = i + 1)
		    paramStruct.FirstName = xmlFirstName[i].XmlText;

		    for (i = 1; i LTE ArrayLen(xmlLastName); i = i + 1)
		    paramStruct.LastName = xmlLastName[i].XmlText;

		    for (i = 1; i LTE ArrayLen(xmlEmail); i = i + 1)
		    paramStruct.Email = xmlEmail[i].XmlText;

		    for (i = 1; i LTE ArrayLen(xmlDirectLine); i = i + 1)
		    paramStruct.DirectLine = xmlDirectLine[i].XmlText;

		    for (i = 1; i LTE ArrayLen(xmlPassword); i = i + 1)
		    paramStruct.Password = xmlPassword[i].XmlText;

		    for (i = 1; i LTE ArrayLen(xmlLanguage); i = i + 1)
		    paramStruct.Language = xmlLanguage[i].XmlText;

		    for (i = 1; i LTE ArrayLen(xmlSony1Iagreechoice); i = i + 1)
		    paramStruct.Sony1Iagreechoice = xmlSony1Iagreechoice[i].XmlText;

		    for (i = 1; i LTE ArrayLen(xmlSony1Iagree); i = i + 1)
		    paramStruct.Sony1Iagree = xmlSony1Iagree[i].XmlText;

		    for (i = 1; i LTE ArrayLen(xmlNoCommsTypeALL); i = i + 1)
		    paramStruct.NoCommsTypeALL = xmlNoCommsTypeALL[i].XmlText;

		    for (i = 1; i LTE ArrayLen(xmlTCIagreeSony1); i = i + 1)
		    paramStruct.TCIagreeSony1 = xmlTCIagreeSony1[i].XmlText;

		    for (i = 1; i LTE ArrayLen(xmlJobFunctions); i = i + 1)
		    paramStruct.JobFunctions = xmlJobFunctions[i].XmlText;

		    for (i = 1; i LTE ArrayLen(xmlSAPAccountNumber); i = i + 1)
		    paramStruct.SAPAccountNumber = xmlSAPAccountNumber[i].XmlText;

		    for (i = 1; i LTE ArrayLen(xmlCompanyName); i = i + 1)
		    paramStruct.CompanyName = xmlCompanyName[i].XmlText;

		    for (i = 1; i LTE ArrayLen(xmlCompanyAddress1); i = i + 1)
		    paramStruct.CompanyAddress1 = xmlCompanyAddress1[i].XmlText;

		    for (i = 1; i LTE ArrayLen(xmlCompanyAddress2); i = i + 1)
		    paramStruct.CompanyAddress2 = xmlCompanyAddress2[i].XmlText;

		    for (i = 1; i LTE ArrayLen(xmlCompanyAddress3); i = i + 1)
		    paramStruct.CompanyAddress3 = xmlCompanyAddress3[i].XmlText;

		    for (i = 1; i LTE ArrayLen(xmlCity); i = i + 1)
		    paramStruct.City = xmlCity[i].XmlText;

		    for (i = 1; i LTE ArrayLen(xmlPostCode); i = i + 1)
		    paramStruct.PostCode = xmlPostCode[i].XmlText;

		    for (i = 1; i LTE ArrayLen(xmlCompanyCountry); i = i + 1)
		    paramStruct.CompanyCountry = xmlCompanyCountry[i].XmlText;

		    for (i = 1; i LTE ArrayLen(xmlCountry); i = i + 1)
		    paramStruct.Country = xmlCountry[i].XmlText;

		    for (i = 1; i LTE ArrayLen(xmlWebAddress); i = i + 1)
		    paramStruct.WebAddress = xmlWebAddress[i].XmlText;

		    for (i = 1; i LTE ArrayLen(xmlBusinessType); i = i + 1)
		    paramStruct.BusinessType = xmlBusinessType[i].XmlText;

		    for (i = 1; i LTE ArrayLen(xmlVATnumber); i = i + 1)
		    paramStruct.VATnumber = xmlVATnumber[i].XmlText;
		</cfscript>

    	<!--- FirstName Check Start --->
	    <cfif LTRIM(RTRIM(paramStruct.FirstName)) NEQ ''>
			<cfset responseStruct.FirstNameStatus = "200">
			<cfset responseStruct.FirstNameStatusText = "First Name Ok">
		<cfelse>
			<cfset responseStruct.FirstNameStatus = "406">
			<cfset responseStruct.FirstNameStatusText = "First Name Not Acceptable">
		</cfif>
    	<!--- FirstName Check End --->

    	<!--- LastName Check Start --->
	    <cfif LTRIM(RTRIM(paramStruct.LastName)) NEQ ''>
			<cfset responseStruct.LastNameStatus = "200">
			<cfset responseStruct.LastNameStatusText = "Last Name Ok">
		<cfelse>
			<cfset responseStruct.LastNameStatus = "406">
			<cfset responseStruct.LastNameStatusText = "Last Name Not Acceptable">
		</cfif>
    	<!--- LastName Check End --->

    	<!--- Address1 Check Start --->
	    <cfif LTRIM(RTRIM(paramStruct.CompanyAddress1)) NEQ ''>
			<cfset responseStruct.CompanyAddress1Status = "200">
			<cfset responseStruct.CompanyAddress1StatusText = "Company Address1 Ok">
		<cfelse>
			<cfset responseStruct.CompanyAddress1Status = "406">
			<cfset responseStruct.CompanyAddress1StatusText = "Company Address1 Not Acceptable">
		</cfif>
    	<!--- Address1 Check End --->

    	<!--- CompanyName Check Start --->
	    <cfif LTRIM(RTRIM(paramStruct.CompanyName)) NEQ ''>
			<cfset responseStruct.CompanyNameStatus = "200">
			<cfset responseStruct.CompanyNameStatusText = "Company Name Ok">
		<cfelse>
			<cfset responseStruct.CompanyNameStatus = "406">
			<cfset responseStruct.CompanyNameStatusText = "Company Name Not Acceptable">
		</cfif>
    	<!--- CompanyName Check End --->

    	<!--- Email Check Start --->
	    <cfif LTRIM(RTRIM(paramStruct.Email)) NEQ ''>
		    <cfset EmailCheck01 = application.com.email.isValidEmail(emailAddress = paramStruct.Email)>
		    <cfif EmailCheck01>
				<cfquery name="EmailCheck02"  datasource = "#application.sitedatasource#">
				SELECT count(*) AS 'CheckCount1' FROM person
				WHERE 1=1 AND Email  =  <cf_queryparam value="#paramStruct.Email#" CFSQLTYPE="CF_SQL_VARCHAR" >
				</cfquery>
				<cfif EmailCheck02.CheckCount1 EQ 0>
					<cfset responseStruct.EmailStatus = "200">
					<cfset responseStruct.EmailStatusText = "Email Ok">
				<cfelse>
					<cfset responseStruct.EmailStatus = "403">
					<cfset responseStruct.EmailStatusText = "Email Already Used">
				</cfif>
			<cfelse>
				<cfset responseStruct.EmailStatus = "406">
				<cfset responseStruct.EmailStatusText = "Email Not Acceptable">
		    </cfif>
		<cfelse>
			<cfset responseStruct.EmailStatus = "406">
  			<cfset responseStruct.EmailStatusText = "Email Not Acceptable">
		</cfif>
		<!--- Email Check End --->

		<!--- Password Check Start --->
		<cfset PasswordCheck = application.com.login.checkPasswordComplexity(password=paramStruct.Password)>

		<cfif PasswordCheck.isOK>
			<cfset responseStruct.PasswordStatus = "200">
			<cfset responseStruct.PasswordStatusText = "Password Ok">
		<cfelse>
			<cfset responseStruct.PasswordStatus = "406">
			<cfset responseStruct.PasswordStatusText = "Password Not Acceptable:" & PasswordCheck.reason>
		</cfif>
		<!--- Password Check End --->

		<!--- Person Country Check Start --->
	    <cfif StructKeyExists(countryidlookupfromisocodestr, "#paramStruct.COUNTRY#")>
 			<cfset responseStruct.UserCountryStatus = "200">
			<cfset responseStruct.UserCountryStatusText = "Person Country Ok">
			<cfset UserCountry = #countryidlookupfromisocodestr[paramStruct.COUNTRY]#>
			<cfset responseStruct.UserCountryId = UserCountry>
	    <cfelse>
 			<cfset responseStruct.UserCountryStatus = "406">
			<cfset responseStruct.UserCountryStatusText = "Person Country Not Found">
	    </cfif>
		<!--- Person Country Check End --->

		<!--- Person Language Check Start --->
		<cfset languagefindkey = StructFindValue(languageisocodelookupstr, "#paramStruct.LANGUAGE#")>

		<cfif ArrayIsEmpty(languagefindkey) NEQ 'YES'>
			<cfif StructKeyExists(languagefindkey[1], "key")>
				<cfset responseStruct.UserLanguageCode = paramStruct.LANGUAGE>
	 			<cfset responseStruct.UserLanguageStatus = "200">
				<cfset responseStruct.UserLanguageStatusText = "Person Language Ok">
				<cfset UserLanguageStep1 = #languagefindkey[1].key#>
				<cfset UserLanguage = "#languageLookupFromIDstr[UserLanguageStep1]#">
				<cfset responseStruct.UserLanguageID = UserLanguage>
		    <cfelse>
	 			<cfset responseStruct.UserLanguageStatus = "406">
				<cfset responseStruct.UserLanguageStatusText = "Person Language Not Acceptable">
			</cfif>
		<cfelse>
 			<cfset responseStruct.UserLanguageStatus = "406">
			<cfset responseStruct.UserLanguageStatusText = "Person Language Not Acceptable">
		</cfif>
		<!--- Person Language Check End --->

		<!--- Company Country Check Start --->
	    <cfif StructKeyExists(countryidlookupfromisocodestr, "#paramStruct.COMPANYCOUNTRY#")>
 			<cfset responseStruct.UserCompanyCountryStatus = "200">
			<cfset responseStruct.UserCompanyCountryStatusText = "Company Country Ok">
			<cfset UserCompanyCountry = #countryidlookupfromisocodestr[paramStruct.COMPANYCOUNTRY]#>
			<cfset responseStruct.UserCompanyCountryId = UserCompanyCountry>
	    <cfelse>
 			<cfset responseStruct.UserCompanyCountryStatus = "406">
			<cfset responseStruct.UserCompanyCountryStatusText = "Company Country Not Acceptable">
	    </cfif>
		<!--- Company Country Check End --->

			responseStruct.UserVATnumberStatus="";
			responseStruct.UserVATnumberStatusText="";

		<!--- VATnumber Check Start --->
	    <cfif StructKeyExists(countryidlookupfromisocodestr, "#paramStruct.COMPANYCOUNTRY#")>

			<cfset CompanyCountryDetails = application.com.commonQueries.getCountry(countryid=responseStruct.UserCompanyCountryId)>

			<cfif CompanyCountryDetails.VATrequired>

			    <cfif LTRIM(RTRIM(paramStruct.VATnumber)) NEQ ''>
		 			<cfset responseStruct.UserVATnumberStatus = "200">
					<cfset responseStruct.UserVATnumberStatusText = "VAT Number Ok">
				<cfelse>
					<cfset responseStruct.UserVATnumberStatus = "406">
		  			<cfset responseStruct.UserVATnumberStatusText = "VAT Number Not Acceptable">
				</cfif>

			<cfelse>
	 			<cfset responseStruct.UserVATnumberStatus = "200">
				<cfset responseStruct.UserVATnumberStatusText = "VAT Number Ok">
			</cfif>

	    <cfelse>
 			<cfset responseStruct.UserVATnumberStatus = "406">
			<cfset responseStruct.UserVATnumberStatusText = "No Company Country - Can't Check VAT Rules">
	    </cfif>
		<!--- VATnumber Check End --->


		<cfif responseStruct.EmailStatus EQ 200 AND
				responseStruct.PasswordStatus EQ 200 AND
				responseStruct.UserCountryStatus EQ 200 AND
				responseStruct.UserLanguageStatus EQ 200 AND
				responseStruct.UserCompanyCountryStatus EQ 200 AND
				responseStruct.FirstNameStatus EQ 200 AND
				responseStruct.LastNameStatus EQ 200 AND
				responseStruct.CompanyAddress1Status EQ 200 AND
				responseStruct.CompanyNameStatus EQ 200 AND
				responseStruct.UserVATnumberStatus EQ 200>

	<!---   **********************************************************************
			insert the organisation record - START
	************************************************************************* --->

				<!--- <cfif CompanyCountryDetails.VATrequired>
					<cfset orgMatchKey = paramStruct.CompanyName & " #application.delim1# " & paramStruct.VATnumber>
				<cfelseif isdefined("request.orgMatchNameMethodID") and request.orgMatchNameMethodID eq 2>
					<cfset orgMatchKey = replace("#paramStruct.WebAddress#","http://","")>
				 <!--- 2011-05-19	NAS	LID5984 --->
				<cfelseif isdefined("request.useVATinMatchName") and request.useVATinMatchName>
					<cfset orgMatchKey = paramStruct.CompanyName & " #application.delim1# " & paramStruct.VATnumber>
				<cfelse>
					<cfset orgMatchKey = paramStruct.CompanyName>
				</cfif>

				<cfset responseStruct.orgMatchKey = "#orgMatchKey#">

				<cfscript>
					updateExistingOrgs = application.com.dbTools.updateOrgMatchname(countryIDlist=responseStruct.UserCompanyCountryId);
				</cfscript>

				<cfscript>
					checkOrg = application.com.dbTools.checkOrgMatchname(application.siteDataSource,responseStruct.orgMatchKey,responseStruct.UserCompanyCountryId);
				</cfscript>

 				<cfif checkOrg.recordcount GTE 1>
					<cfset responseStruct.thisOrgID = checkOrg.organisationID>
					<cfset responseStruct.thisOrgAction = "found">
				<cfelse>
					<cfset responseStruct.thisOrgAction = "Try To Add">
					<cftry>
						<cfscript>
							responseStruct.thisOrgID = application.com.relayDataload.insertOrganisation(
								CountryID="#responseStruct.UserCompanyCountryId#",
								organisationName="N'#paramStruct.CompanyName#'",
								MatchName="#responseStruct.orgMatchKey#",
								orgURL="'#paramStruct.WebAddress#'",
								Active=1,
								VATnumber="'#paramStruct.VATnumber#'"
							);
						</cfscript>
						<cfset responseStruct.thisOrgAction = "added">
						<cfcatch type = "DATABASE">
						    <cfset responseStruct.commitIt = "No">
							<cfset responseStruct.message = responseStruct.message & cfcatch.message & " QueryName:insNewOrganisation">
						</cfcatch>
					</cftry>
					<!--- add the dataSource --->
					<cftry>
						<CFQUERY NAME="insOrgDataSource" datasource="#application.siteDataSource#">
							INSERT INTO OrgDataSource(organisationID, DataSourceID, RemoteDataSourceID)
							VALUES(<cf_queryparam value="#responseStruct.thisOrgID#" CFSQLTYPE="cf_sql_integer" >,1,'0')</CFQUERY>
						<cfcatch type = "DATABASE">
						    <cfset responseStruct.commitIt = "No">
							<cfset responseStruct.message = responseStruct.message & cfcatch.message & " QueryName:insOrgDataSource">
						</cfcatch>
					</cftry>

				</cfif> --->

				<!--- 2016/09/01		NJH JIRA PROD2016-1190 - replaced old matching/dataload code with call to new matching functions and call to relayEntity.--->
				<cfset var orgDetails = {CountryID=responseStruct.UserCompanyCountryId,
								organisationName=paramStruct.CompanyName,
								orgURL=paramStruct.WebAddress,
								Active=1,
								VATnumber=paramStruct.VATnumber}>
				<cfset var insertOrgResult = application.com.relayEntity.insertOrganisation(organisationDetails=orgDetails)>
				<cfset responseStruct.thisOrgID = insertOrgResult.entityID>
				<cfset responseStruct.thisOrgAction = insertOrgResult.errorCode eq "ENTITY_EXISTS"?"found":"added">
				<cfset responseStruct.message = insertOrgResult.message>
				<cfset responseStruct.commitIt = insertOrgResult.isOK>

	<!---   **********************************************************************
			insert the organisation record - END
	************************************************************************* --->

	<!---   **********************************************************************
			insert the location record - START
	************************************************************************* --->

	<!--- <cfset updateExistingLocs = application.com.dbTools.updateLocationMatchname(organisationIDlist=responseStruct.thisOrgID)>
	<cfset checkLoc = application.com.dbTools.checkLoc(SiteName=paramStruct.CompanyName,
														Address1=paramStruct.CompanyAddress1,
														Address4=paramStruct.City,
														PostalCode=paramStruct.PostCode,
														OrgID=responseStruct.ThisOrgId,
														countryid=responseStruct.UserCountryId)>

	<cfif checkLoc.recordcount eq 1>
		<cfset responseStruct.thisLocID = checkLoc.locationID>
		<cfset responseStruct.thisLocAction = "found">
	<cfelse>

		<cftry>

			<cfset locMatchKeyList=application.com.dbTools.getLocationMatchnameEntries()>

			<cfset locMatchName = paramStruct.CompanyAddress1 & " #application.delim1# " & paramStruct.City & " #application.delim1# " & paramStruct.PostCode>

			<cfset responseStruct.locMatchName = "#locMatchName#">

			<cfscript>
				responseStruct.thisLocID = application.com.relayDataload.insertLocation(
					Address1="N'#paramStruct.CompanyAddress1#'",
					Address2="N'#paramStruct.CompanyAddress2#'",
					Address3="N'#paramStruct.CompanyAddress3#'",
					Address4="N'#paramStruct.City#'",
					PostalCode="N'#paramStruct.PostCode#'",
					CountryID="#responseStruct.UserCountryId#",
					OrganisationID="#responseStruct.ThisOrgId#",
					SiteName="N'#paramStruct.CompanyName#'",
					Telephone="'#paramStruct.DirectLine#'",
					Active=1,
					MatchName="#responseStruct.locMatchName#"
				);
			</cfscript>

			<cfset responseStruct.thisLocAction = "added">
			<cfcatch type = "DATABASE">
			    <cfset responseStruct.commitIt = "No">
				<cfset responseStruct.message = responseStruct.message & cfcatch.message & " QueryName:insNewLocation">
			</cfcatch>
		</cftry>
		<cftry>
			<CFQUERY NAME="insLocDataSource" DATASOURCE="#application.siteDataSource#">
				INSERT INTO LocationDataSource(LocationID, DataSourceID, RemoteDataSourceID)
				VALUES(<cf_queryparam value="#responseStruct.thisLocID#" CFSQLTYPE="cf_sql_integer" >,1,'0')
			</CFQUERY>
			<cfcatch type = "DATABASE">
			    <cfset responseStruct.commitIt = "No">
				<cfset responseStruct.message = responseStruct.message & cfcatch.message & " QueryName:insLocDataSource">
			</cfcatch>
		</cftry>

	</cfif> --->

	<!--- 2016/09/01		NJH JIRA PROD2016-1190 - replaced old matching/dataload code with call to new matching functions and call to relayEntity. --->
	<cfset locationDetails = {Address1=paramStruct.CompanyAddress1,
					Address2=paramStruct.CompanyAddress2,
					Address3=paramStruct.CompanyAddress3,
					Address4=paramStruct.City,
					PostalCode=paramStruct.PostCode,
					CountryID=responseStruct.UserCountryId,
					OrganisationID=responseStruct.ThisOrgId,
					SiteName=paramStruct.CompanyName,
					Telephone=paramStruct.DirectLine,
					Active=1}>


	<cfset var locInsertResult = application.com.relayEntity.insertLocation(locationDetails=locationDetails)>
	<cfset responseStruct.thisLocID = locInsertResult.entityID>
	<cfset responseStruct.thisLocAction = locInsertResult.errorCode eq "ENTITY_EXISTS"?"found":"added">
	<cfset responseStruct.message = locInsertResult.message>
	<cfset responseStruct.commitIt = locInsertResult.isOK>

	<!---   **********************************************************************
			insert the location record - END
	************************************************************************* --->

	<!---   **********************************************************************
			insert the person record - START
	************************************************************************* --->
		<!--- first check whether we have this person in the database --->
		<!--- NYB 2009-04-30 Sophos - replaced actually query with function call: --->
		<!---
		RMB - 2012-02-27 0 Took out line below as this dose nothing
		updateExistingPers = application.com.dbTools.updatePersonMatchname(countryIDlist=variables.countryid);
		 --->
		<!--- NJH 2010-10-25 P-FNL079 used insertPerson function in relayEntity in an effort to consolidate code --->
		<cftry>
		<cfset personDetails.Salutation = paramStruct.Salutation>
		<cfset personDetails.FirstName = paramStruct.FirstName>
		<cfset personDetails.LastName = paramStruct.LastName>
		<cfset personDetails.Username = paramStruct.FirstName & ' ' & paramStruct.LastName>
		<cfset personDetails.Password = application.com.login.encryptRelayPassword(username = personDetails.Username, password = paramStruct.Password)>
		<cfset personDetails.PasswordDate = now()>
		<cfset personDetails.LoginExpires = CreateODBCDate("01-Jan-92")>
		<cfset personDetails.FirstTimeUser = 1>
		<cfset personDetails.OfficePhone = paramStruct.DirectLine>
		<cfset personDetails.Email = paramStruct.Email>
		<cfset personDetails.Language = responseStruct.UserLanguageID>
		<cfset personDetails.LocationID = responseStruct.thisLocID>
		<cfset personDetails.OrganisationID = responseStruct.ThisOrgId>
		<cfset personDetails.Active = 1>

		<cfset personInsResult = application.com.relayEntity.insertPerson(personDetails=personDetails,insertIfExists="false")>
		<cfif personInsResult.exists and listLen(personInsResult.entitiesMatchedList) eq 1>
			<cfset responseStruct.thisPerAction = "found">
		<cfelse>
			<cfset responseStruct.thisPerAction = "added">
		</cfif>
		<cfset responseStruct.thisPersonID = personInsResult.entityID>

		 	<cfcatch type = "DATABASE">
			    <cfset responseStruct.commitIt = "No">
				<cfset responseStruct.message = responseStruct.message & cfcatch.message & " QueryName:insNewPerson">
			</cfcatch>
		</cftry>
	<!---   **********************************************************************
			insert the person record - END
	************************************************************************* --->

	<!---   **********************************************************************
			insert the flag records - START
	************************************************************************* --->

			<!---   **********************************************************************
					person flag records - START
			************************************************************************* --->

			<cfif responseStruct.thisPerAction EQ 'added'>

			<cfif LTRIM(RTRIM(paramStruct.JobFunctions)) NEQ ''>

				<CFQUERY NAME="GetFlagIDS" DATASOURCE="#application.siteDataSource#">
				 SELECT * FROM dbo.vFlagDef WHERE FlagGroupTextID = 'JobFunction' AND
				 Flag  IN ( <cf_queryparam value="#paramStruct.JobFunctions#" CFSQLTYPE="cf_sql_varchar"  list="true"> ) AND
				 flagTextID IS NOT NULL AND
				 flagTextID != ''
				</CFQUERY>

				<cfoutput query="GetFlagIDS">

					<cfset SetUpFlag = application.com.flag.setBooleanFlag(entityID = responseStruct.thisPersonID, flagTextID = '#FLAGTEXTID#')>

				</cfoutput>

			</cfif>

			<cfif LTRIM(RTRIM(paramStruct.NoCommsTypeALL)) NEQ ''>

				<cfif paramStruct.NoCommsTypeALL>

					<cfset SetUpFlag = application.com.flag.setBooleanFlag(entityID = responseStruct.thisPersonID, flagTextID = 'NoCommsTypeALL')>

				</cfif>

			</cfif>

			<cfif LTRIM(RTRIM(paramStruct.Sony1Iagreechoice)) NEQ ''>

				<cfif paramStruct.Sony1Iagreechoice>

					<cfset SetUpFlag = application.com.flag.setBooleanFlag(entityID = responseStruct.thisPersonID, flagTextID = 'Sony1Iagreechoice')>

				</cfif>

			</cfif>

			<cfif LTRIM(RTRIM(paramStruct.Sony1Iagree)) NEQ ''>

				<cfif paramStruct.Sony1Iagree>

					<cfset SetUpFlag = application.com.flag.setBooleanFlag(entityID = responseStruct.thisPersonID, flagTextID = 'Sony1Iagree')>

				</cfif>

			</cfif>

			<cfif LTRIM(RTRIM(paramStruct.TCIagreeSony1)) NEQ ''>

				<cfif paramStruct.TCIagreeSony1>

					<cfset SetUpFlag = application.com.flag.setBooleanFlag(entityID = responseStruct.thisPersonID, flagTextID = 'Sony1TCAgree')>

				</cfif>

			</cfif>

				<cfif LTRIM(RTRIM(paramStruct.SAPAccountNumber)) NEQ ''>

				<cfset SetUpFlag = application.com.flag.setBooleanFlag(entityID = responseStruct.thisPersonID, flagTextID = 'dns_perApplied')>

				<cfelse>

				<cfset SetUpFlag = application.com.flag.setBooleanFlag(entityID = responseStruct.thisPersonID, flagTextID = 'sony1_perApplied')>

				</cfif>

			</cfif>

			<!---   **********************************************************************
					person flag records - END
			************************************************************************* --->

			<!---   **********************************************************************
					organisation flag records - START
			************************************************************************* --->

			<cfif responseStruct.thisOrgAction EQ 'added'>

			<cfif LTRIM(RTRIM(paramStruct.BusinessType)) NEQ ''>

				<CFQUERY NAME="GetFlagIDS" DATASOURCE="#application.siteDataSource#">
				 SELECT * FROM dbo.vFlagDef WHERE FlagGroupTextID = 'RegistrationBusinessType' AND
				 Flag  IN ( <cf_queryparam value="#paramStruct.BusinessType#" CFSQLTYPE="cf_sql_varchar"  list="true"> ) AND
				 flagTextID IS NOT NULL AND
				 flagTextID != ''
				</CFQUERY>

				<cfoutput query="GetFlagIDS">

					<cfset SetUpFlag = application.com.flag.setBooleanFlag(entityID = responseStruct.thisOrgID, flagTextID = '#FLAGTEXTID#')>

				</cfoutput>

			</cfif>

			<cfif LTRIM(RTRIM(paramStruct.SAPAccountNumber)) NEQ ''>

					<cfset SetUpFlag = application.com.flag.setFlagData(entityID = responseStruct.thisOrgID, flagId = 'SAPACCOUNTCODE', data = paramStruct.SAPAccountNumber)>
					<cfset SetUpFlag = application.com.flag.setBooleanFlag(entityID = responseStruct.thisOrgID, flagTextID = 'dns_orgApplied')>

					<cfif isnumeric(paramStruct.SAPAccountNumber)>
					<cftry>
						<cfset responseStruct.sapAdded = application.com.DN_SAP_Account.add_Organisation_SAP(organisationID=responseStruct.thisOrgID,SAPAccount=paramStruct.SAPAccountNumber)>
						<cfcatch>
							<cfset responseStruct.sapAddedStatus = 406>
							<cfset responseStruct.sapAddedResponce = "Error calling Dealernet SAP Account.">
						</cfcatch>
					</cftry>
						<cfif ISDEFINED('responseStruct.sapAdded.status') AND responseStruct.sapAdded.status>
							<cfset responseStruct.sapAddedStatus = 200>
							<cfset responseStruct.sapAddedResponce = "SAP Account Number Successful">
						<cfelse>
							<cfset responseStruct.sapAddedStatus = 406>
							<cfset responseStruct.sapAddedResponce = "The Account information cannot be retrieved from SAP. This could be due to an invalid SAP Account being entered.">
			</cfif>
					<cfelse>
						<cfset responseStruct.sapAddedStatus = 406>
						<cfset responseStruct.sapAddedResponce = "SAP Account Number Invalid">
			</cfif>
				<cfelse>
				<cfset SetUpFlag = application.com.flag.setBooleanFlag(entityID = responseStruct.thisOrgID, flagTextID = 'sony1_orgApplied')>
					<cfset responseStruct.sapAddedStatus = 200>
					<cfset responseStruct.sapAddedResponce = "No SAP Account Number">
				</cfif>

			</cfif>

			<!---   **********************************************************************
					organisation flag records - END
			************************************************************************* --->

	<!---   **********************************************************************
			insert the flag records - END
	************************************************************************* --->

			<cfset wsResponseID = "200">
			<cfset wsResponseText = "Successful">
			<cfset responseStruct.MainStatus = "#wsResponseID#">
			<cfset responseStruct.MainStatusText = "#wsResponseText#">

		<cfelse>

			<cfset responseStruct.MainStatus = "400">
			<cfset responseStruct.MainStatusText = "Faild:">

				<cfif responseStruct.FirstNameStatus NEQ 200>
					<cfset responseStruct.MainStatusText = "#responseStruct.MainStatusText# / #responseStruct.FirstNameStatusText#">
				</cfif>

				<cfif responseStruct.LastNameStatus NEQ 200>
					<cfset responseStruct.MainStatusText = "#responseStruct.MainStatusText# / #responseStruct.LastNameStatusText#">
				</cfif>

				<cfif responseStruct.CompanyAddress1Status NEQ 200>
					<cfset responseStruct.MainStatusText = "#responseStruct.MainStatusText# / #responseStruct.CompanyAddress1StatusText#">
				</cfif>

				<cfif responseStruct.CompanyNameStatus NEQ 200>
					<cfset responseStruct.MainStatusText = "#responseStruct.MainStatusText# / #responseStruct.CompanyNameStatusText#">
				</cfif>

				<cfif responseStruct.EmailStatus NEQ 200>
					<cfset responseStruct.MainStatusText = "#responseStruct.MainStatusText# / #responseStruct.EmailStatusText#">
				</cfif>

				<cfif responseStruct.PasswordStatus NEQ 200>
					<cfset responseStruct.MainStatusText = "#responseStruct.MainStatusText# / #responseStruct.PasswordStatusText#">
				</cfif>

				<cfif responseStruct.UserCountryStatus NEQ 200>
					<cfset responseStruct.MainStatusText = "#responseStruct.MainStatusText# / #responseStruct.UserCountryStatusText#">
				</cfif>

				<cfif responseStruct.UserLanguageStatus NEQ 200>
					<cfset responseStruct.MainStatusText = "#responseStruct.MainStatusText# / #responseStruct.UserLanguageStatusText#">
				</cfif>

				<cfif responseStruct.UserCompanyCountryStatus NEQ 200>
					<cfset responseStruct.MainStatusText = "#responseStruct.MainStatusText# / #responseStruct.UserCompanyCountryStatusText#">
				</cfif>

				<cfif responseStruct.UserVATnumberStatus NEQ 200>
					<cfset responseStruct.MainStatusText = "#responseStruct.MainStatusText# / #responseStruct.UserVATnumberStatusText#">
				</cfif>

		</cfif>

		<cfoutput>
		<cfsavecontent variable="ResponseXML">
			<Response>
				<STATUS>#responseStruct.MainStatus#</STATUS>
				<STATUSTEXT>#responseStruct.MainStatusText#</STATUSTEXT>
				<CHECKS>
					<FIRSTNAMESTATUS>#responseStruct.FIRSTNAMESTATUS#</FIRSTNAMESTATUS>
					<FIRSTNAMESTATUSTEXT>#responseStruct.FIRSTNAMESTATUSTEXT#</FIRSTNAMESTATUSTEXT>
					<LASTNAMESTATUS>#responseStruct.LASTNAMESTATUS#</LASTNAMESTATUS>
					<LASTNAMESTATUSTEXT>#responseStruct.LASTNAMESTATUSTEXT#</LASTNAMESTATUSTEXT>
					<COMPANYNAMESTATUS>#responseStruct.COMPANYNAMESTATUS#</COMPANYNAMESTATUS>
					<COMPANYNAMESTATUSTEXT>#responseStruct.COMPANYNAMESTATUSTEXT#</COMPANYNAMESTATUSTEXT>
					<COMPANYADDRESS1STATUS>#responseStruct.COMPANYADDRESS1STATUS#</COMPANYADDRESS1STATUS>
					<COMPANYADDRESS1STATUSTEXT>#responseStruct.COMPANYADDRESS1STATUSTEXT#</COMPANYADDRESS1STATUSTEXT>
					<EMAILSTATUS>#responseStruct.EMAILSTATUS#</EMAILSTATUS>
					<EMAILSTATUSTEXT>#responseStruct.EMAILSTATUSTEXT#</EMAILSTATUSTEXT>
					<PASSWORDSTATUS>#responseStruct.PASSWORDSTATUS#</PASSWORDSTATUS>
					<PASSWORDSTATUSTEXT>#responseStruct.PASSWORDSTATUSTEXT#</PASSWORDSTATUSTEXT>
					<USERCOUNTRYSTATUS>#responseStruct.USERCOUNTRYSTATUS#</USERCOUNTRYSTATUS>
					<USERCOUNTRYSTATUSTEXT>#responseStruct.USERCOUNTRYSTATUSTEXT#</USERCOUNTRYSTATUSTEXT>
					<USERLANGUAGESTATUS>#responseStruct.USERLANGUAGESTATUS#</USERLANGUAGESTATUS>
					<USERLANGUAGESTATUSTEXT>#responseStruct.USERLANGUAGESTATUSTEXT#</USERLANGUAGESTATUSTEXT>
					<USERCOMPANYCOUNTRYSTATUS>#responseStruct.USERCOMPANYCOUNTRYSTATUS#</USERCOMPANYCOUNTRYSTATUS>
					<USERCOMPANYCOUNTRYSTATUSTEXT>#responseStruct.USERCOMPANYCOUNTRYSTATUSTEXT#</USERCOMPANYCOUNTRYSTATUSTEXT>
					<USERVATNUMBERSTATUS>#responseStruct.USERVATNUMBERSTATUS#</USERVATNUMBERSTATUS>
					<USERVATNUMBERSTATUSTEXT>#responseStruct.USERVATNUMBERSTATUSTEXT#</USERVATNUMBERSTATUSTEXT>
					<USERSAPNUMBERSTATUS>#responseStruct.SAPADDEDSTATUS#</USERSAPNUMBERSTATUS>
					<USERSAPNUMBERSTATUSTEXT>#responseStruct.SAPADDEDRESPONCE#</USERSAPNUMBERSTATUSTEXT>
				</CHECKS>
				<ACTIONS>
					<THISORGACTION>#responseStruct.THISORGACTION#</THISORGACTION>
					<THISORGID>#responseStruct.THISORGID#</THISORGID>
					<THISLOCACTION>#responseStruct.THISLOCACTION#</THISLOCACTION>
					<THISLOCID>#responseStruct.THISORGID#</THISLOCID>
					<THISPERACTION>#responseStruct.THISPERACTION#</THISPERACTION>
					<THISPERSONID>#responseStruct.THISPERSONID#</THISPERSONID>
				</ACTIONS>
			</Response>
		</cfsavecontent>
		</cfoutput>

		<cfscript>
			relayWSXMLResponse = build_XML_Response(
				wsMethod="RELAY_PORTALREGISTRATION",
				wsResponseID="#wsResponseID#",
				wsResponseText="#wsResponseText#",
				receivedParameter=build_ReceivedParameters(argumentCollection=arguments),
				relayContent="#ResponseXML#");
		</cfscript>

 		<!--- <cffile action="delete" file="D:\Web\dump.html"> --->

		<cfset DebugStruct.XMLRequest = XMLRequest>
		<cfset DebugStruct.paramStruct = paramStruct>
		<cfset DebugStruct.responseStruct = responseStruct>

		<cfreturn relayWSXMLResponse>

	</cffunction>


	<!--- 2012-09-03 - SNY-118 - RMB Added New Start --->

	<cffunction name="RELAY_VALIDATEANDCHANGEPASSWORD" access="remote" output="false" returntype="XML" hint="Authentication Services for RelayWS">
		<cfargument name="paramPersonID" type="numeric" required="true">
		<cfargument name="paramPassword" type="string" required="true">
		<cfargument name="paramOldPassword" type="string" required="true">

		<cfset var GETUSERDETAILS = "">
		<cfset var userXML = "">
		<cfset var userDetailXML = "">
		<cfset var DoCheck = "">
		<cfset var DoPwCheck = "">
		<cfset var PasswordChangeStatus = "">
		<cfset var LoginStatus = "">

		<cfset var checkPersonById = "">
		<cfset var checkPersonByEmail = "">
		<cfset var authenticationOfOldPassword = "">
		<cfset var personNotAuthenticatedOldPw = "">

		<cfscript>
			var forgottenStruct = structNew();
			forgottenStruct.Request.UserRequestPersonID = #paramPersonID#;
			forgottenStruct.Request.UserRequestPassword = '#paramPassword#';
			forgottenStruct.Request.RequestStatusCode = '406' ;
			forgottenStruct.Request.RequestStatusText = 'Not Acceptable' ;
			forgottenStruct.Request.ValidUserStatus = 'false' ;
			forgottenStruct.Request.ValidUserEmail = 'false' ;
			forgottenStruct.Request.ValidUserDetails = 'false' ;
			forgottenStruct.Request.ValidPasswordStatus = 'false' ;
			forgottenStruct.Request.ValidPasswordText = 'PASSWORD NOT CHANGED' ;
		</cfscript>

		<!--- 2012-08-23 - RMB - P-SNY118 - Extra function to ensure old password is ok --->
		<cfif isdefined("arguments.paramOldPassword") AND arguments.paramOldPassword NEQ ''>
			<cfscript>
				forgottenStruct.Request.ValidOldPasswordStatus = 'false' ;
				forgottenStruct.Request.ValidOldPasswordText = 'OLD PASSWORD NOT CHECKED' ;
			</cfscript>
		<cfelse>
			<cfscript>
				forgottenStruct.Request.ValidOldPasswordStatus = 'N/A' ;
				forgottenStruct.Request.ValidOldPasswordText = 'N/A' ;
			</cfscript>
		</cfif>

			<cfquery name="checkPersonById"  datasource = "#application.sitedatasource#">
			SELECT personId, email, userName, password FROM person
			WHERE personId = #paramPersonID#
			</cfquery>

			<cfif checkPersonById.recordcount LTE 0>

					<cfscript>
						forgottenStruct.Request.RequestStatusCode = '404' ;
						forgottenStruct.Request.RequestStatusText = 'Unknown User ID' ;
					</cfscript>

			<cfelse>

			<cfset DoCheck = application.com.login.isPersonAValidUserOnGivenSite(personid = checkPersonById.personId, domain=CGI.HTTP_HOST)>

				<cfif NOT DoCheck.ISVALIDUSER>

					<cfscript>
						forgottenStruct.Request.RequestStatusCode = '401' ;
						forgottenStruct.Request.RequestStatusText = 'Unauthorized User' ;
					</cfscript>

				<cfelse>

					<cfscript>
						forgottenStruct.Request.ValidUserStatus = DoCheck.ISVALIDUSER ;
					</cfscript>

								<cfquery name="checkPersonByEmail"  datasource = "#application.sitedatasource#">
								SELECT count(*) AS 'CheckCount1' FROM person
								WHERE 1=1
									AND Email  =  <cf_queryparam value="#checkPersonById.email#" CFSQLTYPE="CF_SQL_VARCHAR" >
								</cfquery>

								<cfif checkPersonByEmail.CheckCount1 GTE 2>

									<cfscript>
										forgottenStruct.Request.RequestStatusCode = '409' ;
										forgottenStruct.Request.RequestStatusText = 'VALID USER with Duplicate Email Records Found' ;
										forgottenStruct.Request.ValidUserEmail = 'false' ;
									</cfscript>

								<cfelse>

									<cfscript>
										forgottenStruct.Request.ValidUserEmail = '#checkPersonById.email#' ;
									</cfscript>

								</cfif>

								<!--- 2012-08-23 - RMB - P-SNY118 - Extra function to ensure old password is ok --->
								<cfif isdefined("arguments.paramOldPassword") AND arguments.paramOldPassword NEQ ''>
									<cfscript>
									authenticationOfOldPassword = application.com.login.authenticateUserV2(urldecode(checkPersonById.userName,"utf-8"),arguments.paramOldPassword);
									</cfscript>
								</cfif>

				</cfif>

			</cfif>

			<cfif forgottenStruct.Request.ValidUserStatus>

				<cfset GETUSERDETAILS = GET_USERDETAILS(personID=paramPersonID)>

				<cfoutput>
					<cfsavecontent variable="userDetailXML">
						#GETUSERDETAILS#
					</cfsavecontent>
				</cfoutput>

				<!--- 2012-08-23 - RMB - P-SNY118 - Extra function to ensure old password is ok --->
				<cfif isdefined("arguments.paramOldPassword") AND arguments.paramOldPassword NEQ ''>

					<cfif authenticationOfOldPassword.userQuery.recordCount>

						<cfscript>
							forgottenStruct.Request.ValidOldPasswordStatus = '200' ;
							forgottenStruct.Request.ValidOldPasswordText = 'Successful' ;
						</cfscript>

					<cfelseif authenticationOfOldPassword.failedUserQuery.recordcount >
						<!--- This is the case where the username was correct but something else was wrong --->
						<cfscript>
							forgottenStruct.Request.ValidOldPasswordStatus = '409' ;
							forgottenStruct.Request.ValidOldPasswordText = 'BAD OLD PASSWORD' ;
						</cfscript>
					<cfelse>

						<cfscript>
							forgottenStruct.Request.ValidOldPasswordStatus = '409' ;
							forgottenStruct.Request.ValidOldPasswordText = 'BAD OLD PASSWORD' ;
						</cfscript>

					</cfif>

				</cfif>


				<cfset DoPwCheck = application.com.login.checkPasswordComplexity(password=paramPassword).isoK>

				<cfscript>
				forgottenStruct.Request.ValidUserDetails = GETUSERDETAILS ;
				forgottenStruct.Request.ValidPasswordStatus = DoPwCheck ;
				</cfscript>

				<!--- 2012-08-23 - RMB - P-SNY118 - Extra function to ensure old password is ok --->
				<cfif ((isdefined("arguments.paramOldPassword") AND forgottenStruct.Request.ValidOldPasswordStatus EQ  '200') OR
						NOT isdefined("arguments.paramOldPassword"))>
				<!--- 2012-08-23 - RMB - P-SNY118 - Extra function to ensure old password is ok --->

					<cfif forgottenStruct.Request.ValidPasswordStatus>

						<cfset PasswordChangeStatus = application.com.login.changeUserPassword(userName=checkPersonById.userName, currentPassword =checkPersonById.password, newPassword1=paramPassword, newPassword2=paramPassword, passwordEncrypted='true')>

						<cfscript>
						forgottenStruct.Request.ValidPasswordText = PasswordChangeStatus.MESSAGE ;
						</cfscript>

						<cfif PasswordChangeStatus.MESSAGE EQ ''>
							<cfset LoginStatus = RELAY_LOGINMAIN(relayusername=checkPersonById.userName, relaypassword=paramPassword)>

								<cfscript>
								forgottenStruct.Request.RequestStatusCode = '200' ;
								forgottenStruct.Request.RequestStatusText = 'Successful' ;
								forgottenStruct.Request.LoginStatus = LoginStatus ;
								</cfscript>

						</cfif>

					</cfif>



				<!--- 2012-08-23 - RMB - P-SNY118 - Extra function to ensure old password is ok --->
				<cfelse>

					<cfscript>
					forgottenStruct.Request.RequestStatusCode = '404' ;
					forgottenStruct.Request.RequestStatusText = 'Old Password Invalid' ;
					</cfscript>

				</cfif>
				<!--- 2012-08-23 - RMB - P-SNY118 - Extra function to ensure old password is ok --->

			</cfif>

		<cfoutput>
		<cfsavecontent variable="ResponseXML">
			<Response>
				<UserRequestPersonID>#forgottenStruct.Request.UserRequestPersonID#</UserRequestPersonID>
				<UserRequestPassword>#forgottenStruct.Request.UserRequestPassword#</UserRequestPassword>
				<RequestStatusCode>#forgottenStruct.Request.RequestStatusCode#</RequestStatusCode>
				<RequestStatusText>#forgottenStruct.Request.RequestStatusText#</RequestStatusText>
				<ValidUserStatus>#forgottenStruct.Request.ValidUserStatus#</ValidUserStatus>
				<ValidUserEmail>#forgottenStruct.Request.ValidUserEmail#</ValidUserEmail>
				<ValidPasswordStatus>#forgottenStruct.Request.ValidPasswordStatus#</ValidPasswordStatus>
				<ValidPasswordText>#forgottenStruct.Request.ValidPasswordText#</ValidPasswordText>
				<cfif isdefined("arguments.paramOldPassword") AND arguments.paramOldPassword NEQ ''>
				<ValidOldPasswordStatus>#forgottenStruct.Request.ValidOldPasswordStatus#</ValidOldPasswordStatus>
				<ValidOldPasswordText>#forgottenStruct.Request.ValidOldPasswordText#</ValidOldPasswordText>
				</cfif>
				<UserDetails>
					#userDetailXML#
				</UserDetails>
			</Response>
		</cfsavecontent>
		</cfoutput>

		<cfscript>
			relayWSXMLResponse = build_XML_Response(
				wsMethod="RELAY_VALIDATEPASSWORD",
				wsResponseID="#forgottenStruct.Request.RequestStatusCode#",
				wsResponseText="#forgottenStruct.Request.RequestStatusText#",
				receivedParameter=build_ReceivedParameters(argumentCollection=arguments),
				relayContent="#ResponseXML#");
		</cfscript>

		<cfreturn relayWSXMLResponse>

	</cffunction>
	<!--- 2012-09-03 - SNY-118 - RMB Added New End --->

	<!--- 2012-09-03 - SNY-118 - RMB Added New - Start --->
	<cffunction name="RELAY_PROFILEUPDATE" access="remote" output="false" returntype="XML" hint="PORTAL PROFILE UPDATE Services for RelayWS">
		<cfargument name="paramRequest" type="string" required="true">

		<cfset var XMLRequest = "#XmlParse(urlDecode(ARGUMENTS.PARAMREQUEST))#">

		<cfscript>
		var paramStruct = structNew();
		var XmlStruct = structNew();
		var checkValidValueQuery = structNew();
		var responseStruct = structNew();
		var Person = structNew();

		var countryidlookupfromisocodestr = application.countryidlookupfromisocodestr;
		var languageisocodelookupstr = application.languageisocodelookupstr;
		var languageLookupFromIDstr = application.languageLookupFromIDstr;
		var languagefindkey = "";
		var PersonUpdate = "";

		XmlStruct.PersonID = XmlSearch(XMLRequest, "/ProfileUpdateDetails/PersonID/");
	    for (i = 1; i LTE ArrayLen(XmlStruct.PersonID); i = i + 1)
	    paramStruct.PersonID = XmlStruct.PersonID[i].XmlText;

		XmlStruct.Country = XmlSearch(XMLRequest, "/ProfileUpdateDetails/Country/");
	    for (i = 1; i LTE ArrayLen(XmlStruct.Country); i = i + 1)
	    paramStruct.Country = XmlStruct.Country[i].XmlText;

		XmlStruct.Salutation = XmlSearch(XMLRequest, "/ProfileUpdateDetails/Salutation/");
	    for (i = 1; i LTE ArrayLen(XmlStruct.Salutation); i = i + 1)
	    paramStruct.Salutation = XmlStruct.Salutation[i].XmlText;

		XmlStruct.FirstName = XmlSearch(XMLRequest, "/ProfileUpdateDetails/FirstName/");
	    for (i = 1; i LTE ArrayLen(XmlStruct.FirstName); i = i + 1)
	    paramStruct.FirstName = XmlStruct.FirstName[i].XmlText;

		XmlStruct.LastName = XmlSearch(XMLRequest, "/ProfileUpdateDetails/LastName/");
	    for (i = 1; i LTE ArrayLen(XmlStruct.LastName); i = i + 1)
	    paramStruct.LastName = XmlStruct.LastName[i].XmlText;

		XmlStruct.Email = XmlSearch(XMLRequest, "/ProfileUpdateDetails/Email/");
	    for (i = 1; i LTE ArrayLen(XmlStruct.Email); i = i + 1)
	    paramStruct.Email = XmlStruct.Email[i].XmlText;

		XmlStruct.Telephone = XmlSearch(XMLRequest, "/ProfileUpdateDetails/Telephone/");
	    for (i = 1; i LTE ArrayLen(XmlStruct.Telephone); i = i + 1)
	    paramStruct.Telephone = XmlStruct.Telephone[i].XmlText;

		XmlStruct.Mobile = XmlSearch(XMLRequest, "/ProfileUpdateDetails/Mobile/");
	    for (i = 1; i LTE ArrayLen(XmlStruct.Mobile); i = i + 1)
	    paramStruct.Mobile = XmlStruct.Mobile[i].XmlText;

		XmlStruct.Language = XmlSearch(XMLRequest, "/ProfileUpdateDetails/Language/");
	    for (i = 1; i LTE ArrayLen(XmlStruct.Language); i = i + 1)
	    paramStruct.Language = XmlStruct.Language[i].XmlText;

		XmlStruct.ManagingDirector = XmlSearch(XMLRequest, "/ProfileUpdateDetails/JobFunctions/ManagingDirector/");
	    for (i = 1; i LTE ArrayLen(XmlStruct.ManagingDirector); i = i + 1)
	    paramStruct.ManagingDirector = XmlStruct.ManagingDirector[i].XmlText;

		XmlStruct.FinancialManager = XmlSearch(XMLRequest, "/ProfileUpdateDetails/JobFunctions/FinancialManager/");
	    for (i = 1; i LTE ArrayLen(XmlStruct.FinancialManager); i = i + 1)
	    paramStruct.FinancialManager = XmlStruct.FinancialManager[i].XmlText;

		XmlStruct.Management = XmlSearch(XMLRequest, "/ProfileUpdateDetails/JobFunctions/Management/");
	    for (i = 1; i LTE ArrayLen(XmlStruct.Management); i = i + 1)
	    paramStruct.Management = XmlStruct.Management[i].XmlText;

		XmlStruct.MarketingContact = XmlSearch(XMLRequest, "/ProfileUpdateDetails/JobFunctions/MarketingContact/");
	    for (i = 1; i LTE ArrayLen(XmlStruct.MarketingContact); i = i + 1)
	    paramStruct.MarketingContact = XmlStruct.MarketingContact[i].XmlText;

		XmlStruct.PurchaseContact = XmlSearch(XMLRequest, "/ProfileUpdateDetails/JobFunctions/PurchaseContact/");
	    for (i = 1; i LTE ArrayLen(XmlStruct.PurchaseContact); i = i + 1)
	    paramStruct.PurchaseContact = XmlStruct.PurchaseContact[i].XmlText;

		XmlStruct.SalesContact = XmlSearch(XMLRequest, "/ProfileUpdateDetails/JobFunctions/SalesContact/");
	    for (i = 1; i LTE ArrayLen(XmlStruct.SalesContact); i = i + 1)
	    paramStruct.SalesContact = XmlStruct.SalesContact[i].XmlText;

		XmlStruct.ServiceContact = XmlSearch(XMLRequest, "/ProfileUpdateDetails/JobFunctions/ServiceContact/");
	    for (i = 1; i LTE ArrayLen(XmlStruct.ServiceContact); i = i + 1)
	    paramStruct.ServiceContact = XmlStruct.ServiceContact[i].XmlText;

		XmlStruct.TechnicalContact = XmlSearch(XMLRequest, "/ProfileUpdateDetails/JobFunctions/TechnicalContact/");
	    for (i = 1; i LTE ArrayLen(XmlStruct.TechnicalContact); i = i + 1)
	    paramStruct.TechnicalContact = XmlStruct.TechnicalContact[i].XmlText;

		XmlStruct.Other_01 = XmlSearch(XMLRequest, "/ProfileUpdateDetails/JobFunctions/Other_01/");
	    for (i = 1; i LTE ArrayLen(XmlStruct.Other_01); i = i + 1)
	    paramStruct.Other_01 = XmlStruct.Other_01[i].XmlText;

		XmlStruct.ISManager = XmlSearch(XMLRequest, "/ProfileUpdateDetails/JobFunctions/ISManager/");
	    for (i = 1; i LTE ArrayLen(XmlStruct.ISManager); i = i + 1)
	    paramStruct.ISManager = XmlStruct.ISManager[i].XmlText;

		XmlStruct.PostSalesSupportManager = XmlSearch(XMLRequest, "/ProfileUpdateDetails/JobFunctions/PostSalesSupportManager/");
	    for (i = 1; i LTE ArrayLen(XmlStruct.PostSalesSupportManager); i = i + 1)
	    paramStruct.PostSalesSupportManager = XmlStruct.PostSalesSupportManager[i].XmlText;

		XmlStruct.PostSalesSupport_TechnicalEngi = XmlSearch(XMLRequest, "/ProfileUpdateDetails/JobFunctions/PostSalesSupport_TechnicalEngi/");
	    for (i = 1; i LTE ArrayLen(XmlStruct.PostSalesSupport_TechnicalEngi); i = i + 1)
	    paramStruct.PostSalesSupport_TechnicalEngi = XmlStruct.PostSalesSupport_TechnicalEngi[i].XmlText;

		XmlStruct.PrimarySalesContact = XmlSearch(XMLRequest, "/ProfileUpdateDetails/JobFunctions/PrimarySalesContact/");
	    for (i = 1; i LTE ArrayLen(XmlStruct.PrimarySalesContact); i = i + 1)
	    paramStruct.PrimarySalesContact = XmlStruct.PrimarySalesContact[i].XmlText;

		XmlStruct.ServiceAdminContact = XmlSearch(XMLRequest, "/ProfileUpdateDetails/JobFunctions/ServiceAdminContact/");
	    for (i = 1; i LTE ArrayLen(XmlStruct.ServiceAdminContact); i = i + 1)
	    paramStruct.ServiceAdminContact = XmlStruct.ServiceAdminContact[i].XmlText;

		XmlStruct.ServiceDirector = XmlSearch(XMLRequest, "/ProfileUpdateDetails/JobFunctions/ServiceDirector/");
	    for (i = 1; i LTE ArrayLen(XmlStruct.Language); i = i + 1)
	    paramStruct.ServiceDirector = XmlStruct.ServiceDirector[i].XmlText;

		XmlStruct.NoCommsTypeALL = XmlSearch(XMLRequest, "/ProfileUpdateDetails/NoCommsTypeALL/");
	    for (i = 1; i LTE ArrayLen(XmlStruct.NoCommsTypeALL); i = i + 1)
	    paramStruct.NoCommsTypeALL = XmlStruct.NoCommsTypeALL[i].XmlText;
		</cfscript>

		<!--- <cfdump var="#paramStruct#" format="html" output="D:\Web\paramStruct.html"> --->

		<cfif NOT StructKeyExists(paramStruct, "PersonID")>

			<cfscript>
			responseStruct.PersonIDStatus = '406';
			responseStruct.PersonIDStatusText = 'PersonID Missing in paramRequest';
			</cfscript>

		<cfelse>

			<cfscript>
			responseStruct.isPersonAValid = application.com.login.isPersonAValidUserOnGivenSite(personid = paramStruct.PersonID, domain=CGI.HTTP_HOST).isvaliduser;

			if(NOT responseStruct.isPersonAValid){
				responseStruct.PersonIDStatus = '406';
				responseStruct.PersonIDStatusText = 'PersonID Invalid';
			} else {
				responseStruct.PersonIDStatus = '200';
				responseStruct.PersonIDStatusText = 'PersonID Valid';
			}
			</cfscript>

		</cfif>

		<!--- Country Check Start --->
	    <cfif NOT StructKeyExists(paramStruct, "Country") OR NOT StructKeyExists(countryidlookupfromisocodestr, "#paramStruct.Country#")>

			<cfscript>
			responseStruct.UserCountryStatus = '406';
			responseStruct.UserCountryStatusText = 'Country Invalid';
			</cfscript>

	    <cfelse>

			<cfscript>
			responseStruct.UserCountryStatus = '200';
			responseStruct.UserCountryStatusText = 'Country Valid';
			responseStruct.UserCountry = '#countryidlookupfromisocodestr[paramStruct.COUNTRY]#';
			</cfscript>

	    </cfif>
		<!--- Country Check End --->

		<!--- Language Check Start --->
		<cfscript>
		languagefindkey = StructFindValue(languageisocodelookupstr, "#paramStruct.LANGUAGE#");
		</cfscript>

		<cfif ArrayIsEmpty(languagefindkey) NEQ 'YES'>
			<cfif StructKeyExists(languagefindkey[1], "key")>

				<cfset UserLanguageStep1 = #languagefindkey[1].key#>
				<cfset UserLanguage = "#languageLookupFromIDstr[UserLanguageStep1]#">
				<cfset responseStruct.UserLanguageID = UserLanguage>

				<cfscript>
				responseStruct.UserLanguageStatus = '200';
				responseStruct.UserLanguageStatusText = 'Language Valid';
				</cfscript>

		    <cfelse>
				<cfscript>
				responseStruct.UserLanguageStatus = '406';
				responseStruct.UserLanguageStatusText = 'Language Invalid';
				</cfscript>
			</cfif>
		<cfelse>
			<cfscript>
			responseStruct.UserLanguageStatus = '406';
			responseStruct.UserLanguageStatusText = 'Language Invalid';
			</cfscript>
		</cfif>
		<!--- Language Check End --->

    	<!--- Email Check Start --->
	    <cfif NOT StructKeyExists(paramStruct, "PersonID") OR NOT StructKeyExists(paramStruct, "Email")>

			<cfscript>
			responseStruct.EmailStatus = '406';
			responseStruct.EmailStatusText = 'Email Invalid';
			</cfscript>

	    <cfelse>

	    <cfif LTRIM(RTRIM(paramStruct.Email)) NEQ ''>
		    <cfset EmailCheck01 = application.com.email.isValidEmail(emailAddress = paramStruct.Email)>
		    <cfif EmailCheck01>
				<cfquery name="EmailCheck02"  datasource = "#application.sitedatasource#">
				SELECT count(*) AS 'CheckCount1' FROM person
				WHERE 1=1 AND Email =  <cf_queryparam value="#paramStruct.Email#" CFSQLTYPE="CF_SQL_VARCHAR" > AND
				PersonID != #paramStruct.PersonID#
				</cfquery>
				<cfif EmailCheck02.CheckCount1 EQ 0>

					<cfquery name="EmailCheck03"  datasource = "#application.sitedatasource#">
					SELECT count(*) AS 'CheckCount1' FROM person
					WHERE 1=1 AND Email  =  <cf_queryparam value="#paramStruct.Email#" CFSQLTYPE="CF_SQL_VARCHAR" >
					</cfquery>
					<cfif EmailCheck03.CheckCount1 LTE 1>
						<cfset responseStruct.EmailStatus = "200">
						<cfset responseStruct.EmailStatusText = "Email Valid">
					<cfelse>
						<cfset responseStruct.EmailStatus = "403">
						<cfset responseStruct.EmailStatusText = "Duplicate email address">
					</cfif>
				<cfelse>
					<cfset responseStruct.EmailStatus = "403">
					<cfset responseStruct.EmailStatusText = "Email Already Used">
				</cfif>
			<cfelse>
				<cfset responseStruct.EmailStatus = "406">
				<cfset responseStruct.EmailStatusText = "Email Invalid">
		    </cfif>
		<cfelse>
			<cfset responseStruct.EmailStatus = "406">
  			<cfset responseStruct.EmailStatusText = "Email Missing">
		</cfif>

		</cfif>
		<!--- Email Check End --->

    	<!--- FirstName Check Start --->
	    <cfif StructKeyExists(paramStruct, "FirstName") AND LTRIM(RTRIM(paramStruct.FirstName)) NEQ ''>
			<cfset responseStruct.FirstNameStatus = "200">
			<cfset responseStruct.FirstNameStatusText = "First Name Valid">
		<cfelse>
			<cfset responseStruct.FirstNameStatus = "406">
			<cfset responseStruct.FirstNameStatusText = "First Name Invalid">
		</cfif>
    	<!--- FirstName Check End --->

    	<!--- LastName Check Start --->
	    <cfif StructKeyExists(paramStruct, "LastName") AND LTRIM(RTRIM(paramStruct.LastName)) NEQ ''>
			<cfset responseStruct.LastNameStatus = "200">
			<cfset responseStruct.LastNameStatusText = "Last Name Valid">
		<cfelse>
			<cfset responseStruct.LastNameStatus = "406">
			<cfset responseStruct.LastNameStatusText = "Last Name Invalid">
		</cfif>
    	<!--- LastName Check End --->

    	<!--- Salutation Check Start --->
	    <cfif StructKeyExists(paramStruct, "Salutation") AND LTRIM(RTRIM(paramStruct.Salutation)) NEQ '' AND
		    	StructKeyExists(responseStruct, "UserCountry")>

		    <cfset responseStruct.ValidValueQuery = application.com.relayForms.getValidValueQueryOrStatementFromDB(fieldName = 'Person.Salutation', countryid = responseStruct.UserCountry)>

		    <!--- 2012-09-13 - Changed query to use displayvalue and not DataValue - RMB --->
		    <cfquery name = "checkValidValueQuery" dbtype = "query">
			select * from responseStruct.ValidValueQuery
			WHERE displayvalue = '#paramStruct.Salutation#'
			</cfquery>
		    <cfif checkValidValueQuery.recordcount GTE 1>
				<cfset responseStruct.SalutationStatus = "200">
				<cfset responseStruct.SalutationStatusText = "Salutation Valid">
			<cfelse>
				<cfset responseStruct.SalutationStatus = "406">
				<cfset responseStruct.SalutationStatusText = "Salutation Invalid">
			</cfif>
		<cfelse>
			<cfset responseStruct.SalutationStatus = "406">
			<cfset responseStruct.SalutationStatusText = "Salutation Invalid">
		</cfif>
    	<!--- Salutation Check End --->

	    <cfif NOT StructKeyExists(paramStruct, "Mobile")>
			<cfset paramStruct.Mobile = "">
		</cfif>

	    <cfif NOT StructKeyExists(paramStruct, "Telephone")>
			<cfset paramStruct.Telephone = "">
		</cfif>

	    <cfif NOT StructKeyExists(paramStruct, "ManagingDirector")>
		    <cfset paramStruct.ManagingDirector = "false">
		</cfif>

	    <cfif NOT StructKeyExists(paramStruct, "FinancialManager")>
		    <cfset paramStruct.FinancialManager = "false">
		</cfif>

	    <cfif NOT StructKeyExists(paramStruct, "Management")>
		    <cfset paramStruct.Management = "false">
		</cfif>

	    <cfif NOT StructKeyExists(paramStruct, "MarketingContact")>
		    <cfset paramStruct.MarketingContact = "false">
		</cfif>

	    <cfif NOT StructKeyExists(paramStruct, "PurchaseContact")>
		    <cfset paramStruct.PurchaseContact = "false">
		</cfif>

	    <cfif NOT StructKeyExists(paramStruct, "SalesContact")>
		    <cfset paramStruct.SalesContact = "false">
		</cfif>

	    <cfif NOT StructKeyExists(paramStruct, "ServiceContact")>
		    <cfset paramStruct.ServiceContact = "false">
		</cfif>

	    <cfif NOT StructKeyExists(paramStruct, "TechnicalContact")>
		    <cfset paramStruct.TechnicalContact = "false">
		</cfif>

	    <cfif NOT StructKeyExists(paramStruct, "Other_01")>
		    <cfset paramStruct.Other_01 = "false">
		</cfif>

	    <cfif NOT StructKeyExists(paramStruct, "ISManager")>
		    <cfset paramStruct.ISManager = "false">
		</cfif>

	    <cfif NOT StructKeyExists(paramStruct, "PostSalesSupportManager")>
		    <cfset paramStruct.PostSalesSupportManager = "false">
		</cfif>

	    <cfif NOT StructKeyExists(paramStruct, "PostSalesSupport_TechnicalEngi")>
		    <cfset paramStruct.PostSalesSupport_TechnicalEngi = "false">
		</cfif>

	    <cfif NOT StructKeyExists(paramStruct, "PrimarySalesContact")>
		    <cfset paramStruct.PrimarySalesContact = "false">
		</cfif>

	    <cfif NOT StructKeyExists(paramStruct, "ServiceAdminContact")>
		    <cfset paramStruct.ServiceAdminContact = "false">
		</cfif>

	    <cfif NOT StructKeyExists(paramStruct, "ServiceDirector")>
		    <cfset paramStruct.ServiceDirector = "false">
		</cfif>

	    <cfif NOT StructKeyExists(paramStruct, "NoCommsTypeALL")>
		    <cfset paramStruct.NoCommsTypeALL = "false">
		</cfif>

		<cfif StructKeyExists(responseStruct, "isPersonAValid") AND responseStruct.isPersonAValid>

			<cfif StructKeyExists(responseStruct, "EmailStatus") AND responseStruct.EmailStatus EQ 200 AND
					StructKeyExists(responseStruct, "FirstNameStatus") AND responseStruct.FirstNameStatus EQ 200 AND
					StructKeyExists(responseStruct, "LastNameStatus") AND responseStruct.LastNameStatus EQ 200 AND
					StructKeyExists(responseStruct, "PersonIDStatus") AND responseStruct.PersonIDStatus EQ 200 AND
					StructKeyExists(responseStruct, "SalutationStatus") AND responseStruct.SalutationStatus EQ 200 AND
					StructKeyExists(responseStruct, "UserCountryStatus") AND responseStruct.UserCountryStatus EQ 200 AND
					StructKeyExists(responseStruct, "UserLanguageStatus") AND responseStruct.UserLanguageStatus EQ 200>

				<cfscript>
				Person.Email = paramStruct.Email;
				Person.FirstName = paramStruct.FirstName;
				Person.LastName = paramStruct.LastName;
				Person.Language = responseStruct.UserLanguageID;
				Person.Mobile = paramStruct.Mobile;
				Person.OfficePhone = paramStruct.Telephone;
				Person.MobilePhone = paramStruct.Salutation;
				PersonUpdate = application.com.relayPLO.updatePersonDetails(personID = paramStruct.PersonID, personDetails = Person);
				if(PersonUpdate.isOK){
					responseStruct.PersonUpdateStatus = '200';
					responseStruct.PersonUpdateStatusText = 'Person Updated';
				} else {
					responseStruct.PersonUpdateStatus = '400';
					responseStruct.PersonUpdateStatusText = 'Person NOT Updated';
				}
				</cfscript>

				<cfif IsBoolean(paramStruct.ManagingDirector)>
				<cfif paramStruct.ManagingDirector>
					<cfset application.com.flag.setBooleanFlag(entityID = paramStruct.PersonID, flagTextID = 'ManagingDirector')>
				<cfelse>
					<cfif application.com.flag.isFlagSetForPerson(PersonID = paramStruct.PersonID, flagID = 'ManagingDirector')>
						<cfset application.com.flag.unsetBooleanFlag(entityID = paramStruct.PersonID, flagID = 'ManagingDirector')>
					</cfif>
				</cfif>
				</cfif>

				<cfif IsBoolean(paramStruct.FinancialManager)>
				<cfif paramStruct.FinancialManager>
					<cfset application.com.flag.setBooleanFlag(entityID = paramStruct.PersonID, flagTextID = 'FinancialManager')>
				<cfelse>
					<cfif application.com.flag.isFlagSetForPerson(PersonID = paramStruct.PersonID, flagID = 'FinancialManager')>
						<cfset application.com.flag.unsetBooleanFlag(entityID = paramStruct.PersonID, flagID = 'FinancialManager')>
					</cfif>
				</cfif>
				</cfif>

				<cfif IsBoolean(paramStruct.Management)>
				<cfif paramStruct.Management>
					<cfset application.com.flag.setBooleanFlag(entityID = paramStruct.PersonID, flagTextID = 'Management')>
				<cfelse>
					<cfif application.com.flag.isFlagSetForPerson(PersonID = paramStruct.PersonID, flagID = 'Management')>
						<cfset application.com.flag.unsetBooleanFlag(entityID = paramStruct.PersonID, flagID = 'Management')>
					</cfif>
				</cfif>
				</cfif>

				<cfif IsBoolean(paramStruct.MarketingContact)>
				<cfif paramStruct.MarketingContact>
					<cfset application.com.flag.setBooleanFlag(entityID = paramStruct.PersonID, flagTextID = 'MarketingContact')>
				<cfelse>
					<cfif application.com.flag.isFlagSetForPerson(PersonID = paramStruct.PersonID, flagID = 'MarketingContact')>
						<cfset application.com.flag.unsetBooleanFlag(entityID = paramStruct.PersonID, flagID = 'MarketingContact')>
					</cfif>
				</cfif>
				</cfif>

				<cfif IsBoolean(paramStruct.PurchaseContact)>
				<cfif paramStruct.PurchaseContact>
					<cfset application.com.flag.setBooleanFlag(entityID = paramStruct.PersonID, flagTextID = 'PurchaseContact')>
				<cfelse>
					<cfif application.com.flag.isFlagSetForPerson(PersonID = paramStruct.PersonID, flagID = 'PurchaseContact')>
						<cfset application.com.flag.unsetBooleanFlag(entityID = paramStruct.PersonID, flagID = 'PurchaseContact')>
					</cfif>
				</cfif>
				</cfif>

				<cfif IsBoolean(paramStruct.SalesContact)>
				<cfif paramStruct.SalesContact>
					<cfset application.com.flag.setBooleanFlag(entityID = paramStruct.PersonID, flagTextID = 'SalesContact')>
				<cfelse>
					<cfif application.com.flag.isFlagSetForPerson(PersonID = paramStruct.PersonID, flagID = 'SalesContact')>
						<cfset application.com.flag.unsetBooleanFlag(entityID = paramStruct.PersonID, flagID = 'SalesContact')>
					</cfif>
				</cfif>
				</cfif>

				<cfif IsBoolean(paramStruct.ServiceContact)>
				<cfif paramStruct.ServiceContact>
					<cfset application.com.flag.setBooleanFlag(entityID = paramStruct.PersonID, flagTextID = 'ServiceContact')>
				<cfelse>
					<cfif application.com.flag.isFlagSetForPerson(PersonID = paramStruct.PersonID, flagID = 'ServiceContact')>
						<cfset application.com.flag.unsetBooleanFlag(entityID = paramStruct.PersonID, flagID = 'ServiceContact')>
					</cfif>
				</cfif>
				</cfif>

				<cfif IsBoolean(paramStruct.TechnicalContact)>
				<cfif paramStruct.TechnicalContact>
					<cfset application.com.flag.setBooleanFlag(entityID = paramStruct.PersonID, flagTextID = 'TechnicalContact')>
				<cfelse>
					<cfif application.com.flag.isFlagSetForPerson(PersonID = paramStruct.PersonID, flagID = 'TechnicalContact')>
						<cfset application.com.flag.unsetBooleanFlag(entityID = paramStruct.PersonID, flagID = 'TechnicalContact')>
					</cfif>
				</cfif>
				</cfif>

				<cfif IsBoolean(paramStruct.Other_01)>
				<cfif paramStruct.Other_01>
					<cfset application.com.flag.setBooleanFlag(entityID = paramStruct.PersonID, flagTextID = 'Other_01')>
				<cfelse>
					<cfif application.com.flag.isFlagSetForPerson(PersonID = paramStruct.PersonID, flagID = 'Other_01')>
						<cfset application.com.flag.unsetBooleanFlag(entityID = paramStruct.PersonID, flagID = 'Other_01')>
					</cfif>
				</cfif>
				</cfif>

				<cfif IsBoolean(paramStruct.ISManager)>
				<cfif paramStruct.ISManager>
					<cfset application.com.flag.setBooleanFlag(entityID = paramStruct.PersonID, flagTextID = 'ISManager')>
				<cfelse>
					<cfif application.com.flag.isFlagSetForPerson(PersonID = paramStruct.PersonID, flagID = 'ISManager')>
						<cfset application.com.flag.unsetBooleanFlag(entityID = paramStruct.PersonID, flagID = 'ISManager')>
					</cfif>
				</cfif>
				</cfif>

				<cfif IsBoolean(paramStruct.PostSalesSupportManager)>
				<cfif paramStruct.PostSalesSupportManager>
					<cfset application.com.flag.setBooleanFlag(entityID = paramStruct.PersonID, flagTextID = 'PostSalesSupportManager')>
				<cfelse>
					<cfif application.com.flag.isFlagSetForPerson(PersonID = paramStruct.PersonID, flagID = 'PostSalesSupportManager')>
						<cfset application.com.flag.unsetBooleanFlag(entityID = paramStruct.PersonID, flagID = 'PostSalesSupportManager')>
					</cfif>
				</cfif>
				</cfif>

				<cfif IsBoolean(paramStruct.PostSalesSupport_TechnicalEngi)>
				<cfif paramStruct.PostSalesSupport_TechnicalEngi>
					<cfset application.com.flag.setBooleanFlag(entityID = paramStruct.PersonID, flagTextID = 'PostSalesSupport_TechnicalEngi')>
				<cfelse>
					<cfif application.com.flag.isFlagSetForPerson(PersonID = paramStruct.PersonID, flagID = 'PostSalesSupport_TechnicalEngi')>
						<cfset application.com.flag.unsetBooleanFlag(entityID = paramStruct.PersonID, flagID = 'PostSalesSupport_TechnicalEngi')>
					</cfif>
				</cfif>
				</cfif>

				<cfif IsBoolean(paramStruct.PrimarySalesContact)>
				<cfif paramStruct.PrimarySalesContact>
					<cfset application.com.flag.setBooleanFlag(entityID = paramStruct.PersonID, flagTextID = 'PrimarySalesContact')>
				<cfelse>
					<cfif application.com.flag.isFlagSetForPerson(PersonID = paramStruct.PersonID, flagID = 'PrimarySalesContact')>
						<cfset application.com.flag.unsetBooleanFlag(entityID = paramStruct.PersonID, flagID = 'PrimarySalesContact')>
					</cfif>
				</cfif>
				</cfif>

				<cfif IsBoolean(paramStruct.ServiceAdminContact)>
				<cfif paramStruct.ServiceAdminContact>
					<cfset application.com.flag.setBooleanFlag(entityID = paramStruct.PersonID, flagTextID = 'ServiceAdminContact')>
				<cfelse>
					<cfif application.com.flag.isFlagSetForPerson(PersonID = paramStruct.PersonID, flagID = 'ServiceAdminContact')>
						<cfset application.com.flag.unsetBooleanFlag(entityID = paramStruct.PersonID, flagID = 'ServiceAdminContact')>
					</cfif>
				</cfif>
				</cfif>

				<cfif IsBoolean(paramStruct.ServiceDirector)>
				<cfif paramStruct.ServiceDirector>
					<cfset application.com.flag.setBooleanFlag(entityID = paramStruct.PersonID, flagTextID = 'ServiceDirector')>
				<cfelse>
					<cfif application.com.flag.isFlagSetForPerson(PersonID = paramStruct.PersonID, flagID = 'ServiceDirector')>
						<cfset application.com.flag.unsetBooleanFlag(entityID = paramStruct.PersonID, flagID = 'ServiceDirector')>
					</cfif>
				</cfif>
				</cfif>

				<cfif IsBoolean(paramStruct.NoCommsTypeALL)>
				<cfif paramStruct.NoCommsTypeALL>
					<cfset application.com.flag.setBooleanFlag(entityID = paramStruct.PersonID, flagTextID = 'NoCommsTypeALL')>
				<cfelse>
					<cfif application.com.flag.isFlagSetForPerson(PersonID = paramStruct.PersonID, flagID = 'NoCommsTypeALL')>
						<cfset application.com.flag.unsetBooleanFlag(entityID = paramStruct.PersonID, flagID = 'NoCommsTypeALL')>
					</cfif>
				</cfif>
				</cfif>

			<cfelse>

				<cfscript>
				responseStruct.PersonUpdateStatus = '400';
				responseStruct.PersonUpdateStatusText = 'Person NOT Updated';
				</cfscript>

			</cfif>

		<cfelse>

			<cfscript>
			responseStruct.PersonUpdateStatus = '400';
			responseStruct.PersonUpdateStatusText = 'Person NOT Updated';
			</cfscript>

		</cfif>

		<!--- 2012-09-13 - RMB - removed debug - <cffile action="delete" file="D:\Web\responseStruct.html">
		<cfdump var="#DATEFORMAT(now(), "DD-MM-YYYY")# #TIMEFORMAT(now(), "HH:MM:SS")#" format="html" output="D:\Web\responseStruct.html">
		<cfdump var="#XMLRequest#" format="html" output="D:\Web\responseStruct.html">
		<cfdump var="#paramStruct#" format="html" output="D:\Web\responseStruct.html">
		<cfdump var="#responseStruct#" format="html" output="D:\Web\responseStruct.html">
		<cfdump var="#Person#" format="html" output="D:\Web\responseStruct.html">
		<cfdump var="#PersonUpdate#" format="html" output="D:\Web\responseStruct.html"> --->

		<cfoutput>
		<cfsavecontent variable="ResponseXML">
			<Response>
				<ISPERSONAVALID>#responseStruct.ISPERSONAVALID#</ISPERSONAVALID>
				<PersonUpdateStatus>#responseStruct.PersonUpdateStatus#</PersonUpdateStatus>
				<PersonUpdateStatusText>#responseStruct.PersonUpdateStatusText#</PersonUpdateStatusText>
				<EMAILSTATUS>#responseStruct.EMAILSTATUS#</EMAILSTATUS>
				<EMAILSTATUSTEXT>#responseStruct.EMAILSTATUSTEXT#</EMAILSTATUSTEXT>
				<PERSONIDSTATUS>#responseStruct.PERSONIDSTATUS#</PERSONIDSTATUS>
				<PERSONIDSTATUSTEXT>#responseStruct.PERSONIDSTATUSTEXT#</PERSONIDSTATUSTEXT>
				<SALUTATIONSTATUS>#responseStruct.SALUTATIONSTATUS#</SALUTATIONSTATUS>
				<SALUTATIONSTATUSTEXT>#responseStruct.SALUTATIONSTATUSTEXT#</SALUTATIONSTATUSTEXT>
				<COUNTRYSTATUS>#responseStruct.USERCOUNTRYSTATUS#</COUNTRYSTATUS>
				<COUNTRYSTATUSTEXT>#responseStruct.USERCOUNTRYSTATUSTEXT#</COUNTRYSTATUSTEXT>
				<COUNTRYSTATUS>#responseStruct.USERLANGUAGESTATUS#</COUNTRYSTATUS>
				<COUNTRYSTATUSTEXT>#responseStruct.USERLANGUAGESTATUSTEXT#</COUNTRYSTATUSTEXT>
			</Response>
		</cfsavecontent>
		</cfoutput>

		<cfscript>
			relayWSXMLResponse = build_XML_Response(
				wsMethod="RELAY_PROFILEUPDATE",
				wsResponseID="#responseStruct.PersonUpdateStatus#",
				wsResponseText="#responseStruct.PersonUpdateStatusText#",
				receivedParameter=build_ReceivedParameters(argumentCollection=arguments),
				relayContent="#ResponseXML#");
		</cfscript>

		<cfreturn relayWSXMLResponse>

	</cffunction>
	<!--- 2012-09-03 - SNY-118 - RMB Added New - End --->

	<cffunction name="build_ReceivedParameters" access="private" output="false" returntype="Array">

		<cfset var argumentsList = structkeylist(arguments)>
		<cfset var receivedParameter = arrayNew(2)> <!--- NJH 2009-04-21 CR-SNY655 - initialised receievedParameter variable --->

		<cfscript>
			for(i=1;i lte listlen(argumentsList);i=i+1)
			{
				receivedParameter[i][1]="#listGetAt(argumentsList,i)#";
				receivedParameter[i][2]="#evaluate("arguments.#listGetAt(argumentsList,i)#")#";
			}
		</cfscript>

		<cfreturn receivedParameter>

	</cffunction>

</cfcomponent>
