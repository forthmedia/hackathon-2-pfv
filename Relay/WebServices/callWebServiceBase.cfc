<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent output="false">

	<cffunction name="getComponentAndMethodPointer">
		<cfargument name="webservicename" required="true">
		<cfargument name="methodname" required="true">
		<cfargument name="webservicesFolder" required="true" hint="The name of the folder (in Relay/ code/ or code/cfTemplates/ where webservices should be serviced for">

		<cfset var result = {}>

		<!--- Deal with automatic extension of the CFC, look for extensions in code directories--->
		<cfif fileexists ("#application.paths.code#/#webservicesFolder#/#webservicename#.cfc" )>
			<cfset result.webServicePath = "/code/#webservicesFolder#/#webservicename#">
		<cfelseif fileexists ("#application.paths.code#/cftemplates/#webservicesFolder#/#webservicename#.cfc" )>
			<cfset result.webServicePath = "/code/cftemplates/#webservicesFolder#/#webservicename#">
		<cfelse>
			<cfset result.webServicePath = "/#webservicesFolder#/#webservicename#">
		</cfif>


		<!---
			Do security.
			We pass the cfc object and the filename to the getCFCFileAndMethodSecurityRequirements()
			This function will look in the securityXML and at the metadata for the individual method to get the security requirements
		 --->

		<cfobject
		    component = "#result.WebServicePath#"
		    name = "result.component">
		<cfset result.methodPointer = result.component[methodname]>

		<cfreturn result>
	</cffunction>

	<cffunction name="cleanseInputArguments" returnType="struct" hint="removes arguments not to be passed to the webservice itself">
		<cfargument name="argumentsScope" type="struct" required="true">

		<cfset args = structCopy(argumentsScope)>
		<cfset structDelete(args,"webservicename")>
		<cfset structDelete(args,"methodname")>
		<cfset structDelete(args,"_cf_nodebug")>
		<cfset structDelete(args,"formstate")>

		<cfreturn args >
	</cffunction>

	<cffunction name="prepareOutputResult">
		<cfargument name="resultIn" type="any" required="true">

		<cfset var resultOut=arguments.resultIn> <!--- NJH 2016/09/06 JIRA PROD2016-2213 --->
		<cfif isValid("String",arguments.resultIn) and not isNumeric(arguments.resultIn) and not isBoolean(arguments.resultIn)> <!--- DCC 2014-12-08 CASE 442933 check not a number thanks to WAB, 2015-10-13 added not boolean test (for Dawid)--->
			<cfset resultOut = application.com.request.postProcessContentString(content=arguments.resultIn)>
		<cfelseif isStruct(arguments.resultIn)>
			<cfset resultOut = application.com.request.postProcessContentStructure(arguments.resultIn)>
		</cfif>

		<cfreturn resultOut >
	</cffunction>


	<!---
		WAB 2011/11
		This function tries to give an appropriate format of result if there is a security failure,
		looks at the method metadata to get the return type,
		For a structure it assumes isOK and Message

		This idea may or may not work!
		More description on wiki
	--->

	<cffunction name="createSecurityFailureResult" output="false">
		<cfargument name="Message" required="true">
		<cfargument name="methodpointer" required="true">

		<cfset var result = "">
		<cfset var methodMetaData = getMetaData (methodPointer)>
			<cfif structKeyExists (methodMetaData,"returnType") and methodMetaData.returnType is "struct">
				<cfset result = {isok=false,message=message}>
			<cfelse>
				<cfset result = message>
			</cfif>

		<cfreturn result>

	</cffunction>



</cfcomponent>

