<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
2012/01/18 - RMB
--->
<cfcomponent output="false"><cfsetting enablecfoutputonly="true">
<cfprocessingdirective pageencoding="utf-8">

	<cffunction name="getsolutionAreas" access="remote" output="false" returntype="xml">
		<cfargument name="solIDs" default="" required="no">
		
		<cfset var relayWSXMLResponse = "">
		<cfset var statusText = "">
		<cfset var userXML = "">

		<cfquery name="solList" datasource = "#application.sitedatasource#">	
		SELECT
			SA1.solutionAreaId,
			SA1.solutionAreaName
		FROM
			trngsolutionArea SA1 inner join
			trngCourse TC1 on TC1.solutionAreaId = SA1.solutionAreaId left outer join
            trngModule TM1 on TM1.courseId = TC1.courseId 
		<cfif solIDs IS NOT '' AND solIDs IS NOT 'notset'>
		WHERE
			SA1.solutionAreaId IS NOT NULL AND
			<cfset i = 0>
			(<cfloop index = "ListElement" list = "#solIDs#" delimiters = ","> 
				<cfset i++>
				SA1.solutionAreaId = <cfqueryparam cfsqltype="CF_SQL_INTEGER" null="false" value="#ListElement#">
				<cfif i LT ListLen(solIDs)>	OR </cfif>	
			</cfloop>)
		</cfif>
		GROUP BY
			SA1.solutionAreaId,
			SA1.solutionAreaName
		ORDER BY
			SA1.solutionAreaName DESC
		</cfquery>
		
		<cfset i = 0>
		<cfif solList.recordcount gt 0>
<cfsavecontent variable="userXML"><cfoutput><allsolutions><cfloop query="solList"><solution rowcount="#i#"><cfif solList.solutionAreaId NEQ ''><solutionAreaId><![CDATA[#solList.solutionAreaId#]]></solutionAreaId></cfif><cfif solList.solutionAreaName NEQ ''><solutionAreaName><![CDATA[#solList.solutionAreaName#]]></solutionAreaName></cfif></solution><cfset i++></cfloop></allsolutions></cfoutput></cfsavecontent>
		</cfif>
		
		<cfscript>
			relayWSXMLResponse = build_XML_Response(
				wsMethod="getsolutionAreas",
				wsResponseID="200",
				wsResponseText="#statusText#",
				receivedParameter=build_ReceivedParameters(argumentCollection=arguments),
				relayContent="#trim(userXML)#");
		</cfscript>
		
		<cfreturn trim(relayWSXMLResponse)>
	
	</cffunction>
	
	<cffunction name="getcourses" access="remote" output="false" returntype="xml">
		<cfargument name="solIDs" default="" required="no">
		<cfargument name="courseIDs" default="" required="no">
		<cfargument name="moduleIDs" default="" required="no">
		
		<cfset var relayWSXMLResponse = "">
		<cfset var statusText = "">
		<cfset var userXML = "">
		<cfset var titlePhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "trngcourse", phraseTextID = "title")>

		<cfquery name="courseList" datasource = "#application.sitedatasource#">	
		exec updatePhrasePointerCacheForEntityType
						@entityType  = 'trngcourse',
						@phraseTextID	 ='title',
						@languageid  = 1 ,
						@countryid = 119
		
		select * from 			
						(select distinct  
							courseid,publishedDate,Course_Code,Solution_Area,Level,Series,SortOrder,active,description,
							convert(nVarchar(4000),isnull(p_trngcourse_title.phraseText,'')) as Title_Of_Course,courseid as viewCourseId  			
					from vTrngCourses v with (noLock)
							left join vphrasepointercache as p_trngcourse_title  
											on 		p_trngcourse_title.phraseTextID = 'title' 
												and p_trngcourse_title.entityTypeID = 74 
												and p_trngcourse_title.entityid = courseId 
												and p_trngcourse_title.languageid = 1 
												and  p_trngcourse_title.countryid = 119 
		WHERE
 			v.courseid IS NOT NULL
		<cfif solIDs IS NOT '' AND solIDs IS NOT 'notset'>
			AND
			<cfset i = 0>
				v.courseid  IN  (SELECT courseid FROM trngCourse WHERE solutionAreaId IN
				 (<cfloop index = "ListElement" list = "#solIDs#" delimiters = ","> 
				<cfset i++>
<cfqueryparam cfsqltype="CF_SQL_INTEGER" null="false" value="#ListElement#">
				<cfif i LT ListLen(solIDs)>,</cfif>	
			</cfloop>))
		</cfif>

		<cfif courseIDs IS NOT '' AND courseIDs IS NOT 'notset'>
			 AND
			<cfset i = 0>
			(<cfloop index = "ListElement" list = "#courseIDs#" delimiters = ","> 
				<cfset i++>
				v.courseid = <cfqueryparam cfsqltype="CF_SQL_INTEGER" null="false" value="#ListElement#">
				<cfif i LT ListLen(courseIDs)>	OR </cfif>	
			</cfloop>)
		</cfif>
		
		<cfif moduleIDs IS NOT '' AND moduleIDs IS NOT 'notset'>
			AND
			<cfset i = 0>
				v.courseid  IN  (SELECT courseid FROM trngModule WHERE moduleID IN
				 (<cfloop index = "ListElement" list = "#moduleIDs#" delimiters = ","> 
				<cfset i++>
<cfqueryparam cfsqltype="CF_SQL_INTEGER" null="false" value="#ListElement#">
				<cfif i LT ListLen(moduleIDs)>,</cfif>	
			</cfloop>))
		</cfif>
						
						) as x	
						
					where 1=1
						
						
							and active = 1
						
		order by Title_Of_Course
		</cfquery>
		
		<cfset i = 0>
		<cfif courseList.recordcount gt 0>
			<cfsavecontent variable="userXML">
				<cfoutput>
					<allcourses>
			<cfloop query="courseList">
						<course rowcount="#i#">
							<courseId>#courseList.courseid#</courseId>
							<publishedDate>#courseList.publishedDate#</publishedDate>
							<Course_Code><![CDATA[#courseList.Course_Code#]]></Course_Code>
							<Solution_Area><![CDATA[#courseList.Solution_Area#]]></Solution_Area>
							<Level><![CDATA[#courseList.Level#]]></Level>
							<Series><![CDATA[#courseList.Series#]]></Series>
							<SortOrder>#courseList.SortOrder#</SortOrder>
							<active>#courseList.active#</active>
							<description><![CDATA[#courseList.description#]]></description>
							<Title_Of_Course><![CDATA[#courseList.Title_Of_Course#]]></Title_Of_Course>
							<viewCourseId>#courseList.viewCourseId#</viewCourseId>
						</course>
						<cfset i++>
			</cfloop>
					</allcourses>

				</cfoutput>
			</cfsavecontent>
		</cfif>
		
		<cfscript>
			relayWSXMLResponse = build_XML_Response(
				wsMethod="getcourses",
				wsResponseID="200",
				wsResponseText="#statusText#",
				receivedParameter=build_ReceivedParameters(argumentCollection=arguments),
				relayContent="#userXML#");
		</cfscript>
		
		<cfreturn relayWSXMLResponse>
	
	</cffunction>

	<cffunction name="getmodules" access="remote" output="false" returntype="xml">
		<cfargument name="courseIDs" default="" required="no">
		<cfargument name="moduleIDs" default="" required="no">
		
		<cfset var relayWSXMLResponse = "">
		<cfset var statusText = "">
		<cfset var userXML = "">

		<cfquery name="moduleList" datasource = "#application.sitedatasource#">
exec updatePhrasePointerCacheForEntityType
				@entityType  = 'trngcourse',
				@phraseTextID	 ='title',
				@languageid  = 1 ,
				@countryid = 119

exec updatePhrasePointerCacheForEntityType
				@entityType  = 'trngModule',
				@phraseTextID	 ='title',
				@languageid  = 1 ,
				@countryid = 119

select * from 
					(select distinct 
					convert(nVarchar(4000),isnull(p_trngModule_title.phraseText,'')) as Title_Of_Module,
					convert(nVarchar(4000),isnull(p_trngcourse_title.phraseText,'')) as Title_Of_Course,
				Module_Type,v.Filename,Maximum_Score,Maximum_Time_Allowed, Module_Code, moduleid, courseid,duration,Associated_Quiz,v.SortOrder, v.fileid, points, active, availability, publishedDate

							
				,
				'content/' + fT.path + '/' + f.filename as fileNameAndPath,
				fT.secure,
				f.DeliveryMethod
					 
				from vTrngModules v
						left join vphrasepointercache as p_trngModule_title  
									on 		p_trngModule_title.phraseTextID = 'title' 
										and p_trngModule_title.entityTypeID = 73 
										and p_trngModule_title.entityid = moduleID 
										and p_trngModule_title.languageid = 1 
										and  p_trngModule_title.countryid = 119 
						left join vphrasepointercache as p_trngcourse_title  
									on 		p_trngcourse_title.phraseTextID = 'title' 
										and p_trngcourse_title.entityTypeID = 74 
										and p_trngcourse_title.entityid = courseId 
										and p_trngcourse_title.languageid = 1 
										and  p_trngcourse_title.countryid = 119 

						RIGHT OUTER JOIN files as f on f.fileID = v.fileID
						RIGHT OUTER JOIN filetype as ft on ft.filetypeID = f.filetypeID
						RIGHT OUTER JOIN filetypegroup as fg on fg.filetypegroupid = ft.filetypegroupid


		WHERE v.moduleID IS NOT NULL	
		<cfif courseIDs IS NOT '' AND courseIDs IS NOT 'notset'>
			 AND
			<cfset i = 0>
			(<cfloop index = "ListElement" list = "#courseIDs#" delimiters = ","> 
				<cfset i++>
				v.courseId = <cfqueryparam cfsqltype="CF_SQL_INTEGER" null="false" value="#ListElement#">
				<cfif i LT ListLen(courseIDs)>	OR </cfif>	
			</cfloop>)
		</cfif>
		
		<cfif moduleIDs IS NOT '' AND moduleIDs IS NOT 'notset'>
			AND
			<cfset i = 0>
			(<cfloop index = "ListElement" list = "#moduleIDs#" delimiters = ","> 
				<cfset i++>
				v.moduleID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" null="false" value="#ListElement#">
				<cfif i LT ListLen(moduleIDs)>	OR </cfif>	
			</cfloop>)
		</cfif>
					
					) as x	
					
				where 1=1
					
					
					
					
						and active = 1
						order by Title_Of_Course
		</cfquery>
		
		<cfset i = 0>
		<cfif moduleList.recordcount gt 0>
			<cfsavecontent variable="userXML">
				<cfoutput>
					<allmodules>
			<cfloop query="moduleList">		
						<module rowcount="#i#">
							<moduleid>#moduleList.moduleid#</moduleid>
							<courseid>#moduleList.courseid#</courseid>
							<Title_Of_Module><![CDATA[#moduleList.Title_Of_Module#]]></Title_Of_Module>
							<Title_Of_Course><![CDATA[#moduleList.Title_Of_Course#]]></Title_Of_Course>
							<Module_Type><![CDATA[#moduleList.Module_Type#]]></Module_Type>
							<Filename><![CDATA[#moduleList.Filename#]]></Filename>
							<Maximum_Score>#moduleList.Maximum_Score#</Maximum_Score>
							<Maximum_Time_Allowed>#moduleList.Maximum_Time_Allowed#</Maximum_Time_Allowed>
							<Module_Code><![CDATA[#moduleList.Module_Code#]]></Module_Code>
							<duration>#moduleList.duration#</duration>
							<Associated_Quiz><![CDATA[#moduleList.Associated_Quiz#]]></Associated_Quiz>
							<SortOrder><![CDATA[#moduleList.SortOrder#]]></SortOrder>
							<fileid>#moduleList.fileid#</fileid>							
							<points>#moduleList.points#</points>
							<active>#moduleList.active#</active>
							<availability>#moduleList.availability#</availability>
							<publishedDate>#moduleList.publishedDate#</publishedDate>
							<fileNameAndPath>#moduleList.fileNameAndPath#</fileNameAndPath>
							<filesecure>#moduleList.secure#</filesecure>
							<fileDeliveryMethod>#moduleList.DeliveryMethod#</fileDeliveryMethod>
						</module>
						<cfset i++>
			</cfloop>
					</allmodules>

				</cfoutput>
			</cfsavecontent>
		</cfif>
		
		<cfscript>
			relayWSXMLResponse = build_XML_Response(
				wsMethod="getmodules",
				wsResponseID="200",
				wsResponseText="#statusText#",
				receivedParameter=build_ReceivedParameters(argumentCollection=arguments),
				relayContent="#userXML#");
		</cfscript>
		
		<cfreturn relayWSXMLResponse>
	
	</cffunction>

	<cffunction name="build_ReceivedParameters" access="private" output="false" returntype="Array">
		
		<cfset var argumentsList = structkeylist(arguments)>
		<cfset var receivedParameter = arrayNew(2)> <!--- NJH 2009/04/21 CR-SNY655 - initialised receievedParameter variable --->
		
		<cfscript>
			for(i=1;i lte listlen(argumentsList);i=i+1)
			{
				receivedParameter[i][1]="#listGetAt(argumentsList,i)#";
				receivedParameter[i][2]="#evaluate("arguments.#listGetAt(argumentsList,i)#")#";
			}
		</cfscript>
		
		<cfreturn receivedParameter>
		
	</cffunction>

	<cffunction name="build_XML_Response" access="private" output="false" returntype="xml">
		<cfargument name="wsMethod" type="String" required="true">
		<cfargument name="wsResponseID" type="String" required="true">
		<cfargument name="wsResponseText" type="String" required="false">
		<cfargument name="receivedParameter" type="Array" required="false">
		<cfargument name="relayContent" type="string" required="false">
		<cfset var receivedParameters = "">
		<cfset var receivedParameterNo = "">
		<cfset var relayWSXMLResponse = "">
		<cfset var i = "">
		<cfoutput><cfsavecontent variable="receivedParameters">
		<cfif structKeyExists(arguments,"receivedParameter") and isArray(arguments.receivedParameter)>
			<cfset receivedParameterNo = arraylen(arguments.receivedParameter)>
			<cfif receivedParameterNo gt 0>
				<receivedParameters>
				<cfloop index="i" from="1" to="#receivedParameterNo#" step="1">
					<receivedParameter name="#evaluate("arguments.receivedParameter[#i#][1]")#" value="#evaluate("arguments.receivedParameter[#i#][2]")#"></receivedParameter>
				</cfloop>
				</receivedParameters>
			<cfelse>
				<receivedParameters />
			</cfif>
		<cfelse>
			<receivedParameters />
		</cfif>
		</cfsavecontent></cfoutput><cfoutput><cfxml casesensitive="false" variable="relayWSXMLResponse"><relayResponse><relayTag method="#wsMethod#"><status responseid="#wsResponseID#"><![CDATA[#wsResponseText#]]></status>#receivedParameters#<cfif relayContent is ""><content /><cfelse><content>#relayContent#</content></cfif></relayTag></relayResponse></cfxml></cfoutput>
	<cfreturn relayWSXMLResponse>
	</cffunction>

</cfcomponent>
