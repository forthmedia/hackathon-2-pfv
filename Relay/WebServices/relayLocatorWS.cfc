<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		relayLocatorWS.cfc	
Author:			SSS
Date started:	05-05-2009
	
Description:			
This webservice was setup for the locator for ajex purposes
Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2013/07/22			NJH			Case 436183 - instated the orderByFlagOverride from the locatorDef in the search results, so that we can order by partners flagged with a given flag.
2014-01-08	WAB		During Case 438581 - remove calls to PostProcessContentString.  Done automatically callWebService (now including on structures).

Possible enhancements:


 --->

<cfcomponent output="false">
	
	<cffunction name="SetandGetLatlong" access="remote">
		<cfargument name="LocationID" type="numeric" required="yes">
		
		<cfscript>
			var result = structNew();
		</cfscript>
		
		<cfquery name="recalculateLatLong" datasource="#application.siteDataSource#">
			exec sp_getLatLongForLocation @locationID=#arguments.LocationID#
		</cfquery>
		
		<cfquery name="GetLatLong" datasource="#application.siteDataSource#">
			select latitude, Longitude
			from location
			where locationID = #arguments.LocationID#
		</cfquery>
		
		<cfset result.latitude = GetLatLong.latitude>
		<cfset result.Longitude = GetLatLong.Longitude>
		
		<!--- 2009/06/15 GCC Horrible hack to support CF 7 (8 automatically has this method embeded and retuns JSON if you ask it to) 
		- upper only sesnible in this case because result values are numeric - the result is referenced as ucase in data\locDetail.cfm
		--->			
		<cfreturn result>
	</cffunction>

	<cffunction name="getDisplayResults" access="remote">
		<cfargument name="countryID" type="numeric" required="true">
		<cfargument name="location" type="string" required="false">
		<cfargument name="distance" type="numeric" required="false">
		<cfargument name="company" type="string" required="false">
		<cfargument name="flagIDList" type="string" default=""> <!--- holds checkbox value; radio flags come through as flag_#flagGroupID# --->
		<cfargument name="viewType" type="string" required="false">
		<cfargument name="getResultsStep" type="numeric" default="20" required="false">
		
		<cfset var displayResult = application.com.relayLocator.getDisplayResults(argumentCollection=arguments)>
		
		
		<cfreturn displayResult>
	</cffunction>
	
	<cffunction name="getLocatorSearchCriteria" access="remote" returntype="struct">
		<cfargument name="countryID" type="numeric" required="true">
		<cfset var searchResult = application.com.relayLocator.getLocatorSearchCriteria(argumentCollection=arguments)>
		
		<cfreturn searchResult>
	</cffunction>

</cfcomponent>
