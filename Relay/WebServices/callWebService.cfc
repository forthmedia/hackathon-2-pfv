<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			callWebService.cfc	
Author:				
Date started:		WAB 2010/02/11
	
Description:			

A file which is used to call other webservices (primarily via ajax) 

Reasons for using this file
i) 		automatically allow customer extensions in userfiles\content\webservices
ii) 	does not exposing all methods in a WSDL, none of the methods in the actual webservice need to be remote - (which must be an invitation to hackers)
iii) 	allows individual methods to be given different security levels by using the security attribute
iv)		should allow better responses to security failures (JSON requests get JSON, plain test requests get plain text message)

This file is made open to all in directorySecurity.xml
The security for the webservice which is to be called must be in directorySecurity.xml
(only needs to be entered under the relay/webservices , don't need a separate entry for userfiles

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
WAB 2010/03/24 problem with this scope within the called function.  Changed to use cfinvoke rather than createobject followed by a call
WAB 2011/10/19 Added support for security attribute and better messages when security fails
WAB 2011/11/16 Added support for enforcing argument encryption
WAB 2012-06-13 Improved handling of security, added support for allowBlindPost and other attributes on individual methods.  
				ComponentSecurity and MethodSecurity now handled by a single function
WAB 2014-01-07	Pulled some code out into a new function so it could be called from callWebService.cfm
WAB 2014-01-08	Add call to postProcessContentStructure() so that structures as well as strings get postprocessed
WAB 2014-05-21  CASE 440407 Discovered that did not deal with client cfcs which do not extend core cfcs but are still called through callWebservice
				Also found a bit of code which only worked because I had not var'ed a variable.
WAB 2014-05-21  CASE 440407 Discovered that did not deal with client cfcs which do not extend core cfcs but are still called through callWebservice
DCC 2014-12-08  CASE 442933 check not a number line 87
WAB 2015-10-13  ... and not boolean

Possible enhancements:


 --->
<cfcomponent output="false" extends="callWebServiceBase">
 
 	<cffunction name="callWebService" access="remote"  output="false" returnType="any">
		<cfargument name="webservicename" required="true">
		<cfargument name="methodname" required="true">
	
		<cfset var args = "">
		<cfset var result = "">
		<cfset var methodsecurityResult = "">
		
		<cfset ComponentAndMethod = getComponentAndMethodPointer(webservicename = webservicename, methodname = methodname, webservicesFolder="webservices")>
		<cfset MethodPointer = ComponentAndMethod.methodPointer>

		<!--- look for security requirements in /relay/webservices first--->
		<cfset methodsecurityRequirements = application.com.security.getCFCFileAndMethodSecurityRequirements(componentPath = "/webservices/#webservicename#.cfc", methodPointer = methodPointer)>

		<!--- WAB 2014-05-21 CASE 440407 If there aren't any file specific requirements in relay/webservices, look elsewhere based on the actual path of the cfc.  This deals with custom cfcs which are NOT extensions of core cfcs --->
		<cfif methodsecurityRequirements.fileSpecific is false>
			<cfset methodsecurityRequirementsTryAgain = application.com.security.getCFCFileAndMethodSecurityRequirements(componentPath = "#ComponentAndMethod.webServicePath#.cfc", methodPointer = methodPointer)>
			<cfif methodsecurityRequirementsTryAgain.fileSpecific>
				<cfset methodsecurityRequirements = methodsecurityRequirementsTryAgain>
			</cfif>
		</cfif>
		
		<cfset methodsecurityResult = application.com.security.applySecurityRequirements(SecurityRequirements = methodsecurityRequirements)>
		
		<cfif not methodSecurityResult.isOK>	
			<cfset result = createSecurityFailureResult(methodSecurityResult.message,methodPointer)>
		<cfelse>
			<!--- 
				Now call the method,
				all arguments except these are to be passed through to the function 
			--->
			<cfset var args = cleanseInputArguments(arguments)>
			<cfinvoke
			    component = "#ComponentAndMethod.component#"
			    method = "#methodname#"
			    returnVariable = "result"
			    argumentCollection = #args#
			 >
		</cfif>
		<cfreturn prepareOutputResult(result)>
	</cffunction>

</cfcomponent>

