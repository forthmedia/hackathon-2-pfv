<!--- �Relayware. All Rights Reserved 2014 --->

<!--- Modification History
------------------------------
 SBW	17-jan-2017		Sugar 450943/Jira RT-74  Added if test to detect specifyGroups list and order results accordingly.
--->

<cfcomponent>

	<!--- 2013-07-02	YMA	Case 436045 Return all product groups if product categoryID is not defined. --->
	<cffunction name="getProductGroups" access="remote" returntype="query">
		<cfargument name="productCategoryID">

		<cfset var getProductCategoriesQry = "">

		<cfif isNumeric(arguments.productCategoryID)>
			<cfset getProductCategoriesQry = application.com.relayProduct.getProductGroups(productCategoryID=arguments.productCategoryID)>
		<cfelse>
			<cfset getProductCategoriesQry = application.com.relayProduct.getProductGroups()>
		</cfif>

		<cfreturn getProductCategoriesQry>
	</cffunction>

	<!---2013-01-07	YMA	Product Picker Relaytag - 2013 Roadmap Sprint 2 item 92 - make display product groups for div.
		2015-12-16	WAB	CASE 446898 Decided that we don't need to encrypt the productID (not being tested at other end anyway), and this allows links to be shared
	--->
	<cffunction name="getProductGroupsHTMLForProductPicker" access="remote" returnType="string" hint="Gets product groups for a product category and outputs HTML">
		<cfargument name="productCategoryID" type="numeric">
		<cfargument name="contentID" required="true" type="numeric">
		<cfargument name="imageWidth" default="80" type="numeric">
		<cfargument name="noImageFile" type="string">
		<cfargument name="catalogueShowCols" type="string">
		<cfargument name="specifyGroups" type="string" default="">
		<cfargument name="productCatalogueCampaignID" type="string" default="4">

		<cfset var getProductGroupsQry = "">
		<cfset var productGroupQry = application.com.relayProduct.getProductSelectorGroups(argumentCollection=arguments)>
		<cfset var productGroupsHTML = "">
		<cfset var noGroups = "false">
		<cfset var getGroups = "">

		<cfif arguments.specifyGroups neq "">
			<cfquery dbtype="query" name="getGroups">
				select 1 from
				productGroupQry
				where productGroupID in ( <cf_queryparam value="#arguments.specifyGroups#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			</cfquery>

			<cfif getGroups.recordcount eq 0>
				<cfset noGroups = true>
			</cfif>
		</cfif>

		<cfsavecontent variable="productGroupsHTML">
			<cfoutput>
				<cfif not noGroups>
					<cfif arguments.specifyGroups neq "">
						<!--- SBW: specifyGroups is in the customers preferred order. Loop through specifyGroups, select product groups in that order --->
						<cfloop list="#arguments.specifyGroups#" index="i">

							<cfquery name="productGroupQrySorted" dbtype="query">
							select description,portalTitle,productGroupID,productGroupTitleSort,thumbnailURL,title
							from productGroupQry
							where productGroupID = <cfqueryparam cfsqltype="cf_sql_integer" value="#i#">
							</cfquery>

							<cfif listfind(arguments.specifyGroups,productGroupQrySorted.productGroupID)>  <!--- SBW: Only non-empty groups ?? --->
								<div id="productGroup#productGroupQrySorted.productGroupID#" class="productGroup">
									<div id="productGroupSubDiv">
										<div onclick="javascript:openGroup(#productCatalogueCampaignID#,#productGroupQrySorted.productGroupID#,#arguments.contentID#,#arguments.imageWidth#,'#arguments.noImageFile#','#arguments.catalogueShowCols#')">
											<div class="col-xs-3 col-sm-1 categorySelectorImg">
												<img id="groupSelectorImage" src="<cfif productGroupQrySorted.thumbnailurl neq ''>#productGroupQrySorted.thumbnailurl#<cfelse>#arguments.noImageFile#</cfif>" width="#arguments.imageWidth#px"/>
											</div>
											<div class="col-xs-7 col-sm-10">
												<h4 id="productGroupTitle">#productGroupQrySorted.title#</h4>
												<span id="productGroupDescription">#productGroupQrySorted.description#</span>
											</div>
											<div class="col-xs-1 col-sm-1">
												<span id="ViewAll"><button type="button" class="glyphicon glyphicon-chevron-right productsViewAllBtnSub" id="productsViewAllsub#productGroupQrySorted.productGroupID#" /><!--- phr_ViewAll ---></button></span>
											</div>
										</div>
									</div>
									<div id="productHeading#productGroupQrySorted.productGroupID#" class="productHeading" style="display:none"><h3>phr_Products</h3></div>
									<div id="productSelector#productGroupQrySorted.productGroupID#" class="productSelector" style="display:none"></div>
								</div>
							</cfif>
						</cfloop>
					<cfelse>
						<!--- SBW: Otherwise accept default order from productGroupQry (by productGroupTitleSort) --->
						<cfloop query="productGroupQry">
							<cfif arguments.specifyGroups eq "" or listfind(arguments.specifyGroups,productGroupID)>
								<div id="productGroup#productGroupID#" class="productGroup">
									<div id="productGroupSubDiv">
										<div onclick="javascript:openGroup(#productCatalogueCampaignID#,#productGroupID#,#arguments.contentID#,#arguments.imageWidth#,'#arguments.noImageFile#','#arguments.catalogueShowCols#')">
											<div class="col-xs-3 col-sm-1 categorySelectorImg">
												<img id="groupSelectorImage" src="<cfif thumbnailurl neq ''>#thumbnailurl#<cfelse>#arguments.noImageFile#</cfif>" width="#arguments.imageWidth#px"/>
											</div>
											<div class="col-xs-7 col-sm-10">
												<h4 id="productGroupTitle">#title#</h4>
												<cfif len(#title#) gt 0 and len(#description#) gt 0></cfif>  <!--- Eh?? - SBW --->
												<span id="productGroupDescription">#description#</span>
											</div>
											<div class="col-xs-1 col-sm-1">
												<span id="ViewAll"><button type="button" class="glyphicon glyphicon-chevron-right productsViewAllBtnSub" id="productsViewAllsub#productGroupID#" /><!--- phr_ViewAll ---></button></span>
											</div>
										</div>
									</div>
									<div id="productHeading#productGroupID#" class="productHeading" style="display:none"><h3>phr_Products</h3></div>
									<div id="productSelector#productGroupID#" class="productSelector" style="display:none"></div>
								</div>
							</cfif>
						</cfloop>
					</cfif>

				<cfelse>
					<div id="productGroup" class="productGroup">
						<p>Phr_NoProductGroups</p>
					</div>
				</cfif>
			</cfoutput>
		</cfsavecontent>

		<cfreturn application.com.request.postprocessContentString(productGroupsHTML)>
	</cffunction>

	<cffunction name="getProductsHTMLForProductPicker" access="remote">
		<cfargument name="productGroupID" required="true" type="numeric">
		<cfargument name="contentID" required="true" type="numeric">
		<cfargument name="imageWidth" default="80" type="numeric">
		<cfargument name="noImageFile" type="string">
		<cfargument name="catalogueShowCols" type="string">
		<cfargument name="productCatalogueCampaignID" type="string" default="4">

		<cfset var additionalCols = arguments.catalogueShowCols>
		<cfset var getProductsQry = "">
		<cfset var productsQry = "">
		<cfset var Link = "">
		<cfset var dontAllowRatings = "">
		<cfset var Col = "">

		<cfif ListFindNoCase(arguments.catalogueShowCols,'SKU',',')><cfset additionalCols = listDeleteAt(additionalCols,ListFindNoCase(additionalCols,'SKU',','))></cfif>
		<cfif ListFindNoCase(arguments.catalogueShowCols,'title',',')><cfset additionalCols = listDeleteAt(additionalCols,ListFindNoCase(additionalCols,'title',','))></cfif>
		<cfif ListFindNoCase(arguments.catalogueShowCols,'description',',')><cfset additionalCols = listDeleteAt(additionalCols,ListFindNoCase(additionalCols,'description',','))></cfif>

		<cfset productsQry = application.com.relayProduct.getProductSelectorProducts(argumentCollection=arguments,additionalCols = additionalCols)>

		<cfoutput>
		<div class="products">
			<cfif productsQry.recordcount neq 0>
				<cfloop query="productsQry">
					<cfset link = '/?eid=#contentID#&productID=#productID#'>
					<div id="product#productID#" class="product row" onclick="javascript:openProduct('#link#','#productID#');">
						<div id="productImage" class="col-xs-12 col-sm-12 col-md-1 TierproductImage"><img id="productSelectorImage" src="<cfif productThumbnailurl neq ''>#productThumbnailurl#<cfelse>#arguments.noImageFile#</cfif>" width="#arguments.imageWidth#px"/></div>
						<cfif ListFindNoCase(arguments.catalogueShowCols,"sku")><div id="productSKU" class="col-xs-12 col-sm-12 col-md-1 TierSku">#sku#</div></cfif>
						<cfif ListFindNoCase(arguments.catalogueShowCols,"title")><div id="productTitle" class="col-xs-12 col-sm-12 col-md-3 TierTitle">#title#</div></cfif>
						<cfif ListFindNoCase(arguments.catalogueShowCols,"Description")><div id="productDescription" class="col-xs-12 col-sm-12 col-md-3 TierDescription">#Description#</div></cfif>
						<cfif len(additionalCols) gt 0>
							<cfloop list="#additionalCols#" index = "Col">
								<div id="productRating#Col#" class="col-xs-12 col-sm-12 col-md-3 TierAsset"><span class="columnTitle">phr_asset_#Col#: </span><span class="columnValue">#productsQry[Col][currentRow]#</span></div>
							</cfloop>
						</cfif>
						<div id="productRating" class="col-xs-12 col-sm-12 col-md-2 TierRating">
							#application.com.relayRating.getRatingsWidget(entityID=productID,entityTypeID=application.entityTypeID.product,dontAllowRatings=true)#
						</div>
						<div class="col-xs-12 col-sm-12 col-md-2 TierMore">
							<span id="ViewAll">
								<button type="button" id="ui-link productsViewAll#productID#" class="btn btn-primary">phr_product_picker_more_btn</button>
						 	</span>
						</div>
					</div>
				</cfloop>
			<cfelse>
				<div id="product" class="product row">
					<div id="noResults" class="col-xs-12">phr_NoProducts</div>
				</div>
			</cfif>
		</div>
		</cfoutput>
	</cffunction>

</cfcomponent>