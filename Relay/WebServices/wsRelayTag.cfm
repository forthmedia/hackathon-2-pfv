<!--- �Relayware. All Rights Reserved 2014 --->
<cfinclude template="relaywebservicesINI.cfm">

<cfparam name="variables.personID" default="">
<cfparam name="url.loadURL" default="">
<cfparam name="variables.locale" default="">

<!---- IF A CORRECT SESSION EXISTS ---->

<cfif isdefined("estring")>
	<cfset decryptedString = application.com.encryption.DecryptStringAES(StringToDecrypt=replace(URLDecode(estring),"||","+","ALL"),PassPhrase=request.wsPassPhrase)>
	<cftry>
		<cfloop index="i" list="#decryptedString#" delimiters="&">
			<cfset "variables.#listfirst(i,"=")#" = "#listlast(i,"=")#">
		</cfloop>
		<cfcatch></cfcatch>
	</cftry>
</cfif>

<cfif variables.personID neq "" and url.loadURL neq "" and variables.locale neq "">

	<cfscript>
		countryCode=listgetat(locale,2,"_");
		languageCode=listgetat(locale,1,"_");
	</cfscript>

	<cfquery name="qry_get_country" datasource="#application.sitedatasource#">
		select countryID from country where isocode =  <cf_queryparam value="#countrycode#" CFSQLTYPE="CF_SQL_VARCHAR" > 
	</cfquery>
		
	<cfquery name="userDetail" datasource="#application.sitedatasource#">
		select *
		from person
		where personID =  <cf_queryparam value="#variables.personID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</cfquery>
		
	<cfscript>
		application.com.relayCurrentUser.refresh();
		application.com.login.externalUserLoginProcess(userDetail.PersonID);
		application.com.relaycurrentuser.setshowcontentforcountryID(countryID=qry_get_country.countryID);
		application.com.relaycurrentuser.setlanguage(language=languageCode);
		getElements = application.com.relayElementTree.getTreeForCurrentUser(topElementID=request.currentsite.elementTreeDef.TopID );
		debugInfo.languageid = request.relaycurrentuser.languageid ;
		debugInfo.countryid = request.relaycurrentuser.Content.ShowForCountryID ;
		debugInfo.elements = valuelist(getelements.node);
	</cfscript>
	
	<cfif isDefined("tagArguments") and listLen(tagArguments) gt 0>
		<cfloop list="#request.query_string#" index="valuePair" delimiters="&">
			<cfif listLen(valuePair,"=") eq 2>
				<cfset "#listFirst(valuePair,'=')#" = "#listLast(valuePair,'=')#">
			</cfif>
		</cfloop>
	</cfif>
	<!--- ALEX 2 NIKKI- changed to cflocation from cfinclude, you will need to change query string bit to only include what we need--->
	<!--- 2012/02/10 GCC added domain to avoid potential for abuse as a redirection service to third party sites--->
	<!--- 2012/05/17 GCC added listfirst '@' to avoid escaping of the domain fix - hitting http://www.bbc.co.uk@www.facebook.com arrives at facebook!--->
	<cflocation url="#request.currentsite.ProtocolAndDomain##listfirst(url.loadURL,"@")#?#request.query_string#">
<cfelse>
	You may not enter this zone!
</cfif>

