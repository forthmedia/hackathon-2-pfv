<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		WebServices\relaySCORMWS.cfc
Author:			STCR
Date started:	2012-07-04

Description:	Moved SCORM API functions out of relayElearningWS.cfc in order to expose wsdl during API development.

Amendment History:

Date (YYYY-MM-DD)	Initials 	What was changed
2012-06-20			STCR		P-REL109 Phase 2 - Started adding SCORM API methods for SCO courseware.
2015-06-15			WAB			CASE 444587 Implemented SCORMCommit.  Also pulled out the validateRequestCode which was triplicated

Possible enhancements:

 --->

<cfcomponent>

	<cffunction name="validateRequest" access="private" returntype="boolean">
		<cfargument name="SCORMSessionKey" required="true" type="string" />

		<cfset var result = false>
		<cfif
				isDefined("session.elearning.scorm.SCORMSessionKey") and arguments.SCORMSessionKey eq session.elearning.scorm.SCORMSessionKey
			and len(session.elearning.scorm.SCORMSessionKey) gte 30
			and isDefined("session.elearning.scorm.currentUserModuleProgressID") and isNumeric(session.elearning.scorm.currentUserModuleProgressID)
			and session.elearning.scorm.currentUserModuleProgressID gt 0
		>
			<cfset result = true>
		</cfif>

		<cfreturn result>
	</cffunction>

	<!--- 2012-06-20 STCR P-REL109 Phase 2 --->
	<cffunction name="SCORMInitialize" access="public" returntype="struct">
		<!--- Determine RW Person, course, module from RW session --->
		<cfargument name="SCORMSessionKey" required="true" type="string" /><!--- uuid is assumed unique to that instance of relaySCORM.cfm --->

		<cfset var result = structNew() /><!--- return JSON --->

		<cfif validateRequest(SCORMSessionKey = SCORMSessionKey )
				and isDefined("session.elearning.scorm.currentModuleID") and isNumeric(session.elearning.scorm.currentModuleID)
				and session.elearning.scorm.currentModuleID gt 0>
			<cfset result = application.com.relayElearning.SCORMInitialize(userModuleProgressID=session.elearning.scorm.currentUserModuleProgressID, moduleID=session.elearning.scorm.currentModuleID) />
		<cfelse>
			<cfset result.isOk = false />
		</cfif>

		<!--- <cfif application.testsite eq 2>
			<cfdump var="#timeformat(now(),'medium')#" output="D:\web\srdebug-initializeWS.html" format="html" />
			<cfdump var="session.elearning eq " output="D:\web\srdebug-initializeWS.html" format="html" />
			<cfif isDefined("session.elearning")><cfdump var="#session.elearning#" output="D:\web\srdebug-initializeWS.html" format="html" /></cfif>
			<cfdump var="arguments eq " output="D:\web\srdebug-initializeWS.html" format="html" />
			<cfdump var="#arguments#" output="D:\web\srdebug-initializeWS.html" format="html" />
			<cfdump var="result eq " output="D:\web\srdebug-initializeWS.html" format="html" />
			<cfdump var="#result#" output="D:\web\srdebug-initializeWS.html" format="html" />
		</cfif> --->

		<cfreturn result />
	</cffunction>

	<!--- 2012-06-20 STCR P-REL109 Phase 2 --->
	<cffunction name="SCORMTerminate" access="public" returntype="struct">
		<cfargument name="SCORMSessionKey" required="true" type="string" />

		<cfset var result = structNew() />

		<!--- <cfset result = application.com.relayElearning.SCORMTerminate(SCORMSessionKey=arguments.SCORMSessionKey) /> --->
		<cfif validateRequest(SCORMSessionKey = SCORMSessionKey )  >
			<cfset result = application.com.relayElearning.SCORMTerminate(userModuleProgressID=session.elearning.scorm.currentUserModuleProgressID) />
		<cfelse>
			<cfset result.isOk = false />
		</cfif>

		<cfreturn result />
	</cffunction>

	<!---  --->

	<!---
	WAB 2015-06-15 This is Sev's original comment about SCORM Commit
					We have subsequently implemented SCORM Commit
	THIS COMMENT NOW OUT OF DATE (but left in for historical context)
	vvvvvvvvvvvvvvvvvvvvvvvvvvvv
	2012-06-20 STCR P-REL109 Phase 2
	We are already persisting data during setValue(), so no action required. Keeping the method though because the API may still call it.
	If testing observes too high a latency then we may need to have additional logic in the API in relaySCORM.cfm to reduce the number of round trips to the server.
	Uncertain how we would implement that though - most SCOs do not bother to implement commit(), so we would need to auto-commit (eg per n setValue calls, or per t seconds)
	and would need some basis for judging when to do so, but this is difficult to judge, because we do not know what order a SCO will set values in,
	nor when it has finished setting a block of related values. If the commit method was mandatory for SCOs to implement then we could have fewer webservice calls
	whilst keeping the risk data loss down and whilst avoiding the reliance on us guessing when to commit.

	 WAB 2015-06-15
		Implemented a commit strategy
		We are passed the whole datastore and details of all the keys which need saving
	WAB 2015-11-09 Add SQLSsafe attribute to argument collection
	 --->


	<cffunction name="SCORMCommit" access="public" returntype="struct">
		<cfargument name="SCORMSessionKey" required="true" type="string" />
		<cfargument name="Elements" required="false" type="struct" hint="This is the whole datastore" />
		<cfargument name="keys" required="false" type="array" hint="an array of the elements to be updated"/>
		<cfargument name="argumentCollection" sqlsafe="true">  <!--- WAB 2015-11-09 Added this dummy argument and sqlsafe attribute to prevent the elements structure (which is passed as part of an argumentCollection setting off the SQL injection checker.  I have verified that the queries which use this data are properly queryparam'ed) --->

		<cfset var result = {isOK = true}>
		<cftry>
			<cfif validateRequest(SCORMSessionKey = SCORMSessionKey )>
				<cfloop array = "#keys#" index="keyName">
					<!--- 	get correct item from the structure, will return undefined if does not exist
							then check that it is a writeable element and write
					 --->
					<cfset var element = application.com.structureFunctions.structFind_DotNotation(Elements,keyName)>
					<cfif isDefined("element") and listfindnocase("rw,wo",element.access) is not 0>
						<cfset var tempResult = application.com.relayElearning.SCORMSetValue(userModuleProgressID=session.elearning.scorm.currentUserModuleProgressID, ElementName=keyName, ElementValue=element.value) />
						<cfset result.isOK = result.isOK  && tempResult.isOK>

					<cfelse>
						<cfset result.isOk = false />
					</cfif>
				</cfloop>
			<cfelse>
				<cfset result.isOk = false />
				<cfset result.message = "Invalid SCORMSession Key"/>
			</cfif>
		<cfcatch>
			<cfset result.isOK = false />
			<cfset result.catch = cfcatch />
		</cfcatch>
		</cftry>

		<cfreturn result />
	</cffunction>

	<!--- 2012-06-20 STCR P-REL109 Phase 2 --->
	<cffunction name="SCORMSetValue" access="public" returntype="struct">
		<cfargument name="SCORMSessionKey" required="true" type="string" />
		<cfargument name="ElementName" required="true" type="string" />
		<cfargument name="ElementValue" required="true" type="string" />

		<cfset var result = structNew() />

		<!--- <cfif application.testsite eq 2><cfdump var="#timeFormat(now(), 'medium')# #dateFormat(now(), 'medium')# SCORMSetValue arguments:" output="D:\web\srdebug-scorm-ws.html" format="html"></cfif>
		<cfif application.testsite eq 2><cfdump var="#arguments#" output="D:\web\srdebug-scorm-ws.html" format="html"></cfif> --->

		<!--- <cfset result = application.com.relayElearning.SCORMSetValue(SCORMSessionKey=arguments.SCORMSessionKey, ElementName=arguments.ElementName, ElementValue=arguments.ElementValue) /> --->
		<cfif validateRequest(SCORMSessionKey = SCORMSessionKey )>
			<cfset result = application.com.relayElearning.SCORMSetValue(userModuleProgressID=session.elearning.scorm.currentUserModuleProgressID, ElementName="#arguments.ElementName#", ElementValue="#arguments.ElementValue#") />
		<cfelse>
			<cfset result.isOk = false />
		</cfif>
		<!--- <cfif application.testsite eq 2><cfdump var="session.elearning eq " output="D:\web\srdebug-scorm-ws.html" format="html"></cfif>
		<cfif application.testsite eq 2><cfdump var="#session.elearning#" output="D:\web\srdebug-scorm-ws.html" format="html"></cfif>
		<cfif application.testsite eq 2><cfdump var="result eq " output="D:\web\srdebug-scorm-ws.html" format="html"></cfif>
		<cfif application.testsite eq 2><cfdump var="#result#" output="D:\web\srdebug-scorm-ws.html" format="html"></cfif>
		<cfif application.testsite eq 2><cfdump var="session.elearning eq " output="D:\web\srdebug-scorm-ws.html" format="html"></cfif>
		<cfif application.testsite eq 2><cfdump var="#session.elearning#" output="D:\web\srdebug-scorm-ws.html" format="html"></cfif>
		<cfif application.testsite eq 2><cfdump var="-------" output="D:\web\srdebug-scorm-ws.html" format="html"></cfif> --->
		<cfreturn result />
	</cffunction>

</cfcomponent>

