<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
	NJH 2009/03/11 CR-SNY675 added some new functions to bring back products for claim page.
	NJH 2009/05/19 P-SNY069 - tidied up some small items in some functions
	NJh 2009/07/02 P-FNL069 check if user has access to the transaction
	WAB 2012-03-26 Altered WebService functions to return JSON using returnFormat=json, not an old fashioned abort
 --->
<cfcomponent output="false">
	
	<!--- NJH 2009/03/02 CR-SNY671 - check that we're logged in to use these webservices--->
	<cfif not request.relayCurrentUser.isLoggedIn>
		<script>alert('You must be logged in')</script>
		<CF_ABORT>
	</cfif>
	
		<!--- NJH 2009/07/02 P-FNL069 - check if user has rights to access this cashbackClaim --->
	<cfif isDefined("rwTransactionID")>
		<cfif not application.com.rights.doesUserHaveRightsForEntity(entityType="rwTransaction",entityID=rwTransactionID,permission="view")>
			<CF_ABORT>
		</cfif>
	</cfif>

	<cffunction name="getRWTransactionItems" access="remote" output="false">
		<cfargument name="rwTransactionID" required=true>
		<cfargument name="showSerialNo" type="boolean" default="false"> <!--- NJH 2009/03/02 CR-SNY671 --->
		<cfargument name="showInvoiceLink" type="boolean" default="false"> <!--- NJH 2009/03/02 CR-SNY671 --->
		
		<cfscript>
			var invDocumentQry = "";
			var transactionItemsDisplay_html = "";
			var transactionItems = "";
			var result = "";
			
			transactionItems = application.com.relayIncentive.getTransactionItems(RWTransactionID = arguments.RWTransactionID);
		
			// NJH 2009/03/02 CR-SNY671
			if (arguments.showInvoiceLink) {
				invDocumentQry = application.com.relayIncentive.getInvoiceDocumentDetails(entityID=arguments.rwTransactionID);
			}
		</cfscript>
		
		<cfsavecontent variable="transactionItemsDisplay_html">
			<cf_translate>
				<table width="100%" class="withBorder">
					<tr>
						<th align="center">phr_incentive_report_Product</th>
						<!--- NJH 2009/03/02 CR-SNY671 --->
						<cfif arguments.showSerialNo>
							<th align="center">phr_incentive_report_serialNo</th>
						</cfif>
						<th align="center">phr_incentive_report_Invoice</th>
						<th align="center">phr_incentive_report_EndUser</th>
						<th align="center">phr_incentive_report_DiscountPrice</th>
						<th align="center">phr_incentive_report_Quantity</th>
						<th align="center">phr_incentive_report_Points</th>
					</tr>
					<cfoutput>
					<cfif transactionItems.recordCount>
						<cfloop query="transactionItems">
							<tr>
								<td>#SKU#<br>(#Description#)</td>
								<cfif arguments.showSerialNo>
									<td>#serialNumber#</td>
								</cfif>
								<td>
									<!--- NJH 2009/03/02 CR-SNY671 --->
									<cfif arguments.showInvoiceLink and invDocumentQry.recordCount neq 0>
										<a href="#invDocumentQry.webhandle#" target="_blank">#invoiceNumber#</A><BR>
									<cfelse>
										#invoiceNumber#
									</cfif>
								</td>
								<td>#endUserCompanyName#</td>
								<td>#discountPrice#</td>
								<td>#quantity#</td>
								<td>#points#</td>
							</tr>
						</cfloop>
					<cfelse>
						<tr><td colspan="6">phr_incentive_report_ThereAreNoItemsForThisTransaction</td></tr>
					</cfif>
				</cfoutput>
				</table>
			</cf_translate>
		</cfsavecontent>
	
		<cfset result = structNew()>
		<cfset result.content = application.com.request.postProcessContentString(transactionItemsDisplay_html)>

		<cfreturn result>
	
	</cffunction>
	
	
	<!--- NJH 2009/03/02 CR-SNY671 - function returns the file related to the transaction --->
	<cffunction name="getClaimInvoice" access="remote" output="false">
		<cfargument name="entityType" type="string" default="rwTransaction">
		<cfargument name="entityID" type="numeric" required=true>
		
		<cfscript>
			var invDocumentQry = "";
			invDocumentQry = application.com.relayIncentive.getInvoiceDocumentDetails(entityID=arguments.entityID,entityType=arguments.entityType);
		</cfscript>

		<cfreturn application.com.structureFunctions.QueryToArrayOfStructures(query=invDocumentQry)>

	</cffunction>
	
	<!--- NJH 2009/03/11 CR-SNY675 --->
	<cffunction access="remote" name="getFlaggedProductGroupsForSelect" returntype="query" output="false">
		<cfargument name="productCategoryID" required="no">
		<cfargument name="productCatalogueCampaignID" required="no" type="numeric">
		<cfargument name="countryID" type="numeric" required="true">
		<cfargument name="constrainByFlag" type="boolean" default="true">
		<cfargument name="phrasePrefix" type="string" default="">

		<cfscript>
			var productAndGroupList="";
			var getDistinctProductGroups = "";
			
			if (not isNumeric(arguments.productCategoryID)) {
				arguments.productCategoryID=0;
			}
			
			productAndGroupList = application.com.relayIncentive.getProductAndGroupList(argumentCollection=arguments);
		</cfscript>
		
		<cfset selectProductGroupPhrase = "phr_#arguments.phrasePrefix#selectProductGroup">
		
		<cf_translate phrases="#selectProductGroupPhrase#"/>
		
		<!--- we don't want to select 0 as a blank productID, as that won't force the form validation. This only gets fired if the field contains "" --->
		<cfquery name="getDistinctProductGroups" dbType="query">
			select '' as productGroupID, '#evaluate(selectProductGroupPhrase)#' as productGroupDescription, 1 as orderIndex
			from productAndGroupList
			union
			select distinct cast(productGroupID as varchar), productGroupDescription,2 as orderIndex from productAndGroupList
			<cfif productCategoryID eq 0> <!--- NJH 2009/05/19 P-SNY069 - changed from "" to 0--->
				where 1=0
			</cfif>
			order by orderIndex,productGroupDescription
		</cfquery>
		
 		<cfreturn getDistinctProductGroups/>
	</cffunction>

	
	<!--- NJH 2009/03/11 CR-SNY675 --->
	<cffunction access="remote" name="getFlaggedProductsForSelect" returntype="query" output="false">
		<cfargument name="productCatalogueCampaignID" required="no" type="numeric">
		<cfargument name="countryID" type="numeric" required="true">
		<cfargument name="constrainByFlag" type="boolean" default="true">
		<cfargument name="phrasePrefix" type="string" default="">
		<cfargument name="productGroupID" required="no">
		<cfargument name="productCategoryID" required="no">

		<cfscript>
			var productAndGroupList="";
			var getDistinctProducts = "";
			
			if (structKeyExists(arguments,"productCategoryID") and not isNumeric(arguments.productCategoryID)) {
				arguments.productCategoryID=0;
			}
			if (structKeyExists(arguments,"productGroupID") and not isNumeric(arguments.productGroupID)) {
				arguments.productGroupID=0;
			}
			
			productAndGroupList = application.com.relayIncentive.getProductAndGroupList(argumentCollection=arguments);
		</cfscript>
		
		<cfset selectProductPhrase = "phr_#arguments.phrasePrefix#selectProduct">
		<cf_translate phrases="#selectProductPhrase#"/>
		
		<!--- we don't want to select 0 as a blank productID, as that won't force the form validation. This only gets fired if the field contains "" --->
		<cfquery name="getDistinctProducts" dbType="query">
			select '' as productID, '#evaluate(selectProductPhrase)#' as productDescription, 1 as orderIndex
			from productAndGroupList
			union
			select distinct cast(productID as varchar), sku as productDescription,2 as orderIndex from productAndGroupList
				<cfif structKeyExists(arguments,"productGroupID")> <!--- NJH 2009/05/19 P-SNY069 - check if productGroupID exists --->
				where productGroupID = #arguments.productGroupID#
				</cfif>
			order by orderIndex,productDescription
		</cfquery>
		
		<cfreturn getDistinctProducts/>
	</cffunction>

	<cffunction name="getProductCategoriesForCampaign" access="remote" returnType="query">
		<cfargument name="productCatalogueCampaignID" required="false" default="0">
		
		<cfif not isNumeric(arguments.productCatalogueCampaignID)>
			<cfset structDelete(arguments,"productCatalogueCampaignID")>
		</cfif>
		
		<cfreturn application.com.relayProduct.getProductSelectorCategories(argumentCollection=arguments)/>
	</cffunction>

	<cffunction name="getProductGroupsForCategory" access="remote" returnType="query">
		<cfargument name="ProductCategoryID" required="false">
		
		<cfif not isNumeric(arguments.ProductCategoryID)>
			<cfset structDelete(arguments,"ProductCategoryID")>
		</cfif>
		
		<cfreturn application.com.relayProduct.getProductSelectorGroups(argumentCollection=arguments)/>
	</cffunction>

	<cffunction name="getProducts" access="remote" returnType="query">
		<cfargument name="ProductGroupID" required="false">

		<cfset var products = QueryNew("productID,Title")>
		
		<cfif not isNumeric(arguments.ProductGroupID) or (structKeyExists(arguments,"selected") and arguments.selected neq '' and structKeyExists(arguments,"select"))>
			<cfset structDelete(arguments,"ProductGroupID")>
		</cfif>
		<cfif structKeyExists(arguments,"list") or not structKeyExists(arguments,"selected") or (structKeyExists(arguments,"selected") and arguments.selected neq '')>
			<cfset products = application.com.relayProduct.getProductSelectorProducts(argumentCollection=arguments)>
			<cfquery dbtype="query" name="products">
				select *
				from products
				<cfif structKeyExists(arguments,"select") and (structKeyExists(arguments,"selected") and arguments.selected neq '')>
					where productID in (#arguments.selected#)
				</cfif>
				<cfif structKeyExists(arguments,"list") and (structKeyExists(arguments,"selected") and arguments.selected neq '')>
					where productID not in (#arguments.selected#)
				</cfif>
			</cfquery>
		</cfif>
		
		<cfreturn products/>
	</cffunction>
	
</cfcomponent>

