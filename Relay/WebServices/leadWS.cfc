<cfcomponent name="leadWS" output="false" hint="Lead Webservices">

	<cffunction name="getStageID" access="public" output="false">
		<cfargument name="stageTextID" type="string" required="true" />

		<cfset var qOppStage = queryNew('')>
		<cfset var retVal = 0 />
		<cfquery name="qOppStage">
			SELECT OpportunityStageID
			FROM dbo.oppStage
			WHERE stageTextID = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.stageTextID#" />
		</cfquery>
		<cfif qOppStage.recordCount EQ 1>
			<cfset retVal = val(qOppStage.OpportunityStageID)>
		</cfif>
		<cfreturn retVal />
	</cffunction>

	<cffunction name="getOppTypeID" access="public" output="false">
		<cfargument name="oppTypeTextID" type="string" required="true" />

		<cfset var qOppType = queryNew('')>
		<cfset var retVal = 0 />
		<cfquery name="qOppType">
			SELECT oppTypeID
			FROM dbo.opptype
			WHERE oppTypeTextID = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.oppTypeTextID#" />
		</cfquery>
		<cfif qOppType.recordCount EQ 1>
			<cfset retVal = val(qOppType.oppTypeID)>
		</cfif>
		<cfreturn retVal />
	</cffunction>

	<cffunction name="convertLead" access="public" output="true" returnType="struct">
		<cfargument name="leadId" type="numeric" required="true">

		<!--- 2014-11-18	RPW	CORE-97 Added function SaveConvertLeadData to save suplimentary data passed through --->
		<cfscript>
			//this is just security, people should never see the button if not authorised
			if (request.relaycurrentuser.isInternal || ((application.com.settings.getSetting("leadManager.convertLeadOnPortal") or application.com.settings.getSetting('versions.leadScreen') eq 2) AND application.com.leads.leadCanBeConverted(arguments.leadID)) AND application.com.leads.personCanConvertLead()){
				var result = {isOK=true,message=""};

				var result = application.com.relayLeads.convertLead(leadId=arguments.leadID);
				if (result.isOk) {
					arguments.opportunityID = result.opportunityID;
					var dataResult = application.com.relayLeads.SaveConvertLeadData(argumentCollection=arguments);
					if (!dataResult.isOK) {
						result = dataResult;
					}
					application.com.relayui.setMessage(
						message="phr_Lead_Converted_To #result.opportunityID#",
						closeAfter=5
					);

					//we're going to redirect with this token, we can't directly redirect to the opportunity as its an encrypted link (that seems to get very upset being javascript rediirected)
					var opportunityViewTokenObject=new com.tokens.token(tokenTypeTextID="opportunityViewToken",metadata={opportunityID=result.opportunityID});
					opportunityViewTokenObject.persistToDatabase();
					var opportunityViewToken=opportunityViewTokenObject.getTokenValue();

					if (structKeyExists(arguments,"eid")){
						result["opportunityViewToken"]=opportunityViewToken;
					}
				}

				return result;
			}else{
				var result = {isOK=false,message="Not authorised"};
				return result;
			}



		</cfscript>

	</cffunction>


	<cffunction name="delete" access="remote" output="false" returntype="any">
		<cfargument name="leadId" type="numeric" required="true" encrypted="true">

		<cfreturn application.com.relayLeads.delete(argumentCollection=arguments) />
	</cffunction>

	<!--- 2014-11-18	RPW	CORE-97 Added function DisplayConvertLeadDataForm to display form to enter suplimentary opportunity data
			2016-03-09 WAB BF-544  Force to use stacked layout css by passing in blank class
	--->
	<cffunction name="DisplayConvertLeadDataForm" access="remote" output="false" returntype="string">

		<cfquery name="local.qGetOppCustomerTypeID" datasource="#application.sitedatasource#">
			SELECT
				oppCustomerTypeID
				, customerType
			FROM oppCustomerType
		</cfquery>

		<cfscript>
			var qGetStageID = application.com.opportunity.getStageID(showSystemStages=true,liveStatus=true);
		</cfscript>

		<cfsavecontent variable="local.rtnStr">
			<form id="convertForm" NOVALIDATE>
			<cfset displayAttributes={showValidationErrorsInline=true, class=""}>

			<cf_relayFormDisplay attributeCollection="#displayAttributes#" >
					<h4>phr_opp_OpportunityDetails</h4>
					<cf_relayFormElementDisplay relayFormElementType="text" fieldname="detail" label="phr_opp_OpportunityDetails" currentValue="">
					<cf_relayFormElementDisplay relayFormElementType="date" fieldname="expectedCloseDate" label="phr_opp_ExpectedCloseDate" currentValue="" required="1" message="Required" requiredMessage="Required">
					<cf_relayFormElementDisplay relayFormElementType="select" fieldname="OppCustomerTypeID" label="phr_oppCustomerTypeID" currentValue="" query="#qGetOppCustomerTypeID#" display="customerType" value="oppCustomerTypeID" required="1">
					<cf_relayFormElementDisplay relayFormElementType="select" fieldname="stageID" label="phr_opportunity_stageID" currentValue="" query="#qGetStageID#" display="ItemText" value="ID" required="1">
					<cf_relayFormElementDisplay relayFormElementType="numeric" fieldname="budget" label="phr_opportunity_budget" currentValue="" required="1" requiredMessage="Required" validate="numeric">
					<cf_relayFormElementDisplay relayFormElementType="button" currentValue="phr_Convert" fieldname="frmSubmit" label="">
			</cf_relayFormDisplay>
			</form>
		</cfsavecontent>

		<cfreturn local.rtnStr>
	</cffunction>

</cfcomponent>