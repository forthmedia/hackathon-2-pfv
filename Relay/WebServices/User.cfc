<!--- �Relayware. All Rights Reserved 2014

ALL FUNCTIONS IN THIS CFC ARE AVAILABLE GLOBALLY - ENSURE SECURITY RISKS OF NEW FUNCTIONS ARE CONSIDERED

2014-01-08	WAB		During Case 438581 - remove calls to PostProcessContentString.  Done automatically callWebService (now including on structures).
2015-05-19 	WAB 	CASE 444754 support for Rotation of SessionID on Login. 
--->

<cfcomponent output="false">

	<cffunction access="remote" name="setClientTimeOffset"  hint="" returntype="boolean" output="false">
		<cfargument name="ClientUTCOffsetMinutes" type="numeric" required="yes">

		<cfset application.com.relaycurrentuser.setClientTimeOffset (clientutcOffsetMinutes = ClientUTCOffsetMinutes)>

		<cfreturn true>
	</cffunction>


	<cffunction access="public" name="doLogin"  hint="" returntype="struct" output="false" internalUser="true" requiresXSRFToken="false">  <!--- only on internal site at moment --->
		<cfargument name="frmUsername" >
		<cfargument name="frmPassword">

		<cfset var result = {isOK=true, reason='', message=''}>
		<cfset var mergeStruct = structNew()>

		<cfset var checkLogin = application.com.login.authenticateUserV2(username=frmUserName,password=frmPassword)	>

		<cfset result.message = checkLogin.message>

		<cfif checkLogin.isOK>
			<!---Check if the site is restricted to SSO login --->
			<cfset var siteLoginBehaviour=application.com.singleSignOnSiteControl.getSiteSSOBehaviour(request.currentSite.siteDefID)>
			
			<!--- Allowed because its always allowed--->
			<cfset var approvedToLogin=siteLoginBehaviour.defaultAllowRegularLogin>
			
			<cfif not approvedToLogin>
				<!--- See if we have any special dispensation--->
				<cfset usersOrganisationID=application.com.relayEntity.getEntity(entityTypeId=application.entityTypeId.person, entityID=checkLogin.userQuery.personID, fieldList="organisationID", testLoggedInUserRights=false).recordSet.organisationID>
				
				<cfset approvedToLogin=application.com.flag.checkBooleanFlagByID(entityID=usersOrganisationID, flagID="AllowNonSSOLogin")>
			</cfif>

			<cfif approvedToLogin>
				<cfset result.passwordExpiryWarning = checkLogin.userQuery.passwordExpiryWarning>
				<cfset result.passwordExpiresInDays = checkLogin.userQuery.passwordExpiresInDays>
				<cfset application.com.RelayCurrentUser.setLoggedIn(checkLogin.userQuery.personID,2)>
	
				<cfif result.passwordExpiryWarning>
					<cfset mergeStruct = {passwordExpiresInDays=result.passwordExpiresInDays}>
					<!--- <cf_translate phrases="phr_login_passwordWillExpireInDays" mergeStruct="#mergeStruct#"/> --->
					<cfset result.message = "phr_login_passwordWillExpireInDays">
				</cfif>
				<!--- 
					WAB 2015-05-19 CASE 444754 support for Rotation of SessionID on Login. 
					Session rotation will have been initiated by the setLoggedIn function, but is not actually done until onRequestEnd
					In this case the onRequestEnd fails	because we are in a webservice, so do it here (by setting delay to false)		
				 --->
				<cfset application.com.request.rotateSession(delay = false, clear = false)>
			<cfelse>
				<cfset result.isOK = false>
				<cfset result.message="phr_notAuthorisedToUseUsernameAndPassword">
				<cfset result.reason = "notAuthorisedToUseUsernameAndPassword">
			</cfif>

		<cfelse>
			<cfset result.isOK = false>
			<cfset result.reason = checkLogin.reason>
			<cfif result.reason is 'LoginLock'>
				<cfset result.LoginLockDuration = checkLogin.failedUserQuery.LoginLockDuration>
			</cfif>

		</cfif>

		<cfset result._rwsessiontoken = application.com.security.getSessionToken()>

		<cfset result.message = application.com.relayTranslations.translatePhrase(phrase=result.message,mergeStruct=mergeStruct)>
		<cfreturn result>

	</cffunction>

	<cffunction access="public" name="updatePassword"  hint="" returntype="struct" output="false">
		<cfargument name="frmUsername">
		<cfargument name="frmPassword">
		<cfargument name="frmNewPassword1">
		<cfargument name="frmNewPassword2">

		<cfreturn application.com.login.updatePassword(argumentCollection=arguments)>
	</cffunction>

	<cffunction access="public" name="validatePasswordStrength"  hint="" returntype="struct" output="false">
		<cfargument name="Password">

		<cfset var result = {isOK=true,message=""}>

		<cfset var passwordComplexity =  application.com.login.checkPasswordComplexity (Password)>
		<cfset result.isOK = passwordComplexity.isOK >
		<cfset result.message = passwordComplexity.FullDescription >

		<cfreturn result>
	</cffunction>

	<!--- WAB 2011/11/16 enforce encryption on personid --->
	<!--- 2013-03-27	YMA	Case 431444, invalid email message when actually the user is not valid internal user, also lacking explination of the problem. --->
	<cffunction name="sendUserPassword" access="public" returnType="struct" output="false">
		<cfargument name="personID" type="numeric" required="true" encrypted="true">

		<cfset var result=structNew()>
		<cfset var checkUser = application.com.login.isPersonValidUser(personID=arguments.personID)>
		<cfset var defexists = application.com.email.doesEmailDefExist(emailtextid="SendInternalUserPassword")>

		<cfif checkUser.recordcount is 0>
			<cfset result.message = "Not a valid internal user (The criteria is: has a password, has a username, login is not expired, person is active, location is active).">
		<cfelse>
		    <cfif application.com.email.isValidEmail(checkuser.email)>
			    <cfif defexists>
				    <cfset application.com.email.sendEmail(emailtextid="SendInternalUserPassword",personid=arguments.personID)>
				    <cfset result.message= "Email sent to #checkuser.email#">
			    </cfif>
		    <cfelse>
			    <cfset result.message = "#checkuser.email# is not a valid email address.">
		    </cfif>
		</cfif>

		<cfreturn result>

	</cffunction>

	<cffunction name="clearLock" access="public" returnType="struct" output="false" internalUser="true" loginRequired="2" security="admintask:Level1">
		<cfargument name="personID" type="numeric" required="true">
		<cfset result = {isOK = false,message=""}>
		<!--- can only be done by logged in internal user with admin rights --->
		<cfif request.relaycurrentuser.isloggedin and request.relaycurrentuser.isInternal and application.com.login.checkInternalPermissions("admintask","Level1")>
			<cfset application.com.login.clearLoginLock(personid = personid)>
			<cfset result.isOK = true>
			<cfset result.message = "Lock Cleared">
		</cfif>

		<cfreturn result>
	</cffunction>

	<cffunction name="isRelayUserLoggedIn" access="remote" returnType="boolean" output="false">
		<cfargument name="notickle" required="true">

		<cfreturn request.relayCurrentUser.isLoggedIn>

	</cffunction>


	<cffunction name="getExtLoginForm" access="public" returnType="struct" output="false" requiresXSRFToken="false">
		<cfargument name="username" required="true">
		<cfset var result = {html = "", reload=false}>
		
		<cfset var siteLoginBehaviour=application.com.singleSignOnSiteControl.getSiteSSOBehaviour(request.currentSite.siteDefID)>
		<cfif siteLoginBehaviour.defaultAllowSingleSignOn>
			<!--- as we might need to relaunch an SSO we just get us back to the front page --->
			<cfset result.reload=true>
		<cfelse>
			<cfsaveContent variable="result.html">
					<cfoutput>
					<form style="margin:0" method="post">
					<div id="sessionExpiredOuterDiv">
					<h1>Sign In</h1>
						<div id="message">
							<span id="loginMessage">Your session has expired.</span>
						</div>
						<div id="userPassOuterDiv">
							<div id="userPassInnerDiv">
								<div id="UsernameRow" class="form-group">
									<label for="frmUsername">Username</label>
									<CF_INPUT TYPE="TEXT" ID="frmUserName" NAME="frmUserName" disabled VALUE="#username#" class="form-control">
								</div>
								<div id="PasswordRow" class="form-group">
									<label for="frmPassword">Password</label>
									<INPUT TYPE="PASSWORD" placeholder="Password" ID="frmPassword" NAME="frmPassword" VALUE="" MAXLENGTH="50" AUTOCOMPLETE="off" onblur="submitExtLoginForm(this.form);" onkeypress="passwordKeypress(event)" class="form-control">
								</div>
								<div id="loginButton">
									<a href="##" class="loginLink btn btn-primary" onClick="submitExtLoginForm(this.form);">Sign in to Partner Cloud</a>
								</div>
							</div>
						</div>
					</div>
					</form>
					</cfoutput>
				
			</cfsaveContent>
		</cfif>
		<cfreturn result>

	</cffunction>

	<!--- NJH 2013/02/12 --->
	<cffunction name="setIsJasperServerLoginConfirmed" access="remote" output="false" returnType="boolean">
		<cfset application.com.relayCurrentUser.setIsJasperServerLoginConfirmed(loginConfirmed=true)>
		<cfreturn true>
	</cffunction>

</cfcomponent>

