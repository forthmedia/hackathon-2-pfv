<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			relayws.cfm	
Author:				AJC
Date started:		2007-10-14
	
Description:		Relay webservices V2
					- Stripped out Sony Specific Functions and Variables
					- Added function to create the receivedParameters array from the passed arguments
					- Added the isPersonAValidUserOnTheCurrentSite to the login 
					- Added Arguments - headeronly and trackUsage	

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2009/04/21 			NJH			CR-SNY655 - check if receivedParameter is defined and scope the variable
2013-07-09 			NYB 		Case 435968
2014-01-31	WAB 	removed references to et cfm, can always just use / or /?

 --->

<cfcomponent hint="Relay Web Service Functions">
	
	<cfprocessingdirective pageencoding="utf-8">
	
	<cffunction name="RELAY_LOGIN" access="remote" returntype="xml" output="false" hint="Authentication Services for RelayWS">
		<cfargument name="relayusername" type="String" required="true">
		<cfargument name="relaypassword" type="String" required="true">
		<cfargument name="headeronly" type="Boolean" required="true" hint="If set to true, will only return the basic header information of if the request was successful or not. No user data will be returned">
		<cfargument name="trackUsage" type="Boolean" required="true" hint="If set to true, will update the login usage table and will be shown in reports as a valid login">
		
		<cfscript>
			var relayWSXMLResponse="";
			var authenticationResult = application.com.login.authenticateUserV2(urldecode(arguments.relayUsername,"utf-8"),arguments.relayPassword);
			var personAuthenticated = authenticationResult.userQuery;
			var personNotAuthenticated = ''; //authenticationResult.failedUserQuery;
			var userXML = "this would normally be user xml";
			var statusID = 0;
			var statusText = "";
			var userDetailXML = "";
			var qryUserDetails = "";
			var userCheck = "";
			var receivedParameter=build_ReceivedParameters(argumentCollection=arguments);
		</cfscript>
		
		<cfif personAuthenticated.recordCount gt 0>
			<cfset statusID = 200>
				<cfset qryUserDetails = application.com.relayPLO.getPersonDetails(personID=personAuthenticated.personID)>
				<!--- sets any usergroups set by profile --->
				<cfset application.com.login.addPersonToUserGroupsByProfile(personID=personAuthenticated.personID)>
				
				<cfset userCheck = application.com.login.isPersonAValidUserOnTheCurrentSite(personID=personAuthenticated.personID)>
				
				<cfif userCheck.isValidUser>
					<cfif headeronly is false>
						<cfset userDetailXML = GET_USERXML(personID=personAuthenticated.personID)>
						<cfoutput>
							<cfsavecontent variable="userXML">
								#userDetailXML#
							</cfsavecontent>
						</cfoutput>
					<cfelse>
						<cfset userXML = "">
					</cfif>
					<!---  WAB 2008/10/01
					noticed a block on the db.  
					This cfquery had a maxrows = 1 on it but  SQL still has to bring back a whole record set, so replaced with TOP 1
					--->					
					<cfquery name="get_person_Usage" datasource="#application.sitedatasource#" >
						select top 1 loginDate from Usage
						where personID =  <cf_queryparam value="#personAuthenticated.personID#" CFSQLTYPE="CF_SQL_INTEGER" > 
						order by loginDate Desc
					</cfquery>
					
					<cfif get_person_Usage.recordcount neq 0>
						<cfset statusText = "#dateformat(createodbcdatetime(get_person_Usage.loginDate),"DD/MM/YYYY")# #timeformat(createodbcdatetime(get_person_Usage.loginDate),"HH:MM")#">
					<cfelse>
						<cfset statusText = "#dateformat(createodbcdatetime(now()),"DD/MM/YYYY")# #dateformat(createodbcdatetime(now()),"HH:MM")#">
					</cfif> 
					<cfset application.com.login.insertUsage(personID=personAuthenticated.personID,app="Web Service Login")>
				<cfelse>
					<cfset statusText = "Incorrect Details.">
					<cfset statusID = 404>
					<cfoutput>
						<cfsavecontent variable="userXML">
						</cfsavecontent>
					</cfoutput>
				</cfif>
	
		<cfelseif authenticationResult.failedUserQuery.recordcount >
			<!--- This is the case where the username was correct but something else was wrong --->
			<cfset personNotAuthenticated = authenticationResult.failedUserQuery>
			<cfset statusid = personNotAuthenticated.LoginStatus>
			<cfswitch expression="#statusid#">
				<cfcase value="401"><cfset statusText = personNotAuthenticated.loginAttemptsLeft></cfcase>
				<cfcase value="404"><cfset statusText = "Incorrect Details"></cfcase>
				<cfcase value="423"><cfset statusText = personNotAuthenticated.loginLastLock></cfcase>
			</cfswitch>
			
			<cfoutput>
				<cfsavecontent variable="userXML">
				</cfsavecontent>
			</cfoutput>

		<cfelse>
			<!--- This is the case where the username was  wrong --->
				<cfset statusText = "Incorrect Details.">
				<cfset statusID = 404>
				<cfoutput>
					<cfsavecontent variable="userXML">
					</cfsavecontent>
				</cfoutput>
		
		</cfif>
		
		<cfscript>
			relayWSXMLResponse = build_XML_Response(
				wsMethod="RELAY_LOGIN",
				wsResponseID="#statusID#",
				wsResponseText="#statusText#",
				receivedParameter=receivedParameter,
				relayContent="#userXML#");
		</cfscript>

		<cfreturn relayWSXMLResponse>
	</cffunction>
	
	<cffunction name="RELAY_SETRELAYFLAG" access="remote" returntype="xml" output="false" hint="Sets a relay flag against an entity related to person">
		<cfargument name="personID" type="String" required="false">
		<cfargument name="relayUsername" type="String" required="false">
		<cfargument name="flagTextIDs" type="String" required="true">
		<cfargument name="justUnsetFlag" type="String" required="false" default="false">
		<cfscript>
			var relayWSXMLResponse="";
			var statusID = 0;
			var statusText = "";
			var userDetailXML = "";
			var qryUserDetails = "";
			var errorFlagTextIDs = "";
			var directDebitEnabled = "";
			var entityID = "";
			var receivedParameter=build_ReceivedParameters(argumentCollection=arguments);
		</cfscript>
		<cfif arguments.relayusername is not "">
			<cfquery name="get_personID" datasource="#application.sitedatasource#">
				select personID from person where username =  <cf_queryparam value="#arguments.relayusername#" CFSQLTYPE="CF_SQL_VARCHAR" > 
			</cfquery>
			<cfif get_personID.recordcount neq 0>
				<cfset entityID = get_personID.personID>
			</cfif>
		</cfif>
		<cfif arguments.personID is not "">
			<cfset entityID = arguments.personID>
		</cfif>
		<cfif entityID is not "">
		<cfif listlen(arguments.flagTextIDs) gt 0>
			<cfscript>
				listlength = listLen(arguments.flagTextIDs,"-");
				for(i=1; i lte listlength; i=i+1) {
					theFlagID = listGetAt(arguments.flagTextIDs,i,"-") ;
					if (application.com.flag.doesflagexist(theFlagID)) {
						//					
					} else {
						status = 404;
						listappend(errorFlagTextIDs,theFlagID);
					}		
				}
				if (statusID eq 0)
				{
					for(j=1; j lte listlength; j=j+1) {
						theFlagID = listGetAt(arguments.flagTextIDs,j,"-") ;
						y = application.com.flag.unsetBooleanFlag(entityID=entityID,flagID=theFlagID);
						if (justUnsetFlag eq false or justUnsetFlag eq ""){
							x = application.com.flag.setBooleanFlag(entityID=entityID,flagTextID=theFlagID);
						}				
						
					}
					statusID = 200;
					statusText = "Success";
				}
				else
				{
					statusText = "#errorFlagTextIDs#";
				}
			</cfscript>
		<cfelse>
			<cfset statusID = 400>
			<cfset statusText  = "Bad Request">
		</cfif>
		<cfelse>
			<cfset statusID = 404>
			<cfset statusText  = "Person Not Found">
		</cfif>
		<cfscript>
			relayWSXMLResponse = build_XML_Response(
				wsMethod="RELAY_SETRELAYFLAG",
				wsResponseID="#statusID#",
				wsResponseText="#statusText#",
				receivedParameter=receivedParameter,
				relayContent="");
		</cfscript>

		<cfreturn relayWSXMLResponse>
	</cffunction>
		
	<cffunction name="RELAY_SHOWADVERT" access="remote" returntype="xml" output="false" hint="Shows Events a Person as Rights to">
		<cfargument name="locale" type="String" required="true">
		<cfargument name="elementBranchRootID" type="String" required="true">
		<cfargument name="advertMethod" type="String" required="true">
		<cfargument name="startPosition" type="String" required="true">
		<cfargument name="bannerElementIDList" type="String" required="false">
		
		
		<cfscript>
			var relayWSXMLResponse="";
			var statusID="";
			var statusText="";
			var advertCode_xml="";
			var preadvertCode="";
			var siteDomain = CGI.SERVER_NAME;
			var relayContent = "";
			var advertWidth="207";
			var advertHeight="117";
			var receivedParameter=build_ReceivedParameters(argumentCollection=arguments);
		</cfscript>
		
		<cfif startPosition is "">
			<cfset startPosition = 0>
		</cfif>
		
		<cftry>
			<cfset statusID = 200>
			<cfset statusText="Success">
			<cftry>
				<cfset countryCode=listgetat(locale,2,"_")>
				<cfset languageCode=listgetat(locale,1,"_")>
				<cfcatch>
					<cfset countryCode="xxxxx">
					<cfset languageCode="xxxxx">
				</cfcatch>
			</cftry>
			
			<cfsavecontent variable="advertCode_xml">
				<!--- <cfinclude template="../application .cfm"> --->
				<cfinclude template="relaywebservicesINI.cfm">
				<cfset sitedomain = request.ws_iFrameURL>
				<!--- NJH 2008/08/18 CR-SNY651 - change from http to https --->
				<cfoutput><cfset iFrameURL = "https://#request.ws_iFrameURL#/?etid=#arguments.elementBranchRootID#&elementBranchRootID=#arguments.elementBranchRootID#&advertMethod=#arguments.advertMethod#&startPosition=#arguments.startPosition#&bannerElementIDList=#arguments.bannerElementIDList#">
				<![CDATA[<iframe src="#iFrameURL#" name="relayIFrame" id="relayIFrame" width="#advertwidth#" height="#advertheight#" marginwidth="0" marginheight="0" hspace="0" vspace="0" scrolling="no" frameborder="0" style="border: 0px solid ##FFFFFF; width: #advertwidth#px; height: #advertheight#px;"></iframe>]]></cfoutput>
			</cfsavecontent>
			
			
			<cfcatch>
				<cfset statusID = 400>
				<cfset statusText="Bad Request">
			</cfcatch>
		</cftry>
		
		<cfscript>
			relayWSXMLResponse = build_XML_Response(
			wsMethod="RELAY_SHOWADVERT",
			wsResponseID="#statusID#",
			wsResponseText="#statusText#",
			receivedParameter=receivedParameter,
			relayContent="#advertCode_xml#");
		</cfscript>
		
		<cfreturn relayWSXMLResponse>
		
	</cffunction>
	
	<cffunction name="RELAY_GETCONTENT" access="remote" returntype="xml" output="false" hint="Get Relayware Content">
		<cfargument name="personID" type="String" required="false">
		<cfargument name="relayusername" type="String" required="false">
		<cfargument name="locale" type="String" required="true">
		<cfargument name="etid" type="String" required="true">
		<cfargument name="linkType" type="String" required="true" hint="iframe or URL(default)">
		
		<cfscript>
			var countryCode="";
			var languageCode="";
			var relayWScontentURL_xml="";
			var relayWSXMLResponse="";
			var isWebServiceCall = true;
			var userAuthenticated = false;
			var userLogin = "";
			var returnLink = "";
			var contentURL = "";
			var receivedParameter=build_ReceivedParameters(argumentCollection=arguments);
			
			if (arguments.linkType is "iframe")
				{returnLink = "iframe";}
			else
				{returnLink = "URL";}
			
			if ((not isDefined("arguments.personID") or arguments.personID eq "") and (not isDefined("arguments.relayusername") or arguments.relayusername eq "")) {
				arguments.personID = "404";
			}
			debugInfo = structNew() ;
		</cfscript>
		
		<cfif arguments.locale neq "">
			<cftry>
				<cfset countryCode=listgetat(locale,2,"_")>
				<cfset languageCode=listgetat(locale,1,"_")>
				<cfcatch>
					<cfset countryCode="xxxxx">
					<cfset languageCode="xxxxx">
				</cfcatch>
			</cftry>
			
			<cfquery name="qry_get_country" datasource="#application.sitedatasource#">
				select countryID from country where isocode =  <cf_queryparam value="#countrycode#" CFSQLTYPE="CF_SQL_VARCHAR" > 
			</cfquery>
		
			<cfquery name="qry_get_language" datasource="#application.sitedatasource#">
				select languageID from language where isocode =  <cf_queryparam value="#languagecode#" CFSQLTYPE="CF_SQL_VARCHAR" > 
			</cfquery>
		</cfif>
		
		<cfif isDefined("arguments.personID") and arguments.personID neq "">
			<cfset userLogin = arguments.personID>
		</cfif>
		
		<cfif isDefined("arguments.relayusername") and arguments.relayusername neq "">
			<cfset userLogin = arguments.relayusername>
		</cfif>
		
		<cfif (arguments.locale eq "") or (qry_get_country.recordcount neq 0 and qry_get_language.recordcount neq 0)>
		
			<cfset userAuthenticated = authenticateWebServiceUser(userLogin=userLogin)>
			<cfset wsResponseText = "">
			<cfif userAuthenticated.StatusID neq 200>
				<cfif userAuthenticated.StatusID is 400>
					<cfset wsResponseText = "Bad Request">
				<cfelse>
					<cfset wsResponseText = "Invalid User">
				</cfif>
				
			<cfelse>
			
				<cfif userAuthenticated.statusID eq 200>
					<cfset wsResponseText = "Success">
					
					<!--- <cfinclude template="../application .cfm"> --->
						
					<cfif fileexists("#application.paths.code#\cftemplates\relaywsINI.cfm")>
						<cfinclude template="/code/cftemplates/relaywsINI.cfm">
					<cfelse>
						<cfset request.ws_URL = "#CGI.SERVER_NAME#">
						<cfset request.wsPassPhrase = application.com.settings.getsetting('Security.Encryptionmethods.Rwsso.Passkey')>
						<cfset request.ws_Protocal = "http://">
						<cfset request.ws_URLExpiry = "10">
					</cfif>
						
					<cfset sitedomain = request.ws_URL>
					<cfset encryptedQueryString = URLEncodedFormat(replace(application.com.encryption.EncryptStringAES(StringToEncrypt="locale=#arguments.locale#&personID=#userAuthenticated.userDetail.PersonID#&datetime=#dateadd("s",request.ws_URLExpiry,createdatetime(year(now()),month(now()),day(now()),hour(now()),minute(now()),second(now())))#",PassPhrase=request.wsPassPhrase),"+","||","ALL"))>
					<!--- NJH 2008/08/18 CR-SNY651 - change from http to https --->
					<cfset contentURL = "#request.ws_protocal##request.ws_URL#/WebServices/wsRelayLink.cfm?eString=#encryptedQueryString#&loadURL=/&etid=#arguments.etid#&tagArguments=true">
					<cfif returnLink is "iframe">
						<cfsavecontent variable="relayWScontentURL_xml">
							<cfoutput><![CDATA[<iframe frameborder="0" style="border: 0px solid ##FFFFFF; width: 538px; height: 500px;" name="relayIFrame" id="relayIFrame" width="538" height="500" src="#contentURL#"></iframe>]]></cfoutput>
						</cfsavecontent>
					<cfelse>
						<cfsavecontent variable="relayWScontentURL_xml">
							<cfoutput><![CDATA[#contentURL#]]></cfoutput>
						</cfsavecontent>
					</cfif>
				</cfif>
			</cfif>
		<cfelse>
			<cfset userAuthenticated = structnew()>
			<cfset userAuthenticated.statusID = 403>
			<cfset wsResponseText = "Locale Invalid">
		</cfif>
		
		<cfscript>
			relayWSXMLResponse = build_XML_Response(
				wsMethod="RELAY_GETCONTENT",
				wsResponseID="#userAuthenticated.statusID#",
				wsResponseText= wsResponseText,
				receivedParameter=receivedParameter,
				relayContent="#relayWScontentURL_xml#");
		</cfscript>
		
		<cfreturn relayWSXMLResponse>
	</cffunction>

	<cffunction name="RELAY_SENDEMAIL" access="remote" returntype="xml" output="false" hint="Get Relayware Content">
		<cfargument name="emailTextID" type="String" required="true">
		<cfargument name="personID" type="String" required="false" default="">
		<cfargument name="relayusername" type="String" required="false" default="">
		<cfargument name="cfmailType" type="String" required="false" default="text">
		<cfargument name="bccopy" type="String" required="false" default="" hint="additional blind copy emails">
		<cfargument name="ccAddress" type="String" required="false" default="" hint="additional cc address">
		<cfargument name="recipientAlternativeEmailAddress" type="String" required="false" default="" hint="email personalised to PersonID but sent to a different email address">
		<cfargument name="recipientAlternativePersonID" type="String" required="false" default="" hint="email personalised to arguments.PersonID but sent to different person - recipientAlternativePersonID">
		<cfargument name="countryID" type="String" required="false" default="" hint="">
		<cfargument name="languageID" type="String" required="false" default="" hint="">
		<cfargument name="test" type="boolean" required="false" default="false" hint="">
		<cfargument name="entityID" type="String" required="false" default="0" hint="">
		
		<cfscript>
			var relayWSXMLResponse="";
			var isWebServiceCall = true;
			var userAuthenticated = false;
			var userLogin = "";
			var receivedParameter=build_ReceivedParameters(argumentCollection=arguments);
			
			if ((not isDefined("arguments.personID") or arguments.personID eq "") and (not isDefined("arguments.relayusername") or arguments.relayusername eq "")) {
				arguments.personID = "404";
			}
			debugInfo = structNew() ;
		</cfscript>
				
		<cfif isDefined("arguments.personID") and arguments.personID neq "">
			<cfset userLogin = arguments.personID>
		</cfif>
		
		<cfif isDefined("arguments.relayusername") and arguments.relayusername neq "">
			<cfset userLogin = arguments.relayusername>
		</cfif>

		<cfset userAuthenticated = authenticateWebServiceUser(userLogin=userLogin)>
		
		<cfset wsResponseText = "">
		<cfif userAuthenticated.StatusID neq 200>
			<cfif userAuthenticated.StatusID is 400>
				<cfset wsResponseText = "Bad Request">
			<cfelse>
				<cfset wsResponseText = "Username Invalid">
			</cfif>
				
		<cfelse>
			<cfif userAuthenticated.statusID eq 200>
				
				<cfset emailReturn = application.com.email.sendemail(
						personID=userAuthenticated.userDetail.personID,
						emailTextID=arguments.emailTextID,
						cfmailType=arguments.cfmailType,
						bccopy=arguments.bccopy,
						ccAddress=arguments.ccAddress,
						recipientAlternativeEmailAddress=arguments.recipientAlternativeEmailAddress,
						recipientAlternativePersonID=arguments.recipientAlternativePersonID,
						countryID=arguments.countryID,
						languageID=arguments.languageID,
						test=arguments.test,
						entityID=arguments.entityID)>
				
				<cfset wsResponseText = emailReturn>
				
				<cfif emailReturn is not "Email Sent">
					<cfset userAuthenticated.statusID = 400>
				</cfif>
			</cfif>
		</cfif>
		
		<cfscript>
			relayWSXMLResponse = build_XML_Response(
				wsMethod="RELAY_SENDEMAIL",
				wsResponseID="#userAuthenticated.statusID#",
				wsResponseText= wsResponseText,
				receivedParameter=receivedParameter,
				relayContent="");
		</cfscript>
		
		<cfreturn relayWSXMLResponse>
	</cffunction>
	
	<cffunction name="GET_USERXML" access="remote" returntype="xml" output="false" hint="Get Relayware Content">
		<cfargument name="personID" type="String" required="false">
		<cfargument name="relayusername" type="String" required="false">
		<cfscript>
			var relayWSXMLResponse="";
			var qry_get_per_UGs = "";
			var userDetails = getUserDetails(personID=arguments.personID);
		</cfscript>
		
		<cfquery name="qry_get_per_UGs" datasource="#application.siteDataSource#">
			select Name as ug from rightsGroup rg 
			inner join usergroup ug on rg.usergroupid = ug.usergroupID
			where rg.personID =  <cf_queryparam value="#arguments.personID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfquery>
		
		<cfif userDetails.recordcount gt 0>
			<cfsavecontent variable="relayWSXMLResponse">
				<cfoutput><person username="#userDetails.username#" personID="#arguments.personID#">
					<profile>
						<organisation organisationID="#userDetails.organisationID#">
							<cfif userDetails.organisationName is not ""><organisationName><![CDATA[#userDetails.organisationName#]]></organisationName><cfelse><organisationName /></cfif>
							<cfif userDetails.AKA is not ""><alsoKnowAs><![CDATA[#userDetails.AKA#]]></alsoKnowAs><cfelse><alsoKnowAs /></cfif>
							<cfif userDetails.VATNumber is not ""><VATNumber>#userDetails.VATNumber#</VATNumber><cfelse><VATNumber /></cfif>
							<cfif userDetails.orgCountryISO is not ""><countryISO>#userDetails.orgCountryISO#</countryISO><cfelse><countryISO /></cfif>
						</organisation>
						<location>
							<cfif userDetails.siteName is not ""><sitename><![CDATA[#userDetails.siteName#]]></sitename><cfelse><sitename /></cfif>
							<cfif userDetails.address1 is not ""><address1><![CDATA[#userDetails.address1#]]></address1><cfelse><address1 /></cfif>
							<cfif userDetails.address2 is not ""><address2><![CDATA[#userDetails.address2#]]></address2><cfelse><address2 /></cfif>
							<cfif userDetails.address3 is not ""><address3><![CDATA[#userDetails.address3#]]></address3><cfelse><address3 /></cfif>
							<cfif userDetails.address4 is not ""><address4><![CDATA[#userDetails.address4#]]></address4><cfelse><address4 /></cfif>
							<cfif userDetails.address5 is not ""><address5><![CDATA[#userDetails.address5#]]></address5><cfelse><address5 /></cfif>
							<cfif userDetails.postalCode is not ""><postalCode>#userDetails.postalCode#</postalCode><cfelse><postalCode /></cfif>
							<cfif userDetails.countryISO is not ""><country>#userDetails.countryISO#</country><cfelse><country /></cfif>
						</location>
						<cfif userDetails.salutation is not ""><salutation><![CDATA[#userDetails.salutation#]]></salutation><cfelse><salutation /></cfif>
						<cfif userDetails.firstName is not ""><firstName><![CDATA[#userDetails.firstName#]]></firstName><cfelse><firstName /></cfif>
						<cfif userDetails.lastName is not ""><lastName><![CDATA[#userDetails.lastName#]]></lastName><cfelse><lastName /></cfif>
						<cfif userDetails.email is not ""><email><![CDATA[#userDetails.email#]]></email><cfelse><email /></cfif>
						<cfif userDetails.siteName is not ""><officePhone><![CDATA[#userDetails.siteName#]]></officePhone><cfelse><officePhone /></cfif>
						<cfif userDetails.mobilePhone is not ""><mobilePhone><![CDATA[#userDetails.mobilePhone#]]></mobilePhone><cfelse><mobilePhone /></cfif>
						<cfif userDetails.faxPhone is not "">	<faxPhone><![CDATA[#userDetails.faxPhone#]]></faxPhone><cfelse><faxPhone /></cfif>
						<cfif userDetails.jobDesc is not ""><jobDescription><![CDATA[#userDetails.jobDesc#]]></jobDescription><cfelse><jobDescription /></cfif>
					</profile>
					</cfoutput>
					<cfif qry_get_per_UGs.recordcount is 0>
					<groups />
					<cfelse>
					<groups>
						<cfoutput query="qry_get_per_UGs"><group><![CDATA[#ug#]]></group></cfoutput>
					</groups>
					</cfif>
				</person>
			</cfsavecontent>
		</cfif>	
		
		<cfreturn relayWSXMLResponse>
		
	</cffunction>
	
	<cffunction name="build_XML_Response" access="private" output="false" returntype="xml">
		<cfargument name="wsMethod" type="String" required="true">
		<cfargument name="wsResponseID" type="String" required="true">
		<cfargument name="wsResponseText" type="String" required="false">
		<cfargument name="receivedParameter" type="Array" required="false">
		<cfargument name="relayContent" type="string" required="false">
		
		<cfset var receivedParameters_xml = "">
		<cfset var receivedParameterNo = "">
		<cfset var relayWSXMLResponse = "">
		<cfset var i = "">
		<!--- has a receivedParameter value been passed --->
		<!--- NJH 2009/04/21 CR-SNY655 - check if receivedParameter is defined and scope the variable.--->
		<cfoutput>
			<cfsavecontent variable="receivedParameters_xml">
		<cfif structKeyExists(arguments,"receivedParameter") and isArray(arguments.receivedParameter)>
			<cfset receivedParameterNo = arraylen(arguments.receivedParameter)>
			<cfif receivedParameterNo gt 0>
				<receivedParameters>
				<cfloop index="i" from="1" to="#receivedParameterNo#" step="1">
					<receivedParameter name="#evaluate("arguments.receivedParameter[#i#][1]")#" value="#evaluate("arguments.receivedParameter[#i#][2]")#"></receivedParameter>
				</cfloop>
				</receivedParameters>
			<cfelse>
				<receivedParameters />
			</cfif>
		<cfelse>
			<receivedParameters />
		</cfif>
		</cfsavecontent>
		</cfoutput>
		
		<cfoutput>
		<cfxml casesensitive="false" variable="relayWSXMLResponse">
			<?xml version='1.0' encoding='utf-8'?>
			<relayResponse>
				<relayTag method="#htmleditformat(wsMethod)#">
					<status responseid="#htmleditformat(wsResponseID)#"><![CDATA[#htmleditformat(wsResponseText)#]]></status>
					#receivedParameters_xml#
					<cfif relayContent is "">
					<content />
					<cfelse>
					<content>
						#htmleditformat(relayContent)#
					</content>
					</cfif>
				</relayTag>
			</relayResponse>
		</cfxml>
		</cfoutput>
		<cfreturn relayWSXMLResponse>
	</cffunction>
	
	<cffunction name="build_ReceivedParameters" access="private" output="false" returntype="Array">
		
		<cfset var argumentsList = structkeylist(arguments)>
		<cfset var receivedParameter = arrayNew(2)> <!--- NJH 2009/04/21 CR-SNY655 - initialised receievedParameter variable --->
		
		<cfscript>
			for(i=1;i lte listlen(argumentsList);i=i+1)
			{
				receivedParameter[i][1]="#listGetAt(argumentsList,i)#";
				receivedParameter[i][2]="#evaluate("arguments.#listGetAt(argumentsList,i)#")#";
			}
		</cfscript>
		
		<cfreturn receivedParameter>
	</cffunction>
	
	<cffunction access="private" name="authenticateWebServiceUser" output="false" returntype="any">
		<cfargument name="userLogin" required="true">
		<cfargument name="datasource" default="#application.sitedatasource#">
		<cfset var authenticationDetail = structNew()>
		<cfset var userDetail = "">
		
		<cfset authenticationDetail.userLogin = arguments.userLogin>
		
		<cfquery name="userDetail" datasource="#datasource#">
			select	*
			from	person
			where
				<!--- if it is numeric, then check it as a personID, if it is not, then check it as a username --->
				<cfif isNumeric(userLogin)>
					personID =  <cf_queryparam value="#arguments.userLogin#" CFSQLTYPE="cf_sql_integer" >
				<cfelse>
					username =  <cf_queryparam value="#arguments.userLogin#" CFSQLTYPE="CF_SQL_VARCHAR" > 
					or
					email =  <cf_queryparam value="#arguments.userLogin#" CFSQLTYPE="CF_SQL_VARCHAR" > 
				</cfif>
		</cfquery>
		
		<cfset authenticationDetail.userDetail = userDetail>
		
		<cfif userDetail.recordcount eq 0>
			<cfset authenticationDetail.statusID = 403>
		<cfelseif userDetail.recordcount gt 1>
			<cfset authenticationDetail.statusID = 400>
		<cfelse>
			<cfset authenticationDetail.statusID = 200>
		</cfif>
		
		<cfreturn authenticationDetail>
		
	</cffunction>
	
	<cffunction name="getUserDetails" access="private" output="false" returntype="query">
		<cfargument name="personID" type="String" required="true">
		
		<cfset var userDetails = "">
		
		<cfquery name="userDetails" datasource="#application.sitedatasource#">
			select	*, c.isoCode as countryISO, oc.isoCode as orgcountryISO
			from	person p inner join
					location l on p.locationid = l.locationid inner join
					organisation o on p.organisationid = o.organisationid inner join
					country c on c.countryID = l.countryID inner join
					country oc on oc.countryID = o.countryID
				where	p.personid =  <cf_queryparam value="#arguments.personID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfquery>
		
		<cfreturn userDetails>
	</cffunction>

	<!--- START: 2013-07-09 NYB Case 435968 added --->
	<cffunction name="refreshScreen" access="remote" returnType="void" >
		<cfargument name="frmLocationID" type="string" required="true">
		<cfargument name="frmCountryID" type="numeric" required="true">
		<cfargument name="frmcurrentscreenid" default="newlocation">
		<cfargument name="frmPersonID" type="string" default="0">

		<cfoutput>
			<cfinclude template="/RelayTagTemplates/getrelayscreen.cfm">
		</cfoutput>
		
	</cffunction>
	<!--- END: 2013-07-09 NYB Case 435968 --->
	
</cfcomponent>

