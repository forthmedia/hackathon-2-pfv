<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent output="true">

	<cffunction name="addActivity" access="remote">
		<cfargument name="activityType" type="string" required="true">
		<cfargument name="personID" type="numeric" required="false">
		<cfargument name="organisationID" type="numeric" required="true">
		<cfargument name="countryID" type="numeric" required="false">
		<cfargument name="created" type="datetime" required="false">
		
		<cfreturn application.com.metrics.addActivity(argumentCollection=arguments)>
	</cffunction>

</cfcomponent>