	<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			discussions.cfc
Author:
Date started:

Description:	Presents the discussion groups, the discussions in each group, and the messages in each discussion

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
27-02-2015			ACPK		FIFTEEN-234	Updated layout of discussion messages
2015-06-25			WAB			Altered undocumented change of 2015-05-21 GCC.  References to request.discussionAdmin replaced with a call to a function.
		 						request.discussionAdmin was set in a .cfm file so not available when these functions called as an Ajax webservice
2015-06-29			WAB			Reverted GCC/CTW 2015/05/21 CASE 444784.  Attempt to stop XSS being reflected to browser didn't work and caused the edit text to be encoded (eg space to %20) which wasn't helpful
2015-11-11			WAB			Added lots of HTMLEditFormats to URL to prevent XSS attacks and changed some jsStringFormats which should have been htmleditformats

Possible enhancements:
--->
<cfcomponent output="false">

<cffunction name="isDiscussionAdmin" access="public">
	<cfreturn application.com.login.checkInternalPermissions("socialTask", "Level3")>

</cffunction>


<cffunction name="getDiscussionGroup" access="remote">
	<cfargument name="discussionGroupIDs" required="false" default="">
	<cfargument name="searchString" required="false" default="">
	<cfargument name="pageNum" required="false" default="">
	<cfargument name="recordsPerPage" required="false" default="">
	<cfargument name="cacheQuery" type="boolean" default="true">

	<cfset var getGroupsSQL = "">
	<cfset var tempTable = "####discussionGroups_#replace(replace(session.sessionID,'-','','ALL'),'.','')#">
	<cfset var result = {recordSet=queryNew("discussionGroupID,title,description"),hasMore=false}>

	<cfprocessingdirective suppresswhitespace="yes">
		<cfsavecontent variable="getGroupsSQL">
			<cfoutput>
				SELECT g.discussionGroupID, g.title, g.description
				FROM discussionGroup as g
				<cfif trim(arguments.searchString) is not "">INNER JOIN CONTAINSTABLE(vDiscussionGroupSearch,*,<cf_queryparam value='#application.com.search.prepareSearchString(arguments.searchString)#' cfsqltype="CF_SQL_VARCHAR">) as s ON g.discussionGroupID = s.[key]</cfif>
				WHERE g.active = 1
				<cfif listLen(discussionGroupIDs)>AND g.discussionGroupID IN (<cf_queryParam value="#arguments.discussionGroupIDs#" cfsqltype="CF_SQL_INTEGER" list="true">)</cfif>
				ORDER BY g.title
			</cfoutput>
		</cfsavecontent>
	</cfprocessingdirective>

	<cftry>
		<!--- if we're getting activities since a certain date, then run this against the db directly as we don't want to mess with the original cached query! --->
		<cfif not arguments.cacheQuery>
			<cfquery name="recordSet" datasource="#application.sitedataSource#">
				#preserveSingleQuotes(getGroupsSQL)#
			</cfquery>
			<cfset result.recordSet = recordSet>
		<cfelse>
			<cfif arguments.pageNum eq 0 or not isNumeric(arguments.pageNum)>
				<!--- if we're refreshing the feed, then delete the existing cached query and refresh it --->
				<cfset application.com.dbTools.deleteCachedQuery(tempTable=tempTable)>
				<cfset structAppend(result,application.com.dbTools.dbCacheQuery(tempTable=tempTable,queryString=getGroupsSQL,pageSize=recordsPerPage),true)>
			<cfelse>
				<cfset structAppend(result,application.com.dbTools.getRecordsFromCachedQuery(tempTable=tempTable),true)>
			</cfif>
		</cfif>

		<cfcatch>
			<cfset application.com.errorHandler.recordRelayError_ColdFusion(error=cfcatch)>
		</cfcatch>
	</cftry>

	<cfset result.tablename=tempTable>

	<cfreturn result>
</cffunction>

<cffunction name="getDiscussionMessage" access="public">
	<cfargument name="discussionGroupID" required="false" default="">
	<cfargument name="discussionMessageID" required="false" default="">
	<cfargument name="searchString" required="false" default="">
	<cfargument name="pageNum" required="false" default="">
	<cfargument name="recordsPerPage" required="false" default="300">
	<cfargument name="cacheQuery" type="boolean" default="true">

	<cfset var getMessageSQL = "">
	<cfset var tempTable = "####discussionMessages_#replace(replace(session.sessionID,'-','','ALL'),'.','')#">
	<cfset var result = {recordSet=queryNew("discussionGroupID,title,message,personID,pictureURL,firstName,lastName,organisationName,created"),hasMore=false}>

	<cfif isNumeric(arguments.discussionGroupID)>
		<cfprocessingdirective suppresswhitespace="yes">
			<cfsavecontent variable="getMessageSQL">
				<cfoutput>
					SELECT m.discussionMessageID, m.discussionGroupID, m.title, m.message, m.personID, p.pictureURL, p.firstName, p.lastName, o.organisationName, m.created, count(distinct l.personid) as likes, count(distinct c.entityCommentID) as comments
					FROM discussionMessage m
					INNER JOIN person p ON m.personID = p.personID
					LEFT JOIN organisation o ON p.organisationid = o.organisationid
					LEFT JOIN entityLike l ON m.discussionMessageID = l.entityID AND l.entityTypeID = <cf_queryParam value="#application.entityTypeID.discussionMessage#" cfsqltype="CF_SQL_INTEGER">
					LEFT JOIN entityComment c ON m.discussionMessageID = c.entityID AND c.active = 1
					<cfif trim(arguments.searchString) is not "">INNER JOIN CONTAINSTABLE(vDiscussionMessageSearch,*,<cf_queryparam value='#application.com.search.prepareSearchString(arguments.searchString)#' cfsqltype="CF_SQL_VARCHAR">) as s ON m.discussionMessageID = s.[key]</cfif>
					WHERE m.discussionGroupID = <cf_queryParam value="#arguments.discussionGroupID#" cfsqltype="CF_SQL_INTEGER">
					AND m.active = 1
					<cfif isNumeric(arguments.discussionMessageID)>AND m.discussionMessageID = <cf_queryParam value="#arguments.discussionMessageID#" cfsqltype="CF_SQL_INTEGER"></cfif>
					GROUP BY m.discussionMessageID, m.discussionGroupID, m.title, m.message, m.personID, p.pictureURL, p.firstName, p.lastName, o.organisationName, m.created
					ORDER BY m.created desc
				</cfoutput>
			</cfsavecontent>
		</cfprocessingdirective>

		<cftry>
			<cfif not arguments.cacheQuery>
				<cfquery name="recordSet" datasource="#application.sitedataSource#">
					#preserveSingleQuotes(getMessageSQL)#
				</cfquery>
				<cfset result.recordSet = recordSet>
			<cfelse>
				<cfif arguments.pageNum eq 0 or not isNumeric(arguments.pageNum)>
					<!--- if we're refreshing the feed, then delete the existing cached query and refresh it --->
					<cfset application.com.dbTools.deleteCachedQuery(tempTable=tempTable)>
					<cfset structAppend(result,application.com.dbTools.dbCacheQuery(tempTable=tempTable,queryString=getMessageSQL,pageSize=recordsPerPage),true)>
				<cfelse>
					<cfset structAppend(result,application.com.dbTools.getRecordsFromCachedQuery(tempTable=tempTable),true)>
				</cfif>
			</cfif>

			<cfcatch>
				<cfset application.com.errorHandler.recordRelayError_ColdFusion(error=cfcatch)>
			</cfcatch>
		</cftry>
	</cfif>

	<cfset result.tablename=tempTable>

	<cfreturn result>
</cffunction>

<cffunction name="saveDiscussionMessage" access="remote" returnformat="plain">
	<cfargument name="discussionMessageID" required="false" default="">
	<cfargument name="discussionGroupID" type="numeric" required="true">
	<cfargument name="title" type="string" required="false">
	<cfargument name="message" type="string" required="false">
	<cfset var returnValue = true>
	<cfset var addComma = false>
	<cftry>
		<cfif isNumeric(arguments.discussionMessageID)>
			<cfquery name="qModifyMessage" datasource="#application.siteDataSource#">
				UPDATE discussionMessage
				SET <cfif structKeyExists(arguments,"title")>title = <cf_queryParam value="#arguments.title#" cfsqltype="CF_SQL_VARCHAR"><cfset addComma = true></cfif>
					<cfif structKeyExists(arguments,"message")><cfif addComma>,</cfif>message = <cf_queryParam value="#arguments.message#" cfsqltype="CF_SQL_VARCHAR"></cfif>
				WHERE discussionMessageID = <cf_queryParam value="#arguments.discussionMessageID#" cfsqltype="CF_SQL_INTEGER">
					<cfif not application.com.login.checkInternalPermissions("socialTask", "Level3")>AND personID = <cf_queryParam value="#request.relayCurrentUser.personid#" cfsqltype="CF_SQL_INTEGER"></cfif>
					AND discussionGroupID = <cf_queryParam value="#arguments.discussionGroupID#" cfsqltype="CF_SQL_INTEGER">
			</cfquery>
		<cfelse>
			<cfquery name="qInsertMessage" datasource="#application.siteDataSource#">
				INSERT INTO discussionMessage
					(discussionGroupID, title, message, personID, createdBy)
				VALUES
					(<cf_queryParam value="#arguments.discussionGroupID#" cfsqltype="CF_SQL_INTEGER">,
					<cf_queryParam value="#arguments.title#" cfsqltype="CF_SQL_VARCHAR">,
					<cf_queryParam value="#arguments.message#" cfsqltype="CF_SQL_VARCHAR">,
					<cf_queryParam value="#request.relayCurrentUser.personid#" cfsqltype="CF_SQL_INTEGER">,
					<cf_queryParam value="#request.relayCurrentUser.personid#" cfsqltype="CF_SQL_INTEGER">)
			</cfquery>
		</cfif>
		<cfcatch><cfset var returnValue = "#cfcatch.message# #cfcatch.detail#"></cfcatch>
	</cftry>
	<cfreturn returnValue>
</cffunction>

<cffunction name="deleteDiscussionMessage" access="remote">
	<cfargument name="discussionMessageID" type="numeric" required="false" default="">
	<cfset var returnValue = true>
	<cftry>
		<cfquery name="qDeleteMessage" datasource="#application.siteDataSource#">
			UPDATE discussionMessage
			SET active = 0,
				lastUpdatedBy = <cf_queryParam value="#request.relayCurrentUser.personid#" cfsqltype="CF_SQL_INTEGER">
			WHERE discussionMessageID = <cf_queryParam value="#arguments.discussionMessageID#" cfsqltype="CF_SQL_INTEGER">
				<cfif not application.com.login.checkInternalPermissions("socialTask", "Level3")>AND createdBy = <cf_queryParam value="#request.relayCurrentUser.personid#" cfsqltype="CF_SQL_INTEGER"></cfif>
		</cfquery>
		<cfcatch>
			<cfset var returnValue = false>
		</cfcatch>
	</cftry>
	<cfreturn returnValue>
</cffunction>

<cffunction name="getHTMLForDiscussionGroups" access="remote">
	<cfargument name="discussionGroupIDs" required="false" default="">
	<cfargument name="searchString" required="false" default="">
	<cfargument name="pageNum" required="false" default="">
	<cfargument name="recordsPerPage" required="false" default="">
	<cfargument name="eid" required="false" default="">

	<cfset var discussionGroupsHTML = "">
	<cfset var qGroups = "">
	<cfset var mergeStruct = "">
	<cfset var infoMessage = "">
	<cfset var icon = "">

	<cfinvoke component="webservices.discussions" method="getDiscussionGroup" returnVariable="qGroups" recordsPerPage="#arguments.recordsPerPage#" pageNum="#arguments.pageNum#" discussionGroupIDs="#arguments.discussionGroupIDs#" searchString="#arguments.searchString#"></cfinvoke>
	<cfsavecontent variable="discussionGroupsHTML">
		<cfif trim(arguments.searchString) is not "">
			<cfset mergeStruct = {recordCount="<strong>#qGroups.recordCount#</strong>",searchString="<strong>#htmlEditFormat(arguments.searchString)#</strong>"}>
			<cfset infoMessage = application.com.relayTranslations.translatePhrase(phrase="phr_discussion_search_message",mergeStruct=mergeStruct)>
			<cfoutput>#application.com.relayui.message('#infoMessage# <a href="#request.currentSite.protocolAndDomain#/?eid=#htmleditformat(arguments.eid)#&searchString=">phr_discussion_search_clear</a>', 'info', false)#</cfoutput>
		</cfif>
		<cfloop query="qGroups.recordset">
			<cfset messageLink="#request.currentSite.protocolAndDomain#/?eid=#htmleditformat(arguments.eid)#&discussionGroupID=#htmleditformat(discussionGroupID)#">
			<!--- 2014-08-26	RPW	Discussion group image not displaying on portal --->
			<cfscript>
				icon = "/images/social/icons/group_default.jpg";
				var tmpDirectory = application.userfilesabsolutepath & "/content/linkImages/discussionGroup/" & discussionGroupID;
				if (DirectoryExists(tmpDirectory)) {
					var directoryList = DirectoryList(
						tmpDirectory,false,"name","icon_#discussionGroupID#.*"
					);
					if (ArrayLen(directoryList)) {
						icon = "/content/linkImages/discussionGroup/" & discussionGroupID & "/" & directoryList[1];
					}
				}

			</cfscript>
			<cfoutput>
				<div class="message">

					<div class="body expandable lessWidth">
						<div class="photo">
							<a href="#messageLink#">
								<img src="#icon#" width="80" height="80">
							</a>
						</div>
						<h4><a href="#messageLink#">#htmlEditFormat(title)#</a></h4>
						<p>#htmlEditFormat(description)#</p>
					</div>

					<div class="arrow visible-xs">
						<a href="#messageLink#"><div class="glyphicon glyphicon-chevron-right"></div></a>
					</div>

					<div class="links hidden-xs">
						<a href="#messageLink#">phr_discussion_view</a>
					</div>
				</div>
			</cfoutput>
		</cfloop>
		<cfif qGroups.hasMore>
			<Cfset arguments.pageNum = arguments.pageNum + 1>
			<cfoutput>
				<div class="showMore">
					<div class="body">
						<a id="showMoreGroups" class="noembedly" href="#request.currentSite.protocolAndDomain#/webservices/callWebService.cfc?method=callWebService&webServiceName=discussions&methodName=getHTMLForDiscussionGroups&more=true&searchString=#htmlEditFormat(arguments.searchString)#&pageNum=#htmleditformat(arguments.pageNum)#&recordsPerPage=#htmleditformat(arguments.recordsPerPage)#&eid=#htmleditformat(arguments.eid)#&discussionGroupIDs=#htmleditformat(arguments.discussionGroupIDs)#&returnFormat=plain&_cf_nodebug=true">phr_showMore</a>
					</div>
				</div>
				<script>
					initialiseJscroll("##discussionGroups","a##showMoreGroups");
				</script>
			</cfoutput>
		</cfif>
	</cfsavecontent>

	<cfreturn application.com.request.postprocessContentString(discussionGroupsHTML)>
</cffunction>

<cffunction name="getHTMLForDiscussionMessages" access="remote">
	<cfargument name="discussionMessageID" required="false" default="">
	<cfargument name="discussionGroupID" required="false" default="">
	<cfargument name="searchString" required="false" default="">
	<cfargument name="pageNum" required="false" default="">
	<cfargument name="recordsPerPage" required="false" default="">
	<cfargument name="eid" required="false" default="">
	<cfargument name="showVote" required="false" default="false" type="boolean">


	<cfset var discussionMessagesHTML = "">
	<cfset var qGetMessage = "">
	<cfset var mergeStruct = "">
	<cfset var infoMessage = "">
	<cfset var icon = "">
	<cfset var addMessageOnPageEdit = false>
	<cfset var cols = 1>
	<cfset var stLikes = "">

	<cfinvoke component="webservices.discussions" method="getDiscussionMessage" returnVariable="qGetMessage" recordsPerPage="#arguments.recordsPerPage#" pageNum="#arguments.pageNum#" discussionGroupID="#arguments.discussionGroupID#" discussionMessageID="#arguments.discussionMessageID#" searchString="#arguments.searchString#"></cfinvoke>
	<cfinvoke component="com.entityFunctions" method="getEntityLike" returnVariable="stLikes" entityTypeID="#application.entityTypeID.discussionMessage#"></cfinvoke>

	<cfsavecontent variable="discussionMessagesHTML">
		<cfif trim(arguments.searchString) is not "" and not isNumeric(arguments.discussionMessageID)>
			<cfset mergeStruct = {recordCount="<strong>#qGetMessage.recordCount#</strong>",searchString="<strong>#htmlEditFormat(arguments.searchString)#</strong>"}>
			<cfset infoMessage = application.com.relayTranslations.translatePhrase(phrase="phr_discussion_search_message",mergeStruct=mergeStruct)>
			<cfoutput>#application.com.relayui.message('#infoMessage# <a href="#request.currentSite.protocolAndDomain#/?eid=#htmleditformat(arguments.eid)#&discussionGroupID=#htmleditformat(arguments.discussionGroupID)#&discussionMessageID=#htmleditformat(arguments.discussionMessageID)#&searchString=">phr_discussion_search_clear</a>', 'info', false)#</cfoutput>
		</cfif>
		<cfif not qGetMessage.recordCount>
			<cfoutput>#application.com.relayui.message('phr_discussion_add_first_message', 'info', true)#</cfoutput>
		</cfif>
		<cfloop query="qGetMessage.recordset">
			<cfset commentLink="#request.currentSite.protocolAndDomain#/?eid=#htmleditformat(arguments.eid)#&discussionGroupID=#htmleditformat(arguments.discussionGroupID)#&discussionMessageID=#qGetMessage.recordset.discussionMessageID#">
			<cfoutput>
				<div class="message" data-id="#qGetMessage.recordset.discussionMessageID#">
					<div class="body<cfif not isNumeric(arguments.discussionMessageID)> lessWidth</cfif>">
					<!--- 2015-02-27	ACPK	FIFTEEN-234	Updated layout of discussion messages --->
					<div class="hidden-xs pull-right">#application.com.social.getFormattedDate(created)#</div>
						<div class="photo">
							<img src="<cfif pictureURL is not "">#pictureURL#<cfelse>/images/social/icon_no_photo_80x80.png</cfif>" width="80" height="80" onerror="this.onerror=null;this.src='/images/social/icon_no_photo_80x80.png'">
						</div>
						<div class="nameandcompany">
							<div class="name">#htmlEditFormat(firstName)# #htmlEditFormat(lastName)#</div>
							<div class="company">#htmlEditFormat(organisationName)#</div>
						</div>
						<h4><a href="#request.currentSite.protocolAndDomain#/?eid=#htmleditformat(arguments.eid)#&discussionGroupID=#htmleditformat(arguments.discussionGroupID)#&discussionMessageID=#qGetMessage.recordset.discussionMessageID#">#htmlEditFormat(title)#</a></h4>
						<div class="editable expandable"><div class="expanding"><pre>#htmlEditFormat(message)#</pre></div></div>
					</div>
					<cfif not isNumeric(arguments.discussionMessageID)>
						<div class="arrow visible-xs">
							<a href="#request.currentSite.protocolAndDomain#/?eid=#htmleditformat(arguments.eid)#&discussionGroupID=#htmleditformat(arguments.discussionGroupID)#&discussionMessageID=#qGetMessage.recordset.discussionMessageID#"><div class="glyphicon glyphicon-chevron-right"></div></a>
						</div>
					</cfif>
					<!--- 2015-02-27	ACPK	FIFTEEN-234	Updated layout of discussion messages --->
					<div class="links col-xs-12 col-sm-12 col-md-8 col-lg-6 pull-right">
				<!--- 	<cfset cols = 1>
						<cfif not isNumeric(arguments.discussionMessageID)><cfset cols = cols+1></cfif>
						<cfif personID is request.relayCurrentUser.personID or isDiscussionAdmin()><cfset cols = cols+2></cfif>
						<cfset cols = 12/cols> --->
						<div class="row">
							<ul>
								<li class="col-xs-12 col-sm-3"><a href="" onclick="return saveLike(#application.entityTypeID.discussionMessage#,#qGetMessage.recordset.discussionMessageID#,this,'span.likes')"><cfif structKeyExists(stLikes,qGetMessage.recordset.discussionMessageID)>phr_social_Unlike<cfelse>phr_social_Like</cfif></a><span class="likes">#likes#</span><img src="/images/social/icons/like<cfif not structKeyExists(stLikes,qGetMessage.recordset.discussionMessageID)>_disabled</cfif>.png" width="15" height="18"></li>
								<li class="col-xs-12 col-sm-5">
									<cfif not isNumeric(arguments.discussionMessageID)>
										<a href="#commentLink###commentForm">phr_social_comment</a><a href="#commentLink###comments"><span class="comments">#comments#</span><img src="/images/social/icons/comment_blue.png" width="15" height="14"></a>
									<cfelse>
										phr_social_comment<span class="comments">#comments#</span><img src="/images/social/icons/comment_blue.png" width="15" height="14">
									</cfif>
								</li>
								<cfif personID is request.relayCurrentUser.personID or isDiscussionAdmin()>
									<li class="col-xs-12 col-sm-2"><a href="" class="editMessagelink">phr_sys_edit</a></li>
									<li class="col-xs-12 col-sm-2"><a href="" onclick="return deleteMessage(#qGetMessage.recordset.discussionMessageID#)">phr_sys_delete</a><cfset addMessageOnPageEdit=true></li>
								</cfif>
								<div class="visible-xs col-xs-12"><li>#application.com.social.getFormattedDate(created)#</li></div>
							</ul>
						</div>
					</div>
				</div>
			</cfoutput>
		</cfloop>
		<cfif qGetMessage.hasMore>
			<Cfset arguments.pageNum = arguments.pageNum + 1>
			<cfoutput>
				<div class="showMore">
					<div class="body">
						<a id="showMoreMessages" class="noembedly" href="#request.currentSite.protocolAndDomain#/webservices/callWebService.cfc?method=callWebService&webServiceName=discussions&methodName=getHTMLForDiscussionMessages&more=true&searchString=#htmlEditFormat(arguments.searchString)#&pageNum=#htmleditformat(arguments.pageNum)#&recordsPerPage=#htmleditformat(arguments.recordsPerPage)#&eid=#htmleditformat(arguments.eid)#&discussionGroupID=#htmleditformat(arguments.discussionGroupID)#&discussionMessageID=#htmleditformat(arguments.discussionMessageID)#&showVote=#arguments.showVote#&returnFormat=plain&_cf_nodebug=true">phr_showMore</a>
					</div>
				</div>
				<script>
					initialiseJscroll("##discussionMessages","a##showMoreMessages");
				</script>
			</cfoutput>
		</cfif>
		<cfif addMessageOnPageEdit>
			<cfoutput>
				<script language="javascript" type="text/javascript">
				jQuery(document).ready(function (){
					addMessageEditLink(jQuery('##discussions'));
				});

			 	function addMessageEditLink(scope) {

					jQuery(scope).find('a.editMessagelink').click(function(){
						var thisDiv = jQuery(this).parents('div.message');
						var thisTitle = thisDiv.find('div.body h3 a').html();
						var thisMessage = thisDiv.find('div.editable div.details pre').html();
						if(thisMessage == undefined){
						var thisMessage = thisDiv.find('div.editable pre').html();
						}
						thisDiv.find('div.body h3 a').hide();
						thisDiv.find('div.body h3').append('<input type="text" name="title" data-title="' + thisTitle + '" class="inherit form-control" value="' + thisTitle + '" />').click(function(e){e.stopPropagation();});
						thisDiv.find('div.body div.editable pre').html('<textarea class="inherit form-control" data-message="' + thisMessage + '" name="message">'+ thisMessage + '</textarea>').find('textarea').click(function(e){e.stopPropagation();}).autosize();
						jQuery(this).off().on("click",function(){saveDiscussionMessage(this); return false}).html('Save Changes');
						return false;
					});
					jQuery('##discussions textarea').autosize({append: "\n"});
					jQuery('body').off().on("click",function(){removeAllForms()});
			 	}

			 	function saveDiscussionMessage(e){
					var thisMessage = jQuery(e).parents('.message');
					var title = thisMessage.find('input:text').val();
					var message = thisMessage.find('textarea:visible').val();
					var discussionMessageID = thisMessage.attr('data-id');
					jQuery.ajax({
						type: "POST",
						url: "/WebServices/discussions.cfc?method=saveDiscussionMessage&_cf_nodebug=true",
						data: {title: title, message: message, discussionMessageID: discussionMessageID, discussionGroupID: #arguments.discussionGroupID#},
						dataType: "text",
						error: function(){alert('Error saving changes.')},
						success: function(){removeMessageForm(thisMessage,title,message)},
						cache: false
					});
					return false
				}

				function removeMessageForm(e,t,m){
					jQuery(e).find('div.body h3 a').html(t).show();
					jQuery(e).find('div.body h3 input').remove();
					jQuery(e).find('div.body div.editable div.expanding').html('<div><pre>' + m + '</pre></div>');	// 2013-05-17	YMA	CASE: 434412 It has been necessary to create an additional div as a control for the more / less functionality after an edit has been made.
					jQuery(e).find('a.editMessagelink').off().html('Edit');
					addMessageEditLink(e);

					return setExpander(jQuery(e).find('div.body div.editable div.expanding div'));
				}

				function removeAllForms(){
					jQuery.each(jQuery('##discussions textarea').not(':last'),function(){
						removeMessageForm(jQuery(this).parents('.message'),jQuery(this).parents('.message').find('input').attr('data-title'),jQuery(this).attr('data-message'));
					});
				}
				</script>
			</cfoutput>
		</cfif>
	</cfsavecontent>

	<cfreturn application.com.request.postprocessContentString(discussionMessagesHTML)>
</cffunction>

<cffunction name="getHTMLForDiscussionComments" access="remote">
	<cfargument name="discussionMessageID" required="false" default="">
	<cfargument name="discussionGroupID" required="false" default="">
	<cfargument name="searchString" required="false" default="">
	<cfargument name="pageNum" required="false" default="">
	<cfargument name="recordsPerPage" required="false" default="">
	<cfargument name="eid" required="false" default="">

	<cfset var discussionCommentsHTML = "">
	<cfset var qGetComments = "">
	<cfset var mergeStruct = "">
	<cfset var infoMessage = "">
	<cfset var icon = "">
	<cfset var addCommentOnPageEdit = false>
	<cfset var cols = 1>

	<cfinvoke component="com.entityFunctions" method="getEntityComment" returnVariable="qGetComments" entityID="#arguments.discussionMessageID#" entityTypeID="#application.entityTypeID.discussionMessage#" searchString="#arguments.searchString#" pageNum="#arguments.pageNum#" recordsPerPage="#arguments.recordsPerPage#" cacheQuery="true"></cfinvoke>
	<cfinvoke component="com.entityFunctions" method="getEntityLike" returnVariable="stLikes" entityTypeID="#application.entityTypeID.entityComment#"></cfinvoke>

	<cfsavecontent variable="discussionCommentsHTML">
		<cfoutput><a style="display:none;" name="comments"></a></cfoutput>
		<cfif trim(arguments.searchString) is not "">
			<cfset mergeStruct = {recordCount="<strong>#qGetComments.recordCount#</strong>",searchString="<strong>#htmlEditFormat(arguments.searchString)#</strong>"}>
			<cfset infoMessage = application.com.relayTranslations.translatePhrase(phrase="phr_discussion_search_message",mergeStruct=mergeStruct)>
			<cfoutput>#application.com.relayui.message('#infoMessage# <a href="#request.currentSite.protocolAndDomain#/?eid=#htmleditformat(arguments.eid)#&discussionGroupID=#htmleditformat(arguments.discussionGroupID)#&discussionMessageID=#htmleditformat(arguments.discussionMessageID)#&searchString=">phr_discussion_search_clear</a>', 'info', false)#</cfoutput>
		</cfif>
		<cfloop query="qGetComments.recordSet">
			<cfoutput>
			<div class="comment" data-discussionMessageID="#arguments.discussionMessageID#" data-entityCommentID="#entityCommentID#">
				<div class="photo">
					<img src="<cfif pictureURL is not "">#pictureURL#<cfelse>/images/social/icon_no_photo_80x80.png</cfif>" width="80" height="80" onerror="this.onerror=null;this.src='/images/social/icon_no_photo_80x80.png'">
				</div>
				<div class="nameandcompany"><div class="name">#htmlEditFormat(firstName)# #htmlEditFormat(lastName)#</div><div class="company">#htmlEditFormat(organisationName)#</div></div>
				<div class="body">
					<div class="editable expandable"><div class="expanding"><pre>#htmlEditFormat(comment)#</pre></div></div>
				</div>

				<div class="links col-xs-12 col-sm-10 col-md-6 col-lg-6 text-right pull-right">
					<cfset cols = 1>
					<cfif personID is request.relayCurrentUser.personID or isDiscussionAdmin()><cfset cols = cols+2></cfif>
					<cfset cols = 12/cols>
					<div class="top row">
						<ul>
							<li class="col-xs-#cols#"><a href="" onclick="return saveLike(#application.entityTypeID.entityComment#,#entityCommentID#,this,'span.likes')" title="<cfif structKeyExists(stLikes,entityCommentID)>phr_social_Unlike<cfelse>phr_social_Like</cfif>"><cfif structKeyExists(stLikes,entityCommentID)>phr_social_Unlike<cfelse>phr_social_Like</cfif></a></li>
							<cfif personID is request.relayCurrentUser.personID or isDiscussionAdmin()>
								<li class="col-xs-#cols#"><a href="" class="editCommentlink">phr_sys_edit</a></li>
								<li class="col-xs-#cols#"><a href="" onclick="return deleteComment(#entityCommentID#)">phr_sys_delete</a><cfset addCommentOnPageEdit=true></li>
							</cfif>
						</ul>
					</div>
					<div class="bottom row">
						<ul>
							<li class="col-xs-#cols#"><span class="likes">#likes#</span><img src="/images/social/icons/like<cfif not structKeyExists(stLikes,entityCommentID)>_disabled</cfif>.png" width="15" height="18"></li>
							<li class="col-xs-#cols+cols# text-right">#application.com.social.getFormattedDate(created)#</li>
						</ul>
					</div>
				</div>

			</div>
			</cfoutput>
		</cfloop>
		<cfif qGetComments.hasMore>
			<Cfset arguments.pageNum = arguments.pageNum + 1>
			<cfoutput>
				<div class="showMore">
					<div class="body">
						<a id="showMoreComments" class="noembedly" href="#request.currentSite.protocolAndDomain#/webservices/callWebService.cfc?method=callWebService&webServiceName=discussions&methodName=getHTMLForDiscussionComments&more=true&searchString=#htmlEditFormat(arguments.searchString)#&pageNum=#htmleditformat(arguments.pageNum)#&recordsPerPage=#htmleditformat(arguments.recordsPerPage)#&eid=#htmleditformat(arguments.eid)#&discussionMessageID=#htmleditformat(arguments.discussionMessageID)#&returnFormat=plain&_cf_nodebug=true">phr_showMore</a>
					</div>
				</div>
				<script>
					initialiseJscroll("##discussionComments","a##showMoreComments");
				</script>
			</cfoutput>
		</cfif>
		<cfif addCommentOnPageEdit>
			<cfoutput>
				<script language="javascript" type="text/javascript">
				jQuery(document).ready(function (){
					addCommentEditLink(jQuery('##discussions'));
				});

			 	function addCommentEditLink(scope) {
					jQuery(scope).find('a.editCommentlink').click(function(){
						var thisDiv = jQuery(this).parents('div.comment');
						var thisComment = thisDiv.find('div.editable div.details pre').html();
						if(thisComment == undefined){
						var thisComment = thisDiv.find('div.editable pre').html();
						}
						thisDiv.find('div.body div.editable pre').html('<textarea class="inherit form-control" name="comment" data-comment="' + thisComment + '">' + thisComment + '</textarea>').find('textarea').click(function(e){e.stopPropagation();}).autosize();
						jQuery(this).off().on("click",function(){saveDiscussionComment(this); return false}).html('Save Changes');
						return false;
					});
					jQuery('##discussions textarea').autosize({append: "\n"});
					jQuery('body').off().on("click",function(){removeAllForms()});

			 	}

			 	function saveDiscussionComment(e){
					var thisMessage = jQuery(e).parents('.comment');
					var comment = thisMessage.find('textarea:visible').val();
					var discussionMessageID = thisMessage.attr('data-discussionMessageID');
					var entityCommentID = thisMessage.attr('data-entityCommentID');
					jQuery.ajax({
						type: "POST",
						url: "/WebServices/socialWS.cfc?method=saveEntityComment&_cf_nodebug=false&returnFormat=json",
						data: {comment: comment, entityCommentID: entityCommentID, entityTypeID: #application.entityTypeID.discussionMessage#, entityID: discussionMessageID},
						dataType: "text",
						error: function(){alert('Error saving changes.')},
						success: function(){removeCommentForm(thisMessage,comment)},
						cache: false
					});
					return false
				}

				function removeCommentForm(e,c){
					jQuery(e).find('div.body div.editable div.expanding').html('<div><pre>' + c + '</pre></div>');	// 2013-05-17	YMA	CASE: 434412 It has been necessary to create an additional div as a control for the more / less functionality after an edit has been made.
					jQuery(e).find('a.editCommentlink').off().html('Edit');
					addCommentEditLink(e);

					return setExpander(jQuery(e).find('div.body div.editable div.expanding div'));
				}

				function removeAllForms(){
					jQuery.each(jQuery('##discussions textarea').not(':last'),function(){
						removeCommentForm(jQuery(this).parents('.comment'),jQuery(this).attr('data-comment'));
					});
				}
				</script>
			</cfoutput>
		</cfif>
	</cfsavecontent>

	<cfreturn application.com.request.postprocessContentString(discussionCommentsHTML)>
</cffunction>

</cfcomponent>