<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
WAB 2011/03/15 created a webservice for deleting related files via Ajax
WAB 2011/10/20 changed calls to this function to use callWebservice.cfc, so removed the call method function
2014-10-20 PPB P-KAS040 added getFileDetailsByEntityID()
--->

<cfcomponent>

	<cffunction name="deleteRelatedFile">
		<cfargument name="FileID">

		<cfset var result = application.com.relatedFile.deleteRelatedFileByFileID(FileID)>
		
		
		<cfreturn result>
	
	</cffunction>
	
	<!--- NJH 2012/11/02 Case 431417 --->
	<cffunction name="getRelatedFileCategory" access="remote" returntype="query">
		<cfargument name="fileCategoryEntity" type="string" required="false">
		
		<cfreturn application.com.relatedFile.getRelatedFileCategory(argumentCollection=arguments)>
	</cffunction>

	<!--- 2014-10-20 PPB P-KAS040 added function --->
	<cffunction name="getFileDetailsByEntityID" access="remote" returntype="struct">
		<cfargument name="entityType" type="string" required="true">
		<cfargument name="entityId" type="numeric" required="true">
		<cfargument name="fileCategory" type="string" required="false" default="">
		
		<cfset var result = StructNew()>

		<cfset fileList = application.com.relatedFile.getFileDetailsByEntityID (entityID = arguments.entityId,entityType = arguments.entityType,fileCategory = arguments.fileCategory,getrights = true)>
		
		<cfset result.isOK = true>
		<cfset result.query = fileList>
		
		<cfreturn result>
	</cffunction>

</cfcomponent>
