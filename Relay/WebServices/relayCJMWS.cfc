<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			relayCJMWS.cfc
Author:				ACPK & YMA
Date started:		2014-12-01

Description:		web service calls for CJM.

Date (DD-MMM-YYYY)	Initials 	What was changed
Amendment History:
2015-10-11          ACPK        PROD2015-24 Updated saveCustomizedTemplate to include new parameters;
                                PROD2015-24 Updated exportTemplate to include fileName parameter and delete exportedFilePath from response;
								PROD2015-24 Display custom labels/descriptions/email links for customized templates in table
2015-11-19          ACPK        PROD2015-417 Change Customize Template, Open, and Email links to buttons
2015-12-01          ACPK        PROD2915-467 Wrap title/description of templates with HTMLEditFormat
2016-08-01          DAN         Case 450985 - add option to hide email button on the customized templates tab

Date (DD-MMM-YYYY)  Initials    What was changed
Possible enhancements:

 --->

<cfcomponent>

	<cffunction name="saveTemplateAttributes" access="remote">
		<cfargument name="CJMTemplateID" required="true">
		<cfargument name="data" required="true">

		<cfreturn application.com.cjm.saveTemplateAttributes(argumentCollection=arguments)>
	</cffunction>

	<cffunction name="loadTemplateAttributes" access="remote">
		<cfargument name="CJMTemplateID" required="true">

		<cfreturn application.com.cjm.loadTemplateAttributes(argumentCollection=arguments)>
	</cffunction>

	<cffunction name="convertTemplate" access="remote">
		<cfargument name="CJMTemplateID" required="true">

		<cfset var targetFilePath = "#application.paths.content#\CJMTemplate\#arguments.CJMTemplateID#">

		<cfset var cffile = application.com.fileManager.uploadFile(fileField="form.selectTemplateFile",destination=targetFilePath,nameConflict="Overwrite",accept=accept)>

		<cfreturn cffile>
	</cffunction>

	<cffunction name="loadNewTemplateDesigner" access="remote">
		<cfargument name="CJMTemplateID" required="true">

		<cfset var newTemplateDesigner = "">

		<cfsavecontent variable="newTemplateDesigner">
			<cfoutput>
				<cfinclude template="/cobranding/templateDesigner.cfm">
			</cfoutput>
		</cfsavecontent>

		<cfreturn newTemplateDesigner>
	</cffunction>

	<cffunction name="deleteTemplate" access="remote">
		<cfargument name="CJMTemplateID" required="true">

		<cfreturn application.com.cjm.deleteCJMTemplate(argumentCollection = arguments)>

	</cffunction>

	<cffunction name="getAvailableTemplates" access="public">
		<cfargument name="showDescription" default="true">
		<cfargument name="browserSupported" default="true">
		<cfargument name="deviceSupported" default="true">
		<cfargument name="templateTypeIDs" default="">
		<cfargument name="sortOrder" default="lastUpdated">

		<cfset var availableTemplates = "">
		<cfset var availableTemplatesQry = application.com.cjm.getCJMTemplate(templateTypeIDs=arguments.templateTypeIDs)>
		<cfset var encryptedCJMTemplateID = "">

		<cfsavecontent variable="availableTemplates">
			<cfoutput>
				<table id="AvailableTemplates" data-role="table" class="table table-striped table-hover">
					<thead>
						<th>phr_cjm_templates_Thumbnail</th>
						<th>phr_cjm_templates_label</th>
						<cfif arguments.showDescription>
							<th>phr_cjm_templates_desc</th>
						</cfif>
						<th>phr_cjm_templates_customize</th>
					</thead>
					<tbody>
						<cfquery dbtype="query" name="availableTemplatesSorted">
							select * from availableTemplatesQry
							order by #arguments.sortOrder# DESC
						</cfquery>
						<cfloop query="#availableTemplatesSorted#">
							<tr>
								<td><img src="#CJMTEMPLATETHUMB#" /></td>
								<td>#HTMLEditFormat(cjmTitle)#</td>
								<cfif arguments.showDescription>
									<td>#HTMLEditFormat(cjmDescription)#</td>
								</cfif>
								<cfset encryptedCJMTemplateID = application.com.security.encryptVariableValue("cjmTemplateID",cjmTemplateID)>
	                            <td>
	                                <cfif browserSupported AND deviceSupported>
		                                <!--- 2015-11-19  ACPK    PROD2015-417 Change Customize Template link to button --->
	                                	<a href="javascript:loadEditor({cjmTemplateID:'#encryptedCJMTemplateID#'});" class="btn btn-primary">phr_cjm_templates_customizeTemplate</a>
	                                <cfelseif NOT deviceSupported>
	                                	phr_cjm_devicenotsupported
	                                <cfelse>
	                              		phr_cjm_browsernotsupported
	                                </cfif>
	                            </td>
							</tr>
						</cfloop>
					</tbody>
				</table>
			</cfoutput>
		</cfsavecontent>

		<cfreturn availableTemplates>

	</cffunction>

	<cffunction name="saveCustomizedTemplate" access="remote">
		<cfargument name="CJMTemplateID" required="true">
		<cfargument name="data" required="true">
		<cfargument name="CJMCustomizedTemplateID" required="false" default="0">
		<cfargument name="canvasBody" required="true"> <!--- 2015-11-10   ACPK    PROD2015-24 Added new parameters --->
		<cfargument name="customTitle" required="true">
		<cfargument name="customDescription" required="false">

		<cfreturn application.com.cjm.saveCustomizedTemplate(argumentCollection=arguments)>
	</cffunction>

	<cffunction name="exportTemplate" access="remote">
		<cfargument name="canvasObj" required="true">
		<cfargument name="exportType" required="true" default="pdf">
		<cfargument name="fileName" required="true"> <!--- 2015-11-10    ACPK    PROD2015-24 Added new parameter --->

		<cfset response = application.com.cjm.exportTemplate(argumentCollection=arguments)>
		<cfset StructDelete(response,'exportedFilePath')> <!--- 2015-11-10    ACPK    PROD2015-24 Delete (unneeded) filepath from response to prevent exposing private data --->
		<cfreturn response>
	</cffunction>

	<cffunction name="getCustomizedTemplates" access="public">
		<cfargument name="personID" required="false" default="#request.relaycurrentuser.personID#">
		<cfargument name="showDescription" default="true">
		<cfargument name="browserSupported" default="true">
		<cfargument name="deviceSupported" default="true">
		<cfargument name="templateTypeIDs" default="">
        <cfargument name="showEmailButton" default="true">

		<cfset var customizedTemplates = "">
		<cfset var customizedTemplatesQry = application.com.cjm.getCustomizedTemplate(argumentCollection = arguments)>
		<cfset var encryptedcustomizedTemplateID = "">

		<cfsavecontent variable="customizedTemplates">
			<cfoutput>
				<table id="CustomizedTemplates" data-role="table" class="table table-striped table-hover">
					<thead>
						<th>phr_cjm_templates_Thumbnail</th>
						<th>phr_cjm_templates_label</th>
						<cfif arguments.showDescription>
							<th>phr_cjm_templates_desc</th>
						</cfif>
						<th>phr_cjm_templates_created</th>
						<th>phr_cjm_templates_customize</th>
					</thead>
					<tbody>
						<cfquery dbtype="query" name="customizedTemplatesSorted">
							select * from customizedTemplatesQry
							order by cjmCTCreated DESC
						</cfquery>
						<cfloop query="#customizedTemplatesSorted#">
							<tr>
								<td><img src="#CJMTEMPLATETHUMB#" /></td>
								<td>#HTMLEditFormat(customTitle)#</td> <!--- 2015-11-10    ACPK    PROD2015-24 Show label for customized template --->
								<cfif arguments.showDescription>
									<td>#HTMLEditFormat(customDescription)#</td> <!--- 2015-11-10    ACPK    PROD2015-24 Show description for customized template --->
								</cfif>
								<td>#application.com.dateFunctions.dateTimeFormat(date=cjmCTCreated,showAsLocalTime=true)#</td>
								<cfset encryptedcustomizedTemplateID = application.com.security.encryptVariableValue("cjmCustomizedTemplateID",cjmCustomizedTemplateID)>
								<cfset encryptedCJMTemplateID = application.com.security.encryptVariableValue("cjmTemplateID",cjmTemplateID)>
                                <td>
	                                <cfif browserSupported AND deviceSupported>
		                                <!--- 2015-11-19  ACPK    PROD2015-417 Change Open and Email links to buttons --->
		                                <a href="javascript:loadEditor({cjmTemplateID:'#encryptedCJMTemplateID#',cjmCustomizedTemplateID:'#encryptedcustomizedTemplateID#'});" class="btn btn-primary">phr_cjm_templates_open</a>
                                        <cfif showEmailButton>
                                            <a href="javascript:loadEmailForm('#encryptedcustomizedTemplateID#');" class="btn btn-primary">phr_cjm_templates_email</a> <!--- 2015-11-10    ACPK    PROD2015-24 Show Email link for customized template --->
                                        </cfif>
	                                <cfelseif NOT deviceSupported>
	                                	phr_cjm_devicenotsupported
	                                <cfelse>
	                              		phr_cjm_browsernotsupported
	                                </cfif>
                                 </td>

							</tr>
						</cfloop>
					</tbody>
				</table>
			</cfoutput>
		</cfsavecontent>

		<cfreturn customizedTemplates>

	</cffunction>

	<cffunction name="uploadImage" access="public">
		<cfargument name="cjmTemplateID" required="Yes">

		<cfset var result = {isOK=true,message="File uploaded successfully.",file=""}>
		<cfset var relativeDirectory = "\content\linkImages\CJMTemplate\#arguments.cjmTemplateID#\customizedTemplates\#request.relaycurrentuser.personID#">
		<cfset var destination = "#application.paths.userFiles##relativeDirectory#\">

		<cftry>
			<!--- upload file --->
			<cfset var file = application.com.filemanager.uploadFile(fileField="uploadImage",destination=destination)>
			<!--- convert file to png --->
			<cfset application.com.relayimage.convertImage(sourcePath=file.SERVERDIRECTORY,sourceName=file.SERVERFILE,targetname="#file.SERVERFILENAME#.png")>
			<!--- resize file --->
			<cfset application.com.relayimage.resizeImage(targetWidth=form.targetWidth,targetHeight=form.targetHeight,sourcePath=file.serverDirectory,sourceName="#file.SERVERFILENAME#.png")>
			<!--- set result --->
			<cfset result.file = {serverDirectory=file.serverDirectory,filename="#file.SERVERFILENAME#.png",relativeDirectory=relativeDirectory}>

			<cfcatch>
				<cfset result.isOk = false>
				<cfset result.message = cfcatch.message>
			</cfcatch>
		</cftry>

		<cfoutput>
			<script>
				window.parent.recieveUpload(#SerializeJSON(result)#);
			</script>
		</cfoutput>

		<cf_abort>

	</cffunction>

</cfcomponent>