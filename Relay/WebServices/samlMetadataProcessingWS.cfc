/**
 * samlMetadataProcessingService
 * 
 * author Richard.Tingle
 * date 15/01/16
 * 
 * Because cross browser support for xml parsing seems really finiky (I'm looking at you IE) a web service is provided to do this instead
 * 
 **/
component accessors=true output=false persistent=false {


	remote string function processSPMetadata(required string xmlString) output="true" {

		var returnStruct={isOk="true", message="", assertionConsumerURL="", entityID="", entityName="" };
		try{
			var xml=xmlParse(xmlString);
		}catch (any e){
			returnStruct.isOk=false;
			returnStruct.message="Invalid XML";
		}
		
		if (returnStruct.isOk){
			
			var entityDescriptorNode = xmlSearch(xml, "//*[ local-name() = 'EntityDescriptor' ]");
			
			if(arrayLen(entityDescriptorNode) NEQ 1){
				returnStruct.isOk=false;
				returnStruct.message="XML did not contain exactly 1 EntityDescriptor Node";
			}else{
				var nodeAttributes=entityDescriptorNode[1].XmlAttributes;
				if (structKeyExists(nodeAttributes,"entityID")){
					returnStruct.entityID=nodeAttributes.entityID;
					returnStruct.entityName=nodeAttributes.entityID;
				}else{
					returnStruct.isOk=false;
					returnStruct.message="EntityDescriptor Node did not have an entityID attribute";
				}
			}
		}
		
		if (returnStruct.isOk){
			
			 //there may be several AssertionConsumerService tags, we take the first that has an actual URL
			var assertionConsumerURL = "";

			var assertionConsumerServices = xmlSearch(xml, "//*[ local-name() = 'AssertionConsumerService' ]");
						
			for (var i = 1; i <= arrayLen(assertionConsumerServices); i++) {
                var sevice = assertionConsumerServices[i];
                var nodeAttributes=sevice.XmlAttributes;

                if (structKeyExists(nodeAttributes,"Location")){
                	returnStruct.assertionConsumerURL=nodeAttributes.Location;
                }

            }
			
		}
		

		return SerializeJSON(returnStruct);
	}
	
	remote string function processIDPMetadata(required string xmlString_encoded) output="false" {

		//string was url encoded during transmission to avoid possibility of corruption (especiallly in certificate)
		var xmlString=xmlString_encoded;

		var returnStruct={isOk="true", message="", identityProviderEntityID="", certificate="", authnRequestURL="" };

		
		
		//we grab a java object to parse the string
		var idpMetadataProcessor = application.javaloader.create("singleSignOn.samlserviceprovider.idps.IDPMetadataProcessor");
		
		var parseResult=idpMetadataProcessor.readIDPMetadata(xmlString);

		returnStruct.isOk=parseResult.isValid();
		if (returnStruct.isOk){
			returnStruct.identityProviderEntityID=parseResult.getEntityID();
			returnStruct.authnRequestURL=parseResult.getSingleSignOnServiceURL_POSTBinding();
		}else{
			returnStruct.message=parseResult.getInvalidReason();
		}

		return SerializeJSON(returnStruct);
	}
	

	
}