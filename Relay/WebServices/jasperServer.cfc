<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent>
	
	<cffunction name="getJasperServerParameter" access="remote">
	
		<cfset var result = {isOK = true}>

		<cfif request.relayCurrentUser.isLoggedIn and request.relayCurrentUser.isInternal>
			<cfset result.Parameter = application.com.jasperServer.getJasperServerLoginParamsForCurrentUser()>
			<cfset result.path = application.com.jasperServer.getjasperserverpath()>
		<cfelse>
			<cfset result.isOK = false>
		</cfif>
	
		<cfreturn result>
	</cffunction>

</cfcomponent>
