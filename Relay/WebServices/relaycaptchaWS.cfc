<!--- �Relayware. All Rights Reserved 2014 --->

<cfcomponent extends="relay.com.mxajax">


<cffunction name="getcaptchavalidation" access="remote">
	<cfargument name="userResponse" required="true">
	<cfargument name="captchaResponse" required="true">
	
	<cfset var validatecaptcha = "">

	<cfset validatecaptcha = application.com.commonQueries.validatecaptcha(userResponse=userResponse, captchaResponse=captchaResponse)>
	
	<cfreturn validatecaptcha>
</cffunction>

<cffunction name="drawCaptcha" access="remote">
	<cfargument name="captchaerror" default="false">
	
	<cfset drawcaptchaHTML = application.com.commonQueries.drawCaptcha(captchaerror=captchaerror)>

		<cfoutput>
		#drawcaptchaHTML#
		</cfoutput>
	
</cffunction>

</cfcomponent>