<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		WebServices\relayOpportunityWS.cfc
Author:			NJH
Date started:	2010/03/03

Description:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2008-11-11			NYB 		changed the column names returned for getSpecialisationItems
2009-10-30			AJC			Quickbids core upgrades
2010-06-14			NAS			P-CLS002 Fields added as switches to turn fields/functionality on/off
28-01-2011			NYB  		P-NET002 - added format products from settings
2012-03-13 			PPB 		AVD001 new setting
2012-03-16 			PPB 		Netgear - added NULLIF(ProductType,0)
2012/05/04			IH			Case 428082 cast result.PRODUCTTYPE to string before calling toLowerCase()
2012/07/18			NJH			Mobile - altered product picker to display product groups as well as products. Can only delete them.
2012/08/22 			NAS 		Case 430196 Deal Reg Form does not ignore Product Groups with InActive products
2013-10-01 			NYB 		Case 437100 - replaced textfield with relaytextfield for frmSpecialPriceReason
2014-11-19			RPW			Function getDistiSalesPerson - allowed blank parameter "theID" to be passed in.
2014-12-05			PPB			P-ABS001 add showSeats
2014-12-08			PPB			P-ABS001 removed a couple of unwanted styles from ProductPicker
2014-09-22			AHL			Case 441630 Inactive Distributor should not be displayed in "Distributor" drop down
2015-02-10			RPW			Leads - Added ability to customize leads form for customers.
2015-06-25          DAN         Case 444803 - fix for null ref in javascript when 'show seats' setting is disabled
2015-11-16          DAN         PROD2015-364 - products country restrictions
2016/01/26			NJH			Removed enablecfoutputonly and set the output property to false on the component.

Possible enhancements:
The NULLIF(ProductType,0) fix done on 2012-03-16 should probably return NULL for any numeric

NB. LSNUMERIC INPUT CHANGES TEMPORARILY REVERTED changes done for system-wide numeric input (against Case 430249) exist in branch RW8.3.1 rev 9208 (they didn't make it to 2012Summer) of this file, I'd recommend comparing with a copy of that file when developing the enhancement

 --->
<cfcomponent output="false">

	<cfif structKeyExists(request,"relayCurrentUser") and structKeyExists(request.relayCurrentUser,"isLoggedIn") and not request.relayCurrentUser.isLoggedIn>
		<cfoutput><script>alert('You must be logged in')</script></cfoutput>
		<CF_ABORT>
	</cfif>

	<cf_include template="\code\cftemplates\opportunityINI.cfm" checkIfExists="true">

	<!--- NJH 2009/07/02 P-FNL069 - check if user has rights to access this opportunity --->
 	<cfif isDefined("opportunityID")>
		<cfif not application.com.rights.doesUserHaveRightsForEntity(entityType="opportunity",entityID=opportunityID,permission="view")>
			<CF_ABORT>
		</cfif>
	</cfif>

	<!--- NJH 2010/03/24 P-PAN002 --->
	<cffunction name="getOppProductsDisplay" access="remote" output="true" returnType="string">
		<cfargument name="opportunityID" type="numeric" required="true">
		<cfargument name="readOnly" type="boolean" default="false">

		<cfset var oppProductsDisplay_html = "">
		<cfset var oppProductPicker_html = "">
		<cfset var oppProductCrossSell_html = "">
		<cfset var oppProductTotal_html = "">

		<cfset oppProductPicker_html = getOppProductsPicker(argumentCollection=arguments)>
		<cfset oppProductCrossSell_html = getOppProductsCrossSell(argumentCollection=arguments)>
		<cfset oppProductTotal_html = getOppProductsTotals(argumentCollection=arguments)>

		<cfsavecontent variable="oppProductsDisplay_html">
			<cfoutput>
				#oppProductPicker_html#
				#oppProductCrossSell_html#
				<!--- 2010-06-08			NAS			P-CLS003 Fields added as switches to turn fields/functionality off --->
				<cfif request.relayCurrentUser.isInternal OR application.com.settings.getSetting("leadManager.products.useOppProductTotals") >
					#oppProductTotal_html#
				</cfif>
			</cfoutput>
		</cfsavecontent>

		<cfreturn oppProductsDisplay_html>
	</cffunction>

	<cffunction name="getOppProductsCrossSell" access="remote" returnType="string">
		<cfargument name="opportunityID" type="numeric" required="true">

		<cfset var oppProductsCrossSell_html = "">

		<cfsavecontent variable="oppProductsCrossSell_html">
			<cf_translate>
				<cfoutput>

					<!--- if product has cross-selling products set up--->
					<cfset qryCrossSellProducts = application.com.opportunity.getCrossSellProducts(opportunityID=opportunityID)>
					<div class="form-horizontal">
					<cfif qryCrossSellProducts.recordcount GT 0>
						<div class="form-group row">
							<div class="col-xs-6 col-xs-offset-6">
								phr_why_not_upsell_these_products?
							</div>
						</div>
						<cfloop query="qryCrossSellProducts">
							<div class="form-group row">
								<div class="col-xs-6 control-label">
									#Description#
								</div>
								<div class="col-xs-6">
									<CF_INPUT class="btn btn-primary" name="frmAddCrossSellBtn" type="button" onclick="javascript:addCrossSellProduct(#ProductGroupId#,#ProductModelId#,#MinSeats#,#MaxSeats#)"  value="phr_opp_addCrossSellProduct" >
								</div>
							</div>
						</cfloop>
					</cfif>

					<!---
							cross-selling was originally going to allow a phrase to be entered on a per-product basis now we store a list CrossSellSKUs on the product table
							this functionality could still be used in conjuntion with the new way of doing things
					--->
			 		<cfset qGetCrossSellingPhrases = application.com.relayTranslations.getCrossSellingPhrases(opportunityID=arguments.opportunityID)>
					<cfif qGetCrossSellingPhrases.recordcount GT 0>
						<cfloop query="qGetCrossSellingPhrases">
							<div class="form-group row">
								<div class="col-xs-6 col-xs-6">
									phr_crossSelling_product_#qGetCrossSellingPhrases.productID#
								</div>
							</div>
						</cfloop>
					</cfif>
					</div>
				</cfoutput>
			</cf_translate>
		</cfsavecontent>

		<cfreturn oppProductsCrossSell_html>
	</cffunction>


	<!--- START: 2009-10-30 - AJC - Quickbids core upgrades --->
	<cffunction access="remote" returntype="query" name="getDistributorByAccountManager" hint="Retrieves foreign key data for distiLocationID THIS.qdistiLocationID to the query object.">
		<cfargument name="vendorAccountManagerPersonID" type="numeric" required="true" hint="personID value">
		<cfargument name="distiLocFlagList" type="string" required="true" hint="">

		<cfquery name="qryFlagGroup" datasource="#application.siteDataSource#">
			select distinct entitytypeid from flaggroup fg inner join flag f on fg.flagGroupid = f.flagGroupid  WHERE f.flagtextID  in ( <cf_queryparam value="#distiLocFlagList#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )
		</cfquery>

		<cfif qryFlagGroup.recordcount eq 0>
			<cfset tablename = "location">
			<cfset ItemText = "siteName">
		<cfelseif qryFlagGroup.entitytypeid eq 1>
			<cfset tablename = "location">
			<cfset ItemText = "siteName">
		<cfelse>
			<cfset tablename = "organisation">
			<cfset ItemText = "organisationName">
		</cfif>

		<cfquery name="qryDistiLocationID" datasource="#application.siteDataSource#">
			<cfif NOT isdefined('arguments.locationID')>
				select 0 as ID, 'No distributor assigned' as itemText, 0 as SortOrder
				UNION
			</cfif>
			SELECT #tablename#ID as ID, #itemText# as ItemText, 1 as SortOrder
			FROM #tablename#
			WHERE #tablename#ID IN (SELECT EntityID
			      FROM BooleanFlagdata b INNER JOIN Flag f ON b.flagid = f.flagid
			            WHERE f.flagtextID  in ( <cf_queryparam value="#distiLocFlagList#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> ))
			AND countryID =  <cf_queryparam value="#THIS.countryID#" CFSQLTYPE="CF_SQL_INTEGER" >
			<cfif isdefined('arguments.locationID')>
				AND #tablename#ID = <cfqueryparam value="#val(arguments.locationID)#" cfsqltype="cf_sql_integer">
			</cfif>
			ORDER BY SortOrder, ItemText
		</cfquery>

		<cfset THIS.qDistiLocationID = qryDistiLocationID>

		<cfreturn qryDistiLocationID>
	</cffunction>

	<!--- START: 2009-10-30 - AJC - Quickbids core upgrades --->
	<cffunction access="remote" name="getPartnerSalesPerson" returntype="query" hint="gets the partner sales person(s)">
		<cfargument name="locationID" type="numeric" required="false" hint="locationID value" default="0">

		<cfscript>
				var qry_get_partnerSalesPerson = "";
		</cfscript>

		<cfquery name="qry_get_partnerSalesPerson" datasource="#application.siteDataSource#">
			select 0 as personID, 'Choose person' as itemText, 1 as sortIndex
			UNION
			Select personID, FirstName + ' ' + LastName as itemText, 2 as sortIndex
			FROM Person
			WHERE locationID = #arguments.locationID#
			ORDER BY sortIndex, itemText
		</cfquery>

		<cfreturn qry_get_partnerSalesPerson>
	</cffunction>

	<!--- 2014-11-19	RPW	Function getDistiSalesPerson - allowed blank parameter "theID" to be passed in. --->
	<cffunction access="remote" name="getDistiSalesPerson" returntype="query" hint="gets the partner sales person(s)">
		<cfargument name="theID" type="string" required="false" hint="locationID or OrgansationID value" default="0">
		<cfargument name="distiLocFlagList" type="String" default="#application.com.settings.getSetting('leadManager.distiLocFlagList')#">
		<cfargument name="locationID" type="String" default="0">

		<cfset var qry_get_distiSalesPerson=QueryNew("mycol")>

		<cfscript>
				var qry_get_distiSalesPerson = "";
				var qry_get_FlagGroup = "";
		</cfscript>

		<cfquery name="qry_get_FlagGroup" datasource="#application.siteDataSource#">
			select distinct entitytypeid from flaggroup fg inner join flag f on fg.flagGroupid = f.flagGroupid  WHERE f.flagtextID  in ( <cf_queryparam value="#arguments.distiLocFlagList#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )
		</cfquery>

		<cfif arguments.locationID neq 0>
			<cfset arguments.theId = arguments.locationID>
		</cfif>

		<cfquery name="qry_get_distiSalesPerson" datasource="#application.siteDataSource#">
			Select personID , FirstName + ' ' + LastName as itemText, 2 as sortIndex
			FROM Person
			WHERE
			<cfif qry_get_FlagGroup.recordcount eq 0 or qry_get_FlagGroup.entitytypeid eq 1 or arguments.locationID neq 0>
				locationID
			<cfelse>
				organisationID
			</cfif>
				= <cf_queryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.theID#" null="#!IsNumeric(arguments.theID)#">
			ORDER BY sortIndex, itemText
		</cfquery>

		<cfreturn qry_get_distiSalesPerson>
	</cffunction>

	<cffunction access="remote" name="getPartnerLocation" returntype="query" hint="">
		<cfargument name="vendorAccountManagerPersonID" type="numeric" required="false" default="0" hint="vendorAccountManagerPersonID value">
		<cfargument name="organisationAcMngrFlagTextIDs" type="string" required="false" default="" hint="organisationAcMngrFlagTextIDs value">
		<cfargument name="partnerOrgFlagList" type="string" required="false" default="">
		<cfargument name="countryID" type="numeric" required="true" hint="countryID value">

		<cfscript>
			var qry_get_partner_location = "";
		</cfscript>
		<!--- P-FNL079 2009/10/28 - added sortIndex --->
		<cfquery name="qry_get_partner_location" datasource="#application.siteDataSource#">
			<cfif NOT isdefined('arguments.locationID')>
			select 0 as locationID, 'No partner assigned' as itemText, 1 as sortIndex
			UNION
			</cfif>
			SELECT l.locationID, siteName +', '+ address4 +' ('+  c.ISOCode +')' as ItemText, 2 as sortIndex
			FROM location l
			INNER JOIN country c ON c.countryID = l.countryID
			INNER JOIN person p on p.locationid = l.locationid
			WHERE l.OrganisationID IN (SELECT EntityID
				FROM BooleanFlagdata b INNER JOIN Flag f ON b.flagid = f.flagid WHERE f.flagtextID  in ( <cf_queryparam value="#urldecode(partnerOrgFlagList)#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> ))
			AND l.OrganisationID IN
			(SELECT EntityID
				FROM IntegerFlagdata b INNER JOIN Flag f ON b.flagid = f.flagid WHERE f.flagtextID  in ( <cf_queryparam value="#urldecode(organisationAcMngrFlagTextIDs)#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> ) and data = #vendorAccountManagerPersonID#)

			AND l.countryID = #arguments.countryID#

			ORDER BY sortIndex, ItemText
		</cfquery>

		<cfreturn qry_get_partner_location />
	</cffunction>

	<cffunction access="remote" name="getProductGroupProducts" returntype="query" hint="">
		<cfargument name="productGroupID" type="numeric" required="false" default="0">
		<cfargument name="productCatalogueCampaignID" type="numeric" required="false" default="0">
		<cfargument name="countryID" type="numeric" required="false" default="#request.relaycurrentuser.content.showForCountryID#">

		<cfset var qry_get_productGroup_Products="">
		<cfset var opp_SelectProduct = application.com.relayTranslations.translatePhrase("opp_SelectProduct")>

		<cfquery name="qry_get_productGroup_Products" datasource="#application.siteDataSource#">
			select 0 as productID, '#opp_SelectProduct#' as productDescription, 0,
				0 as productGroupID, '' as productGroupDescription
		UNION
			SELECT distinct p.productID as productID, left(p.SKU+' '+replace(replace(replace(p.description,CHAR(34),' inch'),CHAR(39),''),CHAR(47),''),47) as productDescription,p.sortOrder,
				pg.productGroupID, pg.[description] as productGroupDescription
					FROM product p
                    left join vCountryScope vcs on p.hasCountryScope = 1 and vcs.entity='product' and vcs.entityid = productid
				INNER JOIN productGroup pg
				ON p.productGroupID = pg.productGroupID
				where p.campaignID in (#arguments.productCatalogueCampaignID#)
				<cfif request.relaycurrentuser.isinternal>
                    and (p.hascountryScope = 0 or vcs.countryid in (select countryid FROM vCountryRights vcr with (noLock) WHERE vcr.personid = <cf_queryparam value="#request.relayCurrentUser.personID#" cfsqltype="CF_SQL_INTEGER"> ) )
				<cfelse>
					and (p.hascountryScope = 0 or vcs.countryid = <cf_queryparam value="#request.relaycurrentuser.countryID#" cfsqltype="CF_SQL_INTEGER"> )
				</cfif>
				and deleted = 0
				and p.active=1
				and p.productGroupID=#arguments.productGroupID#
				order by productGroupDescription, productGroupID, productDescription
		</cfquery>
		<cfreturn qry_get_productGroup_Products />
	</cffunction>

	<cffunction access="remote" name="getProduct" returntype="string" hint="">
		<cfargument name="productID" type="numeric" required="false" default="0">

		<cfset var qry_get_product="">

		<cfquery name="qry_get_product" datasource="#application.siteDataSource#">
			SELECT ListPrice
			FROM Product p
			where p.productID = #arguments.productID#
		</cfquery>

		<cfreturn qry_get_product.ListPrice />
	</cffunction>
	<!--- END: 2009-10-30 - AJC - Quickbids core upgrades --->

	<cffunction name="getOppProductsTotals" access="remote" returnType="string">
		<cfargument name="opportunityID" type="numeric" required="true">
		<cfargument name="readOnly" type="boolean" default="false">

		<cfset var oppProductsTotal_html = "">
		<cfset var qryGetOppProductsTotals = "">
		<cfset var appliedOppPromotions = application.com.opportunity.getAppliedOppPromotions(opportunityID=arguments.opportunityID)>
		<cfset var currencySign = "">
		<cfset var useOppProductTotals = application.com.settings.getSetting("leadManager.products.useOppProductTotals")>
		<cfset var useColSpan = 6>
		<cfset var useStyleHide = "">

		<cfif request.relayCurrentUser.isInternal OR useOppProductTotals>
			<cfset useColSpan = 8>
		<cfelse>
			<cfset useStyleHide = "display : none;">
		</cfif>

		<cfset qryGetOppProductsTotals = application.com.opportunity.getOppProductsTotals(opportunityID=arguments.opportunityID).query>

		<cfset currencySign = qryGetOppProductsTotals.currencySign>

		<cfsavecontent variable="oppProductsTotal_html">
			<cf_translate>
				<cfoutput>

					<script>
						checkIfPromotionAlreadyApplied = function(opportunityID,promoCode,readOnly) {
							//added if to stop the checking of blank promo codes LID3745,3744
							if (promoCode != '') {
								var timeNow = new Date();
								page = '/webservices/relayOpportunityWS.cfc?wsdl&method=checkIfPromotionAlreadyApplied'
								page = page + '&promotionCode='+promoCode;
								page = page + '&opportunityID='+opportunityID;

								var myAjax = new Ajax.Request(
									page,
									{
										method: 'get',
										parameters: 'returnFormat=json',
										evalJSON: 'force',
										debug : false,
										asynchronous: false,
										onComplete: function (requestObject,JSON) {
															json = requestObject.responseJSON;
															if (json.PROMOTIONALREADYAPPLIED==0) {
																applyPromotion(opportunityID,promoCode,readOnly);
															}
														}
										}
									);
							}
						}


						applyPromotion = function(opportunityID,promoCode,readOnly) {

							var timeNow = new Date();
							page = '/webservices/relayOpportunityWS.cfc?wsdl&method=applyOppPromotions&time='+timeNow
							page = page + '&promotionCode='+promoCode;
							page = page + '&opportunityID='+opportunityID;
							page = page + '&triggerPoints=promocode,subtotal';

							var myAjax = new Ajax.Request(
								page,
								{
									method: 'get',
									parameters: 'returnFormat=json',
									evalJSON: 'force',
									debug : false,
									asynchronous: false,
									onComplete: function (requestObject,JSON) {
														updateOppProductsDiv(opportunityID,readOnly);
													}
									}
								);
						}
					</script>
					<div class="form-horizontal">
					<div class=" form-group row">
						<div class="col-xs-6 align-right">
							<b>phr_opp_SubTotal</b>
						</div>
						<div class="col-xs-6">
							#currencySign##decimalFormat(qryGetOppProductsTotals.subTotal)#
						</div>
					</div>
					<div class=" form-group row">
						<div class="col-xs-6 col-xs-offset-6">
							<b>phr_opp_PromotionDiscount</b>
						</div>
					</div>
					<cfloop query="appliedOppPromotions">
						<cfset discountedProducts = application.com.opportunity.getDiscountedAmountForPromotion(opportunityID=opportunityID,promotionID=rwPromotionCode)>
						<cfset textColor="">
						<cfif NOT (active eq 1 and (StartDate eq "" or StartDate lt now()) and (EndDate eq "" or EndDate gt now())) >
							<cfset productList = "phr_Expired">
							<cfset textColor="red">
						<cfelseif NOT application.com.opportunity.checkIfCountryInScopeForPromotion(opportunityID=opportunityID,rwPromotionID=appliedOppPromotions.rwPromotionId)>		<!--- 2010/12/02 PPB added check --->
							<cfset productList = "phr_CountryNoLongerQualifiesForPromotion">
							<cfset textColor="red">
						<cfelseif NOT application.com.opportunity.checkIfPersonInScopeForPromotion(opportunityID=opportunityID,rwPromotionID=appliedOppPromotions.rwPromotionId)>		<!--- 2010/12/02 PPB added check --->
							<cfset productList = "phr_PersonNoLongerQualifiesForPromotion">
							<cfset textColor="red">
						<cfelse>
							<cfset productList = valueList(discountedProducts.sku)>
						</cfif>
						<cfif productList neq "">
							<div class="form-group row">
								<div class="col-xs-6 align-right">
									#rwPromotionCode# (#productList#)
								</div>
								<div class="col-xs-6">
									<span style="color:#textColor#">-#currencySign##decimalFormat(arraySum(listToArray(valueList(discountedProducts.discount))))#</span>
								</div>
							</div>
						</cfif>
					</cfloop>

					<!--- 2012-03-13 PPB AVD001 new setting --->
					<cfif (not arguments.readOnly and application.com.settings.getSetting("leadManager.allowPromotionCodeEntry"))>
						<div class="form-group row">
							<div class="col-xs-6 control-label">
								phr_opp_ApplyPromotionCode
							</div>
							<div class="col-xs-6">
								<CF_INPUT class="form-control" type="text" name="frmOppPromotion" onBlur="checkIfPromotionAlreadyApplied(#arguments.opportunityID#,this.value,false)">
							</div>
						</div>
					</cfif>
					<!--- 2010-06-14			NAS			P-CLS002 Fields added as switches to turn fields/functionality on/off --->
					<!--- 2012-03-13 PPB AVD001 changed logic so we don't see SP totals if we are not using SP --->
					<cfif ((application.com.settings.getSetting("leadManager.specialPricing.useSpecialPricingOnInternal") and  request.relayCurrentUser.isInternal) OR application.com.settings.getSetting("leadManager.specialPricing.useSpecialPricingOnExternal")) and (qryGetOppProductsTotals.statusTextID neq "" and qryGetOppProductsTotals.statusTextID neq "StandardPricing")>
						<div class="form-group row">
							<div class="col-xs-6 align-right">
								<b>phr_opp_SpecialPriceDiscount</b></div>
							<div class="col-xs-6">
								-#currencySign##decimalFormat(qryGetOppProductsTotals.specialPriceDiscount)#
							</div>
						</div>
					</cfif>
					<div class="form-group row">
						<div class="col-xs-6 align-right">
							<b>phr_opp_Total</b>
						</div>
						<div class="col-xs-6">
							<b>#currencySign##decimalFormat(qryGetOppProductsTotals.total)#</b>
						</div>
					</div>
				</cfoutput>
			</cf_translate>
		</cfsavecontent>

		<cfreturn oppProductsTotal_html>
	</cffunction>


	<!--- <cffunction name="getOpportunityPartnerContacts" access="remote" returnType="query">
		<cfargument name="organisationID" type="numeric">

		<cfreturn application.com.opportunity.getOpportunityPartnerContacts(argumentCollection=arguments)>
	</cffunction> --->


	<cffunction name="getOpportunityEndCustomerContacts" access="remote" returnType="query">
		<cfargument name="organisationID" type="numeric">

		<cfreturn application.com.opportunity.getOpportunityEndCustomerContacts(argumentCollection=arguments)>
	</cffunction>


	<cffunction name="getOpportunityPartners" access="remote" returnType="query">
		<cfargument name="countryID" type="string">
		<!--- <cfargument name="showNullText" required="false" default="true"> --->
		<cfif not isNumeric(arguments.countryID)>
			<cfset arguments.countryID = 0>
		</cfif>
		<cfreturn application.com.opportunity.getOppPartners(argumentCollection=arguments)>
	</cffunction>


	<cffunction name="getOppPartnerSalesPerson" access="remote" returnType="query">
		<cfargument name="locationID" required="true">
		<cfargument name="showNullText" required="false" default="true">

		<cfif not isNumeric(arguments.locationID)>
			<cfset arguments.locationID=0>
		</cfif>
		<cfreturn application.com.opportunity.getOppPartnerSalesPerson(argumentCollection=arguments)>
	</cffunction>

	<cffunction name="getOppPartnerSalesContacts" access="remote" returnType="query">
		<cfargument name="organisationID" required="true">
		<cfargument name="showNullText" required="false" default="true">

		<cfif not isNumeric(arguments.organisationID)>
			<cfset arguments.organisationID=0>
		</cfif>
		<cfreturn application.com.opportunity.getOpportunityEndCustomerContacts(argumentCollection=arguments)>
	</cffunction>


	<cffunction name="getOppDistiLocations" access="remote" returnType="query">
		<cfargument name="locationID" required="false">
		<cfargument name="countryID" required="false">
		<cfargument name="filterInactive" type="boolean" default=false><!--- 2014-09-22 AHL Case 441630 Inactive Distributor should not be displayed in "Distributor" drop down --->

		<cfif structKeyExists(arguments,"locationID") and not isNumeric(arguments.locationID)>
			<cfset arguments.locationID=0>
		</cfif>
		<!--- 2015-02-10	RPW	Leads - Added ability to customize leads form for customers. Catered for blank countryID --->
		<cfscript>
			if (StructKeyExists(arguments,"countryID") && !IsNumeric(arguments.countryID)) {
				arguments.countryID = 0;
			}
		</cfscript>
		<cfreturn application.com.opportunity.getOppDistiLocations(argumentCollection=arguments)>
	</cffunction>


	<cffunction name="getOpportunityDistiPerson" access="remote" returnType="query">
		<cfargument name="distiLocationID" required="true">
		<!--- <cfargument name="showNullText" required="false" default="true"> --->

		<cfif not isNumeric(arguments.distiLocationID)>
			<cfset arguments.distiLocationID=0>
		</cfif>
		<cfreturn application.com.opportunity.getOppDistiSalesPerson(argumentCollection=arguments)>
	</cffunction>


	<cffunction name="insertOppProduct" access="remote" returntype="struct">
		<cfargument name="opportunityId" type="numeric" required="true">
		<cfargument name="productID" type="numeric" required="true">
		<cfargument name="quantity" type="numeric" default=1>

		<cfreturn application.com.opportunity.addProductToOpportunity(argumentCollection=arguments)>
	</cffunction>

<!--- 	<cffunction name="addProductToSession" access="remote">
		<cfargument name="opportunityID">
		<cfargument name="productID">

		<cfif not (structKeyExists(session,"opportunity"))>
			<cfset session.opportunity = structNew()>
		</cfif>

		<cfif not (structKeyExists(session.opportunity,"ID") and session.opportunity.ID neq arguments.opportunityID)>
			<cfset session.opportunity.opportunityID = arguments.opportunityID>
		</cfif>

		<cfif not structKeyExists(session.opportunity,"productsToAdd")>
			<cfset session.opportunity.productsToAdd = "">
		</cfif>

		<cfif not listFind(session.opportunity.productsToAdd,arguments.productID)>
			<cfset session.opportunity.productsToAdd = listAppend(session.opportunity.productsToAdd,arguments.productID)>
		</cfif>

	</cffunction> --->

	<cffunction name="removeOppProduct" access="remote" returnType="any">
		<cfargument name="opportunityId" type="numeric" required="true">
		<cfargument name="productID" type="numeric" required="false">
		<cfargument name="oppProductID" type="numeric" required="false">

		<cfset application.com.opportunity.removeProductFromOpportunity(argumentCollection=arguments)>
	</cffunction>


	<cffunction name="checkIfPromotionAlreadyApplied" access="remote" returnType="any">
		<cfargument name="opportunityID" type="numeric" required="true">
		<cfargument name="promotionCode" type="string" required="false">

		<cfreturn application.com.opportunity.checkIfPromotionAlreadyApplied(argumentCollection=arguments)>
	</cffunction>


	<cffunction name="applyOppPromotions" access="remote" returnType="any">
		<cfargument name="opportunityID" type="numeric" required="true">
		<cfargument name="promotionCode" type="string" required="false">
		<cfargument name="triggerPoints" type="string" required="false" default="">
		<cfargument name="excludeTriggerPoints" type="string" required="false" default="">

		<cfset application.com.opportunity.applyPromotion(argumentCollection=arguments)>

	</cffunction>


	<cffunction name="getOppSpecialPricingDisplay" access="remote" returnType="string">
		<cfargument name="opportunityID" type="numeric" required="true">

		<cfset var oppProductsDisplay_html = "">
		<cfset var getOppProducts = "">

		<cfquery name="getOppProducts" datasource="#application.siteDataSource#">
			select p.productID,p.sku, c.currencySign, op.unitPrice, op.PromotionPrice, ISNULL(op.promotionPrice,op.unitPrice) AS price, op.specialPrice,
                       	<!--- LID 3765 NAS 2010-07-27 --->
						case when ISNULL(promotionPrice,unitPrice) > 0 THEN
						cast(round(((ISNULL(promotionPrice,unitPrice)-specialPrice)/unitPrice) *100,0) as int)
						else 0
						end
						as percentage,
                        description_defaultTranslation as description, subTotal, quantity, ISNULL(promotionPrice,unitPrice)*Quantity as priceByQuantity,
						<!--- LID 3745 NAS 2010-07-26 --->
						cast(Case
						WHEN specialprice > 0 THEN specialPrice * Quantity
						  ELSE NULL
						end as decimal(8,2)) as specialpriceByQuantity
			from product p inner join opportunityProduct op on p.productID = op.productOrGroupID and op.productOrGroup = 'P'
				inner join opportunity o on op.opportunityID = o.opportunityID
				inner join currency c on c.currencyISOCode = o.currency
			where op.opportunityID = #arguments.opportunityID#
		</cfquery>

		<cfsavecontent variable="oppProductsDisplay_html">
			<cfoutput>

				<script>
					calculateDiscountPercentage = function (specialPrice,productID) {
						var prodPercObj = document.getElementById('frmSpecialPricePercDiscount_'+productID);
						var priceByQuantity = parseFloat(prodPercObj.getAttribute('price') * prodPercObj.getAttribute('quantity'));

						if (specialPrice != '') {
							prodPercObj.value = Math.round((priceByQuantity-specialPrice)/priceByQuantity*10000)/100;
						} else {
							prodPercObj.value = '';
						}

						<!--- calculate the discount for each product if the total percentage has been filled in --->
						if (productID == 'Total') {
							calcDiscountTotal();
						}
					}

					calculateDiscount = function (productID,percentage) {

						var prodDiscountObjArray = new Array();
						var thisProductID = 0;
						var prodPercObj = "";

						if (productID != undefined) {
							prodDiscountObjArray[0] = document.getElementById('frmSpecialPrice_'+productID);
						} else {
							myRegExp = new RegExp('frmSpecialPrice_');
							prodDiscountObjArray=getElementsByRegularExpression(myRegExp,'INPUT');
						}

						for (i=0;i<prodDiscountObjArray.length;i++) {
							priceByQuantity = parseFloat(prodDiscountObjArray[i].getAttribute('price')*prodDiscountObjArray[i].getAttribute('quantity'));

							if (percentage != '') {
								prodDiscountObjArray[i].value = Math.round((priceByQuantity -((priceByQuantity/100)*percentage))*100)/100;
							} else {
								prodDiscountObjArray[i].value = '';
							}

							<!--- if we are calculating the discount from the total, then set the percentage for each product as well as the discount --->
							if (productID == 'Total') {
								thisProductID = prodDiscountObjArray[i].getAttribute('productID');

								if (thisProductID != null) {
									prodPercObj = document.getElementById('frmSpecialPricePercDiscount_'+thisProductID);
									prodPercObj.value = percentage;
								}
							}
						}
					}

					calcDiscountTotal = function() {
						percentageObj=document.getElementById('frmSpecialPricePercDiscount_Total');
						calculateDiscount('Total',percentageObj.value);
						clearProductPerc();
						clearProductPrice();
					}

					calcDiscountPercTotal = function () {
						discount=document.getElementById('frmSpecialPrice_Total');
						calculateDiscountPercentage(discount.value,'Total');
						//clearProductPerc();
					}

					clearTotal = function () {
						percentage=document.getElementById('frmSpecialPricePercDiscount_Total');
						percentage.value = '';

						discount=document.getElementById('frmSpecialPrice_Total');
						discount.value = '';
					}

					clearProductPerc = function() {
						myRegExp = new RegExp('frmSpecialPricePercDiscount_');
						percTextArray=getElementsByRegularExpression(myRegExp,'INPUT');

						for (i=0;i<percTextArray.length;i++) {
							if (percTextArray[i].id != 'frmSpecialPricePercDiscount_Total') {
								percTextArray[i].value = '';
							}
						}
					}

					clearProductPrice = function() {
						myRegExp = new RegExp('frmSpecialPrice_');
						priceTextArray=getElementsByRegularExpression(myRegExp,'INPUT');

						for (i=0;i<priceTextArray.length;i++) {
							if (priceTextArray[i].id != 'frmSpecialPrice_Total') {
								priceTextArray[i].value = '';
							}
						}
					}
				</script>

				<cf_translate>
					<!--- PPB 2010-06-03 I introduced a additional attribute quantity on the form fields to be able to calculate the discounts to be compatible with the way the price attributes is used;
					however after posting the form we need to know the quantity of each product to divide by to convert back so as to store unit specialprices on the database so introduced a hidden form field (we could use this hidden field for the calcs too but i was unsure whether the use of prodDiscountObjArray=getElementsByRegularExpression(myRegExp,'INPUT') above would confuse issues... so I just left it working! ) --->
				<p><b>phr_opp_CalculateSpecialPriceForIndividualProducts</b></p>
				<table data-role="table" data-mode="reflow" class="responsiveTable table table-striped table-bordered table-condensed withBorder">
					<thead>
					<tr>
						<td><b>phr_opp_Description</b></td>
						<td><b>phr_opp_Price</b></td>
						<td><b>phr_opp_SpecialPrice</b></td>
						<td><b>phr_opp_DiscountPercentage</b></td>
					</tr>
					</thead>
					<tbody>
					<cfloop query="getOppProducts">
						<tr>
							<td>#getOppProducts.description#</td>
							<td>#currencySign##decimalFormat(priceByQuantity)#</td>
							<!--- LID 3745 NAS 2010-07-26 --->
							<td>
								<div class="form-group">
									<div class="col-xs-1 col-md-3 control-label">#currencySign#</div>
									<div class="col-xs-4 col-sm-5 col-md-9">
										<CF_INPUT class="form-control" type="text" id="frmSpecialPrice_#productID#" name="frmSpecialPrice_#productID#" value="#specialpriceByQuantity#" price="#price#" quantity="#quantity#" productID="#productID#" onBlur="javascript:calculateDiscountPercentage(this.value,#productID#);clearTotal();" size=5>
										<CF_INPUT type="hidden" id="frmSpecialPriceQuantity_#productID#" name="frmSpecialPriceQuantity_#productID#" value="#quantity#">
									</div>
								</div>
							</td>
							<td id="frmSpPercentage_#productID#">
								<div class="form-group">
									<div class="col-xs-1 col-md-3 control-label">%</div>
									<div class="col-xs-4 col-sm-5 col-md-9">
										<CF_INPUT class="form-control" type="text" id="frmSpecialPricePercDiscount_#productID#" name="frmSpecialPricePercDiscount_#productID#" value="#percentage#" price="#price#" quantity="#quantity#" productID="#productID#" <!--- onBlur="javascript:calculateDiscount(this.value,#productID#);clearTotal();" ---> size=3>
									</div>
								</div>
								<div class="form-group">
									<div class="col-xs-8 col-xs-offset-4 col-sm-5 col-sm-offset-7 col-md-8 col-md-offset-3">
										<CF_INPUT type="button" id="frmCalculatePerc" value="Calculate Discount" onClick="calculateDiscount(#productID#,frmSpecialPricePercDiscount_#productID#.value);clearTotal();">
									</div>
								</div>
							</td>
						</tr>
					</cfloop>
					</tbody>
				</table>
				<p><b>or</b> phr_opp_CalculateSpecialPriceForTotalOpportunity</p>
				<table data-role="table" data-mode="reflow" class="responsiveTable table table-striped table-bordered table-condensed withBorder">
					<thead>
						<tr>
							<td><b>phr_opp_Price</b></td>
							<td><b>phr_opp_SpecialPrice</b></td>
							<td><b>phr_opp_DiscountPercentage</b></td>
						</tr>
					</thead>
					<tbody>
					<cfset totalPrice = arraySum(listToArray(valueList(getOppProducts.priceByQuantity)))>
						<tr>
							<td>#getOppProducts.currencySign##decimalFormat(totalPrice)#</td>
							<td>
								<div class="form-group">
									<div class="col-xs-1 col-md-3 control-label">#getOppProducts.currencySign#</div>
									<div class="col-xs-4 col-sm-5 col-md-9">
										<CF_INPUT class="form-control" type="text" id="frmSpecialPrice_Total" name="frmSpecialPrice_Total" size=5 price="#totalPrice#" quantity="1">
										<input type="hidden" id="frmSpecialPriceQuantity_Total" name="frmSpecialPriceQuantity_Total" value="1">
									</div>
								</div>
								<div class="form-group">
									<div class="col-xs-8 col-xs-offset-4 col-sm-5 col-sm-offset-7 col-md-8 col-md-offset-3">
										<input class="btn btn-primary" type="button" id="frmCalculateTotal" value="Calculate Percentage" onClick="javascript:calcDiscountPercTotal();">
									</div>
								</div>
							</td>
							<td>
								<div class="form-group">
									<div class="col-xs-1 col-md-3 control-label">%</div>
									<div class="col-xs-4 col-sm-5 col-md-9">
										<CF_INPUT class="form-control" type="text" id="frmSpecialPricePercDiscount_Total" name="frmSpecialPricePercDiscount_Total" size=3 price="#totalPrice#" quantity="1">
									</div>
								</div>
								<div class="form-group">
									<div class="col-xs-8 col-xs-offset-4 col-sm-5 col-sm-offset-7 col-md-8 col-md-offset-3">
										<input class="btn btn-primary" type="button" id="frmCalculatePerc" name="frmCalculatePerc" value="Calculate Discount" onClick="javascript:calcDiscountTotal();">
									</div>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
				<p>phr_opp_enterSpecialPricingReason</p>
					<!--- START: 2013-10-01 NYB Case 437100 - replaced: ---
					<textarea rows="5" cols="50" id="frmSpecialPriceReason" name="frmSpecialPriceReason"></textarea>
					!--- 2013-10-01 NYB Case 437100 - with: --->
					<cfset specialPriceReasonMaxSize = application.com.applicationvariables.getTableInfoStructure(tableName="oppPricingStatusHistory")>
					<cfset specialPriceReasonMaxSize = specialPriceReasonMaxSize.comments.MAXLENGTH>
					<cf_relaytextfield FIELDNAME="frmSpecialPriceReason" id="frmSpecialPriceReason" CURRENTVALUE="" maxlength="#specialPriceReasonMaxSize#" rows="5" cols="50" usecfform="false">
					<!--- END: 2013-10-01 NYB Case 437100 --->
					<!--- <input type="hidden" id="frmApplyForSpecialPricing" name="frmApplyForSpecialPricing" value="yes"> --->
				</cf_translate>
			</cfoutput>
		</cfsavecontent>

		<cfreturn oppProductsDisplay_html>
	</cffunction>


	<cffunction name="getOpportunityProductCount" access="remote" returntype="numeric" hint="Returns the number of products for an opportunity">
		<cfargument name="opportunityID" type="numeric">

		<cfreturn application.com.opportunity.getOpportunityProductCount(argumentCollection=arguments)>
	</cffunction>

	<!--- 2013-07-02	YMA	Case 436045 Return empty if any of the parameters are not available. --->
	<cffunction name="getProducts" access="remote" returntype="query">
 		<cfargument name="productGroupID" default="">
		<cfargument name="countryID" default="#request.relaycurrentuser.content.showForCountryID#">
		<cfargument name="campaignID" default="">

		<cfscript>
			var qryProducts = queryNew("sku,display,orderindex");
		</cfscript>

		<cfif isNumeric(arguments.productGroupID) and isNumeric(arguments.countryID) and isNumeric(arguments.campaignID)>
			<cfquery name="qryProducts" datasource="#application.siteDataSource#">
				select '' as sku, 'None' as display, 1 as orderIndex
				union
				select distinct sku, '('+SKU+') '+description as display, 2 as orderIndex
					from product p
                    left join vCountryScope vcs on p.hasCountryScope = 1 and vcs.entity='product' and vcs.entityid = productid
					where productGroupID =  <cf_queryparam value="#arguments.productGroupID#" CFSQLTYPE="cf_sql_integer" >
					<!--- internal site only - called when adding/editting products --->
                    and (p.hascountryScope = 0 OR vcs.countryid in (select countryid FROM vCountryRights vcr with (noLock) WHERE vcr.personid = <cf_queryparam value="#request.relayCurrentUser.personID#" cfsqltype="CF_SQL_INTEGER"> ) )
					and campaignID =  <cf_queryparam value="#arguments.campaignID#" CFSQLTYPE="cf_sql_integer" >
					and deleted=0
					and active=1
				order by orderIndex,sku
			</cfquery>
		</cfif>
		<cfreturn qryProducts>
	</cffunction>

	<cffunction name="validateQuantityField" access="remote" returntype="boolean">
		<cfargument name="productID" type="numeric" required="true">
		<cfargument name="quantity" type="numeric" required="true">

		<cfquery name="getMinMaxSeatForProduct" datasource="#application.siteDataSource#">
			select maxSeats, minSeats, productType from product where productID=#arguments.productID#
		</cfquery>

		<cfif getMinMaxSeatForProduct.productType eq "License">
			<cfif (getMinMaxSeatForProduct.minSeats lte arguments.quantity) and (getMinMaxSeatForProduct.maxSeats gte arguments.quantity)>
				<cfreturn true>
			<cfelse>
				<cfreturn false>
			</cfif>
		</cfif>

		<cfreturn true>
	</cffunction>

	<cffunction name="getOppProductsPicker" access="remote" output="false" returnType="string">
		<cfargument name="opportunityid" type="numeric" required="true">
		<cfargument name="readOnly" type="boolean" default="false">

		<cfset var oppProductsDisplay_html = "">
		<cfset var getOppProducts = "">
		<cfset var oppDetails = application.com.opportunity.getOpportunityDetails(opportunityID=arguments.opportunityID)>
		<cfset var oppPricebook = application.com.opportunity.getOppPricebook(opportunityID=oppDetails.opportunityID)>
		<cfset var useOppProductTotals = application.com.settings.getSetting("leadManager.products.useOppProductTotals")>
		<cfset var productTitlePhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "product", phraseTextID = "title",baseTableAlias="p") >
		<cfset var productGroupTitlePhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "productGroup", phraseTextID = "title",baseTableAlias="pg") >
		<cfset var useStyleHide = "">
		<cfset var showSeats = application.com.settings.getSetting("leadManager.Products.showSeats")>

		<!--- NJH 2012/07/18 - changed the product picker to accept product groups as well, as the mobile will select groups rather than products..
			do a union to get the product groups
		 --->
		<cfquery name="getOppProducts" datasource="#application.siteDataSource#">
			select op.opportunityProductID as oppProductID,p.productID,p.sku, c.currencySign,
				#preserveSingleQuotes(productTitlePhraseQuerySnippets.select)# as description,
				op.quantity, op.quantity*op.unitPrice as price,
				case when p.productType='License' then cast(minSeats as varchar)+'-'+cast(maxSeats as varchar) else '' end as seats,
				op.productOrGroup
			from product p
				#preserveSingleQuotes(productTitlePhraseQuerySnippets.join)#
				inner join opportunityProduct op on p.productID = op.productOrGroupID and op.productOrGroup = 'P'
				inner join opportunity o on op.opportunityID = o.opportunityID
				inner join currency c on c.currencyISOCode = o.currency
			where o.opportunityID = #arguments.opportunityID#
			union
			select op.opportunityProductID as oppProductID,0 as productID,'' as sku, '' as currencySign,
				#preserveSingleQuotes(productGroupTitlePhraseQuerySnippets.select)# as description,
				op.quantity, op.quantity*op.unitPrice as price,
				'' as seats, op.productOrGroup
			from productGroup pg
				#preserveSingleQuotes(productGroupTitlePhraseQuerySnippets.join)#
				inner join opportunityProduct op on pg.productGroupID = op.productOrGroupID and op.productOrGroup = 'G'
				inner join opportunity o on op.opportunityID = o.opportunityID
			where o.opportunityID = #arguments.opportunityID#
		</cfquery>

		<cfset appliedOppPromotions = application.com.opportunity.getAppliedOppPromotions(opportunityID=arguments.opportunityID)>


		<cfsavecontent variable="oppProductsDisplay_html">
				<cfoutput>
				<cf_translate encode="javascript">
				<script>
					// 2016-04-15 AHL Product Combined/Select Box
					populateProductGroup = function(opportunityID,options) {
						$('productGroupRow1').hide();
						$('productRow1').show();
						$('qtyDiv').hide()

						populateProductPickerSearchAndSelect(opportunityID);
					}

					// load Products select box
					populateProducts = function(productGroupID,options) {
						// make sure the row of the table is visible, clear out and hide the other 2 select boxes, clear out price
						$('productRow1').show();
						$('qtyDiv').hide()
						options = {nullText:'phr_Ext_SelectAValue'}
						populateSelectBoxViaAjax ('frmProduct','/webservices/callWebService.cfc?webServiceName=relayOpportunityws&methodname=getOppPricebookProducts','callWebService','ProductGroupid=' + productGroupID + '&useFormat=oppProductDropdown&pricebookid=' + '<cfoutput>#oppPricebook.pricebookID#</cfoutput>',options)
					}

					// load product detail fields
					populateProductDetails = function(productID) {
						page = '/webservices/callWebService.cfc?'
						parameters = 'method=callWebService&webServiceName=relayOpportunityWS&methodName=getProductSeats&productId='+productID+'&returnFormat=json&_cf_nodebug=true';

						var myAjax = new Ajax.Request(
								page,
								{
									method: 'get',
									parameters: parameters,
									evalJSON: 'force',
									debug : false,
									asynchronous: false
								}

							);

						var result = myAjax.transport.responseText.evalJSON();
						// 2015-06-25 DAN Case 444803 - avoid NPE as productSeatsDiv may not exist
                        if ($('productSeatsDiv') && result.PRODUCTTYPE.toString().toLowerCase() == 'license') {
							$('productSeatsDiv').innerHTML=result.MINSEATS + '-' + result.MAXSEATS;
						}

						$('qtyDiv').show()
						$('frmAddProductBtn').disabled = false;
					}

				</script>
				</cf_translate>
				</cfoutput>

				<cfif not request.relayCurrentUser.isInternal and not useOppProductTotals>
					<cfset useStyleHide = "display : none;">
				</cfif>

				<cf_translate>
					<cfoutput>
					<table data-role="table" data-mode="reflow" class="responsiveTable table table-striped table-bordered table-condensed withBorder" id="oppProductsDisplayTable">
						<thead>
							<tr>
								<th>phr_Description</th>
								<th>phr_Quantity</th>
								<!--- START 2014-12-05 PPB P-ABS001 --->
								<cfif showSeats>
									<th>phr_Seats</th>
								</cfif>
								<!--- END 2014-12-05 PPB P-ABS001 --->
								<cfif request.relayCurrentUser.isInternal OR useOppProductTotals>
									<th>phr_Price</th>
								</cfif>
								<th>&nbsp;</th>
							</tr>
						</thead>
						<tbody>
					</cfoutput>

					<!--- NYB 28-01-2011 P-NET002 - added: --->
					<cfset descriptionFormat = application.com.settings.getSetting("leadManager.formatting.opportunityEdit.oppProductList")>
							<cfoutput query="getOppProducts">
								<!--- START: NYB 28-01-2011 P-NET002 - added: --->
								<cfset mergeStruct = structnew()>
								<cfset mergestruct = application.com.structureFunctions.queryRowToStruct(query=getOppProducts,row=getOppProducts.currentrow)>
									<tr>
										<cfif productOrGroup eq "P">
											<cfset descriptionFormatted = application.com.relayTranslations.checkForAndEvaluateCFVariables(phrasetext=descriptionFormat,mergeStruct=mergeStruct)>
										<cfelse>
											<cfset descriptionFormatted = description>
										</cfif>
										<!--- END: NYB 28-01-2011 P-NET002 - added: --->
										<!--- NYB 28-01-2011 P-NET002 - changed from #description#: --->
										<td><cfif descriptionFormatted neq "">#descriptionFormatted#<cfelse>&nbsp;</cfif></td>
										<td><cfif quantity neq "">#quantity#<cfelse>&nbsp;</cfif></td>
										<!--- START 2014-12-05 PPB P-ABS001 --->
										<cfif showSeats>
											<td><cfif seats neq "">#seats#<cfelse>&nbsp;</cfif></td>
										</cfif>
										<!--- END 2014-12-05 PPB P-ABS001 --->
										<cfif request.relayCurrentUser.isInternal OR useOppProductTotals>
											<td><cfif productOrGroup eq "P"><cfif price neq "">#currencySign##decimalFormat(price)#<cfelse>&nbsp;</cfif><cfelse>&nbsp;</cfif></td>
										</cfif>
										<td><cfif not arguments.readOnly><CF_INPUT type="button" value="phr_opp_delete" id="frmDeleteBtn" onClick="javascript:deleteOppProduct(#arguments.opportunityID#,#oppProductID#);"><cfelse>&nbsp;</cfif></td>
									</tr>
							</cfoutput>


							<cfif oppPricebook.pricebookID eq "">
								<cfoutput><span style="color:red">No pricebook found.</span></cfoutput>
							<cfelseif oppPricebook.pricebookExpired eq true>
								<cfoutput><span style="color:red">phr_PricebookExpired</span></cfoutput>
							<cfelse>
								<cfif not arguments.readOnly>
									<cfoutput>
									<tr>
										<td>
											<div class="form-group">
												<cfif not arguments.readOnly>
													<div class="col-xs-6 col-sm-12">
														<input class="btn btn-primary" type="button" value="phr_opp_addProduct" id="frmAddBtn" onClick="javascript:populateProductGroup(#arguments.opportunityID#);jQuery(this).hide();">
													</div>
												</cfif>
												<div class="col-xs-6 col-sm-12" id="productGroupRow1" style="display:none">
													<select class="form-control" id="frmProductGroup" onchange="populateProducts(this.value)">
													</select>
												</div>
												<div class="col-xs-6 col-sm-12" id="productRow1" style="display:none">
													<select class="form-control" id="frmProduct" onchange="populateProductDetails(this.value)">
													</select>
												</div>
											</div>
										</td>
										<td id="qtytd">											<!--- 2014-12-08 PPB P-ABS001 removed style="#useStyleHide#" --->
											<div class="form-group">
												<div class="col-xs-6 col-sm-12" id="qtyDiv" style="display:none">
													<input class="form-control" type="text" id="frmProductQty" name="frmProductQty" size="5">
												</div>
											</div>
										</td>
										<cfif showSeats>
											<td id="blanktd">										<!--- 2014-12-08 PPB P-ABS001 removed style="#useStyleHide#" --->
												<div id="productSeatsDiv">&nbsp;</div>
											</td>
										</cfif>
										<cfif request.relayCurrentUser.isInternal OR useOppProductTotals>
											<td id="priceTD" style="#useStyleHide#">&nbsp;</td>
										</cfif>
										<td>
											<div class="form-group">
												<div class="col-xs-6 col-sm-12">
													<CF_INPUT class="btn btn-primary" type="button" disabled="true" value="phr_opp_AddProduct" id="frmAddProductBtn" onClick="javascript:addOppProduct(#arguments.opportunityID#,frmProduct.value,frmProductQty.value)">
												</div>
											</div>
										</td>
									</tr>
									</cfoutput>
								</cfif>
							</cfif>
					<cfoutput>
							</tbody>
						</table>
					</cfoutput>
				</cf_translate>

		</cfsavecontent>

		<cfreturn oppProductsDisplay_html>
	</cffunction>


	<cffunction name="getOppProductGroups" access="public" returntype="query">
		<cfargument name="organisationID" 	type="numeric" required="false">
		<cfargument name="countryID" type="numeric" required="false">
		<cfargument name="currency" type="string" required="false">
		<cfargument name="opportunityID" type="numeric" required="true">

		<cfset var productGroupQry = application.com.opportunity.getOppProductGroups(argumentCollection=arguments)>
		<cfset var getProductGroupQryForSelect = "">

		<cfquery name="getProductGroupQryForSelect" dbtype="query">
			select productGroupId as [value], description as display from productGroupQry
		</cfquery>

		<cfreturn getProductGroupQryForSelect>

	</cffunction>

	<cffunction name="getOppPricebookProducts" access="public" returntype="query">
		<cfargument name="pricebookID" type="numeric" required="true">
		<cfargument name="productGroupID" type="numeric" required="false">
		<cfargument name="useFormat" type="string" default=""><!--- NYB 28-01-2011 P-NET002 --->

		<cfset var productQry = "">
		<cfset var getProductQryForSelect = "">
		<cfset var displayField = "description"><!--- NYB 28-01-2011 P-NET002 --->
		<cfset var orderby = "description">
		<cfset var productTitlePhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "product", phraseTextID = "title",baseTableAlias="p") >

		<cfset orderBy = application.com.settings.getSetting("leadManager.formatting.opportunityEdit.oppProductOrder")>
		<!--- START: NYB 28-01-2011 P-NET002 - added: --->
		<cfif useFormat neq "">
			<cfset displayField = application.com.settings.getSetting("leadManager.formatting.opportunityEdit.#useFormat#")>
			<cfset displayField = Replace(Replace(displayField,"]]","##","all"),"[[","##","all")>
			<cfset displayField = application.com.dbTools.ConvertMaskToSqlEntry(MaskString=displayField)>
		</cfif>

		<!--- dirty validation --->
		<cftry>
			<cfquery name="getProductQryForSelect" datasource="#application.siteDataSource#">
				select productId as [value], #preservesinglequotes(displayField)# as display from
				(
					select p.productID,p.sku,
					#preservesinglequotes(productTitlePhraseQuerySnippets.select)# as description
					from product p with (noLock)
					#preservesinglequotes(productTitlePhraseQuerySnippets.join)#
					inner join pricebookEntry pbe with (noLock) on p.productID = pbe.productID
					inner join pricebook pb with (noLock) on pb.pricebookID = pbe.pricebookID
					inner join productgroup pg with (noLock) on pg.productgroupid = p.productgroupid
					where p.deleted != 1
					and p.active=1
					and pb.pricebookID = #arguments.pricebookID#
					and p.productGroupID = #arguments.productGroupID#
					and isNull(pbe.active,1) = 1			<!--- 2012/08/22 NAS 430196 Deal Reg Form does not ignore InActive products --->
					and pbe.deleted != 1					<!--- 2012/08/31 NAS 430196 Deal Reg Form does not ignore InActive products --->
				) as productQry
				order by <cf_queryObjectName value="#orderBy#">
			</cfquery>
			<cfcatch type="any">
		<!--- END: NYB 28-01-2011 P-NET002 - added: --->
				<cfset productQry = application.com.opportunity.getOppPricebookProducts(argumentCollection=arguments)>
				<cfquery name="getProductQryForSelect" dbtype="query">
					select productId as [value], description as display from productQry
				</cfquery>
		<!--- START: NYB 28-01-2011 P-NET002 - added: --->
			</cfcatch>
		</cftry>
		<!--- END: NYB 28-01-2011 P-NET002 - added: --->

		<cfreturn getProductQryForSelect>
	</cffunction>

	<!---
	2016-04-15 AHL
	Getting all the products from the product group
	--->
	<cffunction name="getOppAllProducts" access="remote" returntype="struct" output="false" >
			<cfargument name="oppID" type="numeric" <!---required="true"---> default="1"/>

			<cfset var result = {isOk='true',productList = structNew()}/>
			<cfset var productQry = "">
			<cfset var productGroupTitlePhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "productGroup", phraseTextID = "title",baseTableAlias="pg")/>
			<cfset var productTitlePhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "product", phraseTextID = "title",baseTableAlias="p") />
			<cfset var oppPricebook = application.com.opportunity.getOppPricebook(opportunityID=arguments.oppID)>

			<cfquery name="productQry" dataSource="#application.siteDataSource#">
				SELECT
					p.productID,
					p.sku,
					#preservesinglequotes(productTitlePhraseQuerySnippets.select)# AS description
					,pg.productGroupID
					,#preserveSingleQuotes(productGroupTitlePhraseQuerySnippets.select)# AS pgDescription
					FROM product p WITH (noLock)
					#preservesinglequotes(productTitlePhraseQuerySnippets.join)#
					INNER JOIN pricebookEntry pbe WITH (noLock) ON p.productID = pbe.productID
					INNER JOIN pricebook pb WITH (noLock) ON pb.pricebookID = pbe.pricebookID
					INNER JOIN productgroup pg WITH (noLock) ON pg.productgroupid = p.productgroupid
					LEFT JOIN opportunityProduct op  WITH (noLock) ON op.ProductORGroupID = p.ProductID AND op.ProductORGroup = 'P' AND op.OpportunityID = <cfqueryparam cfsqltype="cf_sql_numeric" value='#arguments.oppID#' />
					#preserveSingleQuotes(productGroupTitlePhraseQuerySnippets.join)#
					WHERE p.deleted != 1
					AND p.active=1
					AND pb.pricebookID = <cfqueryparam cfsqltype="cf_sql_numeric" value='#oppPricebook.pricebookID#' />
					AND ISNULL(pbe.active,1) = 1
					AND pbe.deleted != 1
					AND op.opportunityID IS NULL
					ORDER BY
						#preserveSingleQuotes(productGroupTitlePhraseQuerySnippets.select)#,
						#preservesinglequotes(productTitlePhraseQuerySnippets.select)#
			</cfquery>

			<cfloop query='productQry'>
				<cfif !StructKeyExists(result.productList,productQry.productGroupID)>
					<cfset result.productList[productQry.productGroupID] = {
							group = productQry.pgDescription,
							id = productQry.productGroupID,
							productList = ArrayNew(1)}/>
				</cfif>
				<cfset ArrayAppend(
									result.productList[productQry.productGroupID].productList,
									{
										productID = productQry.productID,
										sku = productQry.sku,
										description = productQry.description
									}
								)/>
			</cfloop>

			<cfreturn result/>

	</cffunction>

	<cffunction name="getProductSeats" access="public" returnType="struct">
		<cfargument name="productID" type="numeric" required="true">

		<cfset var result = structNew()>
		<cfset var getProductSeats = "">

		<!--- 2012-03-16 PPB added NULLIF(ProductType,0) cos Netgear had 0 against lots of products and I'm not sure where they came from and it causes a crash --->
		<cfquery name="getProductSeats" datasource="#application.siteDataSource#">
			select minSeats, maxSeats, NULLIF(ProductType,'0') AS ProductType from product where productID=#arguments.productID#
		</cfquery>

		<cfset result.minSeats = getProductSeats.minSeats>
		<cfset result.maxSeats = getProductSeats.maxSeats>
		<cfset result.productType = getProductSeats.productType>

		<cfreturn result>
	</cffunction>

	<cffunction name="getOppProbability" access="remote" returntype="query">
		<cfargument name="StageID" required="true">
		<cfset var result = "">
		<cfquery name="result" datasource="#application.siteDataSource#">
			select #application.com.opportunity.getOppProbability(arguments.StageID)# as probability
		</cfquery>
		<cfreturn result>
	</cffunction>

</cfcomponent>
