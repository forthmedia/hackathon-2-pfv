<!--- �Relayware. All Rights Reserved 2014 --->
<!---
Author:			Alex Connell
Date created:	2007-04-25

	This provides the extended mxAjax functionality for Claims

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2009/04/20			NJH			Bug Fix All Sites Issue 2033 - changed query in getCompanyAccountAndBudgetInfo - added budgetPeriodAllocationID to subquery to return only single row.
2009-10-07			AJC			Renamed BudgetHolder (table, ID columns, phrases) to BudgetGroup
1009-10-12			AJC			Created function to return activities in json format
2011/04/08 			PPB 		LID6105 setting fundManager.orgAccountManagerTextID can now be a comma delimited list
2011/04/18			PPB			LEX052: moved several functions to relayFundmanager.cfc (we want to deprecate this cfc)
2016/10/21			atrunov		Case https://relayware.atlassian.net/browse/SSPS-49, integrated User Group Rule for fund account into existing functionality
--->


<cfcomponent extends="relay.com.mxajax">
<!--- 
	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	             Returns the budgets.            
	 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name="getSelectBudgets" returntype="array" hint="Returns a set of budgets for a passed BudgetGroupID">
		<cfargument name="BudgetGroupID" type="string" default="0">
		
		<cfset var budgets = "">
		
		<cfif not isnumeric(BudgetGroupID)>
			<cfset BudgetGroupID = 0>
		</cfif>

 		<cfset budgets = application.com.relayFundManager.getBudgets(BudgetGroupID=arguments.BudgetGroupID)>
		
		<cfset budgetsArray = ArrayNew(1)>
		<cf_translate phrases="phr_fund_PleaseSelectABudget"/>

		<cfif budgets.recordCount>
			<cfset ArrayAppend(budgetsArray, "0,#phr_fund_PleaseSelectABudget#")>
			<cfloop query="budgets">
				<cfset ArrayAppend(budgetsArray, "#budgetID#,#budget#")>
			</cfloop>
		<cfelse>
			<cfset ArrayAppend(budgetsArray, " , ")>
		</cfif>
	
		<cfreturn budgetsArray> 
	</cffunction>
 --->	
<!--- 	
	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	             Returns the budget periods available.            
	 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name="getSelectBudgetPeriods" access="remote" returntype="array" hint="Returns a set of available budget periods for a passed budgetID">
		<cfargument name="budgetID" type="string" default="0">
		
		<cfset var qry_get_SelectBudgetPeriods = "">
		
		<cfif not isnumeric(budgetID)>
			<cfset budgetID = 0>
		</cfif>
		
		<cfset budgetPeriodsArray = ArrayNew(1)>
		
		<cf_translate phrases="phr_fund_AllPeriodsHaveFundsAllocated"/>
	
		<cfif budgetID neq 0>
			<cfquery name="getExistingPeriodsForBudget" datasource="#application.siteDataSource#">
				select budgetPeriodID from budgetPeriodAllocation where 
					budgetID = #arguments.budgetID#
			</cfquery>
		
			<cfif getExistingPeriodsForBudget.recordCount>
				<CFQUERY NAME="qry_get_SelectBudgetPeriods" DATASOURCE="#application.sitedatasource#">
					SELECT budgetPeriodID, description from budgetPeriod
					WHERE budgetPeriodID not in (#valueList(getExistingPeriodsForBudget.budgetPeriodID)#)
				</CFQUERY>
			<cfelse>
				<cfset qry_get_SelectBudgetPeriods = application.com.relayFundManager.getBudgetPeriod()>
			</cfif>
			
			<cfif qry_get_SelectBudgetPeriods.recordCount>
				<cfloop query="qry_get_SelectBudgetPeriods">
					<cfset ArrayAppend(budgetPeriodsArray, "#budgetPeriodID#,#description#")>
				</cfloop>
			<cfelse>
				<cfset ArrayAppend(budgetPeriodsArray, " , #phr_fund_AllPeriodsHaveFundsAllocated#")>
			</cfif>
		<cfelse>
			<cfset ArrayAppend(budgetPeriodsArray, " , ")>		
		</cfif>
		
		<cfreturn budgetPeriodsArray>
	</cffunction>
 --->	
	
	
	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	             Returns the organisations in a country.            
	 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name="getSelectOrganisations" access="remote" returntype="array" hint="Returns organisations within a country">
		<cfargument name="countryID" type="string" default="0">
		
		<cfset var qry_get_SelectOrganisations = "">
		
		<cfif not isnumeric(countryID)>
			<cfset countryID = 0>
		</cfif>
		
		<cfset organisationsArray = ArrayNew(1)>
	
		<CFQUERY NAME="qry_get_SelectOrganisations" DATASOURCE="#application.sitedatasource#">
			SELECT organisationID, organisationName from organisation
			WHERE countryID =  <cf_queryparam value="#countryID#" CFSQLTYPE="CF_SQL_INTEGER" >  and organisationID not in
				(select organisationID from fundCompanyAccount)
		</CFQUERY>
			
		<cfif qry_get_SelectOrganisations.recordCount>
			<cfloop query="qry_get_SelectOrganisations">
				<cfset ArrayAppend(organisationsArray, "#organisationID#,#organisationName#")>
			</cfloop>
		<cfelse>
			<cfset ArrayAppend(organisationsArray, " , ")>		
		</cfif>
	
		<cfreturn organisationsArray> 
	</cffunction>
	
	
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	             Returns the budget remaining for a request activity            
	 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name="getSelectBudgetPeriodAllocations" access="remote" returntype="array" hint="Returns the budget remaining for a request activity">
		<cfargument name="budgetPeriodAllocationID" type="string" default="0">
		
		<cfset var qry_get_SelectBudgetAllocations = "">
		
		<cfif not isnumeric(budgetPeriodAllocationID)>
			<cfset budgetPeriodAllocationID = 0>
		</cfif>
		
		<cfset budgetAllocationsArray = ArrayNew(1)>
		<cf_translate phrases="phr_fund_NoAmountHasBeenAllocated"/>
	
		<CFQUERY NAME="qry_get_SelectBudgetAllocations" DATASOURCE="#application.sitedatasource#">
			SELECT amount - (select isNull(sum(amount),0) from fundRequestActivityApproval where budgetPeriodAllocationID = bpa.budgetPeriodAllocationID) as budgetRemaining
			from budgetPeriodAllocation bpa
			WHERE budgetPeriodAllocationID =  <cf_queryparam value="#budgetPeriodAllocationID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</CFQUERY>
			
		<cfif qry_get_SelectBudgetAllocations.recordCount>
			<cfloop query="qry_get_SelectBudgetAllocations">
				<cfset ArrayAppend(budgetAllocationsArray, "#budgetRemaining#")>
			</cfloop>
		<cfelse>
			<cfset ArrayAppend(budgetAllocationsArray, "#phr_fund_NoAmountHasBeenAllocated#")>		
		</cfif>
	
		<cfreturn budgetAllocationsArray> 
	</cffunction>
	
	
<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	             Returns the organisations in a country.            
	 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name="getSelectBudgetGroups" access="remote" returntype="array" hint="Returns available BudgetGroups for an account">
		<cfargument name="organisationID" type="string" default="0">
		<cfargument name="fundManagerID" type="string" default="0">
		<cfargument name="budgetTypeID" type="string" default="0">
		
		<cfset var qry_get_SelectBudgetGroups = "">
		<cfset var fundManagersQry = "">

		<cfset fundManagersQry = application.com.relayFundManager.getAccountsByFundManagerID()>
		
		<cfif not isnumeric(organisationID)>
			<cfset organisationID = 0>
		</cfif>
		
		<cfset BudgetGroupsArray = ArrayNew(1)>
		
		<cf_translate phrases="phr_fund_noBudgetGroupAvailable"/>
		<cfset noBudgetPhrase = phr_fund_noBudgetGroupAvailable>
		
		<!--- NJH 2008/06/24 Bug Fix Sony 518 - only bring back Budget Groups for an org's account that have fund managers attached to them --->
		<cfquery name="qry_get_SelectBudgetGroups" datasource="#application.siteDataSource#">
			select distinct bh.description, bh.BudgetGroupID from BudgetGroup bh inner join
				budget b on bh.BudgetGroupID = b.BudgetGroupID inner join 
				fundAccountBudgetAccess faba on faba.budgetID = b.budgetID inner join
				fundCompanyAccount fca on faba.budgetID = fca.budgetID and faba.accountID = fca.accountID
			where accountClosed=0
				and organisationID =  <cf_queryparam value="#organisationID#" CFSQLTYPE="CF_SQL_INTEGER" >
				and fca.accountID in (<cfqueryparam cfsqltype="cf_sql_integer" value="#valueList(fundManagersQry.data)#" list="true">)
		</cfquery>
		
		<cf_translate phrases="phr_fund_PleaseSelectABudgetGroup"/>

		<cfif qry_get_SelectBudgetGroups.recordCount>
			<cfset ArrayAppend(BudgetGroupsArray, "#phr_fund_PleaseSelectABudgetGroup#")>
			<cfloop query="qry_get_SelectBudgetGroups">
				<cfset ArrayAppend(BudgetGroupsArray, "#BudgetGroupID#, #description#")>
			</cfloop>
		<cfelse>
			<cfset ArrayAppend(BudgetGroupsArray, " 0, #noBudgetPhrase#")>		
		</cfif>
	
		<cfreturn BudgetGroupsArray> 
	</cffunction>
	
<!--- 	
	<cffunction name="getBudgetAuthorisation" access="remote" returntype="array" hint="Returns whether a budget needs authorisation">
		<cfargument name="budgetID" type="string" default="0">
		<cfargument name="authorisedBudgetIDs" required="yes" type="string">
		
		<cfset var doesBudgetNeedAuthorisation = "">
		
		<cfif not isnumeric(budgetID)>
			<cfset budgetID = 0>
		</cfif>
		
		<cfset budgetAuthorisationArray = ArrayNew(1)>
		
		<cfif budgetID neq 0>
			<cfquery name="doesBudgetNeedAuthorisation" datasource="#application.siteDataSource#">
				select entityTypeID from budget where budgetID = #arguments.budgetID#
			</cfquery>
			
			<!--- if it's not in list of budget types to authorise, than authorise it  --->
			<cfif not listContains(arguments.authorisedBudgetIDs,doesBudgetNeedAuthorisation.entityTypeID)>
				<cfset ArrayAppend(budgetAuthorisationArray,"1")>
			<cfelse>
				<cfset ArrayAppend(budgetAuthorisationArray,"0")>
			</cfif>
		<cfelse>
			<cfset ArrayAppend(budgetAuthorisationArray,"0")>
		</cfif>
		
		<cfreturn budgetAuthorisationArray>
	</cffunction>
 --->	
<!--- 	
	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	             Returns a structure with available budget periods            
	 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name="getBudgetPeriodAndAuthorisation" access="remote" returntype="struct" hint="Returns available budget periods and whether a budget needs authorisation">
		<cfargument name="budgetID" required="yes" type="string">
		<cfargument name="authorisedBudgetIDs" required="yes" type="string">
		
		<cfset retData = StructNew()>
		<cfset retData.periods = getSelectBudgetPeriods(arguments.budgetID)>
		<cfset retData.authorisation = getBudgetAuthorisation(arguments.budgetID,arguments.authorisedBudgetIDs)>
		<cfreturn retData>
	</cffunction>
 --->
	
	
	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	             Returns the account for a BudgetGroup and a fund manager.            
	 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	<cffunction name="getCompanyAccountAndBudgetInfo" access="remote" returntype="array" hint="Returns the account,the budget type and the amount allocated if it's an org level budget for a Budget Group and a fund manager combination">
		<cfargument name="BudgetGroupID" type="string" default="0">
		<cfargument name="fundManagerID" type="string" default="0">
		<cfargument name="fundsBudgetPeriodType" type="string" default="1">
		
		<cfset var qry_get_SelectCompanyAccounts = "">
		
		<cfif not isnumeric(BudgetGroupID)>
			<cfset BudgetGroupID = 0>
		</cfif>
		
		<cfif not isnumeric(fundManagerID)>
			<cfset fundManagerID = 0>
		</cfif>
		
		<cfset companyAccountsArray = ArrayNew(1)>
		<cf_translate phrases="phr_fund_NoAccountExists"/>
		
		<cfif fundManagerID neq 0>
			<cfset fundManagersQry = application.com.relayFundManager.getAccountsByFundManagerID(fundManagerID=arguments.fundManagerID)>
		</cfif>
	
		<!--- if it's an org level budget, then we want to return the budget available so that when a request is submitted,
			a check is made to ensure that the requested amount doesn't exceed the available budget
			
			NJH 2009/04/20 Bug Fix All Sites Issue 2033 - add budgetPeriodAllocationID to sub-query to return only the single row.
		 --->
		<cfquery name="qry_get_SelectCompanyAccounts" datasource="#application.sitedatasource#" maxrows=1>
			select fca.accountID, o.organisationName, b.entityTypeID, 
				case when b.entityTypeID=2 then 
					(select amount - sum_Approved from budgetPeriodAllocation bpa1 inner join budgetPeriod bp1
						on bpa1.budgetPeriodID = bp1.budgetPeriodID inner join vFundRequestList frl
						on frl.budgetPeriodID = bpa1.budgetPeriodID and frl.accountID = fca.accountID and frl.budgetPeriodAllocationID = bpa1.budgetPeriodAllocationID
					where datediff(day,bp1.startDate,GetDate()) >=0 and datediff(day,bp1.endDate,GetDate()) <=0
						and bpa1.authorised=1 
						and bp1.periodTypeID =  <cf_queryparam value="#arguments.fundsBudgetPeriodType#" CFSQLTYPE="CF_SQL_INTEGER" > 
						and bpa1.budgetID=b.budgetID
						and bpa1.budgetPeriodAllocationID = bpa.budgetPeriodAllocationID) else null end as OrgBudgetAllocated
			from fundCompanyAccount fca inner join organisation o
				on fca.organisationID = o.organisationID inner join fundAccountBudgetAccess faba
				on faba.accountID = fca.accountID and faba.budgetID = fca.budgetID inner join budget b
				on faba.budgetID = b.budgetID and b.BudgetGroupID =  <cf_queryparam value="#arguments.BudgetGroupID#" CFSQLTYPE="CF_SQL_INTEGER" > 
				inner join budgetPeriodAllocation bpa on b.budgetID = bpa.budgetID inner join budgetPeriod bp
				on bp.budgetPeriodID = bpa.budgetPeriodID and datediff(day, bp.startDate, GetDate()) >=0 and datediff(day,bp.enddate,GetDate()) <=0 and bp.periodTypeID =  <cf_queryparam value="#arguments.fundsBudgetPeriodType#" CFSQLTYPE="CF_SQL_INTEGER" >  and bpa.authorised = 1
			where fca.accountClosed = 0
				<cfif isDefined("fundManagersQry") and fundManagersQry.recordCount gt 0>
				and fca.accountID  in ( <cf_queryparam value="#valueList(fundManagersQry.data)#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
				<!--- we want to get accounts for a fund manager, but the fund manager doesn't exist and so we should return no records --->
				<cfelseif isDefined("fundManagersQry") and fundManagersQry.recordCount eq 0>
				and 1=0
				</cfif>
				<!--- if they have an org level budget, display that else the country --->
				and entityTypeID = 
						(select min(entityTypeID) from budget b1 inner join budgetPeriodAllocation bpa1
							on b1.budgetID = bpa1.budgetID inner join budgetPeriod bp1
							on bp1.budgetPeriodID = bpa1.budgetPeriodID and datediff(day,bp1.startDate,GetDate()) >=0 and datediff(day,bp1.endDate,GetDate()) <=0 and bp1.periodTypeID =  <cf_queryparam value="#arguments.fundsBudgetPeriodType#" CFSQLTYPE="CF_SQL_INTEGER" >  and bpa1.authorised=1 inner join fundCompanyAccount fca1
							on fca1.budgetID = b1.budgetID and fca1.organisationID = o.organisationID
							where
							(entityTypeId = 2 and entityid = o.organisationID)
								or 
							(entityTypeId = 3 and entityid = o.countryID)
								or
							(entityTypeID=4 and entityID in (
								select countryGroupID from countryGroup where countryMemberID = o.countryID))
							and b1.BudgetGroupID =  <cf_queryparam value="#arguments.BudgetGroupID#" CFSQLTYPE="CF_SQL_INTEGER" > 
						)
		</cfquery>
			
		<cfif qry_get_SelectCompanyAccounts.recordCount>
			<cfloop query="qry_get_SelectCompanyAccounts">
				<cfset ArrayAppend(companyAccountsArray, "#accountID#")>
				<cfset ArrayAppend(companyAccountsArray, "#organisationName#")>
				<cfset ArrayAppend(companyAccountsArray, "#entityTypeID#")>
				<cfset ArrayAppend(companyAccountsArray, "#orgBudgetAllocated#")>
			</cfloop>
		<cfelse>
			<cfset ArrayAppend(companyAccountsArray, "0")>
			<cfset ArrayAppend(companyAccountsArray, "--------------------------------------------")>
			<cfset ArrayAppend(companyAccountsArray, "")>
			<cfset ArrayAppend(companyAccountsArray, "")>
		</cfif>
	
		<cfreturn companyAccountsArray> 
	</cffunction>
	
	
	<!--- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	             Returns the account for a BudgetGroup and an organisationID.
				 This is called when an internal user creates a fund activity
	 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ --->
	
	<cffunction name="getSelectFundAccount" access="remote" returntype="array" hint="Returns a fund account for an organisationID and a Budget Group.">
		<cfargument name="organisationID" required="yes" type="string">
		<cfargument name="BudgetGroupID" required="yes" type="string">
		<cfargument name="fundsBudgetPeriodType" type="string" default="1">
		
		<cfset var getFundAccountFromOrgAndBudgetGroup = "">

		<cfif not isnumeric(organisationID)>
			<cfset organisationID = 0>
		</cfif>
		
		<cfif not isnumeric(BudgetGroupID)>
			<cfset BudgetGroupID = 0>
		</cfif>
		
		<cfset fundAccountArray = ArrayNew(1)>
		
		<cfif organisationID neq 0 and BudgetGroupID neq 0>
			<cftry>
			<cfquery name="getFundAccountFromOrgAndBudgetGroup" datasource="#application.siteDataSource#">
				select fca.accountID,b.entityTypeID,
					case when b.entityTypeID=2 then 
					(select amount - sum_Approved from budgetPeriodAllocation bpa inner join budgetPeriod bp 
						on bpa.budgetPeriodID = bp.budgetPeriodID inner join vFundRequestList frl
						on frl.budgetPeriodID = bpa.budgetPeriodID and frl.accountID = fca.accountID
					where datediff(day,bp.startDate,GetDate()) >=0 and datediff(day,bp.endDate,GetDate()) <=0
						and bpa.authorised=1 
						and bp.periodTypeID =  <cf_queryparam value="#arguments.fundsBudgetPeriodType#" CFSQLTYPE="CF_SQL_INTEGER" > 
						and bpa.budgetID=b.budgetID) else null end as OrgBudgetAllocated 
				from fundCompanyAccount fca
					inner join fundAccountBudgetAccess faba on fca.accountID = faba.accountID and fca.budgetID = faba.budgetID 
					inner join budget b on faba.budgetID = b.budgetID
					inner join organisation o on fca.organisationID = o.organisationID
				inner join budgetPeriodAllocation bpa on b.budgetID = bpa.budgetID inner join budgetPeriod bp
					on bp.budgetPeriodID = bpa.budgetPeriodID and datediff(day, bp.startDate, GetDate()) >=0 and 	
					datediff(day,bp.enddate,GetDate()) <=0 and bp.periodTypeID =  <cf_queryparam value="#arguments.fundsBudgetPeriodType#" CFSQLTYPE="CF_SQL_INTEGER" >  
					and bpa.authorised = 1
				where o.organisationID =  <cf_queryparam value="#arguments.organisationID#" CFSQLTYPE="CF_SQL_INTEGER" >  
					and b.BudgetGroupID =  <cf_queryparam value="#arguments.BudgetGroupID#" CFSQLTYPE="CF_SQL_INTEGER" > 
					and entityTypeID = 
						(select min(entityTypeID) from budget b1 inner join budgetPeriodAllocation bpa1
							on b1.budgetID = bpa1.budgetID inner join budgetPeriod bp1
							on bp1.budgetPeriodID = bpa1.budgetPeriodID and datediff(day,bp1.startDate,GetDate()) >=0 and datediff(day,bp1.endDate,GetDate()) <=0 and bp1.periodTypeID =  <cf_queryparam value="#arguments.fundsBudgetPeriodType#" CFSQLTYPE="CF_SQL_INTEGER" >  and bpa1.authorised=1 inner join fundCompanyAccount fca1
							on fca1.budgetID = b1.budgetID and fca1.organisationID = o.organisationID
							where
							(entityTypeId = 2 and entityid = o.organisationID)
								or 
							(entityTypeId = 3 and entityid = o.countryID)
								or
							(entityTypeID=4 and entityID in (
								select countryGroupID from countryGroup where countryMemberID = o.countryID))
							and b1.BudgetGroupID =  <cf_queryparam value="#arguments.BudgetGroupID#" CFSQLTYPE="CF_SQL_INTEGER" > 
						)
			</cfquery>
			<cfcatch type="any">
				<cfoutput>
					<script language="javascript">
						alert("Error");					
					</script>
					<cfdump var="#arguments#">

					<pre>



					QUERY:
					
					select fca.accountID,b.entityTypeID,
						case when b.entityTypeID=2 then 
						(select amount - sum_Approved from budgetPeriodAllocation bpa inner join budgetPeriod bp 
							on bpa.budgetPeriodID = bp.budgetPeriodID inner join vFundRequestList frl
							on frl.budgetPeriodID = bpa.budgetPeriodID and frl.accountID = fca.accountID
						where datediff(day,bp.startDate,GetDate()) >=0 and datediff(day,bp.endDate,GetDate()) <=0
							and bpa.authorised=1 
							and bp.periodTypeID=#htmleditformat(arguments.fundsBudgetPeriodType)#
							and bpa.budgetID=b.budgetID) else null end as OrgBudgetAllocated 
					from fundCompanyAccount fca
						inner join fundAccountBudgetAccess faba on fca.accountID = faba.accountID and fca.budgetID = faba.budgetID 
						inner join budget b on faba.budgetID = b.budgetID
						inner join organisation o on fca.organisationID = o.organisationID
					inner join budgetPeriodAllocation bpa on b.budgetID = bpa.budgetID inner join budgetPeriod bp
						on bp.budgetPeriodID = bpa.budgetPeriodID and datediff(day, bp.startDate, GetDate()) >=0 and 	
						datediff(day,bp.enddate,GetDate()) <=0 and bp.periodTypeID =#htmleditformat(arguments.fundsBudgetPeriodType)# 
						and bpa.authorised = 1
					where o.organisationID = #htmleditformat(arguments.organisationID)# 
						and b.BudgetGroupID = #htmleditformat(arguments.BudgetGroupID)#
						and entityTypeID = 
							(select min(entityTypeID) from budget b1 inner join budgetPeriodAllocation bpa1
								on b1.budgetID = bpa1.budgetID inner join budgetPeriod bp1
								on bp1.budgetPeriodID = bpa1.budgetPeriodID and datediff(day,bp1.startDate,GetDate()) >=0 and datediff(day,bp1.endDate,GetDate()) <=0 and bp1.periodTypeID=#htmleditformat(arguments.fundsBudgetPeriodType)# and bpa1.authorised=1 inner join fundCompanyAccount fca1
								on fca1.budgetID = b1.budgetID and fca1.organisationID = o.organisationID
								where
								(entityTypeId = 2 and entityid = o.organisationID)
									or 
								(entityTypeId = 3 and entityid = o.countryID)
									or
								(entityTypeID=4 and entityID in (
									select countryGroupID from countryGroup where countryMemberID = o.countryID))
								and b1.BudgetGroupID = #htmleditformat(arguments.BudgetGroupID)#
							)
					
					</pre>					
				</cfoutput>			
			</cfcatch>
			</cftry>
			<!--- if it's not in list of budget types to authorise, than authorise it  --->
			<cfif getFundAccountFromOrgAndBudgetGroup.recordCount gt 0>
				<cfset ArrayAppend(fundAccountArray,"#getFundAccountFromOrgAndBudgetGroup.accountID#")>
				<cfset ArrayAppend(fundAccountArray,"#getFundAccountFromOrgAndBudgetGroup.entityTypeID#")>
				<cfset ArrayAppend(fundAccountArray,"#getFundAccountFromOrgAndBudgetGroup.OrgBudgetAllocated#")>
			<cfelse>
				<cfset ArrayAppend(fundAccountArray,"0")>
				<cfset ArrayAppend(fundAccountArray,"0")>
				<cfset ArrayAppend(fundAccountArray,"0")>
			</cfif>
		<cfelse>
			<cfset ArrayAppend(fundAccountArray,"0")>
			<cfset ArrayAppend(fundAccountArray,"0")>
			<cfset ArrayAppend(fundAccountArray,"0")>
		</cfif>
		
		<cfreturn fundAccountArray>
	</cffunction>

	<!--- START: 1009-10-12 - AJC - Created function to return activities in json format --->
	<cffunction name="getFundActivityApprovals" access="remote" hint="Returns the fund activity table for the report expander">
		<cfargument name="fundRequestID" type="numeric" required="true">
		<cfargument name="orgAccountManagerTextID" type="string" required="false" default="">
		
		<cfscript>
			//var addModuleUrl = ""; doesn't appear to be used
			var getFundRequestActivities = "";
			var activityItemsDisplay_html = "";
			var result = "";
			// var access = application.com.rights.doesUserHaveRightsForEntity(entityType="trngPersonCertification",entityID=arguments.personCertificationID,permission="view");
		
			// if (not access) {
			//	application.com.globalFunctions.abortIt();
			//}
		</cfscript>
		
		<!--- 2011/04/08 PPB LID6105 setting fundManager.orgAccountManagerTextID can now be a comma delimited list --->
		<cfquery name="getFundRequestActivities" datasource="#application.siteDataSource#">
			select distinct fra.*
			from vFundRequestActivities fra
			where fra.fundRequestID=#fundRequestID#
			and 
			(
				fra.fundRequestActivityID IN
				(
					select fra2.fundRequestActivityID
						from fundRequest fr inner join
						fundRequestActivity fra2 on fr.fundrequestID = fra2.fundrequestID inner join
						fundCompanyAccount fca on fr.accountID = fca.accountID inner join
						budget b on fca.budgetID = b.budgetID inner join
						budgetgroup bg on b.budgetgroupID = bg.budgetgroupID inner join
						budgetgroupApprover bga on bg.budgetgroupID = bga.budgetgroupID inner join
						organisation o on fca.organisationID = o.organisationID inner join
						countryGroup cg on o.countryID = cg.countryMemberID inner join
						fundApprover fa on cg.countrygroupID = fa.countryID and bga.fundapproverID = fa.fundapproverID
						inner join fundApprovalLevel fal on fa.fundApprovalLevelID = fal.fundApprovalLevelID and fal.approvalLevel = fra2.currentApprovalLevel
						inner join fundApprovalStatus fas on fra2.fundApprovalStatusID = fas.fundApprovalStatusID
						where fa.personID = #request.relaycurrentuser.personID#
						and (fas.fundStatusTextID != 'approved' or fas.fundStatusTextID is null)
				)
				or
				fra.fundRequestActivityID IN
				(
				
					select fra2.fundRequestActivityID
						from fundRequest fr inner join
						fundRequestActivity fra2 on fr.fundrequestID = fra2.fundrequestID inner join
						fundCompanyAccount fca on fr.accountID = fca.accountID inner join
						budget b on fca.budgetID = b.budgetID inner join
						budgetgroup bg on b.budgetgroupID = bg.budgetgroupID inner join
						organisation o on fca.organisationID = o.organisationID inner join
						fundApprovalStatus fas on fra2.fundApprovalStatusID = fas.fundApprovalStatusID
						inner join integerflagdata ifd on o.OrganisationID = ifd.entityID and ifd.data = #request.relaycurrentuser.personID#
						inner join Flag f ON ifd.flagid = f.flagid and f.flagtextID  IN ( <cf_queryparam value="#arguments.orgAccountManagerTextID#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )
						where bg.accountManagerApproval= 1 and
						fra2.currentApprovalLevel=1
						and (fas.fundStatusTextID != 'approved' or fas.fundStatusTextID is null)
				)
			)
			
			order by fundRequestActivityID desc
		</cfquery>

		<cfsavecontent variable="activityItemsDisplay_html">
			<cf_translate>
				<div id="fundActivityDataDiv">
				<table width="100%" class="withBorder" id="fundActivityDataTable">
					<cfif (getFundRequestActivities.recordCount gt 0)>
						<tr>
							<th valign="top">phr_fund_fundRequestActivityID</th>
							<th valign="top">phr_fund_requestType</th>
							<th valign="top">phr_fund_totalCost</th>
							<th valign="top">phr_fund_requestedAmount</th>
							<th valign="top">phr_fund_approvedAmount</th>
							<th valign="top">phr_fund_startDate</th>
							<th valign="top">phr_fund_endDate</th>
							<th valign="top">phr_fund_status</th>
							<th valign="top">phr_fund_approve</th>
						</tr>
						<cfoutput query="getFundRequestActivities">
							<tr>
								<td valign="top">#htmleditformat(fundRequestActivityID)#</td>
								<td valign="top">#htmleditformat(requestType)#</td>
								<td valign="top">#htmleditformat(totalCost)#</td>
								<td valign="top">#htmleditformat(requestedAmount)#</td>	
								<td valign="top">#htmleditformat(approvedAmount)#</td>
								<td valign="top">#htmleditformat(startDate)#</td>
								<td valign="top">#htmleditformat(endDate)#</td>
								<td valign="top">#htmleditformat(status)#</td>
								<td valign="top"><a href="fundRequestActivities.cfm?mode=edit&fundRequestActivityID=#fundRequestActivityID#">phr_fund_approve</td>	
							</tr>
						</cfoutput>
					<cfelse>
						<tr>
							<td>phr_fundNoActivitiesToApproval.</td>
						</tr>
					</cfif>
				</table>
				</div>
			</cf_translate>
		</cfsavecontent>
		
		<cfset result = structNew()>
		<cfset result.content = activityItemsDisplay_html>

		<cfreturn result>
	
	</cffunction>
	<!--- END: 1009-10-12 - AJC - Created function to return activities in json format --->
	
</cfcomponent>
