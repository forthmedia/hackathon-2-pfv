﻿<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		connectorWS.cfc
Author:			NJH
Date started:	18-06-2015

Description:	Connector Webservices.

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2015/06/18			NJH			Added function to delete connector mapping.
20156/07/27			NJH			Added a popover to the relatedRecords function to show the data for the given record
Possible enhancements:


 --->

<cfcomponent output="false">

	<cffunction name="getRelatedRecords" access="public" returnType="string" output="false" hint="Returns error records or dependent records, depending on the recordType passed through">
		<cfargument name="connectorType" type="string" required="true">
		<cfargument name="queueID" type="string" required="true">
		<cfargument name="recordType" type="string" required="true" hint="Dependent or Error">

		<cfset var result = "">
		<cfset var relatedRecordsFunction = application.getObject("connector.com.connector")["get#arguments.recordType#Records"]>
		<cfset var relatedRecords = relatedRecordsFunction(argumentCollection=arguments)>
		<cfset var thStyle = "">
		<cfset var columnList = "Name,Record_Type,RelaywareID,RemoteID,lastModified,queueStatus">
		<cfset var columnName = "">
		<cfset var responseID = "">

		<cfif arguments.recordType eq "error">
			<cfset columnList = "message,statusCode,field,value">
		</cfif>

		<cfif (arguments.recordType eq "error" and relatedRecords.errorResponseID[1] neq "")>
			<cfset responseID = relatedRecords.errorResponseID[1]>
		<cfelseif arguments.recordType neq "error">
			<cfset responseID = relatedRecords.connectorResponseID[1]>
		</cfif>

		<cfset var addActionColum = false>

		<cfsavecontent variable="local.bodyRows">
			<cfoutput>
				<cfloop query="relatedRecords">
					<tr>
						<cfloop list="#columnList#" index="columnName">
							<td>
								<cfif columnName eq "name" and relatedRecords["RelaywareID"][currentRow] neq "">
									<a href="" onclick="javascript:viewEntityDetails('#entity#',#relaywareID#);return false;" class="smallLink">#htmlEditFormat(name)#</a>
								<cfelseif columnName eq "lastModified">
									#lsDateFormat(relatedRecords[columnName][currentRow])# #lsTimeFormat(relatedRecords[columnName][currentRow])#
								<cfelse>
									#htmlEditFormat(relatedRecords[columnName][currentRow])#
								</cfif>
							</td>
						</cfloop>

						<cfset var relatedDashboardStatus = relatedRecords["dashboardStatus"][currentRow]>
						<cfset var relatedRecordType = relatedDashboardStatus eq "Failed" ? "error" : "dependent">
						<cfset var relatedQueueStatus = relatedRecords["queueStatus"][currentRow]>
						<cfset var relatedQueueId = relatedRecords["queueID"][currentRow]>
						<!--- NJH 2016/03/24 - BF-608 this really should only be for error records. For dependent records, we just want to get the record that we're dependent on. Put a try/catch around this so that if we error for whatever reason,
							we can still see the error row. --->
						<cfif responseID neq "" and arguments.recordType neq "Dependent">
							<cftry>
								<td class="viewDataTd">
									<cfset addActionColum = true>
									<cfset var args = {connectorResponseID=responseID}>
									<cfif relatedRecords.direction[1] eq "import">
										<cfset args.remoteID = relatedRecords.remoteID[currentRow]>
									<cfelse>
										<cfset args.relaywareID = relatedRecords.relaywareID[currentRow]>
									</cfif>

									<a href="##" class="recordDataPopover" data-toggle="popover" data-placement="auto" title="Data Details" data-content="#getPopoverContent(dataQuery= application.getObject('connector.com.connector').getDataForIDFromConnectorResponse(argumentCollection=args))#" data-trigger="hover">View Data</a>
								</td>

								<cfcatch>
									<cfset application.com.errorHandler.recordRelayError_Warning(type="Connector Dashboard",Severity="error",catch=cfcatch)>
								</cfcatch>
							</cftry>
						<cfelseif relatedQueueStatus eq "HasDependency" or relatedDashboardStatus eq "Failed">
							<td class="#relatedRecordType#">
								<a href="##" class="smalLink" onclick="javascript:showRelatedRecords(this,'#relatedQueueId#','#relatedRecordType#','#connectorType#');return false;">
									<img src="/images/icons/bullet_arrow_down.png">
								</a>
							</td>
							<cfset addActionColum = true>
						</cfif>
					</tr>
					<cfif arguments.recordType eq "Dependent" and (relatedQueueStatus eq "HasDependency" or relatedDashboardStatus eq "Failed")>
						#getRelatedRecords(arguments.connectorType, relatedQueueId, relatedRecordType)#
					</cfif>
				</cfloop>
			</cfoutput>
		</cfsavecontent>

		<cfsavecontent variable="local.headerRow">
			<cfoutput>
				<tr>
					<cfloop list="#columnList#" index="columnName">
						<th>phr_connector<cfif columnName eq "remoteID">_#arguments.connectorType#</cfif>_report_#columnName#</th>
					</cfloop>
					<cfif addActionColum><th></th></cfif>
				</tr>
			</cfoutput>
		</cfsavecontent>

		<cfsavecontent variable="result">
			<cfoutput>
				<tr id="relatedRecords_#arguments.queueID#" class="relatedRecords">
					<td colspan="10">
						<div id="relatedRecordsDiv_#arguments.queueID#" class="relatedRecordDiv">
							<table class="withBorder relatedRecordsTable">
								#local.headerRow#
								#local.bodyRows#
							</table>
						</div>
					</td>
				</tr>
			</cfoutput>
		</cfsavecontent>

		<cfreturn result>
	</cffunction>


	<cffunction name="getPopoverContent" access="private" returnType="string" hint="Returns the html to be put into the popover, given a query with the data in it">
		<cfargument name="dataQuery" type="query" required="true">

		<cfset var queryCol = "">
		<cfset var dataContent = "<div class='popoverContent'>">

		<cfloop list="#arguments.dataQuery.columnList#" index="queryCol">
			<cfset dataContent = dataContent&"<p><span>#queryCol#</span>:#arguments.dataQuery[queryCol][1]#</p>">
		</cfloop>
		<cfset dataContent = dataContent & "</div>">

		<cfreturn dataContent>
	</cffunction>


	<cffunction name="getColumnLookupValues" access="public" output="false" returnType="array" hint="Returns potential Relayware values that need to be or could be mapped">
		<cfargument name="connectorType" type="string" required="true">
		<cfargument name="column" type="string" required="true">
		<cfargument name="tablename" type="string" required="true">

		<!--- JIRA PROD2016-1292 - return datavalue/displayValue..to be consistent with rest of RW.  --->
		<cfset var qryGetValues = queryNew("dataValue,displayValue,mappedValue,ID")>
		<cfset var result = {}>
		<cfset var selectTablename="vFlagDef">
		<cfset var columnName = "flagTextID">
		<cfset var displayColumnName = "flag">

		<!--- <cfset var tableColumnMetaData = application.com.relayEntity.getTableFieldStructure(tablename=arguments.tablename,fieldname=arguments.column)> --->
		<cfset var tableColumnMetaData = application.com.relayEntity.getEntityFieldMetaData(tablename=arguments.tablename,column=arguments.column)>

		<cfif tableColumnMetaData.recordCount>
			<cfset var pickListSql = tableColumnMetaData.pickListValuesSql[1]>
			<cfset pickListSql = evaluate('"#pickListSql#"')> <!--- JIRA PROD2016-2171 in case we have valid values with CF variables in them --->

			<cfquery name="qryGetValues">
				select dataValue, displayValue, cvm.value_remote as mappedValue, isNull(cvm.ID,0) as ID
				from
					(#preserveSingleQuotes(pickListSql)#) v
						left join (connectorColumnValueMapping cvm inner join vconnectorMapping m on cvm.connectorMappingID = m.ID)
							on cvm.value_relayware = cast(v.dataValue as nvarchar(max)) and m.column_relayware = <cf_queryparam value="#arguments.column#" cfsqltype="cf_sql_varchar"> and m.object_relayware = <cf_queryparam value="#arguments.tablename#" cfsqltype="cf_sql_varchar"> and m.connectorType=<cf_queryparam value="#arguments.connectorType#" cfsqltype="cf_sql_varchar">
				where len(isNull(dataValue,'')) > 0
				order by displayValue
			</cfquery>
		</cfif>

		<cfreturn application.com.structureFunctions.QueryToArrayOfStructures(query=qryGetValues)>
	</cffunction>


	<cffunction name="getRemoteColumnLookupValues" access="public" output="false" returnType="array" hint="Returns potential Remote object values that need to be or could be mapped">
		<cfargument name="connectorType" type="string" required="true">
		<cfargument name="column" type="string" required="true" hint="The name of the remote object column">
		<cfargument name="object" type="string" required="true" hint="The name or Id of the remote object">

		<cfset var qryPickList = application.getObject("connector.com.connector").getPickListValuesForRemoteObjectColumn(argumentCollection=arguments)>

		<cfreturn application.com.structureFunctions.QueryToArrayOfStructures(query=qryPickList)>
	</cffunction>


	<cffunction name="getTableColumns" access="public" output="false" returnType="struct" hint="Returns the columns for a given Relayware table">
		<cfargument name="tablename" type="string" required="true">
		<cfargument name="dataType" type="string" required="false" hint="Filter the columns based on a given datatype">

		<cfset var fieldMetaData = application.com.relayEntity.getEntityFieldMetaData(argumentCollection=arguments)>

		<cfreturn application.com.structureFunctions.queryToStruct(query=fieldMetaData,key="name")>
	</cffunction>


	<cffunction name="validateWhereClause" access="public" output="false" returnType="struct" hint="Validates a where clause for a given relayware or remote object">
		<cfargument name="object" type="string" required="true">
		<cfargument name="relayware" type="boolean" required="true">
		<cfargument name="whereClause" type="string" required="true">

		<cfset var validateQuery = "">
		<cfset var result = {isOK=true,message="Validated Successfully"}>

		<cfif arguments.whereClause neq "">
			<cftry>
				<cfif arguments.relayware>

					<cfif application.com.security.checkStringForInjection(string=arguments.whereClause,type="dangerousSql").isOK>

						<cfset var entityTypeStruct = application.com.relayEntity.getEntityType(entityTypeID=arguments.object)>

						<!--- run a query against the view so that we can include flags as part of the where clause --->
						<cfif entityTypeStruct.tablename neq "">
							<cfquery name="validateQuery">
								select top 1 #entityTypeStruct.uniqueKey# from v#entityTypeStruct.tablename# as #entityTypeStruct.tablename# where #preserveSingleQuotes(arguments.whereClause)#
							</cfquery>
						<cfelse>
							<cfset result.message = "Invalid Object">
						</cfif>
					<cfelse>
						<cfset result.message = "Invalid Statement">
					</cfif>

				<cfelse>

					<cfset var qryRemoteObjects = application.getObject("connector.com.connector").getRemoteObjects(connectorType=arguments.connectorType)>

					<cfquery name="qryRemoteObjects" dbType="query">
						select name from qryRemoteObjects where lower(name)=<cf_queryparam value="#lcase(arguments.object)#" cfsqltype="cf_sql_varchar">
					</cfquery>

					<cfif qryRemoteObjects.recordCount>
						<cfset var apiResponse = application.getObject("connector.com.connector").sendApiRequest(connectorType=arguments.connectorType,method="query",object=arguments.object,queryString="select Id from #arguments.object# where "&arguments.whereClause & " order by id limit 1")>
						<cfset result.isOk = apiResponse.isOK>
						<cfif not result.isOk>
							<cfset result.message = apiResponse.message>
						</cfif>
					<cfelse>
						<cfset result.message = "Invalid object">
					</cfif>

				</cfif>

				<cfcatch>
					<cfset result.isOK = false>
					<cfset result.message = cfcatch.message>
					<cfset application.com.errorHandler.recordRelayError_Warning(type="Connector",Severity="error",catch=cfcatch,WarningStructure=arguments)>
				</cfcatch>
			</cftry>
		</cfif>

		<cfreturn result>
	</cffunction>


	<cffunction name="getRemoteIDColumn" access="public" output="false" returnType="string" hint="Returns the ID column for a given remote object">
		<cfargument name="connectorType" type="string" required="true">
		<cfargument name="object" type="string" required="true" hint="The name or ID of the remote object">

		<cfreturn application.getObject("connector.com.connector").getRemoteIDColumn(argumentCollection=arguments)>
	</cffunction>


	<cffunction name="deleteConnectorMapping" access="public" output="false" returnType="boolean" hint="Deletes a connector mapping and recreates the views">
		<cfargument name="connectorMappingID" type="numeric" required="true" hint="The ID of the mapping to delete">
		<cfargument name="recreateViews" type="boolean" default="true" hint="Recreate the transformation views?">

		<cfreturn application.getObject("connector.com.connectorMapping").deleteConnectorMapping(argumentCollection=arguments)>
	</cffunction>


	<cffunction name="getDataFromConnectorResponse" access="public" output="false" returnType="query" hint="Gets the data from the connectorResponse table showing what the state of the data was at the time of sending">
		<cfargument name="connectorType" type="string" default="#application.com.settings.getSetting("connector.type")#">
		<cfargument name="connectorResponseID" type="numeric" required="true">

		<cfreturn application.getObject("connector.com.connector").getDataFromConnectorResponse(argumentCollection=arguments)>
	</cffunction>

</cfcomponent>