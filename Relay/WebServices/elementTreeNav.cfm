<!--- �Relayware. All Rights Reserved 2014 --->
<!---
psuedo webservice which does AJAX stuff for elements\elementTreeNav_tree

2010/09/15  WAB  	LID 3905 - Deal with nodes which have GetChildrenFromParentID set (also change in elementTreeNav webservice)
2013/01/03	YMA		2013 Roadmap Content Deletion
2014-10-06	RPW		CORE-708 Wrong pop up confirmation while searching for Relaytags under Content Manager - Removed alert(), not neccessary as item already displayed.
 --->

		<cfparam name = "frmShowTreeStatusID" default = 0>
		<cfparam name = "frmShowTreeForLoggedInPerson" default = "-1">
		<cfparam name = "frmShowTreeForUserGroups" default = "-1">
		<cfparam name = "frmShowTreeForCountryID" default = "-2">
		<cfparam name = "frmDepth" default = "2">


<cfswitch expression = #url.method#>
	<cfcase value="addItems">

		<cfparam name="id">


		<!--- WAB LID 3905 Do a little check to make sure that this isn't a node that gets its content from somewhere else --->
		<cfquery name="checkForChildrenFromOtherNode" datasource = "#application.sitedatasource#">
		select e.id, e.getchildrenfromparentid, e2.headline as otherHeadline from element e
			left join element e2 on e.getchildrenfromparentid = e2.id
		where e.id =  <cf_queryparam value="#id#" CFSQLTYPE="CF_SQL_INTEGER" >  and e.getchildrenfromparentid <> 0
		</cfquery>

		<cfif checkForChildrenFromOtherNode.recordCount is 0>

			<cfif frmShowTreeForUserGroups contains "-1"><cfset tempUserGroupList  = -1><cfelseif listfind(frmShowTreeForUserGroups,0) is not 0><cfset tempUserGroupList  = 0><cfelseif frmShowTreeForUserGroups is not ""><cfset tempUserGroupList = frmShowTreeForUserGroups></cfif>

				<cfset tempUserGroupList = "-1">
				<cfquery name="getElements" datasource="#application.sitedatasource#" >
					exec getElementTree
					@ElementID =  <cf_queryparam value="#ID#" CFSQLTYPE="CF_SQL_INTEGER" > ,
					@countryid = '#iif(frmShowTreeForCountryID is -2,de(request.relaycurrentuser.countrylist),de(frmShowTreeForCountryID))#',
					@statusid =  <cf_queryparam value="#listfirst(frmShowTreeStatusID,"|")#" CFSQLTYPE="CF_SQL_INTEGER" > ,
					@Depth =  <cf_queryparam value="#frmDepth#" CFSQLTYPE="CF_SQL_INTEGER" > ,
					@isLoggedIn =  <cf_queryparam value="#frmShowTreeForLoggedInPerson#" CFSQLTYPE="CF_SQL_INTEGER" > ,
					@usergrouplist =  <cf_queryparam value="#tempUserGroupList#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
					@date = null,
					@returnEditRightsForPersonID = #request.relayCurrentUser.personid#,
					@returnViewRightsLists = 1
				</cfquery>


				<!--- this function creates all the javascript for adding items to the tree	 --->
				<cfset dtreeFunctions_JS = application.com.relayelementtree.generateDTreeAddFunctions(getElements,false)>
				<cfset dtreeFunctions_JS = replace(dtreeFunctions_JS,"d.add","d.addWithCheck","ALL")>

				<!--- WAB 2013-05   CASE 434366 deal with removing items from dtree if they are deleted/retired--->
				<cfoutput>
					<script>
							//	alert ('#getElements.recordcount# items #jsStringFormat(valuelist(getElements.node))#')
								// remove all items which have 'disappeared'
								d.removeChildrenNotInList (#getElements.parentid#,'#valuelist(getElements.node)#')

								<cfif getElements.recordcount is not 0>
									addDtreeItems =	function () {#dtreeFunctions_JS#}
									 addDtreeItems();
									d.redrawParent(#jsStringFormat(id)#);     // WAB 2007-02-07 discovered that need to do the redraw from the parent of the node otherwise can't handle nodes getting added under other nodes (and pages becoming folders.  Hope doesn't affect performance too much)
								//	d.redraw(#jsStringFormat(id)#);

								</cfif>

								<!--- if the query brought back 0 records or 1 record (which would just be the parent record) then there are no child records and we need to display a no items message --->
								<cfif getElements.recordcount lte 1>
									d.redrawNoItems(#jsStringFormat(id)#);
								</cfif>
					</script>
				</cfoutput>
			<cfelse>
				<cfoutput>
					<script>
						d.addWithCheck('#jsStringFormat(checkForChildrenFromOtherNode.id)#_Child','#jsStringFormat(checkForChildrenFromOtherNode.id)#','Children Imported From #jsStringFormat(checkForChildrenFromOtherNode.getchildrenfromparentid)# (#jsStringFormat(checkForChildrenFromOtherNode.otherHeadline)#)','','','','','','','','',false);
						d.redrawParent(#jsStringFormat(id)#)
					</script>
				</cfoutput>


			</cfif>
	</cfcase>


	<cfcase value="searchAndAdd">
		<cfparam name = "goto">
		<cfparam name = "searchin" default = "">
		<cfparam name = "searchCurrentTree" default = "0">


				<cfset ElementTreeCacheID = application.com.relayelementTree.getElementTreeCacheID(listfirst(currentDisplayedNodes))>

					<!--- find item --->
					<CFQUERY NAME="findElement" DATASOURCE="#application.SiteDatasource#">
					select e.id, headline,parentid, case when ecd.elementid is not null then 1 else 0 end as inTree
					from element e
						<cfif searchCurrentTree is 0>left</cfif> join
						elementTreecachedata ecd on e.id = ecd.elementid and ecd.elementTreecacheID =  <cf_queryparam value="#elementTreecacheID#" CFSQLTYPE="CF_SQL_INTEGER" >


					where
						<cfif isNumeric(goto)>
						id =  <cf_queryparam value="#goto#" CFSQLTYPE="CF_SQL_INTEGER" >
						<cfelse>
						elementtextID =  <cf_queryparam value="#goto#" CFSQLTYPE="CF_SQL_VARCHAR" >
						</cfif>
						or headline  like  <cf_queryparam value="%#goto#%" CFSQLTYPE="CF_SQL_VARCHAR" >
					union
						select entityID, headline,parentid, case when ecd.elementid is not null then 1 else 0 end as inTree
					from
						phrases p inner join phraseList pl on p.phraseID = pl.phraseID
						 inner join element e on pl.entityID = e.id
						<cfif searchCurrentTree is 0>left</cfif> join
						elementTreecachedata ecd on e.id = ecd.elementid and ecd.elementTreecacheID =  <cf_queryparam value="#elementTreecacheID#" CFSQLTYPE="CF_SQL_INTEGER" >

					where
					entityTypeID = 10
					<cfif searchin is "allcontent">
					and phrasetext  like  <cf_queryparam value="%#goto#%" CFSQLTYPE="CF_SQL_VARCHAR">
					<cfelse>
					and contains (p.phraseText,<cf_queryparam value="""#goto#*""" CFSQLTYPE="CF_SQL_VARCHAR" > )
					</cfif>
					order by case when ecd.elementid is not null then 1 else 0 end desc
					</CFQUERY>

					<!--- if there is 1 result, want to know whether it is in the current tree and so can be displayed
						unfortunately a brand new node won't necessarily have got into the elementtreecache so #inTree# is not completely fullproof (May be this could be changed!), so I need to do another check on the parent list
					--->
					<cfif findElement.recordcount is 1>

						<!--- get parents --->
						<cfquery name="getParents" datasource="#application.sitedatasource#" >
						exec getElementParents @elementID =  <cf_queryparam value="#findElement.id#" CFSQLTYPE="CF_SQL_INTEGER" > , @topid =  <cf_queryparam value="#listfirst(currentDisplayedNodes)#" CFSQLTYPE="CF_SQL_INTEGER" >
						</CFQUERY>

						<cfset parentList = valueList(getParents.elementid)>
					</cfif>


					<cfif findElement.recordcount is 1 and listfind(parentList,listfirst(currentDisplayedNodes))>


						<cfquery name="getStartingNode" dbtype="query">
						select * from getparents where elementid in (#currentDisplayedNodes#) order by generation desc
						</CFQUERY>
						<cfset startingNode = getStartingNode.elementid>

						<cfquery name="getTree" datasource="#application.sitedatasource#" >
						exec getElementTree
							@elementID =  <cf_queryparam value="#listfirst(currentDisplayedNodes)#" CFSQLTYPE="CF_SQL_INTEGER" > ,
							@branchList =  <cf_queryparam value="#parentList#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
							@returnBranchSiblings = 1,
							@countryid = '#iif(frmShowTreeForCountryID is -2,de(request.relaycurrentuser.countrylist),de(frmShowTreeForCountryID))#',
							@statusid =  <cf_queryparam value="#listfirst(frmShowTreeStatusID,"|")#" CFSQLTYPE="CF_SQL_INTEGER" > ,
							@Depth=#frmDepth#,
							@isLoggedIn = #frmShowTreeForLoggedInPerson#,
							@date = null,
							@returnEditRightsForPersonID = #request.relayCurrentUser.personid#,
							@returnViewRightsLists = 1
						</cfquery>

						<cfquery name="elementsToAdd" dbtype=query >
						select * from getTree where node not in (#currentDisplayedNodes#)
						</cfquery>



							<!--- this function creates all the javascript for adding items to the tree	 --->
							<cfset dtreeFunctions = application.com.relayelementtree.generateDTreeAddFunctions(elementsToAdd,false)>
							<cfset dtreeFunctions = replace(dtreeFunctions,"d.add","d.addWithCheck","ALL")>

							<cfoutput>
							<SCRIPT type="text/javascript">
							addDtreeItems = function () {
								#dtreeFunctions#
							}
								// 	alert ('#ElementsToAdd.recordcount# items')
								<cfif ElementsToAdd.recordcount is not 0>
									 addDtreeItems();
									d.redraw(#startingNode#);
									d.openTo (#findElement.id#,true);
								</cfif>
								<!--- 2014-10-06	RPW	CORE-708 Wrong pop up confirmation while searching for Relaytags under Content Manager - Removed alert(), not neccessary as item already displayed. --->
								  if (divtoggle_searchResults){divtoggle_searchResults('open')}  // must make sure that the div is open if it exists
							</script>

					 		<table>
									<cfloop query="findElement" >
								<tr>
									<td>
										<a id = "sr-#id#-#parentid#" contextmenu = "element" href="javascript:void(openNewTab('element_#ID#','#replace(headline,"'","\'","ALL")# (#ID#)','/elements/elementEdit.cfm?RecordID=#ID#',{reuseTab:true,iconClass:'content',reuseTabJSFunction: function () {return true}}));" title="EID #id#" class="smalllink" contextmenu="page" onmouseover="javascript:fnSuppressMouseOver(event)">#htmleditformat(headline)#  </A>
									</td>
								</tr>
									</cfloop>
							</table>

							</cfoutput>



				<cfelseif findElement.recordcount  gte 1>

					 		<cfoutput><table></cfoutput>
							<cfoutput query="findElement" group = "intree">
								<tr>
									<td>
										<cfif intree is 1>Items In Current Tree<cfelse>Items Not In Current Tree</cfif>
									</td>
								</tr>

								<cfoutput>
								<tr>
									<td>
										<a id = "sr-#id#-#parentid#" contextmenu = "element" href="javascript:void(openNewTab('element_#ID#','#replace(headline,"'","\'","ALL")# (#ID#)','/elements/elementEdit.cfm?RecordID=#ID#',{reuseTab:true,iconClass:'content',reuseTabJSFunction: function () {return true}}));" title="EID #id#" class="smallLink" contextmenu="page" onmouseover="javascript:fnSuppressMouseOver(event)">#htmleditformat(headline)#  </A>
									</td>
								</tr>
								</cfoutput>
							</cfoutput>
							<cfoutput></table></cfoutput>
						<SCRIPT type="text/javascript">
						  if (divtoggle_searchResults){divtoggle_searchResults('open')}  // must make sure that the div is open if it exists
						</script>

				<cfelse>
					<cfoutput>
					<SCRIPT type="text/javascript">
					  if (divtoggle_searchResults){divtoggle_searchResults('open')}  // must make sure that the div is open if it exists
					</script>
					#htmleditformat(goto)# Not Found <cfif searchCurrentTree is 1> in Current Tree</cfif>
					</cfoutput>
				</cfif>





	</cfcase>


	<cfcase value="pipeLineReport">
		<cfparam name = "parentElementID">
<!--- 		 <cfparam name = "frmShowTreeForCountryID" default = "9"> --->
		 <cfparam name = "frmShowTreeStatusID" default = "4">
		 <cfparam name = "frmGoingLiveInNextXDays" default = "4">
		 <cfparam name = "frmHasGoneLiveInPastXDays" default = "4">
		 <cfparam name = "frmUpdatedInPastXDays" default = "4">

		<cfscript>
			//BF-466 validate against non numeric, ommitted and excessive values
			frmGoingLiveInNextXDays=sanitiseNumericDayInput(frmGoingLiveInNextXDays);
			frmHasGoneLiveInPastXDays=sanitiseNumericDayInput(frmHasGoneLiveInPastXDays);
			frmUpdatedInPastXDays=sanitiseNumericDayInput(frmUpdatedInPastXDays);
			
		</cfscript>

		<cfset ElementTreeCacheID = application.com.relayelementTree.getElementTreeCacheID(parentElementID)>


		<cfquery name="getElementsGoneLive" datasource="#application.sitedatasource#" >
		select e.*, date as livedate
		 from element  e inner join elementTreeCachedata ecd on e.id = ecd.elementID
		where
		ecd.elementTreeCacheID =  <cf_queryparam value="#ElementTreeCacheID#" CFSQLTYPE="CF_SQL_INTEGER" >
		and
		(Date > getDate()- <cf_queryparam value="#frmHasGoneLiveInPastXDays#" CFSQLTYPE="CF_SQL_NUMERIC" > and Date < getDate() + <cf_queryparam value="#frmGoingLiveInNextXDays#" CFSQLTYPE="CF_SQL_NUMERIC" > )

		order by
		date
		</cfquery>

	 	<cfquery name="getElementsRecentlyUpdated" datasource="#application.sitedatasource#" >
		select e.*
		 from element e inner join elementTreeCachedata ecd on e.id = ecd.elementID
		where
			ecd.elementTreeCacheID =  <cf_queryparam value="#ElementTreeCacheID#" CFSQLTYPE="CF_SQL_INTEGER" >
				and
			statusid = 4
			and
			(LASTUPDATED > getDate()- <cf_queryparam value="#frmUpdatedInPastXDays#" CFSQLTYPE="CF_SQL_NUMERIC" >)
		order by
		date
		</cfquery>


		<cfoutput>
		<table>
		<tr><td colspan=2 ><B>Items going/gone live</B></td></tr>

		<cfif getElementsGoneLive.recordcount is 0>
			<tr>
			<td>&nbsp;</td>
			<td>
				None
			</td>
			</tr>
		</cfif>

		<cfloop  query="getElementsGoneLive" >
			<tr>
			<td>&nbsp;</td>
			<td align=left>
				<a id = "live-#id#-#parentid#" contextmenu = "page" href="elementEdit.cfm?RecordID=#id#" target="editFrame" title="EID #id# &##10 Right Click For Options" class="smalllink" contextmenu="page">#htmleditformat(headline)#</A>

				<BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<cfif liveDate is not "" and liveDate gt now() >
					Goes Live #htmleditformat(application.com.datefunctions.DayDiffFromTodayInWords(livedate))#
				<cfelseif liveDate is not "" and  liveDate gt now() - frmHasGoneLiveInPastXDays >
					Went Live #htmleditformat(application.com.datefunctions.DayDiffFromTodayInWords(livedate))#
				</cfif>
			</td>
			</tr>

		</cfloop >

		<tr><td colspan=2 ><B>Live Items recently updated</B></td></tr>

		<cfif getElementsRecentlyUpdated.recordcount is 0>
			<tr>
			<td>&nbsp;</td>
			<td>
				None
			</td>
			</tr>
		</cfif>
		<cfloop  query="getElementsRecentlyUpdated" >

			<tr>
			<td>&nbsp;</td>
			<td align=left>
				<a id = "up-#id#-#parentid#" contextmenu = "page" href="elementEdit.cfm?RecordID=#id#" target="editFrame" title="EID #id# &##10 Right Click For Options"  class="smalllink" contextmenu="page" >#htmleditformat(headline)#</A>
			<td>
			</tr>

		</cfloop >
		</table>
		</cfoutput>

	</cfcase>

	<cfcase value="pageDelete">
		<cfparam name="id">

		<cfset deleteList = application.com.elementtreenav.getArchivePageFamily(id=id)>

		<cfquery name="deleteElementRecord" datasource="#application.sitedatasource#" >
			delete from element where ID in (<cf_queryparam value="#valuelist(deleteList.node)#" CFSQLTYPE="CF_SQL_INTEGER" LIST="True">)
		</cfquery>

	</cfcase>

	<cfcase value="pageRestore">
		<cfparam name="id">


		<cfquery name="restoreArchiveRecord" datasource="#application.sitedatasource#" >
			update element set statusID = 0 where ID = <cf_queryparam value="#id#" CFSQLTYPE="CF_SQL_INTEGER">
		</cfquery>

	</cfcase>

	<cfcase value="sendToArchive">
		<cfparam name="id">

		<cfquery name="ArchiveRecord" datasource="#application.sitedatasource#" >
			update element set statusID = 6 where ID = <cf_queryparam value="#id#" CFSQLTYPE="CF_SQL_INTEGER">
		</cfquery>
	</cfcase>

	<cfcase value="loadArchive">
		<cfinclude template="/elements/archiveTree.cfm">
	</cfcase>

</cfswitch>

<cfscript>

	numeric function sanitiseNumericDayInput(any numberOfDays){
		var maximumNumberOfDays=36500; //quite low as it goes off the end of available dates, is 100 years
		if (not IsNumeric(numberOfDays) or LCase(numberOfDays) EQ "e"){
			numberOfDays=0;
		}else if (numberOfDays>maximumNumberOfDays){
			numberOfDays=maximumNumberOfDays;
		}else if (numberOfDays<0){
			numberOfDays=0;
		}
		return numberOfDays;
	}

</cfscript>





