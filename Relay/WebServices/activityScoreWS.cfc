<!---
  --- activityScoreWS
  --- ---------------
  --- 
  --- author: Richard.Tingle
  --- date:   10/03/16
  --->
<cfcomponent accessors="true" output="false" persistent="false">


	<cffunction name="getActivitiesInModule" access="public" output="false" returntype="any">
		<cfargument name="moduleTextId" required="true" hint="In addition to a moduleTextId this can be 'Any' or 'Other', any does what you'd imagine, other returns any activity whose entity isn't attached to a particular module' ">
		<cfargument name="entityTypeIDForScoring" type="numeric" required="true" hint="Some activities don't have data on certain levels, this filters those out">
		
		<cfreturn application.com.activityScore.getActivitiesInModule(moduleTextId, entityTypeIDForScoring)/>
	</cffunction>
	
	
	<cffunction name="getScoringMethodsForActivity" access="public" returnType="any">
		<cfargument name="activityTypeId" required="true"> 
		<cfscript>

			
			if (activityTypeId==""){
				return QueryNew("method,display");
			}else{
				var methodDetailsArray=application.com.activityScore.getScoringTechniquesForActivity(activityTypeId);
			
				//translate any phrases
				
				for(var i=1;i<=arrayLen(methodDetailsArray);i++){
					methodDetailsArray[i].display=application.com.relayTranslations.translateStringWithAutoEncode(methodDetailsArray[i].display);
				}

				return application.com.structureFunctions.arrayOfStructuresToQuery(theArray=methodDetailsArray);
				
			}
		
		
			
		
		</cfscript>
	
	</cffunction>



	<cffunction name="displayScoringMethodOptions" access="public" output="false" returntype="string">
		<cfargument name="scoringMethod" required="true">
		<cfargument name="activityTypeID" required="true">

		<cfset var optionHTML="">
		<!---shouldn't really come up but can cause errors during initial page load --->
		<cfif isNull(scoringMethod) OR scoringMethod EQ "" OR scoringMethod EQ "null">
			<cfset scoringMethod="default">
		</cfif>
		<cfif scoringMethod eq "default">
			<cfsavecontent variable="optionHTML">
				<cf_relayFormElementDisplay relayFormElementType="numeric" maxlength="9" required="true" fieldname="scorePerActivity" label="phr_score_scorePerActivity" currentValue="">
			</cfsavecontent>
		<cfelse>
			<!--- get the custom options --->

			<cfset requiredParameterData=application.com.activityScore.getRequiredParametersForActivityScoringMethod(scoringMethod)>
			<cfif activityTypeID neq '' && isNumeric(activityTypeID)>
				<cfset activityScoringFields = getScoringFields(activityTypeID)>
			<cfelse>
				<cfthrow message="activityTypeID is not Numeric, so we can't receive scoring type for current activity">
			</cfif>

			<cfsavecontent variable="optionHTML">
				<cfloop index="i" from=1 to="#arrayLen(requiredParameterData)#">
				
					<!---Allow through types that we recognise, otherwise just allow free text --->
					<!---ESZ 9/12/2016 Activity Scoring - Percentage Points Awarding Request [#2216]--->
					<cfif listContainsNoCase("string", requiredParameterData[i].parameterType)>
						<cfset var type= "select" >

						<cf_relayFormElementDisplay
								relayFormElementType="#type#"
								fieldname="#requiredParameterData[i].parameterName#"
								required="#requiredParameterData[i].parameterRequired#"
								label="#requiredParameterData[i].parameterPhrase#"
								<!---bindFunction="cfc:webservices.callWebService.callWebservice(webServiceName='activityScoreWS',methodName='getScoringFields',activityTypeId=35)"--->
								<!---bindFunction="cfc:webservices.callWebService.callWebservice(webServiceName='activityScoreWS',methodName='testREST',activityTypeId=35)"--->
								<!---bindOnLoad="true"--->
								<!---ValidValues="#names#"--->
                                query="#activityScoringFields#"
								display="displayValue" value="dataValue"
								currentValue="">
						<cfelse>
						<cfset var type= listContainsNoCase("numeric", requiredParameterData[i].parameterType)?requiredParameterData[i].parameterType:"text" >
						<cf_relayFormElementDisplay
								relayFormElementType="#type#"
								fieldname="#requiredParameterData[i].parameterName#"
								required="#requiredParameterData[i].parameterRequired#"
								label="#requiredParameterData[i].parameterPhrase#"
								currentValue="">
					</cfif>

				</cfloop>
			</cfsavecontent>

		</cfif>



		<cfreturn optionHTML>
	</cffunction>


<!---ESZ 9/12/2016 Activity Scoring - Percentage Points Awarding Request [#2216]--->
	<cffunction name="getScoringFields" access="public" output="false" returntype="query" hint="Returns an query of fields">
		<cfargument name="activityTypeID" required="true">

		<cfset var filteredByTypeID=application.com.activityScore.getAssociatedEntityTypeIDForActivity(activityTypeID)>
		<cfset var tablenameCur = application.entityType[filteredByTypeID].TABLENAME>
		<cfset var filteredByTypeID=application.com.activityScore.getAssociatedEntityTypeIDForActivity(activityTypeID)>

<!--- Set row data. --->

		<cfscript>

			var noSortedValidMethodsArray=ArrayNew(1);
			var validMethodsArray=ArrayNew(1);

			var qFields = QueryNew("displayValue,dataValue");
			var count = 0;
			//12-10-2016 ESZ Activity Scoring - inappropriate entity fields available for scoring [#2407]
			var queryService=new Query();
			queryService.setDatasource(application.siteDatasource);
			queryService.addParam(name="tablenameCur",value=tablenameCur,cfsqltype="cf_sql_varchar");
			//20-10-2016 ESZ Weighted Score fields do not show when Weighted score is selected. [#2564]
			queryResult=queryService.execute(
				sql="select Name from vFieldMetadata where dataType = 'numeric' and isPrimaryKey = 0  and name not like 'createdby%' and name not like '%updatedby%' and tablename = :tablenameCur");

			var entities = application.com.structureFunctions.QueryToArrayOfStructures(queryResult.getResult());
			for(var i=1; i LTE ArrayLen(entities); i=i+1){
				var entity = entities[i].NAME;
				if(application.com.flag.doesFlagExist(flagID=entity)) {
					newRow = QueryAddRow(qFields);
					var currentEnityName = entity;
					var currentEnityValue = application.com.flag.getFlagIdByFlagTextId(flagTextId=entity);
					ArrayAppend(noSortedValidMethodsArray,currentEnityName);
					QuerySetCell(qFields, "displayValue", currentEnityName );
					QuerySetCell(qFields, "dataValue",currentEnityValue );
					count++;
				} else {
					newRow = QueryAddRow(qFields);
					ArrayAppend(noSortedValidMethodsArray,entity);
					QuerySetCell(qFields, "displayValue", entity );
					QuerySetCell(qFields, "dataValue",entity );
					count++;
				}
			}

			//11-04-2016 ESZ Activity Scoring - 'Profile to use for scoring' dropdown should have 'No profiles available' message, [#2360]
			if(count == 0) {
				newRow = QueryAddRow(qFields);
				QuerySetCell(qFields, "displayValue", "No profiles available" );
				QuerySetCell(qFields, "dataValue", "" );
			}
			return qFields;
		</cfscript>

	</cffunction>

	<cffunction name="displayFilter" access="public" output="false" returntype="string">
		<cfargument name="filterName" required="true">
		<cfargument name="activityTypeID" required="true">
		<cfargument name="currentFilter" required="true">

		<!--- ignore the currentFilter if the filteredByTypeID isn't the same'--->
		<cfscript>
			var filteredByTypeID=application.com.activityScore.getAssociatedEntityTypeIDForActivity(activityTypeID);

			if (currentFilter!=""){
				var currentFilterTypeID=deserializejson(currentFilter).entityTypeID;

				if (currentFilterTypeID!=filteredByTypeID){
					arguments.currentFilter="";
				}
			}

		</cfscript>

		<cfsavecontent variable="optionHTML">
			<cf_filterLocallyAnyEntity name="#filterName#" filteredByTypeID="#filteredByTypeID#" currentFilter="#currentFilter#">
		</cfsavecontent>

		<cfreturn optionHTML/>
	</cffunction>
	
	<cffunction name="getScoringEntityTypeIDForScheme" access="public" output="false" returntype="struct">
		<cfargument name="schemeID" required="true" type="numeric">
		<cfscript>
				var returnStruct={};
				returnStruct.entityTypeID=application.com.ActivityScore.getScoreRuleGrouping(groupingID=arguments.schemeID).recordSet.entityTypeID;

				return returnStruct;
		</cfscript>
	</cffunction>


</cfcomponent>