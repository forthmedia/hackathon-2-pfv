<!--- �Relayware. All Rights Reserved 2014 --->
<!--- DJH 2000-09-06 
Mods:  WAB 2000-05-04   Altered Download Functionality - all put into a single file
	  NJH 2008/09/25 elearning 8.1 - Added functionality to grade students. The status is set in the trngUserModuleProgress table for a given userModuelProgressID.
	
	2012-08-23 	PPB Case 430289 close quotation marks when defining variable frmTotalChecks
	2012-08-24	IH	Case 430313 fix if statement around cf_header
--->

<cfparam name="refreshCaller" default="false">

<CFIF NOT IsDefined("frmAction")>
	<cflocation url="eventHome.cfm"addToken="false">
	<CF_ABORT>
</CFIF>

<cfif frmAction eq "updatePersonEvent">
	<cfset message = application.com.dbTools.updateColumnValue(frmPrimaryKeyIDs,evaluate(frmNewValueField),frmTableName,frmPrimaryKeys,frmColumnName)>

	<!--- update list of personids with new event information. --->
	<!--- <CF_ABORT>
	<cfquery name="updatePersonEvent" datasource="#application.siteDataSource#">
		UPDATE	eventFlagData
		SET 	flagID = #frmOtherEvents#
		WHERE 	entityID in (#frmPersonIDs#) AND
				flagID = #frmFlagID#
	</cfquery> --->
	
	<cf_head>
	<cf_title>Events - Lists</cf_title>
	</cf_head>
	
	<cfif refreshCaller>
		<SCRIPT type="text/javascript">
			function doUnload() {
				opener.focus();
				opener.location.reload();
			}
			
			window.onunload = doUnload;
			
		</script>
	</cfif>
	<div align="center">
		<cfoutput>#application.com.security.sanitiseHTML(message)#</cfoutput>
		<p>
		<A HREF="javascript: window.close()">Close Window</A>
		</p>
	</div>
	
	
	<CF_ABORT>
</cfif>

<cfif frmAction eq "movepersonstoevent">
	<!--- get other events associated to current event's group --->
	<cfquery name="getNewEvents" datasource="#application.siteDataSource#">
		SELECT  flagID as newFlagID, eventname + '-' + Title as Title
		FROM 	eventDetail ed inner join eventGroup eg on
		ed.eventGroupID = eg.eventGroupID
		WHERE 	ed.eventGroupID = (select eventGroupID from eventDetail where flagID =  <cf_queryparam value="#flagID#" CFSQLTYPE="CF_SQL_INTEGER" > )
	</cfquery>
	
	<cf_head>
	<cf_title>Events - Lists</cf_title>
	</cf_head>
	

	<div align="center">
	<cfif getNewEvents.recordCount gt 0>
	<FORM NAME="personEventChange" ACTION="eventTask.cfm" METHOD="post">
	<CFOUTPUT>
		<p>
		To change the event for selected delegates, select the relevant event from the list below and click "continue".
		</p>
		<CF_INPUT NAME="frmFlagID" TYPE="Hidden" VALUE="#FlagID#">
		<CF_INPUT NAME="frmPersonIDs" TYPE="Hidden" VALUE="#frmTotalChecks#">
		<INPUT NAME="frmRunInPopUp" TYPE="Hidden" VALUE="true">
		<INPUT NAME="frmColumnName" TYPE="Hidden" VALUE="FlagID">
		<INPUT NAME="frmPrimaryKeys" TYPE="Hidden" VALUE="EntityID|FlagID">
		<CF_INPUT NAME="frmPrimaryKeyIDs" TYPE="Hidden" VALUE="#frmTotalChecks#|#FlagID#">
		<INPUT NAME="frmNewValueField" TYPE="Hidden" VALUE="frmOtherEvents">
		<INPUT NAME="frmTableName" TYPE="Hidden" VALUE="eventFlagData">
		<INPUT NAME="frmAction" TYPE="Hidden" VALUE="updatePersonEvent">
		<CF_INPUT NAME="refreshCaller" TYPE="Hidden" VALUE="#refreshCaller#">

		<!--- extra fields required to get the invitation email to get it sent automatically --->
		<!--- only want the invitation email --->
		<select name="frmOtherEvents">
			<cfloop query="getNewEvents">
				<option value="#newFlagID#"<cfif newFlagID eq flagID> selected</cfif>>#htmleditformat(title)#</option>
			</cfloop>
			
		</select>
	</CFOUTPUT>
	<p>
	<input type="submit" id="submitPersonEventChange" value="Continue">
	<!--- <A HREF="JavaScript: document.personEventChange.submit();"><IMG SRC="<CFOUTPUT></CFOUTPUT>/images/buttons/c_continue_e.gif" WIDTH=105 HEIGHT=21 BORDER=0 ALT="Continue"></A> --->
	</p>
	</FORM>
	<cfelse>
	<form>
	<p>
	There are no events available to associate the selected delegate/s to.
	</p>
	<p>
	<A HREF="javascript: window.close()">Close Window</A>
	</p>
	</form>
	</cfif>
	</div>
	
	
	<CF_ABORT>
</cfif>


<cfif frmAction is "invallcomm" or frmAction is "allcomm" or frmAction is "alldown">
	<!--- 2012-07-25 PPB P-SMA001 commented out
	<CFINCLUDE TEMPLATE="../templates/qrygetcountries.cfm">
	 --->
	<!--- you can pass this select as the frmPersonIDs, as it gets used in an in clause later on --->
	
	<!--- 2012-07-25 PPB P-SMA001 commented out	
	<cfset frmTotalChecks = 
	"select
		entityid
	from 
		eventflagdata as efd
		inner join person as p
		on efd.entityid = p.personid
		inner join location as l
		on l.locationid = p.locationid
	where
		flagid = #flagid#
		and l.countryid in (#variables.countrylist#)"
	>
	 --->
	
	<!--- 2012-07-25 PPB P-SMA001  --->	
	<!--- 2012-08-23 PPB Case 430289 needed to close the quotation marks --->	
	<cfset frmTotalChecks = 
	"select
		entityid
	from 
		eventflagdata as efd
		inner join person as p
		on efd.entityid = p.personid
		inner join location as l
		on l.locationid = p.locationid
	where
		flagid = #flagid# 
		#application.com.rights.getRightsFilterWhereClause(entityType='EventFlagData',alias='l').whereClause#" 
	>
	
</cfif>


<cfif frmAction is "updatemaindetails">
	<!--- user wants to update details of certain users from eventflagdata to person --->
	<cf_eventNotificationEmail action="compile" changes="Delegate details updated in main database">
	<cf_eventNotificationEmail action="send" FlagID="#FlagID#">
	
	<cf_head><cf_title>Events - Update delegates details in main database</cf_title></cf_head>
	

	<cfif isDefined("confirmedUpdate")>
		<!--- user has confirmed that they really want to do this --->
		<cfquery name="updatemaindatabase" datasource="#application.siteDataSource#">
		
		update
			p
		set
			p.FirstName = case when e.FirstName is null or datalength(rtrim(e.FirstName)) = 0 then p.FirstName else e.FirstName end,
			p.LastName = case when e.LastName is null or datalength(rtrim(e.LastName)) = 0 then p.LastName else e.LastName end,
			P.OfficePhone = case when e.Telephone is null or datalength(rtrim(e.Telephone)) = 0 then p.OfficePhone else e.Telephone end,
			P.MobilePhone = case when e.Mobile is null or datalength(rtrim(e.Mobile)) = 0 then p.MobilePhone else e.Mobile end,
			p.FaxPhone = case when e.Fax is null or datalength(rtrim(e.Fax)) = 0 then p.FaxPhone else e.Fax end,
			p.Email = case when e.Email is null or datalength(rtrim(e.Email)) = 0 then p.Email else e.Email end,
			p.JobDesc = case when e.JobTitle is null or datalength(rtrim(e.JobTitle)) = 0 then p.JobDesc else e.JobTitle end,
			p.Title = case when e.Title is null or datalength(rtrim(e.Title)) = 0 then p.Title else e.Title end
		from
			person as p
			inner join eventflagdata as e
			on p.personid = e.entityid
		where
			e.FlagID =  <cf_queryparam value="#FlagID#" CFSQLTYPE="CF_SQL_INTEGER" > 
			and e.entityid  in ( <cf_queryparam value="#frmCheckIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			and 
			(
			p.FirstName <> e.FirstName
			or p.LastName <> e.LastName
			)
		</cfquery>
		
		<div align="center">
			<p align="justify">
			The records you selected have been updated. Click Close below to close this window and refresh the delegate list.
			</p>

			<a href="javascript:window.opener.document.search.submit(); this.close();">Close Window</a>
		</div>
	<cfelse>
		<p><br></P>
		<div align="center">
		<!--- give the user a chance to change their mind --->
		<p align="justify">
		Are you sure you want to update the personal details of the checked users? This will replace any information held
		in the main database with the information which has been supplied specifically for this event.
		</p>
		<br>
		<p align="justify">
		NOTE: You should NOT update details when a name is changing from one person to another, as the person details of the 
		original delegate will be lost. Only use this feature to correct spelling errors in the main database details
		</p>
		<br>
		<P align="justify">
		If you're sure you wish to proceed, click continue. Otherwise click cancel.		
		</p>
		
		<form name="eventTask" action="eventTask.cfm" method="post">
			<CFOUTPUT>
			<CF_INPUT TYPE="Hidden" NAME="frmCheckIDs" VALUE="#frmTotalChecks#">
			<CF_INPUT TYPE="Hidden" NAME="FlagID" VALUE="#FlagID#">
			<INPUT TYPE="Hidden" NAME="frmAction" VALUE="updatemaindetails">
			<CF_INPUT TYPE="Hidden" NAME="frmEventTable" VALUE="#frmEventTable#">
			<input type="hidden" name="confirmedUpdate" value="yes">
			</CFOUTPUT>

			<input type="submit" id="submitEventTask" value="Continue">
			<!--- <A HREF="JavaScript: document.eventTask.submit();"><IMG SRC="<CFOUTPUT></CFOUTPUT>/images/buttons/c_continue_e.gif" WIDTH=105 HEIGHT=21 BORDER=0 ALT="Continue"></A> --->
			&nbsp;&nbsp;&nbsp; <input type="button" id="closeEventTask" value="Cancel" onClick="JavaScript: window.close();"><!--- <A HREF="JavaScript: window.close();"><IMG SRC="<CFOUTPUT></CFOUTPUT>/images/buttons/c_cancel_e.gif" WIDTH=84 HEIGHT=21 BORDER=0 ALT="Cancel"></A> --->

		</form>
		
		</div>
	</cfif>
		
	

<cfelseif frmAction is "invcomm" or frmAction is "invallcomm">
	<cfif frmAction is "invallcomm">
		<cf_eventNotificationEmail action="compile" changes="Invitation sent to all Delegates">
	<cfelse>
		<cf_eventNotificationEmail action="compile" changes="Invitation sent to some/all Delegates">	
	</cfif>
	<cf_eventNotificationEmail action="send" FlagID="#FlagID#">

	
	<cf_head>
	<cf_title>Events - Lists</cf_title>
	</cf_head>
	<cf_body onLoad="JavaScript:document.eventSendComm.submit();">
	<FORM NAME="eventSendComm" ACTION="../communicate/commheader.cfm" METHOD="post">
	<CFOUTPUT>
		<CF_INPUT NAME="frmFlagID" TYPE="Hidden" VALUE="#FlagID#">
		<CF_INPUT NAME="frmPersonIDs" TYPE="Hidden" VALUE="#frmTotalChecks#">
		<INPUT NAME="frmRunInPopUp" TYPE="Hidden" VALUE="true">
		
		<!--- extra fields required to get the invitation email to get it sent automatically --->
		<!--- only want the invitation email --->
		<cfset emailType="Invitation">
		<cfinclude template="sql_getEmails.cfm">
		
		<input type="hidden" name="frmCommTypeLID" value="49">
		<!--- frmTitle is the subject of the email --->
		<CF_INPUT type="hidden" name="frmTitle" value="#getEmails.subject#">
		<!--- frmDescription is the "objective" of the email (make this the same as frmTitle) --->
		<CF_INPUT type="hidden" name="frmDescription" value="#getEmails.subject#">
		
		<CF_INPUT type="hidden" name="frmMsg" value="#getEmails.messagetext#">
		
		<input type="hidden" name="relfileFileCategory" value="InvitationEmailAttachment">
		<input type="hidden" name="relfileEntityType" value="event">
	</CFOUTPUT>
	</FORM>
	
	
	<CF_ABORT>
<CFELSEIF frmAction IS "Comm" or frmAction is "allcomm">
<!--- if sending a communication to some delegates then submit a form to the communicaion template --->
	<cfif frmAction is "allcomm">
		<cf_eventNotificationEmail action="compile" changes="Communication sent to all Delegates">
	<cfelse>
		<cf_eventNotificationEmail action="compile" changes="Communication sent to some/all Delegates">
	</cfif>

	<cf_eventNotificationEmail action="send" FlagID="#FlagID#">
	
	<cf_head>
	<cf_title>Events - Lists</cf_title>
	</cf_head>
	<cf_body onLoad="JavaScript:document.eventSendComm.submit();">
	<FORM NAME="eventSendComm" ACTION="../communicate/commheader.cfm" METHOD="post">
	<CFOUTPUT>
		<CF_INPUT NAME="frmFlagID" TYPE="Hidden" VALUE="#FlagID#">
		<CF_INPUT NAME="frmPersonIDs" TYPE="Hidden" VALUE="#frmTotalChecks#">
		<INPUT NAME="frmRunInPopUp" TYPE="Hidden" VALUE="true">
	</CFOUTPUT>
	</FORM>
	
	
	<CF_ABORT>
<CFELSEIF frmAction IS "down" or frmAction is "alldown">
	<CFQUERY NAME="GetEventDetails" datasource="#application.siteDataSource#">
		SELECT Title, Date
		FROM EventDetail
		WHERE FlagID =  <cf_queryparam value="#FlagID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</CFQUERY>
	
	<cf_head>
	<cf_title>Events - Lists</cf_title>
	</cf_head>
	<cf_body onLoad="JavaScript:document.eventDown.submit();">
	<FORM NAME="eventDown" ACTION="../download/downcreate.cfm" METHOD="post">
	<CFOUTPUT>
		<CF_INPUT NAME="frmFlagID" TYPE="Hidden" VALUE="#FlagID#">
		<CF_INPUT NAME="frmPersonIDs" TYPE="Hidden" VALUE="#frmTotalChecks#">
		<CF_INPUT NAME="frmTitle" TYPE="Hidden" VALUE="#GetEventDetails.Title# - #DateFormat(GetEventDetails.Date, 'dd-mmm-yyyy')#">
		<INPUT NAME="frmDescription" TYPE="Hidden" VALUE="Event Invitation">
		<INPUT NAME="frmRunInPopUp" TYPE="Hidden" VALUE="true">
		<INPUT NAME="frmProjectRef" TYPE="Hidden" VALUE="">
		<INPUT NAME="frmCommTypeLID" TYPE="Hidden" VALUE="0">
		<INPUT NAME="frmCommFormLID" TYPE="Hidden" VALUE="4">
		<INPUT NAME="frmNotes" TYPE="Hidden" VALUE="">
		<INPUT NAME="frmBtn" TYPE="Hidden" VALUE="download">
	</CFOUTPUT>
	</FORM>
	
	
	<CF_ABORT>
<CFELSEIF frmAction IS "add">
	<!--- these are the selections that the user has rights to that can be added to the event 
	note that it doesn't include selections already assigned to the event --->
	<!--- wab altered so that selections already assigned to the event can be assigned again  - users tend to add more people to a selection and then need to  add those people to the event--->
	<!--- WAB 2005-05-17 altered to get selections from cfc --->	
	<cfset selAvailable = application.com.selections.getSelections()>
	
<!--- 	<CFQUERY NAME="selAvailable" datasource="#application.siteDataSource#">
		SELECT s.SelectionID,
				ltrim(s.Title) as title,
				s.Description,
				s.Created AS dateCreated,
				s.RecordCount AS theRecordCount
	  	  FROM Selection AS s, SelectionGroup AS sg, Person AS p, UserGroup AS ug
		 WHERE sg.SelectionID = s.SelectionID
		   AND sg.UserGroupID = #request.relayCurrentUser.usergroupid#
		   AND ug.UserGroupID = s.CreatedBy
		   AND p.PersonID = ug.PersonID
<!--- 		   AND s.SelectionID NOT IN (SELECT SelectionID FROM EventSelections WHERE FlagID = #FlagID#) --->
	   ORDER BY s.Title
	</CFQUERY> --->
	<!--- these are the selections already assigned to the event --->
	<CFQUERY NAME="selAssigned" datasource="#application.sitedatasource#">
		SELECT LTrim(s.Title) AS Title
		FROM Selection AS s, EventSelections AS es
		WHERE es.FlagID =  <cf_queryparam value="#FlagID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		AND es.SelectionID = s.SelectionID
	</CFQUERY>
	
	<cf_head>
		<cfif session.eventWizard is not "true">
			<cf_title>Events - Add Delegates</cf_title>
		</cfif>
		<SCRIPT type="text/javascript">
		//<![CDATA[
		function closeWindow() {	
			document.myForm.submit();
			<CFIF FindNoCase("msie", cgi.http_user_agent) GT 0>
				window.close();
			<CFELSE>
				setInterval("window.close()", 1000);
			</CFIF>
		}
		function checkForm() {
			var form = document.UpdateEvent
			var pass = 1;
			if (form.Selections.selectedIndex == -1) {
				pass = 0;
			}
			if (pass == 0) {
				alert("\nYou must select at least one selection to add to the event.");
			} else {
				form.submit();
			}
		}
		//]]>
		</SCRIPT>	
	</cf_head>	
	
	<DIV ALIGN="center">
	<FORM NAME="UpdateEvent" ACTION="EventAddDelegates.cfm" METHOD="post">
	<input type="hidden" name="dirty" value="0">
	<CFOUTPUT>
	<CF_INPUT TYPE="hidden" NAME="frmEventTable" VALUE="#frmEventTable#">
	<CF_INPUT TYPE="hidden" NAME="FlagID" VALUE="#FlagID#">
	<INPUT TYPE="Hidden" NAME="frmAction" VALUE="Add">
	</CFOUTPUT>
	<TABLE CELLPADDING="2" CELLSPACING="2" BORDER="0">
	<TR>
		<TD WIDTH="150" CLASS="Label" VALIGN="top">Selections Included</TD>
		<TD WIDTH="20">&nbsp;</TD>
		<TD WIDTH="150" CLASS="Label" VALIGN="top">Selections Available</TD>
	</TR>
	<TR>	
		<TD VALIGN="top">
			<CFIF selAssigned.RecordCount GT 0>
				<CFOUTPUT QUERY="selAssigned">#htmleditformat(Title)#<BR></CFOUTPUT>
			<CFELSE>
				No Selections currently assigned to this event.
			</CFIF>
		</TD>
		<TD WIDTH="20">&nbsp;</TD>
		<TD VALIGN="top">
			<CFIF selAvailable.RecordCount GT 0>
				<SELECT NAME="Selections" MULTIPLE SIZE="10">
					<CFOUTPUT QUERY="selAvailable"><OPTION VALUE="#SelectionID#">#DateFormat(DateCreated, "dd-mmm-yyyy")# - #htmleditformat(Title)# - #htmleditformat(theRecordCount)#</CFOUTPUT>
				</SELECT>
			<CFELSE>
				No Selections Available
			</CFIF>
		</TD>
	</TR>
	<TR>
		<TD COLSPAN="3" ALIGN="center">
			<CFIF selAvailable.RecordCount GT 0>
				<P><input type="button" id="checkFormBtn" value="Save" onClick="JavaScript:checkForm();">
				<!--- <A HREF="JavaScript:checkForm();"><CFOUTPUT><IMG SRC="/images/buttons/c_save_e.gif" BORDER="0"></CFOUTPUT></A> --->
				&nbsp;&nbsp; <input type="button" id="closeFormBtn" value="Close" onClick="JavaScript:window.close();"><!--- <A HREF="javascript: window.close()">Cancel</A> --->
				</FORM>
			<CFELSE>
				</FORM>
				<FORM NAME="myForm" ACTION="eventDelegateList.cfm" TARGET="main" METHOD="post">
				<CFOUTPUT>
				<CF_INPUT TYPE="Hidden" NAME="FlagID" VALUE="#FlagID#">
				</CFOUTPUT>
				</FORM>
				You do not have any selections that can be added to this event.
				<P>
				<A HREF="javascript: window.close()">Close Window</A>
			</CFIF>
		</TD>
	</TR>
	</TABLE>
	<cfif session.eventWizard is not "true">
	
	
		<CF_ABORT>
	</cfif>	


<CFELSEIF left(frmAction,9) IS "selection">
	<!--- 2000-10-09WAB added selection functions --->
		<CFSET frmpersonids=frmTotalChecks>
		<CFSET frmtask=replacenocase(frmAction,"Selection","","ONE")>
		<CFSET frmruninpopup=true>

		<CFINCLUDE template="../selection/selectalter.cfm">

	
<cfelseif frmAction is "eventDown">

	<!--- creates a download file of all the ids in frmCheckIDS --->
	<cfinclude template="eventDownloadDelegates.cfm">


<!--- NJH 2008/09/26 elearning 8.1 - if an event has a module tied to it, then we give the option of grading students. Set their userModuleProgress to pass or fail --->
<cfelseif frmAction is "gradeStudents">

	<cfquery name="GetModuleStatuses" datasource="#application.siteDataSource#">
		select statusID,description from trngUserModuleStatus
			where statusTextID not in ('InProgress')
		order by description
	</cfquery>
	
	
	
	<cf_head>
	<cf_title>Events - Lists</cf_title>
	</cf_head>
	
	
		<cfset request.relayFormDisplayStyle="HTML-table">
		<cfform name="gradeStudentsForm" method="post" action="eventTaskAction.cfm">
			<cf_relayFormDisplay>
				<cf_relayFormElementDisplay relayFormElementType="hidden" fieldname="frmPersonIDs" currentValue="#frmTotalChecks#" label="">
				<cf_relayFormElementDisplay relayFormElementType="hidden" fieldname="frmAction" currentValue="#frmAction#" label="">
				<cf_relayFormElementDisplay relayFormElementType="hidden" fieldname="flagID" currentValue="#flagID#" label="">
				<cf_relayFormElementDisplay relayFormElementType="select" fieldname="frmStatusID" currentValue="" label="Module Status" query="#GetModuleStatuses#" display="description" value="statusID">
				<cf_relayFormElementDisplay relayFormElementType="submit" fieldname="frmGradeStudents" currentValue="Submit" label="">
			</cf_relayFormDisplay>
		</cfform>
	
	


<cfelse >

	<CFQUERY NAME="getFlagTextID" datasource="#application.siteDataSource#">
	SELECT FlagTextID, FlagGroupTextID
	FROM Flag, flagGroup
	WHERE FlagID =  <cf_queryparam value="#FlagID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	AND flag.flaggroupid = flaggroup.flaggroupid 
	</CFQUERY>

	<CFQUERY NAME="getValidValues" datasource="#application.siteDataSource#">
		SELECT ValidValue
		FROM ValidFieldValues
		WHERE FieldName =  <cf_queryparam value="flag.#getFlagTextID.flagTextID#.#frmAction#" CFSQLTYPE="CF_SQL_VARCHAR" > 
	</CFQUERY>


	<CFIF getValidValues.RecordCount EQ 0 >
		<!--- Look for valid values associated with parent flaggroup --->
		<CFQUERY NAME="getValidValues" datasource="#application.siteDataSource#">
			SELECT ValidValue
			FROM ValidFieldValues
			WHERE FieldName =  <cf_queryparam value="flagGroup.#getFlagTextID.flagGroupTextID#.#frmAction#" CFSQLTYPE="CF_SQL_VARCHAR" > 
		</CFQUERY>
	</cfif>
		
	
	
	<cf_head>
	<cf_title>Events - Lists</cf_title>
	</cf_head>
	
	<DIV ALIGN="center">
	<CFIF getValidValues.RecordCount EQ 0 AND frmAction IS NOT "delete">
		An error has occured and this procces will now terminate.
		<P>The error is:
		<BR>No valid values have been set for this event.
		<P><A HREF="javascript: window.close()">Close Window</A>
		<CF_ABORT>
	<CFELSE>
		<FORM NAME="eventTask" ACTION="eventTaskAction.cfm" METHOD="post">
		
		<CFIF frmAction IS "delete">
			Are you sure you want to permanently delete these people from the registration process?
			<P>
			Note that only delegates with a Registration Status of 'To Be Invited' will be deleted.
		<CFELSE>
			<CFOUTPUT>
			To batch update the value of <B>#htmleditformat(frmAction)#</B> for all the people you checked, 
			select a value from the list below and click continue.
			</CFOUTPUT>
			<P>
			
			<SELECT NAME="frmValue">
				<CFOUTPUT QUERY="getValidValues">
					<OPTION VALUE="#ValidValue#">#htmleditformat(ValidValue)#
				</CFOUTPUT>
			</SELECT>
		</CFIF>
		
		<CFOUTPUT>
		<CF_INPUT TYPE="Hidden" NAME="frmCheckIDs" VALUE="#frmTotalChecks#">
		<CF_INPUT TYPE="Hidden" NAME="FlagID" VALUE="#FlagID#">
		<CF_INPUT TYPE="Hidden" NAME="frmAction" VALUE="#frmAction#">
		<CF_INPUT TYPE="Hidden" NAME="frmEventTable" VALUE="#frmEventTable#">
		</CFOUTPUT>
		
		<P>
		<input type="submit" id="submitEventTask" value="Submit">
		<!--- <A HREF="JavaScript: document.eventTask.submit();"><IMG SRC="<CFOUTPUT></CFOUTPUT>/images/buttons/c_continue_e.gif" WIDTH=105 HEIGHT=21 BORDER=0 ALT="Continue"></A> --->
		<CFIF frmAction IS "delete">
			&nbsp;&nbsp;&nbsp; <input type="button" id="cancelEventTask" value="Cancel" onClick="JavaScript: window.close();"><!--- <A HREF="JavaScript: window.close();"><IMG SRC="<CFOUTPUT></CFOUTPUT>/images/buttons/c_cancel_e.gif" WIDTH=84 HEIGHT=21 BORDER=0 ALT="Cancel"></A> --->
		</CFIF>
		
		</FORM>
	</CFIF>
	</DIV>
	
	





</cfif>


