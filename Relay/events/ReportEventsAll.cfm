<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			ReportEventsAll.cfm	
Author:				AJC
Date started:		2006-05-02
	
Description:		Report to show event, team and number of members in team

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2012-10-04	WAB		Case 430963 Remove Excel Header 

Still To Do
===========

Possible enhancements:

 --->

<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">
<cfparam name="openAsExcel" type="boolean" default="false">

	<cf_head>
		<cf_title>Event Management</cf_title>
	</cf_head>
	
	
	<cfinclude template="eventSubTopHead.cfm">

<cfparam name="sortOrder" default="eventtitle">
<cfparam name="numRowsPerPage" default="50">
<cfparam name="startRow" default="1">
<cfparam name="dateFormat" type="string" default="">

<cfparam name="keyColumnList" type="string" default="eventteamname"><!--- this can contain a list of columns that can be edited --->
<cfparam name="keyColumnURLList" type="string" default="ReportEventsTeam.cfm?EventTeamID="><!--- this can contain a list of matching columns that contain the URL of the editor template --->
<cfparam name="keyColumnKeyList" type="string" default="eventteamid"><!--- this can contain a list of matching key columns e.g. primary key --->

<cfparam name="checkBoxFilterName" type="string" default="">
<cfparam name="checkBoxFilterLabel" type="string" default="">


<!--- This bit of code works out all the flags which are associated with eventTeams and builds a query to bring back all the values
		it could be adjusted to just bring back certain flags/flaggroups
		This uses code being developed by WAB for dynamically joining flag tables, still a bit under development!

 --->

		<!--- get all the groups of eventTeam flags --->
		<CFQUERY NAME="eventTeamFlags" DATASOURCE="#application.SiteDataSource#">
		select * from flagGroup where entityTypeID =  <cf_queryparam value="#application.entityTypeID["eventTeam"]#" CFSQLTYPE="CF_SQL_INTEGER" >  and parentflaggroupid = 0
		</CFQUERY>

		
		<cfset flagjoins = application.com.flag.getJoinInfoForListOfFlagGroups(valueList(eventTeamFlags.flagGroupID))>
		<CFQUERY NAME="getEvents" DATASOURCE="#application.SiteDataSource#">
			select 
					<cfloop index = i from = "1" to = "#arrayLen(flagjoins)#">
						#flagjoins[i].selectField# as  #flagjoins[i].alias# ,
					</cfloop> 
					e.* 
			from vEventTeamNumbers e
						<cfloop index = i from = "1" to = "#arrayLen(flagjoins)#">
							#flagjoins[i].join# 
						</cfloop>
			where 1=1
			<cfinclude template="../templates/tableFromQuery-QueryInclude.cfm">
			order by #htmleditformat(sortOrder)# 
		</CFQUERY  > 

		<!--- get names of the flags/groups for doing column names --->
		<cfset flagcolumnNameList = "">
		<cfset flagcolumnAliasList = "">
		<cfloop index = i from = "1" to = "#arrayLen(flagjoins)#">
			<cfset flagcolumnNameList = listappend(flagcolumnNameList,flagjoins[i].name)>
			<cfset flagcolumnAliasList = listappend(flagcolumnAliasList,flagjoins[i].alias)>
		</cfloop> 



<cfoutput>
<table width="100%" cellpadding="3">
	<tr>
		<td><strong>Event Teams</strong></td>
		<td align="right">&nbsp;</td>
	</tr>
	<tr><td colspan="2">&nbsp;</td></tr>
</table>
</cfoutput>

<cf_translate>
<CF_tableFromQueryObject 
	queryObject="#getEvents#"
	queryName="getEvents"
	sortOrder = "#sortOrder#"
	openAsExcel = "#openAsExcel#"
	numRowsPerPage="#numRowsPerPage#"
	startRow="#startRow#"
	
	keyColumnList="#keyColumnList#"
	keyColumnURLList="#keyColumnURLList#"
	keyColumnKeyList="#keyColumnKeyList#"
	
 	hideTheseColumns=""
	ShowTheseColumns="eventtitle,eventteamname,Team_Members,#flagcolumnAliasList#"
	ColumnHeadingList="Event,Team,Team Members,#flagcolumnNameList#"
	
	dateFormat=""
	FilterSelectFieldList="eventtitle,eventteamname,Team_Members"
	FilterSelectFieldList2=""
	GroupByColumns=""
	radioFilterLabel=""
	radioFilterDefault=""
	radioFilterName=""
	radioFilterValues=""
	checkBoxFilterName=""
	checkBoxFilterLabel=""
	allowColumnSorting="no"
>
</cf_translate>



