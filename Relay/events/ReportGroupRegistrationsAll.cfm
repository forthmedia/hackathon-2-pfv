<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
2012-10-04	WAB		Case 430963 Remove Excel Header 
 --->
<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">
<cfparam name="openAsExcel" type="boolean" default="False">

	<cf_head>
		<cf_title>Event Management</cf_title>
	</cf_head>
	
	
	<cfinclude template="eventSubTopHead.cfm">

<cfparam name="sortOrder" default="Name">
<cfparam name="numRowsPerPage" default="100">
<cfparam name="startRow" default="1">
<cfparam name="dateFormat" type="string" default="Reg_Date">

<cfparam name="keyColumnList" type="string" default="Name"><!--- this can contain a list of columns that can be edited --->
<cfparam name="keyColumnURLList" type="string" default="../data/dataframe.cfm?frmsrchPersonID="><!--- this can contain a list of matching columns that contain the URL of the editor template --->
<cfparam name="keyColumnKeyList" type="string" default="PersonID"><!--- this can contain a list of matching key columns e.g. primary key --->

<cfparam name="checkBoxFilterName" type="string" default="">
<cfparam name="checkBoxFilterLabel" type="string" default="">

<cfparam name="ReportGroupRegAll" type="string" default="No">
<cfparam name="ReportGroupRegAlleventGroupIDs" type="string" default="">

<cfquery name="getEventReg" datasource="#application.SiteDataSource#">
	select 
		personID, 
		isnull(First_Name,' ') + ' ' + isnull(last_Name,' ') as Name,
		Email,
		EventGroupID,
		Payment_Type,
		Payment_Received,
		Reg_Date,
		Reg_Status,
		Event,
		Race,
		Auth_code,
		personCount as Person_Count
	from vEventReport
	WHERE 1=1
	<cfif isDefined("ReportGroupRegAlleventGroupIDs") and ReportGroupRegAll eq "Yes">
	 	and EventGroupID  in ( <cf_queryparam value="#ReportGroupRegAlleventGroupIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	</cfif>
	<cfinclude template="../templates/tableFromQuery-QueryInclude.cfm">
	<cfif (isdefined("FORM.Payment_Received") and FORM.Payment_Received eq "1")>
	and Payment_Received = 1
	</cfif>
	
	
	order by <cf_queryObjectName value="#sortOrder#"> 
</cfquery>

<cfoutput>
<table width="100%" cellpadding="3">
	<tr>
		<td><strong>All Registrations for Events.</strong></td>
		<td align="right">&nbsp;</td>
	</tr>
	<tr><td colspan="2">&nbsp;</td></tr>
</table>
</cfoutput>

<cf_translate>
<CF_tableFromQueryObject 
	queryObject="#getEventReg#"
	queryName="getEventReg"
	sortOrder = "#sortOrder#"
	openAsExcel = "#openAsExcel#"
	numRowsPerPage="#numRowsPerPage#"
	startRow="#startRow#"
	
	keyColumnList="#keyColumnList#"
	keyColumnURLList="#keyColumnURLList#"
	keyColumnKeyList="#keyColumnKeyList#"
	
 	hideTheseColumns="EventGroupID,Email,Auth_code"
	ShowTheseColumns="personID,Name,Reg_Date,Race,Payment_Type,Payment_Received,Reg_Status,Person_Count"
	
	dateFormat="dateFormat"
	NumberFormat="Person_count"
	FilterSelectFieldList="Race"
	FilterSelectFieldList2="Reg_Status"
	
	GroupByColumns="Race"
	totalTheseColumns="Person_Count"
	
	radioFilterLabel=""
	radioFilterDefault=""
	radioFilterName=""
	radioFilterValues=""
	checkBoxFilterName="Payment_Received"
	checkBoxFilterLabel="Payment Received?"
	allowColumnSorting="yes"
>
</cf_translate>
<p></p>


