<!--- �Relayware. All Rights Reserved 2014 --->

<cfparam name="attributes.pageTitle" type="string" default="Events">
<cfparam name="attributes.pagesBack" type="string" default="1">
<cfparam name="attributes.thisDir" type="string" default="">

<CF_RelayNavMenu pageTitle="#attributes.pageTitle#" thisDir="#attributes.thisDir#">
	<CFIF listFindNoCase("eventDetails.cfm,eventWebContent.cfm,eventManager.cfm,eventDelegateList.cfm",listLast(SCRIPT_NAME,"/"))>
		<CF_RelayNavMenuItem MenuItemText="List Events" CFTemplate="/events/eventList.cfm">
	</CFIF> 
</CF_RelayNavMenu>