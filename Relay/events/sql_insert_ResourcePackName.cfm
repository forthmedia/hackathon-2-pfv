<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File:			sql_insert_ResourcePackName.cfm
Author:			DJH
Date created:	1 February 2000

Description:

Version history:
1

--->

<cfquery name="insert_ResourcePackName" datasource="#application.siteDataSource#">
insert into ResourcePackName
(
	ResourcePackName
)
values
(
	<cf_queryparam value="#frmResourcePackName#" CFSQLTYPE="CF_SQL_VARCHAR" >
)
</cfquery>