<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File:			EventHome.cfm
Author:			DJH
Date created:	27 January 2000

Description:
Displays a list of selectable events

Version history:
1
2008/10/24	NJH	Sophos RelayWare Set Up Bug Fix Issue 1102 - display the count of those invited,registered and attending
2009/05/07  SSS S-01028 Added a title to make it easier to use the eventlist screen
2012/11/12  YMA CASE:431820 - changed to remove sub-tables to improve formatting
2016/01/29	NJH	Replaced with TFQO.
--->

<cf_title>Events List</cf_title>

<cfset ShowSave = "no">
<cfinclude template="eventtophead.cfm">

<CFIF GetEvent.RecordCount GT 0>

	<cfset showTheseColumns = "title,location,VenueRoom,PreferredStartDate,Owner,ISOCode">
	<cfif session.AttendeeStats>
		<cfset showTheseColumns = listAppend(showTheseColumns,"EstAttendees,noInvited,noRegistered,actualAttendees")>
	</cfif>
	<cfset showTheseColumns = listAppend(showTheseColumns,"EventStatus")>

	<CF_tableFromQueryObject
			queryObject=#GetEvent#
			keyColumnList="title"
			keyColumnKeyList="flagId"
			keyColumnURLList=" "
			keyColumnOnClickList="javascript:void(assignSelect(document.tophead.FlagID*comma ##FlagID##));return false;"
			showTheseColumns="#showTheseColumns#"
			dateFormat="PreferredStartDate"
			useInclude="false"
			translateColumns=true
			ColumnTranslationPrefix="phr_eventList_"
		>
<form name="UpdateEvent" action="" method="post">
		<input type="hidden" name="dirty">
	</form>
	<!--- YMA 2012/11/12 CASE:431820 - changed to remove sub-tables to improve formatting --->
 	<!--- <TABLE class="eventListTable table table-hover table-striped withBorder">
	<form name="UpdateEvent" action="" method="post">
		<input type="hidden" name="dirty">
	</form>
	<CFOUTPUT QUERY="GetEvent" GROUP="EventGroupID">
	<cfset row=0>
	<TR>
		<TH COLSPAN="12" ALIGN="LEFT">#htmleditformat(EventName)#</TH>
	</TR>
	<TR>
	<!--- SSS 2009/05/07 S-01028 added a title --->
		<TD WIDTH="400" class="Label"><B>Title</B></TD>
		<TD WIDTH="400" class="Label"><B>City</B></TD>
		<TD WIDTH="400" class="Label"><B>Venue</B></TD>
		<TD WIDTH="400" class="Label"><B>Date</B></TD>
		<TD WIDTH="300" class="Label"><B>Country</B></TD>
		<!---<TD WIDTH="300" class="Label"><B>Date</B></TD>	--->
		<TD WIDTH="100" class="Label"><B>Owner</B></TD>
		<cfif session.AttendeeStats>
			<TD WIDTH="100" class="Label"><B>Est no of<br> attendees</B></TD>
			<TD WIDTH="100" class="Label"><B>No invited</B></TD>
			<TD WIDTH="100" class="Label"><B>No registered</B></TD>
			<TD WIDTH="100" class="Label"><B>Actual attendees</B></TD>
		</cfif>
		<TD WIDTH="100" class="Label"><B>Status</B></TD>
	</TR>
		<CFOUTPUT><!---group="FlagID"--->
			<cfset row=row+1>
			<cfif row mod 2 is 0>
				<cfset bgcol="FEFFD7">
			<cfelse>
				<cfset bgcol="white">
			</cfif>
			<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
				<!--- SSS 2009/05/07 S-01028 added a title --->
				<TD WIDTH="300"><a class="smallLink" href="javascript:void(assignSelect(document.tophead.FlagID, #FlagID#))">#htmleditformat(title)#</a>&nbsp;</TD>
				<TD WIDTH="300"><a class="smallLink" href="javascript:void(assignSelect(document.tophead.FlagID, #FlagID#))">#htmleditformat(location)#</a>&nbsp;</TD>
				<TD WIDTH="300"><a class="smallLink" href="javascript:void(assignSelect(document.tophead.FlagID, #FlagID#))">#htmleditformat(VenueRoom)#</a>&nbsp;</TD>
				<TD WIDTH="300">#dateformat(PreferredStartDate,"dd-mmm-yyyy")#</TD>
				<TD WIDTH="300">#htmleditformat(ISOCode)#</TD>
				<TD WIDTH="100">#htmleditformat(FirstName)# #htmleditformat(LastName)# #htmleditformat(OfficePhone)#</TD>
				<!--- NJH 2008/10/24 Sophos RelayWare Set Up Bug Fix Issue 1102 - am returning the count of those invited,registered and attending. It was previously a '&nbsp;' --->
				<cfif session.AttendeeStats>
					<TD WIDTH="100">#htmleditformat(EstAttendees)#&nbsp;</TD>
					<TD WIDTH="100">#htmleditformat(noInvited)#</TD>
					<TD WIDTH="100">#htmleditformat(noRegistered)#</TD>
					<TD WIDTH="100">#htmleditformat(actualAttendees)#</TD>
				</cfif>
				<TD WIDTH="100">#htmleditformat(EventStatus)#&nbsp;</TD>
			</TR>
			<tr></tr>
		</CFOUTPUT>
	</CFOUTPUT>
	</TABLE> --->
<CFELSE>
	<H2>There are currently no Events that you have permission to view.</H2>
</CFIF>

<cfinclude template="eventFooter.cfm">