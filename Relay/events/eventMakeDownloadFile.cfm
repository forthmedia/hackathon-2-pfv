<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 

eventMakeDownloadFile.cfm

Author:  WAB

Date  2000-04-05

Purpose:

To put the download file creation into one place - at the moment it happens in two separate places


Parameters:
query getLists  (required)  
downloadStart  (optional)   start row
downloadEnd		(optional)  end row


Mods
WAB 2001-06-11  Added start and end parameters (optional)
			added address details to the download 

--->

 

<!---Set variables for the file Download--->
<CF_TemporaryFileName FileName="DelegateList2" Extension = "xls">
<CFSET statFilePath = dosFilePath>
<CFSET webstatFilePath = webFilePath>

<CFPARAM name="downloadStart" default="1">
<CFPARAM name="downloadEnd" default="#getLists.recordCount#">



<!---Column Headings for the Download File--->
<CFSET ColumnHeadings = 'PersonID	Title	First  Name	Last Name	Email	Partner Type	Site	Address1	Address2	Address3	Town	Address5	Postal Code	Country	Language	Telephone	Mobile Phone	Fax Phone	Status	Course Content	Wave	Registration Number	Category	Arrive	Depart	RegistrationDate'>
<CFSET delim = "#Chr(9)#">
<!---Data to write in the Download File--->
<CFSET List = '"##EntityID####delim####Title####delim####FirstName####delim####LastName####delim####Email####delim####PartnerType####delim####SiteName####delim####Address1####delim####Address2####delim####Address3####delim####Address4####delim####address5####delim####PostalCode####delim####ISOCode####delim####AssignedLanguage####delim####eTelephone####delim####eMobile####delim####eFax####delim####RegStatus####delim####iif(len(AssignedCourseContent) is 0,DE(CourseContent),DE(AssignedCourseContent))####delim####iif(len(AssignedWave) is 0,DE(Wave),DE(AssignedWave))####delim####PartnerRegID####Delim####employeeCat####delim####dateformat(PartArrive,"dd-mm-yy")####delim####dateformat(PartDepart,"dd-mm-yy")####Delim####dateformat(ConfirmationSent,"dd-mm-yy")## ##timeformat(ConfirmationSent,"HH:mm")##"'>

<!--- Open file and write column headings row --->
<CFFILE ACTION="WRITE" FILE="#StatFilePath#" OUTPUT="#ColumnHeadings#">


<CFLOOP query="getLists" startRow="#downloadStart#" EndRow="#downloadEnd#" >
		<!--- Write data line to file--->
		<CFSET thisLine = evaluate("#List#")>
		<CFFILE ACTION="APPEND" FILE="#StatFilePath#" OUTPUT="#thisLine#">
</CFLOOP>



