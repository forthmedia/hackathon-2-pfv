<!--- �Relayware. All Rights Reserved 2014 --->



<cf_head>
	<cf_title>Organisation Allocation Help</cf_title>
</cf_head>


<FONT SIZE=2><P>Please note:  ALL REGIONAL ALLOCATION MUST BE COMPLETED BY THE CLOSE OF BUSINESS THURSDAY, 25 FEB.  REGISTRATIONS WILL NOT BE ACCEPTED FROM YOUR REGION UNTIL YOU HAVE COMPLETED YOUR REGIONAL ALLOCATION.</P>

<P>While we can guarantee your region the allocation we have offered, please remember PARTNER REGISTRATIONS WILL BE ACCEPTED ON A FIRST-COME, FIRST-SERVED BASIS.  It is to your partners advantage to register early to guarantee their wave and language preferences.</P>

<P>&nbsp;</P>
<P>STEP 3: ORGANISATION ALLOCATIONS</P>

<P>The registration system for 3Com University April 1999 has been developed to allow you to do the following:</P>

<UL>
<LI>Send out an unlimited number of invitations</LI>
<LI>Limit the number of Sales and Pre-Sales places for each company</LI>
<LI>Specify specific individuals from each company to receive a place</LI></UL>


<P>&nbsp;</P>
<P>To access the &quot;Organisation Allocation&quot; page on 3Contact:</P>

<OL>

<LI>Go to </FONT><A HREF="http://www.3contact.com/"><FONT SIZE=2>www.3contact.com</FONT></A><FONT SIZE=2> and log in using your user id and password.</LI>

<LI>From the &quot;Welcome to 3Contact Screen&quot;, choose the &quot;Reports&quot; button on the left side of the screen.</LI>

<LI>On the &quot;List of Available Reports&quot;, under the 3Com University heading, choose &quot;Organisation Allocation&quot;.  Now you are looking at the organisation allocation screen for your region.</LI>

<LI>Scroll down the screen until you reach the column headings &quot;Organisation&quot;, &quot;Sales Places&quot;, &quot;Pre-Sales Places&quot;, etc.  These headings are defined as follows:</LI></OL>
<DIR>

<B><P>&quot;Organisation&quot;</B> = Company</P>
<B><P>&quot;Sales Places&quot;</B> = the number of sales places you wish to allocate to that company</P>
<B><P>&quot;Pre-Sales Places&quot;</B> = the number of pre-sales places you wish to allocate to that company</P>
<B><P>&quot;On Invite List&quot;</B> = the number of people, from that company, that are on your invitee list who's invitations have not yet been sent.</P>
<B><P>&quot;Invitation Sent&quot;</B> = the number of people, from that company, that have been sent an invitation.</P>
<B><P>&quot;Preferred Attendees&quot;</B> = the number of places, from that company, that you have assigned to SPECIFIC individuals.  This will be used by TEAM when they have more registrations than places allocated.</P>
<B><P>&quot;Registered&quot;</B> = the number of people, from that company, that have been confirmed a place.  This is set automatically when Team confirm each attendees place on 3ComU.</P>
<B><P>&quot;Details&quot;</B> = When you click on &quot;Details&quot; you will see all the people from that organisation who are in the 3Contact system.  You are able to see which people from that company are on your invitee list.  To add people from that company to your invitee list, place a check in the box in the &quot;On Invitee List&quot; column.  You are able to specify anyone on the invitee list as a &quot;Preferred Attendee&quot; by placing a check in the box in the &quot;Preferred Attendee&quot; column.</P>
<B><P>&quot;Total Num. People on 3Contact&quot;</B> = the number of people, from that organisation, that have records in the 3Contact system.  If you want to add more simple select the Data button on the left button panel, find the site and add additional people.</P>
</DIR>

<OL START=5>

<LI>Scroll to the bottom of the page to see the option to navigate to the organisations you want by First letter of their name.</LI></OL>


<B><P>To assign the number of spaces to each company:</P>
<OL>

</B><LI>Scroll down to that company's row.</LI>
<LI>Enter the number of Sales places you want them to have in the box under &quot;Sales Places&quot;.</LI>
<LI>Enter the number of Pre-Sales places you want them to have in the box under &quot;Pre-Sales Places.&quot;</LI>
<LI>The system will not allow you to allocate more spaces for organisations than the total for your region.  A running total is kept in the top panel of the screen.</LI>
<LI>Save changes after you make them. </LI></OL>


<B><P>To assign specific people from a company a place:</P>
<OL>

</B><LI>Click on the &quot;Details&quot; button in that company's row.</LI>
<LI>Find the person you want to place on the &quot;Preferred Attendee&quot; list and place a check in the &quot;Preferred Attendee&quot; box in that person's row.  Because he is a &quot;Preferred Attendee&quot;, this person will be allocated a place when he registers.</LI>
<LI>To save your changes click on the Save Button.</LI></OL>


<P>To merge duplicate organisations into one</P>
<OL>

<LI>Tick the 'Check' box next to each of the organisation records that you want to merge</LI>
<LI>Scroll to the top of the screen and </LI></OL>


<B><P>EXAMPLE</P>
</B>
<P>There are 7 people in the 3Contact system for TEAM Events. I want to assign TEAM 2 sales places and 3 pre-sales places and I do not care who from TEAM attends 3Com University.</P>

<P>Step 1:  &#9;Selecting an Invitee List</P>
<P>I have added all 7 people to my invitation list.</P>

<P>Step 2:  &#9;Sending the Invitation</P>
<P>I have sent all 7 people an invitation.</P>

<P>Step 3: Organisation Allocations</P>
<OL>

<OL>

<LI>Find the organisation TEAM on the Organisation Allocation screen.  In the Sales box I would type a 3 and in the Pre-Sales box I would type a 2.</LI></OL>
</OL>
<DIR>
<DIR>

<P>This tells the registration centre that</P></DIR>
</DIR>

<OL>

<OL>

<LI>You want TEAM to have 5 places (2 Sales and 3 Pre-Sales)</LI>
<LI>You don't care who from TEAM attends.</LI></OL>
</OL>


<P>If you want to specify that Jody McMurry and Lucie Smith from TEAM attend you would:</P>
<OL>

<OL>

<LI>Click on the &quot;Details&quot; button in the row for TEAM.</LI>
<LI>Mark Jody McMurry and Lucie Smith as &quot;Preferred Attendees&quot; by placing a check in the &quot;Preferred Attendee&quot; column.</LI></OL>
</OL>
<DIR>
<DIR>

<P>This tells the registration centre that</P></DIR>
</DIR>

<OL>

<OL>

<LI>You want TEAM to have 5 places (2 Sales and 3 Pre-Sales)</LI>
<LI>You want Jody McMurry and Lucie Smith to attend.</LI>
<LI>You don't care who from TEAM takes the other 3 places you have allocated to TEAM.</LI></OL>
</OL>
</FONT>





