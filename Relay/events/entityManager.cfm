<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File:			entityManager.cfm
Author:			DJH
Date created:	14 March 2000

Description:	Simply includes the eventManager template - 
				because the entityManager template was not called by form submission, 
				but included in eventManager, the form submissions in ../templates/entityManager wont work.

Version history:
1

--->

<cfinclude template="eventManager.cfm">