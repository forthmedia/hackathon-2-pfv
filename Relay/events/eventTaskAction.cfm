<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		eventTaskAction.cfm	
Author:			  
Date started:	
	
Description:			

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2008-09-26			NJH			elearning 8.1 - added functionality to set the user module progress records to pass/fail
2012-09-17			IH			Case 430710 add lastUpdatedByPerson and LastUpdatedBy columns to updateEventData query					
2013-02-07 			PPB 		Case 433635 when grading students insert a userModuleProgress row (for the associated module) if there isn't one (or update and existing one) 

Possible enhancements:
 --->


<CFIF NOT IsDefined("frmAction")>
	<cflocation url="eventHome.cfm"addToken="false">
	<CF_ABORT>
</CFIF>


<!--- NJH 2008-09-26 elearning 8.1 --->
<CFIF frmAction is "gradeStudents">
	<!--- START 2013-02-07 PPB Case 433635 replaced this section 
	<cfquery name="getUserModuleProgressIDs" datasource="#application.siteDataSource#">
		select userModuleProgressID from eventFlagData where flagID =  <cf_queryparam value="#flagID#" CFSQLTYPE="CF_SQL_INTEGER" >  and entityID  in ( <cf_queryparam value="#frmPersonIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			and userModuleProgressID is not null
	</cfquery>
	
	<cfscript>
		argumentStruct = structNew();
		argumentStruct.frmStatusID = frmStatusID;
	</cfscript>
	
	<cfloop query="getUserModuleProgressIDs">
		<cfscript>
			application.com.relayElearning.updateUserModuleProgress(userModuleProgressID=userModuleProgressID,argumentsStruct=argumentStruct);
		</cfscript>
	</cfloop>
 	END 2013-02-07 PPB Case 433635 --->	

	<!--- START 2013-02-06 PPB Case 433635 get the moduleId in case any of the checked persons don't have a userModuleProgressID yet and we will need to create one --->
	<cfquery name="qryEventModule" datasource="#application.siteDataSource#">
		select moduleId from eventDetail where flagID =  <cf_queryparam value="#flagID#" CFSQLTYPE="CF_SQL_INTEGER"> 
	</cfquery>
	<cfset frmModuleID = qryEventModule.moduleId>

	<cfquery name="qryDelegates" datasource="#application.siteDataSource#">
		select entityId AS personId,IsNull(userModuleProgressID,0) AS userModuleProgressID from eventFlagData where flagID =  <cf_queryparam value="#flagID#" CFSQLTYPE="CF_SQL_INTEGER" >  and entityID  in ( <cf_queryparam value="#frmPersonIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	</cfquery>
	
	<cfloop query="qryDelegates">
		<cfset frmPersonID = qryDelegates.personId>
		<cfset userModuleProgressID = qryDelegates.userModuleProgressID>
		<cfset frmRegStatus = "Attended">
		<cfset eventFlagID = flagId>
		<cfset moduleStatusId = frmStatusID>

		<cfinclude template="/EventsPublic/RegUpdate.cfm" >			<!--- NB don't use cf_include cos it's in a loop and we don't need it so save the overhead --->
	</cfloop>
	<!--- END 2013-02-06 PPB Case 433635 --->
	
	<CFSET msg = "The student modules have successfully been graded.">
	
<CFELSEIF frmAction IS "delete">
	<cf_eventNotificationEmail action="compile" changes="Some Delegates removed from Delegate list">
	<cf_eventNotificationEmail action="send" FlagID="#FlagID#">
	
	<CFQUERY NAME="deleteEventData" datasource="#application.siteDataSource#">
		DELETE from #frmEventTable#
		WHERE EntityID  IN ( <cf_queryparam value="#frmCheckIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		AND FlagID =  <cf_queryparam value="#FlagID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		AND RegStatus = 'To Be Invited'
	</CFQUERY>
	
	<CFSET msg = "Delegates successfully deleted from the registration process.">
<CFELSE>

	<cf_eventNotificationEmail action="compile" changes="Delegate status has been changed">
	<cf_eventNotificationEmail action="send" FlagID="#FlagID#">

	<CFQUERY NAME="updateEventData" datasource="#application.siteDataSource#">
		UPDATE #frmEventTable# 
		SET #frmAction# =  <cf_queryparam value="#frmValue#" CFSQLTYPE="CF_SQL_VARCHAR" >,
			LastUpdatedBy=#request.relayCurrentUser.usergroupid#,
			lastUpdatedByPerson = #request.relayCurrentUser.personID#
		WHERE EntityID  IN ( <cf_queryparam value="#frmCheckIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		AND FlagID =  <cf_queryparam value="#FlagID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</CFQUERY>

	<CFSET msg = "The update has been successful.">
	
	<cfif frmAction eq "regstatus" and frmValue eq "Attended" and application.com.settings.getSetting("socialMedia.enableSocialMedia") and application.com.settings.getSetting("socialMedia.share.eventDetail.attended")>
		<cfquery name="getEventDetails" datasource="#application.siteDataSource#">
			select title,share from eventDetail where flagID =  <cf_queryparam value="#FlagID#" CFSQLTYPE="CF_SQL_INTEGER" > and share=1
		</cfquery>
	
		<!--- NJH 2012/10/23 Social CR - check if event is shareable --->
		<cfif getEventDetails.recordCount>
			<cfset mergeStruct = structNew()>
			<cfset mergeStruct.event = getEventDetails>
			
			<cfloop list="#frmCheckIDs#" index="personID">
				<cfquery name="getPersonName" datasource="#application.siteDataSource#">
					select firstname,lastname from person where personID =  <cf_queryparam value="#personID#" CFSQLTYPE="CF_SQL_INTEGER" > 
				</cfquery>
				
				<cfset mergeStruct.person = {firstname=getPersonName.firstname,lastname=getPersonName.lastname}>
				<cfset application.com.service.addRelayActivity(action="attended",performedOnEntityID=flagId,performedOnEntityTypeID=application.entityTypeID["eventDetail"],performedByEntityID=personID,mergeStruct=mergeStruct)>
			</cfloop>
		</cfif>
	</cfif>
</CFIF>




<cf_head>
<cf_title>Events - Lists</cf_title>

<SCRIPT type="text/javascript">
<!--

function submitForm() {	
	document.myForm.submit();
	<CFIF FindNoCase("msie", cgi.http_user_agent) GT 0>
		window.close();
	<CFELSE>
		setInterval("window.close()", 1000);
	</CFIF>
}

function closeWin() {
	if(!opener.closed){
		opener.location.href=opener.location.href;
	}
	self.close();
}

//-->
</SCRIPT>

</cf_head>



<DIV ALIGN="center">

<CFOUTPUT>
#htmleditformat(msg)#
<P>
<FORM NAME="myForm" ACTION="eventDelegateList.cfm" TARGET="main" METHOD="post">
<CF_INPUT TYPE="Hidden" NAME="FlagID" VALUE="#FlagID#">
<!--- NJH 2008-09-26 elearning 8.1 - just wanted to close the window --->
<cfif frmAction is "gradeStudents">
	<INPUT TYPE="Button" VALUE="Close Window" onClick="javascript: self.close();">
<cfelse>
	<INPUT TYPE="Button" VALUE="Close Window" onClick="javascript: closeWin();">
</cfif>
</FORM>
</CFOUTPUT>

</DIV>


