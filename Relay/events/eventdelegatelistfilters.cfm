<!--- �Relayware. All Rights Reserved 2014
SB 2015/12/09 Bootstrap and removed tables
 --->
<CFOUTPUT>
		<FORM NAME="search" ACTION="eventDelegateList.cfm" METHOD="post">

		<CF_INPUT TYPE="Hidden" NAME="FlagID" VALUE="#FlagID#">
		<CF_INPUT TYPE="Hidden" NAME="filter" VALUE="#filter#">
		<CF_INPUT TYPE="Hidden" NAME="criteria" VALUE="#criteria#">
		<CF_INPUT TYPE="Hidden" NAME="countryID" VALUE="#countryID#">
		<CF_INPUT TYPE="Hidden" NAME="countryGroupID" VALUE="#countryGroupID#">
		<CF_INPUT TYPE="Hidden" NAME="frmOrigStart" VALUE="#Start#">
		<CF_INPUT TYPE="Hidden" NAME="frmTotalChecks" VALUE="#frmTotalChecks#">
		<CF_INPUT TYPE="Hidden" NAME="frmEventTable" VALUE="#EventTable#">
		<INPUT TYPE="Hidden" NAME="frmAction" VALUE="">
		<CF_INPUT TYPE="Hidden" NAME="frmAllCheckIDs" VALUE="#frmAllCheckIDs#">

		<div class="form-group">
			<label>Order by</label>
			<SELECT NAME="frmOrder" onChange="this.form.submit();" class="form-control">
				<OPTION VALUE="LastName" <CFIF frmOrder IS "LastName"> SELECTED </CFIF> >Last Name
				<OPTION VALUE="SiteName" <CFIF frmOrder IS "SiteName"> SELECTED </CFIF> >Site
				<OPTION VALUE="Country" <CFIF frmOrder IS "Country"> SELECTED </CFIF> >Country
				<OPTION VALUE="RegStatus" <CFIF frmOrder IS "RegStatus"> SELECTED </CFIF> >Status
			</SELECT>
		</div>
		<div class="form-group">
			<label>Search for a Name</label>
			<CF_INPUT type="Text" name="srchName" value="#srchName#" size="20" >
			<input type="Submit" value="Go" class="btn btn-primary margin-top-10">
			<span class="help-block">
				<p>(any sort) or email</p>
			</span>
		</div>

		<div class="form-group">
			<label>Search for a Reg ID</label>
			<CF_INPUT type="Text" name="srchRegID" value="#srchRegID#" size="20">
			<input type="Submit" value="Go" class="btn btn-primary margin-top-10">
		</div>

		<div class="form-group">
			<label>Filter by Status</label>
			<select name="frmRegStatus" onChange="this.form.submit();" class="form-control">
				<option value="">Choose one
				<option value="">Don't filter by Status
				<cfloop query="statuses">
					<option value="#regstatus#" <cfif frmRegStatus is  "#regstatus#">selected</cfif>>#htmleditformat(regstatus)#
				</cfloop>
			</select>
		</div>
	</form>
</CFOUTPUT>