<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
2012-10-04	WAB		Case 430963 Remove Excel Header 
 --->
<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">
<cfparam name="openAsExcel" type="boolean" default="false">

	<cf_head>
		<cf_title>New Registrations</cf_title>
	</cf_head>
	

<cfparam name="sortOrder" default="country,Company,name">
<cfparam name="numRowsPerPage" default="50">
<cfparam name="startRow" default="1">

<cfparam name="checkBoxFilterName" type="string" default="">
<cfparam name="checkBoxFilterLabel" type="string" default="">

<cfparam name="keyColumnList" type="string" default="Name"><!--- this can contain a list of columns that can be edited --->
<cfparam name="keyColumnURLList" type="string" default="/data/dataframe.cfm?frmsrchPersonID="><!--- this can contain a list of matching columns that contain the URL of the editor template --->
<cfparam name="keyColumnKeyList" type="string" default="personid"><!--- this can contain a list of matching key columns e.g. primary key --->

<cfparam name="emailTextID" type="string" default="TeamManagerPasswordEmail"> 

<!--- 2006-03-16 - GCC - CR_SNY588 Change Password --->
<cfif application.com.settings.getSetting("security.passwordGenerationMethod")  eq "user">
	<cfset emailTextID = "TeamManagerPasswordEmail">
</cfif> 

<cfinclude template="reportTeamManagerEmailFunctions.cfm">

	<cfscript>
		qGetEmailDetails = application.com.email.getEmailDetails(emailTextID);
	</cfscript>
	 
	<CFIF qGetEmailDetails.recordcount neq 1>
		
		<cfoutput><p>You must add an email to Workflow/Email with the textID of #htmleditformat(emailTextID)#</p></cfoutput>
		<CF_ABORT>
	</CFIF>

 <cfscript>
 	if(structKeyExists(URL, "frmRowIdentity")){
		for(x = 1; x lte listLen(URL.frmRowIdentity); x = x + 1){
			application.com.login.createUserNamePassword(listGetAt(URL.frmRowIdentity,x));
			application.com.email.sendEmail(emailTextID,listGetAt(URL.frmRowIdentity,x));
			getFullPersonDetails = application.com.commonqueries.getFullPersonDetails(listGetAt(URL.frmRowIdentity,x));
		}
 }
 </cfscript> 

<cfquery name="getTeamManagers" datasource="#application.SiteDataSource#">
select Name, Company, Country, OrgID, personid, Username
from vEventTeamManagers
	WHERE 1=1		
	<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
	order by <cf_queryObjectName value="#sortOrder#"> 
</cfquery>

<cfoutput>
<table width="100%" cellpadding="3">
	<tr>
		<td><strong>Captains Login - Team Managers</strong></td>
		<td align="right">&nbsp;</td>
	</tr>
	<tr><td colspan="2">Here you can send team managers their password</td></tr>
</table>
</cfoutput>

<cf_translate>
<CF_tableFromQueryObject 
	queryObject="#getTeamManagers#"
	queryName="getTeamManagers"
	sortOrder = "#sortOrder#"
	openAsExcel = "true"
	numRowsPerPage="#numRowsPerPage#"
	startRow="#startRow#"
	
	keyColumnList="#keyColumnList#"
	keyColumnURLList="#keyColumnURLList#"
	keyColumnKeyList="#keyColumnKeyList#"
	keyColumnOpenInWindowList="yes"
	
	showTheseColumns="Name,Username,Company,Country"
 	hideTheseColumns="orgID,PersonID"
	columnTranslation="true"
	
	FilterSelectFieldList="Country,Company"
	FilterSelectFieldList2="Country,Company"
	GroupByColumns=""
	radioFilterLabel=""
	radioFilterDefault=""
	radioFilterName=""
	radioFilterValues=""
	checkBoxFilterName=""
	checkBoxFilterLabel=""
	allowColumnSorting="no"
	
	rowIdentityColumnName="PersonId"
	functionListQuery="#comTableFunction.qFunctionList#"
>
</cf_translate>



