<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File:			eventResourceEditor.cfm
Author:			DJH
Date created:	1 February 2000

Description:
Allows a user to add/view/update resources given the appropriate rights.

Version history:
1

--->

<cf_title>Event Resource Booking</cf_title>

<cfparam name="frmAction" default="">
<cfinclude template="eventTopHead.cfm">
<cfinclude template="sql_select_Resource.cfm">
<cfinclude template="sql_select_ResourceType.cfm">
<cfinclude template="sql_GetEventDetails.cfm">

<SCRIPT type="text/javascript">
	function submitForm()
	{
		document.eventStep.action = "eventWebContent.cfm?frmAction=Files"
		document.eventStep.submit()
	}	
</script>


<cfswitch expression="#frmAction#">
	<cfcase value="EnterResources">
		<cf_eventNotificationEmail action="Compile" changes="Resources added">
		<cf_eventNotificationEmail action="send" FlagID="#FlagID#">
		
		<CFQUERY NAME="InsertResources" datasource="#application.siteDataSource#">
		insert into ResourceBooking 
		(
			ResourceID, 
			FlagID, 
			DateRequiredStart, 
			LocationID, 
			SupplierID, 
			ContactID 
		)
		select 
			r.resourceID, 
			<cf_queryparam value="#FlagID#" CFSQLTYPE="CF_SQL_INTEGER" >, 
			<cf_queryparam value="#createodbcdate(getEventDetails.PreferredStartDate)#" CFSQLTYPE="CF_SQL_TIMESTAMP" >, 
			0, 
			0, 
			0
		from 
			Resource AS r, 
			ResourcePackItems AS rpi
		where 
			r.resourceID=rpi.resourceID
			and rpi.resourcePackID  in ( <cf_queryparam value="#ResourcePackID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		</CFQUERY>	
	</cfcase>
	
	<cfcase value="UpdateResourceBooking">
		<cf_eventNotificationEmail action="Compile" changes="Resources updated">
		<cf_eventNotificationEmail action="send" FlagID="#FlagID#">

		<CFQUERY NAME="UpdateResourceBooking" datasource="#application.siteDataSource#">
			update 
				ResourceBooking 
			set 
				<cfif budget is not "">
					budget =  <cf_queryparam value="#budget#" CFSQLTYPE="cf_sql_numeric" >, 
				</cfif>
				<cfif number is not "">
					number =  <cf_queryparam value="#number#" CFSQLTYPE="CF_SQL_Integer" > , 
				</cfif>
				<cfif status is not "">
					status =  <cf_queryparam value="#status#" CFSQLTYPE="CF_SQL_INTEGER" > 
				</cfif>
			where 
				resourceBookingID =  <cf_queryparam value="#ResourceBookingID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</CFQUERY>	
	</cfcase>
</cfswitch>


<CFQUERY NAME="GetResourcePacks" datasource="#application.siteDataSource#">
	<!--- What about country scope? --->
	Select 
		ResourcePackID, 
		ResourcePackName 
	from 
		resourcePackName
	where
		Active = 1
		and resourcePackID not in 
		(select distinct 
			ResourcePackID 
		from 
			ResourcePackItems as rpi, 
			ResourceBooking as rb 
		where 
			rpi.ResourceID = rb.ResourceID
			and rb.FlagID =  <cf_queryparam value="#FlagID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		)
</CFQUERY>


<!---
SELECT 
EventResCountbyPack.ResourcePackID, 
EventResCountbyPack.Expr1-PackResCount.CountOfResourceID AS Expr2

FROM 

(SELECT ResourcePackItems.ResourcePackID, Sum(CASE when a.ResourceID>0  then 1 else 0 end) AS Expr1
FROM ResourcePackItems LEFT JOIN 
(SELECT DISTINCT ResourceBooking.ResourceID
FROM ResourceBooking
WHERE (((ResourceBooking.FlagID)=1134))) AS a ON ResourcePackItems.ResourceID = a.ResourceID
GROUP BY ResourcePackItems.ResourcePackID) AS EventResCountbyPack
 
INNER JOIN 

(SELECT ResourcePackItems.ResourcePackID, Count(ResourcePackItems.ResourceID) AS CountOfResourceID
FROM ResourcePackItems
GROUP BY ResourcePackItems.ResourcePackID) AS PackResCount
 
ON 
EventResCountbyPack.ResourcePackID = PackResCount.ResourcePackID

WHERE (((EventResCountbyPack.Expr1-PackResCount.CountOfResourceID)<0))

--->

<table border="0">
<tr valign="top">
<td><cf_WhatsLeftToDo FlagID="#FlagID#"></td>
<td>
<cfif session.eventWizard is "true">	
	<table border="0" cellpadding="4" cellspacing="4" width="80%">
		<tr><td class="label"><b>Steps required in setting up an event</b></td></tr>
		<tr>
			<td class="creambackground" valign="top">
				<b>Step <cfif ByInvitationOnly>7<cfelse>6</cfif>:</b><br>
				<div class="Margin">
				<cfif UseAgency>
					Though you've chosen to use an agency to manage the event, you may still wish to add
					the resources required for this event.
				</cfif> The event resources are added here. Resources are grouped into "packs". You can add
				whichever packs are applicable to the event here. Once you've added the packs, the items in those
				packs are added to this event. You will then be able to allocate numbers required etc to each of the
				items. <cfif isOwner>If you need to edit specific resources or add new resources, <A HREF="eventResourceEditor.cfm?FlagID=<cfoutput>#htmleditformat(FlagID)#</cfoutput>">click here</A>.</cfif>
				</div>
			</td>
		</tr>
	</table>
</cfif>
</td>	
</tr>
</table>
			

<CFQUERY NAME="GetResourceBookings" datasource="#application.siteDataSource#">
SELECT  
	rt.ResourceType, 
	r.ResourceName, 
	rb.DateRequiredStart, 
	rb.SupplierID, 
	rb.FlagID, 
	rb.ResourceBookingID,
	rb.Status, 
	rb.Number,
	rb.Budget,
	l.SiteName
FROM
	ResourceBooking as rb inner join Resource as r on rb.ResourceID = r.ResourceID
	inner join ResourceType as rt on r.ResourceTypeID = rt.ResourceTypeID 
	left join Location as l on l.LocationID = rb.LocationID 
where
	rb.FlagID =  <cf_queryparam value="#FlagID#" CFSQLTYPE="CF_SQL_INTEGER" > 
</CFQUERY>


<CFQUERY NAME="GetSuppliers" datasource="#application.siteDataSource#">
	<!--- Get list of suppliers from locations --->
	Select 
		sitename,
		locationID 
	from 
		Location as l,
		flag as f,
		booleanFlagData as bfd
	where 
		l.LocationID = bfd.EntityID
		AND bfd.FlagID = f.FlagID
		AND f.FlagTextID = 'ResourceSupplier'
</CFQUERY>

<TABLE border="0" align="center">
	<TR>
		<Td class="label">Resource</Td>
		<Td class="label">No. required</Td>
		<Td class="label">Status</Td>
		<Td class="label">Supplier</Td>
		<Td class="label">Date required<BR> on site</Td>
		<Td class="label">Budget</Td>
		<Td class="label"></Td>
	</TR>
<CFOUTPUT QUERY="GetResourceBookings">
	<FORM ACTION="eventResourceBookings.cfm" METHOD="POST">
		<input type="hidden" name="dirty" value="0">
		<INPUT TYPE="Hidden" NAME="frmAction" VALUE="UpdateResourceBooking">
		<CF_INPUT TYPE="Hidden" NAME="ResourceBookingID" VALUE="#ResourceBookingID#">
		<CF_INPUT TYPE="Hidden" NAME="FlagID" VALUE="#FlagID#">
		<TR>
			<TD>#htmleditformat(ResourceName)#</TD>
			<TD><CF_INPUT TYPE="Text" NAME="Number" VALUE="#Number#" SIZE="6"></TD>
			<TD><SELECT NAME="Status">
				<OPTION VALUE="Not-required"<CFIF status IS 'Not-required'>Selected</CFIF>>Not-required </OPTION>
		   		<OPTION VALUE="Required" <CFIF status IS 'required'>Selected</CFIF>>Required</OPTION>
		   		<OPTION VALUE="Booked" <CFIF status IS 'Booked'>Selected</CFIF>>Booked</OPTION></SELECT>
			<TD>
			
				<SELECT NAME="Supplier">
					<OPTION VALUE="">Specify supplier</OPTION>
			   		<CFLoop QUERY="GetSuppliers"><OPTION VALUE="#LocationID#">#htmleditformat(SiteName)#</OPTION></CFLoop>
				</SELECT>
			</TD>
			<TD>#dateformat(DateRequiredStart,"dd-mmm-yyyy")#</TD>
			<TD><CF_INPUT TYPE="Text" NAME="budget" VALUE="#Budget#" SIZE="6"></TD>
			<TD><INPUT TYPE="Submit" VALUE="Update"></TD>
		</TR>
	</FORM>
</CFOUTPUT>
</TABLE>

<cfif getResourcePacks.recordcount gt 0 and IsOwner>
<table CELLPADDING="4" CELLSPACING="4" width="80%" BORDER="0" align="center">
<FORM name="ResourcePacks" ACTION="eventResourceBookings.cfm" METHOD="POST" onsubmit="return this.ResourcePackID.selectedIndex > -1">
	<CFOUTPUT>
		<CF_INPUT TYPE="Hidden" NAME="FlagID" VALUE="#FlagID#">
		<INPUT TYPE="Hidden" NAME="frmAction" VALUE="EnterResources">
	</CFOUTPUT>
	<tr>
		<td class="label">
		<b>Select one or more resource packs</b><br>
		<div class="Margin">
			<table border="0">
			<tr valign="top">
			<td>
			The resource packs below are available to add to the event. Any resource 
			packs already added will not be displayed in the list. You can choose 
			more than one resource pack to add by holding ctrl and clicking multiple selections.
			</td>
			<td>
			<SELECT NAME="ResourcePackID" VALUE="ResourcePackID" DISPLAY="ResourcePackName" size="6" multiple>
				<cfoutput QUERY="GetResourcePacks">
					<option value="#ResourcePackID#">#htmleditformat(ResourcePackName)#
				</cfoutput>
			</SELECT><br>
			<INPUT TYPE="Submit" VALUE="Add resources">
			</td>
			</tr>
			</table>
		</div><br>
		<div align="right">
		</div>
		</td>
	</tr>
</table>

</FORM>
</cfif>



<form name="eventStep" action="" method="post">
	<CF_INPUT type="hidden" name="FlagID" value="#FlagID#">
	<input type="hidden" name="dirty" value="0">
</form>

<cfif session.eventWizard is "true">	
<table>
<tr>
<td class="creambackground">
<b>Finished!</b><br>
That was the final step. If you want to go back through any of the steps to set up your 
event, click back/next buttons towards the bottom of each screen, or click through to a particular
section using the "What's left to do?" navigation aid.
</td>
</tr>
</table>
</cfif>

<cfset displaynext="no">
<cfinclude template="eventFooter.cfm">