<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File:			sql_select_ResourcePackName.cfm
Author:			DJH
Date created:	1 February 2000

Description:

Version history:
1

--->

<cfquery name="select_ResourcePackName" datasource="#application.siteDataSource#">
select
	ResourcePackID,
	ResourcePackName
from
	ResourcePackName
where
	Active = 1
</cfquery>