<!--- �Relayware. All Rights Reserved 2014 --->



<cf_head>
	<cf_title>Event Filters</cf_title>
</cf_head>


<!--- save the content for the xml to define the editor --->
<cfsavecontent variable="xmlSource">
<cfoutput>
<editors>
	<editor id="232" name="thisEditor" entity="eventFilters" title="Event Filters">
		
		<field name="FlagID" label="Event" description="" control="select" query="select flagID as value, title as display from eventDetail order by title"/>		
		<field name="sex" label="Sex/Gender" description="" control="select" query="select '' as value, 'Either gender' as display UNION select 'F' as value, 'Female only' as display UNION select 'M' as value, 'Male only' as display"></field>
		<field name="minDateofBirth" label="Min_Date_Of_Birth" description=""/>
		<field name="MaxDateofBirth" label="Max_Date_of_Birth" description=""/>
	</editor>
</editors>
</cfoutput>
</cfsavecontent>

<cfparam name="table" type="string" default="datasource">
<CFPARAM NAME="screenTitle" DEFAULT="">
<CFPARAM NAME="sortOrder" DEFAULT="Event_Group,event">

<cfif isDefined("editor") and editor eq "yes" and isDefined("add") and add eq "yes">
	<CF_RelayXMLEditor
		editorName = "thisEditor"
		xmlSourceVar = "#xmlSource#"
		add="yes"
		thisEmailAddress = "relayhelp@foundation-network.com"
	>
<cfelseif isDefined("editor") and editor eq "yes">
	<CF_RelayXMLEditor
		xmlSourceVar = "#xmlSource#"
		editorName = "thisEditor"
		thisEmailAddress = "relayhelp@foundation-network.com"
	>
		
<cfelse>

	<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
		<TR>
			<TD ALIGN="LEFT" CLASS="Submenu">Event Filters</TD>
			<TD COLSPAN="2" ALIGN="right" CLASS="Submenu">
				<A HREF="reportEventFilters.cfm?editor=yes&add=yes" CLASS="Submenu">Add</a> 
			</TD>		
		</TR>
	</TABLE>
	<cfquery name="getData" datasource="#application.siteDataSource#">
		select * from vEventFilters
		order by <cf_queryObjectName value="#sortOrder#">
	</cfquery>
	
	<CF_tableFromQueryObject 
		queryObject="#getData#" 
		
		keyColumnList="event"
		keyColumnURLList="reportEventFilters.cfm?editor=yes&flagID="
		keyColumnKeyList="flagID"
		
		hideTheseColumns=""

		tableAlign="center"
		
		HidePageControls="no"
		useInclude = "false"
		
		dateFormat=""
		sortOrder="#sortorder#">
	</cfif>
	






