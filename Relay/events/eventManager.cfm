<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File:			eventManager.cfm
Author:			DJH
Date created:	14 March 2000

Description:	Only called when eventWizard is switched on - basically displays the entitymanager screen with some additional
				help text

Version history:
1

--->
<cf_title>Event Manager</cf_title>

<cfif isDefined("RecordID")>
	<cfset FlagID = RecordID>
</cfif>

<cfinclude template="sql_getEventDetails.cfm">
<cfinclude template="eventTopHead.cfm">
<cfset session.eventStep = 2>

<cf_WhatsLeftToDo FlagID="#FlagID#">

	<cfif session.eventWizard is "true">
		<div class="col-xs-12 col-sm-8 col-md-9 col-lg-10">
			<h2>Steps required in setting up an event</h2>
			<h3>Step 2:</h3>
			<cfif IsOwner>
				<p>Now you've created the basic event information, you need to assign the appropriate managers for this event.
				Level 1 managers can view events. Level 2 Managers can be assigned to manage events. Level 3 Managers can create new events. To add an event manager, highlight their name
				from the drop down, and click Add.</p>
				<cfif UseAgency>
					 <p>Don't forget to assign a manager from the agency you are using to allow them to take over
					 management of the event.</p>
				</cfif>
				</div>
			<cfelse>
				<p>The next step would be to assign event managers to the event. You do not have the necessary rights
					to be able to assign new event managers to this event.
				</p>
			</cfif>
		</div>
	</cfif>
<!---SB end div from WhatsLeftToDo --->
</div>
<SCRIPT type="text/javascript">
	function submitForm(whereto)
	{
		if(whereto == -1)
		{
			document.eventStep.action = "eventDetails.cfm?eventStep=1"
		}
		else
		{
			document.eventStep.action = <cfif ByInvitationOnly>"eventAddDelegateWiz.cfm?eventStep=3"<cfelse>"eventWebContent.cfm?frmAction=editAgenda&eventStep=4"</cfif>
		}
		document.eventStep.submit()
	}
</script>
<cfif isOwner>
	<cfif not isdefined("form.RecordID")>
		<cfparam name="form.RecordID" default="#url.RecordID#">
		<cfparam name="form.Entity" default="#URL.Entity#">
		<cfparam name="form.ShortName" default="#URL.ShortName#">
	</cfif>
	<cfinclude template="/templates/EntityManager.cfm">
	<cfif isDefined("Add")>
		<cf_eventNotificationEmail action="compile" changes="Event Manager added">
	</cfif>
	<cfif isDefined("Remove")>
		<cf_eventNotificationEmail action="compile" changes="Event Manager deleted">
	</cfif>
	<cfif isDefined("Add") or isDefined("Remove")>
		<cf_eventNotificationEmail action="send" FlagID="#FlagID#">
	</cfif>
</cfif>

<form name="eventStep" action="" method="post">
	<CF_INPUT type="hidden" name="FlagID" value="#FlagID#">
	<input type="hidden" name="dirty" value="0">
</form>
<div class="internalBodyPadding">
	<cfinclude template="eventFooter.cfm">
</div>