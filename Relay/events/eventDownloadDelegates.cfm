<!--- �Relayware. All Rights Reserved 2014 --->
<!--- get the countries the current user has rights to view
returns a list of CountryIDs as a variable called Variables.CountryList 

Mods:  WAB 2000-05-04   Altered Download Functionality - all put into a single file
2000-06-11  WAB Tried to make getLists query to work better (not much success)
			added address details to the download - These come from the eventflagdata table if they have been entered, otherwise from the location table.



--->
<cfset CreateCountryRightsTable = true>
<CFINCLUDE TEMPLATE="../templates/qrygetcountries.cfm">

<!--- get the delegate list --->
<CFQUERY NAME="GetLists" datasource="#application.siteDataSource#">
	SELECT 
		et.EntityID,
		et.PartnerRegID, 
		et.Title, 
		et.FirstName, 
		et.LastName, 
		et.Email,
		et.SiteName, 
		case when et.address1 is Null then  l.address1 else et.address1 end as address1,
		case when et.address1 is Null then  l.address2 else et.address2 end as address2,
		case when et.address1 is Null then  l.address3 else et.address3 end as address3,
		case when et.address1 is Null then  l.address4 else et.address4 end as address4,
		case when et.address1 is Null then  l.address5 else et.address5 end as address5,
		case when et.address1 is Null then  l.PostalCode else et.postalcode end as postalcode, 
		isnull(et.Telephone, l.Telephone) as eTelephone,
		isnull(et.Fax, l.Fax) as eFax,
		isnull(et.Mobile, p.MobilePhone) as eMobile,
		et.AssignedLanguage, 
		et.RegStatus, 
		et.AssignedCourseContent,
		et.CourseContent,		
		et.AssignedWave,
		et.Wave,
		et.employeeCat,
		et.PartArrive,
		et.PartDepart,
		et.ConfirmationSent,		
		et.transferredTo,
		et.transferredFrom,
		p.PersonID,
		p.Language, 
		c.ISOCode, 
		p.firstname as p_firstname, 
		p.lastname as p_lastname, 
		focus_f.name as PartnerType,
		l.sitename as l_sitename
	FROM 
		eventflagdata AS et
		inner join Person AS p
		on p.PersonID = et.EntityID
		inner join Location as l
		on l.locationid= p.locationid
		inner join Country AS c
		on c.CountryID = l.CountryID
		left join 
		(
				BooleanFlagData as focus_bfd 
				INNER JOIN Flag as focus_f ON focus_bfd.FlagID = focus_f.FlagID 
				INNER JOIN FlagGroup as focus_fg ON focus_f.FlagGroupID = focus_fg.FlagGroupID
				and focus_fg.FlagGroupTextID  in ( <cf_queryparam value="#thePartnerType#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )				
		) 
		on l.locationid = focus_bfd.entityid
		inner join 
		#countryRightsTableName# AS R
		ON L.countryid = r.countryid

	WHERE 
		et.FlagID =  <cf_queryparam value="#FlagID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		<CFIF isDefined("frmCheckIDs")>and et.entityid  in ( <cf_queryparam value="#frmCheckIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )</cfif>
	order by
		p.lastname,
		p.firstname
</CFQUERY>




<cfinclude template = "eventMakeDownloadFile.cfm">


<cfoutput>
The file has now been generated,  please click on the link below to download <BR>
 <A HREF = "#webstatFilePath#" >Download File</a>
</cfoutput>  
