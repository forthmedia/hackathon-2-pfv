<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File:			events/sql_insert_resource.cfm
Author:			DJH
Date created:	1 February 2000

Description:
sql query to list resources

Version history:
1

--->

<cfquery name="insert_resource" datasource="#application.siteDataSource#">
insert into Resource
(
	ResourceTypeID,
	<cfif frmQuantityOnHand is not "">
		QuantityOnHand,
	</cfif>
	ResourceName,
	Description
	<!---OwnerID--->
)
values
(
	<cf_queryparam value="#frmResourceTypeID#" CFSQLTYPE="CF_SQL_INTEGER" >,
	<cfif frmQuantityOnHand is not "">
		<cf_queryparam value="#frmQuantityOnHand#" CFSQLTYPE="cf_sql_integer" >,
	</cfif>
	<cf_queryparam value="#frmResourceName#" CFSQLTYPE="CF_SQL_VARCHAR" >,
	<cf_queryparam value="#frmDescription#" CFSQLTYPE="CF_SQL_VARCHAR" >
	<!---OwnerID--->
)
</cfquery>
