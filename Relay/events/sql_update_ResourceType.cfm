<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File:			sql_update_ResourceType.cfm
Author:			DJH
Date created:	1 February 2000

Description:

Version history:
1

--->

<cfquery name="update_ResourceType" datasource="#application.siteDataSource#">
update 
	ResourceType
set
	ResourceType =  <cf_queryparam value="#frmResourceType#" CFSQLTYPE="CF_SQL_VARCHAR" > 
	<cfif isDefined("frmDeleteMe") and frmDeleteMe is 1>
		,
		Active = 0
	</cfif>
where
	ResourceTypeID =  <cf_queryparam value="#frmResourceTypeID#" CFSQLTYPE="CF_SQL_INTEGER" > 
</cfquery>