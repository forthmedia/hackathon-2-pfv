<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File:			eventAddDelegateWiz.cfm
Author:			DJH
Date created:	14 March 2000

Description:	Only called when eventWizard is switched on - basically displays the Add delegate screen with some additional
				help text

Version history:
1

--->
<cf_title>Add Delegates</cf_title>

<cfif isDefined("RecordID")>
	<cfset FlagID = RecordID>
</cfif>
<cfinclude template="sql_getEventDetails.cfm">
<cfinclude template="eventTopHead.cfm">

<cfset session.eventStep = 3>
<cfoutput>
<script type="text/javascript">
	function submitForm(whereto)
	{
		if(whereto == -1)
		{
			document.eventStep.action = "eventManager.cfm?RecordID=#jsStringFormat(FlagID)#&Entity=Flag&ShortName=EventTask"
		}
		else
		{
			document.eventStep.action = "eventDelegateList.cfm?FlagID=#jsStringFormat(FlagID)#&eventStep=3"
		}
		document.eventStep.submit()
	}
</script>
</cfoutput>



<table border="0">
<tr valign="top">
	<td><cf_WhatsLeftToDo FlagID="#FlagID#"></td>
	<td>
<cfif session.eventWizard is "true">	
	<table border="0" cellpadding="4" cellspacing="4" align="center">
		<tr><td class="label"><b>Steps required in setting up an event</b></td></tr>
		<tr>
			<td class="creambackground" valign="top">
				<b>Step 3:</b><br>
				<cfif IsOwner>
					<div class="Margin">
						You've chosen to have delegates invited - not to allow open invitation. You need to add the delegates you wish to 
						invite here. This is done by choosing an existing selection which holds the delegates to be invited.
					<cfif UseAgency>
						 
					</cfif>
				<cfelse>
					<div class="Margin">
						This is where you would choose the delegates to invite to the event. You do not have the necessary permissions to 
						invite delegates to this event.
					</div>
				</cfif>
				</div>
			</td>
		</tr>
	</table>
</cfif>
	</td>
</tr>
</table>
<cfset frmAction="Add">

<cfif isOwner>
<!--- get the name of the event table --->
<CFQUERY NAME="GetEventTable" datasource="#application.siteDataSource#">
	SELECT 
		ft.DataTable, 
		f.FlagTextID
	FROM 
		Flag AS f, 
		FlagGroup AS fg, 
		FlagType AS ft
	WHERE 
		f.FlagGroupID = fg.FlagGroupID
		AND fg.FlagTypeID = ft.FlagTypeID
		AND f.FlagID =  <cf_queryparam value="#FlagID#" CFSQLTYPE="CF_SQL_INTEGER" > 
</CFQUERY>

<CFSET frmEventTable = "#GetEventTable.DataTable#FlagData">


<cfinclude template="EventTask.cfm">
</cfif>
<form name="eventStep" action="" method="post">
	<CF_INPUT type="hidden" name="FlagID" value="#FlagID#">
	<input type="hidden" name="dirty" value="0">
</form>

<cfinclude template="eventFooter.cfm">