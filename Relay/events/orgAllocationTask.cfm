<!--- �Relayware. All Rights Reserved 2014 --->
<!--- <CFOUTPUT>Debug: Organisationids = #frmorgsonpage#</cfoutput><BR> --->
<!--- <CFOUTPUT>flagsonpage #frmOrgFlagsOnpage# </CFOUTPUT> --->

<CFSET freezedate=now()>

<!--- Loop though organisations and update name if necessary --->
<CFLOOP list="#frmorgsonpage#" index="CurrentOrgID">
	<!---  update a organisation name if it has changed--->
	<CFIF evaluate( "frmOrgName_"&CurrentOrgID) IS NOT evaluate( "frmOrgNameOriginal_"&CurrentOrgID)>

		<CFQUERY NAME="updateOrgName" datasource="#application.sitedatasource#">
		UPDATE Organisation
		Set OrganisationName =  <cf_queryparam value="#evaluate("frmOrgName_"&CurrentOrgID)#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
		lastupdatedby= 	#request.relayCurrentUser.usergroupid#,
	  	lastupdated =  <cf_queryparam value="#freezedate#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
		WHERE OrganisationID =  <cf_queryparam value="#CurrentOrgID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</CFQUERY>	
	
	</CFIF>
</cfloop>

<CFIF frmevent IS "3ComUP">
<!--- hack here no updating to do for 3comuinternal --->

<!--- Need to check that no one else has been editing allocations since page created--->
<CFQUERY NAME="getAllocatedToOrganisations" datasource="#application.sitedatasource#">
select sum(data) as amount
		from IntegerFlagDataOrg as ifd, flag as f
		WHERE (f.flagtextid =  <cf_queryparam value="#frmEventPrefix#orgsalesplaces" CFSQLTYPE="CF_SQL_VARCHAR" >  or f.flagtextid =  <cf_queryparam value="#frmEventPrefix#orgpresalesplaces" CFSQLTYPE="CF_SQL_VARCHAR" > )
		AND ifd.flagid=f.flagid
		AND ifd.countryid =  <cf_queryparam value="#frmcurrentregionID#" CFSQLTYPE="CF_SQL_INTEGER" > 
</cfquery>


<CFIF (getallocatedToOrganisations.amount IS frmOriginalOrgSalesPlacesAllocated + frmOriginalOrgPreSalesPlacesAllocated) or (getallocatedToOrganisations.amount is "")>
<!---  if no change since start (or if answer is null)--->


<!--- Loop through flag and organisation updating flags --->
<CFLOOP list="#frmOrgFlagsOnpage#" index="formvariable">

		<CFSET flagtextid=listgetat(formvariable,3,"_")>
		<CFSET datatype=listgetat(formvariable,1,"_")>
		<CFSET table=listgetat(formvariable,2,"_")>
		
		<CFIF datatype IS not "flag"> Can only process flags! <CF_ABORT> </CFIF>
		<CFIF table IS not "IntegerFlagDataOrg"> Can only update integer table!<CF_ABORT> </CFIF>

<CFLOOP list="#frmorgsonpage#" index="CurrentOrgID">


	<CFSET originaldata = evaluate( "#formvariable#original_"&CurrentOrgID)>
	<CFSET newdata = evaluate( "#formvariable#_"&CurrentOrgID)>

	<!---  update a organisation allocation if it has changed--->
	<CFIF originaldata IS NOT newdata>

		<CFQUERY NAME="doesorganisationexist" datasource="#application.sitedatasource#">
		<!--- might have been deduped --->
		Select * from organisation
		where organisationid =  <cf_queryparam value="#CurrentOrgID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</CFQUERY>
		
	  <CFIF doesorganisationexist.recordcount is 0>
			<CFOUTPUT>#CurrentOrgID# dispappeared!<BR></cfoutput>
		
	  <CFELSE>
				



		<CFQUERY NAME="doesflagexist" datasource="#application.sitedatasource#">
		Select * from IntegerFlagDataOrg as ifd, flag as f
		Where ifd.entityid =  <cf_queryparam value="#CurrentOrgID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		AND ifd.countryid =  <cf_queryparam value="#frmcurrentregionid#" CFSQLTYPE="CF_SQL_INTEGER" > 
		and f.flagid=ifd.flagid
		and f.flagtextid =  <cf_queryparam value="#flagtextid#" CFSQLTYPE="CF_SQL_VARCHAR" > 
		</CFQUERY>

		<CFIF doesflagexist.recordcount is 0>

			<CFQUERY NAME="AddOrgIntFlag" datasource="#application.sitedatasource#">
			Insert into IntegerFlagDataOrg (flagid, entityid, countryid, data, createdby, created, lastupdatedby, lastupdated)
			select flagid ,	<cf_queryparam value="#CurrentOrgID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#frmcurrentregionid#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#newdata#" CFSQLTYPE="cf_sql_integer" >,<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >, <cf_queryparam value="#freezedate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >,<cf_queryparam value="#freezedate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
			from flag where flagtextid =  <cf_queryparam value="#flagtextid#" CFSQLTYPE="CF_SQL_VARCHAR" > 
			</cfquery>
		
		<CFELSE>
		
			<CFQUERY NAME="updateOrgIntFlag" datasource="#application.sitedatasource#">
			Update IntegerFlagDataOrg set 
				data =  <cf_queryparam value="#newdata#" CFSQLTYPE="cf_sql_integer" >,
				lastupdatedby= 	#request.relayCurrentUser.usergroupid#,
			  	lastupdated =  <cf_queryparam value="#freezedate#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
 			Where entityid =  <cf_queryparam value="#CurrentOrgID#" CFSQLTYPE="CF_SQL_INTEGER" > 
			AND countryid =  <cf_queryparam value="#frmcurrentregionid#" CFSQLTYPE="CF_SQL_INTEGER" > 
			AND flagid in (select flagid from flag where flagtextid =  <cf_queryparam value="#flagtextid#" CFSQLTYPE="CF_SQL_VARCHAR" > )
			</cfquery>
		
		</cfif>
	  </CFIF>
	</CFIF>

</cfloop>
</cfloop>

<CFELSE>

	<CFSET Message = "Someone else was editing the allocations while your previous page was open. <BR>
I am afraid that we could not save the allocations which you made" >

</cfif>

</cfif>

<CFINCLUDE Template="#frmReturnTo#">