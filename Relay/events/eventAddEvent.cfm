<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File:			EventAddEvent.cfm
Author:			? then DJH
Date created:	27 January 2000

Description:
Allows an authorised user to add a new event

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2006-05-02			AJC			Added hidden field code

Version history:
1
2 Amended to more logically see how its made up - now uses cfswitch
	WAB 2001-06-15		Modification to take account of new triggers on flagtable (these make sure that flagtextids are valid and unique and also return the inserted row)
	SSS 2008/04/14 The country host country select box has been changed to the single select from a multi select box because the database and the code where written to only accept a integer not a varchar.
	NJH 2008/09/25 elearning 8.1 - added moduleID's to an event, if the module is a classroom or webinar module.
	NJH 2009/01/28	Bug Fix All Sites Issue 1698 - use the createFlagGroup function to insert a flaggroup which then updates the flag structure.
					In this fix, the flagGroup name has been changed from being "events" to "events - "+ the event name.
	NJH 2009/02/09 P-SNY047 changed from form to cfform as ugRecordmanager changed to using cfselect
	NYB 2009/03/02 NABU RW 8.1 bug 1874
	NYB 2009-05-12 Sophos -	Bugzilla bug 2139 - Title input field of Events set up doesn't accept enough characters
					requires release of getColumnSize function in dbTools
	NJH 2009/09/15	LID 2616. Added translate tags to translate modules.
	NYB 2009-09-28  LHID2672 - increase size of title, preventing it from ever erroring
	NJH 2010/01/29 	LID 3050 - clean up the event flagTextID before checking if it exists.
	IH	2012/03/20	Case 424420 prevent backspace on date fields to go back to previous page on IE
	NJH 2012/10/25	Social CR - set the share column when adding an event
	2014-06-20 	REH 	Case 439403 changed characters remaining caption to appear below textarea box
	2015/12/02  SB		Bootstrap
	2016-01-12	WAB		Visibility Project (and BF-226).  Change UGRecordManager to refer to the actual entity (which is eventDetail rather than event)
--->
<cf_title>Add Event</cf_title>

<cfset ShowNav="NO">
<cfinclude template="eventTopHead.cfm">
<cfinclude template="sql_getPartnerTypes.cfm">
<cfparam name="frmPartnerType" default="#valuelist(getPartnerTypes.FlagID)#">
<cfparam name="frmCampaignID" default="">
<!--- 2006-05-02 AJC CHW001 Default for hiding fields --->
<cfparam name="EventHideFormFields" default="">

<!--- Make sure the Javascript client validation is updated to include all the new fields  --->

<CFIF NOT SuperOK>
	You do not have permissions to create a new event.
	<CF_ABORT>
</CFIF>

<cfparam name="frmAction" default="addEventEdit">
<cfif frmAction is "">
	<cfset frmAction="addEventEdit">
</cfif>

<cfswitch expression="#frmAction#">

<cfcase value="addEventEdit">
	<CFQUERY NAME="getEventGroups" datasource="#application.siteDataSource#">
		SELECT
			EventGroupID,
			EventName,
			FlagGroupID
		FROM
			EventGroup
			Where testgroup <> 1 or testgroup is null
	</CFQUERY>

	<!--- get the countries the current user has rights to view
	returns a list of CountryIDs as a variable called Variables.CountryList --->
	<CFINCLUDE TEMPLATE="../templates/qrygetcountries.cfm">

	<!--- get the countries that user has rights to view  --->
	<!--- 2006-05-04 AJC - Adding in country regions and cfc'ising the data calls --->

<!--- 	<CFQUERY NAME="countriesAvailable" datasource="#application.siteDataSource#">
		SELECT
			CountryDescription,
			CountryID
		FROM
			Country
		WHERE
			CountryID IN (#Variables.CountryList#)
		ORDER BY
			CountryDescription
	</CFQUERY> --->
	<cfscript>
		mycountries = createObject("component","relay.com.opportunity");
		getRegions = application.com.relayCountries.getRegions(showCurrentUsersCountriesOnly=true);
		countriesAvailable = application.com.relayCountries.getCountries(showCurrentUsersCountriesOnly=true);
	</cfscript>

	<CFQUERY NAME="getCountrygroups" datasource="#application.siteDataSource#">
		SELECT DISTINCT b.CountryDescription AS CountryGroup, b.CountryID
		FROM Country AS a, Country AS b, CountryGroup AS c
		WHERE a.CountryID  IN ( <cf_queryparam value="#Variables.CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		AND (b.ISOcode IS null OR b.ISOcode = '')
		AND c.CountryGroupID = b.CountryID
		AND c.CountryMemberID = a.CountryID
		ORDER BY b.CountryDescription
	</CFQUERY>

	<!--- START2:  NYB 2009-09-28 LHID2672 - moved following to above javascript: --->
	<!--- START:  NYB 2009-05-18 Sophos - added: --->
	<cfset eventTableColSize = application.com.dbTools.getColumnSize(tableName='eventDetail',returnColumns='true')>
	<!--- END:  2009-05-18 Sophos - --->

	<!--- START: NYB 2009-09-28 LHID2672 --->
	<cfinclude template="../templates/relayFormJavaScripts.cfm">
	<!--- END: NYB 2009-09-28 LHID2672 --->
	<!--- END2: NYB 2009-09-28 LHID2672 --->

	<cfoutput>
	<SCRIPT type="text/javascript">
	<!--
	function submitForm() {
		var form = document.addEvent;
		var msg = "";
		if (form.frmEventGroupID.selectedIndex == 0) {
			msg += "* Event Group\n";
		}
		if (form.frmLocation.value.length == 0)
		{
			msg += "* City\n"
		}
		if (form.frmVenueRoom.value.length == 0)
		{
			msg += "* Venue Address\n"
		}
		if (form.frmCountryID.selectedIndex == 0) {
			msg += "* Country\n";
		}
		if (!checkInteger(form.frmEstAttendees.value)||form.frmEstAttendees.value.length == 0) {
			msg += "* Estimated No of Attendees must be a valid number\n";
		}
		<cfif listfind(EventHideFormFields,"frmMaxTeamSize") is 0>
		if (!checkInteger(form.frmMaxTeamSize.value)||form.frmMaxTeamSize.value.length == 0) {
		msg += "* Maximum Team Size must be a valid number\n";
		}
		</cfif>
		// 2006-05-09 AJC P_CHW001 removed and put in the edit form (eventdteail.cfm)
		if (form.frmEligibleCountryID.selectedIndex == -1) {
		msg += "* Eligible Countries\n";
		}
		/*START: NYB 2009-09-28 LHID2672 - removed:
		if (!checkEuroDate(form.frmPreferredStartDate.value)) {
			msg += "* Event Start Date must be completed and of the form dd-mmm-yyyy\n";
		}
		if (!checkEuroDate(form.frmPreferredEndDate.value)) {
			msg += "* Event End Date must be completed and of the form dd-mmm-yyyy\n";
		}
		END: NYB 2009-09-28 LHID2672*/
		if (!alertChecked(form.frmInvitationType))
		{
			msg += "* Unique Invitation number required must have a selection\n";
		}
		<cfif listfind(EventHideFormFields,"frmAudienceType") is 0>
		if (!alertChecked(form.frmAudienceType))
		{
			msg += "* Audience must have a selection\n";
		}
		</cfif>
		//if (!checkStartEnd(form.frmPreferredStartDate.value,form.frmPreferredEndDate.value)) {
		//	msg += "* End Date must be after Start Date\n";
		//}
		if (msg != "") {
			alert("\nYou must provide the following information for this Event:\n\n" + msg);
		} else {
			//START: NYB 2009-09-28 LHID2672
			i = form.frmEventGroupID.selectedIndex;
			txt=form.frmEventGroupID.options[i].text;
			dateVar = "#dateformat(frmpreferredStartDate,'dd-mmm-yyyy')#";
			if ((form.frmTitle.value.length <= #eventTableColSize.title# && form.frmUseTitleOnly.checked) || txt.length + form.frmTitle.value.length + form.frmLocation.value.length + dateVar.length <= #eventTableColSize.title#)
			{
				form.submit();
			} else {
				var r=confirm("Default Title is  too long.  \n\nPlease click OK to truncate or Cancel to amend/select Title Only.");
				if (r==true)
				  {
					form.submit();
				  }
			}
			//END: NYB 2009-09-28 LHID2672
		}
	}

	//-->
	</SCRIPT>
	</cfoutput>

	<!--- <cfif session.eventWizard is "true">
		<div class="grey-box-top">
			<h2>Steps required in setting up an event</h2>
			<h3>Step 1:</h3>
			<p>Firstly you'll need to enter some basic information about the event. The information	required can be entered below. All fields on this page need to be completed as they decide to some extent which screens you'll need to fill in next.</p>
		</div>
	</cfif> --->

	<!--- NJH 2009/02/09 P-SNY047 changed from form to cfform as ugRecordmanager changed to using cfselect --->
	<CFFORM NAME="addEvent" ACTION="eventAddEvent.cfm" METHOD="post">
	<input type="hidden" name="dirty" value="0">
	<INPUT TYPE="Hidden" NAME="frmAction" VALUE="addEventAdd">
	<div id="addEvents" class="row">
		<div class="col-xs-12 col-sm-6">
			<div class="grey-box-top">
				<CFOUTPUT>
					<CFIF Msg NEQ "">
						<p class="bg-danger">#Msg#</p>
					</CFIF>
				</cfoutput>
				<div class="form-group">
					<label for="frmEventGroupID" class="required">Group this Event belongs to.</label>
					<SELECT NAME="frmEventGroupID" id="frmEventGroupID" class="form-control">
						<OPTION VALUE="0">Please select an Event Group
						<CFOUTPUT QUERY="getEventGroups">
							<OPTION VALUE="#EventGroupID#" <CFIF ListFind(frmEventGroupID,EventGroupID) GT 0> SELECTED </CFIF> onchange="dirty.value=1">#EventName#
						</CFOUTPUT>
					</SELECT>
					<A HREF="javascript: document.addEvent.frmAction.value='AddGroupEdit'; document.addEvent.submit()">Add New Event Group</A>
					<span class="help-block">
						<p>
							This is required. Event groups are a way of relating a series of events together. The event group will also supply some default information for your new event.
						</p>
					</span>
				</div>
				<CFOUTPUT>
				<div class="form-group">
					<label for="frmTitle">Title of the event.</label>
					<!--- START:  NYB 2009-05-18 Sophos - replaced MAXLENGTH="100" with MAXLENGTH="#eventTableColSize.title#" --->
					<CF_INPUT NAME="frmTitle" id="frmTitle" TYPE="Text" SIZE="40" MAXLENGTH="#eventTableColSize.title#" VALUE="#frmTitle#" onchange="dirty.value=1">
					<!--- END:  2009-05-18 Sophos - --->
					<div class="checkbox">
						<label>
							<input type="checkbox" name="frmUseTitleOnly" value="true"<cfif frmTitle neq "">checked</cfif>> Use Title Only:
						</label>
					</div>
					<span class="help-block">
						<p>This is optional. When the event has been saved, the title will be made up of either:</p>
						<ol>
							<li>The event group, any text you put here, the city and the start date or</li>
							<li>Any text you put here (for this, please check the "Use Title Only" checkbox).</li>
						</ol>
					</span>
				</div>
				<div class="form-group">
					<label for="frmLocation" class="required">City/Location</label>
					<!--- START:  NYB 2009-05-18 Sophos - replaced MAXLENGTH="100" with MAXLENGTH="#eventTableColSize.Location#" --->
					<CF_INPUT NAME="frmLocation" id="frmLocation" TYPE="Text" SIZE="40" MAXLENGTH="#eventTableColSize.Location#" VALUE="#frmLocation#" onchange="dirty.value=1">
					<!--- END:  2009-05-18 Sophos --->
					<span class="help-block">
						Required. This shows on the standard web registration page.  This should contain the city in which the event is being held.
						If this event is an on-line event put Online.
					</span>
				</div>
				<div class="form-group">
					<label class="frmVenueRoom required" class="required">Venue Details/Webex Login Details</label>

					<!--- 2014-06-20 REH Case 439403 changed characters remaining caption to appear below textarea box and aligned to the right --->
					<!--- START:  NYB 2009-05-18 Sophos - replaced MAXLENGTH="100" with MAXLENGTH="#eventTableColSize.VenueRoom#" --->
					<!--- START:  NYB 2009-09-28 LHID2672 - replaced MAXLENGTH="#eventTableColSize.VenueRoom#" with onKeyDown="limitText(this,#eventTableColSize.VenueRoom#);" ---
					<textarea NAME="frmVenueRoom" id="frmVenueRoom" onKeyDown="limitText(this,#eventTableColSize.VenueRoom#);" onKeyUp="limitText(this,#eventTableColSize.VenueRoom#);" onchange="dirty.value=1" wrap="hard">#frmVenueRoom#</textarea>
					LHID2672 - WITH: --->
					<CF_relayTextField fieldName="frmVenueRoom" id="frmVenueRoom" maxChars="500" currentValue="#frmVenueRoom#" cols="40" rows="4" helpText="phr_dateFieldHelpText" maxCharsPosition="bottom" maxCharsTextAlign="right">
					<!--- END:  NYB 2009-09-28 LHID2672 --->
					<!--- END:  2009-05-18 Sophos --->
					<span class="help-block">
						<p>Required. This shows on the standard web registration page.  It should contain the full address of the venue
						where the event is being held or if it is a webex should contain the webex login details.</p>
					</span>
				</div>
				</cfoutput>
				<div class="form-group">
					<label for="frmCountryID" class="required">Country</label>
					<CFIF countriesAvailable.RecordCount GT 0 or getRegions.RecordCount GT 0>
					<!--- 2006-05-04 AJC CR_CHW001 - Add Country Groups --->
					<!--- 2008/04/14 SSS Bug allsites 37 made this in to a single select not muliti select --->
					<select name="frmCountryID" id="frmCountryID" class="form-control">
						<OPTION VALUE="0" SELECTED> Select a Country
						<cfif getRegions.RecordCount GT 0>
							<CFOUTPUT QUERY="getRegions">
								<OPTION VALUE="*#CountryID#"> #HTMLEditFormat(CountryDescription)#
							</CFOUTPUT>
								<OPTION VALUE="0">-----------------------------------------
						</cfif>
						<cfif countriesAvailable.RecordCount GT 0>
							<CFOUTPUT QUERY="countriesAvailable">
								<OPTION VALUE="#CountryID#"> #HTMLEditFormat(CountryDescription)#
							</CFOUTPUT>
						</cfif>
					</SELECT>
					<!--- <SELECT NAME="frmCountryID" onchange="dirty.value=1">
						<option value="0">Please select a country
						<CFOUTPUT QUERY="countriesAvailable">
							<OPTION VALUE="#CountryID#" <CFIF ListFind(frmCountryID, CountryID) GT 0> SELECTED </CFIF> >#CountryDescription#
						</CFOUTPUT>
					</SELECT>&nbsp; --->
					<CFELSE>
						None
					</CFIF>
					<span class="help-block">
						This is required. This is the country in which the event is being hosted.
					</span>
				</div>
				<cfoutput>
				<div class="form-group">
					<label>Event Visibility</label>
					<CF_UGRecordManager entity="EventDetail" entityid="newflagid" Form="addEvent">
					<span class="help-block">
						<p>This is the list of user groups you can assign to this event.</p>
					</span>
				</div>
				<div class="form-group">
					<label for="frmEstAttendees" class="required">Estimated number of attendees</label>
					<CF_INPUT NAME="frmEstAttendees" id="frmEstAttendees" TYPE="Text" SIZE="5" MAXLENGTH="5" VALUE="#frmEstAttendees#" onchange="dirty.value=1">
					<span class="help-block">
						<p>This is required. This is the total number of attendees you anticipate being able to attend the event.</p>
					</span>
				</div>
				<cfif listfind(EventHideFormFields,"frmMaxTeamSize") is 0>
				<div class="form-group">
					<label id="frmMaxTeamSize">Maximum Team Size</label>
					<CF_INPUT NAME="frmMaxTeamSize" id="frmMaxTeamSize" TYPE="Text" SIZE="5" MAXLENGTH="5" VALUE="#frmMaxTeamSize#" onchange="dirty.value=1">
					<span class="help-block">
						<p>This is the maximum number of people you can have per team in this event.</p>
					</span>
				</div>
				</cfif>
				<div class="form-group">
					<label for="frmPreferredStartDate" class="required">Preferred start date</label>
					<!--- START: NYB 2009-09-28 LHID2672 - REPLACED:---
					<INPUT NAME="frmPreferredStartDate" TYPE="Text" SIZE="11" MAXLENGTH="11" VALUE="#frmPreferredStartDate#" onchange="dirty.value=1"><br>
					(This must be in a dd-mmm-yyyy format)
					!--- NYB 2009-09-28 LHID2672 - WITH: --->
					<cf_relaydatefield fieldname = "frmPreferredStartDate" id="frmPreferredStartDate" currentvalue = "#frmPreferredStartDate#" formname="addEvent" helpText="Click to choose date" anchorname="x">
					<!--- END: NYB 2009-09-28 LHID2672 --->
					<span class="help-block">
						<p>This is required. This is the date you would like the event to start on. If you are not planning on using an agency to
						manage the event, this date will not change. If an agency is used, the actual start date may differ slightly.</p>
					</span>
				</div>
				<div class="form-group">
					<label for="frmPreferredEndDate" class="required">Preferred end date</label>
					<!--- START: NYB 2009-09-28 LHID2672 - REPLACED:---
					<INPUT NAME="frmPreferredEndDate" TYPE="Text" SIZE="11" MAXLENGTH="11" VALUE="#frmPreferredEndDate#" onchange="dirty.value=1"><br>
					(This must be in a dd-mmm-yyyy format)
					!--- NYB 2009-09-28 LHID2672 - WITH: --->
					<cf_relaydatefield fieldname = "frmPreferredEndDate" currentvalue = "#frmPreferredEndDate#" formname="addEvent" helpText="Click to choose date" anchorname="y">
					<!--- END: NYB 2009-09-28 LHID2672 --->
					<span class="help-block">
						<p>This is required. This is the date you would like the event to finish on. If you are not planning on using an agency to
						manage the event, this date will not change. If an agency is used, the actual end date may differ slightly.</p>
					</span>
				</div>
				<div class="form-group">
					<label for="frmEmail">Notify this email address of any changes.</label>
					<!--- START:  NYB 2009-05-18 Sophos - replaced MAXLENGTH="100" with MAXLENGTH="#eventTableColSize.Email#" --->
					<CF_INPUT TYPE="text" NAME="frmEmail" id="frmEmail" SIZE="40" MAXLENGTH="#eventTableColSize.Email#" onchange="dirty.value=1">
					<!--- END:  2009-05-18 Sophos --->
					<span class="help-block">
						<p>This is optional. If you are the owner of this event and you have set up other event managers, you can enter your email address
						in this field and you will be notified if anyone changes the details of this event sent to.</p>
						<p>(Please separate multiple email addresses with a comma.)</p>
					</span>
				</div>
				</CFOUTPUT>
			</div>
		</div>
		<div class="col-xs-12 col-sm-6">
			<div class="grey-box-top">
			<div class="form-group">
				<label for="frmEligibleCountryID" class="required">Eligible countries</label>
				<CFIF countriesAvailable.RecordCount GT 0>
				<SELECT NAME="frmEligibleCountryID" class="form-control" id="frmEligibleCountryID" <CFIF countriesAvailable.RecordCount LT 10>SIZE="<CFOUTPUT>#countriesAvailable.RecordCount#</CFOUTPUT>" <CFELSE> SIZE="10" </CFIF> MULTIPLE onchange="dirty.value=1">
					<CFOUTPUT QUERY="countriesAvailable">
						<OPTION VALUE="#CountryID#" <CFIF ListFind(frmCountryID, CountryID) GT 0> SELECTED </CFIF> >#CountryDescription#
					</CFOUTPUT>
				</SELECT>
				<CFELSE>
					None
				</CFIF>
				<cf_invertselection	form="addEvent"	displayas="ANCHOR" select="frmEligibleCountryID">
				<span class="help-block">
					<p><b>(Hold down Ctrl to click multiple countries)</b></p>
					<p>This is required. This is the list of countries you would like to be eligible to attend this event. You will still be
					able to add to or remove from this list onces the initial selections have been made, though you will not be able to remove
					a country once a delegate from that country has registered for the event.</p>
				</span>
			</div>
			<div class="form-group">
				<label for="frmUseAgency" class="required">Will you use an agency to manage this event</label>
				<select name="frmUseAgency" id="frmUseAgency" onchange="dirty.value=1" class="form-control">
					<option value="0" checked>No
					<option value="1">Yes
				</select>
				<span class="help-block">
					<p>This is required. If you wish to manage the event yourself, you will be accountable to deliver
					everything required to ensure success of the event.</p>
				</span>
			</div>
			<cfif listfind(EventHideFormFields,"frmPartnerType") is 0>
			<div class="form-group">
				<label>Eligible partner types</label>
				<cfoutput query="getPartnerTypes">
					<label class="checkbox-inline">#name# <CF_INPUT type="checkbox" name="frmPartnerType" value="#FlagID#" checked="#iif( frmPartnerType contains FLagID,true,false)#" onclick="dirty.value=1"></label>
				</cfoutput>
				<span class="help-block">
					<p>This is optional. This is the partner types you would like to be considered eligible for this event.</p>
				</span>
			</div>
			</cfif>
			<div class="form-group">
					<label class="required">Open or restricted registration</label>
					<div class="radio">
						<label>Open registration <INPUT NAME="frmInvitationType" TYPE="radio" VALUE="Open" checked onclick="dirty.value=1"></label>
					</div>
					<div class="radio">
						<label>By invitation only <INPUT NAME="frmInvitationType" TYPE="radio" VALUE="InvOnly" onclick="dirty.value=1"></label>
					</div>
					<span class="help-block">
						<p>Required. Open registration events will allow anyone who should be able to register  If you decide the event is open by invitation only, you will
						need to select the appropriate delegates to invite. The invited delegates will then be required to enter a unique invitation
						reference number to authenticate their eligibility.</p>
					</span>
			</div>
			<!--- START: 2013-07-31	MS	P-KAS026 Allow the users to set the visibility of Register or Unregister Buttons --->
			<h2>Portal Buttons</h2>
			<div class="form-group">
				<div class="checkbox">
					<label>
						Hide Register for Event button
						<CF_INPUT type="checkbox" name="frmEventHideRegBtn" <!--- checked=#GetEventDetails.share# ---> value="1" onchange="dirty.value=1" <!--- disabled="#not(eventShare)#" --->><span title="When checked users won't be able to see the Register for Event button on portal'."><img src="/images/misc/iconhelpsmall.gif">
					</label>
				</div>
				<div class="checkbox">
					<label>
						Hide Un-Register from Event button
						<CF_INPUT type="checkbox" name="frmEventHideUnRegBtn" <!--- checked=#GetEventDetails.share# ---> value="1" onchange="dirty.value=1" <!--- disabled="#not(eventShare)#" --->><span title="When checked users won't be able to see the UnRegister from an already registered event on portal."><img src="/images/misc/iconhelpsmall.gif">
					</label>
				</div>
			</div>
			<!--- END  : 2013-07-31	MS	P-KAS026 Allow the users to set the visibility of Register or Unregister Buttons --->
			<cfif listfind(EventHideFormFields,"frmAudienceType") is 0>
				<div class="form-group">
					<label>Audience</label>
					<div class="checkbox">
						<label>
							Sales <INPUT NAME="frmAudienceType" TYPE="checkbox" VALUE="Sales" onclick="dirty.value=1; if(this.checked) frmAudienceType[2].checked = false">
						</label>
					</div>
					<div class="checkbox">
						<label>Pre-Sales <INPUT NAME="frmAudienceType" TYPE="checkbox" VALUE="Pre-Sales" onclick="dirty.value=1; if(this.checked) frmAudienceType[2].checked = false"></label>
					</div>
					<div class="checkbox">
						<label>Not applicable <INPUT NAME="frmAudienceType" TYPE="checkbox" VALUE="NA" onclick="dirty.value=1; if(this.checked) {frmAudienceType[0].checked = false; frmAudienceType[1].checked = false}"></label>
					</div>
					<span class="help-block">
						<p>This is required. This is to indicate what streams of delegates should be allowed to attend the event.</p>
					</span>
				</div>
			</cfif>
			<cfif listfind(EventHideFormFields,"frmScantron") is 0>
				<div class="form-group">
					<label>Scantron forms</label>
					<div class="form-group">
						<label>Address</label>
						<INPUT NAME="frmScantronShippingAddress" TYPE="text" VALUE=""  onchange="dirty.value=1" class="form-control">
					</div>
					<div class="form-group">
						<label>Telephone</label>
						<INPUT NAME="frmScantronTel" TYPE="text" VALUE="" onchange="dirty.value=1" class="form-control">
					</div>
					<div class="form-group">
						<label>Contact</label>
						<INPUT NAME="frmScantronContact" TYPE="text" VALUE="" onchange="dirty.value=1" class="form-control">
					</div>
					<span class="help-block">
						<p>This is optional. This is to indicate the office address for the forms.</p>
					</span>
				</div>
			<cfelse>
				<input type="hidden" name="frmScantronShippingAddress" value="">
				<input type="hidden" name="frmScantronTel" value="">
				<input type="hidden" name="frmScantronContact" value="">
			</cfif>
		<!--- <TR>
				<TD valign="top" colspan="4" class="label">
					<b>Half Day Event</b><br>
					<div Class="Margin">
						This is required. This is to indicate any other activities carried out on the day.
					</div>
					<div align="right">
					Additional Events <INPUT NAME="frmExtraProgramme" TYPE="text" VALUE="" onchange="dirty.value=1">

					</div>
				</TD>
			</TR>	 --->
			<cfif listfind(EventHideFormFields,"frmVenueRating") is 0>
				<div class="form-group">
					<label>Venue Rating</label>
					<select name="frmVenueRating" onchange="dirty.value=1" class="form-control">
						<option value="1" >One star
						<option value="2" >Two Star
						<option value="3" >Three Star
						<option value="4" >Four Star
						<option value="5" >Five Star
					</select>
					<span class="help-block">
						<p>This is optional. This is to indicate the star rating of the venue.</p>
					</span>
				</div>
			<cfelse>
				<input type="hidden" name="frmVenueRating" value="">
			</cfif>
			<cfif listfind(EventHideFormFields,"frmTypeofrefreshment") is 0>
			<div class="form-group">
					<label>Type of Refreshment</label>
					<select name="frmTypeofrefreshment" onchange="dirty.value=1" class="form-control">
						<option value="FingerBuffet">Finger buffet
						<option value="BeerandSandwich">Beer and Sandwich
						<option value="HotandColdBuffet">Hot and Cold Buffet
						<option value="Other">Other
					</select>
					<span class="help-block">
						<p>This is optional. This is to indicate the level of food served.</p>
					</span>
			</div>
			<cfelse>
				<input type="hidden" name="frmTypeofrefreshment" value="">
			</cfif>
			<div class="form-group">
				<label for="frmRegistrationScreenID">Registration Screen</label>
				<cfquery name="getScreens" datasource="#application.siteDataSource#">
					select 	ScreenID, ScreenName
					from 	Screens
					where 	active = 1
					order by ScreenName
				</cfquery>
				<select name="frmRegistrationScreenID" id="frmRegistrationScreenID" onchange="dirty.value=1" class="form-control">
					<option value="">Select a screen
					<cfoutput query="getScreens">
					<option value="#ScreenID#"<cfif isDefined("frmRegistrationScreenID") and frmRegistrationScreenID eq ScreenID> selected</cfif>>#ScreenName#
					</cfoutput>
				</select>
				<span class="help-block">
					<p>This is optional. This is the additional information capture screen to which the users will be redirected after registration.</p>
				</span>
			</div>

			<!--- START:  NYB 2009/03/02 NABU RW 8.1 bug 1874 - moved queries from below (*1), added cfif around 2nd query --->
			<cfscript>
				qryModules = application.com.relayElearning.GetTrainingModulesData();
			</cfscript>
			<cfif qryModules.recordcount gt 0>
				<cfquery name="qryModules" dbType="query">
					select * from qryModules where module_type in ('Classroom','Webinar')
				</cfquery>
			</cfif>
			<!--- END:  NYB 2009/03/02 NABU RW 8.1 bug 1874 - moved queries  --->

			<!--- START:  NYB 2009/03/02 NABU RW 8.1 bug 1874:  --->
			<cfif qryModules.recordcount gt 0>
			<!--- END:  NYB 2009/03/02 NABU RW 8.1 bug 1874  --->
				<!--- NJH 2008/09/23 add a module to an event - elearning 8.1 --->
				<div class="form-group">
					<label for="frmModuleID">Associate eLearning Module</label>
					<!--- (*1) NYB 2009/03/02  --->

					<select name="frmModuleID" id="frmModuleID" onchange="dirty.value=1" class="form-control">
						<option value="">Select a module
						<cfoutput query="qryModules">
						<option value="#moduleID#"<cfif isDefined("frmModuleID") and frmModuleID eq moduleID> selected</cfif>>#title_of_module#
						</cfoutput>
					</select>
					<span class="help-block">
						<p>For Education events. Optional.</p>
					</span>
				</div>
			<!--- START:  NYB 2009/03/02 NABU RW 8.1 bug 1874:  --->
			</cfif>
			<!--- END:  NYB 2009/03/02 NABU RW 8.1 bug 1874  --->

				<!--- NJH 2012/10/23  - Social CR  - share an event --->
				<div class="form-group">
					<label>Social Updates</label>
					<cfset eventShare = true>
					<cfif not application.com.settings.getSetting("socialMedia.enableSocialMedia")>
						<cfset eventShare = false>
					</cfif>
					<CF_INPUT type="checkbox" name="frmEventShare" checked=#eventShare# value="1" disabled=#not(eventShare)#><cfif not eventShare><span title="Social Media has not been enabled."><img src="/images/misc/iconhelpsmall.gif"></span></cfif>
					<span class="help-block">
						<p>Allow social updates about this event</p>
					</span>
				</div>

			<cfif application.com.settings.getSetting("campaigns.displayCampaigns")>
				<div class="form-group">
					<label for="frmCampaignID">Phr_Sys_AssociateCampaign</label>
					<cfset getCampaigns = application.com.relayCampaigns.GetCampaigns(currentValue=frmCampaignID)>
					<SELECT NAME="frmCampaignID" id="frmCampaignID" class="form-control">
						<OPTION value="">Phr_Ext_SelectAValue</OPTION>
						<cfloop query="getCampaigns">
							<cfoutput><OPTION VALUE="#getCampaigns.campaignID#" <cfif frmCampaignID is getCampaigns.campaignID>SELECTED</cfif>>#htmleditformat(getCampaigns.campaignName)#</OPTION></cfoutput>
						</cfloop>
					</SELECT>
				</div>
			</cfif>
			</div>
		</div>
	</div>
<!--- 		End of form --->
	</CFFORM>
</cfcase>

<cfcase value="addEventAdd">
	<!--- check start date is before end date --->
	<CFIF DateCompare(frmPreferredStartDate, frmPreferredEndDate) IS 1>
		<CFSET Msg = Msg & "Please ensure the Start Date is before the End Date<BR>">
	</CFIF>

	<!--- attempt to create a unique flagtextid for this event based on the event name and location with
	all spaces removed. --->

	<!--- first, get the event group name --->
	<cfquery name="EventGroupName" datasource="#application.siteDataSource#">
	select
		EventName
	from
		EventGroup
	where
		EventGroupID =  <cf_queryparam value="#frmEventGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</cfquery>

	<!--- START:  NYB 2009-09-28 LHID2672 - added: --->
	<cfset titleColSize = application.com.dbTools.getColumnSize(tableName="eventDetail",returnColumns="true",columnName="title")>
	<!--- END:  NYB 2009-09-28 LHID2672 --->

	<cfif form.frmTitle is "">
		<cfset frmTitle = "#EventGroupName.EventName# #frmLocation# #dateformat(frmpreferredStartDate,'dd-mmm-yyyy')#">
	<cfelseif isDefined("frmUseTitleOnly") and frmUseTitleOnly>
		<cfset frmTitle = "#form.frmTitle#">
	<cfelse>
		<cfset frmTitle = "#EventGroupName.EventName# #form.frmTitle# #frmLocation# #dateformat(frmpreferredStartDate,'dd-mmm-yyyy')#">
	</cfif>

	<!--- START:  NYB 2009-09-28 LHID2672 - added: --->
	<cfset frmTitle = left(frmTitle,titleColSize.title)>
	<!--- END:  NYB 2009-09-28 LHID2672 --->

	<cfif isDefined("frmUseTitleOnly") and frmUseTitleOnly>
		<cfset frmFlagTextID = replace("#form.frmTitle#"," ","","ALL")>
	<cfelse>
		<cfset frmFlagTextID = replace("#EventGroupName.EventName##Left(frmLocation,6)#"," ","","ALL")>
	</cfif>

	<cfset frmFlagTextID = left(frmFlagTextID,23)>

	<!--- NJH 2010/01/29 LID 3050 - need to clean up the flagTextID first before checking if it exists, as there is a trigger on the flag table
		that first cleans it up before putting it into the flag table. --->
	<cfquery name="getCleanedFlagTextID" datasource="#application.siteDataSource#">
		declare @newFlagTextID varchar(250)
		exec stripNonCFVariableCharacters @inString =  <cf_queryparam value="#frmFlagTextID#" CFSQLTYPE="CF_SQL_VARCHAR" > , @outString = @newFlagTextID output
		select @newFlagTextID as cleanFlagTextID
	</cfquery>

	<cfset frmFlagTextID = getCleanedFlagTextID.cleanFlagTextID>

<!--- now make sure flagTextID is unique - if not, append a counter to the end of it --->

	<!--- check that the user supplied FlagTextID is unique in the database --->
	<CFQUERY NAME="getEventFlagTextIDs" datasource="#application.siteDataSource#">
		SELECT
			FlagTextID
		FROM
			Flag
		where
			FlagTextID  like  <cf_queryparam value="#frmFlagTextID#%" CFSQLTYPE="CF_SQL_VARCHAR" >
	</CFQUERY>

	<CFIF getEventFlagTextIDs.recordcount GT 0>
		<cfset frmFlagTextID = frmFlagTextID & (getEventFlagTextIDs.recordcount + 1)>
	</CFIF>

	<CFIF Msg NEQ "">
		<CFlocation url="eventAddEvent.cfm?Msg=#Msg#"addToken="false">
		<CF_ABORT>
	<CFELSE>
		<CFSET FreezeDate = request.requestTimeODBC>
		<CFQUERY NAME="getEventFlagGroup" datasource="#application.siteDataSource#">
			SELECT
				FlagGroupID
			FROM
				eventGroup
			WHERE
				eventGroupID =  <cf_queryparam value="#frmEventGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</CFQUERY>

<!---
	WAB 2007-12-03 problems with CF8, replaced with call to cfc (which is now correct way to do it anyhow)
		<CFQUERY  NAME="qaddEventFlag" datasource="#application.siteDataSource#">
			INSERT INTO Flag
			(
				FlagTextID,
				FlagGroupID,
				Name,
				Active,
				CreatedBy,
				Created,
				LastUpdatedBy,
				LastUpdated
			)
			VALUES
			(
				'#frmFlagTextID#',
				'#getEventFlagGroup.FlagGroupID#',
				'#frmFlagTextID#',
				1,
				#request.relayCurrentUser.personid#,
				#CreateODBCDate(FreezeDate)#,
				#request.relayCurrentUser.personid#,
				#CreateODBCDate(FreezeDate)#
			)

		</CFQUERY>

<!--- 	WAB 2001-06-15 removed, trigger now returns the inserted record
	<CFQUERY NAME="getEventFlag" datasource="#application.siteDataSource#">
			SELECT
				FlagID
			FROM
				Flag
			WHERE
				FlagTextID = '#frmFlagTextID#'
		</CFQUERY>
 --->
		<CFSET newFlagID = QaddEventFlagResult.FlagID>
 --->
		<CFSET newFlagID  = application.com.flag.createFlag (
					FlagTextID = frmFlagTextID,
					FlagGroup = getEventFlagGroup.FlagGroupID,
					FlagType = 'event' ,
					Name = frmFlagTextID,
					Description="#frmFlagTextID# Event" ) >

		<!--- NJH 2009/09/22 LID 2616 - a little bit of error handling. Ideally, would like to get message from createFlag function,
			but it's a bit too much work for the 8.2 timescale. --->
		<cfif newFlagID eq 0>
			<cfset msg = "Problem creating flag for the new event.">
			<CFlocation url="eventAddEvent.cfm?Msg=#Msg#"addToken="false">
			<CF_ABORT>
		</cfif>

		<CFQUERY NAME="addEvent" datasource="#application.siteDataSource#">
			INSERT INTO EventDetail
			(
				FlagID,
				OwnerID,
				Title,
				Location,
				VenueRoom,
				CountryID,
				EstAttendees,
				EventGroupID,
				PreferredStartDate,
				PreferredEndDate,
				Email,
				<!---EmailCC,--->
				UseAgency,
				<!---Agenda,--->
				InvitationType,
				PartnerType,
				AudienceType,
				Created,
				CreatedBy,
				LastUpdated,
				venuerating,
				typeofrefreshment,
				ScantronShippingAddress,
				ScantronTel,
				ScantronContact,
				<cfif frmRegistrationScreenID neq "">
				RegistrationScreenID,
				</cfif>
				<!--- NYB 2009/03/02 NABU RW 8.1 bug 1874 - added 'isDefined("frmModuleID") and ' to cfif --->
				<cfif isDefined("frmModuleID") and frmModuleID neq "">
				moduleID,
				</cfif>
				<!--- hack - make sure it gets removed later on --->
				<cfif frmEventGroupID is 86>
					ConfirmationFileID,
				</cfif>
				LastUpdatedBy,
				MaxTeamSize,
				share,
				<!--- START: 2013-07-31	MS	P-KAS026 Allow the users to set the visibility of Register or Unregister Buttons --->
				hideRegisterForEventBtn,
				hideUnRegisterForEventBtn,
				<!--- END  : 2013-07-31	MS	P-KAS026 Allow the users to set the visibility of Register or Unregister Buttons --->
				campaignID
			)
			VALUES
			(
				<cf_queryparam value="#newFlagID#" CFSQLTYPE="CF_SQL_INTEGER" >,
				<cf_queryparam value="#request.relayCurrentUser.personid#" CFSQLTYPE="cf_sql_integer" >,
				<cf_queryparam value="#frmTitle#" CFSQLTYPE="CF_SQL_VARCHAR" >,
				<cf_queryparam value="#frmLocation#" CFSQLTYPE="CF_SQL_VARCHAR" >,
				<cf_queryparam value="#frmVenueRoom#" CFSQLTYPE="CF_SQL_VARCHAR" >,
				<cf_queryparam value="#frmCountryID#" CFSQLTYPE="CF_SQL_VARCHAR" >,
				<cf_queryparam value="#frmEstAttendees#" CFSQLTYPE="CF_SQL_Integer" >,
				<cf_queryparam value="#frmEventGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >,
				<cf_queryparam value="#CreateODBCDate(frmPreferredStartDate)#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
				<cf_queryparam value="#CreateODBCDate(frmPreferredEndDate)#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
				<cf_queryparam value="#frmEmail#" CFSQLTYPE="CF_SQL_VARCHAR" >,
				<!--- '#frmEmailCC#',--->
				<cf_queryparam value="#frmUseAgency#" CFSQLTYPE="CF_SQL_bit" >,
				<!--- frmAgenda, --->
				<cf_queryparam value="#frmInvitationType#" CFSQLTYPE="CF_SQL_VARCHAR" >,
				<cf_queryparam value="#frmPartnerType#" CFSQLTYPE="CF_SQL_VARCHAR" >,
				<cf_queryparam value="#frmAudienceType#" CFSQLTYPE="CF_SQL_VARCHAR" >,
				<cf_queryparam value="#CreateODBCDate(FreezeDate)#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
				<cf_queryparam value="#request.relayCurrentUser.personid#" CFSQLTYPE="cf_sql_integer" >,
				<cf_queryparam value="#CreateODBCDate(FreezeDate)#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
				<cf_queryparam value="#frmVenueRating#" CFSQLTYPE="CF_SQL_VARCHAR" >,
				<cf_queryparam value="#frmTypeofrefreshment#" CFSQLTYPE="CF_SQL_VARCHAR" >,
				<cf_queryparam value="#frmScantronShippingAddress#" CFSQLTYPE="CF_SQL_VARCHAR" >,
				<cf_queryparam value="#frmScantronTel#" CFSQLTYPE="CF_SQL_VARCHAR" >,
				<cf_queryparam value="#frmScantronContact#" CFSQLTYPE="CF_SQL_VARCHAR" >,
				<cfif frmRegistrationScreenID neq "">
				<cf_queryparam value="#frmRegistrationScreenID#" CFSQLTYPE="cf_sql_integer" >,
				</cfif>
				<!--- NYB 2009/03/02 NABU RW 8.1 bug 1874 - added 'isDefined("frmModuleID") and ' to cfif --->
				<cfif isDefined("frmModuleID") and frmModuleID neq "">
				<cf_queryparam value="#frmModuleID#" CFSQLTYPE="cf_sql_integer" >,
				</cfif>
				<!--- hack - make sure it gets removed later on --->
				<cfif frmEventGroupID is 86>
					181,
				</cfif>
				<cf_queryparam value="#request.relayCurrentUser.personid#" CFSQLTYPE="cf_sql_integer" >,
				<cf_queryparam value="#frmMaxTeamSize#" CFSQLTYPE="CF_SQL_Integer" >,
				<cf_queryparam value="#structKeyExists(form,'frmEventShare')#" CFSQLTYPE="CF_SQL_Integer" >,
				<!--- START: 2013-07-31	MS	P-KAS026 Allow the users to set the visibility of Register or Unregister Buttons --->
				<cfif structKeyExists(form,"frmEventHideRegBtn")><cf_queryparam value="#form.frmEventHideRegBtn#" CFSQLTYPE="CF_SQL_INTEGER" ><cfelse><cf_queryparam value="0" CFSQLTYPE="CF_SQL_INTEGER" ></cfif>,
				<cfif structKeyExists(form,"frmEventHideUnRegBtn")><cf_queryparam value="#form.frmEventHideUnRegBtn#" CFSQLTYPE="CF_SQL_INTEGER" ><cfelse><cf_queryparam value="0" CFSQLTYPE="CF_SQL_INTEGER" ></cfif>,
				<!--- END  : 2013-07-31	MS	P-KAS026 Allow the users to set the visibility of Register or Unregister Buttons --->
				<cf_queryparam value="#frmCampaignID#" CFSQLTYPE="CF_SQL_INTEGER" null="#iif(isNumeric(frmCampaignID),de('no'),de('yes'))#">
			)
		</CFQUERY>
		<cfset form.newFlagID=newFlagID>

		<CF_UGRecordManagerTask>
		<!--- add the owner as a default event manager --->
		<cfset RecordID = #newFlagID#>
		<cfset Entity = "Flag">
		<cfset Shortname = "EventTask">

		<CFQUERY NAME="UpdateManagers" datasource="#application.siteDataSource#">
			INSERT INTO RecordRights
			(
				UserGroupID,
				Entity,
				RecordID,
				Permission
			)
			values
			(
				<cf_queryparam value="#request.relayCurrentUser.userGroupId#" CFSQLTYPE="cf_sql_integer" >,
				<cf_queryparam value="#Entity#" CFSQLTYPE="CF_SQL_VARCHAR" >,
				<cf_queryparam value="#RecordID#" CFSQLTYPE="CF_SQL_INTEGER" >,
				0
			)
		</cfquery>

		<CFLOOP LIST="#frmEligibleCountryID#" INDEX="theCountry">
			<CFQUERY NAME="addCountries" datasource="#application.siteDataSource#">
				INSERT INTO EventCountry
				(
					FlagID,
					CountryID,
					LastUpdated,
					LastUpdatedBy
				)
				VALUES
				(
					<cf_queryparam value="#newFlagID#" CFSQLTYPE="CF_SQL_INTEGER" >,
					<cf_queryparam value="#theCountry#" CFSQLTYPE="CF_SQL_INTEGER" >,
					<cf_queryparam value="#CreateODBCDate(FreezeDate)#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
					<cf_queryparam value="#request.relayCurrentUser.personid#" CFSQLTYPE="cf_sql_integer" >
				)
			</CFQUERY>
		</CFLOOP>
	</CFIF>
	<!--- record should now have been successfully added - return to the eventlist/go on to the next stage of the event process --->
	<cfif session.eventWizard is "true">
		<cflocation url="eventManager.cfm?RecordID=#newFlagID#&Entity=Flag&ShortName=EventTask&FlagID=#newFlagID#"addToken="false">
	<cfelse>
		<cflocation url="eventlist.cfm"addToken="false">
	</cfif>

</cfcase>

<cfcase value="addGroupEdit">
	<!--- submit form function ensures a value was entered --->
	<SCRIPT type="text/javascript">
	<!--

	function submitForm() {
		var form = document.addEventGroup;
		var msg = "";
		if (trim(form.frmEventGroup.value) == "") {
			msg += "Please enter the name of the new Event Group";
		}
		if (msg != "") {
			alert(msg);
		} else {
			form.submit();
		}
	}
	//-->
	</SCRIPT>

	<!--- form to actually add the new group --->

	<FORM NAME="addEventGroup" ACTION="eventAddEvent.cfm" METHOD="post">
	<CFOUTPUT>
	<INPUT TYPE="Hidden" NAME="frmAction" VALUE="addGroupAdd">
	<CF_INPUT TYPE="Hidden" NAME="frmLocation" VALUE="#frmLocation#">
	<CF_INPUT TYPE="Hidden" NAME="frmVenueRoom" VALUE="#frmVenueRoom#">
	<CF_INPUT TYPE="Hidden" NAME="frmCountryID" VALUE="#frmCountryID#">
	<CF_INPUT TYPE="Hidden" NAME="frmPreferredStartDate" VALUE="#frmPreferredStartDate#">
	<CF_INPUT TYPE="Hidden" NAME="frmPreferredEndDate" VALUE="#frmPreferredEndDate#">
	<CF_INPUT TYPE="Hidden" NAME="frmEmail" VALUE="#frmEmail#">
	<CF_INPUT TYPE="Hidden" NAME="frmEmailCC" VALUE="#frmEmailCC#">
	<CF_INPUT TYPE="Hidden" NAME="frmUseAgency" VALUE="#frmUseAgency#">
	<CF_INPUT TYPE="Hidden" NAME="frmEligibleCountryID" VALUE="#frmEligibleCountryID#">
	<CF_INPUT TYPE="Hidden" NAME="frmInvitationType" VALUE="#frmInvitationType#">
	<CF_INPUT TYPE="Hidden" NAME="frmAudienceType" VALUE="#frmAudienceType#">
	<CF_INPUT TYPE="Hidden" NAME="frmPartnerType" VALUE="#frmPartnerType#">
	<CF_INPUT TYPE="Hidden" NAME="frmRegistrationScreenID" VALUE="#frmRegistrationScreenID#">
	<cfif isDefined("frmModuleID")><CF_INPUT TYPE="Hidden" NAME="frmModuleID" VALUE="#frmModuleID#"></cfif>
	<CF_INPUT TYPE="Hidden" NAME="frmMaxTeamSize" VALUE="#frmMaxTeamSize#">
	</CFOUTPUT>
	<TABLE CELLPADDING="2" CELLSPACING="2" BORDER="0" align="center" width="70%">
	<TR>
		<TD class="label">
			<b>Event Group</b><br>
			<div Class="Margin">
				This is required. This is the name of the new event group which you will use to group subsequent series of events
				together.
			</div>
			<div align="right">
			<INPUT NAME="frmEventGroup" TYPE="Text" SIZE="50" MAXLENGTH="50">
			</div>
		</TD>
	</TR>
<!---	<TR>
		<TD COLSPAN="3" ALIGN="center"><BR><BR><A HREF="JavaScript:submitForm();"><CFOUTPUT><IMG SRC="/images/buttons/c_save_e.gif" WIDTH=84 HEIGHT=21 BORDER="0"></CFOUTPUT></A></TD>
	</TR>
--->
	</TABLE>
	</FORM>
</cfcase>

<cfcase value="addGroupAdd">

	<cftransaction>
		<!--- need to add a record into the FlagGroup table for some reason... --->
		<cfquery name="getHighestFlagGroup" datasource="#application.siteDataSource#">
			select
				max(FlagGroupID) as highest
			from
				FlagGroup
		</cfquery>
		<!--- flagGroupTextID can be a maximum of 25 chars --->
		<cfset maxchars = 25>

		<cfset newFlagGroupTextID = replace(frmEventGroup, " ", "", "ALL")>

		<cfset length1 = len(newFlagGroupTextID)>

		<cfset length2 = len(getHighestFlagGroup.highest)>

		<cfif length1 + length2 gt maxchars>

			<cfset newFlagGroupTextID = left(newFlagGroupTextID, (length1 + length2) - maxchars)>

		</cfif>

		<cfset newFlagGroupTextID = '#newFlagGroupTextID##getHighestFlagGroup.highest#'>


		<cfquery name="eventFlagType" datasource="#application.siteDataSource#">
			select
				FlagTypeID
			from
				FlagType
			where
				name = 'Event'
		</cfquery>

		<!--- NJH 2009/01/28 Bug Fix All Sites Issue 1698 --->
		<cfset newFlagGroupID = application.com.flag.createFlagGroup(
					flagType="event",
					EntityType="person",
					FlagGroupTextID=newFlagGroupTextID,
					Name="events - #frmEventGroup#",
					order=100,
					expiry=request.requestTime,
					download=0
					)>

		<!--- <cfquery name="addFlagGroup" datasource="#application.siteDataSource#">
			insert into flagGroup
			(
				ParentFlagGroupID,
				FlagTypeID,
				EntityTypeID,
				Active,
				FlagGroupTextID,
				Name,
				OrderingIndex,
				expiry,
				scope,
				viewing,
				edit,
				search,
				download,
				viewingaccessrights,
				editaccessrights,
				searchaccessrights,
				downloadaccessrights,
				Createdby,
				Created,
				LastUpdatedBy,
				LastUpdated
			)
			values
			(
				0,
				#eventflagtype.flagTypeID#,
				0,
				1,
				'#newFlagGroupTextID#',
				'events',
				100,
				#createodbcdate(now())#,
				0,
				1,
				1,
				1,
				0,
				0,
				0,
				0,
				0,
				0,
				#createodbcdate(now())#,
				0,
				#createodbcdate(now())#
			)
		</cfquery>

		<!--- get the flaggroupid of the flaggroup just added --->

		<cfquery name="getNewFlagGroup" datasource="#application.siteDataSource#">
			select
				max(FlagGroupID) as newFlagGroupID
			from
				FlagGroup
		</cfquery>--->


		<!--- make sure they don't enter too long an event group name
		event name can't be over fifty chars anyway, though need to drop appname down,
		because 'event' is prefixed.
		--->
		<cfset appname = replace(frmEventGroup, " ", "", "ALL")>
		<cfif len("event#appname#") gt 50>
			<cfset appname = left(appname, len("event#appname#") - 50)>
		</cfif>


		<!--- add the new event group --->
		<CFQUERY NAME="addEventGroup" datasource="#application.siteDataSource#">
			INSERT INTO EventGroup
			(
				EventName,
				AppName,
				FlagGroupID
			)
			VALUES
			(
				<cf_queryparam value="#frmEventGroup#" CFSQLTYPE="CF_SQL_VARCHAR" >,
				<cf_queryparam value="#appname#" CFSQLTYPE="CF_SQL_VARCHAR" >,
				#newFlagGroupID# <!--- NJH 2009/01/28 Bug Fix All Sites Issue 1698 - was #getNewFlagGroup.newFlagGroupID# --->
			)
		</CFQUERY>

		<!--- fetch the eventgroupID of the newly added event group--->

		<cfquery name="GetEventGroupID" datasource="#application.siteDataSource#">
			select
				eventGroupID
			from
				EventGroup
			where
				EventName =  <cf_queryparam value="#frmEventGroup#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</cfquery>

		<!--- add the default validfieldvalues in for this eventgroup --->

 		<cfquery name="addvalidvaluesregstatus" datasource="#application.siteDataSource#">
			insert into validfieldvalues
			(
				fieldname,
				datavalue,
				validvalue,
				countryid,
				defaultfieldcase,
				defaultfieldvalue,
				sortorder,
				LanguageID,
				InAnalysis,
				Score
			)
			select
				<cf_queryparam value="FlagGroup.#newFlagGroupTextID#.RegStatus" CFSQLTYPE="CF_SQL_VARCHAR" >,
				null,
				'To be invited',
				0,
				'T',
				0,
				0,
				null,
				1,
				null
			union select
				<cf_queryparam value="FlagGroup.#newFlagGroupTextID#.RegStatus" CFSQLTYPE="CF_SQL_VARCHAR" >,
				null,
				'Invited',
				0,
				'T',
				0,
				1,
				null,
				1,
				null
			union select
				<cf_queryparam value="FlagGroup.#newFlagGroupTextID#.RegStatus" CFSQLTYPE="CF_SQL_VARCHAR" >,
				null,
				'RegistrationSubmitted',
				0,
				'T',
				0,
				2,
				null,
				1,
				null
			union select
				<cf_queryparam value="FlagGroup.#newFlagGroupTextID#.RegStatus" CFSQLTYPE="CF_SQL_VARCHAR" >,
				null,
				'Attended',
				0,
				'T',
				0,
				3,
				null,
				1,
				null
			union select
				<cf_queryparam value="FlagGroup.#newFlagGroupTextID#.RegStatus" CFSQLTYPE="CF_SQL_VARCHAR" >,
				null,
				'Cancelled',
				0,
				'T',
				0,
				4,
				null,
				1,
				null
			union select
				<cf_queryparam value="FlagGroup.#newFlagGroupTextID#.RegStatus" CFSQLTYPE="CF_SQL_VARCHAR" >,
				null,
				'Confirmed',
				0,
				'T',
				0,
				5,
				null,
				1,
				null
			union select
				<cf_queryparam value="FlagGroup.#newFlagGroupTextID#.wave" CFSQLTYPE="CF_SQL_VARCHAR" >,
				null,
				'AM',
				0,
				'T',
				0,
				0,
				null,
				1,
				null
			union select
				<cf_queryparam value="FlagGroup.#newFlagGroupTextID#.wave" CFSQLTYPE="CF_SQL_VARCHAR" >,
				null,
				'PM',
				0,
				'T',
				0,
				1,
				null,
				1,
				null
			union select
				<cf_queryparam value="FlagGroup.#newFlagGroupTextID#.wave" CFSQLTYPE="CF_SQL_VARCHAR" >,
				null,
				'AMPM',
				0,
				'T',
				0,
				2,
				null,
				1,
				null
		</cfquery>

	</cftransaction>
	<!--- now submit the form, to make sure any data already in existence gets remembered --->

	<FORM NAME="addEventGroup" ACTION="eventAddEvent.cfm" METHOD="post">
	<CFOUTPUT>
	<INPUT TYPE="Hidden" NAME="frmAction" VALUE="AddEventEdit">
	<CF_INPUT type="hidden" name="frmEventGroupID" value="#GetEventGroupID.EventGroupID#">
	<CF_INPUT type="hidden" name="frmTitle" value="#frmTitle#">
	<CF_INPUT TYPE="Hidden" NAME="frmLocation" VALUE="#frmLocation#">
	<CF_INPUT TYPE="Hidden" NAME="frmVenueRoom" VALUE="#frmVenueRoom#">
	<CF_INPUT TYPE="Hidden" NAME="frmCountryID" VALUE="#frmCountryID#">
	<CF_INPUT TYPE="Hidden" NAME="frmEstAttendees" VALUE="#frmEstAttendees#">
	<CF_INPUT TYPE="Hidden" NAME="frmPreferredStartDate" VALUE="#frmPreferredStartDate#">
	<CF_INPUT TYPE="Hidden" NAME="frmPreferredEndDate" VALUE="#frmPreferredEndDate#">
	<CF_INPUT TYPE="Hidden" NAME="frmEmail" VALUE="#frmEmail#">
	<CF_INPUT TYPE="Hidden" NAME="frmEmailCC" VALUE="#frmEmailCC#">
	<CF_INPUT TYPE="Hidden" NAME="frmUseAgency" VALUE="#frmUseAgency#">
	<CF_INPUT TYPE="Hidden" NAME="frmEligibleCountryID" VALUE="#frmEligibleCountryID#">
	<CF_INPUT TYPE="Hidden" NAME="frmInvitationType" VALUE="#frmInvitationType#">
	<CF_INPUT TYPE="Hidden" NAME="frmAudienceType" VALUE="#frmAudienceType#">
	<CF_INPUT TYPE="Hidden" NAME="frmPartnerType" VALUE="#frmPartnerType#">
	<CF_INPUT TYPE="Hidden" NAME="frmRegistrationScreenID" VALUE="#frmRegistrationScreenID#">
	<CF_INPUT TYPE="Hidden" NAME="frmMaxTeamSize" VALUE="#frmMaxTeamSize#">

	</CFOUTPUT>
	</FORM>
	<SCRIPT type="text/javascript">
	<!--
	document.addEventGroup.submit();
	//-->
	</SCRIPT>

</cfcase>

</cfswitch>
<cfset displaysave="yes">
<cfinclude template="eventFooter.cfm">