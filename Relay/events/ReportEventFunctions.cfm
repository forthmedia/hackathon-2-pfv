<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			ReportEventFunctions.cfm
Author:				MDC
Date started:		2005-04-02
	
Description:			

creates functionList query for tableFromQueryObject custom tag

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
0d-APR-2005			MDC			Initial version

Possible enhancements:


 --->

<cfscript>
comTableFunction = CreateObject( "component", "relay.com.tableFunction" );

comTableFunction.functionName = "Phr_Sys_EventFunctions";
comTableFunction.securityLevel = "";
comTableFunction.url = "";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();

comTableFunction.functionName = "--------------------";
comTableFunction.securityLevel = "";
comTableFunction.url = "";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();

comTableFunction.functionName = "Phr_Sys_Selections";
comTableFunction.securityLevel = "";
comTableFunction.url = "";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();

//comTableFunction.functionName = "&nbsp;Opportunity Letter";
//comTableFunction.securityLevel = "";
//comTableFunction.url = "/content/mergeDocs/MSWordLetterTemplate.cfm?personid=2&ignoreExtraneousParameter=";
//comTableFunction.windowFeatures = "width=1024,height=800,toolbar=1,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1";
//comTableFunction.functionListAddRow();

comTableFunction.functionName = "&nbsp;Save checked records as a selection";
comTableFunction.securityLevel = "";
comTableFunction.url = "/selection/selectalter.cfm?frmtask=save&frmruninpopup=true&frmpersonids=";
comTableFunction.windowFeatures = "width=500,height=600,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1";
comTableFunction.functionListAddRow();

</cfscript>

