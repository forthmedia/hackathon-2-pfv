<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Amendments
2000-01-06 WAB Amended download - was missing out the first line.  Also fiddled with it!
2000-01-07 WAB Added extra line to warn if name is different from main database
2000-01-07 WAB Added extra line to advise if the registration has been transferred from/to someone else
2000-01-12 WAB Added function to display just the checked items.  Also changed working of frmCheckISs and frmAllCheckIDs a bit
2000-03-24 DJH Added Partner Type to the page and download of page
2000-05-04   WAB Altered Download Functionality - all put into a single file
2001-04-11 EMS added regdate field for registration date in delegate list am pulling info from the confirmationsent field however as the regdate doesnt always seem to be getting set
2000-06-11  WAB Tried to make getLists query to work better (), also corrected error in download of current page (was always doing whole list not just the page)
			added address details to the download - These come from the eventflagdata table if they have been entered, otherwise from the location table.
2008/09/24 NJH Added notes and related files for a delegate (student) as part of elearning 8.1
2008/10/23 NJH Sophos RelayWare Set Up Bug Fix Issue 1104 - Placed a check around the reloading the page to view only checked delegates as it was falling over when no delegates were checked.
2009/07/17 NJH P-FNL069 encrypt url variables for call to entityRelatedFile
2014/04/15 DCC added hook to custom page
2015/12/07 SB Bootstrap and removed tables
--->
<cfif fileexists("#application.paths.code#\cftemplates\Modules\events\customeventDelegateList.cfm")>
 	<cfinclude template="/Code/CFTemplates/Modules/events/customeventDelegateList.cfm">
<cfelse>

	<cf_title>Event Delegate List</cf_title>

	<CFSET ManagerOK = 0>
	<CFSET SuperOK = 0>

	<CFIF checkPermission.updateOkay GT 0><CFSET ManagerOK = 1></CFIF>
	<CFIF checkPermission.addOkay GT 0><CFSET SuperOK = 1></CFIF>


	<!--- get the countries the current user has rights to view
	returns a list of CountryIDs as a variable called Variables.CountryList --->
	<cfset CreateCountryRightsTable = true>    <!--- WAB added --->
	<cfinclude template="/templates/qrygetcountries.cfm">
	<cfset CreateCountryRightsTable = false>

	<!--- make sure it's a valid request --->
	<CFIF NOT IsDefined("FlagID")>
		<cflocation url="eventHome.cfm"addToken="false">
		<CF_ABORT>
	<CFELSEIF NOT IsNumeric(FlagID)>
		<cflocation url="eventHome.cfm"addToken="false">
		<CF_ABORT>
	</CFIF>

	<!--- set default variables --->
	<CFSET FreezeDate = request.requestTimeODBC>
	<CFSET Start = 1>
	<CFPARAM NAME="frmTotalChecks" DEFAULT="">
	<CFPARAM NAME="frmOrder" DEFAULT="LastName">
	<CFPARAM NAME="filter" DEFAULT="">
	<CFPARAM NAME="criteria" DEFAULT="">
	<CFPARAM NAME="srchName" DEFAULT="">
	<CFPARAM NAME="srchRegID" DEFAULT="">
	<CFPARAM NAME="countryID" DEFAULT="">
	<CFPARAM NAME="countryGroupID" DEFAULT="">
	<CFPARAM NAME="frmCheckIDs" DEFAULT="">	<!--- all checks on page just submitted --->
	<CFPARAM NAME="frmAllCheckIDs" DEFAULT=""> <!--- all checks on all the other pages --->
	<cfif frmCheckIDs is not "">
		<CFSET frmAllCheckIds = listappend(frmAllCheckIds,frmCheckIds)>  <!--- combine them so that frmAllCheckIds is now all the checked IDs --->
	</cfif>
	<!--- need to dynamically get the name of the event flag data table as this might change --->
	<CFQUERY NAME="GetEventTable" datasource="#application.siteDataSource#">
		SELECT
			ft.DataTable,
			f.FlagTextID
		FROM
			Flag AS f,
			FlagGroup AS fg,
			FlagType AS ft
		WHERE
			f.FlagGroupID = fg.FlagGroupID
			AND fg.FlagTypeID = ft.FlagTypeID
			AND f.FlagID =  <cf_queryparam value="#FlagID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</CFQUERY>
	<CFSET EventTable = "#GetEventTable.DataTable#FlagData">

	<!--- get the Event details --->
	<cfinclude template="sql_GetEventDetails.cfm">

	<!--- get the delegate list --->
	<!--- WAB altered to try and speed up, with not very much success! --->
	<CFQUERY NAME="GetLists" datasource="#application.siteDataSource#">
		SELECT
	--		et.*,
			et.EntityID,
			et.PartnerRegID,
			et.Title,
			et.FirstName,
			et.LastName,
			et.Email,
			et.SiteName,
			case when et.address1 is Null then  l.address1 else et.address1 end as address1,
			case when et.address1 is Null then  l.address2 else et.address2 end as address2,
			case when et.address1 is Null then  l.address3 else et.address3 end as address3,
			case when et.address1 is Null then  l.address4 else et.address4 end as address4,
			case when et.address1 is Null then  l.address5 else et.address5 end as address5,
			case when et.address1 is Null then  l.PostalCode else et.postalcode end as postalcode,
			isnull(et.Telephone, l.Telephone) as eTelephone,
			isnull(et.Fax, l.Fax) as eFax,
			isnull(et.Mobile, p.MobilePhone) as eMobile,
			et.AssignedLanguage,
			et.RegStatus,
			et.AssignedCourseContent,
			et.CourseContent,
			et.AssignedWave,
			et.Wave,
			et.employeeCat,
			et.PartArrive,
			et.PartDepart,
			et.ConfirmationSent,
			et.transferredTo,
			et.transferredFrom,
			p.PersonID,
			p.Language,
			c.ISOCode,
			p.firstname as p_firstname,
			p.lastname as p_lastname,
			focus_f.name as partnertype,
			l.sitename as l_sitename
		FROM
			#EventTable# AS et
			inner join Person AS p
			on p.PersonID = et.EntityID
			inner join Location as l
			on l.locationid= p.locationid
			inner join Country AS c
			on c.CountryID = l.CountryID
			left join
			(
					BooleanFlagData as focus_bfd
					INNER JOIN Flag as focus_f ON focus_bfd.FlagID = focus_f.FlagID
					INNER JOIN FlagGroup as focus_fg ON focus_f.FlagGroupID = focus_fg.FlagGroupID
					and focus_fg.FlagGroupTextID  in ( <cf_queryparam value="#thePartnerType#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )
			)
			on l.locationid = focus_bfd.entityid
			inner join
			#countryRightsTableName# AS R
			ON L.countryid = r.countryid


		WHERE
			et.FlagID =  <cf_queryparam value="#FlagID#" CFSQLTYPE="CF_SQL_INTEGER" >
			<!--- if this is called from the stats page then filter the list accordingly --->
			<!--- search for regid / srchname ignores the other search parameters --->
			<cfif isDefined("frmFocusLevel") and frmFocusLevel is not "">
				and focus.PartnerType =  <cf_queryparam value="#frmFocusLevel#" CFSQLTYPE="CF_SQL_VARCHAR" >
			</cfif>
			<cfif isDefined("frmRegStatus") and frmRegStatus is not "">
				and et.regstatus =  <cf_queryparam value="#frmRegStatus#" CFSQLTYPE="CF_SQL_VARCHAR" >
			</cfif>

			<CFIF srchRegID NEQ "">
				AND et.PartnerRegID =  <cf_queryparam value="#srchRegID#" CFSQLTYPE="CF_SQL_INTEGER" >
			<!--- NJH 2008/10/23 Sophos RelayWare Set Up Bug Fix Issue 1104- added check that frmAllCheckIDs was defined and neq "" --->
			<CFELSEIF isdefined("srchChecks") and isDefined("frmAllCheckIDs") and frmAllCheckIDs neq "">
				AND et.entityid  in ( <cf_queryparam value="#frmAllCheckIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			<cfelseIF srchName NEQ "">
					AND (et.firstname  like  <cf_queryparam value="#srchName#%" CFSQLTYPE="CF_SQL_VARCHAR" >
						OR et.lastname  like  <cf_queryparam value="#srchName#%" CFSQLTYPE="CF_SQL_VARCHAR" >
						OR et.email  like  <cf_queryparam value="#srchName#%" CFSQLTYPE="CF_SQL_VARCHAR" >
						or et.sitename  like  <cf_queryparam value="#srchName#%" CFSQLTYPE="CF_SQL_VARCHAR" > )
			<CFELSE>
				<CFIF filter NEQ "">
					AND et.#filter# =  <cf_queryparam value="#criteria#" CFSQLTYPE="CF_SQL_VARCHAR" >
				</CFIF>
				<CFIF filter NEQ "" AND CountryID NEQ "" AND CountryID NEQ "0">
					AND l.CountryID =  <cf_queryparam value="#countryID#" CFSQLTYPE="CF_SQL_INTEGER" >
				<CFELSEIF filter NEQ "" AND CountryGroupID NEQ ""  >
					AND l.CountryID in (select countrymemberid from countrygroup where countrygroupid =  <cf_queryparam value="#countrygroupid#" CFSQLTYPE="CF_SQL_INTEGER" > )
				</CFIF>
			</CFIF>
		ORDER BY
			et.#frmOrder#
			<CFIF frmOrder NEQ "LastName">
				, et.LastName
			</CFIF>
	</CFQUERY>

	<CFIF GetLists.RecordCount GT Start>
		<!--- GCC - 2005/09/28 - A_674 --->
		<!--- define the start record --->
		<CFIF IsDefined("Forward")>
			 <CFSET Start = frmOrigStart + ListLimit>
	 	<CFELSEIF IsDefined("Back")>
			 <CFSET Start = frmOrigStart - ListLimit>
		<CFELSEIF IsDefined("frmOrigStart")>
		 	 <CFSET Start = frmOrigStart>
		</CFIF>
	</CFIF>

	<!--- these are the field values that can be included in analayis -
	was to be used to dynamically populate the pop-up menu --->
	<CFQUERY NAME="getEventFieldValues" datasource="#application.siteDataSource#">
		SELECT DISTINCT vfv.FieldName
		FROM ValidFieldValues AS vfv
		WHERE vfv.InAnalysis = 1
		AND FieldName  LIKE  <cf_queryparam value="flag.#GetEventTable.FlagTextID#%" CFSQLTYPE="CF_SQL_VARCHAR" >
	</CFQUERY>

	<!--- SWJ & MDC: 2007-02-14 This has been moved --->
	<cfquery name="statuses" datasource="#application.siteDataSource#">
		select distinct
			regstatus
		from
			eventflagdata
		where
			flagid =  <cf_queryparam value="#flagID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</cfquery>
	<cfparam name="frmRegStatus" default="">

	<!--- structure used to get nice names for the field values that can be included in analayis --->
	<CFSCRIPT>
		AnalysisNames = StructNew();
		StructInsert(AnalysisNames,"RegStatus","Registration Status");
		StructInsert(AnalysisNames,"assignedwave","Assigned Wave");
		StructInsert(AnalysisNames,"assignedCourseContent","Assigned Course");
		StructInsert(AnalysisNames,"assignedLanguage","Assigned Language");
		StructInsert(AnalysisNames,"sitename","Delegate Site");
	</CFSCRIPT>

	<!--- set variable EventManager to specifiy if the user
	has been given explicit rights to this event --->
	<CFQUERY NAME="GetRecordRights" datasource="#application.siteDataSource#">
		SELECT RecordID
		FROM RecordRights
		WHERE Entity = 'Flag'
		AND UserGroupID =  <cf_queryparam value="#ListGetAt(Cookie.user, 2, "-")#" CFSQLTYPE="CF_SQL_INTEGER" >
		AND RecordID =  <cf_queryparam value="#flagid#" CFSQLTYPE="CF_SQL_INTEGER" >
	</CFQUERY>
	<CFSET EventManager = GetRecordRights.RecordCount>
	<cfinclude template="eventTopHead.cfm">

	<SCRIPT type="text/javascript">

	<CFINCLUDE TEMPLATE="../screen/jsViewEntity.cfm">

	</SCRIPT>


	<cf_WhatsLeftToDo FlagID="#FlagID#">
	<cfif session.eventWizard is "true">
		<div class="col-xs-12 col-sm-8 col-md-9 col-lg-10">
			<h2>Steps required in setting up an event</b></h2>
			<h3>Step 3a:</h3>
			<p>
				You've chosen to have delegates invited - not to allow open invitation. You've just chosen your selections,
				and this screen displays each delegate included in those selections. You can invite people now, or come back later
				to do so.
			</p>
			<cfif UseAgency>
			<p>
				Alternatively, you may wish to allow the Agency to sort out the invitations.
			</p>
			</cfif>
		</div>
	</cfif>
	<!---SB end div from WhatsLeftToDo --->
	</div>
	<CFIF getLists.RecordCount IS 0>
		<p>No Delegates that you have permission to view have been added to this event.</p>

		<TABLE class="01">
		<TR VALIGN="top">
			<TD ALIGN="right"><CFIF ManagerOK and eventManager><cfparam name="addonly" default="yes"><CFINCLUDE TEMPLATE="eventPopUp.cfm"><CFELSE>&nbsp;</CFIF></TD>
		</TR>
		</TABLE>
		<FORM NAME="doList" ACTION="eventTask.cfm" METHOD="post" TARGET="PopUp">
		<CFOUTPUT>
		<CF_INPUT TYPE="Hidden" NAME="FlagID" VALUE="#FlagID#">
		<CF_INPUT TYPE="Hidden" NAME="frmEventTable" VALUE="#EventTable#">
		<INPUT TYPE="Hidden" NAME="frmAction" VALUE="">
		</CFOUTPUT>
		</FORM>
		<cfoutput>
		<form name="eventStep" action="" method="post">
			<CF_INPUT type="hidden" name="FlagID" value="#FlagID#">
			<input type="hidden" name="dirty" value="0">
		</form>
		<SCRIPT type="text/javascript">
			function submitForm(whereto)
			{
				if(whereto == -1)
				{
					document.eventStep.action = "eventAddDelegateWiz.cfm?eventStep=3&FlagID=#jsStringFormat(FlagID)#"
				}
				else
				{
					document.eventStep.action = "eventWebContent.cfm?frmAction=EditAgenda&eventStep=3"
				}
				document.eventStep.submit()
			}
		</script>
		</cfoutput>


		<cfinclude template="eventFooter.cfm">
		<CF_ABORT>
	</CFIF>
	<CFIF ManagerOK AND EventManager>
		<div class="row">
			<div class="col-xs-12 col-sm-6">
				<div class="grey-box-top">
					<cfinclude template="eventDelegateListFilters.cfm">
				</div>
			</div>
			<div class="col-xs-12 col-sm-6">
				<div class="grey-box-top">
					<CFINCLUDE TEMPLATE="eventPopUp.cfm">
				</div>
			</div>
		</div>
	</CFIF>


	<CFSET Title = GetEventDetails.Title>

	<!---
	WAB: removed to eventMakeDownloadFile.cfm
	 <!---Set variables for the file Download--->
	<CF_TemporaryFileName FileName="DelegateList" Extension = "xls">
	<CFSET statFilePath = dosFilePath>
	<CFSET webstatFilePath = webFilePath>

	<!---Column Headings for the Download File--->
	<CFSET ColumnHeadings = 'Name	Surname	Email	Partner Type	Site	Town	Country	Language	Telephone	Mobile Phone	Fax Phone	Status	Course Content	Wave	Registration Number	Category	Arrive	Depart	RegistrationDate'>
	<CFSET delim = "#Chr(9)#">
	<!---Data to write in the Download File--->
	<CFSET List = '"##FirstName####delim####LastName####delim####Email####delim####PartnerType####delim####SiteName####delim####Town####delim####ISOCode####delim####AssignedLanguage####delim####eTelephone####delim####eMobile####delim####eFax####delim####RegStatus####delim####iif(len(AssignedCourseContent) is 0,DE(CourseContent),DE(AssignedCourseContent))####delim####iif(len(AssignedWave) is 0,DE(Wave),DE(AssignedWave))####delim####PartnerRegID####Delim####employeeCat####delim####dateformat(PartArrive,"dd-mm-yy")####delim####dateformat(PartDepart,"dd-mm-yy")####Delim####dateformat(ConfirmationSent,"dd-mm-yy")## ##timeformat(ConfirmationSent,"HH:mm")##"'>

	<!--- Open file and write column headings row --->
	<CFFILE ACTION="WRITE" FILE="#StatFilePath#" OUTPUT="#ColumnHeadings#">
	 --->


	<TABLE class="table table-hover table-striped">
		<tr>
			<td colspan="3">
			<!--- NJH 2008/10/23 Sophos RelayWare Set Up Bug Fix Issue 1104 - removed the target as it was opening in a new window in the new look and feel. --->
			<FORM NAME="doList" ACTION="eventDelegateList.cfm" METHOD="post" <!--- TARGET="main" --->>
			<CFOUTPUT>
			<CF_INPUT TYPE="Hidden" NAME="FlagID" VALUE="#FlagID#">
			<CF_INPUT TYPE="Hidden" NAME="filter" VALUE="#filter#">
			<CF_INPUT TYPE="Hidden" NAME="criteria" VALUE="#criteria#">
			<CF_INPUT TYPE="Hidden" NAME="countryID" VALUE="#countryID#">
			<CF_INPUT TYPE="Hidden" NAME="countryGroupID" VALUE="#countryGroupID#">
			<CF_INPUT TYPE="Hidden" NAME="frmOrigStart" VALUE="#Start#">
			<CF_INPUT TYPE="Hidden" NAME="frmTotalChecks" VALUE="#frmTotalChecks#">
			<CF_INPUT TYPE="Hidden" NAME="frmEventTable" VALUE="#EventTable#">
			<INPUT TYPE="Hidden" NAME="frmAction" VALUE="">
			<CF_INPUT TYPE="Hidden" NAME="frmAllCheckIDs" VALUE="#frmAllCheckIDs#">
			<!---  SWJ & MDC: 2007-02-14 No longer relevant <input type="hidden" name="frmFocusLevel" value="#frmFocusLevel#"> --->
			<CF_INPUT type="hidden" name="frmRegStatus" value="#frmRegStatus#">
			</CFOUTPUT>

			<CFOUTPUT>
				<cfif getlists.recordcount gt 1>
					<CFIF (Start + ListLimit - 1) LT GetLists.RecordCount>
						Record(s) #htmleditformat(Start)# - #Evaluate("Start + ListLimit - 1")# of #GetLists.RecordCount#
					<CFELSE>
						Record(s) #htmleditformat(Start)# - #GetLists.RecordCount# of #GetLists.RecordCount#
					</CFIF>
				</cfif>
			</CFOUTPUT>
			</td>
		</tr>
		<TR>
			<th>&nbsp;</th>
			<th>Delegate</th>
			<th>Site</th>
			<th>Level</th>
			<th>Country</th>
			<th>Language</th>
			<th>Telephone</th>
			<th>Mobile</th>
			<th>Status</th>
			<!--- SWJ & MDC: 2007-02-14 switched off for the moment <TD CLASS="Label">Course Content</TD> --->
			<th>Registration Date</th>
			<!--- 2008/09/24 - elearning 8.1 add notes/related files for a delegate --->
			<th>Notes</th>
			<th>Related Files</th>
			<!--- SWJ & MDC: 2007-02-14 switched off for the moment  <TD WIDTH="20" CLASS="Label">Wave</TD> --->
			<th>&nbsp;</th>
			<cfif getEventDetails.ShowEditRegistration><CFIF SuperOK >
				<th>Register</th>
			</CFIF></cfif>
		</TR>
	<CFOUTPUT QUERY="GetLists" STARTROW="#Start#" MAXROWS="#ListLimit#">


		<!--- get the partner type for this person --->
		<!---
		<cfquery name="PartnerType" datasource="#application.siteDataSource#">
		SELECT
			f.Name
		FROM
			Person as p INNER JOIN
			Location as l ON p.LocationID = l.LocationID
			INNER JOIN BooleanFlagData as bgf ON l.LocationID = bgf.EntityID
			INNER JOIN Flag as f ON bgf.FlagID = f.FlagID
			INNER JOIN FlagGroup as fg ON f.FlagGroupID = fg.FlagGroupID
		WHERE
			p.personID = #GetLists.PersonID#
			<!--- hack added for Valerie Vliet ---->
			and fg.FlagGroupTextID in ('#thePartnerType#' <cfif flagid is 1236 or flagid is 1237>,'nppPartnerType'</cfif>)
		</cfquery>

		<cfset PartnerType=PartnerType.Name>
		--->


		<TR>
			<TD>
			<CFIF ManagerOK AND EventManager><INPUT TYPE="checkbox" NAME="frmCheckIDs" VALUE="#EntityID#"
				<CFIF ListFind(frmAllCheckIDs, EntityID) GT 0>
					<CFSET frmAllCheckIDs = ListDeleteAt(frmAllCheckIDs, ListFind(frmAllCheckIDs, EntityID))> CHECKED
				</CFIF> >
			<CFELSE>
				&nbsp;
			</CFIF>
			</TD>
			<TD>#FirstName# #LastName#</TD>
			<TD>#SiteName#</TD>
			<td>#PartnerType#</td>
			<TD>#ISOCode#</TD>
			<TD>#AssignedLanguage#</TD>
			<td>#eTelephone#</td>
			<td>#eMobile#</td>
			<TD>#htmleditformat(RegStatus)#</TD>
			<!--- SWJ & MDC: 2007-02-14 switched off for the moment <TD>#iif(AssignedCourseContent is "", DE(CourseContent), DE(AssignedCourseContent))#</TD> --->
			<TD>#htmleditformat(ConfirmationSent)#</TD>
			<!--- NJH 2008/09/24 elearning 8.1 - add/view notes and related files for a delegate --->
			<td><a href="javascript:void(openWin('/data/entityNoteView.cfm?frmEntityTypeID=22&frmEntityType=Delegate&frmEntityID=#EntityID#','PopUp','width=700,height=500,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1'));">View</a></td>
			<td>
				<!--- NJH 2009/07/17 P-FNL069 - encrypt the url variables and use relatedFile rather than hardcoded to v2, as related file now calls v2--->
				<cf_relatedFile action="variables" entity="#EntityID#" entitytype="EventFlagData">

				<cfset encryptedUrlVariables = application.com.security.encryptQueryString("?entityID=#EntityID#&entityType=EventFlagData")>
				<a href="javascript:void(window.open('/screen/entityRelatedFilePopup.cfm#encryptedUrlVariables#','UploadFiles','width=600,height=350,status,resizable,toolbar=0'))" onMouseOver="window.status='';return true" onMouseOut="window.status='';return true">View/Upload</a>
			</td>
			<!--- SWJ & MDC: 2007-02-14 switched off for the moment <TD WIDTH="20">#iif(AssignedWave is "", DE(Wave), DE(CourseContent))#</TD> --->
			<TD><CFIF ManagerOK AND EventManager><!---MDC commented out as it does not work <A HREF="JavaScript:viewEntity__('Person',saveChecks(),'#EntityID#','Events','','','eventflagtextid=#GetEventTable.FlagTextID#');">edit</A> --->&nbsp;<CFELSE>&nbsp;</CFIF></TD>
			<cfif getEventDetails.ShowEditRegistration>
			<TD>
					<CFIF SuperOK ><A HREF="#evaluate("'#getEventDetails.RegURL#'")#">#htmleditformat(PartnerRegID)#</a>
					<CFELSE>#htmleditformat(partnerRegID)#
					</CFIF>
			</TD>
			</cfif>
		</TR>
			<CFIF FirstName is not p_firstname or lastName is not p_lastname or sitename is not l_sitename>
				<TR>
					<TD>&nbsp</td>
					<TD colspan="8">Note: Person details in main database are: #htmleditformat(p_firstname)# #htmleditformat(p_lastname)# #htmleditformat(l_sitename)#</td>
				</tr>
			</cfif>

			<CFIF TransferredTo is not "">
				<CFQUERY NAME="TransferredTo" datasource="#application.siteDataSource#">
					Select Firstname, Lastname
					From Person
					Where Personid =  <cf_queryparam value="#TransferredTo#" CFSQLTYPE="CF_SQL_INTEGER" >
				</CFQUERY>
				<TR>
					<TD>&nbsp</td>
					<TD colspan ="8">Transferred to #htmleditformat(TransferredTo.FirstName)# #htmleditformat(TransferredTo.LastName)#	</td>
				</tr>
			</cfif>

			<CFIF TransferredFrom is not "">
				<CFQUERY NAME="TransferredFrom" datasource="#application.siteDataSource#">
					Select Firstname, Lastname
					From Person
					Where Personid =  <cf_queryparam value="#TransferredFrom#" CFSQLTYPE="CF_SQL_INTEGER" >
				</CFQUERY>
				<TR>
					<TD>&nbsp</td>
					<TD colspan ="8">Transferred from #htmleditformat(TransferredFrom.FirstName)# #htmleditformat(TransferredFrom.LastName)#	</td>
				</tr>
			</cfif>


	<!--- 		removed to eventMakeDownloadFile.cfm
		<!--- Write data line to file--->
			<CFSET thisLine = evaluate("#List#")>
			<CFFILE ACTION="APPEND" FILE="#StatFilePath#" OUTPUT="#thisLine#">
	 --->

	</CFOUTPUT>
	</TABLE>

	<!---
		<!---Download Link--->
		<A HREF="<CFOUTPUT>#webstatFilePath#</CFOUTPUT>">Download this page</A>
	 --->

	<cfset downloadStart = start>
	<cfset downloadEnd = start + listLimit -1>
	<!--- <cfinclude template = "eventMakeDownloadFile.cfm">

	<cfoutput>
	 <A HREF = "#webstatFilePath#" >Download this page</a>
	</cfoutput>  --->



	<!---
	WAB removed this
	<CFIF GetLists.RecordCount GT ListLimit> --->
		<!--- SWJ & MDC: 2007-02-14 switched off for the moment <a href="eventDownloadDelegates.cfm?dontLoadStyles=yes&FlagID=<cfoutput>#FlagID#</cfoutput>">Download all Delegates</a> --->
	<!--- </cfif> --->

	<!--- NJH 2008/10/23 Sophos RelayWare Set Up Bug Fix Issue 1104 - added a check to ensure that some people had been checked before submitting the form. --->
	<script>
		function reloadPageWithCheckedPeople() {
			var form = document.doList;
			var count = 0;
			for (j=0; j<form.frmCheckIDs.length; j++){
				thisDelegate = form.frmCheckIDs[j];
				if (thisDelegate.checked) {
					count = count+1;
				}
			}
			if (count != 0) {
				form.frmOrigStart.value=1;
				form.submit();
			} else {
				alert('No people have been checked.')
			}
		}
	</script>

	<CFIF ManagerOK AND EventManager>
		<label>Reload this Page showing only checked people</label>
	<!--- NJH 2008/10/23 Sophos RelayWare Set Up Bug Fix Issue 1104 - changed from a submit to a button and a hidden variable --->
	<input type="button" name="srchChecks" value="Go" onClick="javascript:reloadPageWithCheckedPeople()" class="btn btn-primary">
	<input type="hidden" name="srchChecks" value="Go">
	<!--- <input type="Submit" name="srchChecks" value="Go"> ---></cfif>
	<!---
	<CFOUTPUT>

	<INPUT TYPE="Hidden" NAME="FlagID" VALUE="#FlagID#">
	<INPUT TYPE="Hidden" NAME="filter" VALUE="#filter#">
	<INPUT TYPE="Hidden" NAME="criteria" VALUE="#criteria#">
	<INPUT TYPE="Hidden" NAME="countryID" VALUE="#countryID#">
	<INPUT TYPE="Hidden" NAME="countryGroupID" VALUE="#countryGroupID#">
	<INPUT TYPE="Hidden" NAME="frmOrigStart" VALUE="#Start#">
	<INPUT TYPE="Hidden" NAME="frmTotalChecks" VALUE="#frmTotalChecks#">
	<INPUT TYPE="Hidden" NAME="frmEventTable" VALUE="#EventTable#">
	<INPUT TYPE="Hidden" NAME="frmAction" VALUE="">
	<INPUT TYPE="Hidden" NAME="frmAllCheckIDs" VALUE="#frmAllCheckIDs#">
	</CFOUTPUT>
	--->
	<P>
	<CFIF GetLists.RecordCount GT ListLimit>

				<CFIF Start GT 1>
					<input type="submit" name="Back" value="Previous" class="btn btn-primary">
				</CFIF>

				<CFOUTPUT>
				<CFIF (Start + ListLimit - 1) LT GetLists.RecordCount>
					Record(s) #htmleditformat(Start)# - #Evaluate("Start + ListLimit - 1")# of #GetLists.RecordCount#
				<CFELSE>
					Record(s) #htmleditformat(Start)# - #GetLists.RecordCount# of #GetLists.RecordCount#
				</CFIF>
				</CFOUTPUT>


				<CFIF (Start + ListLimit - 1) LT GetLists.RecordCount>
					<input type="submit" name="Forward" value="Next" class="btn btn-primary">
				</CFIF>

	</CFIF>
	</FORM>

	<form name="eventStep" action="" method="post">
		<CF_INPUT type="hidden" name="FlagID" value="#FlagID#">
		<input type="hidden" name="dirty" value="0">
	</form>

	<cfoutput>
	<SCRIPT type="text/javascript">
		function submitForm(whereto)
		{
			if(whereto == -1)
			{
				document.eventStep.action = "eventAddDelegateWiz.cfm?eventStep=3&FlagID=#jsStringFormat(FlagID)#"
			}
			else
			{
				document.eventStep.action = "eventWebContent.cfm?frmAction=EditAgenda&eventStep=3"
			}
			document.eventStep.submit()
		}
	</script>
	</cfoutput>


	<cfinclude template="eventFooter.cfm">
</cfif>