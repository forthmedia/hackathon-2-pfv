<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File:			EventDiary.cfm
Author:			DJH
Date created:	27 January 2000

Description:
Displays a list of selectable events for a particular month.

Version history:
1

Amendment History
2008-07-22		NYF		Bug 528 - replaced href value around event link as function required ShowNav to be true to work which it wasn't. Kept in option in case it ever is.
2015-11-25  	SB		Bootstrap
--->

<cf_title>Event Diary</cf_title>

<cfparam name="dateFrom" default="#createdate(year(now()),month(now()),01)#">
<!--- get the events that the user has country rights to see --->

<cfinclude template="eventTopHead.cfm">


<CFQUERY NAME="getEvent" datasource="#application.siteDataSource#">
	SELECT
		eg.EventName,
		eg.EventGroupID,
		ed.FlagID,
		ed.Title,
		ed.Location,
		ed.VenueRoom,
		ed.CountryID,
		ed.EstAttendees,
		ed.EventGroupID,
		ed.PreferredStartDate,
		ed.PreferredEndDate,
		ed.AllocatedStartDate,
		ed.AllocatedEndDate,
		isnull(ed.AllocatedStartDate,ed.PreferredStartDate) as StartDate,
		isnull(ed.AllocatedEndDate,ed.PreferredEndDate) as EndDate,
		ed.Email,
		ed.EmailCC,
		ed.UseAgency,
		ed.InvitationType,
		ed.EventStatus,
		p.FirstName,
		p.LastName,
		p.OfficePhone,
		ed.Date,
		c.ISOCode,
		c.COUNTRYDESCRIPTION
	FROM
		EventDetail as ed,
		EventGroup as eg,
		Person AS p,
		Country as c
	WHERE
		eg.EventGroupID = ed.EventGroupID
		AND p.PersonID = ed.OwnerID
		and (
			select count(*)
			from eventcountry as ec
			where ec.flagid = ed.flagid
			<!--- 2012-08-01 PPB P-SMA001 commented out
			and ec.countryid  IN ( <cf_queryparam value="#Variables.CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			--->
			#application.com.rights.getRightsFilterWhereClause(entityType="EventCountry",alias="ec").whereClause# 	<!--- 2012-08-01 PPB P-SMA001 Case 429847 probably this would be neater as a join on the main query and use entityType="EventDetail"  --->
			) > 0
		AND ed.CountryID = c.CountryID
	ORDER BY
		isnull(ed.AllocatedStartDate, ed.PreferredStartDate),
		ed.FlagID

</CFQUERY>
<!--- PJP - 2012-08-13:  Added in Default dateFrom and dateTo, so that inital page load doesn't appear blank --->
<cfif getEvent.recordcount>
	<cfset firstDate=now()>
	<cfparam name="dateFrom" default="#createDate(year(firstDate), month(firstDate), 1)#">
	<cfset lastStartDate=ArrayMax(ListToArray(ValueList(getEvent.StartDate)))>
	<cfparam name="dateTo" default="#createDate(year(lastStartDate), month(lastStartDate), DaysInMonth(lastStartDate))#">
</cfif>
<!--- now loop through every day in the month, to create a line per day populating each day with
all the events for that day --->
<cfset thisDate = dateFrom>
	<cfset row = 0>
	<cfset monthwas = 0>
	<table class="table table-hover table-striped">
	<cfloop condition="datecompare(thisDate, dateFrom) gte 0 and datecompare(thisDate, dateTo) lte 0">
		<cfif month(thisDate) is not monthwas>
			<cfset monthwas = month(thisDate)>
			<tr>
				<td colspan="<cfif session.AttendeeStats>9<cfelse>4</cfif>"><cfoutput><h3>#htmleditformat(monthAsString(month(thisdate)))#</h3></cfoutput></td>
			</tr>
			<TR>
				<th class="Label"><B>Date</B></th>
				<th class="Label"><B>Event</B></th>
				<th class="Label"><B>Country</B></th>
				<th class="Label"><B>Owner</B></th>
				<cfif session.AttendeeStats>
					<th WIDTH="100" class="Label"><B>Est no of<br> attendees</B></th>
					<th WIDTH="100" class="Label"><B>No invited</B></th>
					<th WIDTH="100" class="Label"><B>No registered</B></th>
					<th WIDTH="100" class="Label"><B>Actual attendees</B></th>
					<th WIDTH="100" class="Label"><B>Status</B></th>
				</cfif>
			</TR>
		</cfif>
		<cfset row = row + 1>
		<cfoutput>
		<cfif dayofweek(thisDate) is 1 or dayofweek(thisDate) is 7>
			<cfset class="evenrow">
		<cfelse>
			<cfset class="oddrow">
		</cfif>
		<tr class="#class#">
			<td>
				<!--- PJP 2012-08-13: Made the date column look better --->
				<table cellpadding="0" cellspacing="0" style="background-color:transparent">
					<tr>
						<td width="65px" align="left">#dateformat(thisDate,"dddd")#</td>
						<td width="20px" align="left">#dateformat(thisDate,"dd")#</td>
						<td width="25px" align="left">#dateformat(thisDate,"mmm")#</td>
						<td width="20px" align="left">#dateformat(thisDate,"yyyy")#</td>
					</tr>
				</table>
			</td>
		</cfoutput>
		<cfset eventCtr=0>
		<cfloop query="getEvent">
		<cfoutput>
			<cfif thisDate is getEvent.startDate>
				<cfset eventCtr = eventCtr + 1>
				<cfif eventCtr gt 1>
					</tr>
					<tr>
					<td nowrap>
					<!--- PJP 2012-08-13: Made the date column look better --->
					<table cellpadding="0" cellspacing="0" style="background-color:transparent">
						<tr>
							<td width="65px" align="left">#dateformat(thisDate,"dddd")#</td>
							<td width="20px" align="left">#dateformat(thisDate,"dd")#</td>
							<td width="25px" align="left">#dateformat(thisDate,"mmm")#</td>
							<td width="20px" align="left">#dateformat(thisDate,"yyyy")#</td>
						</tr>
					</table>
					</td>
				</cfif>
				<!--- 2008-07-22 NYF replaced ->
				<TD WIDTH="300" bgcolor="#bgcol#"><a style="text-decoration: none" href="javascript:void(assignSelect(document.tophead.FlagID, #FlagID#))">#EventName# #location#</a>&nbsp;</TD>
				 <- with: --->
				<TD WIDTH="300" ><cfif ShowNav or application.com.flag.doesFlagExist(FlagId=FlagId)><a style="text-decoration: none" href=<cfif ShowNav>"javascript:void(assignSelect(document.tophead.FlagID, #htmleditformat(FlagID)#))"<cfelse>"eventDetails.cfm?flagid=#htmleditformat(FlagID)#"</cfif>>#htmleditformat(title)#</a><cfelse>#htmleditformat(title)#</cfif>&nbsp;</TD>
				<!--- 2008-07-22 NYF replaced ->
				<TD WIDTH="300" bgcolor="#bgcol#"><a style="text-decoration: none" href="javascript:void(assignSelect(document.tophead.FlagID, #FlagID#))">#ISOCode#</a>&nbsp;</TD>
				 <- with: --->
				<TD WIDTH="300" ><cfif ShowNav or application.com.flag.doesFlagExist(FlagId=FlagId)><a style="text-decoration: none" href=<cfif ShowNav>"javascript:void(assignSelect(document.tophead.FlagID, #htmleditformat(FlagID)#))"<cfelse>"eventDetails.cfm?flagid=#htmleditformat(FlagID)#"</cfif>>#htmleditformat(COUNTRYDESCRIPTION)#</a><cfelse>#htmleditformat(COUNTRYDESCRIPTION)#</cfif>&nbsp;</TD>
				<!--- <- 2008-07-22 NYF --->
				<TD WIDTH="100" >#htmleditformat(FirstName)# #htmleditformat(LastName)# #htmleditformat(OfficePhone)#</TD>
				<cfif session.AttendeeStats>
					<TD WIDTH="100" >#htmleditformat(EstAttendees)#&nbsp;</TD>
					<TD WIDTH="100" >&nbsp;</TD>
					<TD WIDTH="100" >&nbsp;</TD>
					<TD WIDTH="100" >&nbsp;</TD>
					<TD WIDTH="100" >#htmleditformat(EventStatus)#&nbsp;</TD>
				</cfif>
			<cfelseif getEvent.startDate gt thisDate and eventCtr lt 1>
				<td colspan="<cfif session.AttendeeStats>8<cfelse>3</cfif>" >&nbsp;</td>
				<cfbreak>
			<cfelseif thisDate gt lastStartDate>
				<td colspan="<cfif session.AttendeeStats>8<cfelse>3</cfif>" >&nbsp;</td>
				<cfbreak>
			</cfif>
		</cfoutput>
		</cfloop>
		<cfset thisDate = dateAdd("d",1,thisDate)>
		<cfif eventCtr lte 1></tr></cfif>
	</cfloop>
</table>