<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File:			sql_select_ResourceType.cfm
Author:			DJH
Date created:	1 February 2000

Description:

Version history:
1

--->

<cfparam name="whereclause" default="no">

<cfquery name="select_ResourceType" datasource="#application.siteDataSource#">
select
	ResourceTypeID,
	ResourceType
from
	ResourceType
where
	0=0
	and Active = 1
<cfif whereclause>
	<cfif isDefined("ResourceTypeID")>
		and ResourceTypeID =  <cf_queryparam value="#ResourceTypeID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</cfif>
	<cfif isDefined("ResourceType")>
		and ResoureType =  <cf_queryparam value="#ResourceType#" CFSQLTYPE="cf_sql_integer" >
	</cfif>
</cfif>
order by
	ResourceType,
	ResourceTypeID
</cfquery>
