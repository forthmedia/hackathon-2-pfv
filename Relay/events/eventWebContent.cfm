<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File:			eventWebContent.cfm
Author:			DJH
Date created:	27 January 2000

Description:
Allows the Web content and management to be handled

Version history:
1
NJH 2008/11/07 Bug Fix Sophos RelayWare Set Up Issue 1312.. flagID was hardcoded to be some flagID... changed it to be a variable
NAS 2010/03/17 3139 Add image path to Event 'DevEdit'
2011/10/05 PPB LID7683 allow suppression of confirmation email with checkbox
2011/10/18 PPB LID7683 put a <cf_translate> tag pair around a phrase
25/4/2014 DCC Added hook for custom
2014-10-02	RPW	CORE-763 Not able to set up an Event  (also dealt with as Case 442019 2014/10/15 DXC in previous branch)

--->

<cfif fileexists("#application.paths.code#\cftemplates\Modules\events\customeventWebContent.cfm")>
 	<cfinclude template="/Code/CFTemplates/Modules/events/customeventWebContent.cfm">
<cfelse>

	<cf_title>Events Web Content</cf_title>

	<cfinclude template="eventtophead.cfm">
		<!--- also sets IsOwner --->
	<cfparam name="frmAction" default="Agenda">
	<cfif frmAction is "">
		<cfset frmAction="editAgenda">
	</cfif>

	<!--- SWJ & MDC: 2007-02-14 switched off for the moment <cfif session.eventWizard is not "true">
		<table border="0" width="100%" cellpadding="2" cellspacing="2" style="background-color: ffffee;">
		<tr>
			<cfoutput>
				<cfif frmAction contains "Agenda">
					<td width="30%" bgcolor="ffff77" align="center">
						<span>Manage Agenda</span>
					</td bgcolor>
				<cfelse>
					<td width="30%" align="center">
						<a href="eventWebContent.cfm?frmAction=agenda&FlagID=#FlagID#" class="button">Manage Agenda</a>
					</td>
				</cfif>
				<cfif frmAction contains "files">
					<td width="30%" bgcolor="ffff77" align="center">
						<span>Manage Collateral</span>
					</td bgcolor>
				<cfelse>
					<td width="30%" align="center">
						<a href="eventWebContent.cfm?frmAction=files&FlagID=#FlagID#" class="button">Manage Images/Documents</a>
					</td>
				</cfif>
				<cfif frmAction contains "emails">
					<td width="30%" bgcolor="ffff77" align="center">
						<span>Manage Emails</span>
					</td bgcolor>
				<cfelse>
					<td width="30%" align="center">
						<a href="eventWebContent.cfm?frmAction=emails&FlagID=#FlagID#&ShowSave=No" class="button">Manage Emails</a>
					</td>
				</cfif>
			</cfoutput>
		</tr>
		</table>
	</cfif> --->
	<cfswitch expression="#frmAction#">
	<!--- 2007-02-15 SWJ now no longer used #
	<cfcase value="agenda">
		<form name="UpdateEvent" action="eventWebContent.cfm?frmAction=editAgenda" method="post">
			<input type="hidden" name="dirty" value="0">
			<CF_INPUT type="hidden" name="FlagID" value="#FlagID#">
			<table border="0">
				<tr>
					<td class="Label" align="right" valign="top"><p>Agenda</p></td>
					<cfoutput query="GetEventDetails">
						<td valign="top"><p>#Agenda#</p></td>
					</cfoutput>
				</tr>
				<cfif IsOwner>
				<tr>
					<td colspan="2" align="center">
					<input type="submit" value="Edit" name="editAgenda">
					</td>
				</tr>
				</cfif>
			</table>
		</form>
	</cfcase> --->

	<cfcase value="editAgenda,updateAgenda">
		<cfif not isDefined("eventStep")>
			<cfif not isdefined("form.dirty") and not isDefined("form.editAgenda")>
				<!--- user must have chosen to edit the screen then changed their minds, not made any changes
				and navigated to a new record --->
				<cflocation url="eventWebContent.cfm?frmAction=editAgenda&FlagID=#FlagID#"addToken="false">
			</cfif>
		</cfif>

		<cf_WhatsLeftToDo FlagID="#FlagID#"></td>

			<cfif session.eventWizard is "true">
			<div class="col-xs-12 col-sm-8 col-md-9 col-lg-10">
				<h2>Steps required in setting up an event</h2>
				<h3>Step <cfif ByInvitationOnly>4<cfelse>3</cfif>:</h3>
				<p>Add the details of your agenda here. This agenda shows when the user clicks on the event details link when registering for an event via the portal.</p>
			</div>
			</cfif>
		<!---SB end div from WhatsLeftToDo --->
		</div>
		<SCRIPT type="text/javascript">
			function submitForm(whereto)
			{
				var form=document.UpdateEvent
				form.submit()
			}
		</script>
		<cfif URL.frmAction eq "updateAgenda">
			<cf_eventNotificationEmail action="Compile" changes="Agenda updated">
			<cf_eventNotificationEmail action="send" FlagID="#FlagID#">

			<cfquery name="updateAgenda" datasource="#application.siteDataSource#">
				update
					eventDetail
				set
					Agenda =  <cf_queryparam value="#FORM.frmNewAgenda_HTML#" CFSQLTYPE="CF_SQL_VARCHAR" >
				where
					FlagID =  <cf_queryparam value="#FlagID#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfquery>

			<!--- NJH 2008/11/07 Bug Fix Sophos RelayWare Set Up Issue 1312.. flagID was hardcoded to be some flagID... changed it to be a variable --->
			<cflocation url="eventwebcontent.cfm?frmaction=emails&eventstep=5&flagID=#FlagID#"addToken="false">

		</cfif>
		<!--- 2007-02-15 SWJ Moved here because updates were not happening --->
		<cfinclude template="sql_GetEventDetails.cfm">

		<cfif IsOwner>
		<!--- <link href="#request.currentsite. stylesheet#" rel="stylesheet" media="screen,print" type="text/css"> --->
			<form name="UpdateEvent" action="eventWebContent.cfm?frmAction=updateAgenda&eventStep=4" method="post">
				<input type="hidden" name="dirty" value="0">
				<CF_INPUT type="hidden" name="FlagID" value="#FlagID#">
				<cfoutput query="GetEventDetails">
					<h2>Edit Website Popup Agenda</h2>
						<!--- <cfmodule template="/devedit/class.devedit.cfm"
							setName="frmNewAgenda"
							setWidth="450"
							setHeight="350"
							SetDocumentType="de_DOC_TYPE_SNIPPET"
							disablePreviewMode = "0"
							setValue="#Agenda#"
							setSnippetStyleSheet="/code/styles/DefaultPartnerStyles.css"
							setDeveditPath="/devedit"
							<!--- NAS 2010/03/17 3139 Add image path to Event 'DevEdit' --->
							setImagePath="/content/MiscImages"
							hideSaveButton="1"
							>  --->

						<cf_includeJavascriptOnce template="/ckeditor/ckeditor.js">
						<cf_includeJavascriptOnce template="/ckfinder/ckfinder.js">

						<!--- 2013/11/21	YMA	Case 437728 Allow style tag to be used in wysiwyg --->
						<cfoutput><TextArea name="frmNewAgenda_HTML" id = "frmNewAgenda_HTML" relayContext="event">#HTMLEDITFORMAT(Agenda)#</TEXTAREA></cfoutput>

						<script>
							<cfoutput>CKInstanceName = 'frmNewAgenda_HTML';

							var editor = CKEDITOR.replace( CKInstanceName,
								 {
									<cfif fileExists("#application.paths.relay#/javascript/ckeditor_config_event.js")>
									 	customConfig : '/javascript/ckeditor_config_event.js',
									<cfelse>
									 	customConfig : '/javascript/ckeditor_config.js',
									</cfif>
										filebrowserLinkUploadUrl : '/ckfinder/core/connector/cfm/connector.cfm?command=QuickUpload&type=Files&relayContext=event&currentFolder=/personal (#jsStringFormat(request.relayCurrentUser.personID)#)/',
										filebrowserImageUploadUrl : '/ckfinder/core/connector/cfm/connector.cfm?command=QuickUpload&type=Images&relayContext=event&currentFolder=/personal (#jsStringFormat(request.relayCurrentUser.personID)#)/',
										filebrowserFlashUploadUrl : '/ckfinder/core/connector/cfm/connector.cfm?command=QuickUpload&type=Flash&relayContext=event&currentFolder=/personal (#jsStringFormat(request.relayCurrentUser.personID)#)/',
										height:300
										 }
									 );

									CKFinder.setupCKEditor( editor, {basePath:'/ckfinder/'});
							</cfoutput>
						</script>

						<!--- <textarea name="frmAgenda" cols="60" rows="15" onchange="dirty.value=1">#Agenda#</textarea><br> --->
<!--- 						<cfif isOwner and cgi.http_user_agent contains "MSIE"><a href="eventWebContent.cfm?frmAction=editAgendaWebEdit&flagID=#FlagID#"><img src="/images/HTMLEDitor/edit.jpg" width=18 height=18 alt="Edit HTML" border="0"></a></cfif> --->
				</cfoutput>
			</form><br />
		<cfelse>
			<cfoutput>#GetEventDetails.Agenda#</cfoutput><!--- 2013-03-06 - RMB - 434102 - REMOVED htmleditformat --->
		</cfif>
	</cfcase>

	<!--- <cfcase value="editAgendaWebEdit">

	<SCRIPT type="text/javascript">
		function submitForm()
		{
			myEditor1copyValue();
			document.frmDocMaint.submit()
		}
	</script>

		<!--- update using the webeditpro program ---->
		<cfinclude template="/images/HTMLEditor/HTMLEditEventAgenda.cfm">
	</cfcase> --->

	<!--- SWJ 2007-02-14 This has been removed because the update is now handled above

	<cfcase value="updateAgendaOld">
		<cfif IsOwner>
			<cf_eventNotificationEmail action="Compile" changes="Agenda updated">
			<cf_eventNotificationEmail action="send" FlagID="#FlagID#">
			<cfif isdefined("TextHTML")>
				<cfset frmAgenda = TextHTML>
			</cfif>
			<cfquery name="updateAgenda" datasource="#application.siteDataSource#">
				update
					eventDetail
				set
					Agenda = '#frmAgenda#'
				where
					FlagID = #FlagID#
			</cfquery>
		</cfif>

		<cfif isDefined("url.eventStep")>
			<cfif url.eventStep is 2>
				<cflocation url="eventManager.cfm?RecordID=#FlagID#&Entity=Flag&ShortName=EventTask&FlagID=#FlagID#">
			<cfelseif url.eventStep is 3>
				<cflocation url="eventDelegateList.cfm?eventStep=3&FlagID=#FlagID#">
			<cfelseif url.eventStep is 5>
				<cflocation url="eventWebContent.cfm?frmAction=Emails&FlagID=#FlagID#"addToken="false">
			</cfif>

		<cfelse>
			<cflocation url="eventWebContent.cfm?frmAction=Agenda&FlagID=#FlagID#">
		</cfif>

	</cfcase> --->

	<!--- <cfcase value="files">
		<table border="0">
		<tr valign="top">
		<td><cf_WhatsLeftToDo FlagID="#FlagID#"></td>
		<td>
		<cfif session.eventWizard is "true">
			<table border="0" cellpadding="4" cellspacing="4" width="80%">
				<tr><td class="label"><b>Steps required in setting up an event</b></td></tr>
				<tr>
					<td class="creambackground" valign="top">
						<b>Step <cfif ByInvitationOnly>6<cfelse>5</cfif>:</b><br>
						<cfif IsOwner>
							<div class="Margin">
							<cfif UseAgency>
								Though you've chosen to use an agency to manage the event, you may still wish to set up
								the event collateral.
							</cfif>
							The event collateral can be added here.
						<cfelse>
							The event collateral would be added here - you do not have the necessary rights to add event collateral
						</cfif>
						</div>
					</td>
				</tr>
			</table>
		</cfif>
		</td>
		</tr>
		</table>
		<SCRIPT type="text/javascript">
			function submitForm(whereto)
			{
				var form=document.UpdateEvent
				if (!form) form=document.UpdateEvent[0]
				if (whereto != null)
				{
					if(whereto == -1)
					{
						form.action = "<cfif ByInvitationOnly>eventWebContent.cfm?eventStep=5&frmAction=emails<cfelse>eventWebContent.cfm?eventStep=4&frmAction=emails</cfif>"
					}
					else
					{
						form.action = "eventResourceBookings.cfm"
					}
				}
				form.submit()
			}
		</script>
		<cfif isOwner>
			<cfif isDefined("form.RelatedFile_ActualFileName")>
				<cf_eventNotificationEmail action="Compile" changes="Collateral amended">
				<cf_eventNotificationEmail action="send" FlagID="#FlagID#">
			</cfif>
			<cf_relatedFile action="list,put" entity="#flagid#" entitytype="event">
		<cfelse>
			<cf_relatedFile action="list" entity="#flagid#" entitytype="event">
		</cfif>
		<form name="UpdateEvent" action="eventWebContent.cfm" method="post">
			<input type="hidden" name="FlagID" value="<cfoutput>#FlagID#</cfoutput>">
			<input type="hidden" name="dirty" value="0">
		</form>
	</cfcase>
	 --->
	<cfcase value="Emails,updateEmails">
		<cfif URL.frmAction eq "updateEmails">
			<cf_eventNotificationEmail action="Compile" changes="Email updated">
			<cf_eventNotificationEmail action="send" FlagID="#FlagID#">
			<cfinclude template="sql_getEmails.cfm">
			<!--- loop through each email message - remember that the sql statement performs a left join between the eventGroupEmail and
			eventDetailEmail to see whether an event specific record has been created --->
			<cfloop query="getEmails">
				<!--- check whether for this type a specific email has been saved --->
				<cfif getEmails.EmailType is #url.EmailType#>
					<!--- found the coinciding email type - check whether subject and message contain text --->
					<cfparam name="frmSubject" default='#iif("#Subject##MessageText#" is "", de(getEmails.defaultSubject), de(getEmails.Subject))#'>
					<cfparam name="frmMessageText" default='#iif("#Subject##MessageText#" is "", de(getEmails.defaultMessageText), de(getEmails.MessageText))#'>
					<!--- 2014-10-02	RPW	CORE-763 Not able to set up an Event --->
					<cfif "#getEmails.subject##getEmails.messageText#" is not "">
						<!--- a record exists! update it! --->
						<cfinclude template="sql_updateEventDetailEmail.cfm">
					<cfelse>
						<!--- no record exists! add it! --->
						<cfinclude template="sql_insertEventDetailEmail.cfm">
					</cfif>
					<!--- a record was found - only one record gets updated per time - there may be
					more records in the query above but we don't need to see the rest - break --->
					<cfbreak>
				</cfif>
			</cfloop>
		</cfif>

		<cf_WhatsLeftToDo FlagID="#FlagID#">

			<cfif session.eventWizard is "true">
				<div class="col-xs-12 col-sm-8 col-md-9 col-lg-10">
					<h2>Steps required in setting up an event</b></h2>
					<h3>Step <cfif ByInvitationOnly>5<cfelse>4</cfif>:</h3>

					<cfif UseAgency>
						<p>Though you've chosen to use an agency to manage the event, you may still wish to set up
						the emails.</p>
					</cfif>
					<p>The emails are set up here. The current text for the emails is shown and can be edited below.</p>
				</div>
			</cfif>
			<!---SB end div from WhatsLeftToDo --->
		</div>
		<SCRIPT type="text/javascript">
			function submitForm(whereto)
			{
				var form=document.UpdateEmail1

				/*if (form == null) form = document.eventStep
				if (whereto != null)
				{
					if(whereto == -1)
					{
						form.action += "&eventStep=<cfif ByInvitationOnly>4<cfelse>3</cfif>"
					}
					else
					{
						form.action += "eventWebContent.cfm?frmAction=updateEmails&eventStep=5"
					}
				}*/
				form.submit()
			}
		</script>

		<SCRIPT type="text/javascript">
			// used by the onsubmit event of the form to ensure blank emails are not generated.
			function checkNotBlank(aform)
			{
				if (aform.frmSubject.value.length == 0 || aform.frmMessageText.value.length == 0)
				{
					alert("Subject and message text cannot be blank!")
					return false
				}
				return true
			}

			// returns an email to its default state - effectively deletes the specific one from the table
			function defaultIt(FlagID,EmailType)
			{
				if(confirm("If you're sure you want to return this email to the default, click OK. Otherwise click cancel."))
				{
					document.forms[0].action="eventWebContent.cfm?frmAction=defaultEmails&FlagID=" + FlagID + "&emailType=" + EmailType
					document.forms[0].submit()
				}
			}

		</script>
		<cfinclude template="sql_GetEventDetails.cfm">
		<cfinclude template="sql_getEmails.cfm">

		<!--- loop through each email message displaying an updatable form (given appropriate rights) for
		each record - remember that the sql statement performs a left join between the eventGroupEmail and
		eventDetailEmail to see whether an event specific record has been created --->
		<cf_translate>
		<cfloop query="getEmails">
			<cfoutput>
				<form name="UpdateEmail#getEmails.currentrow#" action="eventWebContent.cfm?frmAction=UpdateEmails&FlagID=#FlagID#&EmailType=#EmailType#" method="post" onSubmit="return checkNotBlank(this)">
					<input type="hidden" name="dirty" value="0">
				<div class="row">
					<div class="col-xs-12 col-sm-6">
						<div class="grey-box-top">
							<div class="form-group">
								<h2>Email Type: #htmleditformat(getEmails.emailType)# <cfif "#Subject##MessageText#" is "">(Default Message)<cfelse>(Event Specific Message)</cfif></h2>
							</div>
							<div class="form-group">
								<label><cfif "#Subject##MessageText#" is "">Default<cfelse>Current</cfif> subject line of the email:</label>
								<cfif IsOwner>
									<p>To change the subject, edit below</p>
								</cfif>
							</div>
							<div class="form-group">
								<label><cfif "#Subject##MessageText#" is "">#htmleditformat(getEmails.defaultSubject)#<cfelse>#htmleditformat(getEmails.Subject)#</cfif></label>
								<cfif IsOwner>
									<input type="text" class="form-control" size="35" name="frmSubject" value="<cfif "#Subject##MessageText#" is "">#htmleditformat(getEmails.defaultSubject)#<cfelse>#htmleditformat(getEmails.Subject)#</cfif>">
								</cfif>
							</div>
							<div class="form-group">
								<label>
									<cfif "#Subject##MessageText#" is "">Default<cfelse>Current</cfif> message text of the email
								</label>
								<cfif IsOwner>
									<p>To change the message text, edit below:</p>
								</cfif>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6">
						<div class="grey-box-top">
							<!--- START 	2013-01-28		MS 		P-NET026	Made the Person Merge Fields available for use in the email body so added a description here on how to use them --->
							<div class="form-group">
								<label>phr_eventManagerEmailConfirmationInstructionsToUseMergeStruct:</label>
							<!--- END 	2013-01-28		MS 		P-NET026	Made the Person Merge Fields available for use in the email body so added a description here on how to use them --->

								<cfif "#Subject##MessageText#" is "">#htmleditformat(getEmails.defaultMessageText)#<cfelse>#paragraphformat(htmleditformat(getEmails.MessageText))#</cfif>
								<cfif IsOwner>
									<textarea name="frmMessageText" cols="35" rows="10" wrap="virtual" class="form-control"><cfif "#Subject##MessageText#" is "">#getEmails.defaultMessageText#<cfelse>#getEmails.MessageText#</cfif></textarea>
										<!--- <div align="center">
											<input type="submit" value="Update the email"><br>
										</div>
										<p>
										<cfif "#Subject##MessageText#" is not "">
										To return to the default message <a href="javascript:defaultIt(#FlagID#,'#EmailType#')">click here</a>. Caution: All your changes
										to this message will be lost!
										</cfif> --->
								</cfif>
								<span class="help-block">
									<p>Salutation:[[person.Salutation]]<br/>
									First name:[[person.FirstName]]<br/>
									Last name: [[person.LastName]]<br/>
									Full Name: [[person.fullname]]</p>
								</span>
							</div>
							<!--- START: 2011/10/05 PPB LID7683 allow suppression of email --->
							<cfif IsOwner>
								<div class="checkbox">
									<label>
										phr_event_SuppressConfirmationEmail
										<input type="checkbox" name="frmSuppressEmail" <cfif SuppressEmail>checked</cfif>>
									</label>
								</div>
							</cfif>
							<!--- END: 2011/10/05 PPB LID7683 --->
						</div>
					</div>
				</div>
				</form>
			</cfoutput>
		</cfloop>
		</cf_translate>
	</cfcase>

	<!--- <cfcase value="updateEmailsOld">
		<cfif isOwner>
			<cf_eventNotificationEmail action="Compile" changes="Email updated">
			<cf_eventNotificationEmail action="send" FlagID="#FlagID#">
			<cfinclude template="sql_getEmails.cfm">
			<!--- loop through each email message - remember that the sql statement performs a left join between the eventGroupEmail and
			eventDetailEmail to see whether an event specific record has been created --->
			<cfloop query="getEmails">
				<!--- check whether for this type a specific email has been saved --->
				<cfif getEmails.EmailType is #url.EmailType#>
					<!--- found the coinciding email type - check whether subject and message contain text --->
					<cfif "#getEmails.subject##getEmails.messageText#" is not "">
						<!--- a record exists! update it! --->
						<cfinclude template="sql_updateEventDetailEmail.cfm">
					<cfelse>
						<!--- no record exists! add it! --->
						<cfinclude template="sql_insertEventDetailEmail.cfm">
					</cfif>
					<!--- a record was found - only one record gets updated per time - there may be
					more records in the query above but we don't need to see the rest - break --->
					<cfbreak>
				</cfif>
			</cfloop>
		</cfif>
		<!--- now display the form again --->
		<cfif isDefined("url.eventStep")>
			<cfif url.eventStep is 3>
				<cflocation url="eventAddDelegateWiz.cfm?eventStep=3&FlagID=#FlagID#&eventStep=3">
			<cfelseif url.eventStep is 4>
				<cflocation url="eventWebContent.cfm?frmAction=EditAgenda&FlagID=#FlagID#&eventStep=4">
			<cfelseif url.eventStep is 6>
				<cflocation url="eventWebContent.cfm?frmAction=Files&FlagID=#FlagID#&eventStep=6">
			</cfif>

		<cfelse>
			<cflocation url="eventWebContent.cfm?frmAction=emails&FlagID=#FlagID#">
		</cfif>
	</cfcase> --->

	<cfcase value="defaultEmails">
		<cfinclude template="sql_deleteEventDetailEmail.cfm">
		<cflocation url="eventWebContent.cfm?frmAction=Emails&FlagID=#FlagID#"addToken="false">
	</cfcase>

	</cfswitch>
	<form name="eventStep" action="" method="post">
		<CF_INPUT type="hidden" name="FlagID" value="#FlagID#">
		<input type="hidden" name="dirty" value="0">
	</form>
	<cfinclude template="eventFooter.cfm">
</cfif>