<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		ReportRegistrationsAll.cfm	
Author:			  
Date started:	
	
Description:			

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
21-Oct-2008			NJH			Added date filters and defaulted the registration date to be of the current month to restrict the amount of data
								being brought back on the initial hit.
2012-10-04	WAB		Case 430963 Remove Excel Header 

Possible enhancements:


 --->
<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">
<cfparam name="openAsExcel" type="boolean" default="false">

	<cf_head>
		<cf_title>Event Management</cf_title>
	</cf_head>
	
	<cfinclude template="eventSubTopHead.cfm">

<cfparam name="sortOrder" default="company">
<cfparam name="numRowsPerPage" default="100">
<cfparam name="startRow" default="1">
<cfparam name="dateFormat" type="string" default="registered,event_date">

<cfparam name="keyColumnList" type="string" default="company"><!--- this can contain a list of columns that can be edited --->
<cfparam name="keyColumnURLList" type="string" default="../data/dataframe.cfm?frmsrchOrgID="><!--- this can contain a list of matching columns that contain the URL of the editor template --->
<cfparam name="keyColumnKeyList" type="string" default="OrgID"><!--- this can contain a list of matching key columns e.g. primary key --->

<cfparam name="checkBoxFilterName" type="string" default="">
<cfparam name="checkBoxFilterLabel" type="string" default="">

<cfparam name="ReportRegAll" type="string" default="No">
<cfparam name="ReportRegAlleventGroupIDs" type="string" default="">

<!--- NJH 2008-10-21 Trend Support Issue 1189 - start --->

<!--- 
<cfparam name="form.FRMWHERECLAUSEC" default="#left(monthAsString(datepart("m",now())),3)#-#datepart("yyyy",now())#">
<cfparam name="form.FRMWHERECLAUSED" default="#left(monthAsString(datepart("m",now())),3)#-#datepart("yyyy",now())#">
--->

<!--- SKP 2010-04-19 Trend Support Issue 3152 - start --->
<!--- monthAsString is locale specific - i.e. causes sql errors --->
<!--- fix for Issue 1189 by NJH (above) commented out --->

<cfparam name="form.FRMWHERECLAUSEC" default="#dateformat(now(),'mmm-yyyy')#">
<cfparam name="form.FRMWHERECLAUSED" default="#dateformat(now(),'mmm-yyyy')#">

<!--- SKP 2010-04-19 Trend Support Issue 3152 - end --->

<cfset tableName="vEventRegPastWeek">
<cfset dateField="Registered">
<cfset dateRangeFieldName="Registered">

<cfparam name="useFullRange" default="false">

<cfinclude template="/templates/DateQueryWhereClause.cfm"> 
<!--- NJH 2008-10-21 Trend Support Issue 1189 - end --->

<cfquery name="getEventReg" datasource="#application.SiteDataSource#">
	select Name, Company, Reg_Status, Registered, Event_Group, Event, Event_Date, OrgID, 
	Job_Description, Event_Country, Event_Location, PersonID
	from vEventRegPastWeek
	WHERE 1=1
	<cfif isDefined("ReportRegAlleventGroupIDs") and ReportRegAll eq "Yes">
	 	and EventGroupID  in ( <cf_queryparam value="#ReportRegAlleventGroupIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	</cfif>
	<cfinclude template="../templates/tableFromQuery-QueryInclude.cfm">
	order by <cf_queryObjectName value="#sortOrder#"> 
</cfquery>

<cfoutput>
<table width="100%" cellpadding="3">
	<tr>
		<td><strong>All registrations for events.</strong></td>
		<td align="right">&nbsp;</td>
	</tr>
	<tr><td colspan="2">&nbsp;</td></tr>
</table>
</cfoutput>

<cf_translate>

 <cfinclude template="ReportEventFunctions.cfm">

<CF_tableFromQueryObject 
	queryObject="#getEventReg#"
	queryName="getEventReg"
	sortOrder = "#sortOrder#"
	openAsExcel = "#openAsExcel#"
	numRowsPerPage="#numRowsPerPage#"
	startRow="#startRow#"
	
	keyColumnList="#keyColumnList#"
	keyColumnURLList="#keyColumnURLList#"
	keyColumnKeyList="#keyColumnKeyList#"
	
 	hideTheseColumns="OrgID,personid"
	ShowTheseColumns="Name,Job_Description,Company,Reg_Status,Registered,Event_Group,Event,Event_Location,Event_Date,Event_Country"
	queryWhereClauseLabel="Registered"
	dateFormat="#dateFormat#"
	FilterSelectFieldList="Name,Company,Event_Group,Event,Event_Country"
	FilterSelectFieldList2="Name,Company,Event_Group,Event,Event_Country"
	GroupByColumns=""
	radioFilterLabel=""
	radioFilterDefault=""
	radioFilterName=""
	radioFilterValues=""
	checkBoxFilterName=""
	checkBoxFilterLabel=""
	allowColumnSorting="yes"
	
	rowIdentityColumnName="personid"
	functionListQuery="#comTableFunction.qFunctionList#"
>
</cf_translate>


