<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
2012-10-04	WAB		Case 430963 Remove Excel Header 
 --->


<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">
<cfparam name="openAsExcel" type="boolean" default="false">

	<cf_head>
		<cf_title>Event Management</cf_title>
	</cf_head>
	
	
	<cfinclude template="eventSubTopHead.cfm">

<cfparam name="sortOrder" default="Event_Group_ID,Event">
<cfparam name="numRowsPerPage" default="50">
<cfparam name="startRow" default="1">

<cfparam name="checkBoxFilterName" type="string" default="">
<cfparam name="checkBoxFilterLabel" type="string" default="">

<cfquery name="getEventAndGroupDetails" datasource="#application.SiteDataSource#">
	select Event_Group, Event_Group_ID, Event, Event_flag_ID, 
	Event_Flag_Text_ID, Event_Status
	from vEventAndGroupDetails
	WHERE 1=1
	<cfinclude template="../templates/tableFromQuery-QueryInclude.cfm">
	order by <cf_queryObjectName value="#sortOrder#"> 
</cfquery>

<cfoutput>
<table width="100%" cellpadding="3">
	<tr>
		<td><strong>Event Group and Event Details.</strong></td>
		<td align="right">&nbsp;</td>
	</tr>
	<tr><td colspan="2">&nbsp;</td></tr>
</table>
</cfoutput>

<cf_translate>
<CF_tableFromQueryObject 
	queryObject="#getEventAndGroupDetails#"
	queryName="getEventAndGroupDetails"
	sortOrder = "#sortOrder#"
	openAsExcel = "#openAsExcel#"
	numRowsPerPage="#numRowsPerPage#"
	startRow="#startRow#"
	
	hideTheseColumns=""
	ShowTheseColumns="Event_Group,Event_Group_ID,Event,Event_flag_ID,Event_Flag_Text_ID,Event_Status"
	
	FilterSelectFieldList="Event_Group,Event_Status"
	FilterSelectFieldList2="Event_Group,Event_Status"
	GroupByColumns=""
	radioFilterLabel=""
	radioFilterDefault=""
	radioFilterName=""
	radioFilterValues=""
	checkBoxFilterName=""
	checkBoxFilterLabel=""
	allowColumnSorting="yes"
>
</cf_translate>



