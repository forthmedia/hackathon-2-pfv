<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File:			sql_GetEventDetails.cfm
Author:			DJH
Date created:	9 Feb 2000

Description:	SQL query to get a particular event

Version history:
1

20.10.04	ICS	- added sort order for events
23-09-08	NJH	- added moduleID

--->

<!--- get the event details --->
<CFQUERY NAME="GetEventDetails" datasource="#application.siteDataSource#">
	SELECT 
		eg.EventGroupID,
		eg.EventName,
		ed.OwnerID, 
		ed.FlagID, 
		ed.Title,
		ed.Location,
		ed.VenueRoom,
		ed.CountryID,
		ed.EstAttendees,
		ed.PreferredStartDate, 
		ed.PreferredEndDate,
		ed.AllocatedStartDate,
		ed.AllocatedEndDate,
		ed.Email,
		ed.EmailCC,
		ed.UseAgency,
		ed.InvitationType,
		ed.AudienceType,
		ed.PartnerType,
		ed.EventStatus,
		ed.Agenda,
		ed.CheckList,
		ed.ShowEditRegistration,
		ed.RegURL,
		ed.RegistrationScreenID,
		ed.SortOrder,
		ed.MaxTeamSize,
		ed.moduleID,
		ed.share, /*NJH 2012/10/25 social CR*/
		p.FirstName, 
		p.LastName, 
		p.OfficePhone,
		VenueRating,
		Typeofrefreshment,
		ScantronShippingAddress,
		ScantronTel,
		ScantronContact,
		ScantronLanguages,
		<!--- START: 2013-07-31	MS	P-KAS026 Allow the users to set the visibility of Register or Unregister Buttons --->
		ed.hideRegisterForEventBtn,
		ed.hideUnRegisterForEventBtn,		
		<!--- END  : 2013-07-31	MS	P-KAS026 Allow the users to set the visibility of Register or Unregister Buttons --->
		ed.campaignID
	FROM 
		EventDetail AS ed, 
		Person AS p,
		eventGroup as eg
	WHERE 
		eg.eventGroupID = ed.eventGroupID
		AND ed.OwnerID = p.PersonID
		AND ed.FlagID =  <cf_queryparam value="#FlagID#" CFSQLTYPE="CF_SQL_INTEGER" > 	
</CFQUERY>

<!--- set variable so we can test if the current user is the owner of this event --->
<CFIF GetEventDetails.OwnerID EQ request.relayCurrentUser.personID>
	<CFSET IsOwner = 1>
<CFELSE>
	<CFSET IsOwner = 0>
</CFIF>

<!--- also, if the current user is assigned as a manager to this event, they are also to be considered owners --->

<CFQUERY NAME="GetRecordRights" datasource="#application.siteDataSource#">
	SELECT RecordID
	FROM RecordRights
	WHERE Entity = 'Flag'
	AND UserGroupID = #request.relayCurrentUser.userGroupID#
	AND RecordID =  <cf_queryparam value="#flagid#" CFSQLTYPE="CF_SQL_INTEGER" > 	
</CFQUERY>

<cfif GetRecordRights.RecordCount gt 0>
	<cfset IsOwner = 1>
</cfif>
