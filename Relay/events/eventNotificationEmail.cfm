<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File:			eventNotificationEmail.cfm
Author:			DJH
Date created:	4 September 2000

Description:	set up as a custom tag, will compile changes and then
				Sends a notification email to the specified person when a change occurs in the event.

Version history:
1

--->
<cfparam name="attributes.action" default="">
<cfparam name="attributes.changes" default="">
<cfparam name="attributes.FlagID" default="">
<cfparam name="thispersonid" default="#request.relayCurrentUser.personid#">

<cfif 
	(attributes.action is "send" and attributes.FlagID is "")
	or (attributes.action is "compile" and attributes.changes is "")>
	Error: You have not supplied the necessary attributes to the CF_eventNotificationEmail custom tag.
	<p>If action is "compile", you must also supply the changes attribute.</p>
	<p>If action is "send", you must also supply flagid attribute.</p>	
	<cfexit>
</cfif>

<cfparam name="caller.compiledchanges" default="">

<cfif attributes.changes is not "">
	<cfset caller.compiledchanges = caller.compiledchanges & "- " & attributes.changes & "
">
</cfif>

<cfif attributes.action is "compile">
	<!--- don't want to send email message at this point --->
	<cfexit>
</cfif>

<!--- assume action is "send" --->
<!--- find the details --->

<cfquery name="EmailTo" datasource="#application.siteDataSource#">
select
	e.Email,
	e.title,
	e.location,
	p.firstname,
	p.lastname
from
	EventDetail as e
	inner join person as p 
	on #thispersonid# = p.personid
where
	FlagID =  <cf_queryparam value="#attributes.FlagID#" CFSQLTYPE="CF_SQL_INTEGER" > 
</cfquery>

<cfif emailto.email is "">
	<!--- notification email is not filled in - don't send anything --->
	<cfexit>
</cfif>

<!--- test hack - email addresses in the format fname lname <fname@lname.com>
don't work on dev for some reason --->
<cfif cgi.http_host contains "foundation-network.com">
	<cfset mailfrom = "relayhelp@foundation-network.com">
<cfelse>
	<!--- NJH 2010/10/15 caller.support email is not defined, and I can't find where it's set in caller....
	so, I've replaced it with the code below
	<cfset mailfrom = "#caller.supportEmail#"> --->
	<cfset mailfrom = application.com.settings.getSetting("emailAddresses.supportemailaddress")>
</cfif>

<cf_mail to="#EmailTo.email#" from="#mailfrom#" subject="Event Changes - #emailto.title# - made on #dateformat(now(),"dd-mmm-yyyy")#">
<cfoutput>
NOTICE

#emailto.title# (#emailto.location#) has been changed by #emailto.firstname# #emailto.lastname# in the following areas:

#caller.compiledchanges#

Note: If you want to stop receiving these emails when event details change, you will need to blank out the 'Notification Email' address under basic event details.

</cfoutput>
</cf_mail> 
