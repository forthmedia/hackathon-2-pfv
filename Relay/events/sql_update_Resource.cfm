<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File:			events/sql_update_resource.cfm
Author:			DJH
Date created:	1 February 2000

Description:
sql query to update resources

Amendment History:
Date (YYYY-MM-DD)	Initials 	What was changed
2015-09-24			ACPK		FIFTEEN-292 Fixed error caused by deleting record with no Resource Type assigned

--->

<cfquery name="update_resource" datasource="#application.siteDataSource#">
update
	Resource
set
	<!--- 2015-09-24	ACPK	FIFTEEN-292 Fixed error caused by deleting record with no Resource Type assigned --->
	<cfif frmResourceTypeID is not "">
		ResourceTypeID =  <cf_queryparam value="#frmResourceTypeID#" CFSQLTYPE="CF_SQL_INTEGER" > ,
	</cfif>
	<cfif frmQuantityOnHand is not "">
		QuantityOnHand =  <cf_queryparam value="#frmQuantityOnHand#" CFSQLTYPE="CF_SQL_Integer" > ,
	</cfif>
	ResourceName =  <cf_queryparam value="#frmResourceName#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
	Description =  <cf_queryparam value="#frmDescription#" CFSQLTYPE="CF_SQL_VARCHAR" >
	<cfif isDefined("frmDeleteMe") and frmDeleteMe is 1>
		,
		Active = 0
	</cfif>
	<!---OwnerID = #frmOwnerID#--->
where
	ResourceID =  <cf_queryparam value="#frmResourceID#" CFSQLTYPE="CF_SQL_INTEGER" >
</cfquery>
