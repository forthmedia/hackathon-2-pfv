<!--- �Relayware. All Rights Reserved 2014 --->
<!---  Screen hacked from orgallocationlist.cfm gives various count by country

Assumes the following flagtextids with a prefix defined in eventprefix:
		ToBeInvited   (actually can be selection)
		Invited
		Preferred
		Registered
		Submitted
		
		
 --->


<CFPARAM NAME="frmEventPrefix" DEFAULT="U99">

<CFPARAM name="frmStartLetter" Default="">		<!--- used for jumping through organisations --->
<CFSET displaystart = 1>

<CFSET frmEventName="3Com University 99">


<!--- these are used to decide on valid people (hence valid organisations) to display --->
<CFSET possibleinvitesselectiontextid="U99PossibleInvitees">
<CFSET actualinvitesselectiontextid="U99tobeinvited">      <!--- can be more than one of these for individual countries --->
<CFSET actualinvitesflagtextid="U99tobeinvited">



<!---  These are the lists which drive everything - laid out in this strange way to make them easier to understand--->
<!--- 						1						2										3						4							5							6								7							8		 					9 								10		--->
<CFSET columnsource=      "OrgSalesPlaces"&			",tobeinvited"&  					",tobeinvited"&				",invited"&				",preferred"&				",preferrednotregistered"&	",registered"&			",OrgPreSalesPlaces" &			",PeopleInOrganisation"&				",regsubmitted" &			",regviaweb" &			",regviaemail" &				",regviafax">
<CFSET columnsourcetype=  "_flag"&               	",_selection"&						",_flag"&					",_flag"&				",_flag"&					",_special"&					",flag"&				",_flag"&						",_special"&						",flag"&					",flag"&				",_flag"&					",_flag">
<!--- note that alias's are case sensitive (javascript) --->
<CFSET columnalias=       "OrgSalesPlaces"&     	",ToBeInvited1"&					",tobeinvited"&				",Invited"&				",Preferred"&				",PreferredNotRegistered"&	",Registered"&			",OrgPreSalesPlaces"&			",PeopleInOrganisation"&				",RegSubmitted"&			",Regviaweb"&			",Regviaemail"&				",Regviafax">
<CFSET columntitle=       "Sales Places"&       	",On Invite<BR>List"&  				",To Be Invited<BR>Flag"&	",Invite<BR>Sent"&		",Preferred"&				",*Preferred<BR>Attendees"&	", Registration Confirmed"&			",Pre Sales Places"&	",Total Num. People on 3Contact"&	",Registration Submitted"&	",Registered by Web"&	",Registered by email"&		",Registered by fax">
<CFSET columnsourcetable= "integerflagdataorg"& 	",selectiontag"&					",booleanflagdata"&			",booleanflagdata"&		",booleanflagdata"&			",booleanflagdata"&			",booleanflagdata"&		",integerflagdataorg"&			",-"&									",booleanflagdata"&			",booleanflagdata"&		",booleanflagdata"&			",booleanflagdata"> 
<CFSET columnfunction=    "_sum"&                	",_count"&							",_count"&					",_count"&				",_count"&					",_count"&					",count"&				",_sum"&							",_count"&							",count"	&				",count"		&		",_count"		&			",_count"	>  
<CFSET columndisplayas=   "_Value"&             	",_value"&							",_value"&					",_value"&				",_value"&						",_value"&					",value"&				",_value"&						",_value"&							",value"	&				",value"		&		",_value"		&			",_value"	>  
<!--- <CFSET columnorder =      "1,8,2,6,10,7"> --->
<CFSET columnorder =      "10,7,11">

<!--- these are the flag names of flags stored in IntegerFlagDataOrg --->
<CFSET regionsalesplacesflag = "RegionSalesPlaces">
<CFSET regionpresalesplacesflag = "RegionPreSalesPlaces">

<!--- get the user's countries --->
<CFINCLUDE TEMPLATE="../templates/qrygetcountries.cfm">
<!--- qrygetcountries creates a variable CountryList --->

<!--- 	Note definition of a region is if it has a value in regionsalesplacesflag, or regionpredsalesplacesflag -
		Note that this is because some countries (CH and AU) are considered separate regions for this stuff --->

<!--- Find all the 3ComU Regions that the user can use--->
<!--- I think that we are going to have to assume that a user who can access a single country in a region can see them all for this project --->
<CFQUERY NAME="getUserRegions" datasource="#application.siteDataSource#">
<!---  These are the real regions--->
SELECT DISTINCT b.CountryDescription AS Region, b.CountryID
 FROM Country AS a, Country AS b, CountryGroup AS c
WHERE a.CountryID  IN ( <cf_queryparam value="#Variables.CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
  AND c.CountryGroupID = b.CountryID
  AND c.CountryMemberID = a.CountryID
  AND b.countryid in (select distinct entityid from integerflagdata, flag where (flagtextid =  <cf_queryparam value="#frmeventprefix##RegionSalesPlacesflag#" CFSQLTYPE="CF_SQL_VARCHAR" >  or flagtextid =  <cf_queryparam value="#frmeventprefix##RegionPreSalesPlacesflag#" CFSQLTYPE="CF_SQL_VARCHAR" > ) and data >0 )

 UNION
<!--- These are the countries masquerading as regions --->
SELECT DISTINCT b.CountryDescription AS Region, b.CountryID
 FROM Country AS b
WHERE b.CountryID  IN ( <cf_queryparam value="#Variables.CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	and b.countryid in (select distinct entityid from integerflagdata, flag where (flagtextid =  <cf_queryparam value="#frmeventprefix##RegionSalesPlacesflag#" CFSQLTYPE="CF_SQL_VARCHAR" >  or flagtextid =  <cf_queryparam value="#frmeventprefix##RegionPreSalesPlacesflag#" CFSQLTYPE="CF_SQL_VARCHAR" > ) and data >0 )
ORDER BY Region <!--- b.countrydescription --->
</CFQUERY>

		<CFSET currentregions=valuelist(getuserregions.countryid)>	

<!--- these are the regions where the allocation is complete - later we use this to make the columns non updateable --->
<CFQUERY NAME="getcompletedRegions" datasource="#application.siteDataSource#">
	Select distinct entityid
	from booleanflagdata as bfd, flag as f
	where f.flagid=bfd.flagid
	and f.flagtextid='U99RegionAllocationCompl'
</CFQUERY>


<!---
<CFIF NOT IsDefined("frmregionID")>
	<!--- decide upon a default region based on users location , but check they actually have rights to it! --->

	<!--- gets the region the user is currently in  --->
	<CFQUERY NAME="getRegion" datasource="#application.sitedatasource#">
	SELECT 	l.CountryID,
			cg.CountryGroupID,
			r.CountryDescription as RegionName
	FROM 	Country as r, CountryGroup as cg, Person as p, Location as l, country as c
	WHERE 	p.PersonID= #request.relayCurrentUser.personid#
	AND 	p.LocationID=l.locationid
	AND 	l.CountryID=cg.countrymemberid
	AND 	cg.CountryGroupID=r.countryid
	and    	cg.countrygroupID in (#currentregions#) 
	</CFQUERY>


	<CFSET frmregionID=listfirst(valuelist(getregion.countrygroupid))>
	<CFSET regionname=listfirst(valuelist(getregion.RegionName))>



	<CFIF getregion.recordcount is 0>
		<!--- could not find a default region 
			This means that the user is located in a country which they do not have rights to
			so  try setting it to one of their regions from the get user regions query--->
		<CFSET frmregionID=listfirst(valuelist(getuserregions.countryid))>
		<CFSET regionname=listfirst(valuelist(getuserregions.Region))>			
		
	
	</CFIF>



</CFIF>
--->

	<!--- get ids of all countries in all the 'users region' --->
	<CFQUERY NAME="getCountries" datasource="#application.sitedatasource#" DEBUG >
	SELECT  countrymemberid  as countryid
	from 	countrygroup
	where	countrygroupid  in ( <cf_queryparam value="#currentregions#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	
	UNION
	
	SELECT  countryid 
	FROM 	country
	where	countryid  in ( <cf_queryparam value="#currentregions#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	and		(country.isocode='' or country.isocode=null)
	
	</CFQUERY>
	
			<CFSET currentcountries=valuelist(getcountries.countryid)>	


	
<!--- list of selectionids which give us the people (and therefore organisations) to display later on  --->
	<CFQUERY NAME="getValidPeopleSelections" datasource="#application.sitedatasource#" DEBUG >
	select selectionid
	from selection
	where selectiontextid =  <cf_queryparam value="#possibleinvitesselectiontextid#" CFSQLTYPE="CF_SQL_VARCHAR" > 
	or selectiontextid =  <cf_queryparam value="#actualinvitesselectiontextid#" CFSQLTYPE="CF_SQL_VARCHAR" > 	
	</CFQUERY>

	<CFQUERY NAME="getValidPeopleFlags" datasource="#application.sitedatasource#" DEBUG >
	select flagid
	from flag
	where flagtextid =  <cf_queryparam value="#actualinvitesflagtextid#" CFSQLTYPE="CF_SQL_VARCHAR" > 	
	</CFQUERY>



<CFSET freezedate=now()>


<!--- <!--- get list of organisation ids --->

	<CFQUERY NAME="getOrgIds" datasource="#application.sitedatasource#" DEBUG >

 	 	SELECT 	distinct l.OrganisationID, o.organisationname
		from organisation as o, location as l, person as p
		where 	p.locationid=l.locationid
		and 	l.organisationid=o.organisationid
		AND 	l.countryID in (#currentcountries#) 
<!--- Remove this line when  U99Invited Flag Has Been Populated --->
		AND   p.personid in (select entityid from selectiontag where status <> 0 and selectionid in (#valuelist(getValidPeopleSelections.selectionid)#) )

			



<!--- add this line when U99Invited Flag Has Been Populated --->
<!--- 		AND   p.personid in (select entityid from booleanflagdata where flagidid in (#valuelist(getValidPeopleFlags.flagid)#) ) --->

 </CFQUERY> 

<CFSET thisregionOrgIDS=#valuelist(getorgids.organisationid)#>  --->



<!--- get ids of all columns required for the main part of the screen--->
<CFQUERY NAME="getFlagIDs" datasource="#application.sitedatasource#">
Select
	<CFLOOP index="I" from="1"  to="#listlen(columnsource)#">
		<CFIF trim(listgetat(columnsourcetype,I)) IS "Flag"> 
			(select flagid from flag WHERE flagtextid =  <cf_queryparam value="#frmEventPrefix##listgetat(columnsource,I)#" CFSQLTYPE="CF_SQL_VARCHAR" > ) as #listgetat(columnalias,I)#,
		</CFIF>
<!--- 		<CFIF trim(listgetat(columnsourcetype,I)) IS "Selection"> 
			(select selectionid	from selection where selectiontextid='#frmEventPrefix##listgetat(columnsource,I)#') as #listgetat(columnalias,I)#,
		</CFIF>
 --->	</CFLOOP>
1
from dual
</cfquery> 

<!--- 
<!--- this is a complete hack - the selection query above started to return more than one selection (because multiple selections were used for invites) and this caused problems --->
<CFQUERY NAME="getselectionIDs" datasource="#application.sitedatasource#">

	<CFLOOP index="I" from="1"  to="#listlen(columnsource)#">
	<!--- 	<CFIF trim(listgetat(columnsourcetype,I)) IS "Flag"> 
			(select flagid from flag WHERE flagtextid='#frmEventPrefix##listgetat(columnsource,I)#') as #listgetat(columnalias,I)#,
		</CFIF>
	 --->	<CFIF trim(listgetat(columnsourcetype,I)) IS "Selection"> 
			select selectionid as #listgetat(columnalias,I)#
			from selection 
			where selectiontextid='#frmEventPrefix##listgetat(columnsource,I)#' 
		</CFIF>
	</CFLOOP>

</cfquery> 
 --->




<!--- get name and statistics on each region for the main part of the screen--->
 <CFQUERY NAME="getStats" datasource="#application.sitedatasource#">
SELECT 	cc.countrydescription,
		cc.countryid,
		<CFIF DBType IS "Access">
		LEFT(cc.countrydescription,1) as firstletter,
		<CFELSE>		
		SUBSTRING(cc.countrydescription,1,1) as firstletter,
		</CFIF>		

	<CFLOOP index="I" from="1"  to="#listlen(columnsource)#">
		<CFIF trim(listgetat(columnsourcetype,I)) IS "Flag"> 
			<CFIF trim(listgetat(columnfunction,I)) IS "Sum"> 
		 			(select sum(data) 
					from #listgetat(columnsourcetable,I)# as a
					WHERE  a.flagid =  <cf_queryparam value="#evaluate("getflagids."&listgetat(columnalias,I))#" CFSQLTYPE="CF_SQL_INTEGER" > 
					AND a.countryid=cc.countryid) as #listgetat(columnalias,I)#,
			<CFELSEIF trim(listgetat(columnfunction,I)) IS "Count"> 
<!--- 				(select count(1) 
					from person as p, #listgetat(columnsourcetable,I)# as a, location as l, countrygroup as cg
					WHERE a.flagid =  <cf_queryparam value="#evaluate("getflagids."&listgetat(columnalias,I))#" CFSQLTYPE="CF_SQL_INTEGER" > 
					AND a.entityid=p.personid
					AND p.active <>0 
					and p.locationid=l.locationid
					and l.countryid=cg.countrymemberid
					and cg.countrygroupid=cc.countryid) as #listgetat(columnalias,I)#, 
 --->
 				(select count(1) 
					from #listgetat(columnsourcetable,I)# as a, perslocorgreg as p
					WHERE a.flagid =  <cf_queryparam value="#evaluate("getflagids."&listgetat(columnalias,I))#" CFSQLTYPE="CF_SQL_INTEGER" > 
					AND a.entityid=p.personid
					AND p.active <>0 
					and p.countrygroupid=cc.countryid) as #listgetat(columnalias,I)#, 


			</CFIF>
		<CFELSEIF trim(listgetat(columnsourcetype,I)) IS "Selection"> 
			<CFIF trim(listgetat(columnfunction,I)) IS "Count"> 
				(select count(1) 
					from person as p, #listgetat(columnsourcetable,I)# as a, location as l, countrygroup as cg
					WHERE a.selectionid  in ( <cf_queryparam value="#evaluate("valuelist(getselectionids."&listgetat(columnalias,I)&")")#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
					AND a.entityid=p.personid
					AND p.active <>0 
					AND a.status<>0
					and p.locationid=l.locationid
					and l.countryid=cg.countrymemberid
					and cg.countrygroupid=cc.countryid) as #listgetat(columnalias,I)#, 

			</CFIF>
		<CFELSEIF trim(listgetat(columnsourcetype,I)) IS "Special"> 
			<CFIF trim(listgetat(columnalias,I)) IS "PreferredNotRegistered"> 
			(select count(1) 
					from person as p, booleanflagdata as a, location as l, countrygroup as cg
					WHERE a.flagid =  <cf_queryparam value="#getflagids.Preferred#" CFSQLTYPE="CF_SQL_INTEGER" > 
					AND a.entityid=p.personid
					AND p.active <>0 
					and p.locationid=l.locationid
					and l.countryid=cg.countrymemberid
					and cg.countrygroupid=cc.countryid
					and p.personid not in (	select 	entityid 
											from 	booleanflagdata as a 											
											where 	a.flagid =  <cf_queryparam value="#getflagids.Registered#" CFSQLTYPE="CF_SQL_INTEGER" > ))
							 				as #listgetat(columnalias,I)#,

			<CFELSEIF trim(listgetat(columnalias,I)) IS "PeopleInOrganisation"> 
	 		(select count(1) 
					from person as p, location as l
					WHERE p.organisationid=o.organisationid
					AND p.locationid = l.locationid
					AND 	l.countryID  in ( <cf_queryparam value="#currentcountries#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) 
					AND p.active <>0 ) as #listgetat(columnalias,I)#,

			</CFIF>
		</CFIF>


	</CFLOOP>
1
from country as cc
where cc.countryid  in ( <cf_queryparam value="#currentregions#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
ORDER By cc.countrydescription
</CFQUERY> 



<!--- <!--- get the number of places allocated to this region --->
<CFQUERY NAME="getRegionStats1" datasource="#application.sitedatasource#">
Select
	(Select sum(data)
		from Country as c, flag as f, integerflagdata as ifd
		where c.countryid = #frmregionid#
		and ifd.entityid = c.countryid
		and ifd.flagid = f.flagid
		and f.flagtextid = '#frmeventprefix##regionsalesplacesflag#') as salesplaces,
	(Select sum(data)
		from Country as c, flag as f, integerflagdata as ifd
		where c.countryid = #frmregionid#
		and ifd.entityid = c.countryid
		and ifd.flagid = f.flagid
		and f.flagtextid = '#frmeventprefix##regionpresalesplacesflag#') as presalesplaces,
	(select sum(data) 
		from IntegerFlagDataOrg as ifd, flag as f
		WHERE f.flagtextid='#frmEventPrefix#OrgSalesPlaces'
		AND ifd.flagid=f.flagid
		AND ifd.countryid=#frmregionID#) as SalesPlacesAllocated ,
	(select sum(data) 
		from IntegerFlagDataOrg as ifd, flag as f
		WHERE f.flagtextid='#frmEventPrefix#OrgPreSalesPlaces'
		AND ifd.flagid=f.flagid
		AND ifd.countryid=#frmregionID#) as  PreSalesPlacesAllocated,

		
1
FROM DUAL		
</cfquery>

<!--- and another query - same as above except combined htey are too big for SQL --->
<CFQUERY NAME="getRegionStats2" datasource="#application.sitedatasource#">
Select
	(select count(1) as amount
		from location as l, person as p,booleanflagdata as bfd, flag as f
		WHERE f.flagtextid='#frmEventPrefix#Registered'
		and f.flagid=bfd.flagid
		AND bfd.entityid=p.personid
		AND p.locationid=l.locationid
		and l.countryid in (#currentcountries#)) as registered,
	(select count(1)  as amount
	from location as l, person as p, booleanflagdata as bfd, flag as f
	WHERE f.flagtextid='#frmEventPrefix#Preferred'
	and f.flagid=bfd.flagid
	AND bfd.entityid=p.personid
	AND p.locationid=l.locationid
	and l.countryid in (#currentcountries#)
	and p.personid not in (select entityid 
							from 	booleanflagdata as bfd, flag as f
							WHERE 	f.flagtextid='#frmEventPrefix#Registered'
							and 	f.flagid=bfd.flagid) ) as preferrednotregistered,

1
FROM DUAL		
</cfquery>



<!--- If the current region is in the list of regions which have completed their allocation, then we need to disable all the updating --->
<CFIF listcontains(valuelist(getcompletedRegions.entityid),frmregionid) IS  NOT 0>
	<CFSET columndisplayas=replace(columndisplayas,"update","value","ALL")>
</cfif>
 --->



<CFIF getStats.RecordCount GT application.showRecords>

	<!--- showrecords in application .cfm 
			defines the number of records to display --->
	
	<!--- define the start record --->

	<CFIF frmStartLetter is not "">
	<!---  user has selected a letter to go to --->
		<CFSET firstletters=valuelist(getStats.firstletter)>
		<CFSET Max = listlen(firstletters)>
		<CFSET I = 1>
		
		  <CFLOOP Condition="CompareNoCase(ListGetat(firstletters,I),frmstartletter) is -1 and (I LESS THAN MAX)"> 

		  	<CFSET I=I+1>
		  </CFLOOP>
		<CFSET displaystart= I>
 	<CFELSEIF IsDefined("FORM.ListForward.x")>
		 <CFSET displaystart = frmStart + application.showRecords>
 	<CFELSEIF IsDefined("FORM.ListBack.x")>		 
		 <CFSET displaystart = frmStart - application.showRecords>
	<CFELSEIF IsDefined("frmStart")>
	 	 <CFSET displaystart = frmStart>
	</CFIF>

</CFIF>







<cf_head>
	<cf_title><cfoutput>#request.CurrentSite.Title#</cfoutput></cf_title>
<SCRIPT type="text/javascript">

<!--
         function viewregion(ID) {
//		calls sub screen with person details				
                var form = document.FlagGroupForm;
                newWindow=window.open('orgallocationlist.cfm?frmregionID='+ID+'&frmeventPrefix=<CFOUTPUT>#frmeventprefix#&frmeventname=#replace(frmeventname," ","_","ALL")#</cfoutput>', 'OrgAlloc',  'width=700,height=500,toolbar=0,location=0,directories=0,status=1,menuBar=0,scrollBars=1,resizable=1') ; 
				newWindow.focus()
				return 
        } 

		function doForm() {
			var form = document.ThisForm;
			form.submit(); 
		}

	function checktextbox(textbox) {


		if (textbox.value=='') {
			alert('The Organisation name should not be blank');

			}

		
	}		
	
	
<!--- 	function docalcs(allocationbox,flagid,organisationid,column) {

		var form = document.ThisForm;
		
		originalcolumnformvariable='flag_integerflagdataorg_<CFOUTPUT>#frmeventprefix#</CFOUTPUT>'+column+'Original_'
		columnformvariable='flag_integerflagdataorg_<CFOUTPUT>#frmeventprefix#</CFOUTPUT>'+column+'_'

		if (column=='OrgSalesPlaces'){
			 othercolumn='OrgPreSalesPlaces'

		} else {
			othercolumn='OrgSalesPlaces'

		}
			

			peoplepreferrednotregistered=parseInt(eval('form.PreferredNotRegistered_'+organisationid+'.value'))
			peopleregistered=  	parseInt(eval('form.Registered_'+organisationid+'.value'))
			
			thisOrgPreSalesPlaces = eval('form.flag_integerflagdataorg_<CFOUTPUT>#frmeventprefix#</CFOUTPUT>OrgPreSalesPlaces_'+organisationid+'.value')
			thisOrgSalesPlaces =eval('form.flag_integerflagdataorg_<CFOUTPUT>#frmeventprefix#</CFOUTPUT>OrgSalesPlaces_'+organisationid+'.value')			

//		alert('Debug: This Org Sales Places: '+thisOrgSalesPlaces)
//		alert('Debug: This Org PreSales Places: '+thisOrgPreSalesPlaces)
//		alert('Debug: This Org People Registered: '+peopleregistered)

		if (isNaN(parseFloat(allocationbox.value)) == true) {
			alert('Error: You must enter a number');
			allocationbox.value='0'
			}
		else {
			if (thisOrgSalesPlaces +thisOrgPreSalesPlaces <peoplepreferrednotregistered + peopleregistered) {
				alert('Warning: Allocation is less than Preferred People + People Registered');

				}
			else {if (thisOrgSalesPlaces +thisOrgPreSalesPlaces <peopleregistered){
				alert('Error: Allocation is less than number of people registered');
				allocationbox.value=peopleregistered-eval('this'+othercolumn)
			}


			}
			
			}

		change=0

//		This section adds up the all the changes in values to work out the current number of places left
//		These are the names of the form variables for this column, less the organisation

				
		for (j=0; j<form.elements.length; j++){

			thiselement = form.elements[j];

			if (thiselement.name.substring(0,columnformvariable.length) == columnformvariable) { 
				change=change+parseInt(thiselement.value)
			
			}
			if (thiselement.name.substring(0,originalcolumnformvariable.length) == originalcolumnformvariable) { 
				change=change-parseInt(thiselement.value)
			
			}

			
		}


		thisColumnOriginalPlacesLeft=parseInt(eval('form.frmOriginal'+column+'Available.value'))
		placesLeft=thisColumnOriginalPlacesLeft-change

//		alert('Debug: Original places left this column ' +thisColumnOriginalPlacesLeft)
//		alert('Debug: Places left now, this column ' + placesLeft)
				
		
		if (placesLeft >= 0){
//			if there are enough places then update value displayed on screen
			if (column == 'OrgSalesPlaces'){
			form.frmOrgSalesPlacesAvailable.value=placesLeft
			}
			if (column == 'OrgPreSalesPlaces'){
			form.frmOrgPreSalesPlacesAvailable.value=placesLeft
			}

		}

		else
		{alert ('Error: You do not have any more places left to allocate')

			if (column == 'OrgSalesPlaces'){
			allocationbox.value=peopleregistered- thisOrgPreSalesPlaces    
			}
			if (column == 'OrgPreSalesPlaces'){
			allocationbox.value=peopleregistered- thisOrgSalesPlaces    
			}
				//should reset to original value
				//ought to redo running total		
		}

	

	}
 --->

	function saveChecks() {
		// create a string of all the checked organisations
		var form = document.ThisForm;
		var checkedItems = "0";
		for (j=0; j<form.elements.length; j++){
			thiselement = form.elements[j];
			if (thiselement.name == 'frmCheckIDs') { 
				if (thiselement.checked) {
					if (checkedItems == "0") {
						checkedItems = thiselement.value;
					} else {
						checkedItems += "," + thiselement.value;
					}
				}
			}
		}
		return checkedItems;
	}
	
	function countChecks() {
		// count all the checked organisations
		var form = document.ThisForm;
		var count = 0;
		for (j=0; j<form.elements.length; j++){
			thiselement = form.elements[j];
			if (thiselement.name == 'frmCheckIDs') { 
				if (thiselement.checked) {
					count = count+1
					
				}
			}
		}
		return count
	}	
	
	
function go()	{
	
	if (document.selecter.select1.options[document.selecter.select1.selectedIndex].value != "none") 
		{
		location = document.selecter.select1.options[document.selecter.select1.selectedIndex].value
		document.selecter.select1.selectedIndex=0     //resets menu to top item
		}
}

	function openWin( windowURL,  windowName, windowFeatures ) { 

		return window.open( windowURL, windowName, windowFeatures ) ; 
	}


	function noChecks(number){
				alert('You must select atleast ' + number + ' organisations')
				document.selecter.select1.selectedIndex=0
				return
	}
	
	function doChecks (setting) {
//	checks or unchecks all checkboxes on page		
			var form = document.ThisForm;

		for (j=0; j<form.elements.length; j++){

			thiselement = form.elements[j];

			if (thiselement.name == 'frmCheckIDs') { 
				thiselement.checked = setting
			}
		}

	}


//-->

</SCRIPT>

<!--- The main screen starts here ............................ --->


</cf_head>


<!--- Drop Down Box --->
<!--- <CFOUTPUT>
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
<TR>
	<TD ALIGN="LEFT" VALIGN="TOP">
		<B><FONT FACE="Arial, Chicago, Helvetica" SIZE="+1" COLOR="Navy">
		Region Stats Screen</FONT></B>
	</TD>
	<TD ALIGN="RIGHT" VALIGN="TOP">
		<form name="selecter">
			<select name="select1" size=1 onchange="go()">
			<option value=none>Dedupe functions
			<option value=none>--------------------
			<option value="JavaScript: if (countChecks()>1) {  newWindow = openWin( '..#thisdir#/orgdedupe.cfm?frmorgids='+saveChecks() ,'PopUp', 'width=450,height=450,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1' ); newWindow.focus()} else {noChecks('two')}  ">Dedupe Checked Organisations
		<!--- 	<option value="JavaScript: doChecks(true)">Check all Organisations on page --->
			<!--- <option value="JavaScript: doChecks(false)">Uncheck all Organisations on page --->
			</select>
		</form>
	</TD>
</TR>
<TR>
	<TD ALIGN="LEFT" VALIGN="TOP">
		<A HREF="JavaScript:newWindow = openWin( '..#thisdir#/orgAllocationHelp.cfm' ,'PopUp', 'width=450,height=450,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1' ); newWindow.focus()">Click here</A> for help
	</TD>

</TR>
</TABLE>
</CFOUTPUT> --->

<CENTER>
<H1><CFOUTPUT><FONT FACE="Arial, Chicago, Helvetica" SIZE="4">Analysis of registrations for #htmleditformat(frmEventName)#</CFOUTPUT></H1>
<CFIF IsDefined('message')>
	
	<P><FONT FACE="Arial, Chicago, Helvetica" SIZE="2"><CFOUTPUT>#application.com.security.sanitiseHTML(message)#</CFOUTPUT></FONT>

</CFIF>

<FORM ACTION="<CFOUTPUT>#htmleditformat(thisdir)#/orgAllocationTask.cfm</CFOUTPUT>" NAME="ThisForm" METHOD="POST">

<CFOUTPUT>
	<CF_INPUT TYPE="HIDDEN" NAME="frmStart" VALUE="#Variables.displaystart#">
<!--- 	<INPUT TYPE="HIDDEN" NAME="frmCurrentRegionid" VALUE="#frmregionid#"> --->
	<CF_INPUT TYPE="HIDDEN" NAME="frmReturnTo" VALUE="..\#thisdir#\regallocationlist.cfm">		
	<CF_INPUT TYPE="HIDDEN" NAME="frmEventPrefix" VALUE="#frmeventprefix#">		
	<CF_INPUT TYPE="HIDDEN" NAME="frmEventName" VALUE="#frmeventname#">		
</CFOUTPUT>

<!--- Header --->
<P><TABLE BORDER=0 BGCOLOR="#FFFFCC" CELLPADDING="5" CELLSPACING="0" WIDTH="500">
	
<!--- 	<TR><TD COLSPAN="2" ALIGN="CENTER" BGCOLOR="#000099"><FONT FACE="Arial, Chicago, Helvetica" SIZE="2" COLOR="#FFFFFF"><B><CFOUTPUT>#frmEventName#</cfoutput></B></FONT></TD></TR>
	<TR><TD ALIGN="CENTER" COLSPAN=2><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>		
			Delegate Allocation for 
				<SELECT NAME="frmRegionID" onchange="form.submit()">
				<CFOUTPUT QUERY="getUserRegions">
					<OPTION VALUE="#CountryID#" <CFIF countryid IS 	frmregionid>Selected</cfif>> #HTMLEditFormat(Region)#
				</CFOUTPUT>
			</SELECT>

		<BR></FONT></TD>
<CFOUTPUT><TD></td>
	<TR>
		<TD ALIGN="CENTER" COLSPAN=1><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
			Total Sales Places allocated to Region
		<BR></FONT></TD>
		<TD ALIGN="CENTER" COLSPAN=1><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
			#getRegionStats1.salesplaces# <BR>
		<BR></FONT></TD>

	</TR>
	<TR>
		<TD ALIGN="CENTER" COLSPAN=1><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
			Total Pre Sales Places allocated to Region
		<BR></FONT></TD>
		<TD ALIGN="CENTER" COLSPAN=1><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
			#getRegionStats1.presalesplaces#
		<BR></FONT></TD>
	</TR>

	<TR><TD ALIGN="CENTER" COLSPAN=1><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
			Total Sales Places allocated to individual organisations
		<BR></FONT></TD>
		<TD ALIGN="CENTER" COLSPAN=1><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
			#VAL(getRegionStats1.salesplacesallocated)#
			
		<BR></FONT></TD>
	</TR>
	<TR><TD ALIGN="CENTER" COLSPAN=1><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
			Total Pre Sales Places allocated to individual organisations
		<BR></FONT></TD>
		<TD ALIGN="CENTER" COLSPAN=1><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
			#val(getRegionStats1.presalesplacesallocated)#
		<BR></FONT></TD>
	</TR>

	<TR><TD ALIGN="CENTER" COLSPAN=1><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
			Currently registered
		<BR></FONT></TD>
		<TD ALIGN="CENTER" COLSPAN=1><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
			#getRegionStats2.Registered#
		<BR></FONT></TD>
	</TR>
	<TR><TD ALIGN="CENTER" COLSPAN=1><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
			Preferred Delegates
		<BR></FONT></TD>
		<TD ALIGN="CENTER" COLSPAN=1><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
			#getRegionStats2.PreferredNotRegistered#
		<BR></FONT></TD>
	</TR>
	<CFSET Salesplaceslefttoallocate=VAL(getRegionStats1.SalesPlaces) - VAL(getRegionStats1.SalesPlacesAllocated)>
	<CFSET PreSalesplaceslefttoallocate=VAL(getRegionStats1.PreSalesPlaces) - VAL(getRegionStats1.PreSalesPlacesAllocated)>
	<TR><TD ALIGN="CENTER" COLSPAN=1><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
			Sales Places left to allocate to organisations
		<BR></FONT></TD>
		<TD ALIGN="CENTER" COLSPAN=1><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
			<FONT FACE="Arial,Chicago" SIZE="2"><INPUT TYPE="Text" NAME="frmOrgSalesPlacesAvailable"  VALUE="#Salesplaceslefttoallocate#" DISABLED SIZE="2"	></FONT>
			
		<BR></FONT></TD>
	</TR>

		<TR><TD ALIGN="CENTER" COLSPAN=1><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
			Pre Sales Places left to allocate to organisations
		<BR></FONT></TD>
		<TD ALIGN="CENTER" COLSPAN=1><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
			
			<FONT FACE="Arial,Chicago" SIZE="2"><INPUT TYPE="Text" NAME="frmOrgPreSalesPlacesAvailable"  VALUE="#PreSalesplaceslefttoallocate#" DISABLED SIZE="2"	></FONT>

			
		<BR></FONT></TD>
	</TR>

		<INPUT TYPE="HIDDEN" NAME="frmTotalOrgSalesPlaces" 				VALUE="#val(getRegionStats1.salesplaces)#">			
		<INPUT TYPE="HIDDEN" NAME="frmTotalOrgPreSalesPlaces" 				VALUE="#val(getRegionStats1.presalesplaces)#">			
		<INPUT TYPE="HIDDEN" NAME="frmOriginalOrgSalesPlacesAllocated" 	VALUE="#val(getRegionStats1.salesplacesallocated)#">			
		<INPUT TYPE="HIDDEN" NAME="frmOriginalOrgPreSalesPlacesAllocated" 	VALUE="#val(getRegionStats1.presalesplacesallocated)#">			
		<INPUT TYPE="HIDDEN" NAME="frmOriginalOrgSalesPlacesAvailable" 	VALUE="#Salesplaceslefttoallocate#">			
		<INPUT TYPE="HIDDEN" NAME="frmOriginalOrgPreSalesPlacesAvailable" 	VALUE="#preSalesplaceslefttoallocate#">				
</TABLE>
</CFOUTPUT>		 --->
<!--- End of Header --->
	

<CFIF getStats.Recordcount IS 0>
	
	<P><FONT FACE="Arial, Chicago, Helvetica" SIZE="2">There are no Organisations matching you country.</FONT>

<CFELSE>

<CENTER>
<!--- Next and Previous Buttons --->
<TABLE BORDER=0>
	
		<TR><TD><FONT FACE="Arial, Helvetica" SIZE=2>
				<CFIF Variables.displaystart GT 1>
					<INPUT TYPE="Image" NAME="ListBack" SRC="<CFOUTPUT></CFOUTPUT>/images/buttons/c_prev_e.gif" WIDTH=64 HEIGHT=21 BORDER=0 ALT="Save and Previous">
				</CFIF>
				</FONT></TD>
<!--- 		<TD>		<INPUT TYPE="Image" NAME="ListSave" SRC="<CFOUTPUT></CFOUTPUT>/images/buttons/c_save_e.gif" WIDTH=64 HEIGHT=21 BORDER=0 ALT="Save this screen"></td> --->
			<TD><FONT FACE="Arial, Helvetica" SIZE=2>
				<CFIF (Variables.displaystart + application.showRecords - 1) LT getStats.RecordCount>
					<INPUT TYPE="Image" NAME="ListForward" SRC="<CFOUTPUT></CFOUTPUT>/images/buttons/c_next_e.gif" WIDTH=64 HEIGHT=21 BORDER=0 ALT="Save and Next">
				</cFIF>
				</FONT></TD>
		</TR>
<!--- 		<TR>
			<TD align="center" colspan="3"><FONT FACE="Arial, Helvetica" SIZE=2>
	 				<CFIF (Variables.displaystart + application.showRecords - 1) LT getStats.RecordCount>
						<CFOUTPUT>Record(s) #Variables.displaystart# - #Evaluate("Variables.displaystart + application.showRecords - 1")#</CFOUTPUT>
					<CFELSE>
	 					<CFOUTPUT>Record(s) #Variables.displaystart# - #getStats.RecordCount#</cFOUTPUT>
					</CFIF>
				</FONT></TD>
		</tr>
 --->		</TABLE>
	
</CENTER>

<CENTER>
<!--- Output Column Titles --->
	<P><TABLE BORDER="0" CELLPADDING="5" CELLSPACING="0" WIDTH="90%">
	<TR>
	<!--- <TD ALIGN="CENTER" VALIGN="BOTTOM"><FONT FACE="Arial,Chicago" SIZE="2"><B>Check</B></FONT></TD> --->
		<TD ALIGN="CENTER" VALIGN="BOTTOM"><FONT FACE="Arial,Chicago" SIZE="2"><B>Country</B></FONT></TD>


	<CFLOOP index="J" from="1"  to="#listlen(columnorder)#">
		<CFSET I=listgetat(columnorder,J)>
	<CFOUTPUT>
		<CFIF listgetat(columnTitle,I) IS NOT "0"> 
		<TD ALIGN="CENTER" VALIGN="BOTTOM"><FONT FACE="Arial,Chicago" SIZE="2"><B>#listgetat(columnTitle,I)#</B></FONT></TD>
		</CFIF>

	</CFOUTPUT>
	</CFLOOP>

	</TR>
<!--- End of Column Titles --->	

<!--- Loop through data --->	
	<CFSET orgsonpage="">
	<CFSET endrow=Min(Variables.displaystart + application.showRecords, getStats.recordcount)>
	<CFLOOP QUERY="getStats" STARTROW="#Variables.displaystart#" EndRow="#endrow#">

	
		<CFOUTPUT>	
		<TR>
		<!--- <TD ALIGN="CENTER" ><FONT FACE="Arial,Chicago" SIZE="2"><INPUT TYPE="checkbox" NAME="frmCheckIDs" VALUE="#CountryID#"></FONT></TD> --->
			<TD ALIGN="CENTER" ><FONT FACE="Arial,Chicago" SIZE="2">#htmleditformat(CountryDescription)#</FONT></TD>	

		</CFOUTPUT>

	<!--- loop through dynamic columns --->
	<CFLOOP index="J" from="1"  to="#listlen(columnorder)#">
		<CFSET I=listgetat(columnorder,J)>

		<CFOUTPUT>	

		<CFIF trim(listgetat(columndisplayas,I)) IS "Update"> 
				<TD ALIGN="CENTER" ><FONT FACE="Arial,Chicago" SIZE="2"><CF_INPUT TYPE="Text" NAME="#listgetat(columnsourcetype,I)#_#listgetat(columnsourcetable,I)#_#frmeventprefix##listgetat(columnsource,I)#_#organisationid#" onchange="docalcs(this,'#frmeventprefix##listgetat(columnsource,I)#',#organisationid#,'#listgetat(columnalias,I)#');" VALUE="#val(evaluate(listgetat(columnalias,I)))#" SIZE="3"></FONT></TD>	
				<!---  eg:																			flag         _   integerflagdataorg			_ U99orgallocated			_213												--->
					<CF_INPUT TYPE="HIDDEN" NAME="#listgetat(columnsourcetype,I)#_#listgetat(columnsourcetable,I)#_#frmeventprefix##listgetat(columnsource,I)#Original_#organisationid#" VALUE="#val(evaluate(listgetat(columnalias,I)))#">			

		<CFELSEIF trim(listgetat(columndisplayas,I)) IS "Value"> 
			<TD ALIGN="CENTER" ><FONT FACE="Arial,Chicago" SIZE="2">#val(evaluate(listgetat(columnalias,I)))#</FONT></TD>

		<CFELSEIF trim(listgetat(columndisplayas,I)) IS "ValuewithVariable"> 
			<TD ALIGN="CENTER" ><FONT FACE="Arial,Chicago" SIZE="2">#evaluate(listgetat(columnalias,I))#</FONT></TD>
				<CF_INPUT TYPE="HIDDEN" NAME="#listgetat(columnalias,I)#_#organisationid#" VALUE="#val(evaluate(listgetat(columnalias,I)))#">			

		</CFIF>

		</CFOUTPUT>

	</CFLOOP>
	<!--- end of dynamc columns --->


			<CFOUTPUT><TD ALIGN="CENTER" ><FONT FACE="Arial,Chicago" SIZE="2"><A HREF="JavaScript:viewregion('#Countryid#');">Details</a></FONT></TD></CFOUTPUT>
																																											 
		</TR>

<!--- 	<CFSET orgsonpage=listappend(orgsonpage,organisationid)> --->
			
	</CFLOOP>
	</TABLE>


	<!--- get list of updating flags on page	 --->
	<CFSET FlagsOnPage="">
	<CFLOOP index="I" from="1"  to="#listlen(columnsource)#">
		<CFIF listgetat(columndisplayas,I) IS "Update"> 
			<CFSET flagsonPage=listappend(flagsonpage,listgetat(columnsourcetype,I)&"_"&listgetat(columnsourcetable,I)&"_"&frmeventprefix&listgetat(columnsource,I))>
		</CFIF>
	</CFLOOP>

	<cfoutput>
<!--- 	<INPUT TYPE="HIDDEN" NAME="frmOrgsOnpage" VALUE="#orgsonpage#">
	<INPUT TYPE="HIDDEN" NAME="frmOrgFlagsOnpage" VALUE="#flagsonpage#">	 --->
	</cfoutput>
	
	<TABLE BORDER=0>
<!--- Previous and next buttons --->	
		<TR><TD><FONT FACE="Arial, Helvetica" SIZE=2>
				<CFIF Variables.displaystart GT 1>
					<INPUT TYPE="Image" NAME="ListBack" SRC="<CFOUTPUT></CFOUTPUT>/images/buttons/c_prev_e.gif" WIDTH=64 HEIGHT=21 BORDER=0 ALT="Save and Previous">
				</CFIF>
				</FONT></TD>
<!--- 		<TD>		<INPUT TYPE="Image" NAME="ListSave" SRC="<CFOUTPUT></CFOUTPUT>/images/buttons/c_save_e.gif" WIDTH=64 HEIGHT=21 BORDER=0 ALT="Save this screen"></td> --->
			<TD><FONT FACE="Arial, Helvetica" SIZE=2>
				<CFIF (Variables.displaystart + application.showRecords - 1) LT getStats.RecordCount>
					<INPUT TYPE="Image" NAME="ListForward" SRC="<CFOUTPUT></CFOUTPUT>/images/buttons/c_next_e.gif" WIDTH=64 HEIGHT=21 BORDER=0 ALT="Save and Next">
				</cFIF>
				</FONT></TD>
		</TR>
<!--- 		<TR>
			<TD align="center" colspan="3"><FONT FACE="Arial, Helvetica" SIZE=2>
	 				<CFIF (Variables.displaystart + application.showRecords - 1) LT getStats.RecordCount>
						<CFOUTPUT>Record(s) #Variables.displaystart# - #Evaluate("Variables.displaystart + application.showRecords - 1")#</CFOUTPUT>
					<CFELSE>
	 					<CFOUTPUT>Record(s) #Variables.displaystart# - #getStats.RecordCount#</cFOUTPUT>
					</CFIF>
					<CFOUTPUT>of #getStats.Recordcount#</CFOUTPUT>
				</FONT></TD>
		</tr>
 --->
<!--- 	<!--- Goto first letter  --->
	<cfif listlen(orgsonpage) LE getStats.Recordcount>
		<TR>
			<TD align="center" colspan="3"><FONT FACE="Arial, Helvetica" SIZE=2>

				<SELECT NAME="frmStartLetter" onchange="form.submit()">

					<OPTION VALUE="" > Go to First Letter
					<CFLOOP Index="I" FROM="1" TO="26">
						<CFOUTPUT><OPTION VALUE="#chr(I+64)#" > #chr(I+64)#</CFOUTPUT>
					</CFLOOP>

				</SELECT>
				</FONT></TD>
		</tr>
	</CFIF>

 --->
		</TABLE>
	
	

</CENTER>	
	</FORM>


</CFIF>

<FONT FACE="Arial, Helvetica" SIZE=2>Note:  Preferred figure above does not include those Preferred Delegates who have now registered</FONT>
</CENTER>
</FORM>


