<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		eventAddPopup.cfm
Author:
Date started:	10-10-2008

Description:

Amendment History:

Date 				Initials 	What was changed
10-Oct-2008			NJH 		Bug Fix Sophos Issue 889 - don't show events for registration that are cancelled.
2012-06-19 			PPB 		Case 428840 changed CFSQLTYPE="CF_SQL_VARCHAR" for event RegStatus
2012-07-03			IH			Case 428840 add lastUpdatedBy to addFlagEvent table
2013-10-22			PPB			Case 436900 when status is RegistrationSubmitted set the RegDate
2015-11-12          ACPK        Added Bootstrap

Possible enhancements:


 --->

<CFIF IsDefined("form.frmFlagID")>
	<CFIF IsDefined("form.frmFlagID")>
		<CFSET FreezeDate = request.requestTimeODBC>

		<!--- need to dynamically get the name of the event flag data table as this might change --->
		<CFQUERY NAME="GetEventTable" datasource="#application.siteDataSource#">
			SELECT ft.DataTable, f.FlagTextID
			FROM Flag AS f, FlagGroup AS fg, FlagType AS ft
			WHERE f.FlagGroupID = fg.FlagGroupID
			AND fg.FlagTypeID = ft.FlagTypeID
			AND f.FlagID =  <cf_queryparam value="#frmFlagID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</CFQUERY>
		<CFSET EventTable = "#GetEventTable.DataTable#FlagData">
		<CFQUERY NAME="GetEventDetail" datasource="#application.siteDataSource#">
			select * from eventDetail where flagid =  <cf_queryparam value="#frmFlagID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</CFQUERY>

		<cfif isdefined("FRMREGISTER")>
			<cfset RegStatus = "RegistrationSubmitted">
		<cfelse>
			<cfset RegStatus = "To Be Invited">
		</cfif>

		<CFLOOP LIST="#form.frmCheckIDs#" INDEX="theEntity">
			<!--- 2012-06-19 PPB Case 428840 changed CFSQLTYPE="CF_SQL_VARCHAR" for RegStatus --->
			<!--- 2012-07-03 IH	Case 428840 add lastUpdatedBy to addFlagEvent table  --->
			<CFQUERY NAME="addFlagEvent" datasource="#application.siteDataSource#">
				INSERT INTO #EventTable#
				<!--- START 2013-10-22 PPB Case 436900 when status is RegistrationSubmitted set the RegDate --->
				(EntityID, FlagID, FirstName, LastName, SiteName, CountryID, Country, Email, LastUpdated, lastUpdatedBy, RegStatus
				<cfif RegStatus eq "RegistrationSubmitted">
					, RegDate
				</cfif>
				, Wave)
					SELECT <cf_queryparam value="#theEntity#" CFSQLTYPE="CF_SQL_INTEGER" >, <cf_queryparam value="#frmFlagID#" CFSQLTYPE="CF_SQL_INTEGER" >, p.FirstName, p.LastName, l.SiteName, c.CountryID, c.CountryDescription, p.email, <cf_queryparam value="#CreateODBCDate(FreezeDate)#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,<cf_queryparam value="#request.relayCurrentUser.personID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#RegStatus#" CFSQLTYPE="CF_SQL_VARCHAR" >
					<cfif RegStatus eq "RegistrationSubmitted">
						, <cf_queryparam value="#CreateODBCDate(FreezeDate)#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
					</cfif>
					,'A'
				<!--- END 2013-10-22 PPB Case 436900 --->
					FROM Person AS p, Location AS l, Country AS c
					WHERE p.PersonID =  <cf_queryparam value="#theEntity#" CFSQLTYPE="CF_SQL_INTEGER" >
					AND p.LocationID = l.LocationID
					AND l.CountryID = c.CountryID
					AND NOT EXISTS (SELECT * FROM #EventTable# WHERE EntityID =  <cf_queryparam value="#theEntity#" CFSQLTYPE="CF_SQL_INTEGER" >  AND FlagID =  <cf_queryparam value="#frmFlagID#" CFSQLTYPE="CF_SQL_INTEGER" > )
			</CFQUERY>

			<cfif RegStatus eq "RegistrationSubmitted" and application.com.settings.getSetting("socialMedia.enableSocialMedia") and application.com.settings.getSetting("socialMedia.share.eventDetail.registered") and GetEventDetail.share>
				<cfquery name="getPersonDetails" datasource="#application.siteDataSource#">
					select firstname, lastname from person where personID =  <cf_queryparam value="#theEntity#" CFSQLTYPE="CF_SQL_INTEGER" >
				</cfquery>

				<cfset mergeStruct = structNew()>
				<cfset mergeStruct.event = GetEventDetail>
				<cfset mergeStruct.person = {firstname=getPersonDetails.firstname,lastname=getPersonDetails.lastname}>
				<cfset application.com.service.addRelayActivity(action="registered",mergeStruct=mergeStruct,performedOnEntityID=frmFlagID,performedOnEntityTypeID=application.entityTypeID["eventDetail"],performedByEntityID=theEntity)>
			</cfif>
		</CFLOOP>
	<CFELSE>
		An error has occured and this process will now terminate.
		<P>
		<A HREF="javascript: window.close()">Close Window</A>
		<CF_ABORT>
	</CFIF>


<!--- WAB: 2000-12-07 this line did say:
not isDefined("frmRegister") and getEventDetail.regURL is not ""
but I don't think that this is right (although I suspect that I did it!)
 --->
	<CFIF not isDefined("frmRegister") or getEventDetail.regURL is "">

		<!--- stop here, don't go to registration --->



		<cf_head>
		<cf_title>Events - Add Delegates</cf_title>
		</cf_head>

		<DIV>
			<p>Events updated successfully.</P>
			<A HREF="javascript: window.close()">Close Window</A>
		</DIV>



	<CFELSE>

		<!--- go straight to registration --->
		<!--- get registration URL for first person in the list who has not registered --->
		<CFQUERY NAME="GetRegID" datasource="#application.siteDataSource#">
		Select
			*
		from
			#EventTable#
		where
			entityid  in ( <cf_queryparam value="#frmCheckIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			and flagid =  <cf_queryparam value="#frmFlagID#" CFSQLTYPE="CF_SQL_INTEGER" >
			and regComplete = 0
		</CFQUERY>

		<CFSET partnerRegID  = getRegID.partnerRegID>

		<CFSET URL = evaluate("'#getEventDetail.RegURL#'")>

		<SCRIPT>
			<CFOUTPUT>window.location.href = '#jsStringFormat(url)#'</cfoutput>

		</script>

		<CFOUTPUT>#url#</cfoutput>

<!--- 		<CFINcLUDE template = "#url#">
 --->







	</cfif>

<CFELSE>
	<CFSET PassedPeople = 0>
	<CFSET EventManager = 0>

	<CFIF IsDefined("frmPersonIDs")>
		<CFIF frmPersonIDs NEQ "">
			<CFSET PassedPeople = 1>
		</CFIF>
	</CFIF>

	<CFQUERY NAME="GetRecordRights" datasource="#application.siteDataSource#">
		SELECT rr.RecordID
		FROM RecordRights AS rr, EventDetail AS ed
		WHERE rr.Entity = 'Flag'
		AND rr.RecordID = ed.FlagID
		AND rr.UserGroupID =  <cf_queryparam value="#ListGetAt(Cookie.user, 2, "-")#" CFSQLTYPE="CF_SQL_INTEGER" >
	</CFQUERY>
	<CFSET UsersEvents = ValueList(GetRecordRights.RecordID)>
	<CFIF UsersEvents EQ "">
		<CFSET UsersEvents = 0>
	</CFIF>
	<CFIF GetRecordRights.RecordCount GT 0>
		<CFSET EventManager = 1>
	</CFIF>

	<CFIF NOT PassedPeople OR NOT EventManager>
		<SCRIPT type="text/javascript">
		<!--
		function closeWindow() {
			window.close();
		}
		alert("You have not checked any people or you do not have permission to add people to events.");
		closeWindow();
		//-->
		</SCRIPT>
		<CF_ABORT>
	</CFIF>

	<CFSET FreezeDate = request.requestTimeODBC>

	<!--- get the countries the current user has rights to view
	returns a list of CountryIDs as a variable called Variables.CountryList --->
	<!--- 2012-07-25 PPB P-SMA001 commented out
	<CFINCLUDE TEMPLATE="../templates/qrygetcountries.cfm">
	 --->
	<CFQUERY NAME="GetEvents" datasource="#application.siteDataSource#">
		SELECT DISTINCT ed.FlagID, ed.Title, p.FirstName, p.LastName, p.OfficePhone, ed.Date
		FROM EventDetail as ed, Person AS p, EventCountry AS ec
		WHERE ec.FlagID = ed.FlagID
		AND p.PersonID = ed.OwnerID
		<!--- 2012-07-25 PPB P-SMA001 commented out
		AND ec.CountryID  IN ( <cf_queryparam value="#Variables.CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		 --->
		#application.com.rights.getRightsFilterWhereClause(entityType="EventCountry",alias="ec").whereClause# 	<!--- 2012-07-25 PPB P-SMA001 --->
		AND ed.FlagID  IN ( <cf_queryparam value="#UsersEvents#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		/*
		 NJH 2008/10/10 Bug Fix Sophos Issue 889 - don't get events that are cancelled... not sure if this is the best method
		 as it looks like there are registrationOpen/closed fields which might be better to use.. but they don't like they're getting populated
		*/
		and ed.eventStatus <> 'Cancelled'
		ORDER BY ed.Date DESC
	</CFQUERY>


	<cf_head>
	<cf_title>Events - Add Delegates</cf_title>
	<SCRIPT type="text/javascript">
	<!--
		function checkForm() {
			var form = document.AddDelegates;
			var msg = "";
			if (form.frmFlagID.selectedIndex == 0) {
				msg += "\nYou must choose an event.";
			}
			if (msg != "") {
				alert(msg);
			} else {
				form.submit();
			}
		}
	//-->
	</SCRIPT>
	</cf_head>


	<FORM CLASS="form-horizontal" NAME="AddDelegates" ACTION="EventAddPopup.cfm" METHOD="post">
		<CFOUTPUT>
			<CF_INPUT TYPE="hidden" NAME="frmCheckIDs" VALUE="#frmPersonIDs#">
			<!--- hack by WAB to allow adding to event and registration all in one --->
			<CFIF isDefined("frmRegister")><INPUT TYPE="hidden" NAME="frmregister" VALUE="1"></cfif>
		</CFOUTPUT>
		<div class="form-group">
			<div class="col-xs-12">
				<CFIF GetEvents.RecordCount GT 0>
					<label for="frmFlagID">Events Available</label>
					<SELECT ID="frmFlagID" CLASS="form-control" NAME="frmFlagID">
						<OPTION VALUE="-1">Please choose an event</OPTION>
						<CFOUTPUT QUERY="GetEvents">
						    <OPTION VALUE="#FlagID#">#DateFormat(Date, "dd-mmm-yyyy")# - #htmleditformat(Title)#</OPTION>
						</CFOUTPUT>
					</SELECT>
				<CFELSE>
					<p>No Events Available</p>
				</CFIF>
			</div>
		</div>
		<div class="form-group">
            <div class="col-xs-12">
				<CFIF GetEvents.RecordCount GT 0>
					<cf_input type="button" class="btn-primary btn" onclick="JavaScript:checkForm();" value="Save">
					<!---<A HREF="JavaScript:checkForm();"><CFOUTPUT><IMG SRC="/images/buttons/c_save_e.gif" BORDER="0"></CFOUTPUT></A>--->
				<CFELSE>
					<A HREF="javascript: window.close()">Close Window</A>
				</CFIF>
		</div>
        </div>
	</FORM>


</CFIF>
