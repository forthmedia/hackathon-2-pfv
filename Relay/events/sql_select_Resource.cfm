<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File:			events/sql_select_resource.cfm
Author:			DJH
Date created:	1 February 2000

Description:
sql query to list resources

Version history:
1

--->

<cfparam name="whereclause" default="no">

<cfquery name="select_resource" datasource="#application.siteDataSource#">
select
	r.ResourceID,
	r.ResourceTypeID,
	rt.ResourceType,
	r.QuantityOnHand,
	r.ResourceName,
	r.Description,
	r.OwnerID
from
	Resource as r,
	ResourceType as rt
where	
	r.ResourceTypeID = rt.ResourceTypeID
	and r.Active = 1
<cfif whereclause>
	<cfif isDefined("ResourceID")>
		and ResourceID =  <cf_queryparam value="#ResourceID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</cfif>
	<cfif isDefined("ResourceTypeID")>
		and ResoureTypeID =  <cf_queryparam value="#ResourceTypeID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</cfif>
</cfif>
order by
	ResourceName,
	ResourceID
</cfquery>
