<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File:			sql_getPartnerTypes.cfm
Author:			DJH
Date created: 	14 February 2000

Description:	Gets a list of partner types

Version history:
1

--->

<cfquery name="getPartnerTypes" datasource="#application.siteDataSource#">
select 
	f.flagID,
	f.Name
from
	Flag as F,
	FlagGroup as FG
where
	F.FlagGroupID = fg.FlagGroupID
	and fg.FlagGroupTextID in ('orgpartnertype','PartnerType')
	and fg.active = 1
</cfquery>