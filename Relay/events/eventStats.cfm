<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
Mods:
WAB 240100 Modified so that defaults to regstatus report
WAB 240100 Modified getEventAnalysis 'cause it wasn't working properly 
		(although I think I had probably broken it)
 --->
<cf_title>Event Statistics</cf_title>

<cfinclude template="eventTopHead.cfm">

<CFDIRECTORY ACTION="LIST" DIRECTORY="#Path#\TempGraphics" FILTER="#request.relayCurrentUser.usergroupid#_*.gif" NAME="MyFiles">

<CFLOOP QUERY="MyFiles">
	<CFIF fileExists("#Path#\TempGraphics\#Name#")>
		<CFFILE ACTION="DELETE" FILE="#Path#\TempGraphics\#Name#">
	</cfif>
</CFLOOP>


<CFPARAM name="frmAnalysis" default ="RegStatus">
<CFPARAM name="frmChooseCountryID" default ="0,All Countries">

<CFSET ManagerOK = 0>
<CFSET SuperOK = 0>

<CFIF checkPermission.updateOkay GT 0><CFSET ManagerOK = 1></CFIF>
<CFIF checkPermission.addOkay GT 0><CFSET SuperOK = 1></CFIF>

<!--- 
<CFOUTPUT>
ManagerOK #ManagerOK#<BR>
SuperOK #SuperOK#
</CFOUTPUT>
 --->

<CFIF NOT IsDefined("FlagID")>
	<cflocation url="eventHome.cfm"addToken="false">
	<CF_ABORT>
<CFELSEIF NOT IsNumeric(FlagID)>
	<cflocation url="eventHome.cfm"addToken="false">
	<CF_ABORT>
</CFIF>

<CFQUERY NAME="GetRecordRights" datasource="#application.siteDataSource#">
	SELECT 
		RecordID
	FROM 
		RecordRights
	WHERE 
		Entity = 'Flag'
		AND UserGroupID =  <cf_queryparam value="#ListGetAt(Cookie.user, 2, "-")#" CFSQLTYPE="CF_SQL_INTEGER" > 
</CFQUERY>

<CFSET Allowed = ValueList(GetRecordRights.RecordID)>

<CFQUERY NAME="GetEventTable" datasource="#application.siteDataSource#">
	SELECT 
		ft.DataTable, 
		f.FlagTextID,
		fg.FlagGroupTextID
	FROM 
		Flag AS f, 
		FlagGroup AS fg, 
		FlagType AS ft
	WHERE 
		f.FlagGroupID = fg.FlagGroupID
		AND fg.FlagTypeID = ft.FlagTypeID
		AND f.FlagID =  <cf_queryparam value="#FlagID#" CFSQLTYPE="CF_SQL_INTEGER" > 
</CFQUERY>

<CFSET EventTable = "#GetEventTable.DataTable#FlagData">

<cfinclude template="sql_GetEventDetails.cfm">

<CFQUERY NAME="getEventCountry" datasource="#application.siteDataSource#">
	SELECT DISTINCT 
		c.CountryID, c.CountryDescription
	FROM 
		Country AS c, 
		#EventTable# AS et, 
		person as p, 
		location as l
	WHERE 
		c.CountryID = l.countryid
		AND l.locationid = p.locationid
		AND p.personid = et.entityid
		AND et.FlagID =  <cf_queryparam value="#FlagID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	order by 
		c.countrydescription
</CFQUERY>

<CFQUERY NAME="getEventAnalysis" datasource="#application.siteDataSource#">
 	SELECT DISTINCT 
		vfv.FieldName
	FROM 
		ValidFieldValues AS vfv
	WHERE 
		vfv.InAnalysis = 1
		AND FieldName  LIKE  <cf_queryparam value="flag.#GetEventTable.FlagTextID#%" CFSQLTYPE="CF_SQL_VARCHAR" > 

	UNION

	SELECT DISTINCT 
		vfv.FieldName
	FROM 
		ValidFieldValues AS vfv
	WHERE 
		vfv.InAnalysis = 1
		AND FieldName  LIKE  <cf_queryparam value="flagGroup.#GetEventTable.FlagGroupTextID#%" CFSQLTYPE="CF_SQL_VARCHAR" > 	
		and not exists (SELECT 1
						FROM 
							ValidFieldValues AS vfv
						WHERE 
							vfv.InAnalysis = 1
							AND FieldName  LIKE  <cf_queryparam value="flag.#GetEventTable.FlagTextID#%" CFSQLTYPE="CF_SQL_VARCHAR" > 
						)

</CFQUERY>


<CFSCRIPT>
	AnalysisNames = StructNew();
 	StructInsert(AnalysisNames,"Regstatus","Registration Status");
	StructInsert(AnalysisNames,"wave","Registration Wave");
	StructInsert(AnalysisNames,"CourseContent","Course Content");
 	StructInsert(AnalysisNames,"sitename","Delegate Site");
	StructInsert(AnalysisNames,"RegType","Registration Method (Web/fax etc.)");
	StructInsert(AnalysisNames,"EmployeeCat","Employee Categories");
	StructInsert(AnalysisNames,"assignedwave","Assigned Wave");
	StructInsert(AnalysisNames,"assignedCourseContent","Assigned Course");
	StructInsert(AnalysisNames,"assignedLanguage","Assigned Language");	
</CFSCRIPT>

<CFQUERY NAME="getEventPeople" datasource="#application.siteDataSource#">
	SELECT DISTINCT 
		FlagID
	FROM 
		#EventTable#
	WHERE 
		FlagID =  <cf_queryparam value="#FlagID#" CFSQLTYPE="CF_SQL_INTEGER" > 
</CFQUERY>

	<CFQUERY NAME="getAnalysisDataTotal" datasource="#application.siteDataSource#">
		SELECT 
			Count(*) AS Total
		FROM 
			#EventTable# AS efd , 
			person as p, 
			location as l
		WHERE 
			l.locationid = p.locationid
			AND p.personid = efd.entityid
			<CFIF ListFirst(frmChooseCountryID) NEQ 0>
				AND l.CountryID =  <cf_queryparam value="#ListFirst(frmChooseCountryID)#" CFSQLTYPE="CF_SQL_INTEGER" > 
			</CFIF>
			AND efd.FlagID =  <cf_queryparam value="#FlagID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</CFQUERY>

	<CFSET Total = getAnalysisDataTotal.Total>

	<CFQUERY NAME="getAnalysisData" datasource="#application.siteDataSource#">
		SELECT 
			efd.#frmAnalysis# AS Analysis, 
			Count(efd.#frmAnalysis#) AS EachTotal, 
			Fraction=(Convert(Real, Count(efd.#frmAnalysis#))/#Total#)*100
		FROM 
			#EventTable# AS efd, 
			person as p, 
			location as l
		WHERE 
			l.locationid = p.locationid
			AND p.personid = efd.entityid
			<CFIF ListFirst(frmChooseCountryID) NEQ 0>
				AND l.CountryID =  <cf_queryparam value="#ListFirst(frmChooseCountryID)#" CFSQLTYPE="CF_SQL_INTEGER" > 
			</CFIF>
			AND efd.FlagID =  <cf_queryparam value="#FlagID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		GROUP BY 
			efd.#frmAnalysis#
		ORDER BY 
			EachTotal DESC
	</CFQUERY>
	
	<CFIF IsDefined("CrossTab")>
		<cFQUERY NAME="Columns" datasource="#application.sitedatasource#" >
			select 
				countrygroupid as columnid,  
				countrydescription as columnheading
			from 
				eventflagdata, 
				person,
				location, 
				countrygroup, 
				country
			where 
				entityid = personid 
				and  person.locationid = location.locationid 
				and location.countryid = countrygroup.countrymemberid
				and country.countryid = countrygroup.countrygroupid 
				and flagID =  <cf_queryparam value="#FlagID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	 		group by 
				countrygroupid, countrydescription
			order by 
				countrydescription
		</CFQUERY>
	
		<CFSET  columnids = valuelist(columns.columnid)>
	
		<cFQUERY NAME="getstats" datasource="#application.sitedatasource#" >
			select 
				efd.#frmAnalysis# as rowheading, 
				upper(efd.#frmAnalysis#) as rowID , 
				countrygroupid as columnid,  
				count(1) as number, 
				countrydescription
			from 
				eventflagdata as efd, 
				person,
				location, 
				countrygroup, 
				country
			where 
				entityid = personid 
				and  person.locationid = location.locationid 
				and location.countryid =countrygroup.countrymemberid
				and country.countryid = countrygroup.countrygroupid 
				and flagid =  <cf_queryparam value="#FlagID#" CFSQLTYPE="CF_SQL_INTEGER" > 
			group by 
				efd.#frmAnalysis#, 
				upper(efd.#frmAnalysis#), 
				countrygroupid, 
				countrydescription
			order by 
				efd.#frmAnalysis#, 
				countrydescription
		</CFQUERY>	
	</CFIF>
	
	<CFSET TimeStamp = TimeFormat(Now(), "hhmmss")>
	<CFSET GraphicsFileName = "#request.relayCurrentUser.usergroupid#_#TimeStamp#.gif">
	<CFSET GraphicsFileNameAndPath = "TempGraphics/#request.relayCurrentUser.usergroupid#_#TimeStamp#.gif">	
	

	<CFX_GIFGD ACTION="DRAW_PIE"
		OUTPUT="#path#/#GraphicsFileNameAndPath#"
		LEGENDS="#ValueList(getAnalysisData.Analysis)#"
		DATA="#ValueList(getAnalysisData.EachTotal)#"
		SIZE="275"
		MAXROWS="15"
		HEADER="#getEventDetails.Title#"
		FOOTER="#ListLast(frmChooseCountryID)# - #StructFind(AnalysisNames,frmAnalysis)# (#DateFormat(Now(),'dd-mmm-yyyy')#, #TimeFormat(Now(),'HH:mm')#)"> <!--- NJH 2006/10/02 hh:mm tt --->



<CFOUTPUT>
<SCRIPT type="text/javascript">
<!--

function emailStats() {
	newWindow = openWin( 'eventStatsEmail.cfm?FlagID=#FlagID#&Event=#URLEncodedFormat(getEventDetails.Title)#' ,'PopUp', 'width=450,height=450,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1' );
	newWindow.focus();
}

function openWin( windowURL,  windowName, windowFeatures ) { 
	return window.open( windowURL, windowName, windowFeatures ); 
}

//-->
</SCRIPT>
</CFOUTPUT>

<!---<TABLE WIDTH="100%" CELLPADDING="2" CELLSPACING="2" BORDER="0">
<TR>
	<TD ALIGN="left"><H1>Event Statistics</H1></TD>
	<TD ALIGN="right"><A HREF="eventHome.cfm">Events Main Page</A></TD>
</TR>
</TABLE>
<P>
<H1><CFOUTPUT>#getEventDetails.Title#</CFOUTPUT></H1>
<P>--->
<CFIF getEventPeople.RecordCount EQ 0 OR getEventAnalysis.RecordCount EQ 0>
	Sorry, no statistical analysis can be performed because<BR>
	<CFIF getEventAnalysis.RecordCount EQ 0>
		there are no Analysis fields defined for this event.
	<CFELSE>
		there are no delegates assigned to this event.
	</CFIF>
	<CF_ABORT>
</CFIF>
Please choose a country and the required analysis
<P>
<FORM NAME="eventStats" ACTION="eventStats.cfm" METHOD="post">

<CFOUTPUT>
<CF_INPUT TYPE="hidden" NAME="FlagID" VALUE="#FlagID#">

</CFOUTPUT>

<SELECT NAME="frmChooseCountryID">
	<OPTION VALUE="0,All Countries" <CFIF IsDefined("frmChooseCountryID")><CFIF ListFirst(frmChooseCountryID) EQ 0>SELECTED</CFIF></CFIF>>All Countries
	<CFOUTPUT QUERY="getEventCountry"><OPTION VALUE="#CountryID#,#CountryDescription#" <CFIF IsDefined("frmChooseCountryID")><CFIF ListFirst(frmChooseCountryID) EQ CountryID>SELECTED</CFIF></CFIF>>#htmleditformat(CountryDescription)#</CFOUTPUT>
</SELECT>
&nbsp;&nbsp;&nbsp;
<SELECT NAME="frmAnalysis">

	<CFOUTPUT QUERY="getEventAnalysis"> <OPTION VALUE="#ListLast(FieldName, '.')#" <CFIF IsDefined("frmAnalysis")> <CFIF frmAnalysis IS ListLast(FieldName, '.')> SELECTED </CFIF> </CFIF> >#StructFind(AnalysisNames,ListLast(FieldName, '.'))#</CFOUTPUT>
</SELECT>
&nbsp;&nbsp;
<INPUT TYPE="submit" NAME="StatSubmit" VALUE="  Go  "><BR>

<INPUT TYPE="checkbox" NAME="Crosstab" VALUE="1" <CFIF IsDefined("CrossTab")>CHECKED</CFIF> >Display Analysis in the CrossTab fashion

</FORM>

	<CFOUTPUT>
	<P>Analysis of #StructFind(AnalysisNames,frmAnalysis)# for <B>#htmleditformat(ListLast(frmChooseCountryID))#</B>
	</CFOUTPUT>
	<P>
	<CFIF NOT IsDefined("CrossTab")  OR frmAnalysis IS 'sitename'>
	<TABLE CELLPADDING="2" CELLSPACING="2" BORDER="0">
	<CFOUTPUT>
		<CFIF ManagerOK>
			<TR>
				<TD COLSPAN="3"><A HREF="JavaScript:emailStats();">E-mail these stats to a #htmleditformat(application.clientName)# user</A></TD>
			</TR>
		</CFIF>
		<TR>
			<TD COLSPAN="3">&nbsp;</TD>
		</TR>
		<TR>
			<TD COLSPAN="3">#htmleditformat(Total)# Delegates</TD>
		</TR>
		<TR>
			<TD COLSPAN="3">&nbsp;</TD>
		</TR>
		<TR>
			<TD WIDTH="200" CLASS="label">#StructFind(AnalysisNames,frmAnalysis)#</TD>
			<TD WIDTH="100" CLASS="label">Delegates</TD>
			<TD WIDTH="20" CLASS="label">%</TD>
		</TR>
	</CFOUTPUT>
	<CFOUTPUT QUERY="getAnalysisData">
		<TR>
			<TD WIDTH="200"><A HREF="eventDelegateList.cfm?FlagID=#FlagID#&countryid=#ListFirst(frmChooseCountryID)#&filter=#URLEncodedFormat(frmAnalysis)#&criteria=#URLEncodedFormat(Analysis)#">#htmleditformat(Analysis)#</A></TD>
			<TD WIDTH="100">#htmleditformat(EachTotal)#</TD>
			<TD WIDTH="20">#DecimalFormat(Fraction)#</TD>
		</TR>
	</CFOUTPUT>
	<TR>
		<TD COLSPAN="3"><BR><CFOUTPUT><IMG SRC="/#GraphicsFileNameAndPath#" BORDER="0"></CFOUTPUT></TD>
	</TR>
</TABLE>
	</CFIF>

<CFIF IsDefined("CrossTab") AND frmAnalysis IS NOT "sitename">

	<!---Set variables for the file DownloadLink--->
	<CFSET UserGroupID = #request.relayCurrentUser.usergroupid#>
	<CFSET DownloadFile = "CrossTab_#UserGroupID#.xls">
	<CFSET localPath = "#path#\tempGraphics\#downloadfile#">
	<CFSET webPath = "/tempGraphics/#downloadfile#">
	
	<CF_FNLCrossTab Query1="Columns" 
					Query2="GetStats"
					Action="eventDelegateList.cfm?filter=#frmAnalysis#&FlagID=#FlagID#" 
					ColumnName="CountryGroupID" 
					RowName="Criteria"
					DownloadLink = "Yes"
					DownloadLocalPath = "#localPath#"
					DownloadWebPath = "#webpath#"
					>
</CFIF>