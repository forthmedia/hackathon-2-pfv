<!--- �Relayware. All Rights Reserved 2014 --->
<!--- amendments
	1999-03-15 WAB problem with the counts by organisation not counting within the current region.  Had to add a location table and countrytest to all the counts.  Then had too many tables so removed some columns 
 --->


<!---  Screen for allocating places at 3Com U to organisations 
		optional parameter frmregionID, otherwise users location region used 

Assumes the following flagtextids with a prefix defined in eventprefix:
		ToBeInvited   (actually can be selection)
		Invited
		Preferred
		Registered
		Submitted
		
		
 --->
<CFPARAM NAme="frmDisplayMode" Default="Screen"> <!--- other would be download --->

 
<CFPARAM Name="frmEvent" Default="">	
<CFPARAM NAME="frmEventPrefix" DEFAULT="">
<CFPARAM name="frmStartLetter" Default="">		<!--- used for jumping through organisations --->
<CFSET displaystart = 1>


<!--- these are used to decide on valid people (hence valid organisations) to display --->
<!--- removed this to speed things up now that all this should have been done<CFSET possibleinvitesselectiontextid="U99PossibleInvitees"> --->
<CFSET possibleinvitesselectiontextid="">
<CFSET actualinvitesselectiontextid="U99tobeinvited">      <!--- can be more than one of these for individual countries --->
<CFSET actualinvitesflagtextid="U99tobeinvited">


<CFQUERY NAME="getReportDefinition" datasource="#application.siteDataSource#">
Select 	sourcetable,
		sourcetextid,
		function,
		displayas,
		title,
		download,
		display
 from reportdef
where reportid =  <cf_queryparam value="#frmreportdef#" CFSQLTYPE="CF_SQL_INTEGER" > 
and active = 1
order by sortorder
</CFQUERY>

 
<CFSET ColumnAlias = valuelist(getreportdefinition.sourcetextid)>
<CFSET ColumnSource = valuelist(getreportdefinition.sourcetextid)>
<CFSET ColumnSourceTable = valuelist(getreportdefinition.sourcetable)>
<CFSET ColumnTitle = valuelist(getreportdefinition.title)>
<CFSET ColumnFunction = valuelist(getreportdefinition.function)>
<CFSET ColumnDisplayas = valuelist(getreportdefinition.displayas)>
<CFSET ColumnDisplay = valuelist(getreportdefinition.display)>
<CFSET ColumnDownload = valuelist(getreportdefinition.download)>

<CFSET columnsourcetype="">
<CFLOOP index=I from="1"   to="#listlen(columnsourcetable)#">
	<CFIF find("flag",listgetat(columnsourcetable,I)) IS NOT 0>
		<CFSET columnsourcetype=listappend(columnsourcetype, "flag")>
	<CFELSEIF find("selection",listgetat(columnsourcetable,I)) IS NOT 0>
		<CFSET columnsourcetype=listappend(columnsourcetype, "selection")>
	<CFELSEIF find("special",listgetat(columnsourcetable,I)) IS NOT 0>
		<CFSET columnsourcetype=listappend(columnsourcetype, "special")>
	<CFELSE>
		<CFSET columnsourcetype=listappend(columnsourcetype, " ")>
	</cfif>
</cfloop>


<!--- these are the flag names of flags stored in IntegerFlagDataOrg --->
<CFSET regionsalesplacesflag = "RegionSalesPlaces">
<CFSET regionpresalesplacesflag = "RegionPreSalesPlaces">

<!--- get the user's countries --->
<CFINCLUDE TEMPLATE="../templates/qrygetcountries.cfm">
<!--- qrygetcountries creates a variable CountryList --->

<!--- 	Note definition of a region is if it has a value in regionsalesplacesflag, or regionpredsalesplacesflag -
		Note that this is because some countries (CH and AU) are considered separate regions for this stuff --->

<!--- Find all the 3ComU Regions that the user can use--->
<!--- I think that we are going to have to assume that a user who can access a single country in a region can see them all for this project --->
<CFQUERY NAME="getUserRegions" datasource="#application.siteDataSource#">
<!---  These are the real regions--->
SELECT DISTINCT b.CountryDescription AS Region, b.CountryID
 FROM Country AS a, Country AS b, CountryGroup AS c
WHERE a.CountryID  IN ( <cf_queryparam value="#Variables.CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
  AND c.CountryGroupID = b.CountryID
  AND c.CountryMemberID = a.CountryID
  AND b.countryid in (select distinct entityid from integerflagdata, flag where (flagtextid =  <cf_queryparam value="#frmeventprefix##RegionSalesPlacesflag#" CFSQLTYPE="CF_SQL_VARCHAR" >  or flagtextid =  <cf_queryparam value="#frmeventprefix##RegionPreSalesPlacesflag#" CFSQLTYPE="CF_SQL_VARCHAR" > ) and data >0 )

 UNION
<!--- These are the countries masquerading as regions --->
SELECT DISTINCT b.CountryDescription AS Region, b.CountryID
 FROM Country AS b
WHERE b.CountryID  IN ( <cf_queryparam value="#Variables.CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	and b.countryid in (select distinct entityid from integerflagdata, flag where (flagtextid =  <cf_queryparam value="#frmeventprefix##RegionSalesPlacesflag#" CFSQLTYPE="CF_SQL_VARCHAR" >  or flagtextid =  <cf_queryparam value="#frmeventprefix##RegionPreSalesPlacesflag#" CFSQLTYPE="CF_SQL_VARCHAR" > ) and data >0 )
ORDER BY Region <!--- b.countrydescription --->
</CFQUERY>


<!--- these are the regions where the allocation is complete - later we use this to make the columns non updateable --->
<CFQUERY NAME="getcompletedRegions" datasource="#application.siteDataSource#">
Select distinct entityid
from booleanflagdata as bfd, flag as f
where f.flagid=bfd.flagid
and f.flagtextid='U99RegionAllocationCompl'
</CFQUERY>


<CFIF NOT IsDefined("frmregionID")>
	<!--- decide upon a default region based on users location , but check they actually have rights to it! --->

	<!--- gets the region the user is currently in  --->
	<CFQUERY NAME="getRegion" datasource="#application.sitedatasource#">
	SELECT 	l.CountryID,
			cg.CountryGroupID,
			r.CountryDescription as RegionName
	FROM 	Country as r, CountryGroup as cg, Person as p, Location as l, country as c
	WHERE 	p.PersonID= #request.relayCurrentUser.personid#
	AND 	p.LocationID=l.locationid
	AND 	l.CountryID=cg.countrymemberid
	AND 	cg.CountryGroupID=r.countryid
	and    	cg.countrygroupID  in ( <cf_queryparam value="#valuelist(getuserregions.countryid)#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) 
	</CFQUERY>


	<CFSET frmregionID=listfirst(valuelist(getregion.countrygroupid))>
	<CFSET regionname=listfirst(valuelist(getregion.RegionName))>


	<CFIF getregion.recordcount is 0>
		<!--- could not find a default region 
			This means that the user is located in a country which they do not have rights to
			so  try setting it to one of their regions from the get user regions query--->
		<CFSET frmregionID=listfirst(valuelist(getuserregions.countryid))>
		<CFSET regionname=listfirst(valuelist(getuserregions.Region))>			
		
	
	</CFIF>



</CFIF>

<CFIF frmregionid IS "ALL">
	<CFSET frmregionid = valuelist(getuserregions.countryid)>
</CFIF>

	<!--- get ids of countries in the 'region' we are using --->
	<CFQUERY NAME="getCountriesInRegion" datasource="#application.sitedatasource#" DEBUG >
	SELECT  countrymemberid
	from 	countrygroup
	where	countrygroupid  IN ( <cf_queryparam value="#frmregionID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	</CFQUERY>
	
	<CFIF getCountriesInRegion.recordcount is 0>
	<!--- 3ComURegion must be a country --->   <!--- got around this by creating entries in countrygroup table for these --->
			<CFSET currentcountries=#frmregionID#>	
	<CFELSE>	
	<!--- 3ComURegion is a real region --->
			<CFSET currentcountries=valuelist(getcountriesinregion.countrymemberid)>	
	</CFIF>

	
<!--- list of selectionids which give us the people (and therefore organisations) to display later on  --->
	<CFQUERY NAME="getValidPeopleSelections" datasource="#application.sitedatasource#" DEBUG >
	select selectionid
	from selection
	where selectiontextid =  <cf_queryparam value="#possibleinvitesselectiontextid#" CFSQLTYPE="CF_SQL_VARCHAR" > 
	or selectiontextid =  <cf_queryparam value="#actualinvitesselectiontextid#" CFSQLTYPE="CF_SQL_VARCHAR" > 	
	</CFQUERY>

	<CFQUERY NAME="getValidPeopleFlags" datasource="#application.sitedatasource#" DEBUG >
	select flagid
	from flag
	where flagtextid =  <cf_queryparam value="#actualinvitesflagtextid#" CFSQLTYPE="CF_SQL_VARCHAR" > 	
	</CFQUERY>



<CFSET freezedate=now()>



<!--- get ids of all columns required for the main part of the screen--->

<CFIF listcontainsnocase(columnsourcetype, "flag") IS NOT 0>
	<CFQUERY NAME="getFlagIDs" datasource="#application.sitedatasource#">
	Select
		<CFLOOP index="I" from="1"  to="#listlen(columnsource)#">
			<CFIF trim(listgetat(columnsourcetype,I)) IS "Flag"> 
				(select flagid from flag WHERE flagtextid =  <cf_queryparam value="#frmEventPrefix##listgetat(columnsource,I)#" CFSQLTYPE="CF_SQL_VARCHAR" > ) as #listgetat(columnalias,I)#,
			</CFIF>
		</CFLOOP>
1
from dual
</cfquery> 
</CFIF>

<!--- this is a complete hack - the selection query above started to return more than one selection (because multiple selections were used for invites) and this caused problems --->
<!--- only handles one selection --->
<CFIF listcontainsnocase(columnsourcetype, "selection") IS NOT 0>
	<CFQUERY NAME="getselectionIDs" datasource="#application.sitedatasource#">
		<CFLOOP index="I" from="1"  to="#listlen(columnsource)#">
			<CFIF trim(listgetat(columnsourcetype,I)) IS "Selection"> 
				select selectionid as #listgetat(columnalias,I)#
				from selection 
				where selectiontextid =  <cf_queryparam value="#frmEventPrefix##listgetat(columnsource,I)#" CFSQLTYPE="CF_SQL_VARCHAR" >  
			</CFIF>
		</CFLOOP>

	</cfquery> 
</cfif>

<cfset temporarydatabaseprefix="TempTables.dbo.">
<cfset TEMPORARYtablename="TempTable_"&request.relayCurrentUser.personid>
<cfset TEMPORARYtable=temporarydatabaseprefix&TEMPORARYtablename>
<!--- need to check for existence --->

<CFQUERY NAME="checkforTemptable" datasource="#application.sitedatasource#" DEBUG >		
select * from #temporarydatabaseprefix#sysobjects where type='u' and name='#temporarytablename#'
</CFQUERY>

<CFIF checkforTemptable.recordcount GT 0>
	<CFQUERY NAME="droptable" datasource="#application.sitedatasource#" DEBUG >
		drop table #temporarytable#
	</cfquery>   
</cfif>

<!--- create temporary table --->
<!--- 	I attempted to do an insert into table - but couldn't define whether fields allowed null --->
 	<CFQUERY NAME="createtable" datasource="#application.sitedatasource#" DEBUG >	
	create table #temporarytable#
	(organisationid int,
	 regionid int,	
		<CFLOOP index="I" from="1"  to="#listlen(columnsource)#">
			<CFIF trim(listgetat(columnfunction,I)) IS "Sum"> 
			#listgetat(columnalias,I)# int NULL,
			<CFELSEIF trim(listgetat(columnfunction,I)) IS "Count"> 
			#listgetat(columnalias,I)# int NULL,
			<cfelse>
<!--- 			#listgetat(columnalias,I)# varchar(255) NULL, --->
			</CFIF>
		</cfloop>
	regionname varchar(255), 
	organisationname varchar(255) NULL)
	
	</cfquery> 


<!--- at list of organisation ids to table--->
<CFQUERY NAME="getOrgIds" datasource="#application.sitedatasource#" DEBUG >
	Insert into #temporarytable# (organisationid, organisationname, regionid, regionname)
 	 	SELECT 	distinct organisation.OrganisationID, 
				organisationname, countrygroupid, countrydescription
		from 	organisation, location , person, countrygroup, country 
		where 	person.locationid=location.locationid
		and 	location.organisationid=organisation.organisationid
		AND 	location.countryID  in ( <cf_queryparam value="#currentcountries#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) 
		AND		location.countryid= countrygroup.countrymemberid
		and		countrygroup.countrygroupid=country.countryid
		and 	countrygroup.countrygroupid  in ( <cf_queryparam value="#frmregionID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )	

		<CFIF frmevent IS "3ComUP">
		AND   	(person.personid in (select entityid from selectiontag where status <> 0 and selectionid  in ( <cf_queryparam value="#valuelist(getValidPeopleSelections.selectionid)#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) )
					OR organisation.organisationid in (select entityid from integerflagdataorg as ifd, flag as f where f.flagid=ifd.flagid and f.flagtextid in ('U99OrgSalesPlaces','U99OrgPreSalesPlaces') )
				)	
		AND		location.sitename not like '3Com%'
		
		</cfif>
		<CFIF frmevent IS "3ComUInternal">
		AND		location.sitename like '3Com%'
		</cfif>
		
<!--- add this line when U99Invited Flag Has Been Populated --->
<!--- 		AND   p.personid in (select entityid from booleanflagdata where flagidid in (#valuelist(getValidPeopleFlags.flagid)#) ) --->
</CFQUERY>





<!--- statistics on each organisation for the main part of the screen (loop through all columns)--->


<CFLOOP index="I" from="1"  to="#listlen(columnsource)#">

<CFIF trim(listgetat(columnfunction,I)) IS "Sum" or trim(listgetat(columnfunction,I)) IS "Count">


 <CFQUERY NAME="updateorgstats" datasource="#application.sitedatasource#">
Update #temporarytable#
	set #listgetat(columnalias,I)# = 

		<CFIF trim(listgetat(columnsourcetype,I)) IS "Flag"> 
			<CFIF trim(listgetat(columnfunction,I)) IS "Sum"> 
					(select sum(data) 
					from #listgetat(columnsourcetable,I)# as a
					WHERE  a.flagid =  <cf_queryparam value="#evaluate("getflagids."&listgetat(columnalias,I))#" CFSQLTYPE="CF_SQL_INTEGER" > 
					AND a.countryid=#temporarytable#.regionid
					AND a.entityid=#temporarytable#.organisationid)

			<CFELSEIF trim(listgetat(columnfunction,I)) IS "Count"> 
					(select count(1) 
					from #listgetat(columnsourcetable,I)# as a, person as p, location as l, countrygroup as cg
					WHERE a.flagid =  <cf_queryparam value="#evaluate("getflagids."&listgetat(columnalias,I))#" CFSQLTYPE="CF_SQL_INTEGER" > 
					AND a.entityid=p.personid
					AND p.active <>0 
					and p.organisationid=#temporarytable#.organisationid
					and p.locationid=l.locationid
					and l.countryid = cg.countrymemberid
					and cg.countrygroupid =#temporarytable#.regionid ) 

			</CFIF>
		<CFELSEIF trim(listgetat(columnsourcetype,I)) IS "Selection"> 
			<CFIF trim(listgetat(columnfunction,I)) IS "Count"> 
					(select count(1) 
					from person as p, #listgetat(columnsourcetable,I)# as a, location as l, countrygroup as cg
					WHERE a.selectionid  in ( <cf_queryparam value="#evaluate("valuelist(getselectionids."&listgetat(columnalias,I)&")")#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
					AND a.entityid=p.personid
					AND p.active <>0 
					AND a.status<>0
					and p.organisationid=#temporarytable#.organisationid
					and p.locationid=l.locationid
					and l.countryid = cg.countrymemberid
					and cg.countrygroupid =#temporarytable#.regionid )

			</CFIF>
		<CFELSEIF trim(listgetat(columnsourcetype,I)) IS "Special"> 
			<CFIF trim(listgetat(columnalias,I)) IS "PreferredNotRegistered"> 
					(select count(1) 
					from person as p
					WHERE p.active <>0 
					and p.organisationid=#temporarytable#.organisationid
					and p.personid in (	select 	entityid 
											from 	booleanflagdata as a 											
											where 	a.flagid =  <cf_queryparam value="#getflagids.Preferred#" CFSQLTYPE="CF_SQL_INTEGER" > )
					and p.personid not in (	select 	entityid 
											from 	booleanflagdata as a 											
											where 	a.flagid =  <cf_queryparam value="#getflagids.Registered#" CFSQLTYPE="CF_SQL_INTEGER" > )
					)							 				

			<CFELSEIF trim(listgetat(columnalias,I)) IS "PeopleInOrganisation"> 
	 			(select count(1) 
					from person as p, location as l
					WHERE p.organisationid=#temporarytable#.organisationid
					AND p.locationid = l.locationid
					AND 	l.countryID  in ( <cf_queryparam value="#currentcountries#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) 
					AND p.active <>0 )

			</CFIF>
		</CFIF>





</CFQUERY> 
</cfif>
</CFLOOP>

<CFQUERY NAME="getOrganisationStats" datasource="#application.sitedatasource#">
Select organisationid,
		<CFIF DBType IS "Access">
		LEFT(organisationname,1) as firstletter,
		<CFELSE>		
		SUBSTRING(organisationname,1,1) as firstletter,
		</CFIF>		
		organisationname, 
		<CFLOOP index="I" from="1"  to="#listlen(columnsource)#">		
		<CFIF left(listgetat(columnsourcetype,I),1) IS NOT "_">
		#listgetat(columnalias,I)#,
		</cfif>
		</CFLOOP>
		1
from #temporarytable#		
order by regionname, organisationname
</CFQUERY> 


<CFIF frmevent IS "3ComUP">
<!--- get the number of places allocated to this region --->
<CFQUERY NAME="getRegionStats1" datasource="#application.sitedatasource#">
Select
	(Select sum(data)
		from Country as c, flag as f, integerflagdata as ifd
		where c.countryid  IN ( <cf_queryparam value="#frmregionid#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		and ifd.entityid = c.countryid
		and ifd.flagid = f.flagid
		and f.flagtextid =  <cf_queryparam value="#frmeventprefix##regionsalesplacesflag#" CFSQLTYPE="CF_SQL_VARCHAR" > ) as salesplaces,
	(Select sum(data)
		from Country as c, flag as f, integerflagdata as ifd
		where c.countryid  in ( <cf_queryparam value="#frmregionid#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		and ifd.entityid = c.countryid
		and ifd.flagid = f.flagid
		and f.flagtextid =  <cf_queryparam value="#frmeventprefix##regionpresalesplacesflag#" CFSQLTYPE="CF_SQL_VARCHAR" > ) as presalesplaces,
	(select sum(data) 
		from IntegerFlagDataOrg as ifd, flag as f
		WHERE f.flagtextid =  <cf_queryparam value="#frmEventPrefix#OrgSalesPlaces" CFSQLTYPE="CF_SQL_VARCHAR" > 
		AND ifd.flagid=f.flagid
		and ifd.entityid in (select organisationid from #temporarytable#)
		AND ifd.countryid  in ( <cf_queryparam value="#frmregionid#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )) as SalesPlacesAllocated ,
	(select sum(data) 
		from IntegerFlagDataOrg as ifd, flag as f
		WHERE f.flagtextid =  <cf_queryparam value="#frmEventPrefix#OrgPreSalesPlaces" CFSQLTYPE="CF_SQL_VARCHAR" > 
		AND ifd.flagid=f.flagid
		and ifd.entityid in (select organisationid from #temporarytable#)
		AND ifd.countryid  in ( <cf_queryparam value="#frmregionid#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )) as  PreSalesPlacesAllocated,

		
1
FROM DUAL		
</cfquery>
</CFIF>

<!--- and another query - same as above except combined they are too big for SQL --->
<CFQUERY NAME="getRegionStats2" datasource="#application.sitedatasource#">
Select
	(select count(1) as amount
		from location as l, person as p,booleanflagdata as bfd, flag as f
		WHERE f.flagtextid =  <cf_queryparam value="#frmEventPrefix#Registered" CFSQLTYPE="CF_SQL_VARCHAR" > 
		and f.flagid=bfd.flagid
		AND bfd.entityid=p.personid
		AND p.locationid=l.locationid
		and l.countryid  in ( <cf_queryparam value="#currentcountries#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		and l.organisationid in (select organisationid from #temporarytable#)) as registered,
	(select count(1)  as amount
	from location as l, person as p, booleanflagdata as bfd, flag as f
	WHERE f.flagtextid =  <cf_queryparam value="#frmEventPrefix#Preferred" CFSQLTYPE="CF_SQL_VARCHAR" > 
	and f.flagid=bfd.flagid
	AND bfd.entityid=p.personid
	AND p.locationid=l.locationid
	and l.countryid  in ( <cf_queryparam value="#currentcountries#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	and l.organisationid in (select organisationid from #temporarytable#)
	and p.personid not in (select entityid 
							from 	booleanflagdata as bfd, flag as f
							WHERE 	f.flagtextid =  <cf_queryparam value="#frmEventPrefix#Registered" CFSQLTYPE="CF_SQL_VARCHAR" > 
							and 	f.flagid=bfd.flagid) ) as preferrednotregistered,

1
FROM DUAL		
</cfquery>


<CFQUERY NAME="getRegionStats3" datasource="#application.sitedatasource#">
Select
	(select count(1) as amount
		from location as l, person as p,booleanflagdata as bfd, flag as f
		WHERE f.flagtextid =  <cf_queryparam value="#frmEventPrefix#RegSubmitted" CFSQLTYPE="CF_SQL_VARCHAR" > 
		and f.flagid=bfd.flagid
		AND bfd.entityid=p.personid
		AND p.locationid=l.locationid
		AND l.sitename not like '3Com%'
		and l.countryid  in ( <cf_queryparam value="#currentcountries#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )) as PartnerSubmitted,
	(select count(1) as amount
		from location as l, person as p,booleanflagdata as bfd, flag as f
		WHERE f.flagtextid =  <cf_queryparam value="#frmEventPrefix#RegSubmitted" CFSQLTYPE="CF_SQL_VARCHAR" > 
		and f.flagid=bfd.flagid
		AND bfd.entityid=p.personid
		AND p.locationid=l.locationid
		AND l.sitename like '3Com%'
		and l.countryid  in ( <cf_queryparam value="#currentcountries#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )) as com3Submitted,
1
FROM DUAL		
</cfquery>




<!--- If the current region is in the list of regions which have completed their allocation, then we need to disable all the updating --->
<!--- unless user is Jodie McMurray 205 --->
<CFIF listcontainsnocase(valuelist(getcompletedRegions.entityid),frmregionid) IS  NOT 0 and request.relayCurrentUser.usergroupid IS NOT 205>
	<CFSET columndisplayas=replace(columndisplayas,"update","value","ALL")>
</cfif>




<CFIF getOrganisationStats.RecordCount GT application.showRecords>

	<!--- showrecords in application .cfm 
			defines the number of records to display --->
	
	<!--- define the start record --->

	<CFIF frmStartLetter is not "">
	<!---  user has selected a letter to go to --->
		<CFSET firstletters=valuelist(getOrganisationStats.firstletter)>
		<CFSET Max = listlen(firstletters)>
		<CFSET I = 1>
		
		  <CFLOOP Condition="CompareNoCase(ListGetat(firstletters,I),frmstartletter) is -1 and (I LESS THAN MAX)"> 

		  	<CFSET I=I+1>
		  </CFLOOP>
		<CFSET displaystart= I>
 	<CFELSEIF IsDefined("FORM.ListForward.x")>
		 <CFSET displaystart = frmStart + application.showRecords>
 	<CFELSEIF IsDefined("FORM.ListBack.x")>		 
		 <CFSET displaystart = frmStart - application.showRecords>
	<CFELSEIF IsDefined("frmStart")>
	 	 <CFSET displaystart = frmStart>
	</CFIF>

</CFIF>


<!---  add this if want to delete temp table after
 <CFQUERY NAME="checkforTemptable" datasource="#application.sitedatasource#" DEBUG >		
select * from #temporarydatabaseprefix#sysobjects where type='u' and name='#temporarytablename#'
</CFQUERY>

<CFIF checkforTemptable.recordcount GT 0>
	<CFQUERY NAME="droptable" datasource="#application.sitedatasource#" DEBUG >
		drop table #temporarytable#
	</cfquery>   
</cfif> --->


<CFIF frmDisplayMode IS "Screen">




<cf_head>
	<cf_title><cfoutput>#request.CurrentSite.Title#</cfoutput></cf_title>
<SCRIPT type="text/javascript">

<!--
        function viewPeople(orgID) {
//		calls sub screen with person details				
                var form = document.FlagGroupForm;
                newWindow=window.open('orgpeople.cfm?frmorgid='+orgID+'&frmeventPrefix=<CFOUTPUT>#frmeventprefix#&frmeventname=#replace(frmeventname," ","_","ALL")#&frmcurrentcountries=#currentcountries#&frmregionid=#frmregionid#</cfoutput>', 'OrgPeople',  'width=700,height=500,toolbar=0,location=0,directories=0,status=1,menuBar=0,scrollBars=1,resizable=1') ; 
				newWindow.focus()
				return 
        }

		function doForm() {
			var form = document.ThisForm;
			form.submit(); 
		}

	function checktextbox(textbox) {


		if (textbox.value=='') {
			alert('The Organisation name should not be blank');

			}

		
	}		
	
	
	function docalcs(allocationbox,flagid,organisationid,column) {

		var form = document.ThisForm;
		
		originalcolumnformvariable='flag_integerflagdataorg_<CFOUTPUT>#frmeventprefix#</CFOUTPUT>'+column+'Original_'
		columnformvariable='flag_integerflagdataorg_<CFOUTPUT>#frmeventprefix#</CFOUTPUT>'+column+'_'

		if (column=='OrgSalesPlaces'){
			 othercolumn='OrgPreSalesPlaces'

		} else {
			othercolumn='OrgSalesPlaces'

		}


			peoplepreferrednotregistered=parseInt(eval('form.PreferredNotRegistered_'+organisationid+'.value'))
			peopleregistered=  	parseInt(eval('form.Registered_'+organisationid+'.value'))
			thisOrgPreSalesPlaces = eval('form.flag_integerflagdataorg_<CFOUTPUT>#frmeventprefix#</CFOUTPUT>OrgPreSalesPlaces_'+organisationid+'.value')
			thisOrgSalesPlaces =eval('form.flag_integerflagdataorg_<CFOUTPUT>#frmeventprefix#</CFOUTPUT>OrgSalesPlaces_'+organisationid+'.value')			
//		alert('Debug: This Org Sales Places: '+thisOrgSalesPlaces)
//		alert('Debug: This Org PreSales Places: '+thisOrgPreSalesPlaces)
//		alert('Debug: This Org People Registered: '+peopleregistered)


	
		if (isNaN(parseFloat(allocationbox.value)) == true) {
			alert('Error: You must enter a number');
			allocationbox.value='0'
			}
		else {
			if (thisOrgSalesPlaces +thisOrgPreSalesPlaces <peoplepreferrednotregistered + peopleregistered) {
				alert('Warning: Allocation is less than Preferred People + People Registered');

				}
			else {if (thisOrgSalesPlaces +thisOrgPreSalesPlaces <peopleregistered){
				alert('Error: Allocation is less than number of people registered');
				allocationbox.value=peopleregistered-eval('this'+othercolumn)
			}


			}
			
			}

		change=0

//		This section adds up the all the changes in values to work out the current number of places left
//		These are the names of the form variables for this column, less the organisation


	
		for (j=0; j<form.elements.length; j++){

			thiselement = form.elements[j];

			if (thiselement.name.substring(0,columnformvariable.length) == columnformvariable) { 
				change=change+parseInt(thiselement.value)
			
			}
			if (thiselement.name.substring(0,originalcolumnformvariable.length) == originalcolumnformvariable) { 
				change=change-parseInt(thiselement.value)
			
			}

			
		}


		thisColumnOriginalPlacesLeft=parseInt(eval('form.frmOriginal'+column+'Available.value'))
		placesLeft=thisColumnOriginalPlacesLeft-change

//		alert('Debug: Original places left this column ' +thisColumnOriginalPlacesLeft)
//		alert('Debug: Places left now, this column ' + placesLeft)
				

		if (placesLeft >= 0){
//			if there are enough places then update value displayed on screen
			if (column == 'OrgSalesPlaces'){
			form.frmOrgSalesPlacesAvailable.value=placesLeft
			}
			if (column == 'OrgPreSalesPlaces'){
			form.frmOrgPreSalesPlacesAvailable.value=placesLeft
			}

		}

		else
		{alert ('Error: You do not have any more places left to allocate')

			if (column == 'OrgSalesPlaces'){
			allocationbox.value=peopleregistered- thisOrgPreSalesPlaces    
			}
			if (column == 'OrgPreSalesPlaces'){
			allocationbox.value=peopleregistered- thisOrgSalesPlaces    
			}
				//should reset to original value
				//ought to redo running total		
		}

	

	}


	function saveChecks() {
		// create a string of all the checked organisations
		var form = document.ThisForm;
		var checkedItems = "0";
		for (j=0; j<form.elements.length; j++){
			thiselement = form.elements[j];
			if (thiselement.name == 'frmCheckIDs') { 
				if (thiselement.checked) {
					if (checkedItems == "0") {
						checkedItems = thiselement.value;
					} else {
						checkedItems += "," + thiselement.value;
					}
				}
			}
		}
		return checkedItems;
	}
	
	function countChecks() {
		// count all the checked organisations
		var form = document.ThisForm;
		var count = 0;
		for (j=0; j<form.elements.length; j++){
			thiselement = form.elements[j];
			if (thiselement.name == 'frmCheckIDs') { 
				if (thiselement.checked) {
					count = count+1
					
				}
			}
		}
		return count
	}	
	
	
function go()	{
	
	if (document.selecter.select1.options[document.selecter.select1.selectedIndex].value != "none") 
		{
		location = document.selecter.select1.options[document.selecter.select1.selectedIndex].value
		document.selecter.select1.selectedIndex=0     //resets menu to top item
		}
}

	function openWin( windowURL,  windowName, windowFeatures ) { 

		return window.open( windowURL, windowName, windowFeatures ) ; 
	}


	function noChecks(number){
				alert('You must select atleast ' + number + ' organisations')
				document.selecter.select1.selectedIndex=0
				return
	}
	
	function doChecks (setting) {
//	checks or unchecks all checkboxes on page		
			var form = document.ThisForm;

		for (j=0; j<form.elements.length; j++){

			thiselement = form.elements[j];

			if (thiselement.name == 'frmCheckIDs') { 
				thiselement.checked = setting
			}
		}

	}


//-->

</SCRIPT>

<!--- The main screen starts here ............................ --->


</cf_head>


<!--- Drop Down Box --->
<CFOUTPUT>
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
<TR>
	<TD ALIGN="LEFT" VALIGN="TOP">
		<B><FONT FACE="Arial, Chicago, Helvetica" SIZE="+1" COLOR="Navy">
		Organisation Allocation Screen</FONT></B>
	</TD>
	<TD ALIGN="RIGHT" VALIGN="TOP">
		<form name="selecter">
			<select name="select1" size=1 onchange="go()">
			<option value=none>Dedupe functions
			<option value=none>--------------------
			<option value="JavaScript: if (countChecks()>1) {  newWindow = openWin( '..#htmleditformat(thisdir)#/orgdedupe.cfm?frmorgids='+saveChecks() ,'PopUp', 'width=450,height=450,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1' ); newWindow.focus()} else {noChecks('two')}  ">Dedupe Checked Organisations
		<!--- 	<option value="JavaScript: doChecks(true)">Check all Organisations on page --->
			<!--- <option value="JavaScript: doChecks(false)">Uncheck all Organisations on page --->
			</select>
		</form>
	</TD>
</TR>
<TR>
	<TD ALIGN="LEFT" VALIGN="TOP">
		<FONT FACE="Arial,Chicago" SIZE="1">
		Use this screen to: 
		1) Allocate places at #htmleditformat(frmeventname)# to organisations, 
		2) Deduplicate Organisations, 
		3) Rename Organisations<BR>
		<A HREF="JavaScript:newWindow = openWin( '..#thisdir#/orgAllocationHelp.cfm' ,'PopUp', 'width=450,height=450,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1' ); newWindow.focus()">Click here</A> for help
		</FONT>
	</TD>

</TR>
</TABLE>
</CFOUTPUT>

<CENTER>

<CFIF IsDefined('message')>
	
	<P><FONT FACE="Arial, Chicago, Helvetica" SIZE="2"><CFOUTPUT>#application.com.security.sanitiseHTML(message)#</CFOUTPUT></FONT>

</CFIF>

<FORM ACTION="<CFOUTPUT>#htmleditformat(thisdir)#/orgAllocationTask.cfm</CFOUTPUT>" NAME="ThisForm" METHOD="POST">

<CFOUTPUT>
	<CF_INPUT TYPE="HIDDEN" NAME="frmStart" VALUE="#Variables.displaystart#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmCurrentRegionid" VALUE="#frmregionid#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmReturnTo" VALUE="..\#thisdir#\orgallocationlist.cfm">		
	<CF_INPUT TYPE="HIDDEN" NAME="frmEventPrefix" VALUE="#frmeventprefix#">		
	<CF_INPUT TYPE="HIDDEN" NAME="frmEventName" VALUE="#frmeventname#">		
	<CF_INPUT TYPE="HIDDEN" NAME="frmEvent" VALUE="#frmEvent#">		

</CFOUTPUT>

<!--- Header --->
<P><TABLE BORDER=0 BGCOLOR="#FFFFCC" CELLPADDING="5" CELLSPACING="0" WIDTH="500">
	
	<TR><TD COLSPAN="2" ALIGN="CENTER" BGCOLOR="#000099"><FONT FACE="Arial, Chicago, Helvetica" SIZE="2" COLOR="#FFFFFF"><B><CFOUTPUT>#htmleditformat(frmEventName)#</cfoutput></B></FONT></TD></TR>


	<TR><TD ALIGN="CENTER" COLSPAN=2><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>		
			Delegate Allocation for 
				<SELECT NAME="frmRegionID" onchange="form.submit()">
				<CFOUTPUT QUERY="getUserRegions">
					<OPTION VALUE="#CountryID#" <CFIF countryid IS 	frmregionid>Selected</cfif>> #HTMLEditFormat(Region)#
				</CFOUTPUT>
			</SELECT>

		<BR></FONT></TD>
	<CFOUTPUT><TD></td>
	<CFIF frmevent IS "3ComUP">
	<TR>
		<TD ALIGN="CENTER" COLSPAN=1><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
			Total Sales Places allocated to Region
		<BR></FONT></TD>
		<TD ALIGN="CENTER" COLSPAN=1><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
			#htmleditformat(getRegionStats1.salesplaces)# <BR>
		<BR></FONT></TD>

	</TR>

	<TR>
		<TD ALIGN="CENTER" COLSPAN=1><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
			Total Pre Sales Places allocated to Region
		<BR></FONT></TD>
		<TD ALIGN="CENTER" COLSPAN=1><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
			#htmleditformat(getRegionStats1.presalesplaces)#
		<BR></FONT></TD>
	</TR>

	<TR><TD ALIGN="CENTER" COLSPAN=1><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
			Total Sales Places allocated to individual organisations
		<BR></FONT></TD>
		<TD ALIGN="CENTER" COLSPAN=1><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
			#htmleditformat(VAL(getRegionStats1.salesplacesallocated))#
			
		<BR></FONT></TD>
	</TR>
	<TR><TD ALIGN="CENTER" COLSPAN=1><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
			Total Pre Sales Places allocated to individual organisations
		<BR></FONT></TD>
		<TD ALIGN="CENTER" COLSPAN=1><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
			#htmleditformat(val(getRegionStats1.presalesplacesallocated))#
		<BR></FONT></TD>
	</TR>

	<TR><TD ALIGN="CENTER" COLSPAN=1><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
			Partner Registrations Submitted
		<BR></FONT></TD>
		<TD ALIGN="CENTER" COLSPAN=1><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
			#htmleditformat(getRegionStats3.partnersubmitted)#
		<BR></FONT></TD>
	</TR>
	<TR><TD ALIGN="CENTER" COLSPAN=1><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
			Preferred Delegates
		<BR></FONT></TD>
		<TD ALIGN="CENTER" COLSPAN=1><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
			#htmleditformat(getRegionStats2.PreferredNotRegistered)#
		<BR></FONT></TD>
	</TR>
	</CFIF>

	<CFIF frmevent IS "3ComUInternal">	
	<TR><TD ALIGN="CENTER" COLSPAN=1><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
			3Com Registrations Submitted
		<BR></FONT></TD>
		<TD ALIGN="CENTER" COLSPAN=1><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
			#htmleditformat(getRegionStats3.com3submitted)#
		<BR></FONT></TD>
	</TR>
	</CFIF>
	
	<TR><TD ALIGN="CENTER" COLSPAN=1><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
			Registrations Confirmed
		<BR></FONT></TD>
		<TD ALIGN="CENTER" COLSPAN=1><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
			#htmleditformat(getRegionStats2.Registered)#
		<BR></FONT></TD>
	</TR>

	<CFIF frmevent IS "3ComUP">
	<CFSET Salesplaceslefttoallocate=VAL(getRegionStats1.SalesPlaces) - VAL(getRegionStats1.SalesPlacesAllocated)>
	<CFSET PreSalesplaceslefttoallocate=VAL(getRegionStats1.PreSalesPlaces) - VAL(getRegionStats1.PreSalesPlacesAllocated)>
	<TR><TD ALIGN="CENTER" COLSPAN=1><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
			Sales Places left to allocate to organisations
		<BR></FONT></TD>
		<TD ALIGN="CENTER" COLSPAN=1><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
			<FONT FACE="Arial,Chicago" SIZE="2"><CF_INPUT TYPE="Text" NAME="frmOrgSalesPlacesAvailable"  VALUE="#Salesplaceslefttoallocate#" DISABLED SIZE="2"	></FONT>
			
		<BR></FONT></TD>
	</TR>

		<TR><TD ALIGN="CENTER" COLSPAN=1><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
			Pre Sales Places left to allocate to organisations
		<BR></FONT></TD>
		<TD ALIGN="CENTER" COLSPAN=1><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
			
			<FONT FACE="Arial,Chicago" SIZE="2"><CF_INPUT TYPE="Text" NAME="frmOrgPreSalesPlacesAvailable"  VALUE="#PreSalesplaceslefttoallocate#" DISABLED SIZE="2"	></FONT>

			
		<BR></FONT></TD>
	</TR>

	
		<CF_INPUT TYPE="HIDDEN" NAME="frmTotalOrgSalesPlaces" 				VALUE="#val(getRegionStats1.salesplaces)#">			
		<CF_INPUT TYPE="HIDDEN" NAME="frmTotalOrgPreSalesPlaces" 				VALUE="#val(getRegionStats1.presalesplaces)#">			
		<CF_INPUT TYPE="HIDDEN" NAME="frmOriginalOrgSalesPlacesAllocated" 	VALUE="#val(getRegionStats1.salesplacesallocated)#">			
		<CF_INPUT TYPE="HIDDEN" NAME="frmOriginalOrgPreSalesPlacesAllocated" 	VALUE="#val(getRegionStats1.presalesplacesallocated)#">			
		<CF_INPUT TYPE="HIDDEN" NAME="frmOriginalOrgSalesPlacesAvailable" 	VALUE="#Salesplaceslefttoallocate#">			
		<CF_INPUT TYPE="HIDDEN" NAME="frmOriginalOrgPreSalesPlacesAvailable" 	VALUE="#preSalesplaceslefttoallocate#">				
		</CFIF>

</TABLE>
</CFOUTPUT>		
<!--- End of Header --->
	

<CFIF getOrganisationStats.Recordcount IS 0>
	
	<P><FONT FACE="Arial, Chicago, Helvetica" SIZE="2">There are no Organisations matching you country.</FONT>

<CFELSE>

<CENTER>
<!--- Next and Previous Buttons --->
<TABLE BORDER=0>
	
		<TR><TD><FONT FACE="Arial, Helvetica" SIZE=2>
				<CFIF Variables.displaystart GT 1>
					<INPUT TYPE="Image" NAME="ListBack" SRC="<CFOUTPUT></CFOUTPUT>/images/buttons/c_prev_e.gif" BORDER=0 ALT="Save and Previous">
				</CFIF>
				</FONT></TD>
		<TD>		<INPUT TYPE="Image" NAME="ListSave" SRC="<CFOUTPUT></CFOUTPUT>/images/buttons/c_save_e.gif" BORDER=0 ALT="Save this screen"></td>
			<TD><FONT FACE="Arial, Helvetica" SIZE=2>
				<CFIF (Variables.displaystart + application.showRecords - 1) LT getOrganisationStats.RecordCount>
					<INPUT TYPE="Image" NAME="ListForward" SRC="<CFOUTPUT></CFOUTPUT>/images/buttons/c_next_e.gif" WIDTH=64 HEIGHT=21 BORDER=0 ALT="Save and Next">
				</cFIF>
				</FONT></TD>
		</TR>
		<TR>
			<TD align="center" colspan="3"><FONT FACE="Arial, Helvetica" SIZE=2>
	 				<CFIF (Variables.displaystart + application.showRecords - 1) LT getOrganisationStats.RecordCount>
						<CFOUTPUT>Record(s) #htmleditformat(Variables.displaystart)# - #Evaluate("Variables.displaystart + application.showRecords - 1")#</CFOUTPUT>
					<CFELSE>
	 					<CFOUTPUT>Record(s) #htmleditformat(Variables.displaystart)# - #getOrganisationStats.RecordCount#</cFOUTPUT>
					</CFIF>
				</FONT></TD>
		</tr>
		</TABLE>
	
</CENTER>

<CENTER>
<!--- Output Column Titles --->
	<P><TABLE BORDER="0" CELLPADDING="5" CELLSPACING="0" WIDTH="90%">
	<TR><TD ALIGN="CENTER" VALIGN="BOTTOM"><FONT FACE="Arial,Chicago" SIZE="2"><B>Check</B></FONT></TD>
		<TD ALIGN="CENTER" VALIGN="BOTTOM"><FONT FACE="Arial,Chicago" SIZE="2"><B>Organisation</B></FONT></TD>


	<CFLOOP index="I" from="1"  to="#listlen(columndisplayas)#">
	<CFOUTPUT>
		<CFIF listgetat(columndisplay,I) IS 1> 
		<TD ALIGN="CENTER" VALIGN="BOTTOM"><FONT FACE="Arial,Chicago" SIZE="2"><B>#listgetat(columnTitle,I)#</B></FONT></TD>
		</CFIF>

	</CFOUTPUT>
	</CFLOOP>

	</TR>
<!--- End of Column Titles --->	

<!--- Loop through data --->	
	<CFSET orgsonpage="">
	<CFSET endrow=Min(Variables.displaystart + application.showRecords, getorganisationstats.recordcount)>
	<CFLOOP QUERY="getOrganisationStats" STARTROW="#Variables.displaystart#" EndRow="#endrow#">

	
		<CFOUTPUT>	
		<TR><TD ALIGN="CENTER" ><FONT FACE="Arial,Chicago" SIZE="2"><CF_INPUT TYPE="checkbox" NAME="frmCheckIDs" VALUE="#OrganisationID#"></FONT></TD>
			<TD ALIGN="CENTER" ><FONT FACE="Arial,Chicago" SIZE="2"><CF_INPUT TYPE="Text" NAME="frmOrgName_#organisationid#"  VALUE="#OrganisationName#" onchange="checktextbox(this);" SIZE="25"></FONT></TD>	
				<CF_INPUT TYPE="HIDDEN" NAME="frmOrgNameOriginal_#organisationid#" VALUE="#OrganisationName#">			
		</CFOUTPUT>

	<!--- loop through dynamic columns --->
	<CFLOOP index="I" from="1"  to="#listlen(columndisplayas)#">

	  <CFIF listgetat(columndisplay,I) IS 1> 
		<CFOUTPUT>	

		<CFIF trim(listgetat(columndisplayas,I)) IS "Update"> 
				<TD ALIGN="CENTER" ><FONT FACE="Arial,Chicago" SIZE="2"><CF_INPUT TYPE="Text" NAME="#listgetat(columnsourcetype,I)#_#listgetat(columnsourcetable,I)#_#frmeventprefix##listgetat(columnsource,I)#_#organisationid#" onchange="docalcs(this,'#frmeventprefix##listgetat(columnsource,I)#',#organisationid#,'#listgetat(columnalias,I)#');" VALUE="#val(evaluate(listgetat(columnalias,I)))#" SIZE="3"></FONT></TD>	
				<!---  eg:																			flag         _   integerflagdataorg			_ U99orgallocated			_213												--->
					<CF_INPUT TYPE="HIDDEN" NAME="#listgetat(columnsourcetype,I)#_#listgetat(columnsourcetable,I)#_#frmeventprefix##listgetat(columnsource,I)#Original_#organisationid#" VALUE="#val(evaluate(listgetat(columnalias,I)))#">			

		<CFELSEIF trim(listgetat(columndisplayas,I)) IS "Value"> 
			<TD ALIGN="CENTER" ><FONT FACE="Arial,Chicago" SIZE="2">#val(evaluate(listgetat(columnalias,I)))#</FONT></TD>

		<CFELSEIF trim(listgetat(columndisplayas,I)) IS "ValuewithVariable"> 
			<TD ALIGN="CENTER" ><FONT FACE="Arial,Chicago" SIZE="2">#evaluate(listgetat(columnalias,I))#</FONT></TD>
				<CF_INPUT TYPE="HIDDEN" NAME="#listgetat(columnalias,I)#_#organisationid#" VALUE="#val(evaluate(listgetat(columnalias,I)))#">			

		</CFIF>

		</CFOUTPUT>
 	  </CFIF>
	</CFLOOP>
	<!--- end of dynamc columns --->


			<CFOUTPUT><TD ALIGN="CENTER" ><FONT FACE="Arial,Chicago" SIZE="2"><A HREF="JavaScript:viewPeople('#Organisationid#');">Details</a></FONT></TD></CFOUTPUT>
																																											 
		</TR>

	<CFSET orgsonpage=listappend(orgsonpage,organisationid)>
			
	</CFLOOP>
	</TABLE>


	<!--- get list of updating flags on page	 --->
	<CFSET FlagsOnPage="">
	<CFLOOP index="I" from="1"  to="#listlen(columnsource)#">
		<CFIF listgetat(columndisplayas,I) IS "Update"> 
			<CFSET flagsonPage=listappend(flagsonpage,listgetat(columnsourcetype,I)&"_"&listgetat(columnsourcetable,I)&"_"&frmeventprefix&listgetat(columnsource,I))>
		</CFIF>
	</CFLOOP>

	<cfoutput>
	<CF_INPUT TYPE="HIDDEN" NAME="frmOrgsOnpage" VALUE="#orgsonpage#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmOrgFlagsOnpage" VALUE="#flagsonpage#">	
	</cfoutput>
	
	<TABLE BORDER=0>
<!--- Previous and next buttons --->	
		<TR><TD><FONT FACE="Arial, Helvetica" SIZE=2>
				<CFIF Variables.displaystart GT 1>
					<INPUT TYPE="Image" NAME="ListBack" SRC="<CFOUTPUT></CFOUTPUT>/images/buttons/c_prev_e.gif" BORDER=0 ALT="Save and Previous">
				</CFIF>
				</FONT></TD>
		<TD>		<INPUT TYPE="Image" NAME="ListSave" SRC="<CFOUTPUT></CFOUTPUT>/images/buttons/c_save_e.gif" BORDER=0 ALT="Save this screen"></td>
			<TD><FONT FACE="Arial, Helvetica" SIZE=2>
				<CFIF (Variables.displaystart + application.showRecords - 1) LT getOrganisationStats.RecordCount>
					<INPUT TYPE="Image" NAME="ListForward" SRC="<CFOUTPUT></CFOUTPUT>/images/buttons/c_next_e.gif" WIDTH=64 HEIGHT=21 BORDER=0 ALT="Save and Next">
				</cFIF>
				</FONT></TD>
		</TR>
		<TR>
			<TD align="center" colspan="3"><FONT FACE="Arial, Helvetica" SIZE=2>
	 				<CFIF (Variables.displaystart + application.showRecords - 1) LT getOrganisationStats.RecordCount>
						<CFOUTPUT>Record(s) #htmleditformat(Variables.displaystart)# - #Evaluate("Variables.displaystart + application.showRecords - 1")#</CFOUTPUT>
					<CFELSE>
	 					<CFOUTPUT>Record(s) #htmleditformat(Variables.displaystart)# - #getOrganisationStats.RecordCount#</cFOUTPUT>
					</CFIF>
					<CFOUTPUT>of #getOrganisationStats.Recordcount#</CFOUTPUT>
				</FONT></TD>
		</tr>

	<!--- Goto first letter  --->
	<cfif listlen(orgsonpage) LE getOrganisationStats.Recordcount>
		<TR>
			<TD align="center" colspan="3"><FONT FACE="Arial, Helvetica" SIZE=2>

				<SELECT NAME="frmStartLetter" onchange="form.submit()">

					<OPTION VALUE="" > Go to First Letter
					<CFLOOP Index="I" FROM="1" TO="26">
						<CFOUTPUT><OPTION VALUE="#chr(I+64)#" > #chr(I+64)#</CFOUTPUT>
					</CFLOOP>

				</SELECT>
				</FONT></TD>
		</tr>
	</CFIF>


		</TABLE>
	
	

</CENTER>	
	</FORM>


</CFIF>

<!--- <FONT FACE="Arial, Helvetica" SIZE=2>Note:  Preferred figure above does not include those Preferred Delegates who have now registered</FONT> --->
</CENTER>
</FORM>
<CFOUTPUT>
<A HREF="orgallocationlist.cfm?frmregionid=#frmregionid#&frmdisplaymode=download&frmevent=#frmevent#">Click here to Download this report</a> <BR>
<A HREF="orgallocationlist.cfm?frmregionid=all&frmdisplaymode=download&frmevent=#frmevent#">Click here to Download this report for all regions</a>
</cfoutput>



<!--- <CFQUERY NAME="droptable" datasource="#application.sitedatasource#" DEBUG >
	drop table #temporarytable#
</cfquery>    --->

<CFELSEIF frmdisplaymode IS "download">

<!--- download table to user--->	

<CFSET delim = "#Chr(9)#">

<!--- save as file --->


<CFSET fullname = "e:\ftphome\3contact\ftp\"&request.relayCurrentUser.personid&"-tempdownload.txt">

<!--- Create a header row --->
<CFSET header="">

	<CFLOOP index="I" from="1"  to="#listlen(columndownload)#">
		<CFIF listgetat(columndownload,I) IS 1> 
			<CFSET Header=Header & replace(listgetat(columnTitle,I),"<BR>"," ","ALL") & delim>
		</CFIF>
	</cfloop>

		<CFFILE ACTION="WRITE"
			FILE="#fullname#"
			OUTPUT="#header#">

	
<CFLOOP QUERY="getOrganisationStats">
	<CFSET row="">	
	<CFLOOP index="I" from="1"  to="#listlen(columndownload)#">
		<CFIF listgetat(columndownload,I) IS 1> 
			<CFSET row=row & evaluate(listgetat(columnalias,I)) & delim>
		</CFIF>
	</cfloop>


	<!--- append data to file --->
	<CFFILE ACTION="APPEND"
			FILE="#fullname#"
			OUTPUT="#row#">
</CFLOOP>	
			

<CFCONTENT TYPE="application/x-unknown"
           FILE="#fullname#"		  
           DELETEFILE="No">
		   

</CFIF>		   




