<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
2010-12-07	MS	LID4737: Fixing title of the report to say last month instead of last week
2012-10-04	WAB		Case 430963 Remove Excel Header 
 --->
<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">
<cfparam name="openAsExcel" type="boolean" default="false">

	<cf_head>
		<cf_title>Event Management</cf_title>
	</cf_head>
	
	
	<cfinclude template="eventSubTopHead.cfm">

<cfparam name="sortOrder" default="company">
<cfparam name="numRowsPerPage" default="50">
<cfparam name="startRow" default="1">
<cfparam name="dateFormat" type="string" default="registered,event_date">

<cfparam name="keyColumnList" type="string" default="company"><!--- this can contain a list of columns that can be edited --->
<cfparam name="keyColumnURLList" type="string" default="../data/dataframe.cfm?frmsrchOrgID="><!--- this can contain a list of matching columns that contain the URL of the editor template --->
<cfparam name="keyColumnKeyList" type="string" default="OrgID"><!--- this can contain a list of matching key columns e.g. primary key --->

<cfparam name="checkBoxFilterName" type="string" default="">
<cfparam name="checkBoxFilterLabel" type="string" default="">

<cfquery name="getEventReg" datasource="#application.SiteDataSource#">
	select Name, Company, Registered, Event_Group, Event, Event_Date, OrgID 
	from vEventRegPastWeek
	WHERE 1=1 and Registered>  getdate()-30
	<cfinclude template="../templates/tableFromQuery-QueryInclude.cfm">
	order by <cf_queryObjectName value="#sortOrder#"> 
</cfquery>

<cfoutput>
<table width="100%" cellpadding="3">
	<tr>
		<!--- 2010-12-07	MS		LID4737: Title fixed --->
		<td><strong>Registration for events within the last month.</strong></td>
		<td align="right">&nbsp;</td>
	</tr>
	<tr><td colspan="2">&nbsp;</td></tr>
</table>
</cfoutput>

<cf_translate>
<CF_tableFromQueryObject 
	queryObject="#getEventReg#"
	queryName="getEventReg"
	sortOrder = "#sortOrder#"
	openAsExcel = "#openAsExcel#"
	numRowsPerPage="#numRowsPerPage#"
	startRow="#startRow#"
	
	keyColumnList="#keyColumnList#"
	keyColumnURLList="#keyColumnURLList#"
	keyColumnKeyList="#keyColumnKeyList#"
	
 	hideTheseColumns="OrgID"
	ShowTheseColumns="Name,Company,Registered,Event_Group,Event,Event_Date"
	
	dateFormat="#dateFormat#"
	FilterSelectFieldList="Name,Company,Event_Group,Event"
	FilterSelectFieldList2="Name,Company,Event_Group,Event"
	GroupByColumns=""
	radioFilterLabel=""
	radioFilterDefault=""
	radioFilterName=""
	radioFilterValues=""
	checkBoxFilterName=""
	checkBoxFilterLabel=""
	allowColumnSorting="yes"
>
</cf_translate>


