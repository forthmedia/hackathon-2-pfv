<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			ReportEventsAll.cfm	
Author:				AJC
Date started:		2006-05-02
	
Description:		Report to show event, team and number of members in team

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2012-10-04	WAB		Case 430963 Remove Excel Header 

Still To Do
===========

Possible enhancements:

 --->

<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">
<cfparam name="openAsExcel" type="boolean" default="false">

	<cf_head>
		<cf_title>Event Management</cf_title>
	</cf_head>
	
	
	<cfinclude template="eventSubTopHead.cfm">


<cfparam name="sortOrder" default="Event, Team_Name, Name">
<cfparam name="numRowsPerPage" default="500">
<cfparam name="startRow" default="1">
<cfparam name="dateFormat" type="string" default="">

<cfparam name="keyColumnList" type="string" default="Name"><!--- this can contain a list of columns that can be edited --->
<cfparam name="keyColumnURLList" type="string" default="/data/dataframe.cfm?frmsrchPersonID="><!--- this can contain a list of matching columns that contain the URL of the editor template --->
<cfparam name="keyColumnKeyList" type="string" default="PersonID"><!--- this can contain a list of matching key columns e.g. primary key --->

<cfparam name="checkBoxFilterName" type="string" default="">
<cfparam name="checkBoxFilterLabel" type="string" default="">

<cfinclude template="reportEventsTeamDetailedFunctions.cfm">

<CFQUERY NAME="GetTeamMembers" DATASOURCE="#Application.SiteDataSource#">
		select Event, Team_Name, Name, PersonID, Event_Status
		from vEventTeamsDetailed
		where 1=1
		<cfinclude template="../templates/tableFromQuery-QueryInclude.cfm">
		ORDER BY <cf_queryObjectName value="#sortOrder#">
</CFQUERY>

<cfoutput>
<table width="100%" cellpadding="3">
	<tr>
		<td><strong>Event Teams - Detailed</strong></td>
		<td align="right">&nbsp;</td>
	</tr>
	<tr><td colspan="2">&nbsp;</td></tr>
</table>
</cfoutput>

<cf_translate>
			
<CF_tableFromQueryObject 
	queryObject="#GetTeamMembers#"
	queryName="GetTeamMembers"
	sortOrder = "#sortOrder#"
	openAsExcel = "#openAsExcel#"
	numRowsPerPage="#numRowsPerPage#"
	startRow="#startRow#"
	
	keyColumnList="#keyColumnList#"
	keyColumnURLList="#keyColumnURLList#"
	keyColumnKeyList="#keyColumnKeyList#"
	keyColumnOpenInWindowList="yes"
	
 	hideTheseColumns=""
	ShowTheseColumns="Event, Team_Name, Name, PersonID, Event_Status"
	
	dateFormat=""
	FilterSelectFieldList="Event,Team_Name,Event_Status"
	FilterSelectFieldList2="Event,Team_Name,Event_Status"
	GroupByColumns=""
	radioFilterLabel=""
	radioFilterDefault=""
	radioFilterName=""
	radioFilterValues=""
	checkBoxFilterName=""
	checkBoxFilterLabel=""
	allowColumnSorting="no"
	
	rowIdentityColumnName="PersonId"
	functionListQuery="#comTableFunction.qFunctionList#"
>


</cf_translate>



