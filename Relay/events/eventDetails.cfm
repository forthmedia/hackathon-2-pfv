<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File:			EventDetails.cfm
Author:			DJH
Date created:	27 January 2000

Description:
Allows a user to update a particular event given the appropriate rights.

Version history:
1

20.10.04	ICS	- added sort order for events
2007/10/08	WAB added htmleditformat to title editing
2008/09/25  NJH  elearning 8.1 - added moduleID's to an event, if the module is a classroom or webinar module.
2009/02/11	NJH	P-SNY047 - changed form to cfform as ugRecordManager now uses cfselect
2009/03/23	NJH Bug Fix All Sites Issue 1988- added a distinct to the module query..
2012/03/20	IH	Case 424420 prevent backspace on date fields to go back to previous page on IE, add datepicker to date fields
2012/10/25	NJH	social CR - add ability to turn sharing off for a particular event.
2012-12-01	IH	Case 432352 add JavaScript maxLength checking on frmVenueRoom as maxLength on text areas is not supported by IE and Opera
2014-10-29	RPW	CORE-803 Hide Register and Unregister buttons not working under Events module
2015-07-06  DAN 445094 - fix for new events not showing on the Activity Stream
2015-11-24  ACPK    Removed typo from eventDetailsTYable1 section
2015-12/07	SB	Bootstrap
2016-01-29	WAB Removed call to Sel All Function
--->
<cf_title>Event Details</cf_title>

<!--- 2006-05-02 AJC CHW001 Default for hiding fields --->
<cfparam name="EventHideFormFields" default="">
<cfparam name="frmCampaignID" default="">
<cfset freezedate=request.requestTime>
<CFIF NOT IsDefined("FlagID")>
	<CFlocation url="eventHome.cfm" addToken="false">
	<CF_ABORT>
<CFELSEIF NOT IsNumeric(FlagID)>
	<CFlocation url="eventHome.cfm" addToken="false">
	<CF_ABORT>
</CFIF>

<cfinclude template="sql_getPartnerTypes.cfm">

<cfinclude template="eventTopHead.cfm">

<!--- need to dynamically get the name of the event flag data table as this might change --->
<CFQUERY NAME="GetEventTable" datasource="#application.siteDataSource#">
	SELECT
		ft.DataTable,
		f.FlagTextID
	FROM
		Flag AS f,
		FlagGroup AS fg,
		FlagType AS ft
	WHERE
		f.FlagGroupID = fg.FlagGroupID
		AND fg.FlagTypeID = ft.FlagTypeID
		AND f.FlagID =  <cf_queryparam value="#FlagID#" CFSQLTYPE="CF_SQL_INTEGER" >
</CFQUERY>
<CFSET EventTable = "#GetEventTable.DataTable#FlagData">

<!--- if the form has been submitted then update the event details and countries--->
<cfif isdefined("form.frmLocation")>
	<!--- only the event owner can update event details --->
	<CFIF IsOwner and form.dirty is "1">

		<CFQUERY NAME="updateEvent" datasource="#application.siteDataSource#">
			UPDATE
				EventDetail
			SET
				Title =  <cf_queryparam value="#frmTitle#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
				Location =  <cf_queryparam value="#frmLocation#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
				VenueRoom =  <cf_queryparam value="#frmVenueRoom#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
				CountryID =  <cf_queryparam value="#frmCountryID#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
				EstAttendees =  <cf_queryparam value="#frmEstAttendees#" CFSQLTYPE="CF_SQL_Integer" > ,
				PreferredStartDate =  <cf_queryparam value="#CreateODBCDate(frmPreferredStartDate)#" CFSQLTYPE="CF_SQL_TIMESTAMP" > ,
				PreferredEndDate =  <cf_queryparam value="#CreateODBCDate(frmPreferredEndDate)#" CFSQLTYPE="CF_SQL_TIMESTAMP" > ,
				Email =  <cf_queryparam value="#frmEmail#" CFSQLTYPE="CF_SQL_VARCHAR" > ,

				<!---EmailCC = '#frmEmailCC#',--->
				UseAgency =  <cf_queryparam value="#frmUseAgency#" CFSQLTYPE="CF_SQL_bit" > ,
				InvitationType =  <cf_queryparam value="#frmInvitationType#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
				AudienceType =  <cf_queryparam value="#frmAudienceType#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
				<cfif isdefined("form.frmPartnerType")>
					PartnerType =  <cf_queryparam value="#frmPartnerType#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
				</cfif>
				<!---Agenda = '#frmAgenda#',--->
				EventStatus =  <cf_queryparam value="#frmEventStatus#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
				LastUpdatedBy = #request.relayCurrentUser.personid#,
				LastUpdated =  <cf_queryparam value="#CreateODBCDate(FreezeDate)#" CFSQLTYPE="CF_SQL_TIMESTAMP" > ,
				venuerating =  <cf_queryparam value="#frmVenueRating#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
				typeofrefreshment =  <cf_queryparam value="#frmTypeofrefreshment#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
				ScantronShippingAddress =  <cf_queryparam value="#frmScantronShippingAddress#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
				ScantronTel =  <cf_queryparam value="#frmScantronTel#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
				ScantronContact =  <cf_queryparam value="#frmScantronContact#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
				RegistrationScreenID =  <cf_queryparam value="#frmRegistrationScreenID#" CFSQLTYPE="CF_SQL_INTEGER" > ,
				<cfif frmModuleID neq "">
				moduleID =  <cf_queryparam value="#frmModuleID#" CFSQLTYPE="CF_SQL_INTEGER" > ,
				</cfif>
				<cfif application.com.settings.getSetting("socialMedia.enableSocialMedia")>share = <cf_queryparam value="#structKeyExists(form,'frmEventShare')#" CFSQLTYPE="CF_SQL_BIT" >,</cfif>
				SortOrder =  <cf_queryparam value="#frmSortOrder#" CFSQLTYPE="CF_SQL_INTEGER" > ,
				MaxTeamSize =  <cf_queryparam value="#frmMaxTeamSize#" CFSQLTYPE="CF_SQL_Integer" >,
				<!--- START: 2013-07-31	MS	P-KAS026 Allow the users to set the visibility of Register or Unregister Buttons --->
				hideRegisterForEventBtn = <cfif structKeyExists(form,"frmEventHideRegBtn")><cf_queryparam value="#form.frmEventHideRegBtn#" CFSQLTYPE="CF_SQL_INTEGER" ><cfelse><cf_queryparam value="0" CFSQLTYPE="CF_SQL_INTEGER" ></cfif>,
				hideUnRegisterForEventBtn = <cfif structKeyExists(form,"frmEventHideUnRegBtn")><cf_queryparam value="#form.frmEventHideUnRegBtn#" CFSQLTYPE="CF_SQL_INTEGER" ><cfelse><cf_queryparam value="0" CFSQLTYPE="CF_SQL_INTEGER" ></cfif>,
				campaignID = <cf_queryparam value="#frmCampaignID#" CFSQLTYPE="CF_SQL_INTEGER" null="#iif(isNumeric(frmCampaignID),de('no'),de('yes'))#">
				<!--- END  : 2013-07-31	MS	P-KAS026 Allow the users to set the visibility of Register or Unregister Buttons --->
			WHERE
				FlagID =  <cf_queryparam value="#form.FlagID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</CFQUERY>

		<!--- 2006-05-10 --->
		<CF_UGRecordManagerTask>

		<cf_eventNotificationEmail action="compile" changes="Basic event details">

		<CFIF IsDefined("form.addCountry")>
			<CFLOOP LIST="#addCountry#" INDEX="theCountry">
				<CFQUERY NAME="addCountries" datasource="#application.siteDataSource#">
					INSERT INTO EventCountry
					(
						FlagID,
						CountryID,
						LastUpdated,
						LastUpdatedBy
					)
					VALUES
					(
						<cf_queryparam value="#form.FlagID#" CFSQLTYPE="CF_SQL_INTEGER" >,
						<cf_queryparam value="#theCountry#" CFSQLTYPE="CF_SQL_INTEGER" >,
						<cf_queryparam value="#CreateODBCDate(FreezeDate)#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
						<cf_queryparam value="#request.relayCurrentUser.personid#" CFSQLTYPE="cf_sql_integer" >)
				</CFQUERY>
			</CFLOOP>
			<cf_eventNotificationEmail action="compile" changes="Eligible countries added">
		</CFIF>

		<CFIF IsDefined("form.DeleteCountry")>
			<CFQUERY NAME="deleteCountries" datasource="#application.siteDataSource#">
				DELETE FROM
					EventCountry
				WHERE
					CountryID  IN ( <cf_queryparam value="#form.deleteCountry#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
					AND FlagID =  <cf_queryparam value="#form.FlagID#" CFSQLTYPE="CF_SQL_INTEGER" >
			</CFQUERY>
			<cf_eventNotificationEmail action="compile" changes="Eligible countries deleted">

		</CFIF>


		<!--- update the allocated spaces for each non-removable country
			NJH 2013/12/20 - Case 438083 - check if field exists, as we now disable any of these fields if they are empty
		 --->
		<CFLOOP LIST="#SpacesList#" INDEX="theIndex">
			<CFIF (structKeyExists(form,theIndex) and IsNumeric(Evaluate(theIndex))) or  not structKeyExists(form,theIndex)>
				<CFQUERY NAME="updateEvent" datasource="#application.siteDataSource#">
					UPDATE
						EventCountry
					SET
						AllocatedSpaces#ListFirst(theIndex, "x")# = <cfif structKeyExists(form,theIndex)>#Evaluate(theIndex)#<cfelse>null</cfif>,
						LastUpdatedBy = #request.relayCurrentUser.personid#,
						LastUpdated =  <cf_queryparam value="#CreateODBCDate(FreezeDate)#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
					WHERE
						FlagID =  <cf_queryparam value="#form.FlagID#" CFSQLTYPE="CF_SQL_INTEGER" >
						AND CountryID =  <cf_queryparam value="#ListLast(theIndex, "x")#" CFSQLTYPE="CF_SQL_INTEGER" >
				</CFQUERY>
			</CFIF>
		</CFLOOP>
		<cf_eventNotificationEmail action="send" FlagID="#form.FlagID#">

		<!---
			NJH 2012/01/17 - Social Media - add an activity when adding an event becomes agreed and available to the partners
			NJH 2012/10/23 - Social CR - check if event is shareable
		--->

		<!--- /* 2013-03-08 - RMB - 434185 - Added structKeyExists(form,'frmEventShare') */ --->
        <cfset requestDate = LSDateFormat(request.requestTime, "yyyy-mm-dd")> <!--- 2015-07-06 DAN 445094 - format the request time to just date for consistent comparison with event start date --->
		<cfif form.frmEventStatus eq "agreed" and form.frmEventStatus_orig neq "agreed" and application.com.settings.getSetting("socialMedia.enableSocialMedia")
              and application.com.settings.getSetting("socialMedia.share.eventDetail.added") AND (structKeyExists(form,'frmEventShare') AND form.frmEventShare)
              <!--- and dateDiff("h",request.requestTime,frmPreferredStartDate) gt 0> --->
              and dateDiff("d", requestDate, frmPreferredStartDate) GTE 0> <!--- 2015-07-06 DAN 445094 - fix for dateDiff so we can also have events that are starting on the same day --->
			<cfset shareMergeStruct = {event=frmTitle}>
			<cfset application.com.service.addRelayActivity(action="added",performedOnEntityID=form.FlagID,performedOnEntityTypeID=application.entityTypeID["eventDetail"],mergeStruct=shareMergeStruct,performedByEntityID=application.com.settings.getSetting("theClient.clientOrganisationID"),performedByEntityTypeID=2,url="")>
		</cfif>

	<cfelse>
		<CF_UGRecordManagerTask>
	</CFIF>


	<cfif session.eventWizard is "true">
		<cfif url.eventstep is 2>
			<cflocation url="eventManager.cfm?RecordID=#FlagID#&Entity=Flag&ShortName=EventTask&FlagID=#FlagID#" addToken="false">
		</cfif>
	</cfif>
</CFIF>

<!--- get the event details --->
<cfinclude template="sql_GetEventDetails.cfm">

<cfparam name="attributes.dateType" default="#request.relaycurrentuser.localeInfo.dateType#">
<cfset dateTypeInfo = application.com.dateFunctions.getDateTypeFormattingInfo(attributes.dateType)>

<!--- get the countries the current user has rights to view
returns a list of CountryIDs as a variable called Variables.CountryList --->
<CFINCLUDE TEMPLATE="../templates/qrygetcountries.cfm">
<!---
<CFSET CountriesAssignedList = 0>
--->
<!--- get the countries that can be removed, ie there are no delegates
from that country assoicated with this event --->
<!---<CFQUERY NAME="countriesAssignedRemovable" datasource="#application.siteDataSource#">
	SELECT
		c.CountryDescription,
		c.CountryID
	FROM
		EventCountry AS ec
		INNER JOIN Country AS c ON ec.CountryID = c.CountryID
	WHERE
		ec.FlagID = #FlagID#
		AND c.CountryID NOT IN (SELECT CountryID FROM #EventTable# AS et WHERE et.FlagID = #FlagID#)
	ORDER BY
		c.CountryDescription
</CFQUERY>

<CFIF countriesAssignedRemovable.RecordCount GT 0>
	<CFSET CountriesAssignedList = ValueList(countriesAssignedRemovable.CountryID)>
</CFIF>--->

<!--- get the countries that cannot be removed, ie there are delegates
from that country assoicated with this event --->
<CFQUERY NAME="countriesAssigned" datasource="#application.siteDataSource#">
SELECT
	c.CountryDescription,
	c.CountryID,
	ec.AllocatedSpacesA,
	ec.AllocatedSpacesB,
	ec.AllocatedSpacesC,
	count(efd.flagid) as DelegatesAssigned
FROM
	EventCountry AS ec
	INNER JOIN Country AS c ON ec.CountryID = c.CountryID
	left join eventflagdata as efd on efd.countryid = ec.countryid
	and efd.flagid = ec.flagid
WHERE
	ec.FlagID =  <cf_queryparam value="#FlagID#" CFSQLTYPE="CF_SQL_INTEGER" >
group by
	c.CountryDescription,
	c.CountryID,
	ec.AllocatedSpacesA,
	ec.AllocatedSpacesB,
	ec.AllocatedSpacesC
ORDER BY
	c.CountryDescription
</CFQUERY>


<!--- get the countries that cannot be removed, ie there are delegates
from that country assoicated with this event --->

<!---

<CFQUERY NAME="countriesAssignedNonRemovable" datasource="#application.siteDataSource#">
	SELECT
		c.CountryDescription,
		c.CountryID,
		ec.AllocatedSpacesA,
		ec.AllocatedSpacesB,
		ec.AllocatedSpacesC
	FROM
		EventCountry AS ec
		INNER JOIN Country AS c ON ec.CountryID = c.CountryID
	WHERE
		ec.FlagID = #FlagID#
		and
		AND c.CountryID NOT IN (#CountriesAssignedList#)
	ORDER BY
		c.CountryDescription
</CFQUERY>

--->

<!---<CFIF countriesAssignedNonRemovable.RecordCount GT 0>--->
	<CFSET CountriesAssignedList = ValueList(countriesAssigned.CountryID)>
<!---</CFIF>--->

<!--- get the countries that user has rights to view and are not already assigned to this event --->
<CFQUERY NAME="countriesAvailable" datasource="#application.siteDataSource#">
	SELECT
		CountryDescription,
		CountryID
	FROM
		Country
	WHERE
		CountryID  IN ( <cf_queryparam value="#Variables.CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		<cfif CountriesAssignedList is not "">
			AND CountryID  NOT IN ( <cf_queryparam value="#CountriesAssignedList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		</cfif>
	ORDER BY
		CountryDescription
</CFQUERY>

<!--- get the countries that user has rights to view  --->
<CFQUERY NAME="CountriesAllowed" datasource="#application.siteDataSource#">
	SELECT
		CountryDescription,
		CountryID
	FROM
		Country
	WHERE
		CountryID  IN ( <cf_queryparam value="#Variables.CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	ORDER BY
		CountryDescription
</CFQUERY>

<CFSET SpacesList = "">

<SCRIPT type="text/javascript">

<cfif isOwner>
function submitForm(whereto) {
	var form = document.UpdateEvent;
	var msg = "";
	if (form.frmLocation.value.length == 0)	{
		msg += "* Event Location\n";
	}
	//if (form.frmVenueRoom.value.length == 0)
	//{
	//	msg += "* Room\n"
	//}
	if (form.frmVenueRoom.value.length > 100) {
		msg += "* Venue field can only contain up to 100 characters. You have entered " + form.frmVenueRoom.value.length + ". \n"
	}
	if (form.frmCountryID.selectedIndex == 0) {
		msg += "* Event Country\n";
	}
	if (!checkInteger(form.frmEstAttendees.value)||form.frmEstAttendees.value.length == 0) {
		msg += "* Estimated No of Attendees must be a valid number\n";
	}
	<cfif listfind(EventHideFormFields,"frmMaxTeamSize") is 0>
	if (!checkInteger(form.frmMaxTeamSize.value)||form.frmMaxTeamSize.value.length == 0) {
		msg += "* Maximum Team Size must be a valid number\n";
	}
	</cfif>
<!--- 2012/03/20 IH commented out because it is not compatible with US or ISO date formats
	if (!checkThisDate(form.frmPreferredStartDate.value)) {
		msg += "* Event Start Date must be completed\n";
	}
	if (!checkThisDate(form.frmPreferredEndDate.value)) {
		msg += "* Event End Date must be completed\n";
	}
 --->
	<cfif isdefined("form.frmPartnerType")>
		if (!alertChecked(form.frmPartnerType))	{
			msg += "* Which Partner Type must have a selection\n";
		}
	</cfif>
	if (!alertChecked(form.frmInvitationType))	{
		msg += "* Unique Invitation number required must have a selection\n";
	}
	<cfif listfind(EventHideFormFields,"frmAudienceType") is 0>
	if (!alertChecked(form.frmAudienceType))	{
		msg += "* Audience must have a selection\n";
	}
	</cfif>
	//if (!checkStartEnd(form.frmStartDate.value,form.frmEndDate.value)) {
	//	msg += "* End Date must be after Start Date\n";
	//}

	if (msg != "") {
		alert("\nYou must provide the following information for this Event:\n\n" + msg);
	}
	else {
		// if the event wizard is in use, the whereto parameter will exist.
		if(whereto != null)	{
			if (whereto == 0)
				form.action += "?eventStep=1"
			else
				form.action += "?eventStep=2"
		}


		// NJH 2013/12/20 - Case 438083 - disable the many country assigned form fields if they are empty as we are getting a 500 when submitting due to too many form fields
		jQuery('#UpdateEvent').find(':input[name^="Ax"]').filter(function() { return jQuery(this).val() == ""; }).attr("disabled", "disabled");
		jQuery('#UpdateEvent').find(':input[name^="Bx"]').filter(function() { return jQuery(this).val() == ""; }).attr("disabled", "disabled");
		jQuery('#UpdateEvent').find(':input[name^="Cx"]').filter(function() { return jQuery(this).val() == ""; }).attr("disabled", "disabled");
		form.submit();
	}
}
<cfelse>
function submitForm(whereto)
{
	form=document.UpdateEvent
	if(whereto != null)
	{
		if (whereto == -1) form.action = "eventlist.cfm"
		else if (whereto == 0) form.action += "?eventStep=1"
		else form.action = "<cfoutput>eventManager.cfm?RecordID=#jsStringFormat(FlagID)#&Entity=Flag&ShortName=EventTask&FlagID=#jsStringFormat(FlagID)#</cfoutput>"
	}
	form.submit();
}

</cfif>

//-->
</SCRIPT>


<cfset urlroot="cfdev">
<cf_WhatsLeftToDo FlagID="#FlagID#">

	<cfif session.eventWizard is "true">
		<div class="col-xs-12 col-sm-8 col-md-9 col-lg-10">
			<h2>Steps required in setting up an event</b></h2>
			<h3>Step 1:</h3>
			<p>This is where the basic information about the event is maintained. The information required is entered below.</p>
		</div>
	</cfif>
<!---SB end div from WhatsLeftToDo --->
</div>

<!--- 2014/02/13	YMA	Case 438887 Re-added form action for IE --->
<FORM id="UpdateEvent" NAME="UpdateEvent" METHOD="POST" ACTION="EventDetails.cfm">
<div class="row">
	<div class="col-xs-12 col-sm-6">
		<div class="grey-box-top">
			<input type="hidden" name="dirty" value="0">
		<CFOUTPUT>
			<CF_INPUT TYPE="hidden" NAME="FlagID" VALUE="#FlagID#">
			<CF_INPUT TYPE="hidden" NAME="IsOwner" VALUE="#IsOwner#">
			<div class="form-group">
				<label>This Event is owned by:</label> #htmleditformat(GetEventDetails.FirstName)# #htmleditformat(GetEventDetails.LastName)# (#htmleditformat(GetEventDetails.OfficePhone)#)
			</div>

			<div class="form-group">
				<label>Group this Event belongs to:</label>
				#htmleditformat(GetEventDetails.EventName)#
				<span class="help-block">
					<p>This is now read only. Once an event group has been chosen, it cannot be changed.</p>
				</span>
			</div>

			<div class="form-group">
				<label>Title of the event:</label>
				<cfif IsOwner>
					<CF_INPUT NAME="frmTitle" TYPE="Text" SIZE="40" MAXLENGTH="100" VALUE="#GetEventDetails.Title#" onchange="dirty.value=1">
				<cfelse>
					#htmleditformat(GetEventDetails.Title)#
				</cfif>
				<span class="help-block">
					<p><b>The title is made up of:</b> the event group, any text you put in the title field, the city and the start date.</p>
				</span>
			</div>

			<div class="form-group">
				<label>City: </label>
				<cfif IsOwner>
					<CF_INPUT NAME="frmLocation" TYPE="Text" SIZE="40" MAXLENGTH="100" VALUE="#GetEventDetails.Location#" onchange="dirty.value=1">
				<cfelse>
					#htmleditformat(GetEventDetails.Location)#
				</cfif>
				<span class="help-block">
					<p>This is required. This is the city in which the event is being held.</p>
				</span>
			</div>

			<div class="form-group">
				<label>Venue:</label>
				<cfif IsOwner>
					<TEXTAREA NAME="frmVenueRoom" rows="4" cols="40" MAXLENGTH="100" onchange="dirty.value=1" wrap="hard" class="form-control">#GetEventDetails.VenueRoom#</textarea>
				<cfelse>
					#htmleditformat(GetEventDetails.VenueRoom)#
				</cfif>
				<span class="help-block">
					<p>This is required. This is the full address of where the event is being held.</p>
				</span>
			</div>
		</cfoutput>
			<div class="form-group">
				<label>Country: </label>
				<CFIF CountriesAllowed.RecordCount GT 0>
					<cfif IsOwner>
						<SELECT NAME="frmCountryID" onchange="dirty.value=1" class="form-control">
							<option value="0">Please select a country
							<CFOUTPUT QUERY="CountriesAllowed">
								<OPTION VALUE="#CountryID#" <CFIF CountriesAllowed.CountryID is GetEventDetails.CountryID>SELECTED</CFIF> >#htmleditformat(CountryDescription)#
							</CFOUTPUT>
						</SELECT>&nbsp;
					<cfelse>
						<CFOUTPUT QUERY="CountriesAllowed">
							<CFIF CountriesAllowed.CountryID is GetEventDetails.CountryID>#htmleditformat(CountryDescription)#</CFIF>
						</CFOUTPUT>
					</cfif>
				<CFELSE>
					None
				</CFIF>
				<span class="help-block">
					<p>This is required. This is the country in which the event is being held.</p>
				</span>
			</div>
		<cfoutput>
			<div class="form-group">
				<label>Estimated number of attendees: </label>

					<CFIF IsOwner>
						<CF_INPUT TYPE="text" NAME="frmEstAttendees" SIZE="5" MAXLENGTH="5" VALUE="#GetEventDetails.EstAttendees#" onchange="dirty.value=1">
					<CFELSE>
						#htmleditformat(GetEventDetails.EstAttendees)#
					</CFIF>
				<span class="help-block">
					<p>This is required. This is the total number of attendees you anticipate being able to attend the event.</p>
				</span>
			</div>
			<cfif listfind(EventHideFormFields,"frmMaxTeamSize") is 0>
			<div class="form-group">
				<label>Maximum Team Size</label>
				<CFIF IsOwner>
					<CF_INPUT TYPE="text" NAME="frmMaxTeamSize" SIZE="5" MAXLENGTH="5" VALUE="#GetEventDetails.MaxTeamSize#" onchange="dirty.value=1">
				<CFELSE>
					#htmleditformat(GetEventDetails.MaxTeamSize)#
				</CFIF>
				<span class="help-block">
					<p>This is the maximum number of people you can have per team in this event.</p>
				</span>
			</div>
			</cfif>
			<div class="form-group">
				<label>Preferred start date:</label>
					<CFIF IsOwner>
						<cf_relaydatefield fieldname = "frmPreferredStartDate" currentvalue = #GetEventDetails.PreferredStartDate# formname="UpdateEvent" anchorname="x" yearSelectEndOffset="5" yearSelectStartOffset="5" onchange="dirty.value=1;">
					<CFELSE>
						#DateFormat(GetEventDetails.PreferredStartDate,dateTypeInfo.dateMask)#
					</CFIF>
				<span class="help-block">
					<p>This is required. This is the date you would like the event to start on. If you are not planning on using an agency to
					manage the event, this date will not change. If an agency is used, the actual start date may differ slightly.</p>
				</span>
			</div>

			<div class="form-group">
				<label>Preferred end date:</label>
				<CFIF IsOwner>
					<cf_relaydatefield fieldname = "frmPreferredEndDate" currentvalue = #GetEventDetails.PreferredEndDate# formname="UpdateEvent" anchorname="y" id="frmPreferredEndDate" yearSelectEndOffset="5" yearSelectStartOffset="5" onchange="dirty.value=1;">
				<CFELSE>
					<CFIF GetEventDetails.PreferredEndDate NEQ "">#DateFormat(GetEventDetails.PreferredEndDate,dateTypeInfo.dateMask)#</CFIF>
				</CFIF>
				<span class="help-block">
					<p>This is required. This is the date you would like the event to finish on. If you are not planning on using an agency to
					manage the event, this date will not change. If an agency is used, the actual end date may differ slightly.</p>
				</span>
			</div>

			<div class="form-group">
				<label>Notification email:</label>
				<CFIF IsOwner>
					<CF_INPUT TYPE="text" NAME="frmEmail" SIZE="40" MAXLENGTH="100" VALUE="#GetEventDetails.Email#" onchange="dirty.value=1"> <br>(Please separate multiple email addresses with a comma.)
				<CFELSE>
					<!--- display one per line --->
					<cfloop list="#GetEventDetails.Email#" delimiters="," index="listitem">
						#htmleditformat(listitem)#<br>
					</cfloop>
				</CFIF>
				<span class="help-block">
					<p>This is optional. This is the email address you would like to have notification of changes to this event sent to.</p>
				</span>
			</div>
		</cfoutput>
			<div class="form-group">
				<label>Countries assigned</label>
					<CFIF countriesAssigned.RecordCount GT 0>
						<CFIF IsOwner>
							<TABLE class="eventDetailsTYable4 table table-hover table-striped">
							<TR ALIGN="center">

								<TD align="right"><b>Country</b></TD>
								<TD align="center"><b>Pre Sales</b></TD>
								<TD align="center"><b>Sales</b></TD>
								<TD align><b>Post Sales</b></TD>
								<td align="left"><b>Remove?/<br>No. delegates assigned</b></td>
							</TR>
							<CFOUTPUT QUERY="countriesAssigned">
							<TR>

								<TD ALIGN="right"><B>#htmleditformat(CountryDescription)#</B></TD>
								<TD ALIGN="center"><CF_INPUT NAME="Ax#CountryID#" TYPE="TEXT" SIZE="4" MAXLENGTH="4" VALUE="#AllocatedSpacesA#" onchange="dirty.value=1"></TD>
								<TD ALIGN="center"><CF_INPUT NAME="Bx#CountryID#" TYPE="TEXT" SIZE="4" MAXLENGTH="4" VALUE="#AllocatedSpacesB#" onchange="dirty.value=1"></TD>
								<TD ALIGN="center"><CF_INPUT NAME="Cx#CountryID#" TYPE="TEXT" SIZE="4" MAXLENGTH="4" VALUE="#AllocatedSpacesC#" onchange="dirty.value=1"></TD>
								<td><cfif DelegatesAssigned is 0 and IsOwner>
										<CF_INPUT type="checkbox" name="deleteCountry" value="#CountryID#" onchange="dirty.value=1">
									<cfelse>(#htmleditformat(DelegatesAssigned)# delegates assigned)
									</cfif>
									<cfif countriesAssigned.currentrow is 1 and recordcount gt 1 and isOwner>
										<cf_InvertSelection checkbox="deleteCountry" form="UpdateEvent" displayas="anchor">
									</cfif>
								</td>

							</TR>
							<CFIF SpacesList EQ "">
								<CFSET SpacesList = SpacesList & "Ax#CountryID#,Bx#CountryID#,Cx#CountryID#">
							<CFELSE>
								<CFSET SpacesList = SpacesList & ",Ax#CountryID#,Bx#CountryID#,Cx#CountryID#">
							</CFIF>
							</CFOUTPUT>
							</TABLE>
						<CFELSE>
							<div class="row">
								<CFOUTPUT QUERY="countriesAssigned">
									<div class="col-xs-12 col-sm-4 col-md-4 col-lg-3">
										#htmleditformat(CountryDescription)#
									</div>
								</CFOUTPUT>
							</div>
						</CFIF>
						<CFELSE>
							None
					</CFIF>
				<span class="help-block">
						<p>Maximum number of delegates allowed by sales stream, per country.</p>
					<cfif isOwner>
						<p>If a checkbox exists for the country, it means that the country has not had any delegates assigned, and the country can be removed. To remove the country, click the checkbox and save the data.</p>
					</cfif>
				</span>
			</div>
		</div>
	</div>
	<div class="col-xs-12 col-sm-6">
		<div class="grey-box-top">

			<!---
			<TR>
				<TD VALIGN="top" colspan="4" class="label">
					<b>Countries without delegates Assigned<CFIF IsOwner> - removable</CFIF></b><br>
					<div class="margin">
						This is the list of countries which are allowed to have delegates attend this event, but where no
						delegates are yet reqistered. To remove countries from this list, highlight them.
					</div>
					<div align="right">
						<CFIF countriesAssignedRemovable.RecordCount GT 0>
							<CFIF IsOwner>
								<SELECT NAME="deleteCountry" <CFIF countriesAssignedRemovable.RecordCount LT 10>SIZE="<CFOUTPUT>#countriesAssignedRemovable.RecordCount#</CFOUTPUT>" <CFELSE> SIZE="10" </CFIF> MULTIPLE onchange="dirty.value=1">
									<CFOUTPUT QUERY="countriesAssignedRemovable">
										<OPTION VALUE="#CountryID#">#CountryDescription#
									</CFOUTPUT>
								</SELECT>
							<CFELSE>
								<CFOUTPUT QUERY="countriesAssignedRemovable">#CountryDescription#<BR></CFOUTPUT>
							</CFIF>
						<CFELSE>
							None
						</CFIF>
						<CFIF countriesAssignedRemovable.RecordCount GT 1 and IsOwner>
							<br>
							(Hold down Ctrl to click multiple selections)<br>
							<cf_invertselection	form="UpdateEvent" displayas="ANCHOR" select="deleteCountry" onclick="dirty.value=1">
						</cfif>
					</div>
				</td>

			</TR>
			--->
			<CFIF IsOwner>
				<div class="form-group">
					<label>Countries Available</label>
					<CFIF countriesAvailable.RecordCount GT 0>
					<SELECT NAME="addCountry" class="form-control" <CFIF countriesAvailable.RecordCount LT 10>SIZE="<CFOUTPUT>#countriesAvailable.RecordCount#</CFOUTPUT>" <CFELSE> SIZE="10" </CFIF> MULTIPLE onchange="dirty.value=1">
						<CFOUTPUT QUERY="countriesAvailable">
							<OPTION VALUE="#CountryID#">#htmleditformat(CountryDescription)#
						</CFOUTPUT>
					</SELECT>
					<CFELSE>
						<B>All countries are asigned to this event.</B>
					</CFIF>
					<CFIF countriesAvailable.RecordCount GT 1 and IsOwner>
						<br>(Hold down Ctrl to click multiple selections)<br>
						<cf_invertselection	form="UpdateEvent"	displayas="ANCHOR" select="addCountry" onclick="dirty.value=1">
					</cfif>
					<span class="help-block">
						<p>This is the list of countries you can assign to this event to allow delegates from that country to attend.
						Highlight any additional countries you would like to assign to this event. Once the record is saved they will
						appear the list above.</p>
					</span>
				</div>
				<div class="form-group">
					<label>Event Visibility</label>
					<CF_UGRecordManager entity="EventDetail" entityid="#flagid#" Form="UpdateEvent">
					<span class="help-block">
						<p>This is the list of user groups you can assign to this event.</p>
					</span>
				</div>
			</CFIF>

			<div class="form-group">
				<label>Will you use an agency to manage this event?</label>
				<cfif IsOwner>
					<select name="frmUseAgency" class="form-control" onchange="dirty.value=1">
						<option value="1" <cfif GetEventDetails.UseAgency is 1>selected</cfif>>Yes
						<option value="0" <cfif GetEventDetails.UseAgency is 0>selected</cfif>>No
					</select>
				<cfelse>
					<cfif GetEventDetails.UseAgency is 1>Yes<cfelse>No</cfif>
				</cfif>
			</div>
			<div class="form-group">
				<label>Event Status:</label>
				<cfif IsOwner>
					<CF_INPUT type="hidden" id="frmEventStatus_orig" name="frmEventStatus_orig" value="#GetEventDetails.EventStatus#">
					<select name="frmEventStatus" onchange="dirty.value=1" class="form-control">
						<option value="Initial" <cfif GetEventDetails.EventStatus is "Initial">selected</cfif>>Initial
						<!---<option value="Requested" <cfif GetEventDetails.EventStatus is "Requested">selected</cfif>>Requested--->
						<option value="Agreed" <cfif GetEventDetails.EventStatus is "Agreed">selected</cfif>>Agreed
						<option value="Complete" <cfif GetEventDetails.EventStatus is "Complete">selected</cfif>>Complete
						<option value="Cancelled" <cfif GetEventDetails.EventStatus is "Cancelled">selected</cfif>>Cancelled
					</select>
				<cfelse>
					<cfoutput>#htmleditformat(GetEventDetails.EventStatus)#</cfoutput>
				</cfif>
				<span class="help-block">
					<p><b>Events can have the following statuses:</b></p>
					<ul>
						<li>Initial -  This is where the event is at the planning process - no delegates can register for the event.</li>
						<li>Agreed - This is where the event has been agreed as being allowed to go live, and delegates can be registered.</li>
						<li>Complete - The event has finished.</li>
						<li>Cancelled - This is where the event has been cancelled.</li>
					</ul>
				</span>
			</div>

			<!--- START: 2013-07-31	MS	P-KAS026 Allow the users to set the visibility of Register or Unregister Buttons --->
			<h2>
				Portal Buttons</b>
			</h2>

			<div class="form-group">
				<label class="checkbox">
					Hide Register for Event button
					<cfif IsOwner>
						<CF_INPUT type="checkbox" name="frmEventHideRegBtn" checked=#GetEventDetails.hideRegisterForEventBtn# value="1" onchange="dirty.value=1" <!--- disabled="#not(eventShare)#" --->><span title="When checked users won't be able to see the Register for Event button on portal'."><img src="/images/misc/iconhelpsmall.gif">
					<cfelse>
						<cfif GetEventDetails.hideRegisterForEventBtn>Yes<cfelse>No</cfif>
					</cfif>
				</label>
			</div>
			<div class="form-group">
				<label class="checkbox">
					Hide Un-Register from Event button
					<cfif IsOwner>
						<CF_INPUT type="checkbox" name="frmEventHideUnRegBtn" checked=#GetEventDetails.hideUnRegisterForEventBtn# value="1" onchange="dirty.value=1" <!--- disabled="#not(eventShare)#" --->><span title="When checked users won't be able to see the UnRegister from an already registered event on portal."><img src="/images/misc/iconhelpsmall.gif">
					<cfelse>
						<cfif GetEventDetails.hideUnRegisterForEventBtn>Yes<cfelse>No</cfif>
					</cfif>
				</label>
			</div>
			<!--- END  : 2013-07-31	MS	P-KAS026 Allow the users to set the visibility of Register or Unregister Buttons --->
			<cfif listfind(EventHideFormFields,"frmPartnerType") is 0>
			<div class="form-group">
				<label>Eligible partner types:</label>
				<cfif IsOwner>
					<cfoutput query="getPartnerTypes">
						#htmleditformat(name)# <CF_INPUT type="checkbox" name="frmPartnerType" value="#FlagID#" checked="#iif( listcontains(getEventDetails.PartnerType,getPartnerTypes.FlagID),true,false)#" onclick="dirty.value=1">
					</cfoutput>
				<cfelse>
					<cfoutput query="getPartnerTypes">
						<cfif listcontains(getEventDetails.PartnerType,getPartnerTypes.FlagID)>
							#htmleditformat(name)#&nbsp;
						</cfif>
					</cfoutput>
				</cfif>
				<span class="help-block">
					<p>This is optional. This is the partner types you would like to be considered eligible for this event.</p>
				</span>
			</div>
			</cfif>
			<div class="form-group">
				<label>Open or restricted registration:</label>
				<cfif IsOwner>
					Open registration <INPUT NAME="frmInvitationType" TYPE="radio" VALUE="Open" <cfif getEventDetails.InvitationType is "Open">checked</cfif> onclick="dirty.value=1">
					By invitation only <INPUT NAME="frmInvitationType" TYPE="radio" VALUE="InvOnly" <cfif getEventDetails.InvitationType is "InvOnly">checked</cfif> onclick="dirty.value=1">
				<cfelse>
					<cfif getEventDetails.InvitationType is "open">
						Open to all
					<cfelse>
						By Invitation only
					</cfif>
				</cfif>
				<span class="help-block">
					<p>This is required. This is to allow you to indicate whether the event should be open for anyone to attend, or whether
					to only allow delegates with an invitation to attend. If you decide the event is open by invitation only, you will
					need to select the appropriate delegates to invite. The invited delegates will then be required to enter a unique invitation
					reference number to authenticate their eligibility.</p>
				</span>
			</div>

			<cfif listfind(EventHideFormFields,"frmAudienceType") is 0>
			<div class="form-group">
				<label>Audience</label>
				<cfif IsOwner>
					<label class="checkbox">
						<INPUT NAME="frmAudienceType" TYPE="checkbox" VALUE="Sales"<cfif GetEventDetails.AudienceType contains "Sales">checked</cfif> onclick="dirty.value=1; if(this.checked) frmAudienceType[2].checked = false"> Sales
					</label>
					<label class="checkbox">
						<INPUT NAME="frmAudienceType" TYPE="checkbox" VALUE="Pre-Sales" <cfif GetEventDetails.AudienceType contains "Pre-Sales">checked</cfif> onclick="dirty.value=1; if(this.checked) frmAudienceType[2].checked = false"> Pre-Sales
					</label>
					<label class="checkbox">
						<INPUT NAME="frmAudienceType" TYPE="checkbox" VALUE="NA" <cfif GetEventDetails.AudienceType contains "NA">checked</cfif> onclick="dirty.value=1; if(this.checked) {frmAudienceType[0].checked = false; frmAudienceType[1].checked = false}"> Not applicable
					</label>
				<cfelse>
					<cfoutput>#htmleditformat(GetEventDetails.AudienceType)#</cfoutput>
				</cfif>
				<span class="help-block">
					<p>This is required. This is to indicate what streams of delegates should be allowed to attend the event.</p>
				</span>
			</div>
			</cfif>
			<cfif listfind(EventHideFormFields,"frmScantron") is 0>
			<div class="form-group">
				<h2>Scantron forms:</h2>
				<cfoutput>
				<label>Address</label> <CF_INPUT NAME="frmScantronShippingAddress" TYPE="text" VALUE="#GetEventDetails.ScantronShippingAddress#"  onchange="dirty.value=1">
				<label>Telephone</label> <CF_INPUT NAME="frmScantronTel" TYPE="text" VALUE="#GetEventDetails.ScantronTel#" onchange="dirty.value=1">
				<label>Contact</label> <CF_INPUT NAME="frmScantronContact" TYPE="text" VALUE="#GetEventDetails.ScantronContact#" onchange="dirty.value=1">
				</cfoutput>
				<span class="help-block">
					<p>This is optional. This is to indicate the office address for the forms.</p>
				</span>
			</div>
			<cfelse>
				<input type="hidden" name="frmScantronShippingAddress" value="">
				<input type="hidden" name="frmScantronTel" value="">
				<input type="hidden" name="frmScantronContact" value="">
			</cfif>
			<!--- <TR>
					<TD valign="top" colspan="4" class="label">
						<b>Half Day Event</b><br>
						<div Class="Margin">
							This is required. This is to indicate any other activities carried out on the day.
						</div>
						<div align="right">
						Additional Events <INPUT NAME="frmExtraProgramme" TYPE="text" VALUE="" onchange="dirty.value=1">

						</div>
					</TD>
				</TR>	 --->
			<cfif listfind(EventHideFormFields,"frmVenueRating") is 0>
				<div class="form-group">
					<label>Venue Rating:</label>
					<select name="frmVenueRating" onchange="dirty.value=1" class="form-control">
						<option value="1" <CFIF GetEventDetails.VenueRating EQ 1>SELECTED</CFIF>>One star
						<option value="2" <CFIF GetEventDetails.VenueRating EQ 2>SELECTED</CFIF>>Two Star
						<option value="3" <CFIF GetEventDetails.VenueRating EQ 3>SELECTED</CFIF>>Three Star
						<option value="4" <CFIF GetEventDetails.VenueRating EQ 4>SELECTED</CFIF>>Four Star
						<option value="5" <CFIF GetEventDetails.VenueRating EQ 5>SELECTED</CFIF>>Five Star
					</select>
					<span class="help-block">
						<p>This is optional. This is to indicate the star rating of the venue.</p>
					</span>
				</div>
			<cfelse>
				<input type="hidden" name="frmVenueRating" value="">
			</cfif>
			<cfif listfind(EventHideFormFields,"frmTypeofrefreshment") is 0>
				<div class="form-group">
					<label>Type of Refreshment:</label>
					<select name="frmTypeofrefreshment" onchange="dirty.value=1" class="form-control">
						<option value="FingerBuffet" <CFIF GetEventDetails.Typeofrefreshment EQ "FingerBuffet">SELECTED</CFIF>>Finger buffet
						<option value="BeerandSandwich" <CFIF GetEventDetails.Typeofrefreshment EQ "BeerandSandwich">SELECTED</CFIF>>Beer and Sandwich
						<option value="HotandColdBuffet" <CFIF GetEventDetails.Typeofrefreshment EQ "HotandColdBuffet">SELECTED</CFIF>>Hot and Cold Buffet
						<option value="Other" <CFIF GetEventDetails.Typeofrefreshment EQ "Other">SELECTED</CFIF>>Other
					</select>
					<span class="help-block">
						<p>This is optional. This is to indicate the level of food served.</p>
					</span>
				</div>
			<cfelse>
				<input type="hidden" name="frmTypeofrefreshment" value="">
			</cfif>
				<div class="form-group">
					<label>Registration Screen:</label>

					<cfquery name="getScreens" datasource="#application.siteDataSource#">
						select 	ScreenID, ScreenName
						from 	Screens
						where 	active = 1
						<cfif not isOwner>and screenID = <cf_queryparam cfsqltype="CF_SQL_INTEGER" value="#GetEventDetails.RegistrationScreenID#"></cfif>
						order by ScreenName
					</cfquery>

					<cfif isOwner>
						<select name="frmRegistrationScreenID" onchange="dirty.value=1" class="form-control">
							<option value="0"<cfif GetEventDetails.RegistrationScreenID eq 0> selected</cfif>>None
							<cfoutput query="getScreens">
							<option value="#ScreenID#"<cfif GetEventDetails.RegistrationScreenID eq ScreenID> selected</cfif>>#htmleditformat(ScreenName)#
							</cfoutput>
						</select>
					<cfelse>
						<cfoutput><cfif GetEventDetails.RegistrationScreenID eq 0>None<cfelse>#htmlEditFormat(getScreens.screenName)#</cfif></cfoutput>
					</cfif>
					<span class="help-block">
						<p>This is optional. This is the screen to which the users will be redirected after registration.</p>
					</span>
				</div>

				<!--- NJH 2008/09/23 add a module to an event - elearning 8.1 --->
				<div class="form-group">
					<label>Associate eLearning Module:</label>

					<cfscript>
						qryModules = application.com.relayElearning.GetTrainingModulesData();
					</cfscript>

					<!--- 2008-12-22 NYB BugFix - isn't putting in Module --->
					<cfif not isDefined("frmModuleID")>
						<cfset frmModuleID = GetEventDetails.moduleID>
						<cfif frmModuleID eq "">
							<cfset frmModuleID = 0>
						</cfif>
					</cfif>

					<!--- NJH 2009/03/23 Bug Fix All Sites Issue 1988- added a distinct to the moduleId and title --->
					<cfquery name="qryModules" dbType="query">
						select distinct moduleID, title_of_module from qryModules where module_type in ('Classroom','Webinar')
						<cfif not isOwner>and moduleID = <cf_queryparam cfsqltype="CF_SQL_INTEGER" value="#frmModuleID#"></cfif>
					</cfquery>

					<cfif isOwner>
						<select name="frmModuleID" onchange="dirty.value=1" class="form-control">
							<option value="">Select a module
							<cfoutput query="qryModules">
							<option value="#moduleID#"<cfif isDefined("frmModuleID") and frmModuleID eq moduleID> selected</cfif>>#htmleditformat(title_of_module)#
							</cfoutput>
						</select>
					<cfelse>
						<cfoutput>#HTMLEditFormat(qryModules.title_of_module)#</cfoutput>
					</cfif>
					<span class="help-block">
						<p>For Education events. Optional.</p>
					</span>
				</div>

				<!--- NJH 2012/10/23  - Social CR  - share an event --->
				<div class="form-group">
					<label>Social Updates:</label>
					<cfset eventShare = true>
					<cfif not application.com.settings.getSetting("socialMedia.enableSocialMedia")>
						<cfset eventShare = false>
					</cfif>
					<cfif IsOwner>
						<CF_INPUT type="checkbox" name="frmEventShare" checked=#GetEventDetails.share# value="1" onchange="dirty.value=1" disabled="#not(eventShare)#"><cfif not eventShare><span title="Social Media has not been enabled."><img src="/images/misc/iconhelpsmall.gif"></span></cfif>
					<cfelse>
						<cfif GetEventDetails.share>Yes<cfelse>No</cfif>
					</cfif>
					<span class="help-block">
						<p>Allow social updates about this event</p>
					</span>
				</div>

				<cfif application.com.settings.getSetting("campaigns.displayCampaigns")>
					<div class="form-group">
						<label>Phr_Sys_AssociateCampaign:</label>
						<cfset getCampaigns = application.com.relayCampaigns.GetCampaigns(currentValue=GetEventDetails.campaignID)>
						<!--- 2014-10-29	RPW	CORE-803 Hide Register and Unregister buttons not working under Events module --->
						<cfif IsOwner>
						<SELECT NAME="frmCampaignID" class="form-control">
							<OPTION value="">Phr_Ext_SelectAValue</OPTION>
							<cfloop query="getCampaigns">
								<cfoutput><OPTION VALUE="#getCampaigns.campaignID#" <cfif GetEventDetails.campaignID is getCampaigns.campaignID>SELECTED</cfif>>#htmleditformat(getCampaigns.campaignName)#</OPTION></cfoutput>
							</cfloop>
						</SELECT>
						<cfelse>
							<cfif IsNumeric(GetEventDetails.campaignID)>
								<cfquery name="qGetSelectedCampaign" dbtype="query">
									SELECT *
									FROM getCampaigns
									WHERE campaignID = #GetEventDetails.campaignID#
								</cfquery>
								<cfoutput>#htmleditformat(qGetSelectedCampaign.campaignName[1])#</cfoutput>
							<cfelse>
								None
							</cfif>
						</cfif>
					</div>
				</cfif>

				<div class="form-group">
					<label>Sort Order</label>
					<cfif isOwner><cfinclude template="event_sortorder.cfm"><cfelse><cfif GetEventDetails.sortorder eq "">0<cfelse><cfoutput>#htmlEditFormat(GetEventDetails.sortorder)#</cfoutput></cfif></cfif>
					<span class="help-block">
						<p>Use to sort your events when displayed in registration screens.</p>
					</span>
				</div>

			<!---<TR>
				<TD CLASS="label" ALIGN="right" valign="top">Agenda:</TD>
				<TD WIDTH="20">&nbsp;</TD>
				<TD valign="top">
					<cfif IsOwner>
						<textarea name="frmAgenda" rows="8" cols="45" wrap="hard"><cfoutput>#GetEventDetails.Agenda#</cfoutput></textarea>
					<cfelse>
						<cfoutput>#GetEventDetails.Agenda#</cfoutput>
					</cfif>
				</TD>
			</TR>--->


			<!---<tr>
				<td class="Label" align="right" valign="top">Check List</td>
				<TD WIDTH="20">&nbsp;</TD>
				<TD VALIGN="top" colspan="2">
					<CFOUTPUT><a href="Javascript:newWin = openWin('../events/checklist.cfm?frmEntityID=#flagid#', 'PopUp','menu=0,height=500,width=400,scrollbars'); newWin.focus()">Checklist</a></cfoutput>
				</td>
			</tr>--->

			<CFIF IsOwner and session.eventWizard is not "true">
				<CFQUERY NAME="GetAssigned" datasource="#application.siteDataSource#">
					SELECT
						ug.Name,
						rr.UserGroupID
					FROM
						RecordRights AS rr
						INNER JOIN UserGroup AS ug ON rr.UserGroupID = ug.UserGroupID
					WHERE
						rr.RecordID =  <cf_queryparam value="#FlagID#" CFSQLTYPE="CF_SQL_INTEGER" >
						AND rr.Entity = 'Flag'
					ORDER
						BY ug.Name
				</CFQUERY>
				<div class="form-group">
						<label>Managers Assigned to this Event:</label>
						<CFOUTPUT QUERY="GetAssigned"><p>#htmleditformat(Name)#</p></CFOUTPUT>
						<a href="javascript:ManageEventManagers()" class="btn btn-primary">Manage Event Managers</a>
						<span class="help-block">
							<p>Managers assigned to the event are allowed to update certain aspects of an event.</p>
							<ul>
								<li>Level 1 managers can .</li>
								<li>Level 2 Managers can .</li>
								<li>Level 3 Managers can .</li>
							</ul>
							<p>To add an event manager, click the "Manage Event Managers" link, then in the pop up window,
							highlight their name from the drop down, and click Add. Note: The newly assign managers names won't appear here
							unless you refresh this screen.</p>
							<cfif UseAgency>
								 <p>Don't forget to assign a manager from the agency you are using to allow them to take over
								 management of the event.</p>
							</cfif>
						</span>
				</div>
			</CFIF>
			<CFOUTPUT><CF_INPUT TYPE="hidden" NAME="SpacesList" VALUE="#SpacesList#"></CFOUTPUT>
		</div>
	</div>
</div>
</FORM>

<FORM NAME="thisForm" ACTION="../templates/entityManager.cfm" METHOD="post" TARGET="PopUp">
	<CFOUTPUT>
	<CF_INPUT TYPE="Hidden" NAME="RecordID" VALUE="#FlagID#">
	<INPUT TYPE="Hidden" NAME="Entity" VALUE="Flag">
	<INPUT TYPE="Hidden" NAME="ShortName" VALUE="EventTask">
	</CFOUTPUT>
</FORM>
<cfparam name="displaysave" default="yes">
<cfinclude template="eventFooter.cfm">