﻿<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File:			EventTopHead.cfm
Author:			DJH
Date created:	27 January 2000

Description:
Provides a top level navigation for the events pages.
Updates the navigation tools based on user rights.

Version history:
1

2007-06-26  altered some javascript to allow add event to work when no existing events
2008/10/24	NJH	Sophos RelayWare Set Up Bug Fix Issue 1102 - return the count of those invited,registered and attending in the event query
2009/05/07  SSS S-01084 added a button to go back if you come from another template to the diary view
2009-09-28  NYB LHID2672 - limit size of Title drop down
2013/04/08	YMA	Case 434610 Removed white space between params.
2015-11-25  SB		Bootstrap
--->

<!---

<cf_head>
<cf_title>Events</cf_title> --->

<cfparam name="session.showstatus" default="Initial,Agreed"> <!--- 2013/04/08	YMA	Case 434610 Removed white space between params.--->
<!--- NYB 2009-09-28 LHID2672 - added param to limit the size of the drop down when overly long titles are used: --->
<cfset eventDropDownSize="100">

<cfif isDefined("form.showStatus")>
	<cfset session.showstatus=form.showstatus>
</cfif>

<!--- RMB - 2013-09-16 - P-KAS026 - START --->
<cfparam name="session.showeventCountryID" default="0">

<cfif StructKeyExists(form, "frmCountryID")>
	<cfset session.showeventCountryID=form.frmCountryID>
</cfif>
<!--- RMB - 2013-09-16 - P-KAS026 - END --->

<cfparam name="ShowSave" default="yes">
<cfparam name="ShowNav" default="yes">
<cfset templateName = listlast(cgi.script_name,"/")>

<!--- we don't wnat the eventWizard session variable to time out - if it exists recreate it --->
<cfif isDefined("session.eventWizard")>
	<cfset session.eventWizard = session.eventWizard>
</cfif>

<!--- default the eventWizard parameters to yes --->

<cfparam name="Session.EventWizard" default="yes">
<cfparam name="session.eventStep" default="1">

<cfif isDefined("url.eventStep")>
	<cfset session.eventstep = url.eventstep>
</cfif>


<!--- check whether the eventWizard has changed states --->

<cfif parameterexists(URL.eventWizard)>
	<cfset session.eventWizard = URL.eventWizard>
</cfif>

<cfif session.EventWizard is "true">
	<cfset ShowNav = false>
	<cfset ShowSave = false>
</cfif>

<cfparam name="FlagID" default="">

<cf_head>
<SCRIPT type="text/javascript">

	// function to ensure at least one item in either a checkbox or radio group is selected
	function alertChecked(groupname)
	{
		// whether a checkbox or a radio, they both follow the same array structure
		var retval = false
		for (var i = 0; i < groupname.length; i++)
		{
			if (groupname[i].checked)
			{
				retval = true
				break
			}
		}
		return retval
	}

	// function to check whether any changes have been made to the data - if they have, offers
	// an opportunity to save before moving from the record
	function saveAlert()
	{
		var form=document.UpdateEvent
		if (!form) form=document.addEvent
		if(!form) return false
		if(!form.dirty) return false
		if (form.dirty.value==1)
		{
			if (confirm("Warning: You have made changes, but not saved them. If you would like to save the changes, click OK, otherwise click Cancel."))
			{
				submitForm()
				return true
			}
		}
		return false
	}

	// function to change to and from the wizard - if any changes have been made to data allows the user
	// to cancel the action - warning that data will be lost.
	function eventWizard(switchOn)
	{
		var now = new Date()
		var thistemplate = <cfoutput>"#jsStringFormat(templatename)#"</cfoutput>
		now = now.getTime()
		<cfif templatename contains "eventlist.cfm">
			window.location.href = "eventList.cfm?eventWizard=" + switchOn + "&dummytime=" + now
		<cfelse>
		var form=document.UpdateEvent
		if (!form) form=document.addEvent
		if (!form) form=document.eventStep
		if (form.dirty.value==1)
		{
			if (confirm("Warning: You have added some data. If you want to change to or from the event wizard now, any data will be lost. If you are sure you are happy with this click OK. Otherwise click cancel."))
			{
				window.location.href = thistemplate + "?eventWizard=" + switchOn + "&dummytime=" + now + "<cfif FlagID is not "">&FlagID=<cfoutput>#jsStringFormat(FlagID)#</cfoutput></cfif>"
			}
		}
		else
		{
			window.location.href = "eventDetails.cfm?eventWizard=" + switchOn + "&dummytime=" + now + "<cfoutput><cfif FlagID is not "">&FlagID=#jsStringFormat(FlagID)#</cfif></cfoutput>"
		}
		</cfif>
	}

	// searches a select, and selects the row with a matching value
	function assignSelect(aselect, valueToCheck)
	{
		var found = false
		for (var i = 0; i < aselect.options.length; i++)
		{
			if (valueToCheck == aselect.options[i].value)
			{
				found=true
				aselect.selectedIndex = i
				break
			}
		}
		if (found)
		{
			if(document.tophead.frmChooserLeft.selectedIndex == 0) {
				document.tophead.frmChooserLeft.selectedIndex = 1
			}
			document.tophead.action = document.tophead.frmChooserLeft.options[document.tophead.frmChooserLeft.selectedIndex].value
			aselect.form.submit()
		}
	}

	// used by the left and right buttons to move the selct box forwards and backwards
	function gotoRecord(offset)
	{
		var currentRecord = document.tophead.FlagID.selectedIndex
		// if the record were trying to get to is less than the first record,
		// or after the last record, or is an event group ignore it
		// (may need to change this if we start managing groups directly too)
		if (currentRecord + offset < 1 || currentRecord + offset > document.tophead.FlagID.length -1)
		{
			return false
		}
		else
		{
			// if the record we're trying to get to has a blank value, it must be a group -
			// check whether the next one or previous one to that is valid, and if so move to that
			// it could happen that two groups could sit next to each other - This solution uses
			// recursion, so should sort itself without a problem
			if(document.tophead.FlagID.options[currentRecord + offset].value.length == 0)
			{
				if(offset < 0)
					gotoRecord(offset - 1)
				else
					gotoRecord(offset + 1)
				return true
			}
			document.tophead.FlagID.selectedIndex = currentRecord + offset
			loadScreen(document.tophead.FlagID)
		}
		return true
	}


	// function to load a new screen
	function loadScreen(aselect)
	{
		var form = aselect.form
		var theselect = aselect.name

		if(saveAlert())
			return false
		if (theselect == "FlagID")
		{
			theselect = "frmChooserLeft"
		}
 		if(checkSubmission(form))
 		{
			eval("form.action = form." + theselect + ".options[form." + theselect + ".selectedIndex].value")
 			form.submit()
 		}
		return true
	}

	function checkSubmission(aform)
	{
		var retval = true
		// if an event has not been chosen, but the details page is chosen ignore it.
		if (aform.FlagID.value.length == 0 && aform.frmChooserLeft.options[aform.frmChooserLeft.selectedIndex].value != "eventList.cfm")
		{
			retval= false
		}
		// if an event is chosen, and frmChooser left is still set to list, set it to details by default.
		if (aform.FlagID.value.length > 0 && aform.frmChooserLeft.options[aform.frmChooserLeft.selectedIndex].value == "eventList.cfm")
		{
			retval = true
			aform.frmChooserLeft.selectedIndex = 1
		}
		// if add event is chosen from frmChooserRight, set both other selectedIndex to zero
		if (aform.frmChooserRight.options[aform.frmChooserRight.selectedIndex].value == "eventAddEvent.cfm")
		{
			retval = true
			aform.FlagID.selectedIndex = 0
			aform.frmChooserLeft.selectedIndex = 0
		}
		return retval
	}

	// other functions

	function trim (InString)  {
		OutString = "";
		if (InString) {
			for (Count=0; Count < InString.length; Count++)  {
				TempChar=InString.substring (Count, Count+1);
				if (TempChar != " ") {
					OutString=OutString + TempChar;
				}
			}
		}
		return (OutString);
	}

	function checkEuroDate(theDate) {
	    if (theDate.length == 0)
	        return false;
		isplit = theDate.indexOf('-');

		if (isplit == -1 || isplit == theDate.length)
			return false;
	    sDay = theDate.substring(0, isplit);
		monthSplit = isplit + 1;
		isplit = theDate.indexOf('-', monthSplit);

		if (isplit == -1 || (isplit + 1 ) == theDate.length)
			return false;
	    sMonth = theDate.substring((sDay.length + 1), isplit);
		sYear = theDate.substring(isplit + 1);
		sMonth = checkMonth(sMonth);

		if (!numberRange(sMonth, 1, 12)) // check month
			return false;
		else
		if (!checkInteger(sYear)) //check year
			return false;
		else
		if (!numberRange(sYear, 0, null)) //check year
			return false;
		else
		if (!checkInteger(sDay)) //check day
			return false;
		else
		if (!checkDay(sYear, sMonth, sDay)) //check day
			return false;
		else
			return true;
	}

	function checkMonth(theMonth) {
		var monthList1 = new Array ("jan","feb","mar","apr","may","jun","jul","aug","sep","oct","nov","dec");
		var returnMonth = -1;
		for(var i = 0; i < 12; i++) {
			if(monthList1[i] == theMonth.toLowerCase()) {
				returnMonth = i+1;
			}
		}
		return returnMonth;
	}

	function checkStartEnd(PreferredStartDate,PreferredEndDate) {
		// PreferredStartDate = "1999-11-11";
		alert(PreferredStartDate);
		PreferredStartDate = toGMTString(PreferredStartDate);
		alert(PreferredStartDate);
		// PreferredEndDate = parse(PreferredEndDate);
		// if (PreferredEndDate > PreferredStartDate) {
		//	 return false;
		// }
		return true;
	}

	function checkDay(checkYear, checkMonth, checkDay) {
		maxDay = 31;
		if (checkMonth == 4 || checkMonth == 6 ||
				checkMonth == 9 || checkMonth == 11)
			maxDay = 30;
		else
		if (checkMonth == 2) {
			if (checkYear % 4 > 0)
				maxDay =28;
			else
			if (checkYear % 100 == 0 && checkYear % 400 > 0)
				maxDay = 28;
			else
				maxDay = 29;
		}
		return numberRange(checkDay, 1, maxDay);
	}

	function checkInteger(checkThis) {
		var newLength = checkThis.length
		for(var i = 0; i != newLength; i++) {
			aChar = checkThis.substring(i,i+1)
			if(aChar < "0" || aChar > "9") {
				return false
			}
		}
		return true
	}

	function numberRange(object_value, min_value, max_value) {
	    if (min_value != null) {
	        if (object_value < min_value)
			return false;
		}

	    if (max_value != null) {
			if (object_value > max_value)
			return false;
		}
	    return true;
	}

	function openWin( windowURL,  windowName, windowFeatures ) {
		return window.open( windowURL, windowName, windowFeatures );
	}

	function ManageEventManagers()
	{
		newWindow = window.open( 'about:blank' ,'PopUp', 'width=650,height=500,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1' )
		document.thisForm.submit()
		newWindow.focus()
	}
</script>
</cf_head>

<CFSET FreezeDate = request.requestTimeODBC>

<cfparam name="Session.AttendeeStats" default="no">

<cfif parameterexists(URL.AttendeeStats)>
	<cfset session.AttendeeStats = URL.AttendeeStats>
</cfif>


<!--- mask any fields which didnt exist --->
<!--- default parameters --->
<cfparam name="FlagID" default="">
<cfparam name="frmFlagID" default="">
<cfparam name="frmChooserLeft" default="#templateName#">
<cfparam name="frmChooserRight" default="#templateName#">
<cfif isDefined("form.FlagID")>
	<cfset frmFlagID = form.flagID>
<cfelseif isDefined("url.FlagID")>
	<cfset frmFlagID = url.flagID>
</cfif>
<CFPARAM NAME="frmAction" DEFAULT="">
<CFPARAM NAME="Msg" DEFAULT="">
<cfparam name="frmTitle" default="">
<cfparam name="frmLocation" default="">
<cfparam name="frmVenueRoom" default="">
<cfparam name="frmCountryID" default="">
<cfparam name="frmEventGroupID" default="">
<cfparam name="frmEstAttendees" default="">
<cfparam name="frmEmail" default="">
<cfparam name="frmEmailCC" default="">
<cfparam name="frmUseAgency" default="">
<cfparam name="frmInvitationType" default="">
<cfparam name="frmEventStatus" default="">
<cfparam name="frmAudienceType" default="">
<cfparam name="frmMaxTeamSize" default="1">
<!---<cfparam name="frmPartnerType" default="">--->
<cfparam name="frmEligibleCountryID" default="">
<CFPARAM NAME="frmPreferredStartDate" DEFAULT="#DateFormat(Now(), 'dd-mmm-yyyy')#">
<CFPARAM NAME="frmPreferredEndDate" DEFAULT="#DateFormat(Now(), 'dd-mmm-yyyy')#">
<cfparam name="frmAgenda" default="09:00 -
10:00 -
11:00 -
12:00 -
13:00 -
14:00 -
15:00 -
16:00 - ">


<!--- check what the user is allowed to do

SuperOK is allowed to add events.

--->

<CFSET ManagerOK = 0>
<CFSET SuperOK = 0>

<CFIF checkPermission.updateOkay GT 0><CFSET ManagerOK = 1></CFIF>
<CFIF checkPermission.addOkay GT 0><CFSET SuperOK = 1></CFIF>
<!--- 2012-07-25 PPB P-SMA001 commented out
<!--- get the countries the current user has rights to view
returns a list of CountryIDs as a variable called Variables.CountryList --->
<cfinclude template="/templates/qrygetcountries.cfm">
 --->
<cfparam name="dateSortOrder" default="desc">

<!--- RMB - 2013-09-16 - P-KAS026 - START --->
<CFINCLUDE TEMPLATE="../templates/qrygetcountries.cfm">
<CFQUERY NAME="CountriesAllowed" datasource="#application.siteDataSource#">
	SELECT
		CountryDescription,
		CountryID
	FROM
		Country
	WHERE
		CountryID  IN ( <cf_queryparam value="#Variables.CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	ORDER BY
		CountryDescription
</CFQUERY>
<!--- RMB - 2013-09-16 - P-KAS026 - END --->

<!--- get the events that the user has country rights to see --->
<CFQUERY NAME="getEvent" datasource="#application.siteDataSource#">
SELECT eg.EventName, eg.EventGroupID, ed.FlagID,
	ed.Title, ed.Location,
	ed.VenueRoom, ed.CountryID,
	ed.EstAttendees,ed.EventGroupID,
	ed.PreferredStartDate, ed.PreferredEndDate,
	ed.AllocatedStartDate,ed.AllocatedEndDate,
	isnull(ed.AllocatedStartDate,ed.PreferredStartDate) as StartDate,
	isnull(ed.AllocatedEndDate,ed.PreferredEndDate) as EndDate,
	ed.Email,ed.EmailCC,
	ed.UseAgency,ed.InvitationType,
	ed.EventStatus,p.FirstName, p.LastName,
	p.OfficePhone, ed.Date, c.ISOCode,
	p.fullname+ case when len(isNull(p.officePhone,'')) > 0 then '('+p.officePhone+')' else '' end as owner,
	<!--- NJH 2008/10/24 Sophos RelayWare Set Up Bug Fix Issue 1102 - am returning the count of those invited,registered and attending.--->
	(select count(*) from eventFlagData with (noLock) where regStatus='Invited' and flagID=ed.flagID) as noInvited,
	(select count(*) from eventFlagData with (noLock) where regStatus in ('RegistrationSubmitted','RegistrationStarted') and flagID=ed.flagID) as noRegistered,
	(select count(*) from eventFlagData with (noLock) where regStatus='Attended' and flagID=ed.flagID) as actualAttendees
	FROM
		EventDetail ed with (noLock)
		inner join EventGroup eg with (noLock) on eg.EventGroupID = ed.EventGroupID
		inner join Person p with (noLock) on p.PersonID = ed.OwnerID
		inner join country c with (noLock) on ed.CountryID = c.CountryID
	WHERE (select count(*) from eventcountry as ec with (noLock)
			where ec.flagid = ed.flagid
			<!--- 2012-07-25 PPB P-SMA001 commented out
			and ec.countryid  IN ( <cf_queryparam value="#Variables.CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			 --->
			#application.com.rights.getRightsFilterWhereClause(entityType="EventCountry",alias="ec").whereClause# 	<!--- 2012-07-25 PPB P-SMA001 --->
			) > 0
		<!--- PJP 2012-08-13:  Added in check for templatename because evenDiary should not restrict event status for drop down dates--->
		<cfif isDefined("session.showstatus") and templatename contains "eventlist.cfm">
			and ed.eventstatus  in ( <cf_queryparam value="#session.showstatus#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )
		</cfif>


<!--- RMB - 2013-09-16 - P-KAS026 - START --->
		<cfif StructKeyExists(session, "showeventCountryID") AND (session.showeventCountryID NEQ 0 AND  session.showeventCountryID NEQ '') AND
				templatename contains "eventlist.cfm">
			AND ed.CountryID = <cf_queryparam value="#session.showeventCountryID#" CFSQLTYPE="CF_SQL_INTEGER"  list="false"> OR
 		(select count(*) from eventcountry as ecf with (noLock)
			where ecf.flagid = ed.flagid AND
				ecf.CountryID = <cf_queryparam value="#session.showeventCountryID#" CFSQLTYPE="CF_SQL_INTEGER"  list="false">
			#application.com.rights.getRightsFilterWhereClause(entityType="EventCountry",alias="ecf").whereClause# 	<!--- 2012-07-25 PPB P-SMA001 --->
			) > 0
		</cfif>

<!--- RMB - 2013-09-16 - P-KAS026 - END --->
	ORDER BY
		eg.EventGroupID,ed.SortOrder,
		isnull(ed.AllocatedStartDate, ed.PreferredStartDate),
		ed.FlagID

</CFQUERY>

<!--- count the number of records available --->
<cfset thisrecord=0>
<cfset records=0>
<CFOUTPUT QUERY="getEvent" GROUP="EventGroupID">
	<cfoutput group="FlagID">
		<cfset records=records+1>
		<cfif frmFlagID is FlagID>
			<cfset thisrecord = records>
		</cfif>
	</cfoutput>
</cfoutput>

<!--- get the name that this template is included in --->

<Cfset thispage = listfirst(listlast(cgi.script_name,"/"),".")>
<cfif thispage contains "event">
	<cfset thispage = mid(thispage,6,len(thispage))>
</cfif>
<!--- if there are two words, split em --->
<cfset spacehere=refind('[a-z][A-Z]',thispage)>
<cfif spacehere gt 0>
	<cfset thispage=left(thispage,spacehere) & " " & mid(thispage,spacehere +1,len(thispage))>
</cfif>
<cfset thispage = ucase(thispage)>
<cfif right(thispage,3) is "WIZ">
	<cfset thispage = left(thispage,len(thispage) - 3)>
</cfif>

<!--- table to format the output --->
<div class="greyBackgroundInternalTopHead">
	<form name="tophead" action="" method="post">
	<CF_INPUT type="hidden" name="frmAction" value="#frmAction#">
	<cfif ShowNav or templatename contains "eventlist.cfm">
	<div class="actionEvents margin-bottom-15 col-sm-offset-8 pull-right">
		<select name="frmChooserRight" onChange="if(this.selectedIndex > 0) loadScreen(this)" class="form-control">
			<option value="">Select an action <span class="caret"></span>
			<cfif FlagID is "">
				<cfif session.AttendeeStats is "no">
					<option value="<cfoutput>#htmleditformat(templateName)#</cfoutput>?AttendeeStats=Yes">Display Attendee Stats
				<cfelse>
					<option value="<cfoutput>#htmleditformat(templateName)#</cfoutput>?AttendeeStats=No">Hide Attendee Stats
				</cfif>
				<option value="eventDiary.cfm" <cfif frmChooserRight contains "eventDiary.cfm">selected</cfif>>Diary view
				<!---<option value="eventGantt.cfm" <cfif frmChooserRight contains "eventGantt.cfm">selected</cfif>>Gantt view--->
				<option value="eventResourceEditor.cfm" <cfif frmChooserRight contains "eventResourceEditor.cfm">selected</cfif>>ResourceEditor
			</cfif>
			<cfif FlagID is not "">
				<option value="eventClone.cfm" <cfif templateName contains "eventClone.cfm">selected</cfif>>Copy this event
				<cfif templateName contains "eventWebContent.cfm">
					<option value="eventWebContent.cfm?frmAction=Agenda" <cfif frmAction contains "Agenda" or frmChooserRight contains "Agenda">selected</cfif>>Manage Agenda
					<option value="eventWebContent.cfm?frmAction=files" <cfif frmAction contains "files" or frmChooserRight contains "files">selected</cfif>>Manage Images/Documents
					<option value="eventWebContent.cfm?frmAction=emails" <cfif frmAction contains "emails" or frmChooserRight contains "emails">selected</cfif>>Manage Emails
				</cfif>
				</cfif>
			<cfif SuperOK>
				<option value="eventAddEvent.cfm" <cfif frmChooserRight contains "eventAddEvent.cfm">selected</cfif>>Add Event
			</cfif>
		</select>
	</div>
	</cfif>
	<cfif session.eventWizard and isDefined("GetEventDetails.title")>
	<div class="grey-box-top">
		<div class="row">
			<div class="form-group">
				<div class="col-xs-12 col-sm-6 subMenu">
					<span><cfoutput>#htmleditformat(getEventDetails.title)#</cfoutput></span>
				</div>
				<div class="col-xs-12 col-sm-6 subMenu">
					<!--- SWJ & MDC: 2007-02-14 switched off for the moment <a href="javascript:eventWizard(false)" CLASS="SubMenu">Use standard navigation</a> --->
						<a href="javascript:eventWizard(true)" CLASS="SubMenu">Use Event Wizard</a>
				</div>
			</div>
		</div>
	</div>
	</cfif>
	<cfif ShowNav or templatename contains "eventlist.cfm">
		<div class="grey-box-top">
			<div class="row">
				<div class="form-group">
					<div class="col-xs-12 col-sm-2 col-md-2">
						<a border=0 href="javascript:void(gotoRecord(-1))" class="btn btn-primary"><< Previous event</a>
					</div>
					<div class="col-xs-12 col-sm-4 col-md-6">
						<select name="FlagID" onchange="loadScreen(this)" class="form-control">
							<CFOUTPUT QUERY="GetEvent" GROUP="EventGroupID">
								<!--- START: NYB 2009-09-28 LHID2672 - added left and cfif len > eventDropDownSize --->
								<option value="">#left(EventName,eventDropDownSize)# <cfif len(EventName) gt eventDropDownSize>...</cfif>
								<cfoutput group="FlagID">
									<cfset titleVar = "&nbsp;&nbsp;&nbsp; #Title# #dateformat(PreferredStartDate,'dd-mmm-yyyy')#">
									<option value="#FlagID#" <cfif frmFlagID is FlagID>selected</cfif>>#left(titleVar,eventDropDownSize)# <cfif len(titleVar) gt eventDropDownSize>...</cfif>
								</cfoutput>
								<!--- END: NYB 2009-09-28 LHID2672 --->
							</cfoutput>
						</select>
					</div>
					<div class="col-xs-12 col-sm-4 col-md-2" id="eventTopHeadRecord">
						<cfoutput>#htmleditformat(thisrecord)# of #htmleditformat(records)#</cfoutput>
					</div>
					<div class="col-xs-12 col-sm-2 col-md-2 text-align-right">
						<a border=0 href="javascript:void(gotoRecord(1))" class="btn btn-primary">Next event >></a>
					</div>
				</div>
			</div>
		</div>
	</cfif>

	<cfif isdefined("IsOwner")>
		<cfif not IsOwner>
			<cfset ShowSave="No">
		</cfif>
	</cfif>

	<!--- NJH 2016/01/27 JIRA BF-187 - don't show grey header area if it's going to be empty --->
	<cfif ShowNav or templatename contains "eventlist.cfm" or ShowSave or cgi.script_name contains "eventList.cfm">
	<div class="grey-box-top">
		<div class="row">
			<div class="form-group">
				<cfif ShowNav or templatename contains "eventlist.cfm">
					<div class="col-xs-12 col-sm-4">
						<select name="frmChooserLeft" onchange="if(this.selectedIndex == 0) this.form.FlagID.selectedIndex = 0; loadScreen(this)" class="form-control">
							<option value="eventList.cfm" <cfif frmChooserLeft contains "eventList.cfm">selected</cfif>>Events List
							<option value="eventDetails.cfm" <cfif frmChooserLeft contains "eventDetails.cfm">selected</cfif>>Event Details
							<option value="eventDelegateList.cfm" <cfif frmChooserLeft contains "eventDelegateList.cfm">selected</cfif>>Delegate List
							<option value="eventWebContent.cfm" <cfif frmChooserLeft contains "eventWebContent.cfm">selected</cfif>>Agenda
							<!--- NJH 2011/10/24 LID 5611 - removed this option as not sure what it's supposed to be doing and it does't appear to be doing anything at all
							<option value="javascript:ManageEventManagers()" <cfif frmChooserLeft contains "javascript:ManageEventManagers()">selected</cfif>>Event Managers --->
						</select>
					</div>
					<div class="col-xs-12 col-sm-4">
						<CFIF CountriesAllowed.RecordCount GT 0>
							<SELECT NAME="frmCountryID" onchange="javascript:document.tophead.action='EventList.cfm'; document.tophead.frmChooserLeft.selectedIndex=0; document.tophead.submit()" class="form-control">
									<option value="0">phr_EventsFilterByCountry
									<option value="0">phr_EventsFilterAllCountries
								<CFOUTPUT QUERY="CountriesAllowed">
									<OPTION VALUE="#CountryID#">#htmleditformat(CountryDescription)#
								</CFOUTPUT>
							</SELECT>
						</CFIF>
					</div>
				</cfif>
				<div class="col-xs-12 col-sm-4">
					<cfif ShowSave><a href="javascript:void(submitForm())" border="0" Class="Button">Save this record</a></cfif>
					<cfif cgi.script_name contains "eventList.cfm">
						<!--- display some checkboxes to limit the list. --->
						<label class="checkbox-inline">
							<input type="checkbox" name="showstatus" value="Initial" <cfif session.showstatus contains "Initial">checked</cfif>> Initial
						</label>
						<label class="checkbox-inline">
							<input type="checkbox" name="showstatus" value="Agreed" <cfif session.showstatus contains "Agreed">checked</cfif>> Agreed
						</label>
						<label class="checkbox-inline">
							<input type="checkbox" name="showstatus" value="Complete" <cfif session.showstatus contains "Complete">checked</cfif>>Complete
						</label>
						<!--- <a class="checkbox-inline" href="javascript:document.tophead.action='EventList.cfm'; document.tophead.frmChooserLeft.selectedIndex=0; document.tophead.submit()">Refresh</a> --->
					</cfif>
				</div>
			</div>
		</div>
	</div>
	</cfif>
</form>

	<!--- any template specific selection criteria can go in here --->
<cfswitch expression="#templateName#">
	<cfcase value="eventDiary.cfm">
		<!--- find out the first date and last date an event is booked in the date column
		--->
		<cfset firstDate=now()>
		<cfset firstDate= createDate(year(firstDate), month(firstDate), 1)>
		<cfset lastStartDate=ArrayMax(ListToArray(ValueList(getEvent.StartDate)))>
		<cfset lastDate= createDate(year(lastStartDate), month(lastStartDate), DaysInMonth(lastStartDate))>
		<cfparam name="dateTo" default="#lastDate#">

		<SCRIPT type="text/javascript">
			// make sure the first date is greater than or equal to the last date
			function formCheck(aform)
			{
				if(aform.dateFrom.selectedIndex > aform.dateTo.selectedIndex)
				{
					alert("the 'From' date must be less than or equal to the 'To' date!")
					return false
				}
				return true
			}
		</script>

		<!--- now create a select, starting from the first month and work forward until the last month is arrived at --->
		<form name="DateChooser" action="eventDiary.cfm" method="post" onSubmit="return formCheck(this)">
		<CF_INPUT type="hidden" name="FlagID" value="#FlagID#">
		<CF_INPUT type="hidden" name="frmChooserLeft" value="#frmChooserLeft#">
		<CF_INPUT type="hidden" name="frmChooserRight" value="#frmChooserRight#">


			<div class="grey-box-top">
				<div class="row">
					<div class="form-group">
						<div class="col-xs-12 col-sm-5">
							<label>Choose a month to view events from:</label>
							<cfset aMonth = firstDate>
							<select name="dateFrom" class="form-control">
								<cfloop condition="datecompare(aMonth, lastDate) lt 1">
									<cfoutput><option value="#aMonth#" <cfif aMonth is dateFrom>selected</cfif>>#dateFormat(aMonth,"mmm-yyyy")#</cfoutput>
									<cfset aMonth=dateAdd("m",1,aMonth)>
								</cfloop>
							</select>
						</div>
						<div class="col-xs-12 col-sm-5">
							<label>to:</label>
							<cfset aMonth=createdate(year(firstDate), month(firstDate), daysInMonth(firstDate))>
							<select name="dateTo" class="form-control">
								<cfloop condition="datecompare(aMonth, lastDate) lt 1">
									<cfoutput><option value="#aMonth#" <cfif aMonth is dateTo>selected</cfif>>#dateFormat(aMonth,"mmm-yyyy")#</cfoutput>
									<cfset aMonth=dateAdd("m",1,aMonth)>
									<cfset aMonth=createDate(year(aMonth), Month(aMonth), daysInMonth(aMonth))>
								</cfloop>
							</select>
						</div>
						<div class="col-xs-12 col-sm-2" id="chooseEventsBtn">
							<input type="submit" value="Refresh" class="btn btn-primary">
							<!--- SSS 2009/05/07 S-01084 add a button to allow to go back if you have come from the eventlist page --->
							<cfif comparenocase(HTTP_REFERER, "http://dev-cfint/index.cfm?") NEQ 0>
								<input type="button" value="back" onclick="javascript:goBack('eventList.cfm');" class="btn btn-primary">
							</cfif>
						</div>
					</div>
				</div>
			</div>
		</form>
	</cfcase>
		<cfcase value="eventDelegateList.cfm">
<!--- 			<table border="0" width="100%" cellpadding="0" cellspacing="0">
			<tr>
			<FORM NAME="search" ACTION="eventDelegateList.cfm" METHOD="post">
			<CFOUTPUT>
			<INPUT TYPE="Hidden" NAME="FlagID" VALUE="#FlagID#">
			<INPUT TYPE="Hidden" NAME="filter" VALUE="#filter#">
			<INPUT TYPE="Hidden" NAME="criteria" VALUE="#criteria#">
			<INPUT TYPE="Hidden" NAME="countryID" VALUE="#countryID#">
			<INPUT TYPE="Hidden" NAME="countryGroupID" VALUE="#countryGroupID#">
			<INPUT TYPE="Hidden" NAME="frmOrigStart" VALUE="#Start#">
			<INPUT TYPE="Hidden" NAME="frmTotalChecks" VALUE="#frmTotalChecks#">
			<INPUT TYPE="Hidden" NAME="frmEventTable" VALUE="#EventTable#">
			<INPUT TYPE="Hidden" NAME="frmAction" VALUE="">
			<INPUT TYPE="Hidden" NAME="frmAllCheckIDs" VALUE="#frmAllCheckIDs#">
			</CFOUTPUT>
			<TD valign="top" ALIGN="left">
				Order by <br>
				<SELECT NAME="frmOrder" onChange="this.form.submit();">
					<OPTION VALUE="LastName" <CFIF frmOrder IS "LastName"> SELECTED </CFIF> >Last Name
					<OPTION VALUE="SiteName" <CFIF frmOrder IS "SiteName"> SELECTED </CFIF> >Site
					<OPTION VALUE="Country" <CFIF frmOrder IS "Country"> SELECTED </CFIF> >Country
					<OPTION VALUE="RegStatus" <CFIF frmOrder IS "RegStatus"> SELECTED </CFIF> >Status
				</SELECT>
			</TD>
			<td valign="top" ALIGN="LEFT">
				Search for a Name<br>
				<INPUT type="Text" name="srchName" value="<CFOUTPUT>#srchName#</cfoutput>" size="20" >
				<input type="Submit" value="Go">
				<BR>(any sort) or email

			</td>

			<td valign="top" ALIGN="LEFT">
				Search for a Reg ID<br>
				<INPUT type="Text" name="srchRegID" value="<CFOUTPUT>#srchRegID#</cfoutput>" size="20">
				<input type="Submit" value="Go" >
			</td>

			<td valign="top" ALIGN="LEFT" >
				<cfquery name="statuses" datasource="#application.siteDataSource#">
					select distinct
						regstatus
					from
						eventflagdata
					where
						flagid = #flagID#
				</cfquery>
				<cfparam name="frmRegStatus" default="">
				Filter by Status
				<select name="frmRegStatus" onChange="this.form.submit();">
					<option value="">Choose one
					<option value="">Don't filter by Status
					<cfoutput query="statuses">
						<option value="#regstatus#" <cfif frmRegStatus is  "#regstatus#">selected</cfif>>#regstatus#
					</cfoutput>
				</select>
			</td>

			<td valign="top" ALIGN="LEFT"">
				<cfquery name="focusLevels" datasource="#application.siteDataSource#">
					SELECT
						f.Name as focusLevel
					FROM
						Flag as f
						INNER JOIN FlagGroup as fg ON f.FlagGroupID = fg.FlagGroupID
						<!--- hack added for Valerie Vliet ---->
						and fg.FlagGroupTextID in ('#thePartnerType#' <cfif flagid is 1236 or flagid is 1237>,'nppPartnerType'</cfif>)
				</cfquery>
				<cfparam name="frmFocusLevel" default="">
				Filter by Focus level
				<select name="frmFocusLevel" onChange="this.form.submit();">
					<option value="">Choose one
					<option value="">Don't filter by Focus level
					<cfoutput query="focuslevels">
						<option value="#focusLevel#" <cfif frmFocusLevel is  "#focusLevel#">selected</cfif>>#focusLevel#
					</cfoutput>
				</select>
			</td>
			</form>
			</td>
			</tr>
			</table>
 --->
		</cfcase>
	</cfswitch>
</div>
<!--- SSS 2009/05/07 add a function to go back S-01084 --->
<script>
	function goBack (url){
		location.replace(url);
	}
</script>

