<!--- �Relayware. All Rights Reserved 2014 --->

<!--- report of modifications made by a set of users between two dates --->
<CFSETTING showdebugoutput="no">
<cfif isDefined("Countryid")>
<cfquery name="getuserlist" datasource="#application.siteDataSource#">
select name,usergroupid from usergroup, person,location
where usergroup.personid=person.personid
and person.locationid=location.locationid
and location.countryid=9
</cfquery>

</cfif>


<CFSET userlist = "#valuelist(getuserlist)#">
<CFPARAM name = "frmUsers" default = "#userlist#">
<CFPARAM name = "startDate" default="#createODBCdateTime('1999-01-09')#">  
<CFPARAM name = "endDate"  default = "#now()#">


	<CFQUERY NAME="getUsers" datasource="#application.sitedatasource#">
	Select usergroupid , Name
	from usergroup 
	where usergroupid  in ( <cf_queryparam value="#userList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	order by name
	</CFQUERY>



<FORM action="stevedossett.cfm" method="POST">
<CFOUTPUT>
<TABLE>
<TR>
	<TD colspan="2">Modifications made on #request.CurrentSite.Title# between 
		<CF_INPUT type="text" value="#dateformat(startdate,"dd-mm-yy")#" name="startdate" size = "8" maxlength="8">
		<INPUT type="hidden" name="startdate_eurodate">
		  and 
		<CF_INPUT type="text" value="#dateformat(enddate,"dd-mm-yy")#" name="enddate">
		<INPUT type="hidden" name="enddate_eurodate">
			by
	</td>
</tr>

	<CFLOOP Query="getUsers">
		<TR>
			<TD>&nbsp;</td><TD><CF_INPUT type = "checkbox" name= "frmUsers" value="#usergroupid#"CHECKED="#iif( listcontains(frmUsers,usergroupid),true,false)#" > #htmleditformat(Name)# </td>
		</tr>
	</cfloop>

<TR><TD colspan="2">

</CFOUTPUT>
	<INPUT type="submit" value="Run report" name="runReport">
</td></tr>
</table>	
</form>
<CFIF not isdefined("runReport")><CF_ABORT></cfif>

<TABLE>

	<TR><TD><B>Totals for each table</b></td></tr>	
<CFLOOP index="table" list="Person,Location">


	<!--- get total additions and deletions for each table --->
	<CFLOOP index="Action" list="Addition,Deletion"> 
		<CFQUERY NAME="getAddDelete" datasource="#application.sitedatasource#">
		SELECT distinct recordid
 		FROM modregister left outer join modentitydef on modregister.modentityid = modentitydef.modentityid
		WHERE actionbycf  in ( <cf_queryparam value="#frmusers#" CFSQLTYPE="CF_SQL_Integer"  list="true"> )
		AND left(Action,2) =  <cf_queryparam value="#left(table,1)##left(action,1)#" CFSQLTYPE="CF_SQL_VARCHAR" > 
		AND moddate >  <cf_queryparam value="#startDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
		AND moddate <  <cf_queryparam value="#endDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
		</CFQUERY>	

		<CFOUTPUT >
			<TR><TD>#htmleditformat(Table)# #htmleditformat(action)# </TD><TD>#getAddDelete.recordcount#  </td></tr>	
		</cfoutput>

		
	</CFLOOP>


		<!--- get total mods for each table (and associated flags)--->
		<CFQUERY NAME="getMods" datasource="#application.sitedatasource#">
		SELECT distinct recordid
 		FROM modregister left outer join modentitydef on modregister.modentityid = modentitydef.modentityid
		WHERE actionbycf  in ( <cf_queryparam value="#frmUsers#" CFSQLTYPE="CF_SQL_Integer"  list="true"> )
		AND left(Action,2) =  <cf_queryparam value="#left(table,1)#M" CFSQLTYPE="CF_SQL_VARCHAR" > 
		AND moddate >  <cf_queryparam value="#startDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
		AND moddate <  <cf_queryparam value="#endDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
	
	 	UNION 

		SELECT distinct recordid
		FROM modregister ,flag , flaggroup, flagentitytype
		WHERE actionbycf  in ( <cf_queryparam value="#frmUsers#" CFSQLTYPE="CF_SQL_Integer"  list="true"> )
		AND modregister.flagid= flag.flagid
		AND flag.flaggroupid = flaggroup.flaggroupid 
		AND flaggroup.entitytypeid = flagentitytype.entitytypeid
		AND Action in ('FA','FM')  			<!--- not doing FD - tends to show spurious results of deletions --->
		AND upper(left(tableName,1)) =  <cf_queryparam value="#left(table,1)#" CFSQLTYPE="CF_SQL_VARCHAR" >  
		AND moddate >  <cf_queryparam value="#startDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
		AND moddate <  <cf_queryparam value="#endDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 

		</CFQUERY>

		<CFOUTPUT >
		<TR><TD>#htmleditformat(Table)# Modification </TD><TD>#getMods.recordcount#   </td></tr>	
		</cfoutput>


</cfloop>

	<TR><TD>&nbsp;</td></tr>	
	<TR><TD><B>Breakdown by field / flag</b></td></tr>	

<!--- get breakdown of mods by field --->
<CFLOOP index="table" list="Person,Location">

 
	<CFQUERY NAME="getFieldMods" datasource="#application.sitedatasource#">
	SELECT modentitydef.fieldname as field, count(distinct recordid) as countof
 	FROM modregister left outer join modentitydef on modregister.modentityid = modentitydef.modentityid
	WHERE actionbycf  in ( <cf_queryparam value="#frmUsers#" CFSQLTYPE="CF_SQL_Integer"  list="true"> )
	AND (left(Action,2) =  <cf_queryparam value="#left(table,1)#A" CFSQLTYPE="CF_SQL_VARCHAR" >  or left(Action,2) =  <cf_queryparam value="#left(table,1)#M" CFSQLTYPE="CF_SQL_VARCHAR" > )
	AND moddate >  <cf_queryparam value="#startDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
	AND moddate <  <cf_queryparam value="#endDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
	group by modentitydef.fieldname
	</CFQUERY>	

	<CFQUERY NAME="getFlagMods" datasource="#application.sitedatasource#">
	SELECT flaggroup.name as flaggroupname, flag.name as flagname, count(distinct recordid) as countof
	FROM modregister ,flag , flaggroup, flagentitytype
	WHERE actionbycf  in ( <cf_queryparam value="#frmUsers#" CFSQLTYPE="CF_SQL_Integer"  list="true"> )
	AND modregister.flagid= flag.flagid
	AND flag.flaggroupid = flaggroup.flaggroupid 
	AND flaggroup.entitytypeid = flagentitytype.entitytypeid
	AND Action in ('FA','FM')  			<!--- not doing FD - tends to show spurious results of deletions --->
	AND upper(left(tableName,1)) =  <cf_queryparam value="#left(table,1)#" CFSQLTYPE="CF_SQL_VARCHAR" >  
	AND moddate >  <cf_queryparam value="#startDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
	AND moddate <  <cf_queryparam value="#endDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
	group by flaggroup.name , flag.name 
	order by flaggroup.name , flag.name 
	</CFQUERY>	



		<CFOUTPUT >

		</cfoutput>

	<CFOUTPUT query="getFieldMods">
		<TR><TD>#htmleditformat(Table)# #htmleditformat(field)#  </TD><TD>#htmleditformat(countof)#   </td></tr>
	</cfoutput>

	<CFOUTPUT query="getFlagMods">
		<TR><TD>#htmleditformat(Table)# Flag: #htmleditformat(flaggroupname)#: #htmleditformat(flagname)# </TD><TD>#htmleditformat(countof)#   </td></tr>
	</cfoutput>

	
</cfloop>



	<CFQUERY NAME="getLogins" datasource="#application.sitedatasource#">
		select (select username from person where person.personid = usage.personid) as name, 
		count(logindate) as countof
		from usage 
		where personid in (select personid from usergroup where usergroupid in (50,185,186,277,287,419,476,500,537,540,571,704,718,803,804,806,808))
		AND logindate >  <cf_queryparam value="#startDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
		AND logindate <  <cf_queryparam value="#endDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
		group by  personid
	</CFQUERY>	

	<TR><TD>&nbsp;</td></tr>	
	<TR><TD><B>Usage</b></td></tr>	
		
	<CFOUTPUT query="getLogins">
		<TR><TD>#htmleditformat(name)#  </TD><TD>#htmleditformat(countof)#   </td></tr>
	</cfoutput>

	
</table>

