<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File:			WhatsLeftToDo.cfm
Author:			DJH
Date created:	14 March 2000

Description:	Displays a list of bullets of the things that still need to be done with an event - implemented as a custom tag

Usage:			<cf_WhatsLeftToDo FlagID="" Query="">

FlagID:	This is the FlagID for the event in question
Query: (optional) if defined, gives the name of the query to be created containing the outstanding items for the event

Version history:
1
Mods:
WAB 2001-06-11  altered by WAB so that just doesn't bring back all the data which isn't needed
DCC	25/4/2014  	Added hook for custom
SB 	2015/12/07 	SB Bootstrap and removed tables

--->
<cfif fileexists("#application.paths.code#\cftemplates\Modules\events\customWhatsLeftToDo.cfm")>
 	<cfinclude template="/Code/CFTemplates/Modules/events/customWhatsLeftToDo.cfm">
<cfelse>
	<!--- make sure the minimum info was supplied --->
	<cfif not isDefined("attributes.FlagID")>
		<cfexit>
	</cfif>

	<!--- possible items in the list are described here (please keep this up to date!):

	Choose Event Managers:
		Displays the event managers screen in the current window - is considered completed if one or more
		event managers have been added already.

	Add Delegates:
		If "By Invitation only",  displays the delegate list screen in the current window. Considered completed if selections
		have been added.

	Send Delegates Invites:
		If "By Invitation only" and delegates have been added, displays the delegate list screen/sends the invites - not sure yet.
		Considered completed if all delegates have been sent an invite.

	Add Agenda:
		Displays the agenda portion of the web content screen. Considered completed if the agenda exists.

	Add Collateral:
		Displays the file upload portion of the web content screen. Considered complete if one or more files have been uploaded

	Add Emails:
		Displays the emails portion of the web content screen. Considered complete if one or more emails have been created.

	--->


	<cfset caller.ByInvitationOnly = false>
	<cfset caller.UseAgency = false>

	<!--- now do the queries here to help decide what has been/not been completed. --->

	<cfif isDefined("caller.GetEventDetails.recordcount")>
		<cfset GetEventDetails = caller.GetEventDetails>
	<cfelse>
		<cfset flagID = attributes.FlagID>
		<cfinclude template="sql_GetEventDetails.cfm">
	</cfif>

	<cfset ChooseEventManagersComplete = false>
	<!--- do a query to see whether more than one event manager has been set up - pinched straight from ../templates/entitymanager.cfm --->
	<cfset RecordID = caller.FlagID>
	<cfset Entity = "Flag">
	<cfset Shortname = "EventTask">
	<CFQUERY NAME="getSecurityTypeID" datasource="#application.siteDataSource#">
		SELECT SecurityTypeID
		FROM SecurityType
		WHERE ShortName =  <cf_queryparam value="#ShortName#" CFSQLTYPE="CF_SQL_VARCHAR" >
	</CFQUERY>

	<CFSET SecurityTypeID = getSecurityTypeID.SecurityTypeID>
	<!--- PKP 2008-03-03 TREND BUG 147 Changed below query to return the same username list as on the ../templates/entitymanager.cfm page to get the correct record count being returned  --->
	<CFQUERY NAME="GetAssigned" DATASOURCE="#caller.application.SiteDataSource#">
	SELECT max(p.username) AS Name, rr.UserGroupID  , max(r.Permission) AS Permission
	FROM RecordRights AS rr
	left join  RightsGroup AS rg on rr.UserGroupID = rg.UserGroupID
	left join person p on p.personid = rg.personid
	left join  RightsGroup AS rg2 on rg.personid = rg2.personid
	left  join Rights AS r  on r.UserGroupID = rg2.UserGroupID
	WHERE rr.RecordID =  <cf_queryparam value="#RecordID#" CFSQLTYPE="CF_SQL_INTEGER" >  AND rr.Entity =  <cf_queryparam value="#Entity#" CFSQLTYPE="CF_SQL_VARCHAR" >    AND r.SecurityTypeID =  <cf_queryparam value="#SecurityTypeID#" CFSQLTYPE="CF_SQL_INTEGER" >

		GROUP BY rr.UserGroupID
	ORDER BY name
	</CFQUERY>
	<cfif GetAssigned.recordcount gt 1>
		<cfset ChooseEventManagersComplete = true>
	</cfif>



	<cfset AddDelegatesComplete = false>
	<!--- do a query to see whether delegates have been added yet pulled straight out of
	eventDelegateList.cfm --->
	<!--- get the countries the current user has rights to view
	returns a list of CountryIDs as a variable called Variables.CountryList --->

	<cfset CreateCountryRightsTable = true>    <!--- WAB added --->
	<cfinclude template="/templates/qrygetcountries.cfm">
	<cfset CreateCountryRightsTable = false>

	<!--- get the name of the event table --->
	<CFQUERY NAME="GetEventTable" DATASOURCE="#caller.application.SiteDataSource#">
		SELECT
			ft.DataTable,
			f.FlagTextID
		FROM
			Flag AS f,
			FlagGroup AS fg,
			FlagType AS ft
		WHERE
			f.FlagGroupID = fg.FlagGroupID
			AND fg.FlagTypeID = ft.FlagTypeID
			AND f.FlagID =  <cf_queryparam value="#attributes.FlagID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</CFQUERY>
	<CFSET EventTable = "#GetEventTable.DataTable#FlagData">

	<!--- get the delegate list --->
	<!--- (altered by WAB so that just doesn't bring back all the data which isn't needed)   --->
	<CFQUERY NAME="GetLists" datasource="#application.siteDataSource#">
		select
			personid
		from
			#EventTable# AS et
			inner join
			Person AS p on p.personid = et.entityid
			inner join
			Location as l on l.locationid= p.locationid
			inner join
			#countryRightsTableName# AS R
			ON L.countryid = r.countryid

		where
		et.FlagID =  <cf_queryparam value="#attributes.FlagID#" CFSQLTYPE="CF_SQL_INTEGER" >

	</CFQUERY>


	<cfif getlists.recordcount gt 0>
		<cfset AddDelegatesComplete = true>
	</cfif>


	<cfset SendDelegatesInvitesComplete = false>




	<cfset AddAgendaComplete = false>
	<cfif GetEventDetails.Agenda is not "">
		<cfset AddAgendaComplete = true>
	</cfif>



	<cfset AddCollateralComplete = false>
	<!--- check to see whether some collateral has been uploaded --->
	<!--- <cfparam name="Path" default="#caller.path#">
	<cfparam name="sitedomain" default="#caller.sitedomain#">
	<cfparam name="application.SiteDataSource" default="#caller.application.SiteDataSource#"> --->
	<cf_relatedfile action="list" noupdate="true" query="Collateral" entity="#attributes.FlagID#" entityType="event">
	<cfif Collateral.recordcount gt 0>
		<Cfset AddCollateralComplete = true>
	</cfif>

	<cfset AddEmailsComplete = false>
	<cfquery name="checkEmails" datasource="#application.siteDataSource#">
		select
			FlagID
		from
			EventDetailEmail
		where
			FlagID =  <cf_queryparam value="#attributes.FlagID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</cfquery>
	<cfif checkEmails.recordcount gt 0>
		<cfset AddEmailsComplete = true>
	</cfif>

	<cfset AddResourcesComplete = false>
	<cfquery name="checkResources" datasource="#caller.application.SiteDataSource#">
		select
			ResourceID
		from
			ResourceBooking
		where
			FlagID =  <cf_queryparam value="#attributes.FlagID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</cfquery>
	<cfif checkResources.recordcount gt 0>
		<cfset AddResourcesComplete=true>
	</cfif>


	<cfif GetEventDetails.InvitationType is "Open">
		<cfset caller.ByInvitationOnly = false>
	<cfelse>
		<cfset caller.ByInvitationOnly = true>
	</cfif>

	<cfset caller.UseAgency = GetEventDetails.UseAgency>

	<cfif isdefined("attributes.Query")>
		<!--- return a query rather than display output --->
	<cfelse>

		<SCRIPT type="text/javascript">
			function whatsLeftLoader(theLocation)
			{
				var seconds = new Date()
				var form = document.UpdateEvent
				if (!form) form = document.eventStep
				if (form.dirty.value==1)
				{
					if (confirm("Warning: You have changed some data. If you want to change screens now, any data will be lost. If you are sure you are happy with this click OK. Otherwise click cancel."))
					{
						window.location.href = theLocation + "&dummy=" + seconds.getTime()
					}
				}
				else
				{
					window.location.href = theLocation + "&dummy=" + seconds.getTime()
				}
			}
		</script>
	<div class="row">
		<!--- display the Whats Left box --->
		<div id="actionsForEvent" class="col-xs-12 col-sm-4 col-md-3 col-lg-2">
			<div class="grey-box-top">
				<h3>Actions for this event</h3>
				<cfif caller.UseAgency><div align="right" class="smaller">(Agency managed event)</div></cfif>
				<cfoutput>
					<ul>
						<li><a class="smaller" href="javascript: whatsLeftLoader('eventDetails.cfm?eventStep=1&FlagID=#attributes.FlagID#&displaysave=yes')">Basic event details<img border=0 src="/images/MISC/navytick.gif" width="10" height="10"></a>
						<li><a class="smaller" href="javascript: whatsLeftLoader('eventManager.cfm?RecordID=#attributes.FlagID#&Entity=Flag&ShortName=EventTask&FlagID=#attributes.FlagID#')">Set event managers<cfif ChooseEventManagersComplete><img border=0 src="/images/MISC/navytick.gif" width="10" height="10"></cfif></a></li>

	<!--- SWJ & MDC: 2007-02-14 switched off for the moment				<a class="smaller" href="javascript: whatsLeftLoader('eventAddDelegateWiz.cfm?eventStep=3&FlagID=#attributes.FlagID#')">Add delegates you wish to invite<cfif AddDelegatesComplete><img border=0 src="/images/MISC/navytick.gif" width="10" height="10"></cfif></a>&nbsp;&nbsp;<br> --->
					<cfif AddDelegatesComplete>
						<li><a class="smaller" href="javascript: whatsLeftLoader('eventDelegateList.cfm?eventStep=3&FlagID=#attributes.FlagID#')">View Delegates<cfif SendDelegatesInvitesComplete><img border=0 src="/images/MISC/navytick.gif" width="10" height="10"></cfif></a></li>
					</cfif>
					<li><a class="smaller" href="javascript: whatsLeftLoader('eventWebContent.cfm?frmAction=editAgenda&eventStep=4&FlagID=#attributes.FlagID#')">Edit Web Popup Agenda<cfif AddAgendaComplete><img border=0 src="/images/MISC/navytick.gif" width="10" height="10"></cfif></a></li>
					<li><a class="smaller" href="javascript: whatsLeftLoader('eventWebContent.cfm?frmAction=Emails&eventStep=5&FlagID=#attributes.FlagID#')">Edit Confirmation email<cfif AddEmailsComplete><img border=0 src="/images/MISC/navytick.gif" width="10" height="10"></cfif></a></li>
					<!--- SWJ & MDC: 2007-02-14 switched off for the moment <a class="smaller" href="javascript: whatsLeftLoader('eventWebContent.cfm?frmAction=Files&eventStep=6&FlagID=#attributes.FlagID#')">Add collateral<cfif AddCollateralComplete><img border=0 src="/images/MISC/navytick.gif" width="10" height="10"></cfif></a>&nbsp;&nbsp;<br>
					<a class="smaller" href="javascript: whatsLeftLoader('eventResourceBookings.cfm?eventStep=7&FlagID=#attributes.FlagID#')">Add resources<cfif AddResourcesComplete><img border=0 src="/images/MISC/navytick.gif" width="10" height="10"></cfif></a>&nbsp;&nbsp;<br> --->
					</ul>
				</cfoutput>
			</div>
		</div>
	</cfif>
</cfif>