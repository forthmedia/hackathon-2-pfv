<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
WAB 2009-09-07 LID 2515 Removed #application. userfilespath# , implemented getTemporaryFolderdetails() .  Unable to test
--->

<CFIF NOT IsDefined("FlagID")>
	<cflocation url="eventHome.cfm"addToken="false">
	<CF_ABORT>
<CFELSEIF NOT IsNumeric(FlagID)>
	<cflocation url="eventHome.cfm"addToken="false">
	<CF_ABORT>
</CFIF>

<CFQUERY NAME="GetEventTable" datasource="#application.siteDataSource#">
	SELECT 
		ft.DataTable, 
		f.FlagTextID
	FROM 
		Flag AS f, 
		FlagGroup AS fg, 
		FlagType AS ft
	WHERE 
		f.FlagGroupID = fg.FlagGroupID
		AND fg.FlagTypeID = ft.FlagTypeID
		AND f.FlagID =  <cf_queryparam value="#FlagID#" CFSQLTYPE="CF_SQL_INTEGER" > 
</CFQUERY>

<CFSET EventTable = "#GetEventTable.DataTable#FlagData">

<CFIF IsDefined("form.frmCountryID")>
	<CFQUERY NAME="getAnalysisDataTotal" datasource="#application.siteDataSource#">
		SELECT 
			Count(*) AS Total
		FROM 
			#EventTable# AS efd 
		WHERE 
			efd.FlagID =  <cf_queryparam value="#FlagID#" CFSQLTYPE="CF_SQL_INTEGER" > 
			AND efd.CountryID =  <cf_queryparam value="#ListFirst(frmCountryID)#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</CFQUERY>

	<CFSET Total = getAnalysisDataTotal.Total>

	<CFQUERY NAME="getAnalysisData" datasource="#application.siteDataSource#">
		SELECT 
			efd.#frmAnalysis# AS Analysis, 
			Count(efd.#frmAnalysis#) AS EachTotal, 
			Fraction=(Convert(Real, Count(efd.#frmAnalysis#))/#Total#)*100
		FROM 
			#EventTable# AS efd
		WHERE 
			efd.FlagID =  <cf_queryparam value="#FlagID#" CFSQLTYPE="CF_SQL_INTEGER" > 
			AND efd.CountryID =  <cf_queryparam value="#ListFirst(frmCountryID)#" CFSQLTYPE="CF_SQL_INTEGER" > 
		GROUP BY 
			efd.#frmAnalysis#
	</CFQUERY>
	<CFSET ColoursList = "000000,FF0000,FFFF00,0000FF,008080,008000,808000,800000,00FF00,00FFFF,000080,FF00FF,800080,F0E68C,B0E0E6,FF69B4,7B68EE,191970,CD5C5C,FAFAD2,B8860B,F5DEB3,66CDAA,D2B48C">
	<CFSET Colours = "">
	<CFLOOP FROM="1" TO="#getAnalysisData.RecordCount#" INDEX="theIndex">
		<CFSET Colours = ListAppend(Colours, ListGetAt(ColoursList,theIndex))>
	</CFLOOP>
	
	<cfset TemporaryFolderdetails = application.com.filemanager.getTemporaryFolderDetails()>
	
	<CFX_PIECHART
	    WIDTH=400
	    HEIGHT=400
	    ITEMS="#ValueList(getAnalysisData.Analysis)#"
	    VALUES="#ValueList(getAnalysisData.EachTotal)#"
	    COLORS="#Colours#"
	    TITLE="#frmTitle#"
	    TitleFontName="Arial"
	    TitleFontHeight="10"
	    ShowLegend="Yes"
	    LegendFontName="Arial"
	    LegendFontHeight="12"
	    DrawBorders="No"
	    ShowDateTime="Yes"
	    BackgroundColor="ffffff"
	    FontColor="040404"
		Quality=80
	    OutputJPG="#TemporaryFolderdetails.path##FlagID##frmAnalysis#.jpg">
</CFIF>

<cfinclude template="sql_GetEventDetails.cfm">

<CFQUERY NAME="getEventCountry" datasource="#application.siteDataSource#">
	SELECT 
		ec.CountryID, 
		c.CountryDescription
	FROM 
		eventCountry AS ec, 
		Country AS c
	WHERE 
		ec.FlagID =  <cf_queryparam value="#FlagID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		AND ec.CountryID = c.CountryID
</CFQUERY>

<CFQUERY NAME="getEventAnalysis" datasource="#application.siteDataSource#">
	SELECT DISTINCT 
		vfv.FieldName
	FROM 
		ValidFieldValues AS vfv
	WHERE 
		vfv.InAnalysis = 1
		AND FieldName  LIKE  <cf_queryparam value="flag.#GetEventTable.FlagTextID#%" CFSQLTYPE="CF_SQL_VARCHAR" > 
</CFQUERY>

<CFSCRIPT>
	AnalysisNames = StructNew();
	StructInsert(AnalysisNames,"Regstatus","Registration Status");
	StructInsert(AnalysisNames,"wave","Registration Wave");
	StructInsert(AnalysisNames,"sitename","Delegate Site");
</CFSCRIPT>




<cf_head>
	<cf_title>Event - Stats</cf_title>

<CFOUTPUT>
<SCRIPT type="text/javascript">
<!--

function emailStats() {
	newWindow = openWin( 'eventStatsEmail.cfm?FlagID=#FlagID#&Event=#URLEncodedFormat(getEventDetails.Title)#' ,'PopUp', 'width=450,height=450,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1' )
}

function openWin( windowURL,  windowName, windowFeatures ) { 
	return window.open( windowURL, windowName, windowFeatures ); 
}

//-->
</SCRIPT>
</CFOUTPUT>

</cf_head>



<TABLE WIDTH="100%" CELLPADDING="2" CELLSPACING="2" BORDER="0">
<TR>
	<TD ALIGN="left"><H1>Event Statistics</H1></TD>
	<TD ALIGN="right"><A HREF="eventHome.cfm">Events Main Page</A></TD>
</TR>
</TABLE>
<P>
<H1><CFOUTPUT>#htmleditformat(getEventDetails.Title)#</CFOUTPUT></H1>
<P>
Please choose a country and the required analysis
<P>
<FORM NAME="eventStats" ACTION="eventStats2.cfm" METHOD="post">

<CFOUTPUT>
<CF_INPUT TYPE="hidden" NAME="FlagID" VALUE="#FlagID#">
<CF_INPUT TYPE="hidden" NAME="frmTitle" VALUE="#getEventDetails.Title#">
</CFOUTPUT>

<SELECT NAME="frmCountryID">
	<CFOUTPUT QUERY="getEventCountry"><OPTION VALUE="#CountryID#,#CountryDescription#" <CFIF IsDefined("frmCountryID")><CFIF ListFirst(frmCountryID) EQ CountryID>SELECTED</CFIF></CFIF>>#htmleditformat(CountryDescription)#</CFOUTPUT>
</SELECT>
&nbsp;&nbsp;&nbsp;
<SELECT NAME="frmAnalysis">
	<CFOUTPUT QUERY="getEventAnalysis"><OPTION VALUE="#ListLast(FieldName, '.')#" <CFIF IsDefined("frmAnalysis")><CFIF frmAnalysis IS ListLast(FieldName, '.')>SELECTED</CFIF></CFIF>>#StructFind(AnalysisNames,ListLast(FieldName, '.'))#</CFOUTPUT>
</SELECT>
&nbsp;&nbsp;
<INPUT TYPE="submit" NAME="StatSubmit" VALUE="  Go  ">
</FORM>

<P>

<CFIF IsDefined("form.frmCountryID")>
	<CFOUTPUT>Analysis of #StructFind(AnalysisNames,frmAnalysis)# for <B>#htmleditformat(ListLast(frmCountryID))#</B></CFOUTPUT>
	<P>
	<TABLE CELLPADDING="2" CELLSPACING="2" BORDER="0">
	<CFOUTPUT>
		<TR>
			<TD COLSPAN="3"><A HREF="JavaScript:emailStats();">E-mail these stats to a #htmleditformat(application.clientName)# user</A></TD>
		</TR>
		<TR>
			<TD COLSPAN="3">&nbsp;</TD>
		</TR>
		<TR>
			<TD COLSPAN="3">#htmleditformat(Total)# Delegates</TD>
		</TR>
		<TR>
			<TD COLSPAN="3">&nbsp;</TD>
		</TR>
		<TR>
			<TD WIDTH="200" CLASS="label">#StructFind(AnalysisNames,frmAnalysis)#</TD>
			<TD WIDTH="100" CLASS="label">Delegates</TD>
			<TD WIDTH="20" CLASS="label">%</TD>
		</TR>
	</CFOUTPUT>
	<CFOUTPUT QUERY="getAnalysisData">
		<TR>
			<TD WIDTH="200">#htmleditformat(Analysis)#</TD>
			<TD WIDTH="100">#htmleditformat(EachTotal)#</TD>
			<TD WIDTH="20">#DecimalFormat(Fraction)#</TD>
		</TR>
	</CFOUTPUT>
	<TR>
		<TD COLSPAN="3"><CFOUTPUT><IMG SRC="#TemporaryFolderdetails.webpath#\#FlagID##frmAnalysis#.jpg" WIDTH="400" HEIGHT="400" BORDER="0"></CFOUTPUT></TD>
	</TR>
</TABLE>
</CFIF>



