<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File:			eventResourceEditor.cfm
Author:			DJH
Date created:	1 February 2000

Description:
Allows a user to add/view/update resources given the appropriate rights.

Version history:
1

--->

<cf_title>Event Resource Editor</cf_title>

<cfparam name="FlagID" default="">
<cfparam name="frmAction" default="Resources">
<cfinclude template="eventTopHead.cfm">
<Cfif FlagID is not "">
<a href="eventResourceBookings.cfm?FlagID=<cfoutput>#htmleditformat(FlagID)#</cfoutput>">Return to the event</a>
</cfif>
<div>

<!--- first display a list of the existing resources --->

<table border="0" width="100%" cellpadding="0" cellspacing="0" style="background-color: ffffee;">
<tr>
	<cfoutput>
		<cfif frmAction contains "Resources">
			<td width="25%" class="activeResource" align="center">
				<span>Manage Resources</span>
			</td bgcolor>
		<cfelse>
			<td width="25%" align="center">
				<a href="eventResourceEditor.cfm?frmAction=Resources<cfif FlagID is not "">&FlagID=#htmleditformat(FlagID)#</cfif>" class="button">Manage Resources</a>
			</td>
		</cfif>
		<cfif frmAction contains "ResourceTypes">
			<td width="25%" class="activeResource" align="center">
				<span>Manage Resource Types</span>
			</td bgcolor>
		<cfelse>
			<td width="25%" align="center">
				<a href="eventResourceEditor.cfm?frmAction=ResourceTypes<cfif FlagID is not "">&FlagID=#htmleditformat(FlagID)#</cfif>" class="button">Manage Resource Types</a>
			</td>			
		</cfif>
		<cfif frmAction contains "ResourcePacks">
			<td width="25%" class="activeResource" align="center">
				<span>Manage Resource Packs</span>
			</td bgcolor>
		<cfelse>
			<td width="25%" align="center">
				<a href="eventResourceEditor.cfm?frmAction=editResourcePacks<cfif FlagID is not "">&FlagID=#htmleditformat(FlagID)#</cfif>" class="button">Manage Resource Packs</a>
			</td>
		</cfif>
		<cfif frmAction contains "ResourcePackNames">
			<td width="25%" class="activeResource" align="center">
				<span>Manage Resource Pack Names</span>
			</td bgcolor>
		<cfelse>
			<td width="25%" align="center">
				<a href="eventResourceEditor.cfm?frmAction=editResourcePackNames<cfif FlagID is not "">&FlagID=#htmleditformat(FlagID)#</cfif>" class="button">Manage Resource Pack Names</a>
			</td>
		</cfif>
	</cfoutput>
</tr>
</table>
<cfswitch expression="#frmAction#">
<cfcase value="Resources">
	<SCRIPT type="text/javascript">
		function checkSubmitResources(aform)
		{
			if( aform.frmResourceTypeID.selectedIndex > 0 && aform.frmResourceName.value.length > 0)
				return true
			else
				alert("You must choose a value for Resource Type and give the resource a name!")
			return false
		}
	</script>
	<cfinclude template="sql_select_Resource.cfm">
	<cfinclude template="sql_select_ResourceType.cfm">
	<table border="0" cellpadding="1" cellspacing="1">
	<!--- list all the resources currently in the resource table --->
	<tr>
		<td class="label">Resource Type</td>
		<td class="label">Quantity<br>on hand</td>
		<td class="label">Resource Name</td>
		<td class="label">Description</td>
		<td class="label">Owner</td>
		<td>&nbsp;</td>
	</tr>
	<cfloop query="select_resource">
		<form name="updateresource" action="eventResourceEditor.cfm?frmAction=UpdateResources" method="post" onsubmit="return checkSubmitResources(this)">
		<input type="hidden" name="frmDeleteMe" value="0">
		<cfoutput>
		<CF_INPUT type="hidden" name="FlagID" value="#FlagID#">
		<CF_INPUT type="hidden" name="frmResourceID" value="#select_resource.ResourceID#">
		</cfoutput>
		<tr>
			<td>
				<select name="frmResourceTypeID" class="smaller">
				<option value="">Resource Type
				<cfset ThisResourceTypeID = select_resource.ResourceTypeID>
					<cfoutput query="select_ResourceType">					
						<option value="#ResourceTypeID#" <cfif ThisResourceTypeID is select_ResourceType.ResourceTypeID>selected</cfif>>#htmleditformat(ResourceType)#
					</cfoutput>
				</select>
			</td>
			<cfoutput>
			<td><CF_INPUT type="text" size="3" name="frmQuantityOnHand" value="#QuantityOnHand#" class="smaller"></td>
			<td><CF_INPUT type="text" size="17" name="frmResourceName" value="#ResourceName#" class="smaller"></td>
			<td><CF_INPUT type="text" size="20" name="frmDescription" value="#Description#" class="smaller"></td>
			<td>#htmleditformat(OwnerID)#</td>
			</cfoutput>
			<td><input type="submit" class="smaller" value="Update"></td>
			<td><input type="button" class="smaller" value="Delete" onclick="if(confirm('If you are sure you want to delete this record, click OK. Otherwise click Cancel')) frmDeleteMe.value=1; this.form.submit()"></td>
		</tr>
		</form>
	</cfloop>

	<form name="addresource" action="eventResourceEditor.cfm?frmAction=AddResources" method="post" onsubmit="return checkSubmitResources(this)">
	<CF_INPUT type="hidden" name="FlagID" value="#FlagID#">

	<tr>
		<td>
			<select name="frmResourceTypeID" class="smaller">
				<option value="">Resource Type
				<cfoutput query="select_ResourceType">					
					<option value="#ResourceTypeID#">#htmleditformat(ResourceType)#
				</cfoutput>
			</select>
		</td>
		<td><input type="text" size="3" name="frmQuantityOnHand" class="smaller"></td>
		<td><input type="text" size="17" name="frmResourceName" class="smaller"></td>
		<td><input type="text" size="20" name="frmDescription" class="smaller"></td>
		<td>&nbsp;</td>
		<td><input type="submit" class="smaller" value="  Add   "></td>
	</tr>
	</form>	
	</table>
</cfcase>
<!--- next bit updates the resources --->
<cfcase value="updateResources">
	<cfinclude template="sql_update_resource.cfm">
	<cflocation url="eventResourceEditor.cfm?frmAction=Resources&FlagID=#FlagID#"addToken="false">
</cfcase>
<!--- next bit adds the resources --->
<cfcase value="addResources">
	<cfinclude template="sql_insert_resource.cfm">
	<cflocation url="eventResourceEditor.cfm?frmAction=Resources&FlagID=#FlagID#"addToken="false">
</cfcase>

<cfcase value="ResourceTypes">
	<SCRIPT type="text/javascript">
		function checkSubmitResourceTypes(aform)
		{
			if(aform.frmResourceType.value.length > 0)
				return true
			else
				alert("You must provide a value for Resource Type!")
			return false		
		}
	</script>
	<cfinclude template="sql_select_ResourceType.cfm">
	<table border="0" cellpadding="1" cellspacing="1">
	<!--- list all the resources currently in the resource table --->
	<tr>
		<td class="label">Resource Type</td>
	</tr>
	<cfoutput query="select_ResourceType">
		<form name="updateresourcetype" action="eventResourceEditor.cfm?frmAction=UpdateResourceTypes" method="post" onsubmit="return checkSubmitResourceTypes(this)">
		<CF_INPUT type="hidden" name="FlagID" value="#FlagID#">
		<CF_INPUT type="hidden" name="frmResourceTypeID" value="#select_ResourceType.ResourceTypeID#">
		<input type="hidden" name="frmDeleteMe" value="0">
		<tr>
			<td><CF_INPUT type="text" size="20" name="frmResourceType" value="#ResourceType#" class="smaller"></td>
			<td><input type="submit" class="smaller" value="Update"></td>
			<td><input type="button" class="smaller" value="delete" onclick="if(confirm('If you are sure you want to delete this record, click OK. Otherwise click Cancel.')) frmDeleteMe.value=1; this.form.submit()"></td>
		</tr>
		</form>
	</cfoutput>

	<form name="addresourcetype" action="eventResourceEditor.cfm?frmAction=AddResourceTypes" method="post" onsubmit="return checkSubmitResourceTypes(this)">
	<CF_INPUT type="hidden" name="FlagID" value="#FlagID#">

	<tr>
		<tr>
			<td><input type="text" size="20" name="frmResourceType" class="smaller"></td>
			<td><input type="submit" class="smaller" value="  Add   "></td>
		</tr>
	</tr>
	</form>	
	</table>

</cfcase>
<!--- update the resource types --->
<cfcase value="updateResourceTypes">
	<cfinclude template="sql_update_ResourceType.cfm">
	<cflocation url="eventResourceEditor.cfm?frmAction=ResourceTypes&FlagID=#FlagID#"addToken="false">
</cfcase>
<!--- add the resource types --->
<cfcase value="addResourceTypes">
	<cfinclude template="sql_insert_ResourceType.cfm">
	<cflocation url="eventResourceEditor.cfm?frmAction=ResourceTypes&FlagID=#FlagID#"addToken="false">
</cfcase>


<cfcase value="editResourcePackNames">
	<cfinclude template="sql_select_ResourcePackName.cfm">
	<SCRIPT type="text/javascript">
		function checkSubmitResourcePackNames(aform)
		{
			if(aform.frmResourcePackName.value.length > 0)
				return true
			else
				alert("You must provide a value for Resource Pack Name!")
			return false		
		}
	</script>
	<table border="0" cellpadding="1" cellspacing="1">
	<!--- list all the resources currently in the resource table --->
	<tr>
		<td class="label">Resource Pack Name</td>
	</tr>
	<cfoutput query="select_ResourcePackName">
		<form name="updateresourcepackname" action="eventResourceEditor.cfm?frmAction=UpdateResourcePackNames" method="post" onsubmit="return checkSubmitResourcePackNames(this)">
		<input type="hidden" name="frmDeleteMe" value="0">
		<CF_INPUT type="hidden" name="FlagID" value="#FlagID#">
		<CF_INPUT type="hidden" name="frmResourcePackID" value="#ResourcePackID#">
		<tr>
			<td><CF_INPUT type="text" size="20" name="frmResourcePackName" value="#ResourcePackName#" class="smaller"></td>
			<td><input type="submit" class="smaller" value="Update"></td>
			<td><input type="button" class="smaller" value="Delete" onclick="if(confirm('If you are sure you want to delete, click OK. Otherwise click Cancel.')) frmDeleteMe.value=1; this.form.submit()"></td>
		</tr>
		</form>
	</cfoutput>

	<form name="addresourcepackname" action="eventResourceEditor.cfm?frmAction=AddResourcePackNames" method="post" onsubmit="return checkSubmitResourcePackNames(this)">
	<CF_INPUT type="hidden" name="FlagID" value="#FlagID#">

	<tr>
		<tr>
			<td><input type="text" size="20" name="frmResourcePackName" class="smaller"></td>
			<td><input type="submit" class="smaller" value="  Add   "></td>
		</tr>
	</tr>
	</form>	
	</table>
</cfcase>

<cfcase value="AddResourcePackNames">
	<cfinclude template="sql_insert_ResourcePackName.cfm">
	<cflocation url="eventResourceEditor.cfm?frmAction=editResourcePackNames&FlagID=#FlagID#"addToken="false">
</cfcase>

<cfcase value="updateResourcePackNames">
	<cfinclude template="sql_update_ResourcePackName.cfm">
	<cflocation url="eventResourceEditor.cfm?frmAction=editResourcePackNames&FlagID=#FlagID#"addToken="false">
</cfcase>


<!--- edit resource packs --->
<cfcase value="editResourcePacks">
	<cfparam name="frmResourcePackName" default="">
	<cfparam name="frmResourcePackID" default="">
	<cfparam name="frmAvailableResources" default="">
	<cfparam name="frmResourcesInPack" default="">

	<cfquery name="getResourcePackNames" datasource="#application.siteDataSource#">
		select
			ResourcePackName,
			ResourcePackID
		from
			ResourcePackName
		where
			Active = 1
		order by
			ResourcePackName	
	</cfquery>
	
	<cfif frmResourcePackID is not "">

		<!--- this bit does the adding/removing --->
		<cfif frmAvailableResources is not "">
			<cfloop list="#frmAvailableResources#" index="aResource">
				<cfquery name="insertResources" datasource="#application.siteDataSource#">
					insert into ResourcePackItems
					(
						ResourcePackID,
						ResourceID
					)
					values
					(
						<cf_queryparam value="#frmResourcePackID#" CFSQLTYPE="CF_SQL_INTEGER" >,
						<cf_queryparam value="#aResource#" CFSQLTYPE="CF_SQL_INTEGER" >
					)
				</cfquery>
			</cfloop>
		</cfif>
		
		<cfif frmResourcesInPack is not "">
			<cfquery name="" datasource="#application.siteDataSource#">
				delete from ResourcePackItems
				where
					ResourceID  in ( <cf_queryparam value="#frmResourcesInPack#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
					and ResourcePackID =  <cf_queryparam value="#frmResourcePackID#" CFSQLTYPE="CF_SQL_INTEGER" > 
			</cfquery>
		</cfif>


		<!--- a resource pack has been selected --->
		<cfquery name="getResourcePackItems" datasource="#application.siteDataSource#">
			select
				rpi.ResourcePackID,
				rpi.ResourceID,
				r.ResourceName
			from
				ResourcePackItems as rpi,
				Resource as r
			where
				rpi.ResourceId = r.ResourceID
				and rpi.ResourcePackID =  <cf_queryparam value="#frmResourcePackID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfquery>
		
		<cfquery name="getAvailableResources" datasource="#application.siteDataSource#">
			select
				ResourceID,
				ResourceName
			from
				Resource
			where
				ResourceID not in (select ResourceID from ResourcePackItems where ResourcePackID =  <cf_queryparam value="#frmResourcePackID#" CFSQLTYPE="CF_SQL_INTEGER" > )
				and Active = 1
			order by 
				ResourceName			
		</cfquery>
		
		
	</cfif>
	<!--- show a drop down with all the current resource packs --->
	<table border="0" cellpadding="4" cellspacing="4" width="80%">
	<form name="ResourcePacks" action="eventResourceEditor.cfm" method="post">
		<CF_INPUT type="hidden" name="FlagID" value="#FlagID#">
		<input type="hidden" name="frmAction" value="editResourcePacks">
		<tr>
			<td class="label">
				<b>Choose Resource Pack</b>
				<div class="margin">
				Choose a resource pack from the drop down.
				</div>
				<div align="right">
					<select name="frmResourcePackID" onchange="if(this.selectedIndex > 0) this.form.submit()">
						<option value="0" <cfif frmResourcePackID is ""> selected</cfif>>Choose a resource pack
						<cfoutput query="getResourcePackNames">
							<option value="#ResourcePackID#" <cfif ResourcePackID is frmResourcePackID>selected</cfif>>#htmleditformat(ResourcePackName)#
						</cfoutput>
					</select><br>					
				</div>
				
			</td>
		</tr>
		<cfif frmResourcePackID is not "">
		<tr valign="middle">
			<td class="label">
				<b>Choose the Resources</b>
				<div class="Margin">
					The Resources belonging to this resource pack are shown in the right hand column. The remaining resources available to be
					placed in the resource pack are in the left hand column.
				</div>				
				<div align="right">
				<table border="0">
				<tr>
				<td>
					<select name="frmAvailableResources" multiple size="10">
					<cfoutput query="getAvailableResources">
						<option value="#ResourceID#">#htmleditformat(ResourceName)#
					</cfoutput>
					</select>
				</td>
				<td>
					<input type="button" value="<<" onclick="if(frmResourcesInPack.selectedIndex > -1) this.form.submit()"><br>
					<input type="button" value=">>" onclick="if(frmAvailableResources.selectedIndex > -1) this.form.submit()">
				</td>
				<td>
					<select name="frmResourcesInPack" multiple size="10">
					<cfoutput query="getResourcePackItems">
						<option value="#ResourceID#">#htmleditformat(ResourceName)#
					</cfoutput>
					</select>
				</td>
				</tr>
				</table>
				</div>
			</td>
		</tr>
		</cfif>
	</form>
	</table>

</cfcase>

</cfswitch>

</div>