<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File:			sql_getEmails.cfm
Author:			DJH
Date created:	15 February 2000

Description:	Gets a list of the default emails/specific emails for the specified eventDetail

Version history:1

2011/10/05 PPB LID7683 allow suppression of confirmation email
--->

<cfquery name="UpdateEventDetailEmail" datasource="#application.siteDataSource#">
Update
	EventDetailEmail
Set
	Subject =  <cf_queryparam value="#frmSubject#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
	MessageText =  <cf_queryparam value="#frmMessageText#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
	SuppressEmail =  <cf_queryparam value="#iif(IsDefined("frmSuppressEmail"),1,0)#" CFSQLTYPE="CF_SQL_bit" > 
where
	FlagID =  <cf_queryparam value="#FlagID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	and EventDetailEmailType =  <cf_queryparam value="#EmailType#" CFSQLTYPE="CF_SQL_VARCHAR" > 
</cfquery>



