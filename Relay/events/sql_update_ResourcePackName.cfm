<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File:			sql_update_ResourcePackName.cfm
Author:			DJH
Date created:	1 February 2000

Description:

Version history:
1

--->

<cfquery name="update_ResourcePackName" datasource="#application.siteDataSource#">
update 
	ResourcePackName
set
	ResourcePackName =  <cf_queryparam value="#frmResourcePackName#" CFSQLTYPE="CF_SQL_VARCHAR" > 
	<cfif isDefined("frmDeleteMe") and frmDeleteMe is 1>
		,
		Active = 0
	</cfif>
where
	ResourcePackID =  <cf_queryparam value="#frmResourcePackID#" CFSQLTYPE="CF_SQL_INTEGER" > 
</cfquery>