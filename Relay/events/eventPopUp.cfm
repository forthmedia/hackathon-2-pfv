<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Mods:  WAB 2000-05-04   Added link to download event details of checked people
			 NJH 2008/09/25 - elearning 8.1 - show option of grading people if event is tied to a module.
--->

<SCRIPT type="text/javascript">
<!--

function go()
{
	if (document.selecter.select1.options[document.selecter.select1.selectedIndex].value != "none")
	{
		location = document.selecter.select1.options[document.selecter.select1.selectedIndex].value;
		document.selecter.select1.selectedIndex=0;
	}
}

function openWin( windowURL,  windowName, windowFeatures )
{
	return window.open( windowURL, windowName, windowFeatures );
}

function noChecks(){
	alert('You have not checked any people');
	document.selecter.select1.selectedIndex=0;
	return
}


function doChecks (setting) {
	var form = document.doList;
	for (j=0; j<form.elements.length; j++) {
		thiselement = form.elements[j];
		if (thiselement.name == 'frmCheckIDs') {
			thiselement.checked = setting;
		}
	}
}

function saveChecks() {
	var form = document.doList;
	var checkedItems = "";
	var totalChecks = ""
	for (j=0; j<form.elements.length; j++){
		thiselement = form.elements[j];
		if (thiselement.name == 'frmCheckIDs') {
			if (thiselement.checked) {
				if (checkedItems == "") {
					checkedItems = thiselement.value;
				} else {
					checkedItems += "," + thiselement.value;
				}
			}
		}
	}
	if (form.frmAllCheckIDs.value != "" && checkedItems != "") {
		totalChecks = form.frmAllCheckIDs.value + "," + checkedItems;
	} else if (form.frmAllCheckIDs.value == "" && checkedItems != "") {
		totalChecks = checkedItems;
	} else if (form.frmAllCheckIDs.value != "" && checkedItems == "") {
		totalChecks = form.frmAllCheckIDs.value;
	} else {
		totalChecks = "";
	}
	return totalChecks;
}

function resetForm() {
	var form = document.doList;
	form.frmAction.value='';
	form.target='main';
	form.action='eventDelegateList.cfm';
}

function goOption(checksRequired, frmAction)
{
	var form = document.doList;

	if(checksRequired && saveChecks()=='')
		noChecks()
	else
	{
		newWindow = openWin( 'about:blank' ,'PopUp', 'width=' + parseInt(screen.availWidth * 0.8)  + ',height=' + parseInt(screen.availHeight * 0.6) + ',toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1' );
		form.frmAction.value = frmAction;
		if (checksRequired) form.frmTotalChecks.value=saveChecks();
		form.target='PopUp';
		form.action='eventTask.cfm';
		form.submit();
		<CFIF FindNoCase("msie", cgi.http_user_agent) GT 0>
			resetForm();
		<CFELSE>
			setInterval('resetForm()', 1000);
		</CFIF>
		newWindow.focus();
	}
}

//-->
</SCRIPT>
<cfparam name="addonly" default="no">
<form name="selecter">
 	<select name="select1" onchange="go()" class="form-control">
		<option value=none>Select a function
		<option value=none>--------------------
		<CFOUTPUT>
			<CFIF ManagerOK and eventManager>
				<cfif addonly is false>
					<option value="">Updates
					<!--- SWJ & MDC: 2007-02-14 switched off for the moment <option value="javascript: goOption(true, 'wave')">&nbsp;&nbsp;Update wave status of checked people --->
					<option value="JavaScript: goOption(true,'regstatus')">&nbsp;&nbsp;Update invite status of checked people
					<!--- NJH 2008/09/24 elearning 8.1 - if a module is associated with the event, then give the option of grading people --->
					<cfif isDefined("getEventDetails.moduleID") and (getEventDetails.moduleID neq "" or (getEventDetails.moduleID neq 0))>
						<option value="JavaScript: goOption(true,'gradeStudents')">&nbsp;&nbsp;Grade checked people
					</cfif>
					<option value="JavaScript: goOption(true,'updatemaindetails')">&nbsp;&nbsp;Update database details of checked people
					<option value="JavaScript: goOption(true, 'movepersonstoevent')">&nbsp;&nbsp;Change event of checked people
				</cfif>
				<cfif SuperOK>
					<option value="">Delegates
					<option value="JavaScript: goOption(false, 'add')">&nbsp;&nbsp;Add delegates
					<option value="JavaScript: goOption(true, 'delete')">&nbsp;&nbsp;Remove Delegates
				</cfif>
				<cfif addonly is false>
					<option value="">Communications & Downloads
<!--- 					<option value="JavaScript: goOption(true, 'eventdown')">&nbsp;&nbsp;Download Event Details of checked people --->
					<!--- MDC 2006-01-06  commented out as concerned about comms process
					<option value="JavaScript: goOption(true, 'invcomm')">&nbsp;&nbsp;Send invitation to checked people
					<option value="Javascript: goOption(false,'invallcomm')">&nbsp;&nbsp;Send invitation to all people
					<option value="JavaScript: goOption(true, 'comm')">&nbsp;&nbsp;Send other communication to checked people
					<option value="Javascript: goOption(false,'allcomm')">&nbsp;&nbsp;Send other communication to all people --->
					<option value="JavaScript: goOption(true, 'down')">&nbsp;&nbsp;Download checked people
					<option value="JavaScript: goOption(false, 'alldown')">&nbsp;&nbsp;Download all people
				</cfif>
			</cfif>

					<!--- 2000-10-09 WAB added selection dropdown --->
					<option value=none>Selections
					<option value="JavaScript: goOption(true, 'selectionAdd')">&nbsp&nbsp;  Add checked people to a selection
					<option value="JavaScript: goOption(true, 'selectionRemove')">&nbsp;&nbsp; Remove checked people from a selection
					<option value="JavaScript: goOption(true, 'selectionSave')">&nbsp;&nbsp; Save checked people as new selection




			<cfif addonly is false>
				<option value="">Check
				<option value="JavaScript: doChecks(true)">&nbsp;&nbsp;Check all people on page
				<option value="JavaScript: doChecks(false)">&nbsp;&nbsp;Uncheck all people on page
			</cfif>
		</CFOUTPUT>
	</select>
</form>