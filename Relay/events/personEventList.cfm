<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File:			personEventList.cfm (based onEventHome.cfm)
Author:			PPB
Date created:	11 February 2010

Description:
Displays a list of a person's events and allows the CAM/other to set whether the person attended each event and on which date

Version history:
--->

<cfset ShowSave = "yes">

<cfparam name="saveData" type="string" default="0">

<cfif saveData neq "0">
	<cfloop list="#form.FieldNames#" index="FormField">
    	<cfif Left(FormField, 10) eq "ATTENDEDYN">
			<cfset flagID = mid(FormField,12,10)>			<!--- arbitrarily use 10, to get all characters from position 12 to end, typically the id will be 4 or 5 chars  --->
			<cfset attendedYN = evaluate("form." & FormField)>
			
			<cfif attendedYN eq 0 or attendedYN eq 1>		<!--- one radio button or the other has been clicked --->
				<cfif attendedYN eq 0>
					<cfset regStatus = "Did Not Attend">
					<cfset attendedDate = "">				<!--- clear date in case it was previously set incorrectly ('' is switched to NULL in SQL) --->
				<cfelse>
					<cfset regStatus = "Attended">
					<cfset attendedDate = evaluate("FORM.ATTENDEDDATE_" & flagID)>
				</cfif>

				<cfquery name="updateEvent" datasource="#application.siteDataSource#">
					UPDATE eventFlagData
					SET RegStatus =  <cf_queryparam value="#regStatus#" CFSQLTYPE="CF_SQL_VARCHAR" > 
						<cfif attendedDate eq "">
							,AttendedDate = NULL
						<cfelse>
							,AttendedDate =  <cf_queryparam value="#attendedDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
						</cfif>
					WHERE EntityID =  <cf_queryparam value="#FRMCURRENTENTITYID#" CFSQLTYPE="CF_SQL_INTEGER" > 
					and	FlagId =  <cf_queryparam value="#flagID#" CFSQLTYPE="CF_SQL_INTEGER" > 
				</cfquery> 
			</cfif>
			
			
		</cfif>
	</cfloop>
	
	<p class="messageText"> Your changes have been saved!</p>
</cfif>





<cf_head>
	<cf_title>Events</cf_title>
</cf_head>





<!--- get the events that the user has country rights to see --->
<cfquery NAME="getEvent" datasource="#application.siteDataSource#">
	SELECT eg.EventName, eg.EventGroupID, 
		ed.FlagID, ed.Title, ed.VenueRoom, ed.EventGroupID, 
		isnull(ed.AllocatedStartDate,ed.PreferredStartDate) as StartDate,
		isnull(ed.AllocatedEndDate,ed.PreferredEndDate) as EndDate,
		efd.RegStatus, efd.AttendedDate
	
	FROM 
		EventDetail ed with (noLock)
		inner join EventGroup eg with (noLock) on eg.EventGroupID = ed.EventGroupID 
		inner join eventFlagData efd with (noLock) on efd.FlagID = ed.FlagID 

	WHERE 
		efd.EntityID =  <cf_queryparam value="#FRMCURRENTENTITYID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	and ed.eventstatus <> 'Cancelled'	 
	
	ORDER BY
		isnull(ed.AllocatedStartDate, ed.PreferredStartDate) DESC
		
</cfquery>


<cfif GetEvent.RecordCount GT 0>
 	<TABLE CELLPADDING="2" CELLSPACING="2" BORDER="0">
	<cfform name="personEvents" action="/events/personEventList.cfm?#request.query_string#&saveData=1" method="post">
		
		<cfoutput query="GetEvent" GROUP="EventGroupID">
		<cfset row=0>
		<TR>
			<TH COLSPAN="3" ALIGN="LEFT">#htmleditformat(EventName)#</TH>
		</TR>
		<TR>
			<TD>
				<TABLE CELLPADDING="5" CELLSPACING="0" BORDER="0">
				<TR>
					<TD WIDTH="400" class="Label"><B>Title</B></TD>
					<TD WIDTH="400" class="Label"><B>Venue</B></TD>
					<TD WIDTH="400" class="Label"><B>Attended ?</B></TD>
				</TR>
				<cfoutput>
					<cfset row=row+1>
					<cfif row mod 2 is 0>
						<cfset bgcol="FEFFD7">
					<cfelse>
						<cfset bgcol="white">
					</cfif>
					<TR <cfif CurrentRow MOD 2 IS NOT 0> class="oddrow"<cfelse> class="evenRow"</cfif>>
						<TD WIDTH="300">#htmleditformat(title)#&nbsp;</TD>
						<TD WIDTH="300">#htmleditformat(VenueRoom)#&nbsp;</TD>
						<TD WIDTH="300">
							No <CF_INPUT type="radio" name="attendedYN_#FlagID#" value="0" checked="#iif( RegStatus eq 'Did Not Attend',true,false)#" onclick="didNotAttendClicked('#FlagID#')"> 
							Yes <CF_INPUT type="radio" name="attendedYN_#FlagID#" value="1" checked="#iif( RegStatus eq 'Attended',true,false)#" onclick="attendedClicked('#FlagID#')">
							<cfset lastDateDisplayed = StartDate>
							<select id="attendedDate_#FlagID#" name="attendedDate_#FlagID#">
								<option></option>
								<cfloop condition="lastDateDisplayed lte EndDate">
									<option <cfif AttendedDate eq lastDateDisplayed>selected</cfif>>#dateformat(lastDateDisplayed,"dd-mmm-yyyy")# </option>
									<cfset lastDateDisplayed = DateAdd("d",1,lastDateDisplayed)> 
								</cfloop>
							</select>
						</TD>
					</TR>
					<tr></tr>
				</cfoutput>
				</TABLE>
			</TD>
		</TR>
		</cfoutput>

		<TR>
			<TD colspan="99" align="right"><A href="javascript:submitPrimaryForm();"><img src="/images/buttons/c_save_e.gif" border=0 alt="Save"></A></TD>
		</TR>
	</cfform>	
	</TABLE>
<cfelse>
	<H2>There are currently no Events for this person.</H2>
</cfif>


<script type="text/javascript">
	
	//this function must exist for the Save button at the top of the list to be enabled
	function submitPrimaryForm() {
		document.personEvents.submit()
	}
	
	function didNotAttendClicked(flagID) {
		
		document.getElementById('attendedDate_' + flagID).selectedIndex = 0;		//ie clear any selected date
	}


	function attendedClicked(flagID) {
		
		if (document.getElementById('attendedDate_' + flagID).selectedIndex == 0) {
			document.getElementById('attendedDate_' + flagID).selectedIndex = 1;	//set the 1st in the list to be selected to save the user effort (esp if there is only 1 day or it is a multi-day event at which the person attends more than 1 day s.t. we don't care which date is picked) 
		}
		
	}
	
</script>





