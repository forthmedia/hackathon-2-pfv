<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File:			EventClone.cfm
Author:			DJH
Date created:	27 January 2000

Description:
Allows an authorised user to clone an event based on an existing event.


Version history:
1

--->

<cf_title>Clone Event</cf_title>

<!--- get the event details --->
<cfinclude template="sql_GetEventDetails.cfm">
<cfinclude template="sql_getPartnerTypes.cfm">
<cfset cloneFlagID = FlagID>
<cfinclude template="eventTopHead.cfm">

<SCRIPT type="text/javascript">
	function submitForm()
	{
		var form = document.UpdateEvent
		form.submit()
	}
</script>



<CFQUERY NAME="GetEventDetails" datasource="#application.siteDataSource#">
	SELECT 
		ed.OwnerID, 
		ed.FlagID, 
		ed.Title,
		ed.Location,
		ed.VenueRoom,
		ed.CountryID,
		ed.EstAttendees,
		ed.PreferredStartDate, 
		ed.PreferredEndDate,
		ed.AllocatedStartDate,
		ed.AllocatedEndDate,
		ed.Email,
		ed.EmailCC,
		ed.UseAgency,
		ed.InvitationType,
		ed.AudienceType,
		ed.PartnerType,
		ed.EventStatus,
		ed.Agenda,
		ed.CheckList,
		p.FirstName, 
		p.LastName, 
		p.OfficePhone,
		eg.EventGroupID, 
		eg.EventName,
		c.CountryDescription
	FROM 
		EventDetail AS ed, 
		Person AS p,
		eventGroup as eg,
		country as c
	WHERE 	
		ed.OwnerID = p.PersonID
		and eg.eventGroupID = ed.eventGroupID
		and c.countryID = ed.countryID
		AND ed.FlagID =  <cf_queryparam value="#CloneFlagID#" CFSQLTYPE="CF_SQL_INTEGER" > 	
</CFQUERY>

<cfquery name="countries" datasource="#application.siteDataSource#">
	select
		ec.CountryID,
		c.CountryDescription
	from
		EventCountry as ec,
		country as c
	where
		ec.FlagID =  <cf_queryparam value="#cloneFlagID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		and c.CountryID = ec.CountryID
</cfquery>

<CFIF NOT SuperOK>
	You do not have permissions to create a new event.
	<CF_ABORT>
</CFIF>

<CFQUERY NAME="GetAssigned" datasource="#application.siteDataSource#">
	SELECT 
		ug.Name, 
		rr.UserGroupID
	FROM 
		RecordRights AS rr 
		INNER JOIN UserGroup AS ug ON rr.UserGroupID = ug.UserGroupID
	WHERE 
		rr.RecordID =  <cf_queryparam value="#cloneFlagID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		AND rr.Entity = 'Flag'
	ORDER 
		BY ug.Name
</CFQUERY>

<CFIF frmAction IS "AddEvent" AND Msg IS "">
	<!--- check start date is before end date --->
	<CFIF DateCompare(frmPreferredStartDate, frmPreferredEndDate) IS 1>
		<CFSET Msg = Msg & "Please ensure the Start Date is before the End Date<BR>">
	</CFIF>

	<!--- attempt to create a unique flagtextid for this event based on the event name and location with
	all spaces removed. --->
	<cfset frmTitle = "#GetEventDetails.EventName# #frmLocation# dateformat(frmPreferredStartDate,'dd-mmm-yyyy')">
	<cfset frmFlagTextID = replace("#cloneFlagID##Left(frmLocation,6)#"," ","","ALL")>
	<!--- now make sure it's unique - if not, append a counter to the end of it --->
	<!--- check that the user supplied FlagTextID is unique in the database --->
	<CFQUERY NAME="getEventFlagTextIDs" datasource="#application.siteDataSource#">
		SELECT 
			FlagTextID
		FROM 
			Flag
		where
			FlagTextID  like  <cf_queryparam value="#frmFlagTextID#%" CFSQLTYPE="CF_SQL_VARCHAR" > 
	</CFQUERY>

	<CFIF getEventFlagTextIDs.recordcount GT 0>
		<cfset frmFlagTextID = frmFlagTextID & (getEventFlagTextIDs.recordcount + 1)>
	</CFIF>
	<CFIF Msg NEQ "">
		<CFLOCATION URL="eventClone.cfm?Msg=#Msg#"addToken="false">
		<CF_ABORT>	
	<CFELSE>
		<CFSET FreezeDate = request.requestTimeODBC>
		<CFQUERY NAME="getEventFlagGroup" datasource="#application.siteDataSource#">
			SELECT 
				fg.FlagGroupID
			FROM 
				FlagGroup AS fg, 
				FlagType AS ft
			WHERE 
				fg.FlagTypeID = ft.FlagTypeID
				AND ft.Name = 'Event'
		</CFQUERY>
		
		<CFQUERY NAME="addEventFlag" datasource="#application.siteDataSource#">
			INSERT INTO Flag
			(
				FlagTextID, 
				FlagGroupID, 
				Name, 
				Active, 
				CreatedBy, 
				Created, 
				LastUpdatedBy, 
				LastUpdated
			)
			VALUES
			(
				<cf_queryparam value="#frmFlagTextID#" CFSQLTYPE="CF_SQL_VARCHAR" >, 
				<cf_queryparam value="#getEventFlagGroup.FlagGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >, 
				<cf_queryparam value="#frmFlagTextID#" CFSQLTYPE="CF_SQL_VARCHAR" >,
				1, 
				<cf_queryparam value="#request.relayCurrentUser.personid#" CFSQLTYPE="cf_sql_integer" >, 
				<cf_queryparam value="#CreateODBCDate(FreezeDate)#" CFSQLTYPE="CF_SQL_TIMESTAMP" >, 
				<cf_queryparam value="#request.relayCurrentUser.personid#" CFSQLTYPE="cf_sql_integer" >, 
				<cf_queryparam value="#CreateODBCDate(FreezeDate)#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
			)
		</CFQUERY>
		
		<CFQUERY NAME="getEventFlag" datasource="#application.siteDataSource#">
			SELECT 
				FlagID
			FROM 
				Flag
			WHERE 
				FlagTextID =  <cf_queryparam value="#frmFlagTextID#" CFSQLTYPE="CF_SQL_VARCHAR" > 
		</CFQUERY>
		
		<CFSET newFlagID = getEventFlag.FlagID>		
		
		<!--- insert the event - take data from the original data --->

		<CFQUERY NAME="addEvent" datasource="#application.siteDataSource#">
			INSERT INTO EventDetail
			(
				FlagID, 
				OwnerID, 
				Title, 
				Location,
				VenueRoom,
				CountryID,
				EstAttendees,
				EventGroupID, 
				PreferredStartDate, 
				PreferredEndDate,
				Email,
				EmailCC,
				UseAgency,
				Agenda,
				InvitationType,
				PartnerType,
				AudienceType,
				Created, 
				CreatedBy, 
				LastUpdated, 
				LastUpdatedBy
			)
			select
				<cf_queryparam value="#newFlagID#" CFSQLTYPE="CF_SQL_INTEGER" >, 
				<cf_queryparam value="#request.relayCurrentUser.personid#" CFSQLTYPE="cf_sql_integer" >, 
				Title,
				<cf_queryparam value="#frmLocation#" CFSQLTYPE="CF_SQL_VARCHAR" >,
				<cf_queryparam value="#frmVenueRoom#" CFSQLTYPE="CF_SQL_VARCHAR" >,
				CountryID,
				EstAttendees,
				EventGroupID, 
				<cf_queryparam value="#createodbcdate(frmPreferredStartDate)#" CFSQLTYPE="CF_SQL_TIMESTAMP" >, 
				<cf_queryparam value="#createodbcdate(frmPreferredEndDate)#" CFSQLTYPE="CF_SQL_TIMESTAMP" >, 
				Email,
				EmailCC,
				UseAgency,
				Agenda,
				InvitationType,
				PartnerType,
				AudienceType,
				<cf_queryparam value="#CreateODBCDate(FreezeDate)#" CFSQLTYPE="CF_SQL_TIMESTAMP" >, 
				<cf_queryparam value="#request.relayCurrentUser.personid#" CFSQLTYPE="cf_sql_integer" >, 
				<cf_queryparam value="#CreateODBCDate(FreezeDate)#" CFSQLTYPE="CF_SQL_TIMESTAMP" >, 
				#request.relayCurrentUser.personid#
			from
				EventDetail
			where
				FlagID =  <cf_queryparam value="#cloneFlagID#" CFSQLTYPE="CF_SQL_INTEGER" > 				
		</CFQUERY>
		
		<CFQUERY NAME="addCountries" datasource="#application.siteDataSource#">
			INSERT INTO EventCountry
			(
				FlagID, 
				CountryID, 
				LastUpdated, 
				LastUpdatedBy
			)
			select
				<cf_queryparam value="#newFlagID#" CFSQLTYPE="CF_SQL_INTEGER" >, 
				CountryID, 
				<cf_queryparam value="#CreateODBCDate(FreezeDate)#" CFSQLTYPE="CF_SQL_TIMESTAMP" >, 
				<cf_queryparam value="#request.relayCurrentUser.personid#" CFSQLTYPE="CF_SQL_INTEGER" >
			from
				EventCountry
			where
				FlagID =  <cf_queryparam value="#cloneFlagID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</CFQUERY>
	</CFIF>

	<!--- insert any event managers --->
	<cfif getassigned.recordcount gt 0>
		<cfset RecordID = #newFlagID#>
		<cfset Entity = "Flag">
		<cfset Shortname = "EventTask">
		
		<CFQUERY NAME="UpdateManagers" datasource="#application.siteDataSource#">
			INSERT INTO RecordRights
			(
				UserGroupID,
				Entity, 
				RecordID, 
				Permission
			)
			SELECT 
				rr.UserGroupID,
				<cf_queryparam value="#Entity#" CFSQLTYPE="CF_SQL_VARCHAR" >, 
				<cf_queryparam value="#RecordID#" CFSQLTYPE="CF_SQL_INTEGER" >, 
				0			
			FROM 
				RecordRights AS rr 
				INNER JOIN UserGroup AS ug ON rr.UserGroupID = ug.UserGroupID
			WHERE 
				rr.RecordID =  <cf_queryparam value="#cloneFlagID#" CFSQLTYPE="CF_SQL_INTEGER" > 
				AND rr.Entity = 'Flag'
			ORDER 
				BY ug.Name
		</CFQUERY>
	</cfif>
	<!--- record should now have been successfully added - return to the eventlist --->
	<cflocation url="eventlist.cfm"addToken="false">

	<CF_ABORT>
</CFIF>



<FORM NAME="UpdateEvent" ACTION="eventClone.cfm" METHOD="post">
<CF_INPUT type="hidden" name="FlagID" value="#cloneFlagID#">
<INPUT TYPE="Hidden" NAME="frmAction" VALUE="AddEvent">
<TABLE CELLPADDING="2" CELLSPACING="2" BORDER="0">
<CFOUTPUT>
	<CFIF Msg NEQ "">
		<TR>
			<TD COLSPAN="3" ALIGN="center"><FONT COLOR="Red">#htmleditformat(Msg)#</FONT></TD>
		</TR>
	</CFIF>
<TR>
	<TD CLASS="label" ALIGN="right">Group this Event belongs to:</TD>
	<TD WIDTH="20">&nbsp;</TD>
	<TD colspan="2">
		#htmleditformat(getEventDetails.eventName)#
	</TD>
</TR>
	<TR>
		<TD CLASS="label" valign="top" ALIGN="right">City:</TD>
		<TD WIDTH="20">&nbsp;</TD>
		<TD colspan="2"><CF_INPUT type="text" name="frmLocation" size="40" maxlength="100" value="#getEventDetails.location#"></TD>
	</TR>
	<TR>
		<TD CLASS="label" valign="top" ALIGN="right">Venue:</TD>
		<TD WIDTH="20">&nbsp;</TD>
		<TD colspan="2"><textarea name="frmVenueRoom" cols="40" rows="4">#getEventDetails.VenueRoom#</textarea></TD>
	</TR>
	<TR>
		<TD CLASS="label" valign="top" ALIGN="right" VALIGN="top">Country:</TD>
		<TD WIDTH="20">&nbsp;</TD>
		<TD VALIGN="top" colspan="2">
			#htmleditformat(getEventDetails.countryDescription)#
		</TD>
	</TR>
	<TR>
		<TD CLASS="label" valign="top" ALIGN="right">Estimated no of attendees:</TD>
		<TD WIDTH="20">&nbsp;</TD>
		<TD colspan="2">#htmleditformat(getEventDetails.EstAttendees)#</TD>
	</TR>
	<TR>
		<TD CLASS="label" valign="top" ALIGN="right">Preferred Start Date:</TD>
		<TD WIDTH="20">&nbsp;</TD>
		<TD colspan="2"><CF_INPUT NAME="frmPreferredStartDate" TYPE="Text" SIZE="11" MAXLENGTH="11" VALUE="#frmPreferredStartDate#"> &nbsp; (dd-mmm-yyyy)</TD>
	</TR>
	<TR>
		<TD CLASS="label" valign="top" ALIGN="right">Preferred End Date:</TD>
		<TD WIDTH="20">&nbsp;</TD>
		<TD colspan="2"><CF_INPUT NAME="frmPreferredEndDate" TYPE="Text" SIZE="11" MAXLENGTH="11" VALUE="#frmPreferredEndDate#"> &nbsp; (dd-mmm-yyyy)</TD>
	</TR>
	<TR>
		<TD CLASS="Label" valign="top" align="right">Email (To):</TD>
		<TD WIDTH="20">&nbsp;</TD>
		<TD colspan="2">
			#htmleditformat(getEventDetails.Email)#
		</TD>
	</TR>
	<TR>
		<TD CLASS="Label" valign="top" align="right">Email (CC):</TD>
		<TD WIDTH="20">&nbsp;</TD>
		<TD colspan="2">
			#htmleditformat(getEventDetails.EmailCC)#
		</TD>
	</TR>
</CFOUTPUT>
<TR>
	<TD CLASS="label" ALIGN="right" VALIGN="top">Countries eligible to attend:</TD>
	<TD WIDTH="20">&nbsp;</TD>
	<TD VALIGN="top" colspan="2">
		<cfoutput query="countries">
			#htmleditformat(countryDescription)#<br>
		</cfoutput>
	</TD>		
</TR>

<TR>
	<TD CLASS="label" ALIGN="right" VALIGN="top">Use Agency:</TD>
	<TD WIDTH="20">&nbsp;</TD>
	<TD VALIGN="top" colspan="2">		
		<cfif getEventDetails.UseAgency>Yes<cfelse>No</cfif>
	</TD>
</TR>

<TR>
	<TD CLASS="label" ALIGN="right">Invitation Type:</TD>
	<TD WIDTH="20">&nbsp;</TD>
	<TD valign="top" colspan="2">
		<cfif getEventDetails.InvitationType is "open">Open to All<cfelse>By Invitation only</cfif>
	</TD>
</TR>

<TR>
	<TD CLASS="label" ALIGN="right">Partner Type:</TD>
	<TD WIDTH="20">&nbsp;</TD>
	<TD valign="top" colspan="2">
		<cfoutput query="getPartnerTypes">
			<cfif listcontains(getEventDetails.PartnerType,getPartnerTypes.FlagID)>
				#htmleditformat(name)#&nbsp;
			</cfif>
		</cfoutput>
		<!---<cfif getEventDetails.PartnerType is "NP">nPP Only<cfelse>Open to all</cfif>--->
	</TD>
</TR>

<TR>
	<TD CLASS="label" ALIGN="right">Audience:</TD>
	<TD WIDTH="20">&nbsp;</TD>
	<TD valign="top" colspan="2">
		<cfif getEventDetails.AudienceType contains "Sales">
			Sales<br>
		</cfif>
		<cfif getEventDetails.AudienceType contains "Pre-Sales">
			Pre-Sales
		</cfif>		
	</TD>
</TR>

<TR>
	<TD CLASS="label" ALIGN="right" valign="top">Agenda:</TD>
	<TD WIDTH="20">&nbsp;</TD>
	<TD valign="top" colspan="2">
		<cfoutput>#htmleditformat(getEventDetails.Agenda)#</cfoutput>
	</TD>
</TR>
<TR>
	<TD CLASS="Label" align="right" VALIGN="top">Managers Assigned to this Event</TD>
	<TD WIDTH="20">&nbsp;</TD>
	<TD VALIGN="top" colspan="2">
		<CFOUTPUT QUERY="GetAssigned">#htmleditformat(Name)#<BR></CFOUTPUT><br>		
	</TD>
</TR>

</TABLE>
</FORM>

<cfinclude template="eventFooter.cfm">
