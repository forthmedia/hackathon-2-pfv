<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File:			sql_getEmails.cfm
Author:			DJH
Date created:	15 February 2000

Description:	Gets a list of the default emails/specific emails for the specified eventDetail

Version history:1

2011/10/05 PPB LID7683 allow suppression of confirmation email
--->

<cfquery name="insertEventDetailEmail" datasource="#application.siteDataSource#">
insert into EventDetailEmail
(
	FlagID,
	EventDetailEmailType,
	Subject,
	MessageText,
	SuppressEmail
)
values
(
	<cf_queryparam value="#FlagID#" CFSQLTYPE="CF_SQL_INTEGER" >,
	<cf_queryparam value="#EmailType#" CFSQLTYPE="CF_SQL_VARCHAR" >,
	<cf_queryparam value="#frmSubject#" CFSQLTYPE="CF_SQL_VARCHAR" >,
	<cf_queryparam value="#frmMessageText#" CFSQLTYPE="CF_SQL_VARCHAR" >,
	<cf_queryparam value="#iif(IsDefined("frmSuppressEmail"),1,0)#" CFSQLTYPE="CF_SQL_bit" >
)
</cfquery>



