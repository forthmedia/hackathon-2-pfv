<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Mods:
WAB 1999-12-01: This code wasn't filtering the addition of delegates by the users countries.  This caused problems when using selections containing people from multiple countries that the user didn't necessarily have rights to
DJH 2000-03-28: Modified to allow it to be used with the event wizard functionality.
 --->

<!--- 2012-07-25 PPB P-SMA001 commented out
<CFINCLUDE template="..\TEMPLATES\qryGetCountries.cfm">
 --->
<CFSET FreezeDate = request.requestTimeODBC>

<!--- add selections to the event if required --->

<CFIF IsDefined("form.Selections")>
	<CFTRANSACTION>
	<CFLOOP LIST="#form.Selections#" INDEX="ListItem">
		<CFQUERY NAME="addFlagEvent" datasource="#application.siteDataSource#">
			INSERT INTO #frmEventTable#
			(EntityID,FlagID,FirstName,LastName,SiteName,CountryID,Country,Email,LastUpdated,LastUpdatedby,Created, Createdby, RegStatus,LocationID)
				SELECT p.PersonID,<cf_queryparam value="#FlagID#" CFSQLTYPE="CF_SQL_INTEGER" >,p.FirstName,p.LastName,l.SiteName,c.CountryID,c.CountryDescription,p.email,<cf_queryparam value="#CreateODBCDateTime(FreezeDate)#" CFSQLTYPE="cf_sql_timestamp" >,<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >,<cf_queryparam value="#CreateODBCDateTime(FreezeDate)#" CFSQLTYPE="cf_sql_timestamp" >,<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >,'To Be Invited',l.locationID
				FROM Person AS p, SelectionTag AS st, Location AS l, Country AS c
				WHERE p.PersonID = st.EntityID
				AND p.LocationID = l.LocationID
				AND l.CountryID = c.CountryID
				<!--- 2012-07-25 PPB P-SMA001 commented out
				AND l.countryid  in ( <cf_queryparam value="#countrylist#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
				 --->
				#application.com.rights.getRightsFilterWhereClause(entityType="person",alias="p").whereClause# 	<!--- 2012-07-25 PPB P-SMA001 --->
				AND st.Status <> 0
				AND st.SelectionID =  <cf_queryparam value="#ListItem#" CFSQLTYPE="CF_SQL_INTEGER" > 
				AND NOT EXISTS (SELECT * FROM #frmEventTable# AS et WHERE st.EntityID = et.EntityID AND et.FlagID =  <cf_queryparam value="#FlagID#" CFSQLTYPE="CF_SQL_INTEGER" > )
		</CFQUERY>

		<!--- WAB altered so that selections can be added to an event more than once --->
		<!--- has this selection already been added to the event? --->
		<!--- then either update or add a record to event selections --->
		<CFQUERY NAME="checkEventSelections" datasource="#application.siteDataSource#">
			select * from EventSelections 
			where FlagID =  <cf_queryparam value="#FlagID#" CFSQLTYPE="CF_SQL_INTEGER" > 
			and SelectionID =  <cf_queryparam value="#ListItem#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</CFQUERY>
				
		<CFIF checkEventSelections.recordcount IS 0>
			<CFQUERY NAME="addEventSelections" datasource="#application.siteDataSource#">
			INSERT INTO EventSelections
				(FlagID, SelectionID, Created, Createdby, LastUpdated, LastUpdatedBy)
				VALUES
				(<cf_queryparam value="#FlagID#" CFSQLTYPE="CF_SQL_INTEGER" >, <cf_queryparam value="#ListItem#" CFSQLTYPE="CF_SQL_INTEGER" > ,<cf_queryparam value="#CreateODBCDateTime(FreezeDate)#" CFSQLTYPE="cf_sql_timestamp" >,<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >,<cf_queryparam value="#CreateODBCDateTime(FreezeDate)#" CFSQLTYPE="cf_sql_timestamp" >,<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >)
			</CFQUERY>
		<CFELSE>
			<CFQUERY NAME="updateEventSelections" datasource="#application.siteDataSource#">
			update eventselections
			set lastupdated = #CreateODBCDateTime(FreezeDate)#,
			lastupdatedby = #request.relayCurrentUser.usergroupid#
			where FlagID =  <cf_queryparam value="#FlagID#" CFSQLTYPE="CF_SQL_INTEGER" > 
			and SelectionID =  <cf_queryparam value="#ListItem#" CFSQLTYPE="CF_SQL_INTEGER" > 
			</CFQUERY>
		</cfif>
	</CFLOOP>
	<cf_eventNotificationEmail action="compile" changes="Potential Delegates added">
	<cf_eventNotificationEmail action="send" FlagID="#FlagID#">
	</CFTRANSACTION>
<CFELSE>
	<CFINCLUDE TEMPLATE="eventTask.cfm">
	<CF_ABORT>
</CFIF>


<cfif not isdefined("session.eventwizard")>
	
	
	<cf_head>
	<cf_title>Events - Add Delegates</cf_title>
	<SCRIPT type="text/javascript">
	<!--
	function submitForm() {	
		document.myForm.submit();
		<CFIF FindNoCase("msie", cgi.http_user_agent) GT 0>
			window.close();
		<CFELSE>
			setInterval("window.close()", 1000);
		</CFIF>
	}
	//-->
	</SCRIPT>
	
	</cf_head>
	
	Selections updated successfully.
	<P>
	<FORM NAME="myForm" ACTION="eventDelegateList.cfm" TARGET="main" METHOD="post">
	<CFOUTPUT>
	<CF_INPUT TYPE="Hidden" NAME="FlagID" VALUE="#FlagID#">
	</CFOUTPUT>
	<INPUT TYPE="Button" VALUE="Close Window" onClick="javascript: submitForm();">
	</FORM>
	
	
<cfelse>
	<cfif session.EventWizard is "true">
		<cflocation url="eventDelegateList.cfm?eventStep=3a&FlagID=#FlagID#"addToken="false">
	</cfif>
</cfif>