<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File:			eventFooter.cfm
Author:			DJH
Date created:	11 February 2000

Description:
Displays a bar along the bottom offering help, return to top, and save functionality

Version history:
1
2014-10-29	RPW	CORE-803 Hide Register and Unregister buttons not working under Events module
2014-11-14 	AXA CORE-923 introduction of displaySave was defaulting the save button as hidden.  changed default to true to display button.
2014-11-14  SB Removed all tables.
--->

<cfparam name="DisplaySave" default="yes"> <!--- 2014-11-14 AXA CORE-923 default to yes otherwise button is always hidden --->
<cfif isdefined("IsOwner")>
	<cfif not IsOwner>
		<cfset DisplaySave="No">
	</cfif>
</cfif>

<div id="eventFooterContainer">
	<div id="eventFooterContainer1stRow">
		<!--- 2014-10-29	RPW	CORE-803 Hide Register and Unregister buttons not working under Events module --->
		<cfif session.eventWizard is "true" and cgi.script_name does not contain "eventList.cfm" AND DisplaySave>
			<form>
				<input type="button" value="Save" id="eventFooterSave" class="btn btn-primary" onClick="javascript:void(submitForm(0))">&nbsp;&nbsp;
				<!--- <cfif displaysave is "no">
					<input type="button" value="Back" onClick="javascript:void(submitForm(-1))">&nbsp;&nbsp;
				<cfelse>
					<input type="button" value="Save" onClick="javascript:void(submitForm(0))">&nbsp;&nbsp;
				</cfif>
				<cfparam name="displaynext" default="yes">
				<cfif displaynext is "yes">
					<input type="button" value="Next" onClick="javascript:void(submitForm(1))">
				</cfif> --->
			</form>
		</cfif>
	</div>
	<div id="eventFooterContainer2ndRow">
		<div id="eventFooterContainerSave">
			<cfif ShowSave><a href="javascript:void(submitForm())" Class="Button btn btn-primary">Save this record</a></cfif>
		</div>
		<!--- <div id="scrollTo">
			<a href="javascript:window.scrollTo(0,0);"><span><b>Top</b></span></a>
		</div> --->
	</div>
</div>