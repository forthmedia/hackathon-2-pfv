<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File:			sql_getEmails.cfm
Author:			DJH
Date created:	15 February 2000

Description:	Gets a list of the default emails/specific emails for the specified eventDetail

Version history:

2011/10/13	PPB		LID7683  added SuppressEmail to the query  
2011/10/14	PPB		LID7683  added ISNULL around SuppressEmail in the query in case EventDetailEmail row doesn't exist yet

--->

<cfparam name="emailType" default="confirmation">
<cfquery name="getEmails" datasource="#application.siteDataSource#">
select top 1
	ege.EventGroupID,
	ege.EventGroupEmailType as emailType,
	ege.subject as defaultSubject,
	ege.messagetext as defaultMessageText,
	ede.subject,
	ede.messagetext,
	ISNULL(ede.SuppressEmail,0) AS SuppressEmail
from
	EventDetail as ed 
	INNER JOIN 
	EventGroupEmail as ege
	ON ege.EventGroupID = ed.EventGroupID
	LEFT JOIN EventDetailEmail as ede
	ON ege.EventGroupEmailType = ede.EventDetailEmailType
	and ede.FlagID =  <cf_queryparam value="#FlagID#" CFSQLTYPE="CF_SQL_INTEGER" > 
where
	ed.FlagID =  <cf_queryparam value="#FlagID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	<cfif isDefined("emailtype")>
		and ege.EventGroupEmailType =  <cf_queryparam value="#emailType#" CFSQLTYPE="CF_SQL_VARCHAR" > 
	</cfif>
order by
	ege.EventGroupEmailType desc
</cfquery>

<!--- if the event group email hasn't been created, create some now. --->

<cfif getEmails.recordcount is 0>
	<!--- get this event group id --->
	
	<cfquery name="getEventGroup" datasource="#application.siteDataSource#">
		select
			EventGroupID
		from
			EventDetail
		where
			FlagID =  <cf_queryparam value="#FlagID#" CFSQLTYPE="CF_SQL_INTEGER" > 			
	</cfquery>
	
	<!--- add the default emails --->
	
	<!--- 2007-02-15 SWJ I have commented this out for the time being I hope this will help
	
	<cfquery name="insertInvitation" datasource="#application.siteDataSource#">
	insert into eventGroupEmail
	(
		EventGroupID,
		EventGroupEmailType,
		Subject
	)
	Values
	(
		#getEventGroup.eventGroupID#,
		'Invitation',
		'Event Invitation'
	)
	</cfquery>	 --->

	<cfquery name="insertConfirmation" datasource="#application.siteDataSource#">
	insert into eventGroupEmail
	(
		EventGroupID,
		EventGroupEmailType,
		Subject
	)
	Values
	(
		<cf_queryparam value="#getEventGroup.eventGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >,
		'Confirmation',
		N'Event Confirmation'
	)
	</cfquery>	

	
	<!--- finally include this template so that the query above is re-run --->
	<!--- <cfinclude template="sql_GetEmails.cfm"> --->
</cfif>

