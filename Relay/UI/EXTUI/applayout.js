/* �Relayware. All Rights Reserved 2014 */
/*
File name: applayout.js
 
2008/10/28	WAB/NH   add code to open specific tabs on opening
2008/11/05ish WAB	Added code to display a loading icon
2009/01/05 	WAB		Bug 1485 Added function maximiseIframe for dealing with Iframes within the tab Iframes
2010/04/18 WAB		Corrected code to display loading icon - added lots of var'ing.  Corrected problem when lots of tabs opened consecutively and loading icons did not disappear
2012-11-20 		WAB	432169  improve loading icon, works on non relayware pages, write div to dom rather than having it always there, centre correctly, use onload
2012-12-10	WAB	Problem with Loading Icon in IE (loading icon on tab continued to show).  Set loadingDiv variable to null after the loading div is removed.
2013-03-04	PPB/WAB	Case 433834 Problem with Loading Icon in IE
WAB 2014-09-15 attempt to implement single scrollbar
15/09/2014 SB - Slide in and out menu
2015-12-02 DAN Case 446698 - fixing height for displaying the iframe

 SLIDE OUT MAIN MENU
*/

		Ext.ux.DDTabPanel = Ext.extend(Ext.TabPanel, {

			/**
			 * @method initEvents
			 *
			 *   Overwritten: declare the tabpanel as a drop target
			 */
			initEvents: function(){
				Ext.ux.DDTabPanel.superclass.initEvents.call(this);
				this.dd = new Ext.ux.DDTabPanel.DropTarget(this, {
					ddGroup: 'dd-tabpanel-group'
				});
			},
			
			/**
			 * @method initTab
			 * 	Overwritten: init the drag source after (!) rendering the tab
			 *
			 * @param {Object} tab
			 * @param {Object} index
			 */
			initTab: function(tab, index){
				Ext.ux.DDTabPanel.superclass.initTab.call(this, tab, index);
				tab.position = (index + 1) * 2; // 2, 4, 6, 8, ... (2n)
				tab.on('render', function(tab){
					var id = this.id + '__' + tab.id;
					tab.ds = new Ext.dd.DragSource(id, {
						ddGroup: 'dd-tabpanel-group',
						dropEl: tab,
						dropElHeader: Ext.get(id, true)
					});
					
					tab.ds.beforeDragEnter = function(target, event, id){
						target.tabpanel.activate(this.dropEl);
					};

				}, this);

				// force rendering of the tab
				tab.show();
			}
		});

		/**
		 * @class Ext.ux.DDTabPanel.DropTarget
		 *
		 * 	Implements the drop behavior of the tabpanel
		 *
		 * @param {Object} tabpanel
		 * @param {Object} cfg
		 */
		Ext.ux.DDTabPanel.DropTarget = function(tabpanel, config){
			this.tabpanel = tabpanel;
			// target is the header area of the given tabpanel
			var target = Ext.select('div.x-tab-panel-header', false, 'div#panel').elements[0];
			Ext.ux.DDTabPanel.DropTarget.superclass.constructor.call(this, target, config);
		};

		Ext.extend(Ext.ux.DDTabPanel.DropTarget, Ext.dd.DropTarget, {
			notifyOver: function(dd, e, data){
				if (this.tabpanel.items.length < 2) {
					return 'x-dd-drop-nodrop';
				}
				return 'x-dd-drop-ok';
			},
			
			notifyDrop: function(dd, e, data){
			
				var tabPanelOffset = this.tabpanel.el.dom.offsetLeft;
				var tabs = this.tabpanel.items;
				
				// at this point the items in 'tabs' are sorted by their positions
				var eventPosX = e.getXY()[0];
				var last = tabs.length;
				var newPos = last;
				dd.dropEl.position = last * 2 + 1; // default: 'behind the rest'
				
				for (var i = 0; i < last; i++) {
					var tab = tabs.itemAt(i);
					// Is this tab target of the drop operation?
					var dom = tab.ds.dropElHeader.dom;
					var tabLeft = tabPanelOffset + dom.offsetLeft;
					var tabRight = tabLeft + dom.clientWidth;
					var tabMiddle = tabLeft + dom.clientWidth / 2;
					
					if (eventPosX <= tabRight) {
						dd.dropEl.position = eventPosX > tabMiddle ? tab.position + 1 : tab.position - 1;
						newPos = eventPosX > tabMiddle ? i + 1 : i;
						break;
					}
				}
				
				dd.proxy.hide();
				//console.log('newPos ' + newPos);
				
				dd.el.dom.parentNode.insertBefore(dd.el.dom, dd.el.dom.parentNode.childNodes[newPos]);
				
				// sort tabs by their actual position 
				tabs.sort('ASC', function(a, b){
					return a.position - b.position;
				})
				// adjust tab position values
				tabs.each(function(tab, index){
					tab.position = (index + 1) * 2;
				});
			}
		});
		Ext.onReady(function(){
			Ext.state.Manager.setProvider(new Ext.state.CookieProvider());
			//Ext.QuickTips.init();
			Ext.BLANK_IMAGE_URL = '/javascript/ext/docs/resources/s.gif';
			
			   var viewport = new Ext.Viewport({
					layout:'border',
					items:[
						new Ext.BoxComponent({ // raw
							region:'north',
							el: 'north'
							//height:53
						})/*,{
							region:'south',
							title:'Feeds',
							contentEl: 'south',
							split:true,
							height: 100,
							minSize: 100,
							maxSize: 200,
							collapsible: true,  
							margins:'0 0 0 0'
						}, {
							region:'east',
							title: 'Help',
							collapsible: true,
							split:true,
							width: 225,
							minSize: 175,
							maxSize: 400,
							layout:'fit',
							margins:'0 5 0 0',
							items:
								new Ext.TabPanel({
									border:false,
									activeTab:0,
									tabPosition:'bottom',
									items:[{
										html:'<p>System Help</p>',
										title: 'System Help',
										autoScroll:true
									},
									{
										html:'<p>FAQs</p>',
										title: 'FAQ',
										autoScroll:true
									}
								   ]
								})
						 }*/,{
							region:'west',
							id:'west-panel',
							title:'Menu',
							//split:true,
							//width: 180,
							//minSize: 150,
							//maxSize: 230,
							collapsible: false,
							//margins:'0 0 0 5',
							contentEl:'west',
							autoScroll: false
						},
						// Center Section Tab Control
						Ext.centerTabs = new Ext.ux.DDTabPanel({
							region:'center',
							id:'relayTabPanel',
							deferredRender:false,
							activeTab:0,
							enableTabScroll:true,
							autoScroll:false,
							items:[{
								contentEl:'tabArea',
								title: 'Home',
								id: 'homeTab',
								iconCls: 'homeIcon',
								closable:false,
								containerScroll:false,
								//tabTip:'Home',
								html: '<div class="row"><iframe name="homeIframe" id="homeIframe" class="col-lg-12 col-md-12 col-sm-12" frameborder="0" src="/homepage/homepage.cfm"></iframe></div>'
							}],
								/* NJH Case 435608 2013/06/17 - bit of a HACK for IE 10 - for some reason, content in framesets was not appearing even though it was there. It seemed that by changing the style slightly, that it would reforce the IE engine to redraw 
								the content. So, here we are setting the height of the iframe containing the content to 99%, and then back to 100% */ 
							listeners: {
					            'tabchange': function (tabPanel, tab) {
									if (jQuery.browser.msie && jQuery.browser.version == '10.0') {
										var iframeID = tab.id;
										iframeID = iframeID.replace('theTab_','tabIFrame_');
										jQuery('#'+iframeID).css('height','99%');
										window.setTimeout(function(){jQuery('#'+iframeID).css('height','100%');},100);
									}
					            }
					        }
						})
					 ]
				});
				
				/* WAB 2014-09-15 attempt to implement single scrollbar */
				//iFrameResize({heightCalculationMethod:'offset',resizedCallback:function(){console.log('home iframe resized')}},'#homeIframe');

				Ext.QuickTips.init();
				var index = 0;
				Ext.addTab = function(tabName,tabTitle,href,options){
					// set the default options					
					this.options = {
						reuseTab: false,
						showLoadingIcon: true, 
						reuseTabJSFunction: null,
						iconClass: 'defaultIcon'
						//tabTip: tabTitle
					}
					
					tabName = tabName.replace(/[^0-9a-zA-Z_]/g,'') // CASE 431435
					
					Object.extend(this.options, options || { });   // this is a function in prototype.js  - effectively overwrite this.options with any items passed in
					//alert(this.options.iconClass);
					index++;
					if (this.options.reuseTab) {
						myIndex = ''
					} else {	
						myIndex = index
					}
					
					tabName = tabName.replace(/ _/,''); //get rid of spaces and underscores
					var tabID = 'theTab_'+ tabName+myIndex;
					var iframeID = 'tabIFrame_' + tabName+myIndex

					// reuse tab if options.reuseTab set and if tab already exists with this name/ID
					reuseTab = this.options.reuseTab && Ext.centerTabs.getItem(tabID)

					if (reuseTab) {  
						// alert ('try to reuse tab')
						if (this.options.reuseTabJSFunction) {
	
									// for some reason this line did not work
									// worked fine when a tab was first opened, but if tab was closed then reopened and then reused it didn't work as expected
									// instead loop through collection of known tabs until find a matching one
									iframeObject = window.frames[iframeID]
									// 
		
								/*	for (i=1; i<window.frames.length;i++) {
alert (i)
alert (window.frames[i].id )
										if (window.frames[i].name == iframeID) {
											iframeObject = window.frames[i]
											break
										}
									}
*/
									
							// when reusing the tab, call this bit of javascript
							reuseTabSuccess = this.options.reuseTabJSFunction (iframeObject )
						} 
						
						
							// if no JS function or if JS returned a failure code then just overwrite whatever is there
						if (!this.options.reuseTabJSFunction || !reuseTabSuccess){

							window.frames[iframeID].location.href = href; 
							reuseTabSuccess = true;

						}
	

							// WAB 2012-02-25 change name and icon when re-using tab (usually will be the same, but not always)
							tabObject = Ext.centerTabs.getItem(tabID)
							Ext.changeTabIcon (tabObject,this.options.iconClass)
							tabObject.setTitle(this.options.tabTip)

							Ext.centerTabs.activate(tabID)						
					
					} else {
					
					
		
//						if (typeof(tabTitle) == "undefined") {
//							var tabTitle = "New Tab" + index;
//						}
//						if (typeof(iconClass) == "undefined") {
//							var iconClass = "defaultIcon";
//						}
	
						var iframecall = '<div class="row"><iframe name="'+iframeID+'" id="'+iframeID+ '" src = "' + href + '"  '    + '" class="col-lg-12 col-md-12 col-sm-12 lowerPage" frameborder="0"></iframe></div>';

//						if (typeof(tabTip) == "undefined") {
//							var tabTip = tabTitle;
//						}

						var newTab = Ext.centerTabs.add({
							title: tabTitle,
							id: tabID,
							border: false,
							iconCls: this.options.iconClass,
							containerScroll:false,
							autoScroll:false,
							tabTip: this.options.tabTip,
							html: iframecall,
							closable:true,
							iframeID:iframeID
						}).show();
						
						/* WAB 2014-09-15 attempt to implement single scrollbar */
						//iFrameResize({heightCalculationMethod:'offset',resizedCallback:function(){console.log(iframeID + ' frame resized')}},'#' + iframeID);
						
						// I am going to save a reference to the IFrame in the tabObject
						// unfortunately there seem to be two different ways of referencing an Iframe and I seem to be able to do 
						// some things with one but not the other
						newTab.iFrameObject = document.getElementById (iframeID )
						newTab.iFrameWindowObject = window.frames[iframeID]

						if (this.options.showLoadingIcon) {

							Ext.showLoadingIconOnIFrame (newTab.iFrameObject)
							
						}

						//new Ext.ToolTip({target:tabID, title: '',autoHide : true, html: tabTip ,showDelay:60, autoHeight:true,width:150});
						/*newTab.on('mouseover', function(tab){
							//new Ext.ToolTip({target: e.target, title: '',autoHide : true, htmlepartmentInfo.toString() ,showDelay:60,autoHeight:true,width:300});
							alert("click");
						});*/
						Ext.QuickTips.init();
					}
						
					return tabID;
				}

			//reloadRecentSearches()
	
			// changes Icon Class - returns the old class
			Ext.changeTabIcon =function (tabObject,iconCls) { 
					currentIcon = tabObject.iconCls
			
					tabElement = Ext.centerTabs.getTabEl(tabObject)
					Ext.fly(tabElement).child('.x-tab-strip-text').replaceClass(currentIcon,iconCls);
					tabObject.setIconClass(iconCls);  // Thought that this function would do the rendering, but doesn't seem to - hence the line above which actually changes the class.  This function just keeps the iconClass sored in the JS structure in line with what is happening on the ground
				
				return currentIcon
			
			}



			/* 	WAB 2008/11/05 trying out a loading message
				shows loading icons after a short delay
				then hides them again when the page is loaded (or a window is hidden)
				using a div which can be displayed over the Iframe 
				WAB 2012-11-20 recoded to work better and on non-relayware pages.  now uses an onload event (much more sensible!)	
			*/
			Ext.showLoadingIcon = function (theWindow,delay) {
				return Ext.showLoadingIconOnIFrame (theWindow.frameElement,delay) 
			}


			Ext.showLoadingIconOnIFrame =function (iFrameObject,delay) {
					var tabObject = Ext.getReferenceToTabContainingThisObject (iFrameObject)

						// create a loading icon div, hidden for time being
						// In theory we could created the div in the body of the current document, but in IE the 'blank' document does not have a body
						// Also in cross domain situations we do not have access to the document
						// So instead we create it as a sibling of the IFRAME and then position it

						var loadingDiv = Ext.DomHelper.insertAfter (iFrameObject,{tag:'div',style:'position:absolute;z-index:1;display:none',html:'<img src="/images/misc/loading.svg" >'})
							loadingDiv.style.left = (iFrameObject.clientWidth/2 +100) + 'px'
							loadingDiv.style.top = iFrameObject.offsetTop + (iFrameObject.clientHeight/2 -50) + 'px'
						// store the tab icon class
						var iconCls = tabObject.iconCls
						
						// create a function to remove the icon when page loads
						Ext.get(iFrameObject).on('load', LoadFunctionName = function() {				// 2013-03-04 PPB/WAB Case 433834 now uses an event listener for IE8 compatibility
								// remove the loading icon and set the variable to null (otherwise when I test for it later, IE tells me it is stil there)
       							loadingDiv.parentNode.removeChild(loadingDiv)
       							loadingDiv = null
								// change the loading icon back to the correct one
								Ext.changeTabIcon (tabObject,iconCls)
								// get rid of the onload function 
								Ext.get(this).un('load', LoadFunctionName)			// 2013-03-04 PPB/WAB Case 433834
			            }
			            )		// 2013-03-04 PPB/WAB Case 433834
						var showLoadingIconAfterDelay = function () {
							
							// if after required delay the loading div is still there then show it
							if (loadingDiv)	{
								// change the tab icon
								var currentIcon = Ext.changeTabIcon (tabObject,'loadingIcon')
								// show the loading icon
								loadingDiv.style.display = '';

							}					

						}
		
						window.setTimeout(showLoadingIconAfterDelay,(delay)?delay:500) 

			}
	
	
			// gets Reference to the Tab IFrame which contains the object passed 
			Ext.getReferenceToTabIFrame  = function (obj) {

				/* WAB 2012-11-21 changed way this worked so could deal with cross domain frames
					reference the IFRAME element rather than the contentWindow
				*/	
				
					var thisFrame = (obj.window)?obj.window.frameElement:obj;   // either obj has a window property (or it is an iframe itself in which case the window is .contentWindow
					var regExp = /tabIFrame_/ ;
					// loop up through parents until we get to one whose name matches tabIFrame_* (or we get to the top)
						thisFrameParent = (thisFrame.parent)?thisFrame.parent:thisFrame.contentWindow.parent
					while (!regExp.test(thisFrame.name) && thisFrame != thisFrameParent) {
						thisFrame = thisFrameParent;
						thisFrameParent = (thisFrame.parent)?thisFrame.parent:thisFrame.contentWindow.parent
					}

					return thisFrame
			}

			Ext.getReferenceToTabContainingThisObject = function (obj) {
			
					iframename = Ext.getReferenceToTabIFrame(obj).name
				// the name of the tab is the same as the name of the iframe with 'tabIFrame_' replaced with 'theTab_'
				tabname = iframename.replace('tabIFrame_','theTab_')
	
				 return  Ext.centerTabs.getItem(tabname)
			}


			// 2008/10/28 Part of T-10 Bug Fix Issue 1
			// check for existence of openStartingTabs function  (set in mainlayout.cfm) 
			if (typeof openStartingTabs != 'undefined') {
				openStartingTabs ()
			}
	
		});

 	/* 
	  WAB 2009/01/05 	
 	  When we put an IFrame on a page we often want a section at the top with the Iframe below.
 	  We want the IFrame to be sized to 100% of the remaining height on the page.  
 		We use a hierachy of Divs to get this effect, however the height=100% does not work and the IFrame ends up the same height as the whole page, even if there is a bit of plain text at the top.
 		This is something to do with the page being within another IFrame (the Ext Iframe)
 		I have hacked together this function to try and get around the problem.
 		It looks at the size of the window and the size of the top bit and sets the Iframe height to what is remaining
 		Main problem is that it doesn't get updated it the whole IE window is resized.
 		If I really understood Ext then I might be able to plumb it in in some way

		Example layout here
			<div id ="wrapperDiv"  style="absolute;  left: 0;  top: 0;width:100%;height:100%; overflow:hidden">
			  <div id ="topDiv" style="z-index: 1;  position: absolute;  left: 0;  top: 0;  width:100%;height:200; overflow:hidden;"  >
			 	include content here
			  </div>
			  <div id="bottomDiv"   style="z-index:1; position:relative;   left:0;   top:200px;   width:100%;   height:100%;"> 
					<IFRAME SRC="xxx" width="100%" height="100%" MARGINHEIGHT=0 MARGINWIDTH=0 FRAMESPACING=0 FRAMEBORDER="0" NORESIZE SCROLLING="AUTO" BORDER=0 onload="relayUI.maximiseIframe(this)">
			 </div>
			</div>


 	*/	
 	
 	function maximiseIframe(obj) {
 	
		parentDiv = obj.parentNode
        siblingDiv = obj.parentNode.parentNode.getElementsByTagName('Div')[0]   // firstNode does not work in IE - blank space appears as first node

        // start: 2015-12-02 DAN Case 446698
		if (parentDiv.innerHTML === siblingDiv.innerHTML && parentDiv.clientHeight == siblingDiv.clientHeight) {
            // in case this is the same div (no siblingDiv)
            obj.height = parentDiv.clientHeight;
		} else {
            obj.height = parentDiv.clientHeight - siblingDiv.clientHeight;
        }
        // end: 2015-12-02 DAN Case 446698
 	
 	}
		
 	
 	/*===================
	  SB 15/09/14
	=====================*/
 	jQuery(document).ready(function() {
 		
 		slideOutMenu();
 		hoverDelay();
 		//mobileMenu();

 		/*===================
 		  SB 25/09/14: Sub Menu into two columns
 		=====================*/
 		jQuery('.subNav > li > ul').columnlist({ size: 2 });
 		
 		/*===================
 		  SB 15/09/14: SLIDE OUT MAIN MENU
 		=====================*/
 		
	 	function slideOutMenu(){ 
			
			jQuery('a#toggleCloseMenu').click(function(){
				
				jQuery('a#toggleCloseMenu').hide();
				jQuery('a#toggleOpenMenu').show();
				jQuery('#west-panel').addClass('west-panel-small');
				jQuery('#relayTabPanel').addClass('relayTabPanel-small');
				jQuery('#leftNav ul.subNav > li > ul').css('left', '3.7em');
				jQuery('#leftNav ul.subNav li div').addClass("removeText");
				jQuery('#relayTabPanel').css('left', '3.7em');
				jQuery('.subMenuTitle').css('display', 'block');
				jQuery('.navHeader').addClass('removeText');
								
			});
			
			jQuery('a#toggleOpenMenu').click(function(){
				
				jQuery('a#toggleCloseMenu').show();
				jQuery('a#toggleOpenMenu').hide();
				//jQuery('#west-panel').addClass( "col-xs-1" );
				jQuery('#west-panel').removeClass('west-panel-small');
				jQuery('#relayTabPanel').removeClass('relayTabPanel-small');
				jQuery('#leftNav ul.subNav > li > ul').css('left', '15em');
				jQuery('#leftNav ul.subNav li div').removeClass("removeText");
				jQuery('#relayTabPanel').css('left', '15.1em');
				jQuery('.subMenuTitle').css('display', 'none');
				jQuery('.navHeader').removeClass('removeText');
				
			});			
		}
	 	/*===================
		  SB 02/10/14: Hover delay on navigation and hover state on navigation
		=====================*/
	 	function hoverDelay(){
	 		function drops_show(){ 
	 	 		jQuery(this).addClass( 'show' ); 
	 	 		jQuery(this).removeClass( 'with-js' );
	 	 		
	 	 		//SB 20/10/15 - Bootstrap into Relayware - Adds left margin onto menu. 
	 	 		var x = jQuery( '#west-panel' ).width();
	 	 		var y = 0.5;
	 	 		var marginTotal = x - y;
	 	 		
	 	 		jQuery( '.drop' ).css("left", marginTotal)
	 	 		
	 	 		
	 	 	}
	 		
	 	 	function drops_hide(){ 
	 	 		jQuery(this).removeClass( 'show' ); 
	 	 		jQuery(this).addClass( 'with-js' ); 
	 	 	}
	 	 	
	 	 	function modileMenu(){ 
	 	 		jQuery(this).addClass( 'show' ); 
	 	 		jQuery(this).removeClass( 'with-js' );
	 	 		
	 	 		//SB 20/10/15 - Bootstrap into Relayware - Adds left margin onto menu. 
	 	 		var x = jQuery( '#west-panel' ).width();
	 	 		var y = 0.5;
	 	 		var marginTotal = x - y;
	 	 		
	 	 		jQuery( '.drop' ).css("left", marginTotal);
	 	 		
	 	 		e.stopPropagation();
	 	 	}
	 		
	 	 	// SB 19/02/16 - Added event for touchscreen devices  
	 	 	if (Modernizr.touchevents) {
	 	 		
	 	 		jQuery( '.drop a' ).click(function(){
	 	 			jQuery( '.leftNavSub' ).removeClass( 'show' ); 
	 	 			jQuery( '.leftNavSub' ).removeClass( 'with-js' );
	 	 			//jQuery( '.navSubHeader' ).removeClass( 'hover' );
	 			});
	 	 		
	 	 	} else {
	 	 		
	 	 		//Do nothing
	 	 		
	 	 	}
	
	 	 	// SB - Menu sensitivity  
	 		var config = {
	 	            sensitivity: 1,
	 	            interval: 80,
	 	            timeout: 200,
	 	            over: drops_show,
	 	            out: drops_hide
	 	    },
	 	    
	 	    config2 = {
	                sensitivity: 1,
	                interval: 1,
	                timeout: 300,
	                over: drops_show,
	 	            out: drops_hide
	            };
	 	        
	 		jQuery( "ul.subNav li.leftNavSub" ).each(function () {
	 			jQuery(this).hoverIntent(config);
	        });
	 		
	 		jQuery( ".mastHeadnavDropHover" ).each(function () {
	 			jQuery(this).hoverIntent(config2);
	        });
	 		
	 		//jQuery("ul.subNav li.leftNavSub").addClass('with-js');
	 		//jQuery(".mastHeadnavDropHover").addClass('with-js');		 		
	 	}	 	
 	});	