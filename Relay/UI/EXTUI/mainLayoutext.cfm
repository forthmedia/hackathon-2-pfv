<!--- �Relayware. All Rights Reserved 2014 --->
<!---

2008-06-16 WAB moved all the menu files into the UI directory
2008/10/28	WAB/NH   add code to open specific tabs on opening.  Based on parameter url.openTab (a list of tabs) .  For backwards compatability can use url.st  (starting templates), but this is dealt with in index.cfm
2010/06    WAB Put CFOUTPUT round whole code so will run with enablecfoutputonly
2010/10/12 	WAB title now comes from currentsite.title and has dev/test added automatically
2011/09/12   WAB mods to jasperserver tickle
2012/01/18 - RMB - P-LEN24 - CR066 Added to script to set jasperServerLoggedIn all call onload to "tickleJasperServerLogin".
WAB 2012-05-14 CASE 434001  Reconfigured the code for detecting the jasperServerLogin.  Now uses an onload listener on the iframe (using javascript\jasperserverloaddetect.js)
--->
<!--- 2013-09-10	IH	Case 436556 set showdebugoutput to false to fix weird disappearing tabs issue on Chrome --->
<cfsetting showdebugoutput="false">
<cfoutput>
	<cf_head>
		<!---SB 22/10/15 ADDED GOOGLE FONTS --->
		<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
		<!---<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300italic,300,400italic,600,700,600italic,700italic,800,800italic' rel='stylesheet' type='text/css'>--->

		<!---STANDARD TEMPLATES CSS --->
		<cf_includeCssOnce template="/styles/relayui.css">
		<cf_includeCssOnce template="/javascript/ext/resources/css/ext-all.css">
		<!--- <link rel="stylesheet" type="text/css" media="screen" href="/styles/relayui.css" />
		<link rel="stylesheet" type="text/css" href="/javascript/ext/resources/css/ext-all.css" /> --->

		<!---SB 07/11/14 STYLESHEET SWITCHER--->
		<cf_includeCssOnce template="/styles/themeBlue.css" title="styles1">
		<cf_includeCssOnce template="/styles/themeDark.css" rel="alternate stylesheet" title="styles2">
		<cf_includeCssOnce template="/styles/themeLight.css" rel="alternate stylesheet" title="styles3">
		<cf_includeCssOnce template="/styles/themePastel.css" rel="alternate stylesheet" title="styles4">
		<!--- <link rel="stylesheet" type="text/css" href="styles/themeBlue.css" title="styles1" media="screen" />
		<link rel="alternate stylesheet" type="text/css" href="styles/themeDark.css" title="styles2" media="screen" />
		<link rel="alternate stylesheet" type="text/css" href="styles/themeLight.css" title="styles3" media="screen" />
		<link rel="alternate stylesheet" type="text/css" href="styles/themePastel.css" title="styles4" media="screen" /> --->

		<!---SB 15/10/15 SB BOOTSTRAP CSS--->
		<cf_includeCssOnce template="/javascript/bootstrap/css/bootstrap.min.css">
		<cf_includeCssOnce template="/styles/relayware-mobile.css">
		<cf_includeCssOnce template="/styles/relayware-tablet.css">
		<cf_includeCssOnce template="/styles/relayware-desktop-medium.css">
		<cf_includeCssOnce template="/styles/relayware-desktop-large.css">

		<!--- <link rel="stylesheet" type="text/css" href="/javascript/bootstrap/css/bootstrap.min.css" />
		<link rel="stylesheet" type="text/css" href="/styles/relayware-mobile.css" />
		<link rel="stylesheet" type="text/css" href="/styles/relayware-tablet.css" />
		<link rel="stylesheet" type="text/css" href="/styles/relayware-desktop-medium.css" />
		<link rel="stylesheet" type="text/css" href="/styles/relayware-desktop-large.css" /> --->

		<!---2014-11-11 SB ADD IE8 SPECIFIC CSS FILES--->
		<!--[if lt IE 9]>
			<link href='styles/ie8-and-down.css' rel='stylesheet' type='text/css' />
		<![endif]-->

		<!---2014-11-07 SB STYLESHEET SWITCHER JAVASCRIPT--->
		<script type="text/javascript">
			jQuery(document).ready(function() {
				jQuery('.styleswitch').click(function()
				{
					switchStylestyle(this.getAttribute("rel"));
					return false;
				});
				var c = readCookie('style');
				if (c) switchStylestyle(c);

				//SB - 26/11/15 Detect device orientation - 08/02/16 SB We need to add this back but it was breaking the IE9 menu hover
				/*detectOrientation();

				function detectOrientation() {

					// Find matches
					var mql = window.matchMedia("(orientation: portrait)");

					// If there are matches, we're in portrait
					if(mql.matches) {
						alert("This website is best viewed in landscape mode. Please rotate your device to get a better user experience.");
					} else {
						// Landscape orientation
					}
				}*/
				//SB - 26/11/15 END

			});

			<!---
				NJH 2016/01/27 - removed this block of JS as it's causing a number of errors and it's not doing what it thinks it's doing. So, this needs to be tidied up at some point
			//SB - 15/10/15 Added this iFrameResize for Bootstrap using JS.
			var buffer = 20; //scroll bar buffer
			var iframe = document.getElementById('homeIframe');
			var iframe = document.getElementsByClassName('lowerPage');

			function pageY(elem) {
			    return elem.offsetParent ? (elem.offsetTop + pageY(elem.offsetParent)) : elem.offsetTop;
			}

			function resizeIframe() {
			    var height = document.documentElement.clientHeight;
			    height -= pageY(document.getElementById('homeIframe'))+ buffer ;
			    height -= pageY(document.getElementsByClassName('lowerPage'))+ buffer ;
			    height = (height < 0) ? 0 : height;
			    document.getElementById('homeIframe').style.height = height + 'px';
			    document.getElementsByClassName('lowerPage').style.height = height + 'px';
			}

			// .onload doesn't work with IE8 and older.
			if (iframe.attachEvent) {
			    iframe.attachEvent("onload", resizeIframe);
			} else {
			    iframe.onload=resizeIframe;
			}

			window.onresize = resizeIframe;
			//SB - 15/10/15 END
			 --->

			function switchStylestyle(styleName)
			{
				jQuery('link[rel*=style][title]').each(function(i)
				{
					this.disabled = true;
					if (this.getAttribute('title') == styleName) this.disabled = false;
				});
				createCookie('style', styleName, 365);
			}

			// cookie functions
			function createCookie(name,value,days)
			{
				if (days)
				{
					var date = new Date();
					date.setTime(date.getTime()+(days*24*60*60*1000));
					var expires = "; expires="+date.toGMTString();
				}
				else var expires = "";
				document.cookie = name+"="+value+expires+"; path=/";
			}
			function readCookie(name)
			{
				var nameEQ = name + "=";
				var ca = document.cookie.split(';');
				for(var i=0;i < ca.length;i++)
				{
					var c = ca[i];
					while (c.charAt(0)==' ') c = c.substring(1,c.length);
					if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
				}
				return null;
			}
			function eraseCookie(name)
			{
				createCookie(name,"",-1);
			}
			// end cookie functions
		</script>
		<!---end 2014-11-07 SB STYLESHEET SWITCHER--->
	</cf_head>

		<cf_includeJavascriptOnce template = "/javascript/ext/adapter/ext/ext-base.js">
		<cf_includeJavascriptOnce template = "/javascript/ext/ext-all.js">
		<cf_includeJavascriptOnce template = "/javascript/ext/docs/resources/TabCloseMenu.js">
		<cf_includeJavascriptOnce template = "/javascript/lib/jquery/jquery.columnlist.js">
		<cf_includeJavascriptOnce template = "/javascript/lib/jquery/jquery.hoverIntent.js">
		<cf_includeJavascriptOnce template = "/javascript/lib/fancybox/jquery.fancybox.pack.js">
		<cf_includeCSSOnce template = "/javascript/lib/fancybox/jquery.fancybox.css">
		<!---<cf_includeJavascriptOnce template = "/javascript/lib/jquery/jquery.mobile.js">--->
		<!---2015-10-15 SB BOOTSTRAP JS--->
		<cf_includeJavascriptOnce template = "/javascript/bootstrap/js/bootstrap.js">
		<!--- WAB AND SB 2014-09-15 attempt to implement single scrollbar --->
		<!--- <cf_includeJavascriptOnce template = "/javascript/iframeResizer/iframeResizer.js"> --->

		<cf_includeJavascriptOnce template = "/UI/EXTUI/applayout.js">
		<div class="row">
			<div id="north" class="col-xs-12">
				<!---<button type="button" data-target=".navbar-collapse" data-toggle="mobile-wrapper" class="navbar-toggle">
		            <span class="sr-only">Toggle navigation</span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
			    </button>--->
				<cfinclude template="menuExt.cfm">
			</div>
		</div>
		<div id="west" class="notActive">
			<div id="westHeaderToggle">
				<a href="##" class="toggleMenuLink fa fa-angle-double-right" id="toggleOpenMenu"></a>
				<a href="##" class="toggleMenuLink fa fa-angle-double-left" id="toggleCloseMenu"></a>
			</div>
			<cfinclude template="ajaxLmenuXMLextDtree.cfm">
		</div>
		<div id="tabArea"></div>

		<!--- NJH 2011/03/01 P-FNL075 - tickle the JasperServer session along
		WAB 2011/09/12 added a _cf_nodebug to the jasperserverLogin call - if the page has debug on it requests an image from cfide, which fires the missing template handler, which tickles the session and the user never gets logged out!
						also changed to an asynchronous process
		 --->
		<cfif application.com.relayMenu.isModuleActive("ReportManager")>

			<cfset jasperServerFunctions =  createobject("component","relay.com.jasperServer")>

			<cf_includeJavascriptOnce template="/javascript/jasperserverLoadDetect.js">

			<iframe src="" width=0 height=0 id="jsIframe" style="display:none"></iframe>
			<script>

				jasperserverLoadDetect ('jsIframe')

				function tickleJasperServerLogin() {
					var userLoggedIn = false;
					var page = '/webservices/user.cfc?wsdl&method=isRelayUserLoggedIn'
					var params = 'noTickle=true&_cf_nodebug=true&returnFormat=json';
					page = page + '&' + params  // WAB 2011/10/19 added params to URL because noTickle is only tested for in URL scope

					// changed to post to ensure proper refresh
					var myAjax = new Ajax.Request(
						page,
						{
							method: 'post',
							debug : false,
							onComplete: function  (requestObject,JSON) {
								userLoggedIn = requestObject.responseText.evalJSON();
								if (userLoggedIn == true) {
									$('jsIframe').src = '/jasperServer/jasperServerLogin.cfm?noTickle=true&_cf_nodebug=true';
									runTickleJasperServerLogin();
								} else {
									$('jsIframe').src = '#jasperServerFunctions.getJasperServerPath()#/exituser.html';
									jasperServerLoggedIn = false;
								}
							}
					});

				}

				function runTickleJasperServerLogin() {
					var timeoutMins = 10;
					var milliseconds = (timeoutMins*60)*1000;

					setTimeout(tickleJasperServerLogin,milliseconds);
				}

				tickleJasperServerLogin()
			</script>


		</cfif>

		<!---
			2008/10/28 Part of T-10 Bug Fix Issue 1
			pickup url.openTab to open particular tabs on start up
			could be extended to get list of tabs from some sort of preferences
			note that the openStartingTabs function is checked for and run by applayout.js after ext has initialised
		 --->
		<cfif structKeyExists(URL,"openTab") and URL.openTab neq "">
			<script>
				<cfoutput>
					function openStartingTabs () {
						<cfloop index = "thisTab" list = "#url.openTab#">
							openTabByName('#jsStringFormat(thisTab)#','#jsStringFormat(request.query_string)#');
						</cfloop>
					}
				</cfoutput>
			</script>
		</cfif>
</cfoutput>