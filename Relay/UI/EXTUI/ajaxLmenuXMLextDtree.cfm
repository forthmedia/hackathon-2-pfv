<!--- �Relayware. All Rights Reserved 2014 --->

<!--- Anyone with any sort of permissions can see this frame --->
<!--- amendment History

Version	 Date		By 		Description

1.0		1998-01-13		LW 		first version
1.1		1999-10-13	SWJ 	converted to work with elements
1.2		2001-10-13	SWJ		added recent searches and search field
1.3		2001-10-20	SWJ 	added tabs concept with menuTabs.cfm
1.4		2001-11-02	CPS		Amend call to getElementsBranch to include session variable FNLElementStatus
1.5		2002-01-05	SWJ		Ammended code to work with XML file
1.6		2002-01-31	SWJ		Ammended code to suppress some records
1.7		2002-02-02	SWJ		Rewrote to work with new XML file structure and submenu
		2004-12-13	WAB		Altered the way security is handled in the xml file
		2006-02-19	SWJ		Added additional methods for searching from the left nav
		2006-02-25	NM		Added new stylesheet approach
		2007-10-09	NJH		Added client specific search criteria as part of CR-TNDNAB502
		2008/02/20	WAB		changed the recentSearches to be Ajax and altered some JS
		2008/03/03	SWJ		Added the link to Analytics
		2008/03/03	SWJ		Added the link to Selections
		2008/06/04	NYF		Amendments as part of T10 release/restructure
		2008/07/15	WAB		Mods to search javascript
		2008-07-25	NYF		Bug 803: replaced setting validMenuItemIDs value with a call to [new]
							function getValidMenuItemIDs
		2008/10/01	WAB		Correct search javascript to do drilldown on 3pane
		2008/10/28	WAB/NH  Added opening tab by name (function searches through dtree structure and then opens the appropriate tab)
		2008/11/07  NJH		Bug Fix All Sites Issue 1314 - fixed problem with opening/closing recent searches div. Am only now calling reloadRecentSearches when the div is visible.
		2008/12/03 	WAB		Bug 1440 Various problem with searches and accented characters and & and +
							Basic problem was encoding, but knock on effects with 3 pane and drilldown
							Eventually solved by putting into a js object and then making a query string appropriate for the 3pane or the drilldown

		2008/12/03 	WAB		Extended the openTabByName function to allow items which are not on the menus, currently just a hard coded structure but could be extended
							Used to change the AdvancedSearch Link
		2009/01/27	SWJ		Made phoneListTask control the phone List meneu element in myTools
		2009/05/06	GCC		LID:2185 Added backslashes to extension.xml check to ensure it picks up menus whether absolutePath does or does not have a trailing slash
		2010/12/??	PPB		added myMDF
		2011/03/24	PPB		set myMDF to FundTask level2
		2011-06-26	NYB		P-REL106
		2012-01		IH		Sprint Menu Changes - switch to jquery re-orderable menu
		2012-02-03	WAB		Sprint Menu Changes.  Rationalise menu XML code.  Access menus through getMenuXMLDoc().  Client extensions dealt with automatically.
		2015/02/02	NJH		Added fullID to the childID so that we have distinct IDs for child menu items.
		2016-06-27	WAB		PROD2016-1334 callRelayRecordManager security.  Add support for an encrypt attribute on the menu XML
--->
<cf_includeJavascriptOnce template="/javascript/login.js" translate=true>

<cf_head>
	<cfoutput><script>var userName = "#jsStringFormat(request.relaycurrentuser.person.username)#"</script></cfoutput>
</cf_head>

<CFPARAM NAME="URL.menu" DEFAULT="AppsMenu">
<CFSET MenuChoice = URLDecode(URL.menu)>

<!--- <cfset menuXML = application.com.relayMenu.getMenuXMLDoc()>

<cfset selectedElements = XmlSearch(menuXML, "//MenuItem")> --->
<cfset selectedElements = application.com.xmlFunctions.xmlSearchWithLock(xmlDoc=application.com.relayMenu.getMenuXMLDoc(),xPathString="//MenuItem",lockName="applicationMenu")>


<cfset validMenuItemIDs = application.com.relayMenu.getValidMenuItemIDs()>
<cfset leftNavOrder = application.com.settings.getsetting("ui.menuorder")>

<!---
<cfif structKeyExists(URL,"resetNavigation")>
	<cfset application.com.settings.InsertUpdateDeleteSetting(variablename="ui.menuorder",variablevalue="",personid="#request.relaycurrentuser.personid#")>
<cfelse>
	<cfset leftNavOrder = application.com.settings.getsetting("ui.menuorder")>
</cfif>
--->

<cfset passedSecurity = false>
<cfset stNavOrder = {}>
<cfset thisOrderID = 0>
<cfset maxOrderID = 0>
<cfset parentOrderID = 0>

<cfif listLen(leftNavOrder)>
	<cfset orderValue = 1>
	<cfloop list="#leftNavOrder#" index="i">
		<cfset stNavOrder[i] = orderValue++>
	</cfloop>
	<cfset stTempNav = {}>
	<cfloop array="#selectedElements#" index="i">
		<cfif application.com.login.checkSecurityList(i.XmlAttributes.SECURITY)>
			<cfif structKeyExists(stNavOrder,i.xmlAttributes.fullID)>
				<cfset thisOrderID = stNavOrder[i.xmlAttributes.fullID]>
				<cfset basePos = thisOrderID>
			<cfelse>
				<cfparam name="basePos" default="0">
				<cfset thisOrderID = (thisOrderID + (basePos++/1000))>
			</cfif>
			<cfif thisOrderID gt maxOrderID>
				<cfset maxOrderID = thisOrderID>
			</cfif>
			<cfif i.XmlAttributes.GENERATION is 1>
				<cfset parentOrderID = thisOrderID>
			<cfelseif thisOrderID lte parentOrderID>
				<cfset thisOrderID = (parentOrderID + (basePos++/1000))>
			<!--- 2014/10/29	YMA	CORE-904 When the nav item next above a generation 1 item is moved it moves the generation 1 item too.  This fixes it. --->
			<cfelseif thisOrderID gt parentOrderID>
				<cfset thisOrderID = (parentOrderID + (basePos++/1000))>
			</cfif>
			<cfset stTempNav[i.xmlAttributes.fullID] = {}>
			<cfset stTempNav[i.xmlAttributes.fullID].e = i>
			<cfset stTempNav[i.xmlAttributes.fullID].orderBy = thisOrderID>
		</cfif>
	</cfloop>

	<cfset aTempNavOrder = structSort(stTempNav,"numeric","asc","orderBy")>
	<cfset aNavOrdered = []>
	<cfloop array="#aTempNavOrder#" index="i">
		<cfset arrayAppend(aNavOrdered,stTempNav[i].e)>
	</cfloop>
	<cfset passedSecurity = true>
<cfelse>
	<cfset aNavOrdered = selectedElements>
</cfif>

<cfoutput>
	<cfset i = 1>
	<cfset itemClass="">
	<ul id="leftNav">

		<cfset subNavOpen = false>
		<cfloop array="#aNavOrdered#" index="xmlNode">

			<cfif passedSecurity>
				<cfset hasCorrectSecurity = true>
			<cfelse>
				<cfset hasCorrectSecurity = application.com.login.checkSecurityList(xmlNode.XmlAttributes.SECURITY)>
			</cfif>
			<cfif hasCorrectSecurity>
				<cfif xmlNode.XmlAttributes.GENERATION EQ 1>
					<cfif subNavOpen></ul></li></cfif>

					<cfset subNavOpen = true>
					<li id="#xmlNode.XmlAttributes.fullID#">
						<div class="navHeader"><span class="closed">phr_sys_nav_#replace(LTrim(xmlNode.XmlAttributes.ItemName)," ","_","ALL")#</span></div>
						<ul class="subNav">

				<cfelseif xmlNode.XmlAttributes.GENERATION eq 2 and listfindNoCase(validMenuItemIDs,xmlNode.XmlAttributes.Module) and (not structKeyExists(xmlNode.XmlAttributes,"condition") or evaluate(xmlNode.XmlAttributes.condition))>	<!--- NJH 2014/05/07 - check condition on top level --->
					<cfset parentExtData = "">
					<cfif structKeyExists(xmlNode.XmlAttributes,"EXTDATA")>
						<cfset parentExtData = "," & xmlNode.XmlAttributes.extData>
					</cfif>

					<cfif structKeyExists(xmlNode,"xmlChildren") and arrayLen(xmlNode.xmlChildren)>
						<cfset viewText = 'phr_sys_nav_#replace(LTrim(xmlNode.XmlAttributes.ItemName)," ","_","ALL")#'>
						<li id="#xmlNode.XmlAttributes.fullID#" class="leftNavSub">

							<cfset iconClass = lcase(xmlNode.XmlAttributes.module)>
							<cfif structKeyExists(xmlNode.XmlAttributes,"url")>
								<a href="javascript:manageClick('#replace(xmlNode.XmlAttributes.ItemName," ","","All")#','#viewText#','#xmlNode.XmlAttributes.URL#?module=#xmlNode.XmlAttributes.module#',true,{reuseTab:true,reuseTabJSFunction:function(iframeObj){return true},iconClass:'#iconClass#'#parentExtData#})"><div class="navSubHeader navIcon #iconClass#">#viewText#</div></a>
							<cfelse>
								<div class="navSubHeader navIcon #iconClass#">#viewText#</div>
							</cfif>

							<cfif not structKeyExists(xmlNode.XmlAttributes,"url")>
								<ul class="drop">
									<h3 class="subMenuTitle">#viewText#</h3>

								<cfset thisArray = xmlnode.xmlChildren>
								<cfset stXmlChildren = {}>
								<cfset basePos = 2000>

								<cfloop from="1" to="#arrayLen(thisArray)#" index="i">
									<cfif structKeyExists(thisArray[i].xmlAttributes,"fullID")>
										<cfset thisPos = 0>
										<cfif structKeyExists(stNavOrder,thisArray[i].XmlAttributes.fullID)>
											<cfset thisPos = stNavOrder[thisArray[i].XmlAttributes.fullID]>
										</cfif>
										<cfif !thisPos>
											<cfset thisPos = basePos++>
										</cfif>
										<cfset thisArray[i].XmlAttributes.sortOrder = thisPos>
										<cfset stXmlChildren[thisPos] = thisArray[i].XmlAttributes>
									</cfif>
								</cfloop>

								<cfset aSorted = structSort(stXmlChildren,"numeric","asc","sortOrder")>

								<cfloop array="#aSorted#" index="i">
									<cfset xmlAttributes = stXmlChildren[i]>
									<cfif application.com.login.checkSecurityList(xmlAttributes.SECURITY) and (not structKeyExists(XmlAttributes,"condition") or evaluate(XmlAttributes.condition))>
										<cfif structKeyExists(xmlAttributes,"fullID")>
											<cfset viewText = 'phr_sys_nav_#replace(LTrim(xmlAttributes.tabText)," ","_","ALL")#'>
											<cfset menuUrl = application.com.relaytranslations.checkForAndEvaluateCfVariables(phraseText=xmlAttributes.URL)>

											<cfset childExtData = "">
											<cfif structKeyExists(XmlAttributes,"EXTDATA")>
												<cfset childExtData = childExtData & "," & XmlAttributes.extData>
											</cfif>

											<cfif structKeyExists(XmlAttributes,"encrypt") and XmlAttributes.encrypt>
												<cfset menuUrl = application.com.security.encryptURL (url = menuUrl, singleSession = true)>
											</cfif>

											<cfset urlToCall = "manageClick('tabItem_#i#_#xmlAttributes.fullID#','#viewText#','#menuUrl#',null,{iconClass:'#iconClass#'#childExtData#})">

											<li id="#xmlAttributes.fullID#"><a href="javaScript:#urlToCall#">#viewText#</a></li>
										</cfif>
									</cfif>
								</cfloop>

								</ul>
							</cfif>
						</li>
					</cfif>
				</cfif>
			</cfif>
		</cfloop>
		<cfif subNavOpen>
			</ul></li>
		</cfif>

	</ul>
</cfoutput>


<cf_IncludeJavascriptOnce template = "/javascript/navandsitesearch.js" translate="true">

<!--- <cfoutput><div style="float:right;"><a href="?resetNavigation=1">Reset Menu</a></div>&nbsp;</cfoutput> --->
<cfoutput><div id="navUpdated"></div></cfoutput>