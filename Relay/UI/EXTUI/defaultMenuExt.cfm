<!--- �Relayware. All Rights Reserved 2014 --->
<!---
	WAB 2005-05-23   removed mention of session.securityStructure and removed call to qryGetUserName (all required data in request.relaycurrentuser and the session.security)
	SWJ 2007-03-31 added request.RelayWareServerID so that we can tell which server we are looking at when there is a server farm involved
	WAB 2010/06 Added instance name to the title - Visible only on Dev Site
	WAB 2011/06/01 LID 6769  Security Scan - encrypt link to persondetails.cfm
	RPW 2014-08-22	My Details screen should open Dashboard
	SB 	2014-10-24	Removed Colon on headHeading
	NJH	2015/03/09	Jira Fifteen-21 - SSO to Launch Pad for help. Opens new window
    DAN 2016-06-01  PROD2016-1191 - display hosting group name if specified
	--->
<!--- styles to manage the look of the login page --->
<cf_include template="/code/styles/topMenuStyles.cfm" checkIfExists="true">

<cfset request.document.showFooter = false>
<cf_IncludeJavascriptOnce template = "/javascript/navandsitesearch.js" translate="true">
<cf_IncludeCssOnce template = "/styles/masthead.css">
<!--- <script type="text/javascript">
(function (jQuery) {

  switch_style = {

    onReady: function () {
      this.switch_style_click();
    },

    switch_style_click: function(){
    	jQuery(".box").click(function(){
    		var id = jQuery(this).attr("id");

    		jQuery("#switch_style").attr("href", "/styles/" + id + ".css");
    	});
    },
  };

  jQuery().ready(function () {
	  switch_style.onReady();
  });

})(jQuery);

</script> --->
<cfoutput>
	<header id="masthead">
		<div id="row">
			<!-- Logo -->
			<div id="rwLogo" class="col-xs-1 col-sm-2 col-md-2 col-lg-1">
			</div>
			<!-- Title -->
			<div id="headHeading" class="col-xs-3 col-sm-5 col-md-5 col-lg-5">
				<h1 id="site" title="#application.com.relayCurrentSite.getEnvironmentName()# (#request.currentsite.instance.name#)">
					#request.currentSite.Title#
					<cfif application.com.relayCurrentSite.isEnvironment('dev,test')>
						(#request.currentsite.instance.name#)
					</cfif>
				</h1>
			</div>
			<div id="profile-and-search" class="col-xs-8 col-sm-5 col-md-5 col-lg-6 padding-offset">
				<!-- Profile and help -->
				<div id="headUsername" class="floatRight">
					<ul id="mastHeadnav">
						<li class="mastHeadnavDropHover">
						  <span><!--phr_you_profile--></span>
							<ul>
								<li id="username">
								<!--- RPW 2014-08-22	My Details screen should open Dashboard --->
									<a href="javascript:manageClick('myDetails','Phr_Sys_MyDetails','#application.com.security.encryptURL("/data/dataFrame.cfm?frmsrchPersonID=#request.relayCurrentUser.personid#")#','',{iconClass:'accounts'});" title="phr_sys_editYourDetails">#htmleditformat(request.relayCurrentUser.fullname)#</a>
								</li>
								<li id="logout">
									<a href="/endInternalSession.cfm" title="Phr_Sys_Logout">
										Phr_Sys_Logout
									</a>
								</li>
								<li class="styleSheetPick"><h3>Theme</h3></li>
								<li id="styleSheetColourBox">
									<ul>
										<li class="styleSheetPick styleSheetPickSub"><a href="serversideSwitch.html?style=style1" rel="styles1" class="styleswitch blue"></a></li>
										<li class="styleSheetPick styleSheetPickSub"><a href="serversideSwitch.html?style=style3" rel="styles3" class="styleswitch lightGrey"></a></li>
										<li class="styleSheetPick styleSheetPickSub"><a href="serversideSwitch.html?style=style2" rel="styles2" class="styleswitch grey"></a></li>
										<li class="styleSheetPick styleSheetPickSub"><a href="serversideSwitch.html?style=style4" rel="styles4" class="styleswitch pastel"></a></li>
									</ul>
								</li>
								<li id="versionNumber" class="versionNumber">
									<!--- WAB 2016-04-07 BF-564, get result from a function - will now include the maintenance version number --->
									<cfset version = application.com.relaycurrentSite.getSiteFriendlyVersionString()>
										phr_versionNumber #version#
								</li>
                                <cfset hostingGroup = application.com.settings.getSetting(variablename="system.hostingGroup")>
                                <cfif hostingGroup NEQ "">
                                    <li id="hostingGroup" class="versionNumber">
                                        phr_hostingGroup #hostingGroup#
                                    </li>
                                </cfif>
							</ul>
						</li>
						<li id="about">
							<!--- javascript:manageClick('AboutRelayware','phr_sys_nav_about_relayware','/fileManagement/fileGroupFileListing.cfm?FileTypeHeading=User Guides&showAdvancedSearch=false&fileEdit=false',true,{reuseTab:true,reuseTabJSFunction:function(iframeObj){return true},iconClass:'about'}); --->

							<!--- Jira Fifteen-21 - SSO to Launch Pad for help --->
							<cf_includeJavascriptOnce template="/javascript/openWin.js">

							<a href="" onclick="javascript:openWin('/templates/help.cfm','Relayware Help');return false;" title="phr_sys_about_Relayware">
								<!--phr_sys_nav_about_relayware-->
							</a>
						</li>
					</ul>
				</div>
				<!-- END Profile and help -->
				<!-- Search box -->
				<div id="searchContainer" class="floatRight">
					<div id="headForm" class="floatRight">
						<form onsubmit = "return siteSearch();" id="siteSearchForm">
							<input type="text" onblur="this.value=(this.value=='') ? 'Search' : this.value;" onfocus="this.value=(this.value=='Search') ? '' : this.value;" placeholder="Search..." name="searchTextString" class="searchBoxAlt form-control" maxlength="50">
							<span id="searchButton">
								<input type="image" src="/images/searchHeaderIcon.png" alt="Search" />
							</span>
						</form>
					</div>
					<div id="recentSearchesDiv" class="floatRight">
						<a href="" onclick="return reloadRecentSearches()">Recent</a> | <a href="" onclick="javascript:manageClick('advancedSearch','Advanced Search','/data/dataSearch.cfm',null,{iconClass:'search'}); jQuery('##siteSearchResults').hide(); return false;">Advanced</a>
					</div>
				</div>
				<!-- END Search box -->
			</div>
		</div>
		<div id="siteSearchResults">
			<div id="siteSearchResultsClose">
				<a href="" onclick="jQuery('##siteSearchResults').hide();return false;">
				</a>
			</div>
			<div id="siteSearchResultsInner">
			</div>
		</div>
	</header>
</cfoutput>
