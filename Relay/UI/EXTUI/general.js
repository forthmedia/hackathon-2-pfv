/* �Relayware. All Rights Reserved 2014 */
/*
 * Ext JS Library 2.0 Beta 1
 * Copyright(c) 2006-2007, Ext JS, LLC.
 * licensing@extjs.com
 * 
 * http://extjs.com/license
 */

Ext.BLANK_IMAGE_URL = 'ext/resources/images/default/s.gif';

Ext.example = function(){
    var msgCt;

    
    return {
        msg : function(title, format){
            if(!msgCt){
                msgCt = Ext.DomHelper.insertFirst(document.body, {id:'msg-div'}, true);
            }
            msgCt.alignTo(document, 't-t');
            var s = String.format.apply(String, Array.prototype.slice.call(arguments, 1));
            var m = Ext.DomHelper.append(msgCt, {html:createBox(title, s)}, true);
            m.slideIn('t').pause(1).ghost("t", {remove:true});
        },

        init : function(){
            var t = Ext.get('exttheme');
            if(!t){ // run locally?
                return;
            }
            var theme = Cookies.get('exttheme') || 'aero';
            if(theme){
                t.dom.value = theme;
                Ext.getBody().addClass('x-'+theme);
            }
            t.on('change', function(){
                Cookies.set('exttheme', t.getValue());
                setTimeout(function(){
                    window.location.reload();
                }, 250);
            });

            var lb = Ext.get('lib-bar');
            if(lb){
                lb.show();
            }
        }
    };
}();

Ext.example.shortBogusMarkup = '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Sed metus nibh, sodales a, porta at, vulputate eget, dui. Pellentesque ut nisl. Maecenas tortor turpis, interdum non, sodales non, iaculis ac, lacus. Vestibulum auctor, tortor quis iaculis malesuada, libero lectus bibendum purus, sit amet tincidunt quam turpis vel lacus. In pellentesque nisl non sem. Suspendisse nunc sem, pretium eget, cursus a, fringilla vel, urna.';
Ext.example.bogusMarkup = '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Sed metus nibh, sodales a, porta at, vulputate eget, dui. Pellentesque ut nisl. Maecenas tortor turpis, interdum non, sodales non, iaculis ac, lacus. Vestibulum auctor, tortor quis iaculis malesuada, libero lectus bibendum purus, sit amet tincidunt quam turpis vel lacus. In pellentesque nisl non sem. Suspendisse nunc sem, pretium eget, cursus a, fringilla vel, urna.<br/><br/>Aliquam commodo ullamcorper erat. Nullam vel justo in neque porttitor laoreet. Aenean lacus dui, consequat eu, adipiscing eget, nonummy non, nisi. Morbi nunc est, dignissim non, ornare sed, luctus eu, massa. Vivamus eget quam. Vivamus tincidunt diam nec urna. Curabitur velit.</p>';

Ext.example.latestCampaign = '<p style="font:normal 11px arial, tahoma, helvetica, sans-serif;"><a target="_blank" href="http://www.sony1.net/AITTurbo"><img width="100%" alt="" src="AIT_530_104_UK.gif" border="0"/></a><br/><br/><b/><span style="color: #e4791b; FONT-SIZE: 1.1em">It is time your customers made the move to AIT</span></b><br/><br/><span>For a limited time, when your customers trade-in their DAT drive and replace with an AIT Turbo drive, they can claim 5 media, plus NovaStor TapeCopy software*.</span><br/><br/><span><b>Please login to download the range of tools to help you communicate the advantages of AIT and promote this special offer.</b></span><br/><br/><span><a style="color: #EA7725;" target="_blank" href="https://www.sonyaitpromotion.net/Terms.aspx">*Terms and conditions apply.</a></span></p>';

Ext.example.graphIncentive = '<iframe frameborder="0" style="padding-left: 5px; padding-top: 0px" src="http://fnl-europe/cfdev/dashboard/dashboardIncluder.cfm?templateURL=displayChartSet.cfm&filter=Incentive" scrolling="no" height="325" width="100%"/>';

Ext.example.graphSalesOutBar = '<iframe frameborder="0" style="padding-left: 5px; padding-top: 0px" src="http://fnl-europe/cfdev/dashboard/dashboardIncluder.cfm?templateURL=displayChartSet.cfm&filter=Sales Out" scrolling="no" height="325" width="100%"/>';

Ext.example.graphLeadPipelinePie = '<iframe frameborder="0" style="padding-left: 5px; padding-top: 0px" src="http://fnl-europe/cfdev/dashboard/dashboardIncluder.cfm?templateURL=displayChartSet.cfm&filter=Lead Metrics" scrolling="no" height="325" width="100%"/>';



Ext.onReady(Ext.example.init, Ext.example);


// old school cookie functions grabbed off the web
var Cookies = {};
Cookies.set = function(name, value){
     var argv = arguments;
     var argc = arguments.length;
     var expires = (argc > 2) ? argv[2] : null;
     var path = (argc > 3) ? argv[3] : '/';
     var domain = (argc > 4) ? argv[4] : null;
     var secure = (argc > 5) ? argv[5] : false;
     document.cookie = name + "=" + escape (value) +
       ((expires == null) ? "" : ("; expires=" + expires.toGMTString())) +
       ((path == null) ? "" : ("; path=" + path)) +
       ((domain == null) ? "" : ("; domain=" + domain)) +
       ((secure == true) ? "; secure" : "");
};

Cookies.get = function(name){
	var arg = name + "=";
	var alen = arg.length;
	var clen = document.cookie.length;
	var i = 0;
	var j = 0;
	while(i < clen){
		j = i + alen;
		if (document.cookie.substring(i, j) == arg)
			return Cookies.getCookieVal(j);
		i = document.cookie.indexOf(" ", i) + 1;
		if(i == 0)
			break;
	}
	return null;
};

Cookies.clear = function(name) {
  if(Cookies.get(name)){
    document.cookie = name + "=" +
    "; expires=Thu, 01-Jan-70 00:00:01 GMT";
  }
};

Cookies.getCookieVal = function(offset){
   var endstr = document.cookie.indexOf(";", offset);
   if(endstr == -1){
       endstr = document.cookie.length;
   }
   return unescape(document.cookie.substring(offset, endstr));
};