<!--- �Relayware. All Rights Reserved 2014 --->
<!---  Amendment History:
2011/05/25	NYB		REL106 various changes.  Moved -v3 functions to end.
2014-01-17  AXA		Case 438630 - passing params from relaytag
2015-02-11 	ACPK 	FIFTEEN-114 Fixed fancybox not closing when Close Form button is clicked
2015-02-12 	ACPK 	FIFTEEN-114 Removed superfluous Close Form button
2016-03-21	AHL Sugar 448593 This is a fancy box now
2016-09-21	RJT		PROD2016-1413 Made organisation name a required field as its essential later in the process
2016-09-21	RJT		PROD2016-2370 Limit phone number entry length to what can actually be put in DB
--->
<cf_displayBorder
	noborders=true
>

<!--- START 2014-01-17  AXA		Case 438630 - passing params from relaytag --->
<cfparam name="aboutPersonID" default="" />
<cfparam name="emailTextID" default="LocatorPartnerLead" />
<cfparam name="Enquiry" default="">
<!--- END 2014-01-17  AXA		Case 438630 - passing params from relaytag --->
<cfset qGetCountries=application.com.commonQueries.getCountries()>
<cfif structkeyexists(url,"UseProducts") and url.UseProducts eq 1 and structkeyexists(url,"countryid")>
	<cfset GetInterestProducts=application.com.relaylocator.GetProductsOfInterest(countryid=url.countryid)>
</cfif>

<cf_includeOnce template="/templates/relayFormJavaScripts.cfm">


<form action="/locatorPublic/locatorEnterDetails.cfm" method="post" name="EnterdetailsForm" >
<cf_relayFormDisplay class="">

<cfif isdefined("frmsendmessage")>
	<cfquery name="getcountry" datasource="#application.siteDataSource#">
		select LocalCountryDescription
		from country
		where countryID =  <cf_queryparam value="#locatorcountryID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</cfquery>

	<cfset merge = structnew()>
	<cfset merge.LocatorFirstName = LocatorFirstName>
	<cfset merge.LocatorLastName = LocatorLastName>
	<cfset merge.LocatorOrganisationName = LocatorOrganisationName>
	<cfset merge.LocatorContactEmail = LocatorContactEmail>
	<cfset merge.LocatorPhoneNumber = LocatorPhoneNumber>
	<cfset merge.locatoraddress1 = locatoraddress1>
	<cfset merge.locatoraddress2 = locatoraddress2>
	<cfset merge.locatorcity = locatoraddress4>
	<cfset merge.locatorpostalcode = locatorpostalcode>
	<cfif isdefined("locatorInterestproductName")>
		<cfset merge.locatorInterestproductName = locatorInterestproductName>
	</cfif>
	<cfset merge.Countryname = getcountry.LocalCountryDescription>
	<cfset merge.Comments = Comments>
	<!--- START 2014-01-17  AXA		Case 438630 - passing params from relaytag--->
	<cfif len(trim(aboutPersonID))>
		<cfif isDefined("locatorEnquiry")>
			<cfset merge.Enquiry = locatorEnquiry>
		</cfif>
		<cfset merge.CurrentSituation = merge.Comments>
		<cfset application.com.email.sendEmail(personID=aboutPersonID,emailTextID=emailTextID,mergeStruct=merge)>
	<cfelse>
		<cfloop index="locindex" list="#locationlist#" delimiters=",">

			<!--- lead type lead --->
			<cfquery name="insertlocatorLead" datasource="#application.siteDataSource#">
				declare @leadTypeId int, @progressID int;
				select @leadTypeID = lookupID from lookupList where fieldname='leadTypeID' and lookupTextID='Lead'
				select @progressID = lookupID from lookuplist where fieldname = 'leadProgressID' and lookuptextID = 'OpenNotContacted'

				insert into Lead(progressID, leadTypeID,leadTypeStatusID,FirstName,LastName,officePhone,Address1,Address2,Address4,PostalCode,CountryID<cfif isdefined("locatorInterestproductName")>,custom1</cfif>,partnerLocationId,company,email,lastupdatedbyperson,lastUpdatedBy)
			values(@progressID,@leadTypeID,0,<cf_queryparam value="#LocatorFirstName#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#LocatorLastName#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#LocatorPhoneNumber#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#locatoraddress1#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#locatoraddress2#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#locatoraddress4#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#locatorpostalcode#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#locatorcountryID#" CFSQLTYPE="CF_SQL_VARCHAR" ><cfif isdefined("locatorInterestproductName")>,<cf_queryparam value="#locatorInterestproductName#" CFSQLTYPE="CF_SQL_VARCHAR" ></cfif>,<cf_queryparam value="#locindex#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#LocatorOrganisationName#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#LocatorContactEmail#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#request.relaycurrentuser.personID#" CFSQLTYPE="CF_SQL_Integer">,<cf_queryparam value="#request.relaycurrentuser.usergroupID#" CFSQLTYPE="CF_SQL_Integer">)
			</cfquery>
			<cfset LocatorPartnerContact = application.com.flag.getFlagData(flagid="isLocatorContact",entityid=locindex)>

			<cfif LocatorPartnerContact.recordcount EQ 1>
				<cfset merge.PartnerName = LocatorPartnerContact.data_name>
				<cfset application.com.email.sendEmail(emailtextid = emailTextID,personid = variables.LocatorPartnerContact.data, mergeStruct = merge)>
			</cfif>
		</cfloop>
	</cfif>
	<!--- END 2014-01-17  AXA Case 438630 - wrapping old code in else and closing cfif--->
			<b>phr_ThankYou</b>

	<cf_relayFormElementDisplay relayFormElementType="HTML" currentValue="phr_locator_PartnerEmailSent" label="" spanCols="yes">

<cfelse>
	<cfif request.relaycurrentuser.isunknownuser is true>
		<cfset defaultCountryID=request.relaycurrentuser.possiblecountryidforunknownuser>
	<cfelse>
		<cfset defaultCountryID=request.relaycurrentuser.countryid>
	</cfif>

	<h2>phr_LocatorReferral_ContactFormInstruction</h2>		<!--- 2016-03-21 AHL/WAB adding styling --->

	<CF_relayFormElementDisplay relayFormElementType="text" currentValue="" id="" fieldName="LocatorFirstName" label="phr_Sys_FirstName" required="Yes">

	<CF_relayFormElementDisplay relayFormElementType="text" currentValue="" id="" fieldName="LocatorLastName" label="phr_Sys_LastName" required="Yes">

	<CF_relayFormElementDisplay relayFormElementType="text" currentValue="" id="OrganisationName" fieldName="LocatorOrganisationName" label="phr_Sys_Organisation" required="Yes">

	<CF_relayFormElementDisplay relayFormElementType="text" currentValue="" id="contactemail" fieldName="LocatorContactEmail" label="phr_Sys_Email" required="yes" validate="email">

	<CF_relayFormElementDisplay relayFormElementType="text" currentValue="" id="phonenumber" fieldName="LocatorPhoneNumber" label="phr_Sys_Telephone" required="yes" maxLength="20">

	<CF_relayFormElementDisplay relayFormElementType="text" currentValue="" id="invaddress1" fieldName="locatoraddress1" label="phr_Sys_Address" >

	<CF_relayFormElementDisplay relayFormElementType="text" currentValue="" id="locatoraddress2" fieldName="locatoraddress2" label="">

	<CF_relayFormElementDisplay relayFormElementType="text" currentValue="" id="locatoraddress4" fieldName="locatoraddress4" label="phr_sys_cityRegion" >

	<CF_relayFormElementDisplay relayFormElementType="text" currentValue="" id="locatorpostalcode" fieldName="locatorpostalcode" label="phr_Sys_PostalCode" required="yes">

	<CF_relayFormElementDisplay relayFormElementType="select" currentValue="#defaultCountryID#" fieldName="locatorcountryID" id="locatorcountryID" label="phr_Sys_Country" query="#qGetCountries#" display="country" value="countryid" required="yes">
	<!--- START 2014-01-17  AXA		Case 438630 - passing params from relaytag--->
	<cfif len(trim(Enquiry)) GT 0>
		<CF_relayFormElementDisplay relayFormElementType="select" id="locatorEnquiry" fieldName="locatorEnquiry" label="phr_orgProfile_Enquiry" CURRENTVALUE="" validvalues="#Enquiry#">
	</cfif>
	<!--- END 2014-01-17  AXA		Case 438630 - passing params from relaytag--->
	<CF_relayFormElementDisplay relayFormElementType="textArea" fieldName="comments" currentValue="" id="locatorcomments" label="phr_Sys_Comments" maxLength="2000" rows="5">

	<cfif structkeyexists(url,"UseProducts") and url.UseProducts eq 1>
		<CF_relayFormElementDisplay relayFormElementType="select" currentValue="" fieldName="locatorInterestproductName" id="InterestproductName" label="phr_sys_nav_Products" query="#GetInterestProducts#" display="InterestProductName" value="InterestProductName" multiple="yes" size=5 required="yes">
	</cfif>

	<cf_encryptHiddenFields>
		<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#locationlist#" fieldname="locationlist">
		<!--- START 2014-01-17  AXA		Case 438630 - passing params from relaytag--->
		<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#aboutPersonID#" fieldname="aboutPersonID">
		<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#emailTextID#" fieldname="emailTextID">
		<!--- END 2014-01-17  AXA		Case 438630 - passing params from relaytag--->
	</cf_encryptHiddenFields>

	<cf_relayFormElement relayFormElementType="submit" fieldname="frmsendmessage" label="" currentValue="phr_sys_Submit" valueAlign="left" spanCols="true">
	<cf_relayFormElement relayFormElementType="button" fieldname="frmsendmessage" label="" currentValue="phr_locator_cancel" onClick="parent.jQuery.fancybox.close();" valueAlign="left" spanCols="true">	<!--- 2016-03-21 AHL Sugar 448593 This is a fancy box now. I personally think that this button is redundant. --->

</cfif>
</cf_relayFormDisplay>
</form>