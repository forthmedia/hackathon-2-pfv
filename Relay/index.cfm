<!--- �Relayware. All Rights Reserved 2014 --->
<!---
WAB 2010/12/14  Implement a way of forcing a session to be on a particular instance
				Only works on CF Clustering
				Works by setting the jessionid cookie to the first 4 characters of the jsessionids being created by that particular instance
--->

<cfif isdefined("forceinstance") and forceinstance is not "">

	<cfquery name = "getInstances" datasource = "#application.sitedatasource#">
	select * from coldfusionInstance_Autopopulated
	where
		instancename =  <cf_queryparam value="#forceinstance#" CFSQLTYPE="CF_SQL_VARCHAR" >
	</cfquery>

	<cfif getInstances.jsessionid is not "" and getInstances.instanceName is not application.instance.name>
			<cfset application.com.globalFunctions.cfcookie(name="jsessionid", value = #left(getInstances.jsessionid,4)#)>
		<!--- Now redirect, having removed the parameter forceInstance
			Has to go to client to get cookie set
		--->
		<cfoutput>
		<script>
			location.href = '#request.currentsite.protocolAndDomain#?#application.com.regExp.removeItemFromNameValuePairString(inputString = request.query_string, itemToDelete = "forceInstance",delimiter="&")#'
		</script>
		</cfoutput>
		<CF_ABORT>
	</cfif>

</cfif>

<!---- if fromlogin exists, then do the login
		if not
			are cookies set?
				yes: do frameset
				no: do login.cfm
---->

<!--- 1999-05-21  WAB solve problems with blank user cookie after going into partner apps
	  2001-04-23	CPS	Mod to delete rights given from the partner facing site
	  2002-07-17	SWJ mod to store resetBlind.cfm in /relayware/root/ line 84
	  2004-05-10 AH & SWJ changed the logic of the initial external code block below
	  2005-01-15 SWJ added request.alternativePortalPath to allow us to define an alternative
	  			to ET.cfm as a starting point for a portal.  This should be defined in RelayINI.cfm
	2005-02-21		WAB removed line which removes person from usergroups (deletePartnerUserGroups())
	2005-09-06	WAB added a check to catch case when cookie.session is defined but cookie.user is not.  Not sure how this comes about but causes "user is not defined in cookie" errors later on

	2008-01		WAB added support for secure login
	2008-04-30	WAB dealt with popup login box on https server
	2008/07/14 		WAB 		removed references to relayCurrentVersion.relaycurrentUser Version
	2008-08-06 AJC P-SOP003 - Req705 - Internal NTLM Autologin
	2008/10/28	WAB/NH   Bug Fix T-10 Issue 1 - deal with startingtemplate (url.st) when using version 2 of the UI
2008-11-17	JvdW Added robots meta tag to prevent indexing by Google
--->

<!---If the User is external take him to the Partners Login area--->

<CFIF not request.relayCurrentUser.isInternal>  <!--- WAB 2006-01-25 Remove form.userType     not compareNoCase(UserType, "External") request.relayCurrentUser.isInternal--->
	<cfif isDefined("request.alternativePortalPath")>
		<!--- by setting request.alternativePortalPath in relayINI.cfm, you can call an
		alternative starting place for external site --->
		<CFINCLUDE template="#request.alternativePortalPath#">
	<cfelse>
		<!--- et.cfm draws the portal using display border --->
		<cfinclude template="/et.cfm">
	</cfif>
	<CF_ABORT>
</CFIF>

<cfhtmlhead text='<meta name="robots" content="noindex, nofollow">'>

<!--- everything below deals with loging into the internal application --->
<CFIF IsDefined("FORM.fromlogin")>
	<!--- this is coming from login so ignore cookie security --->

	<!--- login is not case sensitive --->
	<cfscript>
		//authenticate
		checkUsernameStruct = application.com.login.authenticateUserV2 (username=form.frmusername, password=form.frmpassword, isInternal = true);
		//checkUsername = application.com.login.authenticateInternalUser(form.frmusername, form.frmpassword);
		checkUsername = checkUsernameStruct.userQuery;
	</cfscript>

	<!---If authentication failed, display message, include login screen again and abort--->
	<CFIF checkUsername.RecordCount IS NOT 1>
		<!--- <CFSET message = "Invalid login.  Please try again."> --->
		<cfset message = checkUsernameStruct.message>

		<!--- NJH 2010/06/29 - if password is expired, redirect them to resendPW.cfm where they can update their password --->
		<cfset loginTemplate="login.cfm">
		<cfif checkUsernameStruct.reason eq "passwordExpired">
			<cfset url.spl=application.com.emailmerge.generatePasswordLink(personID=checkUsernameStruct.failedUserQuery.personID)>
			<cfset url.pwe=1>
			<cfset loginTemplate="resendPW.cfm">
		</cfif>
		<CFINCLUDE TEMPLATE="#loginTemplate#">
		<CF_ABORT>

	<cfelse>

			<!--- set user coookies --->
			<!--- DAM 2001 Nov 19

					Where a machine is used by more than one user a user may open a session
					and find that they are using cookie variables that belonged to
					an earlier session. If the earlier session was openned by an other user then
					the value of some cookie variables may be incorrect.

					This is prevented here by ensuring that the cookies are all destroyed prior to
					establishing a new session.

			--->
			<CFINCLUDE TEMPLATE="resetblind.cfm">

			<cfset application.com.RelayCurrentUser.setLoggedIn(checkUsername.personID,2)>

			<!--- 2008/04/14 GCC - removed CGI.server_port_secure = true check because even if we have set request.currentsite.useSecureLogin
			to true and are on https it may not be secure which leaves our app stuck in https - causes problems in application behaviour if we don't 'flip' back to http --->
			<cfif request.currentsite.useSecureLogin>

				<!--- need to relocate back to in secure site, session information should be preserved --->
				<script>
					relocateTo = window.location.href.replace('https:','http:')
		 			//	alert (relocateTo);
					window.location.href = relocateTo;
				</script>
				<CF_ABORT>
			</cfif>

	</CFIF>

</cfif>


<CFIF not request.relaycurrentUser.isLoggedIn and not request.relaycurrentUser.isLoggedInExpired >   <!--- WAB 2005-09-06 added test for cookie.user--->
			<CFINCLUDE TEMPLATE="login.cfm">
<CFelseif  request.relaycurrentUser.isLoggedInExpired>
			<!--- session expired --->
			<!--- Superflous message
				<CFSET message = "Your session has expired.  Please login again."> --->
			<CFINCLUDE TEMPLATE="login.cfm">

<CFELSEIF request.relaycurrentUser.isLoggedIn and IsDefined("wabTestInPopUP")>

			<!--- pre 2006 WAB testing a pop-up login form
				Note that this code went live and works successfully but never lost its wabtest variable
				2008-04-30  WAB the code did not work if the login was on a secure server and window.opener was not secure (cross site scripting presumably)
							however by moving the code down the file a bit and adding request.relaycurrentUser.isLoggedIn, I was able to take advantage of the relocation from https to http above
							so the sequence becomes ..
								pop up login loads
								is submitted by https
								login takes place and page js relocated to http version
								this code runs and refreshes the opener
			--->



			<script>
					window.opener.location.reload();
					window.close();
			</script>

<cfelse>
			<!---  Ok to carry on--->

		<!--- wab added this to counteract problem with blank user cookies left over from partner apps
		maybe should test for user cookie of form xxx-yyy --->
		<CFIF isDefined("request.relayCurrentUser.personid")>
			<CFIF Cookie.user is "">
				<!--- user cookie is blank (probably after using some partner apps) --->
				<CFSET message = "Please login again.">
				<CFINCLUDE TEMPLATE="login.cfm">
				<CF_ABORT>
			</cfif>
		</CFIF>

		<CFIF isDefined("relocateto")>
			<CFSET MainTemplate = "#relocateto#">
		<CFELSE>
			<CFSET MainTemplate = "/templates/subframe.cfm?module=Home">
		</CFIF>

		<!---st will contain the starting template that we want to go to after auto login and will usually be set by remote.cfm --->
		<!--- 2008/10/28 Bug Fix T-10 Isse 1 - for backwards compatabilty convert url.st to url.opentab
			Check if the element exists in the V2 structure. If not, use that from V1
		--->
		<cfif structKeyExists(url,"st")>
			<cfif structKeyExists(application.startingTemplateStrV2,"#url.st#")>
				<cfset url.openTab = application.startingTemplateStrV2[url.st]>
			<cfelseif structKeyExists(application.startingTemplateStr,"#url.st#")>
				<cfset url.openTab = application.startingTemplateStr[url.st]>
			</cfif>
		</cfif>

		<cfinclude template="UI/EXTUI/mainLayoutext.cfm">


</CFIF>


