<!--- �Relayware. All Rights Reserved 2014 --->
<!---
WAB 2005-06-28 added setting of relaycurrentuser...showTranslationLinks which will take over from cookie

 --->

	<CFQUERY NAME="FindLanguageRights" DATASOURCE="#application.SiteDataSource#">
		SELECT l.Languageid
		FROM Language as l, booleanflagdata as bfd, flag as f, flaggroup as fg
		WHERE fg.flaggrouptextid='LanguageRights'
		and 	fg.flaggroupid = f.flaggroupid
		and 	f.name=l.language
		and bfd.flagid= f.flagid
		and bfd.entityid = #request.relayCurrentUser.personid#
	</CFQUERY>


	<CFIF findlanguagerights.recordcount is not 0>
		<cfset application.com.globalFunctions.cfcookie(NAME="translator", VALUE="#valuelist(findlanguagerights.languageid)#")>
		<CFOUTPUT>Translator cookieset to #htmleditformat(cookie.translator)#</CFOUTPUT>
		<cfset application.com.relayCurrentUser.updateContentSettings(ShowTranslationLinks = true)>
	<CFELSE>
		You do not have translation rights
	</CFIF>
