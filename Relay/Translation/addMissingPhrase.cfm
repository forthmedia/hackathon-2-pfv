<!--- �Relayware. All Rights Reserved 2014 --->
<!--- WAB 2010-10-13  removed cf_get phrase  and replaced with CF_translate --->

<cf_title>Add Missing Phrase</cf_title>

<CFOUTPUT><SCRIPT type="text/javascript" SRC="/javascript/dotranslation.js"></SCRIPT></CFOUTPUT>


<CFIF isDefined("phrasesToTranslate")>
	<TABLE WIDTH="600" BORDER="0" CELLSPACING="3" CELLPADDING="3" ALIGN="center">
		<TH>Phrases to add</TH>
		<cf_translate>
		<CFLOOP INDEX="phrase" LIST="#phrasesToTranslate#">
			<CFOUTPUT>
				<TR>
					<TD>
						phr_#htmleditformat(phrase)#
					</TD>
				</TR>
			</CFOUTPUT>
		</CFLOOP>
		<cf_translate>
	</TABLE>
<CFELSE>
	<CFSET phrasesToTranslate = "">
</CFIF>



<FORM method="post">
	<TABLE WIDTH="600" BORDER="0" CELLSPACING="3" CELLPADDING="3" ALIGN="center">
		<tr>
			<td colspan="2">Insert the phrases you wish to add to the system.
			You can add more than one phrase by separating them with a comma but no spaces.
			</td>
		</tr>
		<tr>
			<td>Phrase(s) to translate</td>
			<td><CFOUTPUT><CF_INPUT TYPE="text" NAME="phrasesToTranslate" 
				VALUE="#phrasesToTranslate#" SIZE="80" MAXLENGTH="255">
				</CFOUTPUT>
			</td>
			<tr><td colspan="2"><input type="submit"></td></tr>
		</tr>
		
		<TR><TD COLSPAN="3"><cfinclude template="/translation/toolsInclude.cfm"></TD></TR>
	</table>

</FORM>


