<!--- �Relayware. All Rights Reserved 2014 --->
<!---

called from editPhrase.cfm

Creates a table for adding a new phrase

WAB 08 June 2005

WAB 2009/09/22 added cfform JS verification including  webservices in webservices/relayTranslatiosnWS.cfc
SB  2015/11/13 Boostrap and removed tables.
 --->



	 <CFQUERY NAME="FindAdditionRights" DATASOURCE="#application.SiteDataSource#">
		SELECT
			1
		FROM
			booleanflagdata as bfd, flag as f
		WHERE
			f.flagtextid='AddNewPhraseRight'
			and bfd.flagid= f.flagid
			and bfd.entityid = #request.relayCurrentUser.personid#
	</CFQUERY>

<CFIF FindAdditionRights.recordCOunt is not 0>

		<CFQUERY NAME="getLanguages" DATASOURCE="#application.SiteDataSource#">
			select languageid as datavalue, language as displayValue from language where languageid in (#request.relaycurrentuser.LanguageEditRights#)
		</CFQUERY>




	<script>
		function validatePhrasetextID (form_object, input_object, object_value) {

			// if blank then return false - actually I don't think that the function will be called in this case because it is required
			if (object_value =='') {
				return false
			}

				page = '/webservices/relayTranslationsWS.cfc?method=validateNewPhraseTextID&returnFormat=JSON'
				parameters = 'phraseTextID=' + object_value
				var myAjax = new Ajax.Request(
												page,
												{
													method: 'get',
													parameters:  parameters,
													asynchronous:false,
													onComplete: 	function (requestObject,JSON) {

																			json = requestObject.responseText.stripScripts().evalJSON(true);  // the strip scripts allows me to have a general login required <script> in the web service as well as a JSON version

																		}
												}
												)
				// note that request is asynchronous, because cf verification will need an immediate answer
				if (!json) {
					return true   // some sort of failure in the webservice, will have to leave verification to the server
				} else if (!eval(json.ISOK)) {
						// webservice says that there is a problem
						// we don't want the bog standard message from the cfinput, we are going to put our own message in the message 'stack'
					    _CF_onError(form_object, input_object.name, object_value, json.MESSAGE);
    	            	_CF_error_exists = true;
					return true // actually return true because we don't want the bog standard message
				} else {
					return true   // all OK, return true
				}



		}

	</script>

	<CFparam name="startAddPhraseOpen" default= true>
<!--- WAB 2009/09/20 changed to cfform and added requireds--->
<FORM ACTION="language_result.cfm" METHOD="post" NAME="Add_Record" id="Add_Record" novalidate="true">
	<cf_relayFormDisplay>
	<cfoutput>
		<!--- <div id="addPhraseInsertAction">
			<CF_divopenclose rememberSettings=false divid="addPhrase1" alsoToggleDivIDs="addPhrase2" startOpen=#startAddPhraseOpen#>
				<CF_divopenclose_image><b>Add an entirely new phrase to the database</b></CF_divopenclose_image>
			</CF_divopenclose>
		</div> --->
		<div id="addPhrase1">
			<cfif isdefined('newPhraseTextID')><cfset value = newPhraseTextID><cfelse><cfset value = ""></cfif>
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<label for="newtextid" required="true">Phrase Identifier <!--- Enter an Identifier ---> <!-- StartDoNotTranslate --></label>
					<cf_INPUT TYPE="TEXT" class="form-control" id="newtextid" name="newtextid" value="#value#" required=true message="Enter an Identifier" validateat="onsubmit" onvalidate="validatePhrasetextID">
					<!-- EndDoNotTranslate -->
					<!--- note the case of  phrLanguageid, if it is set to phrLanguageID it will interfere with js in editphraseinsert if they appear on the same page--->
					<label>Any Country</label>
					<CF_displayValidValues
						formFieldName = "phrLanguageid"
							validValues="#getLanguages#"
							currentValue = "#request.relaycurrentuser.languageid#"
							showNull = true
							NullText = "Select a Language"
					>
					<input type="hidden" name = "phrcountryid" value = "0">
				</div>
				<div class="col-xs-12 col-sm-6">
					<label for="Newphrase" required="true">Content</label>
					<TEXTAREA NAME="Newphrase" id="Newphrase" ROWS="3" COLS="80" WRAP VIRTUAL required=true message="Enter the Phrase Translation" class="form-control"></TEXTAREA>
				</div>
			</div>
			<cf_relayFormElementDisplay type="submit" value="Add New Phrase" name="AddNewPhrase">
			<!--- 	<INPUT class="btn btn-primary" TYPE="submit" VALUE="Add New Phrase" NAME="AddNewPhrase">
			</div> --->
		</div>
	</cfoutput>
	</cf_relayFormDisplay>
</FORM>
</CFIF>