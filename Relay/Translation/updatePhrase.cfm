<!--- �Relayware. All Rights Reserved 2014 --->

<!--- 
called by editPhrase.cfm does updates of phrases


Author: Emma a long time ago

Mods:
2001-01-09  WAB altered all this code so that it used the update code in the 
getPhrase tag.
2009-04-17 NYB Sophos increased size of NEWTEXTID from 50 to value obtained by dtTools.getTableDetails

 --->


<cfif structKeyExists(form,"addNewPhrase")>
	<!--- NYB 2009-04-22 Sophos - added: PhraseTextIDLength --->
	<cfset PhraseTextIDLength = application.com.dbTools.getTableDetails(tableName="PhraseList",returnColumns="true",columnName="PhraseTextID").Column_Length>
	<cfif NEWTEXTID is not "">
		<CFSET NEWTEXTID=REReplace(LEFT(FORM.NEWTEXTID,PhraseTextIDLength),"[^_0-9a-zA-Z]","","ALL")>
	<cfelse>
		<CFSET NEWTEXTID=REReplace(LEFT(FORM.NEWPHRASE,PhraseTextIDLength),"[^_0-9a-zA-Z]","","ALL")>
	</cfif>
	<cfset identifier = "phrupd_#NEWTEXTID#_0_0">
	<CFSET form[identifier] = Newphrase>
	<CFSET form.fieldNames = listappend(form.fieldnames,identifier)>
	<cfset caller.frmphraseid = newTextID>	
</CFIF>
 
<cfset application.com.relayTranslations.ProcessPhraseUpdateForm()>