<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File name:		ModulePhrases.cfm	
Author:			Will, David and SWJ
Date created:	05-July-2000

Description:

Date Tested:
Tested by:

Amendment History:

Date (YYYY-MMM-DD)	Initials 	What was changed
2007-05-14		???
WAB 2010/10/13  removed get TextPhrases and replaced with CF_translate
--->
<!--- template to list all the phrases used in tradeup --->

<cf_displayBorder>
<cf_translate>


<CFPARAM NAME="Module" DEFAULT="PL_">
<CFPARAM name="sections" default = ".">   <!--- WAB added this - needed some way of not having sections, but setting to blank doesn't work in the loop, so set to . and then test for this  --->
<CFPARAM name = "showPhrasetextID" default = "false">  <!--- determines whether to show the phrase textID or the whole phrase --->

<CFSET Sections = ListChangeDelims(sections, "," , " ")>

<!--- these sections are not to be translated: TC,LOD --->
<CFSET counter=0>

	<CFQUERY NAME="LocatorManager" DATASOURCE="#application.SiteDataSource#">
		SELECT 	R.PersonID
		FROM 	RIGHTSGROUP R, USERGROUP U
 		where 	R.PersonID = #request.relayCurrentUser.personid#
		and	  	U.UserGroupID = R.UserGroupID
		and 	U.Name = 'Locator Manager'
	</cfquery>

	<CFQUERY NAME="GetLanguageList" DATASOURCE="#application.SiteDataSource#">
		SELECT 1 as SortOrder, l.Languageid, l.language
		FROM Language as l, booleanflagdata as bfd, flag as f, flaggroup as fg
		WHERE fg.flaggrouptextid='LanguageRights'
		and 	fg.flaggroupid = f.flaggroupid
		and 	f.name=l.language
		and bfd.flagid= f.flagid
		and bfd.entityid = #request.relayCurrentUser.personid#
		and Language = 'English'
		union
		SELECT 2 as SortOrder, l.Languageid, l.language
		FROM Language as l, booleanflagdata as bfd, flag as f, flaggroup as fg
		WHERE fg.flaggrouptextid='LanguageRights'
		and 	fg.flaggroupid = f.flaggroupid
		and 	f.name=l.language
		and bfd.flagid= f.flagid
		and bfd.entityid = #request.relayCurrentUser.personid#
		and Language != 'English'
		order by SortOrder,l.language
	</CFQUERY>
		
	<!--- Check is user has translation rights --->
	<CFIF GetLanguageList.recordcount>
		<CFSET ForceTranslate=1>
	<cfelse>
		<CFSET ForceTranslate=0>
	</cfif>	
	
	<CFIF LocatorManager.RecordCount>
		<!--- LocatorManagerGroup, so show all languages --->
		<CFQUERY NAME="GetLanguageList" DATASOURCE="#application.SiteDataSource#">
			SELECT 1 as SortOrder, l.Languageid, l.language
			FROM Language as l
			where language = 'English' 
			union
			SELECT 2 as SortOrder, l.Languageid, l.language
			FROM Language as l
			where language != 'English'
			order by SortOrder,l.language
		</CFQUERY>
	</cfif>

<CFLOOP index="section" list="#Sections#">
	<CFIF section is not ".">
		<CFQUERY NAME="PhraseList" DATASOURCE="#application.SiteDataSource#">
		SELECT PhraseTextID
		FROM PhraseList 
		WHERE PhraseTextID  LIKE  <cf_queryparam value="#Module#[_]#Section#[_]%" CFSQLTYPE="CF_SQL_VARCHAR" > 
		</CFQUERY>
	<CFELSE>
		<CFQUERY NAME="PhraseList" DATASOURCE="#application.SiteDataSource#">
		SELECT PhraseTextID
		FROM PhraseList 
		WHERE PhraseTextID  LIKE  <cf_queryparam value="#Module#[_]%" CFSQLTYPE="CF_SQL_VARCHAR" > 
		</CFQUERY>
	</cfif>		
	
	
<CFOUTPUT>
	

<h2>#htmleditformat(section)#</h2>
<table class="ModulePhrases" cellspacing="0">
<!--- 2008-07-11 NYF replaced: 		
	<TR>
		<CFIF ForceTranslate>
		<TD valign=top>T<BR>R<BR>A<BR>N<BR>S<BR>L<BR>A<BR>T<BR>E</td>
		</cfif>
		<TD valign=top><b>English Phrase</b></td>
		<cfloop query="GetLanguageList">
			<TD Valign=top align=center><cfloop index="POS" from="1" to='#Len("#Language#")#'>#Mid(Language, POS, 1)#<BR></cfloop></td>
		</cfloop>
with ->  --->		
		
	<CFIF ForceTranslate>
		<TR>
			<TD class="corner">&nbsp;</td>
			<TD class="TranslateHeading" colspan="#GetLanguageList.recordcount#">phr_translated</td></TR>
	</cfif>
	<TR>
		<TD class="phraseHeading"><CFIF showPhraseTextID>phr_PhraseTextID<cfelse>phr_Phrase</cfif></td>
		<cfloop query="GetLanguageList">
			<TD class="Language">#htmleditformat(Language)#</td>
		</cfloop>
<!--- <- 2008-07-11 NYF --->	

	</tr>
		<CFLOOP index="phrase" list="#valuelist(PhraseList.PhraseTextID)#">
<!--- 2008-07-11 NYF removed, because it's not being used anywhere: 	
			<CFSET counter=counter+1>
--->		
		<TR>
			<TD class="phrase">
				<CFIF showPhraseTextID>
					#htmleditformat(phrase)#
				<CFELSE>
					phr_#htmleditformat(phrase)#
				</cfif>
	 		</td>
<!--- 2008-07-11 NYF removed: 			
			<CFIF ForceTranslate>
				<TD>#evaluate("tlink_#phrase#")#</td>
			</cfif>
 --->
			<cfloop query="GetLanguageList">
				<CFQUERY NAME="Translated" DATASOURCE="#application.SiteDataSource#" DBTYPE="ODBC">
					SELECT L.PHRASEID
					FROM PHRASES p, PHRASELIST l
					WHERE P.PHRASEID = L.PHRASEID
					AND   P.LANGUAGEID = #LanguageID#
					AND   L.PHRASETEXTID = '#phrase#'
				</cfquery>
				<!--- Check if translation exists for this language or not. --->
				<CFIF Translated.RecordCount><TD class="Translated">X<cfelse><TD class="UnTranslated">&nbsp;</cfif></td>
			</cfloop>
			</tr>
		</cfloop> 
	</cfoutput>	
</table>
<P class="spacer">&nbsp;</p>

</cfloop>


</cf_translate>

</CF_displayBorder>

<!--- 	 --->


