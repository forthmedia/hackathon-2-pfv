<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			chooseloadPhraseFile.cfm
Author:				NJH
Date started:		2006-07-29

Purpose:	Choose text file from \translation\loadPhrases dir to load phrases from.
			Then choose add/update actions. These include:
				Add: add/ don't add new phrases
				Update: overwrite/merge/ignore conflicts with existing phrases

Usage:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed

Possible enhancements:

WAB 2008/07/09  Change to loadPhrasesFromFile so that can load phrases from userfiles
SB	2014/10/15	Added title and changed id for internal styling
WAB JIRA PROD2015-513 Implement an 'overwrite if unedited' option
--->

<CFSET loadPhrasesDir = "#application.paths.relay#\Translation\LoadPhrases">
<CFSET UserloadPhrasesDir = "#application.paths.code#\LoadPhrases">

<CFIF directoryExists(UserloadPhrasesDir)>
	<!--- Directory OK. --->
<CFELSE>
	<CFDIRECTORY ACTION="CREATE" DIRECTORY="#UserloadPhrasesDir#">
</CFIF>

<cfdirectory action="list" name="PhraseFiles" directory="#loadPhrasesDir#" filter="*_phrases.txt">
<cfdirectory action="list" name="userPhraseFiles" directory="#UserloadPhrasesDir#" filter="*_phrases.txt">

<cfquery name="qryPhraseFiles" dbtype="query">
select 'relay' as location, 'relay|' + name as locationAndName, lower(name) as lowerCaseName, * from PhraseFiles
union
select 'user' as location, 'user|' + name as locationAndName, lower(name) as lowerCaseName, * from userPhraseFiles
order by lowerCaseName
</cfquery>

<cfif qryPhraseFiles.recordCount eq 0>
	<cfoutput>There are no phrase files in #htmleditformat(loadPhrasesDir)# to load</cfoutput>
	<CF_ABORT>
</cfif>


<cfparam name = "frmPhraseFile" default = "#qryPhraseFiles.locationAndName[1]#">


<cf_title>Load Phrase Files</cf_title>

<cfset request.relayFormDisplayStyle = "HTML-table">
<cf_relayFormDisplay>
	<cfform name="phraseFileForm" action="chooseloadPhraseFile.cfm" method="post">
		<cf_relayFormElementDisplay relayFormElementType="select" class="form-control" currentValue="#frmPhraseFile#" fieldName="frmPhraseFile" label="phr_sys_translations_phraseFileToLoad" query="#qryPhraseFiles#" display="Name" value="locationAndName" onChange="javascript:phraseFileForm.submit();">
	</cfform>

	<cfform name="loadPhrasesForm" action="addUpdatePhrases.cfm" method="post">
		<cf_relayFormElement relayFormElementType="hidden" fieldname="frmPhraseFile" currentValue="#frmPhraseFile#">

		<cfset sysPhrases  = application.com.relayTranslations.getPhraseFileQuery (phraseFileName = frmPhraseFile)>

		<cf_querySim>
		addPhraseAction
		display, value
		phr_Add|true
		phr_Ignore|false
		</cf_querySim>

		<cf_querySim>
		updatePhraseAction
		display, value
		phr_Overwrite If Unedited|overwriteUnEdited
		phr_Overwrite|overwrite
		phr_Merge|merge
		phr_Ignore|ignore
		</cf_querySim>

		<cf_relayFormElementDisplay relayFormElementType="radio" currentValue="true" fieldname="frmAddPhrases" label="phr_sys_translations_newPhrases" query="#addPhraseAction#" display="display" value="value">
		<cf_relayFormElementDisplay relayFormElementType="radio" currentValue="overwriteUnEdited" fieldname="frmUpdatePhrases" label="phr_sys_translations_conflictingPhrases" query="#updatePhraseAction#" display="display" value="value">
		<cf_relayFormElementDisplay relayFormElementType="submit" class="btn btn-primary" spanCols="yes" valueAlign="left" currentValue="phr_sys_translations_AddUpdatePhrases" label="" fieldname="frmSubmit">

		<cf_relayFormElementDisplay relayFormElementType="HTML" currentValue="" label="phr_sys_translations_Phrases" fieldname="frmPhrases" valueAlign="left">
			<cf_relayFormDisplay formWidth="675" formId="stripes">
				<tr><th>phr_sys_translations_phraseTextID</th><th>phr_sys_translations_phraseText</th></tr>
				<cfoutput query="SysPhrases">
					<tr><td width="35%">#htmleditformat(phraseTextID)#</td><td width="65%">#htmleditformat(phraseText)#</td></tr>
				</cfoutput>
			</cf_relayFormDisplay>
		</cf_relayFormElementDisplay>

	</cfform>

</cf_relayFormDisplay>