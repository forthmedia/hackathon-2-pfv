<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			translationReport.cfm
Author:				RMC
Date started:		16-September-2014

Description:		This report shows all phrases and associated translations and generates an excel report
Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
17-Nov-2014			RJT 		Bug fix for CORE-961 Nested replace functions used to encode HTML within phrasetext in SQL query which was being rendered (causing bugs) rather than just showing the HTML
20-Nov-2014			RJT 		Bug fix for CORE-979 &restrictToNonEntityPhrases=false added to cfparam keyColumnOnClickList to allow these phrases to be populated in the language result editor
25-Nov-2014			RJT 		With significant help from Willian Bibby second filter added (so the first one can default to "EntityName - other" as it is very slow to do all entity types) and language added as an option for the filter. Also general code tidy

Possible enhancements:

 --->

<cfparam name="openAsExcel" type="boolean" default="false">
<cfparam name="sortOrder" default="phrasetextid">
<cfparam name="keyColumnList" type="string" default="phraseTextID"><!--- this can contain a list of columns that can be edited --->
<cfparam name="keyColumnKeyList" type="string" default="phraseTextID"><!--- this can contain a list of matching key columns e.g. primary key --->
<cfparam name="keyColumnOnClickList" type="string" default="javascript:void(openNewTab('Translate'*comma'Translate'*comma'/translation/Language_Result.cfm?frmphraseid=##phraseId##&reportLink=true&restrictToNonEntityPhrases=false'*comma{iconClass:'content'}));return false"><!--- this can contain a list of matching columns that contain the URL of the editor template --->
<cfparam name="keyColumnURLList" type="string" default="phraseTextID">
<cfparam name="showHTMLCode" type="boolean" default="false">

<!--- If generating an Excel file... --->
<cfif openAsExcel>
	<cfsetting requesttimeout="1200">
</cfif>

<!--- Set variable name for excel file name OR set page title and include required JS files --->
<cfif openAsExcel>
	<cfset variable.Filename = "portalTranslationReport">
<cfelse>

	<cf_includeJavascriptOnce template = "/javascript/extExtension.js">
	<cf_includeJavascriptOnce template = "/javascript/viewEntity.js">
</cfif>
<cfoutput>
	<cf_title>Portal translation report</cf_title>
</cfoutput>
<!--- Add link to trigger Excel export --->
<cf_RelayNavMenu pageTitle="" thisDir="Translations">
	<cf_RelayNavMenuItem MenuItemText="Excel" CFTemplate="javascript:openAsExceljs();" >
</cf_RelayNavMenu>

<!--- Determine the query filter criteria where appropriate  --->
<cfset filterByAppID = "">
<cfparam name="form.FILTERSELECT1" default= "entityName">
<cfparam name="form.FILTERSELECTVALUES1" default= "Other">
<!--- <cfif not structisempty(form)>
	<cfset formKeys = StructKeyList(form)>
	<cfset numFilters = application.com.globalFunctions.ListContainsCount(formKeys,"FILTERSELECT")>
	<cfif numFilters gt 1>
		<cfif form.FILTERSELECT1 eq "entityName">
			<cfset filterByEntityName = form.FILTERSELECTVALUES1>
		</cfif>
		<cfif form.FILTERSELECT1 eq "appidentifier">
			<cfset filterByAppID = form.FILTERSELECTVALUES1>
		</cfif>
	</cfif>
</cfif> --->

<cfif not len(sortOrder)>
	<cfset sortOrder = "phrasetextid">
</cfif>


<cfquery name ="filterQueryObject">
select distinct
	CASE v.entityTypeID WHEN 0 THEN 'Other' ELSE s.entityName END as entityName ,
	appidentifier ,
	ISNULL(l.Language,'No language') AS [Language_]
from
			vPhrases v
				LEFT OUTER JOIN Language l on v.languageid = l.languageid
				LEFT OUTER JOIN Country c ON v.countryid = c.CountryID
				INNER JOIN	schemaTable s ON v.entityTypeID = s.entitytypeid
				LEFT OUTER JOIN phraserequest r ON v.phrasetextid = r.phrasetextid AND v.entityID = r.entityid

</cfquery>
<!--- Run query to return phrases/translations --->
<!--- RJET 17-Nov-2014 Bugfix CORE-961 Nested replace functions used to encode HTML within phrasetext which was being rendered (causing bugs) rather than just showing the HTML --->
<cfquery name="getPhrasesTranslations" datasource="#application.siteDataSource#">
select * from (
	SELECT		v.phraseid,
				v.phrasetextid + case when v.entityTypeID <> 0 then '_' + s.entityName + '_' + convert(varchar,v.entityID) else  '' end as phrasetextid ,
				replace(
					replace(
      					replace(v.phrasetext,'&','&amp')
						,'<','&lt')
					,'>','&gt')
		 		AS phrasetext,
				v.entityID, v.entityTypeID,
				ISNULL(c.CountryDescription,'All countries') AS CountryDescription,
				ISNULL(l.Language,'No language') AS [Language_],
				CASE v.entityTypeID WHEN 0 THEN 'Other' ELSE s.entityName END AS entityName,
				r.appidentifier
	FROM		vPhrases v LEFT OUTER JOIN phraserequest r ON v.phrasetextid = r.phrasetextid AND v.entityID = r.entityid
				LEFT OUTER JOIN Language l on v.languageid = l.languageid
				LEFT OUTER JOIN Country c ON v.countryid = c.CountryID
				INNER JOIN	schemaTable s ON v.entityTypeID = s.entitytypeid
) as x
	WHERE
				len(LTRIM(phrasetext))!=0
				<cfinclude template="\templates\tableFromQuery-QueryInclude.cfm">

	ORDER BY	<cf_queryObjectName value="#sortorder#">

</cfquery>

<cfif openAsExcel>
	<cfloop from="1" to="#getPhrasesTranslations.recordcount#" index="i">
		<cfset getPhrasesTranslations.phrasetext[i] = htmleditformat(getPhrasesTranslations.phrasetext[i])>
	</cfloop>
</cfif>

<!--- Output table of results --->
<cfoutput>
<CF_tableFromQueryObject queryObject="#getPhrasesTranslations#"
	showTheseColumns="phrasetextid,phrasetext,CountryDescription,Language_,appidentifier"
	ColumnHeadingList="Phrase Text ID,Phrase Text,Country,Language,App ID"
	useInclude="false"
	openAsExcel="#openAsExcel#"
	FilterSelectFieldList="entityName,appidentifier,language_"
	FilterSelectFieldList2="entityName,appidentifier,language_"
	keyColumnList="#keyColumnList#"
	keyColumnURLList="#keyColumnURLList#"
	keyColumnKeyList="#keyColumnKeyList#"
	keyColumnOnClickList="#keyColumnOnClickList#"
	disableShowAllLink="true"
		tfqoversion =1
	filterQueryObject= #filterQueryObject#
>
</cfoutput>