<!--- �Relayware. All Rights Reserved 2014
2015/12/02 SB Bootstrap
2106-01-29 WAB Remove twoSelects Combo



--->
<cf_translate>
<!--- submitted --->
<cfif isDefined("doAction")>
	<cfswitch expression="#doAction#">
		<cfcase value="new">
			<cftransaction>
				<cfquery name="insertTCG" datasource="#application.sitedatasource#">
					insert into	translationCountryGroup
					(tcGroupName,languageID,createdBy,lastUpdatedBy)
					values
					(<cf_queryparam value="#form.tcGroupName#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#form.language#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#request.relaycurrentuser.usergroupid#" CFSQLTYPE="cf_sql_integer" >,<cf_queryparam value="#request.relaycurrentuser.usergroupid#" CFSQLTYPE="cf_sql_integer" >)
					select scope_identity() as newTCGID
				</cfquery>
				<cfif insertTCG.recordcount gt 0>
					<cfloop list="#form.selectedCountries#" index="countryID">
						<cfquery name="insertTCGCountries" datasource="#application.sitedatasource#">
							insert into	translationCountryGroupCountries
							(countryID,translationCountryGroupID)
							values
							(<cf_queryparam value="#countryID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#insertTCG.newTCGID#" CFSQLTYPE="CF_SQL_INTEGER" >)
						</cfquery>
					</cfloop>
				</cfif>
			</cftransaction>
			<cflocation url="translationCountryGroupView.cfm"addToken="false">
		</cfcase>
		<cfcase value="edit">
			<cftransaction>
				<cfquery name="updateTCG" datasource="#application.sitedatasource#">
					update	translationCountryGroup
					set		tcGroupName =  <cf_queryparam value="#form.tcGroupName#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
							lastUpdatedBy = #request.relaycurrentuser.usergroupid#,
							lastUpdated = #createODBCDateTime(now())#
					where	translationCountryGroupID =  <cf_queryparam value="#form.TCGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >
				</cfquery>
				<cfquery name="deleteOldTCGCountries" datasource="#application.sitedatasource#">
					delete from	translationCountryGroupCountries
					where		translationCountryGroupID =  <cf_queryparam value="#form.TCGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >
				</cfquery>
				<cfloop list="#form.selectedCountries#" index="countryID">
					<cfquery name="insertTCGCountries" datasource="#application.sitedatasource#">
						insert into	translationCountryGroupCountries
						(countryID,translationCountryGroupID)
						values
						(<cf_queryparam value="#countryID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#form.TCGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >)
					</cfquery>
				</cfloop>
			</cftransaction>
			<cflocation url="translationCountryGroupView.cfm"addToken="false">
		</cfcase>
	</cfswitch>
</cfif>

<cfparam name="action" default="new">
<cfparam name="TCGroupID" default="0">

<cfset countries = application.com.relaycountries.getCountries()>
<cfset tcgCountriesLIST = "">



<cf_head>
	<script type="text/javascript">
		<!--

		function verifyForm(task) {
			var form = document.tcgAction;
			var msg = "";

			if (form.tcGroupName.value == "") {
				msg += "* phr_tcgName\n";
			}

			if (form.selectedCountries.options.length == 1) {
				msg += "* phr_selectedCountries\n";
			}

			if (msg != "") {
				alert("\nphr_provideInfoForTranslationCountryGroup:\n\n" + msg);
			} else {
				form.submit();
			}
		}

		//-->
	</SCRIPT>
</cf_head>


<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
	<TR class="Submenu">
		<TD ALIGN="LEFT" VALIGN="TOP" class="Submenu">&nbsp;phr_addEditTranslationCountryGroup</TD>
		<TD ALIGN="right" VALIGN="TOP" class="Submenu">
			<a href="translationCountryGroupAction.cfm?action=new" class="Submenu">phr_addNew</a>&nbsp;<font color=white>|</font>&nbsp;<A HREF="JavaScript:verifyForm('#action#');" class="Submenu">phr_save</a>&nbsp;<font color=white>|</font>&nbsp;<a href="translationCountryGroupView.cfm" class="Submenu">phr_back</a> &nbsp;&nbsp;
		</TD>
	</TR>
</TABLE>

<cfif isDefined("Message") and message is not "">
<center><p><cfoutput>#application.com.security.sanitiseHTML(message)#</cfoutput></p></center>
</cfif>
<!--- get group detail. used by new action... add empty row in case statement --->
<cfquery name="getTCGroupData" datasource="#application.sitedatasource#">
	select	tcg.*, l.language, l.languageID, tcgc.*
	from	translationCountryGroup tcg inner join
			translationCountryGroupCountries tcgc on tcg.translationCountryGroupID = tcgc.translationCountryGroupID inner join
			language l on l.languageID = tcg.languageID
	where	tcg.translationCountryGroupID =  <cf_queryparam value="#TCGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >
</cfquery>

<cfswitch expression="#action#">
	<cfcase value="new">
		<cfscript>
			queryAddRow(getTCGroupData);
		</cfscript>
	</cfcase>
	<cfcase value="edit">
		<cfset tcgCountriesLIST = valueList(getTCGroupData.countryID)>
	</cfcase>
</cfswitch>

<!--- get countries in the right query format for twoSelectsComboQuery --->
<cfquery name="countriesQRY" dbtype="query">
	select		countryID as dataValue, countryDescription as displayValue
	from		countries
	order by	countryDescription
</cfquery>
<cfoutput>
<CFFORM name="tcgAction"  method="post">
	<cfset request.relayFormDisplayStyle = "HTML-table">
	<cf_relayFormDisplay>
		<cf_relayFormElement relayFormElementType="hidden" currentValue="#action#" fieldName="doAction">
		<cf_relayFormElement relayFormElementType="hidden" currentValue="#TCGroupID#" fieldName="TCGroupID">

		<TABLE BORDER=0 CELLPADDING=5 CELLSPACING=0 BGCOLOR="white" WIDTH="320">
			<TR>
				<TD>phr_tcgName</TD>
				<TD><cf_relayFormElement relayFormElementType="text" currentValue="#getTCGroupData.tcGroupName#" fieldName="tcGroupName" maxLength="80"></TD>
			</TR>

			<TR>
				<TD>phr_tcgLanguage</TD>
				<TD>
					<cfquery name="getLanguageDetail" datasource="#application.sitedatasource#">
						select	languageID, language
						from	language
						<cfif action is "edit" and isDefined("getTCGroupData") and getTCGroupData.recordcount gt 0>
						where	languageID =  <cf_queryparam value="#getTCGroupData.languageID#" CFSQLTYPE="CF_SQL_INTEGER" >
						</cfif>
					</cfquery>
					<cfif action is "new">
						<cf_relayFormElement relayFormElementType="select" currentValue="#getTCGroupData.languageID#" fieldName="language" query="#getLanguageDetail#" display="language" value="languageID">
					<cfelse>
						<cfif getLanguageDetail.recordcount gt 0>
							<cf_relayFormElement relayFormElementType="hidden" currentValue="#getTCGroupData.languageID#" fieldName="language">
							<b>#htmleditformat(getLanguageDetail.language)#</b>
						</cfif>
					</cfif>
				</TD>
			</TR>
			<TR>
				<TD>phr_tcgCountries</TD>
				<TD>
					<cf_select name = "selectedCountries" query = "#countriesQRY#" selected = #tcgCountriesLIST# multiple = "true">
				</TD>
			</TR>
			<TR>
				<TD COLSPAN="2">
					<A HREF="JavaScript:verifyForm('#action#');"><IMG SRC="/images/buttons/c_save_e.gif" BORDER="0"></A>
				</TD>
			</TR>
		</TABLE>
	</cf_relayFormDisplay>
</CFFORM>
</cfoutput>




</cf_translate>