<!--- �Relayware. All Rights Reserved 2014 --->
<!---

WAB 2005-06-28 added setting of relaycurrentuser...showTranslationLinks which will take over from cookie

 --->
<cf_title>Set Translator Cookie</cf_title>

<CFIF request.relaycurrentuser.isInternal> <!--- WAB 2006-01-25 Remove form.userType    <CFIF UserType is "internal">--->
	<cfinclude template="translationTopHead.cfm">
</cfif>

<CFIF not isDefined("request.relayCurrentUser.personid")>
	Please get a user account to use this function
	<CFMAIL TO="help@foundation-network.com" FROM="#application.AdminEmail#"
					SUBJECT="Illegal Translation Attempt">
	On #request.CurrentSite.Title# someone attemted to setCookie.cfm
	<CFIF Isdefined("REMOTE_ADDR")>
	REMOTE_ADDR: #REMOTE_ADDR#
	</CFIF>

	</CFMAIL>
	<CF_ABORT>
</CFIF>

<CFQUERY NAME="FindLanguageRights" DATASOURCE="#application.SiteDataSource#">
	SELECT l.Languageid, language
	FROM Language as l, booleanflagdata as bfd, flag as f, flaggroup as fg
	WHERE fg.flaggrouptextid='LanguageRights'
	and 	fg.flaggroupid = f.flaggroupid
	and 	f.name=l.language
	and bfd.flagid= f.flagid
	and bfd.entityid = #request.relayCurrentUser.personid#
</CFQUERY>


<CFIF findlanguagerights.recordcount is not 0>
	<cfset application.com.globalFunctions.cfcookie(NAME="translator", VALUE="#valuelist(findlanguagerights.languageid)#")>
	<cfset application.com.relayCurrentUser.updateContentSettings(ShowTranslationLinks = true)>


	Translator cookie set to allow translations in the following languages <CFOUTPUT QUERY="FindLanguageRights">#htmleditformat(language)#, </CFOUTPUT>
	<cfif cookie.translator neq "#valuelist(findlanguagerights.languageid)#">
	<SCRIPT>
		window.location.reload()
	</SCRIPT>

	</cfif>

<CFELSE>
	You do not have translation rights
</CFIF>

<BR><BR><CFOUTPUT><a href="/index.cfm">Login</a></CFOUTPUT>
<BR><BR><CFOUTPUT><a href="/translation/unsetCookie.cfm">Unset cookie</a></CFOUTPUT>



