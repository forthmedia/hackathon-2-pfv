<!--- �Relayware. All Rights Reserved 2014 --->
<!---

WAB 2004-12-21 mods to deal with languageid = 0
WAB June 05  Major mods to translation process
WAB 2006-02-20  Added exactMatch parameter
WAB 2009/09/22 added searchPhrasetextID parameter
WAB 2009/09/30  HTML edit format the results - to prevent JS running
			output country as well as language
			output entity type & id if required
WAB 2012-11-18 CASE 432273 Added DONOTTRANSLATE tags to prevent translation of phrase within phrase
WAB 2013-20-23	CASE 432365 Moved DONOTTRANSLATE tags outside of the loop, otherwise overloads onRequestEnd if there are 1000s of results
RMC	2014-09-11	Added code to remove single/double quotes from search string as these were throwing an error.
				Recrafted query
RJT 2014-11-20 Core 979 - added restrictToNonEntityPhrases option, this allows for when this is populated for NonEntityPhrases which can occure from Relay/Translation/translationReport.cfm
SB  2015-11-26 Bootstrap
 --->


<cfparam name = "exactMatch" default = "0">
<cfparam name = "searchPhrasetextID" default = "0">  <!--- WAB 2009/09/23 added parameter to allow limiting search to the phrasetextid--->
<cfparam name="restrictToNonEntityPhrases" type="boolean" default = "true"> <!--- Added for bug core 979 Comes in from the URL, see also Relay/Translation/translationReport.cfm --->



<cf_includeonce template = "\translation\updatePhrase.cfm">

<cf_title>Phrase Search Result</cf_title>

<CFIF PARAMETEREXISTS(LANGUAGECHOICE) IS "YES">
	<CFQUERY NAME="getLanguages" datasource="#application.SITEDATASOURCE#">
		SELECT  phrases.languageid
		FROM  phrases INNER JOIN language
		ON phrases.languageid=language.languageid
		WHERE (language.language =  <cf_queryparam value="#LanguageChoice#" CFSQLTYPE="CF_SQL_VARCHAR" > )
	</CFQUERY>
</CFIF>

<!--- WAB 2013-20-23 CASE 432365 Recrafted query
	Split out into two queries with a UNION rather than an OR and a 'temporary table'
	cut run time from >45s to ~2s when searching for word Password (which returns a few 1000 rows)
--->
<cfquery name="getPhrases" datasource="#application.SITEDATASOURCE#">
SELECT
	 p.PhraseID, PhrasetextID,  PhraseText,
	entityid,entityTypeID	,
	isNull(l.language,'No Language') as language,
	isNull(c.countrydescription,'Any Country') as countrydescription
 FROM
	 	(
		<CFIF isDefined("description") and len(description) gt 0 and searchPhrasetextID is 0>
			select
				p.ident, p.phraseid, phrasetextID, entityid, entityTypeID
			from
				phrases p inner join phraselist pl on p.phraseID = pl.phraseid
			where
				<!--- RMC: 11/September/2014 Added code to remove single/double quotes from search string --->
				<cfset description = replacenocase(description,chr(34)," ","all")><!--- Double quotes --->
				<cfset description = replacenocase(description,chr(39)," ","all")><!--- Single quotes --->
				<cfset descriptionWithQuotation = """ + #description# + """>

				 CONTAINS(phraseText, <cf_queryparam value="#descriptionWithQuotation#" CFSQLTYPE="CF_SQL_VARCHAR" >)
				<cfif exactMatch is 1>
					AND phraseText = <cf_queryparam value="#description#" CFSQLTYPE="CF_SQL_VARCHAR" >
				</cfif>
				and entityTypeID not in (61,10)

			UNION
		</CFIF>

			select
				p.ident, p.phraseid, phrasetextID, entityid, entityTypeID
			from
				phrases p inner join phraselist pl on p.phraseID = pl.phraseid

			where
				<CFIF isDefined("frmphraseid") and isNumeric(frmPhraseID) and frmPhraseID gt 0>
					(p.PhraseID =  <cf_queryparam value="#frmphraseid#" CFSQLTYPE="CF_SQL_INTEGER" > )
				<cfelseif 	isDefined("frmphraseid") and not isNumeric(frmPhraseID) >
					(PhraseTextID =  <cf_queryparam value="#frmphraseid#" CFSQLTYPE="CF_SQL_VARCHAR" > ) and entityid=0 and entitytypeid = 0
				<CFelseIF isDefined("description") and len(description) gt 0>
					<cfset descriptionWithNoSpaces = replace(description," ","","ALL")>
					<cfif exactMatch is 1>
						(
							phraseTextID =  <cf_queryparam value="#descriptionWithNoSpaces#" CFSQLTYPE="CF_SQL_VARCHAR" >
						)
					<cfelse>
						(
							phraseTextID  LIKE  <cf_queryparam value="%#descriptionWithNoSpaces#%" CFSQLTYPE="CF_SQL_VARCHAR" >
						)
					</cfif>
				<cfelse>
					1 = 0
				</cfif>
				<CFIF restrictToNonEntityPhrases> <!--- condition added for bug core 979 --->
					AND entityTypeID = 0
				</CFIF>
		) AS IDS


	 INNER JOIN phrases p ON IDs.ident = p.ident
	 left JOIN language l on p.languageID = l.languageID
	 left JOIN country c on p.countryID = c.countryID



	order by entityTypeID, phrasetextid
</cfquery>

<cfquery name="getPhraseCount" dbtype="query">
	Select distinct PhraseID from getPhrases
</cfquery>

<cfinclude template="/translation/Language_Search.cfm">

<div class="row">
	<div class="col-xs-12">
		<!-- StartDoNotTranslate -->
		<CFOUTPUT>
			<CFIF isDefined("description") and len(description) gt 0>
			<p><b>#getPhraseCount.RECORDCOUNT# phrase(s) found containing "#htmleditformat(description)#"</b></p>
				<cfif getPhraseCount.recordcount gt 1>
				<p>Choose a phrase to edit/add translations for #htmleditformat(description)#.</p>
				</cfif>
			<cfelse>
			<!--- WAB 2009/09/22 removed this.  In these cases we have passed in a specfic phraseid/phraseTextid so only be expecting a single result anyway
				<strong>#getPhraseCount.RECORDCOUNT# phrase(s) found containing #getPhrases.phraseTextID#</strong><br>
				Use the screen below to edit the content.
				--->
			</CFIF>
		</CFOUTPUT>
		<!-- EndDoNotTranslate -->
	</div>
</div>
<CFIF getPhraseCount.RECORDCOUNT IS "1">
	<CFSET FRMPHRASEID=getPhrases.PHRASEID>

	<cfinclude template="/translation/editphraseinsert.cfm">
	<!--- <cfinclude template="/translation/try.cfm"> --->
<cfelseif getPhraseCount.RECORDCOUNT IS "0">
	<CFSET FRMPHRASEID=0>
	<CFIF isDefined("description") and len(description) gt 0>
		<cfset newPhraseTextID = description>
	<cfelseif isDefined("frmphraseid") and not isNumeric(frmPhraseID)>
		<cfset newPhraseTextID = frmPhraseID>
	<cfelse>
		<cfset newPhraseTextID = "">
	</cfif>
	<!---
		WAB 2005-06-08 trying to just have one set of editing code so now use editphrase.cfm
	<cfinclude template="/translation/try.cfm"> --->

	<CFSET FRMPHRASEID=-1>
	<CFSET newPhraseTextID=#REReplace(LEFT(newPhraseTextID,50),"[^_0-9a-zA-Z]","","ALL")#>
	<cfinclude template="/translation/addphraseinsert.cfm">


<CFELSE>
<!-- StartDoNotTranslate -->
	<cfoutput query="getPhrases" group="phraseID" groupcasesensitive="Yes">
		<CFIF CurrentRow MOD 2 IS NOT 0><CFSET RowClass="oddrow"><CFELSE><CFSET RowClass="evenRow"></CFIF>
		<div class="border-bottom">

			<cfoutput>
				<div class="row">
					<div class="col-xs-12">
						<!--- WAB 2009/09/30 added countryDescription --->
					   <strong>#htmleditformat(language)#, #htmleditformat(countryDescription)#</strong><br>#left(htmleditformat(PhraseText),800)# </div>
				</div>
			</cfoutput>
			<div class="row">
				<div class="col-xs-12 languageResultsButtons">
					<a href="/translation/Language_Result.cfm?frmphraseid=#URLEncodedFormat(PhraseID)#" class="btn btn-primary"><cfif entityTypeID is not 0 and structKeyexists (application.entityType,entityTypeID)>#application.entityType[entityTypeID].tablename# #htmleditformat(entityID)# </cfif>#htmleditformat(PhraseTextID)#</a>
					<!--- &description=#URLEncodedFormat(description)#&PhraseTextID=#URLEncodedFormat(PhraseTextID)# --->
			  		<a href="/translation/populateFromAnotherPhrase.cfm?phraseTextID=#description#&phraseIDToPopulate=#URLEncodedFormat(PhraseID)#" class="btn btn-primary">Populate from another phrase</a>
			  	</div>
			</div>
		</div>
	</cfoutput>
<!-- EndDoNotTranslate -->
	<!--- <cfinclude template="/translation/try.cfm"> --->
	<!--- <cfinclude template="/translation/addPhraseInsert.cfm"> --->
</CFIF>