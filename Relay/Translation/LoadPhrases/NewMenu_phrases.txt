sys_nav_Account_Management|Accounts|1|0
sys_nav_Add_New_Account|Add New Account|1|0
sys_nav_Administer|Set Up|1|0
sys_nav_All_Reports|All Reports|1|0
sys_nav_Application|Relayware Apps|1|0
sys_nav_bitly|Bitly|1|0
sys_nav_Catalog_Management|Catalogs|1|0
sys_nav_close_other_tabs|Close Other Tabs|1|0
sys_nav_close_tab|Close Tab|1|0
sys_nav_Data_DeDuplication_Automatic|Data De-Duplication (Automatic)|1|0
sys_nav_Data_DeDuplication_Fuzzy_Rules|Data De-Duplication (Fuzzy Rules)|1|0
sys_nav_Discussion_Groups|Discussion Groups|1|0
sys_nav_discussions|Discussions|1|0
sys_nav_eLearning|Training|1|0
sys_nav_Email_Redirection|Email Redirection|1|0
sys_nav_Email_to_List|Email Marketing|1|0
sys_nav_embedly|Embedly|1|0
sys_nav_Facebook|Facebook|1|0
sys_nav_File_Library|Media Library|1|0
sys_nav_File_Manager|Media Library|1|0
sys_nav_File_Type_and_Group_Manager|Media Groups|1|0
sys_nav_Home_Page_Options|Home Page Options|1|0
sys_nav_Lead_Manager|Sales Automation|1|0
sys_nav_linkedin|LinkedIn|1|0
sys_nav_Marketing_Collaboration|Marketing|1|0
sys_nav_messages|Message to Activity Stream|1|0
sys_nav_My_Favorites|Favourites|1|0
sys_nav_My_Favourite_Reports|Favourite Reports|1|0
sys_nav_my_selectionsTarget|Lists|1|0
sys_nav_New_Account_Approvals|New Account Approvals|1|0
sys_nav_Opportunities|Opportunities|1|0
sys_nav_plugins|Plug-ins|1|0
sys_nav_Portal|Portal and CMS|1|0
sys_nav_Product_Catalogues|Catalogues|1|0
sys_nav_Product_Categories|Product Categories|1|0
sys_nav_Product_Groups|Product Groups|1|0
sys_nav_Reporting|Reporting|1|0
sys_nav_ReportManager|Report Designer|1|0
sys_nav_Reports|Reports|1|0
sys_nav_Review_Files|Items for Review|1|0
sys_nav_Sales_Collaboration|Sales Automation|1|0
sys_nav_Sales_Setup|Sales Setup|1|0
sys_nav_Screen_Emails|Standard Emails|1|0
sys_nav_Screens_and_Profiles|Business Rules|1|0
sys_nav_Segmentation|Target Lists|1|0
sys_nav_selections|Target Lists|1|0
sys_nav_twitter|Social Marketing|1|0
sys_nav_User_Access_Rights|User Access Rights|1|0
sys_nav_User_Management|User Management|1|0
sys_nav_about_Relayware|About Relayware|1|0
sys_nav_AccountDatabase|Account Database|1|0
sys_nav_Accounts|Accounts|1|0
sys_nav_Activity_Scores|Activity Scores|1|0
sys_nav_Actions|Actions|1|0
sys_nav_Accounts_withCount|Accounts<span id="numberAccounts">[[numberAccounts]]</span>|1|0
sys_nav_Activity_Approvals|Activity Approvals|1|0
sys_nav_Admin|Administration|1|0
sys_nav_Admin_Tools|Admin Tools|1|0
sys_nav_Administration|Administration|1|0
sys_nav_Allocate|Allocate|1|0
sys_nav_Analytics|Analytics|1|0
sys_nav_Applications|Applications|1|0
sys_nav_Approval_Engine|Approval Engine|1|0
sys_nav_Approvals|Approvals Manager|1|0
sys_nav_Attributes|Attributes|1|0
sys_nav_Banner|Banner Builder|1|0
sys_nav_Border_Editing|Web Borders|1|0
Sys_nav_Budget_Group_Period_Allocations|Budget Group  Allocations|1|0
sys_nav_Budgets|Budget Allocation|1|0
sys_nav_Campaigns|Campaigns|1|0
sys_nav_Cashback|Cashback|1|0
sys_nav_Cashback_Claim_Approvals|Claim Approvals|1|0
sys_nav_User_Groups|User Groups|1|0
sys_nav_Catalogues|Catalogues|1|0
sys_nav_Certification|Certification|1|0
sys_nav_certifications|Certifications|1|0
sys_nav_CJM|Co-Marketing Manager|1|0
sys_nav_CJM_Campaigns|Campaigns|1|0
sys_nav_Claims|Claims|1|0
sys_nav_Claims_Catalogue|Claims Catalogue|1|0
sys_nav_Communications|Communication|1|0
sys_nav_Communications_V3|Communications|1|0
sys_nav_Config|Config|1|0
sys_nav_Configuration|Administer|1|0
sys_nav_Configuration_Tests|Configuration Tests|1|0
sys_nav_Configure|Configure|1|0
sys_nav_connector|Connector|1|0
sys_nav_Content|Portal|1|0
sys_nav_Content_Editor|Content Editor|1|0
sys_nav_Content_Manager|Manage Content|1|0
sys_nav_Content_Trees|Content Trees|1|0
sys_nav_Content_Types|Content Types|1|0
sys_nav_Courses|Courses|1|0
sys_nav_Courseware|Courseware|1|0
sys_nav_CSV_import|CSV Import|1|0
sys_nav_Data_Load|Load Data|1|0
sys_nav_Data_Manager|Manage Data|1|0
sys_nav_Data_Tools|Data|1|0
sys_nav_Dedupe|Dedupe|1|0
sys_nav_Diary|Diary|1|0
sys_nav_Download|Export Data|1|0
sys_nav_eCommerce|eCommerce|1|0
Sys_nav_Edit_Geodata|Edit Geodata|1|0
sys_nav_eLab_Courses|eLab Courses|1|0
sys_nav_eLabs|eLabs|1|0
sys_nav_eLearning|Training|1|0
sys_nav_Email_Redirection|Email Redirection|1|0
sys_nav_Email_Templates|Email Templates|1|0
sys_nav_Emails|Emails|1|0
sys_nav_eMarketing|Communication|1|0
sys_nav_eService|Service Manager|1|0
sys_nav_Event_Manager|Events|1|0
sys_nav_Events|Events|1|0
sys_nav_Feedback|Feedback|1|0
sys_nav_File_Type|File Type|1|0
sys_nav_File_Type_Group|File Type Group|1|0
sys_nav_Files|Files|1|0
sys_nav_Forecast_List|Forecast List|1|0
sys_nav_Forecast_Viewer|Forecast Viewer|1|0
sys_nav_Forecasting|Forecasts|1|0
sys_nav_Fund_Accounts|Fund Accounts|1|0
sys_nav_Fund_Manager|Marketing Collaboration|1|0
sys_nav_Fund_Manager_withCount|Fund Manager<span id="numberFundManagers">[[numberFundManagers]]</span>|1|0
sys_nav_Fund_Requests|Fund Requests|1|0
sys_nav_Global_Replace|Global Replace|1|0
Sys_Nav_Help|Help|1|0
sys_nav_Home|Home|1|0
sys_nav_HTML_Content|HTML Content|1|0
sys_nav_Incentive_Manager|Incentives|1|0
sys_nav_Invoices|Invoices|1|0
sys_nav_Issue_List|Issue List|1|0
sys_nav_KnowledgeBase_Editor|KnowledgeBase Editor|1|0
sys_nav_Lead_Opportunity|Opportunity Manager|1|0
sys_nav_Leads_withCount|Leads<span id="numberLeads">[[numberLeads]]</span>|1|0
sys_nav_Leads|Leads|1|0
sys_nav_List_Reports|All Reports|1|0
sys_nav_Locator|Partner Locator|1|0
sys_nav_Locator_Definitions|Locator Definitions|1|0
sys_nav_Manage|Manage|1|0
sys_nav_Match_Locations|Match Locations|1|0
sys_NAV_Matching|Matching|1|0
sys_nav_Media_Manager|Media Manager|1|0
sys_nav_Modules|Modules|1|0
sys_nav_My_Accounts|My Accounts|1|0
sys_nav_My_Actions|My Actions|1|0
sys_nav_My_Approvals|My Approvals|1|0
sys_nav_My_Favourite_Reports_withCount|Favorite Reports<span id="numberFavouriteReports">[[numberFavouriteReports]]</span>|1|0
sys_nav_My_Links|My Favourites|1|0
Sys_Nav_My_Opportunities|My Opportunities|1|0
Sys_Nav_My_Phone_List|Phone List|1|0
sys_nav_my_selections|Selected Lists|1|0
sys_nav_My_Selections_withCount|Selected Lists<span id="numberSelections">[[numberSelections]]</span>|1|0
sys_Nav_My_Teams|My Teams|1|0
Sys_Nav_My_Timesheets|My Timesheets|1|0
sys_nav_News_Manager|RSS Manager|1|0
sys_nav_NewsLetters|NewsLetters|1|0
sys_nav_Opportunities_withCount|Opportunities<span id="numberOpps">[[numberOpps]]</span>|1|0
sys_nav_Reports_withCount|Reports<span id="numberReports">[[numberReports]]</span>|1|0
sys_nav_Actions_withCount|Actions<span id="numberActions">[[numberActions]]</span>|1|0
sys_nav_Orders|Orders|1|0
sys_nav_Outlook_calendar|Calendar|1|0
sys_nav_Outlook_contacts|Contacts|1|0
sys_nav_Outlook_inbox|Inbox|1|0
sys_nav_Outlook_tasks|Tasks|1|0
sys_nav_OAuth2_Client_Applications|Manage Oauth2 Client Applications|1|0
sys_nav_OAuth2_Authorisation_Servers|Manage Oauth2 Authorisation Servers|1|0
sys_nav_overview|Overview|1|0
sys_nav_Partners|Partners|1|0
sys_nav_PCM_Masks|PCM Masks|1|0
sys_nav_Phone_list|Phone list|1|0
sys_nav_PhoneList|Phonelist|1|0
sys_nav_Phoning|Phoning|1|0
sys_nav_Pipeline|Pipeline|1|0
sys_nav_Presentations|Presentations|1|0
sys_nav_Prizes|Approval|1|0
sys_nav_Prizes_Catalogue|Reward Catalog|1|0
sys_nav_Processes|Processes|1|0
sys_nav_Product_Group_Shipping|Product Group Shipping|1|0
sys_nav_product_trees|Product Trees|1|0
sys_nav_Products|Products|1|0
sys_nav_Profile_Manager|Profiles|1|0
sys_nav_Profiles|Profiles|1|0
sys_nav_Projects|Project Manager|1|0
sys_nav_Promotions|Promotions|1|0
sys_nav_Purchase_Orders|Purchase Orders|1|0
sys_nav_Questions|Questions|1|0
sys_nav_Quizzes|Quizzes|1|0
Sys_Nav_RecentSearches|Recent Searches|1|0
sys_nav_registration_Approvals|Registration Approvals|1|0
sys_nav_registrations|Registrations|1|0
sys_nav_Relay_Vars|Relay Vars|1|0
sys_nav_Requirements|Requirements|1|0
sys_nav_Reset_Sales_History_Dates|Reset Sales History Dates|1|0
sys_nav_Resources|Resources|1|0
sys_nav_Revenue|Revenue|1|0
sys_nav_Rights|Rights|1|0
Sys_nav_sales_force|SalesForce|1|0
sys_nav_Sales_Out_Data|Sales Out Data|1|0
sys_nav_Salesforce_connector_dashboard|Salesforce Connector Dashboard|1|0
sys_nav_Salesforce_connector_objects|Salesforce Connector Objects|1|0
sys_nav_Salesforce_connector_field_mappings|Salesforce Connector Field Mappings|1|0
sys_nav_Salesforce_connector_value_mappings|Salesforce Connector Value Mappings|1|0
sys_nav_Salesforce_connector_mappings|Salesforce Connector Mappings|1|0
sys_nav_SalesForce_Opportunity_Synch|SalesForce Opportunity Synch|1|0
sys_nav_SalesForce_POL_Synch|SalesForce POL Synch|1|0
sys_nav_SalesForce_Pricebook_Import|SalesForce Pricebook Import|1|0
sys_nav_SAML_Service_Providers|Manage SAML Service Providers|1|0
sys_nav_SAML_Identity_Providers|Manage SAML Identity Providers|1|0
sys_nav_ScoreCard|Score Card|1|0
sys_nav_scorecard_Tiering|Scorecard Tiering|1|0
sys_nav_scorecard_Variance|Scorecard Variance|1|0
sys_nav_scorecard_Weightings|Weightings|1|0
sys_nav_Scored_Accounts|Scored Accounts|1|0
sys_nav_Screens|Screens|1|0
sys_nav_Search|Search|1|0
sys_nav_Security|Security|1|0
sys_nav_Security_Manager|Users &amp; Security|1|0
sys_nav_Service_Contracts|Service Contracts|1|0
sys_nav_Service_Requests|Service Requests|1|0
sys_nav_Setup|Setup|1|0
sys_nav_SF_connector_dashboard|SF Connector Dashboard|1|0
sys_nav_Site_Content_Trees|Content Trees|1|0
sys_nav_Site_Manager|Content Manager|1|0
sys_nav_social|Social|1|0
sys_nav_Social_Administration|Administer Social Relayware|1|0
sys_nav_Special_Pricing|Special Pricing|1|0
sys_nav_specialisations|Specializations|1|0
sys_nav_SSO|Single Sign On|1|0
sys_nav_Summary|Summary|1|0
sys_nav_System_Utilities|System Utilities|1|0
sys_nav_Telemarketing|Telemarketing|1|0
sys_nav_Template_Attributes|Template Attributes|1|0
sys_nav_Templates|Templates|1|0
sys_nav_Tests|Tests|1|0
sys_nav_Timesheets|Timesheets|1|0
sys_nav_Trade_Up|Trade Up|1|0
sys_nav_trainingprograms|Training Programs|1|0
sys_nav_Translate|Translation|1|0
sys_nav_user_guides|User Guides|1|0
sys_nav_Users|Users|1|0
sys_nav_Utilities|Utilities|1|0
sys_nav_Valid_Values|Valid Values|1|0
sys_nav_Workflow_Screens|Screens|1|0
sys_nav_Workflows|Workflows|1|0
sys_nav_XML_import|XML Import|1|0
