<!--- WAB 2016-01-28 BF-386  moved from 2013 file and changed to remove a BR --->
activity|Activity|1|0
files_groupAndType|Media Group & Resource Type|1|0
eventList_Title|Title|1|0
eventList_City|City|1|0
eventList_Venue|Venue|1|0
eventList_Date|Date|1|0
eventList_Country|Country|1|0
eventList_Owner|Owner|1|0
eventList_EstAttendees|Est. No of Attendees|1|0
eventList_noInvited|No Invited|1|0
eventList_noRegistered|No Registered|1|0
eventList_actualAttendees|Actual Attendees|1|0
eventList_EventStatus|Event Status|1|0
commMessageHeader|Message|1|0
filter_emptyDataNotPermitted|Empty data is not permitted in the filter|1|0
pricebook_pricebookID|Pricebook ID|1|0
pricebook_organisationID|Organisation|1|0
pricebook_name|Pricebook Name|1|0
pricebook_countryID|Country|1|0
pricebook_currency|Currency|1|0
pricebook_startDate|Effective From Date|1|0
pricebook_endDate|Effective To Date|1|0
pricebook_file|Select a File To Import|1|0
RWPromotionTypeId|Promotion Type|1|0
SSO_SAMLIdentityProviders|SAML Identity Providers|1|0
SSO_SAMLIdentityProviders_title|SAML Identity Providers|1|0
sys_nav_OAuth2_Client_Applications|Manage OAuth2 Client Applications|1|0
sys_nav_OAuth2_Authorisation_Servers|Manage OAuth2 Authorization Servers|1|0
sys_nav_OAuth2_Client_Applications|Manage OAuth2 Client Applications|1|0
sys_nav_SAML_Service_Providers|Manage SAML Service Providers|1|0
sys_nav_SAML_Identity_Providers|Manage SAML Identity Providers|1|0
sys_nav_SSO_Login_Behaviour|Manage SSO Login Behaviour|1|0
SAML_PopulatedFromMetaData|Populate from metadata|1|0
SAML_LeaveBlankToUseNameID|Leave field blank to use the NameID|1|0
SSO_LoginBehaviourManagement|Login behavior|1|0
showWaterMark|Show Watermark|1|0
<!--- WAB 2016-02-08 This phrase, for some mysterious reason, had acquired the translation 'For more information, visit our website.' ! --->
sys_noneSelected|None Selected|1|0
unableToAttendEvent|Unable To Attend Event|1|0
activityStream_All|All|1|0
activityStream_Following|Following|1|0
activityStream_Flagged|Flagged|1|0
elearning_certificationValidity|Validity (in months)|1|0
module_ExpiryDate|Expiry Date|1|0
noApprovalStatus|No Approval Status|1|0
certification_personCertificationExpiryDate|Valid To Date|1|0
activityStream_Actions|Actions|1|0
activityStream_Messages|Messages|1|0
registration_domainDoesNotMatch|The email domain provided does not match to an authorized partner. The email address supplied must use a valid domain for the authorized partner and the country selected should be where the company is based/headquartered.|1|0
registration_locationNotListed|Location not listed|1|0
registration_selectCompany|Select Company|1|0
score_addAdjustment|Add Score Adjustment|1|0
score_adjustScoreBy|Adjust score by amount|1|0
score_adjustDate|Adjustment effective date|1|0
score_adjustReason|Reason for adjustment|1|0
score_manualScoreAdjustment|Score Manual Adjustment|1|0
score_viewAccount|View Detail|1|0
score_scoreCollectedByEntity|Score Collected by Entity|1|0
score_scoreRuleGrouping|Scheme|1|0
score_scoreRuleGroupings|Schemes|1|0
score_scoreRuleGrouping_Name|Scheme Name|1|0
score_numberOfActivities|Number of Activities|1|0
score_Score|Score|1|0
score_TotalScore|Total Score|1|0
score_customRule|Custom Rule|1|0
score_activityType|Activity Type|1|0
score_addRuleToGroup|Add Rule to Scheme|1|0
score_scoreRuleGrouping_Description|Description|1|0
score_scoreRuleGrouping_scoreRuleTitle|Score Rule Title|1|0
score_pointsPerActivity|Award score per activity|1|0
score_scorePerActivity|Score per Activity|1|0
score_scoringCap|Score Limit|1|0
score_rolledUp|Rolled up score|1|0
score_scoringMethod|Scoring Method|1|0
score_ActivityTime|Activity Time|1|0
score_RunningTotal|Running Total|1|0
score_cantDisableGroupingWithActiveRules|Score schemes can't be inactivated while they still have active rules|1|0
score_rulesCantBeActiveInInactiveGroups|Rules can't be active in inactive schemes|1|0
score_limitOfGroupingsReached|The limit for the number of permitted (active) schemes has been reached, please deactivate some old schemes before activating this one|1|0
score_scoringCap_restrictionPeriod|Limit reset as per scheme setup|1|0
score_FilterPeopleWhoCanRecieveScore|Filter who can receive score|1|0
score_commRead_basedOnResponsiveness|Award score based on read responsiveness|1|0
score_commRead_maxiumumPointsPerActivity|Maximum points per activity|1|0
score_commRead_minimumPointsPerActivity|Minimum points per activity|1|0
score_commRead_dailyFalloff|Score reduction per day|1|0
score_AllScoreRules|Score Rules in All Schemes|1|0
score_backToGrouping|Back to Scheme|1|0
score_period_allTime|All time|1|0
score_period_annually|Annually|1|0
score_period_quarterly|Quarterly|1|0
score_period_monthly|Monthly|1|0
score_cappingPeriodType|Score Limit Reset Period|1|0
score_cappingResetMonth|Limit Reset Month|1|0
score_cappingResetDay|Limit Reset Day|1|0
score_RulesInGroup|Rules in Scheme|1|0
score_Rule|Rule|1|0
score_Rules|Rules|1|0
score_score|Score|1|0
score_awards_based_on_emtity_field|Weight score based on a profile|1|0
score_profile_for_score|Profile to use for weighting|1|0
score_points_per|Score weighting for activity|1|0
relayware_module|Module|1|0
relayware_site|Site|1|0
Sys_AssociateCatalog|Associate with a Catalog|1|0
required|Required|1|0
sys_Select_Values|Select Values|1|0
trngPersonCertification_personID|Person|1|0
trngPersonCertification_expiredDate|Expired Date|1|0
trngPersonCertification_expiryDate|Expiry Date|1|0
Incentive_Statement_S|Spent points - confirmed orders|1|0
viewDetails|View details|1|0
Sys_msDynamicsID|MS Dynamics ID|1|0
sys_nav_Detailed_Error_Report|Detailed Error Report|1|0
sys_salesforceID|Salesforce ID|1|0
product_msDynamicsID|MS Dynamics ID|1|0
ShowActive|Show Active|1|0
ShowInactive|Show Inactive|1|0
elearning_Certification|Certification|1|0
elearning_certification_name|Certification Name|1|0
elearning_certificationFunctions|Certification Functions|1|0
elearning_certificationNotAutoPassedMessage|The certification could not be passed because a rule cannot be satisfied eg. the number of modules to pass is greater then the number of modules assigned to that certification OR the only moduleSet contained in the rule is Inactive|1|0
elearning_certificationID|Certification ID|1|0
elearning_CertificationProgress|Certification Progress|1|0
elearning_CertificationRule|Certification Rule|1|0
elearning_certificationRuleID|Certification Rule ID|1|0
elearning_CertificationStatus|Certification Status|1|0
elearning_CertificationSummary|Certification Summary|1|0
elearning_certificationType|Certification Type|1|0
elearning_confirmInactivateCertifications|Are you sure you want to inactivate the selected certifications?\n This will expire all person certifications with this certification.|1|0
elearning_NoModuleHaveBeenStartedForCertification|No modules have been started for this certification|1|0
elearning_personCertificationID|Person Certification ID|1|0
report_certification_Certification_Type|Certification Type|1|0
report_certification_CertificationCode|Certification Code|1|0
report_certification_CertificationTitle|Certification Title|1|0
report_certification_certificationType|Certification Type|1|0
selection_CertificationStatuses|Certification Statuses|1|0
sys_nav_Certification|Certification|1|0
sys_TrainingReportFunctions|Certification Report Functions|1|0
elearning_ActivateCertifications|Activate Certifications|1|0
elearning_availableCertifications|Available Certifications|1|0
elearning_CertificationsPassed|Certifications Passed|1|0
elearning_ConfirmActivateCertifications|Are you sure you want to activate the selected certifications?|1|0
elearning_InactivateCertifications|Inactivate Certifications|1|0
elearning_NoAvailableCertificationsToRegister|There are no further available certifications to register for|1|0
elearning_noCertificationsHaveBeenRegistered|No valid certifications have yet been registered|1|0
elearning_NoNewRulesToAddAsNoCertificationsExist|No new rules to add as no active certifications exist|1|0
elearning_numOfCertifications|Number of Certifications|1|0
elearning_registerCertificationFor|Register Certifications For|1|0
elearning_TheFollowingCertsHaveBeenRegistered|The following certifications have successfully been registered|1|0
selection_Certifications|Certifications|1|0
sys_nav_certifications|Certifications|1|0
crm_ID|[[getSetting('connector.type')]] ID|1|0
elearning_modules_AssociatedQuiz|Associated Quiz|1|0
oppPleaseSelectAProduct|Please select a product|1|0
order_Catalog|Catalog|1|0
PartnerCloud|Relayware|1|0
notAuthorisedToUseUsernameAndPassword|You are not authorized to use Username and Password on this site|1|0
saml_identity_provider_saved|Identity provider saved|1|0
SAML_IDPDetails|Identity Provider Details|1|0
saml_SAMLIdentityProviders_title|Identity Providers|1|0
saml_identityProviderName|Identity Provider|1|0
saml_identityProviderEntityID|Entity ID|1|0
saml_authnRequestURL|Authentication Request URL|1|0
SAML_uniqueIdentifier_RWSide|Unique Identifying Relayware Field|1|0
SAML_uniqueIdentifier_Assertion|Unique Identifying Assertion Field (if not NameID)|1|0
saml_allowAutoCreate|Allow Autocreation of People|1|0
saml_creationOrganisationSelectionMethod|Method to determine location of new people|1|0
SAML_SelectLocOrg|All people created at set location|1|0
SAML_DetectLocOrg|Location determined from assertion data|1|0
saml_selectedOrganisation|Selected Organization|1|0
saml_selectedLocation|Selected Location|1|0
SAML_Location_uniqueIdentifier_RWSide|Unique Location Identififying Field on Relayware |1|0
SAML_location_uniqueIdentifier_Assertion|Unique Location Identifying Field in Assertion|1|0
SAML_RelaywareField|Relayware Field|1|0
SAML_MappingMethod|Mapping Method|1|0
SAML_Value|Assertion Field or Value|1|0
SAML_usergroupControl|Usergroup Control|1|0
SAML_assertionAttributeToControlUsergroups|Assertion Attribute Containing Usergroups|1|0
SAML_ControlledUsergroups|Usergroups Controlled by Identity Provider|1|0
SAML_StartingUsergroups|Default Usergroups for Newly Created People|1|0
SAML_StartingUsergroupCountries|Default Country Rights for Newly Created People|1|0
saml_allowAddUsergroups|Allow Identity Provider to Control Usergroups|1|0
saml_allowControlLanguageRights|Allow Identity Provider to Control Country Rights|1|0
SAML_assertionAttributeToControlCountryRights|Assertion Attribute Containing Country Rights|1|0
SAML_FromAssertion|Use assertion data|1|0
SAML_HardCoded|Use a set value|1|0
saml_cantAddDuplicateIDP|Can't create a duplicate identity provider. An identity provider with this EntityID already exists.|1|0
SAML_IDP_areYouSureYouWantToDelete|Are you sure that you want to delete this identity provider?|1|0
SAML_singleSignOnFailed|Unfortunately you are not authorized to access this site|1|0
SAML_OnlyWithOneMethod|Autostart of single sign on only possible if there is a single login method|1|0
SAML_ServiceProviderForSites|Type of Site To Use Identity Provider for|1|0
SAML_onCreationPortalApprovalStatus|Approval Status on Creation|1|0
SAML_authorisedSites|Sites Identity Provider is Authorized to Authenticate|1|0
OAuth2_serverName|Authorization Server Name|1|0
OAuth2_client_id|Client ID|1|0
OAuth2_client_secret|Client Secret|1|0
OAuth2_token_url|Token Endpoint URL|1|0
OAuth2_data_url|Data Endpoint URL|1|0
OAuth2_server_uid|Server Unique Identifier|1|0
OAuth2_relayware_uid|Relayware Unique Identifier|1|0
OAuth2_create_server|Create New Authorization Server|1|0
OAuth2_edit_server|Manage OAuth2 Authorization Servers|1|0
OAuth2_create_client|Create New Client Application|1|0
OAuth2_edit_client|Manage OAuth2 Client Application|1|0
OAuth2_requestURL|Request URL|1|0
OAuth2_authorise_url|Authorize URL|1|0
OAuth2_auth_server_saved|Authorization Server saved|1|0
OAuth2_auth_server_not_saved|Authorization Server NOT saved - duplicate name|1|0
OAuth2_server_areYouSureYouWantToDelete|Are you sure you want to delete this authorization server?|1|0
OAuth2_client_areYouSureYouWantToDelete|Are you sure you want to delete this client application?|1|0
OAuth2_serverNameHint|Friendly name displayed on login button|1|0
OAuth2_clientIDHint|ID of Relayware as provided by server|1|0
OAuth2_clientSecretHint|Secret provided by this server|1|0
OAuth2_requestHint|URL to request authorization token from (leave blank if not required)|1|0
OAuth2_authHint|URL user is directed to for login|1|0
OAuth2_tokenHint|URL to request access token from|1|0
OAuth2_dataHint|Endpoint for user data (leave blank if same as token URL)|1|0
OAuth2_scopeHint|Scope of data Authorization Server allows access to (if required)|1|0
OAuth2_serverUidHint|User's Relayware field to match with the server field|1|0
OAuth2_relayUidHint|User's server field to match with the Relayware field|1|0
OAuth2_serverTypeHint|Allow this server to provide a login for Relayware or for portals|1|0
OAuth2_portalListHint|Portals this server can provide a login for|1|0
OAuth2_activeHint|Active or deactive this server for logins on allowed sites|1|0
OAuth2_serversTitle|OAuth2 Authorization Server Settings|1|0
OAuth2_editAllScreenText|This page can be used to set which 3rd party authorization servers can authorize users to login to Relayware|1|0
OAuth2_editNew|This page can be used to register a new 3rd party authorization server that can be used to login to Relayware|1|0
OAuth2_editExisting|This page can be used to edit this 3rd party authorization server for logging into Relayware|1|0
OAuth2_editNewClient|This form can be used to register a new 3rd party application to use Relayware as an OAuth2 authentication server|1|0
OAuth2_editExistingClient|This page can be used to edit a new 3rd party application that uses Relayware as an OAuth2 authentication server|1|0
OAuth2_siteType|Type of site to use Authorization Server for|1|0
OAuth2_manageServers|Manage Authorization Servers|1|0
OAuth2_manageClientsTitle|Manage Client Applications|1|0
OAuth2_setStatus|Set status|1|0
OAuth2_scope|Scope|1|0
OAuth2_token_Prefix|Access token prefix|1|0
OAuth2_prefixHint|Prefix to be used with an access token when requesting data (default is 'bearer')|1|0
OAuth2_redirectionURL|Redirection URL|1|0
OAuth2_redirectionURLis|Redirection URL(s)|1|0
OAuth2_noRedirectURL|To get a redirection URL, select a site on the form field|1|0
OAuth2_createSecretLink|Create Secret Link|1|0
OAuth2_secretLink|Secret Link|1|0
OAuth2_clientName|Client Name|1|0
OAuth2_manageClients|Manage Existing Client Applications|1|0
OAuth2_editAllClientsScreenText|This page can be used to set which 3rd party client applications are authorized to use Relayware as an OAuth2 authorization server|1|0
OAuth2_client_deleted|Client application deleted|1|0
OAuth2_scopesUsedByClient|Scopes used by client|1|0
OAuth2_scopeEditTitle|OAuth Authorization Server Scope(Endpoint) Settings|1|0
OAuth2_oneScopeEditTitle|Edit Authorization Server Scope|1|0
OAuth2_scopeEditDescription|This page can be used to view the available endpoints and edit non built-in endpoints for scopes (data to send to clients)|1|0
portal|Portal|1|0
SSO_sitewideSettings|Manage SAML and OAuth2 SSO Login Behaviour Per Site|1|0
server_deleted|Server deleted|1|0
OAuth2_manageScopes|Manage Existing Endpoints (Scopes)|1|0
OAuth2_scopeName|Scope name|1|0
OAuth2_clientSecretText|A client secret link is a <strong>one time use</strong> URL leading to a page containing the information, including client secret, needed for a third party to configure their client application.|1|0
OAuth2_clientSecretDescription|Please send the following URL to the third party. <strong>This link will expire</strong> after the first view|1|0
OAuth2_createClientSecretLink|Create 'Generate Client Secret' link|1|0
OAuth2_SSOError|Single Sign On Error|1|0
OAuth2_selectSiteForUrl|Select a portal site(s) to view redirection URL(s)|1|0
OAuth2_clientScopesHint|Sets of data this client is permitted to request|1|0
OAuth2_assertionAttributeToControlUsergroups|Assertion Attribute Containing Usergroups|1|0
OAuth2_ControlledUsergroups|Usergroups Controlled by Authorization Server|1|0
OAuth2_StartingUsergroups|Default Usergroups for Newly Created People|1|0
OAuth2_StartingUsergroupCountries|Default Country Rights for Newly Created People|1|0
OAuth2_allowAddUsergroups|Allow Authorization Server to Control Usergroups|1|0
OAuth2_allowControlLanguageRights|Allow authorization Server to Control Country Rights|1|0
OAuth2_assertionAttributeToControlCountryRights|Assertion Attribute Containing Country Rights|1|0
OAuth2_autocreate|Allow Autocreation of People|1|0
OAuth2_autocreateHint|Autocreate or begin registration for users attempting to SSO in when they are not registered on Relayware|1|0
OAuth2_registerUser|Launch Registration|1|0
OAuth2_autoregisterHint|Launch user registration form instead of automatically creating user|1|0
OAuth2_autocreationOptions|User Autocreation Options|1|0
OAuth2_SelectLocOrg|All people created at set location|1|0
OAuth2_DetectLocOrg|Location determined from assertion data|1|0
OAuth2_creationOrganisationSelectionMethod|Method to determine location of new people|1|0
OAuth2_selectedOrganisation|Selected Organization|1|0
OAuth2_selectedLocation|Selected Location|1|0
OAuth2_Location_uniqueIdentifier_RWSide|Unique Location Identififying Field on Relayware |1|0
OAuth2_location_uniqueIdentifier_Assertion|Unique Location Identifying Field in Assertion|1|0
OAuth2_RelaywareField|Relayware Field|1|0
OAuth2_MappingMethod|Mapping Method|1|0
OAuth2_Value|Assertion Field or Value|1|0
OAuth2_usergroupControl|Usergroup Control|1|0
OAuth2_assertionAttributeToControlUsergroups|Assertion Attribute Containing Usergroups|1|0
OAuth2_ControlledUsergroups|Usergroups Controlled by Authorization Server|1|0
OAuth2_StartingUsergroups|Default Usergroups for Newly Created People|1|0
OAuth2_StartingUsergroupCountries|Default Country Rights for Newly Created People|1|0
OAuth2_allowAddUsergroups|Allow Authorization Server to Control Usergroups|1|0
OAuth2_allowControlLanguageRights|Allow Authorization Server to Control Country Rights|1|0
OAuth2_assertionAttributeToControlCountryRights|Assertion Attribute Containing Country Rights|1|0
OAuth2_countryControl|Country Control|1|0
OAuth2_onCreationPortalApprovalStatus|Approval Status on Creation|1|0
OAuth2_authorisedSites|Authorization Server Name|1|0
add_server|Add server|1|0
add_client|Add client|1|0
active|Active|1|0
inactive|Inactive|1|0
edit_servers|Edit servers|1|0
edit_clients|Edit clients|1|0
loginBehaviorUpdated|Login behavior updated|1|0
sso_allowStandardLogin|Allow Login with Username and Password|1|0
sso_allowSSO|Allow Single Sign On|1|0
sso_beginSSOAutomatically|Begin Single Sign On Automatically|1|0
sso_SAMLSPMetadata|Service Provider Metadata|1|0
Sign_in_with|Sign In With|1|0
hostingGroup|Group:|1|0
fund_accountManagersGroups|Account Managers Groups|1|0
fund_accountValidationMessage|At least one Account Manager or Account Manager Group should be selected|1|0
insufficientFundRequestsPermission|You do not have sufficient permissions to view fund requests|1|0
SAML_certificateToUse|Certificate to use|1|0
SAML_isPermittedToInitiateSAML|Is Identity Provider permitted to initiate SAML|1|0
Sys_SearchOrgSitePeople|Search Companies and People|1|0
Sys_CompanyType|Company Type|1|0
Sys_CompanyName|Company Name|1|0
hostingGroup|Group|1|0
Sys_OrganisationUID|Organization ID|1|0
Sys_LocationUID|Location ID|1|0
Sys_PersonUID|Person ID|1|0
lead_leadNoRights|The requested lead does not exist or you do not have rights to it|1|0
addcontactform_locationNotInListContactAdmin|If your Location does not appear in the list, please contact: <a href="mailto:[[invitedByEmailAddress]]">[[invitedByEmailAddress]]</a>|1|0
ResendPW_EmailAddress|Email Address|1|0
ResendPW_UserName|Username|1|0
Rank|Rank|1|0
scoreCalculatedFrom|Score Calculated From|1|0
scoreCalculatedTo|Score Calculated To|1|0
NextLimitResetDate|Next Limit Reset Date|1|0
ManualAdjustment|Manual Adjustment|1|0
Custom|Custom|1|0
AccountApprovalStatusIs|This accounts approval status is:|1|0
approval_allAccountTypes|All|1|0
approval_filterByAccountStatus|Location Status|1|0
AccountApprovalStatusSetTo|This accounts approval status is now:|1|0
ApprovalsOrgLevel|Approvals should be done at organisation level, please go to Accounts > New Account Approvals|1|0
ApprovalsLocLevel|Approvals should be done at location level, please go to Accounts > New Account Approvals|1|0
translations_text|Show all Translations|1|0
banner_add|Add Banner|1|0
Banner_Builder_title|Title|1|0
Banner_Builder_bannerID|Banner ID|1|0
Banner_Builder_targetUrl|URL|1|0
Banner_Builder_Delete|Delete|1|0
banner_backgroundFile_noteText|Banner background images must be added to the media group 'Banner'|1|0
locator_showMore|More Results|1|0
partner_organisation|Vendor Organization|1|0
partner_organisation|Vendor Location|1|0
vendor_location|Vendor Location|1|0
SAML_misconfigured_isSecure_setAsSecure|This site appears to be set as secure but is being accessed via a non-secure URL. It is likely that the isSecure setting has been misconfigured. SAML endpoints are unlikely to function correctly.|1|0
SAML_misconfigured_isSecure_setAsNonSecure|This site appears to be set as non-secure but is being accessed via a secure URL. It is likely that the isSecure setting has been misconfigured. SAML endpoints are unlikely to function correctly.|1|0
SAML_misconfigured_fixInstructions|This can be corrected by an administrator in Configure > Administration > Edit Site Structure.|1|0
lead_leadProgressStatus|Lead Progress Status|1|0
fund_ClosedActivities|Closed Activities|1|0
campaign_endDateMustBeGreaterThanStart|End date must be set after start date|1|0
report_emailDef_active|Active|1|0
Record_Updated|Record Updated|1|0
lead_created|Lead Created Date|1|0
lead_lastUpdated|Last Updated Date|1|0
lead_leadTypeStatusID|Lead Status|1|0
lead_distiContactPersonID|Distributor Contact|1|0
lead_convertedPersonID|Lead Contact|1|0
lead_progressID|Lead Progress|1|0
email_Filters|Choose Rules|1|0
email_select_filter|Select Rule|1|0
email_CurrentFilters|Current Rules|1|0
sys_connector_fieldMappingTooltip|Please note and match Field Types when defining Field Mappings|1|0
connector_ReportMappingIssue|Report Mapping Issue|1|0
connector_pleaseReportMappingIssues|Please report Mapping Issues and Suggestions to the Relayware Product Team|1|0
connector_rwEnvironment|Relayware Environment|1|0
connector_issue|Issue|1|0
connector_issueReporter|Reporter|1|0
newBusinessPlan_addNew|Add New|1|0
lead_delete|&nbsp;|1|0
sys_connector_report_DownloadAllErrors|Download all errors|1|0
filterByKeywordValue|Filter by Keyword|1|0
sys_connector_report_ID|ID|1|0
sys_connector_report_queueID|Queue ID|1|0
sys_connector_report_connectorType|Connector Type|1|0
sys_connector_report_object|Object|1|0
sys_connector_report_errorCount|Error Count|1|0
sys_connector_report_mostRecentError|Most Recent Error|1|0
sys_connector_report_field|Field|1|0
sys_connector_report_message|Message|1|0
sys_connector_report_detail|Detail|1|0
sys_connector_report_objectID|Object ID|1|0
sys_connector_report_modifiedDateUTC|Date Modified|1|0
sys_connector_report_synchAttempt|Synchronization Attempt|1|0
sys_connector_report_isDeleted|Deleted?|1|0
sys_connector_report_message|Message|1|0
sys_connector_report_field|Field|1|0
sys_connector_report_value|Value|1|0
sys_connector_report_statusCode|Status Code|1|0
sys_connector_report_created|Date Created|1|0
sys_connector_report_connectorResponseID|Connector Response ID|1|0
sys_connector_report_directionSign|Direction|1|0
sys_connector_report_object|Object|1|0
lead_section_contacts|Contacts|1|0
lead_section_updateHistory|Update History|1|0
mla_advanced_search|Advanced Search|1|0
mediaSearch_Filter_Media_Groups|Media Groups|1|0
mla_please_select|Please select|1|0
mla_Go|Go|1|0
mla_clear|Clear|1|0
mla_resource_library_results|Search returned|1|0
mla_resource_library_results_found| results|1|0
mla_List|List|1|0
mla_Grid|Grid|1|0
MLA_backToTop|Back to the top|1|0
mla_FileID|File ID|1|0
mla_title|Title|1|0
mla_Image|Image|1|0
mla_Media_Group|Media Group|1|0
mla_Resource_Type|Type|1|0
mla_File_Extension|File Extension|1|0
mla_File_Size|File Size|1|0
mla_Description|Description|1|0
mla_Language|Language|1|0
mla_Source|Source|1|0
mla_Format|Format|1|0
mla_Publish_Date|Publish Date|1|0
mla_Created_Date|Created|1|0
mla_Last_Updated|Last Updated|1|0
mla_kb|kb|1|0
MLALoadMore|Load More|1|0
MLAConsiderFilters|Consider using filters to narrow down your results.|1|0
mla_First_Page|First Page|1|0
mla_Previous_Page|Previous Page|1|0
mla_Next_Page|Next Page|1|0
mla_Last_Page|Last Page|1|0
mla_FileExtension|File Extension|1|0
relayware_filterByModule|Filter activities on module|1|0
whereClauseValidationStatus|Synch Rules Status|1|0
sys_NoneSelected|None Selected|1|0
connectorMapping_defaultWhenNull|Set Default Value When Null|1|0
connectorMapping_defaultImportValue|Default Import Value|1|0
connectorMapping_defaultExportValue|Default Export Value|1|0
sys_connector_resynch_record|Record|1|0
sys_connector_resynch_synched_successfully|synched successfully|1|0
sys_connector_resynch_failed|failed. Error:|1|0
sys_connector_resynch_synchInProgressNow|Queue sync is in progress now. Please try again later.|1|0
connectorMapping_defaultExportValue|Default Export Value|1|0
ext_NotSelected|Not Selected|1|0
sys_connector_validation_countryNotPassed|Country not passed in or is invalid|1|0
phr_error_required_data_missing|Required field is missing|1|0