<!--- �Relayware. All Rights Reserved 2014 --->

<!--- 
File name:			createLanguageStructure.cfm	
Author:				SWJ
Date started:		2004-10-13
	
Description:		creates a language structure that can be used to get the relay
					languageID from the key

					called from templates\setapplicationcomponents
					
USAGE: 				<cfif not structKeyExists(application, "languageIDLookupStr")>
						<cfinclude template="/translation/createLanguageStructure.cfm">
					</cfif>
					
					<cfset lang = "fr">
					<cfset langISO = "2">

					<cfoutput>
						languageID for FR = #application.languageIDLookupStr[lang]#
						languageISOCode for FR = #application.languageISOCodeLookupStr[langISO]#
					</cfoutput>

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->

<CFQUERY NAME="getLanguages" DATASOURCE="#application.siteDataSource#">		
	Select Language, languageid, buttonid, ISOCode, localLanguageName 
	from Language order by languageID
</CFQUERY>

<cfscript>
	application.languageIDLookupStr = structNew();
	application.languageISOCodeLookupStr = structNew();
	application.languageLookupFromISOCodeStr = structNew();
	application.languageLookupFromIDStr = structNew();
	application.languageIDLookupStr = structNew();
</cfscript>

<cfoutput query="getLanguages">
	<cfset application.languageIDLookupStr[ISOCode] = languageid>
	<cfset application.languageISOCodeLookupStr[languageid] = ISOCode>
	<cfset application.languageLookupFromISOCodeStr[ISOCode] = language>
	<cfset application.languageLookupFromIDStr[languageid] = language>
	<cfset application.languageIDLookupFRomNameStr[language] = languageid>
</cfoutput>




