<!--- �Relayware. All Rights Reserved 2014
2015/12/02 SB Bootstrap. Removed tables. Cleaned up the classes etc.
--->
<cf_translate>
	<!--- get translation country grousp --->
	<cfquery name="getTCGroups" datasource="#application.sitedatasource#">
		select		tcg.*, l.language
		from		translationCountryGroup tcg inner join
					language l on l.languageID = tcg.languageID
		order by	tcg.tcGroupName
	</cfquery>
	<div id="translationCountryGroup">
		<a class="btn btn-primary margin-bottom-15" href="translationCountryGroupAction.cfm?action=new">
			phr_addEditTranslationCountryGroup
		</a>
	</div>
	<cfif isDefined("Message") and message is not "">
	<p>
		<cfoutput>#application.com.security.sanitiseHTML(message)#</cfoutput>
	</p>
	</cfif>
	<table class="table table-hover table-striped">
		<tr bgcolor="#000099">
			<th>phr_translationCountryGroup</th>
			<th>phr_tcgLanguage</th>
		</tr>
	<cfoutput query="getTCGroups">
		<tr>
			<td><a href="translationCountryGroupAction.cfm?TCGroupID=#translationCountryGroupID#&action=edit">#htmleditformat(tcGroupName)#</a></td>
			<td>#htmleditformat(language)#</td>
		</tr>
	</cfoutput>
	</table>
</cf_translate>