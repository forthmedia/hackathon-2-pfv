<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			addUpdatePhrases.cfm	
Author:				NJH
Date started:		2006-07-29
	
Purpose:	Add/Update phrase translations based on parameters passed.

Usage: 			

Parameters:
			frmPhraseFile - .txt file of phrases to load. Should be in \translation\LoadPhrases dir
			frmAddPhrases - add/ don't add new phrases
			frmUpdatePhrases - overwrite/merge/ignore conflicts with existing phrases

Amendment History:


Date (DD-MMM-YYYY)	Initials 	What was changed
2007-09-11		wab     re-wrote the processing as a cfc function so that could be called from remote admin.  required a small amount of changing to the display code as well 
WAB 2008/07/09  Change to loadPhrasesFromFile so that can load phrases from userfiles
01-12-2012	IH	Apply workaround to query of queries data conversion bug

Possible enhancements:

--->


<cf_translate>
<cfif not isDefined("frmPhraseFile")>
	<cfoutput>phraseFile must be passed as a parameter</cfoutput>
	<CF_ABORT>
</cfif>

<cf_title>Load Phrases</cf_title>

<cfscript>
// create an instance of the translation component
// well that's what someone else thinks this does... but it doesn't
// it creates a pointer to the instance that is already resident in memory
myphrase = application.com.relaytranslations;
myphrase.dataSource = application.sitedataSource;
myphrase.userID = request.relayCurrentUser.usergroupid;
myphrase.entityTypeID = "0";
myphrase.languageID = "0";

// set this primaryKey ID for the THIS scope
</cfscript>


<cfset sysPhrases  = application.com.relayTranslations.getPhraseFileQuery (phraseFileName = frmPhraseFile)>


<cfset existingPhrases = "">
<cfset phrasesAdded = "">
<cfset phrasesOverwritten = "">

<!--- We're merging phrases that user has selected for being merged--->
<cfif StructKeyExists(form,"frmSubmitMerge")>
	<cfset phrasesMergedCount = 0>
	
	<cfset request.relayFormDisplayStyle = "HTML-table">
	<cf_relayFormDisplay>
		<tr><th align="left">phr_sys_translations_PhrasesMerged</th></tr>
		<cfloop index="indexValue" from="1" to="#frmMergePhraseCount#">
			<cfif isDefined("frmMerge#indexValue#") and Evaluate("frmMerge#indexValue#") neq "">
				<cfset phrTextIDToMerge = form["frmMerge#indexValue#"]>				
				<cfquery name="getPhrase" dbtype="query">
					select * from sysPhrases 
					where CAST(phraseTextID AS varchar) = '#listfirst(phrTextIDToMerge,"-")#'
						and CAST(languageid AS varchar) = '#listgetat(phrTextIDToMerge,2,"-")#'
						and CAST(countryid AS varchar) = '#listgetat(phrTextIDToMerge,3,"-")#'
				</cfquery>
				
				<cfset mergeResult= application.com.relayTranslations.addNewPhraseAndTranslationv2(mode="overwrite",				
						phraseTextID = getPhrase.phrasetextid,
						phraseText = getPhrase.phrasetext,
						countryID = getPhrase.countryID,
						languageid = getPhrase.languageid)>

				<cf_relayFormElementDisplay relayFormElementType="HTML" currentValue="#mergeresult.message#" label="" spanCols="yes">
				<cfset phrasesMergedCount = #phrasesMergedCount# + 1>
			</cfif>
		</cfloop>
		<cfif phrasesMergedCount eq 0>
			<cf_relayFormElementDisplay relayFormElementType="HTML" currentValue="phr_sys_translations_NoPhrasesMerged" label="" spanCols="yes">
		</cfif>
	</cf_relayFormDisplay>
	
<!--- Initial adding or updating of phrases. If user wants to merge, we get the list of phrases that conflict 
	  and present them to the user. They may then choose the phrases to merge and submit them. --->

<cfelse>

	<cfif frmAddPhrases or frmUpdatePhrases neq "ignore">

		<cfset addedResult = application.com.relaytranslations.addPhrasesFromLoadFile (phraseFileName = frmPhraseFile, addMode="#frmAddPhrases#", updateMode="#frmUpdatePhrases#" )>

		<cfset request.relayFormDisplayStyle = "HTML-table">
		<cfoutput>
		<cf_relayFormDisplay>
			<!--- If phrases were added --->
			<cfif not ListLen(addedResult.added) eq 0>
				<tr><th align="left">phr_sys_translations_PhrasesAdded</th></tr>
				<cfloop index="phraseTextID" list="#addedResult.added#">
					<cf_relayFormElementDisplay relayFormElementType="HTML" currentValue="#phraseTextID#" label="" spanCols="yes">
				</cfloop>
			<cfelseif isDefined("frmAddPhrases") and frmAddPhrases>
				<cf_relayFormElementDisplay relayFormElementType="HTML" currentValue="phr_sys_translations_NoPhrasesAdded" label="" spanCols="yes">
			</cfif>
			
			<!--- If phrases were overwritten --->
			<cfif not ListLen(addedResult.updated) eq 0>
				<tr><th align="left">phr_sys_translations_PhrasesOverwritten</th></tr>
				<cfloop index="phraseTextID" list="#addedResult.updated#">
					<cf_relayFormElementDisplay relayFormElementType="HTML" currentValue="#phraseTextID#" label="" spanCols="yes">
				</cfloop>
			<cfelseif isDefined("frmUpdatePhrases") and frmUpdatePhrases eq "overwrite">
				<cf_relayFormElementDisplay relayFormElementType="HTML" currentValue="phr_sys_translations_NoPhrasesOverwritten" label="" spanCols="yes">
			</cfif>
		</cf_relayFormDisplay>

		<!--- If we have phrases to merge --->
		<cfif not ListLen(addedResult.notupdated) eq 0 and frmUpdatePhrases eq "merge">		
			<cf_relayFormDisplay>
			<cfform name="phraseForm" action="addUpdatePhrases.cfm" method="post">
				<cfset count=0>
				<tr><th align="left" colspan="4">Phrase Merges</th></tr>
				<tr><th>phr_sys_translations_ExistingPhraseTextIDs</th><th>phr_sys_translations_MergeNew</th><th>phr_sys_translations_NewPhrase</th><th>phr_sys_translations_OldPhrase</th></tr>
				<cfloop index="phraseTextID" list="#addedResult.notupdated#">
				
					<cfquery name="newPhrases" dbType="query">
						select phraseText from sysPhrases 
						where CAST(phraseTextID AS varchar) = '#listfirst(phraseTextID,"-")#'
							and CAST(languageid AS varchar) = '#listgetat(phraseTextID,2,"-")#'
							and CAST(countryid AS varchar) = '#listgetat(phraseTextID,3,"-")#'
					</cfquery>
					
					<cfquery name="oldPhrases" datasource="#application.sitedatasource#">
						select phraseText from Phrases where phraseID = 
							(select phraseid from phraseList 
									WHERE phraseTextID =  <cf_queryparam value="#listfirst(phraseTextID,"-")#" CFSQLTYPE="CF_SQL_VARCHAR" > )
							and languageid =  <cf_queryparam value="#listgetat(phraseTextID,2,"-")#" CFSQLTYPE="CF_SQL_INTEGER" > 
							and countryid =  <cf_queryparam value="#listgetat(phraseTextID,3,"-")#" CFSQLTYPE="CF_SQL_INTEGER" > 

						<!--- and phraseText not like '#newPhrases.phraseText#' --->
					</cfquery>
					
					<cfif newPhrases.phraseText neq oldPhrases.phraseText>
						<cfset count=#count#+1>
						<tr>
							<td>#phraseTextID#</td>
							<td align="center"><cf_relayFormElement relayFormElementType="checkbox" fieldname="frmMerge#count#" currentValue="" label="" value="#phraseTextID#"></td>
							<td>#htmleditformat(newPhrases.phraseText)#</td>
							<td>#htmleditformat(oldPhrases.phraseText)#</td>
						</tr>
					</cfif>
				</cfloop>
				<cf_relayFormElement relayFormElementType="hidden" fieldName="frmMergePhraseCount" currentValue="#ListLen(addedResult.notupdated)#">
				<cf_relayFormElement relayFormElementType="hidden" fieldName="frmPhraseFile" currentValue="#frmPhraseFile#">
				<tr>
					<td colspan="4" align="center">
						<cf_relayFormElement relayFormElementType="button" fieldname="frmCancelMerge" label="" currentValue="phr_Cancel" onClick="history.go(-1);">
						<cf_relayFormElement relayFormElementType="submit" fieldname="frmSubmitMerge" label="" currentValue="phr_sys_translations_Merge">
					</td>
				</tr>
			</cfform>
			</cf_relayFormDisplay>	
		</cfif>

		</cfoutput>
	</cfif>
</cfif>



</cf_translate>




