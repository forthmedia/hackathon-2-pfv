<!--- �Relayware. All Rights Reserved 2014 --->
<!---

called from editPhrase.cfm (or elsewhere)

Creates a table for editing the phrase passed in as frmPhraseID
WAB 08 June 2005
WAB 2005-10-18 added a bit of javascript to catch new translations with country not selected
WAB 2009/02/11 Mod so that understands frmPhraseID in the form phraseTextID_Entity_EntityID
WAB 2009/02/11 	Mod so that when no existing translation for a phrase, it gives the add new translation option
WAB 2009/03/05 (raised as LID 2037 in april) problem with a previous change 2009/02/11, not passing entityTypeID through properly.
NJH 2009/05/08	Replaced query with function call
WAB 2009/04   changes related to new translation code, use of common code
NYB 2010-07-14 LHID's 3650 & 3125 - changed logic around Default Language.  See comments below for full info.
WAB 2012-11-18 CASE 432273 Added DONOTTRANSLATE tags to prevent translation of phrase within phrase
NJH	2013/05/30 Case 435229 - if you don't have language rights, then don't show the update button and don't display the word 'Edit'
YMA 2013-05-31 Case 435198 Stop translation fetching from timing out
NJH 2014-01-07 	Case 435198 - remove white space and move the 'DoNotTranslate' comments outside the main loop when displaying the translation editor in an effort to speed up the loading.
WAB 2015-01-12 CASE 443384 DoNotTranslate comments need to be in a cfoutput tag
 --->

	<cfsetting requestTimeOut = "600" enablecfoutputonly="true"> <!--- 2013-05-31	YMA	Case 435198 Stop translation fetching from timing out --->

	<cfparam name="frmphraseid">

	<!--- changes made by DJH 2000-12-18 to accept entitytype and entityid ---->
	<cfparam name="entityid" default="0">
	<cfparam name="entitytypeid" default="0">
	<cfparam name="entitytype" default="">

	<cfif isdefined("entitytype") and entitytype is not "">
		<cfset entitytypeid = application.entityTypeID [entitytype]>
	</cfif>

	<!--- WAB 2009/02/11 Added understanding of Entity Phrases --->
	<CFSET struct = reFindNoCase("([a-zA-Z0-9_]+)_([a-zA-Z0-9]+)_([0-9]+)$",frmPhraseID,1,"true")>
	<CFSET LenArray = struct.len>
	<CFSET PositionArray = struct.pos>
	<cfif PositionArray[1] is not 0>
		<cfset entityID = mid(frmPhraseID,positionArray[4],LenArray[4])>
		<cfset entityTypeID = mid(frmPhraseID,positionArray[3],LenArray[3])>
		<cfset phraseTextID = mid(frmPhraseID,positionArray[2],LenArray[2])>
		<cfset entityType = application.entityType[entityTypeID].tablename>
	<cfelseif not isNumeric(frmphraseid)>
		<cfset phraseTextID = frmphraseid>
	</cfif>

	<cfif isNumeric(frmPhraseId)>
		<cfset AllTranslations = application.com.relayTranslations.getAllTranslatedLanguagesForAPhrase(phraseID=frmPhraseId,returnPhraseText = true)>
		<cfset AllTranslationsWithoutPhraseText = application.com.relayTranslations.getAllTranslatedLanguagesForAPhrase(phraseID=frmPhraseId,returnPhraseText = false)>
	<cfelse>
		<cfset AllTranslations = application.com.relayTranslations.getAllTranslatedLanguagesForAPhrase(phraseTextID=phraseTextID,entityType=entitytype,entityID=entityID,returnPhraseText = true)>
		<cfset AllTranslationsWithoutPhraseText = application.com.relayTranslations.getAllTranslatedLanguagesForAPhrase(phraseTextID=phraseTextID,entityType=entitytype,entityID=entityID,returnPhraseText = false)>
	</cfif>

<!--- 	<CFQUERY NAME="AllTranslations" DATASOURCE="#application.SiteDataSource#">
		SELECT
			pl.phraseTextID,
			pl.entityID,
			pl.entityTypeID,
			p.Phraseid,
			p.Ident,
			p.PhraseText AS Phrase,
			p.LanguageID,
			p.countryid,
			p.defaultForThisCountry,
			(select countrydescription from country where countryid = p.countryid union select 'Any Country' where p.countryid = 0) as countryName,
			case when entityID <> 0 then (select entityName from schemaTable where entityTypeid = pl.entityTypeid ) else '' end as entityType,
			l.LanguageID,
			isNull(l.Language,'No Language') AS Language,
			convert(varchar,p.languageid) + '_' + convert(varchar,p.countryid) as CountryAndLanguage,
			<cfif request.relaycurrentuser.LanguageEditRights is not "" >
			Case when p.languageid in (0,#request.relaycurrentuser.LanguageEditRights#) and p.countryid in (#request.relaycurrentuser.CountryList#,0) then 1 else 0 end
			<cfelse>
			0
			</cfif>
			as hasRightsToEditThisCountryLanguageCombination
		FROM
			phraselist as pl
				left outer Join
			(phrases as p
				left Join
				language l
			on p.languageid=l.LanguageID
			) on pl.phraseid = p.phraseid
		WHERE
			<CFIF isNumeric(frmphraseid)>
				p.Phraseid=#frmphraseid#
			<CFELSE>
				pl.PhraseTextid='#phraseTextID#'
				and pl.entitytypeid = #entitytypeid#
				and pl.entityid = #entityid#
			</cfif>

		order by
			P.COUNTRYID,l.languageID
	</CFQUERY> --->

	<CFSET currentPhraseID = AllTranslations.phraseid>

	<cfif AllTranslations.recordCount is 0>
		<!--- Phrase does not exist, phrasetextid will be as passed in --->


<cfelse>
			<!--- WAB 2009/02/12  If we have found a record then can update phrasetextID, which might have been blank if a phraseIF was passed
			and WAB 2009/03/05 also need to update entityTypeID etc
		--->
		<cfset phraseTextID = AllTranslations.phraseTextID>
		<cfset entityID = AllTranslations.entityID>
		<cfset entityTypeID = AllTranslations.entityTypeID>
		<!--- NJH 2009/05/08 replaced with below<cfset entityType = AllTranslations.entityType> --->
		<cfset entityType = application.entityType[AllTranslations.entityTypeID].tableName>
	</cfif>

	<!--- <CFIF AllTranslations.recordCount is 0 and not isNumeric(frmPhraseID)>
		<CFSET frmPhraseTextID = frmPhraseID>
		<cfinclude template="/translation/addnewphrase.cfm">
		<CF_ABORT>
	</cfif> --->

<cfoutput>
<script>
	function checkForm (form)	{
		newTranslationObj = document.getElementById('newTranslation');
		// if there is a new translation then there must be a language and country selected

		if (newTranslationObj.value.length != 0) {
			if (document.getElementById('phrLanguageID').value == '' ||  document.getElementById('phrCountryID').value == '') {
				alert ('You must choose a Language and Country')
				return;
			}
		}
		form.submit() ;
	}
</script>
</cfoutput>


<CFOUTPUT>
<!--- WAB 2009/02/12  Mods to this area so that bottom bit shows for a previously untranslated  phrase --->
<FORM ACTION="" METHOD="post" NAME="Edit_Record">


	<CF_INPUT TYPE="hidden" NAME="frmphraseid" VALUE="#AllTranslations.PhraseID#">
<!--- 	<input type="hidden" name="currentCountriesAndLanguages" value=",#valuelist(phrase.CountryAndLanguage)#"> --->
</cfoutput>

<cfset hasLanguageRights = true>
<cfif request.relaycurrentuser.LanguageEditRights eq 0 or request.relaycurrentuser.LanguageEditRights eq "">
	<cfset hasLanguageRights = false>
</cfif>

<cfoutput>
	<div class="row">
		<div class="col-xs-12">
			<h4>
				<b><cfif hasLanguageRights>Edit</cfif> Phrase:</b> #htmleditformat(phraseTextID)#<cfif entitytypeid is not 0>(Entity Type: #htmleditformat(entitytype)#, Entity ID: #htmleditformat(entityid)#)</cfif>
			</h4>
		</div>
	</div>
</cfoutput>


<CFIF AllTranslations.recordCount is not 0>

	<!--- <cfif hasLanguageRights>
		<cfoutput>

			Edit this phrase in the boxes below

		</cfoutput>
	</cfif> --->

	<cfoutput><!-- StartDoNotTranslate --></cfoutput>
	<cfloop QUERY="AllTranslations" group="countryid">
			<!--- WAB this bit tries to get the text box a reasonable size for the text
				listlen gets a rough idea of the number of new lines
			 --->
			<CFSET RowNum = min(max(2,listlen(phraseText,chr(10))+1),10)>
			<CFIF len(phraseText) gt 240 >
				<CFSET RowNum = max(10,rowNum)>
			<CFelseIF len(phraseText) gt 60 >
				<CFSET RowNum = max(5,rowNum)>
			</CFIF>

		<cfoutput>
		<div class="row">
		<div class="col-xs-12">
				<B>#htmlEditFormat(countryDescription)#</B>
			</div>
		</div>
		 </cfoutput>
		<cfloop>
			<!---  WAB 2009/09/22 how many translations for this country--->
			<cfset numberOfTranslationsForThisCountry = listValueCount(valuelist(AllTranslations.countryid),countryid)>

		<cfoutput>
			<div class="row">
				<div class="col-xs-4">
				   	<B>#htmlEditFormat(Language)# </B>
						<cfif numberOfTranslationsForThisCountry gt 1 and defaultForThisCountry is 1>
							(Default)
						</cfif>
				</div>

				<div class="col-xs-8">
					<TEXTAREA NAME="PhrUpd_#PhraseTextID#_#entityTypeID#_#entityID#_#languageid#_#countryid#"class="form-control" ROWS="#RowNum#" COLS="80" WRAP VIRTUAL <cfif not userHasEditRights>readonly</cfif>>#htmleditFormat(PhraseText)#</TEXTAREA>
					<cfif userHasEditRights>
						<cfif not (countryid is 0 and defaultForThisCountry is 1)>
							<div class="checkbox">
								<label>
									<CF_INPUT type="checkbox"	name="phrDelete" value="#PhraseTextID#_#entityTypeID#_#entityID#-#languageid#-#countryid#">Delete
								</label>
							</div>
						<cfelse>
							<cfif AllTranslations.recordcount gt 1><BR>Cannot delete the default translation</cfif><!--- not worth showing this message unless there are other items with the delete checkbox displayed --->
						</cfif>
					</cfif>
				</div>
			</div>
		 </cfoutput>
		</cfloop>


	</cfloop>

	<cfoutput><!-- EndDoNotTranslate --></cfoutput>

	<!--- NYB 2010-07-14 LHID's 3650 & 3125 - START - replaced all of:
			<cfset firstPass = true>
			<CFOUTPUT QUERY="AllTranslations" group="countryid">
				<cfset numberOfTranslationsForThisCountry = listValueCount(valuelist(AllTranslations.countryid),countryid)>
				<cfif numberOfTranslationsForThisCountry GT 1>
					<cfif firstpass>
						<TR>
						   <TD VALIGN=TOP align="right" class="label">
								Default Languages
							</Td>
					 	</TR>
						<cfset firstPass = false>
					</cfif>
					<TR>
					   <TD VALIGN=TOP align="right">
							#countryDescription#
						</Td>
						<td>
							<select name="phrDefaultLang_#PhraseTextID#_#entityTypeID#_#entityID#_#countryid#">
								<cfif countryid is not 0>
									<option value = "0">No default
								</cfif>
							<cfoutput>
								<cfif userHasEditRights>
									<option value = "#languageid#" <cfif defaultForThisCountry is 1>selected</cfif>>#language#
								</cfif>
							</cfoutput>
							</select>
						</TD>
				 	</TR>
			 	</cfif>
			</CFOUTPUT>
	--- : with the following
	New Logic:  1. 	doesn't display countries if you don't have rights to any of the languages allocated to them -
					ie, no more empty dropdowns;
				2. 	doesn't display a default language option for a country when you don't have edit rights for
					that countries current default language;
				3.	have removed the <if countryid not equal 0> around the No Default option so you are no
					longer forced to set the default for Any Country when if doesn't have a default language and
					have replaced this with
					<if countryid = 0 and countryid 0 has a default language set and the current user has rights to that language>
					only then don't show the No Default option to the dropdown.
	--->

	<cfquery name="getDefaultCountries" dbtype="query">
		select countryid from AllTranslations
		group by countryid
		having count(languageid) > 1 and sum(userHasEditRights) > 0
	</cfquery>

	<cfset firstPass = true>
	<cfloop QUERY="AllTranslations" group="countryid">
		<cfset countryFound = listFind(valuelist(getDefaultCountries.countryid),countryid)>
		<cfif countryFound GT 0>
			<cfset hasRightsToDefaultLanguage = "true">
			<cfset allowNoDefault = "true">
			<cfoutput>
				<cfif defaultforthiscountry eq 1>
					<cfif userhaseditrights eq 0>
						<cfset hasRightsToDefaultLanguage = "false">
					<cfelse>
						<cfif countryid eq 0>
							<cfset allowNoDefault = "false">
						</cfif>
					</cfif>
				</cfif>
			</cfoutput>

			<cfif hasRightsToDefaultLanguage>
				<cfif firstpass>
					<cfoutput>
					<div class="row">
					   <div class="col-xs-12">
							Default Languages
						</div>
				 	</div>
				 	</cfoutput>
					<cfset firstPass = false>
				</cfif>

				<cfoutput>
				<div class="row">
				   <div class="col-xs-4">
						<h2>#htmleditformat(countryDescription)#</h2>
					</div>
					<div class="col-xs-8">
						<select name="phrDefaultLang_#PhraseTextID#_#entityTypeID#_#entityID#_#countryid#" class="form-control">
							<cfif allowNoDefault>
								<option value = "0">No default
							</cfif>
						<cfloop>
							<cfif userHasEditRights>
								<option value = "#languageid#" <cfif defaultForThisCountry is 1>selected</cfif> class="form-control">#htmleditformat(language)#
							</cfif>
						</cfloop>
						</select>
					</div>
			 	</div>
			 	</cfoutput>
			 </cfif>
		 </cfif>
	</cfloop>
	<!--- NYB 2010-07-14 LHID's 3650 & 3125 - END --->
	<cfoutput><HR></cfoutput>
</cfif>

<cfoutput>
<cfif hasLanguageRights>
<div class="row">
	<div class="col-xs-12">
		<h4><b>Add new translation to:</b> #htmleditformat(phraseTextID)#</h4>
	</div>
</div>
<div class="row">
	<div class="col-xs-4">
		<cfset allLanguagesAndCountriesStruct = application.com.relayTranslations.getAllCountriesAndLanguagesForUser()>

							<cf_twoSelectsRelatedByExclusion
								query1 = #allLanguagesAndCountriesStruct.languages#
								query2 = #allLanguagesAndCountriesStruct.countries#
								excludeQuery = 	#AllTranslationsWithoutPhraseText#
								excludeQueryKey1 = 	"languageID"
								excludeQueryKey2 = 	"countryID"
								name1 = "phrLanguageID"
								name2 = "phrCountryID"
								id1 = "phrLanguageID"
								id2 = "phrCountryID"
								onchangefunction = ""
								selected1 = "#request.relaycurrentuser.languageid#"
								selected2 = "0"
								HTMLBetween = "<BR>"
								>
	</div>
	<div class="col-xs-8">
		<TEXTAREA ID="newTranslation" NAME="PhrUpd_#PhraseTextID#_#entityTypeID#_#entityID#" ROWS="10" COLS="80" WRAP VIRTUAL class="form-control"></TEXTAREA>
	</div>
</div>

<div class="row">
	<div class="col-xs-12">
		<input type="button" name="add_update" value="Add / Update Translations" onClick="javascript:checkForm (this.form)" class="btn btn-primary">
	</div>
</div>
</cfif>
</FORM>
</cfoutput>

<cfsetting enablecfoutputonly="false">