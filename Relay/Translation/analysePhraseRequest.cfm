<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 

Analyse translations in the translationrequest table

WAB 2014-09-16 fix an error that must have slipped in ages ago (a rogue htmleditformat).  Guess this template isn't used much
WAB 2016-11-29 During PROD2016-2837 (getting this page to work) Reformatted
 --->

<CFPARAM name="frmAppIdentifier">
<CFPARAM name="frmPageIdentifier" default="">

<cfparam name = "frmLanguageID" default = "#request.relaycurrentuser.languageid#">
<cfparam name = "frmcountryID" default = "#request.relaycurrentuser.content.showforcountryid#">

 
<cfif isDefined("deleteLoggedPhrases") >
	<cfquery name = "deleteLoggedPhrases" dataSource = "#application.sitedatasource#">
	delete
	from phraseRequest
	where appIdentifier =  <cf_queryparam value="#frmAppIdentifier#" CFSQLTYPE="CF_SQL_VARCHAR" > 
	<cfif frmPageIdentifier is not "">
	and pageIdentifier  in ( <cf_queryparam value="#frmPageIdentifier#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )
	</cfif>
	</cfquery>
</cfif>

<cfquery name = "getPhrases" dataSource = "#application.sitedatasource#">
select distinct phraseTextID, entityid, entitytypeid , pageIdentifier
from phraseRequest
where appIdentifier =  <cf_queryparam value="#frmAppIdentifier#" CFSQLTYPE="CF_SQL_VARCHAR" > 
<cfif frmPageIdentifier is not "">
and pageIdentifier  in ( <cf_queryparam value="#frmPageIdentifier#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )
</cfif>
order by pageIdentifier

</cfquery>

<cfoutput><P><FONT size = "4">Analysis of Phrases in <B>#htmleditformat(frmAppIdentifier)# </b> </font></P></cfoutput>


<!--- drop down of languages/countries --->

<cfquery name="getLanguages" datasource="#application.siteDataSource#">
Select languageid as dataValue, language as displayValue from Language where isocode  in ( <cf_queryparam value="#request.currentSite.livelanguages#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> ) order by language
</cfquery>

<cfquery name="getCountries" datasource="#application.siteDataSource#">
Select countryid as dataValue, countrydescription as displayValue from country where isocode is not null order by countrydescription
</cfquery>



<cfoutput>
<cfform action="analysephraserequest.cfm" method="POST" name="lang">
	<table>
<cfset request.relayFormDisplayStyle = "HTML-table">
<cf_relayFormDisplay>
	<cf_relayFormElementDisplay relayFormElementType="select" query = "#getLanguages#" currentvalue="#frmLanguageID#" fieldname="frmLanguageID"  valueAlign="left" label="Language">
	<cf_relayFormElementDisplay relayFormElementType="select" query = "#getCountries#" currentvalue="#frmCountryID#" fieldname="frmCountryID"  valueAlign="left" label="Country">		
	<CF_INPUT type="hidden" name = "frmAppIdentifier" value = "#frmAppIdentifier#">
	<CF_INPUT type="hidden" name = "frmPageIdentifier" value = "#frmPageIdentifier#">
	<cf_relayFormElementDisplay relayFormElementType="submit" fieldname = "" currentValue = "Change Language/Country" valuealign = center label="">		
	<cf_relayFormElementDisplay relayFormElementType="submit" fieldname = "deleteLoggedPhrases" currentValue = "Clear out Log" valuealign = center label="">		
	<cf_relayFormElementDisplay relayFormElementType="submit" fieldname = "" currentValue = "Refresh" valuealign = center label="">		
</cf_relayFormDisplay>	
	</table>


</cfform>	

</cfoutput>



<cf_Translate doAnalysis = "yes" showTranslationLink = true  LogRequests="false" language = "#frmlanguageid#" countryid = "#frmcountryid#" exactmatch = "false" evaluateVariables=false executeRelaytags=false>

<cfoutput><TABLE></cfoutput>
<cfoutput query="getPhrases" group="pageIdentifier">
	<TR><TD  colspan="2"><FONT size = "4" color="green"><B>Page: #htmleditformat(pageIdentifier)#</b></font></td></tr>

	<cfoutput>
		<tr>
				<TD ><FONT size = "1" color="green">#htmleditformat(phraseTextID)#<cfif EntityTypeID is not "0">_#application.entityType[entityTypeID].tablename#_#EntityID#</cfif></td>
				<td><P>phr_#htmleditformat(phraseTextID)#<cfif EntityTypeID is not "0">_#application.entityType[entityTypeID].tablename#_#EntityID#</cfif></P></td>
		</tr>
	</cfoutput>
</cfoutput>
<cfoutput></table></cfoutput>
</cf_Translate>





 
 
 
 
 
 
