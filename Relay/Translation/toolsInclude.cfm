<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 	*************************************************************
			Check to see if frmPersonID is a contentEditor

	2016-02-03	WAB Mass replace of displayValidValues using lists to use queries (may have touched unused files)
			--->
<CFIF isDefined ("caller.isContentEditor.recordcount")>
	<CFIF caller.isContentEditor.recordcount GT 0>
		<CFSET showContentTools = "yes">
	</CFIF>
<cfelse>
	<CFQUERY NAME="isContentEditor" DATASOURCE="#application.sitedatasource#">
					SELECT 1
					FROM
					rightsgroup r
					INNER JOIN (SELECT userGroupID FROM usergroup
						WHERE Name = 'Content Editor') u
					ON r.userGroupID = u.userGroupID AND r.personid=#request.relayCurrentUser.personid#
				</CFQUERY>
	<CFIF isContentEditor.recordcount GT 0>
		<CFSET showContentTools = "yes">
	</CFIF>
</cfif>

<CFIF isDefined("showContentTools") and showContentTools eq "yes">
<!--- what languages does this person have rights to  --->
	<CFQUERY NAME="FindLanguageRights" DATASOURCE="#application.siteDataSource#">
		SELECT l.Languageid
		FROM Language as l, booleanflagdata as bfd, flag as f, flaggroup as fg
		WHERE fg.flaggrouptextid='LanguageRights'
		and 	fg.flaggroupid = f.flaggroupid
		and 	f.name=l.language
		and bfd.flagid= f.flagid
		and bfd.entityid = #request.relayCurrentUser.personid#
	</CFQUERY>

	 	<CFQUERY NAME="getLanguage" DATASOURCE="#application.siteDataSource#">
		Select LanguageID,  [Language]
		from Language where languageID  in ( <cf_queryparam value="#request.currentSite.liveLanguageIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		order by language
		</CFQUERY>

 	<CFIF isDefined("frmShowLanguageID") >	<!--- checks whether we have come from this tools page --->
		<CFIF frmShowLanguageID is not "">
			<cfset application.com.globalFunctions.cfcookie(NAME="defaultLanguageID", VALUE="#frmShowLanguageID#")>
		</cfif>

		<CFIF isDefined("frmSetTranslatorCookie")>
			<!--- swithches the T on --->
			<cfset application.com.globalFunctions.cfcookie(NAME="translator", VALUE="#valuelist(findlanguagerights.languageid)#")>
			<cfset application.com.relayCurrentUser.updateContentSettings(ShowTranslationLinks = true)>
		<CFELSEIF isDefined("cookie.Translator")>
			<!--- swithches the T off --->
			<cfset application.com.globalFunctions.cfcookie(NAME="translator", VALUE="", EXPIRES="NOW", SECURE="Yes")>
			<cfset application.com.relayCurrentUser.updateContentSettings(ShowTranslationLinks = false)>
		</cfif>

	</cfif>

	<CFSET currentLanguageID = "">
	<CFIF isDefined ("cookie.defaultLanguageID")>
		<CFSET currentLanguageID = cookie.defaultLanguageID>
	</cfif>

<CFOUTPUT>

	<script type="text/javascript">
			function addPhrases ()  {
					newWindow = window.open ('#request.currentSite.httpProtocol##jsStringFormat(listFirst(Internal))#/translation/addMissingPhrase.cfm', 'AddPhrases','location=0,resizable=1,scrollbars=1,menu=0,top=50,left=50,height=300,width=600')
					newWindow.focus()
					}
	</script>


<CFPARAM NAME="TableBGCOLOR" DEFAULT="DADADA">
<CFPARAM NAME="tableBGimage" DEFAULT="1pixel-grey-DADADA.gif">
<CFPARAM NAME="TDClass" DEFAULT="subNavMenuItem">
<TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0" ALIGN="right" BACKGROUND="/images/background/#tableBGimage#">
<TBODY>
<TR>
	<TD CLASS="subNavMenuItem">
		<form method="post" name="translationForm">
			<CF_INPUT TYPE="hidden" NAME="frmScriptName" VALUE="#SCRIPT_NAME#">
			<CF_INPUT TYPE="hidden" NAME="frmQueryString" VALUE="#request.query_string#">
			<BR>

			<DIV CLASS="subNavMenuItem">
			<cf_displayValidValues
				formFieldName="frmShowLanguageID"
				validValues="#getlanguage#"
				dataValueColumn = "languageid"
				displayValueColumn = "Language"
				nullText="Language"
				currentValue="#currentLanguageID#"
				onchange = "this.form.submit()"
				>
			</DIV>

			<CFIF isDefined("request.relayCurrentUser.personid") and listLen(cookie.user,"-") is 3 and FindLanguageRights.recordcount GT 0>
				<INPUT onclick = "this.form.submit()" type="checkBox" name="frmSetTranslatorCookie" value="1"	<CFIF isDefined("cookie.Translator") and cookie.Translator is not "">checked</CFIF> >
				Edit Content
			</cfif>
			<BR><A HREF="javaScript:addPhrases()" class="subNavMenuItem">Add Phrases</A>

		</form>
	</TD>
</TR>
</TBODY>
</TABLE></CFOUTPUT>
</CFIF>
