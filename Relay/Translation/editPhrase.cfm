<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 

editPhrase.cfm

Piece of code for editing and updating phrases

Called from the T links and from language_search.cfm



Mods
WAB 280300 allow frmphraseid to be the phrasetextid or the phraseid
DJH 2000-12-18 to accept entitytype and entityid
WAB 2001-01-09  major modification so that code will handle country scope on phrases
	took the opportunity to make all the updates be done by CF_get Phrase
	Not using CF_get Phrase to get the phrases yet though

WAB 2004-10-26   language 0 not being shown	

WAB June 05   Major mods to get rid of languageid 0 and do some rationalisation
WAB 2009-02-11  Added showAddNewPhrase param so that bottom of form can be hidden
		Also mod to doTranslation.js
 --->

<!--- WAB 2009-02-11 added --->
<cfparam name="showAddNewPhrase" default="true">

<!--- does the updates --->
<cf_includeonce template = "\translation\updatePhrase.cfm">

<cf_head>
    <cf_title>Translations</cf_title>
</cf_head>

<CFIF ISDEFINED("frmphraseid")>
	<cfinclude template = "editPhraseInsert.cfm">
</CFIF>

<cfif showAddNewPhrase>
	<cfinclude template = "addPhraseInsert.cfm">
</cfif>