<!--- �Relayware. All Rights Reserved 2014 --->
<!---

WAB June 05  Major mods to translation process

WAB 2006-02-20  Added exactMatch parameter
SB  2015-11-13  Bootstrap and removed tables
--->
<cfparam name = "exactmatch" default = "0">
<cfparam name = "searchPhrasetextID" default = "0">  <!--- WAB 2009/09/23 added parameter --->


<!--- Search form --->
<div class="row">
	<div class="col-xs-12 col-sm-6">
		<div class="grey-box-top">
			<FORM ACTION="/translation/Language_Result.cfm" METHOD="post">
				<cf_relayFormDisplay>
				<h2>Search Language Dictionary</h2>
				<cfoutput>
					<cf_relayFormElementDisplay type="html" label="Enter a phrase or word to search for in the content language dictionary" required="true">
					<!--- <div class="form-group">
						<label class="required">Enter a phrase or word to search for in the content language dictionary</label> --->
						<INPUT TYPE="hidden" NAME="LanguageChoice" VALUE="">
						<!-- StartDoNotTranslate --><INPUT TYPE="Text" NAME="description" SIZE="50" class="form-control" required="true" VALUE="<cfif isDefined("description")>#htmleditformat(description)#</cfif>"><!-- EndDoNotTranslate -->
					<!--- </div> --->
					</cf_relayFormElementDisplay>
					<div class="checkbox">
						<label>
							<INPUT TYPE="checkbox" NAME="exactMatch" value="1" <cfif exactmatch is 1>checked</cfif>> Exact Match
						</label>
					</div>
					<div class="checkbox">
						<label>
							<INPUT TYPE="checkbox" NAME="searchPhrasetextID" value="1" <cfif searchPhrasetextID is 1>checked</cfif>> Just search Phrase Identifier
						</label>
					</div>
				</cfoutput>
				<INPUT TYPE="submit" value="Search For Phrase" class="btn btn-primary">
				</cf_relayFormDisplay>
			</FORM>
		</div>
	</div>
	<!--- <CFIF findNoCase("language_result.cfm",script_name) eq 0> --->
		<div class="col-xs-12 col-sm-6">
			<div class="grey-box-top">
				<h2>Add a New Phrase</h2>
				<cfset startAddPhraseOpen = false>
				<CFINCLUDE TEMPLATE="addPhraseInsert.cfm">
			</div>
		</div>
	<!--- </CFIF> --->
</div>