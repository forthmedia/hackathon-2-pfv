<!--- �Relayware. All Rights Reserved 2014 --->

<cfparam name="entityName" type="string"> <!--- name of the table --->
<cfparam name="entityIDColumn" type="string"> <!--- name of the column holding the entityID --->
<cfparam name="phrasetextid" type="string"> <!--- values such as title, description, heading, summary, etc, etc --->
<cfparam name="columnToTranslate" type="string"> <!--- column in the table to get the phraseText from --->

<cfquery name="getEntityTypeID" datasource="#application.siteDataSource#">
	select entityTypeID from schemaTable where entityName =  <cf_queryparam value="#entityName#" CFSQLTYPE="CF_SQL_VARCHAR" > 
</cfquery>

<cfquery name="getEntityDetails" datasource="#application.siteDataSource#">
	select #columnToTranslate# as phrasetext, #entityIDColumn# as entityID from #entityName#
</cfquery>


<cfloop query="getEntityDetails">
	<cfset InsertTranslation = application.com.relayTranslations.addNewPhraseAndTranslationV2(
		phrasetextid = phrasetextid,
		entitytypeid = getEntityTypeID.entityTypeID,
		entityid = entityID,
		phrasetext = phrasetext,
		languageid = 1,
		countryid = 0,
		updateCluster="false"
	)>
</cfloop>