<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 

called from editPhrase.cfm

Creates a table for adding a new phrase

WAB 08 June 2005 

 --->



	 <CFQUERY NAME="FindAdditionRights" DATASOURCE="#application.SiteDataSource#">
		SELECT 
			1
		FROM 
			booleanflagdata as bfd, flag as f
		WHERE 
			f.flagtextid='AddNewPhraseRight'
			and bfd.flagid= f.flagid
			and bfd.entityid = #request.relayCurrentUser.personid#
	</CFQUERY>
 
 
<CFIF FindAdditionRights.recordCOunt is not 0>

<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
<FORM ACTION="" METHOD="post" NAME="Add_Record">



			<CFQUERY NAME="getLanguages" DATASOURCE="#application.SiteDataSource#">
				select languageid as datavalue, language as displayValue from language where languageid in (#request.relaycurrentuser.LanguageEditRights#)
			</CFQUERY>

		<cfoutput>
		<TR><TD></TD><TD>
			Add an entirely new phrase that does not appear in the database below.
		<tr>
			<td>
							Enter an Identifier <INPUT TYPE="TEXT" name="newtextid" value="<CFIF isDefined("newPhraseTextID")>#htmleditformat(newPhraseTextID)#</cfif>"><BR>

			</TD>
		</TR>

		<TR><TD valign=top>	<CF_displayValidValues
						formFieldName = "phrLanguageID" 
							validValues="#getLanguages#" 
							currentValue = "#request.relaycurrentuser.languageid#"
							showNull = true
							NullText = "Select a Language"
					> Any Country
					<input type="hidden" name = "phrcountryid" value = "0"> 
		</td>
			<td>
				<TEXTAREA NAME="Newphrase" ROWS="10" COLS="80" WRAP VIRTUAL></TEXTAREA>
			</TD>
		</TR>

		
		<tr>
			<TD colspan=2 align="center"><INPUT TYPE="submit" VALUE="Add New Phrase" NAME="AddNewPhrase"></td>
		</tr>
		</cfoutput>

</FORM>
</TABLE>
</CFIF>

