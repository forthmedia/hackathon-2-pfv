<!--- �Relayware. All Rights Reserved 2014 --->

<!---  

called by the javascript function  doNewTranslation(phrasetextid) 
adds a blank record into the phrase tables so that it can then be edited with the regular code

Mods:
WAB 090600 added check for existence of phrase before trying to add it again

--->


<!--- changes made by DJH 2000-12-18 to accept entitytype and entityid ---->

<cfparam name="entityid" default="0">
<cfparam name="entitytypeid" default="0">

<cfif isdefined("entitytype")>
	<cfquery name="getentitytypeid" DATASOURCE="#application.SiteDataSource#">
		select entitytypeid
		from schematable
		where entityname =  <cf_queryparam value="#entitytype#" CFSQLTYPE="CF_SQL_VARCHAR" > 
	</cfquery>
	<cfif getentitytypeid.recordcount is 1>
		<cfset entitytypeid = getentitytypeid.entitytypeid>	
	</cfif>	
</cfif>


<!--- changes made by WAB 2001-01-09 so that can accept a phrasetextID of the form
	description_element_123
	Note that this isn't strictly a phraseTextID, but please forgive me!


 --->

<cfif listLen(frmPhraseTextID,"_") GTE 3 >

	
		<!--- use a regular expression to extract the stuff --->
		<CFSET struct = reFindNoCase("([a-zA-Z0-9_]*)_([0-9]*)_([0-9]*)$",trim(frmPhraseTextID),1,"true")>					
		<CFSET positionArray = struct.pos>
		<CFSET LenArray = struct.len>					

		<!--- 	WAB 2001-02-26  added check that something was found
			ought to be some more error handling if nothing is found - but there isn't 
		--->
		<cfif positionArray[1] is not 0>
			<cfset entityTypeID = mid(frmPhraseTextID,positionArray[3],LenArray[3])>
			<cfset entityID = mid(frmPhraseTextID,positionArray[4],LenArray[4])>
			<cfset frmphraseTextID = mid(frmPhraseTextID,positionArray[2],LenArray[2])>
		</cfif>

		
</cfif>



<!--- check phrase does not already exist 
	(should only exist if someone has clicked on the link twice without refreshing the page)

 --->
	<CFQUERY NAME="checkPhrase" DATASOURCE="#application.SiteDataSource#">
		SELECT 
			PhraseList.phraseid,
			Phrases.phraseID as checkForTranslation
		FROM 
			PhraseList left outer join phrases on phraseList.phraseid = phrases.phraseid
		where
			1=1
			and phraseTextID =  <cf_queryparam value="#frmPhraseTextID#" CFSQLTYPE="CF_SQL_VARCHAR" > 
			and entitytypeid =  <cf_queryparam value="#entitytypeid#" CFSQLTYPE="CF_SQL_INTEGER" > 
			and entityid =  <cf_queryparam value="#entityid#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</CFQUERY>			
		


<CFIF checkPhrase.recordCount is 0 or checkPhrase.checkForTranslation is "" >   <!--- this will check for either no phrase, or an item in phraseList but nothing in Phrases --->
	<!--- add new phrase --->
	<CFTRANSACTION>
		
		<CFIF checkPhrase.recordCount is 0>
			<CFQUERY NAME="addphrase" DATASOURCE="#application.SiteDataSource#">
				INSERT INTO PhraseList
				(
					Phraseid,
					PhraseTextID, 
					createdby, 
					created,
					entitytypeid,
					entityid
				)
				SELECT 
					max(PHRASEID)+1, 
					<cf_queryparam value="#frmPhraseTextID#" CFSQLTYPE="CF_SQL_VARCHAR" >, 
					<cf_queryparam value="#request.relayCurrentUser.personid#" CFSQLTYPE="cf_sql_integer" >, 					
					getDate(),
					<cf_queryparam value="#entitytypeid#" CFSQLTYPE="CF_SQL_INTEGER" >,
					<cf_queryparam value="#entityid#" CFSQLTYPE="CF_SQL_INTEGER" >
				FROM 
					PhraseList
			</CFQUERY>
		
			<CFQUERY NAME="GetNewID" DATASOURCE="#application.SiteDataSource#">
			SELECT max(PhraseID) AS NewID from PhraseList
			</CFQUERY>
		
			<CFSET frmphraseid = #GetNewID.NewID#>
		<CFELSE>
			<CFSET frmphraseid = #checkPhrase.PhraseID#>
	
		</cfif>
	
			<!---  Jan 01 WAB altered to add with languageID = 0 
					and then changed back to 1
					needs to go back to 0 when all phrases use new code		
			--->
		<CFQUERY NAME="AddNewTranslation" DATASOURCE="#application.SiteDataSource#">
		INSERT INTO Phrases
		(
			Phraseid,
			PhraseText,
			LanguageID,
			CountryID,
			createdby, 
			created, 
			lastupdatedby, 
			lastUpdated
		)
		VALUES 
		(
			<cf_queryparam value="#frmphraseid#" CFSQLTYPE="CF_SQL_INTEGER" >,
			<cf_queryparam value="#frmPhraseTextID#" CFSQLTYPE="CF_SQL_VARCHAR" >,
			1,
			0, 
			<cf_queryparam value="#request.relayCurrentUser.personid#" CFSQLTYPE="cf_sql_integer" >, 
			getDate(), 
			<cf_queryparam value="#request.relayCurrentUser.personid#" CFSQLTYPE="cf_sql_integer" >, 
			getDate()
		)
		</CFQUERY>

	</CFTRANSACTION>

<CFELSE>

	<!--- phrase already exists, just set the id --->	
	
	<!--- just check that there is an entry in the phrases table 
		(causes errors if not--->
	<CFSET frmphraseid = #checkPhrase.PhraseID#>
		
</cfif>

<cfinclude template="/translation/editPhrase.cfm">


