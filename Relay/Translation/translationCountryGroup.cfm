<!--- �Relayware. All Rights Reserved 2014 --->
<cf_translate>

<!--- add phrase to country groups and list countries and whether they had that phrase or not. --->
<cfif isDefined("form.translationCountryGroupID")>
	<cfset doOverWrite = false>
	<cfif isDefined("form.overwriteold") and overwriteold is "true">
		<cfset doOverWrite = true>
	</cfif>
	<cfset phraseInsertDetails = application.com.relayTranslations.addTranslationToCountryGroup('#form.newphrase#','#form.newtextid#',form.translationCountryGroupID,doOverWrite)>
	<cfoutput>
	<cfif arrayLen(phraseInsertDetails) gt 0>
		<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
			<TR>
				<TH>phr_country</TH>
				<TH>phr_phraseAlreadyExists</TH>
				<TH>phr_tcgStatus</TH>
			</TR>
			<cfloop from="1" to="#arrayLen(phraseInsertDetails)#" index="pIndex">
				<TR>
					<TD>#phraseInsertDetails[pIndex].countryDescription#</TD>
					<TD><cfif phraseInsertDetails[pIndex].phraseExists eq 0>phr_no<cfelse>phr_yes</cfif></TD>
					<TD>#phraseInsertDetails[pIndex].message#</TD>
				</TR>
			</cfloop>
		</TABLE>
	</cfif>
	</cfoutput>
</cfif>

<cf_title>Translate for Country Group</cf_title>

<cf_head>    
	<script type="text/javascript">
		function verifyForm(task) {
			var form = document.Add_Record;
			var msg = "";
			var moveOn = false;

			// NJH 2008/10/02 BUG Fix Lexmark Issue 1051 check if checkbox is checked rather than checking the value
			if (form.overwriteold.checked) {
				if (confirm("You have selected to overwrite any existing phrases for this id. Are you sure you want to do this?")) {
					moveOn = true;
				}
			} else {
				moveOn = true;
			}
			if (moveOn) {
				if (form.newtextid.value == "") {
					msg += "* phr_identifier\n";
				}
				if (form.translationCountryGroupID.value == "" && form.translationCountryGroupID.selectedIndex == 0) {
					msg += "* phr_countryGroup\n";
				}
				if (form.Newphrase.value == "") {
					msg += "* phr_translatedText\n";
				}
				if (msg != "") {
					alert("\nphr_provideInfoForTranslation\n\n" + msg);
				} else {
					form.submit();
				}
			}
		}
	</SCRIPT>
</cf_head>

<CFSET FRMPHRASEID=0>
<cfset newPhraseTextID = "">

<CFSET FRMPHRASEID=-1>
<CFSET newPhraseTextID=#REReplace(LEFT(newPhraseTextID,50),"[^_0-9a-zA-Z]","","ALL")#>	
<CFQUERY NAME="FindAdditionRights" DATASOURCE="#application.SiteDataSource#">
	SELECT 
		1
	FROM 
		booleanflagdata as bfd, flag as f
	WHERE 
		f.flagtextid='AddNewPhraseRight'
		and bfd.flagid= f.flagid
		and bfd.entityid = #request.relayCurrentUser.personid#
</CFQUERY>
 
 
<CFIF FindAdditionRights.recordCOunt is not 0>

<cfquery name="getTranslationCountryGroups" DATASOURCE="#application.sitedatasource#">
	select		tcg.tcGroupName+' ('+l.language+')' as displayName, tcg.translationCountryGroupID
	from		translationCountryGroup tcg inner join
				language l on l.languageID = tcg.languageID
	order by	tcg.tcGroupName
</cfquery>

<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
	<TR class="Submenu">
		<TD ALIGN="LEFT" VALIGN="TOP" class="Submenu">&nbsp;Add Phrase to Country Group</TD>
	</TR>
</TABLE>

<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
<FORM ACTION="" METHOD="post" NAME="Add_Record">
	<cfoutput>
	<TR>
		<TD colspan="2">phr_translationCountryGroupPageIntroText</TD>
	</TR>
	<tr>
		<td valign="top">
			<table>
				<tr>
					<td>phr_identifier:</td>
					<td><INPUT TYPE="TEXT" name="newtextid" value="<CFIF isDefined("newPhraseTextID")>#htmleditformat(newPhraseTextID)#</cfif>"></td>
				</tr>
				<tr>
					<td>phr_selectCountryGroup:</td>
					<td>
						<select name="translationCountryGroupID">
							<option value="">--- phr_selectCountryGroup ---</option>
							<cfloop query="getTranslationCountryGroups">
								<option value="#translationCountryGroupID#">#htmleditformat(displayName)#</option>
							</cfloop>
						</select>
					</td>
				</tr>
				<tr>
					<td>phr_OverwriteExistingPhrases:</td>
					<td><input type="checkbox" value="true" name="overwriteold"></td>
				</tr>
			</table>
		</td>
		<td>
			phr_addNewTranslation:<br>
			<TEXTAREA NAME="Newphrase" ROWS="10" COLS="80" WRAP VIRTUAL></TEXTAREA>
		</TD>
	</TR>
	<tr>
		<TD colspan=2 align="center"><INPUT TYPE="button" VALUE="phr_addNewPhrase" NAME="AddNewPhrase" onClick="verifyForm();"></td>
	</tr>
	</cfoutput>
</FORM>
</TABLE>
</CFIF>



</cf_translate>

