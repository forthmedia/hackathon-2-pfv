<!--- �Relayware. All Rights Reserved 2014 --->

<!---
		15-FEB-2011		MS		LID:5672: 8.3a "Populate from another phrase" throws an error message so updated query 'getPhrases' to search on 'phrasetext' along with 'phrasetextid'
 --->
<!--- *********************************************************************
	  we're searching for the phrase to populate from --->
<cfif isDefined('phraseTextID') eq "yes">
	<CFQUERY NAME="getPhrases" datasource="#application.siteDataSource#">
		select distinct pl.phraseID,phraseTextID
		from phraselist pl
			 left outer join phrases p ON p.phraseID = pl.phraseID
			where
				(phrasetextid  like  <cf_queryparam value="%#replace(phraseTextID," ","%","ALL")#%" CFSQLTYPE="CF_SQL_VARCHAR" >
				OR
				phrasetext  like  <cf_queryparam value="%#replace(phraseTextID," ","%","ALL")#%" CFSQLTYPE="CF_SQL_VARCHAR" >
				)
			and pl.phraseID  not in ( <cf_queryparam value="#phraseIDToPopulate#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	</CFQUERY>
</CFIF>

<!--- *********************************************************************
	  we're doing the update --->
<cfif isDefined("form.phraseIDToPopulate") and isDefined("form.phraseIDToPopulateFrom")>
	<CFQUERY NAME="updatePerson" datasource="#application.siteDataSource#">
		INSERT INTO [Phrases]
		([Languageid], [Phraseid], [CountryID], [PhraseText], [createdBy], [created], [lastUpdatedBy], [lastUpdated])
		SELECT [Languageid], <cf_queryparam value="#form.phraseIDToPopulate#" CFSQLTYPE="CF_SQL_INTEGER" >, [CountryID], [PhraseText], [createdBy], getDate(), [lastUpdatedBy], getDate()
			FROM [Phrases]
			where phraseid =  <cf_queryparam value="#form.phraseIDToPopulateFrom#" CFSQLTYPE="CF_SQL_INTEGER" >
			and languageID not in (select languageID FROM [Phrases]
		where phraseid =  <cf_queryparam value="#form.phraseIDToPopulate#" CFSQLTYPE="CF_SQL_INTEGER" > )
	</CFQUERY>
	<CFSET message = "Phrase updated">
</CFIF>


<CFIF isDefined("getPhrases") and getPhrases.recordCount gt 0 and not isDefined('form.frmMove')>
	<CFOUTPUT>
	<div class="Submenu row">
		<div class="col-xs-12 Submenu">
			<p>Populate phraseID #htmleditformat(phraseIDToPopulate)# from another Phrase</p>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-12">
			PhraseID #htmleditformat(phraseIDToPopulate)#
		</div>
	</div>
		</CFOUTPUT>
	<!--- *********************************************************************
	  we don't have any phraseIDs so we first need to search for them --->
	<CFFORM ACTION="populateFromAnotherPhrase.cfm" METHOD="POST" NAME="phraseForm">
<!--- 			<TR>
				<TD VALIGN="top" CLASS="label">Phrase</TD>
				<TD><CFINPUT TYPE="Text" NAME="phraseTextID" MESSAGE="You must put a valid personID" REQUIRED="Yes" SIZE="15"></TD>
			</TR>
			<TR>
				<TD>&nbsp;</TD>
				<TD><INPUT TYPE="submit" VALUE="Search"></TD>
			</TR>

			<TR>
				<TD VALIGN="top"><HR WIDTH="100%" SIZE="1" COLOR="steelblue"></TD>
			</TR>
 --->
		<div class="form-group">
			<label for="">Select the phrase you want to populate from:</label>
			<CFSELECT class="form-control" NAME="phraseIDToPopulateFrom"
		          SIZE="15"
		          MESSAGE="You must select an organisation"
		          QUERY="getPhrases"
		          VALUE="phraseID"
		          DISPLAY="phraseTextID"
		          REQUIRED="Yes"></CFSELECT>
		</div>
		<INPUT TYPE="submit" NAME="frmMove" VALUE="Submit" class="btn btn-primary">

		<CFOUTPUT>
			<CF_INPUT type="hidden" name="phraseIDToPopulate" value="#phraseIDToPopulate#">
			<CF_INPUT type="hidden" name="phraseTextID" value="#phraseTextID#">
		</CFOUTPUT>
	</CFFORM>
<cfelse>

	<cfset message = message>
	<cfset description = form.phraseTextID>
	<cflocation url="/translation/language_result.cfm?FRMPHRASEID=#phraseIDToPopulate#"addToken="false">
	<CF_ABORT>
	<cfinclude template="/translation/language_search.cfm">
	<CFSET FRMPHRASEID=phraseIDToPopulate>
	<cfinclude template="/translation/try.cfm">
</CFIF>






