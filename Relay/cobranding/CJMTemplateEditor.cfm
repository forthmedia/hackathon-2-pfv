<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			CJMTemplateEditor.cfm
Author:				YMA
Date started:		2014-12-09

Description:		Customize CJM Templates and print existing customizations.

Date (DD-MMM-YYYY)	Initials 	What was changed
Amendment History:


Date (DD-MMM-YYYY)	Initials 	What was changed
Possible enhancements:


 --->
<cfparam name="includeCustomSave" default="false" type="boolean"/>

<cf_checkFieldEncryption fieldnames="cjmTemplateID,cjmCustomizedTemplateID">

<cf_displayborder noborders=true>
<cfif isDefined("cjmCustomizedTemplateID")>
	<!--- 	This is a custom save file.  When it exists it overwrites the standard save and export buttons.  
			This file should include javascript to watch for the click event on the button with ID 'customSave'
			Then gather whatever data you need about the customized template from existing cold fusion or javascript variables --->
	<cfif includeCustomSave and fileExists("#application.paths.code#\cftemplates\cobrandingCustomSave.cfm")>
		<cfinclude template="/code/cftemplates/cobrandingCustomSave.cfm">
	</cfif>
	<cfset templateJson = application.com.cjm.loadCustomizedTemplateAttributes(cjmCustomizedTemplateID)>
	<div>
		<cfinclude template="/cobranding/templateEditor.cfm">
	</div>
<cfelse>
	<cfset templateJson = application.com.cjm.loadTemplateAttributes(cjmTemplateID)>
	<div>
		<cfinclude template="/cobranding/templateEditor.cfm">
	</div>
</cfif>