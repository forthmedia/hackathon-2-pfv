<!--- �Relayware. All Rights Reserved 2015 --->
<!---
File name:          CJMTemplateEmailForm.cfm
Author:             ACPK
Date started:       2015-10-30

Description:        A form used to email the customized template

Date (DD-MMM-YYYY)  Initials    What was changed
Amendment History:
2015-11-17          ACPK        PROD2015-400 Removed unneeded table elements; updated ID/class attributes of email address table; show all email addresses in table
2015-11-19          ACPK        PROD2015-421 Collect all email addresses as comma-separated list and remove duplicates
2015-11-27          ACPK        PROD2015-462 Replaced existing phrases with new phrases for consistency
2015-12-01          ACPK        PROD2915-467 Wrap email addresses in results table with HTMLEditFormat

Date (DD-MMM-YYYY)  Initials    What was changed
Possible enhancements:

--->

<cf_displayBorder
	noborders=true
>

<cf_includeJavascriptOnce template="/cobranding/js/cjm.js" translate="true">

<cfparam name="cjmCustomizedTemplateID" type="numeric" default="0">
<cf_checkfieldencryption fieldnames="cjmCustomizedTemplateID">

<cfform action="/cobranding/CJMTemplateEmailForm.cfm" method="post" name="EnterdetailsForm" onSubmit="return FormVerify()">
	<cf_relayFormDisplay class="" width="100%">
	    <!--- If form has been submitted --->
		<cfif isdefined("frmSendEmail")>
		    <!--- CC Address and Email Body are optional parameters, but still need to be defined --->
			<cfparam name="CJMTemplateEmailCCAddress" type="string" default="">
			<cfparam name="CJMTemplateEmailBody" type="string" default="">

			<!--- Remove '.pdf' from end of filename --->
			<cfset fileName = Left(CJMTemplateFileName, len(CJMTemplateFileName)-4)>
			<!--- Get saved data for canvas body of template --->
		    <cfset canvasBody = application.com.cjm.loadCustomizedTemplateCanvasBody(cjmCustomizedTemplateID)>

		    <cfif len(canvasBody) gt 0>
	   		    <!--- Convert saved canvas data into PDF file --->
				<cfset exportedTemplate = application.com.cjm.exportTemplate(canvasObj=canvasBody,exportType="pdf",fileName:fileName)>
				<!--- Revert substituted characters (subject line is already sanitized) --->
	            <cfset CJMTemplateEmailSubject = ReplaceList(CJMTemplateEmailSubject,'&lt;,&gt;,&quot;','<,>,"')>

				<cftry>
					<!--- Create and send an email using the SMTP server --->
					<cf_mail
						to="#CJMTemplateEmailToAddress#" <!--- comma-separated string of email addresses --->
				        from="#application.emailFrom#@#application.mailfromdomain#"
				        subject="#CJMTemplateEmailSubject#"
				        cc="#CJMTemplateEmailCCAddress#"
						type="plain"
						mimeattach="#exportedTemplate.exportedFilePath#" <!--- this should be the filepath of the file to attach --->
						><cfoutput>#CJMTemplateEmailBody#</cfoutput>
						</cf_mail>

					<cfset statusid = 2>
					<cfset returnMessage = "Email Sent">

					<cfcatch type="Any">
						<cfset statusid = -2>
						<cfset returnMessage = "Email failed">
					</cfcatch>
				</cftry>

				<!--- 2015-11-19    ACPK    PROD2015-421 Collect all email addresses within To and CC fields as comma-separated list --->
				<cfset allEmailAddresses = ListChangeDelims(CJMTemplateEmailToAddress & "," & CJMTemplateEmailCCAddress, ",", " ;")>

			    <!--- 2015-11-19    ACPK    PROD2015-421 Remove duplicate email addresses --->
				<cfset emailsaddresses = application.com.globalFunctions.removeDuplicates(type="list",data=allEmailAddresses,delimiter=",")>

				<cfoutput>
					<table id="tfqo_CJMEmailReport" data-role="table" data-mode="reflow" class="responsiveTable table table-hover table-striped table-bordered table-condensed withborder">
					    <thead><tr>
				            <th id="CJMTemplate_emailaddress" valign="top" columnname="CJMTemplate_emailaddress">
				                Phr_CJM_EmailAddress
				            </th>
				            <th id="CJMTemplate_result" valign="top" columnname="CJMTemplate_result">
				                Phr_CJM_DeliverResult
				            </th>
					    </tr></thead>

						<cfset CURRENTROW=0>
					    <cfloop index="myIndex" list="#emailsaddresses#" delimiters=",;">
					        <cfset CURRENTROW++>
					        <cfset currentclass=listGetAt('oddrow,evenRow',CURRENTROW mod 2+1)>

				            <cfif statusid neq 2>
				                <tr class="#currentclass#">
					                <td>#HTMLEditFormat(myindex)#</td><td>Phr_CJM_Email_Failed</td>
								</tr>
				            <cfelse>
				                <tr class="#currentclass#">
					                <td>#HTMLEditFormat(myindex)#</td><td>Phr_CJM_Email_Sent</td>
								</tr>
				            </cfif>
					    </cfloop>
	                </table>
				</cfoutput>
				<cf_relayFormElement relayFormElementType="button" fieldname="frmsendmessage" label="" currentValue="Phr_CJM_CloseWindow" onClick="parent.jQuery.fancybox.close();" valueAlign="left" spanCols="true">

			</cfif>
		<cfelse>
		    <!--- Check that canvas body is not empty string/null (i.e. customized template has been saved after email functionality was implemented) --->
            <cfif len(application.com.cjm.loadCustomizedTemplateCanvasBody(cjmCustomizedTemplateID)) gt 0>
			    <!--- Get custom title for template from database --->
			    <cfquery name="getCustomTitle" datasource="#application.siteDataSource#">
			        select customTitle from CJMCustomizedTemplate
			        where cjmCustomizedTemplateID = <cf_queryParam value="#cjmCustomizedTemplateID#" cfsqltype="cf_sql_integer">
			    </cfquery>
			    <cfset customTitle = getCustomTitle.customTitle>

				<h1>phr_CJM_EmailFormInstruction</h1>

				<CF_relayFormElementDisplay relayFormElementType="text" fieldName="CJMTemplateEmailToAddress" id="CJMTemplateEmailToAddress" label="phr_CJM_To" maxLength="500" currentValue="" required="yes">
				<CF_relayFormElementDisplay relayFormElementType="text" fieldName="CJMTemplateEmailCCAddress" id="CJMTemplateEmailCCAddress" label="phr_CJM_CC" maxLength="500" currentValue="">
				<CF_relayFormElementDisplay relayFormElementType="textArea" fieldName="CJMTemplateEmailSubject" id="CJMTemplateEmailSubject" label="phr_CJM_Subject" maxLength="100" currentValue="" required="yes">
				<CF_relayFormElementDisplay relayFormElementType="textArea" fieldName="CJMTemplateFileName" id="CJMTemplateFileName" label="phr_CJM_Attachment" currentValue="#customTitle#.pdf" readonly="true">
				<CF_relayFormElementDisplay relayFormElementType="textArea" fieldName="CJMTemplateEmailBody" id="CJMTemplateEmailBody" label="phr_CJM_Message" maxLength="500" currentValue="">

			    <cf_encryptHiddenFields>
			       <CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#cjmCustomizedTemplateID#" fieldname="cjmCustomizedTemplateID">
			    </cf_encryptHiddenFields>

				<cf_relayFormElement relayFormElementType="submit" fieldname="frmSendEmail" label="" currentValue="phr_CJM_Send" valueAlign="left" spanCols="true">
				<cf_relayFormElement relayFormElementType="button" fieldname="frmCancelEmail" label="" currentValue="phr_CJM_Cancel" onClick="parent.jQuery.fancybox.close();" valueAlign="left" spanCols="true">

            <!--- If canvas body is undefined (i.e. not saved) --->
            <cfelse>
                <b>phr_CJM_CannotEmailTemplate</b>
                <cf_relayFormElementDisplay relayFormElementType="HTML" currentValue="phr_CJM_OpenAndResaveTemplate" label="" spanCols="yes">
			</cfif>
		</cfif>
	</cf_relayFormDisplay>
</cfform>