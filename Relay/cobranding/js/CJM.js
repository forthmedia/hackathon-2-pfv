//---------------------------------CHANGE PAGE-----------------------------------------
//save current page, then switch to new page and load its rectangles
function changePage(page) {
	JSONoutput = saveToTemplate();
	if(pages[page] !== ''){
		loadBackgroundImage({imageURL:pages[page],jsonTemplate:JSONoutput,page:page});
	}else{
		currentPage = page;
	}
	loadRectangles(JSONoutput,page);
}

//--------------------------------SAVE---------------------------------------------------
//saves current page object to template object, then returns everything as JSON string
function saveToTemplate() {
	templateObj[currentPage] = savePage();
	JSONoutput = JSON.stringify(templateObj);
	return(JSONoutput);
}

//saves URL/dimensions of canvas as first item in current page object, then calls saveRectangles()
function savePage() {
	var pageObj = {};
	//first item in object relates to background image of canvas
	var imageURL = jQuery(canvas).css('background-image')
	if(imageURL == 'none'){
		//if no imageURL then we are using html background.
		pageObj[0] = {
					"type":"background-html",
					"html":jQuery(canvas).find('.backgroundHtml').first().html().replace(/[<]/g,'&#60;').replace(/[>]/g,'&#62;'),
					"width":jQuery(canvas).find('.backgroundHtml').first().css('width'),
					"height":jQuery(canvas).find('.backgroundHtml').first().css('height')};
	}else{
		//chrome/safari automatically removes quote marks from background-image: url(), so separate substring method required
		if ((window.chrome !== null && window.chrome !== undefined && window.navigator.vendor === "Google Inc.") || (Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0)) { //for chrome/safari
			var imageURLTrimmed = imageURL.substring(4, imageURL.length - 1);
		} else { //for all other browers
			var imageURLTrimmed = imageURL.substring(5, imageURL.length - 2);
		}
		pageObj[0] = {"type":"background-image","imageURL":imageURLTrimmed,"width":jQuery(canvas).css('width'),"height":jQuery(canvas).css('height')};
	}
	
	saveRectangles(pageObj);
	return (pageObj);
}
//saves data of each rectangle on current canvas to page object, starting from second item
function saveRectangles(pageObj) {
	jQuery(canvas).find('.rectangle').each(function(i) {
		//attach mutable data to current rectangle
		jQuery(this).data('left', jQuery(this).css('left'));
		jQuery(this).data('top', jQuery(this).css('top'));
		jQuery(this).data('width', jQuery(this).css('width'));
		jQuery(this).data('height', jQuery(this).css('height'));
		//add to main object
		pageObj[i+1] = jQuery(this).data();
	});
}

//-------------------------------TEXT ELEMENT-------------------------------------------
//create custom text element containing text node/anchor tag and update rectangle data
function createTextElement() {
	var textContent = "";
	//set data of text element/content of text node to user-supplied text;
	if (savedElement.usertext !== undefined) {
		textContent = savedElement.usertext;
		jQuery(loadElement).data('usertext', savedElement.usertext);

		//add a class to show that user text has been added
		jQuery(loadElement).addClass("edited")
		
	} else { //otherwise use placeholder text
		textContent = savedElement.placeholder;
		jQuery(loadElement).data('placeholder', savedElement.placeholder);
	}
	//create text node and pre element container
	var textNode  = document.createTextNode(textContent);
	var textElement = document.createElement("pre");
	textElement.className = "textNode";
	jQuery(textElement).append(textNode);
	
	//keep text inside rectangle element by matching dimensions of text element to rectangle
	jQuery(textElement).css('width', loadElement.style.width);
	jQuery(textElement).css('height', loadElement.style.height);
	if (savedElement.type == "text") {//use specified font settings/data for text elements
		jQuery(loadElement).addClass("red");
		loadElement.title="Text Box";
		var properties = ["fontFamily","fontSize","fontWeight","fontStyle","textDecoration","textAlign"];
		for (i = 0; i < 6; i++) {
			var currentProp = properties[i];
			textElement.style[currentProp] = savedElement[currentProp];
			jQuery(loadElement).data(currentProp, savedElement[currentProp])
		}
		textElement.style.color = savedElement.fontColor;
		jQuery(loadElement).data('fontColor', savedElement.fontColor);
	} else { //use these defaults for image elements
		jQuery(loadElement).addClass("blue");
		loadElement.title="Image Box";
		jQuery(textElement).css('font-family', "Arial");
		jQuery(textElement).css('font-size', "14px");
		jQuery(textElement).css('text-align', 'center');
		jQuery(textElement).css('text-decoration', 'none');
		jQuery(textElement).css('color', '#000000');
	}
	jQuery(loadElement).data('href', savedElement.href);
	//if element is set to link: create anchor tag, insert text element into it, then insert this into rectangle;
	if (savedElement.href == "true") {
		var anchorElement = document.createElement("a");
		//use user-supplied hyperlink URL if it exists
		if (savedElement.hyperlink !== undefined) {
			jQuery(anchorElement).attr("href", savedElement.hyperlink);
			jQuery(loadElement).data("hyperlink", savedElement.hyperlink);
		}
		jQuery(anchorElement).attr("onclick","return false"); //don't make hyperlinks accidentally clickable
		jQuery(anchorElement).attr("target","_blank"); // make anchor open in new window
		jQuery(anchorElement).append(textElement);
		jQuery(loadElement).append(anchorElement);
	} else {//otherwise: insert text element into rectangle
		jQuery(loadElement).append(textElement);
	}
}

//-----------------------GET RECTANGLES-------------------------
//clears canvas, converts loaded string back into JSON object, selects current page object, then rebuilds each rectangle in turn
function getRectangles(jsonString,currentPage) {
	deleteAll();
	//loadedTemplate = jQuery.parseJSON(jsonString);
	if (typeof jsonString == 'object'){
		loadedTemplate = jsonString;
	}else{
		loadedTemplate = jQuery.parseJSON(jsonString);
	}
	loadedPage = loadedTemplate[currentPage];
	if (loadedPage !== undefined) {
		jQuery.each(loadedPage, function() {
			savedElement = this;
			prepareRectangle(savedElement);
		});
	}
	resetCursors();
}

//----------------------DESELECT ALL---------------------------
//changes borders of previously selected element back to dashed
function deselectAll() {
	activeElement = "";
	jQuery(canvas).find('.rectangle').each(function() {
		if (jQuery(this).hasClass('selectedRed')) { //for text elements
			jQuery(this).removeClass('selectedRed');
			jQuery(this).find('.resizeButton, .deleteButton').removeClass('selectedRed');
		}
		if (jQuery(this).hasClass('selectedBlue')) { //for image elements
			jQuery(this).removeClass('selectedBlue');
			jQuery(this).find('.resizeButton, .deleteButton').removeClass('selectedBlue');
		}
		jQuery(this).find('.resizeButton, .deleteButton').hide();
	});
}

//-------------------------ZOOM-----------------------------------
//resizes contents of iframe (between 1-100%), adjusts border appearance
function changeZoom(zoomLevel) {
	if (zoomLevel < 101 && zoomLevel > 0) {
		jQuery('#iframe').contents().find('html').css('transform', 'scale(' + zoomLevel/100 + ')');
		if (zoomLevel < 20) {
			jQuery(canvas).find('.rectangle, .resizeButton, .deleteButton').each(function(){
				jQuery(this).css('cssText', jQuery(this).attr('style') + ';border-width: 6px !important');
			});
		} else if (zoomLevel < 40) {
			jQuery(canvas).find('.rectangle, .resizeButton, .deleteButton').each(function(){
				jQuery(this).css('cssText', jQuery(this).attr('style') + ';border-width: 5px !important');
			});
		} else if (zoomLevel < 60) {
			jQuery(canvas).find('.rectangle, .resizeButton, .deleteButton').each(function(){
				jQuery(this).css('cssText', jQuery(this).attr('style') + ';border-width: 4px !important');
			});
		} else if (zoomLevel < 80) {
			jQuery(canvas).find('.rectangle, .resizeButton, .deleteButton').each(function(){
				jQuery(this).css('cssText', jQuery(this).attr('style') + ';border-width: 3px !important');
			});
		} else {
			jQuery(canvas).find('.rectangle, .resizeButton, .deleteButton').each(function(){
				jQuery(this).css('cssText', jQuery(this).attr('style') + ';border-width: 2px !important');
			});
		}
	}
}

//zoom out until canvas width matches iframe width; else use 100%
function adjustZoom(canvasWidth){
    if (canvasWidth > jQuery("#iframe").width()) {
		zoomFactor = jQuery("#iframe").width() / canvasWidth * 100;
    	jQuery('#zoom').slider('value',Math.round(zoomFactor));
    	changeZoom(Math.round(zoomFactor));
    } else {
    	jQuery('#zoom').slider('value',100);
    	changeZoom(100);
    }
}

//---------------------------------LOAD BACKGROUND IMAGE------------------------------------------------
//create image object containing background image, copy into canvas
loadBackgroundImage = function (params) {
	var imageURL = "";
	var width = "";
	var height = "";
	
	//if JSON data already exists, parse it
	if(params.jsonTemplate != ''){
		var jsonTemplate = JSON.parse(params.jsonTemplate);
	}
	//save selected page as current page 
	currentPage = params.page;
	//if JSON data does not exist for current page, create image object for it and set corresponding image as canvas background
	if(params.jsonTemplate == '' || typeof jsonTemplate[params.page] === "undefined"){
		var imageObject = document.createElement("IMG");
		jQuery(imageObject).attr("src",params.imageURL).load(function () {
			imageURL = 'url("' + imageObject.src + '")';
			width = imageObject.width;
			height = imageObject.height;
			setCanvasBackground(imageURL,width,height);
		});
	//otherwise use existing image for canvas background image
	} else {
        //remove any quotation marks from imageURL
		jsonTemplate[params.page][0]['imageURL'] = jsonTemplate[params.page][0]['imageURL'].replace(/"/g,'');

		//if the imageURL is a relative url make it absolute
		if (jsonTemplate[params.page][0]['imageURL'].substring(0,4) != 'http'){
			jsonTemplate[params.page][0]['imageURL'] = location.protocol + '//' + location.host + jsonTemplate[params.page][0]['imageURL'];
		}
		
		imageURL = 'url("' + jsonTemplate[params.page][0]['imageURL'] + '")';
		width = jsonTemplate[params.page][0]['width'].replace("px","");
		height = jsonTemplate[params.page][0]['height'].replace("px","");
		
		setCanvasBackground(imageURL,width,height);
	}
	
	//set/resize canvas to fit provided template, save canvas proportions, find its position in frame
	function setCanvasBackground(imageURL,width,height){
		jQuery(canvas).css('background-image', imageURL);
		jQuery(canvas).css('width', width + 'px');
		canvasWidth = width;
		jQuery(canvas).css('height', height + 'px');
		canvasHeight = height;
		//stores co-ordinates of top-left corner of canvas relative to frame
		function findPosition(object) {
			canvasTop = jQuery(object).offset().top;
			canvasLeft = jQuery(object).offset().left;
		}
		findPosition(canvas);
		adjustZoom(width)
	}
};

//---------------------------------VALIDATE EMAIL FORM------------------------------------------------
    //verify data entered into ToAddress and CCAddress fields
    function FormVerify(){
        //Convert data within To and CC fields into comma/semicolon/space-separated lists
        var nForm = document.EnterdetailsForm;
        var toAddressString = nForm.CJMTemplateEmailToAddress.value;
        var toAddressList = toAddressString.split(/,|;| /);
        var CCAddressString = nForm.CJMTemplateEmailCCAddress.value;
        var CCAddressList = CCAddressString.split(/,|;| /);

        //if To and CC fields contain only formatted email addresses, submit form. Otherwise, return error
        if (toAddressList.every(verifyEmail) && CCAddressList.every(verifyEmail)){
            nForm.submit(); 
        } else {
            alert(phr.sys_formValidation_ValidEmailAddress);
            return false;
        }
    }

    //if current item is formatted email address or empty string, return true. Otherwise, return false
    function verifyEmail(thisValue) {
        if (thisValue == "") {
            return true;
        } else {
            NumberFormatRe = /^[\w_\.\-']+@[\w_\.\-']+\.[\w_\.\-']+$/
            return NumberFormatRe.test(thisValue);
        }
    }