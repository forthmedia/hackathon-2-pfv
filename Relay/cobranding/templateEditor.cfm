<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:          templateEditor.cfm
Author:             ACPK
Date started:       2014-12-10

Description:        Template layout designer

Amendment History:

Date (YYYY-MM-DD)   Initials    What was changed
2014-12-12          ACPK        User can now replace placeholder text of image elements with an uploaded image
2015-02-24          ACPK        FIFTEEN-243 Replaced HTML5 input range object and number polyfill with jQuery UI Slider widget
2015-08-14          ACPK        Fixed IE-specific bug where image files would be uploaded to the server twice, before returning an error message
2015-09-23          ACPK        PROD2015-76 Hide Export Template button if no export options available
2015-11-04          ACPK        PROD2015-323 Encoded filename component of uploaded image URLs
2015-11-06          ACPK        PROD2015-316 Export customized template no longer tries and fails to save template
2015-11-10          ACPK        PROD2015-24 Added fields for custom label/description (prepopulated if viewing customized template) and updated Export/Save Template methods to include these new parameters; extracted buildCanvasObj method
2015-11-17          ACPK        Add for and class attributes to labels
2015-11-23          ACPK        PROD2015-415 Replaced HTML5 select element with jQuery UI Selectmenu widget; Replaced jquery-ui.css with jquery-ui.min.css
2015-12-01          ACPK        PROD2015-467 Replace HTML input elements with cf_input tags for security purposes
2016/02/25			NJH/SB		JIRA PROD2105-520 - remove height on backgroundHTML div
Possible enhancements:


 --->

<cf_includeCssOnce template="/cobranding/css/templateDesigner.css">
<cf_includejavascriptOnce template="/cobranding/js/cjm.js">
<cf_includejavascriptOnce template="/javascript/lib/jquery/jquery-ui.min.js">
<cf_includejavascriptOnce template="/javascript/lib/jquery/spinner.js">
<cf_includeCssOnce template="/javascript/lib/jquery/css/jquery-ui.min.css"> <!--- 2015-11-23   ACPK    PROD2015-415 Replaced jquery-ui.css with jquery-ui.min.css --->

<cfparam name="CJMTemplateID" default="">
<cfparam name="cjmCustomizedTemplateID" default="">

<cfset cjmTemplate = application.com.cjm.getCJMTemplate(CJMTemplateID=CJMTemplateID)>

<cfquery name="getJsonStoredFiles" datasource="#application.siteDataSource#">
    select jsonStoredFiles from CJMTemplate
    <cfif isDefined("cjmCustomizedTemplateID") and cjmCustomizedTemplateID neq "">
        where CJMTemplateID in (select cjmTemplateID from cjmCustomizedTemplate where cjmCustomizedTemplateID = <cf_queryParam value="#cjmCustomizedTemplateID#" cfsqltype="cf_sql_integer">)
    <cfelse>
        where CJMTemplateID = <cf_queryParam value="#CJMTemplateID#" cfsqltype="cf_sql_integer">
    </cfif>
</cfquery>
<cfset jsonStoredFiles = deserializejson(getJsonStoredFiles.jsonStoredFiles)>

<!--- 2015-11-10    ACPK    PROD2015-24 If viewing customized template, get custom title and description --->
<cfif isDefined("cjmCustomizedTemplateID") and cjmCustomizedTemplateID neq "">
    <cfquery name="getCustomTemplateDetails" datasource="#application.siteDataSource#">
        select customTitle, customDescription from CJMCustomizedTemplate
        where cjmCustomizedTemplateID = <cf_queryParam value="#cjmCustomizedTemplateID#" cfsqltype="cf_sql_integer">
    </cfquery>
	<cfset customTitle = getCustomTemplateDetails.customTitle>
    <cfset customDescription = getCustomTemplateDetails.customDescription>
</cfif>

<cfoutput>
    <script>
        var pages = {};

        <cfloop collection="#jsonStoredFiles.pages#" item="page">
            <cfif not structKeyExists(jsonStoredFiles.pages[page],"templateSource") or (jsonStoredFiles.pages[page].templateSource neq "html")>
                pages[#page#] = '#jsonStoredFiles.pages[page].RELATIVEFILESOURCE#';
            <cfelse>
                pages[#page#] = '';
            </cfif>
        </cfloop>
    </script>
    <div id="templateEditorControls" class="row">
		<div class="col-xs-12 col-sm-4">
	        <div class="form-group">
	        	<label>phr_CJM_page</label>
				<!--- 2015-11-23   ACPK    PROD2015-415 Replaced HTML5 select element with jQuery UI Selectmenu widget --->
				<select class="form-control" id="pageNumber">
		            <cfloop index="page" from="1" to="#structCount(jsonStoredFiles.pages)#">
		            	<option #iif(page eq 1,de("selected='selected'"),de(""))# value="#page#">phr_page #page#</option>
		            </cfloop>
		        </select>
			</div>
		</div>

		<div class="col-xs-12 col-sm-4">
			<!--- 2015-11-10    ACPK    PROD2015-24 Display fields for label and description (prepopulated if viewing customized template) --->
			<!--- 2015-11-17    ACPK    Add for and class attributes to labels --->
	        <div class="form-group">
	        	<label for="CJMlabel" class="required">phr_cjm_templates_label</label>
				<!--- 2015-12-01    ACPK    PROD2015-467 Replace HTML input elements with cf_input tags for security purposes --->
	            <cf_input class="form-control" id="CJMlabel" type="text" value=#iif(IsDefined("customTitle"),"customTitle","cjmTemplate.cjmtitle")# maxlength="50" />
	        </div>
	    </div>
        <div class="col-xs-12 col-sm-4">
			<div class="form-group">
	            <label for="CJMdescription">phr_cjm_templates_desc</label>
				<!--- 2015-12-01    ACPK    PROD2015-467 Replace HTML input elements with cf_input tags for security purposes --->
	            <cf_input class="form-control" id="CJMdescription" type="text" value=#iif(IsDefined("customDescription"),"customDescription","cjmTemplate.cjmdescription")# maxlength="1000" />
	        </div>
	    </div>
    </div>
	<div class="templateEditorButtons">
         <div class="buttonContainer">
             <!---   This is a custom save file.  When it exists it overwrites the standard save and export buttons.
                     This file should include javascript to watch for the click event on the button with ID 'customSave'
                     Then gather whatever data you need about the customized template from existing cold fusion or javascript variables --->
             <cfif includeCustomSave and fileExists("#application.paths.code#\cftemplates\cobrandingCustomSave.cfm")>
                 <a href="##" id="customSave" class="btn btn-primary">phr_CJM_SaveTemplate</a>
             <cfelse>
                 <!--- 2015-09-23    ACPK    PROD2015-76 Hide Export Template button if no export options available --->
                 <cfif cjmTemplate.pdfable or cjmTemplate.webable or cjmTemplate.printable>
                     <div class="dropdown pull-left">
                         <button class="btn btn-primary dropdown-toggle" type="button" id="exportMenu" data-toggle="dropdown" aria-expanded="false">
                         phr_CJM_ExportTemplate
                         <span class="fa fa-caret-down"></span>
                         </button>
                         <ul class="dropdown-menu cjmDropdownMenu" role="menu" aria-labelledby="exportMenu">
                             <cfif cjmTemplate.pdfable eq 1><li role="presentation"><a role="menuitem" tabindex="-1" onclick="exportCustomizedTemplate(jQuery('##exportMenu'),'pdf')" href="##">phr_cjm_ExportPDF</a></li></cfif>
                             <cfif cjmTemplate.webable eq 1><li role="presentation"><a role="menuitem" tabindex="-1" onclick="exportCustomizedTemplate(jQuery('##exportMenu'),'zip')" href="##">phr_cjm_ExportWeb</a></li></cfif>
                             <cfif cjmTemplate.printable eq 1><li role="presentation"><a role="menuitem" tabindex="-1" onclick="exportCustomizedTemplate(jQuery('##exportMenu'),'png')" href="##">phr_cjm_ExportImage</a></li></cfif>
                         </ul>
                     </div>
                 </cfif>
                 <a href="##" id="saveButton" onclick="saveCustomizedTemplate(this)" class="btn btn-primary">phr_CJM_SaveTemplate</a>
             </cfif>
         </div>
     </div>
</cfoutput>

<!----------------------------------------------------------------HTML--------------------------------------------------------------->
<hr/>
<div class="form-horizontal">
    <div class="form-group">
        <div class="col-xs-2 control-label">
            <label>phr_CJM_Zoom</label>
        </div>
        <div class="col-xs-2">
            <!--- 2015-02-24    ACPK    FIFTEEN-243 Replaced HTML5 input range object and number polyfill with jQuery UI Slider widget --->
            <div id="zoom"><div id="zoomValue"></div></div>
        </div>
        <div class="col-xs-8">
            <div class="row form-group" id="usertextSection">
                <div class="col-xs-4 control-label">
                    <label>phr_CJM_Entertext</label>
                </div>
                <div class="col-xs-8">
                    <textarea class="form-control" id="usertext" oninput="updateProperty(this)"></textarea>
                </div>
            </div>
            <div class="row form-group" id="fileSection">
                <div class="col-xs-4 control-label">
                    <label>Upload image </label>
                </div>
                <div class="col-xs-8" id="uploadDiv">
                    <div>
                        <cfoutput>
                            <form action="/webservices/callwebservice.cfc?wsdl&method=callwebservice&webservicename=relayCJMWS&methodname=uploadImage&CJMTEMPLATEID=#CJMTEMPLATEID#&returnFormat=plain&_cf_nodebug=true" method="post" enctype="multipart/form-data" name="cjmFileFormAttachments" id="cjmFileFormAttachments">
                                <input id="uploadImage" name="uploadImage" type="file" onchange="doUpload(this)" oninput="doUpload(this)"/>
                                <input name="targetWidth" type="hidden" value=""/>
                                <input name="targetHeight" type="hidden" value=""/>
                            </form>
                        </cfoutput>
                    </div>
                </div>
            </div>
            <div class="row form-group" id="hyperlinkSection">
                <div class="col-xs-4 control-label">
                    <label>phr_CJM_Hyperlink</label>
                </div>
                <div class="col-xs-8">
                    <input class="form-control" id="hyperlink" type="text" placeholder="Enter a URL" oninput="updateProperty(this)"/>
                </div>
            </div>
        </div>
    </div>
</div>
<iframe id="iframe"></iframe>
<script>
    //---------------------------------UPLOAD FILE----------------------------------------------
    //creates temporary iframe to insert form into before submission
    var iframeElement = "";

    <!--- 2015-08-14    ACPK    Fixed IE-specific bug where image files would be uploaded to the server twice, before returning an error message --->
    //creates temporary variable to prevent IE from uploading same file twice, before trying to upload an empty string (caused by onchange attribute in #uploadImage, which is required by Firefox/Chrome)
    var fileUploadedIE = false;

    doUpload = function (obj) {
        if (fileUploadedIE == false && jQuery('#uploadImage').val() != "") { //don't upload same file twice in a row or if input is blank
            fileObj = jQuery(obj);
            theForm = fileObj.closest('form');
            theFormParent = theForm.closest('div');

            //save active element so user can't switch focus to other elements while file uploads
            selectedElement = activeElement;
            // create an iframe to post into
            iframeName = fileObj.attr('name') + '_iframe_' + Math.random()
            iframeElement = jQuery("<iframe>", {
                id : iframeName,
                name : iframeName,
                style : "display:none"
            });
            //insert iframe into document
            jQuery('body').append(iframeElement);
            //replace Browse button with animated spinner
            jQuery('#uploadImage').hide();
            jQuery('#uploadDiv').spinner({position:'left'});
            // we have to post form to somewhere else so save the existing action and target
            // post, then copy the action and target back
            theForm[0].target = iframeName;
            theForm.submit();

            //if IE, temporarily set fileUploadedIE to true to prevent file from being immediately reuploaded
            if (window.navigator.userAgent.indexOf("MSIE") > -1 || window.navigator.userAgent.indexOf("Trident/") > -1) { //if IE, set same
                fileUploadedIE = true;
            }
        } else {
            //reset fileUploadedIE to false so same file can be uploaded for different rectangle element
            fileUploadedIE = false;
        }
    }

    recieveUpload = function (result) {
        if (!result.ISOK) {
            alert (result.MESSAGE);
        }
        //insert image into selected image element
        updateImageElement(result.FILE.RELATIVEDIRECTORY + "\\" + encodeURIComponent(result.FILE.FILENAME), selectedElement); <!--- 2015-11-04    ACPK    PROD2015-323 Encoded filename component of uploaded image URLs --->

        //remove iframe and animated spinner
        jQuery(iframeElement).remove();
        jQuery('#uploadImage').show();
        jQuery('#uploadDiv').removeSpinner();
        //reset upload button when finished
        jQuery('#uploadImage').val(null);
    }

    //inserts uploaded image into selected image element
    updateImageElement = function (imageURL, selectedElement) {
        //NB: Chrome requires the full URL to be passed into backgroundImage
        sourceURL = '<cfoutput>#request.currentsite.httpprotocol##request.currentsite.domainandroot#</cfoutput>' + imageURL.replace(/\\/g, '/');
        formattedURL = 'url("' + sourceURL + '")';
        //jQuery(selectedElement).css('backgroundImage', formattedURL);
        jQuery(selectedElement).data('backgroundImage', formattedURL);
        //if image element contains placeholder text, remove corresponding placeholder data
        if (jQuery(selectedElement).find("pre").hasClass("textNode")) {
            jQuery(selectedElement).removeData('placeholder');
        }

        //add a class to show that user text has been added
        jQuery(selectedElement).addClass("edited")

        //create image object and replace contents of image element with it
        var imageObject = document.createElement("IMG");
        imageObject.src = sourceURL;
        if (jQuery(selectedElement).data('href') == "true") {
            jQuery(selectedElement).children().children().replaceWith(imageObject);
        } else {
            jQuery(selectedElement).children().replaceWith(imageObject);
        }
    }
    //--------------------------------------------SETUP------------------------------------------
    //Initialising global variables
    var scrollingMode = false;
    var element = null;
    var activeElement;
    var mouse = {
        x: 0,
        y: 0,
        startX: 0,
        startY: 0
    };
    var canvasLeft = 0;
    var canvasTop = 0;
    var canvasWidth = 0;
    var canvasHeight = 0;
    var templateObj = {};
    var cjmCustomizedTemplateID = 0;
    <cfif isDefined("cjmCustomizedTemplateID") and cjmCustomizedTemplateID neq "">
        <cfoutput>
            cjmCustomizedTemplateID = #cjmCustomizedTemplateID#;
        </cfoutput>
    </cfif>

    //For Chrome
    jQuery("#iframe").bind("load",function(){
        if (!jQuery.browser.webkit) {
            initiateCanvas();
            initiateKeyBinds();
        }
    });
    //For other browsers
    jQuery(document).ready(function() {
        if (jQuery.browser.webkit) {
            initiateCanvas();
            initiateKeyBinds();
        }
    });
    //Run these methods when page/iframe finishes loading to prepare initial canvas
    function initiateCanvas() {

        //IE 9 doesnt get a body tag inside the Iframe so we need to create it on the fly.
        if(typeof jQuery("#iframe").contents().find('body') == undefined || jQuery("#iframe").contents().find('body').length == 0){
            document.getElementById('iframe').contentWindow.document.write("<html><body></body></html>");
        }

        //insert canvas and corresponding CSS properties into iframe
        jQuery('#iframe').contents().find('html').css('transform-origin','0 0');
        jQuery("#iframe").contents().find('body').html('<div id="canvas" ondragstart="return false;"></div>') //ondragstart attribute required so rectangles can be dragged in non-Firefox browsers
        jQuery("#iframe").contents().find("head").append(jQuery("<link/>",
            {rel: "stylesheet", href: "./css/templatedesigner.css", type: "text/css"}
        ));

        canvas = jQuery("#iframe").contents().find('#canvas');

        if(pages[1] !== ''){ //dont try and load a background image if we didnt set one e.g. its a html template
            loadBackgroundImage({imageURL:pages[1],jsonTemplate:'',page:1});
        }else{
            currentPage = 1;
        }

        addCanvasMethods();
        //convert saved data from JSON string to object, then rebuild rectangles
        <cfoutput>loadRectangles(#templateJson#,jQuery('##pageNumber').val());
        //save copy of object to which future changes can be made

        if(typeof #templateJson# === 'object'){
            templateObj = #templateJson#;
        } else {
            templateObj = jQuery.parseJSON(#templateJson#);
        }
        </cfoutput>

        //hide sections from view
        hideSections();
        jQuery("#usertextSection").css('visibility', 'hidden');
        jQuery('#hyperlinkSection').css('visibility', 'hidden');
        jQuery('#fileSection').hide();
        //apply initial CSS settings to cursor
        resetCursors();

    }

	<!--- 2015-11-23   ACPK    PROD2015-415 Replaced HTML5 select element with jQuery UI Selectmenu widget --->
	//------------------------CHANGE PAGE DROPDOWN-----------------------------------
	//use jQuery UI Selectmenu widget to change current page of template
	jQuery("#pageNumber").selectmenu({
	   change: function(event, ui) {
	       changePage(jQuery(this).val()); <!---see CJM.js--->
	   }
	});

    <!--- 2015-02-24    ACPK    FIFTEEN-243 Replaced HTML5 input range object and number polyfill with jQuery UI Slider widget--->
    //------------------------ZOOM SLIDER WIDGET-----------------------------------
    //use jQuery UI slider widget for zoom (all values in percentages)
    jQuery("#zoom").slider({
        min: 1, //minimum value
        max: 100, //maximum value
        step: 1, //incremental value
        value: 100, //default value on load
        start: function(event, ui){ //when user starts moving handle
            positionLabel(ui);
            jQuery("#zoomValue").show(); //show label
        },
        slide: function(event, ui){ //whenever user moves handle
            changeZoom(ui.value); //resize canvas to current value (see CJM.js)
            positionLabel(ui);
        },
        stop: function(){ //when user stops moving handle
            jQuery("#zoomValue").hide(); //hide label
        },
        create: function(event, ui){ //when widget is initialised
            jQuery("#zoomValue").hide(); //hide label
        }
    });

    //change value of label and position underneath slider handle
    function positionLabel(ui) {
        var handlePosition = jQuery(".ui-slider-handle").css("left").replace(/px/, ''); //get handle position
        var labelPosition = handlePosition - 5 + 'px';
        jQuery("#zoomValue").text(ui.value + "%").css("left", labelPosition);
    }

    //----------------------------------------KEY BINDS-------------------------------------------
    function initiateKeyBinds (){
        jQuery(document.getElementById('iframe').contentWindow.document).keydown(function (e) {
            //pressing backspace (8) or delete (46) prevents user from accidentally navigating to previous page
            if ((e.which === 8 || e.which === 46) && !jQuery(e.target).is("input, usertext")) {
                e.preventDefault();
            }
        });
    }
    //----------------------------MOUSE METHODS-------------------------
    function setMousePosition(e) {
        <!--- 2015-02-24    ACPK    FIFTEEN-246 Replaced HTML5 input range object with jQuery UI Slider widget --->
        //get cursor position relative to top-left corner of canvas, scaled to match zoomed view
        mouse.x = (e.pageX - canvasLeft) * (100 / (jQuery("#zoom").slider("value")));
        mouse.y = (e.pageY - canvasTop) * (100 / (jQuery("#zoom").slider("value")));

        //if dragging on canvas: change value of scrollbars
        if (scrollingMode) {
            iframeWindow = jQuery(jQuery('#iframe')[0].contentWindow);
            iframeWindow.scrollLeft(iframeWindow.scrollLeft() + (mouse.startX - (e.pageX - canvasLeft)));
            iframeWindow.scrollTop(iframeWindow.scrollTop() + (mouse.startY - (e.pageY - canvasTop)));
        }
    }
    //-----------------------------CANVAS METHODS------------------------------
    function addCanvasMethods() {
        //moving cursor over canvas gets its current position
        jQuery(canvas).mousemove(function (e) {
            setMousePosition(e);
        });

        // clicking on canvas deselects active element, clears placeholder text, enters scrolling mode
        jQuery(canvas).mousedown(function (e) {
            deselectAll(); //(see CJM.js)
            hideSections();
            jQuery('#usertext').val("");
            scrollingMode = true;
            mouse.startX = e.pageX - canvasLeft;
            mouse.startY = e.pageY - canvasTop;
            jQuery(canvas).css('cursor', 'move');
            jQuery(canvas).find('.rectangle').css('cursor', 'move');
        });

        //releasing mouse over canvas exits scrolling mode
        jQuery(canvas).mouseup(function () {
            resetCursors();
            scrollingMode = false;
            element = null;
        });
    }

    //-----------------------RECTANGLE METHODS----------------------------
    function addRectangleMethods(element) {
        //when clicking on rectangle itself
        jQuery(element).mousedown(function(event) {
            event.stopPropagation();
            deselectAll(); //deselect all other rectangles (see CJM.js)
            element = this;
            selectRectangle(element); //select current rectangle
            jQuery(element).appendTo(jQuery(canvas)); //bring selected rectangle to front
            //inserts corresponding properties of active element into fields
            jQuery('#usertext').attr('placeholder', jQuery(element).data('placeholder'));
            jQuery('#usertext').val(jQuery(element).data('usertext'));
            if (jQuery(element).data('href') == "true") {
                jQuery('#hyperlinkSection').css('visibility', 'visible');
                //automatically populate hyperlink field with "http://" if hyperlink not yet supplied by user; otherwise populate with previously supplied URL
                if (jQuery(element).data('hyperlink') == undefined || jQuery(element).data('hyperlink') == "") {
                    jQuery('#hyperlink').val("http://");
                } else {
                    jQuery('#hyperlink').val(jQuery(element).data('hyperlink'));
                }
            } else {//else clear hyperlink field and hide it
                jQuery('#hyperlink').val('');
                jQuery('#hyperlinkSection').css('visibility', 'hidden');
            }
        });

        //releasing mouse over any element will exit scrolling mode
        jQuery(element).mouseup(function (event) {
            if (scrollingMode) {
                event.stopPropagation();
                scrollingMode = false;
            }
            resetCursors();
        });

        //doubleclicking element moves focus to placeholder field
        jQuery(element).dblclick(function (event) {
            jQuery('#usertext').focus();
        });
    }



    //------------------------------------LOAD RECTANGLES----------------------------------------------
    //clears canvas, converts loaded string back into JSON object, selects current page object, then rebuilds each rectangle in turn
    function loadRectangles(jsonString,page) {
        getRectangles(jsonString,page); //see CJM.js
    }
    //rebuilds current rectangle element (loadElement) from its associated data (savedElement)
    function prepareRectangle(savedElement) {
        // Load the background as html if the current saved element is html and it isnt already loaded.
        if (savedElement.type == "background-html" && jQuery(canvas).find('.backgroundHtml').length == 0){

            loadElement = document.createElement('div');
            loadElement.innerHTML = savedElement.html.replace(/&#60;/g,'<').replace(/&#62;/g,'>')
            loadElement.className = "backgroundHtml";
            jQuery(loadElement).data('type', savedElement.type);
            loadElement.style.width = savedElement.width;
            //SB 15/02/2016 Issue with border height
            //loadElement.style.height = savedElement.height;

            canvasWidth = savedElement.width;
            canvasHeight = savedElement.height;

            //insert finalised rectangle element into canvas
            jQuery(canvas).append(loadElement);

        }

        if (savedElement.type !== "background-image" && savedElement.type !== "background-html") { //ignore background image and html; already loaded
            //create custom div element based on properties of current rectangle
            loadElement = document.createElement('div');
            loadElement.className = "rectangle";
            jQuery(loadElement).data('type', savedElement.type);
            jQuery(loadElement).css('left',savedElement.left);
            jQuery(loadElement).css('top',savedElement.top);
            loadElement.style.width = savedElement.width;
            loadElement.style.height = savedElement.height;
            //jQuery(loadElement).css('backgroundColor', savedElement.backgroundColor);
            //jQuery(loadElement).data('backgroundColor', savedElement.backgroundColor);
            //jQuery(loadElement).css('opacity', savedElement.opacity);
            //jQuery(loadElement).data('opacity', savedElement.opacity);

            //draggable prevents Chrome user from accidentally highlighting text in element
            loadElement.draggable = "true";
            //add rectangle methods
            addRectangleMethods(loadElement);

            //if text element or image element without assigned background image: create text element containing placeholder text/styling
            if (savedElement.backgroundImage == undefined || savedElement.type == "text") {
                createTextElement(); //see CJM.js
            } else {//if image element with assigned background image
                jQuery(loadElement).addClass("blue");
                //create image object
                var imageObject = document.createElement("IMG");
                imageObject.src = savedElement.backgroundImage.substring(5, savedElement.backgroundImage.length - 2);

                jQuery(loadElement).data('href', savedElement.href);
                if (jQuery(loadElement).data('href') == "true") {
                    jQuery(loadElement).data("hyperlink", savedElement.hyperlink);
                    //create anchor element
                    var anchorElement = document.createElement("a");
                    jQuery(anchorElement).attr("href", savedElement.hyperlink);
                    jQuery(anchorElement).attr("onclick","return false"); //don't make hyperlinks accidentally clickable
                    jQuery(anchorElement).attr("target","_blank"); // make anchor open in new window
                    //append image object to anchor element, then append anchor to div
                    jQuery(anchorElement).append(imageObject);
                    jQuery(loadElement).append(anchorElement);
                } else {//if not hyperlink, insert image element into div
                    //add a class to show that user text has been added
                    jQuery(loadElement).addClass("edited")
                    jQuery(loadElement).append(imageObject);
                }
                jQuery(loadElement).data('backgroundImage', savedElement.backgroundImage);
            }

            //insert finalised div element into canvas
            jQuery(canvas).append(loadElement);
        }
    }

    //--------------------------------USER INPUT DATA----------------------------------------------
    //automatically updates usertext and hyperlink data for active rectangle if changed
    function updateProperty(identifier) {
        jQuery(activeElement).data(jQuery(identifier).attr('id'), jQuery(identifier).val());
        if (jQuery(identifier).attr('id') == "usertext") {
            //selects text node depending on whether or not element contains anchor element
            if (jQuery(activeElement).data('href') == "true") {
                activeTextNode = jQuery(activeElement).children().children().contents().last()[0];
            } else {
                activeTextNode = jQuery(activeElement).contents().last()[0];
            }
            //if no text entered by user, replace text content of element with placeholder text; else use user-specified text
            if (jQuery('#usertext').val() == "") {
                //add a class to show that user text has been added
                jQuery(activeElement).removeClass("edited")
                activeTextNode.textContent = jQuery(activeElement).data('placeholder');
            } else {
                //add a class to show that user text has been added
                jQuery(activeElement).addClass("edited")
                activeTextNode.textContent = jQuery('#usertext').val();
            }
        }
        if (jQuery(identifier).attr('id') == "hyperlink") {
            //if no hyperlink supplied: delete hyperlink by removing href attribute
            if (jQuery('#hyperlink').val() == "") {
                jQuery(activeElement).children().removeAttr("href");
            } else {// else assign value to href
                jQuery(activeElement).children().attr("href", jQuery('#hyperlink').val());
            }
        }
    }

    //--------------------------MISCELLENOUS METHODS-------------------------
    //changes border of selected element to solid, shows usertext/hyperlink fields
    function selectRectangle(element) {
        activeElement = element;
        hideSections();
        if (jQuery(activeElement).data('type') == "text") {
            jQuery(activeElement).addClass('selectedRed');
            jQuery("#usertextSection").show();
            jQuery("#usertextSection").css('visibility', 'visible');
            jQuery('#fileSection').hide();
        } else {
            jQuery(activeElement).addClass('selectedBlue');
            jQuery("#usertextSection").hide();
            jQuery('#fileSection').show();
            jQuery('#fileSection').css('visibility', 'visible');
            jQuery("[name='targetWidth']").val(jQuery(activeElement).css('width').replace(/px/, ''));
            jQuery("[name='targetHeight']").val(jQuery(activeElement).css('height').replace(/px/, ''));
        }
    }

    //hides all fields from page
    function hideSections() {
        jQuery("#usertextSection").css('visibility', 'hidden');
        jQuery('#hyperlinkSection').css('visibility', 'hidden');
        jQuery('#fileSection').css('visibility', 'hidden');
    }

    //deletes all rectangles from canvas
    function deleteAll() {
        jQuery(canvas).find('.rectangle').remove();
        hideSections();
    }

    //resets cursor back to initial values
    function resetCursors() {
        jQuery(canvas).css('cursor', 'default');
        jQuery(canvas).find('.rectangle').css('cursor', 'pointer');
    }

    //----------------------------SAVE TEMPLATE-----------------------------------------
    //save changes to current page, then save everything to database
    function saveCustomizedTemplate(btn) {
        var customTitle = jQuery('#CJMlabel').val();
        var customDescription = jQuery('#CJMdescription').val();

        //prevent user from submitting empty string for template label/filename
        if (customTitle == "") {
            alert("phr_CJM_NoTitleWarning");
            return;
        }

        var templateJSON = saveToTemplate(); //see CJM.js
        var canvasJSON = buildCanvasObj();

        // postURL is the webservice that is being called
        var postURL = '/webservices/callwebservice.cfc?wsdl&method=callWebService&returnformat=json&_cf_nodebug=false';
        // extend postURL with relevant parameters that describe the webservice
        postURL += '&webservicename=relayCJMWS&methodname=saveCustomizedTemplate';

        jQuery(btn).spinner({position:'right'});
        <cfoutput>
            // call the jQuery Ajax to process the webservice
            var result = jQuery.ajax({
                url: postURL,
                type:'post',
                data:{cjmTemplateID:'#CJMTemplateID#',cjmCustomizedTemplateID:cjmCustomizedTemplateID,data:templateJSON,canvasBody:canvasJSON,customTitle:customTitle,customDescription:customDescription} <!--- 2015-11-10     ACPK   Add canvasBody, customTitle, and customDescription to POST request --->
            }).done(function(data){
                jQuery(btn).removeSpinner();
                resultJSON = JSON.parse(data);
                templateJSON = resultJSON.data
                cjmCustomizedTemplateID=resultJSON.CJMCUSTOMIZEDTEMPLATEID;
            }).fail(function(data){
                jQuery(btn).removeSpinner();
            });
        </cfoutput>
    }

    //-----------------------------EXPORT TEMPLATE----------------------------------------
    function exportCustomizedTemplate(btn,exportType) {
    	var fileName = jQuery('#CJMlabel').val();
    	var JSONoutput = saveToTemplate(); //see CJM.js
        var canvasJSON = buildCanvasObj();

    	//prevent user from submitting empty string for file name/title
        if (fileName == "") {
            alert("phr_CJM_NoTitleWarning");
            return;
        }

    	//if exporting to image, check for any div elements with hyperlink property
        if (exportType == 'png') {
            if (JSONoutput.indexOf('"href":"true"') !== -1) {//if hyperlink property found, display confirmation message
                if (!confirm('phr_cjm_imageNoURLWarning')) {//if user presses cancel, function terminates
                    return;
                }
            }
        }

        //Add spinner to the button object passed in and make it disabled
        jQuery(btn).attr('disabled','disabled');
        jQuery(btn).spinner({position:'left',size:'small'});

        // Call message web service to tell user something is happening
        // postURL is the webservice that is being called
        var messagePostURL = '/webservices/callwebservice.cfc?wsdl&method=callWebService&returnformat=plain&_cf_nodebug=false';
        // extend postURL with relevant parameters that describe the webservice
        messagePostURL += '&webservicename=relayUIWS&methodname=message';

        // call the jQuery Ajax to process the webservice
        var result = jQuery.ajax({
            url: messagePostURL,
            type:'get',
            data:{message:"phr_CJM_ExportPleaseWait",type:"info"}
        }).done(function(data){
            jQuery('#message').html(data);
            window.scrollTo(0,0);
            if (window.frameElement) {
                window.parent.scrollTo(0,0);
            }
            //put the page back to the page currently selected.
            changePage(jQuery('#pageNumber option:selected').val())
        });

        // postURL is the webservice that is being called
        var postURL = '/webservices/callwebservice.cfc?wsdl&method=callWebService&returnformat=json&_cf_nodebug=false';
        // extend postURL with relevant parameters that describe the webservice
        postURL += '&webservicename=relayCJMWS&methodname=exportTemplate';

        <cfoutput>
            // call the jQuery Ajax to process the webservice
            var result = jQuery.ajax({
                url: postURL,
                type:'post',
                data:{canvasObj:canvasJSON,exportType:exportType,fileName:fileName} <!--- 2015-11-10    ACPK    PROD2015-24 Added filename to POST request --->
            }).done(function(data){
                resultJSON = JSON.parse(data);

                //remove spinner and re-enable the button
                jQuery(btn).removeSpinner();
                jQuery(btn).removeAttr('disabled');

                //replace message with link to exported document
                jQuery('##message').html('<a href="' + resultJSON.EXPORTEDDOC + '" target="_blank">phr_CJM_ExportFinished</a>');

            }).fail(function(data){
                console.log("Error exporting template.");
            });
        </cfoutput>
    }

    <!--- Build the canvas object for all pages of the template and return it as a string --->
    function buildCanvasObj() {
       //** this must run first before we stop all the images loading **//
        //loop through pages and build a canvasObj of each page
        var canvasObj = {};
        var currentpage = 1;
        var initialpage = jQuery('#pageNumber').val(); //number of last viewed page

        //if user confirms or no div elements with hyperlink property found, function continue
        jQuery(Object.keys(pages)).each(function (){
            changePage(currentpage);
            var canvas = jQuery('#iframe').contents().find('#canvas');
            var canvasWidth =  canvas[0].clientWidth;
            var canvasHeight = canvas[0].clientHeight;
            var canvasBody = jQuery('#iframe').contents().find('body').html().replace(/[<]/g,'&#60;').replace(/[>]/g,'&#62;');

            canvasObj[currentpage] = {
                //save page Width to pagesObj
                'width':canvasWidth,
                //save page Height to pagesObj
                'height':canvasHeight,
                //save page Canvas to pagesObj
                //'canvas':iframeElement.contents().find('#loaderCanvas').html().replace(/onclick[A-Za-z]*?\=\".*?\"/gi,"")
                'canvas':canvasBody.replace(/onclick[A-Za-z]*?\=\".*?\"/gi,"")
            }

            currentpage ++; //move to next page
        });

        //** now stop the page loading **//
        if(typeof window.stop === 'undefined'){
            document.execCommand("Stop");
        }else{
            window.stop();
        }
        changePage(initialpage); //reset canvas view back to initial page

        return JSON.stringify(canvasObj);
    }
</script>