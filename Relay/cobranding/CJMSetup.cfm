<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			CJMSetup.cfm
Author:				ACPK & YMA
Date started:		2014-12-01

Description:		Setup CJM.

Date (DD-MMM-YYYY)	Initials 	What was changed
Amendment History:
2015-02-26			ACPK		FIFTEEN-239 Fixed alignment of custom size input fields in IE11
2015-12-02          ACPK        PROD2015-468 Added removeTopMargin parameter to resizeImage() so thumbnails appear aligned with row data in Co-branded Literature table
2015-12-08          ACPK        PROD2015-450 Moved hyperlinks for opening templates from ID to Title
2015-01-26			RJT			BF-267 Resolved issue where all file types were accepted not just the approved image types
Date (DD-MMM-YYYY)	Initials 	What was changed
Possible enhancements:


 --->
<!--- 2015/05/22	YMA	CJM Template upload is timing out on customer environments so increased timeout to 5 minutes.
					The cause of the timeout is application.com.relayimage.resizeImage() future task to steamline this function. --->

<cfsetting requestTimeOut="300">
<cf_title>phr_CJM_templatesetup</cf_title>
<cfparam name="getData" type="string" default="foo">
<cfparam name="sortOrder" default="title_defaultTranslation">
<cfparam name="numRowsPerPage" type="numeric" default="100">
<cfparam name="startRow" type="numeric" default="1">
<cfparam name="variables.showTheseColumns" default="title,approvalEngineEntityType,maxLevel,numApprovers,numRequestsMade"/>

<cfif not structKeyExists(url,"editor")>
	<CF_RelayNavMenu pageTitle="phr_CJM_templatesetup" thisDir="" navInclude="">
			<CF_RelayNavMenuItem MenuItemText="phr_Add" CFTemplate="CJMSetup.cfm?editor=yes&add=yes&entitytypeID=#application.entityTypeID['CJMTemplate']#">
			<CF_RelayNavMenuItem MenuItemText="phr_Excel" CFTemplate="javascript:openAsExceljs()">
	</CF_RelayNavMenu>
<cfelse>
	<cf_RelayNavMenu>
		<cf_RelayNavMenuItem MenuItemText="phr_Save" CFTemplate="##" onclick="getJSONFromTemplateDesigner(this);">
		<cfif not structKeyExists(url,"CJMTemplateID")><cf_relayNavMenuItem MenuItemText="phr_Back" CFTemplate="CJMSetup.cfm"></cfif>
	</cf_RelayNavMenu>
</cfif>

<cffunction name="postSave" returntype="struct">
	<cfset var result = {isOK=true,message="",imageURL={}}>
	<cfset var updateImageName = "">
	<cfset var fileExt = "">
	<cfset var fileSource = "">
	<cfset var fileDirectory = "">
	<cfset var relativeSource = "">
	<cfset var relativeDirectory = "">
	<cfset var PDFinfo = "">
	<cfset var convertedImage = "">
	<cfset var jsonStoredFiles = {}>
	<cfset var jsonContent = {}>
	<cfset var unzippedFilesHTML = "">
	<cfset var orientation = 'portrait'>
	<cfset var imageWidth = "">
	<cfset var imageHeight = "">

	<cfif structKeyExists(arguments,"filesUploaded") and arguments.filesUploaded neq "">
		<!--- process the uploaded template ready for the designer --->
		<cftry>
			<cfset fileExt = listLast(uploadResult.filesUploaded,'.')>
			<cfset fileSource = "#application.paths.userfiles##uploadResult.filesUploaded#" />
			<Cfset relativeDirectory = listDeleteAt(uploadResult.filesUploaded,listLen(uploadResult.filesUploaded,"/"),"/") & "/" />
			<Cfset relativeFileSource = "#relativeDirectory##uploadResult.filesUploaded#" />
			<cfset fileDirectory = listDeleteAt(fileSource,listLen(fileSource,"/"),"/") & "/" />

			<cfset jsonStoredFiles['originalFile'] = {fileSource=fileSource,fileExt=fileExt,fileDirectory=fileDirectory,relativeDirectory=relativeDirectory,relativeFileSource=relativeFileSource}>

			<cfif fileExt EQ "pdf">
				<cfpdf action="getinfo" name="PDFinfo" source="#fileSource#" />

				<cfloop from="1" to="#PDFinfo.TotalPages#" index="i">
					<cfset convertedImage = application.com.relayImage.convertPdfToImage(source=fileSource,destination=fileDirectory,page=i,width=form.outputWidth,height=form.outputHeight,minWidth=600,minHeight=600)>
					<cfset result.imageURL[i] = "#fileDirectory#/#convertedImage#">

					<cfset imageWidth = application.com.relayImage.getImageWidth("#fileDirectory##convertedImage#")>
					<cfif imageWidth gt round(imageWidth)>
						<cfset imageWidth = round(imageWidth)+1>
					<cfelse>
						<cfset imageWidth = round(imageWidth)>
					</cfif>
					<cfset imageHeight = application.com.relayImage.getImageHeight("#fileDirectory##convertedImage#")>
					<cfif imageHeight gt round(imageHeight)>
						<cfset imageHeight = round(imageHeight)+1>
					<cfelse>
						<cfset imageHeight = round(imageHeight)>
					</cfif>

					<cfset jsonStoredFiles['pages'][i] = {templateSource="pdf",fileName=convertedImage,fileSource=result.imageURL[i],fileDirectory=fileDirectory,relativeDirectory=relativeDirectory,relativeFileSource="#relativeDirectory#/#convertedImage#"}>

					<cfset jsonContent[i][0] = {"type"='background-image',"imageURL"="#relativeDirectory#/#convertedImage#","width"="#imageWidth#px","height"="#imageHeight#px"}>
				</cfloop>
			<!--- if ZIP file, unzip to temp folder, convert index.html via PDF to PNG format, then delete temp folder --->
			<cfelseif fileExt EQ "zip">
				<cfset unzippedFolder = application.com.filemanager.unzipFile(fileAndPath=fileSource,destination="#fileDirectory#\unzipped")>
				<cfset unzippedFilesHTML = DirectoryList("#fileDirectory#\unzipped",false,'name','*.html')>
				<!--- if sizes were not defined default to 1024x1024 --->
				<cfif form.outputWidth eq "">
					<cfset form.outputWidth = 1024>
				</cfif>
				<cfif form.outputHeight eq "">
					<cfset form.outputHeight = 768>
				</cfif>
				<cfsavecontent variable="unzippedHTMLContent">
					<cfinclude template="#relativeDirectory#/unzipped/#unzippedFilesHTML[1]#" />
				</cfsavecontent>

				<cfset unzippedHTMLContent = reReplace(unzippedHTMLContent,"\n","","ALL")><!--- remove line breaks --->
				<cfset unzippedHTMLContent = reReplace(unzippedHTMLContent,"\t","","ALL")><!--- remove tabs --->
				<cfset unzippedHTMLContent = reReplace(unzippedHTMLContent,"\r","","ALL")><!--- remove caraige returns --->
				<!--- <cfset unzippedHTMLContent = reReplace(unzippedHTMLContent,"<head>(?:.)+?</head>","","ALL")> ---><!--- remove head tag --->
				<cfset unzippedHTMLContent = application.com.regExp.replaceHTMLTagsInString(inputString=unzippedHTMLContent,htmlTag="head")>
				<cfset unzippedHTMLContent = reReplace(unzippedHTMLContent,"<html>","","ALL")><!--- remove head tag --->
				<cfset unzippedHTMLContent = reReplace(unzippedHTMLContent,"</html>","","ALL")><!--- remove head end tag --->
				<cfset unzippedHTMLContent = reReplace(unzippedHTMLContent,"<body>","","ALL")><!--- remove body tag --->
				<cfset unzippedHTMLContent = reReplace(unzippedHTMLContent,"</body>","","ALL")><!--- remove body end tag --->

				<cfset unzippedHTMLContent = rereplace(unzippedHTMLContent,' src="',' src="#relativeDirectory#/unzipped/','all')>
				<cfdocument userAgent="html" name="convertedPDF" localURL="no" format="PDF" overwrite="true" unit="in" pageType="custom" pageHeight="#form.outputHeight/300#" pageWidth="#form.outputWidth/300#" margintop="0" marginbottom="0" marginright="0" marginleft="0">
					<cfoutput>#unzippedHTMLContent#</cfoutput>
				</cfdocument>
				<cfset convertedImage = application.com.relayImage.convertPdfToImage(source=convertedPDF,destination=fileDirectory,width=form.outputWidth,height=form.outputHeight,minWidth=600,minHeight=600) />
				<cfset result.imageURL[1] = "#fileDirectory#/#convertedImage#" />

				<cfset imageWidth = application.com.relayImage.getImageWidth("#fileDirectory##convertedImage#")>
					<cfif imageWidth gt round(imageWidth)>
						<cfset imageWidth = round(imageWidth)+1>
					<cfelse>
						<cfset imageWidth = round(imageWidth)>
					</cfif>
				<cfset imageHeight = application.com.relayImage.getImageHeight("#fileDirectory##convertedImage#")>
					<cfif imageHeight gt round(imageHeight)>
						<cfset imageHeight = round(imageHeight)+1>
					<cfelse>
						<cfset imageHeight = round(imageHeight)>
					</cfif>

				<cfset jsonStoredFiles['pages'][1] = {templateSource="html",fileName=convertedImage,fileSource="#fileDirectory##convertedImage#",fileDirectory=fileDirectory,relativeDirectory=relativeDirectory,relativeFileSource="#relativeDirectory##convertedImage#"}>
				<cfset jsonContent[1][0] = {"type"='background-html',"html"="#unzippedHTMLContent#","width"="#imageWidth#px","height"="#imageHeight#px"}>
				<!--- <cfset DirectoryDelete("#fileDirectory#/unzipped", true) /> --->
			<!--- otherwise, use uploaded image file itself --->
			<cfelse>
				<!--- ensure image meets minimum sizes --->
				<cfset var origImageWidth = application.com.relayImage.getImageWidth(fileSource)>
				<cfset var origImageHeight = application.com.relayImage.getImageHeight(fileSource)>
				<cfset imageWidth = origImageWidth>
				<cfset imageHeight = origImageHeight>
				<cfif origImageWidth lt origImageHeight AND imageWidth lt 600>
					<cfset scalingFactor = 600/imageWidth>
					<cfset imageWidth = 600>
					<cfset imageHeight = imageHeight * scalingFactor>
					<cfif imageHeight gt round(imageHeight)>
						<cfset imageHeight = round(imageHeight)+1>
					<cfelse>
						<cfset imageHeight = round(imageHeight)>
					</cfif>
				</cfif>
				<cfif origImageHeight lt origImageWidth AND imageHeight lt 600>
					<cfset scalingFactor = 600/imageHeight>
					<cfset imageHeight = 600>
					<cfset imageWidth = imageWidth * scalingFactor>
					<cfif imageWidth gt round(imageWidth)>
						<cfset imageWidth = round(imageWidth)+1>
					<cfelse>
						<cfset imageWidth = round(imageWidth)>
					</cfif>
				</cfif>
				<cfif imageHeight gt origImageHeight OR imageWidth gt origImageWidth>
					<cfset application.com.relayImage.resizeImage(targetWidth=imageWidth,targetHeight=imageHeight,sourcePath=fileDirectory,sourceName=listlast(uploadResult.filesUploaded,"/"))>
				</cfif>

				<cfset result.imageURL[1] = fileSource>
				<cfset jsonStoredFiles['pages'][1] = {templateSource="image",fileName=listlast(uploadResult.filesUploaded,"/"),fileSource=fileSource,fileDirectory=fileDirectory,relativeDirectory=relativeDirectory,relativeFileSource="#relativeDirectory##listlast(uploadResult.filesUploaded,'/')#"}>
				<cfset jsonContent[1][0] = {"type"='background-image',"imageURL"="#relativeDirectory##listlast(uploadResult.filesUploaded,'/')#","width"="#imageWidth#px","height"="#imageHeight#px"}>
			</cfif>

			<!--- create thumbnail from first page --->
			<cfset jsonStoredFiles['thumbnail'] = {fileName="#arguments.CJMTemplateID#_thumb.png",
													fileSource="#jsonStoredFiles['pages'][1].fileDirectory##arguments.CJMTemplateID#_thumb.png",
													fileDirectory="#jsonStoredFiles['pages'][1].fileDirectory#",
													relativeDirectory="#jsonStoredFiles['pages'][1].relativeDirectory#",
													relativeFileSource="#jsonStoredFiles['pages'][1].relativeDirectory##arguments.CJMTemplateID#_thumb.png"}>
			<cffile action ="copy" source = "#jsonStoredFiles['pages'][1].fileSource#" destination = "#jsonStoredFiles['thumbnail'].fileSource#">

			<!--- 2015-12-02  ACPK    Added removeTopMargin parameter so thumbnails appear aligned with row data in Co-branded Literature table --->
			<cfset thumbnail = application.com.relayimage.resizeImage(targetWidth=80,targetHeight=80,sourcePath="#jsonStoredFiles['thumbnail'].fileDirectory#",sourceName="#jsonStoredFiles['thumbnail'].fileName#",removeTopMargin="true")>

			<cfquery name="updateImageName" datasource="#application.siteDataSource#">
				update CJMTemplate set
					<!--- set thumbnail image in db --->
					CJMTemplateThumb =  <cf_queryparam value="#jsonStoredFiles["thumbnail"].relativeFileSource#" CFSQLTYPE="CF_SQL_VARCHAR" >,
					<!--- update database copy of jsonStoredFiles --->
					jsonStoredFiles =  <cf_queryparam value="#SerializeJSON(jsonStoredFiles)#" CFSQLTYPE="CF_SQL_VARCHAR" >,
					<!--- pre-load jsonContent for all pages --->
					jsonContent =  <cf_queryparam value="#SerializeJSON(jsonContent)#" CFSQLTYPE="CF_SQL_VARCHAR" >
				where CJMTemplateID = <cf_queryParam value="#arguments.CJMTemplateID#" cfsqltype="cf_sql_integer">
			</cfquery>

			<cfcatch>
				<cfset result.isOk=false>
				<cfset result.message=cfcatch.message>
			</cfcatch>
		</cftry>

	</cfif>

	<cfreturn result>
</cffunction>
<script>
	function deleteTemplate (CJMTemplateID) {
		if(confirm("phr_CJM_DeleteConfirmation " + CJMTemplateID)){
			// postURL is the webservice that is being called
			var postURL = '/webservices/callwebservice.cfc?wsdl&method=callWebService&returnformat=json&_cf_nodebug=false';
			// extend postURL with relevant parameters that describe the webservice
			postURL += '&webservicename=relayCJMWS&methodname=deleteTemplate';

			// call the jQuery Ajax to process the webservice
			var result = jQuery.ajax({
				url: postURL,
				type:'get',
				data:{cjmTemplateID:CJMTemplateID}
			}).done(function(data){
				resultJSON = JSON.parse(data);
				var type = "success";
				var message = JSON.parse(data).MESSAGE;
				if(JSON.parse(data).isOK == false){
					type = "error"
				}else{
					jQuery(".CJMTemplateID a:contains('" + CJMTemplateID + "')").closest('tr').remove();
				}

				// postURL is the webservice that is being called
				var messagePostURL = '/webservices/callwebservice.cfc?wsdl&method=callWebService&returnformat=plain&_cf_nodebug=false';
				// extend postURL with relevant parameters that describe the webservice
				messagePostURL += '&webservicename=relayUIWS&methodname=message';

				// call the jQuery Ajax to process the webservice
				var result = jQuery.ajax({
					url: messagePostURL,
					type:'get',
					data:{message:message,type:type}
				}).done(function(data){
					jQuery('#message').html(data);
					jQuery('tr[id*='+CJMTemplateID+']').remove();
					window.scrollTo(0,0);
					if (window.frameElement) {
						window.parent.scrollTo(0,0);
					}
				});
			}).fail(function(data){
				console.log("Error deleting template.");
			});
		}
	}

	function getJSONFromTemplateDesigner (buttonObj) {
		<!--- run saveToTemplate in the iframe --->
		<!--- RJT the below appears to only work for editing, the old conditions may have been trying to look for this, added jQuery('#jsonContent').length!=0 to detect successfully  --->
		if(jQuery('#jsonContent').length!=0 && jQuery('label[for="CJMTemplateID"] + p').html() != '' && jQuery('label[for="CJMTemplateID"] + p').html() != 0){
			var saveJson = saveToTemplate();

			<!--- set value of jsonContent to the json returned above --->
			jQuery('#jsonContent').val(saveJson);
		}
		<!--- continue saving the form --->
		FormSubmit(buttonObj);
	}
</script>
<cfsavecontent variable="xmlSource">
	<cfoutput>
	<editors>
		<editor id="505" name="thisEditor" entity="CJMTemplate" title="phr_CJM_templatesetup" onsubmit="false;">
			<group label="phr_CJM_templateConfiguration">
				<field name="CJMTemplateID" label="phr_CJM_CJMTemplateID" description="The current template's unique ID." control="html"></field>
				<field name="" CONTROL="TRANSLATION" Parameters="phraseTextID=title" required="true" label="phr_CJM_CJMTemplatelabel" description="The template's' label." control="text"></field>
				<field nullText="phr_ext_selectavalue" shownullvalue="true" name="CJMTemplateTypeID" label="phr_CJM_CJMtemplateType" description="" control="select"
					query="select CJMTemplateTypeID as value, CJMTemplateType as display from CJMTemplateType order by CJMTemplateTypeID asc"></field>
				<field name="" CONTROL="TRANSLATION" Parameters="phraseTextID=description,displayAs=TextArea" label="phr_CJM_CJMTemplatedesc" control="text"></field>
				<field name="Printable" label="phr_cjm_CJMTemplatePrintable" control="checkbox"/>
				<field name="PDFable" label="phr_cjm_CJMTemplatePDFable" control="checkbox"/>
				<field name="Webable" label="phr_cjm_CJMTemplateWebable" control="checkbox"/>
				<field name="Active" label="phr_cjm_CJMTemplateActive" control="checkbox" condition="currentRecord.CJMTemplateID neq 0 and currentRecord.CJMTemplateID neq ''"/>

				<field name="" label="phr_CJM_restrictToCountries" description="" control="countryScope"></field>
				<field name="" label="phr_CJM_restrictToUserGroups" description="" control="recordRights" suppressUserGroups="false"></field>

				<field name="printSize" condition="currentRecord.CJMTemplateID eq 0 or currentRecord.CJMTemplateID eq ''" spanCols="true" control="html">
					<cfsavecontent variable="printSize">
						<cfoutput>
							<div class="form-group">
								<label>phr_CJM_OutputSize</label>
								<script>
									function updateOutputFormat() {
										if (jQuery('##outputFormat').find(':selected').hasClass('customSize')) {
											jQuery('##propertiesSection').show();
										} else {
											jQuery('##propertiesSection').hide();
										}

										var outputObj = {width:'',height:''}
										var $_outputFormat = jQuery('##outputFormat').val();
										if ($_outputFormat != '')
											outputObj = jQuery.parseJSON($_outputFormat);
										jQuery('##outputWidth').val(outputObj.width);
										jQuery('##outputHeight').val(outputObj.height);
									}

									function validateSize(obj){
										if(jQuery(obj).val() > 4961){
											alert('phr_cjm_maxSize4961');
											jQuery(obj).val(4961);
										}
										if(jQuery(obj).val() < 600){
											alert('phr_cjm_minSize600');
											jQuery(obj).val(600);
										}
									}
								</script>
								<select id="outputFormat" onchange="updateOutputFormat()" class="form-control">
									<option selected="selected" value="">phr_cjm_chooseavalue</option>
									<option class="customSize" value='{"width":"", "height":""}'>phr_cjm_printCustomSize</option>
									<option value='{"width":4961, "height":3508}'>phr_cjm_printA3Landscape</option>
									<option value='{"width":3508, "height":4961}'>phr_cjm_printA3Portrait</option>
									<option value='{"width":3508, "height":2480}'>phr_cjm_printA4Landscape</option>
									<option value='{"width":2480, "height":3508}'>phr_cjm_printA4Portrait</option>
									<option value='{"width":2480, "height":1748}'>phr_cjm_printA5Landscape</option>
									<option value='{"width":1748, "height":2480}'>phr_cjm_printA5Portrait</option>
									<option value='{"width":1748, "height":1240}'>phr_cjm_printA6Landscape</option>
									<option value='{"width":1240, "height":1784}'>phr_cjm_printA6Portrait</option>
									<option value='{"width":1240, "height":874}'>phr_cjm_printA7Landscape</option>
									<option value='{"width":874, "height":1240}'>phr_cjm_printA7Portrait</option>
									<option value='{"width":2400, "height":1500}'>phr_cjm_JuniorLegalLandscape</option>
									<option value='{"width":1500, "height":2400}'>phr_cjm_JuniorLegalPortrait</option>
									<option value='{"width":3300, "height":2550}'>phr_cjm_LetterLandscape</option>
									<option value='{"width":2550, "height":3300}'>phr_cjm_LetterPortrait</option>
									<option value='{"width":4200, "height":2550}'>phr_cjm_LegalLandscale</option>
									<option value='{"width":2550, "height":4200}'>phr_cjm_LegalPortrait</option>
									<option value='{"width":5100, "height":3300}'>phr_CJM_TabloidLandscape</option>
									<option value='{"width":3300, "height":5100}'>phr_CJM_TabloidPortrait</option>
								</select>
							</div>

							<div class="form-group" id="propertiesSection" style="display:none;">

									<label>phr_CJM_printSize</label>

								<!--- 2015-02-26	ACPK	FIFTEEN-239 Fixed alignment of custom size input fields in IE11 --->
								<div style="display: table-cell;" class="customSizeProperties">
									<label>phr_cjm_Width<input name="outputWidth" id="outputWidth" type="text" onchange="validateSize(this);" /></label><br />
									<label>phr_cjm_Height<input name="outputHeight" id="outputHeight" type="text" onchange="validateSize(this);" /></label>
								</div>
							</div>
						</cfoutput>
					</cfsavecontent>
					#htmlEditFormat(printSize)#
				</field>

				<field condition="currentRecord.CJMTemplateID eq 0 or currentRecord.CJMTemplateID eq ''" name="" control="file" label="phr_cjm_TemplateSource" accept="jpeg,jpg,gif,pjpeg,png,pdf,zip" required="true" parameters="allowFileDelete=false" description=""></field>

			</group>
			<group label="phr_cjm_templateDesigner" condition="currentRecord.CJMTemplateID neq 0 and currentRecord.CJMTemplateID neq ''">
				<field name="templateDesigner" spanCols="true" control="html" condition="currentRecord.CJMTemplateID neq 0 and currentRecord.CJMTemplateID neq ''">
					<cfsavecontent variable="templateDesigner">
						<cfoutput>
							<link rel="stylesheet" media="screen" type="text/css" href="../javascript/lib/colorpicker/css/colorpicker.css" />
							<cf_includeJavascriptOnce template = "../javascript/lib/colorpicker/js/colorpicker.js">
							<div id="templateDesigner"></div>
							<script>
									//checks to see whether browser is IE8 or below
									function isIE8OrBelow() {
										if (window.navigator.userAgent.indexOf("MSIE") > 0 && !document.addEventListener) { //addEventListener not supported by IE until IE9+
											return true;
										} else {
											return false;
										}
									}

									if (!isIE8OrBelow()) {
										<!--- load the templateDesigner --->
										// postURL is the webservice that is being called
										var postURL = '/webservices/callwebservice.cfc?wsdl&method=callWebService&returnformat=plain&_cf_nodebug=false';
										// extend postURL with relevant parameters that describe the webservice
										postURL += '&webservicename=relayCJMWS&methodname=loadNewTemplateDesigner';

										// call the jQuery Ajax to process the webservice
										var result = jQuery.ajax({
											url: postURL,
											type:'get',
											data:{cjmTemplateID:jQuery('label[for="CJMTemplateID"] + p').html()}
										}).done(function(data){
											jQuery('##templateDesigner').html(data);
										}).fail(function(data){
											console.log("Error loading template designer");
										});
									} else {
										alert("phr_CJM_BrowserNotSupported");
									}

							</script>

						</cfoutput>
					</cfsavecontent>
					#htmlEditFormat(templateDesigner)#
				</field>
				<field name="jsonContent" spanCols="true" control="hidden"/>
			</group>

		</editor>
	</editors>
	</cfoutput>
</cfsavecontent>

<cfset getCJMTemplates = application.com.cjm.getCJMTemplate()>
<cfquery name="getCJMTemplates" dbtype="query">
	select *, '<span class="deleteLink">phr_delete</span>' as del from getCJMTemplates
	order by <cf_queryObjectName value="#sortOrder#">
</cfquery>

<!--- 2015-12-08    ACPK    PROD2015-450 Moved hyperlinks for opening templates from ID to Title --->
<cf_listAndEditor
	xmlSource="#xmlSource#"
	cfmlCallerName="CJMSetup"
	keyColumnList="cjmTitle,del"
	keyColumnKeyList=" , "
	keyColumnURLList=" ,##message"
	keyColumnOnClickList="javascript:openNewTab('phr_CJMTemplate ##jsStringFormat(CJMTemplateID)##'*comma'phr_CJMTemplate ##jsStringFormat(CJMTemplateID)##'*comma'/cobranding/CJMSetup.cfm?editor=yes&CJMTemplateID=##jsStringFormat(CJMTemplateID)##'*comma{reuseTab:true*commaiconClass:'communicate'});return false;,deleteTemplate(##jsStringFormat(CJMTemplateID)##);"
	showTheseColumns="CJMTemplateID,cjmTitle,CJMTemplateType,active,del"
	FilterSelectFieldList="CJMTemplateType"
	booleanFormat="active"
	useInclude="false"
	sortOrder="#sortOrder#"
	queryData="#getCJMTemplates#"
	numRowsPerPage="#numRowsPerPage#"
	startRow="#startRow#"
	postSaveFunction = #postSave#
	columnTranslation="true"
	columnTranslationPrefix="phr_CJM_"
	showsave="false"
	hideBackButton="true"
	rowIdentityColumnName="CJMTemplateID"
>