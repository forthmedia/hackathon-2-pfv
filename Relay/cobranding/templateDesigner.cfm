<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			templateDesigner.cfm
Author:				ACPK
Date started:		2014-11-21

Description:		Template layout designer

Amendment History:

Date (YYYY-MM-DD)	Initials 	What was changed
2014-11-24			ACPK		Added support for Chrome, IE and Safari; improved layout; misc bug fixes
2014-11-27			ACPK		Implemented temporary load function; added italic, underline, background colour and opacity settings for rectangle elements; rewrote major sections of code
2014-12-01			ACPK		Rearranged page elements to create initial toolbar layout, made drawing buttons toggleable
2014-12-01			ACPK		Implemented save/load integration with database, bug and UI fixes, cleaned up code
2014-12-02			ACPK		Added key binds, bug fixes/clean up
2014-12-03			ACPK		Added copy/paste functionality, doubleclicking element now sets focus to placeholder text field
2014-12-03			yan			change loadbackground image to use a .load function on the attr src change
2014-12-03			ACPK		Removed obsolete buttons and methods
2014-12-03			yan			bringing template designer functionality into CJMSetup page.
2014-12-08			ACPK		Top-left coordinates of rectangle elements now relative to canvas instead of parent window; cleanup; minor bugfixes
2014-12-09			ACPK		Reimplemented save/load integration with database, cleanup/bugfixes
2014-12-09			ACPK		Added zoom and scrolling functionality, moved CSS into newly created templatedesigner.css file
2015-02-24			ACPK		FIFTEEN-246 Replaced HTML5 input range object with jQuery UI Slider widget
2015-02-25			ACPK		FIFTEEN-257 Fixed non-clickable area not matching color picker
2015-08-13			ACPK		Fixed delete/resize buttons not appearing and contents of rectangle elements not updating in Safari
2016/02/25			NJH/SB		JIRA PROD2105-520 - remove height on backgroundHTML div

Possible enhancements:


 --->

<cf_includeCssOnce template="/cobranding/css/templateDesigner.css">
<cf_includejavascriptOnce template = "/javascript/lib/number-polyfill-master/number-polyfill.js">
<cf_includeCssOnce template="/javascript/lib/number-polyfill-master/number-polyfill.css">
<cf_includeCssOnce template="/javascript/CF/ajax/jquery/jquery-ui.css">

<cfparam name="CJMTemplateID">

<cfquery name="getJsonStoredFiles" datasource="#application.siteDataSource#">
	select jsonStoredFiles from CJMTemplate where CJMTemplateID = <cf_queryParam value="#CJMTemplateID#" cfsqltype="cf_sql_integer">
</cfquery>
<cfset jsonStoredFiles = deserializejson(getJsonStoredFiles.jsonStoredFiles)>

<cfoutput>
	<script>
		var pages = {};

		<cfloop collection="#jsonStoredFiles.pages#" item="page">
			<cfif not structKeyExists(jsonStoredFiles.pages[page],"templateSource") or (jsonStoredFiles.pages[page].templateSource neq "html")>
				pages[#page#] = '#jsonStoredFiles.pages[page].RELATIVEFILESOURCE#';
			</cfif>
		</cfloop>
	</script>
</cfoutput>
<!--- <cf_includejavascriptOnce template="/cobranding/js/cjm.js"> --->
<script src="/cobranding/js/cjm.js"></script> <!--- 2015/10/13	ACPK	This MUST be a script tag instead of cf_includejavascriptOnce due to timing issues --->


<!----------------------------------------------------------------HTML--------------------------------------------------------------->
<div id="templateDesignerWrapper">
	<div id="mainToolbar">
		<cfoutput>
			<select id="pageNumber" onchange="changePage(jQuery(this).val())"><!---see CJM.js--->
				<cfloop index="page" from="1" to="#structCount(jsonStoredFiles.pages)#">
					<option <cfif page eq 1>selected=selected</cfif> value="#page#">phr_page #page#</option>
				</cfloop>
			</select>
		</cfoutput>
		<button class="create unpressed" type="button" title="Draw new text area" onclick="startDrawing('text', this)">New Text Area</button>
		<button class="create unpressed" type="button" title="Draw new image" onclick="startDrawing('image', this)">New Image</button>
		<button id="copyButton" class="buttonNonActive" title="Copy selected element (Ctrl+C)" type="button" onclick="copyRectangle()" disabled="">Copy</button>
		<button id="pasteButton" class="buttonNonActive" title="Paste element (Ctrl+V)" type="button" onclick="pasteRectangle()" disabled="">Paste</button>
		<button title="Remove all rectangles from canvas" type="button" onclick="confirmDeleteAll()">Clear All</button>
		<!--- 2015-02-24	ACPK	FIFTEEN-246 Replaced HTML5 input range object with jQuery UI Slider widget --->
		<div id="zoom"><div id="zoomValue"></div></div>
	<div id="positionToolbar">
		<div id="positionSection">
				<!-- <div class="headerSection"><p><b>Position</b></p></div> -->
        <div class="headerSection"><label>Position</label></div>
				<div class="inputSection"><label for="left">Left:</label><input id="left" type="number" min="0" step="1" onchange="updateProperty(this)" oninput="updateProperty(this)" disabled="" /></div>
				<div class="inputSection"><label for="top">Top:</label><input id="top" type="number" min="0" step="1" onchange="updateProperty(this)" oninput="updateProperty(this)" disabled="" /></div>
			</div>
			<div id="proportionSection">
				<div class="headerSection"><label>Size</label></div>
				<div class="inputSection"><label for="width">Width:</label><input id="width" type="number" min="0" step="1" onchange="updateProperty(this)" oninput="updateProperty(this)" disabled="" /></div>
				<div class="inputSection"><label for="height">Height:</label><input id="height" type="number" min="0" step="1" onchange="updateProperty(this)" oninput="updateProperty(this)" disabled="" /></div>
			</div>
		</div>
	</div>
	<div id="textToolbar" class="textEditorNonActive">
		<div id="fontMenuSection">
			<select id="fontFamily" title="Font" onchange="updateProperty(this)" disabled="">
				<optgroup><option style="font-family:'Arial';" class="Arial" value="Arial" selected="selected">Arial</option></optgroup>
				<optgroup><option style="font-family:'Calibri';" class="Calibri" value="Calibri">Calibri</option></optgroup>
				<optgroup><option style="font-family:'Comic Sans MS';" class="Comic-Sans-MS" value="Comic Sans MS">Comic Sans MS</option></optgroup>
				<optgroup><option style="font-family:'Courier New';" class="Courier-New" value="Courier New">Courier New</option></optgroup>
				<optgroup><option style="font-family:'Georgia';" class="Georgia" value="Georgia">Georgia</option></optgroup>
				<optgroup><option style="font-family:'Impact';" class="Impact" value="Impact">Impact</option></optgroup>
				<optgroup><option style="font-family:'Lucida Sans Unicode';" class="Lucida-Sans-Unicode" value="Lucida Sans Unicode">Lucida Sans Unicode</option></optgroup>
				<optgroup><option style="font-family:'Tahoma';" class="Tahoma" value="Tahoma">Tahoma</option></optgroup>
				<optgroup><option style="font-family:'Times New Roman';" class="Times-New-Roman" value="Times New Roman">Times New Roman</option></optgroup>
				<optgroup><option style="font-family:'Trebuchet MS';" class="Trebuchet-MS" value="Trebuchet MS">Trebuchet MS</option></optgroup>
				<optgroup><option style="font-family:'Verdana';" class="Verdana" value="Verdana">Verdana</option></optgroup>
			</select>
			<input id="fontSize" type="number" onchange="updateProperty(this)" oninput="updateProperty(this)" min="1" max="2000" value="8" disabled="" />
			<div id="fontColorSelector" class="colorSelector" title="Font Color">
				<div id="fontColorSelectorInterior" class="colorSelectorInterior"></div>
			</div>
			<div id="fontColor" class="colorPickerPanel"></div>
		</div>
		<div id="fontStyleSection">
			<button type="button" id="updateBoldButton" class="fontButton unpressed" title="Bold (Ctrl+B)" onclick="updateButton(this)" disabled="">B</button>
			<button type="button" id="updateItalicButton" class="fontButton unpressed" title="Italic (Ctrl+I)" onclick="updateButton(this)" disabled="">I</button>
			<button type="button" id="updateUnderlineButton" class="fontButton unpressed" title="Underline (Ctrl+U)" onclick="updateButton(this)" disabled="">U</button>
		</div>
		<div id="textAlignSection">
			<button type="button" id="textAlignleft" class="alignButton pressed" title="Align Text Left (Ctrl+L)" value="left" onclick="changeAlignment(this)" disabled="">L</button>
			<button type="button" id="textAligncenter" class="alignButton unpressed" title="Align Text Center (Ctrl+E)" value="center" onclick="changeAlignment(this)" disabled="">C</button>
			<button type="button" id="textAlignright" class="alignButton unpressed" title="Align Text Right (Ctrl+R)" value="right" onclick="changeAlignment(this)" disabled="">R</button>
			<button type="button" id="textAlignjustify" class="alignButton unpressed" title="Align Text Justified (Ctrl+J)" value="justify" onclick="changeAlignment(this)" disabled="">J</button>
		</div>
		<!--- <div id="rectangleColorSection">
			<div id="backgroundColorSelector" class="colorSelector" title="Background Color">
				<div id="backgroundColorSelectorInterior" class="colorSelectorInterior"></div>
			</div>
			<div id="backgroundColor" class="colorPickerPanel"></div>
			<input id="opacity" title="Opacity" type="number" value="0.75" step="0.01" min="0.1" max="1" onChange="updateProperty(this)" />
		</div> --->
		<div id="placeholderSection">
			<label for="placeholder">Placeholder text </label><input id="placeholder" type="text" size="40" oninput="updateProperty(this)" disabled=""></input>
			<label for="href"><input id="href" type="checkbox" title="Indicates whether the selected rectangle has a URL" onchange="updateCheckbox(this)" disabled="" /> Hyperlink</label>
		</div>
	</div>
</div>
<iframe id="iframe"></iframe>

<script>
	//Initialising global variables
	var drawingMode = false;
	var dragMode = false;
	var resizingMode = false;
	var deleteMode = false;
	var scrollingMode = false;
	var element = null;
	var activeElement;
	var activeType;
	var activeFontColor = "#000000";
	//var activeBackgroundColor = "#FFFFFF"
	var fontColorPickerOpen = false;
	//var backgroundColorPickerOpen = false;
	var mouse = {
		x: 0,
		y: 0,
		startX: 0,
		startY: 0
	};
	var canvasLeft = 0;
	var canvasTop = 0;
	var canvasWidth = 0;
	var canvasHeight = 0;
	var templateObj = {};

	//--------------------------------------------SETUP------------------------------------------
	initiateCanvas();
	initiateKeyBinds();
	//For other browsers
	jQuery(document).ready(function() {
		if (jQuery.browser.webkit) {
			initiateCanvas();
			initiateKeyBinds();
		}
	});

	//Run these methods when page/iframe finishes loading to prepare initial canvas
	function initiateCanvas() {
		//IE 9 doesnt get a body tag inside the Iframe so we need to create it on the fly.
		if(typeof jQuery("#iframe").contents().find('body') == undefined || jQuery("#iframe").contents().find('body').length == 0){
			document.getElementById('iframe').contentWindow.document.write("<html><body></body></html>");
		}

		//insert canvas and its corresponding CSS properties/methods into iframe
		jQuery('#iframe').contents().find('html').css('transform-origin','0 0')
		jQuery("#iframe").contents().find('body').html('<div id="canvas" ondragstart="return false;"></div>') //ondragstart attribute required so rectangles can be dragged in non-Firefox browsers
		jQuery("#iframe").contents().find("head").append(jQuery("<link/>",
	        {rel: "stylesheet", href: "./css/templatedesigner.css", type: "text/css"}
	    ));
		canvas = jQuery("#iframe").contents().find('#canvas');
		if(pages[1] !== ''){ //dont try and load a background image if we didnt set one e.g. its a html template
			loadBackgroundImage({imageURL:pages[1],jsonTemplate:'',page:1,canvas:canvas});
		}
		addCanvasMethods();
		//---------------------------------WEBSERVICE-----------------------------------------------------
		// postURL is the webservice that is being called
		var postURL = '/webservices/callwebservice.cfc?wsdl&method=callWebService&returnformat=json&_cf_nodebug=false';
		// extend postURL with relevant parameters that describe the webservice
		postURL += '&webservicename=relayCJMWS&methodname=loadTemplateAttributes';

		// call the jQuery Ajax to process the webservice
		<cfoutput>var result = jQuery.ajax({
			url: postURL,
			type:'get',
			data:{cjmTemplateID:"#CJMTemplateID#"}
		}).done(function(data){
			if(jQuery.trim(data) !== '""'){
				templateObj = jQuery.parseJSON(data);
				loadRectangles(data,jQuery('##pageNumber').val());
			}
		});</cfoutput>

		//------------------------COLOR PICKER WIDGETS-----------------------------------
		//insert Font Color Picker widget (default = black)
		jQuery('#fontColor').ColorPicker({
			flat: true,
			color: '#000000',
			onChange: function (hsb, hex, rgb) {
				activeFontColor = '#' + hex;
				jQuery('#fontColorSelector div').css('backgroundColor', activeFontColor);
				jQuery(activeElement).contents().last().css('color', activeFontColor);
				jQuery(activeElement).data("fontColor", activeFontColor);
			}
		});

		/* un-supported by export functionality so removed
		//insert Background Color Picker widget (default = white)
		jQuery('#backgroundColor').ColorPicker({
			flat: true,
			color: '#FFFFFF',
			onChange: function (hsb, hex, rgb) {
				activeBackgroundColor = '#' + hex;
				jQuery('#backgroundColorSelector div').css('backgroundColor', activeBackgroundColor);
				jQuery(activeElement).css('backgroundColor', activeBackgroundColor);
				jQuery(activeElement).data('backgroundColor', activeBackgroundColor);
			}
		});*/

		//set up methods of both color selectors
		function setupColorSelectors(selectorIcon, selectorPanel, selectorMode) {
			//clicking color selector icon opens/closes color selector if toolbar active
			jQuery(selectorIcon).click(function(csToggle) {
				if (!jQuery("#textToolbar").hasClass("textEditorNonActive")) {
					csToggle.stopPropagation();
					if (!selectorMode) {
						jQuery(selectorPanel).show();
					} else {
						jQuery(selectorPanel).hide();
					} selectorMode = !selectorMode
				}
			});
			<!--- 2015-02-25	ACPK	FIFTEEN-257 Fixed non-clickable area not matching color picker --->
			//ensures that clicking on color selector itself won't close it
			jQuery(selectorPanel + " div").click(function(csToggle) {
				csToggle.stopPropagation();
			});
			//clicking anywhere outside color selector panel closes it
			jQuery(document).click(function() {
				if (selectorMode) {
					jQuery(selectorPanel).hide();
					selectorMode = false;
				}
			});
			jQuery("#iframe").contents().click(function() {
				if (selectorMode) {
					jQuery(selectorPanel).hide();
					selectorMode = false;
				}
			});
		}
		//set up and hide color pickers from view
		setupColorSelectors('#fontColorSelector', '#fontColor', fontColorPickerOpen);
		//setupColorSelectors('#backgroundColorSelector', '#backgroundColor', backgroundColorPickerOpen);
		jQuery("#fontColor").hide();
		//jQuery("#backgroundColor").hide();

		//apply initial CSS settings to cursors, buttons and font menu
		resetCursors();
		jQuery("#fontFamily optgroup").each(function(){ //chrome won't apply font styles to options directly, only to optgroups
			jQuery(this).css("font-family", jQuery(this).children('option').val());
		});
		jQuery("#fontFamily").css("font-family", jQuery("#fontFamily").val());
	}

	<!--- 2015-02-24	ACPK	FIFTEEN-246 Replaced HTML5 input range object with jQuery UI Slider widget --->
	//------------------------ZOOM SLIDER WIDGET-----------------------------------
	//use jQuery UI slider widget for zoom (all values in percentages)
	jQuery("#zoom").slider({
		min: 1, //minimum value
		max: 100, //maximum value
		step: 1, //incremental value
		value: 100, //default value on load
		start: function(event, ui){ //when user starts moving handle
			positionLabel(ui);
			jQuery("#zoomValue").show(); //show label
		},
		slide: function(event, ui){ //whenever user moves handle
			changeZoom(ui.value); //resize canvas to current value (see CJM.js)
			positionLabel(ui);
		},
		stop: function(){ //when user stops moving handle
			jQuery("#zoomValue").hide(); //hide label
		},
		create: function(event, ui){ //when widget is initialised
			jQuery("#zoomValue").hide(); //hide label
		}
	});

	//change value of label and position underneath slider handle
	function positionLabel(ui) {
		var handlePosition = jQuery(".ui-slider-handle").css("left").replace(/px/, ''); //get handle position
		var labelPosition = handlePosition - 5 + 'px';
		jQuery("#zoomValue").text(ui.value + "%").css("left", labelPosition);
	}

	//----------------------------------------KEY BINDS-------------------------------------------
	function initiateKeyBinds (){
		jQuery(document.getElementById('iframe').contentWindow.document).keydown(function (e) {
			//pressing backspace (8) or delete (46) deletes selected element when outside of input/text area, prevents user from accidentally navigating to previous page
	  		if ((e.which === 8 || e.which === 46) && !jQuery(e.target).is("input, textarea")) {
		        e.preventDefault();
		        jQuery(activeElement).remove();
				disableFields();
		    }
			//binding Ctrl + [key] keyboard shortcuts when outside input/text area
			if (e.ctrlKey && !jQuery(e.target).is("input, textarea")) {
				switch (String.fromCharCode(e.which).toLowerCase()) {
					case 'b': //toggle bold
						e.preventDefault();
						updateButton('#updateBoldButton');
						break;
					case 'i': //toggle italics
						e.preventDefault();
						updateButton('#updateItalicButton');
						break;
					case 'u': //toggle underline
						e.preventDefault();
						updateButton('#updateUnderlineButton');
						break;
					case 'l': //align text left
						e.preventDefault();
						changeAlignment(textAlignleft);
						break;
					case 'e': //align text center
						e.preventDefault();
						changeAlignment(textAligncenter);
						break;
					case 'r': //align text right
						e.preventDefault();
						changeAlignment(textAlignright);
						break;
					case 'j': //align text justified
						e.preventDefault();
						changeAlignment(textAlignjustify);
						break;
					case 'c': //copy selected element
						e.preventDefault();
						copyRectangle();
						break;
					case 'v': //paste element
						e.preventDefault();
						pasteRectangle();
						break;
				}
			}
		});
	}
	//----------------------------MOUSE METHODS-------------------------
	function setMousePosition(e) {
		<!--- 2015-02-24	ACPK	FIFTEEN-246 Replaced HTML5 input range object with jQuery UI Slider widget --->
		//get cursor position relative to top-left corner of canvas, scaled to match zoomed view
		mouse.x = (e.pageX - canvasLeft) * (100 / (jQuery("#zoom").slider("value")));
		mouse.y = (e.pageY - canvasTop) * (100 / (jQuery("#zoom").slider("value")));

		//if drawing rectangle: set width, height, left, and top properties using cursor position relative to coordinates of top-left corner
		if (drawingMode) {
			if (element !== null) {
				element.style.width = (mouse.x - mouse.startX) + 'px';
				element.style.height = (mouse.y - mouse.startY) + 'px';
				element.style.left = (mouse.x < 0) ? 0 + 'px' : mouse.startX + 'px';
				element.style.top = (mouse.y < 0) ? 0 + 'px' : mouse.startY + 'px';
				jQuery('#width').val(Math.round(element.style.width.replace(/px/, '')));
				jQuery('#height').val(Math.round(element.style.height.replace(/px/, '')));
				jQuery('#left').val(Math.round(element.style.left.replace(/px/, '')));
				jQuery('#top').val(Math.round(element.style.top.replace(/px/, '')));
			}
		}

		//if resizing rectangle: change only width and height properties using cursor position relative to top-left corner
		if (resizingMode) {
			element = activeElement;
			jQuery(canvas).css('cursor', 'nw-resize');
			jQuery(canvas).find('.rectangle').css('cursor', 'nw-resize');
			mouse.x = mouse.x - 20;
			mouse.y = mouse.y - 20;
			if (mouse.x > startX) {
				element.style.width = Math.abs(mouse.x - startX) + 'px';
				jQuery(element).contents().last().css('width', element.style.width);
				jQuery('#width').val(Math.round(element.style.width.replace(/px/, '')));
			}
			if (mouse.y > startY) {
				element.style.height = Math.abs(mouse.y - startY) + 'px';
				jQuery(element).contents().last().css('height', element.style.height);
				jQuery('#height').val(Math.round(element.style.height.replace(/px/, '')));
			}
		}

		//if dragging rectangle: change only left and top properties (ie co-ordinates of top-left corner) relative to position of mouse, and prevent user from dragging rectangle off canvas!
		if (dragMode && !resizingMode) {
			if ((mouse.x - differenceX < 0)) { //too far left
				element.style.left = 0 + 'px';
			} else if ((mouse.x - differenceX + elementWidth) > (canvasWidth)) { //too far right
				element.style.left = (canvasWidth - elementWidth) + 'px';
			} else {
				element.style.left = mouse.x - differenceX + 'px'
			}
			if ((mouse.y - differenceY < 0)) { //too far up
				element.style.top = 0 + 'px';
			} else if ((mouse.y - differenceY + elementHeight) > (canvasHeight)) { //too far down
				element.style.top = (canvasHeight - elementHeight) + 'px';
			} else {
				element.style.top = mouse.y - differenceY + 'px'
			}
			jQuery('#left').val(Math.round(element.style.left.replace(/px/, '')));
			jQuery('#top').val(Math.round(element.style.top.replace(/px/, '')));
		}
		//if dragging on canvas: change value of scrollbars
		if (scrollingMode) {
			iframeWindow = jQuery(jQuery('#iframe')[0].contentWindow);
			iframeWindow.scrollLeft(iframeWindow.scrollLeft() + (mouse.startX - (e.pageX - canvasLeft)));
			iframeWindow.scrollTop(iframeWindow.scrollTop() + (mouse.startY - (e.pageY - canvasTop)));
		}
	}
	//-----------------------------CANVAS METHODS------------------------------
	function addCanvasMethods() {
		//moving cursor over canvas gets its current position
		jQuery(canvas).mousemove(function (e) {
			setMousePosition(e);
		});

		// clicking on canvas deselects active element and clears placeholder text; draws new rectangle when in drawing mode and enters scrolling mode if not
		jQuery(canvas).mousedown(function (e) {
			if (!deleteMode) {
				deselectAll(); //(see CJM.js)
				disableFields();
				if (drawingMode) {
					drawRectangle();
				} else {
					scrollingMode = true;
					mouse.startX = e.pageX - canvasLeft;
					mouse.startY = e.pageY - canvasTop;
					jQuery(canvas).css('cursor', 'move');
					jQuery(canvas).find('.rectangle, .deleteButton, .resizeButton').css('cursor', 'move');
				}
			}
		});

		//releasing mouse button with cursor over canvas exits all modes, resets cursor
		jQuery(canvas).mouseup(function () {
			resetCursors();
			dragMode = false;
			resizingMode = false;
			scrollingMode = false;
			if (drawingMode) { //if drawing rectangle, select it
				selectRectangle(element);
				deactivateButton('.create');
			}
			element = null;
			drawingMode = false;
		});

		//---------------------DRAWING RECTANGLE--------------------------------------
		//creates div element with properties and methods
		function drawRectangle() {
			//set initial coordinates of top-left corner to current mouse position relative to canvas
			mouse.startX = mouse.x;
			mouse.startY = mouse.y;
			if (window.navigator.userAgent.indexOf("MSIE") > 0) { //in IE9, dragging only works with anchor tags with href="#", not div elements
				element = document.createElement('a');
				jQuery(element).attr('href', '#');
				jQuery(element).css('text-decoration', 'inherit');
			} else {
				element = document.createElement('div');
			}
			activeElement = element;
			//attaching appropriate initial data (doesn't appear in HTML source) and classes to element
			element.className = "rectangle";
			jQuery(element).data('type', activeType);
			if (activeType == "text") {
				element.title = "Text Box";
				jQuery(element).addClass("red"); //red = text area
				jQuery(element).data('fontFamily', jQuery("#fontFamily").val());
				jQuery(element).data('fontSize', jQuery("#fontSize").val() + 'px');
				if (jQuery("#updateBoldButton").hasClass("pressed")) {
					jQuery(element).data("fontWeight", "bold");
				} else {
					jQuery(element).data("fontWeight", "normal");
				}
				if (jQuery("#updateItalicButton").hasClass("pressed")) {
					jQuery(element).data("fontStyle", "italic");
				} else {
					jQuery(element).data("fontStyle", "normal");
				}
				if (jQuery("#updateUnderlineButton").hasClass("pressed")) {
					jQuery(element).data("textDecoration", "underline");
				} else {
					jQuery(element).data("textDecoration", "normal");
				}
				jQuery(element).data('textAlign', jQuery(".alignButton.pressed").val());
				jQuery(element).data('fontColor', activeFontColor);
				//jQuery(element).data('backgroundColor', activeBackgroundColor);
				//jQuery(element).css('backgroundColor', activeBackgroundColor);
				//jQuery(element).data('opacity', jQuery("#opacity").val());
				//jQuery(element).css('opacity', jQuery("#opacity").val());
			} else {
				element.title = "Image Box";
				jQuery(element).addClass("blue"); //blue = image element
			}
			if (jQuery("#href").is(":checked")){
				jQuery(element).data("href", "true");
			} else {
				jQuery(element).data("href", "false");
			}
			//set placeholder text as empty string as default
			jQuery(activeElement).data('placeholder', "");
			//set co-ordinates of top-left corner of element
			element.style.left = mouse.x + 'px';
			element.style.top = mouse.y + 'px';
			//make object draggable
			element.draggable = "true";
			//insert resize/delete buttons
			createResizeButton(element);
			createDeleteButton(element);
			//add rectangle methods
			addRectangleMethods();

			//------------------------------TEXT ELEMENT------------------------------------------
			//create text element that contains text node for placeholder text
			var textNode  = document.createTextNode("");
			var textElement = document.createElement("pre");
			textElement.className = "textNode";
			jQuery(textElement).append(textNode);
			//keep text inside rectangle element by matching dimensions
			jQuery(textElement).css('height', element.style.height);
			jQuery(textElement).css('width', element.style.width);

			if (activeType == "text") {//use specified font settings for text elements
				jQuery(textElement).css('font-family', jQuery("#fontFamily").val());
				jQuery(textElement).css('font-size', jQuery("#fontSize").val() + 'px');
				jQuery(textElement).css('font-weight', jQuery(element).data("fontWeight"));
				jQuery(textElement).css('font-style', jQuery(element).data("fontStyle"));
				jQuery(textElement).css('text-decoration', jQuery(element).data("textDecoration"));
				jQuery(textElement).css('text-align', jQuery(element).data("textAlign"));
				jQuery(textElement).css('color', activeFontColor);
			} else { //use these defaults for image elements
				jQuery(textElement).css('font-family', "Arial");
				jQuery(textElement).css('font-size', "14px");
				jQuery(textElement).css('text-align', 'center');
				jQuery(textElement).css('color', "#000000");
			}
			//insert text element into current element
			jQuery(element).append(textElement);

			//inserts rectangle element into document upon creation
			jQuery(canvas).append(element);
		}
	}

	//-----------------------RECTANGLE METHODS----------------------------
	function addRectangleMethods() {
		//when clicking on rectangle itself while not in drawing mode
		jQuery(element).mousedown(function(event) {
			if (!drawingMode && !deleteMode) {
				event.stopPropagation();
				deselectAll(); //deselect all other rectangles (see CJM.js)
				element = this;
				selectRectangle(element); //select current rectangle
				jQuery(element).appendTo(jQuery(canvas)); //bring selected rectangle to front
				//inserts corresponding properties of active element into toolbars
				activeType = jQuery(element).data('type');
				toggleTextToolbar();
				if (activeType == "text") {
					jQuery('#fontFamily').val(jQuery(element).data('fontFamily'));
					jQuery('#fontFamily').css("font-family", jQuery(element).data('fontFamily'));
					jQuery('#fontSize').val(jQuery(element).data('fontSize').replace(/px/, ''));
					if (jQuery(element).data("fontWeight") == "bold") {
						activateButton("#updateBoldButton");
					} else {
						deactivateButton("#updateBoldButton");
					}
					if (jQuery(element).data("fontStyle") == "italic") {
						activateButton("#updateItalicButton");
					} else {
						deactivateButton("#updateItalicButton");
					}
					if (jQuery(element).data("textDecoration") == "underline") {
						activateButton("#updateUnderlineButton");
					} else {
						deactivateButton("#updateUnderlineButton");
					}
					deactivateButton(".alignButton");
					activateButton(jQuery("#textAlign" + jQuery(element).data('textAlign')));
					activeFontColor = jQuery(element).data('fontColor');
					//activeBackgroundColor = jQuery(element).data('backgroundColor');
					jQuery('#fontColor').ColorPickerSetColor(activeFontColor);
					jQuery('#fontColorSelector div').css('backgroundColor', activeFontColor);
					//jQuery('#backgroundColor').ColorPickerSetColor(activeBackgroundColor);
					//jQuery('#backgroundColorSelector div').css('backgroundColor', activeBackgroundColor);
					//jQuery('#opacity').val(jQuery(element).data('opacity'));
				}
				if (jQuery(element).data('href') == "true") {
					jQuery("#href").prop("checked", true);
				} else {
					jQuery("#href").prop("checked", false);
				}
				//changes cursor icon to grabbing
				if (jQuery.browser.mozilla) { //firefox
					jQuery(canvas).find('.rectangle').css('cursor', '-moz-grabbing');
				} else if (jQuery.browser.webkit) { //chrome
					jQuery(canvas).find('.rectangle').css('cursor', '-webkit-grabbing');
				} else { //default
					jQuery(activeElement).css('cursor', 'grabbing');
				}
			}
		});

		//when rectangle starts being dragged: enter drag mode, store width/height of rectangle, and find difference between current cursor position and co-ordinates of top-left corner
		element.ondragstart = function() {
			if (!drawingMode && !deleteMode) {
				dragMode = true;
				element = this;
				startX = parseInt(element.style.left.replace(/px/, ''));
				startY = parseInt(element.style.top.replace(/px/, ''));

				elementWidth = parseInt(element.style.width.replace(/px/, ''));
				elementHeight = parseInt(element.style.height.replace(/px/, ''));
				//find difference between top-left corner of element and current mouse position
				differenceX = mouse.x - startX;
				differenceY = mouse.y - startY;
			}
		}
		//releasing mouse exits dragging/drawing/resizing mode; if drawing, selects drawn rectangle
		jQuery(element).mouseup(function (event) {
			if (scrollingMode) {
				event.stopPropagation();
				scrollingMode = false;
			}
			if (element !== null) {
				dragMode = false;
				resizingMode = false;
				if (drawingMode) {
					jQuery('#placeholder').focus();
					selectRectangle(element);
				}
				element = null;
				drawingMode = false;
				deactivateButton('.create');
			}
			resetCursors();
		});

		//doubleclicking element moves focus to placeholder field
		jQuery(element).dblclick(function (event) {
			jQuery('#placeholder').focus();
		});
	}
	//----------------------RESIZE BUTTON-----------------------------------
	function createResizeButton(element) {
		//create resize button with border
		resizeButton = document.createElement('div');
		resizeButton.className = "resizeButton";
		if (jQuery(element).data('type') == "image") {
			jQuery(resizeButton).addClass("blue");
		} else {
			jQuery(resizeButton).addClass("red");
		}

		//click-and-holding resize button activates resizing mode
		jQuery(resizeButton).mousedown(function(e) {
			if (!drawingMode) {
				activeElement = this.parentNode;
				element = activeElement;
				startX = parseInt(element.style.left.replace(/px/, ''));
				startY = parseInt(element.style.top.replace(/px/, ''));
				resizingMode = true;
				setMousePosition(e);
			}
		});

		//releasing resize button exits resizing mode
		jQuery(resizeButton).mouseup(function() {
			resizingMode = false;
		});

		//add resize button to bottom-right corner of rectangle and hide it
		element.appendChild(resizeButton);
		jQuery(element).find('.resizeButton').hide();
	}

	//----------------------DELETE BUTTON---------------------------------
	function createDeleteButton(element) {
		//create delete button with border
		deleteButton = document.createElement('div');
		deleteButton.className = "deleteButton";
		if (jQuery(element).data('type') == "image") {
			jQuery(deleteButton).addClass("blue");
		} else {
			jQuery(deleteButton).addClass("red");
		}

		//clicking delete button deletes rectangle, disables fields, exits delete mode
		jQuery(deleteButton).on("click",function(event) {
			jQuery(this).parent().remove();
			disableFields();
			deleteMode = false;
		});

		//changes cursor icon to pointer on hover
		jQuery(deleteButton).hover(function() {
			if (!drawingMode && !scrollingMode) {
				jQuery(canvas).find('.deleteButton').css('cursor', 'pointer');
			}
		});

		//moving mouse over/off delete button toggles delete mode (needed for button to work in IE/Chrome)
		jQuery(deleteButton).mouseover(function() {
			deleteMode = true;
		});
		jQuery(deleteButton).mouseleave(function() {
			deleteMode = false;
		});

		//add delete button to top-left corner of rectangle and hide it
		element.appendChild(deleteButton);
		jQuery(element).find('.deleteButton').hide();
	}
	//-----------------------START DRAWING BUTTONS------------------------------------------------------
	//pressing create button activates drawing mode for selected element type
	function startDrawing(selectedType, identifier) {
		disableFields();
		activeType = selectedType;
		if (jQuery(identifier).hasClass("unpressed")) {
			drawingMode = true;
			deselectAll(); //deselect active rectangle (see CJM.js)
			toggleTextToolbar();
			element = null;
			deactivateButton('.create');
			activateButton(identifier);
			jQuery(canvas).css('cursor', 'crosshair'); //change cursor to crosshair
			jQuery(canvas).find('.rectangle, .resizeButton, .deleteButton').css('cursor', 'crosshair');
		}
		//pressing create button second time deactivates drawing mode/buttons
		else {
			drawingMode = false;
			deactivateButton(identifier);
			resetCursors();
		}
	}
	//---------------------------------COPY--------------------------------------------------
	//saves copy of selected element, sets left and top properties to top-left corner of canvas, activates paste button
	function copyRectangle() {
		if (!drawingMode) {
			var returnObj = {};
			jQuery(activeElement).data('left', 0 + 'px');
			jQuery(activeElement).data('top', 0 + 'px');
			jQuery(activeElement).data('width', jQuery(activeElement).css('width'));
			jQuery(activeElement).data('height', jQuery(activeElement).css('height'));
			returnObj[0] = jQuery(activeElement).data()
			JSONcopy = JSON.stringify(returnObj);
			jQuery('#pasteButton').removeClass('buttonNonActive');
			jQuery('#pasteButton').prop('disabled', false);
		}
	}
	//-------------------------------PASTE----------------------------------------------------
	//pastes copied element to canvas
	function pasteRectangle() {
		drawingMode = false;
		deactivateButton('.create')
		var JSONpaste = jQuery.parseJSON(JSONcopy);
		savedElement = JSONpaste[0];
		prepareRectangle(savedElement);
		resetCursors();
	}

	//------------------------------------LOAD RECTANGLES----------------------------------------------
	//clears canvas, converts loaded string back into JSON object, selects current page object, then rebuilds each rectangle in turn
	function loadRectangles(jsonString,page) {
		getRectangles(jsonString,page); //see CJM.js
		drawingMode = false;
		deactivateButton('.create');
	}
	//rebuilds current rectangle element from its associated data
	function prepareRectangle(savedElement) {

		// Load the background as html if the current saved element is html and it isnt already loaded.
		if (savedElement.type == "background-html" && jQuery(canvas).find('.backgroundHtml').length == 0){

			loadElement = document.createElement('div');
			loadElement.innerHTML = savedElement.html.replace(/&#60;/g,'<').replace(/&#62;/g,'>')
			loadElement.className = "backgroundHtml";
			jQuery(loadElement).data('type', savedElement.type);
			loadElement.style.width = savedElement.width;
			//SB 15/02/2016 Issue with border height
			//loadElement.style.height = savedElement.height;

			canvasWidth = savedElement.width;
			canvasHeight = savedElement.height;

			//insert finalised rectangle element into canvas
			jQuery(canvas).append(loadElement);

		}

		if (savedElement.type !== "background-image" && savedElement.type !== "background-html") { //ignore background image and html; already loaded
			//create custom div element for each saved rectangle
			if (window.navigator.userAgent.indexOf("MSIE") > 0) { //in IE9, dragging only works with anchor tags with href="#", not div elements
				loadElement = document.createElement('a');
				jQuery(loadElement).attr('href', '#');
				jQuery(loadElement).css('text-decoration', 'none');
			} else {
				loadElement = document.createElement('div');
			}
			loadElement.className = "rectangle";
			jQuery(loadElement).data('type', savedElement.type);
			loadElement.style.left = savedElement.left;
			loadElement.style.top = savedElement.top;
			loadElement.style.width = savedElement.width;
			loadElement.style.height = savedElement.height;
			//jQuery(loadElement).css('backgroundColor', savedElement.backgroundColor);
			//jQuery(loadElement).data('backgroundColor', savedElement.backgroundColor);
			//jQuery(loadElement).css('opacity', savedElement.opacity);
			//jQuery(loadElement).data('opacity', savedElement.opacity);
			//make object draggable
			loadElement.draggable = "true";
			//add resize/delete buttons
			element = loadElement;
			createResizeButton(loadElement);
			createDeleteButton(loadElement);
			//add rectangle methods
			addRectangleMethods();
			//add text element (see CJM.js)
			createTextElement();

			//insert finalised rectangle element into canvas
			jQuery(canvas).append(loadElement);
		}
	}

	//--------------------------------FONT BUTTONS----------------------------------------------
	//automatically updates corresponding property data for active rectangle if changed (Color Picker and checkboxes have their own methods)
	function updateProperty(identifier) {
		var dataID = jQuery(identifier).attr('id');
		var dataVal = jQuery(identifier).val();

		var updateData = true;
		switch (dataID) {
			//update font family of both text element and selected item in menu
			case "fontFamily":
				jQuery(activeElement).children('pre.textNode').css('font-family', dataVal);
				jQuery("#fontFamily").css("font-family", dataVal);
				break;
			//update font size of text element
			case "fontSize":
				if (jQuery.isNumeric(dataVal) && Math.floor(dataVal) == dataVal && dataVal > 0) { //positive integers only
					dataVal = dataVal + 'px';
					jQuery(activeElement).children('pre.textNode').css('font-size', dataVal);
				} else {
					updateData = false;
				}
				break;
			//update opacity of element
			//case "opacity":
				//jQuery(activeElement).css('opacity', dataVal);
				//break;
			//update content of text element
			case "placeholder":
				jQuery(activeElement).children('pre.textNode').html(dataVal);
				break;
			case "left":
				jQuery(activeElement).css('left',dataVal + 'px');
				break;
			case "top":
				jQuery(activeElement).css('top',dataVal + 'px');
				break;
			case "height":
				jQuery(activeElement).css('height',dataVal + 'px');
				jQuery(activeElement).children('pre.textNode').css('height', dataVal + 'px');
				break;
			case "width":
				jQuery(activeElement).css('width',dataVal + 'px');
				jQuery(activeElement).children('pre.textNode').css('width', dataVal + 'px');
				break;
		}
		//updates data associated with active rectangle
		if (updateData == true) {
			jQuery(activeElement).data(dataID, dataVal);
		}
	}

	//updates font properties of rectangle and text elements, toggles button appearance
	function updateButton(identifier) {
		switch (jQuery(identifier).attr('id')) {
			//change font weight of elements to bold/normal when pressed
			case "updateBoldButton":
				if (jQuery(identifier).hasClass("unpressed")) {
					jQuery(activeElement).data("fontWeight", "bold");
				} else {
					jQuery(activeElement).data("fontWeight", "normal");
				} jQuery(activeElement).contents().last().css('font-weight', jQuery(activeElement).data("fontWeight"));
				break;
			//change font style of elements to italic/normal when pressed
			case "updateItalicButton":
				if (jQuery(identifier).hasClass("unpressed")) {
					jQuery(activeElement).data("fontStyle", "italic");
				} else {
					jQuery(activeElement).data("fontStyle", "normal");
				} jQuery(activeElement).contents().last().css('font-style', jQuery(activeElement).data("fontStyle"));
				break;
			//change text decoration of elements to underline/none when pressed
			case "updateUnderlineButton":
				if (jQuery(identifier).hasClass("unpressed")) {
					jQuery(activeElement).data("textDecoration", "underline");
				} else {
					jQuery(activeElement).data("textDecoration", "none");
				} jQuery(activeElement).contents().last().css('text-decoration', jQuery(activeElement).data("textDecoration"));
				break;
		} toggleButton(identifier);
	}

	//update text alignment of element, toggle button appearance
	function changeAlignment(identifier) {
		jQuery(activeElement).data('textAlign', jQuery(identifier).val());
		jQuery(activeElement).contents().last().css('text-align', jQuery(identifier).val());
		deactivateButton(".alignButton");
		activateButton(identifier);
	}

	//switches class of button between "pressed" and "unpressed"
	function toggleButton(identifier) {
		jQuery(identifier).toggleClass("pressed").toggleClass("unpressed");
	}
	//replaces "unpressed" class of button with "pressed" class
	function activateButton(identifier) {
		jQuery(identifier).addClass("pressed").removeClass("unpressed");
	}
	//replaces "pressed" class of button with "unpressed"
	function deactivateButton(identifier) {
		jQuery(identifier).removeClass("pressed").addClass("unpressed");
	}

	//update href value of element if checkbox changes
	function updateCheckbox() {
		if (jQuery("#href").is(":checked")) {
			jQuery(activeElement).data("href", "true");
		} else {
			jQuery(activeElement).data("href", "false");
		}
	}

	//asks user to confirm whether or not to delete all rectangles from template
	function confirmDeleteAll() {
		var response = confirm("phr_CJM_ConfirmDeleteAll");
		if (response == true) {
			deleteAll();
		}
	}

	//deletes all rectangles from canvas, disables fields
	function deleteAll() {
		jQuery(canvas).find('.rectangle').remove();
		disableFields();
	}

	//--------------------------MISCELLENOUS METHODS-------------------------
	//changes borders of selected element to solid, activates buttons, populates fields
	function selectRectangle(element) {
		activeElement = element;
		jQuery(element).children('.resizeButton, .deleteButton').show();
		if (jQuery(element).data('type') == "text") {
			jQuery(element).addClass('selectedRed');
			jQuery(element).children('.resizeButton, .deleteButton').addClass('selectedRed');
		} else {
			jQuery(element).addClass('selectedBlue');
			jQuery(element).children('.resizeButton, .deleteButton').addClass('selectedBlue');
		}
		toggleTextToolbar();
		jQuery('#copyButton, #left, #top, #width, #height, #placeholder, #href').prop('disabled', false);
		jQuery('#placeholder, #copyButton').removeClass('buttonNonActive');
		jQuery('#left').val(Math.round(element.style.left.replace(/px/, '')));
		jQuery('#top').val(Math.round(element.style.top.replace(/px/, '')));
		jQuery('#width').val(Math.round(element.style.width.replace(/px/, '')));
		jQuery('#height').val(Math.round(element.style.height.replace(/px/, '')));
		jQuery('#placeholder').val(jQuery(element).data('placeholder'));
	}

	//resets cursors back to defaults
	function resetCursors() {
		jQuery(canvas).css('cursor', 'default');
		jQuery(canvas).find('.resizeButton').css('cursor', 'nw-resize');
		jQuery(canvas).find('.deleteButton').css('cursor', 'pointer');
		if (jQuery.browser.mozilla) { //firefox
			jQuery(canvas).find('.rectangle').css('cursor', '-moz-grab');
		} else if (jQuery.browser.webkit) { //chrome
			jQuery(canvas).find('.rectangle').css('cursor', '-webkit-grab');
		} else { //default
			jQuery(canvas).find('.rectangle').css('cursor', 'grab');
		}
	}

	//activates text toolbar/buttons when New Text Area button/text element clicked, deactivates if New Image button/image element selected
	function toggleTextToolbar() {
		if (activeType == "text") {
			jQuery("#textToolbar").addClass('textEditorActive').removeClass('textEditorNonActive');
			jQuery("#fontFamily, #fontSize, #updateBoldButton, #updateItalicButton, #updateUnderlineButton, #textAlignleft, #textAligncenter, #textAlignright, #textAlignjustify").prop('disabled', false);
		} else {
			jQuery("#textToolbar").addClass('textEditorNonActive').removeClass('textEditorActive');
			jQuery("#fontFamily, #fontSize, #updateBoldButton, #updateItalicButton, #updateUnderlineButton, #textAlignleft, #textAligncenter, #textAlignright, #textAlignjustify").prop('disabled', true);
		}
	}
	//clears and disables various fields, buttons and inputs
	function disableFields() {
		jQuery('#left, #top, #width, #height, #placeholder').val("");
		jQuery('#left, #top, #width, #height, #placeholder, #href').prop('disabled', true);
		jQuery('#copyButton, #placeholder').addClass('buttonNonActive');
		jQuery("#textToolbar").addClass('textEditorNonActive').removeClass('textEditorActive');
		jQuery("#copyButton, #fontFamily, #fontSize, #updateBoldButton, #updateItalicButton, #updateUnderlineButton, #textAlignleft, #textAligncenter, #textAlignright, #textAlignjustify").prop('disabled', true);
	}
</script>