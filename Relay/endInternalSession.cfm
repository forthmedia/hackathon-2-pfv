<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		endExternalSession.cfm
Author:			SWJ
Date created:	30 April 2002

	Objective - it sets cookie.PartnerSESSION to "0" 
		
	Syntax	  -	None
	
	Parameters - None required
				
	Return Codes - None 

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Enhancement still to do:



--->

<cfset request.document.showFooter = false>
 
<cfset application.com.RelayCurrentUser.setLoggedOut()>

<!--- NJH 2010/11/30 Had to set a timeout, so that we could log out of jasperserver...  --->
<cfoutput>
	<SCRIPT type="text/javascript">
		window.setTimeout(function logout() {window.location.href = '/index.cfm';},100)
	</script>
</cfoutput>

