<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File:			RelatedFileGet.cfm
Author:			DJH
Date created:	23 March 2000

Description:	Extra template required for the CF_RelatedFile custom tag to allow a file to be 
				downloaded

Version history:
1
1.1 2004-05-27	AJC	P_SNY011 VIP/UAT/7.8.2	GENERAL Bugfix - Save as box now shows correct filename instead of just "RelatedFileGet"
				NJH	P-FNL069 - added secure to the filedetails and expect now an encrypted file ID.
				WAB 2011/03/14 altered to get all info from relatedFile.cfc		
--->

<!--- get the name of the file to get --->

<!--- NJH 2009/07/17 P-FNL069 - expect an encrypted fileID --->
<cf_checkFieldEncryption fieldnames = "fileID">

<cfset getFileDetails = application.com.relatedFile.getFileDetailsByFileID(url.fileid)>
<!--- 
<cfquery name="GetFileDetails" datasource="#application.sitedatasource#">
	select
		rf.FileName,
		rft.FilePath,
		rft.PrefixFile,
		rft.secure
	from
		RelatedFile as rf,
		RelatedFileCategory as rft		
	where
		rft.FileCategoryID = rf.FileCategoryID
		and rf.FileID = #url.FileID#
</cfquery>
--->

<!--- 
<cfset startpath="#application.paths.content#">
<cfif GetFileDetails.secure>
	<cfset startpath="#application.paths.securecontent#">    <!--- WAB 2010/04/12 added slash - not always added to the relayvar--->
</cfif>
--->

<cfif GetFileDetails.recordcount is 1>
	<!--- 
	<cfif GetFileDetails.PrefixFile is true>
		<cfset thefile=listrest(GetFileDetails.fileName,"_")>
	<cfelse>
		<cfset thefile=GetFileDetails.fileName>
	</cfif>
	--->
	<!--- 2005/05/27 AJC START:P_SNY011 VIP/UAT/7.8.2 --->
	<!--- 2013-10-28 IH Case 437488 added weird looking syntax to get non ASCII filenames correct on both IE and FireFox --->
	<cfheader NAME="Content-Disposition" VALUE="attachment; filename*=utf-8''#urlEncodedFormat(getFileDetails.originalFileName)#" charset="utf-8">
	<cfcontent FILE="#GetFileDetails.FileHandle#" DELETEFILE="No">
	<!--- <cfheader name="Content-Disposition" value="inline; filename='#startpath##GetFileDetails.FilePath#\#thefile#'">
	<cfoutput>#startpath##GetFileDetails.FilePath#\#GetFileDetails.Filename#</cfoutput>

	<cfcontent type="unknown" file="#startpath##GetFileDetails.FilePath#\#GetFileDetails.Filename#"> --->
	<!--- 2005/05/27 AJC END:P_SNY011 VIP/UAT/7.8.2 --->
</cfif>

