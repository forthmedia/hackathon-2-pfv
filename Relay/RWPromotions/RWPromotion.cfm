<!--- �Relayware. All Rights Reserved 2014 --->
<!---
Modifications:

2009/02/13		NJH		P-SNY047 - don't show promotions created by 4 are these are 'module promotions' which shouldn't be shown/edited. Also added ordering to the query.
2011/08/25 		MS		P-LEN024 Phase IV Partner Sales Incentives - Added a hook to load Custom Promotion Listing Report and Editing Screen for Lenovo
2014-05-02 		PPB 	Case 439562 added ModuleCode to url params and changed list title
2014-05-09 		PPB 	Case 439562 ProductGroups dropdown not populating
2014-06-17		PPB 	Case 439562 make ProductGroups use a TwoSelects control
2014-06-19		PPB 	Case 439562 issue selecting a large number of Products in a TwoSelects control
2014-07-09		PPB 	Case 439562 made TwoSelects control combos wider for Products/ProductGroups/Categories
2014/08/14		REH 	Case 441308 Product Description field not populated - adding promotion editor product display option to show title, SKU or SKU: title (Title is default)
2014-10-01		RPW		CORE-153 DEMO _ Manage > Sales Collaboration > Promotions : No restrictions selection missing
2014-10-30		RPW		CORE-145 Promotions filter values confused - Show back button for adding new promotions
2016-02-01		WAB		BF-419 Force use of twoSelectsCombo by adding attribute usejQueryMultiSelect = false
2016-02-03		WAB		BF-419 Alter the hiding and showing of sections so that it uses setVisibilityForControlRow() function (so that validation is ignored on hidden sections)

Possible enhancements:
make Categories use a TwoSelects control (do it like ProductGroups by introducing a dropdown Filter and a TwoSelects control and toggle between them; NB. would also involve filtering out arguments.selected in relayProduct.getProductSelectorCategories as per getProductSelectorGroups )

 --->

<!--- START 2011/09/21 	MS		P-LEN024 Phase IV Partner Sales Incentives - Custom Promotion Listing Report and Editing for Lenovo--->
<cfif fileexists("#application.paths.code#\cftemplates\RWPromotion_List.cfm")>
	<cfinclude template="/code/cftemplates/RWPromotion_List.cfm">
<cfelse>

<cf_includeJavascriptOnce template = "/javascript/fnlajax.js">  	<!--- 2014-06-19 PPB Case 439562 --->


<!--- START 2014-05-09 PPB Case 439562 --->
<cfset thisEntity = "">
<cfset thisEntityId = 0>

<cfif StructIsEmpty(form)>		<!--- if the form struct is not empty we've just saved the promotion else we've come from the list --->
	<cfif structKeyExists(url,"RWPromotionId")>
		<cfquery name="qryPromotionScope" datasource="#application.SiteDataSource#" result="qryPromotionScopeResult" >
			SELECT TOP 1 Entity, EntityId
			FROM RWPromotionScope
			WHERE RWPromotioniD =  <cf_queryparam value="#url.RWPromotionId#" CFSQLTYPE="cf_sql_integer" >
		</cfquery>

		<cfif qryPromotionScope.recordcount>
			<cfset thisEntity = UCase(qryPromotionScope.Entity)>
			<cfset thisEntityId = qryPromotionScope.EntityId>
		</cfif>
	</cfif>
<cfelse>

	<!--- 2014-010-01		RPW		CORE-153 DEMO _ Manage > Sales Collaboration > Promotions : No restrictions selection missing --->
	<!--- 2014-10-30		RPW		CORE-145 Promotions filter values confused - Show back button for adding new promotions --->
	<cfif structKeyExists(form,"entityRestriction") AND Len(form.entityRestriction)>
		<cfset thisEntity = UCase(form.entityRestriction)> 					<!--- upper case of thisEntity is important --->	<!--- productID not used at time of writing but may as well have them just in case --->
		<cfset thisEntityId = form[form.entityRestriction]>
	<cfelse>
		<cfset thisEntity = "">
		<cfset thisEntityId = 0>
	</cfif>
</cfif>
<!--- END 2014-05-09 PPB Case 439562 --->

<cfif structKeyExists(url,"editor")>
	<cf_head>

	<script>

		function initiateProductList() {
			<cfif structKeyExists(form,"productID")>
				<cfoutput>
					<cfloop list="#form.productID#" index="i">
						jQuery( "##List_productID" ).find('[value="#i#"]').appendTo("##productID_select");
					</cfloop>
				</cfoutput>
			</cfif>
		}

		function showHideRestrictions() {
				entityRestriction = jQuery('#entityRestriction').val();
				/* build up a structure showing which items need to be visible */
				var restrictionsHash = {
					 	'productCategoryID': ['productCategoryID']
					 , 'productGroupID' : ['productCategoryID','productGroupID']
					 , 'productID' : ['productCategoryID','productGroupFilter','productID']
					 , 'productGroupFilter' : []
				}

				var itemsVisibleArray = (entityRestriction != '')?restrictionsHash[entityRestriction]:[]
				jQuery.each (restrictionsHash, function (index,value) {
					/* 	Note that this setVisibilityForControlRow function hides the row and disables any validation in the hidden area
						In the end this was not necessary here, because we decided to automatically turn required On/Off (because we didn't want the 'filter' items to show as required
						The setRequired function updates the required attribute of the associated label
					 */
					setVisibilityForControlRow ($(index),(itemsVisibleArray.indexOf (index) != -1))
					setRequired($(index),(index == entityRestriction))
				})


			return entityRestriction;
		}

		jQuery(document).ready(function() {
			entityRestriction = showHideRestrictions();
			initiateProductList();
		});

	</script>

	</cf_head>
</cfif>
<cffunction name="postSave" returntype="struct">
	<cfset var entityIDSelected = "">
	<cfset var result = {isOK=true,message=""}>

	<cfif structKeyExists(form, "entityRestriction")>
		<cfswitch expression="#Trim(form.entityRestriction)#">
		    <cfcase value="productCategoryID">
		        <cfif structKeyExists(form, "productCategoryID")>
					<cfset entityIDSelected = form.productCategoryID>
				</cfif>
		    </cfcase>
		    <cfcase value="productGroupID">
		        <cfif structKeyExists(form, "productGroupID")>
					<cfset entityIDSelected = form.productGroupID>
				</cfif>
		    </cfcase>
		    <cfcase value="productID">
		        <cfif structKeyExists(form, "productID")>
					<cfset entityIDSelected = form.productID>
				</cfif>
		    </cfcase>
		</cfswitch>

		<!--- <cfif entityIDSelected neq ''> --->		<!--- 2014-05-09 PPB Case 439562 remove condition so that if a restriction is cleared without setting a new one we still delete the rows from db --->
			<cfset application.com.relayIncentive.updatePromotionScope(RWPromotionID,form.entityRestriction,entityIDSelected)>
		<!--- </cfif> --->								<!--- 2014-05-09 PPB Case 439562 --->
	</cfif>

	<cfreturn result>
</cffunction>

<cfparam name="ModuleCode" default="RW">

<!--- 2014/08/14 REH Case 441308 Product Description field not populated - adding promotion editor product display option --->
<cfparam name="promotionEditorProductDisplay" type="any" default="#application.com.settings.getSetting('products.promotionEditorProductDisplay')#">

<cfsavecontent variable="xmlSource">
	<cfoutput>
	<editors>
		<editor id="232" name="thisEditor" entity="RWPromotion" title="Promotion Editor">																<!--- 2014-05-02 PPB Case 439562 changed list title --->
			<field name="RWPromotionID" label="Unique Promotion ID" description="System generated numeric id for this promotion" control="html"/>
			<field name="RWPromotionCode" label="phr_RWPromotionCode" control="text" size="45" required="true"></field>
			<field name="RWPromotionDescription" label="phr_RWPromotionDescription" control="text" size="45" required="true"></field>
			<field name="StartDate" label="phr_StartDate" description="phr_dateFieldHelpText" control="date"></field>
			<field name="EndDate" label="phr_EndDate" description="phr_dateFieldHelpText" control="date"></field>
			<field name="triggerPoint" label="phr_TriggerPoint" control="select" display="TRIGGERPOINTSLISTDISPLAY" value="TRIGGERPOINTSLISTVALUE" query="func:com.relayIncentive.getTriggerPoints(ModuleCode=#ModuleCode#)"></field>
			<field name="RWPromotionTypeId" label="phr_RWPromotionTypeId" control="select" required="true" display="RWPromotionTypeDescription" value="RWPromotionTypeId" query="func:com.relayIncentive.getPromotionType(#ModuleCode#)"></field>
			<field name="Rationale" label="phr_Rationale" control="textarea" Parameters="rows=5" maxLength="1000" required="false"></field>
			<field name="TotalPoints" label="phr_TotalPoints" control="text" size="45" required="false"></field>
			<field name="active" label="phr_active" default="false" control="checkbox"/>
			<field name="Exclusive" label="phr_Exclusive" default="false" control="checkbox"/>
			<field name="" label="phr_VisibilityScope" description="" control="recordRights" suppressUserGroups="false" userGroupTypes="External,Visibility"></field>
			<field name="" label="phr_PromotionCountryScope" description="" control="countryScope"></field>

			 <!--- START 2014-05-09 PPB Case 439562 --->
			 <!--- <field name="campaignID" label="phr_Sys_AssociateCampaign" nullText="phr_Ext_SelectAValue" control="select" display="CAMPAIGNNAME" value="CAMPAIGNID" query="func:com.relayCampaigns.GetCampaigns()"></field> --->
			<field name="campaignID" label="phr_Sys_AssociateCatalog" nullText="phr_Ext_SelectAValue" control="select" display="CAMPAIGN" value="CAMPAIGNID" query="func:com.relayProduct.getProductCampaigns()"></field>
			 <!--- END 2014-05-09 PPB Case 439562 --->

			<!--- 2014-010-01		RPW		CORE-153 DEMO _ Manage > Sales Collaboration > Promotions : No restrictions selection missing --->
			<field name="entityRestriction" onchange="showHideRestrictions();" label="phr_PromotionEntity" control="select" currentValue="#thisEntity#" query="select 'No Restrictions' as display,'' as value,0 as SortOrder union select 'Product Category' as display,'productCategoryID' as value,1 as SortOrder union select 'Product Group' as display,'productGroupID' as value,2 as SortOrder union select 'Product' as display,'productID' as value,3 as SortOrder order by SortOrder" default="productID"></field>		<!--- 2014-05-14 PPB Case 439562 added SortOrder --->

			<!--- START 2014-05-14 PPB Case 439562 --->
			<field name="productCategoryID" label="phr_ProductCategories" nullText="phr_Ext_SelectAValue" required="false" description="" control="select" bindonload="true" bindfunction="cfc:relay.webservices.relayIncentivews.getProductCategoriesForCampaign({campaignID});" nulltext="phr_Ext_SelectAValue" display="title" value="productCategoryID" currentValue="#thisEntity eq 'productCategoryID'?thisEntityId:''#"></field>

			<field name="productGroupID" label="phr_ProductGroups" nullText="phr_Ext_SelectAValue" required="false" description="" control="select" displayas="twoSelects" useJqueryMultiSelect = "false" bindonload="true" bindfunction="cfc:relay.webservices.relayIncentivews.getProductGroupsForCategory({productCategoryID})"  display="TITLE" value="productGroupID" width="350" validValues="select pg.title_defaultTranslation as Title, pg.productGroupID, <cfif isDefined("RWPromotionID") or isDefined("newRecordID")>CASE WHEN ps.entityID IS NOT NULL THEN 1 ELSE 0 END<cfelse>0</cfif> AS isSelected from RWPromotionScope ps right outer join ProductGroup pg on ps.entityID = pg.productGroupID and ps.entity = 'productGroupID'<cfif isDefined("RWPromotionID")> and  ps.RWPromotionID = #RWPromotionID#</cfif><cfif isDefined("newRecordID")> and  ps.RWPromotionID = #newRecordID#</cfif>"></field>

			<field name="productGroupFilter" label="phr_ProductGroups" nullText="phr_Ext_SelectAValue" required="false" description="" control="select"  bindonload="true" bindfunction="cfc:relay.webservices.relayIncentivews.getProductGroupsForCategory({productCategoryID})" onchange="populateProducts(this.value)"  display="TITLE" value="productGroupID" useCFForm="false"></field>		<!--- 2014-06-19 PPB Case 439562 --->
			<!--- 2014/08/14 REH Case 441308 Product Description field not populated - adding promotion editor product display option --->
			<field name="productID" label="phr_Products" required="true" description="" control="select" displayas="twoSelects" useJqueryMultiSelect = "false" width="350" validValues="SELECT title, productID, <cfif isDefined("RWPromotionID") or isDefined("newRecordID")>CASE WHEN ps.entityID IS NOT NULL THEN 1 ELSE 0 END<cfelse>0</cfif> AS isSelected FROM RWPromotionScope ps RIGHT OUTER JOIN (SELECT CASE WHEN '#promotionEditorProductDisplay#' = 'Product SKU' THEN p.sku WHEN '#promotionEditorProductDisplay#' = 'Product SKU: Title' THEN isNull(p.sku, '') + ': ' + isNull(p.title_defaultTranslation, '') ELSE isNull(p.title_defaultTranslation, p.sku) END AS Title, p.productID FROM Product AS p WHERE p.deleted = 0 ) prod ON ps.entityID = prod.productID AND ps.entity = 'productID' <cfif isDefined("RWPromotionID")> AND  ps.RWPromotionID = #RWPromotionID#</cfif> <cfif isDefined("newRecordID")> AND  ps.RWPromotionID = #newRecordID#</cfif> ORDER BY title" display="title" value="productID" default="0"></field>			<!--- 2014-06-19 PPB Case 439562 --->
			<!--- END 2014-05-14 PPB Case 439562 --->

			<field name="OwnerPersonId" label="OwnerPersonId" default="#LISTGETAT(COOKIE.USER, 1, '-')#" control="hidden"/>
		</editor>
	</editors>
	</cfoutput>
</cfsavecontent>


<!--- 2014-10-30		RPW		CORE-145 Promotions filter values confused - Show back button for adding new promotions - added tableFromQuery-QueryInclude.cfm for filter --->
<CFQUERY name="getData" datasource="#application.SiteDataSource#">
	SELECT *
	  FROM RWPromotion rpr
	      ,RWPromotionType rpt
		  ,person per
     WHERE rpr.RWPromotionTypeId = rpt.RWPromotionTypeId
	   AND per.personId = rpr.OwnerPersonId
	   AND rpt.ModuleCode =  <cf_queryparam value="#ModuleCode#" CFSQLTYPE="CF_SQL_VARCHAR" >
	   <!--- NJH 2009/02/13 P-SNY047- we are now creating module promotions (where a partner gets points for passing a module). These are system
	   generated and we don't want to show them. --->
	   AND rpr.created <> 4
		<cfinclude template="../templates/tableFromQuery-QueryInclude.cfm">
	   order by rpr.rwPromotionCode,rpr.created
</CFQUERY>


<cfparam name="numRowsPerPage" type="numeric" default="50">
<cfparam name="startRow" type="numeric" default="1">

<!--- 2014-10-30		RPW		CORE-145 Promotions filter values confused - Show back button for adding new promotions --->
<cf_listAndEditor
	xmlSource="#xmlSource#"
	cfmlCallerName="RWPromotion"
	keyColumnList="RWPromotionCode"
	keyColumnKeyList=" "
	keyColumnURLList=" "
	keyColumnOnClickList="javascript:openNewTab('##jsStringFormat(RWPromotionCode)##'*comma'##jsStringFormat(RWPromotionCode)##'*comma'/rwpromotions/rwpromotion.cfm?editor=yes&hideBackButton=true&ModuleCode=##ModuleCode##&RWPromotionId=##RWPromotionId##'*comma{reuseTab:true*commaiconClass:'incentive'});return false;"			<!--- 2014-05-02 PPB Case 439562 added ModuleCode to url params --->
	showSaveAndReturn = "false"
	showTheseColumns="RWPromotionCode,RWPromotionDescription,RWPromotionTypeDescription,firstName,LastName"
	FilterSelectFieldList="RWPromotionCode,RWPromotionDescription,RWPromotionTypeDescription,firstName,LastName"
	FilterSelectFieldList2="RWPromotionCode,RWPromotionDescription,RWPromotionTypeDescription,firstName,LastName"
	groupByColumns=""
	useInclude="false"
	sortOrder="RWPromotionId"
	queryData="#getData#"
	numRowsPerPage="#numRowsPerPage#"
	startRow="#startRow#"
	rowIdentityColumnName="RWPromotionId"
	columnTranslation="true"
	columnTranslationPrefix="phr_report_rwpromotion_"
	backform="/rwpromotions/rwpromotion.cfm?ModuleCode=#ModuleCode#"
	postSaveFunction = #postSave#
	hideBackButton="#structKeyExists(URL,'hideBackButton') and url.hideBackButton?true:false#"
>
</cfif>

<!--- START 2014-06-19 PPB Case 439562 replaced bind on productId TwoSelects to resolve the problem with large numbers of selected products (the Coldfusion.Ajax uses a GET whereas populateSelectBoxViaAjax/fnlAjax uses a POST) --->
<script type="text/javascript">
	// load Products select box
	populateProducts = function(productGroupID,options) {
		options = {valueColumn:'PRODUCTID',displayColumn:'TITLE'}
		populateSelectBoxViaAjax ('List_productID','/webservices/callWebService.cfc?webServiceName=relayIncentivews&methodname=getProducts','callWebService','productGroupID=' + productGroupID ,options)
	}
</script>
<!--- END 2014-06-19 PPB Case 439562 --->