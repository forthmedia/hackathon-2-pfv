<!--- �Relayware. All Rights Reserved 2014 --->
<cfif isDefined('URL.StartRow') OR isDefined('Form.StartRow')>
	<cfif isDefined('URL.StartRow')>
		<cfset StartRow=URL.StartRow>
	<cfelse>
		<cfset StartRow=Form.StartRow>
	</cfif>
<cfelse>
	<cfset StartRow=1>
</cfif>

<CFINCLUDE TEMPLATE="RegionsPartnerLevel.cfm">

<CFIF IsDefined("Form.region")>
	<CFQUERY name="GetRecords" datasource="#application.siteDatasource#">
		SELECT org.organisationid
		      ,org.organisationName
		      ,org.Countryid
		      ,cou.countryDescription
		      ,rca.dateTermsAgreed
  		  FROM organisation AS org
    INNER JOIN Country AS cou ON org.countryid = cou.countryid
    INNER JOIN CountryGroup cgr ON cgr.countryMemberId = org.countryId AND cgr.countryGroupId =  <cf_queryparam value="#Form.region#" CFSQLTYPE="CF_SQL_INTEGER" >
    INNER JOIN RWCompanyAccount AS rca ON org.organisationid = rca.organisationid AND rca.dateTermsAgreed IS NOT NULL
         WHERE (SELECT TOP 1 fla.name
                  FROM flag AS fla
				      ,flaggroup AS fgr
					  ,booleanflagdata AS bfd
					  ,location AS loc
		         WHERE loc.organisationid = org.organisationid
		           AND loc.locationid = bfd.entityid
   		           AND bfd.flagid = fla.flagid
         	       AND fla.flaggroupid = fgr.flaggroupid
 		           AND fgr.flaggrouptextid = 'PartnerType'
              ORDER BY fla.flagid) =  <cf_queryparam value="#Form.FocusLevel#" CFSQLTYPE="CF_SQL_VARCHAR" >
      ORDER BY org.organisationName
	</CFQUERY>

	<CFQUERY name="GetRWPromotions" datasource="#application.SiteDataSource#">
		SELECT *
		  FROM RWPromotion rwp
	</CFQUERY>

</CFIF>

<br>
<CF_TRANSLATE>
<SCRIPT type="text/javascript" src="../javascript/datefunctions.js"></script>

<SCRIPT type="text/javascript" src="../javascript/verifyInteger.js"></script>

<SCRIPT type="text/javascript">


<!--

function checkDate(liabilityDate, Points, RWPromotion, dateTermsAgreed) {
	var RWSelection = RWPromotion.selectedIndex;

	if (Points.value != '' && RWPromotion[RWSelection].value != '') {
		if (liabilityDate.value != '') {
			if (compareDates(liabilityDate.value, dateTermsAgreed.value, '')) {
				alert('phr_LiabilityDateCannotBeBeforeDateTermsAgreed');
			}
			else
			{
				if (compareDates(DBServerDate, liabilityDate.value, '')) {
					alert('phr_LiabilityDateCannotBeInFuture');
				}
			}
		}
	}
}

function checkInteger(inputAmount) {
	verifyInteger(inputAmount.value, 'integer');
	if (msg != '') {
		alert(msg);
	}
}

function verifyForm() {
	var form = document.mainForm;
		msg='';

	if (form.frmAllOrganisationId.length == null) {
		var points = eval('form.Points_' + form.frmAllOrganisationId.value + '.value');
		var liabilityDate = eval('form.LiabilityDate_' + form.frmAllOrganisationId.value + '.value');
		var dateTermsAgreed = eval('form.frmDateTermsAgreed_' + form.frmAllOrganisationId.value + '.value');
		var RWSelection = eval('document.mainForm.RWPromotionCode_' + form.frmAllOrganisationId.value + '.selectedIndex');
		var RWPromotion = eval('document.mainForm.RWPromotionCode_' + form.frmAllOrganisationId.value + '[RWSelection].value');

		if (points == '' && RWPromotion != '') {
			 msg += 'phr_PointsMustBeEnteredWithPromotionCode';
		}

		if (points != '' && RWPromotion == '') {
			 msg += 'phr_PromotionCodeMustBeEnteredWithPoints';
		}

		if (points != '') {
			msg += verifyInteger(points, 'integer');
		}

		if (points != '' && RWPromotion != '') {
			if (liabilityDate != '') {

				if (! isDate(liabilityDate, 'Boolean')) {
					msg += 'phr_InvalidDate';
				}
				else
				{
					if (compareDates(liabilityDate, dateTermsAgreed, '')) {
						msg += 'phr_LiabilityDateCannotBeBeforeDateTermsAgreed';
					}
					else
					{
						if (compareDates(DBServerDate, liabilityDate, '')) {
							msg += 'phr_LiabilityDateCannotBeInFuture';
						}
					}
				}
			}
		}
	}
	else
	{
		for (i=0; i<form.frmAllOrganisationId.length; i++) {

			var points = eval('form.Points_' + form.frmAllOrganisationId[i].value + '.value');
			var liabilityDate = eval('form.LiabilityDate_' + form.frmAllOrganisationId[i].value + '.value');
			var dateTermsAgreed = eval('form.frmDateTermsAgreed_' + form.frmAllOrganisationId[i].value + '.value');
			var RWSelection = eval('document.mainForm.RWPromotionCode_' + form.frmAllOrganisationId[i].value + '.selectedIndex');
			var RWPromotion = eval('document.mainForm.RWPromotionCode_' + form.frmAllOrganisationId[i].value + '[RWSelection].value');

			if (points == '' && RWPromotion != '') {
				 msg += 'phr_PointsMustBeEnteredWithPromotionCode';
				 break;
			}

			if (points != '' && RWPromotion == '') {
				 msg += 'phr_PromotionCodeMustBeEnteredWithPoints';
				 break;
			}

			if (points != '') {
				verifyInteger(points, 'integer');
				if (msg != '') {
					break;
				}
			}

			if (points != '' && RWPromotion != '') {
				if (liabilityDate != '') {

					if (! isDate(liabilityDate, 'Boolean')) {
						msg += 'phr_InvalidDate';
						break;
					}

					if (compareDates(liabilityDate, dateTermsAgreed, '')) {
						msg += 'phr_LiabilityDateCannotBeBeforeDateTermsAgreed';
						break;
					}
					else
					{
						if (compareDates(DBServerDate, liabilityDate, '')) {
							msg += 'phr_LiabilityDateCannotBeInFuture';
							break;
						}
					}
				}
			}
		}
	}

	if (msg != '') {
		alert (msg);
		return false;
	}
	else {
		//Don't seem to be any errors on the form
		return true;
	}
}


function formSubmit() {

	var form = document.mainForm;

	if (verifyForm()) {
		form.submit();
	}
}

function getPartners() {

	var form = document.inputCriteria;
	form.submit();

}

//-->

</SCRIPT>

<FORM ACTION="AllocateRWPromotionPoints.cfm" NAME="inputCriteria" METHOD="post">
<TABLE ALIGN="center">
	<CFOUTPUT>
	<!--- Abort if the user does not have access to any regions designated for the InFocus programme --->
	<CFIF getRegions.recordCount EQ 0>
		Sorry this view is only available to the Support Team

		<CF_ABORT>
	<!--- If the user only has access to one region, for example, the InFocus champions, then the report
	      can only be run for that region. Therefore do not show the region selection box --->
	<CFELSEIF getRegions.recordCount EQ 1>
		<CF_INPUT NAME="region" TYPE="Hidden" VALUE="#getRegions.countryId#">
	<CFELSE>
		<TR><TD CLASS="Title2">Select Region:</TD>
			<TD>
				<SELECT NAME="region">
					<CFLOOP QUERY="getRegions">
						<OPTION VALUE="#countryId#" <CFIF IsDefined("region") AND IsDefined("Form.region") AND Form.region EQ countryId>SELECTED</CFIF>>#htmleditformat(countryDescription)#</OPTION>
					</CFLOOP>
				</SELECT>
			</TD>
		</TR>
	</CFIF>
	<TR><TD CLASS="Title2">Select Partner Level:</TD>
		<TD>
			<SELECT NAME="focusLevel">
				<!--- select partner level --->
				<CFLOOP QUERY="getPartnerLevel">
					<OPTION VALUE="#name#" <CFIF IsDefined("focusLevel") AND IsDefined("Form.focusLevel") AND Form.focusLevel EQ name>SELECTED</CFIF>>#htmleditformat(name)#</OPTION>
				</CFLOOP>
			</SELECT>
		</TD>
	</TR>
	</CFOUTPUT>
	<TR>
		<TD ALIGN="center"><A HREF="javascript:getPartners()">phr_GetPartners</A></TD>
		<TD ALIGN="center"><A HREF="RWPromotion_Add.cfm?frmReturnPage=1">phr_AddPromotion</A></TD>
	</TR>

</TABLE>
</FORM>

<FORM ACTION="AllocateRWPromotionPointsTask.cfm" NAME="mainForm" METHOD="post">
	<CFIF IsDefined("Form.Region")>
	<table width="600" border="0" cellspacing="2" cellpadding="3" align="center" bgcolor="White">
		<TR>
			<TH>phr_organisationName</TH>
			<TH>phr_dateTermsAgreed</TH>
			<TH>phr_CurrentBalance</TH>

			<CFIF IsDefined("Form.RedisplayAllocation")>
				<TH>phr_PreviousBalance</TH>
			<CFELSE>
				<TH>phr_RWPromotionCode</TH>
				<TH>phr_Points</TH>
				<TH>phr_LiabilityDate</TH>
			</CFIF>

		</TR>
		<CFIF GetRecords.RecordCount EQ 0>
			<TR>
				<TD COLSPAN="5">phr_NoOrganisationsExist</TD>
			</TR>
		<CFELSE>
			<CFOUTPUT query="GetRecords">
			<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
				<td>
					#htmleditformat(OrganisationName)#</a>
				</td>
				<td>
					#DateFormat(dateTermsAgreed, "dd/mm/yyyy")#
				</td>
				<cfinclude template="/incentive/sql_PointsBalance.cfm">
				<CFSET balancebroughtforward = val(PointsBalance.balance)>
				<TD>#balancebroughtforward#</TD>
				<CFIF IsDefined("Form.RedisplayAllocation")>
					<TD>#evaluate("PreviousBalance_#organisationId#")#</TD>
				<CFELSE>
					<td>
						<SELECT NAME="RWPromotionCode_#OrganisationId#">
							<OPTION VALUE="">
						<!--- select partner level --->
						<CFLOOP QUERY="getRWPromotions">
							<OPTION VALUE="#RWPromotionCode#">#htmleditformat(RWPromotionCode)#</OPTION>
						</CFLOOP>
						</SELECT>
					<td>
						<CF_INPUT TYPE="text" NAME="Points_#OrganisationId#" ONBLUR="checkInteger(Points_#OrganisationId#);">
					</td>
					<td>
<!--- 	 					<INPUT TYPE="text" NAME="LiabilityDate_#OrganisationId#" ONBLUR="checkDate(LiabilityDate_#OrganisationId#, Points_#OrganisationId#, RWPromotionCode_#OrganisationId#, frmDateTermsAgreed_#OrganisationId#);">  --->
	 					<CF_INPUT TYPE="text" NAME="LiabilityDate_#OrganisationId#">
					</td>

					<CF_INPUT TYPE="hidden" NAME="PreviousBalance_#organisationId#" VALUE="#balancebroughtforward#">
					<CF_INPUT TYPE="hidden" NAME="frmDateTermsAgreed_#OrganisationId#" VALUE="#DateFormat(dateTermsAgreed,'dd/mm/yyyy')#">
					<CF_INPUT TYPE="hidden" NAME="frmAllOrganisationId" VALUE="#OrganisationId#">
				</CFIF>
			</tr>
			</CFOUTPUT>
			<CFIF NOT IsDefined("Form.RedisplayAllocation")>
				<CFOUTPUT>
				<CF_INPUT TYPE="hidden" NAME="Region" VALUE="#Region#">
				<CF_INPUT TYPE="hidden" NAME="FocusLevel" VALUE="#FocusLevel#">
				<INPUT TYPE="hidden" NAME="RedisplayAllocation" VALUE="1">
				<TR><TD COLSPAN="6" ALIGN="center"><A HREF="javascript:formSubmit()">Add Points</A></TD></TR>
				</CFOUTPUT>
			</CFIF>
		</CFIF>
	</TABLE>
	</CFIF>
</FORM>
</CF_TRANSLATE>


