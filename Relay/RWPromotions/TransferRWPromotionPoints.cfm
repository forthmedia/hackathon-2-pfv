<!--- �Relayware. All Rights Reserved 2014 --->
<!---

NJH 2009-02-09 P-SNY047 changed from form to cfform as twoSelectsComboQuery changed to using cfselect
WAB 2016-01-29	Removed call to Sel All Function
 --->

<cfif isDefined('URL.StartRow') OR isDefined('Form.StartRow')>
	<cfif isDefined('URL.StartRow')>
		<cfset StartRow=URL.StartRow>
	<cfelse>
		<cfset StartRow=Form.StartRow>
	</cfif>
<cfelse>
	<cfset StartRow=1>
</cfif>

<CFINCLUDE TEMPLATE="RegionsPartnerLevel.cfm">

<CFIF IsDefined("Form.SelectTransferors")>

	<CFQUERY name="GetRecords" datasource="#application.SiteDataSource#">
		SELECT org.organisationid
		      ,org.organisationName
		      ,org.Countryid
		      ,cou.countryDescription
		      ,rca.dateTermsAgreed
  		  FROM organisation AS org
    INNER JOIN Country AS cou ON org.countryid = cou.countryid
    INNER JOIN CountryGroup cgr ON cgr.countryMemberId = org.countryId AND cgr.countryGroupId =  <cf_queryparam value="#Form.region#" CFSQLTYPE="CF_SQL_INTEGER" >
    INNER JOIN RWCompanyAccount AS rca ON org.organisationid = rca.organisationid AND rca.dateTermsAgreed IS NOT NULL
         WHERE (SELECT TOP 1 fla.name
                  FROM flag AS fla
				      ,flaggroup AS fgr
					  ,booleanflagdata AS bfd
					  ,location AS loc
		         WHERE loc.organisationid = org.organisationid
		           AND loc.locationid = bfd.entityid
   		           AND bfd.flagid = fla.flagid
         	       AND fla.flaggroupid = fgr.flaggroupid
 		           AND fgr.flaggrouptextid = 'PartnerLevel'
              ORDER BY fla.flagid) =  <cf_queryparam value="#Form.FocusLevel#" CFSQLTYPE="CF_SQL_VARCHAR" >
      ORDER BY org.organisationName
	</CFQUERY>
</CFIF>

<CFIF IsDefined("Form.FromOrg")>

	<CFQUERY name="GetFromOrg" datasource="#application.SiteDataSource#">
		SELECT org.organisationid
		      ,org.organisationName
  		  FROM organisation AS org
	     WHERE org.organisationID =  <cf_queryparam value="#Form.FromOrg#" CFSQLTYPE="CF_SQL_INTEGER" >
	</CFQUERY>

	<CFQUERY NAME="checkDummyOrg" datasource="#application.SiteDataSource#">
		SELECT *
		  FROM booleanFlagData bfd
	      JOIN flag fla	ON bfd.flagId = fla.flagId AND fla.flagTextId = 'DummyAccount'
         WHERE bfd.entityId =  <cf_queryparam value="#Form.FromOrg#" CFSQLTYPE="CF_SQL_INTEGER" >
	</CFQUERY>

</CFIF>

<CFIF IsDefined("Form.SelectTransferees")>

	<CFQUERY name="GetRecords" datasource="#application.SiteDataSource#">
		SELECT org.organisationid as column1
		      ,org.organisationName as column2
  		  FROM organisation AS org
    INNER JOIN Country AS cou ON org.countryid = cou.countryid
    INNER JOIN CountryGroup cgr ON cgr.countryMemberId = org.countryId AND cgr.countryGroupId =  <cf_queryparam value="#Form.region#" CFSQLTYPE="CF_SQL_INTEGER" >
    INNER JOIN RWCompanyAccount AS rca ON org.organisationid = rca.organisationid AND rca.dateTermsAgreed IS NOT NULL
 	<CFIF checkDummyOrg.recordCount GT 0>
	INNER JOIN booleanFlagData bfd ON bfd.entityId = org.organisationId
	INNER JOIN flag fla ON fla.flagId = bfd.flagId AND fla.flagTextId = 'DummyAccount'
	<CFELSE>
	LEFT OUTER JOIN (SELECT bfd.entityId
	                   FROM booleanFlagData bfd
	             INNER JOIN flag fla ON fla.flagId = bfd.flagId AND fla.flagTextId = 'DummyAccount')
				 AS dummyOrg ON dummyOrg.entityId = org.organisationId
 	</CFIF>
         WHERE org.organisationId != #Form.FromOrg#
	<CFIF checkDummyOrg.recordCount EQ 0>
 		   AND dummyOrg.entityId IS NULL
   	</CFIF>
      ORDER BY org.organisationName
	</CFQUERY>

	<CFQUERY NAME="blank" datasource="#application.SiteDataSource#">
		SELECT organisationId as column1
		      ,organisationName as column2
		  FROM organisation
		 WHERE organisationid = -1
	</CFQUERY>
</CFIF>

<CFIF IsDefined("Form.ToOrgs")>

	<CFQUERY name="GetToOrgs" datasource="#application.SiteDataSource#">
		SELECT org.organisationid
		      ,org.organisationName
  		  FROM organisation AS org
	     WHERE org.organisationID  IN ( <cf_queryparam value="#Form.ToOrgs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	  ORDER BY org.organisationName
	</CFQUERY>

</CFIF>


<!--- Table cell bg colors.
	  Escape the # sign with another # sign. BGColor1="##ffff99" --->
<cfset BGColor1="##FFFFFF">
<cfset BGColor2="##ffff99">

<CF_TRANSLATE>
<SCRIPT type="text/javascript" src="../javascript/verifyInteger.js"></script>

<SCRIPT type="text/javascript">

<!--

function checkInteger(inputAmount) {
	verifyInteger(inputAmount.value, 'integer');
	if (msg != '') {
		alert(msg);
	}
}

function verifyForm() {
	var form = document.transferAmounts;
		balance = eval('form.Balance.value');
		totalPoints=0;
		msg='';

	if (form.ToOrgs.length == null) {
		var points = eval('form.TransferPoints_' + form.ToOrgs.value + '.value');

		if (points == '') {
			 msg += 'phr_PointsAmountMustBeEntered';
		}

		if (points != '') {
			msg += verifyInteger(points, 'integer');
		}

		if (parseInt(points) > parseInt(balance)) {
			msg += 'phr_TransferAmountExceeded';
	 	}

	}
	else
	{
		for (i=0; i<form.ToOrgs.length; i++) {
			var points = eval('form.TransferPoints_' + form.ToOrgs[i].value + '.value');

			if (points == '') {
				 msg += 'phr_PointsAmountMustBeEntered';
				 break;
			}

			if (points != '') {
				verifyInteger(points, 'integer');
				if (msg != '') {
					break;
				}
			}

			totalPoints = parseInt(totalPoints) + parseInt(points);

			if (parseInt(totalPoints) > parseInt(balance)) {
				msg += 'phr_TransferAmountExceeded';
				break;
		 	}
		}
	}

	if (msg != '') {
		alert (msg);
		return false;
	}
	else {
		//Don't seem to be any errors on the form
		return true;
	}
}

function formSubmit() {

	var form = document.transferAmounts;

	if (verifyForm()) {
		form.submit();
	}
}

function submitForm(formName) {

	var form = formName;
	form.submit();

}

//-->

</SCRIPT>

<FORM ACTION="TransferRWPromotionPoints.cfm" NAME="inputCriteria" METHOD="post">
<TABLE ALIGN="center">
	<CFOUTPUT>
	<!--- Abort if the user does not have access to any regions designated for the InFocus programme --->
	<CFIF getRegions.recordCount EQ 0>
		Sorry this view is only available to the Support Team
		<CF_ABORT>
	<!--- If the user only has access to one region, for example, the InFocus champions, then the report
	      can only be run for that region. Therefore do not show the region selection box --->
	<CFELSEIF getRegions.recordCount EQ 1>
		<CF_INPUT NAME="region" TYPE="Hidden" VALUE="#getRegions.countryId#">
	<CFELSE>
		<TR><TD CLASS="Title2">phr_SelectRegion :</TD>
			<TD>
				<SELECT NAME="region">
					<CFLOOP QUERY="getRegions">
						<OPTION VALUE="#countryId#" <CFIF IsDefined("region") AND IsDefined("Form.region") AND Form.region EQ countryId>SELECTED</CFIF>>#htmleditformat(countryDescription)#</OPTION>
					</CFLOOP>
				</SELECT>
			</TD>
		</TR>
	</CFIF>
	<TR><TD CLASS="Title2">phr_SelectFocusLevel :</TD>
		<TD>
			<SELECT NAME="focusLevel">
				<!--- select partner level --->
				<CFLOOP QUERY="getPartnerLevel">
					<OPTION VALUE="#name#" <CFIF IsDefined("focusLevel") AND IsDefined("Form.focusLevel") AND Form.focusLevel EQ name>SELECTED</CFIF>>#htmleditformat(name)#</OPTION>
				</CFLOOP>
			</SELECT>
		</TD>
	</TR>
	</CFOUTPUT>
</TABLE>
<TABLE ALIGN="center">
	<TR>
		<TD ALIGN="center"><A HREF="javascript:submitForm(document.inputCriteria)">phr_GetPartners</A></TD>
	</TR>
	<INPUT TYPE="hidden" NAME="SelectTransferors" VALUE="Select Transferors">
</TABLE>
</FORM>

<CFIF IsDefined("Form.SelectTransferors")>
  	<FORM ACTION="TransferRWPromotionPoints.cfm" METHOD="POST" NAME="transferor">
	<CFIF GetRecords.RecordCount EQ 0>
		<TABLE ALIGN="center">
			<TR>
				<TD COLSPAN="4">phr_NoOrganisationsExist</TD>
			</TR>
		</TABLE>
	<CFELSE>
		<TABLE ALIGN="center">
			<TR>
				<TD CLASS="Title2">phr_SelectTransferor :</TD>
				<TD>
					<SELECT NAME="FromOrg">
						<CFOUTPUT QUERY="getRecords">
							<OPTION VALUE="#organisationId#" <CFIF IsDefined("fromOrg") AND IsDefined("Form.fromOrg") AND Form.fromOrg EQ organisationId>SELECTED</CFIF>>#htmleditformat(organisationName)#</OPTION>
						</CFOUTPUT>
					</SELECT>
				</TD>
			</TR>
		</TABLE>
		<TABLE ALIGN="center">
			<TR>
				<TD ALIGN="center"><A HREF="javascript:submitForm(document.transferor)">phr_SelectTransferees</A></TD>
			</TR>
		 	<CFOUTPUT>
			<CFIF IsDefined("Form.Region")>
				<CF_INPUT TYPE="hidden" NAME="Region" VALUE="#Form.Region#">
			</CFIF>
			<CFIF IsDefined("Form.FocusLevel")>
				<CF_INPUT TYPE="hidden" NAME="FocusLevel" VALUE="#Form.FocusLevel#">
			</CFIF>
			<INPUT TYPE="hidden" NAME="SelectTransferees" VALUE="Select Transferees"></TD>
			</CFOUTPUT>
		</TABLE>
	</CFIF>
  	</FORM>
</CFIF>

<CFIF IsDefined("Form.SelectTransferees")>
	<!--- NJH 2009-02-09 P-SNY047 changed from form to cfform as twoSelectsComboQuery changed to using cfselect --->
	<CFFORM ACTION="TransferRWPromotionPoints.cfm" METHOD="POST" NAME="transferees">
	<P>
	<FONT SIZE="+2"><B>phr_Transferor</B></FONT>
	<TABLE ALIGN="center">
	<TR>
		<TH>phr_organisationName</TH>
		<TH>phr_currentBalance</TH>
		<TH>phr_frozenPoints</TH>
	</TR>
	<CFOUTPUT QUERY="getFromOrg">
		<TR>
			<TD>#htmleditformat(OrganisationName)#</TD>

			<cfinclude template="/incentive/sql_PointsBalance.cfm">
			<CFSET balancebroughtforward = val(PointsBalance.balance)>
			<TD>#balancebroughtforward#</TD>

 			<cfinclude template="/incentive/sql_FrozenPoints.cfm">
			<CFSET frozenPoints = val(FrozenPoints.FrozenPoints)>
			<TD>#frozenPoints#</TD>

		</TR>
	</CFOUTPUT>
	</TABLE>

	<CFIF GetRecords.RecordCount EQ 0>
		<TABLE ALIGN="center">
			<TR>
				<TD COLSPAN="2">There are no Tranferee Organisations available</TD>
			</TR>
		</TABLE>
	<CFELSE>
		<TABLE ALIGN="center">
			<TR>
				<TD colspan="3" align="center">
					<CF_TwoSelectsComboQuery
				    NAME="AllToOrgs"
				    NAME2="ToOrgs"
 				    QUERY1="blank"
 				    QUERY2="GetRecords"
				    SIZE="10"
				    WIDTH="300px"
				    FORCEWIDTH="50"
				    CAPTION="<FONT SIZE=-1><B>phr_AllOrganisations :</B></FONT>"
				    CAPTION2="<FONT SIZE=-1><B>phr_TransferPointsTo :</B></FONT>"
					UP="No"
					DOWN="No"
					FORMNAME="transferees">
				</TD>
			</TR>
		</TABLE>

		<TABLE ALIGN="center">
		<TR>
			<TD ALIGN="center"><A HREF="javascript:submitForm(document.transferees)">phr_InputTransferAmounts</A></TD>
		</TR>
	 	<CFOUTPUT>
		<CFIF IsDefined("Form.Region")>
			<CF_INPUT TYPE="hidden" NAME="Region" VALUE="#Form.Region#">
		</CFIF>
		<CFIF IsDefined("Form.FocusLevel")>
			<CF_INPUT TYPE="hidden" NAME="FocusLevel" VALUE="#Form.FocusLevel#">
		</CFIF>
		<CFIF IsDefined("Form.FromOrg")>
			<CF_INPUT TYPE="hidden" NAME="FromOrg" VALUE="#Form.FromOrg#">
		</CFIF>
		<INPUT TYPE="hidden" NAME="TransferAmounts" VALUE="Transfer Amounts">
		</CFOUTPUT>
		</TABLE>
	</CFIF>
	</CFFORM>
</CFIF>

<CFIF IsDefined("Form.TransferAmounts") OR
      IsDefined("Form.RedisplayTransfer")>
	<FORM ACTION="TransferRWPromotionPointsTask.cfm" METHOD="POST" NAME="transferAmounts">
	<P>
	<FONT SIZE="+2"><B>phr_Transferor</B></FONT>
	<TABLE ALIGN="center">
	<TR>
		<TH>phr_organisationName</TH>
		<TH>phr_currentBalance</TH>
		<TH>phr_frozenPoints</TH>
		<CFIF IsDefined("Form.RedisplayTransfer")>
			<TH>phr_previousBalance</TH>
		</CFIF>
	</TR>
	<CFOUTPUT QUERY="getFromOrg">
		<TR>
			<TD>#htmleditformat(OrganisationName)#</TD>

			<cfinclude template="/incentive/sql_PointsBalance.cfm">
			<CFSET balancebroughtforward = val(PointsBalance.balance)>
			<TD>#balancebroughtforward#</TD>
			<CF_INPUT TYPE="hidden" NAME="PreviousBalance" VALUE="#balancebroughtforward#">
			<CF_INPUT TYPE="hidden" NAME="Balance" VALUE="#balancebroughtforward#">

 			<cfinclude template="/incentive/sql_FrozenPoints.cfm">
			<CFSET frozenPoints = val(FrozenPoints.FrozenPoints)>
			<TD>#frozenPoints#</TD>

			<CFIF IsDefined("Form.RedisplayTransfer")>
				<CFIF CFSTOREDPROC.STATUSCODE NEQ 0>
					<TD><B>Transfer Failed</B></TD>
				<CFELSE>
					<TD>#PreviousBalance#</TD>
				</CFIF>
			</CFIF>
		</TR>

	</CFOUTPUT>
	</TABLE>

	<P>
	<FONT SIZE="+2"><B>phr_Transferees</B></FONT>
	<TABLE ALIGN="center">
	<TR>
		<TH>phr_organisationName</TH>
		<TH>phr_currentBalance</TH>
		<CFIF IsDefined("Form.TransferAmounts")>
			<TH>phr_transferPoints</TH>
		<CFELSE>
			<TH>phr_previousBalance</TH>
		</CFIF>
	</TR>

	<CFIF NOT IsDefined("ToOrgs")>
		<TR>
			<TD COLSPAN="3">No Tranferee Organisations were selected</TD>
		</TR>
	<CFELSE>

		<CFOUTPUT QUERY="getToOrgs">
			<TR>
				<TD>#htmleditformat(OrganisationName)#</TD>

				<cfinclude template="/incentive/sql_PointsBalance.cfm">
				<CFSET balancebroughtforward = val(PointsBalance.balance)>
				<TD>#balancebroughtforward#</TD>
				<CF_INPUT TYPE="hidden" NAME="PreviousBalance_#organisationId#" VALUE="#balancebroughtforward#">

				<CFIF IsDefined("Form.TransferAmounts")>
					<TD><CF_INPUT TYPE="text" NAME="TransferPoints_#organisationId#"></TD>
				<CFELSE>
					<CFIF CFSTOREDPROC.STATUSCODE NEQ 0>
						<TD><B>Transfer Failed</B></TD>
					<CFELSE>
						<TD>#evaluate("PreviousBalance_#htmleditformat(organisationId)#")#</TD>
					</CFIF>
				</CFIF>
			</TR>
			<CF_INPUT TYPE="hidden" NAME="ToOrgs" VALUE="#organisationId#">
		</CFOUTPUT>
	</CFIF>
	</TABLE>

	<CFIF IsDefined("ToOrgs")>
		<CFIF IsDefined("Form.TransferAmounts")>
			<TABLE ALIGN="center">
			<TR>
				<TD ALIGN="center"><A HREF="javascript:formSubmit()">phr_TransferAmounts</A></TD>
			</TR>
			</TABLE>
		 	<CFOUTPUT>
			<CFIF IsDefined("Form.Region")>
				<CF_INPUT TYPE="hidden" NAME="Region" VALUE="#Form.Region#">
			</CFIF>
			<CFIF IsDefined("Form.FocusLevel")>
				<CF_INPUT TYPE="hidden" NAME="FocusLevel" VALUE="#Form.FocusLevel#">
			</CFIF>
			<CFIF IsDefined("Form.FromOrg")>
				<CF_INPUT TYPE="hidden" NAME="FromOrg" VALUE="#Form.FromOrg#">
			</CFIF>
			<INPUT TYPE="hidden" NAME="RedisplayTransfer" VALUE="1">
			</CFOUTPUT>
		</CFIF>
	</CFIF>
	</FORM>
</CFIF>

</CF_TRANSLATE>


