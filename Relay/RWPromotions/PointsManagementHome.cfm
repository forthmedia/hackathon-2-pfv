<!--- �Relayware. All Rights Reserved 2014 --->



<cf_head>
	

<!---Initialize variables expected later in the code--->
<CFPARAM DEFAULT="Last" NAME="frmType"> 



	<SCRIPT>
		
	
<!---Make sure either the name of a company or the name of a person is entered when placing an order---> 
		function checkordering(){
		
		var form = window.document.form1
		var companyfield = form.frmCompany.value
		var namefield = form.frmName.value
		
		
		if (companyfield == '' && namefield == '') {
			alert("You must enter either a Company or a Person's Name")
			
		} 
		else form.submit()
		}
	
	</SCRIPT>
	
</cf_head>


	
<!---Search screen--->
<CFIF NOT IsDefined("frmCompany") AND NOT IsDefined("frmlocationID") AND NOT IsDefined("frmpersonID")>
	
	<TABLE align="center">
		<TR>
			<TD colspan="3" align="center"><H2>Manage a Points Account</H2></TD>
		</TR>
					
		<FORM ACTION="PointsManagementHome.cfm" NAME="form1" METHOD="post">
				
				
				
					
		<TR>
			<TD align="right">Company Name:</TD> 
			<TD colspan="2"><INPUT TYPE="text" NAME="frmCompany"></TD>
		</TR>
		
		

		<TR>
			<TD align="right">Person Name:</TD> 
			<TD colspan="2"><INPUT TYPE="text" NAME="frmName"><BR></TD>
		</TR>
		
		<TR>
			<TD colspan="3">
			<FONT FACE="Arial, Chicago, Helvetica" SIZE=2>
			<INPUT TYPE="RADIO" NAME="frmType" VALUE="First" <CFIF frmType IS 'First'>Checked</CFIF>> First name
			&nbsp; &nbsp;
			<INPUT TYPE="RADIO" NAME="frmType" VALUE="Last" <CFIF frmType IS 'Last'>Checked</CFIF>> Last name
			&nbsp; &nbsp;
			<INPUT TYPE="RADIO" NAME="frmType" VALUE="Full" <CFIF frmType IS 'Full'>Checked</CFIF>> Full name
			</FONT>
			</TD>
		</TR>
		
		
		
		<TR>
			<TD colspan="3" align="center"><A HREF="javascript:checkordering()"><IMG SRC="<CFOUTPUT></CFOUTPUT>/IMAGES/BUTTONS/c_search_e.gif" WIDTH=84 HEIGHT=21 BORDER=0 ALT="Search"></A></TD>
		</TR>
				
		</FORM>

</TABLE>
				
				
				
				

			

<CFELSE>

<!---The SECOND PHASE, get all the people or companies corresponding to the search criteria
and list them or, if a straight match, go directly to the points management screen--->

	<CFQUERY NAME="GetDetails" DATASOURCE="#application.SiteDataSource#" DBTYPE="ODBC">
	SELECT DISTINCT l.Sitename, l.LocationID,l.countryID,l.organisationID,
	<CFIF frmName IS NOT ''>Person.Firstname, Person.LastName, Person.PersonID,</CFIF>
	Country.CountryDescription AS CountryName
	FROM Location l inner join RWCompanyAccount
	on l.organisationID = RWCompanyAccount.organisationID
	inner join Country on l.CountryID = Country.CountryID
	<CFIF frmName IS NOT ''>Person, RWPersonalAccount,</CFIF>
	WHERE 1=1
	
	<CFIF frmName IS NOT ''>
		AND l.LocationID = Person.LocationID 
		AND Person.PersonID = RWPersonalAccount.personID
	</CFIF> 
	
	<CFIF frmCompany IS NOT ''> 
		AND l.SiteName LIKE '#frmCompany#%'
	</CFIF>
	
	<CFIF frmName IS NOT ''>
		<CFSWITCH EXPRESSION="#FrmType#">
			<CFCASE VALUE="First">AND Person.FirstName LIKE '#Trim(frmName)#%'</CFCASE>
			<CFCASE VALUE="Last">AND Person.LastName LIKE '#Trim(frmName)#%'</CFCASE>
			<CFCASE VALUE="Full">AND {fn CONCAT({fn CONCAT(Person.FirstName,' ')},Person.LastName)} = '#frmName#'</CFCASE>
		</CFSWITCH>
	</CFIF>
	
	
	</CFQUERY>
	
	
<!---Company matches--->
	<CFIF GetDetails.RecordCount GT 1>
	
		<CFIF #frmCompany# IS NOT ''>
		
			<TABLE align="center">
			<tr>
				<th>Company to review</th>
				<th></th>
			</tr>
			<cfoutput query="getDetails">
			
				<CFSET countryid = getDetails.countryid>
				<tr>
					<td>
						<A HREF="PointsManagement.cfm?frmorgID=#organisationID#">#htmleditformat(SiteName)#</A></B><BR>
					</td>
					<td align="center">&nbsp;
					</td>
				</tr>	
				
			</cfoutput>
		
			</TABLE>
	
		
<!---Person Matches--->
			<CFELSEIF #frmName# IS NOT ''>
		
			<TABLE align="center">
			
			<cfoutput query="getDetails">
			
				<tr>
					<td>
						<A HREF="PointsManagement.cfm?frmPersonID=#PersonID#&Home=1">#htmleditformat(FirstName)# #htmleditformat(LastName)#</A><BR>
					#htmleditformat(SiteName)#
					</td>
						
				</tr>
						
				
			</cfoutput>
		
			</TABLE>
			
		</CFIF>

	
<!---If a straight match go directly to the Management Screen--->
	<CFELSEIF GetDetails.RecordCount EQ 1>

		<CFIF #frmCompany# IS NOT ''>
	
			<CFLOCATION URL="PointsManagement.cfm?frmlocationID=#GetDetails.locationID#&Home=1"addToken="false">
	
		<CFELSE>
			
			<CFLOCATION URL="PointsManagement.cfm?frmpersonID=#GetDetails.personID#&Home=1"addToken="false">
		
		</CFIF>	
	
<!---If no records found--->
	<CFELSEIF GetDetails.RecordCount EQ 0>
	
		No records matching the given criteria. It could be that a Program Account has not yet been set for your Company.
	
	</CFIF>
	
</CFIF>
	

















