<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Get the countries the user has access to --->
<CFINCLUDE TEMPLATE="../templates/qryGetCountries.cfm">

<!--- Get the regions designated for the InFocus Program --->
<CFQUERY NAME="getIncentiveRegions" datasource="#application.SiteDataSource#">
	SELECT cou.countryId
	      ,cou.countryDescription
	  FROM country cou
		  ,booleanFlagData bfd
		  ,flag fla
	 WHERE cou.countryId = bfd.entityId 
	   AND bfd.flagId = fla.flagId
	   AND fla.flagTextId = 'IncentiveRegion'
  ORDER BY cou.countryId					  
</CFQUERY>

<CFSET IncentiveRegions = ValueList(getIncentiveRegions.countryId)>

<!--- Get the regions designated for the Incentive Program 
where the user has access to at least one country in that region --->	   
<CFQUERY NAME="getRegions" datasource="#application.SiteDataSource#">
	SELECT cou.countryId
	      ,cou.countryDescription
	  FROM country cou
		  ,booleanFlagData bfd
		  ,flag fla
	 WHERE cou.countryId = bfd.entityId 
	   AND bfd.flagId = fla.flagId
	   AND fla.flagTextId = 'IncentiveRegion' <!--- used to be InFocusRegion --->
 	   AND EXISTS (SELECT NULL 
	                 FROM countryGroup cgr
	                WHERE cgr.countryMemberId  IN ( <cf_queryparam value="#CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	                  AND cgr.countryGroupId = cou.countryId)
  ORDER BY cou.countryId					  
</CFQUERY>

<CFSET RegionAccess = ValueList(getRegions.countryId)>

<!--- Get the Partner Levels --->	   
<CFQUERY NAME="getPartnerLevel" datasource="#application.SiteDataSource#">
	SELECT f.name
	  FROM flag f
		  INNER JOIN flagGroup fg ON f.flagGroupId = fg.flagGroupId
	 WHERE fg.flagGroupTextId = 'PartnerType' <!--- used to be partnerFocusLevel --->
</CFQUERY>
