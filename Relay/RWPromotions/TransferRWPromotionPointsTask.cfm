<!--- �Relayware. All Rights Reserved 2014 --->
<CFSET PointsAmountList = ''>
<CFLOOP INDEX="ToOrgId" LIST="#ToOrgs#">
	<CFIF PointsAmountList EQ ''>
		<CFSET PointsAmountList = evaluate("TransferPoints_#ToOrgId#")>
	<CFELSE>	
		<CFSET PointsAmountList = PointsAmountList & ',' & evaluate("TransferPoints_#ToOrgId#")>
	</CFIF>
</CFLOOP>	
			
<CFSTOREDPROC PROCEDURE="RWMultiTransferPoints" DATASOURCE="#application.SiteDataSource#" RETURNCODE="Yes">
	<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_INTEGER" DBVARNAME="@FromOrganisationID" VALUE="#FromOrg#" NULL="No">
	<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_VARCHAR" DBVARNAME="@ToOrganisationIDList" VALUE="#ToOrgs#" NULL="No">
	<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_VARCHAR" DBVARNAME="@PointsAmountList" VALUE="#PointsAmountList#" NULL="No"> 
	<CFPROCPARAM TYPE="In" CFSQLTYPE="CF_SQL_INTEGER" DBVARNAME="@PersonID" VALUE="#request.relayCurrentUser.personid#" NULL="No">					
</CFSTOREDPROC>

<CFINCLUDE TEMPLATE="TransferRWPromotionPoints.cfm">

