<!--- �Relayware. All Rights Reserved 2014 --->
<CFPARAM NAME="current" TYPE="string" DEFAULT="RWPromotion_List.cfm">

<cfparam name="ModuleCode" default="RW">

<cfif ModuleCode EQ "OPP">
	<cfset pageTitle = 'Opportunities'>
<cfelse>
	<cfset pageTitle = 'Incentives'>
</cfif>
	<CF_RelayNavMenu pageTitle="#pageTitle#" thisDir="points">
	<!--- 2014-10-30	RPW	CORE-145 Promotions filter values confused - Show back button for adding new promotions and removed List Promotions --->	
	<CF_RelayNavMenuItem MenuItemText="Add New Promotion" CFTemplate="RWPromotion.cfm?ModuleCode=#ModuleCode#&editor=yes&hideBackButton=false" target="mainSub">
	<cfif ModuleCode EQ "RW" and application.com.login.checkInternalPermissions ("incentiveTask", "Level3")>
		<!--- <CF_RelayNavMenuItem MenuItemText="Transfer Promotion Points" CFTemplate="TransferRWPromotionPoints.cfm" target="mainSub"> --->
	<CF_RelayNavMenuItem MenuItemText="Allocate Promotion Points" CFTemplate="allocateRWPromotionPoints.cfm?ModuleCode=#ModuleCode#" target="mainSub">
	</cfif>
</CF_RelayNavMenu>