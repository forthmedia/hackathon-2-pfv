<!--- �Relayware. All Rights Reserved 2014 --->
<CFLOOP INDEX="frmOrganisationId" LIST="#frmAllOrganisationId#">

	<CFIF IsDefined("RWPromotionCode_#frmOrganisationId#") AND 
	 	  evaluate("RWPromotionCode_#frmOrganisationId#") NEQ '' AND
          IsDefined("Points_#frmOrganisationId#") AND 
		  evaluate("Points_#frmOrganisationId#") NEQ ''>

		<CFSET frmRWPromoId = evaluate("RWPromotionCode_#frmOrganisationId#")>
		<CFSET frmPointsAmount = evaluate("Points_#frmOrganisationId#")>
		<CFSET frmDatePassed = evaluate("LiabilityDate_#frmOrganisationId#")>
	
		<CFINCLUDE TEMPLATE="AddIndividualPoints.cfm">
		
	</CFIF>	

</CFLOOP>

<CFINCLUDE TEMPLATE="AllocateRWPromotionPoints.cfm">
