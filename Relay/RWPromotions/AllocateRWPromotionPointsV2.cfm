<!--- �Relayware. All Rights Reserved 2014 --->
<!---
QUESTIONS
do we need to country scope which countries can be allocated to by users countryRights?

2011/05/05 WAB LID  6480.  Changes done by Nat last month (change to cfform) had removed the JS which submitted some of the forms.  Added back equivalent code
--->

<cf_relayFormDisplay>

<cfif not(application.com.login.checkInternalPermissions ("incentiveTask", "Level3"))>
	<tr><td colspan="2" class="messageText">Sorry this view is only available to Incentive Administrators</td></tr>
<cfelse>
	<cfform name="mainForm" action="AllocateRWPromotionPoints.cfm" method="post">

	<cfif structkeyexists(form,"personID") and structkeyexists(form,"points") and structkeyexists(form,"rwPromotionID") and not structkeyexists(form,"abortSubmit")>
		<cfset frmRWPromoId = form.rwPromotionID>
		<cfset frmPointsAmount = form.points>
		<cfset frmPersonId = form.personID>
	 	<!--- if the last transaction for this person was identical catch it - maybe alter to be time sensitive?--->
		<cfquery name="checkForExistence" datasource="#application.sitedatasource#" maxrows="1">
			SELECT     max(RWTransaction.RWTransactionID) AS thisID,
	        (SELECT     MAX(rwtransactionid)
	        FROM rwtransaction where personid =  <cf_queryparam value="#frmPersonId#" CFSQLTYPE="CF_SQL_INTEGER" > ) AS lastID
			FROM RWTransaction INNER JOIN
	        RWTransactionItems ON RWTransaction.RWTransactionID = RWTransactionItems.RWTransactionID
			WHERE     (RWTransaction.personid =  <cf_queryparam value="#frmPersonId#" CFSQLTYPE="CF_SQL_INTEGER" > ) AND (RWTransactionItems.RWPromotionID =  <cf_queryparam value="#frmRWPromoId#" CFSQLTYPE="CF_SQL_INTEGER" > ) AND (RWTransaction.CreditAmount =  <cf_queryparam value="#frmPointsAmount#" CFSQLTYPE="CF_SQL_Integer" > )
		</cfquery>
		<cfif structkeyExists(form,"checkForExistenceOverride") and form.checkForExistenceOverride eq "True">
			<cfset checkForExistence.thisID = 0>
			<cfset checkForExistence.lastID = 1>
		</cfif>
		<tr><td colspan="2" class="messageText">
		<cfif checkForExistence.recordcount neq 0>
			<cfif checkForExistence.thisID eq checkForExistence.lastID and checkForExistence.lastID is not "">
				<cfoutput>
					<CF_INPUT type="hidden" name="countryID" value="#form.countryID#">
					<CF_INPUT type="hidden" name="rwPromotionID" value="#form.rwPromotionID#">
					<CF_INPUT type="hidden" name="personID" value="#form.personID#">
					<CF_INPUT type="hidden" name="points" value="#form.points#">
					<input type="hidden" name="checkForExistenceOverride" value="True">
					This person last received #htmleditformat(frmpointsAmount)# points with this promotion code. Are you sure?<br>
					<input type="button" name="submitButton" value="Yes" onclick="this.form.submit()"> <input type="button" name="abortSubmit" value="No" onclick="JavaScript:window.history.back();">
				</cfoutput>
				<cfset continueProcessing = "false">
			<cfelse>
				<cfinclude template="AddIndividualPointsV2.cfm">
				phr_Points_Accrued
			</cfif>
		</cfif>
		</td></tr>


	</cfif>

	<cfparam name="continueProcessing" default="true">
	<cfif structkeyexists(form,"countryID") and continueProcessing>
		<cfoutput>
			<CF_INPUT type="hidden" name="countryID" value="#form.countryID#">
		</cfoutput>
		<cfscript>
			incentiveUsers = application.com.relayIncentive.getIncentiveUsers(countryID = form.countryID);
			getAdhocPromotions = application.com.relayIncentive.getAdhocPromotions(countryID = form.countryID);
		</cfscript>

		<cfif getAdhocPromotions.recordcount eq 0>
			There are no adhoc promotions currently active in this country.
		<cfelseif incentiveUsers.recordcount eq 0>
			There are no incentive users in this country.
		<cfelse>
			<tr>
				<td class="label">country:</td>
				<td><cfoutput query="incentiveUsers" maxrows="1">#htmleditformat(countryDescription)#</cfoutput></td>
			</tr>
			<cfoutput>

			<cfquery name="getAdhocPromotions" dbType="query">
				select rwPromotionId, rwPromotionCode+'-'+rwPromotionDescription as description from getAdhocPromotions
			</cfquery>

			<cfparam name="form.rwPromotionId" default=0>
			<cf_relayFormElementDisplay relayFormElementType="select" label="Choose a promotion" fieldname="rwPromotionID" currentValue="#form.rwPromotionId#" query="#getAdhocPromotions#" display="description" value="rwPromotionID" required="true" nullText="Choose a Promotion">


			<cfquery name="incentiveUsers" dbType="query">
				select personID, organisationname+'-'+fullname as name from incentiveUsers
			</cfquery>

			<cf_relayFormElementDisplay label="Choose a person" relayFormElementType="select" fieldname="personID" currentValue="" query="#incentiveUsers#" display="name" value="personID" required="true" nullText="Choose a person">

			<cf_relayFormElementDisplay label="Enter points amount to accrue (negative to remove points)" relayFormElementType="numeric" fieldname="points" currentValue="" size="5" required="true">

			<tr>
				<td  colspan="2"><input type="submit" name="submitButton" value="Allocate Points"></td>
			</tr>
			</cfoutput>
		</cfif>
	<cfelseif continueProcessing>
		<cfscript>
			incentiveCountrys = application.com.relayIncentive.getIncentiveUserCountries(countryRightsScope = true);
		</cfscript>
		<tr>
			<td class="label">Choose a country:</td>
		<cfoutput>
			<td>
				<select name="countryID" onchange="if(this.options[this.selectedIndex].value != '') {this.form.submit()}">
					<option value="">Choose a Country
					<cfloop query="incentiveCountrys">
						<option value="#countryID#">#htmleditformat(countryDescription)#
					</cfloop>
				</select>
			</td>
		</cfoutput>
		</tr>
	</cfif>
	</cfform>
</cfif>

</cf_relayFormDisplay>