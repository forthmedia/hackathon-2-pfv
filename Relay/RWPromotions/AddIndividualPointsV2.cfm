<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name: AddIndividualPointsV2.cfm	
Author:  Gawain Claridge
Date started:		2005/12/09
	
Description: CR_SNY540 Created to be used by Incentive Administrators to add / remove points to an account using 'adhoc' promotion codes

Usage:			

Amendment History:

Date 		Initials 	What was changed
2011/02/18	PPB			LID5723 - post negative accruals as a positive AL transaction
2011/02/21 	PPB 		LID5723 - added try/catch to display error more neatly 

Enhancements still to do:
--->


 
<CFQUERY NAME="GetAccount" DATASOURCE="#application.SiteDataSource#">
SELECT     AccountID, GETDATE() AS date, OrganisationID
FROM         RWCompanyAccount rca
WHERE     (OrganisationID = (select organisationid from person where personid =  <cf_queryparam value="#frmPersonID#" CFSQLTYPE="CF_SQL_INTEGER" > ))
</CFQUERY>
					

<CFIF getAccount.RecordCount EQ 0>

	An account does not exist for this person <BR>
	No Points added

<cfelse>	
	<CFIF IsDefined("frmRWPromoID")> 
		<CFSET PointsType = 'PP'>
	<CFELSE>
		<CFSET PointsType = 'MP'>	
	</CFIF>
	<CFPARAM name="frmDatePassed"  default="#GetAccount.date#">
		<!--- Set the date format to 'YYYY-DD-MM' as it is entered as DD/MM/YYYY. For some reason the month and 
		      day are transposed if the mask 'YYYY-MM-DD' is used --->
	<CFSET frmDatePassed = dateFormat(#frmDatePassed#, "YYYY-MM-DD")>

	<cftry> <!--- 2011/02/21 PPB LID5723 added try/catch to display error more neatly --->
	<cfquery name="PointsAccrued" DATASOURCE="#application.SiteDataSource#">
		<!--- 2011/02/18 PPB LID5723 START  - post negative accruals as a positive AL transaction --->
	
		<cfif frmPointsAmount gte 0>		
		exec RWAccruePoints @AccountID =  <cf_queryparam value="#GetAccount.AccountId#" CFSQLTYPE="CF_SQL_INTEGER" > , @RWTransactionTypeID = 'AC', @AccruedDate =  <cf_queryparam value="#frmDatePassed#" CFSQLTYPE="CF_SQL_TIMESTAMP" > , @PointsAmount =  <cf_queryparam value="#frmPointsAmount#" CFSQLTYPE="CF_SQL_INTEGER" > , @CreatedPersonID = #request.relaycurrentuser.personid#, @PersonID =  <cf_queryparam value="#frmPersonID#" CFSQLTYPE="CF_SQL_INTEGER" > , @Type =  <cf_queryparam value="#pointstype#" CFSQLTYPE="CF_SQL_VARCHAR" > 
		<cfelse>
			exec RWCreateTransaction @OrganisationID =  <cf_queryparam value="#GetAccount.OrganisationID#" CFSQLTYPE="CF_SQL_INTEGER" > , @TXTypeID = 'AL', @PointsAmount =  <cf_queryparam value="#abs(frmPointsAmount)#" CFSQLTYPE="CF_SQL_INTEGER" > , @OrderID = NULL, @PersonID =  <cf_queryparam value="#frmPersonID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfif>
			
		<!--- 2011/02/18 PPB LID5723 END --->
	</cfquery>

		<cfcatch type="database">
			<script type="text/javascript">
				alert('<cfoutput>#replace(cfcatch.detail,"[Macromedia][SQLServer JDBC Driver][SQLServer]","")#</cfoutput>');		
			</script>
		</cfcatch>
	</cftry>

	<cfquery name="LatestTransactionID" DATASOURCE="#application.SiteDataSource#">
		SELECT max(RWTransactionID) as maxTransID
		FROM RWTransaction
	</cfquery>
	<cfscript>
		application.com.relayIncentive.AddRWTransactionItems(RWTransactionID=LatestTransactionID.maxTransID,ItemID=0,quantity=1,points=frmPointsAmount,pointsType='PP',RWPromotionID=frmRWPromoId); 
	</cfscript>
</CFIF>	

