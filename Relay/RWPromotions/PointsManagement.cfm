<!--- �Relayware. All Rights Reserved 2014 --->



<!---If it is a person coming in--->
<CFIF IsDefined("frmpersonID")>

	<!---Get company account the person belongs to--->
	<CFQUERY NAME="GetAccount" DATASOURCE="#application.SiteDataSource#" DBTYPE="ODBC">
		SELECT DISTINCT CompanyAccountID  
		FROM RWPersonalAccount
		WHERE personID = #frmpersonID#
	</CFQUERY>
	
	<CFSET AccountID = GetAccount.CompanyAccountID>
	
	<!---Set entity variables--->
	<CFSET EntityLabel = "frmpersonID">
	<CFSET EntityValue = frmpersonID>
	

<!---If it is a company coming in--->
<CFELSEIF isDefined("frmorgID")>

	<!---Get it's own account--->
	<CFQUERY NAME="GetAccount" DATASOURCE="#application.SiteDataSource#" DBTYPE="ODBC">
		SELECT AccountID 
		FROM RWCompanyAccount
		WHERE organisationID = #frmorgID#
	</CFQUERY>
	
	<CFSET AccountID = GetAccount.AccountID>
	
	<!---Set entity variables--->
	<CFSET EntityLabel = "frmorgID">
	<CFSET EntityValue = frmorgID>
		
		
</CFIF>

<!---Get User's country--->
<CFQUERY NAME="GetCountryID" DATASOURCE="#application.SiteDataSource#" DBTYPE="ODBC">
	SELECT CountryID 
	FROM location, person 
	WHERE location.locationID = person.locationID
	AND personID = #request.relayCurrentUser.personid#
</CFQUERY>


<!---Get Distis for the combo box--->
<CFIF GetCountryID.CountryID EQ 9 OR GetCountryID.CountryID EQ 10 OR GetCountryID.CountryID EQ 87>


	<CFQUERY NAME="GetDistributors" DATASOURCE="#application.SiteDataSource#" DBTYPE="ODBC">
		SELECT sitename,(SELECT data AS email
      	FROM flag AS f, textFlagData AS fd
      		WHERE f.flagid = fd.flagid 
			AND f.flagtextid = 'DistiEmailAddress' 
			AND entityID = locationID) AS DataValue
		FROM flag AS f, integerMultipleFlagData AS fd, location AS l
		WHERE f.flagid = fd.flagid AND f.flagtextid = 'distiCountries' 
		AND l.locationid = fd.entityid AND data = 9
	</CFQUERY>


<CFELSE>

	<CFQUERY NAME="GetDistributors" DATASOURCE="#application.SiteDataSource#" DBTYPE="ODBC">
		SELECT sitename,(SELECT data AS email
      	 	FROM flag AS f, textFlagData AS fd
      		WHERE f.flagid = fd.flagid 
			AND f.flagtextid = 'DistiEmailAddress' 
			AND entityID = locationID) AS DataValue
		FROM flag AS f, integerMultipleFlagData AS fd, location AS l
		WHERE f.flagid = fd.flagid AND f.flagtextid = 'distiCountries' 
		AND l.locationid = fd.entityid AND data = #GetCountryID.CountryID#
	
		
		
		
	</CFQUERY>


</CFIF> 


<!---Form verifications depending on whether it is a Personal or Company account--->
<SCRIPT>

<CFIF IsDefined('EntityLabel')>
	
	<CFIF EntityLabel IS "frmorgID">
		
		function lineitem(){
		
			form = window.document.LineForm
		
			if (form.frmSKU.value == ''){
				alert("Please enter SKU of the product you are claiming");
				form.frmSKU.focus()
			}
			
			else if (form.frmDescription.value == ''){
				alert("Please enter the description of the product you are claiming");
				form.frmDescription.focus()
			}
			
			
			else if (form.frmUnitPoints.value == ''){
				alert("Please enter Unit Points");
				form.frmUnitPoints.focus()
			}
		
			else if (form.frmQuantity.value == ''){
				alert("Please enter Quantity")
				form.frmQuantity.focus()
			}
		
			else{
				form.submit()
			}
			
			
		
		}
		
	<CFELSE>
	
		function lineitem(){
		
			form = window.document.LineForm
		
			if (form.frmSKU.options[form.frmSKU.selectedIndex].value == 0){
				alert("Please select the SKU of the product you are claiming against");
				form.frmSKU.focus()
			}
			
			
			else if (form.frmQuantity.value == ''){
				alert("Please enter Quantity")
				form.frmQuantity.focus()
			}
		
			else{
				form.submit()
			}
			
			
		
		}
	
	
	
	
	</CFIF>
</CFIF>	
		
		function changeDelNote(){
			formtoSubmit = window.document.DelNoteForm
			form = window.document.ClaimForm
			orderID = form.DeliveryNote.options[form.DeliveryNote.selectedIndex].value
			orderfield = formtoSubmit.DeliveryNoteID
			orderfield.value = orderID
			formtoSubmit.submit()
	
		}
	
		
</SCRIPT>







<!---Data Entry if a new delivery note is added to the Company Account--->
<CFIF IsDefined("AddNewClaim")>

	<!---Insert Delivery Note --->
	<CFQUERY NAME="InsertNewDeliveryNote" DATASOURCE="#application.SiteDataSource#" DBTYPE="ODBC">
		
		INSERT INTO RWDeliveryNote(AccountID, DeliveryNoteNumber, Distributor, DeliveryNoteDate, MonetaryValue, 
		<CFIF IsDefined("frmVerified")>Verified,</CFIF> VerifiedNotes)
		
		VALUES(#AccountID#, '#fieldDelNoteNumber#', '#frmDistributor#', #frmDelNoteDate#,
		#frmOrderValue#, <CFIF IsDefined("frmVerified")>1,</CFIF> '#frmVerifiedNotes#')
	
	</CFQUERY>
	
	<!---Get Delivery Note ID--->
	<CFQUERY NAME="GetDelNoteID" DATASOURCE="#application.SiteDataSource#" DBTYPE="ODBC">
		SELECT max(DeliveryNoteID) AS DelNote
		FROM RWDeliveryNote
	</CFQUERY>
	
	<CFSET DeliveryNoteID = GetDelNoteID.DelNote>
		
		
<!---If the details of the delivery note have been changed--->
<CFELSEIF Isdefined("ConfirmUpdateClaim")>
	
	<!---Update Delivery Note Details--->
	<CFQUERY NAME="UpdateDeliveryNote" DATASOURCE="#application.SiteDataSource#" DBTYPE="ODBC">
		UPDATE RWDeliveryNote
		
		SET Distributor = '#frmDistributor#',
		DeliveryNoteNumber = '#fieldDelNoteNumber#', 
		DeliveryNoteDate = #CreateODBCDateTime(frmDelNoteDate)#,
		MonetaryValue = #frmOrderValue#,
	
	<CFIF IsDefined("frmVerified")>
		Verified = 1,
	<CFELSE>
		Verified = 0,
	</CFIF>
		
		VerifiedNotes = '#frmVerifiedNotes#'
		
		WHERE DeliveryNoteID = #DeliveryNoteID#
	
	</CFQUERY>


<!---Insert line items when they are defined--->
<CFELSEIF IsDefined("AddNewProduct")>
	
	<!---Company Account--->
	<CFIF EntityLabel IS 'frmLocationID'>
	
		<CFQUERY NAME="CheckForDuplicates" DATASOURCE="#application.SiteDataSource#" DBTYPE="ODBC">
			SELECT * from RWCompanyAccountItem
			WHERE AccountID = #AccountID#
			AND DeliveryNoteID = '#DeliveryNoteID#'
			AND ProductIDSKU = '#frmSKU#'
		</CFQUERY>
	
		<CFIF CheckForDuplicates.RecordCount EQ 0>
		
			<CFQUERY NAME="InsertNewProductItems" DATASOURCE="#application.SiteDataSource#" DBTYPE="ODBC">
				INSERT INTO RWCompanyAccountItem(AccountID, TransactionTypeID, DeliveryNoteID, ProductIDSKU, ItemDescription, 
										UnitPoints, Quantity, Created, CreatedBy)
				
				VALUES(#AccountID#, 1, '#DeliveryNoteID#', '#frmSKU#', '#frmDescription#', 
						#frmUnitPoints#, #frmQuantity#, #CreateODBCDateTime(Now())#, #request.relayCurrentUser.usergroupid#)
			</CFQUERY>
		
		<CFELSE>
		
			<SCRIPT>
				function duplicate(){
					alert("This SKU was already claimed against this delivery note. \n Please edit the existing record instead of entering a new one")
				
				}
				
				duplicate()
			</SCRIPT>
		
		</CFIF>	
	
	
	<!---Personal Account--->
	<CFELSEIF EntityLabel IS 'frmPersonID'>
	
		<!---Get PersonalAccountID--->
		<CFQUERY NAME="GetPersonalAccount" DATASOURCE="#application.SiteDataSource#" DBTYPE="ODBC">
			SELECT PersonalAccountID 
			FROM RWPersonalAccount
			WHERE PersonID = #EntityValue#
		</CFQUERY>
	
		<CFQUERY NAME="CheckForDuplicates" DATASOURCE="#application.SiteDataSource#" DBTYPE="ODBC">
			SELECT * from RWPersonalAccountItem
			WHERE PersonalAccountID = #GetPersonalAccount.PersonalAccountID#
			AND CompanyAccountItemID = '#frmSKU#'
			
		</CFQUERY>
	
		<CFIF CheckForDuplicates.RecordCount EQ 0>
	
			<CFQUERY NAME="InsertNewProductItems" DATASOURCE="#application.SiteDataSource#" DBTYPE="ODBC">
				INSERT INTO RWPersonalAccountItem(PersonalAccountID, TransactionTypeID, CompanyAccountItemID,
						QuantityClaimed, Created, CreatedBy)
				VALUES(#GetPersonalAccount.PersonalAccountID#, 1, '#frmSKU#', #frmQuantity#,  
						#CreateODBCDateTime(Now())#, #request.relayCurrentUser.usergroupid#)
			</CFQUERY>
			
		
		
		<CFELSE>
		
			<SCRIPT>
				function duplicate(){
					alert("This SKU was already claimed against this delivery note. \n Please edit the existing record instead of entering a new one")
				
				}
				
				duplicate()
			</SCRIPT>
		
		</CFIF>
	
	</CFIF>
	
</CFIF>


<!---Delete a line item--->
<CFIF IsDefined("DeleteProduct")>
	
	<!---Company Account--->
	<CFIF EntityLabel IS 'frmLocationID'>
		
		
		<!---Check if anything from this item was claimed to a Personal Account--->
		<CFQUERY NAME="CheckProduct" DATASOURCE="#application.SiteDataSource#" DBTYPE="ODBC">
		SELECT QuantityClaimed from RWCompanyAccountItem
		WHERE AccountItemID = #CFGRIDKEY#
		</CFQUERY>
		
		
		<!---If nothing has been claimed delete the item--->
		<CFIF CheckProduct.QuantityClaimed EQ 0>
		
			<CFQUERY NAME="DeleteProduct" DATASOURCE="#application.SiteDataSource#" DBTYPE="ODBC">
			DELETE from RWCompanyAccountItem
			WHERE AccountItemID = #CFGRIDKEY#
			</CFQUERY>
		
		<!---If claimed don't delete and pop up a message--->
		<CFELSE>
		
			<SCRIPT>
				function nodelete(){
					alert("There are personal claims against this Company Item, so it cannot be deleted now. \n Please first delete all the Personbal claims against it, then delete this item")	
				}
				nodelete()
			
			</SCRIPT>
		
		
		</CFIF>
	
	<!---Personal Account--->	
	<CFELSEIF EntityLabel IS 'frmPersonID'>
	
		<CFQUERY NAME="DeleteProduct" DATASOURCE="#application.SiteDataSource#" DBTYPE="ODBC">
		DELETE from RWPersonalAccountItem
		WHERE PersonalAccountItemID = #CFGRIDKEY#
		</CFQUERY>
		
	</CFIF>
		
</CFIF>



<!---Get Company or Person details for the page header display--->
<CFQUERY NAME="GetEntity" DATASOURCE="#application.SiteDataSource#">
	
	<!---Company Account--->
	<CFIF EntityLabel IS 'frmorgID'>
		SELECT 'Company' as firstname, o.organisationName as secondname, 
			0 as CurrentPoints  
		from organisation o INNER JOIN RWCompanyAccount ca on
		o.organisationID = ca.organisationID
		WHERE o.organisationID =  <cf_queryparam value="#EntityValue#" CFSQLTYPE="CF_SQL_INTEGER" > 
	
	<!---Personal Account--->
	<CFELSE>
		SELECT p.firstname as firstname, p.lastname as secondname, (pa.CurrentPoints+pa.GracePoints) AS CurrentPoints 
		from person as p, RWPersonalAccount as pa
		where p.personID = pa.personID
		and p.personID =  <cf_queryparam value="#EntityValue#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</CFIF>

</CFQUERY>



<!---If it is a Personal Account--->
<CFIF EntityLabel IS 'frmPersonID'>
	
	<!---Get existing Company Delivery Notes --->
	<CFQUERY NAME="GetDeliveryNotes" DATASOURCE="#application.SiteDataSource#">
		SELECT DISTINCT DeliveryNoteID, DeliveryNoteNumber, DeliveryNoteDate, Distributor 
		from RWDeliveryNote
		WHERE AccountID IN 
			(Select AccountID from RWCompanyAccount where locationID = 
			(Select locationID from Person where PersonID =  <cf_queryparam value="#EntityValue#" CFSQLTYPE="CF_SQL_INTEGER" > ) )
		ORDER BY DeliveryNoteID
		
	
	</CFQUERY>
		
	
	<!---If no delivery notes have been entered for the Company Account this person belongs to
	display message and abbort ulterior processing--->
	<CFIF GetDeliveryNotes.RecordCount EQ 0>
		
		Personal Points are claimed against the previously entered Delivery Note for the company account.<BR> 
		No delivery notes have been registered for this company yet. 
		
		<CF_ABORT>
		
	</CFIF>
		
<!---If a Company Account--->
<CFELSEIF EntityLabel IS 'frmLocationID'>


	<!---List of Delivery Notes...--->
	<CFQUERY NAME="GetDeliveryNotes" DATASOURCE="#application.SiteDataSource#">
		SELECT DISTINCT DeliveryNoteID, DeliveryNoteNumber, DeliveryNoteDate, Distributor	
		FROM RWDeliveryNote
		WHERE AccountID =  <cf_queryparam value="#AccountID#" CFSQLTYPE="CF_SQL_INTEGER" > 	
		ORDER BY DeliveryNoteID
	</CFQUERY>
	


</CFIF>


<!---Page Header--->
<CFOUTPUT query="GetEntity">
<DIV ALIGN="center"><B>Points Account for <FONT COLOR="Red">#htmleditformat(FirstName)# #htmleditformat(SecondName)#</FONT><BR> 
Date: #DateFormat(Now(), "dd/mm/yyyy")# <FONT COLOR="Red">TOTAL POINTS AVAILABLE: #NumberFormat(GetEntity.CurrentPoints, "999999999")#</FONT></B></DIV>
</CFOUTPUT>


<!---Get data for the read-only or update display in the Claim form--->

<CFIF NOT IsDefined("Home") AND NOT IsDefined("NewClaim")>

	<!---Set DeliveryNoteNumber--->
	<CFQUERY NAME="GetHeaderDetails" DATASOURCE="#application.SiteDataSource#" DBTYPE="ODBC">
		SELECT * FROM RWDeliveryNote 
		WHERE DeliveryNoteID = #DeliveryNoteID#
	</CFQUERY>

	
	
<CFELSE>

	<!---If coming from the home page display the latest entry for the given person or company 
	and set DeliveryNoteID and DeliveryNoteNumber--->
	<CFQUERY NAME="GetHeaderDetails" DATASOURCE="#application.SiteDataSource#" DBTYPE="ODBC">
		SELECT * FROM RWDeliveryNote 
		WHERE DeliveryNoteID = (Select max(DeliveryNoteID) FROM RWDeliveryNote 
						 where AccountID = #AccountID#)
	</CFQUERY>
	
	<CFSET DeliveryNoteID = GetHeaderDetails.DeliveryNoteID>
	<CFSET DeliveryNoteNumber = GetHeaderDetails.DeliveryNoteNumber>
	
</CFIF>



<!---Delivery Note Interface with different switches (Company or Personal Account)--->
<FORM ACTION="PointsManagement.cfm" METHOD="post" name="ClaimForm">
	
	<TABLE BGCOLOR="#FFD9D9" align="center" width="500" border="0">
		
		<TR>
			<TD colspan="5" align="center">
			
			<CFIF IsDefined("NewClaim") OR GetDeliveryNotes.RecordCount EQ 0>
				
				<B>ADD NEW DELIVERY NOTE</B>
			
			<CFELSEIF IsDefined("UpdateClaim")>
			
				<B>EDIT DELIVERY NOTE</B>
				
			<CFELSE>
			
				<B>DELIVERY NOTE</B>
			
			</CFIF>
			</TD>
		</TR>
	
		<TR>
			<TD align="center">Delivery Note No.</TD>
			<TD align="center">Delivery Note Date</TD>
			<TD align="center">Order Value</TD>
			<TD colspan ="2" align="center">Distributor</TD>
		</TR>
		
<!---If a claim has already been inserted display read-only data from the header table--->
		
		<TR>
			
			<TD align="center">
			
				<!---Text field for entering the new Delivery Note Number--->
				<CFIF IsDefined("NewClaim") OR GetDeliveryNotes.RecordCount EQ 0>
					
					<INPUT TYPE="text" size="10" NAME="fieldDelNoteNumber">
				
				<!---Text field with the existing value for update--->	
				<CFELSEIF IsDefined("UpdateClaim")>
					
					<CFOUTPUT>
					<CF_INPUT TYPE="text" size="10" NAME="fieldDelNoteNumber" value="#GetHeaderDetails.DeliveryNoteNumber#">
					</CFOUTPUT>
				
				<!---Combo with Delivery Note Numbers--->	
				<CFELSE>
					
					<SELECT NAME="DeliveryNote" onChange="javascript:changeDelNote()">
					<CFOUTPUT Query="GetDeliveryNotes">
						<OPTION value=#DeliveryNoteID# <CFIF IsDefined("Form.DeliveryNoteID")><CFIF DeliveryNoteID EQ Form.DeliveryNoteID>SELECTED</CFIF><CFELSEIF IsDefined("URL.DeliveryNoteID")><CFIF DeliveryNoteID EQ URL.DeliveryNoteID>SELECTED</CFIF><CFELSEIF IsDefined("DeliveryNoteID")><CFIF DeliveryNoteID EQ DeliveryNoteID>SELECTED</CFIF></CFIF> >#htmleditformat(DeliveryNoteNumber)#
					</CFOUTPUT>
					</SELECT>
				
				</CFIF>
			</TD>
			
		<!---The rest of this form works much the same way as the above example--->
		<CFOUTPUT>
			<TD align="center">
				<CFIF IsDefined("NewClaim") OR GetDeliveryNotes.RecordCount EQ 0>
					<INPUT TYPE="text" size="10" NAME="frmDelNoteDate">
					<INPUT TYPE="hidden" NAME="frmDelNoteDate_eurodate">
				<CFELSEIF IsDefined("UpdateClaim")>
					<CF_INPUT TYPE="text" size="10" NAME="frmDelNoteDate" VALUE="#DateFormat(GetHeaderDetails.DeliveryNoteDate, 'dd/mm/yyyy')#">
					<INPUT TYPE="hidden" NAME="frmDelNoteDate_eurodate">
				<CFELSE>
					<B>#DateFormat(GetHeaderDetails.DeliveryNoteDate, "dd/mm/yyyy")#</B>
				</CFIF>
			</TD>
				
			<TD align="center">
				<CFIF IsDefined("NewClaim") OR GetDeliveryNotes.RecordCount EQ 0>
					<INPUT TYPE="text" size="10" NAME="frmOrderValue">
				<CFELSEIF IsDefined("UpdateClaim")>
					<CF_INPUT TYPE="text" size="10" NAME="frmOrderValue" VALUE="#GetHeaderDetails.MonetaryValue#">
				<CFELSE>
					<B>#htmleditformat(GetHeaderDetails.MonetaryValue)#</B>
					
				</CFIF>
			</TD>
		</CFOUTPUT>
			<TD colspan="2" align="center">
				<CFIF IsDefined("NewClaim") OR GetDeliveryNotes.RecordCount EQ 0 OR IsDefined("UpdateClaim")>
					
					<SELECT NAME="frmDistributor">
						<CFOUTPUT QUERY="GetDistributors">
						<OPTION <CFIF IsDefined("UpdateClaim")><CFIF SiteName IS GetHeaderDetails.Distributor>SELECTED</CFIF></CFIF>>#htmleditformat(TRIM(SiteName))#
						</CFOUTPUT>
					</SELECT>
				
				<CFELSE>
					<B><CFOUTPUT>#htmleditformat(GetHeaderDetails.Distributor)#</CFOUTPUT></B>
				
				</CFIF>
			</TD>
		
		</TR>
		
	<CFOUTPUT>
		
		<TR>
			
			<TD align="center">Claim checked</TD>
			<TD colspan="2">Details/Actions of the spot check</TD>
			<TD>&nbsp;</TD>
			
		</TR>
		
		<TR>
			
			<TD align="center">
			<CFIF IsDefined("NewClaim") OR GetDeliveryNotes.RecordCount EQ 0>
				<INPUT TYPE="Checkbox" NAME="frmVerified" VALUE="1">
			<CFELSEIF IsDefined("UpdateClaim")>
				<INPUT TYPE="Checkbox" NAME="frmVerified" VALUE="1" <CFIF GetHeaderDetails.Verified EQ 1>CHECKED</CFIF> >
			<CFELSE>
				<CFIF GetHeaderDetails.Verified EQ 1>
					<B>Yes</B>
				<CFELSE>
					<B>No</B>
				</CFIF>
			</CFIF>
			</TD>
			
			<TD colspan="2">
			<CFIF IsDefined("NewClaim") OR GetDeliveryNotes.RecordCount EQ 0>
				<INPUT TYPE="text" size="25" NAME="frmVerifiedNotes">
			<CFELSEIF IsDefined("UpdateClaim")>
				<CF_INPUT TYPE="text" size="25" NAME="frmVerifiedNotes" VALUE="#GetHeaderDetails.VerifiedNotes#">
			<CFELSE>
				<B>#htmleditformat(GetHeaderDetails.VerifiedNotes)#</B>	
			</CFIF>
			</TD>
			
		<!---Display links for new or update Delivery Note only if it is a Company Account--->
		<CFIF EntityLabel IS "frmLocationID">
				<!---Display Insert button only if a claim hasn't been open yet--->
				<CFIF IsDefined("NewClaim") OR GetDeliveryNotes.RecordCount EQ 0>
					<TD align="center"><A HREF="javascript:window.document.ClaimForm.submit()">Insert</A></TD>
					<INPUT TYPE="Hidden" NAME="AddNewClaim" VALUE="">
				<CFELSEIF IsDefined("UpdateClaim")>
					<TD align="center"><A HREF="javascript:window.document.ClaimForm.submit()">Confirm</A></TD>
					<INPUT TYPE="Hidden" NAME="ConfirmUpdateClaim" VALUE="">
				<CFELSE>
					<TD align="center"><A HREF="javascript:window.document.ClaimForm.submit()">Update</A></TD>
					<INPUT TYPE="Hidden" NAME="UpdateClaim" VALUE="">
				</CFIF>
				
				<CFIF NOT IsDefined("NewClaim") >
					<TD align="center"><A HREF="PointsManagement.cfm?NewClaim=1&#EntityLabel#=#EntityValue#">Add New Claim</A></TD>
				</CFIF>
		</CFIF>
		</TR>
		
	</TABLE>
	
	<!---Pass variables--->
	<CF_INPUT TYPE="Hidden" NAME="EntityLabel" VALUE="#EntityLabel#">
	<CF_INPUT TYPE="Hidden" NAME="EntityValue" VALUE="#EntityValue#">
	<CF_INPUT TYPE="Hidden" NAME="AccountID" VALUE="#AccountID#">
	<CFIF Not IsDefined("NewClaim")>
		<CF_INPUT TYPE="Hidden" NAME="DeliveryNoteID" VALUE="#DeliveryNoteID#">
	</CFIF>
	<CF_INPUT TYPE="Hidden" NAME="DeliveryNoteNumber" VALUE="#DeliveryNoteNumber#">
	
	
	</CFOUTPUT>
	
	</FORM>
	
<!---LINE ITEMS Personal Account--->
<CFIF EntityLabel IS "frmPersonID">

	<!---Get available SKUs which can be claimed against the Company Delivery Note--->	
	<CFQUERY NAME="GetSKUs" DATASOURCE="#application.SiteDataSource#" DBTYPE="ODBC">
		SELECT * 
		FROM RWCompanyAccountItem as cai, RWDeliveryNote as dn
		WHERE cai.DeliveryNoteID = dn.DeliveryNoteID
		AND cai.DeliveryNoteID = #DeliveryNoteID#
		AND cai.QuantityRemaining IS NOT NULL
		AND cai.QuantityRemaining > 0 
	</CFQUERY>

</CFIF>

<!---Line Items insert interface (if at least one delivery note already entered into account)--->
<CFIF NOT IsDefined("NewClaim") AND GetheaderDetails.RecordCount NEQ 0>

	
	
	<FORM ACTION="PointsManagement.cfm" METHOD="post" name="LineForm">
	<TABLE BGCOLOR="#CCE6FF" align="center">
		
		<TR>
			<TD colspan="6" align="center">
			<B>ADD COMPONENTS OF DELIVERY NOTE</B>
			</TD>
		</TR>
	
		<TR>
			<TD>SKU</TD>
		<CFIF EntityLabel IS "frmLocationID">
			<TD>Description</TD>
			<TD align="center">Unit Points</TD>
		
		<CFELSE>
			<TD align="center">Quantity on Delivery Note</TD>
			<TD align="center">Quantity Remaining</TD>
		
		</CFIF>
			<TD align="center">Quantity</TD>
			<!--- <TD align="center">TotalPoints</TD> --->
		</TR>
		
		<TR>
		
		<CFIF EntityLabel IS "frmLocationID">
			
			<TD><INPUT TYPE="text" NAME="frmSKU" size="15"></TD>
		
		<CFELSE>
			
			<SCRIPT type="text/javascript">
			
			var accountInfo = new Array()
			
			accountInfo[0] = new account(0, 0, 0)
			<cfoutput query="getSKUs">
			accountInfo[#jsStringFormat(currentrow)#] = new account(#jsStringFormat(AccountItemID)#, #jsStringFormat(Quantity)#, #jsStringFormat(QuantityRemaining)#)
			</CFOUTPUT>
			
			
			function account(AccountItemID, Quantity, QuantityRemaining)
			{
				this.AccountItemID = AccountItemID
				this.Quantity = Quantity
				this.QuantityRemaining = QuantityRemaining
			}
			
			function assignQuantity(aselectbox){
			
				
				if (aselectbox != 0){
					aselectbox.form.CompanyQuantity.value = accountInfo[aselectbox.selectedIndex].Quantity
					aselectbox.form.CompanyQuantityRemaining.value = accountInfo[aselectbox.selectedIndex].QuantityRemaining
				}					
			}
			</script>			
			<TD>
			<SELECT NAME="frmSKU" onchange="assignQuantity(this)">
				
				<OPTION VALUE=0>Select a SKU
				<CFOUTPUT QUERY="GetSKUs">
					<OPTION VALUE="#AccountItemID#" <CFIF IsDefined("frmSKU")><CFIF AccountItemID EQ frmSKU>SELECTED</CFIF></CFIF> >#htmleditformat(ProductIDSKU)#
				</CFOUTPUT>
			</SELECT>
			</TD>		
		
		</CFIF>	
		
		<CFIF EntityLabel IS "frmLocationID">
			<TD><INPUT TYPE="text" NAME="frmDescription" size="20"></TD>
			<TD align="center"><INPUT TYPE="text" NAME="frmUnitPoints" size="6" ></TD>
		
		<CFELSE>
			
			
			<TD align="center"><INPUT TYPE="text" NAME="CompanyQuantity" size="3" ></TD>
			<TD align="center"><INPUT TYPE="text" NAME="CompanyQuantityRemaining" size="3" ></TD>
			
		
		</CFIF>
			<TD align="center"><INPUT TYPE="text" NAME="frmQuantity" size="3"></TD>
			
			<!--- <TD align="center"><INPUT TYPE="text" NAME="frmTotalPoints" size="6" ></TD> --->
			
			<TD><A HREF="javascript:lineitem()">Insert</A></TD>
			
			<INPUT TYPE="Hidden" NAME="AddNewProduct" VALUE="">
		</TR>
		
		<CFOUTPUT>
		
		
		<CF_INPUT TYPE="Hidden" NAME="EntityLabel" VALUE="#EntityLabel#">
		<CF_INPUT TYPE="Hidden" NAME="EntityValue" VALUE="#EntityValue#">
		<CF_INPUT TYPE="Hidden" NAME="AccountID" VALUE="#AccountID#">
		<CF_INPUT TYPE="Hidden" NAME="DeliveryNoteID" VALUE="#DeliveryNoteID#">
		<CF_INPUT TYPE="Hidden" NAME="DeliveryNoteNumber" VALUE="#DeliveryNoteNumber#">
	
		</CFOUTPUT>
			
		
		
	</TABLE>
	</FORM>
	
	
		
	

	
	
</CFIF>







<!---Get Grid Data --->

<CFIF NOT IsDefined("NewClaim") AND DeliveryNoteID IS NOT ''>

	<!---Company Account--->
	<CFIF EntityLabel IS "frmLocationID" >
	
		<CFQUERY NAME="GetData" DATASOURCE="#application.SiteDataSource#">
			SELECT *, 'Delete' AS Eliminate from RWCompanyAccountItem AS ca
			
			WHERE ca.DeliveryNoteID =  <cf_queryparam value="#ListGetAt(DeliveryNoteID, 1)#" CFSQLTYPE="CF_SQL_INTEGER" > 
			AND ca.AccountID =  <cf_queryparam value="#AccountID#" CFSQLTYPE="CF_SQL_INTEGER" >   	
		</CFQUERY>
		
		
		<FORM ACTION="PointsManagement.cfm" METHOD="POST" NAME="DelNoteForm">
		
		<CFOUTPUT>
				<CF_INPUT TYPE="hidden" NAME="EntityLabel" VALUE="#EntityLabel#">
				<CF_INPUT TYPE="hidden" NAME="EntityValue" VALUE="#EntityValue#">
				<CF_INPUT TYPE="hidden" NAME="DeliveryNoteNumber" VALUE="#DeliveryNoteNumber#">
				<CF_INPUT TYPE="hidden" NAME="AccountID" VALUE="#AccountID#">
				<INPUT TYPE="hidden" NAME="DeliveryNoteID" VALUE="">
		</CFOUTPUT>
		
		
		</FORM>
				
		<CFIF GetData.RecordCount NEQ 0>
					
			<CFFORM ACTION="PointsManagement.cfm" METHOD="POST" NAME="GridForm">
			
			<TABLE align="center" BORDER="0">
			
				<TR>
					
					<TD>
					<INPUT TYPE="Submit" NAME="Update" VALUE="Update Grid">
					</TD>
					
				</TR>
			
				<TR>
					<TD>
			
					<CFGRID NAME="PointsManagement" QUERY="GetData" SORT="Yes" SELECTMODE="EDIT" HEIGHT=250 
					WIDTH=550 BGCOLOR="##CCE6FF" SELECTCOLOR="navy" COLHEADERBOLD="Yes">
			
		
					<CFGRIDCOLUMN NAME="AccountItemID" DISPLAY="No">
					
					
					<CFGRIDCOLUMN NAME="ProductIDSKU" HEADER="SKU">
					<CFGRIDCOLUMN NAME="ItemDescription" HEADER="Description">
					<CFGRIDCOLUMN NAME="UnitPoints" HEADER="Points per Unit">
					<CFGRIDCOLUMN NAME="Quantity" HEADER="Quantity">
					<CFGRIDCOLUMN NAME="Points" HEADER="Total Item Points" SELECT="No">
					
					<CFOUTPUT>
					<CFGRIDCOLUMN NAME="Eliminate" SELECT="Yes" HEADER="Delete" HREFKEY="AccountItemID" HREF="PointsManagement.cfm?DeleteProduct=1&EntityLabel=#EntityLabel#&EntityValue=#EntityValue#&AccountID=#AccountID#&DeliveryNoteID=#DeliveryNoteID#&DeliveryNoteNumber=#DeliveryNoteNumber#" TARGET="_self">                                                       
					</CFOUTPUT>
				
					</CFGRID>
					
					</TD>
				</TR>
				
				<TR>
				
				<TD><A HREF="PointsManagementHome.cfm">Home</A></TD>
				
				</TR>
				<!---Pass the variables necessary for the Grid Refresh--->
				<CFOUTPUT>
				<CF_INPUT TYPE="hidden" NAME="EntityLabel" VALUE="#EntityLabel#">
				<CF_INPUT TYPE="hidden" NAME="EntityValue" VALUE="#EntityValue#">
				<CF_INPUT TYPE="hidden" NAME="AccountID" VALUE="#AccountID#">
				<CF_INPUT TYPE="hidden" NAME="DeliveryNoteID" VALUE="#DeliveryNoteID#">
				<CF_INPUT TYPE="hidden" NAME="DeliveryNoteNumber" VALUE="#DeliveryNoteNumber#">
	
				<INPUT TYPE="Hidden" NAME="Update" VALUE="1">
			
				</CFOUTPUT>
				
				</CFFORM>
			
			</TABLE>
			
		<CFELSE>
		
			<DIV ALIGN="center">No Company Account Items were entered against this delivery note!</DIV>
			
		</CFIF>		
	
	<!---Personal Account--->
	<CFELSEIF EntityLabel IS "frmPersonID">
	
		<CFQUERY NAME="GetData" DATASOURCE="#application.SiteDataSource#">
			SELECT *, 'Delete' AS Eliminate 
			FROM RWCompanyAccountItem 
			INNER JOIN (RWPersonalAccount INNER JOIN RWPersonalAccountItem ON RWPersonalAccount.PersonalAccountID = RWPersonalAccountItem.PersonalAccountID) 
			ON RWCompanyAccountItem.AccountItemID = RWPersonalAccountItem.CompanyAccountItemID
			
			WHERE RWPersonalAccount.PersonID =  <cf_queryparam value="#EntityValue#" CFSQLTYPE="CF_SQL_INTEGER" > 
			AND RWCompanyAccountItem.DeliveryNoteID =  <cf_queryparam value="#DeliveryNoteID#" CFSQLTYPE="CF_SQL_INTEGER" > 

		</CFQUERY>
		
		
		<FORM ACTION="PointsManagement.cfm" METHOD="POST" NAME="DelNoteForm">
		
		<CFOUTPUT>
				<CF_INPUT TYPE="hidden" NAME="EntityLabel" VALUE="#EntityLabel#">
				<CF_INPUT TYPE="hidden" NAME="EntityValue" VALUE="#EntityValue#">
				<CF_INPUT TYPE="hidden" NAME="DeliveryNoteNumber" VALUE="#DeliveryNoteNumber#">
				<CF_INPUT TYPE="hidden" NAME="AccountID" VALUE="#AccountID#">
				<INPUT TYPE="hidden" NAME="DeliveryNoteID" VALUE="">
		</CFOUTPUT>
		
		
		</FORM>
				
		<CFIF GetData.RecordCount NEQ 0>
					
			<CFFORM ACTION="PointsManagement.cfm" METHOD="POST" NAME="GridForm">
			
			<TABLE align="center" BORDER="0">
			
				<TR>
					
					<TD>
					<INPUT TYPE="Submit" NAME="Update" VALUE="Update Grid">
					</TD>
					
				</TR>
			
			
				<TR>
					<TD>
			
					<CFGRID NAME="PointsManagement" QUERY="GetData" SORT="Yes" SELECTMODE="EDIT" HEIGHT=250 
					WIDTH=550 BGCOLOR="##CCE6FF" SELECTCOLOR="navy" COLHEADERBOLD="Yes">
			
		
					<CFGRIDCOLUMN NAME="PersonalAccountItemID" DISPLAY="No">
					
					
					<CFGRIDCOLUMN NAME="ProductIDSKU" HEADER="SKU" SELECT="NO">
					<CFGRIDCOLUMN NAME="ItemDescription" HEADER="Description" SELECT="NO">
					<CFGRIDCOLUMN NAME="PersonalUnitPoints" HEADER="Points per Unit">
					<CFGRIDCOLUMN NAME="QuantityClaimed" HEADER="Quantity">
					<CFGRIDCOLUMN NAME="PersonalPoints" HEADER="Total Item Points" SELECT="No">
					
					<CFOUTPUT>
					<CFGRIDCOLUMN NAME="Eliminate" SELECT="Yes" HEADER="Delete" HREFKEY="PersonalAccountItemID" HREF="PointsManagement.cfm?DeleteProduct=1&EntityLabel=#EntityLabel#&EntityValue=#EntityValue#&AccountID=#AccountID#&DeliveryNoteID=#DeliveryNoteID#&DeliveryNoteNumber=#DeliveryNoteNumber#" TARGET="_self">                                                       
					</CFOUTPUT>
				
					</CFGRID>
					
					</TD>
				</TR>
				
				<TR>
				
				<TD><A HREF="PointsManagementHome.cfm">Home</A></TD>
				
				</TR>
				
				
				<!---Pass the variables necessary for the Grid Refresh--->
				<CFOUTPUT>
				<CF_INPUT TYPE="hidden" NAME="EntityLabel" VALUE="#EntityLabel#">
				<CF_INPUT TYPE="hidden" NAME="EntityValue" VALUE="#EntityValue#">
				<CF_INPUT TYPE="hidden" NAME="AccountID" VALUE="#AccountID#">
				<CF_INPUT TYPE="hidden" NAME="DeliveryNoteID" VALUE="#DeliveryNoteID#">
				<CF_INPUT TYPE="hidden" NAME="DeliveryNoteNumber" VALUE="#DeliveryNoteNumber#">
	
				<INPUT TYPE="Hidden" NAME="Update" VALUE="1">
			
				</CFOUTPUT>
				
				</CFFORM>
			
			</TABLE>
			
		<CFELSE>
		
			<DIV ALIGN="center">No Personal Account Items were entered against this delivery note!</DIV>
			
		</CFIF>		
	
	
	
	
	
	
	
	
	</CFIF>
	



</CFIF>



<!---The Third, Update Phase, after which we should get the Refreshed Grid--->
<CFIF IsDefined("Update")>
	
	<CFIF EntityLabel IS "frmPersonID">
		<CFGRIDUPDATE GRID="PointsManagement" DATASOURCE="#application.SiteDataSource#" TABLENAME="RWPersonalAccountItem" KEYONLY="No" >
	<CFELSE>
		<CFGRIDUPDATE GRID="PointsManagement" DATASOURCE="#application.SiteDataSource#" TABLENAME="RWCompanyAccountItem" KEYONLY="No" >
	</CFIF>
	
	<CFOUTPUT>
		<CFLOCATION URL="PointsManagement.cfm?EntityLabel=#EntityLabel#&EntityValue=#EntityValue#&AccountID=#AccountID#&DeliveryNoteID=#DeliveryNoteID#&DeliveryNoteNumber=#DeliveryNoteNumber#"addToken="false">
	</CFOUTPUT>

</CFIF>
	



