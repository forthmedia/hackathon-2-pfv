<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Hack template which allows user to go back to the flag which they have just added --->
<!--- called from FlagUpdateTask.cfm --->
<!--- frmFlagID has already been set in FlagUpdateTask  --->

<CFSET frmTask="Edit">
<CFSET frmNext="/profileManager/flagEdit">

<cfquery name="getNextFlag" 	 DATASOURCE="#application.SiteDataSource#">
	select top 1 f1.flagid from flag f1
		inner join flag f2 on f1.flaggroupid = f2.flaggroupid and (f1.orderingIndex > f2.orderingIndex  or (f1.orderingIndex = f2.orderingIndex and f1.name > f2.name))
	where
	f2.flagid =  <cf_queryparam value="#frmFlagID#" CFSQLTYPE="CF_SQL_INTEGER" > 	 
	order by f1.orderingIndex asc, f1.name asc

</cfquery>
<cfif getNextFlag.recordcount is not 0>
	<cfset frmFlagID = getNextFlag.flagid>
</cfif>

<cfinclude template="/profileManager/flagEdit.cfm">