<!--- �Relayware. All Rights Reserved 2014 --->
<!---
		Profile Processes
		
		This provides a list of processes that are defined 
		and shows them in the profile manager processes section of the site.
		
		Profile processes are defined as profiles in the system and can have 
		flags with WDDX data associated with them.

		SWJ 24 June 2002

		2005/05/11	WAB   		added check of rights to flags for regprocesses
		2005/06/01	WAB   		modified above so that people with view rights can view the report
		2008/06/11 	GCC/NYF 	If you only have view/ edit rights to one process jump directly into it 
		
--->



<cfquery name="getProcesses" datasource="#application.siteDataSource#">
	select flag.name, flag.flagID 
	from flagGroup fg inner join flag on flag.flagGroupID = fg.flagGroupID
	where flag.flagID in (select distinct flagID from flagProcessAction)
</cfquery>


<!--- 2005-03-09 WAB added concept of having more than one reg process by adding a prefix to the flag and process names 
I search for the flags being set up to give the options for different processes
--->
<cfquery name="getRegProcesses" datasource="#application.siteDataSource#">
	select distinct case when charindex('_',flaggroupTextID) = 0 then '' else substring(flaggroupTextID,1,charindex('_',flaggroupTextID)) end as Prefix,
	 name ,
	case when fg.editaccessrights = 0 and fg.edit = 1 then 1 when fg.editaccessrights = 1 and fgr.edit = 1 then 1 else 0 end as hasEditRights,
	case when fg.viewingaccessrights = 0 and fg.viewing = 1 then 1 when fg.viewingaccessrights = 1 and fgr.viewing = 1 then 1 else 0 end as hasViewRights,
	 fgr.edit,
	 fg.editaccessrights,
	 fg.edit
	 from 
	 flagGroup fg left join (flaggrouprights fgr inner join rightsGroup rg on fgr.userid = rg.usergroupid and rg.personid = #request.relayCurrentUser.personid# AND (fgr.EDIT = 1 or fgr.viewing=1)) on fg.flaggroupid = fgr.flaggroupid 
	where flaggroupTextID like '%orgApprovalStatus'
</cfquery>

<!--- 2008/06/11 GCC/ NYF - T10 - If you only have view/ edit rights to one process jump directly into it --->
<cfquery name="getRegProcessesAccessibleCount" dbtype="query">
	Select * from getRegProcesses where hasEditRights = 1 or hasViewRights = 1
</cfquery>

<cfif getRegProcessesAccessibleCount.recordcount eq 1>
	<cfoutput query="getRegProcessesAccessibleCount">
		<cflocation url="/approvals/regApprovalReport.cfm?RegApprovalPrefix=#prefix#"addToken="false">
	</cfoutput>
<cfelse>

<cf_head>
	<cf_title>Profile processes</cf_title>
</cf_head>

<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
	<TR>
		<TD ALIGN="LEFT" CLASS="SubMenu">&nbsp;Processes</TD>
	</TR>
</TABLE>

<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
	<TR>
		<TD>
			<cfoutput query="getRegProcesses">
				<cfif hasEditRights is 1 or hasViewRights is 1 >
						<A HREF="/approvals/regApprovalReport.cfm?RegApprovalPrefix=#prefix#" CLASS="smallLink">Registration Approvals <cfif prefix neq "">(#htmleditformat(prefix)#)</cfif></A> 	
						<cfif hasEditRights is not 1><BR>(you may view this report but do not have rights to approve people)</cfif>
					<br><br>

				<cfelse>
					Registration Approvals (#htmleditformat(prefix)#)  (you do not have rights to access this attribute)
				</cfif>
			</cfoutput><br>
			<cfif getRegProcesses.recordcount is 0 >No Reg Processes Defined (No orgApprovalStatus flaggroup)</cfif>
		</TD>
	</TR>
	<cfoutput query="getProcesses">
		<TR>
			<TD>
					<A HREF="/profileManager/flagProcessReport.cfm?frmFlagID=#flagID#" CLASS="smallLink">#htmleditformat(name)#</A>

		
				<br>
			</TD>
		</TR>
	</cfoutput>

</TABLE>




</cfif>
