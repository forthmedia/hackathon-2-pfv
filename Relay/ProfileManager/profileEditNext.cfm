<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Hack template which allows user to go back to the flag which they have just added --->
<!--- called from FlagUpdateTask.cfm --->
<!--- frmFlagID has already been set in FlagUpdateTask  --->

<CFSET frmTask="Edit">
<CFSET frmNext="/profileManager/profileEdit">

<cfquery name="getNextFlagGroup" 	 DATASOURCE="#application.SiteDataSource#">
	select top 1 f1.flaggroupid 
		from flaggroup f1
		inner join flaggroup f2 on f1.parentflaggroupid = f2.parentflaggroupid and f1.entityTypeID = f2.entityTypeID and (f1.orderingIndex > f2.orderingIndex  or (f1.orderingIndex = f2.orderingIndex and f1.name > f2.name))
	where
	f2.flaggroupid =  <cf_queryparam value="#frmFlagGroupID#" CFSQLTYPE="CF_SQL_INTEGER" > 	 
	order by f1.orderingIndex asc, f1.name asc
</cfquery>
<cfif getNextFlagGroup.recordcount is not 0>
	<cfset frmflagGroupid = getNextFlagGroup.flagGroupid>
</cfif>

<cfinclude template="/profileManager/profileEdit.cfm">
