<!--- �Relayware. All Rights Reserved 2014 --->

<!---
		Registration Approval Report

		DAM June 2002
		
		RegApprovalReport lists the registrations that have been set up in the 
		system and provides links thru to launch a screen which is used to approve
		or decline them.  This screen is modified on a site by site basis so that 
		site specific stuff can be implemented.
		
		Two flagGroups/profiles are used to manage approvals at a person and organisation level.
		
		These always have the flagGroupTextID PerApprovalStatus and orgApprovalStatus respectively.
		These are initially set to applied when the partner applies to join the program.  
		They are then updated to approved or rejected from this template by an approvals person.
		
		The process of the approvals is defined in a processAction workflow with a 
		processTextID called RegApproval.
		
		2004-12-20 - JC/WAB <CFSET SecurityList="#SecurityTask#:edit"> modified to removed additional 'view' from list
		14/4//5    WAB modification to open in excel, but actually doesn't work properly yet.
		23/5//5    WAB got it to work properly
		23/5//5    WAB also reformatted so that when opening in excel it doesn't export all the links
		2005-06-01	WAB ACTION 624.  Corrected javascript bug when opening dataframe.  Now sets the focus.
		2005-06-01  	WAB changed so that people with view rights can view the report
		2006-4-28 	MDC Changed to show an Client specific registration approval process if it exists.
		2006-20-25	WAB altered so that statuses come directly from the flags table.  Also made the change link into a popup.
--->
<cf_translate>
	<cfif  fileexists("#application.paths.code#\cftemplates\ClientSpecificRegistrationApproval.cfm")>
		<cfinclude template="/code/cftemplates/ClientSpecificRegistrationApproval.cfm">
	
	<cfelse>
		
<cfparam name="openAsExcel" type="boolean" default="false">


<!--- 2005-03-09 WAB added concept of having more than one reg process by adding a prefix to the flag and process names --->
<CFPARAM name = "RegApprovalPrefix" default = "">

<cfquery name="getRegProcess" datasource="#application.siteDataSource#">
	select distinct case when charindex('_',flaggroupTextID) = 0 then '' else substring(flaggroupTextID,1,charindex('_',flaggroupTextID)) end as Prefix,
	 name ,
	case when fg.editaccessrights = 0 and fg.edit = 1 then 1 when fg.editaccessrights = 1 and fgr.edit = 1 then 1 else 0 end as hasEditRights,
	case when fg.viewingaccessrights = 0 and fg.viewing = 1 then 1 when fg.viewingaccessrights = 1 and fgr.viewing = 1 then 1 else 0 end as hasViewRights,
	 fgr.edit,
	 fg.editaccessrights,
	 fg.edit
	 from 
	 flagGroup fg left join (flaggrouprights fgr inner join rightsGroup rg on fgr.userid = rg.usergroupid and rg.personid = #request.relayCurrentUser.personid# AND (fgr.EDIT = 1 or fgr.viewing=1)) on fg.flaggroupid = fgr.flaggroupid 
	where flaggroupTextID =  <cf_queryparam value="#regApprovalPrefix#orgApprovalStatus" CFSQLTYPE="CF_SQL_VARCHAR" > 
</cfquery>

<cfquery name="getOrgFlags" datasource="#application.siteDataSource#">
select flagid,flagtextid,f.name from flag f inner join flagGroup fg on f.flaggroupid = fg.flaggroupid 
where flagGroupTextID =  <cf_queryparam value="#regApprovalPrefix#orgApprovalStatus" CFSQLTYPE="CF_SQL_VARCHAR" > 
</cfquery>
<cfquery name="getperFlags" datasource="#application.siteDataSource#">
select flagid,flagtextid,f.name from flag f inner join flagGroup fg on f.flaggroupid = fg.flaggroupid 
where flagGroupTextID =  <cf_queryparam value="#regApprovalPrefix#perApprovalStatus" CFSQLTYPE="CF_SQL_VARCHAR" > 
</cfquery>


<cfif openAsExcel>
	<cfcontent type="application/msexcel">
	<cfheader name="content-Disposition" value="filename=RegistrationApprovalReport.xls">
	<html xmlns:o="urn:schemas-microsoft-com:office:office"
	xmlns:x="urn:schemas-microsoft-com:office:excel"
	xmlns="http://www.w3.org/TR/REC-html40">
<cfelse>
	
	<cf_head>
		

		<cf_includejavascriptonce template = "/javascript/openWin.js">	
		<cf_includejavascriptonce template = "/javascript/dropdownFunctions.js">	
		<cf_includejavascriptonce template = "/javascript/checkBoxFunctions.js">	
		<cf_includejavascriptonce template = "/javascript/viewEntity.js">	
	<cfoutput>

		<SCRIPT type="text/javascript">
		function openInExcel () {
		
			form=document.FilterForm; 
			form.target = '_blank';
			form.openAsExcel.value = 'True'; 
			form.submit()  ;
			form.openAsExcel.value = 'False'; 
			form.target = '';
		
			return false;
		}
		
		
		</SCRIPT>
		
		
	</cfoutput>
	</cf_head>
	
	
	<CFPARAM NAME="orgstatusFilter" DEFAULT="#RegApprovalPrefix#orgApplied">
	<CFPARAM NAME="perstatusFilter" DEFAULT="#RegApprovalPrefix#perApplied">
	<CFPARAM NAME="countryfilter" DEFAULT="#request.relayCurrentUser.countryid#">
	
	<CFIF isDefined("form.statusFilter") and isDefined("session.statusFilter") and form.statusFilter NEQ session.statusFilter>
		<CFSET session.statusFilter= form.statusFilter>
	</CFIF>
	
	<CFIF isDefined("form.countryfilter") and isDefined("session.countryfilter") and form.countryfilter NEQ session.countryfilter>
		<CFSET session.countryfilter= form.countryfilter>
	</CFIF>
	
	<CFIF isDefined("session.statusFilter")>
		<CFSET statusFilter= session.statusFilter>
	</CFIF>
	
	<CFIF isDefined("session.countryfilter")>
		<CFSET countryfilter= session.countryfilter>
	</CFIF>

</cfif>

<CFSET SecurityTask="Approval">
<CFSET LoginRequired=0>
<CFSET SecurityList="#SecurityTask#:view">
<CFSET continue = "no"><CFSET OverRide = "Yes">	
<cfinclude template="/templates/ApplicationDirectorySecurity.cfm">



<cfinclude template="/templates/qrygetcountries.cfm">

<CFQUERY NAME="getApprovals" DATASOURCE="#application.sitedatasource#">
	SELECT 
	 p.personid,p.firstname  +' '+ p.lastname as fullname,fp.name as PersonStatus,bp.created as PersonStatusDate ,
	 o.Organisationid, o.organisationname, fo.name AS OrganisationStatus, bo.created as OrganisationStatusDate, 
	 c.countrydescription, c.countryid 
	FROM 
	 Person p 
	 INNER JOIN Organisation o ON P.Organisationid = o.Organisationid
	inner JOIN (booleanflagdata bp 
	 INNER JOIN flag fp ON fp.flagid = bp.flagid 
	 INNER JOIN flaggroup fgp ON fgp.flaggroupid = fp.flaggroupid AND fgp.flaggrouptextid =  <cf_queryparam value="#RegApprovalPrefix#PerApprovalStatus" CFSQLTYPE="CF_SQL_VARCHAR" > 
	)
			ON bp.entityid = p.personID
	 INNER JOIN (booleanflagdata bo 
	 INNER JOIN flag fo ON fo.flagid = bo.flagid 
	 INNER JOIN flaggroup fgo ON fgo.flaggroupid = fo.flaggroupid AND fgo.flaggrouptextid =  <cf_queryparam value="#RegApprovalPrefix#orgApprovalStatus" CFSQLTYPE="CF_SQL_VARCHAR" > )
		 ON bo.entityid = o.organisationid

	 INNER JOIN country c ON o.countryid = c.countryid
	  WHERE o.countryid  IN ( <cf_queryparam value="#countrylist#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	  <CFIF countryfilter NEQ 0>
	  	AND o.countryid =  <cf_queryparam value="#countryfilter#" CFSQLTYPE="CF_SQL_INTEGER" > 
	  </CFIF>
	  <CFIF perstatusFilter NEQ 0>
	  	AND fp.flagTextID =  <cf_queryparam value="#perstatusFilter#" CFSQLTYPE="CF_SQL_VARCHAR" >  
	  </CFIF>
	  <CFIF orgstatusFilter NEQ 0>
		AND fo.flagTextID =  <cf_queryparam value="#orgstatusFilter#" CFSQLTYPE="CF_SQL_VARCHAR" > 
	  </CFIF>
	 ORDER BY o.organisationname, p.lastname
	 
</CFQUERY>

<CFQUERY NAME="FilterCountries" DATASOURCE="#application.sitedatasource#">
		SELECT DISTINCT
	 	 c.countrydescription, c.countryid 
	FROM 
	 Person p 
	 INNER JOIN booleanflagdata bp ON bp.entityid = p.personID
	 INNER JOIN flag fp ON fp.flagid = bp.flagid 
	 INNER JOIN flaggroup fgp ON fgp.flaggroupid = fp.flaggroupid AND fgp.flaggrouptextid =  <cf_queryparam value="#RegApprovalPrefix#PerApprovalStatus" CFSQLTYPE="CF_SQL_VARCHAR" > 
	 INNER JOIN Organisation o ON P.Organisationid = o.Organisationid
	 INNER JOIN booleanflagdata bo ON bo.entityid = o.organisationid
	 INNER JOIN flag fo ON fo.flagid = bo.flagid 
	 INNER JOIN flaggroup fgo ON fgo.flaggroupid = fo.flaggroupid AND fgo.flaggrouptextid =  <cf_queryparam value="#RegApprovalPrefix#orgApprovalStatus" CFSQLTYPE="CF_SQL_VARCHAR" > 
	 INNER JOIN country c ON o.countryid = c.countryid
	  WHERE o.countryid  IN ( <cf_queryparam value="#countrylist#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	ORDER BY c.countrydescription
</CFQUERY>

<cfset popupname = "approvalPopup">
<cfset popupparams = "height=200,width=600">


<cfif openAsExcel eq false>
	
	<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
		<TR>	
			<TD ALIGN="left" VALIGN="top" CLASS="submenu">
			<cfoutput>Phr_#htmleditformat(RegApprovalPrefix)#PerApprovalStatus_HeadingText</cfoutput>
			</TD>
			<TD ALIGN="RIGHT" VALIGN="top" CLASS="submenu">
	 			<CFOUTPUT>
				<!--- 	WAB 2005-04-14 this link didn't take account of the filters, so I have changed it so that it submits the form instead
							Unfortunately all future filter changes will open in excel!
				<A HREF="/profilemanager/regApprovalReport.cfm?openAsExcel=true" Class="Submenu">Open in Excel</A>&nbsp; --->
					<a href="javascript:void(openInExcel())" target"_blank" class="Submenu" >Open in Excel</A>&nbsp;
				</CFOUTPUT>
			</TD>
		</TR>
	</table>
	
	
	<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" CLASS="withBorder">
		<CFOUTPUT><FORM ACTION="/profileManager/regApprovalReport.cfm" NAME="FilterForm" METHOD="post" >
			<CF_INPUT type = "hidden" name = "RegApprovalPrefix" value = "#RegApprovalPrefix#">
			<INPUT type = "hidden" name = "openAsExcel" value = "false">
		</CFOUTPUT>
		<TR>
			<TD>Show Country</TD>
			<TD><SELECT NAME="countryfilter" ONCHANGE="javascript:document.FilterForm.submit()">
				<OPTION VALUE="0" <CFIF countryfilter EQ 0>SELECTED</CFIF>>All Countries
				<CFOUTPUT QUERY="FilterCountries">
					<OPTION VALUE="#countryid#" <CFIF countryfilter EQ countryid>SELECTED</CFIF>>#htmleditformat(countrydescription)#
				</CFOUTPUT>
			</SELECT></TD>
			<TD>Filter by Registration Status: &nbsp;</TD>
			<TD>Organisation &nbsp;</TD>
			<TD Width="50">All&nbsp;<INPUT ONCLICK="javascript:document.FilterForm.submit()" TYPE="Radio" NAME="orgstatusFilter" VALUE="0" <CFIF orgstatusFilter EQ "0">Checked</CFIF>></TD>
			<cfoutput query="getOrgFlags">
				<TD Width="70">#htmleditformat(name)#&nbsp;<CF_INPUT ONCLICK="javascript:document.FilterForm.submit()" TYPE="Radio" NAME="orgstatusFilter" VALUE="#flagTextID#" Checked="#iif( orgstatusFilter EQ flagTextID,true,false)#"></TD>
			</cfoutput>
		</TR>
		<TR>
			<TD></TD>
			<TD></TD>
			<TD></TD>
			<TD>Person &nbsp;</TD>
			<TD Width="50">All&nbsp;<INPUT ONCLICK="javascript:document.FilterForm.submit()" TYPE="Radio" NAME="perstatusFilter" VALUE="0" <CFIF perstatusFilter EQ "0">Checked</CFIF>></TD>
			<cfoutput query="getPerFlags">
				<TD Width="70">#htmleditformat(name)#&nbsp;<CF_INPUT ONCLICK="javascript:document.FilterForm.submit()" TYPE="Radio" NAME="perstatusFilter" VALUE="#flagTextID#" Checked="#iif( perstatusFilter EQ flagTextID,true,false)#"></TD>
			</cfoutput>
		</TR>

		</FORM>
	</TABLE>
</cfif>

<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" CLASS="withBorder">
<cfif not openasExcel>
	<tr>	
		<td colspan="3" align="right" nowrap>		
			<cfinclude template="/data/persondropdown.cfm">
		</td>
	</tr>
</cfif>
	<FORM  NAME="mainForm" >
	<TR>
		<TH colspan=2>Organisation/Person</TH>
		<TH>Status</TH>
		<TH>Action</TH>
	</TR>
	<CFIF getApprovals.recordCount gt 0>
		<CFOUTPUT QUERY="getApprovals" GROUP="organisationname">
			<!--- outer query --->
			<TR class="evenrow" VALIGN="bottom">
				<TD HEIGHT="36" colspan=2><cfif not openasExcel><B><A TITLE="Goto Organisation Record" HREF="javascript:newWin = openWin('/data/dataframe.cfm?frmsrchOrgID=#organisationID#','DataWindow','width=780,height=580,toolbar=0,location=0,directories=0,status=1,menuBar=0,scrollBars=1,resizable=1');newWin.focus()">#htmleditformat(organisationname)#</A></B><cfelse>#htmleditformat(organisationname)#</cfif></TD>
 				<TD><B>#htmleditformat(OrganisationStatus)#</B> #dateformat(OrganisationStatusdate,"dd-mmm-yyy")#</TD>
				<CFIF checkPermission.edit GT 0> 
				<TD>
					<cfif not openasExcel>
						<cfif getRegProcess.hasEditRights is 1>
							<cfif OrganisationStatus eq "applied">
								<A TITLE="Approve this Organisation" HREF="javascript:newWin=openWin('/profileManager/profileProcess.cfm?frmOrganisationID=#organisationID#&frmProcessid=#RegApprovalPrefix#RegApproval&frmstepid=0<CFIF orgstatusFilter NEQ 0>&orgstatusFilter=#htmleditformat(orgstatusFilter)#</CFIF><CFIF perstatusFilter NEQ 0>&perstatusFilter=#htmleditformat(perstatusFilter)#</CFIF><CFIF countryfilter NEQ 0>&countryfilter=#htmleditformat(countryfilter)#</CFIF>','popupName','#htmleditformat(popupparams)#');newWin.focus()" >Approve/Reject</a>
							<CFELSE>
								<A  TITLE="Change the Registration Status of this Organisation" HREF="javascript:newWin=openWin('/profileManager/profileProcess.cfm?frmOrganisationID=#organisationID#&frmProcessid=#RegApprovalPrefix#RegApproval&frmstepid=0<CFIF orgstatusFilter NEQ 0>&orgstatusFilter=#htmleditformat(orgstatusFilter)#</CFIF><CFIF perstatusFilter NEQ 0>&perstatusFilter=#htmleditformat(perstatusFilter)#</CFIF><CFIF countryfilter NEQ 0>&countryfilter=#htmleditformat(countryfilter)#</CFIF>','popupName','#htmleditformat(popupparams)#');newWin.focus()" >Change</a>
							</cfif>
						</cfif>
					</cfif>
				</TD>
				<CFELSE>
				<TD>N/A</TD>
				</CFIF>
			</TR>
			<!--- inner query --->
			<CFSET nCount=1>
			<CFOUTPUT>
			<TR<CFIF nCount MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
				<td>&nbsp;</td>	
				<TD><cfif not openasExcel><CF_INPUT TYPE="CHECKBOX" NAME="frmPersonCheck" VALUE="#PersonID#">&nbsp;<A TITLE="Goto Person Record" HREF="javascript:newWin = openWin('/data/dataframe.cfm?frmsrchpersonID=#personID#','DataWindow','width=780,height=580,toolbar=0,location=0,directories=0,status=1,menuBar=0,scrollBars=1,resizable=1');newWin.focus()">#htmleditformat(fullname)#</A><cfelse>#htmleditformat(fullname)#</cfif></TD>
				<TD> #htmleditformat(PersonStatus)# #dateformat(PersonStatusdate,"dd-mmm-yyy")#</TD>
				<CFIF checkPermission.edit GT 0> 
				<TD>
					<cfif not openasExcel>
						<cfif getRegProcess.hasEditRights is 1>
							<cfif PersonStatus eq "applied">
								<A TITLE="Approve this Person" HREF="javascript:newWin=openWin('/profileManager/profileProcess.cfm?frmPersonID=#personID#&frmProcessid=#RegApprovalPrefix#RegApproval&frmstepid=0<CFIF orgstatusFilter NEQ 0>&orgstatusFilter=#htmleditformat(orgstatusFilter)#</CFIF><CFIF perstatusFilter NEQ 0>&perstatusFilter=#htmleditformat(perstatusFilter)#</CFIF><CFIF countryfilter NEQ 0>&countryfilter=#htmleditformat(countryfilter)#</CFIF>','popupName','#htmleditformat(popupparams)#');newWin.focus()">Approve/Reject</a>
							<CFELSE>
								<A TITLE="Change the Registration Status of this Person" HREF="javascript:newWin=openWin('/profileManager/profileProcess.cfm?frmPersonID=#personID#&frmProcessid=#RegApprovalPrefix#RegApproval&frmstepid=0<CFIF orgstatusFilter NEQ 0>&orgstatusFilter=#htmleditformat(orgstatusFilter)#</CFIF><CFIF perstatusFilter NEQ 0>&perstatusFilter=#htmleditformat(perstatusFilter)#</CFIF><CFIF countryfilter NEQ 0>&countryfilter=#htmleditformat(countryfilter)#</CFIF>','popupName','#htmleditformat(popupparams)#');newWin.focus()">Change</a>
							</cfif>
						</cfif>
					</cfif>
				<CFELSE>
				<TD>N/A</TD>
				</CFIF>
			</TR>
			<CFSET nCount=nCount+1>
			</CFOUTPUT>
		</CFOUTPUT>
	<CFELSE>
		<TR><TD colspan="4">No records found</TD></TR>
	</CFIF>
	
</TABLE>
</FORM>
	
	

</cfif>



</cf_translate>