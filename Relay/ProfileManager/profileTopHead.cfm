<!--- �Relayware. All Rights Reserved 2014 --->
<!---
	DAM 10 July 2002
	Added additional logic to ensure that when

2005-10-17	WAB 	implemented function for testing whether user has rights to edit flag/flaggroup definition
2006-04-20 WAB		made a change so that addProfile() passes an entityType (should always be there)
2008-10-14	NYB		Amended error with adding child profiles via "Buld Add" in ProfileEdit
2009/05/27 SSS		Moved the excel link here so that it is part of the navigation

--->

<cfparam name="current" type="string" default="profileEntityList.cfm">
<cfparam name="attributes.pagesBack" type="string" default="1">
<cfparam name="attributes.pageTitle" type="string" default="">
<cfparam name="attributes.thisDir" type="string" default="profileManager">
<cfparam name="frmEntityTypeID" default=0>


<CF_RelayNavMenu pageTitle="#attributes.pageTitle#" thisDir="#attributes.thisDir#">
	<CFIF findNoCase("profileList.cfm",request.query_string,1) OR findNoCase("profileList.cfm",SCRIPT_NAME,1) OR (Isdefined("ThisPageName") AND ThisPageName IS "profileList.cfm")>
		<!--- <CF_RelayNavMenuItem MenuItemText="Back" CFTemplate="JavaScript:window.history.back();" target=""> --->
		<CFIF checkPermission.AddOkay GT 0>
			
			<CF_RelayNavMenuItem MenuItemText="New Profile" CFTemplate="JavaScript:addProfile('#frmEntityTypeID#')" target="">
			<CF_RelayNavMenuItem MenuItemText="Bulk New Profile" CFTemplate="JavaScript:bulkProfile('#frmEntityTypeID#')" target="">

		</CFIF>	
	</CFIF> 
	
	<CFIF findNoCase("profileDetails.cfm",request.query_string,1) OR findNoCase("profileDetails.cfm",SCRIPT_NAME,1) or findNoCase("profileDetails.cfm",request.query_string,1) OR (Isdefined("ThisPageName") AND ThisPageName IS "profileDetails.cfm")>
		<!--- <CF_RelayNavMenuItem MenuItemText="Back" CFTemplate="profileList.cfm" target=""> --->
		<CFIF checkPermission.AddOkay GT 0 and hasRightsToEdit and getFlagGroup.FlagTypeID IS NOT 1>
			<CF_RelayNavMenuItem MenuItemText="Bulk New Profile Attributes" CFTemplate="JavaScript:bulkAddFlag()" target="">
			<CF_RelayNavMenuItem MenuItemText="New Profile Attribute" CFTemplate="JavaScript:addFlag()" target="">
		</CFIF>	
		<CFIF checkPermission.AddOkay GT 0 and hasRightsToEdit and getFlagGroup.FlagTypeID IS 1>
			<CF_RelayNavMenuItem MenuItemText="New Child Profile" CFTemplate="JavaScript:addProfile('',#frmFlagGroupID#)" target="">
		</CFIF>
		<CFIF hasRightsToEdit AND checkPermission.edit GT 0>
			<CF_RelayNavMenuItem MenuItemText="Edit Profile" CFTemplate="JavaScript:editDetail('#frmFlagGroupID#')" target="">		
		</CFIF>
	</CFIF>
 
	<CFIF findNoCase("profileEdit.cfm",request.query_string,1) OR listFindNoCase("profileEdit.cfm,flagGroupUpdateTask.cfm",listLast(SCRIPT_NAME,"/")) OR (Isdefined("ThisPageName") AND ThisPageName IS "profileEdit.cfm")>
		<!--- <CF_RelayNavMenuItem MenuItemText="Back" CFTemplate="JavaScript:window.history.back();" target=""> --->
		<!--- <CF_RelayNavMenuItem MenuItemText="Profile List" CFTemplate="home.cfm" target=""> --->
		<CFIF frmTask IS not "add">
				<CF_RelayNavMenuItem MenuItemText="Save & Previous" CFTemplate="JavaScript:verifyForm('#frmTask#','profileEditPrevious');" target="">
		</CFIF>
		<CF_RelayNavMenuItem MenuItemText="Save" CFTemplate="JavaScript:verifyForm('#frmTask#','ProfileEditAgain');"> <!--- WAB 2005-10-24 changed from ProfileDetails so it works in 3 pane view --->
		<CFIF frmTask IS "add">
			<CF_RelayNavMenuItem MenuItemText="Save & Add New" CFTemplate="JavaScript:verifyForm('#frmTask#','ProfileEdit');">
			<CF_RelayNavMenuItem MenuItemText="Bulk Add" CFTemplate="flagGroupBulkLoad.cfm?frmParentflaggroupid=#frmParentFlagGroupID#" target="">
		<CFELSE>
			<CF_RelayNavMenuItem MenuItemText="Save & Next" CFTemplate="JavaScript:verifyForm('#frmTask#','profileEditNext');" target="">
		</CFIF>

	</CFIF> 
	
 	<CFIF findNoCase("flagGroupBulkLoad.cfm",request.query_string,1) OR findNoCase("flagGroupBulkLoad.cfm",SCRIPT_NAME,1) OR (Isdefined("ThisPageName") AND ThisPageName IS "flagGroupBulkLoad.cfm")>
		<CF_RelayNavMenuItem MenuItemText="Save" CFTemplate="JavaScript:verifyForm('#frmTask#','ProfileEdit');"> 
	</CFIF> 

	<CFIF findNoCase("FlagGroupList.cfm",request.query_string,1) OR findNoCase("FlagGroupList.cfm",SCRIPT_NAME,1) or findNoCase("FlagGroupCounts.cfm",SCRIPT_NAME,1)>
		<!--- <CF_RelayNavMenuItem MenuItemText="Back" CFTemplate="javascript:window.history.back()" target=""> --->
		<CF_RelayNavMenuItem MenuItemText="Organisation Profiles" CFTemplate="FlagGroupList.cfm?frmEntityTypeID=2" target="">
		<CF_RelayNavMenuItem MenuItemText="Location Profiles" CFTemplate="FlagGroupList.cfm?frmEntityTypeID=1" target="">
		<CF_RelayNavMenuItem MenuItemText="Person Profiles" CFTemplate="FlagGroupList.cfm?frmEntityTypeID=0" target="">
	</CFIF> 
	<CFIF findNoCase("flagEdit.cfm",request.query_string,1) OR findNoCase("flagEdit.cfm",SCRIPT_NAME,1) or findNoCase("flagEdit.cfm",request.query_string,1) OR (Isdefined("ThisPageName") AND ThisPageName IS "flagEdit.cfm")>
		<!--- <CF_RelayNavMenuItem MenuItemText="Back" CFTemplate="JavaScript:history.go(-#attributes.pagesBack#);" target=""> --->
		<!--- <CF_RelayNavMenuItem MenuItemText="Profile List" CFTemplate="profileList.cfm" target=""> --->
		<!--- DAM 10 JUly 2002 - Not really required, because it does the same as teh next one !!! --->
<!--- 		<CF_RelayNavMenuItem MenuItemText="Save &amp; return" CFTemplate="JavaScript:verifyForm('#frmTask#','flagEditAgain');" target=""> --->
		<CFIF frmTask IS NOT "add">
			<CF_RelayNavMenuItem MenuItemText="Save & Previous" CFTemplate="JavaScript:verifyForm('#frmTask#','flagEditPrevious');" target="">
		</cfif>
		<CF_RelayNavMenuItem MenuItemText="Save & Return " CFTemplate="JavaScript:verifyForm('#frmTask#','flagEditAgain');" target="">
		<CFIF frmTask IS "add">
			<CF_RelayNavMenuItem MenuItemText="Save & Add New" CFTemplate="JavaScript:verifyForm('#frmTask#','flagEdit');" target="">
			<CF_RelayNavMenuItem MenuItemText="Bulk Add" CFTemplate="flagBulkLoad.cfm?frmflaggroupid=#frmFlagGroupID#" target="">
		<cfelse>
			<CF_RelayNavMenuItem MenuItemText="Save & Next" CFTemplate="JavaScript:verifyForm('#frmTask#','flagEditNext');" target="">
		</CFIF>
	</CFIF> 
	<CFIF (Isdefined("ThisPageName") AND ThisPageName IS "flagbulkload.cfm")>
			<CF_RelayNavMenuItem MenuItemText="Save" CFTemplate="JavaScript:verifyForm('#frmTask#','flagEditAgain');" target="">

	</CFIF> 	
	
	<CFIF findNoCase("ProfileProcesses.cfm",request.query_string,1) OR findNoCase("ProfileProcesses.cfm",SCRIPT_NAME,1)>
		<CF_RelayNavMenuItem MenuItemText="New Profile Process" CFTemplate="" target="">
	</CFIF> 

	<CFIF findNoCase("profileEntityList.cfm",request.query_string,1) OR findNoCase("profileEntityList.cfm",SCRIPT_NAME,1) OR (Isdefined("ThisPageName") AND ThisPageName IS "profileEntityList.cfm")>
		<CFIF checkPermission.AddOkay GT 0>
			<CF_RelayNavMenuItem MenuItemText="New Profile" CFTemplate="JavaScript:addProfile()" target="">
		</CFIF>	
	</CFIF> 
	<!--- 2009/05/27 SSS LH 2279 Moved this here instead of in the template so it is part of the navigation --->
	<CFIF findNoCase("reportTraining_UserCourseQuiz.cfm",SCRIPT_NAME,1)>
		<CF_RelayNavMenuItem MenuItemText="Excel" CFTemplate="javascript:openAsExceljs()" target="">
	</cfif>
</CF_RelayNavMenu>


