<!--- �Relayware. All Rights Reserved 2014 --->

<!---
		flagProcessReport.cfm

		SWJ Sept 2002
			
		This is used to manage the moving of processFlags through the various stages
		that are defined in flagProcessAction table
	WAB 2010-10-13  removed get TextPhrases and replaced with CF_translate
--->


<cf_head>
	
	<cf_includejavascriptonce template = "/javascript/openwin.js">	
</cf_head>


<cfquery name="getStatii" datasource="#application.sitedatasource#">
	select distinct statusFrom as status from flagProcessAction
	union
	select distinct statusTo as status from flagProcessAction
</cfquery>

 <CFIF isdefined("userorgonly")>
	<cfquery name="org" datasource="#application.sitedatasource#">
		SELECT Organisationid FROM person WHERE personid=#request.relayCurrentUser.personid#
	</cfquery>
	<CFSET userorg = org.Organisationid>
 </CFIF>


<cfquery name="getNextStatus" datasource="#application.sitedatasource#">
	select statusFrom, statusTo, processActionTextID from flagProcessAction
</cfquery>

<CFPARAM NAME="statusFilter" DEFAULT="Applied">
<CFPARAM NAME="countryfilter" DEFAULT="0">


<CFSET SecurityTask="Approval">
<CFSET LoginRequired=0>
<CFSET SecurityList="#SecurityTask#:view">
<CFSET continue = "no"><CFSET OverRide = "Yes">	
<cfinclude template="/templates/ApplicationDirectorySecurity.cfm">
<!--- 2012-07-26 PPB P-SMA001 commented out
<cfinclude template="/templates/qrygetcountries.cfm">
 --->
<CFQUERY NAME="getRecordsToProcess" DATASOURCE="#application.sitedatasource#" DBTYPE="ODBC">
	SELECT 
	 pfd.id,
	 pfd.currentStatusTextID,
	 pfd.data,
	 pfd.flagid,
	 pfd.dateStatusChanged,
	 o.Organisationid, o.organisationname, 
	 c.countrydescription, c.countryid 
	FROM 
	 organisation o 
	 INNER JOIN processflagdata pfd ON pfd.entityid = o.organisationID -- needs to be dynamic
	 INNER JOIN flag f ON f.flagid = pfd.flagid 
	 INNER JOIN country c ON o.countryid = c.countryid
	 WHERE pfd.flagID = #frmFlagID#
	 <!--- 2012-07-26 PPB P-SMA001 commented out
	 AND o.countryid IN (#countrylist#)
	  --->
	 #application.com.rights.getRightsFilterWhereClause(entityType="organisation",alias="o").whereClause# 	<!--- 2012-07-26 PPB P-SMA001 --->

	  <CFIF countryfilter NEQ 0>
	  	AND o.countryid = #countryfilter#
	  </CFIF>
	  <CFIF statusFilter NEQ 0>
	  	AND (pfd.currentStatusTextID = '#statusFilter#')
	  </CFIF>
	  <CFIF isdefined("userorgonly")>
	  	AND (pfd.entityid = '#userorg#')
	  </CFIF>
	 ORDER BY o.organisationname
	 
</CFQUERY>

<CFQUERY NAME="FilterCountries" DATASOURCE="#application.sitedatasource#" DBTYPE="ODBC">
	SELECT DISTINCT
	 	 c.countrydescription, c.countryid 
	FROM 
	 organisation o 
	 INNER JOIN processFlagData pfd ON pfd.entityid = o.organisationID 
	 INNER JOIN country c ON o.countryid = c.countryid
	 WHERE pfd.flagID = #frmFlagID#
	 <!--- 2012-07-26 PPB P-SMA001 commented out
	  and o.countryid IN (#countrylist#)
	  --->
	 #application.com.rights.getRightsFilterWhereClause(entityType="organisation",alias="o").whereClause# 	<!--- 2012-07-26 PPB P-SMA001 --->
	  
	  <CFIF statusFilter NEQ 0>
	  	AND (pfd.currentStatusTextID = '#statusFilter#')
	  </CFIF>
	 ORDER BY c.countrydescription
</CFQUERY>

<cf_translate>

<CFSET ThisPageName="flagProcessReport.cfm">
<CFIF request.relayCurrentUser.isInternal> <!--- WAB 2006-01-25 Remove form.userType    <CFIF UserType IS "Internal">--->
	<cfinclude template="/profileManager/profileTopHead.cfm">
</CFIF>

<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" BGCOLOR="White" CLASS="withBorder">
	<CFIF NOT IsDefined("noFilters")>
	<FORM NAME="FilterForm" METHOD="post">
	<TR>
		<TD>Show Country?</TD>
		<TD><SELECT NAME="countryfilter" ONCHANGE="javascript:document.FilterForm.submit()">
			<OPTION VALUE="0" <CFIF countryfilter EQ 0>SELECTED</CFIF>>All Countries
			<CFOUTPUT QUERY="FilterCountries">
				<OPTION VALUE="#countryid#" <CFIF countryfilter EQ countryid>SELECTED</CFIF>>#htmleditformat(countrydescription)#
			</CFOUTPUT>
		</SELECT></TD>
		<TD>Show status? &nbsp;</TD>
		<TD>All&nbsp;<INPUT ONCLICK="javascript:document.FilterForm.submit()" TYPE="Radio" NAME="statusFilter" VALUE="0" <CFIF statusFilter EQ "0">Checked</CFIF>></TD>
		<cfoutput query="getStatii">
			<TD>#htmleditformat(status)#&nbsp;<INPUT ONCLICK="javascript:document.FilterForm.submit()" TYPE="Radio" NAME="statusFilter" VALUE="#status#" <CFIF statusFilter EQ "#status#">Checked</CFIF>></TD>
		</cfoutput>	
	</TR>
	</FORM>
	</CFIF>
</TABLE>
<CFIF isdefined("userorgonly")>
	<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withSideBorder">
	<TR>
		<TH><CFOUTPUT>#htmleditformat(getRecordsToProcess.organisationname)#</CFOUTPUT></TH>
	</TR>
	</TABLE>
	
</CFIF>
<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withSideBorder">
	

	
	<TR>
		<CFIF not IsDefined("nomainrecord")>
			<TH>Phr_MainRecord</TH>
		</CFIF>
		<CFIF isdefined("detailsheader")>
			<TH><CFOUTPUT>#htmleditformat(detailsheader)#</CFOUTPUT></TH>
		<CFELSE>
			<TH>Phr_AdditionalDetails</TH>
		</CFIF>
		<TH>Phr_CurrentStatus</TH>
		<TH>Phr_dateStatusChanged</TH>
		<CFIF not IsDefined("noaction")>
			<TH>Phr_Action</TH>
		</CFIF>
	</TR>
	<CFIF getRecordsToProcess.recordCount gt 0>
		<CFSET nCount=1>
		<CFLOOP QUERY="getRecordsToProcess">
			<CFSET thisrowid=id>
			<CFSET thisflagid=flagid>

			<TR<CFIF nCount MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
				<CFSET nCount=nCount+1>
				<CFIF not IsDefined("nomainrecord")>
				<TD HEIGHT="36">
					<CFIF not isdefined("nomainurl")>
						<B><CFOUTPUT>
							<A TITLE="Goto Organisation Record" HREF="Javascript:openWin('http://#sitedomain##urlroot#/data/dataframe.cfm?frmsrchOrgID=#organisationID#','DataWindow','width=780,height=580,toolbar=0,location=0,directories=0,status=1,menuBar=0,scrollBars=1,resizable=1');">#htmleditformat(organisationName)#</A>
						</CFOUTPUT></B>
					<CFELSE>
						<CFOUTPUT>#htmleditformat(organisationName)#</CFOUTPUT>

					</CFIF>
				</TD>
				</CFIF>
				<TD HEIGHT="36">
					
					<CFOUTPUT>ID = #htmleditformat(thisrowid)#, 
						<CFWDDX ACTION="WDDX2CFML" INPUT="#data#" OUTPUT="RawData">
						<CFLOOP item="thisItem" collection="#RawData#">
							<CFIF thisItem contains "_List">#evaluate("RawData.#htmleditformat(thisItem)#")#&nbsp;</CFIF>
						</CFLOOP>
					</CFOUTPUT>
					</TD>
				<TD>
					<CFOUTPUT>#htmleditformat(currentStatusTextID)#</CFOUTPUT>
				</TD>
				<TD>
					<CFOUTPUT>#htmleditformat(dateStatusChanged)#</CFOUTPUT>
				</TD>

				<CFIF not IsDefined("noaction")>
					<CFIF (isdefined("checkPermission.edit") AND checkPermission.edit GT 0) OR not request.relayCurrentUser.isInternal>  <!--- WAB 2006-01-25 Remove form.userType    usertype IS "EXTERNAL"--->
 
						<TD>
							<CFQUERY NAME="testNextStatus" DBTYPE="query">
							select statusTo, processActionTextID from getNextStatus where statusFrom = '#currentStatusTextID#'
							</CFQUERY>
							<CFOUTPUT QUERY="testNextStatus">
			 					<cfif processActionTextID neq "">
									<A HREF="/profileManager/profileProcess.cfm?status=#statusTo#&flagid=#thisflagid#&rowid=#thisrowid#&frmOrganisationID=#getRecordsToProcess.organisationID#&frmProcessid=#processActionTextID#">Set status to #htmleditformat(statusTo)#</a>
								<CFELSE>
									no action
								</cfif><CFIF testNextStatus.recordcount GT 0><BR><BR></CFIF>
							</CFOUTPUT>
						</TD>
					<CFELSE>
						<TD>N/A</TD>
					</CFIF>
				</CFIF>
			</TR>
		</CFLOOP>
	<CFELSE>
		<TR><TD colspan="4">No records found</TD></TR>
	</CFIF>
	
</TABLE>


</cf_translate>

