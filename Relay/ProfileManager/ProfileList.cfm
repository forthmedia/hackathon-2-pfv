<!--- �Relayware. All Rights Reserved 2014 --->
<!--- the application .cfm checks for read adminTask privleges --->

<!--- Mod History
		250200		 WAB 	 added session variable to remember entitytype
		1999-07-07 		 WAB   adjusted so that all those with admin rights can see and edit all flags
		1999-08-20 	 SWJ 	 add frmEntityTypeID test to constrain the number of flagGroups
		2001-10-20	 SWJ	 Changed over to profile manager
    	2004-11-03	 DF		 Modified drop down list code to not display query records by default, also added JS for the search_by field
		2006-04-20 WAB		made a change so that addProfile points to the correct frame
							also made it know which entityType you are looking at so that the entityType drop downs is prepopulated
							can search on all entityTypes and if so it tells you the type in the report
		2008-06-03	NYF		P-CRL501 - added protection for flaggroup with system flags from deletion:  added:
								,(SELECT top 1 Max(IsSystem) FROM Flag where FlagGroupID=FlagGroup.FlagGroupID) as 			ContainsSysFlag
							to getFlagGroups query, and added ' and ContainsSysFlag eq 0' inside the  <CFIF edit_count IS NOT 0> before HREF="JavaScript:delFlagGroup
2009/09/04	WAB		LID 2549 problem with call to openProfileReportTab when not filtered by entityType.  Doesn't actually fix the bug - which I couldn't reproduce, but came across in passing.
2014/04/09	NJH		RW Roadmap 2014 2 - Don't provide option of deleting a profile if the profile is being used in the connector
2015/12/17  SB		Bootstrap
--->

<script type="text/javascript">
function openProfileReportTab (EntityTypeID,FlagGroupID)  {
	openNewTab('Profile_Reports','Profile Reports','/report/profiles/profileReportFrame.cfm?frmEntityTypeID=' + EntityTypeID + '&frmFlagGroupID=' + FlagGroupID,null,{iconClass:'noIcon',otherParam:'param'})

}
</script>

<cf_includeJavascriptOnce template="/javascript/extExtension.js">
<cf_includejavascriptonce template = "/javascript/openWin.js">

<CFIF isDefined("frmEntityTypeID")>
	<CFSET session.frmEntityTypeID = frmEntityTypeID>
<CFELSEIF isDefined("session.frmEntityTypeID")>
	<CFSET frmEntityTypeID = session.frmEntityTypeID>
<CFELSE>
	<CFPARAM NAME="frmEntityTypeID" DEFAULT="99">
</cfif>

<CFPARAM NAME="entityType" DEFAULT="Unknown">

<CFIF isDefined("searchPhrase")>
	<CFSET session.searchPhrase = searchPhrase>
<CFELSEIF isDefined("session.searchPhrase")>
	<CFSET searchPhrase = session.searchPhrase>
<CFELSE>
	<CFPARAM NAME="searchPhrase" DEFAULT="">
</cfif>

<cfif isdefined('form.searchPhrase')>
	<cfset searchPhrase = form.searchPhrase>
</cfif>

<!--- Get USER countries --->
<!---
NJH 2012/08/20 commented this out to use the new rights snippet that has been introduced.. trying to be consistent.
<cfinclude template="/templates/qrygetcountries.cfm"> --->
<!--- Get country groups for this user to find which FlagGroups with Scope they can view --->

<!--- Find all Country Groups for this Country --->
<!--- Note: no User Country rights implemented for site currently --->
<CFQUERY NAME="getCountryGroups" DATASOURCE="#application.SiteDataSource#">
	SELECT DISTINCT b.CountryID
	 FROM Country AS a, Country AS b, CountryGroup AS c
	WHERE (b.ISOcode IS null OR b.ISOcode = '')
	  AND c.CountryGroupID = b.CountryID
	  AND c.CountryMemberID = a.CountryID
	  #application.com.rights.getRightsFilterWhereClause(entityType="country",alias="a").whereClause#
	 <!---  AND a.CountryID  IN ( <cf_queryparam value="#Variables.CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) ---> <!--- User Country rights --->
	ORDER BY b.CountryID
</CFQUERY>

<CFSET UserCountryList = "0,#request.relayCurrentUser.CountryList#">
<CFIF getCountryGroups.CountryID IS NOT "">
	<!--- top record (or only record) is not null --->
	<CFSET UserCountryList =  UserCountryList & "," & ValueList(getCountryGroups.CountryID)>
</CFIF>

<CFQUERY NAME="getFlagGroups" DATASOURCE="#application.SiteDataSource#">
SELECT
FlagGroup.FlagGroupID AS Grouping,
FlagGroup.FlagGroupID,
FlagGroup.EntityTypeID,
FlagGroup.FlagGroupTextID,
FlagGroup.FlagTypeID,
ft.name as flagType,
FlagGroup.Active,
FlagGroup.Name,
FlagGroup.ParentFlagGroupID,
FlagGroup.ViewingAccessRights,
FlagGroup.EditAccessRights,
FlagGroup.DownloadAccessRights,
FlagGroup.SearchAccessRights,
FlagGroup.OrderingIndex AS parent_order,
-1 AS child_order,
<!--- NYF 2008-06-03 P-CRL501 --->
coalesce((SELECT top 1 IsSystem FROM Flag where FlagGroupID=FlagGroup.FlagGroupID order by IsSystem desc),0) as ContainsSysFlag,
(SELECT Count(FlagID) FROM Flag, FlagGroup AS q WHERE Flag.FlagGroupID = q.FlagGroupID AND FlagGroup.FlagGroupID = q.FlagGroupID) As flag_count,
(SELECT Count(1) FROM FlagGroup AS s WHERE s.FlagGroupID = FlagGroup.FlagGroupID AND
(FlagGroup.CreatedBy=#request.relayCurrentUser.usergroupid#
	OR (s.EditAccessRights = 0 AND s.Edit <> 0)
	or exists (		SELECT 1 FROM rights as r, RightsGroup as rg, SecurityType AS s
	WHERE  r.SecurityTypeID = s.SecurityTypeID
	AND s.ShortName  = 'AdminTask'
	AND r.usergroupid = rg.usergroupid
	AND rg.PersonID=#request.relayCurrentUser.personid# )
	OR (EXISTS (SELECT 1 FROM FlagGroupRights AS t WHERE s.EditAccessRights <> 0
                AND s.FlagGroupID = t.FlagGroupID
                AND t.UserID=#request.relayCurrentUser.usergroupid#
                AND t.Edit <> 0)))) AS edit_count,
case when m.column_relayware is not null then 1 else 0 end as inConnectorMapping
FROM FlagGroup
INNER JOIN FlagType ft ON ft.flagTypeID = flagGroup.flagTypeID
<!--- <cfif isDefined("searchPhrase") and len(searchPhrase) gt 0> --->
left JOIN Flag ON Flag.FlagGroupID = FlagGroup.FlagGroupID   <!--- WAB 2007-12-10 LEFT join so that groups without flags can be found--->
<!--- </cfif> --->
left join vConnectorMapping m on (m.column_relayware = FlagGroup.flagGroupTextID or m.column_relayware = flag.flagTextID) and m.entityTypeID = FlagGroup.entityTypeID
WHERE ParentFlagGroupID = 0
<cfif frmEntityTypeID is not "">
AND FlagGroup.EntityTypeID =  <cf_queryparam value="#frmEntityTypeID#" CFSQLTYPE="CF_SQL_INTEGER" >  <!--- Added by SWJ to constrain the flag list a bit --->
</cfif>
AND FlagGroup.Scope  IN ( <cf_queryparam value="#UserCountryList#" CFSQLTYPE="CF_SQL_Integer"  list="true"> )
AND (FlagGroup.CreatedBy=#request.relayCurrentUser.usergroupid#
	or exists (		SELECT 1 FROM rights as r, RightsGroup as rg, SecurityType AS s
						WHERE  r.SecurityTypeID = s.SecurityTypeID
						AND s.ShortName  = 'AdminTask'
						AND r.usergroupid = rg.usergroupid
						AND rg.PersonID=#request.relayCurrentUser.personid# )
	OR (FlagGroup.ViewingAccessRights = 0 AND FlagGroup.Viewing <> 0)
	OR (FlagGroup.ViewingAccessRights <> 0
		AND EXISTS (SELECT 1 FROM FlagGroupRights
			WHERE FlagGroup.FlagGroupID = FlagGroupRights.FlagGroupID
			AND FlagGroupRights.UserID=#request.relayCurrentUser.usergroupid#
			AND FlagGroupRights.Viewing <> 0)))
<cfif isDefined("searchPhrase") and len(searchPhrase) gt 0>
	AND (FlagGroup.FlagGroupTextID  like  <cf_queryparam value="%#replaceNoCase(searchPhrase," ","%","all")#%" CFSQLTYPE="CF_SQL_VARCHAR" >
		or FlagGroup.Name  like  <cf_queryparam value="%#replaceNoCase(searchPhrase," ","%","all")#%" CFSQLTYPE="CF_SQL_VARCHAR" >
		or Flag.Name  like  <cf_queryparam value="%#replaceNoCase(searchPhrase," ","%","all")#%" CFSQLTYPE="CF_SQL_VARCHAR" >
		or Flag.FlagTextID  like  <cf_queryparam value="%#replaceNoCase(searchPhrase," ","%","all")#%" CFSQLTYPE="CF_SQL_VARCHAR" >
		<cfif isNumeric(searchPhrase)>
		or cast(flag.flagID as varchar(20)) = <cf_queryparam value="#trim(searchPhrase)#" CFSQLTYPE="CF_SQL_VARCHAR" >
		or cast(flagGroup.flagGroupID as varchar(20)) = <cf_queryparam value="#trim(searchPhrase)#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</cfif>
		)
</cfif>
UNION
SELECT
FlagGroup.ParentFlagGroupID,
FlagGroup.FlagGroupID,
FlagGroup.EntityTypeID,
FlagGroup.FlagGroupTextID,
FlagGroup.FlagTypeID,
ft.name as flagType,
FlagGroup.Active,
FlagGroup.Name,
FlagGroup.ParentFlagGroupID,
FlagGroup.ViewingAccessRights,
FlagGroup.EditAccessRights,
FlagGroup.DownloadAccessRights,
FlagGroup.SearchAccessRights,
p.OrderingIndex,
FlagGroup.OrderingIndex,
<!--- NYF 2008-06-03 P-CRL501 --->
coalesce((SELECT top 1 IsSystem FROM Flag where FlagGroupID=FlagGroup.FlagGroupID order by IsSystem desc),0) as ContainsSysFlag,
(SELECT Count(FlagID) FROM Flag, FlagGroup AS q WHERE Flag.FlagGroupID = q.FlagGroupID AND FlagGroup.FlagGroupID = q.FlagGroupID),
(SELECT Count(1) FROM FlagGroup AS s WHERE s.FlagGroupID = FlagGroup.FlagGroupID AND
(FlagGroup.CreatedBy=#request.relayCurrentUser.usergroupid#
	OR (s.EditAccessRights = 0 AND s.Edit <> 0)
	or exists (	SELECT 1 FROM rights as r, RightsGroup as rg, SecurityType AS s
				WHERE  r.SecurityTypeID = s.SecurityTypeID
				AND s.ShortName  = 'AdminTask'
				AND r.usergroupid = rg.usergroupid
				AND rg.PersonID=#request.relayCurrentUser.personid# )
OR (EXISTS (SELECT 1 FROM FlagGroupRights AS t WHERE s.EditAccessRights <> 0
                AND s.FlagGroupID = t.FlagGroupID
                AND t.UserID=#request.relayCurrentUser.usergroupid#
                AND t.Edit <> 0)))),
case when m.column_relayware is not null then 1 else 0 end as inConnectorMapping
FROM FlagGroup
INNER JOIN FlagGroup as p ON p.FlagGroupID = FlagGroup.ParentFlagGroupID
INNER JOIN FlagType ft ON ft.flagTypeID = flagGroup.flagTypeID
<!--- <cfif isDefined("searchPhrase") and len(searchPhrase) gt 0> --->
LEFT  JOIN Flag ON Flag.FlagGroupID = FlagGroup.FlagGroupID <!--- WAB 2007-12-10 LEFT join so that groups without flags can be found--->
<!--- </cfif> --->
left join vConnectorMapping m on (m.column_relayware = FlagGroup.flagGroupTextID or m.column_relayware = flag.flagTextID) and m.entityTypeID = FlagGroup.entityTypeID
WHERE FlagGroup.ParentFlagGroupID <> 0
AND p.ParentFlagGroupID = 0
<cfif frmEntityTypeID is not "">
AND FlagGroup.EntityTypeID =  <cf_queryparam value="#frmEntityTypeID#" CFSQLTYPE="CF_SQL_INTEGER" >  <!--- Added by SWJ to constrain the flag list a bit --->
</cfif>
AND FlagGroup.Scope  IN ( <cf_queryparam value="#UserCountryList#" CFSQLTYPE="CF_SQL_Integer"  list="true"> )
AND (FlagGroup.CreatedBy=#request.relayCurrentUser.usergroupid#
	OR (FlagGroup.ViewingAccessRights = 0 AND FlagGroup.Viewing <> 0)
	or exists (	SELECT 1 FROM RightsGroup as rg, UserGroup as ug
						WHERE rg.UserGroupID = ug.UserGroupID
						AND rg.PersonID=#request.relayCurrentUser.personid# AND ug.Name='Admin-Standard'  )
	or exists (	SELECT 1 FROM rights as r, RightsGroup as rg, SecurityType AS s
						WHERE  r.SecurityTypeID = s.SecurityTypeID
						AND s.ShortName  = 'AdminTask'
						AND r.usergroupid = rg.usergroupid
						AND rg.PersonID=#request.relayCurrentUser.personid# )
	OR (FlagGroup.ViewingAccessRights <> 0
		AND EXISTS (SELECT 1 FROM FlagGroupRights
			WHERE FlagGroup.FlagGroupID = FlagGroupRights.FlagGroupID
			AND FlagGroupRights.UserID=#request.relayCurrentUser.usergroupid#
			AND FlagGroupRights.Viewing <> 0)))

<cfif isDefined("searchPhrase") and len(searchPhrase) gt 0>
	AND (FlagGroup.FlagGroupTextID  like  <cf_queryparam value="%#replaceNoCase(searchPhrase," ","%","all")#%" CFSQLTYPE="CF_SQL_VARCHAR" >
		or FlagGroup.Name  like  <cf_queryparam value="%#replaceNoCase(searchPhrase," ","%","all")#%" CFSQLTYPE="CF_SQL_VARCHAR" >
		or p.FlagGroupTextID  like  <cf_queryparam value="%#replaceNoCase(searchPhrase," ","%","all")#%" CFSQLTYPE="CF_SQL_VARCHAR" >
		or p.Name  like  <cf_queryparam value="%#replaceNoCase(searchPhrase," ","%","all")#%" CFSQLTYPE="CF_SQL_VARCHAR" >
		or Flag.Name  like  <cf_queryparam value="%#replaceNoCase(searchPhrase," ","%","all")#%" CFSQLTYPE="CF_SQL_VARCHAR" >
		or Flag.FlagTextID  like  <cf_queryparam value="%#replaceNoCase(searchPhrase," ","%","all")#%" CFSQLTYPE="CF_SQL_VARCHAR" >
		<cfif isNumeric(searchPhrase)>
		or cast(flag.flagID as varchar(20)) = <cf_queryparam value="#trim(searchPhrase)#" CFSQLTYPE="CF_SQL_VARCHAR" >
		or cast(flagGroup.flagGroupID as varchar(20)) = <cf_queryparam value="#trim(searchPhrase)#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</cfif>
		)
</cfif>

ORDER BY EntityTypeid, parent_order, grouping, child_order, flagGroup.Name
</CFQUERY>

<cfif searchPhrase neq "">
	<cfset newQuery = queryNew(getFlagGroups.columnList)>
	<cfloop query="getFlagGroups">
		<cfscript>
			queryAddRow(newQuery);
		</cfscript>
		<cfloop list="#columnList#" index="col">
			<cfscript>
				querySetCell(newQuery,col,evaluate(col));
			</cfscript>
		</cfloop>
		<cfif not listContains(valueList(getFlagGroups.flagGroupID),Grouping)>
			<cfscript>
				querySetCell(newQuery,"Grouping",flagGroupID);
				querySetCell(newQuery,"ParentFlagGroupID",0);
			</cfscript>
		</cfif>
	</cfloop>
	<cfset getFlagGroups = newQuery>
</cfif>


<cfset application.com.request.setTopHead(checkPermission = checkPermission,topHeadcfm="profileTopHead.cfm",frmEntityTypeID=frmEntityTypeID)>
<cf_title>Flag Group List</cf_title>

<SCRIPT type="text/javascript">

<!--
        function viewDetail(ID) {
                var form = document.FlagGroupForm;
                form.frmFlagGroupID.value = ID;
                form.submit();
                return;
        }

		function addProfile(entityTypeID,parentFlagGroupID) {
                var form = document.NewFlagGroupForm;
				form.frmEntityTypeID.value = entityTypeID
        		if (parentFlagGroupID)
					form.frmParentFlagGroupID.value = parentFlagGroupID
		        form.submit();
                return;
        }

		function bulkProfile(entityTypeID) {
                var form = document.BulkFlagGroupForm;
				form.frmEntityTypeID.value = entityTypeID
                form.submit();
                return;
        }

		function addFlag(flagGroupID) {
                var form = document.NewFlagForm;
				form.frmFlagGroupID.value = flagGroupID
                form.submit();
                return;
        }

		<CFOUTPUT>
        function viewReport(ID) {
                var form = document.FlagGroupForm;
                form.FlagGroupID.value = ID;
				form.action = "/report/profiles/flagGroupCounts.cfm"
                form.submit();
                return;
        }
		</CFOUTPUT>


		function amendOrdering(FlagGroupID,flagGroupName,index) {
                var form = document.AmendOrderingForm;
                form.frmFlagGroupID.value = FlagGroupID;
				form.frmFlagGroupName.value = flagGroupName;
				form.frmOrder.value = index + 1;
                form.submit();
                return;
        }

        function delFlagGroup(ID,flaggroupname,parent,noflags,type) {
                var form = document.DelFlagGroupForm;
                form.frmFlagGroupID.value = ID;
				form.frmFlagTypeID.value = type;
				form.frmFlagGroupName.value = flaggroupname;
				var message = "Are you sure you want to permanently delete the profile '" + flaggroupname + "' ?";
				if (parent) {
					message = message + "\n\nThis will also remove any sub profile data and entity data associated with these.";
				} else {
					if (noflags != "0") {
						message = message + "\n\nThis will permenantly remove the profile and data associated with these.";
					}
				}

				if (confirm(message)) {
                	form.submit();
                	return;
				}
        }


		function checkType() {

				     if(document.functions.frmEntityTypeID.value == 404) {

						    alert("phr_Sys_SelectFilter");
								document.functions.frmEntityTypeID.focus();
								return false;

						 }

		}


//-->

</SCRIPT>


<cfscript>
	qEntitTypes = application.com.profilemanagement.GetEntityTypes(justReturnLiveTypes = false);
</cfscript>

<cfset reportModuleActive = application.com.relayMenu.isModuleActive(moduleId="ReportManager")>
<cfset application.com.request.setDocumentH1(text="Profiles")>

<cfoutput>
<div id="profileListCont">
		  <!--- add the element to the query object that allows "Select" to be the default behaviour --->
		<!--- NJH 2015/09/09 - order the entityTypes by description, rather than by ID	--->

			<cfquery name="qEntitTypes2" dbtype="query">
				select '' as entityTypeID,'Select an Entity Type' as description, 1 as isLive,'select an Entity Type' as description_lower,0 as sortIdx from qEntitTypes
				union
				select cast(ENTITYTYPEID as varchar) as entityTypeID,description,isLive,lower(description) as description_lower,1 as sortIdx from qEntitTypes
				order by sortIdx,isLive desc,description_lower asc
			</cfquery>

	<div class="row">
		<div class="col-xs-6">
			<cfform name="functions" action="profileList.cfm" method="post">
				<CF_INPUT type="hidden" name="entityType" value="#entityType#">
				<div class="form-group">
					<label>Phr_Sys_chooseType</label>
					<cfselect name="frmEntityTypeID" class="form-control" size="1" query="qEntitTypes2"
						value="entityTypeID" display="description"
						selected="#frmEntityTypeID#">
						<!--- onchange="this.form.submit()"> --->
					</cfselect>
				</div>
			<!--- </cfform> --->
		</div>
		<!--- </td>
		<td> --->

			<!--- <form name="functions2" action="/profileManager/profileList.cfm" method="post">
				<input type="hidden" name="frmEntityTypeID" value="#frmEntityTypeID#">
				<input type="hidden" name="entityType" value="#entityType#">--->
		<div class="col-xs-6">
			<div class="form-group">
				<div class="row">
					<div class="col-xs-10">
						<label>phr_sys_searchFor</label>
						<CF_INPUT type="text" name="searchPhrase" size="25" value="#searchPhrase#">
					</div>
					<div class="col-xs-2">
						<input type="submit" onClick="return checkType();" value="Go" class="btn btn-primary form-control margin-top-25">
					</div>
				</div>
				</cfform>
				<cfif request.relayCurrentUser.personid eq 1><cfinclude template="profileManagerDropDown.cfm"></cfif>
			</div>
		</div>
	</div>
	<span class="help-block">
		<CFIF IsDefined('message')>
			<cfparam name="messageType" default="success">
			<p>#application.com.relayUI.message(message=message,type=messageType)#</p>
		</CFIF>
	</span>
</div>
</cfoutput>

<!--- DF / Added to disable data retrieving.. esentially, if the form drop down is not detected then the query is not run --->
<TABLE class="table table-hover table-striped">
<CFIF getFlagGroups.Recordcount IS 0 OR NOT ISDEFINED('form.frmEntityTypeID')>
	<tr class="evenRow">
		<td>
		<cfif getFlagGroups.Recordcount IS 0>
			There are no Profiles of this type.
		</cfif>
		Please choose a type from the drop down above.
		</td>
	</tr>
<CFELSE>

	<FORM ACTION="/profileManager/profileDetails.cfm" NAME="FlagGroupForm" METHOD="POST">
	<INPUT TYPE="HIDDEN" NAME="frmFlagGroupID" VALUE="0">
	<INPUT TYPE="HIDDEN" NAME="FlagGroupID" VALUE="0">

	<CFSET cnt = 0>

	<TR>
		<!--- <TH>&nbsp;</TH> --->
		<th>Phr_Sys_NameIDTextID <BR>Phr_Sys_Clicktoedit</th>
		<th>Phr_Sys_Type</th>
		<th>Phr_Sys_Active</th>
		<th>Apply To<br> Domains</th>
		<th>Phr_Sys_Number<BR>Phr_Sys_Attributes</th>
		<th>Phr_Sys_Report</th>
		<th>Phr_Sys_Access</th>
 		<th>Phr_Sys_Ordering<BR>Phr_Sys_Index</th>
		<CFIF checkPermission.AddOkay GT 0><th>Phr_Sys_Delete</th></CFIF>
	</TR>

	<cfset currentType = "">
	<CFOUTPUT QUERY="getFlagGroups" GROUP="grouping">
		<!--- WAB 2006-04-26 if we have searched in all entity Types then output a title for each type--->
		<cfif frmEntityTypeID is "" and currentType is not entityTypeID>
		<TR>

			<TH align="left" colspan = "9">#application.entityType[entityTypeID].tableName#</TH>
		</TR>
		</cfif>
		<cfset currentType = entityTypeID>
		<CFSET thisFlagGroup = FlagGroupID>
		<CFSET #cnt# = #cnt# + 1>
		<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
			<!--- <td><CF_INPUT TYPE="CHECKBOX" NAME="frmProfileCheck" VALUE="#FlagGroupID#"></td> --->
			<TD>
				<B><a href="/profileManager/profileDetails.cfm?frmFlagGroupID=#FlagGroupID#" target="bottomLeft" class="smallLink">#HTMLEditFormat(Name)#</a> (#htmleditformat(FlagGroupID)#)</B>
				<I><CFIF #FlagGroupTextID# IS NOT "">(#htmleditformat(FlagGroupTextID)#)<CFELSE>(No text ID defined)</CFIF></I>
				<cfif flagTypeID is 1><A href="javascript:addProfile('',#FlagGroupID#)">+</A></cfif>
				<cfif flagTypeID is Not 1><A href="javascript:addFlag(#FlagGroupID#)">+</A></cfif>
				</TD>
			<TD>#htmleditformat(FlagType)#</TD>
			<TD><B><CFIF Active IS NOT 0>Yes<CFELSE>No</CFIF></B></TD>
			<TD><cfif FlagType neq "group" and reportModuleActive and listFind("2,3,4,5,6,12,8,13",flagTypeID)><a href="##" onClick="javascript:openWin('/jasperServer/domainProfiles.cfm?flagGroupID=#htmleditformat(FlagGroupID)#','ApplyProfiles','width=500,height=600,scrollbars=yes');return false;"><IMG SRC="/images/icons/db.png" ALT="Apply #HTMLEditFormat(Name)# to domains" title="Apply #HTMLEditFormat(Name)# to domains" WIDTH="16" HEIGHT="16" BORDER="0"></a></cfif></TD>
			<TD><CFIF flag_count IS NOT 0>#htmleditformat(flag_count)#<CFELSE>-</CFIF></TD>
			<TD>
				<CFIF flag_count IS NOT 0 and listFind("0,1,2",entityTypeID)>
					<!--- <cfif request.r elayCurrentVersion.UIVersion eq 1>
						<A HREF="/report/profiles/profileReportFrame.cfm?frmEntityTypeID=#frmEntityTypeID#&frmFlagGroupID=#FlagGroupID#" TARGET="mainSub" CLASS="smallLink"><IMG SRC="/images/MISC/Icon_Graph.gif" ALT="" WIDTH="16" HEIGHT="16" BORDER="0"></A>
					<cfelse> --->
						<!--- WAB LID 2549 frmEntityTypeID changed to getFlagGroups.EntityTypeID since former was sometimes blank (when showing every profile) --->
						<A HREF="javascript:openProfileReportTab (#getFlagGroups.EntityTypeID#,#FlagGroupID#) " CLASS="smallLink"><IMG SRC="/images/MISC/Icon_Graph.gif" ALT="" WIDTH="16" HEIGHT="16" BORDER="0"></A>
					<!--- </cfif> --->
					<!--- javascript:manageClick('tid_tabItem_tabItem19_4','Reports','/cfdev/report/reportFrame.cfm?rptGroup=ProfileManager',null,{iconClass:'noIcon',otherParam:'param'}) --->
				</CFIF>
			</TD>
			<TD><CFIF EditAccessRights + ViewingAccessRights + SearchAccessRights + DownloadAccessRights IS NOT 0><A HREF="/profileManager/flagGroupUsers.cfm?frmFlagGroupID=#FlagGroupID#&frmBack=flagGroupList" class="smallLink" target="bottomRight">User Specific</A><CFELSE>General</CFIF></TD>
			<TD>
			<CFIF edit_count IS NOT 0>
			<SELECT class="form-control" NAME="frmOrder" onChange="JavaScript:amendOrdering(#FlagGroupID#,'#Replace(Name,"'","\'","ALL")#',this.selectedIndex);">
				<cfloop index = "I" from="1" to = "20">
					<OPTION VALUE=I #IIF(parent_order IS I,DE('SELECTED'),DE(''))#>#htmleditformat(I)#
				</cfloop>
			</SELECT>
			<CFELSE>#htmleditformat(parent_order)#</CFIF>
			</TD>
			<!--- NYF 2008-06-03 P-CRL501 --->
			<CFIF checkpermission.AddOkay GT 0>
			<TD><CFIF edit_count IS NOT 0 and ContainsSysFlag eq 0 and not inConnectorMapping><A HREF="JavaScript:delFlagGroup('#FlagGroupID#','#Replace(Name,"'","\'","ALL")#',1,'#flag_count#','#FlagTypeID#');"><IMG SRC="/images/MISC/iconDeleteCross.gif" BORDER=0></A><CFELSE>&nbsp;</CFIF></TD></CFIF>
		</TR>

		<CFOUTPUT>
			<CFIF getFlagGroups.ParentFlagGroupID IS NOT 0>
			<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
				<!--- <td><CF_INPUT TYPE="CHECKBOX" NAME="frmProfileCheck" VALUE="#FlagGroupID#"></td> --->
				<TD>
					&nbsp;&nbsp;&nbsp; <a href="/profileManager/profileDetails.cfm?frmFlagGroupID=#FlagGroupID#" target="bottomLeft" class="smallLink">#HTMLEditFormat(Name)#</a> (#htmleditformat(FlagGroupID)#)<I>
					<CFIF #FlagGroupTextID# IS NOT "">(#htmleditformat(FlagGroupTextID)#)<CFELSE>(No text ID defined)</CFIF></I>
					<cfif flagTypeID is Not 1><A href="javascript:addFlag(#FlagGroupID#)">+</A></cfif>
				</TD>
				<TD>#htmleditformat(getFlagGroups.FlagType)#</TD>
				<TD><CFIF Active IS NOT 0>Yes<CFELSE>No</CFIF></TD>
				<TD><cfif FlagType neq "group" and reportModuleActive and listFind("2,3,4,5,6,12,8,13",flagTypeID)><a href="##" onClick="javascript:openWin('/jasperServer/domainProfiles.cfm?flagGroupID=#htmleditformat(FlagGroupID)#','ApplyProfiles','width=500,height=600,scrollbars=yes');return false;"><IMG SRC="/images/icons/db.png" ALT="Apply #HTMLEditFormat(Name)# to domains" title="Apply #HTMLEditFormat(Name)# to domains" WIDTH="16" HEIGHT="16" BORDER="0"></a></cfif></TD>
				<TD><CFIF flag_count IS NOT 0>#htmleditformat(flag_count)#<CFELSE>-</CFIF></TD>
				<TD><CFIF flag_count IS NOT 0 and listFind("0,1,2",entityTypeID)>
									<!--- <cfif request. relayCurrentVersion.UIVersion eq 1>
										<A HREF="/report/profiles/profileReportFrame.cfm?frmEntityTypeID=#frmEntityTypeID#&frmFlagGroupID=#FlagGroupID#" TARGET="mainSub" CLASS="smallLink"><IMG SRC="/images/MISC/Icon_Graph.gif" ALT="" WIDTH="16" HEIGHT="16" BORDER="0"></A>
									<cfelse> --->
										<!--- WAB LID 2549 frmEntityTypeID changed to getFlagGroups.EntityTypeID since former was sometimes blank (when showing every profile) --->
										<A HREF="javascript:openProfileReportTab (#getFlagGroups.EntityTypeID#,#FlagGroupID#) " CLASS="smallLink"><IMG SRC="/images/MISC/Icon_Graph.gif" ALT="" WIDTH="16" HEIGHT="16" BORDER="0"></A>
									<!--- </cfif> --->
													</CFIF>
				</TD>
				<TD><CFIF EditAccessRights + ViewingAccessRights + SearchAccessRights + DownloadAccessRights IS NOT 0><A HREF="/profileManager/flagGroupUsers.cfm?frmFlagGroupID=#FlagGroupID#&frmBack=flagGroupList" class="smallLink" target="bottomRight">User Specific</A><CFELSE>General</CFIF></TD>
			<TD>
			<CFIF edit_count IS NOT 0>
			<SELECT class="form-control" NAME="frmOrder" onChange="JavaScript:amendOrdering(#FlagGroupID#,'#Replace(Name,"'","\'","ALL")#',this.selectedIndex);">
				<cfloop index = "I" from="1" to = "20">
					<OPTION VALUE=I #IIF(child_order IS I,DE('SELECTED'),DE(''))#>#htmleditformat(I)#
				</cfloop>

			</SELECT>
			<CFELSE>#htmleditformat(child_order)#</CFIF>
			</TD>
				<!--- NYF 2008-06-03 P-CRL501 --->
	 			<CFIF checkpermission.AddOkay GT 0><TD ALIGN="CENTER" VALIGN="MIDDLE"><CFIF edit_count IS NOT 0 and ContainsSysFlag eq 0 and not inConnectorMapping><A HREF="JavaScript:delFlagGroup('#FlagGroupID#','#Replace(Name,"'","\'","ALL")#',0,'#flag_count#','#FlagTypeID#');"><IMG SRC="/images/MISC/iconDeleteCross.gif" BORDER=0></A><CFELSE>&nbsp;</CFIF></TD></CFIF>
			</TR>
			</CFIF>
		</CFOUTPUT>

	</CFOUTPUT>

	</TABLE>
	<!---  --->
	</FORM>

<CFOUTPUT>
	<FORM METHOD="POST" ACTION="/profileManager/flagGroupUpdateTask.cfm" NAME="AmendOrderingForm">
		<CF_INPUT TYPE="HIDDEN" NAME="frmDate" VALUE="#CreateODBCDateTime(now())#">
		<INPUT TYPE="HIDDEN" NAME="frmFlagGroupID" VALUE="">
		<INPUT TYPE="HIDDEN" NAME="frmFlagGroupName" VALUE="">
		<INPUT TYPE="HIDDEN" NAME="frmOrder" VALUE="">
		<INPUT TYPE="HIDDEN" NAME="frmNext" VALUE="/profileManager/profileList">
		<INPUT TYPE="HIDDEN" NAME="frmTask" VALUE="ordering">
		<CF_INPUT TYPE="hidden" NAME="frmEntityTypeID" VALUE="#frmEntityTypeID#">
	</FORM>

	<FORM METHOD="POST" ACTION="/profileManager/flagGroupUpdateTask.cfm" NAME="DelFlagGroupForm">
		<CF_INPUT TYPE="HIDDEN" NAME="frmDate" VALUE="#CreateODBCDateTime(now())#">
		<INPUT TYPE="HIDDEN" NAME="frmFlagGroupID" VALUE="">
		<INPUT TYPE="HIDDEN" NAME="frmFlagGroupName" VALUE="">
		<INPUT TYPE="HIDDEN" NAME="frmFlagTypeID" VALUE="">
		<INPUT TYPE="HIDDEN" NAME="frmNext" VALUE="/profileManager/profileList">
		<INPUT TYPE="HIDDEN" NAME="frmTask" VALUE="delete">
		<CF_INPUT TYPE="hidden" NAME="frmEntityTypeID" VALUE="#frmEntityTypeID#">
	</FORM>
</CFOUTPUT>
</CFIF>
<cfoutput>
	<FORM METHOD="POST" ACTION="/profileManager/profileEdit.cfm" NAME="NewFlagGroupForm" target="bottomRight">
		<INPUT TYPE="HIDDEN" NAME="frmBack" VALUE="/profileManager/flagGroupList">
		<INPUT TYPE="HIDDEN" NAME="frmTask" VALUE="add">
		<INPUT TYPE="HIDDEN" NAME="frmEntityTypeID" VALUE="0">
		<INPUT TYPE="HIDDEN" NAME="frmParentFlagGroupID" VALUE="0">
	</FORM>

	<FORM METHOD="POST" ACTION="/profileManager/flagGroupbulkload.cfm" NAME="BulkFlagGroupForm" target="bottomRight">
		<INPUT TYPE="HIDDEN" NAME="frmBack" VALUE="/profileManager/flagGroupList">
		<INPUT TYPE="HIDDEN" NAME="frmTask" VALUE="add">
		<INPUT TYPE="HIDDEN" NAME="frmEntityTypeID" VALUE="">
		<!--- NYB 2008-10-14 Sophos bug --->
		<INPUT TYPE="HIDDEN" NAME="frmParentFlagGroupID" VALUE="">
	</FORM>

		<FORM METHOD="POST" ACTION="/profileManager/flagEdit.cfm" NAME="NewFlagForm" target="bottomRight">
			<INPUT TYPE="HIDDEN" NAME="frmFlagGroupID" VALUE="">
			<INPUT TYPE="HIDDEN" NAME="frmTask" VALUE="add">
		</FORM>
</cfoutput>


<!--- debug
<cfdump var="#qEntitTypes#"><br>
<cfdump var="#getFlagGroups#">
--->

</FORM>