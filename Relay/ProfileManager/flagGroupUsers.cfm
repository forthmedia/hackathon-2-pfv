<!--- �Relayware. All Rights Reserved 2014 --->
<!--- the application .cfm checks for read adminTask privleges --->

<!--- Display/Edit right to a flagGroup

	WAB 2000-12-01 altered so that shows usergroups when first loaded, can then show individual users
				also altered queries to be a bit better.
	



 --->
<CFPARAM name="frmShowUsers" default = "0">

<CFQUERY NAME="getFlagGroup" datasource="#application.sitedatasource#" MAXROWS=1 debug>
SELECT Name, Description, Active, Expiry, ViewingAccessRights, EditAccessRights, DownloadAccessRights, SearchAccessRights, EntityTypeID, Scope
FROM FlagGroup
WHERE FlagGroupID =  <cf_queryparam value="#frmFlagGroupID#" CFSQLTYPE="CF_SQL_INTEGER" > 
</CFQUERY>

<CFIF getFlagGroup.Scope IS NOT 0>
	<CFQUERY NAME="getCountry" datasource="#application.sitedatasource#" MAXROWS=1 debug>
	SELECT CountryDescription
	FROM Country
	WHERE CountryID =  <cf_queryparam value="#getFlagGroup.Scope#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</CFQUERY>
</CFIF>
<!--- 2012-07-26 PPB P-SMA001 START commented out

<!--- lists all Country ID's to which User has rights --->
<cfinclude template="/templates/qrygetcountries.cfm">

<!--- Get country groups for this user to find which FlagGroups with Scope they can view --->

<!--- Find all Country Groups for this Country --->
<!--- Note: no User Country rights implemented for site currently --->
<CFQUERY NAME="getCountryGroups" DATASOURCE="#application.SiteDataSource#">
SELECT DISTINCT b.CountryID
 FROM Country AS a, Country AS b, CountryGroup AS c
WHERE (b.ISOcode IS null OR b.ISOcode = '')
  AND c.CountryGroupID = b.CountryID
  AND c.CountryMemberID = a.CountryID
  AND a.CountryID  IN ( <cf_queryparam value="#Variables.CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) <!--- User Country rights --->
ORDER BY b.CountryID
</CFQUERY>

<CFSET UserCountryList = "0,#Variables.CountryList#">
<CFIF getCountryGroups.CountryID IS NOT "">
	<!--- top record (or only record) is not null --->
	<CFSET UserCountryList =  UserCountryList & "," & ValueList(getCountryGroups.CountryID)>
</CFIF>

2012-07-26 PPB P-SMA001  --->
<CFQUERY NAME="getUsers" DATASOURCE="#application.SiteDataSource#">

<CFIF frmShowUsers is not 0>
<!--- people ---> 
	SELECT  
			2 as UserGroupsFirst,
			case when r.userid is not null then 1 else 2 end  as users_with_rights_first,
			p.PersonID as personid,
			ug.usergroupid as userid,
			p.FirstName + ' ' + p.Lastname  as name,
			p.Lastname  ,
			p.Active,
			o.OrganisationName,
			c.CountryDescription,
			r.Viewing, r.Search, r.Edit, r.Download

	FROM Person AS p 
			inner join
		 Location AS l on p.LocationID = l.LocationID
			 inner join
		 Organisation AS o on l.OrganisationID = o.OrganisationID
			 inner join
		 Country AS c on l.CountryID = c.CountryID
			 inner join
		 usergroup as ug on ug.personid = p.personid 
			left outer join 		 
		 FlagGroupRights AS r on r.UserID = ug.usergroupid 	AND r.FlagGroupID =  <cf_queryparam value="#frmFlagGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >  

	<!--- 2012-07-26 PPB P-SMA001 commented out 	 
	WHERE l.CountryID  IN ( <cf_queryparam value="#UserCountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	 --->	
	WHERE 1=1 #application.com.rights.getRightsFilterWhereClause(entityType="Person",alias="p",regionList="#request.relayCurrentUser.regionListSomeRights#").whereClause# 	<!--- 2012-07-25 PPB P-SMA001 --->	
	 
<!--- 	  AND p.Active = true show both but only allow active people to become users --->
  		AND	p.Password <> '' 
		AND	p.Username <> '' 
 		AND p.PasswordDate >= #CreateODBCDateTime(DateAdd("d",-application.com.settings.getSetting("security.internalsites.passwords.expiresin_days"),Now()))# 
		AND	p.LoginExpires > #CreateODBCDateTime(Now())# 


union
</cfif>

<!--- user groups ---> 
 select 
 
	1 as UserGroupsFirst,
	case when r.userid is not null then 1 else 2 end  as users_with_rights_first,
	null as personid, 
	usergroupid as userid, 
	ug.name, 
	null as lastname,
	null , 
	null as organisationname, 
	null as CountryDescription,
		r.Viewing, r.Search, r.Edit, r.Download

from 	userGroup as ug
			left outer join 		 
		FlagGroupRights AS r on r.UserID = ug.usergroupid 
							AND r.FlagGroupID =  <cf_queryparam value="#frmFlagGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >  
where 
	ug.personid is null

 



	ORDER BY userGroupsFirst, users_with_rights_first,LastName, OrganisationName, name

</CFQUERY>




<cf_head>
	<cf_title><cfoutput>#request.CurrentSite.Title#</cfoutput></cf_title>
<SCRIPT type="text/javascript">

<!--
        function updateRights() {
                var form = document.UsersForm;
                form.submit();
                return;
        }
//-->

</SCRIPT>
</cf_head>

<cfset application.com.request.setTopHead(topHeadcfm="profileTopHead.cfm")>

<!--- <CFOUTPUT>
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
<!--- 	WAb removed this line 2005-10-17 - looked rather odd, and the title is repeated a bit lower down
<TR><TD ALIGN="LEFT" VALIGN="TOP"><Div class="heading">Profile Access Rights</DIV></TD></TR> --->	
		<TR ALIGN="right" CLASS="submenu">
		<TD ALIGN="right" VALIGN="top" CLASS="submenu">
		<CFIF #checkPermission.AddOkay# GT 0>
		<A HREF="/profileManager/profileList.cfm" CLASS="submenu">Profiles</A> | 
		<A HREF="JavaScript:document.BackForm.submit();" CLASS="submenu">Back</A>&nbsp;&nbsp;
		</CFIF>
		</TD>
	</TR>
</TABLE>
</CFOUTPUT> --->



<CENTER>

<P><TABLE BORDER=0 BGCOLOR="#FFFFCC" CELLPADDING="5" CELLSPACING="0" WIDTH="320">
	
	<TR><Th COLSPAN="2">PROFILE ACCESS RIGHTS</Th></TR>
	<TR><TD ALIGN="CENTER" COLSPAN=2><CFOUTPUT>#htmleditformat(getFlagGroup.Name)#<BR>#htmleditformat(getFlagGroup.Description)#</CFOUTPUT><BR></TD>
	</TR>
		<TR><TD COLSPAN="2" ALIGN="CENTER"><CFOUTPUT><IMG SRC="/images/misc/rule.gif" WIDTH=230 HEIGHT=3 ALT="" BORDER="0"></CFOUTPUT></TD></TR>
	<TR><TD ALIGN="LEFT">
		Scope:<BR>
		Access Rights:<BR>
		Expires:<BR>
		Active:<BR>
		</TD>
		<TD ALIGN="LEFT">
			<CFOUTPUT>
			<CFIF getFlagGroup.Scope IS 0>All countries and regions<CFELSE>#HTMLEditFormat(getCountry.CountryDescription)#</CFIF> (<CFIF getFlagGroup.EntityTypeID IS 0>Person<CFELSE>Location</CFIF>)<BR>
			<CFIF #getFlagGroup.ViewingAccessRights# + #getFlagGroup.EditAccessRights# + #getFlagGroup.SearchAccessRights# + #getFlagGroup.DownloadAccessRights#IS 0>Open <FONT COLOR="##FF0000">(Rights below ignored)<CFELSE>Restricted (Using rights below)</CFIF></A><BR>
			#DateFormat(getFlagGroup.Expiry,"DD-MMM-YYYY")#<BR>
			<CFIF #getFlagGroup.Active# IS 0>No (Not displayed)<CFELSE>Yes</CFIF></A>
			</CFOUTPUT>		
		<BR></TD>
	</TR>
		<TR><TD COLSPAN="2" ALIGN="CENTER"><CFOUTPUT><IMG SRC="/images/misc/rule.gif" WIDTH=230 HEIGHT=3 ALT="" BORDER="0"></CFOUTPUT></TD></TR>
	<TR><TD COLSPAN="2" ALIGN="CENTER">
			Any users with existing rights listed at top.
		<BR></TD>
	</TR>
</TABLE>

<CFIF getUsers.Recordcount IS 0>
	
	<P>There are no users.

<CFELSE>
<cfoutput>
	<FORM ACTION="/profileManager/flagGroupUsersTask.cfm" NAME="UsersForm" METHOD="POST">
	<CF_INPUT TYPE="HIDDEN" NAME="frmFlagGroupID" VALUE="#frmFlagGroupID#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmDate" VALUE="#CreateODBCDateTime(now())#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmFlagGroupName" VALUE="#getFlagGroup.Name#">
	<INPUT TYPE="HIDDEN" NAME="frmNext" VALUE="/profileManager/flagGroupUsers"> <!--- changed to go back to this page for the 3 pane view --->
	<INPUT TYPE="HIDDEN" NAME="frmBack" VALUE="/profileManager/profileList">
	
	<P><A HREF="JavaScript:updateRights();"><IMG SRC="/images/buttons/c_savereturn_e.gif" WIDTH=145 HEIGHT=22 ALT="Save and Return" BORDER="0"></A>
	<P><A HREF="/profileManager/flagGroupUsers.cfm?frmFlagGroupID=#frmFlagGroupID#&frmShowUsers=1">Show Users and User Groups</A>
	</cfoutput>
	<CFSET cnt = 0>
	<P><TABLE BORDER="0" CELLPADDING="5" CELLSPACING="0" WIDTH="90%">
	<TR>
		<th VALIGN="TOP"><B>Name</B></th>
		<th VALIGN="TOP"><B>Company</B></th>
		<th VALIGN="TOP"><B>Country</B></th>
		<th ALIGN="CENTER" VALIGN="TOP"><B>View</B></th>
		<th ALIGN="CENTER" VALIGN="TOP"><B>Search</B></th>
		<th ALIGN="CENTER" VALIGN="TOP"><B>Edit</B></th>
 		<th ALIGN="CENTER" VALIGN="TOP"><B>Download</B></th>
	</TR>
	
	<CFSET UserList = "0">

	<CFLOOP QUERY="getUsers">

		<CFSET UserList = UserList & ",#UserID#">
	
		<CFSET thisUser = UserID>
	
		<CFSET #cnt# = #cnt# + 1>
	
		<CFOUTPUT>
			<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
			<TD VALIGN="TOP">#HTMLEditFormat(Name)# </TD>
			<TD VALIGN="TOP">#HTMLEditFormat(organisationName)#</TD> <!--- LID 3928 NJH 2010-08-25 - was sitename --->
			<TD VALIGN="TOP">#HTMLEditFormat(CountryDescription)#</TD>
			<TD ALIGN="CENTER" VALIGN="TOP"> <CFIF getflaggroup.viewingaccessrights is not 0> <CFIF #checkPermission.UpdateOkay# GT 0><CF_INPUT TYPE="CHECKBOX" NAME="V#UserID#" VALUE="1" CHECKED="#iif( Viewing IS NOT 0 AND Viewing IS NOT "" ,true,false)#"><CFELSE><CFIF #Viewing# IS NOT 0 AND #Viewing# IS NOT "">Yes</CFIF></CFIF> <CFELSE> &nbsp </CFIF> </TD>
			<TD ALIGN="CENTER" VALIGN="TOP"> <CFIF getflaggroup.Searchaccessrights is not 0> <CFIF #checkPermission.UpdateOkay# GT 0><CF_INPUT TYPE="CHECKBOX" NAME="S#UserID#" VALUE="1" CHECKED="#iif( Search IS NOT 0 AND Search IS NOT "",true,false)#"><CFELSE><CFIF #Search# IS NOT 0 AND #Search# IS NOT "">Yes</CFIF></CFIF> <CFELSE> &nbsp </CFIF> </TD>
			<TD ALIGN="CENTER" VALIGN="TOP"> <CFIF getflaggroup.editaccessrights is not 0> <CFIF #checkPermission.UpdateOkay# GT 0><CF_INPUT TYPE="CHECKBOX" NAME="E#UserID#" VALUE="1" CHECKED="#iif( Edit IS NOT 0 AND Edit IS NOT "",true,false)#"><CFELSE><CFIF #Edit# IS NOT 0 AND #Edit# IS NOT "">Yes</CFIF></CFIF> <CFELSE> &nbsp </CFIF> </TD>
 			<TD ALIGN="CENTER" VALIGN="TOP"> <CFIF getflaggroup.Downloadaccessrights is not 0> <CFIF #checkPermission.UpdateOkay# GT 0><CF_INPUT TYPE="CHECKBOX" NAME="D#UserID#" VALUE="1" CHECKED="#iif( Download IS NOT 0 AND Download IS NOT "",true,false)#"><CFELSE><CFIF #Download# IS NOT 0 AND #Download# IS NOT "">Yes</CFIF></CFIF> <CFELSE> &nbsp </CFIF> </TD>
		</TR>
		</CFOUTPUT>
		
	
	</CFLOOP>
	
	</TABLE>
	
	<P><A HREF="JavaScript:updateRights();"><IMG SRC="<CFOUTPUT>/images/buttons/c_savereturn_e.gif</CFOUTPUT>" WIDTH=145 HEIGHT=22 ALT="Save and Return" BORDER="0"></A>
	
	<CF_INPUT TYPE="HIDDEN" NAME="frmUserList" VALUE="#UserList#">
	</FORM>
	
</CFIF>
<P>


</CENTER>
</FORM>

<cfoutput>
	<FORM METHOD="POST" ACTION="/profileManager/profileList.cfm" NAME="BackForm">
		<INPUT TYPE="HIDDEN" NAME="null" VALUE="0">
	</FORM>
</cfoutput>




