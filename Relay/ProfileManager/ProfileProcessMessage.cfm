<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File name:	PartnerMessage.cfm
Author:		WAB
Date created:	May 2000

Description:  

Throws up a message

Amendment History:

Version 	Date (DD-MMM-YYYY)	Initials 	What was changed
1
2			2001-03-02				WAB

Enhancements still to do:

WAB 2010/10/13  removed get TextPhrases and replaced with CF_translate
--->



<cf_translate>
<CFOUTPUT>

<TABLE border="0" cellpadding="5" cellspacing="0" width="500">
	<TR>
		<TD align="cENTER" colspan="2">
				#application.com.relayUI.message(message=message)#
		</TD>
	</TR>
</TABLE>
<CFIF isdefined("buttonlist") AND isdefined("buttonURLs") AND listlen(buttonlist,",") EQ listlen(buttonURLs,",")>
	<CFSET nCount=1>
	<CFLOOP LIST="#buttonlist#" INDEX="x">
		<A href="#listgetat(buttonURLs,nCount,',')#">phr_#htmleditformat(x)#</A>
		<CFSET nCount=nCount+1>
	</CFLOOP>
</CFIF>
</CFOUTPUT>
</cf_translate>


