<!--- �Relayware. All Rights Reserved 2014 --->
<cfsetting enablecfoutputonly="yes">
<!---

File name:			flagGroupBulkLoad.cfm
Author:				WAB
Date created:		Nov 2006

Description:

Date Tested:
Tested by:

Amendment History:
2008-10-14	NYB		Amended error with adding child profiles via "Buld Add" in ProfileEdit
2009-02-09 	NJH		P-SNY047 changed from form to cfform as ugRecordmanager changed to using cfselect
2012-09-21 	PPB		Case 430402 add translate="true" to restrictKeyStrokes.js call
2014-11-24	RPW		CORE-986 Financial Flags that are bulk loaded cannot be used
29/01/2016  DAN     BF-400 - fix for adding bulk profiles - remove sel All function call


--->
<cfset frmTask= "bulkLoad">
<cfparam name="frmBack" default="">
	<cfparam name="frmparentflaggroupid" default = 0>
	<cfparam name="frmentitytypeID" default = 0>
	<cfset frmflagtypeid = 0>
	<cfset frmname = "">
	<cfset frmdescription = "">
	<cfset frmflaggrouptextid = "">
	<cfset frmnotes = "">
	<cfset frmorder = 1>
	<cfset frmexpiry = "#LSDateFormat(DateAdd('yyyy', 4, Now ()), 'dd-mmm-yyyy')#">
	<cfset frmscope = 0>
	<cfset frmviewingaccessrights = 0>
	<cfset frmeditaccessrights = 0>
	<cfset frmdownloadaccessrights = 0>
<cfset frmsearchaccessrights = 0>
	<cfset frmentitytypeID = 0>
	<cfset frmview = 1>
	<cfset frmsearch = 0>
	<cfset frmedit = 0>
	<cfset frmdownload = 0>
	<cfset frmactive = 1>
	<cfset frmpublicaccess = 0>
	<cfset frmpartneraccess = 0>
	<cfset frmuseinsearchscreen = 0>
	<cfset frmassocLive = 0>


<!--- the application .cfm checks for read adminTask privleges --->

	<cfquery name="GetEntityTypesAndParentGroups" DATASOURCE="#application.SiteDataSource#">
		SELECT DISTINCT
		flagentitytype.entitytypeid,
		tablename,
        FlagGroup.FlagGroupID,
        FlagGroup.Name,
		case when flagentitytype.entitytypeid <=2 then flagentitytype.entitytypeid else -1 end as ordering
        FROM flagentitytype left outer join FlagGroup on flagentitytype.entitytypeid = FlagGroup.entitytypeid
       <!--- WAB 2010-11-17 similar to NJH 2010-09-07 LID 3941 in profileedit.cfm commented out
	        WHERE
				1=1 --->
			AND
			ParentFlagGroupID = 0
			AND FlagTypeID = 1
			AND Active <> 0

			AND FlagGroup.Scope IN (0,#request.relaycurrentuser.CountryList#)
			AND ( FlagGroup.CreatedBy=#request.relayCurrentUser.usergroupid#
				OR
					(FlagGroup.EditAccessRights = 0 AND FlagGroup.Edit <> 0)
				OR EXISTS
						(		SELECT 1 FROM rights as r, RightsGroup as rg, SecurityType AS s
								WHERE  r.SecurityTypeID = s.SecurityTypeID
								AND s.ShortName  = 'AdminTask'
								AND r.usergroupid = rg.usergroupid
								AND rg.PersonID=#request.relayCurrentUser.personid#
						)
				OR (FlagGroup.EditAccessRights <> 0
					AND EXISTS (SELECT 1 FROM FlagGroupRights
						WHERE FlagGroup.FlagGroupID = FlagGroupRights.FlagGroupID
						AND FlagGroupRights.UserID=#request.relayCurrentUser.personid#
						AND FlagGroupRights.Edit <> 0)))
			AND NOT EXISTS (SELECT 1 FROM Flag WHERE Flag.FlagGroupID = FlagGroup.FlagGroupID)

        ORDER BY ordering desc, tablename,name
	</cfquery>
	<!--- NYB 2008-10-14 - changed is not to gt: --->
	<cfif isDefined("frmParentFlagGroupID") and frmParentFlagGroupID gt 0>
		<cfquery name = "getParent" dbtype="query">
			select* from GetEntityTypesAndParentGroups where flagGroupID = #frmParentFlagGroupID#
		</cfquery>

		<cfif getParent.recordCount is not 0>
			<cfset frmEntityTypeID = getParent.entityTypeID>
		</cfif>
	</cfif>

	<cfquery name="getFlagGroupTypes" DATASOURCE="#application.SiteDataSource#">
	SELECT flagTypeID, name FROM FlagType WHERE FlagTypeID <> 1
	union
	SELECT flagTypeID, 'DropDown' FROM FlagType WHERE name = 'Radio'
	ORDER BY Name
	</cfquery>

	<cfquery name="getCountries" DATASOURCE="#application.SiteDataSource#">
		SELECT DISTINCT
			a.CountryID,
			a.CountryDescription
		FROM Country AS a
		WHERE a.CountryID IN (#request.relaycurrentuser.CountryList#)
		AND a.ISOcode IS NOT null
		ORDER BY a.CountryDescription
	</cfquery>

	<cfquery name="getCountrygroups" DATASOURCE="#application.SiteDataSource#">
	SELECT DISTINCT b.CountryDescription AS CountryGroup, b.CountryID
	 FROM Country AS a, Country AS b, CountryGroup AS c
		WHERE a.CountryID IN (#request.relaycurrentuser.CountryList#)
	  AND (b.ISOcode IS null OR b.ISOcode = '')
	  AND c.CountryGroupID = b.CountryID
	  AND c.CountryMemberID = a.CountryID
	ORDER BY b.CountryDescription
	</cfquery>





<cfsetting enablecfoutputonly="no">





<cf_head>
	<cf_title><cfoutput>#request.CurrentSite.Title#</cfoutput></cf_title>

<cf_includejavascriptonce template = "/javascript/restrictkeystrokes.js" translate="true">		<!--- 2012-09-21 PPB Case 430402 add translate="true" --->

<cf_includeJavascriptOnce template = "/javascript/tablemanipulation.js">
<cf_includeJavascriptOnce template = "/javascript/popupDescription.js">
<cf_includeJavascriptOnce template = "/profileManager/js/flag.js"><!--- 2014-11-24	RPW		CORE-986 Financial Flags that are bulk loaded cannot be used - Added flag.js --->

<SCRIPT type="text/javascript">
<!--
       function verifyForm(task,next) {
                var form = document.FlagGroupForm;
                form.frmTask.value = task;
                form.frmNext.value = next;
                var msg = "";
           /*     if (form.frmName.value == "" || form.frmName.value == " ") {
                        msg += "* Name\n";
                }

                if (form.FlagGroupTextIDUnique.value  == 0) {
                        msg += "* A Unique (or Blank) Text Identifier\n";
                }
*/

                if (msg != "") {
                        alert("\nYou must provide the following information for this profile:\n\n" + msg);
                } else {
                	form.submit();
				}
        }



	function checkFlagGroupTextID(value) {
			page = '/webservices/misc.cfc?method=isFlagGroupTextIDUnique'
			parameters = 'flagGroupTextID='  + value

			var myAjax = new Ajax.Updater(
				'thediv',
				page,
				{
					method: 'get',
					parameters: parameters,
					evalScripts: true,
					onComplete: checkFlagGroupTextIDResult
				});

	}


	function checkFlagGroupTextIDResult (requestObject,JSON) {
		if (JSON == false ) {
			alert ('The Text Identifier is not Unique')
			document.FlagGroupForm.FlagGroupTextIDUnique.value = 0
		} else {
			document.FlagGroupForm.FlagGroupTextIDUnique.value = 1
		}

	}

-->

</script>
</cf_head>




<CFIF isdefined("frmNext")><CFSET ThisPageName=frmNext & ".cfm"></CFIF>
<cfoutput>

<cfinclude template="/profileManager/profileTopHead.cfm">

<!--- NJH 2009-02-09 P-SNY047 changed from form to cfform as ugRecordmanager changed to using cfselect --->
<cfform method="POST" action="/profileManager/flagGroupUpdateTask.cfm" name="FlagGroupForm">
<table border="0" cellspacing="2" cellpadding="3" align="center" bgcolor="White">


	<CF_INPUT type="HIDDEN" name="frmDate" value="#CreateODBCDateTime(now())#">
	<cfif frmtask is "edit"><CF_INPUT type="HIDDEN" name="frmFlagGroupID" value="#frmFlagGroupID#"></cfif>
	<input type="HIDDEN" name="frmTask" value="">
	<input type="HIDDEN" name="frmNext" value="">
	<CF_INPUT type="HIDDEN" name="frmBack" value="#frmBack#">
	<!--- WAB 2014-10-22 removed now that expiry is a datepicker and does its own locale specific date validation
	<input type="HIDDEN" name="frmExpiry_eurodate" value="You must enter the expiry date in 'dd-mmm-yyyy' format.">
	--->
<!---
		DAM 11 Sept 2002
		This was causing problems will return to it later

 	<input type="HIDDEN" name="frmPublicAccess_integer" value="Field 'Public' must be an integer.">
	<input type="HIDDEN" name="frmPartnerAccess_integer" value="Field 'Partner Access' must be an integer."> --->

	<tr>
		<td colspan="3">
		<cfif isdefined('message')>
			#application.com.relayUI.message(message=message)#
		<cfelse>
			&nbsp;
		</cfif>
		</td>
	</tr>

	<tr>
		<td valign="TOP">
			Which table does these profiles link to:
		</td>
		<td colspan="2" valign="top">

	<!---
			A

			This tag was added to genericise the selection of profiles
			previously it was hard coded.


	--->
			<cfparam name="frmEntityTypeID" default = "">
			<cf_twoselectsrelated
				query="GetEntityTypesAndParentGroups"
				name1="FRMENTITYTYPEID"
				name2="frmParentFlagGroupID"
				display1="tablename"
				display2="name"
				value1="entitytypeid"
				value2="FlagGroupID"
				forcewidth1="30"
				forcewidth2="60"
				selected1= "#frmEntityTypeID#"
				selected2= "#frmParentFlagGroupID#"
				size1="1"
				size2="auto"
				autoselectfirst="Yes"
				emptytext1="(Select Table)"
				emptytext2="(Optional Parent Profile)"
				forcelength2 = "6"
				formname = "FlagGroupForm">

		</td>
	</tr>
	<!--- <tr>
		<td valign="top">
			Profile Data Type:
		</td>
		<td colspan="2" valign="top">
				<select name="frmFlagTypeID">
					<option value="1"<cfoutput>#IIF(frmFlagTypeID IS 0, DE(" SELECTED"), DE(""))#</cfoutput>> None (Parent)
					<option value="1">-----------------------------------------
					<cfoutput query="getFlagGroupTypes">
						<option value="#FlagTypeID#"#iif(frmflagtypeid is flagtypeid, de(" SELECTED"), de(""))#> #HTMLEditFormat(Name)#
					</cfoutput>
				</select>
		</td>
	</tr> --->

	<tr>
		<td valign="top">
				Profile Details: <BR>


		</td>
		<td colspan="2" valign="top">





<cfset helptext = "Paste from Excel a table containing columns for profile name, type and description into the text area above.  <BR>When you click out of the text area this data will be inserted into the table above.">

<cfsavecontent variable="typeDropdown_html"><cfloop query="getFlagGroupTypes"><option value="#flagTypeID#">#HTMLEditFormat(Name)#</cfloop></cfsavecontent>

<cfset rowStruct = arrayNew(1)>
<cfset rowStruct[1] = structNew()>
<cfset rowStruct[2] = structNew()>
<cfset rowStruct[3] = structNew()>
<cfset rowStruct[1].name = "Name">
<cfset rowStruct[3].name = "Description">
<cfset rowStruct[2].name = "Type">
<cfset rowStruct[1].content = '<input class=flaggrid name="Name_INDEX" Type=text value="" size=30 onBlur=checkForBlankRow("FlagTable")><input type="hidden" name="frmFlagGroupID" value="INDEX">'>
<cfset rowStruct[3].content = '<input class=flaggrid name="Description_INDEX" Type=text value="" size=50>'>
<cfset rowStruct[2].content = '<select class=flaggrid id="Type_INDEX" name="Type_INDEX" onChange="DisplaySubType(''Type_'',''INDEX'',''bulk'');" ><option value="">Select Type#typeDropdown_html#'><!--- 2014-11-24	RPW		CORE-986 Financial Flags that are bulk loaded cannot be used - added DisplaySubType --->




<table id="FlagTable"  cellspacing=0 cellpadding=0 class="flaggrid">
					<tbody>
					<tr class=flaggrid>
						<th class=flaggrid>Name</td>
						<th class=flaggrid>Type</td>
						<th class=flaggrid>Description</td>
					</tr>
</table>
		<div align=left >
			<textarea cols="50" rows="3" name="excelPaste" id="excelPaste" onChange="copyTextAreaToTable('excelPaste','FlagTable');this.value=''" value=""></textarea>
			<BR>
			<!--- 2014-11-24	RPW		CORE-986 Financial Flags that are bulk loaded cannot be used - fixed the popup help --->
			<div onmouseover="popHelp('#helptext#','excelPaste',event)" onmouseout="killHelp()">Excel Paste Area <img src="../images/icons/help.png" alt="" width="16" height="16" border="0"></div>
		</div>

		</td>
	</tr>
<script>
	<cfwddx action="cfml2js" input = "#rowStruct#" topLevelVariable = "newRowFlagTable">
	addRowToTable('FlagTable') ;
</script>

	<tr>
		<td class="Label">Translate Name</td>
		<td  colspan="2" valign="TOP">

				<input type="checkbox" name="frmTranslateLabel" value="1" CHECKED> Create Default Translation

		</td>
	</tr>

	<tr>
		<td valign="top">
			Start Ordering Index at :
		</td>
		<td colspan="2" valign="top">
			<select name="frmOrder"><cfloop index="LoopCount" from="1" to="20">
				<cfif loopcount is frmorder>
					<option value=#loopcount# selected> #htmleditformat(LoopCount)#
				<cfelse>
					<option value=#loopcount#> #htmleditformat(LoopCount)#
				</cfif>
			</cfloop></select>
			<br>Profiles with the same Ordering Index are sorted alphabetically.
		</td>
	</tr>
</table>
<br>
<table width="80%" border="0" cellspacing="2" cellpadding="3" align="center" bgcolor="White">
	<tr>
		<td colspan=4><b>
			Display Details
			<br><br><img src="<CFOUTPUT></CFOUTPUT>/images/misc/rule.gif" WIDTH=100% HEIGHT=3 ALT="" BORDER="0"><br><br></b>
		</td>
	</tr>
	<tr>
		<td valign=top>
			Country Scope:
		</td>
		<td valign=top colspan=3>
				<select name="frmScope">
					<option value="0" #IIF(frmScope IS "0", DE(" SELECTED"), DE(""))#> All Countries/Groups
					<cfloop query="getCountryGroups">
						<option value="#CountryID#"#iif(frmscope is countryid, de(" SELECTED"), de(""))#> #HTMLEditFormat(CountryGroup)#
					</cfloop>
						<option value="0">-----------------------------------------
					<cfloop query="getCountries">
						<option value="#CountryID#"#iif(frmscope is countryid, de(" SELECTED"), de(""))#> #HTMLEditFormat(CountryDescription)#
					</cfloop>
				</select><br><br>
		</td>
	</tr>

		<tr>
			<td>Specify usage of these profiles' attributes </td>
			<th>All users</th>
			<th>Only Specified <br>Users/Groups</th>
			<th>None</th>
		</tr>
		<tr>
			<td align="right">View profile</td>
			<td align="center"><input type="radio" name="frmViewingRights" value="all" <cfif frmview is 1>checked</cfif>></td>
			<td align="center"><input type="radio" name="frmViewingRights" value="userGroup" <cfif frmviewingaccessrights is 1>checked</cfif>></td>
			<td align="center"><input type="radio" name="frmViewingRights" value="none" <cfif frmviewingaccessrights is not 1 and frmview is not 1>checked</cfif>></td>
		</tr>
		<tr>
			<td align="right">Edit profile</td>
			<td align="center"><input type="radio" name="frmEditRights" value="all" <cfif frmEdit is 1>checked</cfif>></td>
			<td align="center"><input type="radio" name="frmEditRights" value="usergroup" <cfif frmEditaccessrights is 1>checked</cfif>></td>
			<td align="center"><input type="radio" name="frmEditRights" value="none" <cfif frmEditaccessrights is not 1 and frmEdit is not 1>checked</cfif>></td>
		</tr>
		<tr>
			<td align="right">Make selections based on profile</td>
			<td align="center"><input type="radio" name="frmSearchRights" value="all" <cfif frmSearch is 1>checked</cfif>></td>
			<td align="center"><input type="radio" name="frmSearchRights" value="usergroup" <cfif frmSearchaccessrights is 1>checked</cfif>></td>
			<td align="center"><input type="radio" name="frmSearchRights" value="none" <cfif frmSearchaccessrights is not 1 and frmSearch is not 1>checked</cfif>></td>
		</tr>
		<tr>
			<td align="right">Download selection</td>
			<td align="center"><input type="radio" name="frmDownloadRights" value="all" <cfif frmDownload is 1>checked</cfif>></td>
			<td align="center"><input type="radio" name="frmDownloadRights" value="usergroup" <cfif frmDownloadaccessrights is 1>checked</cfif>></td>
			<td align="center"><input type="radio" name="frmDownloadRights" value="none" <cfif frmDownloadaccessrights is not 1 and frmDownload is not 1>checked</cfif>></td>
		</tr>
<tr>
		<td align="right">Specify who has rights to update profile definitions
		(The creator and administrator always have this right)
		<BR></td>
		<td colspan="3" valign="top" nowrap>
				<!--- levels 1 view, 2 edit, 3 search, 4 download, 5 updateprofile --->
				<cfif frmTask is "edit">
					<cfset tempentityID = frmFlagGroupID>
				<cfelse>
					<cfset tempentityID = "frmFlagGroupID">
				</cfif>
				<CF_UGRecordManager entity="flagGroup" entityid="#tempentityID#" Form="FlagGroupForm" level = "5" >
		</td>
</tr>
	<tr>
		<td valign="TOP">
			Show the profile attributes in Advanced Search
		</td>
		<td colspan="3" valign="top" nowrap>
			<CF_INPUT type="RADIO" name="frmUseInSearchScreen" value="0"  CHECKED="#iif(frmUseInSearchScreen IS 0,true,false)#"> No &nbsp;&nbsp;
			<CF_INPUT type="RADIO" name="frmUseInSearchScreen" value="1"  CHECKED="#iif(frmUseInSearchScreen IS NOT 0,true,false)#"> Yes <br>
		</td>
	</tr>
	<tr>
		<td valign=top>
			Access expires from this date
		</td>
		<td valign=top colspan=3>
			<CF_relayDatefield fieldname="frmExpiry" currentValue="#DateFormat(frmExpiry,'dd-mmm-yyyy')#">
			<!--- <CF_INPUT type="TEXT" name="frmExpiry" value="#frmExpiry#" SIZE=11 MAXLENGTH=11> (dd-mmm-yyyy)<br> --->
		</td>
	</tr>
	<tr>
		<td valign=top>
			Active :
		</td>
		<td colspan="3" valign="top" nowrap>
			<CF_INPUT type="RADIO" name="frmActive" value="0"  CHECKED="#iif(frmActive IS 0,true,false)#"> No &nbsp;&nbsp;
			<CF_INPUT type="RADIO" name="frmActive" value="1"  CHECKED="#iif(frmActive IS NOT 0,true,false)#"> Yes <br><br>
		</td>
	</tr>

	<tr>
		<td colspan="4" align="centre"><cfoutput><img src="/images/misc/rule.gif" alt="" width="590" height="3" border="0"></cfoutput></td>
	</tr>


</table>


</cfform>
<p>
</cfoutput>

</center>
<!--- </form> NJH 2009-02-09 a closing form tag without an opening form tag. --->







