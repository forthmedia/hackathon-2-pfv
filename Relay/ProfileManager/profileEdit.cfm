<!--- �Relayware. All Rights Reserved 2014 --->

<cfsilent>
<!---

File name:			profileEdit.cfm
Author:				BOCC
Date created:		Long time ago!

Description:		Edits the

Date Tested:
Tested by:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
28-Jun-2000 		SWJ			Changed frmExpiry to use dateAdd
03-Jul-2000			SWJ			Added UseInSearchScreen processing
12-Jul-2000			SWJ			Added Javascript function to prefill flagGroupTextID
03-Aug-2000			DAM			Added TwoSelectRelated Tag to replace the hard coded person and location flag lists
								This required the removal of javasscript which had been used to determine
								what the value of frmParentFlagGroupID parameter should have been.
								There were also two queries which pulled back data for person and location flaggroups
								but this has been replaced by one that brings back all flaggroups
2000-09-15				WAB			Corrected DAM problem when no flaggroups already existed for given entitytype
2000-10-13			WAB			removed Javascript function to prefill flagGroupTextID because it creates invalid ones with spaces, odd characters etc. needs tobe modified and replaced
2001-10-20			SWJ			Changed over to profile manager
2002-02-05			DAM			Added Entity Member Association (see details below)
2005-10-17			WAB 		implemented function for testing whether user has rights to edit flag/flaggroup definition
2007/10 ?            WAB  		Show screens that this flaggroup appear in
2008/03/03           WAB  		Show screens that flags in this flaggroup appear in
2008-10-14	NYB		Amended error with adding child profiles via "Buld Add" in ProfileEdit
2009/02/09 			NJH			P-SNY047 changed from form to cfform as ugRecordmanager changed to using cfselect
2010/09/07 			 NJH 		LID 3941
2012-09-21 			PPB 		Case 430402 add translate="true" to restrictKeyStrokes.js call
2014-08-11			RPW 		Create new profile type: "Financial"
2014-11-24			RPW			CORE-986 Financial Flags that are bulk loaded cannot be used
2015/09/24			NJH			made flagGroupTextIds required and altered the function that checks the uniqueness. Now also check against the entity columns.
2015/09/29			NJH			Resurrect the setTextID functionality
2016/01/06			NJH			Remove the sel All function call in JS
2016/07/27          VSN         Case 450103 Add localisation to the dateformat to prevent server error
2016/09/26			NJH			JIRA PROD2016-2383 - remove the expiry data for flag groups.

--->

<!--- the application .cfm checks for read adminTask privleges --->
<cfif frmtask is "add">

	<cfparam name="frmparentflaggroupid" default = 0>
	<cfparam name="frmentitytypeID" default = 0>
	<cfset frmflagtypeid = 0>
	<cfset frmname = "">
	<cfset frmHelpText = "">
	<cfset frmdescription = "">
	<cfset frmflaggrouptextid = "">
	<cfset frmnotes = "">
	<cfset frmorder = 1>
	<!--- VSN Case 450103 Add localisation to the dateformat to prevent server error BEGIN --->
	<cfset frmexpiry = "#LSDateFormat(DateAdd('yyyy', 4, Now ()), 'dd-mmm-yyyy', 'en_us')#">
	<!--- VSN Case 450103 Add localisation to the dateformat to prevent server error BEGIN --->
	<cfset frmscope = 0>
	<cfset frmviewingaccessrights = 0>
	<cfset frmeditaccessrights = 0>
	<cfset frmdownloadaccessrights = 0>
	<cfset frmsearchaccessrights = 0>
	<cfset frmview = 1>
	<cfset frmsearch = 0>
	<cfset frmedit = 0>
	<cfset frmdownload = 0>
	<cfset frmactive = 1>
	<cfset frmpublicaccess = 0>
	<cfset frmpartneraccess = 0>
	<cfset frmuseinsearchscreen = 0>
	<cfset frmassocLive = 0>
	<CFSET frmFormattingParameters = "">
	<cfset frmUseInAllDomains = 0> <!--- NJH 2011/02/10 P-FNL075 --->

	<cfquery name="getCountries" DATASOURCE="#application.SiteDataSource#">
		SELECT DISTINCT
			a.CountryID,
			a.CountryDescription
		FROM Country AS a
		WHERE a.CountryID IN (#request.relaycurrentuser.CountryList#)
		AND a.ISOcode IS NOT null
		ORDER BY a.CountryDescription
	</cfquery>

	<cfquery name="getCountrygroups" DATASOURCE="#application.SiteDataSource#">
	SELECT DISTINCT b.CountryDescription AS CountryGroup, b.CountryID
	 FROM Country AS a, Country AS b, CountryGroup AS c
		WHERE a.CountryID IN (#request.relaycurrentuser.CountryList#)
	  AND (b.ISOcode IS null OR b.ISOcode = '')
	  AND c.CountryGroupID = b.CountryID
	  AND c.CountryMemberID = a.CountryID
	ORDER BY b.CountryDescription
	</cfquery>

	<cfset usercountrylist = "0,#request.relaycurrentuser.CountryList#">
	<cfif getcountrygroups.countryid is not "">
		<!--- top record (or only record) is not null --->
		<cfset usercountrylist =  usercountrylist & "," & valuelist(getcountrygroups.countryid)>
	</cfif>

	<cfquery name="GetEntityTypes" DATASOURCE="#application.SiteDataSource#">
	SELECT DISTINCT
			flagentitytype.entitytypeid,
			tablename,
	        flagentitytype.description,
	        FlagGroup.FlagGroupID,
	        FlagGroup.Name
	        FROM  flagentitytype left outer join FlagGroup on flagentitytype.entitytypeid = FlagGroup.entitytypeid
	       <!--- NJH 2010/09/07 LID 3941 commented out
	        WHERE
				1=1 --->
			AND
			ParentFlagGroupID = 0
			AND FlagTypeID = 1
			AND Active <> 0

			AND FlagGroup.Scope  IN ( <cf_queryparam value="#UserCountryList#" CFSQLTYPE="CF_SQL_Integer"  list="true"> )
			AND ( FlagGroup.CreatedBy=#request.relayCurrentUser.usergroupid#
				OR
					(FlagGroup.EditAccessRights = 0 AND FlagGroup.Edit <> 0)
				OR EXISTS
						(		SELECT 1 FROM rights as r, RightsGroup as rg, SecurityType AS s
								WHERE  r.SecurityTypeID = s.SecurityTypeID
								AND s.ShortName  = 'AdminTask'
								AND r.usergroupid = rg.usergroupid
								AND rg.PersonID=#request.relayCurrentUser.personid#
						)
				OR (FlagGroup.EditAccessRights <> 0
					AND EXISTS (SELECT 1 FROM FlagGroupRights
						WHERE FlagGroup.FlagGroupID = FlagGroupRights.FlagGroupID
						AND FlagGroupRights.UserID=#request.relayCurrentUser.personid#
						AND FlagGroupRights.Edit <> 0)))
			AND NOT EXISTS (SELECT 1 FROM Flag WHERE Flag.FlagGroupID = FlagGroup.FlagGroupID)

        ORDER BY tablename,name
	</cfquery>

	<!--- NJH 2008/09/16 added  'and is not ""'--->
	<cfif isDefined("frmParentFlagGroupID") and frmParentFlagGroupID is not 0 and frmParentFlagGroupID is not "">
		<cfquery name = "getParent" dbtype="query">
			select* from getEntityTypes where flagGroupID = #frmParentFlagGroupID#
		</cfquery>

		<cfif getParent.recordCount is not 0>
			<cfset frmEntityTypeID = getParent.entityTypeID>
		</cfif>
	</cfif>

	<cfquery name="getFlagGroupTypes" DATASOURCE="#application.SiteDataSource#">
	SELECT * FROM FlagType WHERE FlagTypeID <> 1 ORDER BY Name
	</cfquery>

<cfelse>

<!--- 	<!--- Check if this user has Edit rights for this group as well (as view) --->
	<!--- wab added bit so that adminstrators can do anything --->
	<cfquery name="checkEdit" DATASOURCE="#application.SiteDataSource#">
	SELECT 1
	FROM FlagGroup
	WHERE FlagGroupID = #frmFlagGroupID#
	AND ( CreatedBy=#request.relayCurrentUser.usergroupid#
		OR (EditAccessRights = 0 AND Edit <> 0)
		or exists (		SELECT 1 FROM rights as r, RightsGroup as rg, SecurityType AS s
						WHERE  r.SecurityTypeID = s.SecurityTypeID
						AND s.ShortName  = 'AdminTask'
						AND r.usergroupid = rg.usergroupid
						AND rg.PersonID=#request.relayCurrentUser.personid# )
		OR (EXISTS (SELECT 1 FROM FlagGroupRights AS t WHERE EditAccessRights <> 0
                AND FlagGroupID = t.FlagGroupID
                AND t.UserID=#request.relayCurrentUser.personid#
                AND t.Edit <> 0)))
	</cfquery>

	<cfif checkedit.recordcount is 0>
 --->
	<cfif not application.com.flag.doesCurrentUserHaveRightsToEditFlagGroupDefinition(frmFlagGroupID)>
		<cfset message = "You do not appear to have sufficient rights to access this Profile's information. <P>Please contact support if you need further information.">
		<cfset messageType="info">
		<cfinclude template="/profileManager/profileList.cfm">
		<CF_ABORT>
	</cfif>

	<cfquery name="getFlagGroup" DATASOURCE="#application.SiteDataSource#">
		SELECT DISTINCT FlagGroup.*, FlagType.Name As type_name
		FROM FlagGroup, FlagType
		WHERE FlagGroupID =  <cf_queryparam value="#frmFlagGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >
		AND FlagGroup.FlagTypeID = FlagType.FlagTypeID
	</cfquery>

	<cfset ScreensContainingFlagGroup = application.com.screens.getScreensContainingFlagGroup(frmflagGroupID)>
	<cfset ScreensContainingFlagsInFlagGroup = application.com.screens.getScreensContainingFlagsInFlagGroup(frmflagGroupID)>

	<cfif getflaggroup.scope is not 0>
		<cfquery name="getScope" DATASOURCE="#application.SiteDataSource#">
		SELECT CountryDescription From Country WHERE CountryID =  <cf_queryparam value="#getFlagGroup.Scope#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>
	</cfif>

	<cfif getflaggroup.parentflaggroupid is not 0>
		<cfquery name="getParentFlagGroup" DATASOURCE="#application.SiteDataSource#">
		SELECT Name FROM FlagGroup WHERE FlagGroupID =  <cf_queryparam value="#getFlagGroup.ParentFlagGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>
	</cfif>

	<cfset frmparentflaggroupid = getflaggroup.parentflaggroupid>
	<cfset frmflagtypeid = getflaggroup.flagtypeid>
	<cfset frmname = "#getFlagGroup.Name#">
	<cfset frmnamePhrasetextID = "#getFlagGroup.namePhrasetextID#">
	<cfset frmdescription = "#getFlagGroup.Description#">
	<cfset frmHelpText = getFlagGroup.helpText>
	<cfset frmflaggrouptextid = "#getFlagGroup.FlagGroupTextID#">
	<cfset frmnotes = "#getFlagGroup.Notes#">
	<cfset frmorder = getflaggroup.orderingindex>
	<cfset frmexpiry = #dateformat(getflaggroup.expiry,"DD-MMM-YYYY")#>
	<cfset frmscope = getflaggroup.scope>
	<cfset frmviewingaccessrights = getflaggroup.viewingaccessrights>
	<cfset frmeditaccessrights = getflaggroup.editaccessrights>
	<cfset frmdownloadaccessrights = getflaggroup.downloadaccessrights>
	<cfset frmsearchaccessrights = getflaggroup.searchaccessrights>
	<cfset frmentitytypeID = getflaggroup.entitytypeid>
	<cfset frmview = getflaggroup.viewing>
	<cfset frmsearch = getflaggroup.search>
	<cfset frmedit = getflaggroup.edit>
	<cfset frmdownload = getflaggroup.download>
	<cfset frmactive = getflaggroup.active>
	<cfset frmpublicaccess = getflaggroup.publicaccess>
	<cfset frmpartneraccess = getflaggroup.partneraccess>
	<CFSET frmFormattingParameters = getflaggroup.FormattingParameters>
	<cfset frmuseinsearchscreen = getflaggroup.useinsearchscreen>
	<cfset frmassocLive = getflaggroup.EntityMemberLive>
</cfif>

<!---	Entity Member Association

		DAM 5 March 2002
		There is a new concept of an Entity Member Profile.
		This is predominantly for external sites where users need information to flag groups
		that relate directly to them.

		The first working example is for a hosting organisation who has listed organisations who
		are running recruitment programs. Recruitment is recorded by assigning attributes of
		a relevant flaggroup. The structure of these flaggroups may be different. When the listed
		organisation's users visit the external site they need to see the stats from this flaggroup.
		In order for them to recieve inherent rights to the flaggroup they must be associated to it
		somehow. This is achieved by the inclusion of a new field in flaggroup table called

		EntityMemberID

		Rationalle dictates that this should never be edited unless the organisation is no longer
		in the system.
--->

<CFSET AssocOrg="No">
<CFIF frmTask IS NOT "ADD">
	<CFIF getFlagGroup.EntityMemberID GT 0>
		<cfquery name="getAssociatedOrg" DATASOURCE="#application.SiteDataSource#">
			SELECT OrganisationID, OrganisationName
			FROM Organisation
			WHERE 	active = 1
				AND
					Organisationid =  <cf_queryparam value="#getFlagGroup.EntityMemberID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>
		<CFIF getAssociatedOrg.Recordcount EQ 1>
			<CFSET AssocOrg="Yes">
		</CFIF>
	<CFELSE>
		<CFSET AssocOrg="None">
	</CFIF>
</CFIF>

<CFIF frmTask IS NOT "ADD" OR AssocOrg IS "Yes">
	<!--- We need all the orgs to choose an associated member --->
	<cfquery name="getAllOrgs" DATASOURCE="#application.SiteDataSource#">
		SELECT OrganisationID, OrganisationName
		FROM Organisation
		WHERE active = 1
		ORDER BY OrganisationName
	</cfquery>
</CFIF>

</cfsilent>

<cfif isDefined('frmFlagGroupID') and frmFlagGroupID is not "">
	<cfoutput>
		<script>
			parent.bottomLeft.location = "/profileManager/profileDetails.cfm?frmFlagGroupID=#jsStringFormat(frmFlagGroupID)#";
		</script>
	</cfoutput>
</cfif>


<cf_includejavascriptonce template = "/javascript/restrictkeystrokes.js" translate="true">		<!--- 2012-09-21 PPB Case 430402 add translate="true" --->
<cf_includejavascriptonce template = "/javascript/openWin.js">
<cf_includeJavascriptOnce template = "/profileManager/js/flag.js"><!--- 2014-11-24	RPW	CORE-986 Financial Flags that are bulk loaded cannot be used - Replaced function DisplaySubType with flag.js --->

<cfset application.com.request.setTopHead(topHeadcfm="profileTopHead.cfm")>

<cf_head>
<SCRIPT type="text/javascript">

<!--

       function verifyForm(task,next) {
                var form = document.FlagGroupForm;
                form.frmTask.value = task;
                form.frmNext.value = next;
                var msg = "";
				if (form.frmEntityTypeID.value == "") {
					 msg += "* Entity Type\n";
				}

                if (form.frmName.value == "" || form.frmName.value == " ") {
                        msg += "* Name\n";
                }

                if (form.frmFlagGroupTextID.value.trim() == "" || form.FlagGroupTextIDUnique.value  == 0) {
                        msg += "* A Unique Text Identifier\n";
                }

                if (msg != "") {
                        alert("\nYou must provide the following information for this profile:\n\n" + msg);
                } else {
                	form.submit();
				}
        }

	   function setTextID(){
	   		var form = document.FlagGroupForm;
			if (form.frmFlagGroupTextID.value == "") {
				form.frmFlagGroupTextID.value = form.frmName.value.replace(/\W/g, '');
				jQuery('#frmFlagGroupTextID').change();
			}
	   }
//-->


	function checkFlagGroupTextID(value,entityType) {

		jQuery.ajax({
			type: "GET",
			url: "/webservices/callWebService.cfc?",
			data: {method:'callWebService',webServiceName:'misc',methodName:'isFlagGroupTextIDUnique', flagGroupTextID:value, entityType:entityType, returnFormat:'json'},
			dataType: "json",
			cache: false,
			async: false,
			success: function(data,textStatus,jqXHR) {
				var $_uniqueErrorMsgDiv = jQuery('#notUniqueMessage');
				var $_uniqueMsgDiv = jQuery('#uniqueMessage');
				if (data === false) {
					if ($_uniqueMsgDiv.length == 1) {
						$_uniqueMsgDiv.remove();
					}
					if ($_uniqueErrorMsgDiv.length == 0) {
						jQuery('#frmFlagGroupTextID').after('<div id="notUniqueMessage" class="errorblock">Text ID is not unique</div>')
					}
					document.FlagGroupForm.FlagGroupTextIDUnique.value = 0;
				} else {
					if ($_uniqueErrorMsgDiv.length == 1) {
						$_uniqueErrorMsgDiv.remove();
					}
					if ($_uniqueMsgDiv.length == 0) {
						jQuery('#frmFlagGroupTextID').after('<div id="uniqueMessage" class="successblock">Text ID unique</div>')
					}
					document.FlagGroupForm.FlagGroupTextIDUnique.value = 1;
				}
			}
		})
	}

</script>
</cf_head>


<!--- NJH 2009/02/09 P-SNY047 changed from form to cfform as ugRecordmanager changed to using cfselect --->
<!--- 2014-08-11		RPW Create new profile type: "Financial" --->
<form method="POST" action="/profileManager/flagGroupUpdateTask.cfm" name="FlagGroupForm">
<cf_relayFormDisplay relayFormDisplayStyle = "HTML-table">
<cfoutput>
	<CF_INPUT type="HIDDEN" name="frmDate" value="#CreateODBCDateTime(now())#">
	<!--- NYB 2008-10-14 - replaced 'frmtask is "edit"' with 'frmtask neq "add" and frmtask is "delete"' --->
	<cfif frmtask neq "add" and frmtask neq "delete">
		<CF_INPUT type="HIDDEN" name="frmFlagGroupID" value="#frmFlagGroupID#">
	</cfif>
	<input type="HIDDEN" name="frmTask" value="">
	<input type="HIDDEN" name="frmNext" value="">
	<CF_INPUT type="HIDDEN" name="frmBack" value="#frmBack#">
	<!--- WAB 2014-10-22 removed now that expiry is a datepicker and does its own locale specific date validation
	<input type="HIDDEN" name="frmExpiry_eurodate" value="You must enter the expiry date in 'dd-mmm-yyyy' format.">
	--->
<!---
		DAM 11 Sept 2002
		This was causing problems will return to it later

 	<input type="HIDDEN" name="frmPublicAccess_integer" value="Field 'Public' must be an integer.">
	<input type="HIDDEN" name="frmPartnerAccess_integer" value="Field 'Partner Access' must be an integer."> --->

	<tr>
		<td colspan="3">
			<strong><cfif frmtask is "edit">Edit Details for '#htmleditformat(frmName)#'<cfelse>Add New Profile</cfif></strong>
			<hr width="100%" size="1">
		</td>
	</tr>
	<tr>
		<td colspan="3">
		<cfif isdefined('message')>
			#application.com.relayUI.message(message=message)#
		<cfelse>
			&nbsp;
		</cfif>
		</td>
	</tr>
</cfoutput>
	<tr>
		<td valign="TOP">
			<span class="required">Which Entity Type does this profile link to:</span>
		</td>
		<td colspan="2" valign="top">
			<cfif frmtask is "add">
	<!---
			A

			This tag was added to genericise the selection of profiles
			previously it was hard coded.


	--->
			<cf_twoselectsrelated
				query="GetEntityTypes"
				name1="frmEntityTypeID"
				name2="frmParentFlagGroupID"
				display1="description"
				display2="name"
				value1="entitytypeid"
				value2="FlagGroupID"
				selected1= "#frmEntityTypeID#"
				selected2= "#frmParentFlagGroupID#"
				size1="1"
				size2="auto"
				autoselectfirst="Yes"
				emptytext1="(Select Entity Type)"
				emptytext2="(Optional Parent Profile)"
				forcelength2 = "6"
				formname = "FlagGroupForm">
			<cfelse>
				<cfoutput>
				<CF_INPUT type="HIDDEN" name="frmEntityTypeID" value="#frmEntityTypeID#">
				<CF_INPUT type="HIDDEN" name="frmParentFlagGroupID" value="#frmParentFlagGroupID#">
				#application.entityType[frmentitytypeID].description#	<cfif frmparentflaggroupid is not 0>(with parent #htmleditformat(getParentFlagGroup.Name)#)</cfif>
				</cfoutput>
			</cfif>
		</td>
	</tr>
	<!--- 2014-08-11		RPW Create new profile type: "Financial" --->
	<tr id="frmFlagType">
		<td valign="top">
			Profile Data Type:
		</td>
		<td colspan="2" valign="top">
			<cfif frmtask is "add">
				<!--- 2014-08-11		RPW Create new profile type: "Financial" --->
				<!--- 2014-11-24	RPW	CORE-986 Financial Flags that are bulk loaded cannot be used - Added parameters to DisplaySubType to use flag.js --->
				<select name="frmFlagTypeID" id="frmFlagTypeID" onChange="DisplaySubType('frmFlagTypeID','','single');">
					<option value="1"<cfoutput>#IIF(frmFlagTypeID IS 0, DE(" SELECTED"), DE(""))#</cfoutput>> None (Parent)
					<option value="1">-----------------------------------------
					<cfoutput query="getFlagGroupTypes">
						<option value="#FlagTypeID#"#iif(frmflagtypeid is flagtypeid, de(" SELECTED"), de(""))#> #HTMLEditFormat(Name)#
					</cfoutput>
				</select>
			<cfelse>
				<cfoutput>
				<CF_INPUT type="HIDDEN" name="frmFlagTypeID" value="#frmFlagTypeID#">
					<cfif frmflagtypeid is 1>
						None
					<cfelse>
						#htmleditformat(getFlagGroup.type_name)#
					</cfif>
				</cfoutput>
			</cfif>
		</td>
	</tr>
	<!--- 2014-08-11		RPW Create new profile type: "Financial" --->
	<cfif frmtask NEQ "add" AND frmflagtypeid EQ 14>
	<tr>
		<td valign="top">
			Currency | Decimal Places:
		</td>
		<td colspan="2" valign="top">
			<cfoutput>#getFlagGroup.FlagTypeCurrency# | #getFlagGroup.FlagTypeDecimalPlaces#</cfoutput>
		</td>
	</tr>
	</cfif>
	<tr>
		<td valign="top">
			<span class="required">Profile Name or Text:</span>
		</td>
		<td colspan="2" valign="top">
			<CF_INPUT type="text" name="frmName" value="#frmName#" size="40" maxlength="250" onBlur="javascript:setTextID()" required="true">
		</td>
	</tr>

	<tr>
		<td >Translate Name</td>
		<td  colspan="2" valign="TOP">
			<CFIF frmtask IS "add">
				<input type="checkbox" name="frmTranslateLabel" value="1" CHECKED> Create Default Translation
			<CFelseif FRMnamePhraseTextID is "">
				<input type="checkbox" name="frmTranslateLabel" value="1" > Create Default Translation
			<cfelse>
				<CF_INPUT type="hidden" name="frmnamePhraseTextID" value="#frmNamephraseTextID#" CHECKED>
				<CF_translate showtranslationlink="true" processEvenIfNested="true" translationLinkCharacter="Translate" trimphrase="20"><cfoutput>phr_#htmleditformat(frmNamePhraseTextID)#</cfoutput> </CF_translate>
			</cfif>
		</td>
	</tr>

	<tr>
		<td valign="top">
			<span class="required">Unique Text ID:</span>
		</td>
		<td colspan="2" valign="top">
		<cfif trim(frmflaggrouptextid) is not "">
			<cfoutput>#HTMLEditFormat(Trim(frmFlagGroupTextID))#</cfoutput><br>
			<CF_INPUT type="HIDDEN" name="frmFlagGroupTextID" value="#frmFlagGroupTextID#">
		<cfelse>
		<CF_INPUT type="TEXT" name="frmFlagGroupTextID" value="#frmFlagGroupTextID#" SIZE="28" MAXLENGTH="50" required="true" onKeyPress="return restrictKeyStrokes(event,keybCFVariableNM)" onChange="javascript:checkFlagGroupTextID(this.value,'#frmentitytypeID#')"><br>
			(not required, but once set cannot be changed)<br>
		</cfif>
			<INPUT TYPE="HIDDEN" NAME="FlagGroupTextIDUnique" VALUE="1">			<!--- updated by ajax and then checked on submit --->
		</td>
	</tr>

	<tr>
		<td valign="top">
			Description:
		</td>
		<td colspan="2" valign="top">
			<CF_relayFormElement label = "Description" relayFormElementType="textArea" fieldname = "frmDescription" currentvalue = #frmDescription# size="40" maxLength="160">
		</td>
	</tr>

	<tr>
		<td valign="top">
			Help Text:
		</td>
		<td colspan="2" valign="top">
			<CF_relayFormElement label = "Help Text" relayFormElementType="textArea" fieldname = "frmHelpText" currentvalue = #frmHelpText# size="40" maxLength="160">
		</td>
	</tr>


	<!---
	DAM will be reintroduced soon - needs to be a popup that allows a search for the desired organisation

	<TR>
		<TD valign=top>
			Associated Organisation :
		</TD>
		<TD valign=top>
			<CFIF frmtask IS NOT "Add" OR AssocOrg IS "No">
				<SELECT NAME="frmEntityMemberID">
					<OPTION value="0">None
					<CFOUTPUT QUERY="getAllOrgs">
						<OPTION value="#organisationid#">#OrganisationName#
					</CFOUTPUT>
				</SELECT>

			<CFELSEIF AssocOrg IS "None">
				None
			<CFELSE>
				<CFOUTPUT>#getAssociatedOrg.Organisationname# <I>(#getAssociatedOrg.OrganisationID#)</I></CFOUTPUT>
			</CFIF>
		</TD>
	</TR> --->
	<INPUT TYPE="Hidden" NAME="frmEntityMemberID" VALUE="0">
	<tr>
		<td valign="top">
			Associate with a specific organisation:
		</td>
		<td colspan="2" valign="top">
			Yes : <input type="radio" name="frmassocLive" value="1" <CFIF frmassocLive EQ 1>CHECKED</CFIF>>
			No : <input type="radio" name="frmassocLive" value="0" <CFIF frmassocLive EQ 0>CHECKED</CFIF>>
		</td>
	</tr>

	<tr>
		<td valign="top">
			Comments :
		</td>
		<td colspan="2" valign="top">
			<CF_INPUT type="TEXT" name="frmNotes" value="#frmNotes#" SIZE="50" MAXLENGTH="50">
		</td>
	</tr>
	<tr>
		<td valign="top">
			Ordering :
		</td>
		<td colspan="2" valign="top">
			<select name="frmOrder"><cfloop index="LoopCount" from="1" to="20">
				<cfoutput>
				<cfif loopcount is frmorder>
					<option value=#loopcount# selected> #htmleditformat(LoopCount)#
				<cfelse>
					<option value=#loopcount#> #htmleditformat(LoopCount)#
				</cfif>
				</cfoutput>
			</cfloop></select>
			<br>Profiles with the same Ordering Index are sorted alphabetically.
		</td>
	</tr>
</table>
<br>
<table width="80%" border="0" cellspacing="2" cellpadding="3" align="center" bgcolor="White">
	<tr>
		<td colspan=4><b>
			Display Details
			<br><br><img src="/images/misc/rule.gif" WIDTH=100% HEIGHT=3 ALT="" BORDER="0"><br><br></b>
		</td>
	</tr>
	<tr>
		<td valign=top>
			Country Scope:
		</td>
		<td valign=top colspan=3>
			<cfif frmtask is "Add">
				<select name="frmScope">
					<option value="0"<cfoutput>#IIF(frmScope IS "0", DE(" SELECTED"), DE(""))#</cfoutput>> All Countries/Groups
					<cfoutput query="getCountryGroups">
						<option value="#CountryID#"#iif(frmscope is countryid, de(" SELECTED"), de(""))#> #HTMLEditFormat(CountryGroup)#
					</cfoutput>
						<option value="0">-----------------------------------------
					<cfoutput query="getCountries">
						<option value="#CountryID#"#iif(frmscope is countryid, de(" SELECTED"), de(""))#> #HTMLEditFormat(CountryDescription)#
					</cfoutput>
				</select><br><br>
			<cfelse>
				<cfoutput>
				<CF_INPUT type="HIDDEN" name="frmScope" value="#frmScope#">
				<cfif frmscope is not 0>
					#htmleditformat(getFlagGroup.scope)#
				<cfelse>
					All Countries/Groups
				</cfif>
				</cfoutput>
			</cfif>
		</td>
	</tr>
<!--- 	<tr>
		<td valign="TOP" colspan=3>
			Specify usage of this profile's attributes for specified users/user groups
		</td>
	</tr>
	<tr>
		<td valign=top>
			&nbsp;&nbsp;&nbsp; <input type="Checkbox" name="frmViewingAccessRights" value="1" <cfoutput>#IIF(frmViewingAccessRights IS NOT 0, DE("CHECKED"), DE(""))#</cfoutput>> Only specified users can View
		</td>
		<td valign=top>
			<input type="Checkbox" name="frmEditAccessRights" value="1" <cfoutput>#IIF(frmEditAccessRights IS NOT 0, DE("CHECKED"), DE(""))#</cfoutput>> Only specified users can Edit
		</td>
	</tr>
	<tr>
		<td valign=top>
			&nbsp;&nbsp;&nbsp; <input type="Checkbox" name="frmDownloadAccessRights" value="1" <cfoutput>#IIF(frmDownloadAccessRights IS NOT 0, DE("CHECKED"), DE(""))#</cfoutput>> Only specified users can Download
		</td>
		<td valign=top>
			<input type="Checkbox" name="frmSearchAccessRights" value="1" <cfoutput>#IIF(frmSearchAccessRights IS NOT 0, DE("CHECKED"), DE(""))#</cfoutput>> Only specified users can use in Selections
		</td>
	</tr>
	<tr>
		<td valign=top colspan=3>
			Specify usage of this profile's attributes for all users
		</td>
	</tr>
	<tr>
		<td valign=top>
			&nbsp;&nbsp;&nbsp; <input type="CHECKBOX" name="frmView" value="1" <cfoutput>#IIF(frmView IS NOT 0, DE("CHECKED"), DE(""))#</cfoutput>> All internal users can View
		</td>
		<td valign=top>
			<input type="CHECKBOX" name="frmEdit" value="1" <cfoutput>#IIF(frmEdit IS NOT 0, DE("CHECKED"), DE(""))#</cfoutput>> All internal users can Edit
		</td>
	</tr>
	<tr>
		<td valign=top>
			&nbsp;&nbsp;&nbsp; <input type="CHECKBOX" name="frmDownload" value="1" <cfoutput>#IIF(frmDownload IS NOT 0, DE("CHECKED"), DE(""))#</cfoutput>> All internal users can Download<br><br>
		</td>
		<td valign=top>
			<input type="CHECKBOX" name="frmSearch" value="1" <cfoutput>#IIF(frmSearch IS NOT 0, DE("CHECKED"), DE(""))#</cfoutput>> All internal users can use in Selections<br><br>
		</td>
	</tr>
 --->

	<cfoutput >

		<tr>
			<td>Specify usage of this profile's attributes </td>
			<th>All users</th>
			<th>Only Specified <br>Users/Groups</th>
			<th>None</th>
		</tr>
		<tr>
			<td align="right">View this profile</td>
			<td align="center"><input type="radio" name="frmViewingRights" value="all" <cfif frmview is 1>checked</cfif>></td>
			<td align="center"><input type="radio" name="frmViewingRights" value="userGroup" <cfif frmviewingaccessrights is 1>checked</cfif>></td>
			<td align="center"><input type="radio" name="frmViewingRights" value="none" <cfif frmviewingaccessrights is not 1 and frmview is not 1>checked</cfif>></td>
		</tr>
		<tr>
			<td align="right">Edit this profile</td>
			<td align="center"><input type="radio" name="frmEditRights" value="all" <cfif frmEdit is 1>checked</cfif>></td>
			<td align="center"><input type="radio" name="frmEditRights" value="usergroup" <cfif frmEditaccessrights is 1>checked</cfif>></td>
			<td align="center"><input type="radio" name="frmEditRights" value="none" <cfif frmEditaccessrights is not 1 and frmEdit is not 1>checked</cfif>></td>
		</tr>
		<tr>
			<td align="right">Make selections based on this profile</td>
			<td align="center"><input type="radio" name="frmSearchRights" value="all" <cfif frmSearch is 1>checked</cfif>></td>
			<td align="center"><input type="radio" name="frmSearchRights" value="usergroup" <cfif frmSearchaccessrights is 1>checked</cfif>></td>
			<td align="center"><input type="radio" name="frmSearchRights" value="none" <cfif frmSearchaccessrights is not 1 and frmSearch is not 1>checked</cfif>></td>
		</tr>
		<tr>
			<td align="right">Download this selection</td>
			<td align="center"><input type="radio" name="frmDownloadRights" value="all" <cfif frmDownload is 1>checked</cfif>></td>
			<td align="center"><input type="radio" name="frmDownloadRights" value="usergroup" <cfif frmDownloadaccessrights is 1>checked</cfif>></td>
			<td align="center"><input type="radio" name="frmDownloadRights" value="none" <cfif frmDownloadaccessrights is not 1 and frmDownload is not 1>checked</cfif>></td>
		</tr>
	</cfoutput>

<tr>
		<td align="right">Specify who has rights to update this profile definition
		(The creator and administrator always have this right)
		<BR></td>
		<td colspan="3" valign="top" nowrap>
				<!--- levels 1 view, 2 edit, 3 search, 4 download, 5 updateprofile --->
				<cfif frmTask is "edit">
					<cfset tempentityID = frmFlagGroupID>
				<cfelse>
					<cfset tempentityID = "frmFlagGroupID">
				</cfif>
				<CF_UGRecordManager entity="flagGroup" entityid="#tempentityID#" Form="FlagGroupForm" level = "5" >
		</td>
</tr>
	<tr>
		<td valign="TOP">
			Show the profile attributes in Advanced Search
		</td>
		<td colspan="3" valign="top" nowrap>
			<CF_INPUT type="RADIO" name="frmUseInSearchScreen" value="0"  CHECKED="#iif(frmUseInSearchScreen IS 0,true,false)#"> No &nbsp;&nbsp;
			<CF_INPUT type="RADIO" name="frmUseInSearchScreen" value="1"  CHECKED="#iif(frmUseInSearchScreen IS NOT 0,true,false)#"> Yes <br>
		</td>
	</tr>

	<cfif frmtask neq "Add">
		<cfif ScreensContainingFlagGroup.recordcount is not 0>
		<tr>
			<td valign="TOP">
				This Profile appears in the following Screens:
			</td>
			<td colspan="3" valign="top" nowrap>
				<cfoutput query="ScreensContainingFlagGroup">
					#htmleditformat(screenName)# (#htmleditformat(screenid)#)<br>
				</cfoutput>
			</td>
		</tr>
		</cfif>
		<cfif ScreensContainingFlagsInFlagGroup.recordcount is not 0>
		<tr>
			<td valign="TOP">
				Attributes of this Profile appear in the following Screens:
			</td>
			<td colspan="3" valign="top" nowrap>
				<table>
					<cfoutput query="ScreensContainingFlagsInFlagGroup" group="flagName">
						<tr>
							<TD VALIGN="TOP" COLSPAN=2>#htmleditformat(FLAGnAME)#</TD>
						</TR>
						<TR><TD>&nbsp;&nbsp;</TD>
							<TD>

								<CFOUTPUT>
								#htmleditformat(screenName)# (#htmleditformat(screenid)#)<br>
								</CFOUTPUT>
							</TD>
						</TR>
					</cfoutput>
				</table>

			</td>
		</tr>
		</cfif>

	</cfif>
	<!--- <tr>
		<td valign=top>
			Access expires from this date
		</td>
		<td valign=top colspan=3>
			<CF_relayDatefield fieldname="frmExpiry" currentValue="#DateFormat(frmExpiry,'dd-mmm-yyyy')#">
			<!--- <CF_INPUT type="TEXT" name="frmExpiry" value="#frmExpiry#" SIZE=11 MAXLENGTH=11> (dd-mmm-yyyy)<br> --->
		</td>
	</tr> --->
	<tr>
		<td valign=top>
			Active :
		</td>
		<td colspan="3" valign="top" nowrap>
			<CF_INPUT type="RADIO" name="frmActive" value="0"  CHECKED="#iif(frmActive IS 0,true,false)#"> No &nbsp;&nbsp;
			<CF_INPUT type="RADIO" name="frmActive" value="1"  CHECKED="#iif(frmActive IS NOT 0,true,false)#"> Yes <br><br>
		</td>
	</tr>

	<TR>
		<td valign="top" class="label">Formatting Parameters</td>
		<TD>
			<textarea  NAME="frmFormattingParameters" rows="3" cols=40 ><CFOUTPUT>#frmFormattingParameters#</CFOUTPUT></textarea>


		</TD>
	</TR>

	<tr>
		<td colspan="4" align="centre"><cfoutput><img src="/images/misc/rule.gif" alt="" width="590" height="3" border="0"></cfoutput></td>
	</tr>

	<tr>
		<td colspan="4" align="center">
			<cfoutput>Creator can always view, search, edit and download this profile regardless of rights settings</cfoutput>
		</td>
	</tr>


	<tr>
		<td colspan="4" align="center">
			<cfif frmtask is "edit">
				** Entity type, parent, profile type and scope cannot be changed after creation.
				You may delete the profile completely from the previous screen.
			<cfelse>
				Only profiles which you have edit and scope rights to, are not children
				and which do not have flags	themselves may be selected as a parent.
			</cfif>
		</td>
	</tr>

</cf_relayFormDisplay>


</form>
<CFOUTPUT>
	<form method="POST" action="/profileManager/#frmBack#.cfm" NAME="BackForm">
		<input type="HIDDEN" name="null" value="0">
		<!--- NYB 2008-10-14 - replaced 'frmtask is "edit"' with 'frmtask neq "add" and frmtask is "delete"' --->
		<cfif frmtask neq "add" and frmtask neq "delete">
			<CF_INPUT type="HIDDEN" name="frmFlagGroupID" value="#frmFlagGroupID#">
		</cfif>
	</form>
</cfoutput>
<p>


</center>