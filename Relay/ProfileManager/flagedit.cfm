<!--- �Relayware. All Rights Reserved 2014 --->
<!--- the application .cfm checks for read adminTask privleges --->

<!--- Amendment History
2004-06-09			WAB Added linking to communication types for unsubscribe.  Altered javascript so that saves properly when using editflag()
2000-10-13			WAB			removed Javascript function to prefill flagTextID because it creates invalid ones with spaces, odd characters etc. needs tobe modified and replaced
12-Jul-2000			SWJ			Added Javascript function to prefill flagTextID
2000-02-25		WAB		Added code to set use of valid values and lookup
1999-02-20		WAB		Add Entering of FlagTextID
2005-09-14	WAB		Added item to edit "links to entityTypeID"
2005-10-17	WAB 	implemented function for testing whether user has rights to edit flag/flaggroup definition
2008-04-15	WAB		deal with protected field
2009-04-29   WAB    add parameter for mergeProtect.  Add nicer handling of parameters
					converted to use relayformelements
2009-05-06    WAB 	Added Qtips support
2009-05-11   WAB    actually removed parameter mergeProtect but all other changes stand
2011-06-14	PPB		REL106 - add showInConditionalEmails
2012-09-21	PPB		Case 430402 add translate="true" to restrictKeyStrokes.js call
2015/01/23	NJH		Connector work - removed the hard-coded list of ID that showed in the linkToEntitytype dropdown.
2015/09/08  NJH 	Jira PROD2015-30 - enable flags/groups to be displayed/hidden based on flag being selected. Changed 'requiresValueInOtherFlag' to support a list of flagIds. Also made flagTextID's required and tidied up the function that checks the uniqueness of the flagTextID.
2015/09/24  NJH		Jira PROD2015-84 - Also provide support for required boolean flag groups when a flag value has been set.
2015/09/29	NJH		Resurrect the setTextID functionality
2015/10/20	PYW		P-TAT006 BRD 32. Increase formatting parameters max length to 1000 characters
2017-02-28	WAB		PROD2016-3495 Autogeneration of flagtextids for boolean flags did not work correctly for first flag (supposed to prepend with flagGroupTextID)
--->

<cfparam name="frmFlagID" type="numeric" default="0">
<cfparam name="frmBack" default="">
<cfparam name="pagesBack" type="numeric" default="0">
<cfset pagesBack = pagesBack+1>


<cf_includejavascriptonce template = "/javascript/restrictkeystrokes.js" translate="true">		<!--- 2012-09-21 PPB Case 430402 add translate="true" --->

<cf_includejavascriptonce template = "/javascript/qtip.js">


<!--- Check if this user has Edit rights for this group as well (as view) --->
<!---
WAB removed this query and replaced with a function.
This query looked at the EditAccessRights, but editAccessRights deals with rights to edit the data not the right to edit the "definition" so it was all wrong really

<CFQUERY NAME="checkEdit" DATASOURCE="#application.SiteDataSource#">
	SELECT 1
	FROM FlagGroup
	WHERE FlagGroupID = #frmFlagGroupID#
	AND (CreatedBy=#request.relayCurrentUser.usergroupid#
		OR (EditAccessRights = 0 AND Edit <> 0)
		OR (EXISTS (SELECT 1 FROM FlagGroupRights AS t WHERE EditAccessRights <> 0
                AND FlagGroupID = t.FlagGroupID
                AND t.UserID=#request.relayCurrentUser.personid#
                AND t.Edit <> 0)))
</CFQUERY>

<CFIF checkEdit.RecordCount IS 0>
 --->


<CFIF frmtask IS "add">
	<CFIF not application.com.flag.doesCurrentUserHaveRightsToEditFlagGroupDefinition(frmFlagGroupID)   >
		<CFSET message = "You do not appear to have sufficient rights to access this Flag Group information. <P>Please contact support if you need further information.">
		<!--- <cfinclude template="/profileManager/profileList.cfm"> --->
		<cfoutput>#application.com.relayUI.message(message=message,type="warning")#</cfoutput>
		<CF_ABORT>
	</CFIF>

	<CFSET frmName = "">
	<CFSET frmHelpText = "">
	<CFSET frmDescription = "">
	<CFSET frmFlagTextID = "">
	<CFSET frmNamePhraseTextID = "">
	<CFSET frmOrder = 1>
	<CFSET frmScore = 0>
	<CFSET frmActive = 1>
	<CFSET frmlookup = 0>
	<CFSET frmuseValidValues = 0>
	<CFSET frmProtected = 0>
	<CFSET frmLinksToEntityTypeID = "">
	<CFSET frmWddxStruct = "">
	<CFSET frmFormattingParameters = "">
	<CFSET frmShowInConditionalEmails = 0>

	<CFQUERY NAME="getFlagGroup" DATASOURCE="#application.SiteDataSource#">
	SELECT max (f.OrderingIndex) AS order_max, flagtypeid, fg.name, fg.description, fet.tablename,fg.helpText, fg.flagGroupTextID
	FROM
		FlagGroup as fg
			left join
		Flag as f on f.FlagGroupID = fg.FlagGroupID
			inner join
		FlagEntityType fet on fet.entityTypeID = fg.entityTypeID
	WHERE fg.FlagGroupID =  <cf_queryparam value="#frmFlagGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >
	group by flagtypeid, fg.name, fg.description, fet.tablename,fg.helpText, fg.flagGroupTextID
	</CFQUERY>

	<CFSET frmflagTypeID = getFlagGroup.FlagTypeID>
	<CFSET FlagGroupName = getFlagGroup.Name>
	<CFSET FlagGroupHelpText = getFlagGroup.helpText>
	<CFSET FlagGroupDescription = getFlagGroup.Description>
	<CFSET FlagEntityType = getFlagGroup.tablename>
	<CFIF getFlagGroup.order_max IS NOT "" AND getFlagGroup.order_max LT 99>
		<CFSET frmOrder = getFlagGroup.order_max + 1>
	</CFIF>

<CFELSE>
	<CFIF not application.com.flag.doesCurrentUserHaveRightsToEditFlagDefinition(frmFlagID)   >
		<CFSET message = "You do not appear to have sufficient rights to access this Flag Group information. <P>Please contact support if you need further information.">
		<!--- <cfinclude template="/profileManager/profileList.cfm"> --->
		<cfoutput>#application.com.relayUI.message(message=message,type="warning")#</cfoutput>
		<CF_ABORT>
	</CFIF>

	<CFQUERY NAME="getFlag" DATASOURCE="#application.SiteDataSource#">
		SELECT f.Name, f.Description, f.FlagTextID, f.NamePhraseTextID, f.OrderingIndex,
		f.Active, fg.flagTypeID, f.usevalidValues, f.protected, f.linkstoentityTypeID,f.helpText,
		f.lookup, f.WDDXStruct, f.score, f.ShowInConditionalEmails, fg.name as flagGroupName, fg.helpText as flagGroupHelpText, f.FormattingParameters, fet.tablename
		FROM
			FlagGroup as fg
				inner join
			Flag as f on f.flaggroupid = fg.flaggroupid
				inner join
			FlagEntityType fet on fet.entityTypeID = fg.entityTypeID

		WHERE f.FlagID =  <cf_queryparam value="#frmFlagID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</CFQUERY>

	<CFSET frmName = getFlag.Name>
	<CFSET frmHelpText = getFlag.helpText>
	<CFSET frmDescription = getFlag.Description>
	<CFSET frmFlagTextID = getFlag.FlagTextID>
	<CFSET frmNamePhraseTextID = getFlag.NamePhraseTextID>
	<CFSET frmOrder = getFlag.OrderingIndex>
	<CFSET frmScore = getFlag.Score>
	<CFSET frmActive = getFlag.Active>
	<CFSET frmuseValidValues = getFlag.useValidValues>
	<CFSET frmProtected = getFlag.protected>
	<CFSET frmLookup = getFlag.lookup>
	<CFSET frmflagTypeID = getFlag.FlagTypeID>
	<CFSET frmLinksToEntityTypeID = getFlag.LinksToEntityTypeID>
	<CFSET frmWDDXStruct = getFlag.WDDXStruct>
	<CFSET frmFormattingParameters = getFlag.FormattingParameters>
	<CFSET FlagGroupName = getFlag.FlagGroupName>
	<CFSET flagGroupHelpText = getFlag.flagGroupHelpText>
	<CFSET FlagEntityType = getFlag.tablename>
	<CFSET frmShowInConditionalEmails = getFlag.ShowInConditionalEmails>
</CFIF>

<!--- get other flags in this group --->
<CFQUERY NAME="getFlags" DATASOURCE="#application.SiteDataSource#">
	SELECT f.Name, f.OrderingIndex, f.flagID, fg.name as groupname, fg.description as groupdescription, flaggrouptextid
	FROM Flag as f, flaggroup as fg
	WHERE f.flaggroupid = fg.flaggroupid
	and Fg.FlagGroupID =  <cf_queryparam value="#frmFlagGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >
	ORDER BY f.OrderingIndex, f.Name
</CFQUERY>

	<cfset ScreensContainingFlag = application.com.screens.getScreensContainingFlag(frmflagID)>

<!--- some code for linking Unsubscribe flags to the communication types --->
<CFIF getFlags.groupname & getflags.groupdescription & getflags.flaggrouptextid contains "unsubscribe" or frmFlagTextID contains "NoComms">
	<CFSET showUnsubscribeLink = true>
	<!--- get list of communication types --->
	<CFQUERY NAME="getCommTypes" DATASOURCE="#application.SiteDataSource#">
	select 'NoCommsType'+ltrim(str(lookupid))  as TextID , ItemText, 3 as sortorder from lookuplist where fieldname = 'commtypelid' and isparent = 0 and islive = 1
	union select 'NoCommsTypeALL' , 'All Communication Types' ,1
	union select 'NoCommsTypeOther' , 'Communication Types Without Individual Unsubscribe Attribute' ,2
	order by sortorder,itemtext
	</cfquery>
<cfelse>
	<CFSET showUnsubscribeLink = false>
</CFIF>

	<cfset formattingParametersStructure = application.com.structureFunctions.convertNameValuePairStringToStructure(inputString = frmFormattingParameters,delimiter = ",", dotnotationtoStructure = false)>

<!--- function used to go through the formatting parameters structure, deleting as we go so that at the end we have left any items not dealt with --->
<cffunction name="getFormattingParameterValue">
	<cfargument name="key">
		<cfreturn application.com.structureFunctions.structPop(formattingParametersStructure,key)>
</cffunction>




<cf_head>
<SCRIPT type="text/javascript">

<!--

       function verifyForm(task,next) {

                var form = document.FlagForm;
                form.frmTask.value = task;
                form.frmNext.value = next;
                var msg = "";
                if (form.frmName.value == "" || form.frmName.value == " ") {
                        msg += "* Name\n";
                }

                if (form.FlagTextIDUnique.value  == 0 || form.frmFlagTextID.value.trim() == '') {
                        msg += "* A Unique Text Identifier\n";
                }

                if (msg != "") {
                        alert("\nYou must provide the following information for this flag:\n\n" + msg);
                } else {
                	form.submit();
				}
        }

		function flagEdit(flagID) {
                var form = document.FlagForm;
                form.frmTask.value = "edit";
					form.frmNext.value = 'flagedit'
				form.frmNextFlagID.value = flagID;
                var msg = "";
                if (form.frmName.value == "" || form.frmName.value == " ") {
                        msg += "* Name\n";
                }
                if (msg != "") {
                       // alert("\nYou must provide the following information for this flag:\n\n" + msg);
                   	form.submit();
                } else {
                	form.submit();
				}
        }

        function validValues(fieldName) {
				newWin = window.open ('/dataTools/validvalues.cfm?frmFieldName='+fieldName+'&frmCountryID=0','ValidValues','height=500,width=700,scrollbars=1,resizable=1')
				newWin.focus()
		}


        function translate(flagTextID) {
				newWin = window.open ('/translation/editPhrase.cfm?frmFieldName='+flagTextID+'&frmCountryID=0','translate','height=500,width=700,scrollbars=1,resizable=1')
				newWin.focus()
		}

		<!--- if flag is a boolean flag, then prepend the flagGroupTextID to the flag name --->
	   function setTextID(flagGroupTextID){
	   		var form = document.FlagForm;
	   		var theFlagGroupTextID = flagGroupTextID+'_';
	   		if (form.frmFlagTypeID.value != 2 && form.frmFlagTypeID.value != 3) {
	   			theFlagGroupTextID = '';
	   		}
			if (form.frmFlagTextID.value == "") {
				form.frmFlagTextID.value = theFlagGroupTextID + form.frmName.value.replace(/\W/g, '');
				jQuery('#frmFlagTextID').change();
			}
	   }



	function checkFlagTextID(value,entityType) {
		var $_flagTypeID = jQuery('#frmFlagTypeID').val();

		jQuery.ajax({
			type: "GET",
			url: "/webservices/callWebService.cfc?",
			data: {method:'callWebService',webServiceName:'misc',methodName:'isFlagTextIDUnique', flagTextID:value, returnFormat:'json',flagTypeID:$_flagTypeID, entityType:entityType},
			dataType: "json",
			cache: false,
			async: false,
			success: function(data,textStatus,jqXHR) {
				var $_uniqueErrorMsgDiv = jQuery('#notUniqueMessage');
				var $_uniqueMsgDiv = jQuery('#uniqueMessage');
				if (data === false) {
					if ($_uniqueMsgDiv.length == 1) {
						$_uniqueMsgDiv.remove();
					}
					if ($_uniqueErrorMsgDiv.length == 0) {
						var message = 'Text ID is not unique';
						if ($_flagTypeID == 2 || $_flagTypeID == 3) {
							message += ' or Text ID is the same as the Profile Text ID';
						}
						jQuery('#frmFlagTextID').after('<div id="notUniqueMessage" class="errorblock">'+message+'</div>')
					}
					document.FlagForm.FlagTextIDUnique.value = 0;
				} else {
					if ($_uniqueErrorMsgDiv.length == 1) {
						$_uniqueErrorMsgDiv.remove();
					}
					if ($_uniqueMsgDiv.length == 0) {
						jQuery('#frmFlagTextID').after('<div id="uniqueMessage" class="successblock">Text ID unique</div>')
					}
					document.FlagForm.FlagTextIDUnique.value = 1;
				}
			}
		})
	}

//-->

</SCRIPT>
</cf_head>



<CFSET ThisPageName="flagEdit.cfm">
<cfinclude template="/profileManager/profileTopHead.cfm">


<cfset request.relayFormDisplayStyle = "HTML-table">
<cfset helpPosition = "right">

<FORM METHOD="POST" ACTION="/profileManager/flagUpdateTask.cfm" NAME="FlagForm" enctype="multipart/form-data">
<CF_relayFormDisplay id = "flagEditTable">

<CFOUTPUT>
<!--- <TABLE id="flagEditTable" WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" CLASS="withBorder"> --->
	<CF_INPUT TYPE="HIDDEN" NAME="frmDate" VALUE="#CreateODBCDateTime(now())#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmFlagGroupID" VALUE="#frmFlagGroupID#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmFlagTypeID" VALUE="#frmflagTypeID#">

<!---
	WAB - what is the point of these!
 	<INPUT TYPE="HIDDEN" NAME="flagGroupName" VALUE="#flagGroupName#">
	<INPUT TYPE="HIDDEN" NAME="flagGroupDescription" VALUE="#flagGroupDescription#">
	<INPUT TYPE="HIDDEN" NAME="flagGroupScEn" VALUE="#flagGroupScEn#">
 --->
	<CF_INPUT TYPE="HIDDEN" NAME="frmFlagID" VALUE="#frmFlagID#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmNextFlagID" VALUE="#frmFlagID#">
	<INPUT TYPE="HIDDEN" NAME="frmTask" VALUE="">
	<INPUT TYPE="HIDDEN" NAME="frmNext" VALUE="">
	<CF_INPUT type="hidden" name="pagesBack" value="#pagesBack#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmBack" VALUE="#frmBack#">
</CFOUTPUT>

<CFOUTPUT>
	<cfif isdefined('message') and message neq "">
		<tr>
			<td colspan="2">
				#application.com.relayUI.message(message=message)#
				<!--- <span class="messagetext">#application.com.security.sanitiseHTML(message)#</span> --->
			</td>
		</tr>
	</cfif>

	<TR>
		<TD COLSPAN=2>
			<CFIF frmtask IS "add">
				<strong>ADD NEW PROFILE ATTRIBUTES to '#UCase(HTMLEditFormat(Trim(flagGroupName)))#'</strong>
			<CFELSE>
			 	<strong>PROFILE ATTRIBUTES FOR '#UCase(HTMLEditFormat(Trim(flagGroupName)))#'</strong>
			</CFIF>
		</TD>
	</TR>

	<CF_relayFormElementDisplay class="form-control" label = "Attribute Name" relayFormElementType="text" required="true" fieldname = "frmName" currentvalue = #frmName# size="50" maxLength="250"	onBlur="javascript:setTextID('#frmtask eq "add"?getFlagGroup.flagGroupTextID:getFlags.flagGroupTextID#');" noteText = "#frmFlagID is not 0?frmFlagID:''#" noteTextPosition="Right" noteTextImage ="false" >

<!--- 	<TR>
		<td valign="top" class="label">Attribute Name</td>
		<TD>
			<INPUT TYPE="TEXT" NAME="frmName" VALUE="<CFOUTPUT>#frmName#</CFOUTPUT>" SIZE="50" MAXLENGTH="250" onBlur="javascript:setTextID()"><cfif frmFlagID is not 0>(#frmFlagID#)</cfif>


		</TD>
	</TR>
 --->

	<CF_relayFormElementDisplay label = "Translate Name" relayFormElementType="html">
			<CFIF frmtask IS "add">
				<input type="checkbox" name="frmTranslateLabel" value="1" CHECKED> Create Default Translation
			<CFelseif FRMnamePhraseTextID is "">
				<input type="checkbox" name="frmTranslateLabel" value="1" > Create Default Translation
			<cfelse>
				<CF_INPUT type="hidden" name="frmnamePhraseTextID" value="#frmNamephraseTextID#" CHECKED>
				<CF_translate showtranslationlink="true" processEvenIfNested="true" translationLinkCharacter="Translate" trimphrase="20"><cfoutput>phr_#htmleditformat(frmNamePhraseTextID)#</cfoutput> </CF_translate>
			</cfif>
	</CF_relayFormElementDisplay >

<!--- 	<tr>
		<td class="Label">Translate Name</td>
		<td  colspan="2" valign="TOP">
			<CFIF frmtask IS "add">
				<input type="checkbox" name="frmTranslateLabel" value="1" CHECKED> Create Default Translation
			<CFelseif FRMnamePhraseTextID is "">
				<input type="checkbox" name="frmTranslateLabel" value="1" > Create Default Translation
			<cfelse>
				<input type="hidden" name="frmnamePhraseTextID" value="#frmNamephraseTextID#" CHECKED>
				<CF_translate showtranslationlink="true" processEvenIfNested="true" translationLinkCharacter="Translate" trimphrase="20"><cfoutput>phr_#frmNamePhraseTextID#</cfoutput> </CF_translate>
			</cfif>
		</td>
	</tr>
 --->
<!--- 	<TR>
		<td valign="top" class="label">Attribute Description (on screen)</td>
		<TD>
			<INPUT TYPE="TEXT" NAME="frmDescription" VALUE="#frmDescription#" SIZE="50" MAXLENGTH="60"><BR><BR>
		</TD>
	</TR>
 --->

 </CFOUTPUT>

<cfif not showUnsubscribeLink >
	<CF_relayFormElementDisplay label = "Unique Text ID" relayFormElementType="html" required="true">
		<cfoutput>
		<CFIF Trim(frmFlagTextID) IS NOT "">
			#HTMLEditFormat(Trim(frmFlagTextID))#
			<CF_INPUT TYPE="HIDDEN" NAME="frmFlagTextID" VALUE="#frmFlagTextID#">
		<CFELSE>
			<CF_INPUT TYPE="TEXT" NAME="frmFlagTextID" VALUE="#frmFlagTextID#" SIZE="30" required="true" MAXLENGTH="50" onKeyPress="return restrictKeyStrokes(event,keybCFVariableNM)" onChange="javascript:checkFlagTextID(this.value,'#FlagEntityType#')"><BR>
			<!--- (not required, but once set cannot be changed)<BR> --->
			(once set cannot be changed)<BR> <!--- NJH 2006-09-11 --->
		</CFIF>
			<INPUT TYPE="HIDDEN" NAME="FlagTextIDUnique" VALUE="1">			<!--- updated by ajax and then checked on submit --->
		</cfoutput>
	</CF_relayFormElementDisplay >


<!--- 	<cfoutput>
	<TR>
		<td valign="top" class="label">Unique Text ID</TD>
		<TD>
		<CFIF Trim(frmFlagTextID) IS NOT "">
			#HTMLEditFormat(Trim(frmFlagTextID))#
			<INPUT TYPE="HIDDEN" NAME="frmFlagTextID" VALUE="#frmFlagTextID#">
		<CFELSE>
			<INPUT TYPE="TEXT" NAME="frmFlagTextID" VALUE="#frmFlagTextID#" SIZE="30" MAXLENGTH="30" onKeyPress="return restrictKeyStrokes(event,keybCFVariableNM)" onChange="javascript:checkFlagTextID(this.value)"><BR>
			<!--- (not required, but once set cannot be changed)<BR> --->
			(once set cannot be changed)<BR> <!--- NJH 2006-09-11 --->
		</CFIF>
			<INPUT TYPE="HIDDEN" NAME="FlagTextIDUnique" VALUE="1">			<!--- updated by ajax and then checked on submit --->
		</TD>
	</TR>

	</cfoutput>
 --->
 <cfelse>
	<CF_relayFormElementDisplay label = "Unsubscribe From" relayFormElementType="html">
			<cf_displayValidValues
				validValues  = "#getCommTypes#"
				formFieldName =  "frmFlagTextID"
				currentValue = "#frmFlagTextID#"
				displayValueColumn = "ItemText"
				datavaluecolumn = "TextID"
				allowcurrentvalue = true
				nullText = "Choose Communication Type"
				>

			<BR>If this attribute is to be used to allow the individual to unsubscribe from a type of communication then select the type of communication it is linked to<BR><BR>
			<INPUT TYPE="HIDDEN" NAME="FlagTextIDUnique" VALUE="1">			<!--- updated by ajax and then checked on submit --->
	</CF_relayFormElementDisplay >

<!--- 	<TR>
		<td valign="top" class="label">Unsubscribe From</TD>
		<TD>
			<cf_displayValidValues
				validValues  = "#getCommTypes#"
				formFieldName =  "frmFlagTextID"
				currentValue = "#frmFlagTextID#"
				displayValueColumn = "ItemText"
				datavaluecolumn = "TextID"
				allowcurrentvalue = true
				nullText = "Choose Communication Type"
				>

			<BR>If this attribute is to be used to allow the individual to unsubscribe from a type of communication then select the type of communication it is linked to<BR><BR>
			<INPUT TYPE="HIDDEN" NAME="FlagTextIDUnique" VALUE="1">			<!--- updated by ajax and then checked on submit --->
		</TD>
	</TR>
 --->
 </cfif>

	<CF_relayFormElementDisplay label = "Description" relayFormElementType="textArea" fieldname = "frmDescription" currentvalue = #frmDescription# size="50" maxLength="160"	>

	<CF_relayFormElementDisplay label = "Help Text" relayFormElementType="textArea" fieldname = "frmHelpText" currentvalue = #frmHelpText# size="50" maxLength="160"	>

	<CF_relayFormElementDisplay label = "Ordering" relayFormElementType="html" 	noteText = "Attributes with the same Ordering Index are sorted alphabetically, so for items like categories, give all attributes the same Ordering Index.  Items, like days of the week should be ordered 1 to 7."  noteTextPosition="#helpPosition#">
		<SELECT NAME="frmOrder"><CFLOOP INDEX="LoopCount" FROM="1" TO="99">
				<CFOUTPUT>
				<CFIF LoopCount IS frmOrder>
					<OPTION VALUE=#LoopCount# SELECTED> #htmleditformat(LoopCount)#
				<CFELSE>
					<OPTION VALUE=#LoopCount#> #htmleditformat(LoopCount)#
				</CFIF>
				</CFOUTPUT>
			</CFLOOP></SELECT>
	</CF_relayFormElementDisplay>

	<CF_relayFormElementDisplay label = "Active" relayFormElementType="Radio" list="0#application.delim1#No,1#application.delim1#Yes" fieldname = "frmActive" currentvalue = #frmActive# columns=2>

<!--- 	<TR>
		<td valign="top" class="label">Active</TD>
		<TD>
			<INPUT TYPE="RADIO" NAME="frmActive" VALUE="0" <CFOUTPUT>#IIF(frmActive IS 0, DE("CHECKED"), DE(""))#</CFOUTPUT>> No &nbsp;&nbsp;
			<INPUT TYPE="RADIO" NAME="frmActive" VALUE="1" <CFOUTPUT>#IIF(frmActive IS NOT 0, DE("CHECKED"), DE(""))#</CFOUTPUT>> Yes
		</TD>
	</TR>

 --->

	<CF_relayFormElementDisplay label = "Score" relayFormElementType="text" fieldname = "frmScore" currentvalue = #frmScore# size="5" maxLength="8"	noteText = "Attributes can use scores in various ways depending on the context.  E.g. scores of 1 or 0 are used to determine whether an attribute is complete or not in a phoning campaign or whether a question is right or wrong in a quiz."  noteTextPosition="#helpPosition#">

<!--- 	<TR>
		<td valign="top" class="label">Score</TD>
		<TD>
			<INPUT TYPE="TEXT" NAME="frmScore" VALUE="<CFOUTPUT>#frmScore#</CFOUTPUT>" SIZE="5" MAXLENGTH="25">
			<BR>Attributes can use scores in various ways depending on the context.  E.g. scores of 1 or 0 are used to determine
			whether an attribute is complete or not in a phoning campaign or whether a question is right or wrong in a quiz.<BR><BR>

		</TD>
	</TR>
 --->
	<CFIF frmflagTypeID is not 2 and frmflagTypeID is not 3>  <!--- ie not radio and checkbox --->
				<CFIF frmUseValidValues is 1 and frmFlagTextID is not "">
					<cfset noteText = '<A HREF="javascript:validValues (''flag.#frmFlagTextID#'')">Edit</a>'>
				<cfelseif frmUseValidValues is 1 >
					<cfset noteText = "Set a Unique Text ID before creating valid values">
				<cfelse>
					<cfset noteText = "">
				</cfif>
			<CF_relayFormElementDisplay label = "Use Valid Values" relayFormElementType="Radio" list="0#application.delim1#No,1#application.delim1#Yes" fieldname = "frmUseValidValues" currentvalue = #frmUseValidValues# columns=2 noteText="#noteText#" noteTextPosition="Right" noteTextImage ="false">

					<!---
							<CFOUTPUT>
						<TR>
								<td valign="top" class="label">
									Use Valid Values
									 :
								</td>
								<TD>
									<INPUT TYPE="RADIO" NAME="frmUseValidValues" VALUE="0" #IIF(frmUseValidValues IS 0, DE("CHECKED"), DE(""))#> No &nbsp;&nbsp;
									<INPUT TYPE="RADIO" NAME="frmUseValidValues" VALUE="1" #IIF(frmUseValidValues IS NOT 0, DE("CHECKED"), DE(""))#> Yes
									<CFIF frmUseValidValues is 1 and frmFlagTextID is not "">
										<A HREF="javascript:validValues ('flag.#frmFlagTextID#')">Edit</a>
									</cfif>


								</TD>

							</TR>
					</CFOUTPUT>
					 --->

			<CF_relayFormElementDisplay label = "Do Lookup" relayFormElementType="Radio" list="0#application.delim1#No,1#application.delim1#Yes" fieldname = "frmLookUp" currentvalue = #frmLookup# columns=2 noteText="Select this if the data value stored in this field is different to the value you wish to display" noteTextPosition="#helpPosition#">

				<!---
				<TR>
					<td valign="top" class="label">Do Lookup</td>
					<TD>
						<INPUT TYPE="RADIO" NAME="frmlookup" VALUE="0" <CFOUTPUT>#IIF(frmlookup IS 0, DE("CHECKED"), DE(""))#</CFOUTPUT>> No &nbsp;&nbsp;
						<INPUT TYPE="RADIO" NAME="frmlookup" VALUE="1" <CFOUTPUT>#IIF(frmlookup IS NOT 0, DE("CHECKED"), DE(""))#</CFOUTPUT>> Yes
						<BR>
					</TD>
				</TR>
		 		--->
 </cfif>

	<cfif frmflagTypeID is 5 > <!--- text --->
		<CF_relayFormElementDisplay label = "Display As" relayFormElementType="html">
			<cfset DisplayAs = getFormattingParameterValue("DisplayAs")>
			<cfset DisplayAsValidValues = "Memo">
			<cfif frmWddXStruct is not ""><cfset DisplayAsValidValues =listappend(DisplayAsValidValues ,"Grid")></cfif>
			<cf_displayValidValues
				validValues = "#DisplayAsValidValues#"
				formfieldname = "frmParameter_DisplayAs"
				currentValue = #DisplayAs#
				showNull = true
				nullText ="Input Field (Default)"
				keepOrig = false
				>
		</CF_relayFormElementDisplay>

					<!--- <TR>
							<td valign="top" class="label">Display As</td>
							<TD>
							<cfset DisplayAs = getFormattingParameterValue("DisplayAs")>
							<cfset DisplayAsValidValues = "Memo">
							<cfif frmWddXStruct is not ""><cfset DisplayAsValidValues =listappend(DisplayAsValidValues ,"Grid")></cfif>
							<cf_displayValidValues
								validValues = "#DisplayAsValidValues#"
								formfieldname = "frmParameter_DisplayAs"
								currentValue = #DisplayAs#
								showNull = true
								nullText ="Input Field (Default)"
								keepOrig = false
								>
							</TD>
						</TR>
					 --->
		<cfif DisplayAs is "Memo">

			<CF_relayFormElementDisplay label = "Memo Size" relayFormElementType="html">
				<cfset rows = getFormattingParameterValue("rows")>
				<cfset cols = getFormattingParameterValue("cols")>
				<CFOUTPUT>
				Rows: <CF_INPUT TYPE="TEXT" NAME="frmParameter_rows" value="#rows#" size = 4><BR>
				Cols: <CF_INPUT TYPE="TEXT" NAME="frmParameter_cols" value="#Cols#" size = 4>
				</CFOUTPUT>
			</CF_relayFormElementDisplay >

		<cfelse>

			<cfset mask = getFormattingParameterValue("mask")>
			<CF_relayFormElementDisplay label = "Mask" relayFormElementType="text" fieldname = "frmParameter_mask" currentvalue = #mask# size="20" maxLength="100"	>

		</cfif>


	</cfif> <!--- End of flag type Text --->

		<!--- 2015/09/08 NJH - enable flags/groups to be displayed/hidden based on flag being selected. Show for boolean flags only --->
	<cfif listFind("2,3",frmflagTypeID)>
		<cfswitch expression="#FlagEntityType#">
			<cfcase value="person"><cfset validEntityTypes = "person,location,organisation"></cfcase>
			<cfcase value="location"><cfset validEntityTypes = "location,organisation"></cfcase>
			<cfdefaultcase><cfset validEntityTypes = FlagEntityType></cfdefaultcase>
		</cfswitch>

		<!--- Provide a list of flags that a user could select when choosing to make a flag required on another flag or when choosing to show a flag when on is selected. --->
		<cfquery name="getFlagsForSelecting">
			select distinct display, optGroup, value, entityTypeID from
				(select case when len(isNull(flagTextID,'')) > 0 then flagTextID else cast(flagID as varchar) end as value, entityTable <cfif listLen(validEntityTypes) gt 1>as optGroup</cfif>,
					flagGroup <cfif listLen(validEntityTypes) eq 1>as optGroup</cfif>,
					<cfif listLen(validEntityTypes) gt 1>flagGroup+': '+</cfif >flag+ ' ('+ case when len(isNull(flagTextID,'')) > 0 then flagTextID else cast(flagID as varchar) end +')' as display,
					entityTypeID
				from vflagDef
				where active=1 and isSystem=0 and entityTable in (<cf_queryparam value="#validEntityTypes#" cfsqltype="varchar" list="true">)
			) base
			order by entityTypeID,optGroup,display
		</cfquery>

		<cfquery name="getFlagGroupsForSelecting">
			select distinct display, <cfif listLen(validEntityTypes) gt 1>optGroup,</cfif> value, entityTypeID,dataTable from
				(select case when len(isNull(flagGroupTextID,'')) > 0 then flagGroupTextID else cast(flagGroupID as varchar) end as value,dataTable,
					entityTable <cfif listLen(validEntityTypes) gt 1>as optGroup</cfif>,flagGroup + ' ('+ case when len(isNull(flagGroupTextID,'')) > 0 then flagGroupTextID else cast(flagGroupID as varchar) end +')' as display,
					entityTypeID
				from vflagDef
				where active=1 and isSystem=0 and entityTable in (<cf_queryparam value="#validEntityTypes#" cfsqltype="varchar" list="true">)
			) base
			order by entityTypeID,<cfif listLen(validEntityTypes) gt 1>optGroup,</cfif>display
		</cfquery>

		<cfquery name="getBooleanFlagGroupsForSelecting" dbType="query">
			select distinct display, <cfif listLen(validEntityTypes) gt 1>optGroup,</cfif> [value],entityTypeID
			from getFlagGroupsForSelecting
			where dataTable = 'boolean'
			order by entityTypeID,<cfif listLen(validEntityTypes) gt 1>optGroup,</cfif>display
		</cfquery>

		<!--- required  --->
		<cfset requiresValueInOtherFlag = getFormattingParameterValue("requiresValueInOtherFlag")>
		<cfset requiresValueInOtherFlagGroup = getFormattingParameterValue("requiresValueInOtherFlagGroup")>

		<cfset trStyle = requiresValueInOtherFlag eq "" and requiresValueInOtherFlagGroup eq ""? "display:none;":"">
		<cfset requiresValuesDisabled = requiresValueInOtherFlag eq "" and requiresValueInOtherFlagGroup eq ""?0:1>
		<cf_relayFormElementDisplay label = "Requires Value in Other Profiles or Profile Attributes When Selected?" relayFormElementType="checkbox" currentvalue="#requiresValuesDisabled#" onChange="jQuery('.requiredValues').toggle();" fieldname="requiresValues" disabled="#requiresValuesDisabled#">

		<script>
			function requiresValueOnChange () {
				if(jQuery('#frmParameter_requiresValueInOtherFlag_select option, #frmParameter_requiresValueInOtherFlagGroup_select option').length != 0) {
					jQuery('#requiresValues').prop('disabled',true);
				} else {
					jQuery('#requiresValues').prop('disabled',false);
				}
			}
		</script>

		<CF_relayFormElementDisplay label = "Requires Value in Other Profile Attributes" trClass="requiredValues" onChange="requiresValueOnChange();" trStyle="#trStyle#" relayFormElementType="select" displayAs="twoSelects" width="250" size=8 query="#getFlagsForSelecting#" display="display" value="value" fieldname = "frmParameter_requiresValueInOtherFlag" currentvalue = #requiresValueInOtherFlag# maxLength="100"	noteText = "If setting this profile attribute should automatically make other profile attributes required, then select the other profile attributes here<BR>Note that the other profile attributes have to be specifically included on the same screen"  noteTextPosition="#helpPosition#">

		<cfset requiresValueInOtherFlagMessage = getFormattingParameterValue("requiresValueInOtherFlagMessage")>
		<cfif requiresValueInOtherFlag is not "" or requiresValueInOtherFlagMessage is not "">
			<CF_relayFormElementDisplay label = "Requires Value in Other Profile Attribute Message" trClass="requiredValues" trStyle="#trStyle#" relayFormElementType="text" fieldname = "frmParameter_requiresValueInOtherFlagMessage" currentvalue = #requiresValueInOtherFlagMessage# size="40" maxLength="100"	noteText = "This is the javascript message which appears if the other profile attributes are not set"  noteTextPosition="#helpPosition#">
		</cfif>

		<CF_relayFormElementDisplay label = "Requires Value in Other Profiles" relayFormElementType="select" trClass="requiredValues" onChange="requiresValueOnChange();" trStyle="#trStyle#" displayAs="twoSelects" size=8 width="250" query="#getBooleanFlagGroupsForSelecting#" display="display" value="value" fieldname="frmParameter_requiresValueInOtherFlagGroup" currentvalue = #requiresValueInOtherFlagGroup# maxLength="100"	noteText = "If setting this profile attribute should automatically make other profiles required, then select the other profiles here<BR>Note that the other profiles have to be specifically included on the same screen"  noteTextPosition="#helpPosition#">
		<cfset requiresValueInOtherFlagGroupMessage = getFormattingParameterValue("requiresValueInOtherFlagGroupMessage")>
		<cfif requiresValueInOtherFlagGroup is not "" or requiresValueInOtherFlagGroupMessage is not "">
			<CF_relayFormElementDisplay label = "Requires Value in Other Profile Message" relayFormElementType="text" trClass="requiredValues" trStyle="#trStyle#" fieldname = "frmParameter_requiresValueInOtherFlagGroupMessage" currentvalue = #requiresValueInOtherFlagGroupMessage# size="40" maxLength="100"	noteText = "This is the javascript message which appears if the other profiles are not set"  noteTextPosition="#helpPosition#">
		</cfif>

		<!--- showing --->
		<script>
			function showValueOnChange () {
				if(jQuery('#frmParameter_showOtherFlagsWhenSelected_select option, #frmParameter_showOtherFlagGroupsWhenSelected_select option').length != 0) {
					jQuery('#showValues').prop('disabled',true);
				} else {
					jQuery('#showValues').prop('disabled',false);
				}
			}
		</script>

		<cfset showOtherFlags = getFormattingParameterValue("showOtherFlagsWhenSelected")>
		<cfset showOtherFlagGroups = getFormattingParameterValue("showOtherFlagGroupsWhenSelected")>

		<cfset trStyle = showOtherFlags eq "" and showOtherFlagGroups eq ""?"display:none;":"">
		<cfset showValuesDisabled = showOtherFlags eq "" and showOtherFlagGroups eq ""?0:1>
		<cf_relayFormElementDisplay label = "Show Other Profiles or Profile Attributes When Selected?" relayFormElementType="checkbox" currentvalue="#showValuesDisabled#" onChange="jQuery('.showValues').toggle();" fieldname="showValues" disabled=#showValuesDisabled#>

		<CF_relayFormElementDisplay label = "Show Other Profile Attributes" relayFormElementType="select" trStyle="#trStyle#" trClass="showValues" onChange="showValueOnChange();" displayAs="twoSelects" width="250" size=8 query="#getFlagsForSelecting#" display="display" value="value" fieldname = "frmParameter_showOtherFlagsWhenSelected" currentvalue = #showOtherFlags# maxLength="100"	noteText = "If setting this profile attribute should automatically make other profile attributes display, then select the other profile attributes here<BR>Note that the other profile attributes have to be specifically included on the same screen"  noteTextPosition="#helpPosition#">
		<CF_relayFormElementDisplay label = "Show Other Profiles" relayFormElementType="select" displayAs="twoSelects"  trStyle="#trStyle#" trClass="showValues" onChange="showValueOnChange();" size=8 width="250" query="#getFlagGroupsForSelecting#" display="display" value="value" fieldname="frmParameter_showOtherFlagGroupsWhenSelected" currentvalue = #showOtherFlagGroups# maxLength="100"	noteText = "If setting this profile attribute should automatically make other profiles display, then select the other profiles here<BR>Note that the other profiles have to be specifically included on the same screen"  noteTextPosition="#helpPosition#">
	</cfif>

	<CF_relayFormElementDisplay label = "ShowInConditionalEmails" relayFormElementType="Radio" list="0#application.delim1#No,1#application.delim1#Yes" fieldname = "frmShowInConditionalEmails" currentvalue = #frmShowInConditionalEmails# columns=2>

	<CF_relayFormElementDisplay label = "Protected" relayFormElementType="checkbox" value="1" fieldname = "frmProtected" currentvalue = #frmProtected# 	noteText = "Select this to prevent a #flagEntityType# with this attribute set being deleted"  noteTextPosition="#helpPosition#">

	<!--- NJH Roadmap 2013 2013/04/02 - added support for profile images - to be used in the locator --->
	<cfset filePath = "/content/linkImages/flag/#frmFlagID#/Thumb_#frmFlagID#.png">
	<CF_relayFormElementDisplay label = "Associated Image" relayFormElementType="file" acceptType="image" fieldname = "frmProfileImage" currentvalue = "#filePath#">

<!--- 	<CFOUTPUT>
	<TR>
		<td valign="top" class="label">Protected</td>
		<TD>
			<INPUT TYPE="checkbox" NAME="frmprotected" VALUE="1" #IIF(frmprotected IS 1, DE("CHECKED"), DE(""))#>
			<BR>Select this to prevent a #flagEntityType# with this attribute set being deleted
		</TD>
	</TR>
	</CFOUTPUT>
 --->
<!---
	WAB 2009-05-11
	Toyed with idea of mergeProtection being different from plain protection, but for time being they are the same and have commented out this parameter
	<cfif frmProtected>
			<cfset tempValue = getFormattingParameterValue("mergeProtected")>
			<CF_relayFormElementDisplay label = "Merge Protection" relayFormElementType="checkbox" value="1" fieldname = "frmParameter_mergeProtected" currentvalue = #tempValue# 	noteText = "Select this to prevent a #flagEntityType# with this attribute set being removed during a merge"  noteTextPosition="#helpPosition#">
	</cfif>
--->


		<cfif frmflagTypeID is 6 or frmflagTypeID is 8> <!--- Integer or Integermultiple --->

				<CFQUERY NAME="entityTypes" DATASOURCE="#application.SiteDataSource#">
				select 'null' as dataValue, 'Choose Linked Entity Type' as displayValue , 1 as ordering
					union
				select convert(varchar,entityTypeID) as dataValue, tablename as displayValue, 2
				 from flagEntityType <!--- where entityTypeID in (0,1,2,3,4,67) NJH 2015/01/20 - want to link to products.. Remove this hardcoded list  --->
				 order by ordering, displayValue
				</CFQUERY>


	<CF_relayFormElementDisplay label = "Links to an entity" query="#entityTypes#" relayFormElementType="select" fieldname = "frmLinksToEntityTypeID" currentvalue = #frmLinksToEntityTypeID# 	noteText = "If the integer value in this field is actually an entityID, chose the entity Type."  noteTextPosition="#helpPosition#">

<!--- 		<TR>
			<td valign="top" class="label">Links to an entity</td>
			<TD>
				<cf_displayValidValues
					validValues = #entityTypes#
					formfieldname = "frmLinksToEntityTypeID"
					currentValue = #frmLinksToEntityTypeID#
					showNull = false
					> <BR>

				If the integer value in this field is actually an entityID, chose the entity Type.
			</TD>
		</TR>
 --->
			<cfif frmLinksToEntityTypeID is not "">
				<cfset tempValue = getFormattingParameterValue("protectedLinkedEntity")>
				<cfset helpText = "Select this to prevent any #application.entityType[frmLinksToEntityTypeID].tablename# linked to any #flagEntityType# by this attribute being deleted.">
				<CF_relayFormElementDisplay label = "Protected Linked Entity" relayFormElementType="checkbox" value="1" fieldname = "frmParameter_protectedLinkedEntity" currentvalue = #tempValue# 	noteText = "#helpText#"  noteTextPosition="#helpPosition#">

			</cfif>

		</cfif>

<cfif frmflagTypeID is 5>
	<tr>
		<td valign="top" class="label">WDDX </td>
		<td valign="top">
				<cfset wddxArray = false>
			<CFIF frmWddXStruct is not "">
				<CFWDDX ACTION="WDDX2CFML" INPUT="#frmWddXStruct#" OUTPUT="thisStructure">

				<cfif isArray(thisStructure)>
					<cfset wddxArray = true>
					<cfset thisStructure = thisStructure[1] >
				</cfif>

				This attribute stores a WDDX structure with the following fields<BR>
				You may set valid values by clicking on the link <BR>
				Remember that to use the valid values, you must select "use valid values" in the screen definition<BR>
				Check the box to delete fields (be careful in case they are required in any existing screens)<BR>
				Add new fields adding the names in the boxes below <BR>

				<CFLOOP item="thisItem" collection="#thisStructure#">
					<CFOUTPUT><CF_INPUT type="checkbox" name ="frmDelWDDX" value="#thisItem#"> <A HREF="javascript:validValues('Flag.#frmFlagTextID#.#thisItem#')">#htmleditformat(thisItem)#</a></cfoutput> <BR>
				</cfloop>

			<CFELSE>
				You may define a WDDX structure in which to store multiple fields of information.<BR>
				Enter the name of a field to add below. <BR>
			</cfif>
				<INPUT type="text" name="frmWDDX" value="">
				<BR><INPUT type="checkbox" name="frmWDDXArray" value="1" <cfif wddxarray>checked</cfif>>Allow more than one record to be entered
		</td>
	</tr>

	<cfif frmWddXStruct is not "" >
	<cfoutput>
			<CF_relayFormElementDisplay label = "WDDX Grid Settings" relayFormElementType="html">
				<cfif DisplayAs is not "Grid">Set Display As to "Grid" to make use of these settings</cfif>
			</CF_relayFormElementDisplay>

				<cfset columnList = getFormattingParameterValue("columnList")>
				<CF_relayFormElementDisplay label = "Columns (list)" relayFormElementType="text" currentvalue="#ColumnList#" fieldname = "frmParameter_ColumnList" c size="30"	>

				<cfset columnHeaderList = getFormattingParameterValue("columnHeaderList")>
				<CF_relayFormElementDisplay label = "Column Headings (list)" relayFormElementType="text" currentvalue="#columnHeaderList#" fieldname = "frmParameter_columnHeaderList" size="30"	>

				<cfset fieldSizeList = getFormattingParameterValue("fieldSizeList")>
				<CF_relayFormElementDisplay label = "Column Sizes (list)" relayFormElementType="text" currentvalue="#fieldSizeList#" fieldname = "frmParameter_fieldSizeList" size="30"	>

				<cfset NUMBERBLANKROWS = getFormattingParameterValue("NUMBERBLANKROWS")>
				<CF_relayFormElementDisplay label = "Number of Blank Rows" relayFormElementType="text" currentvalue="#NUMBERBLANKROWS#" fieldname = "frmParameter_NUMBERBLANKROWS" size="4"	>

				<cfset AddRowText = getFormattingParameterValue("AddRowText")>
				<CF_relayFormElementDisplay label = "Text on the Add Row Link" relayFormElementType="text" currentvalue="#AddRowText#" fieldname = "frmParameter_AddRowText" size="30"	>

				<cfset showDelete = getFormattingParameterValue("showDelete")>
				<CF_relayFormElementDisplay label = "Allow Deletion of Rows" relayFormElementType="text" currentvalue="#showDelete#" fieldname = "frmParameter_showDelete" size="30"	>

	</cfoutput>
	</cfif>

</cfif>

	<cfif structCount(formattingParametersStructure) gt 0>
		<TR>
			<td valign="top" class="label" colspan = 2>Undocumented Settings</td>
		</TR>

	</cfif>
	<!--- Now loop through any formatting parameters left in the structure --->
	<cfloop item="parameterName" collection="#formattingParametersStructure#">
		<cfoutput>
				<!--- 2015/10/20 PYW P-TAT006 BRD 32. Increase formatting parameters max length to 1000 characters --->
				<CF_relayFormElementDisplay label = "#parameterName#" relayFormElementType="textArea" currentvalue="#formattingParametersStructure[parameterName]#" fieldname = "frmParameter_#parameterName#" size="30" maxlength="1000"	>
		</cfoutput>
	</cfloop>

				<!--- WAB 2009-04-27 Try Add your own parameters --->
				<cfset newRow = structNew()>
					<cfset newRow.row = structNew()>
					<cfset newRow.cells = arrayNew(1)>
					<cfset newRow.cells [1] = structNew()>
					<cfset newRow.cells [1].content = "<input type='text' name='name_frmParameter_INDEX' length='25'>">
					<cfset newRow.cells [2] = structNew()>
					<cfset newRow.cells [2].content = "<input type='text' name='frmParameter_INDEX' size='40'>">
					<cf_includeJavascriptOnce template = "/javascript/tablemanipulation.js">

 				<script>
					<cfwddx action="cfml2js" input = "#newRow#" topLevelVariable = "newRowFlagEditTable">
					function addNewParameter () {
						addRowToTable('flagEditTable',newRowFlagEditTable,getRowIndexOfRow('flagEditTable','addParameterRow'),null,false)
					}

				</script>

	<TR id="addParameterRow">
		<td valign="top" class="label" colspan=2><a href="javascript:addNewParameter()">Add New Undocumented Setting</a></td>
	</TR>


	<cfif frmtask neq "Add">
		<cfif ScreensContainingFlag.recordcount is not 0>
		<tr>
			<td class="label" valign="TOP">
				This Attribute appears in the following Screens:
			</td>
			<td colspan="3" valign="top" nowrap>
				<cfoutput query="ScreensContainingFlag">
					#htmleditformat(screenName)# (#htmleditformat(screenid)#)<br>
				</cfoutput>
			</td>
		</tr>
		</cfif>
	</cfif>

</CF_relayFormDisplay >
<!--- </table> --->
</FORM>



<FORM METHOD="POST" ACTION="<CFOUTPUT>/profileManager/#htmleditformat(frmBack)#</CFOUTPUT>.cfm" NAME="BackForm">
<CF_INPUT TYPE="HIDDEN" NAME="frmFlagGroupID" VALUE="#frmFlagGroupID#">
</FORM>

<P>


</CENTER>
</FORM>


