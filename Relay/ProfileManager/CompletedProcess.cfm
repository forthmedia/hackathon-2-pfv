<!--- �Relayware. All Rights Reserved 2014 --->
<!---
		************************
			CompleteProcess
		************************
		
		David A McLean 25 June 2002
		For Foundation Network Ltd
		
		The intention is that this template should be the final step in a profile process.
		
		A profile process is a process that takes the user through one (or more) screens containing
		profile data for edit and the final step contains actions that occur as a result of some
		or any of the profiles changing.
		
WAB 2010/10/13  removed get TextPhrases and replaced with CF_translate		
		
--->

<!---
		INITIALISING
--->
<cfparam name="RegApprovalPrefix" default = "">

<CFSET doublequote='"'>


<!--- Trying to determine whether the profiles updated were organisation or Person 

--->
<CFIF IsDefined("FRMORGANISATIONIDLIST")>
	<!--- This means that the screens update contained Organisation Data --->
	<CFSET frmOrganisationID=FRMORGANISATIONIDLIST>
	<CFQUERY NAME="OrgDetails" DATASOURCE="#application.SiteDataSource#">
		SELECT OrganisationName FROM Organisation WHERE OrganisationID =  <cf_queryparam value="#frmOrganisationID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</CFQUERY>
</CFIF>

<CFIF IsDefined("FRMPERSONIDLIST")>
	<!--- This means that the screens update contained Person Data --->
	<CFSET frmPersonID=FRMPERSONIDLIST>
	<CFQUERY NAME="PerDetails" DATASOURCE="#application.SiteDataSource#">
		SELECT FirstName, LastName FROM Person WHERE PersonID =  <cf_queryparam value="#frmPersonID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</CFQUERY>
</CFIF>


<!--- The Process should contain a step that defines the profiles in the screen 
	that need to be checked	to determine what actions to take 
	
	It might also need to be passed a query I think
--->
<CFIF IsDefined("frmOrgFlagsCheck")>
	<CFQUERY NAME="CheckFlags" DATASOURCE="#application.SiteDataSource#">
		SELECT x=CASE WHEN Entityid IS NOT NULL THEN 1 ELSE 0 END,name, FirstName, LastName FROM
		(SELECT Entityid, FlagID, FirstName, LastName FROM integerflagdata i 
		INNER JOIN person p ON p.personid = i.data
		WHERE entityID =  <cf_queryparam value="#frmOrganisationID#" CFSQLTYPE="CF_SQL_INTEGER" > ) a
		RIGHT JOIN 
		(SELECT flagid,name
		FROM Flag
		WHERE flagTextID IN (<cf_queryParam value="#frmOrgFlagsCheck#" CFSQLTYPE="CF_SQL_VARCHAR" list="true">)) b
		ON a.flagid=b.flagid
	</CFQUERY>
</CFIF>
<!--- If we use queries like the one above then the x column can be used to set a score

	This can be useful when an action requires that a certain number of attributes are set 
	
	Below deals with Integer flags only
	--->
<CFSET Score=0>
<cf_translate>
<TABLE WIDTH="600" CELLPADDING="0" CELLSPACING="0" BORDER="0" ALIGN="center">
	<CFOUTPUT>
		<CFIF IsDefined("FRMPERSONIDLIST")>
			<TR><TH colspan=2 HEIGHT="48" VALIGN="middle"><B>phr_PersonRegApprovalTitle</B></TH></TR>
		<CFELSE>
			<TR><TH colspan=2 HEIGHT="48" VALIGN="middle"><B>phr_OrgRegApprovalTitle</B></TH></TR>
		</CFIF>
		
		<TR><TD>&nbsp;</TD></TR>
		<TR><TH>phr_RegApprovalAssignedRole</TH><TH>phr_RegApprovalName</TH></TR>
		</CFOUTPUT>
		<CFOUTPUT QUERY="CheckFlags">
		<TR>
		<TD HEIGHT="24">#htmleditformat(name)#</TD>
		<CFIF x EQ 1>
			<TD HEIGHT="24">#htmleditformat(FirstName)# #htmleditformat(LastName)#</TD>
		<CFELSE>
			<TD HEIGHT="24" CLASS="messagetext">SETTING REQUIRED</TD>
		</CFIF>
		</TR>
		<CFSET Score=Score+x>
	</CFOUTPUT>
	
	<CFIF Score EQ CheckFlags.recordcount>
		<CFSET EntityOk=true>
	<CFELSE>
		<CFSET EntityOk=false>
	</CFIF>
	
	
	<CFOUTPUT>
	<CFIF IsDefined("FRMPERSONIDLIST")>
		<CFQUERY NAME="PerStatus" DATASOURCE="#application.SiteDataSource#">
		SELECT f.name, f.flagtextid FROM booleanflagdata b
		INNER JOIN flag f on f.flagid = b.flagid and b.entityid =  <cf_queryparam value="#frmpersonid#" CFSQLTYPE="CF_SQL_INTEGER" >  
		INNER JOIN flaggroup fg on fg.flaggroupid = f.flaggroupid
		AND fg.flaggrouptextid =  <cf_queryparam value="#RegApprovalPrefix#PerApprovalStatus" CFSQLTYPE="CF_SQL_VARCHAR" > 
		</CFQUERY>
			<CFSET paragraph=doublequote & phr_PerStatus & doublequote>
			<TR><TH colspan=2 HEIGHT="36">#evaluate("#htmleditformat(paragraph)#")#</TH></TR>
		<CFQUERY NAME="OrgStatus" DATASOURCE="#application.SiteDataSource#">
		SELECT f.name, f.flagtextid FROM booleanflagdata b
		INNER JOIN flag f on f.flagid = b.flagid and b.entityid =  <cf_queryparam value="#frmorganisationid#" CFSQLTYPE="CF_SQL_INTEGER" >  
		INNER JOIN flaggroup fg on fg.flaggroupid = f.flaggroupid
		AND fg.flaggrouptextid =  <cf_queryparam value="#RegApprovalPrefix#OrgApprovalStatus" CFSQLTYPE="CF_SQL_VARCHAR" > 
		</CFQUERY>
			<CFSET paragraph=doublequote & phr_OrgStatus & doublequote>
			<TR><TH colspan=2 HEIGHT="36">#evaluate("#htmleditformat(paragraph)#")#</TH></TR>
		
		<CFIF PerStatus.flagtextid EQ "PerApproved" and OrgStatus.flagtextid EQ "OrgApproved">
			<!--- 
			Condition Achieved : Org and Person approved
						Action : Send Email to Partner
			 --->
			<cfinclude template="/partners/sendPartnerPassword.cfm">
			<CFSET paragraph=doublequote & phr_PerAndOrgOK & doublequote>
			<TR><TH colspan=2 HEIGHT="36">#evaluate("#htmleditformat(paragraph)#")#</TH></TR>
		<CFELSEIF OrgStatus.flagtextid EQ "OrgRejected">
			<!--- 
			Condition Achieved : Org Rejected
						Action : Kill all users at org
			 --->
			<CFSET paragraph=doublequote & phr_OrgReject & doublequote>
			<TR><TD ALIGN="center" class="messagetext" colspan=2 HEIGHT="36">#evaluate("#htmleditformat(paragraph)#")#</TD></TR>
			<TR><TH  colspan=2>&nbsp;</TH></TR>
		<CFELSEIF PerStatus.flagtextid EQ "PerApproved" and OrgStatus.flagtextid NEQ "OrgApproved">
			<!--- 
			Condition Achieved : Person approved by org not
						Action : Notify that Org has to be approved
			 --->
			<CFSET paragraph=doublequote & phr_PerOKAndOrgNotOK & doublequote>
			<TR><TD ALIGN="center" class="messagetext" colspan=2 HEIGHT="36">#evaluate("#htmleditformat(paragraph)#")#</TD></TR>
			<TR><TH  colspan=2>&nbsp;</TH></TR>
		
		<CFELSEIF PerStatus.flagtextid EQ "PerRejected">
			<!--- 
			Condition Achieved : Person Rejected
						Action : Kill person's account
			MDC 12th April 2003.  I have added in a send email to partner when rejected for StreamServe
			This is not used for ATI and may need to be amended to show this.
			 --->
		<cfmodule template="/genericEmails/emailSender.cfm" 
			emailTextID="PersonRejectedEmail" cfmailType="HTML" entityid="#frmPersonID#">
			<CFSET paragraph=doublequote & phr_PerReject & doublequote>
			<TR><TD ALIGN="center" class="messagetext" colspan=2 HEIGHT="36">#evaluate("#htmleditformat(paragraph)#")#</TD></TR>
			<TR><TH  colspan=2>&nbsp;</TH></TR>
			
		<CFELSEIF PerStatus.flagtextid EQ "PerPartlyApproved" and OrgStatus.flagtextid EQ "OrgPartlyApproved">
			<!--- 
			Condition Achieved : Org and Person approved
						Action : Send Email to Partner
			 --->
		<cfmodule template="/genericEmails/emailSender.cfm" 
			emailTextID="PartlyApproved">
			<CFSET paragraph=doublequote & phr_PerAndOrgPartial & doublequote>
			<TR><TH colspan=2 HEIGHT="36">#evaluate("#htmleditformat(paragraph)#")#</TH></TR>
		</CFIF>
	
	<CFELSE>
		<CFQUERY NAME="OrgStatus" DATASOURCE="#application.SiteDataSource#">
		SELECT f.name FROM booleanflagdata b
		INNER JOIN flag f on f.flagid = b.flagid and b.entityid =  <cf_queryparam value="#frmorganisationid#" CFSQLTYPE="CF_SQL_INTEGER" >  
		INNER JOIN flaggroup fg on fg.flaggroupid = f.flaggroupid
		AND fg.flaggrouptextid =  <cf_queryparam value="#RegApprovalPrefix#OrgApprovalStatus" CFSQLTYPE="CF_SQL_VARCHAR" > 
		</CFQUERY>
			<CFSET paragraph=doublequote & phr_OrgStatus & doublequote>
			<TR><TH colspan=2 HEIGHT="36">#evaluate("#htmleditformat(paragraph)#")#</TH></TR>
	</CFIF>
	</CFOUTPUT>
	<TR>
		<TD ALIGN="center" HEIGHT="24" VALIGN="middle" colspan=2>
			<CFOUTPUT><A href="/profileManager/regApprovalReport.cfm">phr_Approval_Back</CFOUTPUT></A>
		</TD>
	</TR>
</TABLE>
</cf_translate>
