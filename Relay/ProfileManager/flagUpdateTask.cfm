<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Update flag information --->

<!--- Amendment History
2000-02-25  WAB added support for validvalues and lookup
1999-02-20		WAB		Add Insert of FlagTextID
2004-11-02	SWJ		Modified to deal with NamePhraseTextID
2005-09-14	WAB		code to process #frmlinkstoentityTypeID#
2008/04/15	WAB		deal with protected field
2008/09/22  WAB added handling for score on bulk load
2008/10/21	WAB 	removed cftransaction because I ended up with a nested cftransaction when calling updateFlagStructure if the flaggroup had a view attached.
					I probably ought to put in lots of individual cftransactions, but didn't !
2008/12/12	NJH		Bug Fix Bugs - All Sites Issue 1491 - added a trim around the flagTextID when inserting/updating flags to remove any leading/trailing spaces.
2009/01/30  WAB/NJH  Bug Fix.  Moved application.com.flag.updateFlagStructure(frmFlagID) (where it fell over for bulk loads when frmFlagID was a actually a list).  Now appears at end of each task  Edit, Delete and Ordering
2009/04/29   WAB    Add nicer handling of parameters
2010/02/02	NJH/WAB	LID 2828 - update flag structure once if doing a bulk upload. Do it after all flags have been added.
2011/06/14	PPB		REL106 - add showInConditionalEmails
2016/12/13	NJH		JIRA PROD2016-PROD2016-2957 - added createdBy/lastUpdatedBy person fields
--->

<!--- these parameters are not always on the previous page --->
<CFPARAM name="frmlookup" default="0">
<CFPARAM name="frmscore" default="0">
<CFPARAM name="frmuseValidValues" default="0">
<CFPARAM name="frmProtected" default="0">
<CFPARAM name="frmlinksToEntityTypeID" default="">
<CFPARAM name="frmwddx" default="">
<!--- <CFPARAM name="frmFormattingParameters" default=""> --->
<cfparam name="pagesBack" type="numeric" default="0">
<CFPARAM name = "frmNextFlagID"  default="#frmFlagID#">
<CFPARAM name="frmShowInConditionalEmails" default="0">

<!--- WAB 2009/04/29 --->
<cfset "frmFormattingParameters" = application.com.regExp.CollectFormFieldsIntoNameValuePairs(fieldPrefix="frmParameter",fieldSuffix="")>

<!--- if frmTranslateLabel was set make frmNamePhraseTextID = frmFlagTextID otherwise make it blank.
This is because the translations for flags us namePhraseTextID and we may as well make it the same as flagTextID.
Less data to maintain! --->
<cfparam name="frmNamePhraseTextID" default="">

<!--- <CFTRANSACTION> --->
	<CFSET updatetime = CreateODBCDateTime(Now())>

	<CFQUERY NAME="checkDateTime" DATASOURCE="#application.SiteDataSource#">
		SELECT LastUpdated
		FROM FlagGroup
		WHERE FlagGroupID =  <cf_queryparam value="#frmFlagGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >
		AND datediff(s,LastUpdated,#frmDate#) <= -1
	</CFQUERY>

	<CFIF checkDateTime.RecordCount IS NOT 0>
		<CFSET message = "The record could not be updated since it has been changed by another user since you last viewed it.">
		<cfset messageType="info">
		<CFINCLUDE TEMPLATE="#frmBack#.cfm">
		<CF_ABORT>
	</CFIF>

<CFIF frmTask IS 'add'>
		<CFIF frmWDDX is not "">
			<!--- need to create a WDDX structure to store in the table --->
			<CFSET aStruct = structNew()>
			<CFSET tmp = structInsert(aStruct,frmWDDX,"")>
			<CFWDDX ACTION="CFML2WDDX" INPUT="#aStruct#" OUTPUT="frmWDDX">

		</cfif>

		<cfset newFlagID = application.com.flag.createFlag
			(
			flagGroup  = frmFlagGroupID,
			flagType  = frmflagTypeID,
			name = frmName,
			helpText = frmHelpText,
			description = frmDescription,
			flagTextID = trim(frmFlagTextID),
			NamePhraseTextID = frmNamePhraseTextID,
			order = frmOrder,
			score = frmScore,
			active = frmActive,
			usevalidvalues = frmusevalidvalues,
			protected = frmprotected,
			lookup = frmLookup,
			linkstoentityType = frmlinkstoentityTypeID,
			wddx = frmwddx,
			createdby = request.relaycurrentuser.usergroupid,
			updatetime = updatetime,
			createDefaultTranslationName = #iif(isDefined("frmTranslateLabel"),de(true),de(false))#,
			formattingParameters = frmformattingParameters,
			showInConditionalEmails = frmShowInConditionalEmails
			)>


		<!--- changed from frmFlagId = getFlagID.flagID - reason for this is that later on frmFlagID is set to frmNextFlagID... this should sort out the permissions issue. --->
		<CFSET frmNextFlagID = newFlagID>

		<!---
		Removed WAB 2008/04/23
		this is done in the createFlag function above and we seemed to be getting locking errors here

		<cfscript>
			application.com.flag.updateFlagStructure(frmNextFlagID);
		</cfscript>
		--->

		<cfif isDefined('frmFlagGroupID')>
			<cfoutput>
				<script>
					parent.bottomLeft.location = "/profileManager/profileDetails.cfm?frmFlagGroupID=#jsStringFormat(frmFlagGroupID)#";
				</script>
			</cfoutput>
		</cfif>

		<CFSET message = "Attributes '#HTMLEditFormat(Replace(frmName,Chr(34),"`","ALL"))#' has been added successfully.">

<CFelseIF frmTask IS 'bulkLoad'>

	<cfset flagsAdded = "">
	<cfloop iNDEX="flagID" LIST="#frmFlagID#">
		<cfset Name = form["name_#flagID#"]>
		<cfif structKeyExists(form,"helpText_#flagID#")>
			<cfset helpText = form["helpText_#flagID#"]>
		<cfelse>
			<cfset helpText ="">
		</cfif>
		<!--- WAB 2008/09/22 added handling for score on bulk load --->
		<cfif structKeyExists(form,"score_#flagID#") and isNumeric (form["score_#flagID#"])>
			<cfset score = form["score_#flagID#"]>
		<cfelse>
			<cfset score = 0 >
		</cfif>

		<cfif trim(Name) is not "">
			<!--- NJH/WAB 2010/02/02 LID 2828 - added updateMemoryStructure --->
			<cfset newFlagID = application.com.flag.createFlag
				(
				flagGroup  = frmFlagGroupID,
				flagType  = frmflagTypeID,
				name = Name,
				helpText = helpText,
				<!--- description = Description, NJH left out for now when converting description to help text. can add later if needed--->
				flagTextID = '',
				NamePhraseTextID = frmNamePhraseTextID,
				order = frmOrder,
				score = score,
				active = 1,
				usevalidvalues = frmusevalidvalues,
				protected = frmProtected,
				lookup = frmLookup,
				linkstoentityType = frmlinkstoentityTypeID,
				wddx = frmwddx,
				createdby = request.relaycurrentuser.usergroupid,
				updatetime = updatetime,
				createDefaultTranslationName = #iif(isDefined("frmTranslateLabel"),de(true),de(false))#,
				formattingParameters = frmformattingParameters,
				updateMemoryStructure = false
				)>

			<!--- WAB 2010/02/02 improve performance --->
			<cfset frmOrder = frmOrder + 2>
			<!--- changed from frmFlagId = getFlagID.flagID - reason for this is that later on frmFlagID is set to frmNextFlagID... this should sort out the permissions issue. --->
			<cfset flagsAdded = listappend(flagsadded,newFlagID)>
			<!--- NJH/WAB 2010/02/02 LID 2828 - moved function below outside of loop --->
			<!--- <cfset application.com.flag.updateFlagStructure(newFlagID)> --->

		</cfif>


	</cfloop>

	<!--- NJH/WAB 2010/02/02 LID 2828 - update flag structure outside of loop for all flags just added--->
	<cfset application.com.flag.updateFlagStructure(flagsAdded)>


		<CFSET frmNextFlagID = listfirst(flagsAdded	)>

		<cfif isDefined('frmFlagGroupID')>
			<cfoutput>
				<script>
					parent.bottomLeft.location = "/profileManager/profileDetails.cfm?frmFlagGroupID=#jsStringFormat(frmFlagGroupID)#";
				</script>
			</cfoutput>
		</cfif>

			<CFSET message = "#listLen(flagsAdded)# attributes have been added successfully.">

<CFELSEIF frmTask IS 'ordering'>

	<!--- Amend flag ordering index --->

	<CFQUERY NAME="updateFlag" DATASOURCE="#application.SiteDataSource#">
		UPDATE Flag SET
		 OrderingIndex =  <cf_queryparam value="#frmOrder#" CFSQLTYPE="cf_sql_float" > ,
		 LastUpdatedBy = #request.relayCurrentUser.usergroupid#,
		 LastUpdated =  <cf_queryparam value="#Variables.updatetime#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
		 LastUpdatedByPerson = #request.relayCurrentUser.personID#
		WHERE FlagID =  <cf_queryparam value="#frmFlagID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</CFQUERY>

	<cfset x = application.com.flag.updateFlagStructure(frmFlagID)>

	<CFSET message = "Attributes '#HTMLEditFormat(frmFlagName)#' ordering has been changed to " & frmOrder & " successfully.">

<CFELSEIF frmTask IS 'edit'>
	<CFIF frmWDDX is not "" or (isDefined("frmDelWDDX"))>
		<!--- need to update the default WDDX packet --->
		<!--- first get it and then udpate it --->
		<CFQUERY NAME="getFlag" DATASOURCE="#application.SiteDataSource#">
		Select WDDXStruct from flag where FlagID =  <cf_queryparam value="#frmFlagID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</CFQUERY>

		<CFIF getFlag.WDDXStruct is "">
			<!--- no existing structure, so create a blank one --->
			<CFSET aStruct = structNew()>
		<CFELSE>
			<!--- create structure out of existing WDDX --->
			<CFWDDX ACTION="WDDX2CFML" INPUT="#getFlag.WDDXStruct#" OUTPUT="aStruct">
			<cfif isArray(aStruct)>
				<cfset aStruct = aStruct[1]>
			</cfif>
		</cfif>

		<!--- delete any items defined in frmDelWDDX --->
		<CFPARAM name="frmDelWDDX" default = "">
		<CFLOOP index="thisField" list="#frmDelWDDX#">
			<CFSET x= StructDelete(aStruct,thisField)>
		</cfloop>

		<!--- add field defined in frmWDDX --->
		<CFIF frmWDDX is not "">
			<CFSET x = StructInsert(aStruct,frmWDDX,"")>
		</cfif>

		<cfif isDefined("frmWDDXArray")>
			<cfset array = arraynew(1)>
			<cfset array[1] = aStruct>
			<cfset aStruct = array>
		</cfif>
		<CFWDDX ACTION="CFML2WDDX" INPUT="#aStruct#" OUTPUT="frmWDDX">
	</cfif>
	<!---  Note that edit screen should prevent FlagTextID being updated unless it is blank--->
	<CFQUERY NAME="updateFlag" DATASOURCE="#application.SiteDataSource#">
		UPDATE Flag SET
		 Name =  <cf_queryparam value="#Replace(frmName,Chr(34),"`","ALL")#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
		 helpText =  <cf_queryparam value="#frmHelpText#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
		 Description =  <cf_queryparam value="#frmDescription#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
		 FlagTextID =  <cf_queryparam value="#trim(frmflagtextID)#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
		 NamePhraseTextID =  <cf_queryparam value="#frmNamePhraseTextID#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
		 OrderingIndex =  <cf_queryparam value="#frmOrder#" CFSQLTYPE="cf_sql_float" > ,
		 <CFIF frmScore is not "">
		 score =  <cf_queryparam value="#frmScore#" CFSQLTYPE="CF_SQL_INTEGER" > ,</cfif>
		<CFIF frmWDDX is not "">
			WDDXStruct =  <cf_queryparam value="#frmWDDX#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
		</cfif>
			formattingParameters =  <cf_queryparam value="#frmformattingParameters#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
		<CFIF frmLinksToEntityTypeID is not "">
		LinksToEntityTypeID =  <cf_queryparam value="#frmLinksToEntityTypeID#" CFSQLTYPE="CF_SQL_INTEGER" >  ,
		<cfelse>
		LinksToEntityTypeID = null,
		</cfif>
		 Active =  <cf_queryparam value="#frmActive#" CFSQLTYPE="cf_sql_float" > ,
		usevalidvalues =  <cf_queryparam value="#frmusevalidValues#" CFSQLTYPE="CF_SQL_bit" >  ,
		protected =  <cf_queryparam value="#frmprotected#" CFSQLTYPE="CF_SQL_bit" >  ,
		lookup =  <cf_queryparam value="#frmlookup#" CFSQLTYPE="CF_SQL_bit" > ,
		ShowInConditionalEmails =  <cf_queryparam value="#frmShowInConditionalEmails#" CFSQLTYPE="CF_SQL_bit" > ,
		 LastUpdatedBy = #request.relayCurrentUser.usergroupid#,
		 LastUpdated =  <cf_queryparam value="#Variables.updatetime#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
		 lastUpdatedByPerson = <cf_queryparam value="#request.relayCurrentUser.personID#" CFSQLTYPE="CF_SQL_INTEGER" >
		WHERE FlagID =  <cf_queryparam value="#frmFlagID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</CFQUERY>

		<cfif isDefined("frmTranslateLabel")>
			<cfset application.com.flag.createFlagNameDefaultTranslation (flagid = frmflagid, name = frmname, flagTextID = frmflagTextID, namePhrasetextID = frmnamePhrasetextID, languageid = 1)>
		</cfif>

		<cfset application.com.flag.updateFlagStructure(frmFlagID)>

	<CFSET message = "Attributes '#HTMLEditFormat(Replace(frmName,Chr(34),"`","ALL"))#' details have been updated successfully.">

<CFELSEIF frmTask IS 'delete'>

	<!--- Delete Flag data associated with this flag --->

	<!--- Find type of Flag Group to identify data table
	WAB 2010/11/22 removed, use flagGroup Structure instead
	<CFQUERY NAME="getFlagGroupType" DATASOURCE="#application.SiteDataSource#">
	SELECT FlagTypeID
	FROM FlagGroup
	WHERE FlagGroupID = #frmFlagGroupID#
	</CFQUERY>
	--->
	<cfset flagGroupStructure = application.com.flag.getflagGroupStructure(frmFlagGroupID)>


	<!--- and delete the archived flags --->
	<CFQUERY NAME="DeleteFlags" DATASOURCE="#application.SiteDataSource#">
	DELETE FROM #flagGroupStructure.flagType.dataTableFullName#
	WHERE FlagID =  <cf_queryparam value="#frmFlagID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</CFQUERY>

	<!--- Delete Flag --->

	<CFQUERY NAME="ArchiveFlag" DATASOURCE="#application.SiteDataSource#">
	INSERT INTO xFlag
	(FlagID, FlagGroupID, Name, helpText,Description, OrderingIndex, Active, CreatedBy, Created, LastUpdatedBy, LastUpdated, ArchivedBy, Archived)
	SELECT FlagID, FlagGroupID, Name, helpText,Description, OrderingIndex, Active, CreatedBy, Created, LastUpdatedBy, LastUpdated, <cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >, <cf_queryparam value="#Variables.updatetime#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
  	FROM Flag
 	WHERE FlagID =  <cf_queryparam value="#frmFlagID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</CFQUERY>

	<!--- and delete the archived flag --->
	<CFQUERY NAME="DeleteFlags" DATASOURCE="#application.SiteDataSource#">
	DELETE FROM Flag
	WHERE FlagID =  <cf_queryparam value="#frmFlagID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</CFQUERY>

	<cfset x = application.com.flag.updateFlagStructure(frmFlagID)>

	<CFSET message = "Attributes '#HTMLEditFormat(frmFlagName)#' has been deleted successfully.">

</CFIF>

<!--- NJH Roadmap 2013 2013/04/02 - added support for profile images - to be used in the locator --->
<cfif listFindNoCase("add,edit,delete",form.frmTask)>
	<cfif form.frmTask eq "delete" or form.frmProfileImage neq "" or structKeyExists(form,"frmProfileImage_delete")>
		<cfset filename="Thumb_#frmFlagID#">
		<cfset filePath = "#application.paths.content#\linkImages\flag\#frmFlagID#">
		<cfdirectory name="profileImages" action="list" directory="#filePath#" filter="#filename#.*">
		<cfloop query="profileImages">
			<cffile action="delete" file="#profileImages.directory#\#profileImages.name#">
		</cfloop>
	</cfif>

	<cfif form.frmTask neq "delete" and form.frmProfileImage neq "">
		<cfset createThumbnailResult = application.com.relayImage.CreateThumbnail(targetSize=80,sourceFile="frmProfileImage",targetFilepath=filepath,targetFileName=filename,targetFileExtension="png")>
	</cfif>
</cfif>

<!--- Update this Flag Group's LastUpdated field as well --->
<CFQUERY NAME="updateFlagGroup" DATASOURCE="#application.SiteDataSource#">
	UPDATE FlagGroup SET
	 LastUpdatedBy = #request.relayCurrentUser.usergroupid#,
	 LastUpdated =  <cf_queryparam value="#Variables.updatetime#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
	 LastUpdatedByPerson = #request.relayCurrentUser.personID#
	WHERE FlagGroupID =  <cf_queryparam value="#frmFlagGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >
</CFQUERY>

<!--- </CFTRANSACTION> --->

<!--- NJH 2011/02/11 P-FNL075 Report Designer - add flagGroup to domain if frmUseInAllDomains is true --->
<cfquery name="flagGroupsInDomains" datasource="#application.siteDataSource#">
	select flagGroupId from flagGroupDomain where flagGroupId =  <cf_queryparam value="#frmFlagGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >
</cfquery>
<cfif flagGroupsInDomains.recordCount>
	<cfset application.com.jasperServer.applyFlagGroupsToDomains(flagGroupID=frmFlagGroupID)>
</cfif>


<CF_FormFieldsURLString
	AdditionalVariables="frmflagid"
>

<cfset frmFlagID = frmNextFLagID>
<CFINCLUDE TEMPLATE="#frmNext#.cfm">
<CF_ABORT>
<cflocation url="#frmNext#.cfm?message=#message#&#URLString#" addToken="false">