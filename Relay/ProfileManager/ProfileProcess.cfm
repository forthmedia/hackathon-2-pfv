<!--- �Relayware. All Rights Reserved 2014 --->
<!---
2016/09/05	NJH	JIRA PROD2016-1313 - change org approval to support both loc and/or organisation approval. Have called them 'accounts'
 --->

<cfif (structKeyExists(url,"frmPersonID") and ListLen(url.frmPersonID) eq 1) or (structKeyExists(url,"frmOrganisationID") and ListLen(url.frmOrganisationID) eq 1) or (structKeyExists(url,"frmLocationID") and ListLen(url.frmLocationID) eq 1)>
	<CFIF IsDefined("RowID") and isdefined("flagid") and isdefined("status")>
		<!--- Update the processflagdata row row --->

		<CFQUERY NAME="ChangeStatus" DATASOURCE="#application.SiteDataSource#">
			UPDATE processflagdata
			SET
				CurrentStatusTextID =  <cf_queryparam value="#status#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
				DateStatusChanged = getdate()
			WHERE
				ID =  <cf_queryparam value="#RowID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</CFQUERY>
	</CFIF>

	<CFIF isdefined("Rowid")>
		<CFSET Session.flagident=RowID>
	</CFIF>
	<CFSET frmNoBorder=true>
	<cfinclude template="\screen\redirector.cfm">
<cfelse>
	<cfif structKeyExists(url,"frmPersonID") and url.frmPersonID neq "">
		<cfset entity = "person">
	<cfelseif structKeyExists(url,"frmOrganisationID") and url.frmOrganisationID neq "">
		<cfset entity = "organisation">
	<cfelseif structKeyExists(url,"frmLocationID") and url.frmLocationID neq "">
		<cfset entity = "location">
	<cfelse>
		<cfset entity = "entity">
	</cfif>

	<cfoutput>
		<p>
		You have selected more than one #htmleditformat(entity)# to approve. Please close this window and select a single #htmleditformat(entity)# you wish to approve.
		</p>
	</cfoutput>
</cfif>