function DisplaySubType(obj,item,display) {
	if (jQuery('#frmFlagSubType_'+item).length > 0) {
		jQuery('#frmFlagSubType_'+item).remove();
	}
	var flagType = jQuery("#"+obj+item).val();
	if(flagType==14) {

		jQuery.ajax(
	    	{type:'get',
	        	url:'/webservices/callwebservice.cfc?wsdl&method=callWebService&webserviceName=relayScreens&methodName=GetCurrencies&returnFormat=json',
	        	data:0,
	        	dataType:'json',
	        	error: function() {},
	        	success: function(data,textStatus,jqXHR) {

					switch(display) {
					case "single":
						var selectNameA = 'frmFlagTypeCurrency';
						var selectNameB = 'frmFlagTypeDecimalPlaces';
						break;
					case "bulk":
						var selectNameA = 'frmFlagTypeCurrency_'+item;
						var selectNameB = 'frmFlagTypeDecimalPlaces_'+item;
						break;
					}2

					var defaultCurrency = "UD";
					var currenySelect = '<select name="'+selectNameA+'">';
					currenySelect += '<option value="UD">User Defined</option>';

					for (var c=0;c < data.CURRENCIES.length;c++) {
						var sel = "";
						if (data.CURRENCIES[c].currencyISOCode==defaultCurrency) {
							sel = " selected";
						}
						currenySelect += '<option value="' + data.CURRENCIES[c].currencyISOCode + '"' + sel + '>' + data.CURRENCIES[c].currencySign + ' ' + data.CURRENCIES[c].currencyName + '</option>';
					}

					currenySelect += "</select>";

					var defaultDecimalPlaces = 0;
					var decimalPlaces = '<select name="'+selectNameB+'">';

					for (var d=0;d <= 10;d++) {
						var sel = "";
						if (d==defaultDecimalPlaces) {
							sel = " selected";
						}
						decimalPlaces += '<option value="' + d + '"' + sel + '>' + d + '</option>';
					}

					decimalPlaces += "</select>";
					
					switch(display) {
						case "single":
							var newRow = '<tr id="frmFlagSubType_' + item + '"><td style="line-height:2.0em;">Currency | Decimal Places:</td><td style="line-height:2.0em;">' + currenySelect + ' | ' + decimalPlaces + '</td></tr>';
							break;
						case "bulk":
							var newRow = '<tr id="frmFlagSubType_' + item + '"><td colspan="3" style="line-height:2.0em;">Currency: ' + currenySelect + ' Decimal Places: ' + decimalPlaces + '</td></tr>';
							break;
					}

					jQuery(newRow).insertAfter(jQuery("#"+obj+item).closest('tr'));

				}
		});

	}
}


jQuery(document).ready(function() {
	if (jQuery('floatOver').length==0) {
		jQuery("body").append('<div id="floatOver" style="display:none;position:absolute;border-style:solid;border-color:orange;border-width:2px;z-index:1;background-color:#fff;"></div>');
	}
});


function popHelp(msg,obj,event) 
{
	var content ="<TABLE BORDER=0 CELLPADDING=2 CELLSPACING=0><TR><TD><p>"+msg+"</p></TD></TR></TABLE>";
	
	var floatoverObj = jQuery("#floatOver");

	floatoverObj.html(content);
	floatoverObj.show();

	event = (!window.event) ? event : window.event
	
	var x = event.clientX + document.body.scrollLeft +  1;
	var y = event.clientY + document.body.scrollTop + 1;
	
	var w=window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
	
	if (x > w - floatoverObj.clientWidth){
		x = w - floatoverObj.clientWidth;
	}

	floatoverObj.css("left",x + 5 + 'px');
	floatoverObj.css("top",y + 20 + 'px');
}

function killHelp() 
{
	jQuery("#floatOver").hide();
}
