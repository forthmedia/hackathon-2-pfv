<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Update flag group rights information --->


<CFSET updatetime = CreateODBCDateTime(Now())>

<CFQUERY NAME="checkDateTime" DATASOURCE="#application.SiteDataSource#">
	SELECT LastUpdated
	FROM FlagGroup 
	WHERE FlagGroupID =  <cf_queryparam value="#frmFlagGroupID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	AND LastUpdated >  <cf_queryparam value="#frmDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
</CFQUERY>

<CFIF #checkDateTime.RecordCount# IS NOT 0>	
	<!--- </CFTRANSACTION> --->
	<CFSET #message# = "The record could not be updated since it has been changed by another user since you last viewed it.">
	<CFINCLUDE TEMPLATE="#frmBack#.cfm">
	<CF_ABORT>
</CFIF>

<CFTRANSACTION>
<!--- Delete all flag group rights for these users
		Do by user since frmUserList may be too
		long for IN
--->

<!--- <CFQUERY NAME="DeleteFlagGroupRights" DATASOURCE="#application.SiteDataSource#">
DELETE FROM FlagGroupRights
WHERE UserID IN (#frmUserList#)
</CFQUERY> --->

<!--- Loop through each user and see if they have any rights to be set --->

<CFLOOP LIST="#frmUserList#" INDEX="UserID">

	<CFQUERY NAME="DeleteFlagGroupRights#UserID#" DATASOURCE="#application.SiteDataSource#">
	DELETE FROM FlagGroupRights
	WHERE UserID =  <cf_queryparam value="#UserID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	  AND FlagGroupID =  <cf_queryparam value="#frmFlagGroupID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</CFQUERY>

	<CFSET tView = "0">
	<CFSET tSearch = "0">
	<CFSET tEdit = "0">
	<CFSET tDownload = "0">

	<CFIF IsDefined('V#UserID#')>
		<CFSET tView = Evaluate('V#UserID#')>
	</CFIF>
	<CFIF IsDefined('S#UserID#')>
		<CFSET tSearch = Evaluate('S#UserID#')>
	</CFIF>
	<CFIF IsDefined('E#UserID#')>
		<CFSET tEdit = Evaluate('E#UserID#')>
	</CFIF>
	<CFIF IsDefined('D#UserID#')>
		<CFSET tDownload = Evaluate('D#UserID#')>
	</CFIF>

	<CFIF tView + tSearch + tEdit + tDownload GT 0>
		<CFQUERY NAME="AddRights" DATASOURCE="#application.SiteDataSource#">
		INSERT INTO FlagGroupRights
		(UserID, FlagGroupID, Viewing, Search, Edit, Download, CreatedBy, Created, LastUpdatedBy, LastUpdated)
		VALUES
		(<cf_queryparam value="#UserID#" CFSQLTYPE="CF_SQL_INTEGER" >,
		<cf_queryparam value="#frmFlagGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >,
		<cf_queryparam value="#tView#" CFSQLTYPE="CF_SQL_bit" >, <cf_queryparam value="#tSearch#" CFSQLTYPE="CF_SQL_bit" >, <cf_queryparam value="#tEdit#" CFSQLTYPE="CF_SQL_bit" >, <cf_queryparam value="#tDownload#" CFSQLTYPE="cf_sql_float" >, <cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >, <cf_queryparam value="#Variables.updatetime#" CFSQLTYPE="CF_SQL_TIMESTAMP" >, <cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >, <cf_queryparam value="#Variables.updatetime#" CFSQLTYPE="CF_SQL_TIMESTAMP" >)
		</CFQUERY>
	</CFIF>
		
</CFLOOP>

<CFSET message = "Flag Group '" & frmFlagGroupName & "' user rights have been updated.">

<!--- Update this Flag Group's LastUpdated field as well --->
<CFQUERY NAME="updateFlagGroup" DATASOURCE="#application.SiteDataSource#">
	UPDATE FlagGroup SET
	 LastUpdatedBy = #request.relayCurrentUser.usergroupid#,
	 LastUpdated =  <cf_queryparam value="#Variables.updatetime#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
	WHERE FlagGroupID =  <cf_queryparam value="#frmFlagGroupID#" CFSQLTYPE="CF_SQL_INTEGER" > 
</CFQUERY>

</CFTRANSACTION>

<CFINCLUDE TEMPLATE="#frmNext#.cfm">

<CF_ABORT>

