<!--- �Relayware. All Rights Reserved 2014 --->
<!--- the application .cfm checks for read adminTask privleges --->

<!--- Mod History
  	250200 WAB got link to valid values to work
  	1999-07-07 WAB adjusted so that all those with admin rights can see and edit all flags
	  2000-07-01 SWJ	changed the header area to use styles
		2001-10-20			SWJ			Changed over to profile manager

	2005-10-17	WAB 	implemented function for testing whether user has rights to edit flag/flaggroup definition
	2008-06-03	NYF		P-CRL501 - added protection for system flags from deletion:  added ', flag.isSystem' to getFlags query 'and IsSystem eq 0' inside the <CFIF checkPermission.edit GT 0>  just before HREF="JavaScript:delFlag
	2014/04/09	NJH		RW Roadmap 2014 2 - Don't provide option of deleting a profile attribute if the profile is being used in the connector
	2014-08-26	RPW		Adding Profile Attributes double trouble
--->

<!--- Get country groups for this user to find which FlagGroups with Scope they can view --->

<!--- Find all Country Groups for this Country --->
<!--- Note: no User Country rights implemented for site currently --->
<CFQUERY NAME="getCountryGroups" DATASOURCE="#application.SiteDataSource#">
	SELECT DISTINCT b.CountryID
	 FROM Country AS a, Country AS b, CountryGroup AS c
	WHERE (b.ISOcode IS null OR b.ISOcode = '')
	  AND c.CountryGroupID = b.CountryID
	  AND c.CountryMemberID = a.CountryID
	  #application.com.rights.getRightsFilterWhereClause(entityType="country",alias="a").whereClause#
	 <!---  AND a.CountryID  IN ( <cf_queryparam value="#Variables.CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) ---> <!--- User Country rights --->
	ORDER BY b.CountryID
</CFQUERY>

<CFSET UserCountryList = "0,#request.relayCurrentUser.CountryList#">
<CFIF getCountryGroups.CountryID IS NOT "">
	<!--- top record (or only record) is not null --->
	<CFSET UserCountryList =  UserCountryList & "," & ValueList(getCountryGroups.CountryID)>
</CFIF>

<CFQUERY NAME="getFlagGroup" datasource="#application.sitedatasource#" MAXROWS=1 DEBUG>
SELECT FlagGroup.*, fet.tablename ,fgParent.name as ParentName ,
ug.Name AS LastUpdatedBy, FlagGroup.FlagGroupTextID,
(SELECT CountryDescription FROM Country WHERE CountryID = FlagGroup.Scope) AS flaggroup_region,
(SELECT p.Name FROM FlagGroup AS p WHERE FlagGroup.ParentFlagGroupID <> 0 AND FlagGroup.ParentFlagGroupID = p.FlagGroupID) AS parent,
(SELECT Name FROM FlagType WHERE FlagGroup.FlagTypeID <> 1 AND FlagGroup.FlagTypeID = FlagType.FlagTypeID) AS type
FROM
	FlagGroup
		left join
	UserGroup AS ug on FlagGroup.LastUpdatedBy = ug.UserGroupID
		inner join
	flagEntityType fet on fet.entityTypeID = FlagGroup.entityTypeID
		left join
	FlagGroup as fgParent on flagGroup.parentFlagGroupID = 	fgParent.flagGroupID
WHERE FlagGroup.FlagGroupID =  <cf_queryparam value="#frmFlagGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >
AND FlagGroup.Scope  IN ( <cf_queryparam value="#UserCountryList#" CFSQLTYPE="CF_SQL_Integer"  list="true"> )
AND (FlagGroup.CreatedBy=#request.relayCurrentUser.usergroupid#
	OR (FlagGroup.ViewingAccessRights = 0 AND FlagGroup.Viewing <> 0)
	or exists (	SELECT 1 FROM rights as r, RightsGroup as rg, SecurityType AS s
				WHERE  r.SecurityTypeID = s.SecurityTypeID
				AND s.ShortName  = 'adminTask'
				AND r.usergroupid = rg.usergroupid
				AND rg.PersonID=#request.relayCurrentUser.personid# )

	OR (FlagGroup.ViewingAccessRights <> 0
		AND EXISTS (SELECT 1 FROM FlagGroupRights
			WHERE FlagGroup.FlagGroupID = FlagGroupRights.FlagGroupID
			AND FlagGroupRights.UserID=#request.relayCurrentUser.personid#
			AND FlagGroupRights.Viewing <> 0)))

</CFQUERY>


<!---
WAB 2005-10-17 replaced with call to function

<!--- Check if this user has Edit rights for this group as well (as view) --->
<!--- wab added code to allow administrators to edit all flags --->
<CFQUERY NAME="checkEdit" DATASOURCE="#application.SiteDataSource#">
	SELECT 1
	FROM FlagGroup
	WHERE FlagGroupID = #frmFlagGroupID#
	AND (CreatedBy=#request.relayCurrentUser.usergroupid#
		or exists (	SELECT 1 FROM rights as r, RightsGroup as rg, SecurityType AS s
					WHERE  r.SecurityTypeID = s.SecurityTypeID
					AND s.ShortName  = 'ProfileTask'
					AND r.usergroupid = rg.usergroupid
					AND rg.PersonID=#request.relayCurrentUser.personid# )
		OR (EditAccessRights = 0 AND Edit <> 0)
		OR (EXISTS (SELECT 1 FROM FlagGroupRights AS t WHERE EditAccessRights <> 0
	                AND FlagGroupID = t.FlagGroupID
	                AND t.UserID=#request.relayCurrentUser.usergroupid#
	                AND t.Edit <> 0)))
</CFQUERY>
 --->
<cfset hasRightsToEdit = application.com.flag.doesCurrentUserHaveRightsToEditFlagGroupDefinition(frmFlagGroupID)>

<CFIF getFlagGroup.FlagTypeID IS NOT 1>
	<!--- 2014-08-26	RPW	Adding Profile Attributes double trouble
	Added join condition to vConnectorMapping m1 of "AND ISNULL(m1.column_relayware,'') != ''"
	If flagTextID is blank (it is blank not a NULL) then it joins to all rows in Flag of this entitytypeid and blank flagTextID --->
	<CFQUERY NAME="getFlags" DATASOURCE="#application.SiteDataSource#">
	SELECT distinct Flag.FlagID, Flag.Name, flag.currentCount, Flag.FlagTextID, Flag.OrderingIndex,
	Flag.Active, Flag.LastUpdated, ug.Name AS LastUpdatedBy, flag.score,
	flag.wddxStruct, flag.useValidValues
	<!--- NYF 2008-06-03 P-CRL501 --->
	, flag.isSystem,
	case when cm.value_relayware is not null or m1.ID is not null then 1 else 0 end as inConnectorMapping
	FROM Flag
		left join  UserGroup AS ug on Flag.LastUpdatedBy = ug.UserGroupID
		left join
			(vConnectorMapping m inner join connectorColumnValueMapping cm on m.ID = cm.connectorMappingID) on m.column_relayware =  <cf_queryparam value="#getFlagGroup.FlagGroupTextID[1]#" CFSQLTYPE="CF_SQL_VARCHAR" > and cm.value_relayware = flagTextID and m.entityTypeID=#getFlagGroup.entityTypeID[1]#
		left join vConnectorMapping m1 on m1.column_relayware = flagTextID and m1.entityTypeID=#getFlagGroup.entityTypeID[1]# AND ISNULL(m1.column_relayware,'') != ''
	WHERE Flag.FlagGroupID =  <cf_queryparam value="#frmFlagGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >
	ORDER BY active DESC, Flag.OrderingIndex ASC, Flag.Name ASC
	</CFQUERY>
<cfelse>
	<!--- get child flagGroups --->
			<CFQUERY NAME="getChildFlagGroups" DATASOURCE="#application.SiteDataSource#">
			SELECT
			flagGroup.*,
			ug.name as lastUpdatedByName,
			(SELECT Count(FlagID) FROM Flag, FlagGroup AS q WHERE Flag.FlagGroupID = q.FlagGroupID AND FlagGroup.FlagGroupID = q.FlagGroupID) As flag_count

			FROM FlagGroup
			INNER JOIN FlagType ft ON ft.flagTypeID = flagGroup.flagTypeID
			left join  UserGroup AS ug on Flaggroup.LastUpdatedBy = ug.UserGroupID
			WHERE ParentFlagGroupID =  <cf_queryparam value="#getFlagGroup.flagGroupid#" CFSQLTYPE="CF_SQL_INTEGER" >
			AND FlagGroup.Scope  IN ( <cf_queryparam value="#UserCountryList#" CFSQLTYPE="CF_SQL_Integer"  list="true"> )
			AND (FlagGroup.CreatedBy=#request.relayCurrentUser.usergroupid#
				or exists (		SELECT 1 FROM rights as r, RightsGroup as rg, SecurityType AS s
									WHERE  r.SecurityTypeID = s.SecurityTypeID
									AND s.ShortName  = 'AdminTask'
									AND r.usergroupid = rg.usergroupid
									AND rg.PersonID=#request.relayCurrentUser.personid# )
				OR (FlagGroup.ViewingAccessRights = 0 AND FlagGroup.Viewing <> 0)
				OR (FlagGroup.ViewingAccessRights <> 0
					AND EXISTS (SELECT 1 FROM FlagGroupRights
						WHERE FlagGroup.FlagGroupID = FlagGroupRights.FlagGroupID
						AND FlagGroupRights.UserID=#request.relayCurrentUser.usergroupid#
						AND FlagGroupRights.Viewing <> 0)))

			ORDER BY flagGroup.orderingIndex,flagGroup.Name
			</CFQUERY>



</CFIF>

<CFIF getFlagGroup.RecordCount IS NOT 1>
	<CFSET message = "You do not appear to have sufficient rights to access this Flag Group information. <P>Please contact support if you need further information.">
	<cfset messageType="info">
	<cfinclude template="/profileManager/profileList.cfm">
	<CF_ABORT>
</CFIF>





<cf_head>
	<cf_title>Flag Group Show</cf_title>
<SCRIPT type="text/javascript">

<!--
        function editDetail(ID) {
                var form = document.FlagGroupForm;
                form.frmFlagGroupID.value = ID;
                form.submit();
                return;
        }

        function delFlag(ID,flagname) {
                var form = document.DelFlagForm;
                form.frmFlagID.value = ID;
				form.frmFlagName.value = flagname;
				if (confirm("Are you sure you want to permanently delete the flag '" + flagname + "' ?\n\nThis will also remove any entity data associated with the flag.")) {
                	form.submit();
                	return;
				}
        }

		function editFlag(ID) {
                var form = document.FlagForm;
                form.frmFlagID.value = ID;
                form.submit();
                return;
        }


        function validValues(fieldName) {
				newWin = window.open ('<CFOUTPUT></cfoutput>/dataTools/validvalues.cfm?frmFieldName='+fieldName+'&frmCountryID=0','PopUp','height=500,width=700,scrollbars=1,resizable=1')
				newWin.focus()
		}


<CFIF getFlagGroup.FlagTypeID IS NOT 1>
		function addFlag() {
                var form = document.NewFlagForm;
                form.submit();
                return;
        }

		function bulkAddFlag() {
                var form = document.BulkNewFlagForm;
                form.submit();
                return;
        }
</CFIF>

		function addProfile(entityTypeID,parentFlagGroupID) {
                var form = document.NewFlagGroupForm;
				form.frmEntityTypeID.value = entityTypeID
        		if (parentFlagGroupID) 	form.frmParentFlagGroupID.value = parentFlagGroupID
		        form.submit();
                return;
        }

		function amendOrdering(FlagID,flagname,index) {
                var form = document.AmendOrderingForm;
                form.frmFlagID.value = FlagID;
				form.frmFlagName.value = flagname;
				form.frmOrder.value = index + 1;
                form.submit();
                return;
        }
//-->

</SCRIPT>
</cf_head>


<CFSET ThisPageName="profileDetails.cfm">
<cfinclude template="/profileManager/profileTopHead.cfm">
<CENTER>

<CFIF IsDefined('message')>
	<cfparam name="messageType" default="success">
	<P><CFOUTPUT>#application.com.relayUI.message(message=message,type=messageType)#</CFOUTPUT>

</CFIF>
<table>
	<CFOUTPUT>
	<form action="/profileManager/profileEdit.cfm" method="post" name="FlagGroupForm" id="FlagGroupForm" target="bottomRight">
		<INPUT TYPE="HIDDEN" NAME="frmFlagGroupID" VALUE="0">
		<INPUT TYPE="HIDDEN" NAME="frmBack" VALUE="/profileManager/profileDetails">
		<INPUT TYPE="HIDDEN" NAME="frmTask" VALUE="edit">


<!--- 	<TR><TH COLSPAN="2" ALIGN="CENTER"><B>Phr_Sys_PROFILEDETAILS</B></TH></TR> --->
	<TR><TD COLSPAN="2" ALIGN="CENTER">

		<TABLE>
			<TR><TD colspan ="2">
				<cf_divOpenClose divid="flagGroupDetails" startOpen="false"><cf_divOpenClose_Image>	</cf_divOpenClose >
					<CFIF Trim(getFlagGroup.Name) IS "">-<CFELSE>
						#HTMLEditFormat(Trim(getFlagGroup.Name))#
					</CFIF>
					(<CFIF Trim(getFlagGroup.FlagGroupID) IS "">-<CFELSE>
						#HTMLEditFormat(Trim(getFlagGroup.FlagGroupID))#
					</CFIF>)
					<CFIF Trim(getFlagGroup.FlagGroupTextID) IS "">-<CFELSE>
						(<I>#HTMLEditFormat(Trim(getFlagGroup.FlagGroupTextID))#</I>)
					</CFIF>
					<cfif getFlagGroup.parentFlagGroupID is not 0>
						<BR>Parent: <A HREF="profileDetails.cfm?frmFlagGroupID=#getFlagGroup.parentFlagGroupID#">#htmleditformat(getFlagGroup.parentName)#</A>
					</cfif>
			</TD></TR>
			<tr><td>

			</td></tr>
		</table>


		<table id="flagGroupDetails" class="table table-hover table-striped">
			<TR><TD COLSPAN="2"><HR SIZE="2" COLOR="NAVY"></TD></TR>
			<TR><TD VALIGN="TOP">
					<b>Phr_Sys_Type:</b>
			</TD>
			<TD VALIGN="TOP">
					<CFIF Trim(getFlagGroup.type) IS "">Group<CFELSE>
					#HTMLEditFormat(Trim(getFlagGroup.type))#
						</CFIF>
			</TD></TR>
			<TR><TD VALIGN="TOP">
					<b>Phr_Sys_ScopeEntityType:</b>
			</TD>
			<TD VALIGN="TOP">
					<CFIF getFlagGroup.flaggroup_region IS "">All countries and regions<CFELSE>#HTMLEditFormat(getFlagGroup.flaggroup_region)#</CFIF>
					(#htmleditformat(getFlagGroup.tablename)#)
			</TD></TR>
			<TR><TD COLSPAN="2"><HR SIZE="2" COLOR="NAVY"></TD></TR>
			<TR><TD VALIGN="TOP">
					<b>Phr_Sys_AccessRights:</b>
			</TD>
			<TD VALIGN="TOP">
					<p>
						<b>Viewing:</b> <CFIF  #getFlagGroup.ViewingAccessRights# IS 0>General	<CFELSE>User Specific</CFIF>
					</p>
					<p>
						<b>Edit:</b> <CFIF  #getFlagGroup.EditAccessRights# IS 0>General<CFELSE>User Specific</CFIF>
					</p>
					<p>
						<b>Download:</b> <CFIF  #getFlagGroup.DownloadAccessRights# IS 0>General	<CFELSE>User Specific</CFIF>
					</p>
					<p>
						<b>Search:</b> <CFIF  #getFlagGroup.SearchAccessRights# IS 0>General<CFELSE>User Specific</CFIF>
					</p>
					<CFIF (#getFlagGroup.ViewingAccessRights# + #getFlagGroup.EditAccessRights# +#getFlagGroup.DownloadAccessRights# +#getFlagGroup.SearchAccessRights# is not 0) AND checkPermission.edit GT 0>
					<p>
						<A class="btn btn-primary" HREF="/profileManager/flagGroupUsers.cfm?frmFlagGroupID=#frmFlagGroupID#&frmBack=/profileManager/profileDetails" target="bottomRight">Edit User Rights</A>
					</p>
					</CFIF>
			</TD></TR>
			<TR><TD VALIGN="TOP">
					<b>Phr_Sys_Expires:</b>
			</TD>
			<TD VALIGN="TOP">
					#DateFormat(getFlagGroup.Expiry,"DD-MMM-YYYY")#
			</TD></TR>
			<TR><TD VALIGN="TOP">
					<b>Phr_Sys_Active:</b>
			</TD>
			<TD VALIGN="TOP">
					<CFIF #getFlagGroup.Active# IS 0>No (Not displayed)<CFELSE>Yes</CFIF>
			</TD></TR>
			<TR><TD VALIGN="TOP">
					<b>Phr_Sys_LastUpdatedDateBy:</b>
			</TD>
			<TD VALIGN="TOP">
					#DateFormat(getFlagGroup.LastUpdated,"DD-MMM-YYYY")#
					#htmleditformat(getFlagGroup.LastUpdatedBy)#
			</TD></TR>
			<TR><TD COLSPAN="2"><HR SIZE="2" COLOR="NAVY"></TD></TR>
		</TABLE>
</CFOUTPUT>

	</TD></TR>

		<TR><TD ALIGN="CENTER" COLSPAN=2>
				<CFIF getFlagGroup.FlagTypeID IS NOT 1>
					<CFIF getFlags.RecordCount IS 1>
						1 Profile Attribute
					<CFELSE>
						<CFOUTPUT>Phr_Sys_ProfileAttributesFound: #getFlags.RecordCount#</CFOUTPUT>
					</CFIF>
				<CFELSE>
					<CFOUTPUT>Child Profiles Found: #getChildFlagGroups.recordcount#</CFOUTPUT>
<!--- 					Phr_Sys_ParentProfilesibling --->
				</CFIF>
			<BR></TD>
		</TR>
</FORM>
</TABLE>

<CFIF getFlagGroup.FlagTypeID IS NOT 1>
	<cfoutput>
		<FORM METHOD="POST" ACTION="/profileManager/flagEdit.cfm" NAME="FlagForm" target="bottomRight">
			<CF_INPUT TYPE="HIDDEN" NAME="frmFlagGroupID" VALUE="#frmFlagGroupID#">
<!--- 			<INPUT TYPE="HIDDEN" NAME="flagGroupName" VALUE="#HTMLEditFormat(getFlagGroup.Name)#">
			<INPUT TYPE="HIDDEN" NAME="flagGroupDescription" VALUE="#HTMLEditFormat(getFlagGroup.Description)#">
			<CF_INPUT TYPE="HIDDEN" NAME="frmFlagTypeID" VALUE="#getFlagGroup.FlagTypeID#">
			<INPUT TYPE="HIDDEN" NAME="flagGroupScEn" VALUE="<CFIF getFlagGroup.flaggroup_region IS "">All countries and regions<CFELSE>#HTMLEditFormat(getFlagGroup.flaggroup_region)#</CFIF> (<CFIF getFlagGroup.EntityTypeID IS 0>Person<CFELSE>Location</CFIF>)">
 --->			<INPUT TYPE="HIDDEN" NAME="frmBack" VALUE="/profileManager/profileDetails">
			<INPUT TYPE="HIDDEN" NAME="frmTask" VALUE="edit">
			<INPUT TYPE="HIDDEN" NAME="frmFlagID" VALUE="">
	</cfoutput>
	<CFIF getFlags.RecordCount IS 0>

		<P>Phr_Sys_NoProfileAttributes.

	<CFELSE>

		<table class="table table-hover table-striped">
		<TR>
 			<Th ALIGN="CENTER" VALIGN="BOTTOM" NOWRAP>
				Phr_Sys_Active
			</Th>
			<Th VALIGN="BOTTOM">
				Phr_Sys_NameProfileTextID<BR>Phr_Sys_Clicktoedit
			</Th>
			<Th VALIGN="BOTTOM" NOWRAP>
				Phr_Sys_LastUpdatedDateBy
			</Th>
<!---
	WAB  2005-10-17 can't see why everyone shouldn't see score and current count
<CFIF checkEdit.RecordCount IS NOT 0>
				<!--- <Th ALIGN="CENTER" VALIGN="BOTTOM">
					Phr_Sys_Ordering<BR>Phr_Sys_Index
				</Th> --->
 --->				<Th ALIGN="CENTER" VALIGN="BOTTOM">
					Phr_Sys_Score
				</Th>
				<Th ALIGN="CENTER" VALIGN="BOTTOM">
					Phr_Sys_CurrentCount
				</Th>
				<CFIF getFlags.UseValidValues and getFlags.wddxStruct is not "">
					<th>
						Phr_Sys_ValidValues
					</th>
				</CFIF>
				<CFIF checkPermission.edit GT 0>
					<Th ALIGN="CENTER" VALIGN="BOTTOM">
						Phr_Sys_Delete
					</Th>
				</CFIF>
			<!--- </CFIF> --->

		</TR>

		<CFLOOP QUERY="getFlags">

		<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
			<TD ALIGN="CENTER" VALIGN="MIDDLE">
				<CFIF Active IS 0>No<CFELSE>Yes</CFIF>
			</TD>

			<TD VALIGN="MIDDLE">
				<CFIF checkPermission.edit GT 0>
				<CFIF Trim(Name) IS "">&nbsp;<CFELSE>
				<CFOUTPUT><A HREF="JavaScript:editFlag('#FlagID#');" class="smallLink">#htmleditformat(Trim(Name))#</A> (#htmleditformat(FlagID)# #htmleditformat(FlagTextID)#)</CFOUTPUT>
				</CFIF>
				<CFELSE>
					<CFOUTPUT>#htmleditformat(Trim(Name))# (#htmleditformat(FlagID)# #htmleditformat(FlagTextID)#)</CFOUTPUT>
				</CFIF>
			</TD>
			<TD VALIGN="MIDDLE">
				<CFOUTPUT>#DateFormat(LastUpdated,"DD-MMM-YYYY")#<br>
				#htmleditformat(LastUpdatedBy)#</CFOUTPUT>
			</TD>
			<CFIF hasRightsToEdit>
				<!--- <TD ALIGN="CENTER" VALIGN="MIDDLE">
					<CFIF checkPermission.edit GT 0>
					<SELECT NAME="frmOrder" onChange="JavaScript:amendOrdering(<CFOUTPUT>#FlagID#,'#Replace(Name,"'","\'","ALL")#'</CFOUTPUT>,this.selectedIndex);"><CFLOOP INDEX="LoopCount" FROM="1" TO="20">
						<CFOUTPUT>
						<CFIF LoopCount IS OrderingIndex>
							<OPTION VALUE=#LoopCount# SELECTED> #LoopCount#
						<CFELSE>
							<OPTION VALUE=#LoopCount#> #LoopCount#
						</CFIF>
						</CFOUTPUT>
					</CFLOOP></SELECT>
					<CFELSE>
					<CFOUTPUT>#OrderingIndex#</CFOUTPUT>
					</CFIF>
				</TD> --->
				<TD ALIGN="CENTER" VALIGN="MIDDLE">
					<CFOUTPUT>#htmleditformat(Score)#</CFOUTPUT>
				</TD>
				<TD ALIGN="CENTER" VALIGN="MIDDLE">
					<CFOUTPUT>#htmleditformat(CurrentCount)#</CFOUTPUT>
				</TD>
				<CFIF getFlags.UseValidValues and getFlags.wddxStruct is not "">
					<TD ALIGN="CENTER" VALIGN="MIDDLE">
					<CFOUTPUT><A HREF="JavaScript:validValues('Flag.#FlagTextID#')">ValidValues</A></CFOUTPUT>
					</TD>
				</cfif>
					<!--- NYF 2008-06-03 P-CRL501 - add  and IsSystem eq 0 --->
				<CFIF checkPermission.edit GT 0 and IsSystem eq 0 and not inConnectorMapping>
					<TD ALIGN="CENTER" VALIGN="MIDDLE">
						<CFOUTPUT><A HREF="JavaScript:delFlag('#FlagID#','#Replace(Name,"'","\'","ALL")#');"><IMG SRC="/images/MISC/iconDeleteCross.gif" BORDER=0></A></CFOUTPUT>
					</TD>
				</CFIF>
			</CFIF>
		</TR>
		</CFLOOP>

		</TABLE>

	</CFIF>

	</FORM>
	<cfoutput>
		<FORM METHOD="POST" ACTION="/profileManager/flagEdit.cfm" NAME="NewFlagForm" target="bottomRight">
			<CF_INPUT TYPE="HIDDEN" NAME="frmFlagGroupID" VALUE="#frmFlagGroupID#">
			<INPUT TYPE="HIDDEN" NAME="frmBack" VALUE="/profileManager/profileDetails">
			<INPUT TYPE="HIDDEN" NAME="frmTask" VALUE="add">
		</FORM>

		<FORM METHOD="POST" ACTION="/profileManager/flagBulkLoad.cfm" NAME="BulkNewFlagForm" target="bottomRight">
			<CF_INPUT TYPE="HIDDEN" NAME="frmFlagGroupID" VALUE="#frmFlagGroupID#">
			<INPUT TYPE="HIDDEN" NAME="frmBack" VALUE="/profileManager/profileDetails">
		</FORM>


	</cfoutput>

<cfelse>

	<CFIF getChildflagGroups.RecordCount IS 0>


	<CFELSE>

		<table class="table table-hover table-striped">
		<TR>
 			<Th ALIGN="CENTER" VALIGN="BOTTOM" NOWRAP>
				Phr_Sys_Active
			</Th>
			<Th VALIGN="BOTTOM">
				Phr_Sys_NameProfileTextID<BR>Phr_Sys_Clicktoedit
			</Th>
			<Th VALIGN="BOTTOM" NOWRAP>
				Phr_Sys_LastUpdatedDateBy
			</Th>
			<Th VALIGN="BOTTOM" NOWRAP>
				# of <BR>Attributes
			</Th>

		</TR>

		<CFOUTPUT QUERY="getChildflagGroups">

		<TR>
 			<TD >
				<CFIF Active IS 0>No<CFELSE>Yes</CFIF>
			</TD>
			<TD>
				<CFIF checkPermission.edit GT 0>
				<CFIF Trim(Name) IS "">&nbsp;<CFELSE>
				<A HREF="JavaScript:editDetail('#FlagGroupID#');" class="smallLink">#htmleditformat(Trim(Name))#</A> (#htmleditformat(FlagGroupID)# #htmleditformat(FlagGroupTextID)#)
				</CFIF>
				<CFELSE>
					#htmleditformat(Trim(Name))# (#htmleditformat(FlagGroupID)# #htmleditformat(FlagGroupTextID)#)
				</CFIF>

			</TD>
			<TD>
				#DateFormat(LastUpdated,"DD-MMM-YYYY")#<br>
				#htmleditformat(LastUpdatedByName)#

			</TD>
			<TD>
				#htmleditformat(flag_count)#
			</TD>
		</TR>

		</CFOUTPUT>
	 	</table>
	 </cfif>



</CFIF>

<CFIF hasRightsToEdit IS NOT 0>
	<cfoutput>
	<FORM METHOD="POST" ACTION="/profileManager/flagUpdateTask.cfm" NAME="AmendOrderingForm">
		<CF_INPUT TYPE="HIDDEN" NAME="frmDate" VALUE="#CreateODBCDateTime(now())#">
		<CF_INPUT TYPE="HIDDEN" NAME="frmFlagGroupID" VALUE="#frmFlagGroupID#">
		<INPUT TYPE="HIDDEN" NAME="frmFlagID" VALUE="">
		<INPUT TYPE="HIDDEN" NAME="frmFlagName" VALUE="">
		<INPUT TYPE="HIDDEN" NAME="frmOrder" VALUE="">
		<INPUT TYPE="HIDDEN" NAME="frmNext" VALUE="/profileManager/profileDetails">
		<INPUT TYPE="HIDDEN" NAME="frmTask" VALUE="ordering">
	</FORM>

	<FORM METHOD="POST" ACTION="/profileManager/flagUpdateTask.cfm" NAME="DelFlagForm">
		<CF_INPUT TYPE="HIDDEN" NAME="frmDate" VALUE="#CreateODBCDateTime(now())#">
		<CF_INPUT TYPE="HIDDEN" NAME="frmFlagGroupID" VALUE="#frmFlagGroupID#">
		<CF_INPUT TYPE="HIDDEN" NAME="frmFlagTypeID" VALUE="#getFlagGroup.FlagTypeID#">
		<INPUT TYPE="HIDDEN" NAME="frmFlagID" VALUE="">
		<INPUT TYPE="HIDDEN" NAME="frmFlagName" VALUE="">
		<INPUT TYPE="HIDDEN" NAME="frmNext" VALUE="/profileManager/profileDetails">
		<INPUT TYPE="HIDDEN" NAME="frmTask" VALUE="delete">
	</FORM>

		<FORM METHOD="POST" ACTION="/profileManager/profileEdit.cfm" NAME="NewFlagGroupForm" target="bottomRight">
			<INPUT TYPE="HIDDEN" NAME="frmTask" VALUE="add">
			<INPUT TYPE="HIDDEN" NAME="frmEntityTypeID" VALUE="0">
			<INPUT TYPE="HIDDEN" NAME="frmParentFlagGroupID" VALUE="0">
			<INPUT TYPE="HIDDEN" NAME="frmBack" VALUE="">
		</FORM>

	</cfoutput>
</CFIF>