<!--- �Relayware. All Rights Reserved 2014 --->

<!---

File name:			profileEntityList.cfm
Author:				BOCC
Date created:		Long time ago!

Description:		Edits theflag Group

Date Tested:
Tested by:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2001-10-20			SWJ			Changed over to profile manager
2002-02-02			SWJ			Modified query so that you can pass a list of 
								entitytypeidIDs to it

--->
<cf_translate>

<cfparam name="entIDs" default="">

<CFQUERY NAME="GetEntityTypes" DATASOURCE="#application.SiteDataSource#">
	SELECT flagEntityType.entitytypeid, flagEntityType.tablename, 
		 (select count(*) from FlagGroup 
		 	WHERE flagEntityType.entitytypeid = FlagGroup.EntityTypeID) AS CountFlagGroups, 
	    (select count(*) from flag 
			INNER JOIN FlagGroup ON flag.flagGroupID = FlagGroup.flagGroupID 
			where flagEntityType.entitytypeid = FlagGroup.EntityTypeID) AS CountFlags
	FROM flagEntityType 
		INNER JOIN FlagGroup ON flagEntityType.entitytypeid = FlagGroup.EntityTypeID 
	<cfif entIDs neq "">
		WHERE flagEntityType.entitytypeid  in ( <cf_queryparam value="#entitytypeidIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	</cfif>
		GROUP BY flagEntityType.entitytypeid, flagEntityType.tablename
		ORDER BY flagEntityType.tablename
	
</CFQUERY>

<cf_head>
	<cf_title>Flag Group Entity List</cf_title>
	
	<SCRIPT type="text/javascript">
	<!--
		function addProfile() {
	                var form = document.NewFlagGroupForm;
	                form.submit();
	                return;
	        }
	
	//-->
	</script>
</cf_head>


<CFSET ThisPageName="profileEntityList.cfm">
<cfinclude template="/profileManager/profileTopHead.cfm">
<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
	<TR>
		<TD COLSPAN="3">
		<P>Phr_Sys_DefinedProfilesTable.</P>
		</TD>
	</TR>
	<TR>
		<TH>Phr_Sys_EntitieswithProfiles<BR>Phr_Sys_Clicktoedit</TH>
		<TH>Phr_Sys_NumberofProfiles</TH>
		<TH>Phr_Sys_NumberofProfileAttributes</TH>
	</TR>
<CFOUTPUT QUERY="GetEntityTypes">
	<TR>
		<TD><A HREF="/profileManager/profileList.cfm?frmEntityTypeID=#entitytypeid#&entityType=#tablename#&searchPhrase=" TITLE="Entity Type ID: #entitytypeid#" CLASS="smallLink">
			#htmleditformat(tablename)# profiles</A></TD>
		<TD ALIGN="center">#htmleditformat(CountFlagGroups)#</TD>
		<TD ALIGN="center">#htmleditformat(CountFlags)#</TD>
	</TR>
</CFOUTPUT></TABLE>


<cfoutput>
	<FORM METHOD="POST" ACTION="/profileManager/profileEdit.cfm" NAME="NewFlagGroupForm">
		<INPUT TYPE="HIDDEN" NAME="frmBack" VALUE="/profileManager/flagGroupList">
		<INPUT TYPE="HIDDEN" NAME="frmTask" VALUE="add">
	</FORM>
</cfoutput>






</cf_translate>