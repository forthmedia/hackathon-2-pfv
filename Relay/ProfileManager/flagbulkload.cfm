<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Amendment History

WAB flag Bulk Load.cfm 


WAB 2008-09-16 added score to the bulk load for bulk loading of quizes - not tested yet
Wab 2008-09-22 added call to prototype.js since a function in tablemanipulation.js seemed to require it

2012-09-21 PPB Case 430402 add translate="true" to restrictKeyStrokes.js call

--->
	
<cfparam name="frmBack" default="">
<cfparam name="pagesBack" type="numeric" default="0">
<cfset frmtask = "bulkLoad">
<cfset pagesBack = pagesBack+1>


<cf_includejavascriptonce template = "/javascript/restrictkeystrokes.js" translate="true">		<!--- 2012-09-21 PPB Case 430402 add translate="true" --->
<cf_includeJavascriptOnce template = "/javascript/tablemanipulation.js">
 <!--- WAB added 200 --->
<cf_includeJavascriptOnce template = "/javascript/popupDescription.js">



	<CFIF not application.com.flag.doesCurrentUserHaveRightsToEditFlagGroupDefinition(frmFlagGroupID)   >
	
		<CFSET message = "You do not appear to have sufficient rights to access this Flag Group information. <P>Please contact support if you need further information.">
		<!--- <cfinclude template="/profileManager/profileList.cfm"> --->
		<cfoutput>#application.com.relayUI.message(message=message,type="warning")#</cfoutput>
		<CF_ABORT>
	</CFIF>


	<CFSET frmName = "">
	<CFSET frmDescription = "">
	<CFSET frmFlagTextID = "">
	<CFSET frmNamePhraseTextID = "">
	<CFSET frmOrder = 1>
	<CFSET frmScore = 0>
	<CFSET frmActive = 1>
	<CFSET frmlookup = 0>
	<CFSET frmuseValidValues = 0>
	<CFSET frmLinksToEntityTypeID = "">
	<CFSET frmWddxStruct = "">
		
	<CFQUERY NAME="getFlagGroup" DATASOURCE="#application.SiteDataSource#">
	SELECT max (f.OrderingIndex) AS order_max, flagtypeid, fg.name, fg.description, fg.helpText
	FROM FlagGroup as fg left join Flag as f on f.FlagGroupID = fg.FlagGroupID 
	WHERE fg.FlagGroupID =  <cf_queryparam value="#frmFlagGroupID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	group by flagtypeid, fg.name, fg.description, fg.helpText
	</CFQUERY>
	

	<CFSET frmflagTypeID = getFlagGroup.FlagTypeID>
	<CFSET FlagGroupName = getFlagGroup.Name>
	<CFSET FlagGroupHelpText = getFlagGroup.helpText>
	<CFSET FlagGroupDescription = getFlagGroup.Description>
	<CFIF getFlagGroup.order_max IS NOT "" AND getFlagGroup.order_max LT 99>
		<CFSET frmOrder = getFlagGroup.order_max + 1>
	</CFIF> 

	
	





<cf_head>
	<cf_title><cfoutput>#request.CurrentSite.Title#</cfoutput></cf_title>
<SCRIPT type="text/javascript">

<!--
		
       function verifyForm(task,next) {
                var form = document.FlagForm;
                form.frmTask.value = task;
                form.frmNext.value = next;
                var msg = "";
                if (msg != "") {
                        alert("\nYou must provide the following information for this flag:\n\n" + msg);
                } else {
                	form.submit();
				}
        }

		function flagEdit(flagID) {
                var form = document.FlagForm;
                form.frmTask.value = "edit";
					form.frmNext.value = 'flagedit'
				form.frmNextFlagID.value = flagID;
                var msg = "";
                if (form.frmName.value == "" || form.frmName.value == " ") {
                        msg += "* Name\n";
                }
                if (msg != "") {
                       // alert("\nYou must provide the following information for this flag:\n\n" + msg);
                   	form.submit();
                } else {
                	form.submit();
				}
        }

        function validValues(fieldName) {
				newWin = window.open ('<CFOUTPUT></cfoutput>/dataTools/validvalues.cfm?frmFieldName='+fieldName+'&frmCountryID=0','ValidValues','height=500,width=700,scrollbars=1,resizable=1')	
				newWin.focus()
		}


        function translate(flagTextID) {
				newWin = window.open ('<CFOUTPUT></cfoutput>/translation/editPhrase.cfm?frmFieldName='+flagTextID+'&frmCountryID=0','translate','height=500,width=700,scrollbars=1,resizable=1')	
				newWin.focus()
		}

		
	   function setTextID(){
	   		var form = document.FlagForm;
<!--- WAB removed, needs to be changed to strip out invalid characters --->
//			if (form.frmFlagTextID.value == "") {
//				form.frmFlagTextID.value = form.frmName.value
//			}
	   }


//-->

</SCRIPT>
</cf_head>



<CFSET ThisPageName="flagbulkload.cfm">
<cfinclude template="/profileManager/profileTopHead.cfm">

<CFOUTPUT>
<FORM METHOD="POST" ACTION="/profileManager/flagUpdateTask.cfm" NAME="FlagForm">

<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" CLASS="withBorder">

	<CF_INPUT TYPE="HIDDEN" NAME="frmDate" VALUE="#CreateODBCDateTime(now())#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmFlagGroupID" VALUE="#frmFlagGroupID#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmflagTypeID" VALUE="#frmflagTypeID#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmTask" VALUE="#frmtask#">
	<INPUT TYPE="HIDDEN" NAME="frmFlagID" VALUE="">
	<INPUT TYPE="HIDDEN" NAME="frmNext" VALUE="">
	<CF_INPUT TYPE="HIDDEN" NAME="frmBack" VALUE="#frmBack#">

	<TR>
		<TD COLSPAN=2>
				<strong>ADD NEW PROFILE ATTRIBUTES to '#UCase(HTMLEditFormat(Trim(flagGroupName)))#'</strong>

		</TD>
	</TR>
	
<cfset helptext = "Paste from Excel a table containing columns for attribute names and help text into the text area above.  <BR>When you click out of the text area this data will be inserted into the table above.">
<cfset rowStruct = arrayNew(1)>
<cfset rowStruct[1] = structNew()>
<cfset rowStruct[1].name = "Name">
<cfset rowStruct[1].content = '<input class=flaggrid name="Name_INDEX" Type=text value="" size=30 onBlur=checkForBlankRow("FlagTable")><input type="hidden" name="frmFlagID" value="INDEX">'>

<cfset rowStruct[2] = structNew()>
<cfset rowStruct[2].name = "helpText">
<cfset rowStruct[2].content = '<input class=flaggrid name="helpText_INDEX" Type=text value="" size=50>'>


<cfset rowStruct[3] = structNew()>
<cfset rowStruct[3].name = "Score">
<cfset rowStruct[3].content = '<input class=flaggrid name="Score_INDEX" Type=text value="" size=5>'>


<TR>
		<td valign="top" class="label">Attribute Names</td>
		<TD>
<table id="FlagTable"  cellspacing=0 cellpadding=0 class="flaggrid">
					<tbody>
					<tr class=flaggrid>
						<th class=flaggrid>Name</td>
						<th class=flaggrid>Help Text</td>
						<th class=flaggrid>Score</td>
					</tr>	
</table>
		<div align=left >
			<textarea cols="50" rows="3" name="excelPaste" id="excelPaste" onChange="copyTextAreaToTable('excelPaste','FlagTable');this.value=''" value=""></textarea>
			<BR>
			<div onmouseover="pop('#helptext#','',event)" onmouseout="kill()">Excel Paste Area <img src="../images/icons/help.png" alt="" width="16" height="16" border="0"></div>
		</div>

		</td>
	</tr>
<script>
	<cfwddx action="cfml2js" input = "#rowStruct#" topLevelVariable = "newRowFlagTable">
	addRowToTable('FlagTable') ;
</script>

	
<!--- 	<TR>
		<td valign="top" class="label">Attribute Names</td>
		<TD>
			<TEXTAREA  NAME="frmBulkName" rows=10 cols=60></textarea>
			<BR>Add one name per line.  You can cut and paste columns directly from Excel.


		</TD>
	</TR>
 --->	<tr>
		<td class="Label">Translate Names</td>
		<td  colspan="2" valign="TOP">
				<input type="checkbox" name="frmTranslateLabel" value="1" CHECKED> Create Default Translations
		</td>
	</tr>
	<!--- <TR>
		<td valign="top" class="label">Attribute Descriptions</td>
		<TD>
			<TEXTAREA  NAME="frmBulkDescription" rows=10 cols=60></textarea>
			<BR>Either leave blank or add one name per line.  
		</TD>
	</TR> --->
	
	<TR>
		<td valign="top" class="label">Start Order at</TD>
		<TD>
			<SELECT NAME="frmOrder"><CFLOOP INDEX="LoopCount" FROM="1" TO="99">
				<CFIF LoopCount IS frmOrder>
					<OPTION VALUE=#LoopCount# SELECTED> #htmleditformat(LoopCount)#  
				<CFELSE>
					<OPTION VALUE=#LoopCount#> #htmleditformat(LoopCount)#
				</CFIF>
			</CFLOOP></SELECT>
			<BR>Attributes with the same Ordering Index are sorted alphabetically, so for items like
			categories, give all attributes the same Ordering Index.  Items, like days of the week,
			should be ordered 1 to 7.<BR><BR>
		</TD>
	</TR>

	
	


</FORM>
</TABLE>
</cfoutput>


<FORM METHOD="POST" ACTION="<CFOUTPUT>/profileManager/#htmleditformat(frmBack)#</CFOUTPUT>.cfm" NAME="BackForm">
<CF_INPUT TYPE="HIDDEN" NAME="frmFlagGroupID" VALUE="#frmFlagGroupID#">
</FORM>
										
<P>


</CENTER>
</FORM>


