<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Update flag group information --->

<!--- Amendment History
1999-02-20		WAB		Add Insert of FlagGroupTextID
2000-09-15		WAB		Added check for frmParentFlagGroupID is ""  and insert 0  - subsequent to DAM changes to profileEdit.cfm
2008-10-14	NYB		Amended error with adding child profiles via "Buld Add" in ProfileEdit
2010/02/02	NJH/WAB	LID 2828 - update flagGroup structure once if doing a bulk upload. Do it after all flagGroups have been added.
2010/03/24	NAS		LID 3153 - Unable to delete Profile
2010/08/06	NJH		LID 3795 - added description when adding bulk profile
2011/02/11	NJH		P-FNL075 Report Designer - add flagGroup to domain if frmUseInAllDomains is true
2012-08-07	STCR	P-REL109 Removed call to updateQuizDetailOnProfileChange (which related to LID3153 and LID2462), since QuizTaken entity Flags are disabled in the 2012 Relay Quiz structure
2014-08-11			RPW Create new profile type: "Financial"
2014-11-24	RPW		CORE-986 Financial Flags that are bulk loaded cannot be used
2016/09/26	NJH		JIRA PROD2016-2383 - remove the expiry data for flag groups.
2016/12/13	NJH		JIRA PROD2016-PROD2016-2957 - added createdBy/lastUpdatedBy person fields
--->

<CFTRANSACTION>


<CFSET updatetime = CreateODBCDateTime(Now())>

<cfparam name="frmBack" default="profileList">

<CFIF frmTask IS NOT 'add' and frmTask is Not 'bulkload'>

	<!--- Will need to see if access rights changed, so retreive it now
	      as well --->
	<CFQUERY NAME="checkDateTime" DATASOURCE="#application.SiteDataSource#">
		SELECT LastUpdated,
		(ViewingAccessRights * 8) + (EditAccessRights * 4 ) + (SearchAccessRights * 2) + (DownloadAccessRights *1) as oldaccessrights, CreatedBy     <!--- WAB: I don't think that all this binary stuff is necessary, but I didn't seem to be able to add bit fields without multiplying them by something!--->
		FROM FlagGroup
		WHERE FlagGroupID =  <cf_queryparam value="#frmFlagGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >
		AND LastUpdated >  <cf_queryparam value="#frmDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
	</CFQUERY>

	<CFIF checkDateTime.RecordCount IS NOT 0>

		<CFSET message = "The record could not be updated since it has been changed by another user since you last viewed it.">
		<cfset messageType="info">
		<CFINCLUDE TEMPLATE="#frmBack#.cfm">
		<CF_ABORT>
	</CFIF>

</CFIF>


<CFPARAM NAME="frmView" DEFAULT=0>
<CFPARAM NAME="frmSearch" DEFAULT=0>
<CFPARAM NAME="frmEdit" DEFAULT=0>
<CFPARAM NAME="frmDownload" DEFAULT=0>
<CFPARAM NAME="frmViewingAccessRights" DEFAULT=0>
<CFPARAM NAME="frmSearchAccessRights" DEFAULT=0>
<CFPARAM NAME="frmEditAccessRights" DEFAULT=0>
<CFPARAM NAME="frmDownloadAccessRights" DEFAULT=0>
<CFPARAM name="frmFormattingParameters" default="">
<CFPARAM NAME="frmPublicAccess" DEFAULT=0>
<CFPARAM NAME="frmPartnerAccess" DEFAULT=0>
<CFPARAM NAME="FORM.frmFlagTypeCurrency" DEFAULT="">	<!--- 2014-08-11		RPW Create new profile type: "Financial" --->
<CFPARAM NAME="FORM.frmFlagTypeDecimalPlaces" DEFAULT="">	<!--- 2014-08-11		RPW Create new profile type: "Financial" --->
<!--- <CFPARAM NAME="frmUseInAllDomains" DEFAULT=1> --->

<CFIF frmPublicAccess IS "">
	<CFSET frmPublicAccess = 0>
</CFIF>
<CFIF frmPartnerAccess IS "">
	<CFSET frmPartnerAccess = 0>
</CFIF>


<CFIF frmTask IS 'add'>

		<!--- Add new flag group --->
		<cfset newFlagGroupID = application.com.flag.createFlagGroup
			(
			parent = frmparentFlagGroupID,
			flagType  = frmflagTypeID,
			EntityType = frmEntityTypeID,
			name = frmName,
			helpText = frmHelpText,
			description = frmDescription,
			notes = frmNotes,
			flagGroupTextID = frmFlagGroupTextID,
<!--- 			NamePhraseTextID = frmNamePhraseTextID, --->
			orderingIndex = frmOrder,
			scope = frmScope,
			active = frmActive,
			ViewingAccessRights = iif(frmViewingRights is "usergroup",1,0),
			EditAccessRights = iif(frmEditRights is "usergroup",1,0),
			SearchAccessRights = iif(frmSearchRights is "usergroup",1,0),
			DownloadAccessRights = iif(frmDownloadRights is "usergroup",1,0),
			Viewing = iif(frmViewingRights is "all",1,0),
			Search = iif(frmSearchRights is "all",1,0),
			Edit =iif(frmEditRights is "all",1,0),
			Download = iif(frmDownloadRights is "all",1,0),
			useInSearchScreen = frmUseInSearchScreen,
			<!--- Expiry = CreateODBCDate(frmExpiry), --->
			createdby = request.relaycurrentuser.usergroupid,
			updatetime = updatetime,
			createDefaultTranslationName = #iif(isDefined("frmTranslateLabel"),de(true),de(false))#,
			EntityMemberLive = #frmassocLive# ,
			EntityMemberID = #frmEntityMemberID#,
			FormattingParameters = frmFormattingParameters,
			FlagTypeCurrency = FORM.frmFlagTypeCurrency,	<!--- 2014-08-11		RPW Create new profile type: "Financial" --->
			FlagTypeDecimalPlaces = FORM.frmFlagTypeDecimalPlaces	<!--- 2014-08-11		RPW Create new profile type: "Financial" --->
			)>


<!--- 		<CFQUERY NAME="AddFlagGroup" DATASOURCE="#application.SiteDataSource#">
		INSERT INTO FlagGroup
		(ParentFlagGroupID, FlagTypeID, Name, Description, EntityMemberLive , EntityMemberID, FlagGroupTextID,Notes, OrderingIndex, Scope, ViewingAccessRights, EditAccessRights, SearchAccessRights, DownloadAccessRights,Viewing, Search, Edit, Download, EntityTypeID, useInSearchScreen, Expiry, Active, CreatedBy, Created, LastUpdatedBy, LastUpdated)
		VALUES
		(<CFIF frmParentFlagGroupID is "">0<CFELSE>#frmParentFlagGroupID#</cfif>,
		#frmFlagTypeID#,
<!--- 		'#Replace(frmName,Chr(34),"`","ALL")#', '#frmDescription#',#frmassocLive#,#frmEntityMemberID#, '#frmFlagGroupTextID#','#frmNotes#', #frmOrder#, #frmScope#, #frmViewingAccessRights#, #frmEditAccessRights#, #frmSearchAccessRights#, #frmDownloadAccessRights#,  #frmView#, #frmSearch#, #frmEdit#, #frmDownload#, #frmEntityType#, #frmUseInSearchScreen#, #CreateODBCDate(frmExpiry)#, #frmActive#, #request.relayCurrentUser.usergroupid#, #Variables.updatetime#, #request.relayCurrentUser.usergroupid#, #Variables.updatetime#) --->
		'#Replace(frmName,Chr(34),"`","ALL")#', '#frmDescription#',#frmassocLive#,#frmEntityMemberID#, '#frmFlagGroupTextID#','#frmNotes#', #frmOrder#, #frmScope#, #iif(frmViewingRights is "usergroup",1,0)#, #iif(frmEditRights is "usergroup",1,0)#, #iif(frmSearchRights is "usergroup",1,0)# , #iif(frmDownloadRights is "usergroup",1,0)#,  #iif(frmViewingRights is "all",1,0)#, #iif(frmSearchRights is "all",1,0)#, #iif(frmEditRights is "all",1,0)#, #iif(frmDownloadRights is "all",1,0)#, #frmEntityType#, #frmUseInSearchScreen#, #CreateODBCDate(frmExpiry)#, #frmActive#, #request.relayCurrentUser.usergroupid#, #Variables.updatetime#, #request.relayCurrentUser.usergroupid#, #Variables.updatetime#)
		</CFQUERY>



		<CFQUERY NAME="getFlagGroupID" DATASOURCE="#application.SiteDataSource#">
		SELECT FlagGroupID
		FROM FlagGroup
		WHERE FlagTypeID = #frmFlagTypeID#
		AND LastUpdated = #Variables.updatetime#
		AND Name = '#Replace(frmName,Chr(34),"`","ALL")#'
		</CFQUERY>
 --->
		<!--- set variable so that can go to the flagGroupShowScreen
		set in form scope so that CF_UGRecordManagerTask can use it--->
		<CFSET form.FRMFLAGGROUPID= newFlagGroupID>

		<!---  added WAB 2005-10-21--->
		<CF_UGRecordManagerTask>


		<CFSET message = "Profile '" & #HTMLEditFormat(Replace(frmName,Chr(34),"`","ALL"))# & "' has been added successfully.">

		<CFIF frmParentFlagGroupID IS 0>
			<CFSET message = message & "<P>You may add attributes or other profiles to this now">
		<CFELSE>
			<CFSET message = message & "You may add attributes to this now">
		</CFIF>

<!--- NYB 2008-10-14 - added everything after "bulkLoad" --->
<CFELSEIF (frmTask IS "bulkLoad" and not isDefined("frmNext")) or (frmTask IS 'bulkLoad' and frmNext eq 'ProfileEdit')>
	<cfset flagGroupsAdded = "">
	<cfloop index="flagGroupID" list="#frmFLagGroupID#">

		<cfset name = form["name_#flagGroupID#"]>
		<cfset type = form["type_#flagGroupID#"]>
		<cfset description = form["description_#flagGroupID#"]>	<!--- NJH 2010/08/06 LID 3795 --->
		<!--- 2014-11-24	RPW		CORE-986 Financial Flags that are bulk loaded cannot be used  Added FlagTypeCurrency and FlagTypeDecimalPlaces --->
		<cfscript>
			variables.FlagTypeCurrency = "";
			variables.FlagTypeDecimalPlaces = "";
			if (StructKeyExists(FORM,"frmFlagTypeCurrency_#flagGroupID#")) {
				variables.FlagTypeCurrency = FORM["frmFlagTypeCurrency_#flagGroupID#"];
			}
			if (StructKeyExists(FORM,"frmFlagTypeDecimalPlaces_#flagGroupID#")) {
				variables.FlagTypeDecimalPlaces = FORM["frmFlagTypeDecimalPlaces_#flagGroupID#"];
			}
		</cfscript>

		<cfif name is not "" and type is not "">

			<!--- NJH/WAB 2010/02/02 LID 2828 - added updateMemoryStructure --->
			<cfset newFlagGroupID = application.com.flag.createFlagGroup
				(
				<!--- NYB 2008-10-14 - replaced 0 with frmparentFlagGroupID --->
				<!--- NJH 2010/08/06 LID 3795 - uncommented description.. not sure why it was commented out --->
				parent = frmparentFlagGroupID,
				flagType  = TYPE,
				EntityType = frmEntityTypeID,
				name = NAME,
				description = description,
		<!---		flagGroupTextID = frmFlagGroupTextID,
	 			NamePhraseTextID = frmNamePhraseTextID, --->
				orderingIndex = frmOrder,
				scope = frmScope,
				active = frmActive,
				ViewingAccessRights = iif(frmViewingRights is "usergroup",1,0),
				EditAccessRights = iif(frmEditRights is "usergroup",1,0),
				SearchAccessRights = iif(frmSearchRights is "usergroup",1,0),
				DownloadAccessRights = iif(frmDownloadRights is "usergroup",1,0),
				Viewing = iif(frmViewingRights is "all",1,0),
				Search = iif(frmSearchRights is "all",1,0),
				Edit =iif(frmEditRights is "all",1,0),
				Download = iif(frmDownloadRights is "all",1,0),
				useInSearchScreen = frmUseInSearchScreen,
				Expiry = CreateODBCDate(frmExpiry),
				createdby = request.relaycurrentuser.usergroupid,
				updatetime = updatetime,
				createDefaultTranslationName = #iif(isDefined("frmTranslateLabel"),de(true),de(false))#,
				updateMemoryStructure = false,
				FlagTypeCurrency = variables.FlagTypeCurrency,
				FlagTypeDecimalPlaces = variables.FlagTypeDecimalPlaces
				)>

				<cfset flagGroupsAdded = listappend(flagGroupsAdded,newFlagGroupID)>

		</cfif>

	</cfloop>
	<cfset frmFlagGroupID = listFirst (flagGroupsAdded)>
<!--- NYB 2008-10-14: --->
	<cfset form.frmFlagGroupID = frmFlagGroupID>

	<!--- NJH/WAB 2010/02/02 LID 2828 - update flagGroup structure outside of loop for all flagGroups just added--->
	<cfset application.com.flag.updateFlagGroupStructure(flagGroupsAdded)>

<CFELSEIF frmTask IS 'ordering'>

	<!--- Amend flag group ordering index --->

	<CFQUERY NAME="updateFlag" DATASOURCE="#application.SiteDataSource#">
		UPDATE FlagGroup SET
		 OrderingIndex =  <cf_queryparam value="#frmOrder#" CFSQLTYPE="cf_sql_float" > ,
		 LastUpdatedBy = #request.relayCurrentUser.usergroupid#,
		 LastUpdated =  <cf_queryparam value="#Variables.updatetime#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
		WHERE FlagGroupID =  <cf_queryparam value="#frmFlagGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</CFQUERY>

	<CFSET message = "Profile '#HTMLEditFormat(frmFlagGroupName)#' ordering has been changed to " & frmOrder & " successfully.">

<CFELSEIF frmTask IS 'edit'>
<!---
		DAM 21 Feb 2002
		Updated this to account for UseInSearchScreen which was present on the search screen
		but not being updated
--->


	<cfif isDefined("frmTranslateLabel")>
		<cfset application.com.flag.createFlagGroupNameDefaultTranslation (flagGroupid = frmflagGroupid, name = frmname, flaggroupTextID = frmflagGroupTextID, namePhrasetextID = '', languageid = 1)>
	</cfif>


	<CFQUERY NAME="updateFlag" DATASOURCE="#application.SiteDataSource#">
		UPDATE FlagGroup SET
		 Name =  <cf_queryparam value="#Replace(frmName,Chr(34),"`","ALL")#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
		 helpText =  <cf_queryparam value="#frmHelpText#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
		 Description =  <cf_queryparam value="#frmDescription#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
		 FlagGroupTextID =  <cf_queryparam value="#frmFlagGroupTextID#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
		 Notes =  <cf_queryparam value="#frmNotes#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
		 OrderingIndex =  <cf_queryparam value="#frmOrder#" CFSQLTYPE="cf_sql_float" > ,
		 ViewingAccessRights =  <cf_queryparam value="#iif(frmViewingRights is "usergroup",1,0)#" CFSQLTYPE="CF_SQL_bit" > ,
		 EditAccessRights =  <cf_queryparam value="#iif(frmEditRights is "usergroup",1,0)#" CFSQLTYPE="CF_SQL_bit" > ,
		 SearchAccessRights =  <cf_queryparam value="#iif(frmSearchRights is "usergroup",1,0)#" CFSQLTYPE="CF_SQL_bit" > ,
 		 DownloadAccessRights =  <cf_queryparam value="#iif(frmDownloadRights is "usergroup",1,0)#" CFSQLTYPE="CF_SQL_bit" > ,
		 Viewing =  <cf_queryparam value="#iif(frmViewingRights is "all",1,0)#" CFSQLTYPE="CF_SQL_bit" > ,
		 Search =  <cf_queryparam value="#iif(frmSearchRights is "all",1,0)#" CFSQLTYPE="CF_SQL_bit" > ,
		 Edit =  <cf_queryparam value="#iif(frmEditRights is "all",1,0)#" CFSQLTYPE="cf_sql_bit" >,
		 Download =  <cf_queryparam value="#iif(frmDownloadRights is "all",1,0)#" CFSQLTYPE="cf_sql_float" > ,
		 formattingParameters =  <cf_queryparam value="#frmFormattingParameters#" CFSQLTYPE="CF_SQL_VARCHAR" > ,

<!--- 		 ViewingAccessRights = #frmViewingAccessRights#,
		 EditAccessRights = #frmEditAccessRights#,
		 SearchAccessRights = #frmSearchAccessRights#,
 		 DownloadAccessRights = #frmDownloadAccessRights#,
		 Viewing = #frmView#,
		 Search = #frmSearch#,
		 Edit = #frmEdit#,
		 Download = #frmDownload#,
 --->
		<!--- NJH to protect against flaggroups not necessarily having an expiry date --->
		<!--- <cfif frmExpiry neq "" and isDate(frmExpiry)>
			Expiry =  <cf_queryparam value="#CreateODBCDate(frmExpiry)#" CFSQLTYPE="CF_SQL_TIMESTAMP" > ,
		</cfif> --->
		 Active =  <cf_queryparam value="#frmActive#" CFSQLTYPE="cf_sql_float" > ,
		 EntityMemberLive =  <cf_queryparam value="#frmassocLive#" CFSQLTYPE="CF_SQL_bit" > ,
		 UseInSearchScreen =  <cf_queryparam value="#frmUseInSearchScreen#" CFSQLTYPE="CF_SQL_bit" > ,
		<!---  showInAllDomains = #frmUseInAllDomains#, --->
		 LastUpdatedBy = #request.relayCurrentUser.usergroupid#,
		 LastUpdated =  <cf_queryparam value="#Variables.updatetime#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
		 lastUpdatedByPerson = #request.relayCurrentUser.personID#
		WHERE FlagGroupID =  <cf_queryparam value="#frmFlagGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</CFQUERY>

	<CF_UGRecordManagerTask>

	<CFSET message = "Profile '" & #HTMLEditFormat(Replace(frmName,Chr(34),"`","ALL"))# & "' details have been updated successfully.">

		<CFSET newaccessrights = (frmViewingAccessRights * 8) + (frmEditAccessRights * 4) + (frmSearchAccessRights * 2) + (frmDownloadAccessRights)>

	<CFIF newaccessrights  IS NOT 0
			AND checkDateTime.oldaccessrights IS 0
			AND checkDateTime.CreatedBy IS NOT request.relayCurrentUser.usergroupid>
		<!--- Add the user to FlagGroup Rights if:
				* Access rights are changing from No to Yes
				* If the user is not the creator (who will automatically have rights)
		--->
		<CFQUERY NAME="chkRights" DATASOURCE="#application.SiteDataSource#">
			SELECT UserID
			FROM FlagGroupRights
			WHERE FlagGroupID =  <cf_queryparam value="#frmFlagGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >
			AND UserID = #request.relayCurrentUser.personid#
		</CFQUERY>

		<CFIF chkRights.RecordCount IS 0>
			<!--- user does not already have rights so add a rights record --->

			<CFQUERY NAME="AddRights" DATASOURCE="#application.SiteDataSource#">
				INSERT INTO FlagGroupRights
				(UserID, FlagGroupID, Viewing, Search, Edit, Download, CreatedBy, Created, LastUpdatedBy, LastUpdated)
				VALUES
				(<cf_queryparam value="#request.relayCurrentUser.personid#" CFSQLTYPE="cf_sql_integer" >,<!--- use personID here --->
				<cf_queryparam value="#frmFlagGroupID#" CFSQLTYPE="cf_sql_integer" >,
				1, 1, 1, 1, <cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >, <cf_queryparam value="#Variables.updatetime#" CFSQLTYPE="CF_SQL_TIMESTAMP" >, <cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >, <cf_queryparam value="#Variables.updatetime#" CFSQLTYPE="CF_SQL_TIMESTAMP" >)
			</CFQUERY>
		<CFELSE>
			<!--- user has rights so update the	rights record --->
			<CFQUERY NAME="UpdRights" DATASOURCE="#application.SiteDataSource#">
				UPDATE FlagGroupRights
				SET Viewing = 1,
					Search = 1,
					Edit = 1,
					Download = 1,
					LastUpdated =  <cf_queryparam value="#Variables.updatetime#" CFSQLTYPE="CF_SQL_TIMESTAMP" > ,
					LastUpdatedBy = #request.relayCurrentUser.usergroupid#
				WHERE UserID = 	#request.relayCurrentUser.personid#<!--- use personID here --->
				AND FlagGroupID =  <cf_queryparam value="#frmFlagGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >
			</CFQUERY>
		</CFIF>
	</CFIF>

<CFELSEIF frmTask IS 'delete'>

	<!--- Find if this is a parent, as we have to delete sub flags groups as well --->

	<CFQUERY NAME="getFlagGroups" DATASOURCE="#application.SiteDataSource#">
	SELECT FlagGroupID, FlagGroup.FlagTypeID, FlagGroup.Name,
		flagType.name as TypeName,
		flagType.DataTable as TypeData
	FROM FlagGroup
		inner join flagType on flagGroup.flagTypeID = flagType.flagTypeID
	WHERE FlagGroupID =  <cf_queryparam value="#frmFlagGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >
	OR ParentFlagGroupID =  <cf_queryparam value="#frmFlagGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >
	ORDER BY FlagGroup.Name
	</CFQUERY>

	<CFSET delList = "<P><TABLE BORDER=0><TR><TH><B>Profile</B>&nbsp;&nbsp;</TH><TH><B>Attributes and Entity Data</B></TH></TR>">

	<CFLOOP QUERY="getFlagGroups">
		<!--- Find all flags in this Group --->
			<CFQUERY NAME="getFlags" DATASOURCE="#application.SiteDataSource#">
			SELECT FlagID, Name
			FROM Flag
			WHERE FlagGroupID =  <cf_queryparam value="#FlagGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >
			ORDER BY Name
			</CFQUERY>

			<CFSET delFlags = ValueList(getFlags.FlagID)>

			<CFIF delFlags IS NOT "">
				<CFSET delList = delList & "<TR><TD VALIGN=TOP>#HTMLEditFormat(Name)# (#getFlags.flagID#)</TD><TD VALIGN=TOP>" & #HTMLEditFormat(ValueList(getFlags.Name, ", "))# & "</TD></TR>">
				<!--- Find type of Flag Group to identify data table --->
				<CFQUERY NAME="getFlagGroupType" DATASOURCE="#application.SiteDataSource#">
				SELECT FlagTypeID FROM FlagGroup WHERE FlagGroupID =  <cf_queryparam value="#FlagGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >
				</CFQUERY>
				<!--- <CFSET typeName = #ListGetAt(typeList,getFlagGroupType.FlagTypeID)#>
				<CFSET typeData = #ListGetAt(typeDataList,getFlagGroupType.FlagTypeID)#> --->

				<CFSET typeData = getFlagGroups.typeData>
				<CFSET typeName = getFlagGroups.typeName>

				<CFLOOP LIST="#delFlags#" INDEX="dFlagID">
					<!--- Delete Flag data associated with this flag --->
					<!--- Archive all data for this flag --->
					<CFQUERY NAME="ArchiveFlagData" DATASOURCE="#application.SiteDataSource#">
					INSERT INTO x#typeData#FlagData
					(EntityID, FlagID, CreatedBy, Created,
					<CFIF typeData IS NOT "Boolean" and typedata IS NOT "event">Data,</CFIF>
					LastUpdatedBy, LastUpdated, lastUpdatedByPerson,ArchivedBy, Archived)
					SELECT
					EntityID, FlagID, CreatedBy, Created,
					<CFIF typeData IS NOT "Boolean" and typedata IS NOT "event">Data,</CFIF>
					LastUpdatedBy, LastUpdated, lastUpdatedByPerson, <cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >, <cf_queryparam value="#Variables.updatetime#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
				  	FROM #typeData#FlagData
				 	WHERE FlagID =  <cf_queryparam value="#dFlagID#" CFSQLTYPE="CF_SQL_INTEGER" >
					</CFQUERY>

					<!--- and delete the archived flags --->
					<CFQUERY NAME="DeleteFlags" DATASOURCE="#application.SiteDataSource#">
					DELETE FROM #typeData#FlagData
					WHERE FlagID =  <cf_queryparam value="#dFlagID#" CFSQLTYPE="CF_SQL_INTEGER" >
					</CFQUERY>

					<!--- Delete Flag --->
					<CFQUERY NAME="ArchiveFlag" DATASOURCE="#application.SiteDataSource#">
					INSERT INTO xFlag
					(FlagID, FlagGroupID, Name, Description, OrderingIndex, Active, CreatedBy, Created, LastUpdatedBy, LastUpdated, ArchivedBy, Archived)
					SELECT FlagID, FlagGroupID, Name, Description, OrderingIndex, Active, CreatedBy, Created, LastUpdatedBy, LastUpdated, <cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >, <cf_queryparam value="#Variables.updatetime#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
				  	FROM Flag
				 	WHERE FlagID =  <cf_queryparam value="#dFlagID#" CFSQLTYPE="CF_SQL_INTEGER" >
					</CFQUERY>

					<!--- and delete the archived flag --->
					<CFQUERY NAME="DeleteFlag" DATASOURCE="#application.SiteDataSource#">
					DELETE FROM Flag
					WHERE FlagID =  <cf_queryparam value="#dFlagID#" CFSQLTYPE="CF_SQL_INTEGER" >
					</CFQUERY>

					<CFIF frmFlagTypeID IS NOT 2 AND frmFlagTypeID IS NOT 3>
						<!--- Need to delete record in SelectionField --->

						<CFQUERY NAME="delFlagCriteria" DATASOURCE="#application.SiteDataSource#">
						DELETE FROM SelectionCriteria
						WHERE SelectionField =  <cf_queryparam value="frmFlag_#typeName#_#dFlagID#" CFSQLTYPE="CF_SQL_VARCHAR" >
						</CFQUERY>

						<CFQUERY NAME="delFlagCriteria" DATASOURCE="#application.SiteDataSource#">
						DELETE FROM SelectionField
						WHERE SelectionField =  <cf_queryparam value="frmFlag_#typeName#_#dFlagID#" CFSQLTYPE="CF_SQL_VARCHAR" >
						</CFQUERY>

					</CFIF>


				</CFLOOP>

			<CFELSE>

				<!--- No flags for this group --->

				<CFSET delList = delList & "<TR><TD VALIGN=TOP>#HTMLEditFormat(getFlagGroups.Name)# (#getFlagGroups.flagGroupID#)</TD><TD VALIGN=TOP>none</TD></TR>">

			</CFIF>


					<!--- Delete Flag Group --->

					<CFQUERY NAME="ArchiveFlagGroup" DATASOURCE="#application.SiteDataSource#">
					INSERT INTO xFlagGroup
					(FlagGroupID, ParentFlagGroupID, FlagTypeID, Name, helpText,Description, Notes, OrderingIndex, Expiry, Scope, ViewingAccessRights, EditAccessRights, SearchAccessRights, DownloadAccessRights,Viewing, Edit, Search, Download, EntityTypeID, Active, PublicAccess, PartnerAccess, CreatedBy, Created, LastUpdatedBy, LastUpdated, ArchivedBy, Archived)
					SELECT FlagGroupID, ParentFlagGroupID, FlagTypeID, Name, helpText,Description, Notes, OrderingIndex, Expiry, Scope, ViewingAccessRights, EditAccessRights, SearchAccessRights, DownloadAccessRights, Viewing, Edit, Search, Download, EntityTypeID, Active, PublicAccess, PartnerAccess, CreatedBy, Created, LastUpdatedBy, LastUpdated, <cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >, <cf_queryparam value="#Variables.updatetime#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
				  	FROM FlagGroup
				 	WHERE FlagGroupID =  <cf_queryparam value="#FlagGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >
					</CFQUERY>


					<!--- and delete any flag group rights --->
					<CFQUERY NAME="DeleteRights" DATASOURCE="#application.SiteDataSource#">
					DELETE FROM FlagGroupRights
					WHERE FlagGroupID =  <cf_queryparam value="#FlagGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >
					</CFQUERY>


					<!--- and delete the archived flag group --->
					<CFQUERY NAME="DeleteFlagGroup" DATASOURCE="#application.SiteDataSource#">
					DELETE FROM FlagGroup
					WHERE FlagGroupID =  <cf_queryparam value="#FlagGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >
					</CFQUERY>

					<CFIF frmFlagTypeID IS 2 OR frmFlagTypeID IS 3>
						<!--- Need to delete record in SelectionField --->

						<!--- NJH 2010/09/23 - replaced with below <CFSET typeName = #ListGetAt(typeList,getFlagGroups.FlagTypeID)#> --->
						<CFSET typeName = getFlagGroups.typeName>

						<CFQUERY NAME="delFlagGroupCriteria" DATASOURCE="#application.SiteDataSource#">
						DELETE FROM SelectionCriteria
						WHERE SelectionField =  <cf_queryparam value="frmFlag_#typeName#_#FlagGroupID#" CFSQLTYPE="CF_SQL_VARCHAR" >
						</CFQUERY>

						<CFQUERY NAME="delFlagGroupCriteria" DATASOURCE="#application.SiteDataSource#">
						DELETE FROM SelectionField
						WHERE SelectionField =  <cf_queryparam value="frmFlag_#typeName#_#FlagGroupID#" CFSQLTYPE="CF_SQL_VARCHAR" >
						</CFQUERY>

					</CFIF>



	</CFLOOP>

	<CFSET delList = delList & "</TABLE>">

	<CFSET message = "The following Profiles, attributess and Entity data have been deleted successfully." & delList>
</cfif>


</CFTRANSACTION>

<!--- NJH 2011/02/11 P-FNL075 Report Designer - add flagGroup to domain if frmUseInAllDomains is true --->
<!--- <cfif frmUseInAllDomains>
	<cfset application.com.jasperServer.insertFlagGroupDomain(flagGroupID=frmFlagGroupID)>
</cfif> --->

<cfset application.com.flag.updateFlagGroupStructure(frmFlagGroupID)>

<CF_FormFieldsURLString>
<CFINCLUDE TEMPLATE="#frmNext#.cfm">
<CF_ABORT>