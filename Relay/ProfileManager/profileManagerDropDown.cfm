<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			profileManagerDropDown.cfm	
Author:				SWJ
Date started:		2004-09-30
	
Description:		Provides a list of drop down functions that the user can use to set various
					aspects of the profile data

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->


	<cf_includejavascriptonce template = "/javascript/checkBoxFunctions.js">	
<SCRIPT type="text/javascript">
<!--
function go()	{

	if (document.selecter.select1.options[document.selecter.select1.selectedIndex].value != "none") 
		{
		location = document.selecter.select1.options[document.selecter.select1.selectedIndex].value
		document.selecter.select1.selectedIndex=0     //resets menu to top item
		}
}

function openWin( windowURL,  windowName, windowFeatures ) { 
	return window.open( windowURL, windowName, windowFeatures ) ; 
}

function goOption(checksRequired, frmAction, myform, entityType)
{
	var form = eval('document.'+ myform);
	if(checksRequired && saveChecksNew(entityType, myform)=='0')
		noChecks(entityType)
	else
	{ 		
		newWindow = openWin( 'about:blank' ,'PopUp', 'left=' + parseInt(screen.availWidth * 0.4)  + ', width=' + parseInt(screen.availWidth * 0.4)  + ', top=' + parseInt(screen.availHeight * 0.3)  + ', height=' + parseInt(screen.availHeight * 0.3) + ',	toolbar=0,  location=0,	directories=0, status=0, menuBar=0,	scrollBars=1, resizable=1'); 
		form.frmAction.value = frmAction; 
		// Removed by GC - only submitting one page
		//if (checksRequired) form.frmTotalChecks.value=saveChecksNew(entityType, myform); 
		form.target='PopUp'; 
		form.action='../template/dialogBoxes.cfm';
		form.submit(); 
		<CFIF FindNoCase("msie", cgi.http_user_agent) GT 0>
		//temp	resetForm();
		<CFELSE>
		//temp  setInterval('resetForm()', 1000);
		</CFIF> 
		newWindow.focus();
	}
}
//-->
</SCRIPT>
<cf_translate>
<form name="selecter">
	<select name="select1" size=1 onchange="go()">
	<option value=none>Phr_Sys_Functions
	<option value=none>--------------------
	<CFOUTPUT>
<option value=none>Phr_Sys_Profiles
	<option value="javascript:goOption(true, 'changeParent', 'FlagGroupForm', 'Profile');">&nbsp;phr_sys_ChangeParentForCheckedRecords
	<option value="javascript:goOption(true, 'setNewValue', 'FlagGroupForm', 'Profile');">&nbsp;phr_sys_setNewValueForCheckedRecords
<option value=none>Phr_Sys_Tagging
	<option value="JavaScript:doChecksNew('Profile', true, 'FlagGroupForm');">&nbsp;Phr_Sys_TagAllRecordsOnPage
	<option value="JavaScript:doChecksNew('Profile', false, 'FlagGroupForm');">&nbsp;Phr_Sys_UntagAllRecordsOnPage
	</CFOUTPUT>
	</select>
</form>
</cf_translate>