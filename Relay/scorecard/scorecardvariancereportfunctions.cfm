<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			scorecardVarianceReportFunctions.cfm
Author:				SWJ
Date started:		2007-05-02
	
Description:			

creates functionList query for tableFromQueryObject custom tag

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
02-May-20037		SWJ			Initial version
09-Jul-2008			NJH			P-TND066 1.1.3 Added params and created argumentCollection to pass to the scorecardTierRequestApproval.cfm

Possible enhancements:


 --->

<cfparam name="showApproveChanges" type="boolean" default="true">
<cfparam name="showRejectChanges" type="boolean" default="true">
<cfparam name="sendTierChangeApprovalEmail" type="boolean" default="false">


<cfscript>
argumentsCollection = structNew();
argumentsCollection.approvalFlagTextID='tierChangeApproved';
argumentsCollection.sendTierChangeApprovalEmail=sendTierChangeApprovalEmail;
if (isDefined('promotions')) {
	argumentsCollection.promotions=promotions;
}
	
comTableFunction = CreateObject( "component", "relay.com.tableFunction" );

comTableFunction.functionName = "Scorecard Functions";
comTableFunction.securityLevel = "";
comTableFunction.url = "";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();

comTableFunction.functionName = "--------------------";
comTableFunction.securityLevel = "";
comTableFunction.url = "";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();

if (showApproveChanges) {
comTableFunction.functionName = "&nbsp;Approve Changes";
comTableFunction.securityLevel = "";
	//comTableFunction.url = "javascript:document.frmBogusForm.submit();";
	// CR_LEN507 - Lenovo has two variance reports, one showing promotions and the other showing demotions
	// so we need to know whether we're approving a demotion or a promotion
	
	tempArgumentsCollection = application.com.structureFunctions.convertStructureToNameValuePairs(struct=argumentsCollection,delimiter='&');
	comTableFunction.url = "/scorecard/scorecardTierRequestApproval.cfm?#tempArgumentsCollection#&orgIDList=";
		
	comTableFunction.windowFeatures = "width=400,height=200,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1";
	comTableFunction.functionListAddRow();
}

if (showRejectChanges) {
	// NJH 2008/05/20 P-TND066 1.1.3
	comTableFunction.functionName = "&nbsp;Reject Changes";
	comTableFunction.securityLevel = "";
		// CR_LEN507 - Lenovo has two variance reports, one showing promotions and the other showing demotions
		// so we need to know whether we're approving a demotion or a promotion
	
	argumentsCollection.approvalFlagTextID='tierChangeDenied';
	
	tempArgumentsCollection = application.com.structureFunctions.convertStructureToNameValuePairs(struct=argumentsCollection,delimiter='&');
	comTableFunction.url = "/scorecard/scorecardTierRequestApproval.cfm?#tempArgumentsCollection#&orgIDList=";
	
	comTableFunction.windowFeatures = "width=400,height=200,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1";
comTableFunction.functionListAddRow();
}

comTableFunction.functionName = "Phr_Sys_Tagging";
comTableFunction.securityLevel = "";
comTableFunction.url = "";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();

/*comTableFunction.functionName = "&nbsp;Phr_Sys_TagAllRecordsOnPage";
comTableFunction.securityLevel = "";
comTableFunction.url = "javascript:void(checkboxSetAll(true));//";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();

comTableFunction.functionName = "&nbsp;Phr_Sys_UntagAllRecordsOnPage";
comTableFunction.securityLevel = "";
comTableFunction.url = "javascript:void(checkboxSetAll(false));//";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();*/
</cfscript>


