<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			updateScoreCardTotalv2.cfm	
Author:				NJH
Date started:		2007-02-19
	
Description:	

This file checks for any scorecards that have had flags set	and therefore require updating.

We determine if all the flaggroups in a scorecard section have at least one flag set within them.
If so, we total the flaggroups based on the algorithm specified to give the section total. If all 
sections for a scorecard along a similar graph axis have been totalled, we give the appropriate axis total 
to the score card. So, if all sections for the x axis had a total, we total them to give us the xTotal value for
the scorecard.

NB. IT CAN BE CALLED EITHER FOR BATCHES OR FOR A SINGLE RECORD

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2007-06-13 SWJ Modified to show just how many are processed and how many are left in total.  I have also experimented with uping the 
			number of records processed to 750.  It time out at 1000 records and was running fine at 500 records.

Possible enhancements:


 --->
<cfsetting RequestTimeout = "1200">  
<cfparam name="numScorecards" type="numeric" default="750"> <!--- number of scorecards to process for a scheduled update --->
<cfparam name="entityID" type="numeric" default=0> <!--- update a scorecard for a particular entity --->
<cfparam name="completedScorecardsOnly" type="boolean" default="false"> <!--- only total those scorecards that are fully completed--->





<cf_head>
	<cf_title>Update ScoreCards</cf_title>
</cf_head>


<!--- initialise the logText variable --->
<cfset logText = "">

<cfquery name="createMissingScorecards" datasource="#application.siteDataSource#">
	INSERT INTO [ScoreCard]
	           ([scoreCardDefID],[entityID] ,[updateRequired]
	           ,[created] ,[createdBy] ,[lastUpdated] ,[lastUpdatedBy])
	     select distinct 
	           1 ,organisationID ,getDate() ,getDate() ,4 ,getDate() ,4
	from organisation
	where organisationTypeID = 1
	and organisationID not in (select entityID from scorecard)
</cfquery>

<cfquery name="getScoreCardsNeedingUpdating" datasource="#application.siteDataSource#" maxrows="#numScorecards#">
	select scd.scoreCardDefID, scd.entityTypeID, sc.*
	from scorecard sc inner join 
		scoreCardDef scd on sc.scoreCardDefID = scd.scoreCardDefID 
	where updateRequired is not null
	<cfif entityID neq 0>
		and entityID =  <cf_queryparam value="#entityID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</cfif>
	order by updateRequired desc
</cfquery>

<cfset logText = logText & "#getScoreCardsNeedingUpdating.recordCount# score card records need updating in this batch<br>">

<cfloop query="getScoreCardsNeedingUpdating">
	<cfset rowCount = currentRow>
		<cfif currentRow eq getScoreCardsNeedingUpdating.recordCount>
			<cfset logText = logText & "Updated #rowCount# out of #getScoreCardsNeedingUpdating.recordCount# score card records<br>">
		</cfif>

	<cfset totalScoreCardScore = 0>
	
	<!--- get scorecard sections --->
	<cfquery name="getScoreCardSections" datasource="#application.siteDataSource#">
		select distinct scsd.sectionDefID, algorithmID, graphAxis from scoreCardSectionDef scsd inner join
			scorecardFlagGroupDef scfgd on scsd.sectionDefID = scfgd.sectionDefID
		where scsd.scoreCardDefID =  <cf_queryparam value="#scoreCardDefID#" CFSQLTYPE="CF_SQL_INTEGER" > 
			and scfgd.active=1
	</cfquery>
	
	<!--- loop through the sections, and add totals where all the flaggroups in the section have at least one answer --->
	<cfloop query="getScoreCardSections">
		<cfset totalSectionScore=0>
		<cfset totalSectionWeightedScore=0>
			
		<cfswitch expression="#algorithmID#">
			<cfcase value="1">
				
				<cfquery name="getSectionWeightings" datasource="#application.siteDataSource#">
					exec sp_getScoreCardSectionWeightings @organisationID =  <cf_queryparam value="#getScoreCardsNeedingUpdating.entityID[rowCount]#" CFSQLTYPE="CF_SQL_INTEGER" > , @scorecardDefID =  <cf_queryparam value="#getScoreCardsNeedingUpdating.scoreCardDefID[rowCount]#" CFSQLTYPE="CF_SQL_INTEGER" > , @sectionDefID =  <cf_queryparam value="#sectionDefID#" CFSQLTYPE="CF_SQL_INTEGER" > 
				</cfquery>
				
				<!--- if the section does not contain an unanswered question, total the scores for the section score --->
				<cfif (completedScorecardsOnly and not listfind(valueList(getSectionWeightings.numberAnswered),0)) or (not completedScorecardsOnly)>
					<cfset totalSectionScore = arraySum(ListToArray(valueList(getSectionWeightings.score)))>
					<cfset totalSectionWeightedScore = arraySum(ListToArray(valueList(getSectionWeightings.weightedScore)))>
					
					<!--- applying the country weighting --->
					<!--- <cfset totalSectionWeightedScore = totalSectionWeightedScore * getScoreCardsNeedingUpdating.countryWeighting>
					
					<!--- applying the graph axis weighting --->
					<cfset totalSectionWeightedScore = totalSectionWeightedScore * evaluate("getScoreCardsNeedingUpdating.#graphAxis#Weighting")>
				 ---></cfif>
				
			</cfcase>
		</cfswitch>
		
		<!--- insert/update the section total --->
		<cfif (completedScorecardsOnly and totalSectionScore neq 0) or (not completedScorecardsOnly)>
			<cfquery name="insertSectionScore" datasource="#application.siteDataSource#">
				if exists
					(select sectionDefID from scoreCardSection where 
						sectionDefID =  <cf_queryparam value="#sectionDefID#" CFSQLTYPE="CF_SQL_INTEGER" >  and scoreCardID =  <cf_queryparam value="#getScoreCardsNeedingUpdating.scoreCardID[rowCount]#" CFSQLTYPE="CF_SQL_INTEGER" > )

					update scoreCardSection set sectionTotal =  <cf_queryparam value="#totalSectionScore#" CFSQLTYPE="cf_sql_float" > , sectionTotalWeighted =  <cf_queryparam value="#totalSectionWeightedScore#" CFSQLTYPE="CF_SQL_float" > 
						where scoreCardID =  <cf_queryparam value="#getScoreCardsNeedingUpdating.scoreCardID[rowCount]#" CFSQLTYPE="CF_SQL_INTEGER" > 
							and sectionDefID =  <cf_queryparam value="#sectionDefID#" CFSQLTYPE="CF_SQL_INTEGER" > 
				else
					insert into scoreCardSection (scorecardID,sectionDefID,sectionTotal,sectionTotalWeighted)values 
						(<cf_queryparam value="#getScoreCardsNeedingUpdating.scoreCardID[rowCount]#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#sectionDefID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#totalSectionScore#" CFSQLTYPE="cf_sql_float" >,<cf_queryparam value="#totalSectionWeightedScore#" CFSQLTYPE="CF_SQL_float" >)
			</cfquery>
		</cfif>
		
	</cfloop>
	
	
	<cfloop list="x,y" index="graphAxisIdx">
		
		<!--- if all sections are totalled for a particular axis, total the scorecard for that axis --->
		<cfquery name="getSectionTotals" datasource="#application.siteDataSource#">
			select distinct scsd.sectionDefID, isNull(scs.sectionTotal,0) as sectionTotal, isNull(scs.sectionTotalWeighted,0) as sectionTotalWeighted
			from scoreCardSectionDef scsd left outer join 
				scoreCardSection scs on scsd.sectionDefID = scs.sectionDefID and scs.scoreCardID =  <cf_queryparam value="#scoreCardID#" CFSQLTYPE="CF_SQL_INTEGER" >  inner join
				scoreCardFlagGroupDef scfgd on scfgd.sectionDefID = scsd.sectionDefID
			where scsd.scoreCardDefID =  <cf_queryparam value="#getScoreCardsNeedingUpdating.scoreCardDefID[rowCount]#" CFSQLTYPE="CF_SQL_INTEGER" > 
				and graphAxis =  <cf_queryparam value="#graphAxisIdx#" CFSQLTYPE="CF_SQL_VARCHAR" > 
				and scfgd.active = 1
		</cfquery>
		
		<!--- if we don't have a section total, it's total is 0. So, if no section has a score of 0, add the section totals --->
		<cfif (completedScorecardsOnly and not listfind(valueList(getSectionTotals.sectionTotal),0)) or (not completedScorecardsOnly)>
			<cfset totalScoreCardScore = arraySum(ListToArray(valueList(getSectionTotals.sectionTotal)))>
			<cfset totalScoreCardWeightedScore = arraySum(ListToArray(valueList(getSectionTotals.sectionTotalWeighted)))>
			
			<cfquery name="insertScoreCardScore" datasource="#application.siteDataSource#">
				update scoreCard set 
					<cfif graphAxisIdx eq "x">
						xTotal =  <cf_queryparam value="#totalScoreCardScore#" CFSQLTYPE="CF_SQL_Integer" > , xTotalWeighted =  <cf_queryparam value="#totalScoreCardWeightedScore#" CFSQLTYPE="CF_SQL_float" > ,
					<cfelseif graphAxisIdx eq "y">
						yTotal =  <cf_queryparam value="#totalScoreCardScore#" CFSQLTYPE="CF_SQL_Integer" > , yTotalWeighted =  <cf_queryparam value="#totalScoreCardWeightedScore#" CFSQLTYPE="CF_SQL_float" > ,
					</cfif>
					lastUpdated =  <cf_queryparam value="#now()#" CFSQLTYPE="CF_SQL_TIMESTAMP" > , lastUpdatedBy=#request.relayCurrentUser.personID#
				where scoreCardID =  <cf_queryparam value="#scoreCardID#" CFSQLTYPE="CF_SQL_INTEGER" > 
			</cfquery>
		</cfif>
	</cfloop>
	
	<cfquery name="setUpdateRequiredToNull" datasource="#application.siteDataSource#">
		update scoreCard set updateRequired=null, lastUpdated = getDate(), lastUpdatedBy = #request.relayCurrentUser.userGroupID# 
		where scoreCardID =  <cf_queryparam value="#scoreCardID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</cfquery>
	
	
</cfloop>

<cfquery name="getTotalScoreCardsNeedingUpdating" datasource="#application.siteDataSource#">
	select count(*) as numRecs
	from scorecard sc inner join 
		scoreCardDef scd on sc.scoreCardDefID = scd.scoreCardDefID 
	where updateRequired is not null
	<cfif entityID neq 0>
		and entityID =  <cf_queryparam value="#entityID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</cfif>
</cfquery>

<cfset logText = logText & "#getTotalScoreCardsNeedingUpdating.numRecs# score card records are left that need updating in total">
<cfoutput>#logText#</cfoutput>
<cflog text="#logText#" file="scoreCardUpdates" type="Information" thread="yes" date="yes" time="yes" application="yes">


