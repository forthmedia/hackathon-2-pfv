<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			initialiseOrgScorecards.cfm	
Author:				
Date started:		2007-04-23
	
Description:		Creates scorecard entries for organisations that don't yet have a scorecard for initialisation purposes.
						

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->
 



<cf_head>
	<cf_title>Initialise Organisation Scorecards</cf_title>
</cf_head>



<cfparam name="scorecardDefID" type="numeric" default=1>

<cfquery name="getOrgsWithoutScorecards" datasource="#application.siteDataSource#" maxrows="500">
	select organisationID,organisationName from organisation where organisationID not in
		(select entityID from scorecard)
</cfquery>

<cfloop query="getOrgsWithoutScorecards">
	<cfoutput>Creating scorecard for #htmleditformat(organisationName)# (#htmleditformat(organisationID)#).</cfoutput>
	<cfquery name="insertOrgScoreCard" datasource="#application.siteDataSource#">
		insert into scorecard (scorecardDefId,entityID,updateRequired) values
			(<cf_queryparam value="#scorecardDefID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#organisationID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#now()#" CFSQLTYPE="CF_SQL_TIMESTAMP" >)
	</cfquery>
	
	<cfset entityID=organisationID>
	<cfset completedScorecardsOnly = false>
	<cfinclude template="updateScoreCardTotal.cfm">
</cfloop>





