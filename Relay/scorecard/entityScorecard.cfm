<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			entityScorecard.cfm	
Author:				NJH
Date started:		2007/04/20
	
Description:		The file displays an organisations scorecard. If the user has the correct privileges, a submit
					button is displayed which updates the scorecard. Only Scorecard:Level3 users should be able to update
					the scorecard.
					
					It currently works when included into a screen.	

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->


<cfparam name="frmOrganisationID" type="numeric">
<cfparam name="scoreCardDefID" type="numeric" default="1">


<script type="text/javascript">
	function setDirtyFlag() {
		var form = document.mainForm;
		form.dirty.value=1;
	}
</script>

<cf_translate>
<cfif application.com.login.checkInternalPermissions("scorecardTask","level1")>

	<!--- need to check scorecard task... should be level 3 --->
	<cfif application.com.login.checkInternalPermissions("scorecardTask","level3") and (isDefined("dirty") and dirty eq 1)>
		<cfsilent>
			<cfset orgID = frmOrganisationID>
			
			<!--- update scorecardProfiles --->
			<cfif fileExists("#application.paths.code#\cftemplates\updateScorecardFlags.cfm")>
				<cfinclude template="/code/cftemplates/updateScoreCardFlags.cfm">
			</cfif>
			
			<!--- update the scorecards --->
			<cfset entityID = frmOrganisationID>
			<cfset completedScorecardsOnly = false>
			<cfinclude template="/scorecard/updateScoreCardTotal.cfm">
			
			<!--- update scorecard tier mappings --->
			<cfinclude template="/scorecard/updateScoreCardTierMapping.cfm">
			
			<!--- update scorecard variance record for organisation --->
			<cfinclude template="/scorecard/updateScoreCardVarianceTable.cfm">
		</cfsilent>
	</cfif>
	
	<cfquery name="getEntityScoreCard" datasource="#application.siteDataSource#">
		select * from scorecard where entityID =  <cf_queryparam value="#frmOrganisationID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</cfquery>
	
	<cfif getEntityScoreCard.recordCount gt 0>
		<cfquery name="getScoreCardSections" datasource="#application.siteDataSource#">
			select distinct scsd.sectionDefID,description,
				isNull(sectionTotal,0) as sectionTotal, 
				isNull(sectionTotalWeighted,0) as sectionTotalWeighted,
				graphAxis,
				scsd.weighting as sectionWeighting
			from scoreCardSectionDef scsd 
				left join scorecardSection scs on scsd.sectionDefID = scs.sectionDefID 
					and scoreCardID =  <cf_queryparam value="#getEntityScoreCard.scoreCardID#" CFSQLTYPE="CF_SQL_INTEGER" >  
				inner join scoreCardFlagGroupDef scfgd on scfgd.sectionDefID = scsd.sectionDefID
			where active=1 
			order by graphAxis,description
		</cfquery>
		
		<cfquery name="getOrgName" datasource="#application.siteDataSource#">
			select organisationName, countryID from organisation where organisationID =  <cf_queryparam value="#frmOrganisationID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfquery>
		
		
		<!--- <cfquery name="getScoreCardTierPercentile" datasource="#application.siteDataSource#">
			select * from scoreCardTierPercentile where countryID = #getOrgName.countryID#
		</cfquery> --->
	
		
		<table cellpadding=0 cellspacing=0 class="withBorder" width="100%">
			<cfoutput><tr><th colspan="3">#htmleditformat(getOrgName.organisationName)# phr_scorecard_scorecard</th></tr></cfoutput>
			<tr><th>phr_scorecard_section</th><th>phr_scorecard_sectionTotal</th><th>phr_scorecard_sectionWeighting</th></tr>
			<tr><td colspan="3">&nbsp;</td></tr>
			<cfoutput query="getScoreCardSections" group="graphAxis">
				<tr>
					<th colspan="3">#htmleditformat(graphAxis)# Axis</th>
				</tr>
				
				
					<cfoutput>
				<tr>
					<td width="40px"><span class="label"><a href="javascript:openWin('/scorecard/entityScorecardFlagCalculations.cfm?organisationID=#frmOrganisationID#&sectionDefID=#getScoreCardSections.sectionDefID#','FlagCalculations','width=950,height=600,toolbar=0,location=0,directories=0,status=1,menuBar=0,scrollBars=1,resizable=1');">#htmleditformat(description)#</a></span></td>
					<td align="center">#numberFormat(sectionTotalWeighted,"_.__")#</td>
					<td align="center">#numberFormat(sectionWeighting*100)#%</td></tr>
					</cfoutput>
				
				<tr>
					<td style="border-top:1px solid ##000000">&nbsp;</td>
					<td align="center" style="border-top:1px solid ##000000">#numberFormat(evaluate("getEntityScoreCard."&graphAxis&"TotalWeighted"),"_.__")#%</td>
					<td style="border-top:1px solid ##000000">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="3">&nbsp;</td>
				</tr>
			</cfoutput>

			<cfif application.com.login.checkInternalPermissions("scorecardTask","level1")>
				<tr><td colspan="3" align="right">
					<input type="button" name="frmUpdateScoreCard" id="frmUpdateScoreCard" value="phr_scorecard_recalculateScorecard" onClick="setDirtyFlag();verifyForm();">
					<input type="hidden" name="dirty" id="dirty" value="0">
				</td></tr>
			</cfif>
			<!--- <tr>
				<td colspan="2">
				<cfoutput>
				AffinityOne requires scores greater than x: #numberFormat(getScoreCardTierPercentile.x75Percentile,"___,___")# 
					and y: #numberFormat(getScoreCardTierPercentile.y75Percentile,"___,___")#<br>
				AffinityPlus requires scores greater than x: #numberFormat(getScoreCardTierPercentile.x50Percentile,"___,___")# 
					and y: #numberFormat(getScoreCardTierPercentile.y50Percentile,"___,___")#	
				</cfoutput>
				</td>
			</tr> --->
		
		</table>
	
	<cfelse>
		<table>
			<tr>
				<td>There no scorecard records for this account.</td>
			</tr>
			<cfif application.com.login.checkInternalPermissions("scorecardTask","level3")>
				<tr><td colspan="2" align="right">
					<input type="button" name="frmUpdateScoreCard" id="frmUpdateScoreCard" value="phr_scorecard_recalculateScorecard" onClick="setDirtyFlag();verifyForm();">
					<input type="hidden" name="dirty" id="dirty" value="0">
				</td></tr>
			</cfif>
		</table>

	</cfif>
</cfif>
</cf_translate>

