<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			programRules.cfm	
Author:				SWJ
Date started:		22-Aug-08
Description:		Developed for defining program rules and threshholds.	

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:

TODO: eLearning SWJ Module.cfm - add group by course, check visibility, check fileList
 --->




<cf_head>
	<cf_title>Program Rules</cf_title>
</cf_head>



<cfparam name="getData" type="string" default="foo">
<cfparam name="sortOrder" default="Title_Of_Course">

<!--- save the content for the xml to define the editor 

programRulesID, 
revenue_Tier_1, revenue_Tier_2, revenue_Tier_3, 
training_Tier_1, training_Tier_2, training_Tier_3, 
demo_Req_Tier_1, demo_Req_Tier_2, demo_Req_Tier_3

--->
<cfsavecontent variable="xmlSource">
	<cfoutput>
	<editors>
		<editor id="232" name="thisEditor" entity="programRules" title="Program Rules Editor">
			<field name="programRulesID" label="Program Rule ID" description="This records unique ID." control="hidden"></field>
			<field name="countryID" label="Country Group" description="." control="select" 
					query="select c.countryDescription as display, c.countryID as value from country c where countryID in (287,288,289,290)"></field>
			<field name="revenue_Tier_1" label="Revenue Target Gold" description=""></field>
			<field name="revenue_Tier_2" label="Revenue Target Platinum" description=""></field>
			<field name="revenue_Tier_3" label="Revenue Target Elite" description=""></field>
			
			<field name="training_Tier_1" label="Training Target Gold" description=""></field>
			<field name="training_Tier_2" label="Training Target Platinum" description=""></field>
			<field name="training_Tier_3" label="Training Target Elite" description=""></field>
			
			<field name="demo_Req_Tier_1" label="Demo Required for Gold" description="" control="YorN"></field>
			<field name="demo_Req_Tier_2" label="Demo Required for Platinum" description="" control="YorN"></field>
			<field name="demo_Req_Tier_3" label="Demo Required for Elite" description="" control="YorN"></field>
		</editor>
		
	</editors>
	</cfoutput>
</cfsavecontent>


<cfif not isDefined("URL.editor")>
	<CFQUERY NAME="getData" datasource="demo">
		SELECT * from vProgramRules order by country_group
	 </CFQUERY>
	<!--- <cfset getData = application.com.relayElearning.GetTrainingModulesData(sortOrder=#sortOrder#)> --->
</cfif>

<cf_listAndEditor 
	xmlSource="#xmlSource#" 
	cfmlCallerName="programRules"
	keyColumnList="country_Group"
	keyColumnKeyList="programRulesID"
	
	showRecordRights = "true"
	
	recordRightsLabel = "Who should see this module?"
		
	showTheseColumns="Country_Group,Revenue_Tier_1,Revenue_Tier_2,Revenue_Tier_3"
	
	FilterSelectFieldList="Country_Group"
	FilterSelectFieldList2="Country_Group"
	
	groupByColumns="#sortOrder#"
	
	useInclude="false"
	sortOrder="#sortOrder#"
	topHead="/scorecard/scorecardTopHead.cfm"
	queryData="#getData#"
>



