<!--- �Relayware. All Rights Reserved 2014 --->

<CFPARAM NAME="sortOrder" DEFAULT="organisationName">
<CFPARAM NAME="screenTitle" DEFAULT="Scorecard Tiering Report">
<cfparam name="countryID" type="integer" default="#request.relayCurrentUser.countryID#">

<cf_head>
	<cfoutput><cf_title>#htmleditformat(screenTitle)#</cf_title></cfoutput>
	
	<style>
		td.top {
			background-color: #626262;
			width: 50;
			height: 50;
		}
		
		td.middle {
			background-color: #929292;
			width: 50;
			height: 50;
		}
		
		td.bottom {
			background-color: #b4b4b4;
			width: 50;
			height: 50;
		}
		
		table.tierChart{
			border: 1 Gray;
			width: 100%;
		}
	</style>
</cf_head>



	<CF_relayPageSubMenu pageTitle="#screenTitle#">
	
<p>The tiering report shows how the quadrants map to the partner Tiers</p>
<table>
	<tr>
		<td>
			<table border="1">
				<tr>
					<td class="middle">&nbsp;</td>
					<td class="middle">&nbsp;</td>
					<td class="top">&nbsp;</td>
					<td class="top">&nbsp;</td>
				</tr>
				<tr>
					<td class="bottom">&nbsp;</td>
					<td class="middle">&nbsp;</td>
					<td class="middle">&nbsp;</td>
					<td class="top">&nbsp;</td>
				</tr>
				<tr>
					<td class="bottom">&nbsp;</td>
					<td class="bottom">&nbsp;</td>
					<td class="middle">&nbsp;</td>
					<td class="middle">&nbsp;</td>
				</tr>
				<tr>
					<td class="bottom">&nbsp;</td>
					<td class="bottom">&nbsp;</td>
					<td class="bottom">&nbsp;</td>
					<td class="middle">&nbsp;</td>
				</tr>
			</table>
		</td>
		<td width="100">&nbsp;</td>
		<td valign="top">
			<table border="1">
				<tr>
					<td><strong>Key:</strong></td><td>&nbsp;</td>
				</tr>
				<tr>
					<td><p>Affinity One</p></td><td class="top">&nbsp;</td>
				</tr>
				<tr>
					<td><p>Affinity Plus</p></td><td class="middle">&nbsp;</td>
				</tr>
				<tr>
					<td><p>Affinity</p></td><td class="bottom">&nbsp;</td>
				</tr>
			</table>
		</td>
	</tr>
</table>


