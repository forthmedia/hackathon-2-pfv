<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		updateScoreCardVarianceTable.cfm	
Author:			NJH  
Date started:	2007-???
	
Description:	Updates the scorecard variance table		

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
05-Dec-2008			NJH			Bug Fix Trend Support Issue 1443 - Changed a delete query. Any scorecard variance where the agreed_tier is null was not getting deleted and
								therefore not getting updated.


Possible enhancements:


 --->





<cf_head>
	<cf_title>Update Scorecard Variance</cf_title>
</cf_head>


<p>This process updates the scorecard tier information</p>

<cfparam name="clearVariance" type="boolean" default="false">

<!--- CR-LEN507 NJH 2008-01-25 added Lenovo version... --->
<cfif fileexists("#application.paths.code#\cftemplates\updateScorecardVarianceTable.cfm")>
<!--- Client version loads if there is one --->
	<cfinclude template ="/code/cftemplates/updateScorecardVarianceTable.cfm">
<cfelse>

<!--- if we're updating on the whole table, rather than for a single org --->
<cfif not isDefined("orgID")>

	<cfif clearVariance>
		<cfquery name="truncateScorecardVariance" datasource="#application.siteDataSource#">
			truncate table ScoreCardVariance
		</cfquery>
	<cfelse>
		<!--- delete only the records that have changed --->
		<cfquery name="deleteChangedVarianceRecs" datasource="#application.siteDataSource#">
			delete scorecardVariance 
			from scorecardVariance v WITH (NOLOCK)
				LEFT join 
				(select bfd.entityID, f.name from booleanFlagData bfd WITH (NOLOCK)
					inner join flag f WITH (NOLOCK) on bfd.flagID = f.flagID
					where flagGroupID=507
				) as x on x.entityid = v.organisationID and x.name = v.scorecard_derived_tier
			where
				x.entityID is null
				
				-- NJH 2008-04-03 also delete scorecard recs in which the agreed tier has changed
				delete scorecardVariance
				from scorecardVariance v WITH (NOLOCK)
					LEFT join 
					(select bfd.entityID, f.name from booleanFlagData bfd WITH (NOLOCK)
						inner join flag f WITH (NOLOCK) on bfd.flagID = f.flagID
						where flagGroupID=509
					) as x on x.entityid = v.organisationID and x.name = v.agreed_tier
				where
					x.entityID is null 
					-- NJH 2008-12-05 Bug Fix Trend Support 1443 removed  and v.agreed_tier is not null
		</cfquery>
		
		<!--- if orgs get merged, we get duplicate rows in variance table. We clean these and re-insert the correct tier for the org --->
		<cfquery name="deleteVarianceRecsForMergedOrgs" datasource="#application.siteDataSource#">
			delete from scorecardvariance where organisationID in (
				select organisationID from scorecardvariance 
				group by organisationID
				having count(*) > 1)
		</cfquery>
	</cfif>

	<!--- insert into the table records that should be in here but that aren't --->
	<cfquery name="updateScorecardVarianceTable" datasource="#application.siteDataSource#">
		insert into ScoreCardVariance 
			(organisationID,organisation_Name,isoCode,countryID,Scorecard_Derived_Tier,Scorecard_Derived_Tier_index,Agreed_Tier,Agreed_Tier_index)	
		SELECT 
		   o.OrganisationID, o.OrganisationName as organisation_Name, c.ISOCode, c.CountryID,
		      (SELECT     f.Name
		        FROM          dbo.flag AS f WITH (NOLOCK) INNER JOIN
		                               dbo.BooleanFlagData AS bfd WITH (NOLOCK) ON bfd.FlagID = f.FlagID INNER JOIN
		                               dbo.FlagGroup AS fg WITH (NOLOCK) ON fg.FlagGroupID = f.FlagGroupID
		        WHERE      (fg.FlagGroupTextID = 'ScoreCardDerivedPartnerTier') 
					AND (bfd.EntityID = o.OrganisationID)) 
					AS Scorecard_Derived_Tier,
		      (SELECT     f.orderingIndex
		        FROM          dbo.flag AS f WITH (NOLOCK) INNER JOIN
		                               dbo.BooleanFlagData AS bfd WITH (NOLOCK) ON bfd.FlagID = f.FlagID INNER JOIN
		                               dbo.FlagGroup AS fg WITH (NOLOCK) ON fg.FlagGroupID = f.FlagGroupID
		        WHERE      (fg.FlagGroupTextID = 'ScoreCardDerivedPartnerTier') 
					AND (bfd.EntityID = o.OrganisationID)) 
					AS Scorecard_Derived_Tier_index,
		
		      (SELECT     f.Name
		        FROM          dbo.flag AS f WITH (NOLOCK) INNER JOIN
		                               dbo.BooleanFlagData AS bfd WITH (NOLOCK) ON bfd.FlagID = f.FlagID INNER JOIN
		                               dbo.FlagGroup AS fg WITH (NOLOCK) ON fg.FlagGroupID = f.FlagGroupID
		        WHERE      (fg.FlagGroupTextID = 'AgreedPartnerTier') 
					AND (bfd.EntityID = o.OrganisationID)) 
					AS Agreed_Tier,
		      (SELECT     f.orderingIndex
		        FROM          dbo.flag AS f WITH (NOLOCK) INNER JOIN
		                               dbo.BooleanFlagData AS bfd WITH (NOLOCK) ON bfd.FlagID = f.FlagID INNER JOIN
		                               dbo.FlagGroup AS fg WITH (NOLOCK) ON fg.FlagGroupID = f.FlagGroupID
		        WHERE      (fg.FlagGroupTextID = 'AgreedPartnerTier') 
					AND (bfd.EntityID = o.OrganisationID)) 
					AS Agreed_Tier_index
		FROM         dbo.organisation AS o WITH (NOLOCK) INNER JOIN
		             dbo.Country AS c WITH (NOLOCK) ON c.CountryID = o.countryID
		WHERE     (o.organisationTypeID = 1)
			--GCC removed to include orgs with no agreed tier flag
--and organisationID in (select entityID from booleanFlagData WITH (NOLOCK) where flagID in 
			--		(select flagID from vFlagDef where flagGroupTextID = 'agreedPartnerTier'))
			and organisationID not in 
				(select organisationID from scorecardVariance WITH (NOLOCK))
	</cfquery>
	
	<!--- <cfquery name="updateScorecardVarianceTable" datasource="#application.siteDataSource#">
		DROP TABLE ScoreCardVariance
		--GO
		SELECT 
		   o.OrganisationID, o.OrganisationName as organisation_Name, c.ISOCode, c.CountryID,
		      (SELECT     f.Name
		        FROM          dbo.flag AS f INNER JOIN
		                               dbo.BooleanFlagData AS bfd ON bfd.FlagID = f.FlagID INNER JOIN
		                               dbo.FlagGroup AS fg ON fg.FlagGroupID = f.FlagGroupID
		        WHERE      (fg.FlagGroupTextID = 'ScoreCardDerivedPartnerTier') 
					AND (bfd.EntityID = o.OrganisationID)) 
					AS Scorecard_Derived_Tier,
		      (SELECT     f.orderingIndex
		        FROM          dbo.flag AS f INNER JOIN
		                               dbo.BooleanFlagData AS bfd ON bfd.FlagID = f.FlagID INNER JOIN
		                               dbo.FlagGroup AS fg ON fg.FlagGroupID = f.FlagGroupID
		        WHERE      (fg.FlagGroupTextID = 'ScoreCardDerivedPartnerTier') 
					AND (bfd.EntityID = o.OrganisationID)) 
					AS Scorecard_Derived_Tier_index,
		
		      (SELECT     f.Name
		        FROM          dbo.flag AS f INNER JOIN
		                               dbo.BooleanFlagData AS bfd ON bfd.FlagID = f.FlagID INNER JOIN
		                               dbo.FlagGroup AS fg ON fg.FlagGroupID = f.FlagGroupID
		        WHERE      (fg.FlagGroupTextID = 'AgreedPartnerTier') 
					AND (bfd.EntityID = o.OrganisationID)) 
					AS Agreed_Tier,
		      (SELECT     f.orderingIndex
		        FROM          dbo.flag AS f INNER JOIN
		                               dbo.BooleanFlagData AS bfd ON bfd.FlagID = f.FlagID INNER JOIN
		                               dbo.FlagGroup AS fg ON fg.FlagGroupID = f.FlagGroupID
		        WHERE      (fg.FlagGroupTextID = 'AgreedPartnerTier') 
					AND (bfd.EntityID = o.OrganisationID)) 
					AS Agreed_Tier_index
		into ScoreCardVariance
		FROM         dbo.organisation AS o INNER JOIN
		                      dbo.Country AS c ON c.CountryID = o.countryID
		WHERE     (o.organisationTypeID = 1)
		and organisationID in (select entityID from booleanFlagData where flagID in 
					(select flagID from vFlagDef where flagGroupTextID = 'agreedPartnerTier'))
	</cfquery> --->

<!--- else we're updating the variance table for a single org --->
<cfelse>

	<cfquery name="updateScorecardVarianceTableForOrg" datasource="#application.siteDataSource#">
		DELETE FROM ScoreCardVariance WHERE organisationID =  <cf_queryparam value="#orgID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		
		INSERT INTO ScoreCardVariance (OrganisationID,Organisation_Name,ISOCode,CountryID,Scorecard_Derived_Tier,Scorecard_Derived_Tier_index,Agreed_Tier,Agreed_Tier_index)
		SELECT 
		   o.OrganisationID, o.OrganisationName as organisation_Name, c.ISOCode, c.CountryID,
		      (SELECT     f.Name
		        FROM          dbo.flag AS f WITH (NOLOCK) INNER JOIN
		                               dbo.BooleanFlagData AS bfd WITH (NOLOCK) ON bfd.FlagID = f.FlagID INNER JOIN
		                               dbo.FlagGroup AS fg WITH (NOLOCK) ON fg.FlagGroupID = f.FlagGroupID
		        WHERE      (fg.FlagGroupTextID = 'ScoreCardDerivedPartnerTier') 
					AND (bfd.EntityID = o.OrganisationID)) 
					AS Scorecard_Derived_Tier,
		      (SELECT     f.orderingIndex
		        FROM          dbo.flag AS f WITH (NOLOCK) INNER JOIN
		                               dbo.BooleanFlagData AS bfd WITH (NOLOCK) ON bfd.FlagID = f.FlagID INNER JOIN
		                               dbo.FlagGroup AS fg WITH (NOLOCK) ON fg.FlagGroupID = f.FlagGroupID
		        WHERE      (fg.FlagGroupTextID = 'ScoreCardDerivedPartnerTier') 
					AND (bfd.EntityID = o.OrganisationID)) 
					AS Scorecard_Derived_Tier_index,
		
		      (SELECT     f.Name
		        FROM          dbo.flag AS f WITH (NOLOCK) INNER JOIN
		                               dbo.BooleanFlagData AS bfd WITH (NOLOCK) ON bfd.FlagID = f.FlagID INNER JOIN
		                               dbo.FlagGroup AS fg WITH (NOLOCK) ON fg.FlagGroupID = f.FlagGroupID
		        WHERE      (fg.FlagGroupTextID = 'AgreedPartnerTier') 
					AND (bfd.EntityID = o.OrganisationID)) 
					AS Agreed_Tier,
		      (SELECT     f.orderingIndex
		        FROM          dbo.flag AS f WITH (NOLOCK) INNER JOIN
		                               dbo.BooleanFlagData AS bfd WITH (NOLOCK) ON bfd.FlagID = f.FlagID INNER JOIN
		                               dbo.FlagGroup AS fg WITH (NOLOCK) ON fg.FlagGroupID = f.FlagGroupID
		        WHERE      (fg.FlagGroupTextID = 'AgreedPartnerTier') 
					AND (bfd.EntityID = o.OrganisationID)) 
					AS Agreed_Tier_index

		FROM         dbo.organisation AS o WITH (NOLOCK) INNER JOIN
		                      dbo.Country AS c WITH (NOLOCK) ON c.CountryID = o.countryID
		WHERE     (o.organisationTypeID = 1) and (o.organisationID =  <cf_queryparam value="#orgID#" CFSQLTYPE="CF_SQL_INTEGER" > )
	</cfquery>

</cfif>
</cfif>



