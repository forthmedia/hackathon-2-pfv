<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			scorecardTierRequestApproval.cfm	
Author:				NJH
Date started:		2008/04/03
	
Description:		This file approves or rejects scorecard tier changes for organisations. It sets a flag which is passed
					in. If the sendTierChangeApprovalEmail variable is true, an email is sent to the account manager notifying them
					of the acceptance or rejection of the tier change request .

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->




<cf_head>
	<cf_title>Scorecard Tier Request Approval</cf_title>
</cf_head>



<cfparam name="approvalFlagTextID" type="string">
<cfparam name="orgIDList" type="string">
<cfparam name="sendTierChangeApprovalEmail" type="boolean" default="false">


<cfif fileexists("#application.paths.code#\cftemplates\scorecardTierRequestApproval.cfm")>
	<cfinclude template ="/code/cftemplates/scorecardTierRequestApproval.cfm">
<cfelse>

	<cfif application.com.flag.doesFlagExist(approvalFlagTextID)>
		<cfset thisFlagID = application.com.flag.getFlagStructure(approvalFlagTextID).flagID>
		<cfset emailDefExists = 0>
		
		<cfif sendTierChangeApprovalEmail>
			<!--- if we're approving tier change requests --->
			<cfif approvalFlagTextID eq "tierChangeApproved">
				<cfset scorecardApprovalEmailTextID ="TierChangeAccepted">
			<!--- if we're denying tier change requests --->
			<cfelseif approvalFlagTextID eq "tierChangeDenied">
				<cfset scorecardApprovalEmailTextID ="TierChangeRejected">
			<cfelse>
				<cfset scorecardApprovalEmailTextID ="">
			</cfif>
			
			<cfset emailDefExists = application.com.email.doesEmailDefExist(emailTextID=scorecardApprovalEmailTextID)>
		</cfif>
		
		<!--- getting the list of orgs that need the flag set and any email addresses that we may need 
			We get the email address of the person who's requested the tier change and get the account manager to cc the email to.
		--->
		<cfquery name="getOrgsAndEmailRecipients" datasource="#application.siteDataSource#">
			select v.organisationID, isNull(v.requestBy_personID,0) as recipientID, isNull(amp.email,'') as accManagerEmail, isNull(amp.personID,0) as accManagerPersonID from vScorecardVariance v 
				left outer join (#application.com.flag.getFlagStructure("AccManager").flagType.dataTableFullName# am with (noLock)
					inner join person amp with (noLock) on amp.personID = am.data
					inner join flag fam with (noLock) on fam.flagID = am.flagID and fam.flagTextID = 'AccManager'
				) on am.entityID = v.organisationID
			where v.organisationID  in ( <cf_queryparam value="#orgIDList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		</cfquery>
		
		<cfloop query="getOrgsAndEmailRecipients">
			<cfif recipientID eq accManagerPersonID>
				<cfset accManagerEmail = "">
			</cfif>
			<cfscript>
				if (sendTierChangeApprovalEmail and emailDefExists and recipientID neq 0) {
					application.com.email.sendEmail(emailTextID=scorecardApprovalEmailTextID,personID=recipientID,ccAddress=accManagerEmail);
				}
				application.com.flag.setFlagData(entityID=organisationID,flagID=thisFlagID,data="");
			</cfscript>
		</cfloop>
		
		<cfif structKeyExists(session,"varianceReportCacheID") and getOrgsAndEmailRecipients.recordCount gt 0>
			<!--- force the scorecard variance query to re-run in the parent window if we've updated some flags--->
			<cfset session.varianceReportCacheID = session.varianceReportCacheID + 1>
		</cfif>
		
	<cfelse>
		<cfoutput>Attribute '#approvalFlagTextID#' is not defined. Please set it up in profile manager.</cfoutput>
	</cfif>
	
	<br>
	<br>
	
	<a href="javascript:if(!opener.closed){opener.location.href=opener.location.href};self.close();">Close Window</a>
	
	
	
</cfif>
