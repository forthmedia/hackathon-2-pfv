<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			updateScorecardTierMapping.cfm	
Author:				NJH/WAB
Date started:		2007-06-19
	
Description:		This updates the reseller tierings based on where they fall in the various quadrants

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed

30-10-2007	NJH		Include a client template if it exists after the tier mapping has run.

Possible enhancements:

 --->
<cfsetting RequestTimeout = "1200">  
<cfset timeStamp = createODBCdatetime(request.requestTime)>
<cfparam name="scorecardDefID" type="numeric" default=1>
<cfparam name="defaultTierFlagID" type="numeric" default="2081">

<!--- setting the formulas that determine our lines that then determine the partner tier a reseller falls into --->
<cfset blankFormula = structNew()>
<cfset blankFormula["straight"] = "'YtotalWeighted - (##M##*xtotalWeighted) - ##b##'">  <!--- a straight line:  y=mx+b--->
<cfset blankFormula["ellipse"] = "'(Xtotal * xtotal)/(##a## * ##a##) + (Ytotal * Ytotal)/(##b## * ##b##) - 1'">  <!--- an ellipse: (x^2/a^2) + (y^2/b^2) = 1--->

<!--- get the variables/values that we need in our formula --->
<cfquery name="getLineDefinitions" datasource="#application.siteDataSource#">
	select rtrim(varName) as varName, rtrim(varValue) as varValue, lineNumber from scorecardTierVar 
	where scorecardDefID =  <cf_queryparam value="#scorecardDefID#" CFSQLTYPE="CF_SQL_INTEGER" >  
	order by lineNumber 
</cfquery>

<cfset theLine = structNew()>
<cfoutput query="getLineDefinitions" group="lineNumber">
	<!--- setting the variables to their corresponding values --->
	<cfoutput>
		<cfset "#varname#" = varvalue>
	</cfoutput>
	<!--- substituting the variables with the actual values in the line's formula --->
	<cfset theLine[lineNumber] = evaluate(blankFormula[lineType])>
</cfoutput>


<cfquery name="getFormulae" datasource="#application.siteDataSource#">
	select * from scorecardTier where scorecardDefID =  <cf_queryparam value="#scorecardDefID#" CFSQLTYPE="CF_SQL_INTEGER" >  order by tierLevel desc
</cfquery>

<cfset tierFlagIDList = valueList(getFormulae.tierFlagID)> 

<!--- creating the case statement --->
<cfset caseStatement = "case">
<cfloop query="getFormulae">
	<cfset tempFormula = formula>
	<!--- replace 'Above' with '0 <' and 'Below' with '0 >' --->
	<cfset tempFormula = replaceNoCase(tempFormula,"Above","0 <", "ALL")>
	<cfset tempFormula = replaceNoCase(tempFormula,"Below","0 >=", "ALL")>
	<!--- replace the word 'line' with the formula for that line --->
	<cfloop index="idx" from="1" to="#structCount(theLine)#" step="1">
		<cfset tempFormula = replaceNoCase(tempFormula,"Line#idx#",theLine[idx], "ALL")>
	</cfloop>
	<cfset caseStatement = caseStatement & " when #tempFormula# then #tierFlagID#">
</cfloop>
<cfset caseStatement = caseStatement & " else #defaultTierFlagID# end">
		

<cfif isDefined("clearFG") and clearFG>
	<!--- delete all the derived tiers --->
	<cfquery name="getData" datasource="#application.siteDataSource#">
		delete from booleanFlagData 
			where flagID  in ( <cf_queryparam value="#tierFlagIDList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			<cfif isDefined("orgID") and isNumeric(orgID)>
				and entityID =  <cf_queryparam value="#orgID#" CFSQLTYPE="CF_SQL_INTEGER" > 
			</cfif>
	</cfquery>
	<p>Cleared the old Scorecard derived tiers</p> 
<cfelse>
	<!--- delete only the derived tiers that have changed --->
	<cfquery name="deleteChangedDerivedTiers" datasource="#application.siteDataSource#">
		delete booleanflagdata 
			from scorecard sc WITH (NOLOCK) inner join organisation o WITH (NOLOCK) on sc.entityID = o.organisationID
			inner join booleanflagdata bfd WITH (NOLOCK) on bfd.entityid = o.organisationid and flagid  in ( <cf_queryparam value="#tierFlagIDList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		where
			--xtotal <> 0 and ytotal <> 0 and
			isnull(#caseStatement#,0) <> bfd.flagid
		<cfif isDefined("orgID") and isNumeric(orgID)>
			and sc.entityID =  <cf_queryparam value="#orgID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfif>
			and sc.updateRequired is null
	</cfquery>
</cfif>

<!--- insert the new derived tiers. --->
<cfquery name="insertNewDerivedTiers" datasource="#application.siteDataSource#">
	insert into booleanFlagData (entityID,createdBy,created,lastUpdatedBy,lastUpdated,flagID)
	select distinct sc.entityID, 4, <cf_queryparam value="#timeStamp#" CFSQLTYPE="CF_SQL_TIMESTAMP" >, 4, <cf_queryparam value="#timeStamp#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
		<cf_queryparam value="#caseStatement#" CFSQLTYPE="CF_SQL_INTEGER" >
	from scorecard sc WITH (NOLOCK) 
		inner join organisation o WITH (NOLOCK) on sc.entityID = o.organisationID
		left join booleanflagdata bfd WITH (NOLOCK) on bfd.entityID = o.organisationID and flagid  in ( <cf_queryparam value="#tierFlagIDList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	 where   
		--xtotal <> 0 and ytotal <> 0 and
		bfd.flagid is null
		and sc.scorecardDefID =  <cf_queryparam value="#scorecardDefID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		<cfif isDefined("orgID") and isNumeric(orgID)>
			and sc.entityID =  <cf_queryparam value="#orgID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfif>
		and sc.updateRequired is null
</cfquery>


<!--- NJH 2007/10/30 - include any post tier mapping files. This was needed for Trend --->
<cfif fileExists("#application.paths.code#\cftemplates\postScorecardTierMapping.cfm")>
	<cfinclude template="/code/cftemplates/postScorecardTierMapping.cfm">
</cfif>

