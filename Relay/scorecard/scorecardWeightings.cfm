<!--- �Relayware. All Rights Reserved 2014 --->

<CFPARAM NAME="screenTitle" DEFAULT="Scorecard Weighting">
<CFPARAM NAME="sortOrder" DEFAULT="Axis,Section,profile,flagOrder">


<cf_head>
	<cfoutput><cf_title>#htmleditformat(screenTitle)#</cf_title></cfoutput>
</cf_head>


 

<cfif isDefined("editor") and editor eq "yes">
	<CF_RelayXMLEditor
		editorName = "editScoreCardWeighting"
		Add = "yes"
		thisEmailAddress = "relayhelp@foundation-network.com"
	>
	
<cfelse>

	<CF_relayPageSubMenu pageTitle="#screenTitle#">

	<cfquery name="getData" datasource="#application.siteDataSource#">
		SELECT scsd.description as Section,scsd.graphAxis as axis,fg.Name AS Profile, f.Name AS Attribute, scfw.weighting, f.orderingIndex as flagOrder
			FROM flag AS f 
			INNER JOIN FlagGroup AS fg ON f.FlagGroupID = fg.FlagGroupID 
			INNER JOIN ScoreCardFlagWeighting AS scfw ON f.FlagID = scfw.flagID
			inner join ScoreCardFlagGroupDef as scfgd on scfw.flagGroupID = scfgd.flagGroupID and scfw.sectionDefID = scfgd.sectionDefID and scfgd.active=1
			inner join scorecardSectionDef scsd on scsd.sectionDefID = scfgd.sectionDefID
			where 1=1
			<cfinclude template = "/templates/tableFromQuery-QueryInclude.cfm">
		order by <cf_queryObjectName value="#sortOrder#">
	</cfquery>
	
	<CF_tableFromQueryObject 
		queryObject="#getData#" 
		
		showTheseColumns = "SECTION,PROFILE,ATTRIBUTE,WEIGHTING"
		
		groupByColumns="SECTION"
		
		FilterSelectFieldList="PROFILE,WEIGHTING"
		FilterSelectFieldList2="PROFILE,WEIGHTING"
		
		HidePageControls="no"
		useInclude = "false"
	
		sortOrder="#sortorder#">
</cfif>
	




