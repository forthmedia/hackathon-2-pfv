<!--- �Relayware. All Rights Reserved 2014 --->

<!--- 
File name:			updateScorecardTierMapping.cfm	
Author:				SWJ
Date started:		2007-06-20
	
Description:		This file calls various processes to update scorecard related data data

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->


<cf_head>
	<cf_title>Update Scorecard Partner Tier Mapping</cf_title>
</cf_head>


<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
	<TR class="Submenu">
		<TD ALIGN="LEFT" VALIGN="TOP" class="Submenu">Update Scorecard Partner Tier Mapping</TD>
	</TR>
</TABLE>
<cfoutput>
<p>This updates the scorecard derived partner Tiers in #request.CurrentSite.Title#</p>

<p><a href="/code/cftemplates/updateScorecardTierMapping.cfm" target="thisIframe">Click here</a>
to run the process.</p>
</cfoutput>

<iframe name="thisIframe" id="thisIframe" width="100%" height="600" frameborder="0"></iframe>


