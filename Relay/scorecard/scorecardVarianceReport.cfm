<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			scorecardVarianceReport.cfm
Author:				SWJ
Date started:		2007-05-02
	
Description:			

Provides a screen that shows the variance between partner ties types

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
02-May-20037		SWJ			Initial version

Possible enhancements:


 --->
 
 <cfsetting requesttimeout="600"> <!--- NJH 2008/06/27 Bug Fix Trend 621 - set request time out. --->

<cfif fileexists("#application.paths.code#\cftemplates\scorecardVarianceReport.cfm")>
<!--- Client version of the report loads if there is one --->
	<cfinclude template ="/code/cftemplates/scorecardVarianceReport.cfm">
<cfelse>

	
	<cfparam name="sortOrder" default="organisation_Name">
	<cfparam name="screenTitle" default="Scorecard Variance">
	<cfparam name="countryID" type="integer" default="#request.relayCurrentUser.countryID#">
	<cfparam name="frmCountryGroupID" default="#request.relayCurrentUser.countryID#">
	<cfparam name="alphabeticalIndexColumn" type="string" default="alphabeticalIndex">

	<cfparam name="showTheseCols" type="string" default="ORGANISATION_NAME,ORGANISATIONID,ISOCODE,AGREED_TIER,SCORECARD_DERIVED_TIER,VARIANCE,REQUESTED_TIER,REQUESTED_BY,REQUESTED_DATE,CHANGE_APPROVAL_STATUS,ACCOUNT_MANAGER">
	
	<cfif not isDefined("session.scoreCardCountryID")>
		<!--- set this sessionCountryID the first time the user comes into the screen --->
		<cfset session.scoreCardCountryID = countryID>
		<!--- set a session variable to force the query to update --->
		<cfset session.varianceReportCacheID = 1>
	</cfif>
	
	<!--- <cfoutput>#session.scoreCardCountryID# #form.frmCountryGroupID#</cfoutput> --->
	
	<cfif isDefined("session.scoreCardCountryID") and isDefined("form.frmCountryGroupID") and session.scoreCardCountryID neq form.frmCountryGroupID>
		<!--- here the user has changed coutry so we want to set the session cache to a different number --->
		<cfset session.varianceReportCacheID = session.varianceReportCacheID + 1>
		<!--- reset the local variable so it is remembered in the drop down --->
		<cfset session.scoreCardCountryID = form.frmCountryGroupID>
	</cfif>
	
	
	<cf_head>
		<cfoutput><cf_title>#screenTitle#</cf_title></cfoutput>
	</cf_head>
	
	<!--- ==============================================================================
	SWJ  25-06-2007  Added the filter below to stop level 1 people seeing all countries
	=============================================================================== --->
	<!--- ==============================================================================
	GCC  23-08-2007  Added the country group type filter
	=============================================================================== --->
	<cfparam name="CountryGroupTypeIDFilterList" default="1,2">
	
	<cfquery name="getCountriesQry" datasource="#application.siteDataSource#">
		SELECT DISTINCT a.CountryID,a.CountryDescription, 2 as sortOrder
			FROM Country AS a
			WHERE a.ISOcode IS NOT null
			AND a.CountryID IN (select distinct countryID from ScoreCardVariance)
		  	AND a.CountryID IN (#request.relayCurrentUser.countryList#)
		  	AND (a.countryGroupTypeID  IN ( <cf_queryparam value="#CountryGroupTypeIDFilterList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ))
		<cfif application.com.login.checkInternalPermissions ("scorecardTask", "level2")>
		UNION
		SELECT 0 as countryID, '----------------------------------------' as countryDescription, 1 as sortOrder from
			country
		UNION
		SELECT DISTINCT b.CountryID, b.CountryDescription, 0 as sortOrder
		 FROM Country AS a, Country AS b, CountryGroup AS c
		WHERE (b.ISOcode IS null OR b.ISOcode = '')
		  AND a.CountryID IN (#request.relayCurrentUser.countryList#)
		  AND c.CountryGroupID = b.CountryID
		  AND c.CountryMemberID = a.CountryID
		  AND (a.countryGroupTypeID  IN ( <cf_queryparam value="#CountryGroupTypeIDFilterList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )) AND (b.countryGroupTypeID  IN ( <cf_queryparam value="#CountryGroupTypeIDFilterList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ))
		  </cfif>
		ORDER BY sortOrder, CountryDescription
	</cfquery>
	
	<!--- the flag tierChangeApproved needs to exist in a flag group TierChangeApprovalStatus --->
	<!--- NJH 2008/05/21 P-TND066 Moved the setting of the approval flag to scorecardVarianceReportFunctions.cfm --->
	<!--- <cfset flagIDToGet = "tierChangeApproved">
	<cfif structkeyExists(application. flag,flagIDToGet)>
		<cfset thisFlagID = application. flag['tierChangeApproved'].flagID>
	<cfelse>
		<cfoutput>#flagIDToGet# is not defined please set it up in profile manager.</cfoutput>
	</cfif> 
	
	<cfscript>
	 	// this changes the approval status if that is requested
	 	if(structKeyExists(URL, "frmRowIdentity")){
			//theFlagID = application. flag[tierVarianceApproved];
			for(x = 1; x lte listLen(URL.frmRowIdentity); x = x + 1){
				application.com.flag.setFlagData(entityID=listGetAt(URL.frmRowIdentity,x),flagID=thisFlagID,data="");
			}
		}
	 </cfscript> --->
	 
	<!--- <cfset getCountriesQry = application.com.commonQueries.GetCountries()> --->
	<cfif not isDefined("frmCountryGroupID")>
		<cfset frmCountryGroupID = session.scoreCardCountryID>
	</cfif>
	
	
	<cf_translate>
		<CF_relayPageSubMenu pageTitle="#screenTitle#">
		
		<cfset request.relayFormDisplayStyle="HTML-table">
		
		<cfform name="mainForm" method="post" >
			<cf_relayFormDisplay>
				<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="" label="">
					<span class="label">phr_scoreCard_filterBy:</span> <cf_relayFormElement relayFormElementType="select" 
						fieldname="frmCountryGroupID" currentValue="#session.scoreCardCountryID#" query="#getCountriesQry#" display="countryDescription" value="countryID">
					<cf_relayFormElement relayFormElementtype="submit" fieldname="frmSubmit" currentValue="Go" label="">
				</cf_relayFormElementDisplay>
			</cf_relayFormDisplay>
		</cfform>
		
		<cfscript>
		// create a structure containing the fields below which we want to be reported when the form is filtered
		passThruVars = StructNew();
		StructInsert(passthruVars, "frmCountryGroupID", session.scoreCardCountryID);
		</cfscript>
	
	
	<!--- ==============================================================================
	SWJ  26-06-2007  added check to establish if the current user is just level 1
	=============================================================================== --->
	<cfset scoreCardSecurity = application.com.login.checkInternalPermissions ("scorecardTask")>
		<cfquery name="getBaseData" datasource="#application.siteDataSource#" cachedwithin="#CreateTimeSpan(0, 0, 6, 0)#">
			-- this query will re-run if #session.varianceReportCacheID# changes
			select * 
			from vScoreCardVariance v
				where countryID in (select countryMemberID from countryGroup with (NoLock) where countryGroupID =  <cf_queryparam value="#session.scoreCardCountryID#" CFSQLTYPE="CF_SQL_INTEGER" > )
				and (Scorecard_Derived_Tier <> Agreed_Tier or Agreed_Tier <> Requested_Tier)
				<cfif scoreCardSecurity.level2 eq false and scoreCardSecurity.level3 eq false>
				<!--- this will only happen if this user is level1 --->
					and variance in ('downgrade','same')
					and account_Manager = '#request.relayCurrentUser.fullname#'
				</cfif>
				<cfinclude template = "/templates/tableFromQuery-QueryInclude.cfm">
			order by <cf_queryObjectName value="#sortOrder#">
		</cfquery>
	
		<!--- NJH 2008/05/21 commented this out as I wasn't sure why we're calling the query again without any different filters
		<cfquery name="getData" dbtype="query">
		select * 
			from getBaseData
				where 1 = 1
			<!--- and countryID = #countryID# --->
				<cfinclude template = "/templates/tableFromQuery -QueryInclude.cfm">
			order by <cf_queryObjectName value="#sortOrder#">
			</cfquery> --->
		
		<cfif scoreCardSecurity.level3 eq true>
			<!--- NJH 2008/07/09 P-TND066 just show approval functionality for the standard report --->
			<cfset showApproveChanges = "true">
			<cfset showRejectChanges = "false">
			
			<cfinclude template="scoreCardVarianceReportFunctions.cfm">
			<CF_tableFromQueryObject 
				queryObject="#getBaseData#" 
				numRowsPerPage="50"
				
				showTheseColumns = "#showTheseCols#"
		
				keyColumnList="ORGANISATION_NAME"
				keyColumnURLList="/data/entityScreens.cfm?frmEntityScreen=scorecardDetail&frmEntityType=2&frmcurrentEntityID="
				keyColumnKeyList="organisationID"
				keyColumnOpenInWindowList="yes"
				dateFormat="REQUESTED_DATE"
		
				FilterSelectFieldList="AGREED_TIER,SCORECARD_DERIVED_TIER,REQUESTED_TIER,VARIANCE,ACCOUNT_MANAGER"
				FilterSelectFieldList2="AGREED_TIER,SCORECARD_DERIVED_TIER,REQUESTED_TIER,VARIANCE,ACCOUNT_MANAGER"
			
				passThroughVariablesStructure = "#passThruVars#"
		
				alphabeticalindexcolumn="#alphabeticalIndexColumn#"
				
				rowIdentityColumnName="organisationID"
				functionListQuery="#comTableFunction.qFunctionList#"
			
				sortOrder="#sortorder#">
		<cfelse>
	
			<CF_tableFromQueryObject 
				queryObject="#getBaseData#" 
				numRowsPerPage="50"
				
				showTheseColumns = "#showTheseCols#"
		
				keyColumnList="ORGANISATION_NAME"
				keyColumnURLList="/data/entityScreens.cfm?frmEntityScreen=scorecardDetail&frmEntityType=2&frmcurrentEntityID="
				keyColumnKeyList="organisationID"
				keyColumnOpenInWindowList="yes"
				dateFormat="REQUESTED_DATE"
		
				FilterSelectFieldList="AGREED_TIER,SCORECARD_DERIVED_TIER,REQUESTED_TIER,VARIANCE,ACCOUNT_MANAGER"
				FilterSelectFieldList2="AGREED_TIER,SCORECARD_DERIVED_TIER,REQUESTED_TIER,VARIANCE,ACCOUNT_MANAGER"
			
				passThroughVariablesStructure = "#passThruVars#"
		
				alphabeticalindexcolumn="#alphabeticalIndexColumn#"
				
				sortOrder="#sortorder#">
		</cfif>
	
		
	</cf_translate>
	
	
	
</cfif>

