<!--- �Relayware. All Rights Reserved 2014 --->

<!--- 
File name:			updateScorecardProfiles.cfm	
Author:				SWJ
Date started:		2007-06-20
	
Description:		This file calls various processes to update scorecard related data data

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->


<cf_head>
	<cf_title>Update Scorecard Profiles</cf_title>
</cf_head>


<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
	<TR class="Submenu">
		<TD ALIGN="LEFT" VALIGN="TOP" class="Submenu">Update Scorecard Profiles</TD>
	</TR>
</TABLE>

<p>This runs the process of updating the derived profile data for the scorecard</p>

<cfoutput>
<p><a href="/code/cftemplates/updateScoreCardFlags.cfm?clearFG=true&flagGroupID=456" target="thisIframe">Update the NEW BUSINESS SOD</a></p>
<p><a href="/code/cftemplates/updateScoreCardFlags.cfm?clearFG=true&flagGroupID=457" target="thisIframe">Update the RENEWALS SOD</a></p>
<p><a href="/code/cftemplates/updateScoreCardFlags.cfm?clearFG=true&flagGroupID=455" target="thisIframe">Update the TOTAL SOD</a></p>
</cfoutput>

<iframe id="thisIframe" height="600" width="100%"></iframe>



