<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			entityScorecardFlagCalculations.cfm	
Author:				NJH
Date started:		2007-07-03
	
Description:		The file displays how a scorecard section total has been calculated for a particular organisation

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->




<cf_head>
	<cf_title>Entity Scorecard Flag Calculations</cf_title>
</cf_head>



<cfparam name="algorithmID" type="numeric" default=1>
<cfparam name="organisationID" type="numeric">
<cfparam name="sectionDefID" type="numeric">
<cfparam name="scorecardDefID" type="numeric" default=1>

<!--- <cfset scorecardDefQry = application.com.relayScorecard.getScorecardDef(scorecardDefID=scorecardDefID)> --->
<cfset scorecardSectionDefQry = application.com.relayScorecard.getScorecardSectionDef(sectionDefID=sectionDefID,scorecardDefID=scorecardDefID)>

<cfquery name="getOrgName" datasource="#application.siteDataSource#">
	select organisationName from organisation where organisationID =  <cf_queryparam value="#organisationID#" CFSQLTYPE="CF_SQL_INTEGER" > 
</cfquery>

<cfoutput>
	<h3>Calculations for Scorecard Section '#scorecardSectionDefQry.description#' and Organisation '#getOrgName.organisationName#'</h3>
	</br>
</cfoutput>

<cfswitch expression="#algorithmID#">
	<cfcase value="1">
	
		<cfquery name="getSectionWeightings" datasource="#application.siteDataSource#">
			exec sp_getScoreCardSectionWeightings @organisationID =  <cf_queryparam value="#organisationID#" CFSQLTYPE="CF_SQL_INTEGER" > , @sectionDefID =  <cf_queryparam value="#sectionDefID#" CFSQLTYPE="CF_SQL_INTEGER" > , @scorecardDefID =  <cf_queryparam value="#scorecardDefID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfquery>
		
		<cfloop query="getSectionWeightings">
			<cfquery name="getFlagWeightings" datasource="#application.siteDataSource#">
				select fg.flagGroupID as categoryID, fg.name as category_Name,f.flagID as attribute_ID,f.name as attribute_Name,scfw.weighting,
					case when bfd.entityID is null then 'No' else 'Yes' end as attribute_Set,
					f.orderingIndex
				from flag f inner join scorecardFlagWeighting scfw
					on f.flagID = scfw.flagID and f.flagGroupID = scfw.flagGroupID left outer join booleanFlagData bfd
					on f.flagID = bfd.flagID and entityID =  <cf_queryparam value="#organisationID#" CFSQLTYPE="CF_SQL_INTEGER" >  inner join flagGroup fg
					on fg.flagGroupID = f.flagGroupID inner join scorecardFlagGroupDef scfgd
					on fg.flagGroupID = scfgd.flagGroupID
				where f.flagGroupID =  <cf_queryparam value="#getSectionWeightings.flagGroupID#" CFSQLTYPE="CF_SQL_INTEGER" > 
					and scfgd.active = 1
				<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
			</cfquery>
			
			<cfquery name="UnionFlagWeightingQueries" dbType="query">
				select categoryID,category_Name,attribute_ID,attribute_Name,weighting,attribute_Set,orderingIndex from getFlagWeightings
				<cfif currentRow gt 1>
					union
					select categoryID,category_Name,attribute_ID,attribute_Name,weighting,attribute_Set,orderingIndex from UnionFlagWeightingQueries
				</cfif>
				order by categoryID,orderingIndex
			</cfquery>
		</cfloop>
			
		<cfquery name="getSectionCalculations" dbType="query">
			select cast(100*sectionWeighting as varchar)+'%' as section_Weighting, flagGroupName as category_Name, 
				cast(100*flagGroupWeighting as varchar)+'%' as Category_Weighting, numberOfFlagsInDefinition as flagsInDefinition, 
				numberAnswered,	score, maxScore,
				countryWeighting as Country_Weighting, 
				axisWeighting,
				'100 * ('+cast(score as varchar)+'/'+ cast(maxScore as varchar) +') * '+ cast(flagGroupWeighting as varchar) +' * '+ cast(sectionWeighting as varchar) + ' * ' + cast(countryWeighting as varchar) + ' * ' + cast(axisWeighting as varchar) as calculation,
<!--- 				100 * (score/maxScore) * flagGroupWeighting * sectionWeighting * countryWeighting * axisWeighting as subTotal --->
				cast(weightedScore/sectionWeighting as varchar)+'% out of ' + cast(100*flagGroupWeighting as varchar) + '%' as category_Percentage,
				weightedScore as subTotal
			from getSectionWeightings
			<cfset inQoQ = true>
			<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
		</cfquery>
			
		<p>The section calculation is calculated as follows:<br>
			<b>100 * [Section Weighting] * [Category Weighting] * [Country Weighting] * [score/maxscore].</b>
		</p>
		<p>The score is the sum of the weightings of attributes that have been set. The maximum score is the sum of all weightings of attributes that can possibly be set.<br>
			Use the category/attribute table below as a reference for which attributes have been set.
		</p>
			
		<cfif UnionFlagWeightingQueries.recordCount>
			<cf_tableFromQueryObject 
				queryObject="#getSectionCalculations#" 
				showTheseColumns="Section_Weighting,Category_Name,Category_Weighting,score,maxScore,Country_Weighting,Category_Percentage,subTotal"
				hideTheseColumns=""
				numberFormat="SubTotal"
				totalTheseColumns="SubTotal"
				sortOrder=""
				groupByColumns=""
				HidePageControls="yes"
			>
			
			<h3>Categories/Attributes</h3>
			<cf_tableFromQueryObject 
				queryObject="#UnionFlagWeightingQueries#" 
				showTheseColumns="Category_Name,Attribute_Name,Attribute_Set,Weighting"
				hideTheseColumns="categoryID,orderingIndex"
				<!--- numberFormat="SubTotal"
				totalTheseColumns="SubTotal" --->
				sortOrder="categoryID,orderingIndex"
				groupByColumns="category_Name"
				HidePageControls="yes"
			>
		</cfif>
	</cfcase>
</cfswitch>



