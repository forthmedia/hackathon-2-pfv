<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			scorecardOverviewReport.cfm	
Author:				NJH
Date started:		2007-02-21
	
Description:		Primary Scorecard overview controller

	The four quadrants are as follows:
		Quadrant 1: x is low, y is high
		Quadrant 2: x is high, y is high
		Quadrant 3: x is low, y is low
		Quadrant 4: x is high, y is low

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->



<cf_head>
	<cf_title>Scorecard Overview Report</cf_title>
</cf_head>


<cf_translate>

<cfparam name="frmScoreCardDefID" type="numeric" default=1>

<cfparam name="chartHeight" type="numeric" default=200>
<cfparam name="chartWidth" type="numeric" default=300>

<cfset itemColumn = "name">
<cfset valuecolumn = "flagCount">
<cfset colorList = "##F93400,##56B2E1,##FF9900,##003366,##CCCC33,##9900CC,##FFCC33,##CC0000,##FF99CC,##632029,##800080,##FF0000,##C0C0C0,##008080,##FFFF00,##C0C033,##784384,##729472">
<cfset showLegend = "false">
<cfset chartTitle1 = "phr_scorecard_Top_Left_Title">
<cfset chartTitle2 = "phr_scorecard_Top_Right_Title">
<cfset chartTitle3 = "phr_scorecard_Bottom_Left_Title">
<cfset chartTitle4 = "phr_scorecard_Bottom_Right_Title">

<cfset xQuad1 = "low">
<cfset yQuad1 = "high">
<cfset xQuad2 = "high">
<cfset yQuad2 = "high">
<cfset xQuad3 = "low">
<cfset yQuad3 = "low">
<cfset xQuad4 = "high">
<cfset yQuad4 = "low">

<cfquery name="getCurrentScoreCardDef" datasource="#application.siteDataSource#">
	select * from scoreCardDef where scoreCardDefID =  <cf_queryparam value="#frmScoreCardDefID#" CFSQLTYPE="CF_SQL_INTEGER" > 
</cfquery>

<cfif getCurrentScoreCardDef.recordCount eq 0>
	<cfoutput>
		<cf_translate>phr_scoreCard_NoScoreCardDefinitionExists</cf_translate>
	</cfoutput>
	<CF_ABORT>
</cfif>

<cfquery name="getScoreCardSectionDefs" datasource="#application.siteDataSource#">
	select distinct description, graphAxis from scoreCardSectionDef scsd inner join scoreCardFlagGroupDef scfgd
		on scsd.sectionDefID = scfgd.sectionDefID
	where scsd.scoreCardDefID =  <cf_queryparam value="#frmScoreCardDefID#" CFSQLTYPE="CF_SQL_INTEGER" >  and scfgd.active=1
</cfquery>


<!--- looping through the four quadrants and establishing the x and y condition statements for our query --->
<cfloop from="1" to="4" step="1" index="idx">
	<cfif evaluate("xQuad"&idx) eq "low">
		<cfset "xQuadCondition#idx#" = "xTotal < #getCurrentScoreCardDef.x50PercentValue#">
	<cfelse>
		<cfset "xQuadCondition#idx#" = "xTotal >= #getCurrentScoreCardDef.x50PercentValue#">
	</cfif>
	
	<cfif evaluate("yQuad"&idx) eq "low">
		<cfset "yQuadCondition#idx#" = "yTotal < #getCurrentScoreCardDef.y50PercentValue#">
	<cfelse>
		<cfset "yQuadCondition#idx#" = "yTotal >= #getCurrentScoreCardDef.y50PercentValue#">
	</cfif>
</cfloop>

<!--- get the scorecard definitions --->
<cfquery name="getAllScoreCardDefs" datasource="#application.siteDataSource#">
	select distinct scd.scoreCardDefID, scd.description from scoreCardDef scd inner join
		scoreCardSectionDef scsd on scd.scoreCardDefID = scsd.scoreCardDefID inner join
		scoreCardFlagGroupDef scfgd on scfgd.sectionDefID = scsd.sectionDefID inner join
		scoreCardFlagWeighting scfw on scfw.flagGroupID = scfgd.flagGroupID and scfw.sectionDefID = scfgd.sectionDefID
</cfquery>

<CFQUERY NAME="getCountriesQry" datasource="#application.siteDataSource#">
	SELECT DISTINCT a.CountryID,a.CountryDescription, 2 as sortOrder
		FROM Country AS a
		WHERE a.ISOcode IS NOT null
		<!--- --AND a.CountryID IN (#Variables.CountryList#)  --->
	UNION
	SELECT 0 as countryID, '----------------------------------------' as countryDescription, 1 as sortOrder from
		country
	UNION
	SELECT DISTINCT b.CountryID, b.CountryDescription, 0 as sortOrder
	 FROM Country AS a, Country AS b, CountryGroup AS c
	WHERE (b.ISOcode IS null OR b.ISOcode = '')
	  <!--- --AND a.CountryID IN (#Variables.CountryList#)  --->
	  AND c.CountryGroupID = b.CountryID
	  AND c.CountryMemberID = a.CountryID
	  
	ORDER BY sortOrder, CountryDescription
</CFQUERY>


<!--- <cfset getCountriesQry = application.com.commonQueries.GetCountries()> --->
<cfif not isDefined("frmCountryGroupID")>
	<cfset frmCountryGroupID = request.relayCurrentUser.countryID>
</cfif>

<!--- get the active flag groups for our drop down --->
<cfquery name="getFlagGroupsQry" datasource="#application.siteDataSource#">
	select distinct name, fg.flagGroupID from flagGroup fg inner join
		scorecardFlagGroupDef scfgd on fg.flagGroupID = scfgd.flagGroupID inner join
		scorecardSectionDef scsd on scsd.sectionDefID = scfgd.sectionDefID
		where scfgd.active = 1 and scsd.scoreCardDefID =  <cf_queryparam value="#frmScoreCardDefID#" CFSQLTYPE="CF_SQL_INTEGER" > 
</cfquery>

<cfif not isDefined("frmFlagGroupID") and getFlagGroupsQry.recordCount>
	<cfset frmFlagGroupID = getFlagGroupsQry.flagGroupID[1]>
</cfif>

<cfset request.relayFormDisplayStyle="HTML-table">

<cfform name="scoreCardDefForm" method="post" >
	<cf_relayFormDisplay>
		<cf_relayFormElementDisplay relayFormElementType="select" fieldname="frmScoreCardDefID" label="phr_scoreCard_SelectAScoreCardDefinition" currentValue="#frmScoreCardDefID#" query="#getAllScoreCardDefs#" display="description" value="scoreCardDefID" valueAlign="right" onChange="javascript:scoreCardDefForm.submit();">
		<cf_relayFormElementDisplay relayFormElementType="hidden" fieldname="frmCountryGroupID" label="" currentValue="#frmCountryGroupID#">
		<cf_relayFormElementDisplay relayFormElementType="hidden" fieldname="frmFlagGroupID" label="" currentValue="#frmFlagGroupID#">
	</cf_relayFormDisplay>
</cfform>

<cfform name="mainForm" method="post" >
	<cf_relayFormDisplay>
		<cf_relayFormElementDisplay relayFormElementType="hidden" fieldname="frmScoreCardDefID" label="" currentValue="#frmScoreCardDefID#">
		<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="" label="">
			<span class="label">phr_scoreCard_filterBy:</span> <cf_relayFormElement relayFormElementType="select" fieldname="frmCountryGroupID" currentValue="#frmCountryGroupID#" query="#getCountriesQry#" display="countryDescription" value="countryID">
			<span class="label">phr_scoreCard_breakdownBy:</span> <cf_relayFormElement relayFormElementtype="select" fieldname="frmFlagGroupID" currentValue="#frmFlagGroupID#" query="#getFlagGroupsQry#" display="name" value="flagGroupID">
			<cf_relayFormElement relayFormElementtype="submit" fieldname="frmSubmit" currentValue="Go" label="">
		</cf_relayFormElementDisplay>
	</cf_relayFormDisplay>
</cfform>

<!--- Getting the flags from all four quadrants --->
<cfquery name="getFlagCountInAllQuadrants" datasource="#application.siteDataSource#">
	<cfloop from="1" to="4" step="1" index="idx">
	select '#evaluate("xQuad"&idx)#' as xQuad, '#evaluate("yQuad"&idx)#' as yQuad, name, count(distinct sc.entityID) as flagCount, orderingIndex, scfw.weighting
	from flag f left outer join (booleanFlagData bfd
		inner join scorecard sc on bfd.entityID = sc.entityID and #evaluate("xQuadCondition"&idx)# and #evaluate("yQuadCondition"&idx)#
		inner join organisation o on o.organisationID = sc.entityID
		inner join country c on c.countryID = o.countryID
		inner join countryGroup cg on cg.countryMemberID = c.countryID and cg.countryGroupID =  <cf_queryparam value="#frmCountryGroupID#" CFSQLTYPE="CF_SQL_INTEGER" > )
	on f.flagID = bfd.flagID inner join 
	scoreCardFlagWeighting scfw on scfw.flagID = f.flagID inner join
	scorecardFlagGroupDef scfgd on scfw.flagGroupID = scfgd.flagGroupID inner join
	scorecardSectionDef scsd on scsd.sectionDefID = scfgd.sectionDefID
	where 
	f.flagGroupID =  <cf_queryparam value="#frmFlagGroupID#" CFSQLTYPE="CF_SQL_INTEGER" >  and
	scfgd.active = 1 and scsd.scoreCardDefID =  <cf_queryparam value="#frmScoreCardDefID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	group By name, orderingIndex, scfw.weighting
	<cfif idx lt 4>
		union
	</cfif>
	</cfloop>
	order by orderingIndex
</cfquery>

<!--- get the flags for a specific quadrant --->
<cfloop from="1" to="4" step="1" index="idx">
	<cfquery name="getFlagCountQuad#idx#" dbtype="query">
		select name,flagCount,weighting from getFlagCountInAllQuadrants 
		where 
			xQuad = '#evaluate("xQuad"&idx)#' and
			yQuad = '#evaluate("yQuad"&idx)#'
	</cfquery>
	
	<cfquery name="getCountOfOrgsInQuad#idx#" datasource="#application.siteDataSource#">
		select entityID, xTotal, yTotal from scorecard sc inner join
			organisation o on sc.entityID = o.organisationID inner join
			country c on c.countryID = o.countryID inner join 
			countryGroup cg on cg.countryMemberID = c.countryID
		where #evaluate("xQuadCondition"&idx)# and #evaluate("yQuadCondition"&idx)#
			and cg.countryGroupID =  <cf_queryparam value="#frmCountryGroupID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</cfquery>
</cfloop>


<!--- JS function to create a selection from the pie chart slice that was clicked --->
<cfoutput>
	<script>
		function Chart_OnClick(selectionQryString, item){
			// replace %24ITEMLABEL%24 with the item value. %24ITEMLABEL%24 is $ITEMLABEL$ url encoded.
			selectionQryString = selectionQryString.replace(/%24ITEMLABEL%24/g,escape(item));
			// HACK - %u20AC isn't getting decoded properly, so I'll have to decode it now just after I've encoded it....
			selectionQryString = selectionQryString.replace(/%u20AC/g,'?');
			newWin = window.open('/selection/selectalter.cfm?frmOrganisationIDs='+selectionQryString+'&frmtask=save&frmruninpopup=true','PopUp','width=450,height=450,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1');
			newWin.focus()
		}
	</script>
</cfoutput>



 
<cfoutput>
<table cellpadding=5 cellspacing=0 border=0 class="scoreCardChartTable">

<cfloop from="1" to="4" step="1" index="idx">
	<cfwddx action="CFML2WDDX" input="#evaluate('getFlagCountQuad'&idx)#" output="querypacket">
	<cflock timeout="5" throwontimeout="No" type="EXCLUSIVE" scope="SESSION">
		<cfset session.WDDXQueryPacket = querypacket>
	</cflock>
	
	<!--- the selection query that gets passed to selectalter.cfm which uses it to build our selection --->
	<cfset selectionQryString = URLencodedFormat("select sc.entityID from flag f left outer join (booleanFlagData bfd inner join scorecard sc on bfd.entityID = sc.entityID and #evaluate('xQuadCondition'&idx)# and #evaluate('yQuadCondition'&idx)# inner join organisation o on o.organisationID = sc.entityID inner join country c on c.countryID = o.countryID inner join countryGroup cg on cg.countryMemberID = c.countryID and cg.countryGroupID=#frmCountryGroupID#) on f.flagID = bfd.flagID where f.flagGroupID = #frmFlagGroupID# and f.name = '$ITEMLABEL$' and sc.entityID is not null")>
	
	<cfif idx mod 2>
		<tr>
	</cfif>
		<td class="scoreCardChartQuad#idx#">
			<cfset entityCountQry = evaluate("getCountOfOrgsInQuad"&idx)>
			<cfset entityCount = entityCountQry.recordCount>
			
			<cfif entityCount neq 0>
				<cfset onClickURL = "javascript:Chart_OnClick('#selectionQryString#','$ITEMLABEL$');">
			<cfelse>
				<cfset onClickURL = "">
			</cfif>
			
			<cfset chartTitle = evaluate("chartTitle"&idx)>
			<cfinclude template="/report/chartPie.cfm">
			<br>
			
			phr_scoreCard_totalNumOfPartners: #htmleditformat(entityCount)#
			<!--- if we're in the third (low,low) quadrant, show the count of orgs that have a total of 0 (ie. their scorecards aren't completed) --->
			<cfif idx eq 3>
				<cfquery name="getEntitiesWithNoScore" dbType="query">
					select entityID from getCountOfOrgsInQuad3 where xTotal=0 and yTotal=0
				</cfquery>
				
			<br>phr_scoreCard_numOfPartnersWithNoScore: #getEntitiesWithNoScore.recordCount#
			</cfif>
		</td>
		<cfif idx eq 2>
			<td rowspan="2" valign="top">
			
				<!--- the chart legend --->
				<table cellpadding=0 cellspacing=2 border=0 class="chartLegendTable">
					<cfquery name="getFlagGroupHelpText" datasource="#application.siteDataSource#">
						select helpText from flagGroup where flagGroupID =  <cf_queryparam value="#frmFlagGroupID#" CFSQLTYPE="CF_SQL_INTEGER" > 
					</cfquery>
					
					<cfif getFlagGroupHelpText.recordCount gt 0>
						<tr><td colspan="2">#getFlagGroupHelpText.helpText[1]#</td></tr>
					</cfif>

					<tr><td  colspan="2"><span class="label">phr_scoreCard_Legend</span></td><!--- <td><span class="label">phr_scoreCard_flagWeighting</span></td> ---></tr>
					<cfloop query="getFlagCountQuad1">
						<tr>
							<td style="background-color:#listGetAt(colorList,currentRow)#;border:1px solid ##000000;">&nbsp;</td><td nowrap><strong>#htmleditformat(name)#</strong></td><!--- <td>#weighting#</td> --->
						</tr>
					</cfloop>
					
					<cfquery name="getXAxisSectionDefs" dbtype="query">
						select description from getScoreCardSectionDefs where graphAxis = 'x'
					</cfquery>
					
					<cfquery name="getYAxisSectionDefs" dbtype="query">
						select description from getScoreCardSectionDefs where graphAxis = 'y'
					</cfquery>
					
					<tr><td colspan="2">&nbsp;</td></tr>
					<tr><td colspan="2"><span class="label">phr_scoreCard_xAxis:</span> #htmleditformat(valueList(getXAxisSectionDefs.description))#</td></tr>
					<tr><td colspan="2"><span class="label">phr_scoreCard_yAxis:</span> #htmleditformat(valueList(getYAxisSectionDefs.description))#</td></tr>
					<tr><td colspan="2">&nbsp;</td></tr>
					<tr><td colspan="2">phr_scoreCard_clickOnAPieSliceToCreateASelection.</td></tr>
				</table>
			</td>
		</cfif>
	<cfif not idx mod 2>
		</tr>
	</cfif>
</cfloop>
</table>
</cfoutput>

</cf_translate>


