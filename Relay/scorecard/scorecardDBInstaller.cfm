<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			scorecardDBInstaller.cfm	
Author:				SWJ
Date started:		2007-02-21
	
Description:		Scripts that when run will create tables, insert values that are required and recreate views

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->

<CFTRANSACTION>
	<CFQUERY NAME="checkIfExists" datasource="#application.siteDataSource#">
		Select max(SecurityTypeID) as maxID from SecurityType
	</CFQUERY>

	<cfif checkIfExists.recordCount GT o>
		<CFQUERY NAME="AddSecurityType" datasource="#application.siteDataSource#">
			INSERT INTO SecurityType(ShortName,Description,CountrySpecific)
			VALUES(<cf_queryparam value="#Name#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#Description#" CFSQLTYPE="CF_SQL_VARCHAR" >,0)
		</CFQUERY>
			
		<CFQUERY NAME="GetID" datasource="#application.siteDataSource#">
			Select max(SecurityTypeID) as maxID from SecurityType
		</CFQUERY>
		<CFSET Def = "View,Edit,Add">
		<CFLOOP index="x" FROM="1" TO="3">
			<CFQUERY NAME="AddPermissionDef" datasource="#application.siteDataSource#">
				INSERT INTO PermissionDefinition(SecurityTypeID, PermissionLevel,PermissionName)
				VALUES(<cf_queryparam value="#GetID.MaxID#" CFSQLTYPE="CF_SQL_INTEGER" >, <cf_queryparam value="#x#" CFSQLTYPE="CF_SQL_Integer" >, <cf_queryparam value="#ListGetAt(Def, x)#" CFSQLTYPE="CF_SQL_VARCHAR" >)
			</CFQUERY>
		</CFLOOP>
	</cfif>
</CFTRANSACTION>
