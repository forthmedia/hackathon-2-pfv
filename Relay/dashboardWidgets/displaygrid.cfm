<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			metrics.cfm	
Author:				NM
Date started:		2005-11-09
Description:		Populates cfgrid with query variables.

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:
 --->


<cfparam name="displayType" default="flash">

<cfscript>
	qTabItems = application.com.relayDashboard.getUserTabItemsInModule("#filter#");
</cfscript>
			


<!--- <cf_querySim>
	metricData
	Metric,AssociatedWith,TimeFrame,Objective,Actual,PercentComplete
	Number of New Leads|National Business Group|Q2 2004|25|20|80
	Partner Pipeline Value|National Business Group|2004|1 600 000|1 212 000|76
	Q2 Sales Quota|Scott Kerrington|Q2 2004|1 000 000|870 000|87
	Q3 Revenue To Date|Scott Kerrington|Q3 2004|200 000|195 000|98
	Value of Closed Leads|Scott Kerrington|Q1 2004|20 000|24 000|120
</cf_querySim> --->


<cfif displayType eq "flash">
<!--- create a query from the chart data using the first column's data as the column headers, where the first row of data will be the column headers in the grid (to avoid column naming issues) and the rest the data. --->
	<cfform name="Dashboard" format="flash" timeout="999" width="100%" height="90%">
		<cfoutput query="qTabItems">
			<cfinvoke component="relay.com.relayDashboardData" method="#chartDataMethod#" returnvariable="qChartData"></cfinvoke>
			<cfinvoke component="relay.com.relayDashboard" method="getGridData" chartData="#qChartData#" returnvariable="sGridData"></cfinvoke>
			<cfset nColList = sGridData.colList>
			<cfset nColNames = sGridData.colNames>
			<cfset qGridData = sGridData.qData>
			<cfset gridHeight = 22 + (qGridData.recordCount*20)>
			<cfformgroup type="panel" label="#chartTitle#" style="#request.panelStyle#">
				<cfgrid name="metricsGrid#currentRow#" height="#gridHeight#" query="qGridData" insert="No" delete="No" sort="Yes" bold="No" italic="No" autowidth="true" appendkey="No" highlighthref="No" enabled="Yes" visible="Yes" griddataalign="LEFT" gridlines="Yes" rowheaders="No" rowheaderalign="LEFT" rowheaderitalic="No" rowheaderbold="No" colheaders="yes" colheaderalign="LEFT" colheaderitalic="No" colheaderbold="No" selectmode="EDIT" picturebar="No">
					<cfloop from="1" to="#listLen(nColList)#" index="i">
						<cfgridcolumn name="#listGetAt(nColNames,i)#" header="#listGetAt(nColList,i)#" headeralign="CENTER" dataalign="LEFT" numberformat=",">
					</cfloop>
				</cfgrid>
			</cfformgroup>
		</cfoutput>
	</cfform>
	<cfoutput><br><a href="javascript:openWin('/dashboard/dashboardIncluder.cfm?templateURL=displayGrid.cfm&filter=#filter#&displayType=html','htmlTables','width=700,height=400,toolbar=no,location=no,status=no,menubar=no,scrollbars=auto,resizable,alwaysRaised,dependent,titlebar=no');">view in excel</a></cfoutput>
<cfelse>
	<cfcontent type="application/msexcel">
		<cfheader name="content-Disposition" value="filename=ClaimReport.xls">
		<cfinclude template="//templates/excelHeaderInclude.cfm">
		<cfoutput query="qTabItems">
			<cfinvoke component="relay.com.relayDashboardData" method="#chartDataMethod#" returnvariable="qChartData"></cfinvoke>
			<cfinvoke component="relay.com.relayDashboard" method="getGridData" chartData="#qChartData#" returnvariable="sGridData"></cfinvoke>
			<cfset nColList = sGridData.colList>
			<cfset nColNames = sGridData.colNames>
			<cfset qGridData = sGridData.qData>
			
			#chartTitle#
			<table class="withBorder">
				<cfloop query="qGridData">
					
					<cfif currentRow eq 1>
						<tr>
						<cfloop from="1" to="#listLen(nColList)#" index="i">
							<td class="headings">#listGetAt(nColList,i)#</td>
						</cfloop>
						</tr>
					</cfif>
					<tr>
					<cfloop list="#nColNames#" index="col">
						<td class="withBorder">#htmleditformat(evaluate(col))#</td>
					</cfloop>
					</tr>
				</cfloop>
			</table>
		</cfoutput>
	</cfcontent>
</cfif>


