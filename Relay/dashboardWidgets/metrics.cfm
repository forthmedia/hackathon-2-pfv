<!--- �Relayware. All Rights Reserved 2014 --->
<cfinclude template="dashboardStyles.cfm">
<!--- 
File name:			metrics.cfm	
Author:				NM
Date started:		2005-11-09
Description:		Populates cfgrid with query variables.

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:
 --->

<!--- create query data using querySim --->

<cf_querySim>
	metricData
	Metric,AssociatedWith,TimeFrame,Objective,Actual,PercentComplete
	Number of New Leads|National Business Group|Q2 2004|25|20|80
	Partner Pipeline Value|National Business Group|2004|1 600 000|1 212 000|76
	Q2 Sales Quota|Scott Kerrington|Q2 2004|1 000 000|870 000|87
	Q3 Revenue To Date|Scott Kerrington|Q3 2004|200 000|195 000|98
	Value of Closed Leads|Scott Kerrington|Q1 2004|20 000|24 000|120
</cf_querySim>

<!---
for (var i:Number=0; i < yourGrid.length;i++) {
	if (yourGrid.getItemAt(i).yourGridColumn == someRelevantValue) 
		yourGrid.setPropertiesAt(i, {backgroundColor:0xF7FFB7});
}
--->

<!--- <cfsavecontent variable="loadFunction">
	var listener:Object = {};
	listener.modelChanged = function(evt):Void {
		alert('Data loaded');
		for (var i=0;i<metricsGrid.length;i++) {
			if (metricsGrid.getItemAt(i).PercentComplete < "100") 
			metricsGrid.setPropertiesAt(i, {backgroundColor:0xF7FFB7});
		}
	}
	metricsGrid.addEventListener('modelChanged',listener);
</cfsavecontent> --->
<cfform name="Dashboard" format="flash" timeout="999" width="100%" height="95%">
	<cfformgroup type="panel" label="Metrics" style="#request.panelStyle#">
		<cfgrid name="metricsGrid" height="200" query="metricData" insert="No" delete="No" sort="Yes" bold="No" italic="No" autowidth="true" appendkey="No" highlighthref="No" enabled="Yes" visible="Yes" griddataalign="LEFT" gridlines="Yes" rowheaders="No" rowheaderalign="LEFT" rowheaderitalic="No" rowheaderbold="No" colheaders="yes" colheaderalign="LEFT" colheaderitalic="No" colheaderbold="No" selectmode="EDIT" picturebar="No">
			<cfgridcolumn name="Metric" header="Metric" headeralign="CENTER" dataalign="LEFT" width="125" bold="No" italic="No" select="No" display="Yes" headerbold="No" headeritalic="No">
			<cfgridcolumn name="AssociatedWith" header="Associated With" headeralign="CENTER" dataalign="LEFT" width="125">
			<cfgridcolumn name="TimeFrame" header="Time Frame" headeralign="CENTER" dataalign="LEFT" width="100">
			<cfgridcolumn name="Objective" header="Objective" headeralign="CENTER" dataalign="right" numberformat="____.__" width="100">
			<cfgridcolumn name="Actual" header="Actual" headeralign="CENTER" dataalign="right" width="100" bold="No" italic="No" numberformat="____.__" select="Yes" display="Yes" headerbold="No" headeritalic="No">
			<cfgridcolumn name="PercentComplete" header="% Complete" headeralign="CENTER" dataalign="LEFT" width="75" numberformat="____%">
			<!--- <cfoutput query="metricData">
				<cfgridrow data="#Metric#,#AssociatedWith#,#TimeFrame#,#Objective#,#Actual#,#PercentComplete#">
			</cfoutput> --->
		</CFGRID>
		<!--- <cfformitem type="script">
			function somesuch() {
				
			}
		</cfformitem> --->
		<!--- <cfformitem type="script">
		   for (var i=0; i<metricsGrid.length;i++) {
				if (metricsGrid.getItemAt(i).PercentComplete < "100") 
					metricsGrid.setPropertiesAt(i, {backgroundColor:0xF7FFB7});
			}
		</cfformitem> --->
	</cfformgroup>
</cfform>
