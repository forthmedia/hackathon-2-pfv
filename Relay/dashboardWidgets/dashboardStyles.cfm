<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			dashboardStyles.cfm	
Author:				SWJ
Date started:			/xx/02
Description:			

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->

<cfset request.buttonStyle = "corner-radius: 2; borderThickness: 0;">
<cfset request.radioStyle = "">
<cfset request.panelStyle = "panelBorderStyle:'roundCorners'; themeColor:'haloBlue'; ">
<cfset request.contentPanelStyle = "panelBorderStyle:'roundCorners';themeColor:'haloBlue';">
<cfset request.chartStyle = "default">
<cfset request.chartWidth = "240">
<cfset request.chartHeight = "280">
<cfset request.chartShowBorder = "no">
<cfset request.chartPaintstyle = "light">