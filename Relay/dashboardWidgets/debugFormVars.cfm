<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			debugFormVars.cfm	
Author:				SWJ
Date started:		2005-11-07
Description:		This returns the current form variables in a cfgrid.  This 

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->

<cfformgroup type="panel" label="Form Variables" style="#request.panelStyle#">
	<CFGRID name="FORMGrid" height="330">
		<CFGRIDCOLUMN name="Key" header="Key" headeralign="CENTER" dataalign="LEFT" width="175">
		<CFGRIDCOLUMN name="Value" header="Value" headeralign="CENTER" dataalign="LEFT" width="425">
		<CFLOOP collection="#FORM#" item="aa">
			<CFTRY>
				<CFGRIDROW data="#aa#,#form[aa]#">
			<CFCATCH>
			</CFCATCH>
			</CFTRY>
		</CFLOOP>
	</CFGRID>
</cfformgroup>
