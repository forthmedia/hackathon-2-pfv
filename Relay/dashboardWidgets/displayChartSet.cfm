<!--- �Relayware. All Rights Reserved 2014 --->
<cfscript>
	qTabItems = application.com.relayDashboard.getUserTabItemsInModule("#filter#");
</cfscript>

<cfset vboxWidth = request.chartWidth+25>
<cfset hboxWidth = vboxWidth * 3.1>

<cfoutput query="qTabItems">
	
		<!--- generate the swf file to display --->
		<cfscript>
			tempFileName = application.com.relayDashboard.generateCFChartSWFAndJPGfileAndURL(chartTitle=chartTitle,chartName=chartName,chartDataMethod=chartDataMethod,chartType=chartType);
		</cfscript>
		<!--- in order to display a swf inside a cfform you use a cfinput type image tag --->
		<cfif tempFileName eq false>
			There is no data available for this chart.
		<cfelse>
			<cfparam name="classid" default="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000">
			<cfparam name="codebase" default="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab##version=6,0,0,0">
			<cfparam name="id" default="chart#currentRow#">
			<cfparam name="name" default="chart#currentRow#">
			<cfparam name="align" default="middle">
			<cfparam name="quality" default="high">
			<cfparam name="bgcolor" default="##FFFFFF">
			<cfparam name="allowscriptaccess" default="sameDomain">
			<cfparam name="pluginspage" default="http://www.macromedia.com/go/getflashplayer">
			<cfparam name="type" default="application/x-shockwave-flash">
			<cfparam name="flashVars" default="">
	
			<cfset params = "classid=#classid##application.delim1#
				codebase=#codebase##application.delim1#
				width=#request.chartWidth##application.delim1#
				height=#request.chartHeight##application.delim1#
				id=#id##application.delim1#
				align=#align##application.delim1#
				quality=#quality##application.delim1#
				bgcolor=#bgcolor##application.delim1#
				name=#name##application.delim1#
				allowscriptaccess=#allowscriptaccess##application.delim1#
				pluginspage=#pluginspage##application.delim1#
				type=#type#">
			
			<cfset contentDrawn = application.com.RelayActiveContent.renderActiveContent("#tempFileName#.swf",params)>
			<a href="javascript:openWin('#tempFileName#.jpg','chart#currentRow#','width=#request.chartWidth#,height=#request.chartHeight#,toolbar=no,location=no,status=no,menubar=no,scrollbars=auto,resizable,alwaysRaised,dependent,titlebar=no');">jpg</a>
			
			<cfif isDefined("contentDrawn") and not contentDrawn>
				There was a problem rendering your active content.
			</cfif>
		</cfif>
		
		<!--- tidy up and get id of the temp file --->		
		<!--- <cffile action="DELETE" file="#tempFileName#"> --->
	
</cfoutput>

<!--- NJH 2008/05/30 added the void around the openWin --->
<cfoutput><br><a href="javascript:void(openWin('/dashboard/dashboardIncluder.cfm?templateURL=displayGrid.cfm&filter=#filter#','tables','width=700,height=400,toolbar=no,location=no,status=no,menubar=no,scrollbars=auto,resizable,alwaysRaised,dependent,titlebar=no'));">view as tables</a></cfoutput>
