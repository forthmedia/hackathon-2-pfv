<!--- �Relayware. All Rights Reserved 2014 --->
<cfscript>
	contactsQuery = application.com.relayDashboard.getAll();	
</cfscript>
<!--- <cfdump var="#form#"> --->

<!--- <cfform name="myForm" format="flash" height="320" width="700" skin="haloSilver"> --->
	<cfformitem type="script">
		function applyFilter( term:String, grid:mx.controls.DataGrid, columns:Array ):Void {
		
			var filterTerm:String = term.toString().toLowerCase();
		
			if(filterTerm.length > 0) {
				if(_global.unfilteredData[grid.id] == undefined){
					if (_global.unfilteredData == undefined){
						_global.unfilteredData = {};
					}
					_global.unfilteredData[grid.id]  = grid.dataProvider.slice(0);
				}
				
				var filteredData:Array = [];
		
				for(var i = 0; i< _global.unfilteredData[grid.id].length; i++) {
					var item:Object =  _global.unfilteredData[grid.id][i];
					var added:Boolean = false;
					
					for(var j = 0; j< columns.length; j++){
					 	if(!added){
							var value:String = item[columns[j]].toString().toLowerCase();
							if(value.indexOf(filterTerm) != -1)	{
								filteredData.push(item);
								added = true;
							}
						}
						else {
							break;
						}
					}
				}
		
			grid.dataProvider = filteredData;
		
			}
			else {
				if(_global.unfilteredData[grid.id] != undefined) grid.dataProvider = _global.unfilteredData[grid.id];
			}
		}
	</cfformitem>
	<!--- <cfformgroup type="hbox"> --->	
	<cfformgroup type="panel" label="My Contacts">	
		<cfinput type="text" name="term" onchange="applyFilter(term.text,contactList,['firstName','lastName','email'])" label="Filter by:">
		<cfgrid name="contactList" query="contactsQuery" rowheaders="false">
			<cfgridcolumn name="firstName" header="First Name">
			<cfgridcolumn name="lastName" header="Last Name">
			<cfgridcolumn name="email" header="Email">
		</cfgrid>	
	</cfformgroup>
		
		<!--- <cfformgroup type="panel" label="Search in custom column">
			<cfformgroup type="horizontal">
				<cfinput type="text" name="secondTerm" onchange="applyFilter(secondTerm.text,contactList2,[column.selectedItem.data])" width="90" label="Filter by:">
				<cfselect name="column" label="in:" onchange="secondTerm.text=''" width="90">
						<option value="firstName">First Name</option>
						<option value="lastName">Last Name</option>					
						<option value="email">Email</option>
					</cfselect>
				</cfformgroup>
				<cfgrid name="contactList2" query="contactsQuery" rowheaders="false">
					<cfgridcolumn name="firstName" header="First Name">
					<cfgridcolumn name="lastName" header="Last Name">
					<cfgridcolumn name="email" header="Email">
				</cfgrid>
		</cfformgroup>
	</cfformgroup> --->
	<!--- <cfinput type="submit" name="submit" value="Submit"/> --->
<!--- </cfform> --->
