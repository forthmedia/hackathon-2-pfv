<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			leadManagerPipeline.cfm	
Author:				SWJ
Date started:		2005-11-07
Description:		This returns the current form variables in a cfgrid.  This 

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->

<!--- <cfif fileexists("#application.paths.code#\cftemplates\opportunityINI.cfm")>
	<cfinclude template="/code/cftemplates/opportunityINI.cfm">
</cfif> --->
<cf_include template="\code\cftemplates\opportunityINI.cfm" checkIfExists="true">

<cfset opportunityView = application.com.settings.getSetting("leadManager.opportunityView")>

<cfquery name="getPipeline" datasource="#application.SiteDataSource#">
select status as deal_status, statusSortOrder, stage, stageID, sum(calculated_budget) as This_Subtotal,
sum(overall_customer_Budget) as Subtotal, sum(calculated_budget_this_quarter) as Product_Budget
	from #opportunityView# 
		where 1=1
		
	<!--- <cfif isDefined("pipelinesumtotalTheseColumns") and pipelinesumtotalTheseColumns eq "Product_Budget">
		
	<cfelse>		
			and expectedCloseDate >= '#dateformat(StartDate,"mm-dd-yyyy")#' 
			and expectedCloseDate <= '#dateformat(EndDate,"mm-dd-yyyy")#'
	</cfif> --->		
	and onlyShowInQuarter = 1
	<cfif isDefined("entityID") and entityID neq "0">
		and (entityID =  <cf_queryparam value="#entityID#" CFSQLTYPE="CF_SQL_INTEGER" >  
		or partnerLocationID in (select locationID from location where organisationID =  <cf_queryparam value="#entityID#" CFSQLTYPE="CF_SQL_INTEGER" > )
		or distiLocationID in (select locationID from location where organisationID =  <cf_queryparam value="#entityID#" CFSQLTYPE="CF_SQL_INTEGER" > ))
	</cfif>
	<!--- 2012-06-27 PPB P-SMA001 note: setting checked inside getRightsFilterWhereClause() 		
	<cfif application.com.settings.getSetting("leadManager.countryScopeOpportunityRecords")>
		and countryid in (#request.relayCurrentUser.countryList#) 
	</cfif>
	--->
	#application.com.rights.getRightsFilterWhereClause(entityType="opportunity",alias="#opportunityView#").whereClause#	<!--- 2012-06-27 PPB P-SMA001 --->
	<cfif isDefined("frm_vendoraccountmanagerpersonid") and frm_vendoraccountmanagerpersonid neq "0">
		and vendorAccountManagerPersonID =  <cf_queryparam value="#frm_vendoraccountmanagerpersonid#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</cfif>
	<cfif isDefined("showComplete") and showComplete neq 0>
		AND liveStatus = 1
	</cfif>

	<cfif isDefined("pipelineSumShowOppStageIDs") and pipelineSumShowOppStageIDs eq "Yes">
	 	and stageid  in ( <cf_queryparam value="#pipelineSumShowSumOppStageIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	</cfif>

	group by status, statusSortOrder, stage, stageID 
	order by deal_status, statusSortOrder
</cfquery>

<cfformgroup type="panel" label="Pipeline Summary" style="#request.panelStyle#">
	<CFGRID name="pipelineSummaryGrid" height="200">
		<CFGRIDCOLUMN name="deal_status" header="Status" headeralign="CENTER" dataalign="LEFT" width="175">
		<cfgridcolumn name="This_Subtotal" header="Sub total" headeralign="CENTER" dataalign="LEFT" width="425" bold="No" italic="No" select="No" display="Yes" type="CURRENCY" headerbold="No" headeritalic="No">
		<cfoutput query="getPipeline">
			<cfgridrow data="#deal_status#,#This_Subtotal#">
		</cfoutput>
	</CFGRID>
</cfformgroup>

<!--- you can't put a cfchart call directly inside a cfformGroup so instead we will write it iout to a file below --->
<!--- <cf_chart format="jpg" chartheight="400" title="Pipeline Chart" chartwidth="350" seriesplacement="default" labelformat="currency" show3d="yes" xoffset=".04" yoffset=".04" tipstyle="mouseOver" pieslicestyle="sliced" name="myChart">
	<cf_chartseries type="horizontalbar" query="getPipeline" itemcolumn="deal_status" valuecolumn="this_subtotal" serieslabel="Status" seriescolor="000C0C" paintstyle="light"></cf_chartseries>
</cf_chart>

<cffile action="WRITE" file="#application.userfilesAbsolutePath#/temp/tempChart-#request.relayCurrentUser.personid#.jpg" output="#myChart#">
 --->
<!--- <cfformgroup  type="panel" label="Pipeline Summary Chart" style="#request.panelStyle#" height="450" visible="Yes" enabled="Yes">
	<cfformitem type="html" visible="Yes" enabled="Yes">
		<cfoutput>
		<!--- <a href="#request.currentSite.httpProtocol##cgi.HTTP_HOST##application. userFilesPath#/temp/tempChart-#request.relayCurrentUser.personid#.jpg" target="_blank">Click here</a> --->
		<img src="#request.currentSite.httpProtocol##cgi.HTTP_HOST##application. userFilesPath#/temp/tempChart-#request.relayCurrentUser.personid#.jpg" alt="" width="350" height="400" border="0">
		<!--- <OBJECT classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash2/cabs/swflash.cab##version=4,0,2,0" ID="tempChart_#request.relayCurrentUser.personid#_swf" name="tempChart_#request.relayCurrentUser.personid#_swf" WIDTH="350" HEIGHT="400">
			<PARAM NAME="movie" VALUE="#request.currentSite.httpProtocol##cgi.HTTP_HOST#/#application. userFilesPath#/temp/tempChart-#request.relayCurrentUser.personid#.swf"/>
			<PARAM NAME="quality" VALUE="high"/>
			<PARAM NAME="bgcolor" VALUE="##FFFFFF"/>
			<EMBED src="#request.currentSite.httpProtocol##cgi.HTTP_HOST#/#application. userFilesPath#/temp/tempChart-#request.relayCurrentUser.personid#.swf" quality="high" bgcolor="##FFFFFF" WIDTH="350" HEIGHT="400" TYPE="application/x-shockwave-flash" PLUGINSPAGE="http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash">
			</EMBED>
		</OBJECT> --->
		</cfoutput>
	</cfformitem>
</cfformgroup> --->
