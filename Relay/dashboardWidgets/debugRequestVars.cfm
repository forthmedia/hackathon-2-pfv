<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			debugRequestVars.cfm	
Author:				SWJ
Date started:		2005-11-07
Description:		This returns the current form variables in a cfgrid.  This 

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->
<cfformgroup type="panel" label="Reqest Variables" style="#request.panelStyle#">
	<CFGRID name="RequestGrid" height="330">
		<CFGRIDCOLUMN name="Key" header="Key" headeralign="CENTER" dataalign="LEFT" width="175">
		<CFGRIDCOLUMN name="Value" header="Value" headeralign="CENTER" dataalign="LEFT" width="425">
		<CFLOOP collection="#Request#" item="aa">
			<cfif isSimpleValue(Request[aa])>
				<CFGRIDROW data="#aa#,#Request[aa]#">
			<cfelse>
				<CFGRIDROW data="#aa#,ColdFusion_Structure">
			</cfif>
		</CFLOOP>
	</CFGRID>
</cfformgroup>
