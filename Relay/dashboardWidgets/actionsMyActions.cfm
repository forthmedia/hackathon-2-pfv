<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			debugRequestVars.cfm	
Author:				SWJ
Date started:		2005-11-07
Description:		This returns the current form variables in a cfgrid.  This 

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->

<CFQUERY NAME="GetActions" DATASOURCE="#application.SiteDataSource#">
    SELECT 	a.Description,
			a.Deadline,
			a.ActionID,
			a.timespent AS Estimate,
			p.firstName+' '+p.lastName AS Fullname,
			ast.Status,
			at.actionType,
			at.actionTypeID,
			ast.Status_series,	
			<!--- a.notes,
			CASE entityTypeID 
				WHEN 0 THEN		
					(select	p.firstName+' '+p.lastName+' - '+ o.organisationName+' ' +
						case when isNUll(l.telephone,'') <> '' then '<br>Switch: '+ l.telephone else '' end +  
						case when isNUll(p.officephone,'') <> '' then '<br>DD: ' + p.officephone else '' end +
						case when isNUll(p.mobilephone,'') <> '' then '<br>Mobile: '+p.mobilephone else '' end
						as entityName from person p
					INNER JOIN organisation o ON o.organisationID = p.organisationID
					join location l on l.locationid = p.locationid
					where p.personID = a.entityID) 
				WHEN 2 THEN	
					(select organisationName as entityName from organisation 
					where organisationID = a.entityID)
				WHEN 23 THEN	
					(select detail as entityName from opportunity 
					where opportunityID = a.entityID) 
				ELSE NULL
			END	as entityName, --->
			CASE a.priority 
				WHEN 0 THEN ''
				WHEN 1 THEN 'Low'
				WHEN 2 THEN 'Med'
				WHEN 3 THEN 'Hi'
			END AS priority,
			a.entityTypeID,
			a.entityID,
			a.RequestedBy,
			a.lastUpdated,
			a.lastUpdatedBy,
			a.created,
			a.ownerPersonID,
			(select max(permission) from recordRights 
				where a.actionID = recordid
				AND UserGroupID = #request.relayCurrentUser.personid#
				group by recordID) as permissionLevel
      FROM actions a 
           INNER JOIN actionStatus ast ON ast.actionStatusID = a.actionStatusID 
		   LEFT OUTER JOIN actionType at ON at.actionTypeID = a.actionTypeID 
		   LEFT OUTER JOIN Person p ON a.ownerPersonID = p.PersonID 
     WHERE 1=1 
	 and ownerPersonid = #request.relayCurrentUser.personid# 
</cfquery>

<cfscript>
	tabPage.heightOffset = 220;
	tabPage.widthOffset = 150;
	grid.height = 200;
	grid.width = 500;
</cfscript>

<cfformgroup  type="tabnavigator" visible="Yes" enabled="Yes">
	<cfformgroup  type="page" label="Due today" visible="Yes" enabled="Yes" height="{Stage.height-#tabPage.heightOffset#}" width="{Stage.width-#tabPage.widthOffset#}">
		<CFGRID name="TodayActionsGrid" height="#grid.height#" width="#grid.width#">
			<CFGRIDCOLUMN name="description" header="Description" headeralign="CENTER" dataalign="LEFT" width="175">
			<CFGRIDCOLUMN name="deadline" header="Value" headeralign="CENTER" dataalign="LEFT" width="400">
			<cfoutput query="getActions">
				<cfgridrow data="#description#,#deadline#">
			</cfoutput>
		</CFGRID>
	</cfformgroup>
	<cfformgroup  type="page" label="Due this week" height="{Stage.height-#tabPage.heightOffset#}" width="{Stage.width-#tabPage.widthOffset#}" visible="No" enabled="Yes">
		<CFGRID name="ThisWeekActionsGrid" height="#grid.height#" width="#grid.width#">
			<CFGRIDCOLUMN name="description" header="Description" headeralign="CENTER" dataalign="LEFT" width="175">
			<CFGRIDCOLUMN name="deadline" header="Value" headeralign="CENTER" dataalign="LEFT" width="425">
			<cfoutput query="getActions">
				<cfgridrow data="#description#,#deadline#">
			</cfoutput>
		</CFGRID>
	</cfformgroup>

	<cfformgroup  type="page" label="Due next week" height="{Stage.height-#tabPage.heightOffset#}" width="{Stage.width-#tabPage.widthOffset#}" visible="No" enabled="Yes">
		<CFGRID name="NextWeekActionsGrid" height="#grid.height#" width="#grid.width#">
			<CFGRIDCOLUMN name="description" header="Description" headeralign="CENTER" dataalign="LEFT" width="175">
			<CFGRIDCOLUMN name="deadline" header="Value" headeralign="CENTER" dataalign="LEFT" width="425">
			<cfoutput query="getActions">
				<cfgridrow data="#description#,#deadline#">
			</cfoutput>
		</CFGRID>
	</cfformgroup>
</cfformgroup>
