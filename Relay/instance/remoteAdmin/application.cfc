<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 

Application.cfc in the remoteRelayAdmin Directory

Rather a special case!
Similar to one in clustermanagent 

This file has to be compatible with all code versions 
 --->

<cfcomponent>
	

	<cfset this.mappings = {}>  <!--- seem to need to start with a blank one atleast --->
	<cfset this.sessionManagement = false>
	<!--- still having trouble with Customtag paths.  This technique won't work on boxes sharing core code (probably only a dev situation) --->
	<cfset currentPath  = replaceNoCase(getCurrentTemplatePath(),"instance\remoteAdmin\application.cfc","","ONCE")>
	<cfset customTagPath = currentPath & "..\customtags">
	<cfset this.customtagpaths = customTagPath> 
	
	<!--- This request parameter allows me to reference this cfc later on and update mappings when I know what the appname is  --->
	<cfset request.applicationCFC = this>
	
	<!--- WAB 2013-11-25 we are calling this from a CF8 box which expects a version 1 wsdl.  By default CF10 returns a version 2.  This switches back to version 1 --->
	<cfset this.wssettings.version.publish="1">

	<!--- 
	What I really need to do is set the application in application.cfc
	However at that point arguments.appname is not available
	When you call the cfc on a URL it is available as url.appname however when called by CF as a webservice it is not available
	I tried to use getHTTPRequestData() to extract the value of appname in the request.
	This would work, except for a bug in getHTTPRequestData() [CF8] which means that the whole request fails if this method is called
	I could just use application.cfm but then I would lose the ability to do the application specific mappings
	I have discovered that I can add mappings at run time by using a pointer back to the this scops of application.cfc (request.applicationcfc)

	No Session
	 --->
	

	<cffunction name="setApplication" returnType="struct">
		<cfargument name="appname">
		<cfset var result = {isOK = true}>
	
		<cfif structKeyExists (server,"serverInitialisationObj") and structKeyExists (server.serverInitialisationObj,"structures") and structKeyExists (server.serverInitialisationObj.structures.relayApplicationsStruct,appname)>

				<!---  WAB 2012-03-12 because application name is case sensitive we occasionally get the wrong case stored in our db, so do a check.  Also checks that application exists --->
				<cfset applicationNames = getListOfApplicationNames()>
				<cfset ListIndex = listfindNoCase(applicationNames,appname)>
				<cfif ListIndex is not 0>
					<cfset appname = listGetAt(applicationNames,listIndex)>
				<!--- <cfapplication name = #appname#> --->
				<cfset siteDetails = server.serverInitialisationObj.structures.relayApplicationsStruct[appname]>
	 			<cfset request.applicationCFC.sessionManagement = false>
				<cfset request.applicationCFC.mappings = structCopy(siteDetails.cf_mappings)>  
 				<cfset request.applicationCFC.customtagpaths= listappend(request.applicationCFC.customtagpaths,sitedetails.paths.customtags)>  
				<cfset result.applicationScope = getApplicationScope(appname)>
				<!--- check that say sitedatasource exists --->
				<cfif not structKeyExists (result,"applicationScope") or not structKeyExists (result.applicationScope,"siteDataSource")>
					<cfset result.isOK = false>
				</cfif>
				<cfelse>
					<cfset result.isOK = false>
				</cfif>

				
		 	<cfreturn result>

		<cfelse>
			<cfset result.isOK = false>
			<cfreturn result>
		</cfif>	
	
	

	</cffunction>
	
		<!--- these functions copied from sessionmanagement.cfc --->
	<cffunction access="public" name="getApplicationScope" return="struct">
		<cfargument name="applicationName" type="string" required="yes">

		<cfset appObj = createObject("java","coldfusion.runtime.ApplicationScopeTracker")>
		<cfreturn appObj.getApplicationScope(applicationName)>
			
	</cffunction >

	<cffunction access="public" name="getListOfApplicationNames" return="string">

		<cfset var result = "">
		<cfset appObj = createObject("java","coldfusion.runtime.ApplicationScopeTracker")>
		<cfset apps = appObj.getApplicationKeys()>

		<cfloop condition="#apps.hasMoreElements()#">    
			<cfset result = listAppend(result,apps.nextElement())>    		
		</cfloop>

		<cfreturn result>
			
	</cffunction >
	
	
</cfcomponent>
