<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			remoteRelayAdminUnsecured.cfc
Author:				
Date started:			/xx/07
	
Description:			
cfc needed for remote admin but which needs to be able to be able to be accessed by any browser regardless of IPAddress
originally had this function in webservices\misc - but this relied on the address of the server being set up as in setsitedetails.cfm and this was not always the case


Amendment History:

Date 		Initials 	What was changed
2015-01		WAB			Changes to deal with IPAddress being translated by load balancer.  
						Also added header to deal with Cross Site Scripting issues

Possible enhancements:


 --->

<cfcomponent> 


 	<cffunction access="remote" name="getClientIPAddress"  returntype="struct">
		<cfheader name="Access-Control-Allow-Origin" value="*">

		<!--- must not return the Load Balancer Address--->
		<cfset result = {"clientIPAddress" = getRemoteAddress(), "firewall" = false}>
		
		<cfif result.clientIPAddress is not cgi.remote_Addr>
			<cfset result.firewall = true>
		</cfif>
		
		<cfreturn result>
			
	</cffunction>	
	
	<!--- copied from com.security.cfc --->	
	<cffunction name="getRemoteAddress" output="false">
		<cfset var result = cgi.REMOTE_ADDR>
		<cfif structKeyExists (cgi,"HTTP_CLIENT_IP")>
			<cfset result = cgi.HTTP_CLIENT_IP>
		</cfif>
		<cfreturn result>
	</cffunction>


	
</cfcomponent>
