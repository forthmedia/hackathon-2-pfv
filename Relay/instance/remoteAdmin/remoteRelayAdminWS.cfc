<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			remoteAdmin.cfc
Author:				AJC/WB
Date started:		2006-04-05

Purpose:	Lots of functions that allow backend things to be done remotelyremote

Usage:


Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2006-10-18 New methods added
April 2007	WAB		Lots of work done to make this a useful general tool for doing code synching, email monitoring, and other central tasks

Sept 2007         WAb      functions to compare stored procedures
Jun 2008		WAB			Code to return length of uploaded and old files
2008-07-01 		WAB		Code to delete templates remotely implemented
2008-07-12 		WAB 		Code to get scheduled process information

2008-06-10 		WAB 		changes to allow source code to come from a multitude of locations, added in a filesLocation parameter to the readTemplate and other functions
2008-11-06 		WAB 		chnages to deal with machines hidden behind switches and coldfusion clustering
2008-11-25		WAB			Hacks to get reload application variables to work
2008-12-10		WAB 	Added recurse to getdirectory when filesAnddirectory query passed
2008-12-10		WAB 	Add a function to remove debug IPAddresses
2009-01-08		WAB   deleteTemplate added createBackup switch
2009-01-12		WAB		Added ability to read files from userFiles path and serverroot
2009-01-20		WAB   Added getting dbobjectinfo for query of items
2009-02-04		WAB  Allow GOs in SQL
2009-02-09      WAB  getMailPath function failed on Lexmark instances (to do with instantiating the adminiapi - serverinstance, now have a much better way of working it out throught the java object
2009-03-03		WAB test for application started when running functions on apps
2009-10-28 		WAB		fixed problem with reloading cfc cache
2009-11-09 		WAB still getting mail path wrong.  Need to deal with different versions of CF
2009-12-16		WAB return complete directory entry from readTemplate


2011/9/			WAB		Added some code to deal with infinite loop which occurs if we try to call runFunctionOnSpecificInstance() on a specific IPAddress but the server is reporting that it is on a different IPAddress (because server has multiple IPs)
2011/9/			WAB		Also better functions for testing isTrusted - test all IPAddresses with subnet masks
2013/10					Changes to support CF10 - mainly deciding what port we are on
2015-02-10		WAB		Change to RW breakout IP Address
Possible enhancements:

--->
<cfcomponent>
	<cfsetting enablecfoutputonly="Yes" showdebugoutput="No" >

	<!--- Internal network and via Relayware Breakout --->
	<cfset this.trustedIPAddresses = "192\.168\.0\.|62\.232\.9\.210">


	<!--- WAB 2008-11-05
	To allow remote admin tools to access machines which are hidden behind switches
	The request can come in to any server using the switch address
	The server receiving the request checks whether it is the final destination of the request.
	If it is then it runs the function, if it not is then the request is passed off

	For most servers the IPAddressAndPort variable is left blank, so that the request is dealt with by the receiving machine
	--->



	<cffunction name="runFunctionOnSpecificInstance" access="remote" returntype="struct">
		<cfargument name="methodName" type="string" default = "">
		<cfargument name="IPAddressAndPort" type="string" default = "">
		<cfargument name="getDebug" type="boolean" default = "false">
		<cfargument name="argumentWDDX" type="string" default = "">


		<cfset var requiredipaddress = listFirst(IPAddressAndPort,":")>
		<cfset var requiredport  = "">
		<cfset var result = structNew()>
		<cfset var argumentCollection = structNew()>
		<cfset var thisMachineIP = getServerIPAddress()>
		<cfset var thisMachinePort = getInstancePort()>
		<cfset var theFunctionToRun = "">
		<cfset var theResult = "">
		<cfset var newResult = "">
		<cfset var remoteAdminPath = "">


		<cfif requiredipaddress is not "">
				<!--- make sure we have an IPaddress not a name --->
				<CFOBJECT NAME="iaddrClass"  CLASS="java.net.InetAddress" TYPE="JAVA" ACTION="CREATE">
				<cfset requiredipaddress = iaddrClass.getByName(requiredipaddress).getHostAddress()>
		</cfif>
		<cfif listLen(ipaddressAndPort,":") is 2>
			<cfset requiredport  = listLast(IPAddressAndPort,":")>
		</cfif>

				<cfif argumentWDDX is not "">
 					<CFWDDX ACTION="WDDX2CFML" INPUT="#argumentWDDX#" OUTPUT="argumentCollection">
				</cfif>

		<cfif (requiredipaddress is "") or (requiredipaddress is thisMachineIP and (requiredport  is "" or requiredport  is thisMachinePort))>
			<!--- This is the correct machine to run function on --->
			<cftry>

 	 			<cfset theFunctionToRun = variables[methodName]>
	 			<cfset theResult = theFunctionToRun (argumentCollection = argumentCollection)>

				<!--- this code depends upon the function returning a struct - if it hasn't then we need to doctor --->
	 			<cfif isStruct(theresult)>
					<cfset result = theResult>
				<cfelse>
					<cfset result.result = theResult>
				</cfif>

	 			<cfif structKeyExists(argumentCollection,"appnames") and not structKeyExists(theResult,"isOK")>
					<!---  need to bundle the result up into another structure!--->
					<cfset newResult = structNew()>
					<cfset newResult.apps = result>
					<cfset newResult.isOK = true>
					<cfset result = newResult>
				</cfif>

				<cfcatch>
					<cfset result = {isOK = false,error=cfcatch.message,tagContext = cfcatch.tagcontext[1]}>
					<cfif isDefined("cfcatch.tagcontext")>
						<cfset result.error = result.error & "  " & cfcatch.tagcontext[1].template & " line " & cfcatch.tagcontext[1].line>
					</cfif>

				</cfcatch>

			</cftry>

				<cfif getDebug>
					<cfparam name="result.debug" default="#structNew()#">
		 			<cfset result.debug.ipaddressofinstance = thisMachineIP>
	 				<cfset result.debug.portofinstance = thisMachinePort>
					<cfset result.debug.wddxReceived = argumentWDDX>
					<cfset result.debug.argumentCollection = argumentWDDX>
					<cfset result.remote_addr =  cgi.remote_addr>
				</cfif>

		<cfelseif structKeyExists (argumentCollection,"forwarded") and argumentCollection.forwarded>
				<!--- WAB 2011/9/ added this section --->
				<cfset Result = structNew()>
				<cfset Result.isOK = false>
				<cfset Result.error = structNew() >
				<cfset Result.error.detail =  "IPAddress #requiredipaddress# is wrong">
				<cfset Result.error.message = "" >

		<cfelse>
			<!--- need to pass off to another server using the local address provided--->
 				<cftry>
					<cfif requiredport is "" or requiredport is "80">
						<cfset remoteAdminPath = "instance/remoteAdmin">
					<cfelse>
						<cfset remoteAdminPath = "instance/remoteAdmin">
					</cfif>

					<!--- WAB 2011/9/ set a variable to show that the request has been forwarded once - prevent a runaway loop if the IPaddress given is wrong (can happen if machine has two ip addresses and we lose track) --->
					<cfset argumentCollection.forwarded = true>
					<CFWDDX ACTION="CFML2WDDX" INPUT="#argumentCollection#" OUTPUT="argumentWDDX">


					<cfset wsdlURL = "http://#IPAddressAndPort#/#remoteAdminPath#/remoteRelayAdminWS.cfc?wsdl">
	 				<cfinvoke
						webservice="#wsdlURL#"
						method = "runFunctionOnSpecificInstance"
						methodname = "#methodName#"
						IPAddressAndPort = "#ipaddressAndPort#"
						getdebug = #getDebug#
						argumentwddx = #argumentWDDX#
						returnVariable = "Result"
						timeout = 10
						refreshWSDL="yes"
					/>

					<cfcatch>
						<cfset Result = structNew()>
						<cfset Result.isOK = false>
						<cfset Result.error = structNew() >
						<cfset Result.error.detail = cfcatch.detail >
						<cfset Result.error.message = cfcatch.message >

						<cfif structKeyExists (cfcatch,"cause") and structKeyExists (cfcatch.cause,"type") and cfcatch.cause.type is "java.io.FileNotFoundException" >
							<cfset Result.error.message = "WebService Mapping Does Not Exist #ipaddressAndPort#.  Called from #thisMachineIP# #thisMachineport#">
						</cfif>
						<cfset result.urlused = "#wsdlURL#&method=runFunctionOnSpecificInstance&methodName=#methodName#&IPAddressAndPort=#ipaddressAndPort#&argumentwddx=#urlencodedformat(argumentWDDX)#">

					</cfcatch>
				</cftry>

				<cfif getDebug>
					<cfparam name="result.debug" default="#structNew()#">
					<cfset result.debug.urlused = "HTTP://#ipaddressandport#/#remoteAdminPath#/remoteRelayAdminWS.cfc?wsdl&method=runFunctionOnSpecificInstance&methodName=#methodName#&IPAddressAndPort=#ipaddressAndPort#&argumentwddx=#urlencodedformat(argumentWDDX)#">
 					<cfset result.debug.initialRequestReceivedBy = "#thisMachineIP#:#thisMachinePort#">
				</cfif>

		</cfif>
			<cfset result.isIpAddressTrusted = isIpAddressTrusted()>
		<cfreturn result>


	</cffunction>

	<cffunction name="helloWorld" access="remote" returntype="struct">
	<cfset x = {isOK=true,message="Hello World"}>

	<cfreturn x>

	</cffunction>

	<!--- this was a very nasty method for getting the current port, bit of a mess as well
	basically looks in the configuration XML for the port setting
	wish could find a good one
	also needed in com/clustermanagement.cfc - in time should call a single function
	Can't be replaced with called to application.com because application scope not available here
	--->

	<cffunction name="getInstancePort" access="remote" returntype="string">

		<cfset var result = "">
		<cffile action="read" file="#server.coldfusion.rootdir#/runtime/conf/server.xml" variable="content">
		<cfset xmldoc = xmlparse (content)>
		<cfset XMLresult = XmlSearch(xmldoc, "//Server/Service/Connector[contains(@protocol,'http')]")>

		<cfif arrayLen(XMLresult)>
			<cfset result = XMLresult[1].xmlattributes.port>
		</cfif>
		<cfreturn result>

	</cffunction>

	<cffunction name="isIpAddressTrusted" access="private" returntype="boolean">

		<cfset var result = false>
		<cfset var IpaddressesAndSubnetArray = "">

			<cfif
					reFind(this.trustedIPAddresses,cgi.remote_addr) OR
					cgi.remote_addr is getServerIPAddress() or <!--- calling itself --->
					listrest (reverse(cgi.remote_addr),".") IS 	listrest (reverse(getServerIPAddress()),".")  <!--- last one allows anyone on the same subnet to access the functions - may be dodgy --->
				>
				<cfset result = true>
			<cfelse>
				<!--- more detailed check of ipaddress and subnets --->
				<!--- This code copied from security.cfc --->
				<cfset IpaddressesAndSubnetArray = getIpaddressesAndSubnets()>

				<cfloop index="AddressAndSubnet" array = "#IpaddressesAndSubnetArray#">
					<cfif areIpAddressesOnSameSubnet (AddressAndSubnet.address,cgi.remote_addr, AddressAndSubnet.mask )>
						<cfset result = true>
						<cfbreak>
					</cfif>
				</cfloop>

			</cfif>

			<cfreturn result>

	</cffunction>

	<!--- These functions copied from security.cfc and initialise.cfc --->
	<cffunction name="getIpaddressesAndSubnets" access=private returnType="array">

		<cfset var result = arrayNew(1)>
		<cfset var regExpResultArray = "">
		<cfset var IPConfigString = "">
	<!--- <cfset var regExpObj = createObject("component","com.regExp")>--->

		<cfexecute
			name = "C:\WINDOWS\system32\ipconfig.exe"
			variable ="IPConfigString"
			timeout = "5"
		/>

		<cfset regExpResultArray = refindAllOccurrences ("Address.*?([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}).*?Mask.*?([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})",IPConfigString)>

			<cfloop array="#regExpResultArray#" index="i">
				<cfset tempStruct = {address=i[2],mask=i[3]}>
				<cfset arrayAppend (result,tempStruct)>
			</cfloop>

		<cfreturn result>

	</cffunction>


	<cffunction name="areIpAddressesOnSameSubnet" access=private returnType="boolean">
		<cfargument name = "IPAddress1" required="true">
		<cfargument name = "IPAddress2" required="true">
		<cfargument name = "subnetMask" default="255.255.255.0">

		<cfset var result = true>

			<cfloop index="i" from ="1" to = "4">
				<cfif bitAND (listgetat(ipaddress1,i,"."), listgetat(subNetMask,i,".")) is not bitAND (listgetat(ipaddress2,i,"."), listgetat(subNetMask,i,".")) >
					<cfset result = false>
					<cfbreak>
				</cfif>
			</cfloop>

		<cfreturn result>

	</cffunction>


	<cffunction name="refindAllOccurrences"  access="public" hint="" returntype="array" output="false">
			<cfargument name="reg_expression" type="string" required="true">
			<cfargument name="string" type="string" required="true">
			<cfargument name="groupNames" type="Struct" default="#structNew()#">  <!--- allows the function to return named groups rather than numbered --->
			<cfargument name="singleOccurrence" type="boolean" default="false">

			<cfset var result = arrayNew(1)>
			<cfset var findresult = "">
			<cfset var thisResult = ""> <!---  --->
			<cfset var i = 0>
			<cfif structCount (groupNames) is 0>
				<cfset arguments.groupNames = {1=2,2=3,3=4,4=5,5=6,6=7,7=8,8=9,9=10}> <!--- does limit to 9 groups in a regexp - hope enough --->
			</cfif>

			<cfset findresult = refindnocase (reg_expression,string,0,true)>

			<cfloop condition = "findresult.pos[1] is not 0">
				<cfset thisResult = structNew()>

				<cfset thisResult.String = mid(string,findresult.pos[1],findresult.len[1])>

				<cfloop index="i" from="2" to= "#arrayLen(findresult.pos)#">
					<cfif findresult.pos[i] is not 0>
						<cfset thisResult[groupNames[i-1]] = mid(string,findresult.pos[i],findresult.len[i])>
					<cfelse>
						<cfset thisResult[groupNames[i-1]] = "">
					</cfif>
				</cfloop>

				<cfset arrayAppend(result,thisResult)>

				<cfif singleOccurrence><cfbreak></cfif>
				<cfset findresult = refindnocase (reg_expression,string,findresult.pos[1] + findresult.len[1],true)>

			</cfloop>

		<cfreturn result>

	</cffunction>

	<cffunction name="getServerIPAddress" access="private" returntype="string">

				<cfset iaclass=CreateObject("java", "java.net.InetAddress") >
				<cfset  addr=iaclass.getLocalHost()>
				<cfreturn  addr.getHostAddress()>

	</cffunction>


	<cffunction name="runSQL" access="private" returntype="struct">
		<cfargument name="appname" type="string" required = true>
		<cfargument name="sql" type="string" required = true>

		<cfset var result = structNew()>
		<cfset var doQuery="">

		<cfset setApplication = request.applicationCFC.setApplication(appname)>
		<cfif not setApplication.isOK>
			<cfset result.OK = false>
			<cfset result.NumberOK = 0>
			<cfset result.error.message = "Application Not Available #appname#">
			<cfreturn result>
		<cfelse>

			<cfset applicationScope = setApplication.applicationScope>
			<!--- look for the word GO in the code, if found then need to split into blocks --->

				<cftry>

					<cfset sqlArray = arrayNew(1)>
					<cfset re= "\A(.*?)(\n[\s]*GO[\s]*[\n|\Z]|\Z)">   <!--- regular expression to get rid of GOs - can't deal with GO as either the first or last word without any spaces --->
					<cfset tempSQL = sql>
					<cfset counter = 0>   <!--- incase my regexp goes wrong --->

					<cfloop condition = "len(tempSQL) is not 0 and counter LT 100">
						<cfset counter = counter  + 1>
						<cfset found = refindnocase (re,tempSQL,0,true)>
						<cfset sqlFound = mid(tempSQL,found.pos[2],found.len[2])>
						<cfif trim(sqlFound) is not "">
							<cfset arrayAppend(sqlArray,sqlFound)>
						</cfif>
						<cfif len(tempSQL) - found.len[1] LTE 1 >   <!--- sorry my reExp went awry and left with an odd character at the end (if a line feed) --->
							<cfset tempSQL = "">
						<cfelse>
							<cfset tempSQL = right(tempSQL,len(tempSQL) - found.len[1])>
						</cfif>
					</cfloop>


					<cfset result.queries = arrayNew(1)>
					<cfset result.numberOfQueries = arrayLen(sqlArray)>
					<cfset result.numberNOTOK = 0>
					<cfset result.numberOK = 0>

					<cfloop index = "i" from = "1" to = "#arrayLen(sqlArray)#">
						<cfset result.queries[i] = structNew()>
						<cfset resultPointer = result.queries[i]>
						<cfset resultPointer.sql = 	sqlArray[i]>

						<cftry>
							<cfset thisSQL = sqlArray[i]>

		 					<cfquery result = "doQueryResult" name="doQuery" datasource = "#applicationScope.sitedatasource#">
							 #preserveSingleQuotes(thisSQL)#
							</cfquery>
							<cfset resultPointer.recordcount  = doQueryResult.recordcount>



							<cfif isdefined("doQuery")>
								<cfset resultPointer.result = doQuery>
							<cfelse>
								<cfset resultPointer.result = "">
							</cfif>

								<cfset resultPointer.isOK = true>
								<cfset result.numberOK = result.numberOK  + 1>

							<cfcatch>
								<cfset result.numberNOTOK = result.numberNOTOK + 1>
								<cfset resultPointer.isOK = false>
								<cfset resultPointer.error = structNew()>
								<cfset resultPointer.error.message = cfcatch.message>
								<cfset resultPointer.error.detail = cfcatch.detail>
								<cfset resultPointer.error.sql = sqlArray[i]>

							</cfcatch>
						</cftry>
					</cfloop>
						<!--- note that unfortunately this uses .OK rather than .isOK --->
						<cfset result.OK = not result.numberNOTOK>

					<cfcatch type="Any">
						<cfset result.OK = false>
						<cfset result.error.message = cfcatch.message>
						<cfset result.error.detail = cfcatch.detail>

					</cfcatch>

				</cftry>

			</cfif>

		<cfreturn result>

	</cffunction>



	<cffunction name="getdbObjectInfo" access="private" returntype="struct">
		<cfargument name="appname" type="string" required = true>
		<cfargument name="dbObjectName" type="string" default = "">
		<cfargument name="dbObjectType" type="string" default = "">
		<cfargument name="dbObjectQuery" type="query" default = "#queryNew('dummy')#">

		<cfset var result = structNew()>
		<cfset var doQuery="">


		<cfset setApplication = request.applicationCFC.setApplication(AppName)>
		<cfif setApplication.isOK>
			<cfset applicationScope = setApplication.applicationScope >
				<cftry>

					<!---  this is a sightly convluted query to work out the size of the sp.
		we need to deal with
		a) query analyser can add comments before the create statement which won't be there if the code has been released via CF
		b) query analyser keeps trailing CRs which CF doesn't
		We do a patindex for "create procedure" so that we don't count anything before this
		We replace all cr/lf with blank
		We right trim the final section   (remember that the sp can be split over more than one row in syscomments)

		2008-02-05 Actually ended up replacing all spaces with '', gives a pretty good indication of similarity
		    --->

		<cfquery result = "doQueryResult" name="doQuery" datasource = "#applicationScope.sitedatasource#">

		<!---
		2009-10-12  seemed to have lost the patindex and no longer need the maxC.colid,
				so rewrote and added tab as a character to replace

					select xtype as type, o.name, upper(o.name) as ucaseName, sum(
								len(
									case when c.colid = maxC.colid  then
										rtrim(replace(replace(replace(c.text,char(13),''),char(10),''),' ',''))
									else
										rtrim(replace(replace(replace(c.text,char(13),''),char(10),''),' ',''))
									 end

		)

		 ) as size


		from sysobjects o inner join syscomments c on o.id = c.id
		inner join
		(select o2.id, max(colid) as colid from sysobjects o2 inner join syscomments c2 on o2.id = c2.id  group by o2.id) as maxC on o.id = maxC.id
		 --->
		 			select xtype as type, o.name, upper(o.name) as ucaseName, sum(
								len(
									rtrim(replace(replace(replace(replace(
										case
											when c.colid = 1 and PATINDEX ('%CREATE%PROC%',c.text) <> 0 then
												stuff (c.text,1,PATINDEX ('%CREATE%PROC%',c.text)-1,'')
											when c.colid = 1 and PATINDEX ('%CREATE%View%',c.text) <> 0 then
												stuff (c.text,1,PATINDEX ('%CREATE%view%',c.text)-1,'')
											else
												c.text
									 end
									,char(13),''),char(10),''),char(9),''),' ',''))
		)
		 ) as size


		from sysobjects o inner join syscomments c on o.id = c.id


						where
						<cfif dbObjectType is not "">
							xtype =  <cf_queryparam value="#dbObjectType#" CFSQLTYPE="CF_SQL_VARCHAR" >
							<cfif dbObjectName is not "">and name  in ( <cf_queryparam value="#dbObjectName#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> ) </cfif>
						<cfelse>
							(
								1 = 0
							<cfloop query="dbObjectQuery">
									or
								(xtype =  <cf_queryparam value="#relativeTo#" CFSQLTYPE="CF_SQL_VARCHAR" >  and name  like  <cf_queryparam value="#replace(replace(name,"*.*","%"),"*","%")#" CFSQLTYPE="CF_SQL_VARCHAR" > )
							</cfloop>
							)
						</cfif>

						and name not like 'vdataload%'  -- these are autogenerated so don't get synched
						group by o.id, o.xtype, o.name
					</cfquery>

						<cfset result.isOK = true>
						<cfset result.query = doQuery>
						<!--- <cfset result.dbObjectQuery = doQueryResult> --->

					<cfcatch type="Any">
						<cfset result.isOK = false>
						<cfset result.error.message = cfcatch.message>
						<cfset result.error.detail = cfcatch.detail>
					</cfcatch>

				</cftry>
			<cfelse>
				<cfset result.isOK = false>
				<cfset result.error.message = "Application Not Available">

			</cfif>

		<cfreturn result>

	</cffunction>

	<cffunction name="readdbObject" access="private" returntype="struct">
		<cfargument name="appname" type="string" required = true>
		<cfargument name="dbObjectName" type="string" required = true>
		<cfargument name="dbObjectType" type="string" required = true>

		<cfset var result = structNew()>
		<cfset var temp ="">
		<cfset var  doQuery = "">


		<cfset setApplication = request.applicationCFC.setApplication(AppName)>
		<cfif setApplication.isOK>
			<cfset applicationScope = setApplication.applicationScope >
				<cftry>
					<cfquery name="doQuery" datasource = "#application.sitedatasource#">
					select
						-- name, couldn't do  name - row to long!
					text from sysobjects o inner join syscomments c on o.id = c.id
						where
						xtype  like  <cf_queryparam value="#dbObjectType#" CFSQLTYPE="CF_SQL_VARCHAR" >
						<cfif dbObjectName is not "">and name  in ( <cf_queryparam value="#dbObjectName#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> ) </cfif>
						order by o.id, c.colid
					</cfquery>

						<cfset result.isOK = true>
						<cfset result.text = valuelist(doQuery.text,"")>

					<cfcatch type="Any">
						<cfset result.isOK = false>
						<cfset result.error.message = cfcatch.message>
						<cfset result.error.detail = cfcatch.detail>
					</cfcatch>

				</cftry>
			<cfelse>
				<cfset result.isOK = false>
				<cfset result.error.message = "Application Not Available">

			</cfif>


		<cfreturn result>


	</cffunction>

	<cffunction name="resetFlagStructures" access="private" returntype="struct">
		<cfargument name="appnames" type="string" required = true>

		<cfreturn runAFunctionOnMultipleApps(appnames=appnames,objectname="relay.com.flag",method="updateFlagAndGroupStructure")>

	</cffunction>



	<cffunction name="resetApplicationVariables" access="private" returntype="struct">
		<cfargument name="appnames" type="string" required = true>

		<cfreturn runAFunctionOnMultipleApps(appnames=appnames,objectname="relay.com.applicationVariables",method="reLoadAllVariables")>

	</cffunction>

	<cffunction name="reloadCfcsWithReloadCache" access="private" returntype="struct">
		<cfargument name="appnames" type="string" required = true>
		<cfreturn reloadcfcsprivate(appnames=appnames,reloadcache="yes")>
	</cffunction>



	<cffunction name="reloadcfcs" access="private" returntype="struct">
		<cfargument name="appnames" type="string" required = true>

		<cfreturn reloadcfcsprivate(appnames=appnames,reloadcache="no")>

	</cffunction>



	<cffunction name="reloadcfcsPrivate" access="private" returntype="struct">
		<cfargument name="appnames" type="string" required = true>
		<cfargument name="reloadCache" default="No" >
		<cfset argCollection = structNew()>
		<cfset argCollection.refreshCache = reloadCache>   <!--- WAB 2009-10-28 confusion between reload and refresh.  Needed to be refreshcache passed through --->
		<cfreturn runAFunctionOnMultipleApps(appnames=appnames,objectname = "com.applicationVariables",method = "loadApplicationComponents",argCollection=argCollection,verbose=true)>
<!--- 		<cfreturn runATemplateOnMultipleApps(appnames=appnames,template="..\..\templates\setApplicationComponents.cfm",flush=1,reLoadCache=reLoadCache,verboseoutput=false)>
 --->
	</cffunction>


	<cffunction name="runTemplate" access="private" returntype="struct">
		<cfargument name="appnames" type="string" required = true>
		<cfargument name="template" required=true >


		<cfreturn runATemplateOnMultipleApps(appnames=appnames,template="/relay/#template#")>

	</cffunction>


	<cffunction name="runAFunctionOnMultipleApps" access="private" returntype="struct">
		<cfargument name="appnames" type="string" required = true>
		<cfargument name="objectName" type="string" required = true>
		<cfargument name="method" type="string" required = true>
		<cfargument name="argCollection" type="struct" default="#structNew()#">

		<cfset var appname = "">
		<cfset var result = structNew()>

			<cfloop index="appname" list = "#appnames#">
				<cfset result[appname] = structNew()>
				<cfset setApplication = request.applicationCFC.setApplication(AppName)>

				<cfif setApplication.isOK>
					<cfset applicationScope = setApplication.applicationScope >


					<cftry>
							<cfobject
								name="obj"
								component = #objectName#>

							<cfinvoke
								component = #obj#
								method = #method#
								argumentcollection=#argCollection#
								applicationScope = #applicationScope#
								returnVariable = "x"
								>



							<cfset result[appname].isok = true>
							<cfset result[appname].result = x>
						<cfcatch>
							<cfset result[appname].isok = false>
							<cfset result[appname].error = cfcatch.message>
							<cfif isDefined("cfcatch.tagcontext")>
								<cfset result[appname].error = result[appname].error & "  " & cfcatch.tagcontext[1].template & " line " & cfcatch.tagcontext[1].line>
							</cfif>

						</cfcatch>

					</cftry>
				<cfelse>
					<cfset result[appname].isok = false>
					<cfset result[appname].error = "Application #appname# Not Available.  Available Apps are #getListOfRelayAppsOnThisServer()#">

				</cfif>


			</cfloop>


		<cfreturn result>

	</cffunction>


	<cffunction name="runATemplateOnMultipleApps" access="private" returntype="struct">
		<cfargument name="appnames" type="string" required = true>
		<cfargument name="template" type="string" required = true>


		<cfset var arg = "">
		<cfset var appname = "">
		<cfset var content_html = "">
		<cfset var liveRelayApps = "">
		<cfset var testSite= "">
		<cfset var result = structNew()>


		<!--- arguments passed in in the arguments collection don't seem to be accessible without the arguments scope
			can't explain this, but work round it by putting them in the variables scope
		 --->
		<cfloop item="arg" collection="#arguments#">
			<cfif listfindnocase("appnames,template", arg) is 0>
				<cfset variables[arg] = arguments[arg]>
			</cfif>
		</cfloop>


			<cfloop index="appname" list = "#appnames#">
				<cfset result[appname] = structNew()>

				<cfset setApplication = request.applicationCFC.setApplication(AppName)>
				<cfif setApplication.isOK>
					<cfset applicationScope = setApplication.applicationScope >

						<cftry>
							<!--- <cfset liverelayapps = application. liverelayapps> --->
							<!--- this is a very nasty hack - discovered after doing some work on setapplicationvariables that it no longer worked remotely --->
								<cfif template contains "setapplicationvariables.cfm">
										<cfset structDelete (applicationScope,"appvariables")>
								</cfif>


							<cfsavecontent variable = "content_html">
								<cfinclude template = "#template#">
							</cfsavecontent>

							<cfset result[appname].isOK = true>
							<cfset result[appname].content = content_html>

						<cfcatch>
							<cfset result[appname].isOK = false>
							<cfset result[appname].message = cfcatch.message>
							<cfset result[appname].detail = cfcatch.detail>

						</cfcatch>

						</cftry>

				<cfelse>
					<cfset result[appname].isOK = false>
					<cfset result[appname].content = "Application not started">
				</cfif>


			</cfloop>
		<cfreturn result>

	</cffunction>

	<cffunction name="sendtestingEmail" access="private" returntype="boolean">
		<cfargument name="Subject" type="string" required="true">
		<cftry>
 			<cfmail to="MailMonitor@foundation-network.com"
				from="relayhelp@foundation-network.com"
				subject="#Subject#">
					this is a mail monitor email
			</cfmail>
			<cfcatch>
				<cfreturn false>
			</cfcatch>
		</cftry>

		<cfreturn true>
	</cffunction>

	<cffunction name="getMailServerInfo" access="private" returntype="struct">
		<cfargument name="z"  default = "">   <!--- actually the cfadmin password --->
		<cfreturn getMailServerInfoPrivate(z=z)>
	</cffunction>

	<cffunction name="getMailServerInfoDetailed" access="private" returntype="struct">
		<cfargument name="z"  default = "">   <!--- actually the cfadmin password --->
		<cfargument name="returnFileNames"  default = "false">
		<cfargument name="returnFileContent"  default = "false">
		<cfargument name="returnSubjectAndEmail"  default = "false">

		<cfreturn getMailServerInfoPrivate(argumentCollection = arguments, FileContentLength = 0 )>
	</cffunction>


	<cffunction name="getMailServerInfoPrivate" access="private" returntype="struct">
		<cfargument name="z"  default = "">   <!--- actually the cfadmin password --->
		<cfargument name="returnFileNames"  default = "false">
		<cfargument name="returnFileContent"  default = "false">
		<cfargument name="returnSubjectAndEmail"  default = "false">
		<cfargument name="FileContentLength"  default = "0">

				<cfset var result = structnew()>
				<cfset var mailPathDetails = "">
				<cfset var mailPath = "">
   				<cfset var emailObj = 	createObject("component","relay.com.email")>
				<cfset mailPathDetails = getMailPath (password=z)>

				<cfif mailPathDetails.isOK>
						<cfset mailPath = mailPathDetails.path>
								<cfdirectory action="LIST" directory="#mailPath#\undelivr" name="undelivr">
								<cfdirectory action="LIST" directory="#mailPath#\spool" name="spool">
								<cfset result.undelivrCount = undelivr.recordcount>
								<cfset result.spoolCount = spool.recordcount>
								<cfif returnFileNames>
									<cfset result.undelivr = undelivr>
									<cfset result.spool = spool>

									<cfloop list = "spool,undelivr" index="spoolOrUndelivr">
										<cfset theQuery = result[spoolOrUndelivr]>
										<cfset queryAddColumn(theQuery,"toaddress",arrayNew(1))>
										<cfset queryAddColumn(theQuery,"toaddressValid",arrayNew(1))>
										<cfset queryAddColumn(theQuery,"subject",arrayNew(1))>
										<cfset queryAddColumn(theQuery,"content",arrayNew(1))>
									</cfloop>


								</cfif>

								<cfif returnFileContent>
									<cfloop list = "spool,undelivr" index="spoolOrUndelivr">
										<cfset theQuery = result[spoolOrUndelivr]>
										<cfloop query = "theQuery" startrow="1" endrow = 20>
											<cffile action="READ" file="#mailPath#\#spoolOrUndelivr#\#theQuery.name#" variable="fileContent">
											<cfif FileContentLength is not 0>
												<cfset querySetCell(theQuery,"content",left (fileContent,FileContentLength),currentRow)>
											<cfelse>
												<cfset querySetCell(theQuery,"content",fileContent,currentRow)>
											</cfif>
											<cfset toAddressStruct = reFindNoCase ("\nTo:(.*?)\n", fileContent,1,true)>
											<cfif toAddressStruct.pos[1] is not 0>
												<cfset querySetCell(theQuery,"toAddress",trim(mid(fileContent, toAddressStruct.pos[2], toAddressStruct.len[2])),currentRow)>
												<cfset querySetCell(theQuery,"toAddressValid",emailObj.isValidEmail(toaddress),currentRow)>
											<cfelse>
												<cfset querySetCell(theQuery,"toAddress","",currentRow)>
												<cfset querySetCell(theQuery,"toAddressValid",false,currentRow)>
											</cfif>

										</cfloop>
									</cfloop>
								</cfif>
									<!--- 2007-09-11 WAB added this extra function for a special job to get a list of undelivered emails, could be integrated into the loop above --->
								<cfif returnSubjectAndEmail>
									<cfloop list = "spool,undelivr" index="spoolOrUndelivr">
										<cfset theQuery = result[spoolOrUndelivr]>
										<cfloop query = "theQuery" >

											<cffile action="READ" file="#mailPath#\#spoolOrUndelivr#\#theQuery.name#" variable="fileContent">
											<cfset toAddressStruct = reFindNoCase ("\nTo:(.*?)\n", fileContent,1,true)>
											<cfif toAddressStruct.pos[1] is not 0>
												<cfset querysetCell(theQuery,"toAddress", trim(mid(fileContent, toAddressStruct.pos[2], toAddressStruct.len[2])),currentrow)>
											</cfif>
											<cfset SubjectStruct = reFindNoCase ("\nSubject:(.*?)\n", fileContent,1,true)>
											<cfif SubjectStruct.pos[1] is not 0>
												<cfset querysetCell(theQuery,"subject", trim(mid(fileContent, SubjectStruct.pos[2], SubjectStruct.len[2])),currentrow)>
											</cfif>


										</cfloop>

									</cfloop>

								</cfif>



								<cfset result.isOK = true>
				<cfelse>
						<cfset result.isOK = false>
				</cfif>

				<cfreturn result>


	</cffunction>


	<cffunction name="writeMailServerFile" access="private" returntype="struct">
		<cfargument name="z"  default = "">   <!--- actually the cfadmin password --->
		<cfargument name="FileName"  required = true>
		<cfargument name="FileContent"  required = true>
		<cfargument name="directory"  default = "spool">

				<cfset var result = structnew()>
				<cfset var mailPathDetails = "">
				<cfset var mailPath = "">
   				<cfset var emailObj = 	createObject("component","relay.com.email")>
				<cfset mailPathDetails = getMailPath (password=z)>
				<cfset result.file = fileName>
				<cfif mailPathDetails.isOK>
						<cfset mailPath = mailPathDetails.path>
						<cffile action="WRITE" file="#mailPath#\#directory#\#filename#" output="#fileContent#" <!--- addnewline="#addNewLine#" ---> fixnewline="Yes"  >
						<cfset result.isOK = true>
				<cfelse>
						<cfset result.isOK = false>
				</cfif>

				<cfreturn result>


	</cffunction>

	<cffunction name="getMailPath" access="private" returntype="struct">
		<cfargument name="password"  required = "true">
				<cfset var instanceName = "cfusion">
				<cfset var CFRoot= "">
				<cfset var instanceObj = "">
				<cfset var result= structNew()>

			<cfset CFRoot = server.coldfusion.rootDir>

			<cfset result.Path = "#CFRoot#\mail">

			<!--- WAB 2009-11-09  need to deal with servers where directories are named differently --->
			<cfif not directoryExists (result.Path)>
				<cfthrow message="Mail directory'#CFRoot#\mail' doesn't exist">
			</cfif>

			<cfset result.isOK = true>

			<cfreturn result>

	</cffunction>

	<cffunction name="reSpoolMailFiles" access="private" returntype="struct">
		<cfargument name="z"  default = "">


		<cfreturn movemailfiles(z=z)>

	</cffunction>

	<cffunction name="reSpoolLatestMailFile" access="private" returntype="struct">
		<cfargument name="z"  default = "">
		<cfreturn movemailfiles(z=z, sort = "dateLastModified desc", numberrows=1)>

	</cffunction>

	<cffunction name="reSpoolNamedMailFile" access="private" returntype="struct">
		<cfargument name="z"  default = "">
		<cfargument name="fileName"  required=true>
		<cfreturn movemailfiles(z=z, cfdirectoryFilter = filename)>

	</cffunction>

	<cffunction name="unSpoolMailFiles" access="private" returntype="struct">
		<cfargument name="z"  default = "">
		<cfreturn movemailfiles(movefrom="spool", moveTo="undelivr", z=z)>

	</cffunction>

	<cffunction name="deleteMailFiles" access="private" returntype="struct">
		<cfargument name="z"  default = "">
		<cfargument name="mailFile"  required="yes">

		<cfset createMailDirectory (z, "deleted")>

		<cfset fileList = listqualify (mailFile, "'")>
		<cfreturn movemailfiles(movefrom="undelivr", moveTo="deleted", z=z, filterSQL = "name in (#fileList#)")>

	</cffunction>

	<cffunction name="moveMailFile" access="private" returntype="struct">
		<cfargument name="z"  default = "">
		<cfargument name="moveFrom"  required="yes">
		<cfargument name="moveTo"  required="yes">
		<cfargument name="mailFile"  required="yes">

		<cfset createMailDirectory (z, moveTo)>

		<cfset fileList = listqualify (mailFile, "'")>
		<cfreturn movemailfiles(movefrom=moveFrom, moveTo=moveTo, z=z, filterSQL = "name in (#fileList#)")>

	</cffunction>

	<cffunction name="createMailDirectory" access="private" returntype="boolean">
		<cfargument name="z"  default = "">
		<cfargument name = "directory" required= "yes">

		<cfset var mailPathDetails = getMailPath (password=z)>
		<cfset var mailPath = mailPathDetails.path>
		<cfset dir = mailPath & "\" & directory>
		<cfif not directoryExists (dir )	>
			<cfdirectory action="CREATE" directory="#dir#">
		</cfif>

		<cfreturn true>
	</cffunction>


	<cffunction name="moveMailFiles" access="private" returntype="struct">
		<cfargument name="z"  default = "">   <!--- actually the cfadmin password --->
		<cfargument name = "movefrom" default = "undelivr">
		<cfargument name = "moveto" default = "spool">
		<cfargument name = "startrow" default = "1">
		<cfargument name = "numberrows" default = "0">
		<cfargument name = "sort" default = "dateLastModified asc">
		<cfargument name = "filterSQL" default = "">
		<cfargument name = "CFDirectoryFilter" default = "">

				<cfset var result = structnew()>
				<cfset var mailPath = "">

				<cftry>
					<cfset mailPathDetails = getMailPath (password=z)>

					<cfif mailPathDetails.isOK>
							<cfset mailPath = mailPathDetails.path>
							<cfdirectory action="LIST" directory="#mailPath#\#moveFrom#" name="filesToMOve" sort="#sort#" filter = "#CFDirectoryFilter#">
							<cfif filterSQL is not "">
								<cfquery name = "filesToMove" dbtype="query">
								select * from filesToMove
								where #preserveSingleQuotes(filterSQL)#
								</cfquery>
							</cfif>

							<cfset result.filesMoved = arrayNew(1)>
							<cfset endRow = iif(numberrows is 0,"filesToMove.recordCount","numberRows")>
							<cfloop query="filesToMove" startrow="#startRow#" endRow="#endRow#">
		   						<cffile action="move" source="#mailPath#\#moveFrom#\#filesToMove.name#" destination="#mailPath#\#moveTo#\#filesToMove.name#">
								<cfset arrayappend (result.filesMoved, name)>

							</cfloop>

							<cfset result.isOK = true>
					<cfelse>
								<cfset result.isOK = false>
								<cfset result.error.message = "Login Failed #z#">
					</cfif>

					<cfcatch>
						<cfset result.isOK = false>
						<cfset result.error.message = cfcatch.message>
					</cfcatch>

				</cftry>

						<cfreturn result>


	</cffunction>


	<cffunction name="filterMailFiles" access="private" returntype="struct">
		<cfargument name="z"  required = "true">
		<cfargument name="filterRegExp"  required ="true">
		<cfargument name="returnFilesLeftNames"  default ="false">

		<cfset var result = structnew()>
		<cfset var mailPath = "">

		<cfparam name="moveFrom" default = "undelivr">
		<cfparam name="moveTo" default = "filteredMail">


				<cfif filterRegExp is "">
					<cfset result.isOK = false>
					<cfset result.error = "No RegExp">
					<cfreturn result>
				</cfif>

				<cftry>
					<cfset mailPathDetails = getMailPath (password=z)>

					<cfif mailPathDetails.isOK>
							<cfset mailPath = mailPathDetails.path>
							<cfset result.filesMoved = arrayNew(1)>
							<cfdirectory action="LIST" directory="#mailPath#\#moveFrom#" name="filesToCheck">
							<cfloop query="filesToCheck">
		   						<cffile action="READ" file="#mailPath#\#moveFrom#\#filesToCheck.name#" variable="fileContent">
								<cfif reFindNoCase(filterRegExp,fileContent)>
									<cffile action="move" source="#mailPath#\#moveFrom#\#filesToCheck.name#" destination="#mailPath#\#moveTo#\#filesToCheck.name#">
									<cfset arrayappend (result.filesMoved, name)>
								</cfif>
							</cfloop>
							<cfdirectory action="LIST" directory="#mailPath#\#moveFrom#" name="filesLeft">
							<cfset result.filesLeftCount = filesLeft.recordCount>
							<cfif returnFilesLeftNames>
								<cfset result.filesLeft = filesLeft>
							</cfif>

							<cfset result.isOK = true>
					<cfelse>
								<cfset result.isOK = false>
								<cfset result.error.message = "Login Failed #z#">
					</cfif>

					<cfcatch>
						<cfset result.isOK = false>
						<cfset result.error.message = cfcatch.message>
					</cfcatch>

				</cftry>

		<cfreturn result>

	</cffunction>

<!---
	<cffunction name="getMailLog" access="remote" returntype="struct">
		<cfargument name="z"  default = "">   <!--- actually the cfadmin password --->

				<cfset var result = structnew()>
				<cfset var getdebuggingObj = instantiateAdminApi (password = z, module="debugging")>
				<cfset var debuggingObj = "">
				<cfdump var="#getdebuggingObj.obj.getLogProperty('logdirectory')#">
				<cflog

	</cffunction> --->


 	<cffunction name="listMailFiles" access="private" returntype="struct">
		<cfargument name="z"  default = "">   <!--- actually the cfadmin password --->

		<cfreturn getMailServerInfoPrivate (z=z, returnFileNames = true, returnFileContent = true)	>


	</cffunction>


 	<cffunction name="listMailAddresses" access="private" returntype="struct">
		<cfargument name="z"  default = "">   <!--- actually the cfadmin password --->

		<cfreturn getMailServerInfoPrivate (z=z, returnFileNames = true, returnFileContent = false, returnSubjectAndEmail = true)	>


	</cffunction>


	<cffunction name="instantiateAdminApi" access="private" returnType = "Struct">
		<cfargument name="password">
		<cfargument name="module" >

		<cfset var result = structNew()>
		<cfset var adminObj = createObject("component","cfide.adminapi.administrator")>
		<cfset var login =  adminObj.login(password)>
		<cfif login>
			<cfset result.isOK = true>
		   	<cfset result.Obj = createObject("component","cfide.adminapi.#module#")>
		<cfelse>
			<cfset result.isOK  =false>
		</cfif>

		<cfreturn result>

	</cffunction>

	<cffunction name="getDebugStatus" access="private" returntype="struct">
		<cfargument name="z"  default = "">   <!--- actually the cfadmin password --->

				<cfset var result = structnew()>
				<cfset var getdebuggingObj = instantiateAdminApi (password = z, module="debugging")>
				<cfset var debuggingObj = "">

						<cftry>
						   <!---  Instantiate the debugging object. --->
							<cfif getdebuggingObj.isOK>
								<cfset debuggingObj = getdebuggingObj.obj>
   								<cfset result.enableDebug =debuggingObj.getDebugProperty('enableDebug')>
								<cfset result.isOK = true>
							<cfelse>
								<cfset result.isOK = false>
								<cfset result.error = "Could not Log In">
							</cfif>

							<cfcatch>
								<cfset result.isOK = false>
								<cfset result.error = cfcatch.message>
							</cfcatch>

						</cftry>
						<cfreturn result>

<!--- 								myObj.setDebugProperty(propertyName="enableDebug", propertyValue="true");
 --->
	</cffunction>


	<cffunction name="setDebugStatus" access="private" returntype="struct">
		<cfargument name="z"  default = "">   <!--- actually the cfadmin password --->
		<cfargument name="debugValue" >
		<cfargument name="IPAddress" >

				<cfset var result = structnew()>
				<cfset var getdebuggingObj = instantiateAdminApi (password = z, module="debugging")>
				<cfset var debuggingObj = "">

						<cftry>
							<cfif getdebuggingObj.isOK>
							   	<cfset debuggingObj = getdebuggingObj.obj>
   								<cfset debuggingObj.setDebugProperty(propertyName="enableDebug", propertyValue=debugValue) >
								<cfif IPAddress is not "">
	 								<cfset debuggingObj.setIP(IPAddress) >
									<cfparam name="server.ShowErrorsToTheseIPAddressesRegExp" default = "">
									<cfset ipAddressRegExp = replace(IPAddress,".","\.","ALL")>
									<cfif listFindNoCase(server.ShowErrorsToTheseIPAddressesRegExp, ipAddressRegExp) is 0 >
										<cfset server.ShowErrorsToTheseIPAddressesRegExp = listappend(server.ShowErrorsToTheseIPAddressesRegExp, ipAddressRegExp,"|")>
									</cfif>
								</cfif>
   								<cfset result.enableDebug = debuggingObj.getDebugProperty('enableDebug') >

								<cfset result.isOK = true>
							<cfelse>
								<cfset result.isOK = false>
								<cfset result.error.message = "Login Failed #z#">
							</cfif>

							<cfcatch>
								<cfset result.isOK = false>
								<cfset result.error.message = cfcatch.message>
								<cfset result.error.cfcatch = cfcatch>
							</cfcatch>

						</cftry>

						<cfreturn result>

<!--- 								myObj.setDebugProperty(propertyName="enableDebug", propertyValue="true");
 --->
	</cffunction>

	<cffunction name="removeDebugIPAddresses" access="private" returntype="struct">
		<cfargument name="z"  default = "">   <!--- actually the cfadmin password --->
		<cfargument name="excludeIPAddressRegExp"  default = "">
		<cfargument name="excludeIPAddressRegExp2"  default = "">

		<cfset var result = structnew()>
		<cfset var getdebuggingObj = instantiateAdminApi (password = z, module="debugging")>
		<cfset var debuggingObj = "">


								<cfset result.excludeIPAddressRegExp = excludeIPAddressRegExp>
								<cfset result.excludeIPAddressRegExp2 = excludeIPAddressRegExp2>

								<cfset excludeIPAddressRegExp = listappend (excludeIPAddressRegExp,excludeIPAddressRegExp2,"|")>

								<cfset result.finalexcludeIPAddressRegExp = excludeIPAddressRegExp>

								<cfset result.removed = "">
								<cfset result.kept = "">

						<cftry>
							<cfif getdebuggingObj.isOK>
							   	<cfset debuggingObj = getdebuggingObj.obj>
								<cfset iplist = debuggingObj.getIPList()>

								<cfloop index="thisIP" list="#iplist#">
									<cfif not refindNoCase(excludeIPAddressRegExp,thisIP)>
										<cfset debuggingObj.deleteIP(thisIP)>
										<cfset result.removed = listappend(result.removed,thisIP)>
									<cfelse>
									<cfset result.kept = listappend(result.kept,thisIP)>
									</cfif>

								</cfloop>



								<cfset result.isOK = true>
							<cfelse>
								<cfset result.isOK = false>
								<cfset result.error.message = "Login Failed #z#">
							</cfif>

							<cfcatch>
								<cfset result.isOK = false>
								<cfset result.error.message = cfcatch.message>
								<cfset result.error.cfcatch = cfcatch>
							</cfcatch>

						</cftry>


						<cfreturn result>



	</cffunction>

	<cffunction name="clearTemplateCache" access="private" returntype="struct">
		<cfargument name="z"  default = "">   <!--- actually the cfadmin password --->

				<cfset var result = structnew()>
			   <!---  Instantiate the debugging object. --->
				<cfset var getruntimeObj = instantiateAdminApi (password = z, module="runtime")>
				<cfset var runtimeObj = "">
						<cftry>
							<cfif getruntimeObj.isOK>
							   	<cfset runtimeObj = getruntimeObj.obj>
   								<cfset runtimeObj.clearTrustedCache() >
								<cfset result.isOK = true>
							<cfelse>
								<cfset result.isOK = false>
								<cfset result.error.message = "Login Failed #z#">
							</cfif>

							<cfcatch>
								<cfset result.isOK = false>
								<cfset result.error.message = cfcatch.message>
							</cfcatch>

						</cftry>
						<cfreturn result>

	</cffunction>

	<!--- recurses through a path to make sure that all the directories in the path are created --->
	<cffunction name="createDirectory" access="private" returntype="boolean">
		<cfargument name="path" required=true >

		<cfset path =  rereplace(Path,"[\\/]\Z","")>   <!--- remove trailing / just in case --->
		<cfif directoryExists (path)>
			<cfreturn true>
		<cfelse>
			<cfset re = "[\\/][^\/\\]*\Z">
			<cfset x = reReplaceNoCase (path,re,"")>
			<cfset createDirectory (x)>
			<cfdirectory action="CREATE" directory="#path#">
		</cfif>

		<cfreturn true>
	</cffunction>

	<cffunction name="writeTemplate" access="private" returntype="struct">
		<cfargument name="z"  default = "">   <!--- actually the cfadmin password --->
		<cfargument name="template" required=true >
		<cfargument name="templateContent" required=true >
		<cfargument name="relativeTo" required=true >
		<cfargument name="createbackup" default="false">

				<cfset var result = structnew()>
				<cfset var rootpath=getRootPath(z,relativeTo)>
				<cfset var addNewLine = "No">
				<cfset var getFile = "">

				<cfset result.template = template>

				<!--- I had problems getting files to be exactly the right size, appears as if the last character is a CR then we need to get cffile to add a newline at the end of the file (otherwise we end up 2 bytes short) --->
				<cfif asc(right(templateContent,1)) is 10>
					<cfset  addNewLine = "Yes">
				</cfif>

			<cftry>
				<cfset filePath = "#rootpath#\#template#">

					<cfset regExpObj = createObject("component","relay.com.regExp")>
					<cfset fileDetails = regExpObj.getFileDetailsFromPath(filePath)>

				<cfif createBackup and fileExists(filepath)>
					<cfset timeStamp = dateFormat(now(),"yyyymmdd") & timeformat(Now(),"HHmmss")>
					<cfset newPath = replaceNoCase(fileDetails.path,"web","webbackup","ONCE")>
					<cfset createDirectory (newPath)>
					<cfset newFileName = "#fileDetails.name##timeStamp#.#fileDetails.extension#">
					<cfset newFilePath = "#newPath#\#newFileName#">
					<cffile action="COPY" source="#filePath#" destination="#newFilePath#">
					<cfdirectory action="LIST" directory="#newPath#" name="getFile" filter="#newFileName#">
					<cfset result.backupfile = newFileName>
					<cfset result.oldLen = getFile.size>
				<cfelse>
					<cfset result.backupfile = "">
					<cfset result.oldLen = 0>

				</cfif>

				<!--- WAB 2008-07-09 make sure that path exists --->
				<cfset createDirectory (fileDetails.path)>

				<cffile action="WRITE" file="#filePath#" output="#templateContent#" addnewline="#addNewLine#" fixnewline="Yes" >  <!--- charset="utf-8" --->
				<cfdirectory action="LIST" directory="#filedetails.Path#" name="getFile" filter="#fileDetails.fullfilename#">
				<cfset result.isOK = true>
				<cfset result.len = getFile.size>
				<cfset result.fileQuery = getFile>

				<cfcatch>
					<cfset result.isOK = false>
					<cfset result.error.message = cfcatch.message>
				</cfcatch>

			</cftry>

		<cfreturn result>
	</cffunction>

	<cffunction name="deleteTemplate" access="private" returntype="struct">
		<cfargument name="z"  default = "">   <!--- actually the cfadmin password --->
		<cfargument name="template" required=true >
		<cfargument name="relativeTo" required=true >
		<cfargument name="createBackUp" default =  "true">

				<cfset var result = structnew()>
				<cfset var rootpath=getRootPath(z,relativeTo)>
				<cfset var getFile = "">

				<cfset result.template = template>


			<cftry>
				<cfset filePath = "#rootpath#\#template#">
				<cfif fileExists(filepath)>

						<cfif createBackup>
							<cfset regExpObj = createObject("component","relay.com.regExp")>
							<cfset fileDetails = regExpObj.getFileDetailsFromPath(filePath)>
							<cfset timeStamp = dateFormat(now(),"yyyymmdd") & timeformat(Now(),"HHmmss")>
							<cfset newPath = replaceNoCase(fileDetails.path,"web","webbackup","ONCE")>
							<cfset createDirectory (newPath)>
							<cfset newFileName = "#fileDetails.name##timeStamp#.#fileDetails.extension#">
							<cfset newFilePath = "#newPath#\#newFileName#">
							<cffile action="MOVE" source="#filePath#" destination="#newFilePath#">
							<cfdirectory action="LIST" directory="#newPath#" name="getFile" filter="#newFileName#">
							<cfset result.backupfile = newFileName>
							<cfset result.oldLen = getFile.size>

						<cfelse>
							<cffile action="delete" file="#filePath#" >

						</cfif>


						<cfset result.isOK = true>

				<cfelse>
						<cfset result.isOK = false>
				</cfif>


					<cfcatch>
						<cfset result.isOK = false>
						<cfset result.error.message = cfcatch.message>
					</cfcatch>

			</cftry>

		<cfreturn result>
	</cffunction>


	<cffunction name="restartCF" access="private" returntype="struct">
		<cfargument name="z"  default = "">   <!--- actually the cfadmin password --->

				<cfset var result = structnew()>
			   <!---  Instantiate the debugging object. --->
				<cfset var getInstanceObj = instantiateAdminApi (password = z, module="serverInstance")>
				<cfset var instanceObj = "">

						<cftry>
							<cfif getInstanceObj.isOK>
							   	<cfset instanceObj = getInstanceObj.obj>
								<cfset instanceObj.restartInstance("cfusion") >
								<cfset result.isOK = true>
							<cfelse>
								<cfset result.isOK = false>
								<cfset result.error.message = "Login Failed #z#">
							</cfif>

							<cfcatch>
								<cfset result.isOK = false>
								<cfset result.error.message = cfcatch.message>
							</cfcatch>

						</cftry>
						<cfreturn result>


	</cffunction>

	<cffunction name="readTemplate" access="private" returntype="struct">
		<cfargument name="z" required=true >
		<cfargument name="template" required=true >
		<cfargument name="RelativeTo" required=true >  <!--- Relay,CustomTags,BestCode --->


				<cfset var result= structnew()>
				<cfset var rootpath=getRootPath(z,relativeTo)>
				<cfset var templateContent= "">
				<cfset result.template = template>

			<cftry>
				<cffile action="read" file="#rootPath#\#template#" variable="templateContent" ><!--- charset="utf-8" --->

				<cfset result.isOK = true>
				<cfset result.content = templateContent>
				<cfset result.len = len(templateContent)>

				<cfcatch>
					<cfset result.isOK = false>
					<cfset result.error.message = cfcatch.message>
				</cfcatch>

			</cftry>

		<cfreturn result>
	</cffunction>


	<cffunction name="readTemplateV2" access="private" returntype="struct">
		<cfargument name="z" required=true >
		<cfargument name="template" required=true >
		<cfargument name="RelativeTo" required=true >  <!--- Relay,CustomTags,BestCode --->
		<cfargument name="FilesLocation" required=true >

				<cfset var result= structnew()>
				<cfset var templateContent= "">
				<cfset var rootpath="">

				<cfset result.template = template>
			<cftry>

				<cfset rootpath=getRootPath(z,relativeTo,FilesLocation)>
				<cfset regExpObj = createObject("component","relay.com.regExp")>
				<cfset fileDetails = regExpObj.getFileDetailsFromPath("#rootPath#\#template#")>

				<cfdirectory action="LIST" directory="#filedetails.Path#" FILTER="#fileDetails.fullfilename#" NAME="DIR">

				<cffile action="read" file="#rootPath#\#template#" variable="templateContent" ><!--- charset="utf-8" --->

				<cfset result.isOK = true>
				<cfset result.content = templateContent>
				<cfset result.len = len(templateContent)>
				<cfset result.lenFileSystem = dir.size>
				<cfset result.directoryEntry = dir>   <!--- 2009/12/??  WAB Return the whole directory --->

				<cfcatch>
					<cfset result.isOK = false>
					<cfset result.error.message = cfcatch.message>
				</cfcatch>

			</cftry>

		<cfreturn result>
	</cffunction>


	<cffunction name="getTemplateInfo" access="private" returntype="struct">
		<cfargument name="z" required=true >
		<cfargument name="template" required=true >
		<cfargument name="RelativeTo" required=true >  <!--- Relay,CustomTags --->
		<cfargument name="FilesLocation" default="" >

				<cfset var result= structnew()>
				<cfset var rootpath=getRootPath(z,relativeTo,FilesLocation)>
				<cfset var thefile = listlast("#rootPath#\#template#","\/")>
				<cfset var directory = replaceNoCase("#rootPath#\#template#",thefile,"")>
				<cfset var query = "">
				<cfset result.template = template>

			<cftry>
				<cfdirectory action="LIST" directory="#directory#" filter ="#thefile#" name="query" >

				<cfset result.isOK = true>
				<cfset result.query = query>
				<cfset result.directory = #directory#>
				<cfset result.thefile = thefile>

				<cfcatch>
					<cfset result.isOK = false>
					<cfset result.error.message = cfcatch.message>
					<cfrethrow>
				</cfcatch>

			</cftry>

		<cfreturn result>
	</cffunction>


<!---
	<cffunction name="getDirectoryInfo" access="remote" returntype="struct">
		<cfargument name="z" required=true >

		<!--- Either pass in a single RelativeTo / directory  or a query of all the RelativeTo / directories / files--->
		<cfargument name="directory" default = "">
		<cfargument name="RelativeTo" default = "" >  <!--- Relay,CustomTags,BestCode --->
		<cfargument name="recurse" default = "false">
		<cfargument name="ModifiedSince" default = "">

		<cfargument name="fileAndDirectoryQuery" type="query" default = "#queryNew('dummy')#">  <!--- if relative to bestcode then need to set relativeto = bestcode in  --->



				<cfset var result= structnew()>
				<cfset var rootpath="">
				<cfset var resultfiles = queryNew('dummy')>
				<cfset var filter = "*.cf*|*.htm|*.xml|*.txt|*.js|*.css">
				<cfset var directoryPATH = "">

			<cfif fileAndDirectoryQuery.recordCount is 0 and (directory is "" and relativeTo is "")>
				<cfoutput>requires relativeto and directory or q query </cfoutput>
				<CFABORT>
			</cfif>


			<cftry>

				<cfif fileAndDirectoryQuery.recordcount is 0>
					<!--- getting info for a whole directory (perhaps with recursion) --->
					<cfset rootpath=getRootPath(z,relativeTo)>
					<cfset directoryPATH = "#rootPath#/#directory#">

					<cfdirectory action="LIST" directory="#directorypath#" name="resultfiles" filter="#filter#" recurse="#recurse#">

					<!--- exclude web-inf and cfide directories --->
					<cfquery dbtype="query"	name="resultfiles">
					select * from resultfiles
					where
					directory <> '#rootPath#/web-inf'
					and directory not like '#rootPath#/cfide%'
					</cfquery>

					<cfif modifiedSince is not "">
						<cfquery dbtype="query"	name="resultfiles">
						select * from resultfiles where dateLastModified > #modifiedSince#
						</cfquery>
					</cfif>

						<cfquery dbtype="query"	name="resultfiles">
						select *,
						'#lcase(replacenocase(relativeTo, "bestcode", ""))#' as relativeTo <!---  get rid of bestcode if there (bit of a hack really)--->
						from resultfiles
						</cfquery>

						<cfloop query="resultfiles">
							<!--- remove the rootpath from the directory --->
							<cfset querySetCell(resultfiles,"directory",replace (lcase(replacenocase(resultfiles.directory,rootPath ,"")),'\','/','ALL'),currentrow)>
						</cfloop>




				<cfelse>
					<!--- getting info for a individual directories (and maybe files ) --->
					<cfquery dbtype="query"	name="dirs">
					select distinct relativeTo, directory from fileAndDirectoryQuery
					</cfquery>

					<cfloop query = "dirs">

						<cfquery dbtype="query"	name="filesInThisDirectory">
						select name from fileAndDirectoryQuery where directory = '#dirs.directory#' and relativeTo = '#dirs.relativeTo#'
						</cfquery>


						<cfset rootpath=getRootPath(z,relativeTo &dirs.relativeTo)>
						<cfset directoryPATH = "#rootPath#/#dirs.directory#">
						<cfset fileList = valueList(filesInThisDirectory.name,"|")>

						<cfif fileList is "">
							<cfset thisFilter = filter><!--- no filenames specified so bring back all files --->
						<cfelse>
							<cfset thisFilter = fileList> <!--- filenames specified so use as filter --->
						</cfif>


						<cfdirectory action="LIST" directory="#directorypath#" name="tempfiles" filter="#thisfilter#" recurse="false">

							<cfloop query="tempfiles">
								<!--- remove the rootpath from the directory --->
								<cfset querySetCell(tempfiles,"directory",replace (lcase(replacenocase(tempfiles.directory,rootPath ,"")),'\','/','ALL'),currentrow)>
							</cfloop>

							<!--- add these results to previous ones --->
							<cfquery dbtype="query"	name="resultfiles">
							<cfif resultfiles.recordcount is not 0>
								select * from resultfiles
								union
							</cfif>
							select * ,
								'#lcase(replacenocase(dirs.relativeTo, "bestcode", ""))#' as relativeTo
								from
								tempfiles
							</cfquery>

					</cfloop>


				</cfif>


				<cfset queryaddcolumn(resultfiles,"Path",arrayNew(1))>
				<cfloop query="resultfiles">
					<!--- make all lowercase, change \ to / and remove leading / add a path--->
					<cfset querySetCell(resultfiles,"name",lcase(resultfiles.name),currentrow)>
					<cfset querySetCell(resultfiles,"path",resultfiles.relativeTo & '' & resultfiles.directory & '/' & resultfiles.name,currentrow)>

				</cfloop>

 				<cfset result.isOK = true>
				<cfset result.query = resultfiles>

				<cfcatch>
					<cfset result.isOK = false>
					<cfset result.error.message = cfcatch.message>
					<cfrethrow>
				</cfcatch>

			</cftry>

		<cfreturn result>
	</cffunction>
	 --->

	<cffunction name="getDirectoryInfoV2" access="private" returntype="struct">
		<cfargument name="z" required=true >

		<!--- Either pass in a single RelativeTo / directory  or a query of all the RelativeTo / directories / files--->
		<cfargument name="directory" default = "">
		<cfargument name="filesLocation"  default="">
		<cfargument name="RelativeTo" default = "" >  <!--- Relay,CustomTags,BestCode --->
		<cfargument name="recurse" default = "false">
		<cfargument name="ModifiedSince" default = "">

		<cfargument name="fileAndDirectoryQuery" type="query" default = "#queryNew('dummy')#">  <!--- if relative to bestcode then need to set relativeto = bestcode in  --->



				<cfset var result= structnew()>
				<cfset var rootpath="">
				<cfset var resultfiles = queryNew('dummy')>
				<cfset var filter = "*.cf*|*.htm|*.xml|*.txt|*.js|*.css|*.SQL">
				<cfset var directoryPATH = "">

			<cfif fileAndDirectoryQuery.recordCount is 0 and (directory is "" and relativeTo is "")>
				<cfoutput>requires relativeto and directory or q query </cfoutput>
				<CFABORT>
			</cfif>


			<cftry>

				<cfif fileAndDirectoryQuery.recordcount is 0>
					<!--- getting info for a whole directory (perhaps with recursion) --->
					<cfset rootpath=getRootPath(z,relativeTo,FilesLocation)>
					<cfset directoryPATH = "#rootPath#/#directory#">


					<cfdirectory action="LIST" directory="#directorypath#" name="resultfiles" filter="#filter#" recurse="#recurse#">

					<!--- exclude web-inf and cfide directories --->
					<cfquery dbtype="query"	name="resultfiles">
					select * from resultfiles
					where
					directory <> '#rootPath#/web-inf'
					and directory not like '#rootPath#/cfide%'
					</cfquery>

					<cfif modifiedSince is not "">
						<cfquery dbtype="query"	name="resultfiles">
						select * from resultfiles where dateLastModified > #createodbcdatetime(modifiedSince)#
						</cfquery>
					</cfif>

						<cfquery dbtype="query"	name="resultfiles">
						select *,
						'#lcase(replacenocase(relativeTo, "bestcode", ""))#' as relativeTo <!---  get rid of bestcode if there (bit of a hack really)--->
						from resultfiles
						</cfquery>

								<cfloop query="resultfiles">
								<!--- remove the rootpath from the directory --->
								<cfset querySetCell(resultfiles,"directory",replace (lcase(replacenocase(replacenocase(resultfiles.directory,rootPath &"\",""),rootPath ,"")),'\','/','ALL'),currentrow)>
							</cfloop>



				<cfelse>
					<!--- getting info for a individual directories (and maybe files ) --->
					<cfquery dbtype="query"	name="dirs">
					select distinct relativeTo, directory from fileAndDirectoryQuery
					</cfquery>

					<cfloop query = "dirs">

						<cfquery dbtype="query"	name="filesInThisDirectory">
						select name from fileAndDirectoryQuery where directory = '#dirs.directory#' and relativeTo = '#dirs.relativeTo#'
						</cfquery>


						<cfset rootpath=getRootPath(z,relativeTo &dirs.relativeTo,FilesLocation)>
						<cfset directoryPATH = "#rootPath#/#dirs.directory#">
						<cfset fileList = valueList(filesInThisDirectory.name,"|")>

						<!--- WAb added recurse --->
						<cfif fileList is "" or fileList is "*.*">
							<cfset thisFilter = filter><!--- no filenames specified so bring back all files --->
							<cfset thisRecurse = recurse>
							<cfset result.filter = thisFilter >
						<cfelse>
							<cfset thisFilter = fileList> <!--- filenames specified so use as filter --->
							<cfset thisRecurse = false>
						</cfif>

						<cfdirectory action="LIST" directory="#directorypath#" name="tempfiles" filter="#thisfilter#" recurse="#thisRecurse#">

							<cfloop query="tempfiles">
								<!--- remove the rootpath from the directory --->
								<cfset querySetCell(tempfiles,"directory",replace (lcase(replacenocase(replacenocase(tempfiles.directory,rootPath &"\",""),rootPath ,"")),'\','/','ALL'),currentrow)>
							</cfloop>

							<!--- add these results to previous ones --->
							<cfquery dbtype="query"	name="resultfiles">
							<cfif resultfiles.recordcount is not 0>
								select * from resultfiles
								union
							</cfif>
							select * ,
								'#lcase(replacenocase(dirs.relativeTo, "bestcode", ""))#' as relativeTo
								from
								tempfiles
							</cfquery>

					</cfloop>


				</cfif>


				<cfset queryaddcolumn(resultfiles,"Path",arrayNew(1))>
				<cfloop query="resultfiles">
					<!--- make all lowercase, change \ to / and remove leading / add a path--->
					<cfset querySetCell(resultfiles,"name",lcase(resultfiles.name),currentrow)>
					<cfset querySetCell(resultfiles,"path",resultfiles.relativeTo & iif(resultfiles.directory is not "",de("/"),de("")) & resultfiles.directory & '/' & resultfiles.name,currentrow)>

				</cfloop>
				<cfset result.directoryPATH = directoryPATH>
 				<cfset result.isOK = true>
				<cfset result.query = resultfiles>

				<cfcatch>
					<cfset result.isOK = false>
					<cfset result.error.message = cfcatch.message>
					<cfrethrow>
				</cfcatch>

			</cftry>

		<cfreturn result>
	</cffunction>



<!--- 2009-01-12 WAB added support for userfiles path and serverRoot --->
	<cffunction name="getRootPath" access="private" returntype="string">
		<cfargument name="z" required=true >
		<cfargument name="RelativeTo" required=true >
		<cfargument name="FilesLocation" default="" >		<!--- WAB 2008-10-02  added to allow for release files being in any directory - details being stored in the admin db modify so that RelativeTo can just be a path such as d:\relaywareversions\v6 --->

			<cfset var RootPath = "">
			<cfset var struct = "">
			<cfset var drive = "">
			<CFSET VAR TAGarray = "">
			<cfset var extensionsObj = "">

			<!--- 2009-01-12 WAB modifed for instances when we don't have the password.  Just ends up guessing the relay path (which is usually right, but if it becomes an issue we can probably work it out some other way). Doesn't do CustomTags --->
			<cftry>
				<cfset extensionsObj = instantiateAdminApi (password = z, module="extensions").obj>
				<cfset relayMappingStruct = extensionsObj.getMappings(mapName='/relay')>
				<cfset relayMapping = relayMappingStruct['/relay']>
				<cfcatch>
					<cfset relayMapping = "d:\web">
				</cfcatch>
			</cftry>


			<cfif FilesLocation is not  "">
				<cfset rootpath = FilesLocation & "\" & RelativeTo>
			<cfelse>
				<cfswitch expression="#listfirst(RelativeTo,"-")#"><!--- list first allows for userfiles-appname --->
					<cfcase value="relay">
						<cfset RootPath=relayMapping>
					</cfcase>
					<cfcase value="customTags">
						<cfset TAGarray = extensionsObj.getcustomtagpaths()>

						<cfloop index="i" from = "1" to="#arraylen(TAGarray)#">
							<cfif listlast(TagArray[i],"\") is "customtags" >
								<cfset RootPath = TagArray[i]>
							</cfif>
						</cfloop>
					</cfcase>
					<cfcase value="userfiles">
						<cfset appname = listRest(RelativeTo,"-")>
						<cftry>
							<cfapplication name="#appname#" sessionManagement="yes">
								<cfset rootPath = application.userfilesAbsolutePath & "/content">
							<cfcatch>
							</cfcatch>
						</cftry>
					</cfcase>
					<cfcase value="serverRoot">
						<cfset rootPath = server.coldfusion.rootDir>
					</cfcase>
					</cfswitch>
			</cfif>

		<cfreturn RootPath>
	</cffunction>

	<cffunction name="getPhraseFileList" access="private" returntype="struct">
		<cfargument name="z" required=true >
		<cfargument name="relativeTo" required=true>

			<cfset var result = 	getDirectoryInfo (z=z,relativeto=relativeto,directory="translation/loadphrases")>

		<cfreturn result>


	</cffunction>

	<cffunction name="loadPhrases" access="private" returntype="struct">
		<cfargument name="appnames" type="string" required = true>
		<cfargument name="phraseFileName" type="string" default="">
		<cfargument name="phraseFileContent" type="string" default="">

		<cfset var result = structNew()>


		<cfif phraseFileName is "" and phraseFileContent is "">
			<cfset result.message = "phraseFileName  or  phraseFileContent required ">
			<cfset result.isOK = false>
			<cfreturn result>
		</cfif>



		<cfloop list="#appnames#"	index="appname">
			<cfapplication name="#appname#" sessionManagement="yes">

			<CFQUERY NAME="TheTime" DATASOURCE="#application.SiteDataSource#">
				SELECT GETDATE() AS Now
			</CFQUERY>

			<cfset request.requestTime = TheTime.Now> <!--- createodbcdatetime(TheTime.Now)> --->

			<cfset thisResult = application.com.relayTranslations.addPhrasesFromLoadFile(phraseFileName = phraseFileName, phraseFileContent = phraseFileContent, addMode = true, updateMode = "ignore")>
<!--- 			<cfset structDelete (thisResult,"phrases")> --->
			<cfset result[appname] = thisResult>

		</cfloop>


		<cfreturn result>

	</cffunction>


	<!--- 2008-05-28 GCC first attempt to create function set for reporting inconsistencies between del tables and their counterparts
	 automatically creates missing cols in the del table and extends shortened fields in the del table where the data type is the same
	 left to humans to resolve:
	 	column ordering issues (very hard to correct using code - we should try to move away from our dependency on column ordering for del triggers)
	 	type mismatches as data could be lost scripting these
	 --->
	<cffunction name="createDelTablesReportStruct" access="private" returntype="struct">
		<cfargument name="mode" default="report" required="no">
		<cfargument name="appnames" required="yes" hint="only pass in one at a time - not a list. The name is generic for doSiteActions.">

		<cfscript>
			var qCreateDelTablesReportQuery="";
			var qCreateMismatchedColsQuery="";
			var resolveAndReportResult="";
			var resultStruct=structNew();
			var selfCall="";
		</cfscript>

		<cfapplication name="#arguments.appnames#" sessionManagement="yes">

		<cfquery name="qCreateDelTablesReportQuery" datasource="#application.sitedatasource#">
			declare @table1 table (myident int identity (1,1), table_name varchar(100), column_name varchar(100),Max_Length varchar(50),Data_Type varchar(50),is_Nullable varchar(50))
			declare @table2 table (myident int identity (1,1), table_name varchar(100), column_name varchar(100),Max_Length varchar(50),Data_Type varchar(50),is_Nullable varchar(50))

			insert into  @table1
			(table_name, column_name, Max_length,Data_Type,is_nullable)
			select
				table_name,
				column_name,
				Max_Length = case when Character_Maximum_Length is null then ' ' else cast(Character_Maximum_Length as varchar(50)) end,
				Data_Type,
				is_nullable
				from INFORMATION_SCHEMA.columns with(nolock)
				where table_name in (select name from sysobjects with (nolock) where name in (select left(name,len(name)-3) as delname from sysobjects with (nolock) where type= 'u' and name like '%del'))
				order by table_name,column_name

			insert into  @table2
			(table_name, column_name, Max_length,Data_Type,is_nullable)
			select
				table_name,
				column_name,
				Max_Length = case when Character_Maximum_Length is null then ' ' else cast(Character_Maximum_Length as varchar(50)) end,
				Data_Type,
				is_nullable
				from INFORMATION_SCHEMA.columns with(nolock)
				where table_name in (select name from sysobjects with (nolock) where type= 'u' and name like '%del')
				order by table_name,column_name

			select
				t1.table_name,
				t1.column_name,
				reasonID =
				case when t2.table_name + t2.column_name is null then 1 -- missing col
				when t1.Data_Type <> t2.Data_Type  then 2 -- type mismatch
				when t1.Max_Length > t2.Max_Length then 3 --length of del column is too small
				when t1.is_nullable = 'YES' and t2.is_nullable = 'No' then 4 --nullable mismatch
				else 5 --other
				end,
				t1.Data_Type,
				t2.Data_Type as Del_Data_Type,
				t1.Max_Length,
				t2.Max_Length as Del_Max_Length,
				t1.is_nullable,
				t2.is_nullable as Del_is_nullable
				from
				@table1 t1 left join @table2 t2
				on t1.table_name + 'del' =t2.table_name
				and t1.column_name = t2.column_name
				where t1.table_name + t1.column_name <> left(t2.table_name,len(t2.table_name)-3) + t2.column_name or t2.table_name + t2.column_name is null
				or t1.Max_Length > t2.Max_Length or t2.Max_Length is null
				or t1.Data_Type <> t2.Data_Type or t2.Data_Type is null
				or t1.is_nullable = 'YES' and t2.is_nullable = 'No'
				order by t1.table_name,reasonID, t1.column_name
		</cfquery>

		<cfquery name="qCreateMismatchedColsQuery" datasource="#application.sitedatasource#">
			declare @table1 table (myident int identity (1,1), table_name varchar(100), column_name varchar(100),Max_Length varchar(50),Data_Type varchar(50),is_Nullable varchar(50))
			declare @table2 table (myident int identity (1,1), table_name varchar(100), column_name varchar(100),Max_Length varchar(50),Data_Type varchar(50),is_Nullable varchar(50))

			insert into  @table1
			(table_name, column_name, Max_length,Data_Type,is_nullable)
			select
				table_name,
				column_name,
				Max_Length = case when Character_Maximum_Length is null then ' ' else cast(Character_Maximum_Length as varchar(50)) end,
				Data_Type,
				is_nullable
				from INFORMATION_SCHEMA.columns with(nolock)
				where table_name in (select name from sysobjects with (nolock) where name in (select left(name,len(name)-3) as delname from sysobjects with (nolock) where type= 'u' and name like '%del'))
				order by table_name,column_name

			insert into  @table2
			(table_name, column_name, Max_length,Data_Type,is_nullable)
			select
				table_name,
				column_name,
				Max_Length = case when Character_Maximum_Length is null then ' ' else cast(Character_Maximum_Length as varchar(50)) end,
				Data_Type,
				is_nullable
				from INFORMATION_SCHEMA.columns with(nolock)
				where table_name in (select name from sysobjects with (nolock) where type= 'u' and name like '%del')
				order by table_name,column_name

			select
				top 1
				t1.table_name, t2.table_name as Del_Table_Name, reason = 'Columns are incorrectly ordered: There could be others but you need to fix these first. If they look right an alphabetically ordered previous del table may have extra columns.'
				from
				@table1 t1 inner join @table2 t2
				on t1.table_name + 'del' =t2.table_name
				and t1.column_name = t2.column_name
				where t1.myident <> t2.myident and t2.myident is not null
				order by t1.table_name
		</cfquery>

		<!--- if in resolve mode and there are issues to resolve --->
		<cfif arguments.mode eq "resolveAndReport" and qCreateDelTablesReportQuery.recordcount neq 0>
			<!--- call function to fix problems found above --->
			<cfset resolveAndReportResult = resolveDelTableInconsistencies(delTablesReportQuery=qCreateDelTablesReportQuery,appname=arguments.appnames)>
			<cfset resultStruct.AlterQueryString = resolveAndReportResult>
			<!--- call self to rerun in report mode --->
			<cfset selfCall = createDelTablesReportStruct(mode='report',appnames=arguments.appnames)>
			<cfset resultStruct.report = selfCall.report>
			<cfset resultStruct.MismatchedCols = selfCall.MismatchedCols>
		<cfelse>
			<!--- gather results from report mode --->
			<cfset resultStruct.report = qCreateDelTablesReportQuery>
			<cfset resultStruct.MismatchedCols = qCreateMismatchedColsQuery>
		</cfif>
		<cfset resultStruct.Appname = arguments.appnames>

		<cfreturn resultStruct>
	</cffunction>

	<cffunction name="resolveDelTableInconsistencies" access="private" return="struct">
		<cfargument name="delTablesReportQuery" type="query" required="true">
		<cfargument name="appname" required="yes">

		<cfscript>
			var qGetMissingDelColumns="";
			var executeString="";
			var result=structnew();
		</cfscript>

		<cfquery name="qGetMissingDelColumns" dbtype="query">
			Select
				reasonID,
				table_name,
				column_name,
				Data_Type,
				Max_Length
			from delTablesReportQuery
			where reasonID in (1,3)
		</cfquery>

		<cfif qGetMissingDelColumns.recordcount gt 0>
			<cfset executeString = "">
			<!--- build alter statements - create as null with no defaults is fine for del tables --->
			<cfloop query="qGetMissingDelColumns">
				<cfswitch expression="#reasonID#">
					<!--- missing cols --->
					<cfcase value="1">
						<cfif len(Max_Length)>
							<cfset executeString = executeString & " ALTER TABLE " & table_name & "Del add " & column_name & " " & data_type & "(" & max_length & ") NULL">
						<cfelse>
							<cfset executeString = executeString & " ALTER TABLE " & table_name & "Del add " & column_name & " " & data_type & " NULL">
						</cfif>
					</cfcase>
					<!--- too short but same type - 2 rules out type mismatches --->
					<cfcase value="3">
						<cfset executeString = executeString & " ALTER TABLE " & table_name & "Del ALTER COLUMN " & column_name & " " & data_type & "(" & max_length & ") NULL">
					</cfcase>
				</cfswitch>
			</cfloop>
			<cfset result.queryString=executeString>
			<cfset result.result = "None">
			<cfif executeString neq "">
				<cfset result.result=runSQL(appname=arguments.appname,sql=executeString)>
			</cfif>
		</cfif>

		<cfreturn result>
	</cffunction>

<cffunction name="GetScheduledTasks" access="private" hint="Returns an array of structures with data for all scheduled tasks" output="false" returntype="struct">
	<cfargument name="z"  default = "">
      <cfset var factory = "">
      <cfset var result=structNew()>
      <cfset var resultqry ="">

      <cfobject type="JAVA" action="Create" name="factory" class="coldfusion.server.ServiceFactory">


      <cfset result.allTasks = arrayOfStructuresToQuery(factory.CronService.listAll())>
    <cfset result.isOK = true>

      <cfreturn result>
   </cffunction>

<!--- This code from Ben Forta / http://www.forta.com/blog/index.cfm/28-08-2006/GetScheduledTasks-Function-Returns-Scheduled-Task-List--->
<cffunction name="GetScheduledTasks2" returntype="struct" access="remote" output="yes">
	<cfargument name="z"  default = "">

   <!--- Local vars --->
	<cfset var result= structnew()>
   <cfset var tasks="">

	<cfset var resultQry=QueryNew('path,file,resolveurl,url,publish,password,operation,username,interval,start_date,http_port,task,http_proxy_port,proxy_server,disabled,start_time,request_time_out,end_time,paused')>
   <cfset var OuterStart="">
   <cfset var InnerStart="">
   <cfset var qRETest="">
   <cfset var qRETestinner="">
   <cfset var ScheduleItem="">

   <!--- This call is undocumented --->
   <cfsavecontent variable="tasks">
      <cfschedule action="run" task="__list">
   </cfsavecontent>

   <!--- The start for each schedule entry --->
   <cfset OuterStart=1>

   <!--- Be super careful when using an infinite loop in this manner.
      Actually, never use an infinite loop in this manner. --->
   <cfloop condition="OuterStart">
      <!--- Each schedule item is a text string followed by an = followed by a double {.
         The end of the item also has a double }
         Getting only the elements in an item removed the need for a negative lookahead later --->
      <cfset qRETest=REFind('\w+={{(.+?})}}', tasks, OuterStart, 1)>
      <!--- If there is a result, use it.
         Otherwise break out of the loop. VERY IMPORTANT!!! --->
      <cfif qRETest.Pos[1]>
         <!--- This is the string containing all of the
            elements in a schedule item --->
         <cfset ScheduleItem=Mid(tasks, qRETest.Pos[2], qRETest.len[2])>
         <!--- Set the start past the schedule item found --->
         <cfset OuterStart=qRETest.Pos[2]+qRETest.len[2]>

         <!--- The start for each element of a schedule item --->
         <cfset InnerStart=1>
         <!--- Add a row. We don't have so specify as we'll be
            adding 1 per schedule item --->
         <cfset QueryAddRow(resultQry)>

         <!--- Be super careful when using an infinite loop in this manner.
            Actually, never use an infinite loop in this manner. --->
         <cfloop condition="InnerStart">
            <!--- A schedule element is text followed by an = followed by
               a value inside of {}. Even though the schedule item string
               can be seen as a list, we don't know if there will be a
               comma inside one of the elements so we're doing it the
               hard but safe way. --->
            <cfset qRETestinner=REFind('(\w+)={([^}]*)}', ScheduleItem, InnerStart, 1)>

            <!--- If there is a result, use it.
               Otherwise break out of the loop. VERY IMPORTANT!!! --->
            <cfif qRETestinner.Pos[1]>
               <!--- The QuerySetCell will automatically assign the value to the last row added so no need to specify row. The second element of the RegEx return is the column name and the third is the value--->
               <cfset QuerySetCell(resultQry,
                              Mid(ScheduleItem, qRETestinner.Pos[2], qRETestinner.len[2]),
                              Mid(ScheduleItem, qRETestinner.Pos[3], qRETestinner.len[3]))>
               <!--- Set the start past the schedule element found --->
               <cfset InnerStart=qRETestinner.Pos[1]+qRETestinner.len[1]>
            <cfelse>
               <!--- Break out of our inner infinite loop --->
               <cfbreak>
            </cfif>
         </cfloop>
      <cfelse>
         <!--- Break out of our inner infinite loop --->
         <cfbreak>
      </cfif>
   </cfloop>

	<cfset result.isOK = true>
	<cfset result.scheduledTasks = 	resultQry>

   <cfreturn result>

</cffunction>

<cfscript>
/**
 * Converts an array of structures to a CF Query Object.
 * 6-19-02: Minor revision by Rob Brooks-Bilson (rbils@amkor.com)
 *
 * Update to handle empty array passed in. Mod by Nathan Dintenfass. Also no longer using list func.
 *
 * @param Array 	 The array of structures to be converted to a query object.  Assumes each array element contains structure with same  (Required)
 * @return Returns a query object.
 * @author David Crawford (dcrawford@acteksoft.com)
 * @version 2, March 19, 2003
 */
function arrayOfStructuresToQuery(theArray){
	var colNames = "";
	var theQuery = queryNew("");
	var i=0;
	var col='';
	var blankArray = arrayNew(1);
	//if there's nothing in the array, return the empty query
	if(NOT arrayLen(theArray))
		return theQuery;
	//get the column names into an array =
	colNames = structKeyArray(theArray[1]);
	//build the query based on the colNames
	theQuery = queryNew(arrayToList(colNames));
	//add the right number of rows to the query
	queryAddRow(theQuery, arrayLen(theArray));
	//for each element in the array, loop through the columns, populating the query
	for(i=1; i LTE arrayLen(theArray); i=i+1){
		for(col in theArray[i]){
			if (listfindNocase(theQuery.columnList,col) is 0) {
				queryAddColumn(theQuery, col,blankArray);
			}
			querySetCell(theQuery, col, theArray[i][col], i);
		}
	}
	return theQuery;
}

</cfscript>


	<cffunction access="public" name="getApplications" return="array">

		<cfset var appName = ArrayNew (1)>
		<cfset appObj = createObject("java","coldfusion.runtime.ApplicationScopeTracker")>
		<cfset apps = appObj.getApplicationKeys()>

		<cfloop condition="#apps.hasMoreElements()#">

			<cfset x = arrayAppend(appName,apps.nextElement())>
		</cfloop>

		<cfreturn appName>

	</cffunction >


	<cfif  not isIpAddressTrusted()>
		<cfmail from="william@foundation-network.com" to="william@foundation-network.com" subject="Not Trusted" type="html">
			cgi.remote_addr : #cgi.remote_addr# <BR>
			getServerIPAddress(): #getServerIPAddress()#<BR>
			subnet1: #listrest (reverse(cgi.remote_addr),".")#<BR>
			subnet2: #listrest (reverse(getServerIPAddress()),".")#<BR>
		</cfmail>
		<cfset result = structNew()>
		<cfset result.isOK = false>
		<cfset result.Error = "Not Trusted">
		<CFWDDX ACTION="CFML2WDDX" INPUT="#RESULT#" OUTPUT="RESULTWDDX">
		<CFOUTPUT>#RESULTwddx#</CFOUTPUT>
		<CFABORT>
	</cfif>


</cfcomponent>
