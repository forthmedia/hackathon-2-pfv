<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			instanceManagerWS.cfc	
Author:				
Date started:		WAB 
	
Description:			

This file is used for communicating between coldfusion instances

See comments in application.cfc


Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2012-01-18	WAB	Major changes co-incident with change to using CF_INVOKE
			No longer need shenanigans with applicationScope - although I will have to keep it in for backwads compatibility
2016-03-17	WAB During PROD2016-118  Added minor extra debugging
Possible enhancements:


 --->
 
 
 <cfcomponent>
 
	<cfsetting showdebugoutput="false"> 
 	<cffunction name="callWebservice" access="remote" returntype="struct">
		<cfargument name="webservicename" required="true">
		<cfargument name="methodname" required="true">
		<cfargument name="appname" required="true">	
		<cfargument name="additionalArguments"  default="#structNew()#"> <!--- has default for test purposes --->
	


		<cfset var function = "">
		<cfset var webServiceObject = "">
		<cfset var args = "">
		<cfset var result = {}>
		
		<cftry>
			<!--- WAB this is a bit of a hack so that I can call these functions on a URL for debugging purposes
				additionalArguments is normally a structure, but I haven't worked out how I would pass a structure as a URL parameter
				So for debugging purposes I make a wddx structure which I now have to decode
				Perhaps I should have made additionalArguments a WDDX string anyway - this is actually what I do with the remote admin toold
				
				Actually can be done using argumentCollection and a wddx string
			 --->
			<cfif not isStruct(additionalArguments) >
				<cfif additionalArguments is "">
					<cfset additionalArguments = {}>
				<cfelse>
					<cfwddx action="wddx2cfml" input="#additionalArguments#" output="additionalArguments">
				</cfif>
			</cfif>
	
			<cfif not request.applicationStatus.isOK>
				<cfset result.isOK = false>
				<cfset result.message = request.applicationStatus.message>
	
			<cfelse>
	
				<cfset applicationScope = application>
	
	
				<cftry>		
					<cfset securityObj = applicationScope.com.security>
		
					<cfset trusted = securityObj.isIPAddressTrusted(applicationScope = applicationScope)>	
		
					<cfif not trusted  >
						<cfset result.isOK = false>
						<cfset result.message = "Not Trusted">
					<cfelse>
		
						<cfif fileexists ("#applicationScope.paths.userfiles#/content/webservices/#webservicename#.cfc" )>
							<cfset webServiceObject = createObject ("component","/content/webservices/#webservicename#")>
						<cfelseif fileexists ("#applicationScope.userfilesabsolutepath#/code/cftemplates/webservices/#webservicename#.cfc" )>
							<cfset webServiceObject = createObject ("component","/code/cftemplates/webservices/#webservicename#")>
						<cfelse>
							<cfset webServiceObject = createObject ("component","#webservicename#")>
						</cfif>
			
						<cfset function = webServiceObject[methodname]>

							<cfset tempResult = function (applicationScope = #applicationScope#,argumentCollection = additionalArguments)>
		
							<cfif not isStruct (tempResult)>
								<cfset result = {result=tempResult}>
							<cfelse>	
								<cfset result = tempResult>
							</cfif> 
		
		
					</cfif>
			
	
						<cfcatch>
	
	
							<cfset result.isOK = false>
							<cfset result.message = cfcatch.message>
							<cfif structKeyExists (cfcatch,"tagcontext") and arrayLen (cfcatch.tagcontext)>
								<cfset result.message = result.message   & "  Line:" & cfcatch.tagcontext[1].line & " " & cfcatch.tagcontext[1].template>
							</cfif>

							<cfset applicationScope.com.errorHandler.recordRelayError_CFCATCH(catch=cfcatch,type="Cluster Management Error")>						
	
						</cfcatch>
								
					</cftry>	
			</cfif>	
			
			<cfcatch>
				<cfset result.isOK=false>
				<cfset result.message= cfcatch.message>
			</cfcatch>
		</cftry>
		<cfreturn result>
	</cffunction>
 


 </cfcomponent>
