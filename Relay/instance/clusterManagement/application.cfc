<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 

Application.cfc in the clusterManagement directory

Rather a special case!

Templates in this directory are only ever called by referencing the actual instance port
There will always be an appname parameter passed
We use this to work out which application to access (and therefore which mappings to use)

This file has to be compatible with all code versions - since there is only ever one of them 


2012-01-18  WAB Major changes implemented at same time as implementation of CF_INVOKE
	    Knock on effect is that cluster requests are no longer soap requests, 
	    so I am able to access the url variables and find the value of appname
	    This allows me to fire up with the correct application, and means that I don't need to worry about applicationScope any more
 --->

<cfcomponent>
	
	<cfset this.mappings = {}>  <!--- seem to need to start with a blank one atleast --->
	<cfset this.sessionManagement = false>
	<!--- still having trouble with Customtag paths.  This technique won't work on boxes sharing core code (probably only a dev situation) --->
	<cfset currentPath  = replaceNoCase(getCurrentTemplatePath(),"instance\clustermanagement\application.cfc","","ONCE")>
	<cfset customTagPath = currentPath & "..\customtags">
	<cfset this.customtagpaths = customTagPath> 
	

	<!--- This request parameter allows me to reference this cfc later on and update mappings when I know what the appname is  --->
	<cfset request.applicationCFC = this>


	<!--- 
	What I really need to do is set the application in application.cfc
	However at that point arguments.appname is not available
	When you call the cfc on a URL it is available as url.appname however when called by CF as a webservice it is not available
	I tried to use getHTTPRequestData() to extract the value of appname in the request.
	This would work, except for a bug in getHTTPRequestData() [CF8] which means that the whole request fails if this method is called
	I could just use application.cfm but then I would lose the ability to do the application specific mappings
	I have discovered that I can add mappings at run time by using a pointer back to the this scops of application.cfc (request.applicationcfc)

	No Session
	 --->

	<cfset appname = "">
	<cfif structKeyExists(url,"appname")>
		<cfset appname = url.appname>
	<cfelseif structKeyExists(url,"argumentCollection")>
		<cfwddx action="wddx2cfml" input="#url.argumentCollection#" output="temparguments">
		<cfif structKeyExists (temparguments,"appname")>	
			<cfset appname = temparguments.appname>
		</cfif>	
	</cfif>


	<cfset request.applicationStatus = {isOK=false,message=""}>

	<cfif appname is not "">


		<cfif structKeyExists (server,"serverInitialisationObj") and structKeyExists (server.serverInitialisationObj.structures.relayApplicationsStruct,appname)>


				<cfset siteDetails = server.serverInitialisationObj.structures.relayApplicationsStruct[appname]>
				<cfset this.mappings = structCopy(siteDetails.cf_mappings)>  
 				<cfset this.customtagpaths= listappend(request.applicationCFC.customtagpaths,sitedetails.paths.customtags)>  
				<cfset this.name = siteDetails.cfappname>
				<cfset request.applicationStatus.isok = true>
				<!--- WAB 2016-03-17 During PROD2016-118  Housekeeping Improvements. Added datasource (so can do queries without datasource parameter) --->
				<cfset this.datasource = sitedetails.datasource>

		<cfelse>
				<cfset request.applicationStatus.message="Application #appname# Not Loaded">
		</cfif>


	<cfelse>	
		<cfset request.applicationStatus.message="Appname not provided">
	</cfif>


	

	
</cfcomponent>
