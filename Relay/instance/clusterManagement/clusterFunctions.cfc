<!--- �Relayware. All Rights Reserved 2014 --->
<!---
***********
NOTE NOTE
WAB 2010-10-20 8.3  Note that this file is no longer called directly, but is actually always instantiated by ins\clusterManagement\clusterManagerWS.cfc
***********

manageCluster.cfc


Author  WAB

Date June 2005


This template provides a web service to allow for management of sites on multiple servers
In particular it was designed used for resetting certain application variables on multiple servers
	1) for incrementing CachedContentVersionNumber so that content can be editied on one server and viewed on another
	2) for clearing keys from the phrase structures - for the same reason.

See wiki for details



Mods

WAB 2008-11-19 Added buildNavigationTreeFromDB function
WAB 2009-02-17  Added a readTemplate function
WAB 2009-03-04 found updateFieldInfoStruct function in old version of this template (in the webservices directory)
WAB 2009-06-25 doReload function. Will eventually handle all reload events
WAB 2009-08-25 LID 2512, secure files added getListOfSessionIDs function
WAB 2011-11-10 buildNavigationTreeFromDB() modified so that it works with 8.3

WAB 2010-10-20 8.3  Note that this file is no longer called directly, but is actually always instantiated by instance\clusterManagement\clusterManagerWS.cfc
NJH 2016/09/07	JIRA PROD2016-2232 - added getScheduledTasks
WAB 2016-10-04	PROD2016-2419 Added killListOfSessions() 
 --->


<cfcomponent>

<!---


--->


	<cffunction access="public" name="resetPhraseStructureKey" returntype="boolean" hint="called as a webservice from relaytranslations.cfc">
		<cfargument name="phraseTextID" type="string" required="true">
		<cfargument name="applicationScope" type="struct" required="true">

			<cfreturn applicationScope.com.relaytranslations.resetPhraseStructureKey(phraseTextID=phraseTextID, updateCluster = false,applicationScope=applicationScope)>

	</cffunction>

	<cffunction access="public" name="resetPhraseStructure" returntype="boolean" hint="called as a webservice from relaytranslations.cfc">
		<cfargument name="applicationScope" type="struct" required="true">
		<!--- 2009-01-02 NYB replaced -> ---
		<cfreturn applicationScope.com.relaytranslations.resetPhraseStructure(updateCluster = false)>
		!--- 2009-01-02 NYB with: --->
		<cfset applicationScope.com.relaytranslations.resetPhraseStructure(updateCluster = false,applicationScope=applicationScope)>
		<cfreturn true>
		<!--- <- 2009-01-02 NYB --->
	</cffunction>

	<cffunction access="public" name="doesPhraseStructureKeyExist" returntype="boolean" hint="really just used for tesing">
		<cfargument name="phraseTextID" type="string" required="true">
		<cfargument name="applicationScope" type="struct" required="true">

			<cfif structKeyExists(applicationScope.phraseStruct,phraseTextID)>
				<cfreturn true>
			<cfelse>
				<cfreturn false>
			</cfif>


	</cffunction>

	<cffunction access="public" name="incrementCachedContentVersionNumber" returntype="boolean" hint = "called as a webservice from relayelementTree.cfc">
		<cfargument name="applicationScope" type="struct" required="true">

			<cfset  applicationScope.com.relayElementTree.incrementCachedContentVersionNumber(updateCluster = false,applicationScope=applicationScope)>
			<cfreturn true>

	</cffunction>


	<cffunction access="public" name="updateFlagGroupStructure" returntype="boolean" hint = "called as a webservice from relayelementTree.cfc">
		<cfargument name="flagGroupID" type="string" required="true">
		<cfargument name="applicationScope" type="struct" required="true">

			<cfset  applicationScope.com.flag.updateFlagGroupStructure(updateCluster = false,flagGroupID = flagGroupID,applicationScope=applicationScope )>
			<cfreturn true>

	</cffunction>

	<cffunction access="public" name="updateFlagStructure" returntype="boolean" hint = "called as a webservice from relayelementTree.cfc">
		<cfargument name="flagID" type="string" required="true">
		<cfargument name="applicationScope" type="struct" required="true">

			<cfset  applicationScope.com.flag.updateFlagStructure(updateCluster = false,flagID = flagID,applicationScope=applicationScope )>
			<cfreturn true>


	</cffunction>

	<cffunction access="public" name="updateSettingXMLNode" returntype="boolean" hint = "updates settings for a list of variables">
		<cfargument name="variableNames" type="string" required="true">
		<cfargument name="applicationScope" type="struct" required="true">

			<cfset  applicationScope.com.settings.populateSettingXMLNodeByVariableName(updateCluster = false,variableNames= variablenames )>
			<cfreturn true>

	</cffunction>

	<!--- WAb 2011-03-10 Add this function not particularly useful, other than for confirming that settings are being updated across a cluster --->
	<cffunction access="public" name="getSetting" returntype="boolean" hint = "gets setting for a of variable">
		<cfargument name="variableName" type="string" required="true">
		<cfargument name="applicationScope" type="struct" required="true">

			<cfset  var result = applicationScope.com.settings.getSetting(variableName= variablename)>
			<cfreturn result >

	</cffunction>

	<cffunction access="public" name="getCachedContentVersionNumber" output="true" returntype="numeric" hint="really used for checking that function above is working">
		<cfargument name="applicationScope" type="struct" required="true">

			<cfreturn applicationScope.CachedContentVersionNumber>

	</cffunction>

	<cffunction access="public" name="updateFieldInfoStruct" returntype="struct" hint = "This will reset the blacklist var when a new blacklist item is added">

			<cfargument name="tableName" required="Yes">
			<cfargument name="fieldName" required="Yes">
			<cfargument name="applicationScope" type="struct" required="true">

			<cfset var result = {isOK = true}>

			<cfset  applicationScope.com.ApplicationVariables.updateFieldInfoStructure(updateCluster = false,applicationScope=applicationScope)>
			<cfreturn result>

	</cffunction>

	<cffunction access="public" name="loadSiteDefinitions" output="true" returntype="numeric" hint="reloads SiteDefinitions into Memory">
		<cfargument name="applicationScope" type="struct" required="true">

			<cfreturn applicationScope.com.relayCurrentSite.loadSiteDefinitions(updateCluster = false,applicationScope=applicationScope)>

	</cffunction>

	<cffunction access="public" name="loadElementTreeDefinitions" output="true" returntype="numeric" hint="reloads loadElementTreeDefinitions into Memory">
		<cfargument name="applicationScope" type="struct" required="true">

			<cfreturn applicationScope.com.relayCurrentSite.loadElementTreeDefinitions(updateCluster = false,applicationScope=applicationScope)>

	</cffunction>



	<cffunction access="public" name="testme" output="true" returntype="struct" hint="really used for checking that function above is working">
		<cfargument name="applicationScope" type="struct" required="true">
		<cfargument name="wait" default="false">

		<cfset var result = {}>

		<cfparam name="applicationScope.testMe" default="0">
		<cfset applicationScope.testMe = applicationScope.testMe + 1>

		<cfset result = {isOK=true,message="Hello World #applicationScope.applicationname# #applicationScope.testMe# #iif(wait,de('Waited'),de(''))#"}>
		<cfif wait> <cfset sleep(3000)></cfif>

		<cfreturn result>

	</cffunction>

	<!--- WAB 2008-11-19 --->
	<cffunction access="public" name="buildNavigationTreeFromDB" output="true" returntype="string" hint="runs function to reload product trees">
		<cfargument name="applicationScope" type="struct" required="true">
			<cfargument name="treeID" type="numeric" required="true">

			<cfset var result = "">
			<!--- needed to get request.PRODUCTCATEGORYIMAGESDIRECTORY so include pcmINI
				but this template has some other code in it that fails so I need to cftry it.
				it relies on the fact that PRODUCTCATEGORYIMAGESDIRECTORY is set before the other code fails
			--->
			<cftry>
				<!--- pcmINI is used to over-ride default global application variables and params --->
				<cfset application = applicationScope>
 				<cf_include template="\code\cftemplates\pcmINI.cfm" checkIfExists="true">

				<cfcatch>
				</cfcatch>
			</cftry>

				<cfset result = applicationScope.com.relayProductTree.buildNavigationTreeFromDB(treeid = treeid, updateCluster = false)>

			<cfreturn result>


	</cffunction>


	<cffunction name="readTemplate" access="public" returntype="struct">
		<cfargument name="applicationScope" type="struct" required="true">
		<cfargument name="template" required=true >
		<cfargument name="RelativeTo" required=true >  <!--- Relay,CustomTags,BestCode --->

				<cfset var result= structnew()>
				<cfset var templateContent= "">
				<cfset var rootpath="">

				<cfset result.template = template>

			<cftry>

				<cfswitch expression = #RelativeTo#>
					<cfcase value="relay">
						<cfset rootPath = applicationScope.path & "/relay">
					</cfcase>
					<cfcase value="customTags">
						<cfset rootPath = applicationScope.path & "/customtags">
					</cfcase>
					<cfcase value="userFiles">
						<cfset rootPath = applicationScope.userfilesAbsolutePath>
					</cfcase>
					<cfcase value="serverRoot">
						<cfset rootPath = server.coldfusion.rootDir>
					</cfcase>
				</cfswitch>



				<cffile action="read" file="#rootPath#\#template#" variable="templateContent" ><!--- charset="utf-8" --->

				<cfset result.isOK = true>
				<cfset result.content = templateContent>
				<cfset result.len = len(templateContent)>

				<cfcatch>
					<cfset result.isOK = false>
					<cfset result.error.message = cfcatch.message>
				</cfcatch>

			</cftry>

		<cfreturn result>
	</cffunction>

	<!--- WAB 2009-06-25 --->
	<cffunction access="public" name="doReload" output="true" returntype="struct" hint="">
		<cfargument name="applicationScope" type="struct" required="true">
		<cfargument name="item" required=true >
			<cfreturn applicationScope.com.applicationVariables.doReload(item=item,updateCluster = false,applicationScope=applicationScope)>

	</cffunction>


	<!--- WAB 2009-06-25 used to check whether a lock exists on any other server in a cluster--->
	<cffunction access="public" name="doesLockExist" output="true" returntype="struct" hint="checks whether there is a CF lock of this name in existence">
		<cfargument name="applicationScope" type="struct" required="true">
		<cfargument name="lockName" required=true >

			<cfset result=structNew()>
			<cfset result.isOK = true>

			<cftry>
				<cflock name = "#lockName#" timeout="1" throwontimeout="Yes" type="EXCLUSIVE" >
				</cflock>

				<cfset result.hasLock = false>

				<cfcatch>
					<cfset result.hasLock = true>
				</cfcatch>
			</cftry>

		<cfreturn result>


	</cffunction>


	<!--- WAB 2009-08-25 added --->
	<cffunction access="public" name="getListOfSessionIDs" output="true" returntype="struct" hint="">
		<cfargument name="applicationScope" type="struct" required="true">
		<cfset var result = structNew()>
		<cfset result.isok = true>
		<cfset result.sessionids = applicationScope.com.sessionManagement.getListOfSessionIDs(includeCluster = false,applicationScope=applicationScope)>

		<cfreturn  result>

	</cffunction>


	<!--- WAB 	2016-10-04	PROD2016-2419 --->
	<cffunction access="public" name="KillListOfSessions" output="true" returntype="struct" hint="">
		<cfargument name="applicationScope" type="struct" required="true">
		<cfset var result = structNew()>
		<cfset result.isok = true>
 		<cfset result.sessionsdeleted = applicationScope.com.sessionManagement.KillListOfSessions(includeCluster = false,applicationScope=applicationScope, sessionIDs = sessionids)> 

		<cfreturn result>
		
	</cffunction>


	<cffunction access="public" name="getNumberOfActiveSessions" output="true" returntype="struct" hint="">
		<cfargument name="applicationScope" type="struct" required="true">
		<cfset var result = structNew()>
		<cfset result.isok = true>
		<cfset result.count = listlen(applicationScope.com.sessionManagement.getListOfSessionIDs(includeCluster = false,applicationScope=applicationScope))>

		<cfreturn  result>

	</cffunction>


	<cffunction access="public" name="getRelayCurrentUserStructure" output="true" returntype="struct" hint="">
		<cfargument name="applicationScope" type="struct" required="true">
		<cfargument name="sessionid">
		<!--- needs the session id --->
		<!--- get the session  --->
		<cfset var result = {isok = true,sessionid = sessionid}>

		<cfset sessionScope = applicationScope.com.sessionManagement.getASession(applicationname=applicationScope.applicationname,sessionid=sessionid) >

		<cfset result.currentuser = sessionScope.relaycurrentuserstructure>

		<cfreturn result>
	</cffunction>


	<cffunction access="public" name="runRelayCurrentUserFunction" output="true" returntype="struct" hint="">
		<cfargument name="applicationScope" type="struct" required="true">
		<cfargument name="sessionid">
		<cfargument name="relayCurrentUserFunctionName">
		<!--- needs the session id --->
		<!--- get the session  --->
		<cfset var result = {isok = true,sessionid = sessionid}>

		<cfset sessionScope = applicationScope.com.sessionManagement.getASession(applicationname=applicationScope.applicationname,sessionid=sessionid) >
		<cftry>
			<!--- had to do a cfinvoke to get the scoping correct --->
			<cfinvoke
			    component = "#applicationScope.com.relayCurrentUser#"
			    method = "#relayCurrentUserFunctionName#"
			    returnVariable = "result.functionResult"
			    argumentCollection = "#arguments#"
				sessionscope = "#sessionscope#"
		    >

			<cfcatch>
				<cfset result.isOK = false>
				<cfset result.message = cfcatch.message>
				<cfset result.detail = cfcatch.detail>
				<cfset result.appname = applicationScope.applicationname>
				<cfset result.functionResult = "Error " & cfcatch.message>
			</cfcatch>
		</cftry>


		<cfset result.currentuser = sessionScope.relaycurrentuserstructure>

		<cfreturn result>
	</cffunction>

	<!--- WAB 2016-03-17 PROD2016-118  Housekeeping Improvements Added extra arguments --->
	<cffunction access="public" name="runHouseKeeping" output="true" returntype="struct" hint="">
		<cfargument name="houseKeepingMethodName" default="" >
		<cfargument name="force" default="false" >

		<cfset var result = {isok = true}>

		<cfset result = applicationScope.com.housekeeping.runHouseKeeping(houseKeepingMethodName = houseKeepingMethodName, force= force)>

		<cfreturn result>
	</cffunction>


	<cffunction name="getScheduledTasks" access="public" returntype="struct" hint="Returns the ColdFusion Scheduled Tasks">

		<cfset var result = {isOK=true}>
		<cfset var scheduledTasks  = "">

		<cfschedule action="list" result="scheduledTasks">
		<cfset result.scheduledTasks = scheduledTasks>

		<cfreturn result>
	</cffunction>
</cfcomponent>
