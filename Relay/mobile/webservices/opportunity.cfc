<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent output="false">

		<!--- 2012/07/04 PJP --->
	<cffunction name="getOpportunities" access="public" output="false" returntype="any">
		<cfargument name="opportunityID" type="numeric" required="false">
		<cfargument name="teamOpps" type="boolean" default="false">

		<cfset var result = "">
		<cfset var tmpBudgetQry = "">
		<cfset var tmpProductGrpList="">
		<cfset var tmpProductList = "">
		<cfset var opportunitiesUserHasRightsTo = application.com.opportunity.getOpportunitiesAPartnerHasRightsTo()>
		<cfset var oppTeamResult = arrayNew(2)>
		<cfset var getPartners = "">
		<cfset var getPartnerOpps = "">
		<cfset var arrayIdx = 1>
		<cfset var hideOppStagesFromOppList = application.com.settings.getSetting("leadManager.hideOppStagesFromOppList")>

		<cfquery name="result" datasource = "#application.sitedatasource#">
			select distinct '' as productGroupIds,'' as products, l.telephone,opp.partnerlocationid,opp.entityId,opp.countryID,
				opp.contactpersonid,opp.opportunityid,o.organisationName,p.locationid,
				p.firstname,p.lastname,p.email,opp.budget as oppValue,cast (opp.expectedCloseDate as varchar(20)) as expectedCloseDate,opp.detail,
				opp.currency,
				(select top 1 pg.description from oppProducts oppp
					inner join product p on oppp.productOrGroupID=p.productId and productOrGroup='P'
					inner join productGroup pg on p.productGroupID = pg.productGroupID
				where opportunityId=opp.opportunityID
					group by pg.productGroupID,pg.description
					order by  sum(subtotal) desc) as description,
				isNull(range,'') as budgetRange, opp.estimatedBudget as budget,
				partner.firstname+' '+partner.lastname as partnerName,
				'phr_OppStage_'+oppStage.stageTextID as stage,
				case when oppStatus.statusID is null then 'phr_notAppliedForDealReg' else 'phr_opp_'+oppStatus.statusTextID end as status,
				case when oppStage.stageTextID in (<cf_queryparam value="#application.com.settings.getSetting('leadManager.closedOppStages')#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true">) then 0
					when oppStatus.statusTextID = 'DealRegApplied' then 0
					when oppPS.statusTextID not in ('','StandardPricing') then 0
					else 1 end as canEdit
			from opportunity opp with (noLock)
				inner join person partner with (noLock) on partner.personID = opp.partnerSalesPersonID
				inner join oppStage with (noLock) on oppStage.opportunityStageID = opp.stageID
				left join oppStatus with (noLock) on opp.statusID = oppStatus.statusID
				left join oppPricingStatus oppPS with (noLock) on opp.oppPricingStatusID = oppPS.oppPricingStatusID
				left join organisation o with (noLock) on opp.entityId = o.organisationID
				left join person p with (noLock) on p.organisationID = o.organisationID and p.personId = opp.contactPersonID
				left join location l with (noLock) on o.organisationID = l.organisationID and p.locationID = l.locationID
				left join range r with (noLock) on r.rangeValue = opp.estimatedBudget
			where 1=1
			and l.organisationID = o.organisationID
			and
			<cfif opportunitiesUserHasRightsTo.recordCount>
				opp.opportunityID in (#valueList(opportunitiesUserHasRightsTo.opportunityID)#)
			<cfelse>
				1=0
			</cfif>
			 <cfif hideOppStagesFromOppList neq "">
				AND oppStage.stageTextID  NOT IN (<cf_queryparam value="#hideOppStagesFromOppList#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true">)
			</cfif>
			and oppStage.liveStatus = 1
			and (#request.relaycurrentuser.personID# <cfif arguments.teamOpps> not</cfif> IN (opp.partnerSalesPersonID, opp.partnerSalesManagerPersonID))
			<cfif structKeyExists(arguments,"opportunityID")>
				and opp.opportunityID=<cf_queryparam value="#arguments.opportunityID#" cfsqltype="numeric"/>
			</cfif>
		</cfquery>

		<cf_translateQueryColumn query = #result# columnName="status,stage" onNullShowPhraseTextID = false>

		<cfloop query="result">
			<cfquery name="oppProdsqry" datasource = "#application.sitedatasource#">
				select pg.title_defaultTranslation as description
					from opportunityProduct oppp with (noLock) inner join ProductGroup pg with (noLock)
						on oppp.productOrGroupid = pg.productgroupid and oppp.ProductORGroup='G'
				where OpportunityID = <cf_queryparam value="#result.opportunityID#" cfsqltype="numeric"/>
			</cfquery>
			<cfset tmpProductGrpList = valuelist(oppProdsqry.description) >
			<cfset tmpProductGrpList = replacenocase(tmpProductGrpList,',',', ','all')>
			<cfset QuerySetCell(result,"productgroupids",tmpProductGrpList,currentrow)>

			<cfquery name="oppProdsqry" datasource = "#application.sitedatasource#">
				select p.description +'('+p.sku+')|'+cast(quantity as varchar)+'|'+cast(subtotal as varchar) as products
					from opportunityProduct oppp with (noLock) inner join product p with (noLock)
						on oppp.productOrGroupID = p.productID and oppp.ProductORGroup='P'
				where OpportunityID = <cf_queryparam value="#result.opportunityID#" cfsqltype="numeric"/>
			</cfquery>
			<cfset tmpProductList = valuelist(oppProdsQry.products,application.delim1)>
			<cfset QuerySetCell(result,"products",tmpProductList,currentrow)>
		</cfloop>

		<!--- if returning  "my teams" opportunities, return a 2-dimensional array, where the first element is the partner name and the second is the query of opps. --->
		<cfif arguments.teamOpps>
			<cfquery name="getPartners" dbtype="query">
				select distinct partnerName from result
			</cfquery>

			<cfloop query="getPartners">
				<cfquery name="getPartnerOpps" dbtype="query">
					select * from result where partnerName = '#partnerName#'
				</cfquery>

				<cfset oppTeamResult[arrayIdx][1] = partnerName>
				<cfset oppTeamResult[arrayIdx][2] = getPartnerOpps>

				<cfset arrayIdx++>
			</cfloop>

			<cfset result = oppTeamResult>
		</cfif>

		<cfreturn result>
	</cffunction>


	<cffunction name="getBudgetRanges" access="public" output="false" returnType="query">
		<cfreturn application.com.opportunity.getBudgetRanges()>
	</cffunction>


	<!--- 2012/06/14 PPB P-REL112 --->
	<cffunction name="getProductGroups" access="public" output="false" returnType="query">
		<cfargument name="campaignID" required="false" default="#application.com.settings.getSetting('leadManager.products.productCatalogueCampaignID')#" >

		<cfset var qryProductGroups = "">

		<!--- NJH 2013/02/21 - filter out deleted groups and products... return the new title field rather than the description --->
		<cfquery name="qryProductGroups" datasource="#application.siteDatasource#">
			SELECT DISTINCT pg.ProductGroupId, pg.title_defaultTranslation as Description
			FROM ProductGroup pg with (noLock)
				INNER JOIN Product p  with (noLock) ON p.ProductGroupId=pg.ProductGroupId
			WHERE p.CampaignID = <cf_queryparam value="#arguments.campaignID#" cfsqltype="CF_SQL_INTEGER" >
				and isNull(p.deleted,0) != 1
				and isNull(pg.active,1) = 1
				and isNull(p.active,1) = 1
		</cfquery>

		<cfreturn qryProductGroups>
	</cffunction>

	<!--- 2012/07/04 PJP --->
	<cffunction name="updateOpportunity" access="public" output="false" returntype="array">
		<cfargument name="opportunityID" type="numeric" required="false">
		<cfargument name="currency" type="string" required="false" default="USD">
		<cfargument name="budget" type="numeric" required="true">
		<cfargument name="detail" type="string" required="true">
		<cfargument name="expectedCloseDate" type="string" required="true">
		<cfargument name="firstname" type="string" required="true">
		<cfargument name="lastname" type="string" required="true">
		<cfargument name="companyName" type="string" required="true">
		<cfargument name="telephone" type="string" required="true">
		<cfargument name="email" type="string" required="true">
		<cfargument name="productGroupID" type="any" required="true">

		<cfset var opportunity = structNew()>
		<cfset var productGroupList = "">
		<cfset var message = "ok">
		<cfset var result = arrayNew(1)>
		<cfset var getOpportunitiesQry ="">
		<cfset var getExistingProductGroupIds = "">
		<cfset var prodGroupID = 0>

		<cfset opportunity.currency = arguments.currency>
		<cfset opportunity.estimatedBudget = arguments.budget>
		<cfset opportunity.detail = arguments.detail>
		<cfset opportunity.expectedCloseDate = arguments.expectedCloseDate>

		<cfif application.com.rights.doesUserHaveRightsForEntity(entityType="opportunity",entityID=arguments.opportunityID,permission="edit")>

			<cftransaction action="begin">
				<cftry>
					<cfset endCustomer = upsertEndCustomer(argumentCollection=arguments)>

					<cfif endCustomer.entityID neq 0>
						<cfset opportunity.entityID = endCustomer.entityID>
					</cfif>
					<cfif endCustomer.contactPersonId neq 0>
						<cfset opportunity.contactPersonId = endCustomer.contactPersonId>
					</cfif>
					<cfset updOppStruct = application.com.relayEntity.updateOpportunityDetails(opportunityID=arguments.opportunityID,opportunityDetails=opportunity)>

					<cfset oppProducts.OpportunityId = arguments.opportunityID>
					<cfset oppProducts.ProductORGroup = 'G'>

					<cfif isArray(arguments.productGroupID)>
						<cfset productGroupList = arrayToList(arguments.productGroupID)>
					<cfelse>
						<cfset productGroupList = arguments.productGroupID>
					</cfif>

					<cfquery name="deleteOppProduct" datasource="#application.siteDataSource#">
						update opportunityProduct
						set lastUpdatedBy=<cf_queryparam value="#request.relayCurrentUser.userGroupId#" cfsqltype="CF_SQL_INTEGER" >,
							lastUpdated = getDate(),
							lastUpdatedByPerson = <cf_queryparam value="#request.relayCurrentUser.personID#" cfsqltype="CF_SQL_INTEGER">
						where OpportunityID =  <cf_queryparam value="#arguments.opportunityID#" cfsqltype="CF_SQL_INTEGER" >
							and productOrGroup='G'
							<cfif productGroupList neq "">
							and productOrGroupID not in (<cf_queryparam value="#productGroupList#" cfsqltype="CF_SQL_INTEGER" list="true">)
							</cfif>

						delete opportunityProduct
						where OpportunityID = <cf_queryparam value="#arguments.opportunityID#" cfsqltype="CF_SQL_INTEGER" >
							and productOrGroup='G'
							<cfif productGroupList neq "">
							and productOrGroupID not in (<cf_queryparam value="#productGroupList#" cfsqltype="CF_SQL_INTEGER" list="true">)
							</cfif>
					</cfquery>

					<cfif productGroupList neq "">
						<cfquery name="getExistingProductGroupIds" datasource="#application.siteDataSource#">
							select productOrGroupID from opportunityProduct
							where OpportunityID = <cf_queryparam value="#arguments.opportunityID#" cfsqltype="CF_SQL_INTEGER">
								and productOrGroup='G'
						</cfquery>

						<!--- Update the product group id's for this opportunity --->
						<cfloop list="#productGroupList#" index="prodGroupID">
							<cfif getExistingProductGroupIds.recordCount eq 0 or not listFind(valueList(getExistingProductGroupIds.productOrGroupID),prodGroupID)>
								<cfset oppProducts.ProductORGroupID = prodGroupID>
								<cfset insOppProductsStruct = application.com.relayEntity.insertOppProduct(ProductDetails=oppProducts)>
							</cfif>
						</cfloop>
					</cfif>

					<cftransaction action="commit">

					<cfcatch>
						<cftransaction action="rollback">
					</cfcatch>

				</cftry>
			</cftransaction>
		</cfif>

		<cfset ArrayAppend(result, application.com.relayTranslations.translatePhrase(phrase=message))>

		<cfreturn result>
	</cffunction>

	<!--- 2012/07/04 PJP --->
	<cffunction name="insertOpportunity" access="public" output="false" returntype="array">
		<cfargument name="companyName" type="string" required="true">
		<cfargument name="firstname" type="string" required="true">
		<cfargument name="lastname" type="string" required="true">
		<cfargument name="email" type="string" required="true">
		<cfargument name="budget" type="numeric" required="true">
		<cfargument name="currency" type="string" required="false" default="USD">
		<cfargument name="telephone" type="string" required="true">
		<cfargument name="productGroupID" type="any" required="true">
		<cfargument name="detail" type="string" required="true">
		<cfargument name="expectedCloseDate" type="string" required="true">

		<cfset var opportunity = structNew()>
		<cfset var oppProducts = structNew()>
		<cfset var insOppStruct = structNew()>
		<cfset var insOppProductsStruct = structNew()>
		<cfset var entityID = 0>
		<cfset var contactPersonID = 0>
		<cfset var oppStruct = structNew()>
		<cfset var result = arrayNew(1)>
		<cfset var message = "">
		<cfset var opportunityID = 0>
		<cfset var tmparray = "">
		<cfset var getOpportunitiesqry="">
		<cfset var idx="">
		<cfset var idx2="">
		<cfset var endCustomer = structNew()>

		<cfset endCustomer = upsertEndCustomer(argumentCollection=arguments)>
		<cfset entityId = endCustomer.entityID>
		<cfset contactPersonId = endCustomer.contactPersonId>

		<cfif entityId neq 0 and contactPersonId neq 0>
			<cfset opportunity.currency = arguments.currency>
			<cfset opportunity.entityID = entityID>
			<cfset opportunity.contactPersonID = contactPersonID>
			<cfset opportunity.estimatedBudget = arguments.budget>
			<cfset opportunity.detail = arguments.detail>
			<cfset opportunity.stageID = 1>
			<cfset opportunity.statusID = 0>
			<cfset opportunity.partnerLocationID = request.relayCurrentUser.locationID>
			<cfset opportunity.countryID = request.relayCurrentUser.countryID>
			<cfset opportunity.partnerSalesPersonID = request.relayCurrentUser.personID>
			<cfset opportunity.expectedCloseDate = arguments.expectedCloseDate>
			<!--- Insert Opportunity --->
			<cfset insOppStruct = application.com.relayEntity.insertOpportunity(opportunityDetails=opportunity)>

			<cfif insOppStruct.isOK>
				<cfset opportunityID = insOppStruct.entityID>
				<cfset oppProducts.OpportunityId = opportunityID>
				<cfset oppProducts.ProductORGroup = 'G'>

				<cfif isarray(arguments.productGroupID)>
					<cfset tmparray = arguments.productGroupID>
				<cfelse>
					<cfset tmparray = listtoarray(arguments.productGroupID)>
				</cfif>

				<!--- Insert the product group id's for this opportunity --->
				<cfloop from="1" to="#arraylen(tmparray)#" index="idx">
					<cfset oppProducts.ProductORGroupID = tmparray[idx]>
					<cfset insOppProductsStruct = application.com.relayEntity.insertOppProduct(ProductDetails=oppProducts)>
				</cfloop>

				<!--- <cfif insOppProductsStruct.isOK>
					<cfset message = "phr_opportunity_mobile_successfullyAddedOpportunity">
				<cfelse>
					<cfset message = "phr_opportunity_mobile_unableToAddOpportunityProducts">
				</cfif> --->
			<!--- <cfelse>
				<cfset message = "phr_opportunity_mobile_unableToAddOpportunity"> --->
			</cfif>

		<!--- <cfelse>
			<cfset message = "phr_opportunity_mobile_unableToAddEndCustomer"> --->
		</cfif>

		<cfset result[1] = opportunityID>

		<cfreturn result>

	</cffunction>


	<cffunction name="upsertEndCustomer" access="private" returntype="struct">
		<cfargument name="companyName" type="string" required="true">
		<cfargument name="firstname" type="string" required="true">
		<cfargument name="lastname" type="string" required="true">
		<cfargument name="email" type="string" required="true">
		<cfargument name="telephone" type="string" required="true">
		<cfargument name="opportunityID" type="numeric" default=0>

		<cfset var getOpportunitiesQry = "">
		<cfset var endCustomerOrg = structNew()>
		<cfset var endCustomerLoc = structNew()>
		<cfset var endCustomerPer = structNew()>
		<cfset var insOrgStruct = structNew()>
		<cfset var insLocStruct = structNew()>
		<cfset var insPerStruct = structNew()>
		<cfset var entityID = 0>
		<cfset var contactPersonID = 0>
		<cfset var contactLocationID = 0>
		<cfset var result = {entityID=0,contactPersonID=0}>

		<cfif arguments.opportunityID neq 0>
			<cfset getOpportunitiesQry = application.com.opportunity.getOpportunityDetails(opportunityID=arguments.opportunityID)>

			<cfif getOpportunitiesQry.contactPersonID neq 0>
				<cfquery name="getContactLocationID" datasource="#application.siteDataSource#">
					select locationID from person where personID =  <cf_queryparam value="#getOpportunitiesQry.contactPersonID#" CFSQLTYPE="cf_sql_integer" >
				</cfquery>
				<cfset contactLocationID = getContactLocationID.locationID>
			</cfif>

			<cfif getOpportunitiesQry.entityID neq 0 and getOpportunitiesQry.entityID neq "">
				<cfset entityID = getOpportunitiesQry.entityID>
			</cfif>
			<cfif getOpportunitiesQry.contactPersonID neq 0 and getOpportunitiesQry.contactPersonID neq "">
				<cfset contactPersonID = getOpportunitiesQry.contactPersonID>
			</cfif>
		</cfif>

		<cfset endCustomerOrg.organisationName = arguments.companyName>
		<cfif entityID eq 0>
			<cfset endCustomerOrg.countryID = request.relayCurrentUser.countryID>
			<cfset endCustomerOrg.organisationTypeID = application.organisationType.endCustomer.organisationTypeID>
			<!--- Insert Organisation --->
			<cfset insOrgStruct = application.com.relayEntity.insertOrganisation(organisationDetails=endCustomerOrg,insertIfExists=false)>

			<cfif insOrgStruct.isOk>
				<cfset entityID = insOrgStruct.entityID>
			</cfif>
		<cfelse>
			<cfset updOrgStruct = application.com.relayEntity.updateOrganisationDetails(organisationID=entityID,organisationDetails=endCustomerOrg)>
		</cfif>

		<cfif entityID neq 0>
			<cfset endCustomerLoc.siteName = arguments.companyName>
			<cfset endCustomerLoc.telephone = arguments.telephone>

			<cfif contactLocationID eq 0>
				<cfset endCustomerLoc.countryID = request.relayCurrentUser.countryID>
				<cfset endCustomerLoc.organisationID = entityID>
				<cfset endCustomerLoc.address1 = "">
				<cfset endCustomerLoc.postalCode = "">
				<cfset endCustomerLoc.address4 = "">
				<!--- Insert Location --->
				<cfset insLocStruct = application.com.relayEntity.insertLocation(locationDetails=endCustomerLoc,insertIfExists=false,matchLocationColumns="countryID,telephone,sitename,organisationID")>

				<cfif insLocStruct.isOK>
					<cfset contactLocationID = insLocStruct.entityID>
				</cfif>
			<cfelse>
				<cfset updLocStruct = application.com.relayEntity.updateLocationDetails(locationId=contactLocationID,locationDetails=endCustomerLoc)>
			</cfif>
		</cfif>

		<cfif contactLocationID neq 0>
			<cfset endCustomerPer.firstname=arguments.firstname>
			<cfset endCustomerPer.lastname=arguments.lastname>
			<cfset endCustomerPer.email=arguments.email>

			<cfif contactPersonId eq 0>
				<cfset endCustomerPer.organisationID = insOrgStruct.entityID>
				<cfset endCustomerPer.locationID = insLocStruct.entityID>
				<!--- Insert Person --->
				<cfset insPerStruct = application.com.relayEntity.insertPerson(personDetails=endCustomerPer,insertIfExists=false,matchPersonColumns="organisationID,email")>

				<cfif insPerStruct.isOK>
					<cfset contactPersonID = insPerStruct.entityID>
				</cfif>
			<cfelse>
				<cfset updPerStruct = application.com.relayEntity.updatePersonDetails(personId=contactPersonID,personDetails=endCustomerPer)>
			</cfif>
		</cfif>

		<cfset result.entityID = entityID>
		<cfset result.contactPersonID = contactPersonID>

		<cfreturn result>
	</cffunction>

</cfcomponent>