<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			callWebService.cfc	
Author:				
Date started:		WAB 2010/02/11
	
Description:			

A file which is used to call other webservices (primarily via ajax) 

Reasons for using this file
i) 		automatically allow customer extensions in userfiles\content\webservices
ii) 	does not exposing all methods in a WSDL, none of the methods in the actual webservice need to be remote - (which must be an invitation to hackers)
iii) 	allows individual methods to be given different security levels by using the security attribute
iv)		should allow better responses to security failures (JSON requests get JSON, plain test requests get plain text message)

This file is made open to all in directorySecurity.xml
The security for the webservice which is to be called must be in directorySecurity.xml
(only needs to be entered under the relay/webservices , don't need a separate entry for userfiles

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
WAB 2010/03/24 problem with this scope within the called function.  Changed to use cfinvoke rather than createobject followed by a call
WAB 2011/10/19 Added support for security attribute and better messages when security fails
WAB 2011/11/16 Added support for enforcing argument encryption
Possible enhancements:


NOTE: THis file is based on the file in webservices directory. Any changes to that file should probably result in a change to here as well.
It may be that the code is factorised at some point.
 --->
 
 
 <cfcomponent>
 
	<!--- when calling this function from the mobile, all arguments are passed in a single argument - mobile args. I've added the webserviceName
		and method name as extra arguments for myself to debug... they could probably be gotten rid of at the end of the project. --->
 	<cffunction name="callWebService" access="remote" output="false" returntype="any">
		<cfargument name="mobileArgs" required="true">
		<cfargument name="webserviceName" required="false">
		<cfargument name="methodName" required="false">
	
		<cfset var result = "">
		<cfset var fullwebServicePath = "">
		<cfset var methodsecurityResult = "">
		<cfset var componentsecurityResult = structNew()>
		<cfset var mobileArguments = structNew()>
		
		<!--- NJH 2013/01/14 - have to do this for a new type of call the mobile is making. The arguments are coming through a structure with a key of 1. So, just move
			them into the base argument structure --->
		<cfif structKeyExists(arguments,"1") and isStruct(arguments["1"])>
			<cfset structAppend(arguments,arguments["1"])>
		</cfif>
		
		<cfset request.mobile = structNew()>
		<cfset request.mobile.validRequest = false>
		<cfset request.mobile.args = structNew()>
		
		<cfif isStruct(arguments.mobileArgs)>
			<cfset request.mobile.args = arguments.mobileArgs>
		<cfelse>
			<cfset request.mobile.args = arguments>
		</cfif>
	 	<cfif not structKeyExists(arguments,"webserviceName")>
		<cfset webserviceName = arguments.mobileArgs.webserviceName>
		 </cfif>
		<cfif not structKeyExists(arguments,"methodName")>
		<cfset methodName = arguments.mobileArgs.methodName>
		</cfif>
		
		<!--- NJH 2012/07/25 - Have to call refresh here (again!!!) as the username and password are passed as arguments to this function,
			but for some reason, they are not available in any other scope. So, when currentUser.refresh is called in application.cfc, the
			username and password are not available.
		 --->
		<cfset application.com.relayCurrentUser.refresh()>
		
		<!--- checkSecurity --->		
		<cfset componentSecurityResult = application.com.security.testFileSecurity(scriptname = "/mobile/webservices/#webservicename#.cfc")> 

		<cfif fileexists ("#application.paths.code#/mobile/webservices/#webservicename#.cfc" )>
			<cfset fullwebServicePath = "/code/mobile/webservices/#webservicename#">
		<cfelseif fileexists ("#application.paths.code#/cftemplates/mobile/webservices/#webservicename#.cfc" )>
			<cfset fullwebServicePath = "/code/cftemplates/mobile/webservices/#webservicename#">
		<cfelse>
			<cfset fullwebServicePath = "/mobile/webservices/#webservicename#">
		</cfif>

		<cfset mobileArguments = structCopy(request.mobile.args)>
		<cfset structDelete(mobileArguments,"webservicename")>
		<cfset structDelete(mobileArguments,"methodname")>		
		<cfset structDelete(mobileArguments,"_cf_nodebug")>
		<cfset structDelete(mobileArguments,"formstate")>
		<cfset structDelete(mobileArguments,"key")>
		<cfset structDelete(mobileArguments,"dateTimeStamp")>
		<cfif methodName neq "authenticate">
			<cfset structDelete(mobileArguments,"username")>
			<cfset structDelete(mobileArguments,"password")>
		</cfif>
		
		<!--- we want to check the metadata to look for security settings, so need to instantiate the object first and check the metadata before calling the method --->
		<cfobject
		    component = "#fullWebServicePath#"
		    name = "component">
		
		<cfset methodPointer = component[methodname]>
		<cfset methodsecurityResult = application.com.security.testCFCMethodSecurity(methodPointer = methodPointer)>

		<cfif not componentsecurityResult.isOK>
			<cfset result = createSecurityFailureResult("Please Login",methodPointer)>
		<cfelseif not methodSecurityResult.isOK>	
			<cfset result = createSecurityFailureResult(methodSecurityResult.message,methodPointer)>
			
		<!--- if it's a valid request, then run the method.--->
		<cfelseif request.mobile.validRequest>
		
			<cfset application.com.mobile.trackRequest(methodName=methodName,webserviceName=webserviceName,mobileArgs=mobileArguments)>
		
			<cfinvoke
			    component = "#component#"
			    method = "#methodname#"
			    returnVariable = "result"
			    argumentCollection = #mobileArguments#
			 >
		</cfif>
		
		<cfreturn result>
	</cffunction>

	<!--- 	
		WAB 2011/11	
		This function tries to give an appropriate format of result if there is a security failure, 
		looks at the method metadata to get the return type, 
		For a structure it assumes isOK and Message
		
		This idea may or may not work!
		More description on wiki
	--->

	<cffunction name="createSecurityFailureResult">
		<cfargument name="Message" required="true">
		<cfargument name="methodpointer" required="true">
		
		<cfset var result = "">
		<cfset var methodMetaData = getMetaData (methodPointer)>
			
			<cfif structKeyExists (methodMetaData,"returns") and methodMetaData.returns is "struct">
				<cfset result = {isok=false,message=message}>
			<cfelse>
				<cfset result = message>
			</cfif>

		<cfreturn result>
	
	</cffunction>


 </cfcomponent>

