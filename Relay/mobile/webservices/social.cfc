<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent output="false">

	<cffunction name="getActivityShares" access="remote" returnType="query">
		<cfargument name="startRow" type="numeric" default=1>
		<cfargument name="queryID" type="string" default="">

		<cfset var result = structNew()>
		<cfset var shareQry = "">
		<cfset var timeZoneInfo = application.com.dateFunctions.GetTimeZoneInfoDB()>
		<cfset var argStruct = {serviceConnections="organisation",queryID=arguments.queryID}>
		<cfset var plusMinus = "+">
		<cfset var hoursOffset = replace(replace(timeZoneInfo.utcHourOffset,"+",""),"-","")>
		<cfset var minutesOffset = replace(replace(timeZoneInfo.utcMinuteOffset,"+",""),"-","")>
		<cfset var communication = "">
		<cfset var content = "">
		
		<cfif timeZoneInfo.utcTotalOffset lt 0>
			<cfset plusMinus = "-">
		</cfif>
		<cfif minutesOffset eq 0>
			<cfset minutesOffset = "00">
		</cfif>
		
		<!--- bit of a hack here as I've changed the functionality of the getActivitiesForConnections after the mobile was built. Essentially,
			if a queryId is passed through, then we set the page number of 2 (so that it is not 1!) which means that we will iterate through the query.
			Otherwise, we assume it's a new query and then set the pageNumber to 1 --->
		<cfif arguments.queryID eq "">
			<cfset argStruct.pageNumber = 1>
		<cfelse>
			<cfset argStruct.pageNumber = 2>
		</cfif>
		<cfset result = application.com.social.getActivitiesForConnections(argumentCollection=argStruct)>
		<cfset shareQry = result.recordSet>
		
		<cfloop query="shareQry">
			<cfif activityStreamType eq "communicationMessage">
				<cfset communication = application.com.communications.getCommunication(commid=ID)>
				<cfset content = communication.getMergedContentForSinglePerson(personid=request.relayCurrentUser.personID,test=0)>
				<cfset querySetCell(shareQry,"shareText",content.message.text.phraseText,currentRow)>
			</cfif>	
		</cfloop>

		<!--- we used to return a first and lastname, but then we started using publicProfileName which was just a single field. Rather than Nick changing his code, we are just returning the 
			fullname in the firstname field, and setting the lastname to blank. We needed to do a QOQ here, as Nick had problems with the date being returned as a datetime. So, having to convert it as a string 
			We are also passing back the time off set. In theory, the mobile should pass us a locale which we then work out the dates, but this is the way it was asked for...
			--->
		<cfquery name="shareQry" dbtype="query">
			select cast(created as varchar) as created,shareText,shareUrl,publicProfileName as firstname,'' as lastname,pictureURL,following,personID,shareTitle,
				'#result.tablename#' as queryID,'#result.hasMore#' as hasMore, '#plusMinus#' as plusMinusOffset, #hoursOffset# as hoursOffset, '#minutesOffset#' as minutesOffset
				from shareQry
		</cfquery>
		
		<cf_translateQueryColumn query="#shareQry#" columnName="shareTitle" singlePhrase="false">
		
		<cfreturn shareQry>
		
	</cffunction>

</cfcomponent>