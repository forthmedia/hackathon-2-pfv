<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent output="false">

	<cffunction name="authenticate" output="false" returntype="query">
		<cfargument name="username" type="string" required="true">
		<cfargument name="password" type="string" required="true">
		
		<!--- no need to call authenticateUserV2 here again. It is already called in the mobile request authentication.. we are only here if
			we have passed the initial authentication... so, this is simply returning the user details in effect --->
		<cfset var result = queryNew("personID,countryDescription,currencySign,trainingManager,leadManager,threeTierProducts","integer,varchar,varchar,bit,bit,bit")>
		
		<cfset queryAddRow(result,1)>
		
		<cfset querySetCell(result,"personID",request.relayCurrentUser.personID,1)>
		
		<cfset qryPersonDetails = getPersonDetails(personId=request.relayCurrentUser.personID)> 
		
		<cfif qryPersonDetails.recordcount gt 0>
			<cfset querySetCell(result,"countryDescription",qryPersonDetails.countryDescription,1)>
			<cfset querySetCell(result,"currencySign",qryPersonDetails.currencySign,1)>
		</cfif>
		
		<cfset querySetCell(result,"trainingManager",application.com.relayElearning.isPersonTrainingContact(),1)>
		<cfset querySetCell(result,"leadManager",application.com.opportunity.isUserPortalLeadManager(),1)>
		<cfset querySetCell(result,"threeTierProducts",application.com.relayProduct.getProductCategories().recordCount,1)>
		
		<cfreturn result>
	</cffunction>
	
	<!--- 2012/06/14 PPB P-REL112 --->
	<cffunction name="getPersonDetails" access="private" output="false" returnType="query">
		<cfargument name="personID" required="true">
		
		<cfquery name="qryPersonDetails" datasource="#application.siteDatasource#">
			SELECT DISTINCT c.CountryDescription, ISNULL(cur.CurrencySign,'') AS CurrencySign
			FROM Person p with (noLock) 
			INNER JOIN Location l  with (noLock) ON l.LocationId=p.LocationId
			INNER JOIN Country c  with (noLock) ON c.CountryId=l.CountryId
				LEFT OUTER JOIN Currency cur with (noLock) ON cur.CurrencyISOCode=LEFT(c.CountryCurrency,3)
			WHERE p.PersonID = <cf_queryparam value="#arguments.personID#" cfsqltype="CF_SQL_INTEGER" >  
		</cfquery>
		
		<cfreturn qryPersonDetails>
	</cffunction>
	
	
	<cffunction name="getVersionDetails" access="public" output="false" returnType="query">
		
		<cfset var result = queryNew("version,isMobileOn,isLeadManagerOn,isTrainingManagerOn,isProductManagerOn,isSocialOn","varchar,bit,bit,bit,bit,bit")>
		<cfset var mobileSettings = application.com.settings.getSetting("mobile")>
		
		<cfset queryAddRow(result,1)>
		<cfset querySetCell(result,"version",application.instance.relaywareVersionInfo.version,1)>
		
		<cfset querySetCell(result,"isMobileOn",mobileSettings.enableMobile,1)>
		<cfset querySetCell(result,"isLeadManagerOn",mobileSettings.apps.leadManager,1)>
		<cfset querySetCell(result,"isTrainingManagerOn",mobileSettings.apps.trainingManager,1)>
		<cfset querySetCell(result,"isProductManagerOn",mobileSettings.apps.productManager,1)>
		<cfset querySetCell(result,"isSocialOn",application.com.settings.getSetting("socialMedia.enableSocialMedia"),1)>
		
		<cfreturn result>
	</cffunction>	
	
	
	<cffunction name="getSSOToken" output="false" access="public" returntype="string">		
		<cfargument name="siteURL" type="string" required="yes">
		<cfset var returnVar = "">		
			
		<cfset returnVar = "es=" & application.com.login.createSSOLink(siteURL=arguments.siteURL,personid=request.relaycurrentuser.personid)>					
		
		<cfreturn returnVar>		
	</cffunction>

</cfcomponent>