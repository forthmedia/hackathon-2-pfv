<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent extends="applicationMain" output="false">	
<!---
Have to extend the base applicationMain because we can't have an onRequest method when calling a cfc

RMB 2012-01-12 RMB - Added 'output = "False"' on cfcomponent and cffunction to remove white space for web services.
--->

	<cfsetting showdebugoutput="false">

	<cffunction name="onRequestStart" output="false">
		<cfargument name="thePage" >

		<cfset super.onRequestStart(thePage=arguments.thePage,suppressOutput=true)>

		<cfset getPageContext().getOut().clearBuffer()> 
		
	</cffunction>
</cfcomponent>
