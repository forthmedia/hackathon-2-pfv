<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent output="false">

	<cffunction name="getProductAssets" access="remote">
		<cfargument name="productID" type="numeric" required="true">
		
		<cfset var productAssets = application.com.relayProduct.getProductAssets(productID=arguments.productID)>
		<cfset var productAssetArray = arrayNew(1)>
		<cfset var getDistinctTypes = "">
		<cfset var getFilesWithType = "">
		
		<cfquery name="getDistinctTypes" dbtype="query">
			select distinct type from productAssets order by type
		</cfquery>
		
		<!--- don't push .swfs or .flvs to the mobile.. apparently Apple does not like them --->
		<cfloop query="getDistinctTypes">
			<cfquery name="getFilesWithType" dbtype="query">
				select * from productAssets where type='#getDistinctTypes.type#' 
				and (url not like '%.swf' and url not like '%.flv')
				order by name,url
			</cfquery>
			
			<cfif getFilesWithType.recordCount>
				<cfset arrayAppend(productAssetArray,getFilesWithType)>
			</cfif>
		</cfloop>
		
		<cfreturn productAssetArray>
	</cffunction>
	
	
	<cffunction name="getProductCategories" access="remote" returntype="query">

		<cfreturn application.com.relayProduct.getProductSelectorCategories()>
	</cffunction>


	<cffunction name="getProductGroups" access="remote" returntype="query">
		<cfargument name="productCategoryID" type="numeric" required="false">

		<cfreturn application.com.relayProduct.getProductSelectorGroups(productCategoryID=arguments.productCategoryID)>
	</cffunction>
	
	
	<cffunction name="getProducts" access="remote" returntype="query">
		<cfargument name="productGroupID" type="numeric" required="false">
		<cfargument name="productCategoryID" type="numeric" required="false">
		<cfargument name="productID" type="numeric" required="false">
		
		<cfreturn application.com.relayProduct.getProductSelectorProducts(argumentCollection=arguments)>
	</cffunction>
</cfcomponent>