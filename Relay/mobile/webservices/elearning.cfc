<!--- 
�Relayware. All Rights Reserved 2014 

File name : elearning.cfc

2014-02-04 PPB Case 438147 add VisitId to rating

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2016-05-12			atrunov     PROD2016-778 setting recordRights usage by default and configuring Elearning Visibility with multiple rules


 --->

<cfcomponent output="false">
	
	<cffunction name="mobileServiceCourses" output="false" returntype="query">
		<cfargument name="solIDs" default="" required="no">
		<cfargument name="courseIDs" default="" required="no">
		<cfargument name="moduleIDs" default="" required="no">
		
		<cfset var titlePhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "trngcourse", phraseTextID = "title")>
		<cfset var descPhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "trngcourse", phraseTextID = "description")>
		<cfset var courseList = "">
		<cfset var i = 0>
		<cfset var b = 0>
		<cfset var courseAvgQry = 0>
		<cfset var totalRatings = 0>
		<cfset var courseAvgArray = 0>
		<cfset var courseTRArray = 0>
		<cfset var solutionArray = ArrayNew(1)>		
		<cfset var courseStruct = StructNew()>

		<cfquery name="courseList" datasource = "#application.sitedatasource#">	
			select * from 			
				(select distinct  
					courseid,
					#titlePhraseQuerySnippets.select# as Title_Of_Course ,
					solutionAreaId,
					Solution_Area,
					#descPhraseQuerySnippets.select# as 'description',
					'#request.currentSite.protocolAndDomain#/content/linkImages/trngCourse/'+cast(courseID as varchar)+'/Thumb_'+cast(courseID as varchar)+'.png' as thumbnailURL,
					'#application.paths.content#\linkImages\trngCourse\'+ cast(courseID as varchar)+'\Thumb_'+cast(courseID as varchar)+'.png' as thumbnailPath
				from vTrngCourses v with (noLock)
					#preservesinglequotes(titlePhraseQuerySnippets.join)#
					#preservesinglequotes(descPhraseQuerySnippets.join)#								
				WHERE
		 			v.courseid IS NOT NULL AND
					v.mobileCompatible = 1 AND active = 1 AND
					v.courseid IN (SELECT m.courseid FROM vTrngModules m

        								left join dbo.getRecordRightsTableByPerson (#application.entityTypeID['trngCourse']#, <cf_queryparam value="#request.relayCurrentUser.personID#" CFSQLTYPE="cf_sql_integer">) as rrc on rrc.recordid = m.courseID
        								left join dbo.getRecordRightsTableByPerson (#application.entityTypeID['trngModule']#, <cf_queryparam value="#request.relayCurrentUser.personID#" CFSQLTYPE="cf_sql_integer">) as rrm on rrm.recordid = m.moduleID

									<!--- LEFT OUTER JOIN files as f on f.fileID = m.fileID
									LEFT OUTER JOIN filetype as ft on ft.filetypeID = f.filetypeID
									LEFT OUTER JOIN filetypegroup as fg on fg.filetypegroupid = ft.filetypegroupid
								WHERE
									(CASE
									WHEN (m.filename IS NULL  OR LTRIM(RTRIM(m.filename)) = '')
									THEN f.filename
									ELSE LTRIM(RTRIM(CONVERT(VARCHAR(100), f.fileid))) + '/' + LTRIM(RTRIM(m.filename)) END)
									LIKE '%.mp4' OR

									(CASE
									WHEN (m.filename IS NULL  OR LTRIM(RTRIM(m.filename)) = '')
									THEN f.filename
									ELSE LTRIM(RTRIM(CONVERT(VARCHAR(100), f.fileid))) + '/' + LTRIM(RTRIM(m.filename)) END)
									LIKE '%.html' OR
									(CASE
									WHEN (m.filename IS NULL  OR LTRIM(RTRIM(m.filename)) = '')
									THEN f.filename
									ELSE LTRIM(RTRIM(CONVERT(VARCHAR(100), f.fileid))) + '/' + LTRIM(RTRIM(m.filename)) END)
									LIKE '%.htm'
									 --->
								WHERE dbo.dateAtMidnight(m.publishedDate) <= getDate()
                                AND (rrc.level1 = 1 or ((m.courseHasRecordRightsBitMask & 1) = 0))
                                AND (rrm.level1 = 1 or ((m.moduleHasRecordRightsBitMask & 1) = 0))
								AND m.Active = 1 <!--- 2013-04-30 STCR CASE 432193 - do not return Modules before their Activation Date --->
									)
			<cfif solIDs IS NOT '' AND solIDs IS NOT 'notset'>
				AND	v.courseid  IN  (SELECT courseid FROM trngCourse WHERE solutionAreaId IN (<cf_queryparam cfsqltype="CF_SQL_INTEGER" value="#solIDs#" list="true">))
			</cfif>
	
			<cfif courseIDs IS NOT '' AND courseIDs IS NOT 'notset'>
				 AND
				<cfset i = 0>
				(<cfloop index = "ListElement" list = "#courseIDs#" delimiters = ","> 
					<cfset i++>
					v.courseid = <cf_queryparam cfsqltype="CF_SQL_INTEGER" null="false" value="#ListElement#">
					<cfif i LT ListLen(courseIDs)>	OR </cfif>	
				</cfloop>)
			</cfif>
			
			<cfif moduleIDs IS NOT '' AND moduleIDs IS NOT 'notset'>
				AND	v.courseid  IN  (SELECT courseid FROM trngModuleCourse with(noLock) WHERE moduleID IN (<cf_queryparam cfsqltype="CF_SQL_INTEGER" value="#moduleIDs#" list="true">)
			</cfif>
						
			) as x			
			order by Solution_Area, Title_Of_Course
		</cfquery>
		
		<cfoutput query="courseList" group="solutionAreaId">
			<cfset b = b + 1>
			<cfset courseStruct = StructNew()>
			<cfset UsersolutionAreaId = solutionAreaId>
			<cfset i = 0>
			
			<cfloop query="courseList">
				<cfif courseAvgQry EQ ''>
					<cfset courseAvgQry = 0>
				</cfif>

				<cfif totalRatings EQ ''>
					<cfset totalRatings = 0>
				</cfif>
					
				<cfif UsersolutionAreaId EQ solutionAreaId>
					<cfset i = i  + 1>

					<cfif not fileExists(thumbnailPath)>
						<cfset querySetCell(courseList,"thumbnailPath","",currentRow)>
						<cfset querySetCell(courseList,"ThumbnailUrl","",currentRow)>
					</cfif>	

					<cfscript>
						coursenameid = "course#i#";
						courseStruct[coursenameid] = {courseid = courseid,
											Title_Of_Course = Title_Of_Course,
											solutionAreaId = solutionAreaId,
											Solution_Area = Solution_Area,
											description = description,
											courseAvg = courseAvgQry,
											totalRatings = totalRatings,
											thumbnailURL = thumbnailURL};
						
						courseStruct[coursenameid] = application.com.structureFunctions.structToQuerySimple(struct=courseStruct[coursenameid]);
					</cfscript>
				</cfif>
			</cfloop>
			
			<cfscript>
				solutionArray[b] = {key = Solution_Area, courses = application.com.structureFunctions.structToQuerySimple(struct=courseStruct)};		
			</cfscript>	
		</cfoutput>

		<cfreturn application.com.structureFunctions.arrayOfStructuresToQuery(solutionArray)>

	</cffunction>
	
	<cffunction name="mobileServiceMod" output="false" returntype="any">
		<cfargument name="courseID" type="numeric" required="true">
		
		<cfset var moduleArea = StructNew()>
		<cfset var moduleList = "">
		<cfset var i = 0>
		<cfset var rating = "">
		<cfset var full = "">
		<cfset var moduleTitlePhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "trngModule", phraseTextID = "title")>
		<cfset var courseTitlePhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "trngCourse", phraseTextID = "title")>
		<cfset var thumbnailPath = "">

		<cfquery name="moduleList" datasource = "#application.sitedatasource#">
			select * from 
					(select distinct 
					#moduleTitlePhraseQuerySnippets.select# as Title_Of_Module,
					#courseTitlePhraseQuerySnippets.select# as Title_Of_Course,
				    Module_Type,
					v.Filename,
					Maximum_Score,
					Maximum_Time_Allowed,
					Module_Code,
					v.moduleid,
					courseid,
					duration,
					Associated_Quiz,
					v.SortOrder,
					v.fileid,
					points,
					active,
					availability,
					publishedDate,
				    'content/' + fT.path + '/' + (CASE
					WHEN (v.filename IS NULL  OR LTRIM(RTRIM(v.filename)) = '')
					THEN f.filename
					ELSE LTRIM(RTRIM(CONVERT(VARCHAR(100), f.fileid))) + '/' + LTRIM(RTRIM(v.filename)) END) as fileNameAndPath,
					fT.secure,
					f.DeliveryMethod,
					solutionAreaId,
					Solution_Area,
					
				   (Sum(r.Rating)*1.0/(count(r.personID))) as avgRating,
					FLOOR((Sum(r.Rating)*1.0/(count(r.personID)))) as fullStars,
					count(r.personID) as totalRatings,
					'/content/linkImages/trngModule/'+cast(v.moduleID as varchar)+'/Thumb_'+cast(v.moduleID as varchar)+'.png' as thumbnailURL
					<!--- '#application.paths.content#\linkImages\trngModule\'+ cast(v.moduleID as varchar)+'\Thumb_'+cast(v.moduleID as varchar)+'.png' as thumbnailPath --->
				from vTrngModules v

                        left join dbo.getRecordRightsTableByPerson (#application.entityTypeID['trngCourse']#, <cf_queryparam value="#request.relayCurrentUser.personID#" CFSQLTYPE="cf_sql_integer">) as rrc on rrc.recordid = v.courseID
                        left join dbo.getRecordRightsTableByPerson (#application.entityTypeID['trngModule']#, <cf_queryparam value="#request.relayCurrentUser.personID#" CFSQLTYPE="cf_sql_integer">) as rrm on rrm.recordid = v.moduleID

					#preservesinglequotes(moduleTitlePhraseQuerySnippets.join)#
					#preservesinglequotes(courseTitlePhraseQuerySnippets.join)#
					LEFT OUTER JOIN files f with(noLock) on f.fileID = v.fileID
					LEFT OUTER JOIN filetype ft with(noLock) on ft.filetypeID = f.filetypeID
					LEFT OUTER JOIN filetypegroup fg with(noLock) on fg.filetypegroupid = ft.filetypegroupid
					LEFT OUTER JOIN rating r with(noLock) ON r.entityId = v.moduleId and r.entityTypeID=<cf_queryparam value="#application.entityTypeID.trngModule#" cfsqltype="CF_SQL_INTEGER">

			WHERE v.moduleID IS NOT NULL
                AND (rrc.level1 = 1 or ((v.courseHasRecordRightsBitMask & 1) = 0))
                AND (rrm.level1 = 1 or ((v.moduleHasRecordRightsBitMask & 1) = 0))
				AND	v.mobileCompatible = 1
				AND v.courseId = <cf_queryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.courseId#">
			GROUP BY
					#moduleTitlePhraseQuerySnippets.select#,
					#courseTitlePhraseQuerySnippets.select#,
				    Module_Type,
					v.Filename,
					Maximum_Score,
					Maximum_Time_Allowed,
					Module_Code,
					v.moduleid,
					courseid,
					duration,
					Associated_Quiz,
					v.SortOrder,
					v.fileid,
					points,
					active,
					availability,
					publishedDate,
				    'content/' + fT.path + '/' + (CASE
					WHEN (v.filename IS NULL  OR LTRIM(RTRIM(v.filename)) = '')
					THEN f.filename
					ELSE LTRIM(RTRIM(CONVERT(VARCHAR(100), f.fileid))) + '/' + LTRIM(RTRIM(v.filename)) END),
					fT.secure,
					f.DeliveryMethod,
					solutionAreaId,
					Solution_Area
				) as x	
						
				where 1=1
				and active = 1
				AND dbo.dateAtMidnight(publishedDate) <= getDate() <!--- 2013-04-30 STCR CASE 432193 - do not return Modules before their Activation Date --->
				
						order by Title_Of_Course
		</cfquery>
		
		<cfloop query="moduleList">
			<cfset thumbnailPath = application.paths.content&replaceNoCase(replace(thumbnailURL,"/","\","ALL"),"\content","")>
			<cfif not fileExists(thumbnailPath)>
				<cfset querySetCell(moduleList,"ThumbnailUrl","",currentRow)>
			<cfelse>
				<cfset querySetCell(moduleList,"ThumbnailUrl","#request.currentSite.protocolAndDomain##thumbnailURL#",currentRow)>
			</cfif>
		</cfloop>

<!--- 2013/02/25 GCC removed filter as mobile is ok to handle all training types now out to the device browser 
				AND (fileNameAndPath LIKE '%mp4' OR
					fileNameAndPath LIKE '%HTML' OR
					fileNameAndPath LIKE '%HTM') --->
		<cfreturn moduleList>

	</cffunction>
	
	
	<cffunction name="mobileServiceFileRating" output="false" returntype="query">
		<cfargument name="moduleID" required="yes">
		
		<cfset var moduleArea = StructNew()>
		<cfset var moduleList = "">
		<cfset var i = 0>
		<cfset var rating = "">
		<cfset var full = "">

		<cfquery name="moduleList" datasource = "#application.sitedatasource#">
		select active, fileNameAndPath, avgRating, fullStars, totalRatings from 
					(select distinct 
					v.active, v.publishedDate,
				    'content/' + fT.path + '/' + (CASE
					WHEN (v.filename IS NULL  OR LTRIM(RTRIM(v.filename)) = '')
					THEN f.filename
					ELSE LTRIM(RTRIM(CONVERT(VARCHAR(100), f.fileid))) + '/' + LTRIM(RTRIM(v.filename)) END) as fileNameAndPath,
				   (Sum(r.Rating)*1.0/(count(r.personID))) as avgRating,
					FLOOR((Sum(r.Rating)*1.0/(count(r.personID)))) as fullStars,
					count(r.personID) as totalRatings
					 
				from vTrngModules v

                        left join dbo.getRecordRightsTableByPerson (#application.entityTypeID['trngCourse']#, <cf_queryparam value="#request.relayCurrentUser.personID#" CFSQLTYPE="cf_sql_integer">) as rrc on rrc.recordid = v.courseID
                        left join dbo.getRecordRightsTableByPerson (#application.entityTypeID['trngModule']#, <cf_queryparam value="#request.relayCurrentUser.personID#" CFSQLTYPE="cf_sql_integer">) as rrm on rrm.recordid = v.moduleID

					LEFT OUTER JOIN files f with(noLock) on f.fileID = v.fileID
					LEFT OUTER JOIN filetype ft with (noLock) on ft.filetypeID = f.filetypeID
					LEFT OUTER JOIN filetypegroup fg with (noLock) on fg.filetypegroupid = ft.filetypegroupid
					LEFT OUTER JOIN rating r with (noLock) ON r.entityID = v.moduleId and r.entityTypeID = <cf_queryparam value="#application.entityTypeId.trngModule#" cfsqltype="CF_SQL_INTEGER">

		WHERE v.moduleID IS NOT NULL
        AND (rrc.level1 = 1 or ((v.courseHasRecordRightsBitMask & 1) = 0))
        AND (rrm.level1 = 1 or ((v.moduleHasRecordRightsBitMask & 1) = 0))
        AND v.mobileCompatible = 1
		
		<cfif moduleID IS NOT '' AND moduleID IS NOT 'notset'>
			AND
			<cfset i = 0>
			(
				<cfset i++>
				v.moduleID = <cf_queryparam cfsqltype="CF_SQL_INTEGER" null="false" value="#moduleID#">
			)
		</cfif>
		GROUP BY
					v.active, v.publishedDate,
				    'content/' + fT.path + '/' + (CASE
					WHEN (v.filename IS NULL  OR LTRIM(RTRIM(v.filename)) = '')
					THEN f.filename
					ELSE LTRIM(RTRIM(CONVERT(VARCHAR(100), f.fileid))) + '/' + LTRIM(RTRIM(v.filename)) END)					
					) as x	
					
				where 1=1
				and active = 1 AND dbo.dateAtMidnight(publishedDate) <= getDate() <!--- 2013-04-30 STCR CASE 432193 - do not return Modules before their Activation Date --->
				AND (fileNameAndPath LIKE '%.mp4' OR
					fileNameAndPath LIKE '%.HTML' OR
					fileNameAndPath LIKE '%.HTM')
		</cfquery>

		<cfreturn moduleList>

	</cffunction>
	
	<cffunction name="mobileServiceModRated" output="false" returntype="query">
		<cfargument name="startrow" default="1" required="yes">
		<cfargument name="endRow" default="25" required="yes">
		
		<cfset var moduleArea = StructNew()>
		<cfset var moduleList = "">
		<cfset var i = 0>
		<cfset var rating = "">
		<cfset var full = "">
		<cfset var columnlist = "OrderOfModule, OutOfModules, Title_Of_Module, Title_Of_Course, Module_Type, Filename, Maximum_Score, Maximum_Time_Allowed, Module_Code, moduleid, courseid, duration, Associated_Quiz, SortOrder, fileid, points, active, availability, publishedDate, fileNameAndPath, secure, DeliveryMethod, solutionAreaId, Solution_Area, avgRating, fullStars, totalRatings">
		<cfset var topmods = QueryNew(columnlist)>
		<cfset var moduleTitlePhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "trngModule", phraseTextID = "title")>
		<cfset var courseTitlePhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "trngCourse", phraseTextID = "title")>
		<cfset var moduleByRating = "">

		<cfquery name="moduleByRating" datasource = "#application.sitedatasource#">
			select * from 
					(select distinct
					#moduleTitlePhraseQuerySnippets.select# as Title_Of_Module,
					#courseTitlePhraseQuerySnippets.select# as Title_Of_Course,
				    Module_Type,
					v.Filename,
					Maximum_Score,
					Maximum_Time_Allowed,
					Module_Code,
					v.moduleid,
					courseid,
					duration,
					Associated_Quiz,
					v.SortOrder,
					v.fileid,
					points,
					active,
					availability,
					publishedDate,
				    'content/' + fT.path + '/' + (CASE
					WHEN (v.filename IS NULL  OR LTRIM(RTRIM(v.filename)) = '')
					THEN f.filename
					ELSE LTRIM(RTRIM(CONVERT(VARCHAR(100), f.fileid))) + '/' + LTRIM(RTRIM(v.filename)) END) as fileNameAndPath,
					fT.secure,
					f.DeliveryMethod,
					solutionAreaId,
					Solution_Area,
					
				   (Sum(r.Rating)*1.0/(count(r.personID))) as avgRating,
					FLOOR((Sum(r.Rating)*1.0/(count(r.personID)))) as fullStars,
					count(r.personID) as totalRatings
					 
				from vTrngModules v

                        left join dbo.getRecordRightsTableByPerson (#application.entityTypeID['trngCourse']#, <cf_queryparam value="#request.relayCurrentUser.personID#" CFSQLTYPE="cf_sql_integer">) as rrc on rrc.recordid = v.courseID
                        left join dbo.getRecordRightsTableByPerson (#application.entityTypeID['trngModule']#, <cf_queryparam value="#request.relayCurrentUser.personID#" CFSQLTYPE="cf_sql_integer">) as rrm on rrm.recordid = v.moduleID

					#preserveSingleQuotes(moduleTitlePhraseQuerySnippets.join)#
					#preserveSingleQuotes(courseTitlePhraseQuerySnippets.join)#
					LEFT OUTER JOIN files f with (noLock) on f.fileID = v.fileID
					LEFT OUTER JOIN filetype ft with (noLock) on ft.filetypeID = f.filetypeID
					LEFT OUTER JOIN filetypegroup fg with (noLock) on fg.filetypegroupid = ft.filetypegroupid
					LEFT OUTER JOIN rating r with (noLock) ON r.entityId = v.moduleId and r.entityTypeId = <cf_queryparam value="#application.entityTypeID.trngModule#" cfsqltype="CF_SQL_INTEGER">

		WHERE v.moduleID IS NOT NULL
        AND (rrc.level1 = 1 or ((v.courseHasRecordRightsBitMask & 1) = 0))
        AND (rrm.level1 = 1 or ((v.moduleHasRecordRightsBitMask & 1) = 0))
        AND v.mobileCompatible = 1

		GROUP BY
					#moduleTitlePhraseQuerySnippets.select#,
					#courseTitlePhraseQuerySnippets.select#,
				    Module_Type,
					v.Filename,
					Maximum_Score,
					Maximum_Time_Allowed,
					Module_Code,
					v.moduleid,
					courseid,
					duration,
					Associated_Quiz,
					v.SortOrder,
					v.fileid,
					points,
					active,
					availability,
					publishedDate,
				    'content/' + fT.path + '/' + (CASE
					WHEN (v.filename IS NULL  OR LTRIM(RTRIM(v.filename)) = '')
					THEN f.filename
					ELSE LTRIM(RTRIM(CONVERT(VARCHAR(100), f.fileid))) + '/' + LTRIM(RTRIM(v.filename)) END),
					fT.secure,
					f.DeliveryMethod,
					solutionAreaId,
					Solution_Area
					
					) as x	
					
				where 1=1
				and active = 1 AND dbo.dateAtMidnight(publishedDate) <= getDate() <!--- 2013-04-30 STCR CASE 432193 - do not return Modules before their Activation Date --->
				AND (fileNameAndPath LIKE '%mp4' OR
					fileNameAndPath LIKE '%HTML' OR
					fileNameAndPath LIKE '%HTM')
				ORDER BY avgRating DESC, fullStars DESC, totalRatings DESC, Title_Of_Module
		</cfquery>
	
		<cfloop query="moduleByRating" startrow="#startrow#" endRow="#endRow#">
			<cfset i = i + 1>
		
			<cfscript>		
				QueryAddRow(topmods);
				QuerySetCell(topmods, "OrderOfModule", modulebyrating.currentrow );
				QuerySetCell(topmods, "OutOfModules", modulebyrating.recordcount );
				QuerySetCell(topmods, "Title_Of_Module", Title_Of_Module );
				QuerySetCell(topmods, "Title_Of_Course", Title_Of_Course );
				QuerySetCell(topmods, "Module_Type", Module_Type );
				QuerySetCell(topmods, "Filename", Filename );
				QuerySetCell(topmods, "Maximum_Score", Maximum_Score );
				QuerySetCell(topmods, "Maximum_Time_Allowed", Maximum_Time_Allowed );
				QuerySetCell(topmods, "Module_Code", Module_Code );
				QuerySetCell(topmods, "moduleid", moduleid );
				QuerySetCell(topmods, "courseid", courseid );
				QuerySetCell(topmods, "duration", duration );
				QuerySetCell(topmods, "Associated_Quiz", Associated_Quiz );
				QuerySetCell(topmods, "SortOrder", SortOrder );
				QuerySetCell(topmods, "fileid", fileid );
				QuerySetCell(topmods, "points", points );
				QuerySetCell(topmods, "active", active );
				QuerySetCell(topmods, "availability", availability );
				QuerySetCell(topmods, "publishedDate", publishedDate );
				QuerySetCell(topmods, "fileNameAndPath", fileNameAndPath );
				QuerySetCell(topmods, "secure", secure );
				QuerySetCell(topmods, "DeliveryMethod", DeliveryMethod );
				QuerySetCell(topmods, "solutionAreaId", solutionAreaId );
				QuerySetCell(topmods, "Solution_Area", Solution_Area );
				QuerySetCell(topmods, "avgRating", avgRating );
				QuerySetCell(topmods, "fullStars", fullStars );
				QuerySetCell(topmods, "totalRatings", totalRatings );
			</cfscript>
			
			<cfif i GTE endRow><cfbreak></cfif>
		
		</cfloop>
		
		<cfreturn topmods>

	</cffunction>
	
	<cffunction name="rateModule" access="public" output="false" returnType="string">
		<cfargument name="moduleId" type="numeric" required="true">
		<cfargument name="rating" type="numeric" required="true">
		
		<cfset var moduleRating=structNew()>
		<cfset var result = structNew()>

		<!--- START 2014-02-04 PPB Case 438147 save visitid BUT don't save rating if visitId=0 ie done by a bot --->
		<!--- 2014-07-16 PPB Case 440959 removed from mobile version as visitid is always 0--->
		<!--- <cfif request.relayCurrentUser.visitID eq 0>		
			<cfset result.entityID=0>
		<cfelse> --->
			<cfset moduleRating.entityTypeID = application.entityTypeID.trngModule>
			<cfset moduleRating.entityID = arguments.moduleID>
			<cfset moduleRating.personID = request.relayCurrentUser.personID>
			<cfset moduleRating.rating = arguments.rating>
			<cfset moduleRating.visitID = request.relayCurrentUser.visitID>		
			
			<cfset result = application.com.relayEntity.insertEntity(entityDetails=moduleRating,entityTypeID=application.entityTypeID.rating,insertIfExists=true)>
		<!--- </cfif> --->
		<!--- END 2014-02-04 PPB Case 438147 --->
		
		<cfreturn result.entityID>
	</cffunction>
	
	
	<cffunction name="getModuleRating" access="public" output="false" returnType="query">
		<cfargument name="moduleId" type="numeric" required="true">
		<cfargument name="personId" type="numeric" required="false" default="#request.relayCurrentUser.personID#">
		
		<cfset var qryGetModuleRating = "">
		
		<cfquery name="qryGetModuleRating" datasource="#application.siteDatasource#">
			SELECT rating 
			FROM  rating
			WHERE entityID=<cf_queryparam value="#arguments.moduleID#" cfsqltype="CF_SQL_INTEGER"> 
			AND personID=<cf_queryparam value="#arguments.personID#" cfsqltype="CF_SQL_INTEGER">
			AND entityTypeID = <cf_queryparam value="#application.entityTypeId.trngModule#" cfsqltype="CF_SQL_INTEGER"> 
		</cfquery>
			
		<cfreturn qryGetModuleRating>	<!--- deliberately dont return a struct; may as well be a query for compatibility --->
	</cffunction>

</cfcomponent>