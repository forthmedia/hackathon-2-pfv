<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent output="false">

	<cffunction name="getNewsHeadlines" access="public" hint="Returns news headlines" returntype="query">
	
		<cfset var userTree = "">
		<cfset var getNews = queryNew("ID,headline,summary")>
	
		<cftry>
			<cfset userTree = application.com.relayElementTree.getTreeForCurrentUser(topElementID=request.currentsite.elementTreeDef.TopID)>
			<cfset getNews = application.com.relayElementTree.getElementsFlaggedWithParticularFlag(elementTree=userTree,flagID="WhatsNewElementFlag",demoteVisitedElements=true)>
				
			<cfquery name="getNews" dbtype="query">
				select distinct ID,headline,'phr_summary_element_'+cast(ID as varchar) as summary
				from getNews
				order by visited, overallweighting desc
			</cfquery>
			
			<cf_translateQueryColumn query = #getNews# columnName="headline,summary" onNullShowPhraseTextID = false>
			
			<cfcatch>
				<cfset application.com.errorHandler.recordRelayError_Warning(type="Mobile",Severity="error",catch=cfcatch,WarningStructure=arguments)>
			</cfcatch>
		</cftry>
		
		<cfreturn getNews>
		
	</cffunction>
	
	
	<cffunction name="getNewsContent" access="public" hint="Returns news content" returntype="string">
		<cfargument name="contentID" type="numeric" required="true">
		
		<cfset var content = "">
		<cfset var phraseTextID = "phr_detail_element_#arguments.contentID#">
		
		<cfset request.currentElementTree = application.com.relayElementTree.getTreeForCurrentUser(topElementID=contentID)>
		<cfset request.CurrentElement = application.com.structureFunctions.queryRowToStruct(request.CurrentElementTree,1)>
		
		<cftry>
			<cfset content = application.com.relayTranslations.translatePhrase(phrase=phraseTextID)>
			
			<cfcatch>
				<cfset content = application.com.relayTranslations.translatePhrase(phrase=phraseTextID,evaluateVariables=false)>
				<cfset application.com.errorHandler.recordRelayError_Warning(type="Mobile",Severity="error",catch=cfcatch,WarningStructure=arguments)>
			</cfcatch>
		</cftry>
		
		<cfreturn content>
	</cffunction>
	
</cfcomponent>