<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 

A page to Poll to check that the server is up

Date 2001-06-22

--->
<cfsetting enablecfoutputonly="no" showdebugoutput="No">

<cfoutput>**UP**  (IIS)<BR></cfoutput>

<cftry>
	<cfquery name="test" datasource="#application.siteDataSource#">
		select top 10 countryID from country
	</cfquery>

	<!--- 2007-05-07 SWJ added this test as it throws an error if the temDB log is full. 
		without it, amIup returns OK but meanwhile Relayware cannot function --->
	<cfquery name="tempDBTest" datasource="#application.siteDataSource#">
		exec sp_databases
	</cfquery>
	
	<cfif test.recordcount gt 1>
	**SQLUP<BR>
	</cfif>

	<cfcatch type="Any">
		**SQLERROR<BR>
		<cfoutput>#htmleditformat(cfcatch.message)#</cfoutput>
	</cfcatch>
</cftry>	












