<!--- �Relayware. All Rights Reserved 2014 --->
<!--- NJH/WAB 2006/11/06  - using the border set rather than copy code from the borderset

This doesn't handle framesets at the moment. Going forward we need to allow for sites/pages using framesets (for example: Sony)
 --->

<cf_translate>
<!--- GCC - May be premature if this isn't used everywhere yet but...--->
<CF_DisplayBorder borderset="#request.currentsite.borderset#">
	<cfoutput>
		<table border="0" align="center">
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td><h1>phr_Sys_Page_Not_Found.</h1></td>
			</tr>
			<tr>
				<td>phr_Sys_Please_try_our <a href="#request.currentSite.httpProtocol##cgi.SERVER_NAME#" target="_top" title="Link to homepage">phr_Sys_homepage</a>.</td>
			</tr>					
			<tr>
				<td>&nbsp;</td>
			</tr>				
		</table>
	</cfoutput>
</CF_DisplayBorder>
</cf_translate>


