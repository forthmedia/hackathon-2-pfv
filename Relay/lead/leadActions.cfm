<cfset rights = application.com.rights.getRightsFilterWhereClause(entityType="Lead",alias="l")>
<cfset leadID=structKeyExists(url,"leadID")?url.leadID:form.leadID>

<cfquery name="qLead">
		select leadID
		from dbo.lead l
			inner join country c on l.countryID = c.countryID
			<cfif len(trim(rights.join)) GT 0>
				#rights.join#
			</cfif>
		where 1=1 AND
			convertedOpportunityID IS NULL
			AND leadID = <cf_queryparam cfsqltype="cf_sql_integer" value="#leadID#">
			<cfif len(trim(rights.whereClause)) GT 0>
			#rights.whereClause#
			</cfif>

</cfquery>


<!--- NJH Jira Fifteen-162 Add the links to the slideHeader --->
<cfif request.relayCurrentUser.isInternal>
	<cfif qLead.recordCount GT 0>
	<script>
		jQuery(document).ready(function() {
			jQuery('#slideHeader ul').append('<li><a id="convertLink" href="#">Convert</a></li><li><a id="deleteLink" href="#">Delete</a></li>')
		})
	</script>
	<cfelse>
	<script>
		jQuery(document).ready(function() {
			//2015-10-07	ACPK 	PROD2015-107 Disable Save link on reopen if lead already converted
			jQuery('#slideHeader a.Submenu').addClass('disabled');
			jQuery('#slideHeader a.Submenu').prop('onclick',null).off('click');
		})
	</script>
	</cfif>
</cfif>
