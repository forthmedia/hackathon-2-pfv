<!--- �Relayware. All Rights Reserved 2014 --->
<!---
2015-02-03	RPW	JIRA 2015 RoadmapFIFTEEN-84 Added translations for column headers and changed elementTextID of leads page to leads
2015-02-05	RPW	JIRA 2015 RoadmapFIFTEEN-84 Changed template to include /lead/leads.cfm for editing rather than moving to the leads page.
2015-04-08	MS	P-AER001  Wrap code with CFOUTPUT so that backToList and preSubmit JS functions work with correct vars as they were not returning values. Also extra HASH in front of inline styles frmRowIdentity, actionContainer, save and delete as they started breaking after wrapping code with CFOUTPUT
2016-09-25  DAN PROD2016-2387 - filter leads by type (new tag parameter)
2016-10-26	RJT	PROD2016-2587 Allowed the Relay_SHOWPARTNERLEADS tag to select TFQO columns to display
 --->

<cfparam name="sortOrder" default="LeadID DESC">
<cfparam name="variables.numRowsPerPage" default="50">
<cfparam name="variables.useInclude" default="false">
<cfparam name="variables.keyColumnList" type="string" default="Company">
<cfparam name="variables.keyColumnURLList" type="string" default="##">
<cfparam name="variables.keyColumnKeyList" type="string" default="">
<cfparam name="variables.keyColumnOnCLickList" type="string" default="javascript:editLead('##application.com.security.encryptVariableValue(name='leadID'*commavalue=leadID)##');return false">
<cfparam name="variables.showTheseColumns" type="string" default="leadID,country,Company,name,email,acceptedByPartner,leadApprovalStatus,PartnerSalesPerson,created">
<cfparam name="variables.hideTheseColumns" type="string" default="entityID,vendorAccountManagerPersonID,stageID">
<cfparam name="variables.allowColumnSorting" default="yes">
<cfparam name="variables.columnTranslation" default="true">
<cfparam name="variables.columnTranslationPrefix" default="phr_lead_">
<cfparam name="variables.showAddButton" default="false">
<!--- <cfparam name="variables.showLocLevel" default="false"> --->

<cf_includeJavascriptOnce template ="/lead/js/leadPortal.js">

<!--- Fallback if keyColumnList isn't in selected showTheseColumns RJT --->
<cfif listFind(variables.showTheseColumns,variables.keyColumnList) EQ 0>
	<cfset variables.keyColumnList=ListGetAt(variables.showTheseColumns,1)>
</cfif>

<cfoutput>
<!--- 2015-02-03	RPW	JIRA 2015 RoadmapFIFTEEN-84 Changed elementTextID of leads page to leads --->
<!--- 2015-02-05	RPW	JIRA 2015 RoadmapFIFTEEN-84 Changed template to include /lead/leads.cfm for editing rather than moving to the leads page. --->
<script type="text/javascript">
	function editLead(leadID) {
		//2015-04-29	MS		We also need to pass in ADD=NO to make sure that the code knows what to do oe either show a listing screen or the edit form
		location.href = location.href+'&editor=yes&add=no&leadID='+leadID;
	}

	function addNew() {
		location.href = location.href+'&editor=yes&add=yes';
	}

	function backToList() {
		location.href = '/?eid=#request.currentElement.id#';
	}

	function preSubmit(btn) {
		form = document.editorForm;

                //2014-04-14	MS		We also need to pass in ADD=YES to make sure that the code knows what to do oe either show a listing screen or the edit form
		jQuery(form).attr("action","/?eid=#request.currentElement.id#&editor=yes&add=yes")
		FormSubmit(btn);
	}
</script>

<cfif structKeyExists(url,"leadID")>
	<cfset leadID=url.leadID>
</cfif>
<cfif structKeyExists(form,"leadID")>
	<cfset leadID=form.leadID>
</cfif>

<!--- 2015-02-05	RPW	JIRA 2015 RoadmapFIFTEEN-84 Changed template to include /lead/leads.cfm for editing rather than moving to the leads page. --->
<cfif isDefined('leadID') and leadID NEQ 0 and application.com.leads.leadIsConverted(leadID)>
	<p>phr_leadApprovalStatusID_Converted</p>
<cfelseif StructKeyExists(URL,"editor")>

	<style>
		##frmRowIdentity{
			position: relative;
		}
		##actionContainer{
			float:right
		}
		##save{
			float:left;
			padding-right:10px
		}
		##delete{
			float:right
		}
	</style>

	<cf_input type="button" value="Back" onClick="backToList();">
	<cfset variables.showAccountLeads=true>
	<cfinclude template="/lead/leads.cfm">
	<cfif structKeyExists(url,"editor")>
		<cfoutput>
		<div id="actionContainer">
			<div id="save" class="form-group">
				<cf_input type="button" id="saveButton" value="phr_Save" onClick="preSubmit(this);">
				<cfif NOT structKeyExists(url,'add') or url.add EQ "no">
					<!--- only convert/delete existing leads--->
					<cfif application.com.settings.getSetting("leadManager.convertLeadOnPortal") AND application.com.leads.leadCanBeConverted(leadID)  AND application.com.leads.personCanConvertLead()>
						<cf_input type="button" id="portalConvertLink" value="phr_Convert">
					</cfif>

					<cf_input type="button" id="deleteLink"  value="phr_delete">
				</cfif>
			</div>
		</div>
		</cfoutput>
	</cfif>

<cfelse>

	<cfif variables.showAddButton>
		<p><cf_input type="button" value="Add" onClick="addNew();"></p>
	</cfif>

	<cfif fileexists("#application.paths.code#\cftemplates\Modules\LeadManager\customLeadListPartner.cfm")>
		<cfinclude template="/code/cftemplates/Modules/LeadManager/customLeadListPartner.cfm">
		<cf_abort>
	</cfif>

	<cfscript>
		/*if (variables.showLocLevel) {

			if (!IsDefined("variables.partnerLocationID") || (IsDefined("variables.partnerLocationID") && !variables.partnerLocationID)) {
				variables.partnerLocationID = request.relayCurrentUser.locationID;
			}

			variables.filter = [];
			ArrayAppend(variables.filter,"partnerLocationID = " & variables.partnerLocationID);
			variables.queryObject = application.com.relayLeads.GetLeads(resultType="data",sortOrder=sortOrder,filter=filter,leadTypeID=leadTypeID);

		} else {

			variables.queryObject = application.com.relayLeads.GetOrganisationLeads(organisationID=request.relayCurrentUser.organisationID,sortOrder=sortOrder,leadTypeID=leadTypeID);

		}*/
		/* NJH 2016/11/28 JIRA PROD2016-2642 - at the moment, getLeads function handles this and we don't have to apply the logic in here*/
		variables.queryObject = application.com.relayLeads.GetLeads(resultType="data",sortOrder=sortOrder,leadTypeID=leadTypeID);
	</cfscript>

	<cfif variables.queryObject.recordcount gt 0>
		<CF_tableFromQueryObject
			queryObject="#variables.queryObject#"
			sortOrder = "#sortOrder#"
			numRowsPerPage="#variables.numRowsPerPage#"
			useInclude = "#variables.useInclude#"
			keyColumnList="#variables.keyColumnList#"
			keyColumnURLList="#variables.keyColumnURLList#"
			keyColumnKeyList="#variables.keyColumnKeyList#"
			keyColumnOnClickList="#variables.keyColumnOnCLickList#"
			showTheseColumns="#variables.showTheseColumns#"
			hideTheseColumns="#variables.hideTheseColumns#"
			allowColumnSorting="#variables.allowColumnSorting#"
			columnTranslation="#variables.columnTranslation#"
			columnTranslationPrefix="#variables.columnTranslationPrefix#"
			>

	<cfelse>
		phr_lead_norecords
	</cfif>

</cfif>
</cfoutput>

