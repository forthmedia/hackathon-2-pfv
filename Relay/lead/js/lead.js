
jQuery(document).ready(function() {

	//we use this to check if any changes to the form have been made and if they have 
	//do not allow convert
	jQuery('#editorForm').data('serialize',jQuery('#editorForm').serialize());
	toggleReason();
	converting = false;

	jQuery('#convertLink').click(function(){
		if(converting == false && !jQuery('#convertLink').hasClass("disabled")){
			$('convertLink').spinner({position:'left'});
			convertOpportunity();
		}
	});

	jQuery('#deleteLink').click(function(){
		if (!jQuery('#deleteLink').hasClass("disabled")) {
			if (confirm(phr.lead_confirm_delete)){
				deleteLead(jQuery('#LeadID').val());
			}
			return false;
		}
	});

	jQuery('#message_close').click(function(){
		jQuery('.successblock').hide();
	});

	jQuery('#approvalStatusID').change(function(){
		toggleReason();
	});

	function toggleReason(){
		var $_rejectionReasonRow = jQuery('#rejectionReason_formgroup');
		
			if (jQuery('#approvalStatusID').val() == rejectID){
				 $_rejectionReasonRow.show();
			} else {
				 $_rejectionReasonRow.hide();
			}
		
	}

	function deleteLead(leadId) {
		jQuery.ajax(
	    	{type:'post',
	        	url:'/webservices/callwebservice.cfc?wsdl&method=callWebService&webserviceName=leadWS&methodName=delete&returnFormat=json',
	        	data:{leadId:leadId},
	        	dataType:'json',
	        	success: function(data,textStatus,jqXHR) {
					if (isInternal){
						location.href = '/lead/leads.cfm?active=1';
					} else {
						location.href = '/?eid=' + elementID;
					}
				}
			});
	}
});

function submitWithoutRefresh(formID){
	jQuery.post(window.location.toString(), jQuery('#'+formID).serialize())
}


//returns the url parameter specified (or null if it doesn't exist)
function urlParam(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null){
       return null;
    } else{
       return results[1] || 0;
    }
}

		
function convertOpportunity()  {


	jQuery('#convertDataForm').html("");
	jQuery('#convertDataForm').load('/webservices/callwebservice.cfc?wsdl&method=callWebService&webserviceName=leadWS&methodName=DisplayConvertLeadDataForm&returnFormat=plain',
		function() {
	
		
		var successUIAction=function(message){
			jQuery('#slideHeader a.Submenu, #convertLink, #deleteLink').addClass("disabled");
			jQuery('#slideHeader a.Submenu').prop('onclick',null).off('click');
			jQuery("#editorForm").prepend("<div class='successblock'>" + message + "</div>");
			$('convertLink').removeSpinner();
		}
		var failureUIAction=function(errorThrown){
				$('convertLink').removeSpinner();
				jQuery("#editorForm").prepend("<div class='errorblock'>" + errorThrown + "</div>");
			}
		
		var onCloseUIAction=function(){if (typeof savingData=='undefined') $('convertLink').removeSpinner();}
		
		openConvertDataForm(successUIAction,failureUIAction,onCloseUIAction);

		
	});
}

function saveConvertDataFormAction(form, successUIAction, failureUIAction) {

	var validationResult = relayFormValidation(form);

	if (validationResult) {
		savingData = true;
		jQuery.fancybox.close();
		saveConvertDataForm(successUIAction, failureUIAction);
	}
}

function openConvertDataForm(successUIAction,failureUIAction,onCloseUIAction) {

	var convertDataForm = jQuery("#convertDataForm");

	jQuery.fancybox({
		helpers: {
			overlay: {
				locked: true
			}
		},
		autoSize: false,
		width: '60%',
		height: '50%',
		closeEffect : 'none',
		closeSpeed: 'fast',
		type: 'inline',
		href: convertDataForm,
		beforeClose: function(){onCloseUIAction();}
		

	});
	
	jQuery("#detail").val("Opportunity for " + jQuery("#company").val());
	
	

	//add the save button behaviour to the save button
	jQuery('#convertDataForm #frmSubmit').click(function(){saveConvertDataFormAction(this.form,successUIAction,failureUIAction)});
	
}

function saveConvertDataForm(successUIAction, failureUIAction) {

	var dataObj = {};
	dataObj.Names = jQuery("#Names").val();
	dataObj.detail = jQuery("#detail").val();
	dataObj.expectedCloseDate = jQuery("#expectedCloseDate").val();
	dataObj.OppCustomerTypeID = jQuery("#OppCustomerTypeID").val();
	dataObj.stageID = jQuery("#stageID").val();
	dataObj.budget = jQuery("#budget").val();
	dataObj.leadID = jQuery("#LeadID").val();

	if (typeof eid !== 'undefined'){
		dataObj.eid=eid; //for portal conversions include the eid, we'll need to send this so that the webservice can send back the encrypted link to the created opportunity.
	}
	
	converting = true

	jQuery.ajax(
		{
			type:'post',
			url:'/webservices/callwebservice.cfc?wsdl&method=callWebService&webserviceName=leadWS&methodName=convertLead&returnFormat=json',
			data:dataObj,
			dataType:'json',
			success: function(data,textStatus,jqXHR) {
	   			converting = false;
    			
	   			successUIAction(data.MESSAGE, data.opportunityViewToken);
	   			
	   			
			},

			error: function (jqXHR, textStatus, errorThrown) {
				converting = false; //2015-10-07	ACPK	PROD2015-106 Fixed bug preventing user from reattempting conversion if previous attempt failed
    			
				failureUIAction(errorThrown);

			}

	});
}

function setProvinces(obj,mode,readOnly) {
	var countryID = jQuery(obj).val();

	var provinceValue = "";
	if (jQuery("#Address5").length!="undefined") {
		provinceValue = jQuery("#Address5").val();
	}

	if (mode=='add') {
		provinceValue = "";
	}

	if(!isNaN(countryID)) {

		var dataObj = {};
		dataObj.countryID = countryID;
		dataObj.dataValueColumn = "stateAbbreviation";
		jQuery.ajax(
			{
				type:'post',
				url:'/webservices/callwebservice.cfc?wsdl&method=callWebService&webserviceName=relaycountries&methodName=getProvinceListJSON&returnFormat=json',
				data:dataObj,
				dataType:'json',
				success: function(data,textStatus,jqXHR) {
					if (data.provinces.length > 0) {

						var newElement = '<select class=" form-control" name="Address5" id="Address5">';

						for (var e=0;e < data.provinces.length;e++) {
							var sel = "";
							if (provinceValue==data.provinces[e].stateAbbreviation) {
								sel = "selected";
							}
							newElement += '<option value="' + data.provinces[e].stateAbbreviation + '" ' + sel + '>' + data.provinces[e].state + '</option>';
						}

						newElement += '</select>';

					} else {
						//BF-590 WAB 2016-03-18 Made translations work
						var newElement = '<input maxlength="100" name="Address5" class=" form-control" validateat="onSubmit,onServer" label="' + phr.locator_county + '" id="Address5" size="50" placeholder="' + phr.placeholder_lead_Address5 + '" type="text" value="' + provinceValue + '">';
					}

					var parentObj = document.getElementById("Address5").parentNode;
					parentObj.innerHTML = newElement;

					setRequired(jQuery('#Address5').get(0),(data.provinces.length > 0 && !readOnly)?true:false);
					jQuery('#Address5').prop('readonly',readOnly);
					jQuery('select[id="Address5"]').prop('disabled',readOnly);
				},

    			error: function (jqXHR, textStatus, errorThrown) {
					console.log(errorThrown);
    			}
		});
	}
}