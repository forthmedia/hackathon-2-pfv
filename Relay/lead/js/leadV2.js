jQuery(document).ready(function() {

	jQuery('.deleteLink').parent('a').click(function(){
		if (!jQuery('#deleteLink').hasClass("disabled")) {
			var leadId = jQuery(this).closest('tr[id^="leadid="]').attr('id').split('=')[1];
			if (confirm(phr.lead_confirm_delete)){
				deleteLead(leadId);
			}
			return false;
		}
	});

	function deleteLead(leadId) {
		var $_deleteTR = jQuery('tr[id="leadid='+leadId+'"]')
		var $_deleteTD = jQuery($_deleteTR).find('td.delete');
		$_deleteTD.spinner();
		jQuery.ajax(
	    	{type:'post',
	        	url:'/webservices/callwebservice.cfc?wsdl&method=callWebService&webserviceName=leadWS&methodName=delete&returnFormat=json',
	        	data:{leadId:leadId},
	        	dataType:'json',
	        	success: function(data,textStatus,jqXHR) {
					/* remove the row from the listing table */
					$_deleteTD.removeSpinner();
					$_deleteTR.remove();
				}
			});
	}
});

function getLeadPageURL() {
	return !isInternal?'/?eid='+elementID:'/lead/leads.cfm?';
}

function onCancel() {
	location.href=getLeadPageURL();
}

function editLead(leadID) {
	location.href = getLeadPageURL()+'&leadID='+leadID;
}

function addLead() {
	editLead(0);
}