
jQuery(document).ready(function() {


	converting = false;

	jQuery('#portalConvertLink').click(function(){
		if(converting == false && !jQuery('#portalConvertLink').hasClass("disabled")){
			if(jQuery('#editorForm').serialize()==jQuery('#editorForm').data('serialize')){
				$('portalConvertLink').spinner({position:'center'});
				jQuery('#deleteLink').prop('disabled', true);
				jQuery('#saveButton').prop('disabled', true);
				convertPortalOpportunity('editorForm');
			}else{
				
				
				if (jQuery('#editorForm')[0].checkValidity()){
					submitWithoutRefresh('editorForm');
					$('portalConvertLink').spinner({position:'center'});
					jQuery('#deleteLink').prop('disabled', true);
					jQuery('#saveButton').prop('disabled', true);
					convertPortalOpportunity('editorForm');
				}else{
					//clicking the save button will fail, but will trigger any user messages etc
					jQuery('#saveButton')[0].click();
					
				}


			}
		}
	});

	
});


		
function convertPortalOpportunity()  {
	
	
	jQuery('#convertDataForm').html("");
	jQuery('#convertDataForm').load('/webservices/callwebservice.cfc?wsdl&method=callWebService&webserviceName=leadWS&methodName=DisplayConvertLeadDataForm&returnFormat=plain',
		function() {
	
		
		var successUIAction=function(message, opportunityViewLink){
			onSuccessfulConvert(opportunityViewLink);//the creation of user messages happens in the webservice, theres no need to process one here
		}
		var failureUIAction=function(errorThrown){
				$('portalConvertLink').removeSpinner();
				jQuery("#editorForm").prepend("<div class='errorblock'>" + errorThrown + "</div>");
			}
		
		var onCloseUIAction=function(){if (typeof savingData=='undefined'){
				$('portalConvertLink').removeSpinner();
				jQuery('#deleteLink').prop('disabled', false);
				jQuery('#saveButton').prop('disabled', false);
			}
			
		}
		
		openConvertDataForm(successUIAction,failureUIAction,onCloseUIAction);

		
	});
	
}		

