<!--- �Relayware. All Rights Reserved 2014 --->

<cfparam name="current" type="string" default="index.cfm">
<cfparam name="attributes.templateType" type="string" default="edit">
<cfparam name="attributes.pageTitle" type="string" default="">
<cfparam name="attributes.pagesBack" type="string" default="1">
<cfparam name="attributes.thisDir" type="string" default="/lead">
<cfparam name="attributes.showPrint" type="string" default="true">

<cfset addLeadOnInternal = application.com.settings.getSetting("leadManager.addLeadOnInternal")>

<cfif (!structKeyExists(url,"editor") and application.com.settings.getSetting('versions.leadScreen') eq 1) or (!structKeyExists(url,"leadID") and application.com.settings.getSetting('versions.leadScreen') eq 2)>
	<CF_RelayNavMenu pageTitle="#attributes.pageTitle#" thisDir="#attributes.thisDir#">

	<!--- 2013 Roadmap --->
	<cfif findNoCase("leads.cfm",SCRIPT_NAME,1)> <!--- NJH 2013/04/-8 - added productList.cfm to show menu in incentive claims and rewards catalog --->
		<cfif request.relayCurrentUser.isInternal eq "true" and addLeadOnInternal>
			<cfset addLeadURL = application.com.security.encryptURL(url="leads.cfm?editor=yes&add=yes&hideBackButton=false")>
			<cfset onclick = "">
			<cfif application.com.settings.getSetting('versions.leadScreen') eq 2>
				<cfset addLeadURL = "##">
				<cfset onClick = "javascript:addLead(); return false;">
			</cfif>
			<CF_RelayNavMenuItem MenuItemText="Add" CFTemplate="#addLeadURL#" onclick="#onclick#">
		</cfif>
		<CF_RelayNavMenuItem MenuItemText="Excel" CFTemplate="#cgi["SCRIPT_NAME"]#?#queryString#&openAsExcel=true">

		<cfif attributes.showPrint>			<!--- 2012-03-13 PPB AVD001 hidden for opp v2 for now --->
			<CF_RelayNavMenuItem MenuItemText="Print" CFTemplate="javascript: print()">
		</cfif>

	</cfif>

</CF_RelayNavMenu>
</cfif>