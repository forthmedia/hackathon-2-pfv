<cfparam name="variables.returnURL" default="">

<cfset application.com.request.includeJavascriptOnce(template="/lead/js/leadV2.js",translate=true)>

<!--- if an internal user, get the TFQO attributes from settings --->
<cfif request.relayCurrentUser.isInternal>
	<cfset variables.leadListingParameters = application.com.settings.getSetting("lead.listing")>
	<cfset variables.filterColumns = leadListingParameters.filterColumns>
	<cfset variables.showTheseColumns = leadListingParameters.showTheseColumns>
	<cfset variables.leadTypeID = leadListingParameters.leadTypeID>
	<cfset variables.keyColumnOnCLickList ="javascript:openNewTab('Edit lead ##leadID##'*comma'Edit lead ##leadID##'*comma'/lead/leads.cfm?editor=yes&hideBackButton=true&leadID=##application.com.security.encryptVariableValue(name='leadID'*commavalue=leadID)##'*comma{iconClass:'leadmanager'});return false;">
</cfif>

<!--- EDIT --->
<cfif structKeyExists(url,"leadID") and (url.leadId eq 0 or application.com.security.confirmFieldsHaveBeenEncrypted(fieldNames="leadId")) and application.com.rights.doesUserHaveRightsForEntity(entityType="lead",entityID=url.leadID,permission="edit")>

	<cfset variables.lead = application.com.entities.load('lead', structKeyExists(form,"leadID")?form.leadID:url.leadID)>

	<!--- if the partner is viewing the lead, then update the viewedBypartnerDate if it has not already been set --->
	<cfif !request.relayCurrentUser.isInternal and lead.get("viewedByPartnerDate") eq "" and lead.get("partnerSalesPersonID") eq request.relayCurrentUser.personID>
		<cfset lead.set("viewedByPartnerDate",request.requestTime)>
		<!--- if the lead is a new record, then we don't want to call save yet. Just set the field --->
		<cfif !lead.isNewRecord()>
			<cfset lead.save()>
		</cfif>
	</cfif>
	<cfset variables.formObject = new form.formObject(entity = variables.lead, screenid=(request.relayCurrentUser.isInternal?'leadInternalScreen':'LeadPortalScreen'))>
	<cfset variables.formObject.set("showCancel",(!request.relayCurrentUser.isInternal or url.leadID eq 0))>
	<cfset variables.formObject.setFormAttribute("action",variables.returnURL)>
	<cfoutput>#formObject.render()#</cfoutput>

<!--- LIST --->
<cfelse>

	<cfparam name="variables.sortOrder" default="LeadID DESC">
	<cfparam name="variables.startRow" default="1">

	<cfset variables.hideTheseColumns = "">
	<cfset variables.numRowsPerPage = 50>
	<cfset variables.dateFilter = "created">

	<cfif not listFindNoCase(variables.showTheseColumns,"leadId")>
		<cfset variables.showTheseColumns = listAppend(variables.showTheseColumns,"leadID")>
		<cfset variables.hideTheseColumns = "leadID">
	</cfif>

	<cfset variables.dateFields = application.com.entities.getFieldMetaDataByType("lead","date")>
	<cfset variables.dateFields = valueList(variables.dateFields.name)>
	<cfset variables.bitFields = application.com.entities.getFieldMetaDataByType("lead","bit")>
	<cfset variables.bitFields = valueList(variables.bitFields.name)>

	<cfscript>
		variables.filter = [];
		/* if we're not showing account leads, then just show the leads that the partner has been assigned */
		if (!request.relayCurrentUser.isInternal and not structKeyExists(variables,"showAccountLeads")) {
			ArrayAppend(variables.filter,"partnerSalesPersonID="&request.relayCurrentUser.personID);
		}
		variables.qLeadList = application.com.relayLeads.GetLeads("data",sortOrder,filter,leadTypeID);
		variables.deleteCol = arrayNew(1);
		if (qLeadList.recordCount) {
			arraySet(variables.deleteCol,1,qLeadList.recordCount,'<a><span class="deleteLink">phr_Delete</span></a>');
		}
		queryAddColumn(variables.qLeadList,"delete","VarChar",variables.deleteCol);
		variables.showTheseColumns = listAppend(variables.showTheseColumns,"delete");
	</cfscript>

	<cf_tableFromQueryObject
		keyColumnOnClickList="#variables.keyColumnOnClickList#"
		showTheseColumns="#variables.showTheseColumns#"
		keyColumnList="#listFirst(variables.showTheseColumns)#"
		keyColumnKeyList=""
		keyColumnURLList="javascript:void(0)"
		useInclude="false"
		sortOrder="#variables.sortOrder#"
		queryObject="#variables.qLeadList#"
		numRowsPerPage="#variables.numRowsPerPage#"
		startRow="#variables.startRow#"
		hideTheseColumns="#variables.hideTheseColumns#"
		dateFormat="#variables.dateFields#"
		searchColumnList=""
		booleanFormat="#variables.bitFields#"
		FilterSelectFieldList = "#variables.filterColumns#"
		FilterSelectFieldList2="#listLen(variables.filterColumns) gt 1?variables.filterColumns:''#"
		showTextSearch="true"
		dateFilter="#listFindNoCase(variables.showTheseColumns,variables.dateFilter)?variables.dateFilter:''#"
		rowIdentityColumnName="leadid"
		encryptRowIdentity=true
		columnTranslation="true"
		columnTranslationPrefix="phr_Lead_"
	>
</cfif>