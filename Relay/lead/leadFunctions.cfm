<!--- ©Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			leadFunctions.cfm
Author:				AXA
Date started:		2014-09-19
	
Description:			

creates functionList query for tableFromQueryObject custom tag

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->

<cfscript>
comTableFunction = CreateObject( "component", "relay.com.tableFunction" );

comTableFunction.functionName = "Phr_Sys_LeadFunctions";
comTableFunction.securityLevel = "";
comTableFunction.url = "";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();

comTableFunction.functionName = "--------------------";
comTableFunction.securityLevel = "";
comTableFunction.url = "";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();

comTableFunction.functionName = "&nbsp;Phr_Sys_SetLeadsAsInactive";
comTableFunction.securityLevel = "";
comTableFunction.url = "javascript:if (confirm('phr_lead_confirm_inactive')) {document.frmBogusForm.method = 'post';document.frmBogusForm.submit();} ;";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();

</cfscript>


