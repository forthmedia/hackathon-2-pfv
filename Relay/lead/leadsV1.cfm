<!---
File name:		leads.cfm
Author:			Ali Awan
Date started:

Description:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2014-09-02			RPW			Add Lead Screen on Portal
2014-09-16			RPW			Add Lead Screen on Portal - Change Enquiry Type to use lookUpList
2014-09-16			RPW			Add Lead Screen on Portal - Added email validation
2014-09-22 			AXA 		CORE-654 add ability to perform bulk actions against selected leads on the listing page.
2014-09-23 			NJH 		basecamp - add support for masks on the portal
2014-10-01			RPW			CORE-684 Converted column in leads screen is redundant
2014-10-03			RPW			CORE-712 The screen on the portal which appears after clicking the new lead save button... has a not found error - removed backForm value
2014-10-14			RPW			CORE-777 Textboxes  validation for Phone number and mobile in portal site
2014-11-18			RPW			CORE-97 Phase 1: Extend database fields for Leads
2014-11-19			RPW			Made salutation Not mandatory
2014-11-19			RPW			CORE-97 Phase 1: Extend database fields for Leads
2014-11-24			AXA 		CORE-117 added check for new settings to switch flag for vendoraccountmanager
2014-12-01			RPW			CORE-97 Phase 1: Extend database fields for Leads - Added error handling to fnction saveConvertDataForm
2015-02-10			RPW			Leads - Added ability to customize leads form for customers.
2015-03-06			RPW			Defaulted partnerSalesPersonID on add to current user if read only.
2015-03-06			RPW			FIFTEEN-280 - Page  is not displaying properly after edit and saved the lead record
2015-03-06			RPW			FIFTEEN-279 - Not able to covert lead to opportunity
2015-03-10			RPW			Fixed partnerSalesPersonID dropdown population which depends on partnerLocationID
2015-03-19			RPW			Added phrase to placeholder for Address5.
2015-03-24			RPW			Changed value to default for partnerSalesPersonID and outputted variable.
2015/03/25			NJH			Moved some javascript out into a js file and then include that. Should move all of it out, but some of it is interspersed with CF, so have left some in to be dealt with later when I have the time to test.
2015/03/31			NJH			Try to provide support for readonly fields. Also initialise the fieldDisplay struct with all the fields in the DB. Put an if display around most xml elements.
								Also set JS variables by calling a function so that we can include custom JS variable on the page.
2015-04-16			MS			P-AER001 Empty sring was being passed into backForm which redirected the dealReg page back to where it was originallly trigerred from after saving the deal. I have created a VAR #backForm# that can be passed in from the Content Manager WYSIWYG and it tells you where to go once lead is saved
2015-05-13			NJH			A number of changes to support Nutanix requests. Override some standard labels. Add ability to customise labels were needed. Set Address5 to not required if readonly. Param the showTheseColumns variabl
								for a portal user. Default the statusID for a new lead based on the default lookup value.
2015-06-05			NJH			Set onValidate to validateEmail for the email field
2015-06-10			NJH			Add ability to set default currency isoCode
2015-06-12			MS			P-CML001 Kanban 957 Added new param leadTypeIDFilter to enable user to create lead listing screens based on leadType
2015-06-17			MS			P-CML001 Kanban 957 Ability to hide or show columns on Lead Listing Screen by passing in a param in the relay tag using Contact Manager
2015-07-31          DAN         K-1367 - update deal reg form fields
2015/08/04			NJH			add ability to default the source field
2015/08/06          DAN         add ability to disable phone mask validation
2015/09/01			PYW			#445608 Enable postal code to be displayed on portal - NJH added the arrayFind for backwards compatibility as postalCode needs to be in the sortOrder as well. display values are set to true by default,
								but if the field is not in the sort order, it causes an error
2015/09/11			PYW			#445888 Change showCurrentUsersCountriesOnly value to a variable
2015/09/15			PYW			#445613 Remove hard-coded page title override
2015/09/30			NJH			PROD2015-35 - added param for column filters and added text search and date filter
2015-10-07			ACPK		PROD2015-106/FIFTEEN-394 Add spinner and start conversion if not already in progress and Convert link not disabled;
								FIFTEEN-394 Confirm deletion if delete link not disabled
2015-10-23          DAN         Case 446321 - add ability to make email field requried
2015-11-10          DAN         Case 446321 - add required attribute to firstname and lastname
2015/11/10			NJH			Did some rework. Took the javascript out of this file and put it into leads.js
2015/12/04			ESZ			case 446170 Incorrect Reseller showing for Lead
22/01/2016          DAN         Case 446227 - fix for sending emails for lead approval/rejection caused by using wrong form field name
2016/06/16			NJH			JIRA PROD2016-1273 - check user rights for lead if in edit mode. Also ensure that leadID is encrypted.
2016/09/25          DAN         PROD2016-2387 - filter leads by type (new tag parameter)
2016/10/06          DAN         PROD2016-2465 - allow to stay in the edit view on saving a lead also fix the issue with not keeping fields sort order when saving lead on portal
2016/10/26			RJT			PROD2016-2587 Allowed the Relay_Leads tag to select TFQO columns to display
2016/03/16			SBW			CASE 452338 - Viewed by date not updating when viewed by partner.
Possible enhancements:

 --->

<!---
<cf_checkFieldEncryption fieldNames = "LeadID">
 --->

<cfparam name="qLeadList" type="query" default="#queryNew('')#">
<cfparam name="sortOrder" default="LeadID DESC">
<cfparam name="numRowsPerPage" type="numeric" default="50">
<cfparam name="startRow" type="numeric" default="1">
<cfparam name="hideTheseColumns" type="string" default="leadID">
<cfparam name="url.active" type="Boolean" default="1">
<cfparam name="leadID" type="numeric" default=0>
<cfparam name="backForm" type="string" default=""><!--- 2015-04-16	MS	P-AER001 Added param backForm --->
<cfparam name="leadTypeIDFilter" type="string" default=""><!--- 2015-06-12	MS	P-CML001 Kanban 957 Ability to display only a certain type of lead on the listing screen --->
<cfparam name="showTheseColumns" type="string" default="leadID,country,Company,name,email,acceptedByPartner,leadApprovalStatus,created"><!--- 2015-06-17	MS	P-CML001 Kanban 957 Ability to hide show column on Lead Listing Screen by passing in a param in the relay tag using Contact Manager --->
<cfparam name="validatePhoneMask" type="boolean" default="true"> <!--- 2015/08/06 DAN - add ability to disable phone mask validation --->
<!--- PYW #445888 param userRightsCountriesOnly her as well because internal calls this page directly --->
<cfparam name="userRightsCountriesOnly" default="true">
<cfparam name="filterColumnList" default="country">
<cfparam name="dateFilter" default="created">
<cfparam name="saveAndReturn" default="yes"> <!--- PROD2016-2465 - keep the current behaviour by default --->

<cfif NOT isDefined("columnHeadingList")>
	<cfset columnHeadingList="">

	<!--- Use our map to define the headings RJT --->
	<cfset columnHeadingListMap={leadID="Lead ID",country="Country",Company="Company",name="Name",email="Email",acceptedByPartner="Accepted By Partner",leadApprovalStatus="Approval Status",created="Created"}>

	<cfloop index = "column" list = "#showTheseColumns#">
	    <cfif listLen(columnHeadingList) NEQ 0>
		    <cfset columnHeadingList=columnHeadingList & ",">
		</cfif>
	    <cfset columnHeadingList=columnHeadingList & columnHeadingListMap[column]>
	</cfloop>

</cfif>

<cfset keyColumnList="Company">
<!--- If company isn't in the showTheseColumns use the first one, whatever that is RJT--->
<cfif listFind(showTheseColumns,keyColumnList) EQ 0>
	<cfset keyColumnList=ListGetAt(showTheseColumns,1)>
</cfif>



<!--- START: 2014-09-22 AXA CORE-654 add ability to perform bulk actions against selected leads on the listing page. --->
<cfscript>

	if(structKeyExists(form, "frmRowIdentity")){
		application.com.relayLeads.setActiveLead(leadIDs=form.frmRowIdentity);
		if(request.relayCurrentUser.isInternal){
			dataMsg = 'phr_lead_success_inactive';
			writeOutput("<div class='successblock'>" & dataMsg & "</div>"); /*" This is here as a syntax highlighting correction*/
		}
	}

	if (structKeyExists(url,'editor')) {
		//2015-02-10	RPW	Leads - Added mode.
		if ( StructKeyExists(url,'add') and URL.add eq 'yes') {
			variables.mode = "add";
		} else {
			variables.mode = "edit";
		}
		variables.fieldDisplay = application.com.leads.GetLeadsFormFieldDisplayOptions(isInternal=request.relayCurrentUser.isInternal,mode=variables.mode,leadID=leadID);

		leadColumns = application.com.applicationVariables.getTableInfoStructure(tablename="lead");
		for (item in leadColumns) {
			if (not structKeyExists(variables.fieldDisplay,item)) {
				variables.fieldDisplay[item] = {};
			}
			if (not structKeyExists(variables.fieldDisplay[item],"required")) {
				variables.fieldDisplay[item].required=false;
			}
			if (not structKeyExists(variables.fieldDisplay[item],"readonly")) {
				variables.fieldDisplay[item].readonly=false;
			}
			if (not structKeyExists(variables.fieldDisplay[item],"display")) {
				variables.fieldDisplay[item].display=true;
			}
			if (not structKeyExists(variables.fieldDisplay[item],"label")) {
				variables.fieldDisplay[item].label="phr_lead_"&item;
			}
		}
	}
</cfscript>

<!--- END: 2014-09-22 AXA CORE-654 add ability to perform bulk actions against selected leads on the listing page. --->
<cfset xmlSource = "">
<cfif structKeyExists(url,"editor")>

	<cfif not application.com.rights.getUserRightsForEntity(entityType="lead",entityID=leadID).view>
		phr_lead_leadNoRights
	     <CF_ABORT>
	</cfif>

<cf_head>

	<!--- 2015-03-06	RPW	FIFTEEN-279 - Not able to covert lead to opportunity --->
	<cfif request.relayCurrentUser.isInternal>
		<cfif NOT structKeyExists(url,'add')>
			<cfinclude template="/lead/LeadActions.cfm">
		</cfif>
	</cfif>

	<!--- SBW 03/11/16 If a portal user, update the viewedByPartnerDate if not done already --->
	<cfif NOT request.currentSite.isInternal>
		<cfset application.com.leads.updatePartnerViewedDate(leadID=leadID,partnerSalesPersonId=request.relayCurrentUser.personID)>
	</cfif>

	<!--- 2014-11-18	RPW	CORE-97 Changes convertLead functions to open fancybox form to enter sipplimentary data before converting lead. --->
	<!--- 2014-11-19	RPW	CORE-97 Phase 1: Extend database fields for Leads - Set EndCustomeName in data form after load instead of paasing into the webservice --->
	<!--- 2014-12-01	RPW	CORE-97 Phase 1: Extend database fields for Leads - Added error handling to fnction saveConvertDataForm --->
	<script type="text/javascript">
		<cfoutput>
			#application.com.leads.getJavascriptVariables(leadID=leadID)#
		</cfoutput>

		<!--- 2015-02-10	RPW	Leads - Added setProvinces. --->
		<cfif variables.fieldDisplay.relatedProvince.display>
			jQuery(document).ready(function() {
			<cfoutput>
				setProvinces(jQuery("##countryID"),"#variables.mode#",#variables.fieldDisplay.address5.readonly#);
			</cfoutput>
			});
		</cfif>
	</script>
</cf_head>

<!--- 2014-11-18	RPW	CORE-97 Added fancybox includes --->
<cf_includeJavascriptOnce template ="/javascript/prototypeSpinner.js">
<cf_includeJavascriptOnce template ="/javascript/lib/fancybox/jquery.fancybox.pack.js">
<cf_includeCSSOnce template ="/javascript/lib/fancybox/jquery.fancybox.css">
<cf_includeJavascriptOnce template="/javascript/extExtension.js">
<cf_includeJavascriptOnce template="/lead/js/lead.js" checkIfExists="true" translate="true">


<cffunction name="triggerLeadEmails" output="true" returntype="struct">
	<cfset var emailRecipient = request.relayCurrentUser.personID />
	<cfset var mergeStruct = {leadID=form.leadID} />
	<cfset var rejectStatusID = application.com.relayForms.getLookupList(fieldname='leadApprovalStatusID',textID='rejected').lookupID />
	<cfset var approvedStatusID = application.com.relayForms.getLookupList(fieldname='leadApprovalStatusID',textID='approved').lookupID />

	<cfset result={isok=true,message=""}>

	<cfif structKeyExists(form,'added')>
		<cfif structKeyExists(form,'isOK') and form.isOK>
			<!--- 1 - Lead Successfully Added Trigger Add email --->
			<!--- current user person id for testing purposes only.  should be form.partnerSalesPersonID --->
			<cfset application.com.email.sendEmail(personID=form.partnerSalesPersonID,emailTextID="LeadEmail",mergeStruct=mergeStruct)>
			<cfset application.com.email.sendEmail(personID=form.vendorAccountManagerPersonID,emailTextID="LeadAccountManagerNotification",mergeStruct=mergeStruct)>
			<cfif val(trim(form.vendorAccountManagerPersonID)) GT 0>
				<cfset application.com.email.sendEmail(personID=form.vendorAccountManagerPersonID,emailTextID="LeadNewAccountManagerNotification",mergeStruct=mergeStruct)>
			</cfif>
		</cfif>
	<cfelse>
		<cfif structKeyExists(form,'leadID') AND val(form.leadID) GT 0>
			<cfif structKeyExists(form,'acceptedByPartner')>
				<cfif val(form.acceptedByPartner) EQ 1>
					<!--- Send Accepted Email to: vendorAccountManagerPersonID--->
					<cfset application.com.email.sendEmail(personID=form.vendorAccountManagerPersonID,emailTextID="LeadAcceptedAccountManager",mergeStruct=mergeStruct)>
				<cfelseif form.acceptedByPartner EQ 0>
					<!--- Send Rejected Email --->
					<cfset application.com.email.sendEmail(personID=form.vendorAccountManagerPersonID,emailTextID="LeadRejectedAccountManager",mergeStruct=mergeStruct)>
				</cfif>
			</cfif>
            <!--- START: 22/01/2016 DAN Case 446227 --->
			<cfif structKeyExists(form,"ApprovalStatusID_orig")> <!--- NJH 2016/01/27 BF-391 - check for existance of ApprovalStatusID_orig field as when ApprovalStatusID is in display mode, there is no such field --->
	            <cfif structKeyExists(form,'ApprovalStatusID') AND compareNoCase(trim(form.ApprovalStatusID), trim(form.ApprovalStatusID_orig)) NEQ 0>
					<cfif val(form.ApprovalStatusID) EQ rejectSTatusID>
						<cfset application.com.email.sendEmail(personID=form.partnerSalesPersonID,emailTextID="LeadDeclinedEmail",mergeStruct=mergeStruct)>
					<cfelseif val(form.ApprovalStatusID) EQ approvedStatusID>
						<cfset application.com.email.sendEmail(personID=form.partnerSalesPersonID,emailTextID="LeadApprovedEmail",mergeStruct=mergeStruct)>
						<cfset application.com.email.sendEmail(personID=form.vendorAccountManagerPersonID,emailTextID="LeadAccountManagerApproval",mergeStruct=mergeStruct)>
					</cfif>
				</cfif>
			</cfif>
            <!--- END: 22/01/2016 DAN Case 446227 --->

			<cfset application.com.email.sendEmail(personID=form.vendorAccountManagerPersonID,emailTextID="emailAccMngrLeadStatusChange",mergeStruct=mergeStruct)>
			<cfif structKeyExists(form,"vendorAccountManagerPersonID_orig")>
				<cfif compareNoCase(trim(form.vendorAccountManagerPersonID),trim(form.vendorAccountManagerPersonID_orig)) NEQ 0>
					<cfif val(trim(form.vendorAccountManagerPersonID_orig)) GT 0>
						<cfset application.com.email.sendEmail(personID=form.vendorAccountManagerPersonID,emailTextID="LeadOldAccountManagerNotification",mergeStruct=mergeStruct)>
						<cfif val(trim(form.vendorAccountManagerPersonID)) GT 0>
							<cfset application.com.email.sendEmail(personID=form.vendorAccountManagerPersonID,emailTextID="emailAccMngrLeadAccMngrChange",mergeStruct=mergeStruct)>
						</cfif>
					<cfelse>
						<cfif val(trim(form.vendorAccountManagerPersonID)) GT 0>
							<cfset application.com.email.sendEmail(personID=form.vendorAccountManagerPersonID,emailTextID="LeadNewAccountManagerNotification",mergeStruct=mergeStruct)>
						</cfif>
					</cfif>
				</cfif>
			</cfif>
		</cfif>
	</cfif>

	<cfreturn result>

</cffunction>

<!--- 2014-09-02			RPW			Add Lead Screen on Portal --->
<!--- 2014-11-19	RPW	CORE-97 Phase 1: Extend database fields for Leads - Removed ApprovalStatusID --->

<cfsavecontent variable="xmlSource">
	<cfoutput>
	<editors>
		<editor id="232" name="thisEditor" entity="lead" title="Lead Editor">
			<cfif request.relayCurrentUser.isInternal><field name="leadID" label="phr_lead_leadID" description="This records unique ID." control="html"></field></cfif>

			<!--- 2014-11-19	RPW	Made salutation Not mandatory --->
			<!--- 2015-02-10	RPW	Leads - Added salutation display. --->
			<cfif variables.fieldDisplay.salutation.display>
			<field name="salutation" label="phr_lead_salutation" control="select" description="" validvalues="lead.salutation" display="displayValue" value="dataValue" required="#variables.fieldDisplay.salutation.required#" readonly="#variables.fieldDisplay.salutation.readonly#"/>
			</cfif>
			<lineItem label="phr_lead_endCustomerName" required="#variables.fieldDisplay.endCustomerName.required#" name="EndCustomerName">
				<field name="firstname" readonly="#variables.fieldDisplay.firstname.readonly#" required="#variables.fieldDisplay.firstname.required#"/> <!--- 2015-11-10 Case 446321 - add required attr --->
				<field name="lastname" readonly="#variables.fieldDisplay.lastname.readonly#" required="#variables.fieldDisplay.lastname.required#"/> <!--- 2015-11-10 Case 446321 - add required attr --->
			</lineItem>

            <!--- 2015-07-31 DAN K-1367 - specify requried property for company field --->
			<field name="company" label="phr_lead_company" description="" readonly="#variables.fieldDisplay.company.readonly#" required="#variables.fieldDisplay.company.required#"/>

			<!--- 2014-11-18	RPW	CORE-97 Added extra fields for internal. --->
			<cfif request.relayCurrentUser.isInternal>

				<field name="Address1" label="phr_address1" />
				<field name="Address4" label="phr_address" />
				<field name="Address5" label="phr_address" />
				<field name="PostalCode" label="phr_postalcode" />
			<cfelse>

				<!--- START: 2015-06-18	MS	P-CML001 They want to capture and display end customer address --->
                <!--- 2015-07-31 DAN K-1367 - specify requried property for address fields --->
				<cfif variables.fieldDisplay.Address1.display>
					<field name="Address1" label="phr_address1" readonly="#variables.fieldDisplay.address1.readonly#" required="#variables.fieldDisplay.Address1.required#"/>
				</cfif>
				<cfif variables.fieldDisplay.Address2.display>
					<field name="Address2" label="phr_address2" readonly="#variables.fieldDisplay.address2.readonly#" required="#variables.fieldDisplay.Address2.required#"/>
				</cfif>
				<cfif variables.fieldDisplay.Address4.display>
					<field name="Address4" label="phr_address4" readonly="#variables.fieldDisplay.address4.readonly#" required="#variables.fieldDisplay.Address4.required#"/>
				</cfif>
				<!--- END  : 2015-06-18	MS	P-CML001 They want to capture and display end customer address --->

				<!--- 2015-02-10	RPW	Leads - Added Address5 (province). --->
				<cfif variables.fieldDisplay.relatedProvince.display>
					<field name="Address5" label="phr_locator_county" required="#variables.fieldDisplay.Address5.required#" readonly="#variables.fieldDisplay.address5.readonly#"/>
				</cfif>

				<!--- 2015-09-01 PYW #445608 Display postal code  - NJH added the arrayFind for backwards compatibility as postalCode needs to be in the sortOrder as well. display values are set to true by default,
					but if the field is not in the sort order, it causes an error... --->
				<cfif StructKeyExists(variables.fieldDisplay,"PostalCode") AND variables.fieldDisplay.PostalCode.display and arrayFindNoCase(fieldDisplay.sortOrder,"postalCode")>
					<field name="PostalCode" label="phr_postalcode" required="#variables.fieldDisplay.PostalCode.required#" readonly="#variables.fieldDisplay.PostalCode.readonly#"/>
				</cfif>
			</cfif>

			<cfif variables.fieldDisplay.countryID.readonly>
				<cfscript>
					variables.countryID = application.com.relayLeads.GetLead(LeadID=LeadID).countryID;
					if (!IsNumeric(variables.countryID)) {
						variables.countryID = request.relayCurrentUser.countryID;
					}
					variables.countryIDDisplay = application.com.relayCountries.getCountryDetails(countryID=variables.countryID).CountryDescription;
				</cfscript>
				<field name="countryID" control="hidden" default="#variables.countryID#"/>
				<!--- 2015-02-10	RPW	Leads - Added XmlFormat. --->
				<field name="countryID_Display" label="phr_lead_countryID" value="#XmlFormat(countryIDDisplay)#" readonly="true"/>
			<cfelse>
				<!--- 2015-02-10	RPW	Leads - Added countryOnChange. --->
				<cfif variables.fieldDisplay.relatedProvince.display>
					<cfset variables.countryOnChange = "onChange=""setProvinces(this,'#variables.mode#',#variables.fieldDisplay.address5.readonly#);""">
				<cfelse>
					<cfset variables.countryOnChange = "">
				</cfif>

				<cfscript>
					variables.defaultCountryID = request.relayCurrentUser.countryID;
					if (IsDefined("variables.fieldDisplay.defaultCountryID")) {
						variables.defaultCountryID = variables.fieldDisplay.defaultCountryID;
					}
				</cfscript>

				<!--- 2015/09/11 PYW 445888 Change showCurrentUsersCountriesOnly value to a variable --->
				<field name="countryID" label="phr_lead_countryID" description="The lead country" control="select" query="func:com.leads.getCountries(withCurrencies=true,showCurrentUsersCountriesOnly=#userRightsCountriesOnly#)" display="countryDescription" value="countryID" default="#variables.defaultCountryID#" #variables.countryOnChange# required="#variables.fieldDisplay.countryID.required#" />
				<!--- 2015/12/22 GCC 447046 added countryID_Display to avoid errors when in country read only mode false --->
				<field name="countryID_Display" control="hidden" default=""/>
			</cfif>
			<!--- 2014-09-16			RPW			Add Lead Screen on Portal - Added email validation --->
			<field name="email" label="phr_lead_email" description="" validate="email" readonly="#variables.fieldDisplay.email.readonly#" onValidate="validateEmail" required="#variables.fieldDisplay.email.required#"/> <!--- 2015-10-23 DAN Case 446321 - add ability to make email field requried --->
			<!--- 2014-10-14	RPW	CORE-777 Textboxes  validation for Phone number and mobile in portal site --->
			<cfset phoneMask = "">
            <!--- 2015/08/06 DAN - add ability to disable phone mask validation --->
			<cfif validatePhoneMask and not request.relayCurrentUser.isInternal>
				<cfset phoneMask = application.com.commonQueries.getCountry(countryid=request.relayCurrentUser.countryID).telephoneformat>
			</cfif>
			<cfif Len(phoneMask)>
				<field name="phonemask" control="hidden" value="#phonemask#"/>
				<!--- 2015-02-10	RPW	Leads - Added officePhone.required. --->
				<field name="officePhone" label="phr_lead_officePhone" mask="#phoneMask#" required="#variables.fieldDisplay.officePhone.required#" readonly="#variables.fieldDisplay.officePhone.readonly#"/>
			<cfelse>
				<!--- 2015-02-11	RPW	Leads - Added phonemask to make it compatable with ordering. --->
				<field name="phonemask" control="hidden" value="#phonemask#"/>
				<!--- 2015-02-10	RPW	Leads - Added officePhone.required. --->
				<field name="officePhone" label="phr_lead_officePhone" validate="telephone" required="#variables.fieldDisplay.officePhone.required#" readonly="#variables.fieldDisplay.officePhone.readonly#"/>
			</cfif>

			<!--- 2015-02-10	RPW	Leads - Added website.display. --->
			<cfif variables.fieldDisplay.website.display>
            	<field name="website" label="phr_lead_website" required="#variables.fieldDisplay.website.required#" readonly="#variables.fieldDisplay.website.readonly#"/>
			</cfif>

			<!--- 2015-02-10	RPW	Leads - Added progressID.display. --->
			<cfif variables.fieldDisplay.progressID.display>
				<field name="progressID" label="phr_lead_progressID" type="select" query="func:com.relayForms.getLookupList(fieldname='leadprogressid')" display="translatedItemText" value="LookupID" readonly="#variables.fieldDisplay.progressID.readonly#"/>
			<cfelse>
				<cfscript>
					variables.qGetDefaultProgressID = application.com.relayForms.getLookupList(fieldName="leadprogressid");
					variables.defaultProgressID = variables.qGetDefaultProgressID.lookupID[1];
				</cfscript>
				<field name="progressID" control="hidden" default="#variables.defaultProgressID#"/>
			</cfif>

			<!--- 2015-02-10	RPW	Leads - Added leadTypeID.display. --->
			<cfif variables.fieldDisplay.leadTypeID.display>
				<cfif variables.fieldDisplay.leadTypeID.readonly>
					<cfscript>
						variables.leadTypeID = application.com.relayLeads.GetLead(LeadID=LeadID).leadTypeID;
						if (IsNumeric(variables.leadTypeID)) {
							variables.defaultLeadTypeID = variables.leadTypeID;
							variables.leadTypeIDDisplay = application.com.relayForms.getLookupList(lookupID=variables.leadTypeID).translatedItemText;
						} else {

							variables.qGetleadTypes = application.com.relayForms.getLookupList(fieldName="leadTypeID");
							variables.sql = "SELECT * FROM qTable WHERE lookUpTextID = 'Lead'";
							variables.qObj = new Query();
							variables.qObj.setAttributes(qTable = variables.qGetleadTypes);
							variables.qGetDefaultLeadType = variables.qObj.execute(sql=variables.sql, dbtype="query").getResult();

							variables.defaultLeadTypeID = variables.qGetDefaultLeadType.lookupID;
							variables.leadTypeIDDisplay = variables.qGetDefaultLeadType.translatedItemText;
						}
					</cfscript>
					<field name="leadTypeID" control="hidden" default="#variables.defaultLeadTypeID#"/>
					<!--- 2015-02-10	RPW	Leads - Added XmlFormat. --->
					<field name="leadTypeID_Display" label="phr_lead_leadTypeID" value="#XmlFormat(variables.leadTypeIDDisplay)#" readonly="true"/>
				<cfelse>
					<!--- 2015-02-10	RPW	Leads - Added leadTypeID.required. --->
	            	<field name="leadTypeID" label="phr_lead_leadTypeID" type="select" query="func:com.relayForms.getLookupList(fieldname='leadtypeid')" display="translatedItemText" value="LookupID" required="#variables.fieldDisplay.leadTypeID.required#"/>
				</cfif>
			</cfif>


			<!--- 2014-09-16			RPW			Add Lead Screen on Portal - Change Enquiry Type to use lookUpList --->
			<!--- 2015-02-10	RPW	Leads - Added enquiryType.display. --->
			<cfif variables.fieldDisplay.enquiryType.display>
			<field name="enquiryType" label="phr_lead_enquiryType" type="select" query="func:com.relayForms.getLookupList(fieldname='leadEnquiryTypeID')" display="translatedItemText" value="LookupID" required="#variables.fieldDisplay.enquiryType.required#"/>
			</cfif>

			<cfif variables.fieldDisplay.description.display>
            <field name="description" label="phr_lead_description" control="textarea" required="#variables.fieldDisplay.description.required#" readonly="#variables.fieldDisplay.description.readonly#"/>
			</cfif>
			<!--- lookup --->
			<!--- 2015-02-10	RPW	Leads - Added sourceID.display. --->
			<cfif variables.fieldDisplay.sourceID.display>
				<field name="sourceID" type="select" query="func:com.relayForms.getLookupList(fieldname='leadsourceid')" display="translatedItemText" value="LookupID" required="#variables.fieldDisplay.sourceID.required#" readonly="#variables.fieldDisplay.sourceID.readonly#"/>
			<cfelse>
				<!--- NJH 2015/08/04 - add ability to default the source field --->
				<cfscript>
					variables.qGetDefaultSourceID = application.com.relayForms.getLookupList(fieldName="leadSourceId");
					variables.defaultSourceID = variables.qGetDefaultSourceID.lookupID[1];
				</cfscript>
				<field name="sourceID" control="hidden" default="#variables.defaultSourceID#"/>
			</cfif>
			<cfif variables.fieldDisplay.annualRevenue.display>
    	        <field name="annualRevenue" label="phr_lead_annualRevenue" range="0,9999999999.99" required="#variables.fieldDisplay.annualRevenue.required#"/>
			</cfif>
			<cfif variables.fieldDisplay.currency.display>
            <!--- 2014/10/01	YMA	CORE-652 fields that are required on SF need to be required in RW --->
			<!--- 2015-02-10	RPW	Leads - Added currency.required. --->
            <field name="currency" label="phr_lead_currency" display="currencyISO" value="currencyISO" nullText="" bindfunction="cfc:relay.webservices.relaycountries.getCurrenciesForCountry(countryID={countryID})" bindOnLoad="true" required="#variables.fieldDisplay.currency.required#" />
			<cfelse>
				<cfscript>
					variables.currencyISO = application.com.relayLeads.GetLead(LeadID=LeadID).currency;

					if (!Len(variables.currencyISO)) {
						if (structKeyExists(variables.fieldDisplay.currency,"default")) {
							variables.currencyISO = variables.fieldDisplay.currency.default;
						} else {
							if (structKeyExists(variables,"countryID")) {
								variables.currencyCountryID = variables.countryID;
							} else {
								variables.currencyCountryID = request.relayCurrentUser.countryID;
							}
							variables.qGetCurrencies = application.com.relayEcommerce.getCurrencies(variables.currencyCountryID);
							variables.currencyISO = variables.qGetCurrencies.countrycurrency;
						}
					}
				</cfscript>
				<field name="currency" control="hidden" default="#variables.currencyISO#"/>
			</cfif>

			<!--- lookup --->
			<!--- 2015-02-10	RPW	Leads - Added statusID.display, statusID.readonly. --->
			<cfscript>
				variables.statusID = application.com.relayLeads.GetLead(LeadID=LeadID).approvalStatusID;
				if (not IsNumeric(variables.statusID)) {
					variables.statusID = application.com.relayForms.getDefaultLookupListForField(fieldname="leadApprovalStatusID").lookupID;
				}
			</cfscript>

			<cfif variables.fieldDisplay.statusID.display>
				<cfif variables.fieldDisplay.statusID.readonly>
					<cfscript>

						if (IsNumeric(variables.statusID)) {
							variables.statusIDDisplay = application.com.relayForms.getLookupList(lookupID=variables.statusID).translatedItemText;
						} else {
							variables.statusIDDisplay = "";
						}
					</cfscript>
					<field name="approvalStatusID_Display" label="phr_opportunity_statusID" value="#XmlFormat(variables.statusIDDisplay)#" readonly="true"/>
					<field name="approvalStatusID" control="hidden"/>
				<cfelse>
					<!--- 2014/10/01	YMA	CORE-652 fields that are required on SF need to be required in RW --->
            		<field name="approvalStatusID" label="phr_opportunity_statusID" required="#variables.fieldDisplay.ApprovalStatusID.required#" type="select" query="func:com.relayForms.getLookupList(fieldname='leadApprovalStatusID')" display="translatedItemText" value="LookupID"/>
				</cfif>
			<cfelse>
				<cfscript>
					if (IsNumeric(variables.statusID)) {
						variables.defaultStatusID = variables.statusID;
					} else {
						variables.qGetStatus = application.com.relayForms.getLookupList(fieldName="leadApprovalStatusID");
						variables.sql = "SELECT * FROM qTable WHERE lookUpTextID = 'Pending'";
						variables.qObj = new Query();
						variables.qObj.setAttributes(qTable = variables.qGetStatus);
						variables.qGetDefaultStatus = variables.qObj.execute(sql=variables.sql, dbtype="query").getResult();

						variables.defaultStatusID = variables.qGetDefaultStatus.lookupID;
					}
				</cfscript>

				<field name="approvalStatusID" control="hidden" default="#variables.defaultStatusID#"/>
			</cfif>
            <field name="rejectionReason" label="phr_lead_rejectionReason" control="textArea"/>
			<!--- 2015-02-10	RPW	Leads - Added acceptedByPartner.display, acceptedByPartner.required. --->
			<cfif variables.fieldDisplay.acceptedByPartner.display>
            <field name="acceptedByPartner" label="phr_lead_acceptedByPartner" required="#variables.fieldDisplay.acceptedByPartner.required#"/>
			</cfif>
			<!--- 2015-02-10	RPW	Leads - Added name to Contacts for ordering purposes. --->
			<cfif variables.fieldDisplay.contacts.display><group label="Contacts" collapse="yes" name="Contacts"></cfif>

				<cfscript>
					variables.partnerLocationID = application.com.relayLeads.GetLead(LeadID=LeadID).partnerLocationID;
					// only default to current user if we are adding a new lead
					if (!request.relayCurrentUser.isInternal && structKeyExists(URL,"add") && URL.add && variables.partnerLocationID < 1) {
						variables.partnerLocationID	= request.relayCurrentUser.locationID;
					}

                    variables.countryID = application.com.relayLeads.GetLead(LeadID=LeadID).countryID;
                    if (!IsNumeric(variables.countryID)) {
                        variables.countryID = request.relayCurrentUser.countryID;
                    }
                    variables.partnerLocationIDDisplay = application.com.opportunity.getOppPartners(countryId=variables.countryID,term=variables.partnerLocationID).siteName;
				</cfscript>
				<cfif variables.fieldDisplay.partnerLocationID.display>
					<cfif variables.fieldDisplay.partnerLocationID.readonly>
						<field name="partnerLocationID" control="hidden" default="#variables.partnerLocationID#"/>
						<!--- 2015-02-10	RPW	Leads - Added XmlFormat. --->
						<!---2015/12/04	ESZ	case 446170 Incorrect Reseller showing for Lead--->
                        <field name="partnerLocationID_Display" label="#variables.fieldDisplay.partnerLocationID.label#" readonly="true" control="SELECT"
                            query ="select	distinct l.locationID as value, l.siteName +', '+ l.address4 +' ('+ c.ISOCode +')' as display from location l
									inner join country c on l.countryID = c.countryID where LocationID=(select partnerLocationID from Lead where LeadID='#LeadID#')"/>
                        />
					<cfelse>
						<!--- 2015-02-10	RPW	Leads - Added partnerLocationID.required. --->
						<field name="partnerLocationID" label="#variables.fieldDisplay.partnerLocationID.label#" display="siteName" showNull="true" nullText="phr_opp_NoPartnerAssigned" value="locationID" bindfunction="cfc:relay.webservices.relayOpportunityWS.getOpportunityPartners(countryID={countryID})" bindOnLoad="true" required="#variables.fieldDisplay.partnerLocationID.required#" />
					</cfif>
				<cfelse>
					<cfscript>
						if (!IsNumeric(variables.partnerLocationID)) {
							variables.partnerLocationID = request.relayCurrentUser.locationID;
						}
					</cfscript>
					<field name="partnerLocationID" control="hidden" default="#variables.partnerLocationID#"/>
				</cfif>

				<!--- START: 2015-01-15 MS         P-AER001 --->
				<cfif structKeyExists(variables.fieldDisplay.partnerSalesPersonID,"readonly")>
					<cfset variables.partnerSalesPersonIDReadOnly = variables.fieldDisplay.partnerSalesPersonID.readonly>
				<cfelse>
					<cfset variables.partnerSalesPersonIDReadOnly = false>
				</cfif>
				<!--- ENDn : 2015-01-15 MS         P-AER001 --->

				<cfif variables.fieldDisplay.partnerSalesPersonID.display>
                    <cfif variables.fieldDisplay.countryID.readonly>
						<cfif IsNumeric(variables.partnerLocationID)>
							<!--- 2015-02-10	RPW	Leads - MS Added variables.partnerSalesPersonIDReadOnly. --->
							<field name="partnerSalesPersonID" label="phr_lead_partnerSalesPersonID" nullText="phr_opp_NoSalesPersonAssigned" display="fullname" value="personID" query="func:com.opportunity.getOppPartnerSalesPerson(#variables.partnerLocationID#)" readonly="#variables.partnerSalesPersonIDReadOnly#"/>
						<cfelse>
							<field name="partnerSalesPersonID_Display" label="phr_lead_partnerSalesPersonID" value="" readonly="true"/>
						</cfif>
					<cfelse>
						<!--- 2015-02-10	RPW	Leads - MS Added variables.partnerSalesPersonIDReadOnly. --->
						<cfif variables.partnerSalesPersonIDReadOnly>
							<cfscript>
								variables.partnerSalesPersonIDDisplay = "";

								// 2015-03-06	RPW	Defaulted partnerSalesPersonID on add to current user if read only.
								if(structKeyExists(URL,"add") && URL.add) {
									variables.partnerSalesPersonID = request.relaycurrentuser.personid;
								} else {
									variables.partnerSalesPersonID = application.com.relayLeads.GetLead(LeadID=LeadID).partnerSalesPersonID;
								}

								if (IsNumeric(variables.partnerSalesPersonID)) {
									variables.qGetPerson = application.com.relayLeads.GetPerson(variables.partnerSalesPersonID);
									if (variables.qGetPerson.recordCount) {
										variables.partnerSalesPersonIDDisplay = variables.qGetPerson.FirstName & " " & variables.qGetPerson.LastName;
									}
								}
							</cfscript>
							<!--- 2015-03-24	RPW	Changed value to default for partnerSalesPersonID and outputted variable. --->
							<field name="partnerSalesPersonID" control="hidden" default="#partnerSalesPersonID#"/>
							<field name="partnerSalesPersonID_Display" label="phr_lead_partnerSalesPersonID" value="#XmlFormat(variables.partnerSalesPersonIDDisplay)#" readonly="true"/>
						<cfelse>
							<!--- 2015-03-10	RPW	Fixed partnerSalesPersonID dropdown population which depends on partnerLocationID --->
							<!--- 2015-11-12	YMA	Updated this logic... it was reverse to how it should have been to work.
								NJH 2016/11/02 JIRA PROD2016-2638 - if down here and partner location is readonly, then we want to bind on load
								--->
							<cfscript>
								if (variables.fieldDisplay.partnerLocationID.display) {
									if (variables.fieldDisplay.partnerLocationID.readonly) {
										variables.bindOnLoad = true;
									} else {
										variables.bindOnLoad = false;
									}
								} else {
									variables.bindOnLoad = true;
								}
							</cfscript>
							<field name="partnerSalesPersonID" label="phr_lead_partnerSalesPersonID" nullText="phr_opp_NoSalesPersonAssigned" display="fullname" value="personID" bindfunction="cfc:relay.webservices.relayOpportunityWS.getOppPartnerSalesPerson({partnerLocationID})" bindOnLoad="#variables.bindOnLoad#" readonly="#variables.partnerSalesPersonIDReadOnly#" required="#variables.fieldDisplay.partnerSalesPersonID.required#"/>
						</cfif>
					</cfif>
				<cfelse>
					<field name="partnerSalesPersonID" control="hidden" default="#request.relayCurrentUser.PersonID#"/>
				</cfif>

				<cfif variables.fieldDisplay.partnerSalesManagerPersonID.display AND isNumeric(variables.partnerLocationID)>
				<!--- 2015-02-10	RPW	Leads - Added partnerSalesManagerPersonID.required. --->
				<field name="partnerSalesManagerPersonID" label="phr_lead_partnerSalesManagerPersonID" nullText="phr_opp_NoSalesPersonAssigned" display="fullname" value="personID" bindfunction="cfc:relay.webservices.relayOpportunityWS.getOppPartnerSalesPerson({partnerLocationID})" bindOnLoad="false" required="#variables.fieldDisplay.partnerSalesManagerPersonID.required#"/>
				<cfelse>
					<field name="partnerSalesManagerPersonID" control="hidden" value="personID"/>
				</cfif>
				<cfif variables.fieldDisplay.sponsorPersonID.display>
				<!--- 2015-02-10	RPW	Leads - Added sponsorPersonID.required. --->
            	<lineItem label="phr_lead_sponsorPersonID" required="#variables.fieldDisplay.sponsorPersonID.required#">
					<field name="sponsorFirstname"/>
					<field name="sponsorLastname"/>
				</lineItem>
				</cfif>
				<!--- 2015-02-10	RPW	Leads - Added distiLocationID.display, distiLocationID.readonly. --->
				<cfif variables.fieldDisplay.distiLocationID.display>
					<!--- <cfif variables.fieldDisplay.distiLocationID.readonly>
				<field name="distiLocationID" label="phr_lead_distiLocationID" nullText="phr_opp_NoDistributorAssigned" bindfunction="cfc:relay.webservices.relayOpportunityWS.getOppDistiLocations(countryID={countryID},locationID={partnerLocationID})" display="name" value="ID" makeHidden="true" readonly="true" filter="accountTypeID=[[application.organisationType.endCustomer.organisationTypeID]]"/>
					<cfelse>
				<field name="distiLocationID" label="phr_lead_distiLocationID" nullText="phr_opp_NoDistributorAssigned" bindfunction="cfc:relay.webservices.relayOpportunityWS.getOppDistiLocations(countryID={countryID},locationID={partnerLocationID})" display="name" value="ID" makeHidden="true" readonly="false" filter="accountTypeID=[[application.organisationType.endCustomer.organisationTypeID]]" required="#variables.fieldDisplay.distiLocationID.required#"/>
					</cfif> --->
					<cfparam name="variables.fieldDisplay.distiLocationID.bindFunction" default="cfc:relay.webservices.callWebService.callWebService(webServiceName='relayOpportunityWS',methodName='getOppDistiLocations',countryID={countryID},locationID={partnerLocationID})">
					<cfparam name="variables.fieldDisplay.distiLocationID.query" default="">
					<cfparam name="variables.fieldDisplay.distiContactPersonID.bindOnLoad" default="false">

					<field name="distiLocationID" label="phr_lead_distiLocationID" nullText="phr_opp_NoDistributorAssigned" #structKeyExists(variables.fieldDisplay.distiLocationID,'default')?'default="'&variables.fieldDisplay.distiLocationID.default&'"':''# query="#variables.fieldDisplay.distiLocationID.query#" bindfunction="#variables.fieldDisplay.distiLocationID.bindFunction#" display="name" value="ID" makeHidden="true" filter="accountTypeID=[[application.organisationType.endCustomer.organisationTypeID]]" readonly="#variables.fieldDisplay.distiLocationID.readonly#" required="#variables.fieldDisplay.distiLocationID.readonly?false:variables.fieldDisplay.distiLocationID.required#"/>
				</cfif>
				<!--- 2015-02-10	RPW	Leads - Added distiContactPersonID.display. --->
				<cfif variables.fieldDisplay.distiContactPersonID.display>
					<field name="distiContactPersonID" label="phr_lead_distiContactPersonID" nullText="phr_opp_NoSalesPersonAssigned" #structKeyExists(variables.fieldDisplay.distiContactPersonID,'default')?'default="'&variables.fieldDisplay.distiContactPersonID.default&'"':''# display="itemtext" value="personID" bindfunction="cfc:relay.webservices.relayOpportunityWS.getDistiSalesPerson(locationID={distiLocationID})" bindOnLoad="#variables.fieldDisplay.distiContactPersonID.bindOnLoad#" required="#variables.fieldDisplay.distiContactPersonID.required#" readOnly="#variables.fieldDisplay.distiContactPersonID.readOnly#"/>
				<cfelse>
					<field name="distiContactPersonID" control="hidden" value="personID"/>
				</cfif>

				<cfscript>
					variables.vendorAccountManagerPersonID = application.com.relayLeads.GetLead(LeadID=LeadID).vendorAccountManagerPersonID;
				</cfscript>
				<cfif variables.fieldDisplay.vendorAccountManagerPersonID.display>
					<cfif variables.fieldDisplay.vendorAccountManagerPersonID.readonly>
						<cfscript>
							if (IsNumeric(variables.vendorAccountManagerPersonID)) {
								variables.qGetPerson = application.com.relayLeads.GetPerson(variables.vendorAccountManagerPersonID);
								if (variables.qGetPerson.recordCount) {
									variables.vendorAccountManagerPersonIDDisplay = variables.qGetPerson.FirstName & " " & variables.qGetPerson.LastName;
								} else {
									variables.vendorAccountManagerPersonIDDisplay = "";
								}
							} else {
								variables.vendorAccountManagerPersonIDDisplay = "";
							}
						</cfscript>
						<field name="vendorAccountManagerPersonID" control="hidden" default="#variables.vendorAccountManagerPersonID#"/>
						<field name="vendorAccountManagerPersonID_orig" control="hidden" default="#variables.vendorAccountManagerPersonID#"/>
						<!--- 2015-02-10	RPW	Leads - Added XmlFormat. --->
						<field name="vendorAccountManagerPersonID_Display" label="phr_opp_Account_Manager" value="#XmlFormat(variables.vendorAccountManagerPersonIDDisplay)#" readonly="true"/>
					<cfelse>
						<!--- 2015-02-10	RPW	Leads - Added vendorAccountManagerPersonID.required. --->
	    	          	<field name="vendorAccountManagerPersonID" label="phr_opp_Account_Manager" query="func:com.opportunity.getOppVendorAccountManagers()" nullText="phr_opp_PleaseSelectAnAccountManager" display="personName" value="personID" required="#variables.fieldDisplay.vendorAccountManagerPersonID.required#" />
					</cfif>
				<cfelse>

					<!--- 2015-02-10	RPW	Leads - Fixed variables.partnerLocationID. --->
					<cfscript>
						if (!IsNumeric(variables.vendorAccountManagerPersonID) && IsDefined("variables.partnerLocationID")) {
							// START 2014-11-24 AXA CORE-117 added check for new settings to switch flag for vendoraccountmanager
							// JIRA PROD2016-350 - NJH 2016/05/19 renamed setting
							variables.bUseOrg = application.com.settings.getSetting("plo.useLocationAsPrimaryPartnerAccount");
							if(variables.bUseOrg){
								variables.vendorFlagTextID = 'locAccManager';
							} else {
								variables.vendorFlagTextID = 'accManager';
							}
							// END 2014-11-24 AXA CORE-117 added check for new settings to switch flag for vendoraccountmanager

							variables.parterAccManagerPersonID = application.com.opportunity.getPartnerAccountManager(partnerPersonID=request.relayCurrentUser.PersonID);
							if (variables.parterAccManagerPersonID != 0) {
								variables.vendorAccountManagerPersonID = variables.parterAccManagerPersonID;
							} else {
								if (IsNumeric(variables.partnerLocationID) && variables.partnerLocationID) {
									variables.flagData = application.com.flag.getFlagData(flagID = variables.vendorFlagTextID,entityid = variables.partnerLocationID); /* 2014-11-24 AXA CORE-117 use variable instead of hardcoded textid */
									variables.vendorAccountManagerPersonID = variables.flagData.data;
								}
							}
						}
					</cfscript>

					<field name="vendorAccountManagerPersonID" control="hidden" default="#variables.vendorAccountManagerPersonID#"/>
				</cfif>

				<!--- 2015-02-10	RPW	Leads - Added vendorSalesManagerPersonID.display. --->
				<cfif variables.fieldDisplay.vendorSalesManagerPersonID.display>
              	<field name="vendorSalesManagerPersonID" label="phr_lead_vendorSalesManagerPersonID" query="func:com.opportunity.getvendorSalesManagerPersonID()" nullText="Phr_ChooseVendorSalesManager" display="ItemText" value="ID" required="#variables.fieldDisplay.vendorSalesManagerPersonID.required#" />
				</cfif>
				<cfif variables.fieldDisplay.contacts.display></group></cfif>

				<!--- 2015-02-10	RPW	Leads - Added function to display custom fields. --->
				<cfscript>
					variables.customFields = application.com.leads.GetCustomFields(isInternal=request.relayCurrentUser.isInternal,mode=variables.mode,leadID=leadID);
					writeoutput(variables.customFields);
				</cfscript>

			<cfif request.relayCurrentUser.isInternal>
				<group label="Update History" collapse="yes">
					<field name="viewedByPartnerDate" label="phr_lead_viewedByPartnerDate"/>
	              	<field name="acceptedByPartnerDate" label="phr_lead_acceptedByPartnerDate"/>
	              	<field name="convertedLocationID" label="phr_lead_convertedLocationID" type="text" readonly="true"/>
	              	<field name="convertedPersonID" label="phr_lead_convertedPersonID" type="text" readonly="true"/>
	              	<field name="convertedOpportunityID" label="phr_lead_convertedOpportunityID" type="text" readonly="true"/>
				</group>
			</cfif>
		</editor>
	</editors>
	</cfoutput>
</cfsavecontent>

<!--- 2015-02-10	RPW	Leads - Added ordering for XML. --->
<cfscript>
	if (!request.relayCurrentUser.isInternal) {
		variables.sortOrder = variables.fieldDisplay.sortOrder;
		variables.xmlSource = application.com.leads.OrderXmlSource(xmlSource = variables.xmlSource,sortOrder = variables.sortOrder);
	}
</cfscript>

<cfelse>

	<!--- 2014-08-29	RPW	Add to Home Page options to display Activity Stream and Metrics Charts --->
	<cfscript>
		variables.filter = [];
		if (!request.relayCurrentUser.isInternal and not structKeyExists(variables,"showAccountLeads")) {
			// NJH 2016/11/28 - JIRA PROD2016-2642 - removed the converted clause as that is done in rights. Here we are just getting the partner leads if not getting account leads
			ArrayAppend(variables.filter,"partnerSalesPersonID="&request.relayCurrentUser.personID);
			// NJH 2016/07/19 JIRA PROD-599 Done in rights - we actually want primary contacts to see leads that are not assigned anyways...ArrayAppend(variables.filter,"partnerSalesPersonID = " & request.relayCurrentUser.personid);
		}

		//START:2015-06-15	MS	P-CML001 Kanban 957 Added new param leadTypeIDFilter to enable user to create lead listing screens based on leadType
		if(leadTypeIDFilter != '')
		{
			ArrayAppend(variables.filter,"l.leadTypeID = " & leadTypeIDFilter);
		}
		//END:  2015-06-15	MS	P-CML001 Kanban 957 Added new param leadTypeIDFilter to enable user to create lead listing screens based on leadType
		param name="leadTypeID" default="-1"; //-1 means do not filter lead types
		variables.qLeadList = application.com.relayLeads.GetLeads("data",sortOrder,filter,leadTypeID);
	</cfscript>

</cfif>

<cfparam name="keyColumnOnCLickList" default="javascript:openNewTab('Edit lead ##leadID##'*comma'Edit lead ##leadID##'*comma'/lead/leads.cfm?editor=yes&hideBackButton=true&leadID=##application.com.security.encryptVariableValue(name='leadID'*commavalue=leadID)##'*comma{iconClass:'leadmanager'});return false;">
<cfif NOT request.relaycurrentuser.isinternal>
	<cfset keyColumnOnCLickList = "javascript:editLead('##application.com.security.encryptVariableValue(name='leadID'*commavalue=leadID)##');return false">
</cfif>

<cfscript>
//2014-10-01			RPW			CORE-684 Converted column in leads screen is redundant
//2015-03-06	RPW	FIFTEEN-280 - Page  is not displaying properly after edit and saved the lead record
if (request.relayCurrentUser.isInternal) {

	variables.saveAndReturn = "no";
	variables.showTheseColumns="leadID,country,Company,name,email,created";
	variables.columnHeadingList="Lead ID,Country,Company,Name,Email,Created";

} else {

	variables.saveAndReturn = saveAndReturn;
	//START: 2015-06-17	MS	P-CML001 Kanban 957 Ability to hide or show columns on Lead Listing Screen by passing in a param in the relay tag using Contact Manager
	variables.showTheseColumns=showTheseColumns;
	variables.columnHeadingList=columnHeadingList;
	//END  : 2015-06-17	MS	P-CML001 Kanban 957 Ability to hide or show columns on Lead Listing Screen by passing in a param in the relay tag using Contact Manager
}
</cfscript>
<!--- 2014-09-22 AXA CORE-654 dropdown list of actions against the listing page. --->
<!--- 2014/10/06	YMA	CORE-714 function dropdown should not display on the portal. --->
<cfset functionListQuery = QueryNew( "functionID,functionName,securityLevel,url,windowFeatures" )>
<cfif request.relayCurrentUser.isInternal>
	<cfinclude template="leadFunctions.cfm">
	<cfset functionListQuery = "#comTableFunction.qFunctionList#">
</cfif>
<!--- 2014-09-22 AXA CORE-654 Added params to add checkboxes to listing and functions to perform on the selected items --->
<!--- 2014-10-03			RPW			CORE-712 The screen on the portal which appears after clicking the new lead save button... has a not found error - removed backForm value --->
<!--- <cfif structKeyExists(url,"editor") OR qLeadList.recordCount GTe 0> ---><!--- 2014-09-22 AXA CORE-654 call tfqo if in edit mode, or there are records to display on listing page --->
<!--- 2015-03-06	RPW	FIFTEEN-280 - Page  is not displaying properly after edit and saved the lead record --->

<!--- remove any filters columns that are not in the showColumnList --->
<cfloop list="#filterColumnList#" index="filterCol">
	<cfif not listfindNoCase(variables.showTheseColumns,filterCol)>
		<cfset filterColumnList = listDeleteAt(filterColumnList,listFindNoCase(filterColumnList,filterCol))>
	</cfif>
</cfloop>

<cf_listAndEditor
	xmlSource="#xmlSource#"
	cfmlCallerName="leads"
	keyColumnOnClickList="#keyColumnOnCLickList#"
	showTheseColumns="#variables.showTheseColumns#"
	columnHeadingList="#columnHeadingList#"
	keyColumnList="#keyColumnList#"
	keyColumnKeyList=""
	keyColumnURLList="javascript:void(0)"
	useInclude="false"
	sortOrder="#sortOrder#"
	queryData="#qLeadList#"
	numRowsPerPage="#numRowsPerPage#"
	startRow="#startRow#"
	postSaveFunction="#triggerLeadEmails#"
	rowIdentityColumnName="leadID"
	functionListQuery="#functionListQuery#"
	hideTheseColumns=""
	dateFormat="created"
	backForm="#backForm#"<!--- 2015-04-16	MS	P-AER001 Added VAR #backForm# instead of an EMPTY String --->
	hideBackButton="true"
	treatEmtpyFieldAsNull="true"
	searchColumnList=""
	booleanFormat="converted"
	saveAndReturn="#variables.saveAndReturn#"
	columnTranslation="True"
	columnTranslationPrefix="Phr_Lead_"
	FilterSelectFieldList = "#filterColumnList#"
	FilterSelectFieldList2="#listLen(filterColumnList) gt 1?filterColumnList:''#"
	showTextSearch="true"
	dateFilter="#listFindNoCase(variables.showTheseColumns,dateFilter)?dateFilter:''#"
>
<!---
<cfelse><!--- 2014-09-22 AXA CORE-654 call tfqo if in edit mode, or there are records to display on listing page --->
<h3 class="leads">phr_lead_norecords</h3><!--- 2014-09-22 AXA CORE-654 displays message if there are no records to display --->
</cfif><!--- 2014-09-22 AXA CORE-654 call tfqo if in edit mode, or there are records to display on listing page --->
 --->

<!--- 2014-11-18	RPW	CORE-97 Div to display fancybox form. --->
<cfoutput>
<div id="convertDataForm"></div>
</cfoutput>