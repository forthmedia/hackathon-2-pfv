<!--- �Relayware. All Rights Reserved 2014 --->
<CFQUERY NAME="getAssigned" DATASOURCE="#application.SiteDataSource#">
	SELECT actionStatusID,status from actionStatus WHERE Assigned = 1
</CFQUERY>
<!--- LID:4314 MS 13JAN2011 To Display campaigns registered against actions --->
<CFIF application.com.settings.getSetting("campaigns.displayCampaigns") and qryCampaignID neq "">
	<CFQUERY NAME="getCampaign" DATASOURCE="#application.SiteDataSource#">
		select campaignName from campaign where campaignID =  <cf_queryparam value="#qryCampaignID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</CFQUERY>
</CFIF>
<!--- assume that the following fields are defined (even if "")
		#qryDescription#
		#qryDeadline#
		#qryOwnerID#
		#qryCreatorID#
		#qryStatusID#
		#qryDeliverableID#
		#qryNotes#
		#qryPermission#

		#page_header#
		#function#   "edit" or "add"
		--->

<cf_title>Actions</cf_title>


<CFSET current ="ActionViewForm.cfm">

<!--- NJH 2008-05-20 P-TND066 --->
<cfif structKeyExists(url,"mediaMode") and url.mediaMode neq "print">
<cfinclude template="/actions/actionTopHead.cfm">
</cfif>

<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" CLASS="withBorder">
	<!--- NJH 2008-05-20 P-TND066 start --->
	<CFIF isdefined("actionid")>
	<TR><TD VALIGN="top" CLASS="label">Action ID: </TD>
	     <TD><CFOUTPUT>#htmleditformat(actionid)#</CFOUTPUT></TD></TR>
	</cfif>
	<TR><TD ALIGN="left" class="label">Action With: </TD>
		<TD><CFOUTPUT>#htmleditformat(qryEntityName)#</CFOUTPUT></TD>
	</TR>
	<!--- NJH 2008-05-20 P-TND066 end --->
	<TR><TD ALIGN="left" class="label">Description: </TD>
		<TD><CFOUTPUT>#HTMLEditFormat(qryDescription)#</CFOUTPUT></TD>
	</TR>
	<!--- NJH 2008-05-20 P-TND066 start --->
	<TR><TD ALIGN="left" class="label">Requested By: </TD>
		<TD><CFOUTPUT>#HTMLEditFormat(qryRequestedBy)#</CFOUTPUT></TD>
	</TR>
	<!--- LID:4314 MS 13JAN2011 Displaying campaigns registered against actions --->
	<CFIF application.com.settings.getSetting("campaigns.displayCampaigns")>
	<TR><TD ALIGN="left" class="label">Campaign: </TD>
		<TD><cfif qryCampaignID neq "">
				<CFOUTPUT>#htmleditformat(getCampaign.campaignName)#</CFOUTPUT>
			<cfelse>
				No campaign recorded
			</cfif>
		</TD>
	</TR>
	</CFIF>
	<TR><TD ALIGN="left" class="label">Action Added: </TD>
		<TD><CFOUTPUT>#DateFormat(qryDateEntered,"dd-mmm-yyyy")#</CFOUTPUT></TD>
	</TR>
	<!--- NJH 2008-05-20 P-TND066 end --->
	<TR><TD ALIGN="left"class="label">Deadline: </TD>
		<TD><CFOUTPUT>#DateFormat(qryDeadline,"dd-mmm-yyyy")#
			<cfif len(qryTimeSpent) neq 0>
				&nbsp;&nbsp;&nbsp;Time Estimate:#timeFormat(qryTimeSpent,"hh:mm")#
			</cfif>
		</CFOUTPUT></TD>
	</TR>
	<TR><TD ALIGN="left" class="label">Status: </TD>
		<TD>
		<CFOUTPUT QUERY="getStatus">
		 	<CFIF actionStatusID IS qryStatusID>#HTMLEditFormat(Status)#</CFIF>
		 </CFOUTPUT>
		 <cfif len(qryCompletedDate) neq 0>
			 <CFOUTPUT>&nbsp;&nbsp;&nbsp;Completed At:#TimeFormat(qryCompletedDate,"HH:mm")#</CFOUTPUT>
		 </cfif>
	    </TD>
	</TR>

	<TR><TD ALIGN="LEFT" class="label">Owner: </TD>
		<TD><CFOUTPUT QUERY="getOwners">
		  	<CFIF ownerPersonID IS qryOwnerID>#HTMLEditFormat(FullName)# (#HTMLEditFormat(OrganisationName)#)</CFIF>
		 </CFOUTPUT>
		 </TD></TR>
	<!--- <TR><TD ALIGN="LEFT"><CFOUTPUT>#Deliverable#</CFOUTPUT>: </TD>
		<TD><CFOUTPUT QUERY="getDeliverables">
	       <CFIF #DeliverableID# IS #qryDeliverableID#>#HTMLEditFormat(Deliverable)#<CFELSE>
		   </CFIF>
		 </CFOUTPUT>
	     </TD></TR>
	 --->
	<TR><TD ALIGN="LEFT" VALIGN="TOP"class="label">Notes: </TD>
		<TD VALIGN="TOP"><CFOUTPUT>#HTMLEditFormat(qryNotes)#</CFOUTPUT></TD></TR>



	<CFSET previousowner="">
	<CFSET previousstatus="">
		<tr>
			<th colspan="2" align="center">
				ACTION HISTORY
			</th>
		</tr>
	<cfif getNotes.recordcount eq 0>
		<tr>
			<td colspan="2" align="center">
				No History for this action
			</td>
		</tr>
	<cfelse>
		<CFLOOP query="getnotes">
			<CFOUTPUT>

			<TR style="page-break-after:auto">
				<td align="right" valign="top" class="label">
					#htmleditformat(initials)# <BR>
					#DateFormat(lastUpdated,"dd/mm/yy")#<BR>
					#htmleditformat(type)#
				</td>
				<TD BGCOLOR="FFFFCE" valign="top" >
				<CFIF summary IS NOT ""><i>#htmleditformat(summary)#</i><BR></CFIF>
				<CFIF len(notetext) gt 0>#replace(notetext, chr(13), "<BR>","ALL")#<BR></CFIF>
				<CFIF previousowner IS NOT newowner>Allocated to #htmleditformat(getNotes.newOwnerName)# by #htmleditformat(getNotes.initials)# <BR></CFIF>
				<CFIF previousstatus IS NOT newstatus>Status changed to #htmleditformat(getNotes.newStatusText)#<BR></CFIF>
				<CFIF datestart IS NOT "">Started:  #TimeFormat(datestart,"HH:mm")# &nbsp #DateFormat(datestart,"dd-mmm-yyyy")# <BR></CFIF>
				<CFIF dateend IS NOT "">Finished: #TimeFormat(dateend,"HH:mm")# &nbsp #DateFormat(dateend,"dd-mmm-yyyy")# </CFIF>
				<CFIF timetaken IS NOT "">TimeTaken:  #TimeFormat(timetaken,"HH:mm")#<CFELSEIF timetakenI IS NOT "">TimeTaken (in minutes):  #htmleditformat(timetakenI)#</CFIF>
				<CFIF topic IS NOT "">Topic: #htmleditformat(topic)#<BR></CFIF>
				<CFIF initiator IS NOT "">Initiator: #htmleditformat(initiator)#<BR></CFIF>
				<CFIF type IS NOT "">Category: #htmleditformat(type)#<BR></CFIF>
				<CFIF calls IS NOT "">Amount of calls: #htmleditformat(calls)#<BR></CFIF>
				<CFIF emails IS NOT "">Amount of emails: #htmleditformat(emails)#<BR></CFIF>
				</TD>
			</TR>

			</CFOUTPUT>

		<CFSET previousowner=newowner>
		<CFSET previousstatus=newstatus>

		</CFLOOP>
	</cfif>
<!--- 	<TR><TD></TD><TD><INPUT TYPE="button" VALUE="Return To Action List" onClick="history.back()"></TD></TR> --->
</TABLE>


<!--- NJH 2008-05-20 P-TND066 --->
<cfif structKeyExists(url,"mediaMode") and url.mediaMode neq "print">
<CFIF request.query_string IS NOT "">
	<FORM ACTION="ActionList.cfm?<CFOUTPUT>#htmleditformat(request.query_string)#</CFOUTPUT>" METHOD="POST">
<CFELSE>
	<FORM ACTION="ActionList.cfm" METHOD="GET">
</CFIF>
</FORM>
</cfif>



