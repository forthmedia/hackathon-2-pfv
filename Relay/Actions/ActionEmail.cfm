<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			actionEmail.cfm	
Author:				SWJ
Date started:			/xx/02
	
Description:			

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2004-06-03    WAB 	Added test for blank emails - probably unnecessary 'cause turned out that email was blank because query was wrong!

Possible enhancements:


 --->
<cfparam name="portalaction" default="0">

<cfif isDefined("ActionID") and isNumeric(actionID)>
 
<CFQUERY NAME="getActionDetails" dataSource="#application.siteDataSource#">
	SELECT * FROM actions a 
		INNER JOIN actionType at on a.actionTypeID=at.actionTypeID
	WHERE a.ActionID =  <cf_queryparam value="#ActionID#" CFSQLTYPE="CF_SQL_INTEGER" > 
</CFQUERY>

<cfif isDefined("getActionDetails.entityTypeID") and getActionDetails.entityTypeID neq "" and getActionDetails.entityID neq "">
	<cfif getActionDetails.entityTypeID eq "0">
		<CFQUERY NAME="getEntityDetails" dataSource="#application.siteDataSource#">
			select p.firstName+' '+p.lastName+' - '+ o.organisationName as entityName from person p
			INNER JOIN organisation o ON o.organisationID = p.organisationID
			where p.personID =  <cf_queryparam value="#getActionDetails.entityID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</CFQUERY>
	<cfelseif getActionDetails.entityTypeID eq "2">
		<CFQUERY NAME="getEntityDetails" dataSource="#application.siteDataSource#">
			select organisationName as entityName from organisation where organisationID =  <cf_queryparam value="#getActionDetails.entityID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</CFQUERY>
	<cfelseif getActionDetails.entityTypeID eq "23">
		<CFQUERY NAME="getEntityDetails" dataSource="#application.siteDataSource#">
			select detail as entityName from opportunity where opportunityID =  <cf_queryparam value="#getActionDetails.entityID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</CFQUERY>
	<cfelse>
		<cfset getEntityDetails.entityName = "">
	</cfif>
<cfelse>
	<cfset getEntityDetails.entityName = "">
</CFIF>


	<CFIF getActionDetails.ownerPersonID neq request.relayCurrentUser.personid>

		<CFQUERY NAME="getUserInfo1" dataSource="#application.siteDataSource#">
		SELECT firstName+' '+lastname as FullName,Email
			 FROM person
			WHERE PersonID=#request.relayCurrentUser.personid#
		</CFQUERY>
		
		
		<CFQUERY NAME="getOwnerInfo1" dataSource="#application.siteDataSource#">
		SELECT firstName+' '+lastname as FullName,Email
			 FROM person
			WHERE PersonID =  <cf_queryparam value="#getActionDetails.ownerPersonID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</CFQUERY>
		
		<CFIF getOwnerInfo1.Email NEQ "">
			
			<!--- 2013-03-08 - RMB - 434140 - Changed 'CFMAIL' to 'CF_MAIL'  --->
			<CF_MAIL TO="#getOwnerInfo1.fullName#<#trim(getOwnerInfo1.Email)#>"
		        FROM="#getUserInfo1.fullName#<#trim(getUserInfo1.Email)#>"
		        SUBJECT="Action Request: #getActionDetails.Description#"
		        TYPE="HTML">
			<cfoutput>
    	    <style>
        	    body,p {
            	    font-family: Verdana, Helvetica, Arial, sans-serif;
                	font-size: 12px;
	                color: ##000000;
    	        }
        	    td {font-size: 12px;}
	        </style>
<cfif portalaction eq 1 >
		<TABLE>
			<TR><TD COLSPAN="2"><strong>Action request from #request.CurrentSite.Title#</strong><br></TD></TR>
			<TR><TD COLSPAN="2">#getUserInfo1.fullName# has requested a #getActionDetails.actionType# with #getOwnerInfo1.fullName#<br><br></TD></TR>
			<TR><TD VALIGN="top">Summary:</TD><TD>#getActionDetails.Description#</TD></TR>
			<TR><TD VALIGN="top">Notes:</td><TD>#getActionDetails.Notes# </TD></TR>
			<TR><TD VALIGN="top">Deadline:</td><TD>#DateFormat(getActionDetails.Deadline,"dd-mmm-yyyy")#</TD></TR>
			<TR><TD COLSPAN="2">#attributes.footer#</TD></TR>
		</TABLE>

<cfelse>
<TABLE>
	<TR><TD COLSPAN="2"><strong>Action request from #request.CurrentSite.Title#</strong><br></TD></TR>
			<TR><TD COLSPAN="2">#getUserInfo1.fullName# has requested that you <cfif isDefined("getEntityDetails.entityName") and getActionDetails.actionType eq "registration" and request.RelayCurrentUser.isInternal>approve their customer registration. Please log in to <A href="#request.currentSite.protocolAndDomain#">#request.currentsite.domainandroot#</A>, and then go to Approvals under Manage, tick the relevant approval and choose "Approve checked records" from the drop down box.</cfif></TD></TR>
	<cfif isDefined("getEntityDetails.entityName")>
  		<TR><TD COLSPAN="2">#getActionDetails.actionType# #getEntityDetails.entityName#</TD></TR>
	</cfif>
	<TR><TD VALIGN="top">Summary:</TD><TD>#getActionDetails.Description#</TD></TR>
	<TR><TD VALIGN="top">Notes:</td><TD>#getActionDetails.Notes#</TD></TR>
	<TR><TD VALIGN="top">Deadline:</td><TD>#DateFormat(getActionDetails.Deadline,"dd-mmm-yyyy")#</TD></TR>
			<cfif request.RelayCurrentUser.isInternal><!--- WAB 2006-01-25 Remove form.userType     isDefined("FORM.UserType") and FORM.userType eq "internal"--->
			<TR><TD VALIGN="top"><a href="#request.currentSite.protocolAndDomain#/remote.cfm?a=i&p=#request.relayCurrentUser.personid#&st=003&ActionID=#actionID#">Click to review</a></TD></TR>
	</cfif>
	<!--- /remote.cfm?a=i&p=#session.p#&module=/actions/ActionEdit.cfm&ActionID=#actionID# --->
</TABLE>
</cfif>
</cfoutput>
	</CF_MAIL>
	<!--- 2013-03-08 - RMB - 434140 - Changed 'CFMAIL' to 'CF_MAIL'  --->
	
		<CFELSE>
			<CFSET message = "#getOwnerInfo1.fullName# could not be informed of this change because they do not have an email address. <BR>">
		</CFIF>
	</CFIF>
</cfif>
