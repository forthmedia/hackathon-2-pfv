<!--- �Relayware. All Rights Reserved 2014 --->
<CFSET actionOrgs = application.actionUserOrgIDs>

<!--- can see all the owners --->
<CFQUERY NAME="getOwners" dataSource="#application.siteDataSource#">
	SELECT p.PersonID as ownerPersonID, p.firstName+' '+p.lastName as fullname, 
		o.OrganisationName
	FROM person p 
		inner join organisation o on p.OrganisationID = o.OrganisationID
	where o.organisationID  in ( <cf_queryparam value="#actionOrgs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		and p.active = 1
		ORDER BY p.firstName
</CFQUERY>

<!--- can see all the countries --->
<CFQUERY NAME="getCountries" dataSource="#application.siteDataSource#">
	SELECT countryDescription as country FROM country ORDER BY country
</CFQUERY>

<!--- can see all the actions --->
<CFQUERY NAME="getStatus" dataSource="#application.siteDataSource#">
	SELECT actionStatus.actionStatusID, actionStatus.Status, actionStatus.status_order
		FROM actionStatus
		ORDER BY actionStatus.status_order
</CFQUERY>

<cfquery name="getActionTypes" dataSource="#application.siteDataSource#">
	select actionTypeID, actionType from actionType order by actionType
</cfquery>

<!--- <!--- only those strands upon which the user has read permission --->
<CFQUERY NAME="getStrands" dataSource="#application.siteDataSource#">
SELECT strands.StrandID,
		strands.Strand,
   		SUM((RecordRights.Permission/1) % 2) AS allow_read,
		SUM((RecordRights.Permission/2) % 2) AS allow_edit,
 		SUM((RecordRights.Permission/4) % 2) AS allow_add
	FROM strands, RecordRights, RecordRightsGroup
	WHERE RecordRights.Entity = 'strands'
	  AND RecordRights.RecordID = strands.StrandID
	  AND RecordRights.UserGroupID = RecordRightsGroup.UserGroupID
	  AND RecordRightsGroup.PersonID = #request.relayCurrentUser.usergroupid#
 GROUP BY strands.Strand, strands.StrandID 
 HAVING SUM((RecordRights.Permission/1) % 2) > 0
 ORDER BY strands.Strand</CFQUERY>

<!--- only those deliverables upon which the user has read permission --->
<CFQUERY NAME="getDeliverables" dataSource="#application.siteDataSource#">
SELECT deliverables.DeliverableID,
		deliverables.Deliverable,
   		SUM((RecordRights.Permission/1) % 2) AS allow_read,
		SUM((RecordRights.Permission/2) % 2) AS allow_edit,
 		SUM((RecordRights.Permission/4) % 2) AS allow_add
	FROM deliverables, RecordRights, RecordRightsGroup
	WHERE RecordRights.Entity = 'deliverables'
	  AND RecordRights.RecordID = deliverables.DeliverableID
	  AND RecordRights.UserGroupID = RecordRightsGroup.UserGroupID
	  AND RecordRightsGroup.PersonID = #request.relayCurrentUser.usergroupid#
 GROUP BY deliverables.Deliverable, deliverables.DeliverableID 
 HAVING SUM((RecordRights.Permission/1) % 2) > 0
	ORDER BY deliverables.deliverable</CFQUERY>
 --->
<!--- only those deliverables upon which the user has read permission --->
<CFQUERY NAME="getLiveProjects" dataSource="#application.siteDataSource#">
	<cfif listfind(application.actionUserOrgIDs,request.relayCurrentUser.organisationID) eq 0>
		SELECT TOP 100 PERCENT p.ProjectID, p.ProjectName, 
			CONVERT(bit, SUM(rr.permission & 1)) AS allow_read, 
			CONVERT(bit, SUM(rr.permission & 2)) AS allow_edit, 
			CONVERT(bit, SUM(rr.permission & 4)) AS allow_add
		FROM recordrights rr  
		INNER JOIN Project p ON rr.recordid = p.ProjectID 
		INNER JOIN ProjectStatus ps ON p.ProjectStatusId = ps.ProjectStatusId
		WHERE (ps.IsLive = 1) 
			AND (rr.usergroupid = #request.relayCurrentUser.usergroupid#) 
			AND (rr.entity = 'project')
		GROUP BY p.ProjectID, p.ProjectName
		ORDER BY p.ProjectName
	<cfelse>
		SELECT TOP 100 PERCENT p.ProjectID, p.ProjectName 
		FROM Project p  
		INNER JOIN ProjectStatus ps ON p.ProjectStatusId = ps.ProjectStatusId
		WHERE ps.IsLive = 1
		ORDER BY p.ProjectName
	</cfif>
</CFQUERY>


<!--- only those groups to which the user belongs --->
<CFQUERY NAME="getUserGroups" dataSource="#application.siteDataSource#">
SELECT ug.Name,ug.UserGroupID
	FROM usergroup ug inner join RecordRightsGroup rrg
	on rrg.UserGroupID = ug.UserGroupID
	WHERE rrg.PersonID = #request.relayCurrentUser.usergroupid#
	ORDER BY ug.Name</CFQUERY>


<CFQUERY NAME="getLocations" dataSource="#application.siteDataSource#">
SELECT SiteName, LocationID
	FROM location 
	WHERE organisationID  in ( <cf_queryparam value="#actionOrgs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	ORDER BY SiteName</CFQUERY>

<CFQUERY NAME="getOrganisations" dataSource="#application.siteDataSource#">
SELECT OrganisationID, OrganisationName
	FROM organisation 
	WHERE organisationID  in ( <cf_queryparam value="#actionOrgs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	ORDER BY OrganisationName</CFQUERY>

<!--- <CFQUERY NAME="getDepartments" dataSource="#application.siteDataSource#">
SELECT DepartmentID, Department 
	FROM department ORDER BY Department</CFQUERY> --->
