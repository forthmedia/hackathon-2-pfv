<!--- �Relayware. All Rights Reserved 2014 --->
<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">
<cfparam name="openAsExcel" type="boolean" default="false">
<cfif openAsExcel>
	<cfcontent type="application/msexcel">
	<cfheader name="content-Disposition" value="filename=actionTimeReport.xls">
	<cfinclude template="/templates/excelHeaderInclude.cfm">
<cfelse>
		<cf_title>Action Time Report</cf_title>
</cfif>

<cfparam name="range" default="">
<cfscript>
	// create a structure containing the fields below which we want to be reported when the form is filtered
	passThruVars = StructNew();
</cfscript>

<cfif isDefined("form.thisDateRange")>
	<!--- <cfdump var="#form.thisDateRange#"> --->
	<CFSET StartDate=#CREATEODBCDATE(ListGetAt(#thisDateRange#,1))#>
	<CFSET EndDate=#CREATEODBCDATE(ListGetAt(#thisDateRange#,2))#>
	<CFSET Range=#ListGetAt(#thisDateRange#,3)#>
	<cfscript>
	// create a structure containing the fields below which we want to be reported when the form is filtered
	StructInsert(passthruVars, "thisDateRange", form.thisDateRange);
	</cfscript>
<cfelse>
	<cf_DateRange type="month" value="current" year="#year(now())#">
	<cf_CalcWeek CheckDate="#now()#" BeginDay="2" EndDay="6">
</cfif>


<CFQUERY NAME="GetFigures" DATASOURCE="#application.SiteDataSource#">
	SELECT TOP 100 PERCENT a.actionID, --p.FirstName + ' ' + p.LastName AS fullname, 
		a.createdBy, a.Description, a.created, a.CompletedDate, a.Requestedby, 
	    Project.ProjectName, --p.PersonID, 
		sum(isnull((datePart(Hour,[timetaken]) + (cast(datePart(Minute,timetaken) as decimal(5,2))/60)),0)) as totalTimeTaken
	FROM UserGroup 
		INNER JOIN Person p ON dbo.UserGroup.PersonID = p.PersonID 
		INNER JOIN actions a 
		INNER JOIN actionnote an ON a.actionID = an.actionid 
		INNER JOIN Project ON a.ProjectID = dbo.Project.ProjectID 
		INNER JOIN ProjectStatus ps ON dbo.Project.ProjectStatusId = ps.ProjectStatusId ON dbo.UserGroup.UserGroupID = an.creatorid
	WHERE (ps.IsLive = 1)
	--and an.dateStart between #Startdate# AND #EndDate#
	group by a.actionID, a.createdBy, a.Description, a.created, a.CompletedDate, a.Requestedby, 
	--p.FirstName, p.LastName, 
	project.ProjectName
</CFQUERY>

<CFSET current ="ActionTimeReport">
<cfif openAsExcel eq false>
	<cfinclude template="actionTopHead.cfm">
</cfif>

<cfoutput>
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
<!--- 	<TR class="Submenu">
		<TD ALIGN="LEFT" VALIGN="TOP" class="Submenu">Analysis of Page Visits</TD>
		<cfif openAsExcel eq false>
		<TD ALIGN="right" VALIGN="TOP" class="Submenu">
			<a href="#cgi["SCRIPT_NAME"]#?#queryString#&openAsExcel=true" class="Submenu">Open in Excel</a>
		</TD>
		</cfif>
	</TR>
 --->	<cfif openAsExcel eq true>
		<tr>
			<td>
				Date range: #dateFormat(Startdate,"dd-mmm-yy")# to #dateFormat(endDate,"dd-mmm-yy")#
			</td>
		</tr>
	</cfif>
</TABLE>
</cfoutput>

<cfif openAsExcel eq false>
	<!--- <cfform method="post" name="dateRangeForm" id="dateRangeForm">
		<cf_DateRangeSelectBox BoxName="thisDateRange" SelectedRange="#range#">
		<!--- <cfselect name="countryIDfilter" size="1" query="getElementCountryIDs" 
			value="countryID" display="countryDescription"></cfselect> --->
		<input type="submit" value="Search">
	</cfform> --->
</cfif>

<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
	
	
	<CFOUTPUT QUERY="GetFigures" GROUP="projectName">
	<CFSET Grandtotal = 0>
		<TR><th colspan="6" align="left">Project Name: #htmleditformat(projectName)#</th></TR>
		<TR bgcolor="cccccc">
		    <th>ActionID</th>  <!--- 1 --->
		   <!---  <th>Owner</th> --->  <!--- 2 --->
		    <th>Description</th>  <!--- 3 --->
		    <th>Date Created</th> <!--- 4 --->
			<!--- <th>Status</th> ---> <!--- 5 --->
			<th>Date Completed</th> <!--- 6 --->
			<th>TotalTime</th> <!--- 7 --->
		</TR>

		<CFOUTPUT>
			<TR<CFIF CurrentRow MOD 2 IS NOT 0> BGCOLOR="##FFFFCE"</CFIF>>
			<TD>#htmleditformat(ActionID)#</TD>
			<!--- <TD>#fullname#</TD> --->
			<TD>#htmleditformat(description)#</TD>
			<TD>#htmleditformat(created)#</TD>
			<!--- <TD>#Status#</TD> --->
			<TD>#htmleditformat(CompletedDate)#</TD>
			<TD>#DecimalFormat(TotalTimeTaken)#</TD>
			<CFSET Grandtotal = (#Grandtotal# + #TotalTimeTaken#)>
		</CFOUTPUT>
		
	</TR>
	<TR><th colspan="6" align="left">Total for project name: #htmleditformat(projectName)# = #DecimalFormat(GrandTotal)#</th></TR>
	
	<TR><TD colspan="6"><HR ALIGN="CENTER" SIZE="1" WIDTH="100%" COLOR="Teal"></TD></TR>
	</CFOUTPUT>
	
</TABLE>



