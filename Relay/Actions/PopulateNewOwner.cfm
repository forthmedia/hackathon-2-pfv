<!--- �Relayware. All Rights Reserved 2014 --->
<!---- if the owner was changed, the old owner's personal group
		must be removed from the RecordRights table
		and the new owner's personal group must be added --->
		
<!--- assume the following variables exist
		#qryEntity# the Entity for which we are adding permissions
		#qryRecordID# = the record ID
		#qryOwnerID#
		#qryOldOwnerID#
--->
<!--- 
<CFIF IsDefined("qryOldOwnerID")>
	<!--- delete old Owner's personal Group RecordRights for the existing entity/record
			and the new Owner's personal group RecordRights since they may be not include
			update RecordRights --->

	<CFQUERY NAME="DeleteOldOwnerRights" dataSource="#application.siteDataSource#">
		DELETE FROM RecordRights 
		 WHERE RecordID = #qryRecordID#
		   AND Entity = '#qryEntity#'
		   AND EXISTS (SELECT distinct ug.UserGroupID
		                 FROM usergroup ug inner join recordRights
						 on RecordRights.UserGroupID = ug.UserGroupID
						 WHERE (ug.PersonID = #qryOldOwnerID#
						 		OR 
								ug.PersonID = #qryOwnerID#))</CFQUERY>
</CFIF>
 --->
<!--- owner's group ALWAYS has full permissions
	  this should add one record
	  
		1) send the owner an email notifying him/her that action has
			been added
--->

<CFQUERY NAME="InsertNewOwnerRights" dataSource="#application.siteDataSource#">
INSERT INTO RecordRights (UserGroupID,Entity,RecordID,Permission)
		SELECT usergroup.UserGroupID,
				<cf_queryparam value="#qryEntity#" CFSQLTYPE="CF_SQL_VARCHAR" >,
				<cf_queryparam value="#qryRecordID#" CFSQLTYPE="CF_SQL_INTEGER" >,
				'3' AS Permission
		FROM usergroup
	   WHERE usergroup.PersonID =  <cf_queryparam value="#qryOwnerID#" CFSQLTYPE="CF_SQL_INTEGER" >  
	   AND NOT EXISTS (select distinct UserGroupID
		FROM recordRights  
	   WHERE recordID =  <cf_queryparam value="#qryRecordID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	   and entity =  <cf_queryparam value="#qryEntity#" CFSQLTYPE="CF_SQL_VARCHAR" > 
	   and permission = 3)
</CFQUERY>
		
