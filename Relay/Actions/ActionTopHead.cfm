<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 

Amendment History:  	ActionEdit.cfm

Date 		Initials 	What was changed
2008/10/10	NYB  		CR-TND561-bug5 - Save was causing top links to disappear

Enhancement still to do:

 --->

<CFPARAM NAME="current" TYPE="string" DEFAULT="ActionList.cfm">

<CF_RelayNavMenu moduleTitle="" thisDir="actions">
	<CFIF findNoCase("ActionSearch.cfm",SCRIPT_NAME) or findNoCase("ActionSearch.cfm",request.query_string)>
		<CF_RelayNavMenuItem MenuItemText="phr_Sys_MyActions" CFTemplate="/actions/ActionAutoSearch.cfm">
		<CF_RelayNavMenuItem MenuItemText="phr_Sys_Actions_MyRelatedActions" CFTemplate="/actions/ActionAutoSearch.cfm?srchUserGroupID=#request.relayCurrentUser.usergroupid#">
		<!--- <CF_RelayNavMenuItem MenuItemText="phr_Sys_Help" CFTemplate="javascript:getHelpID(169)"> --->
	</CFIF>

	<!--- NJH 2008/10/10 added the 'current eq "ActionForm.cfm' as when an action is saved, script_name and query_ string were actionTask.cfm --->
	<!--- NYB 2008/10/10 CR-TND561-bug5 - added 'or current eq "ActionEdit.cfm"': --->	
	<CFIF findNoCase("ActionEdit.cfm",SCRIPT_NAME) or findNoCase("ActionEdit.cfm",request.query_string) or current eq "ActionForm.cfm"  or current eq "ActionEdit.cfm">
		<!--- NJH 2008/05/21 P-TND066 Add Print functionality --->
		<CF_RelayNavMenuItem MenuItemText="Print" CFTemplate="javascript:void(openWin('/actions/actionView.cfm?actionID=#actionID#&mediaMode=print','Actions','height=690,width=520,resizable=yes,scrollbars=yes'));">
		<CF_RelayNavMenuItem MenuItemText="phr_Sys_Back" CFTemplate="javascript:history.go(-1)">
		<!--- NJH 2008/07/10 if it's view only, then don't show 'save and return' --->
		<cfif isDefined("viewOnly") and viewOnly eq "no">
			<CF_RelayNavMenuItem MenuItemText="Save" CFTemplate="javaScript:save(' Update & Continue ')">
		<CF_RelayNavMenuItem MenuItemText="phr_Sys_Actions_SaveAndReturn" CFTemplate="javaScript:save(' Update & Return ')">
		</cfif>
	</CFIF>

	<CFIF findNoCase("ActionList.cfm",SCRIPT_NAME) or findNoCase("ActionList.cfm",request.query_string)
		or findNoCase("ActionAutoSearch.cfm",SCRIPT_NAME) or findNoCase("ActionAutoSearch.cfm",request.query_string)
		or findNoCase("myActions.cfm",SCRIPT_NAME) or findNoCase("myActions.cfm",request.query_string)>
    	<CFIF isDefined("frmEntityType") and frmEntityType neq "" and isDefined("frmcurrentEntityID") and isNumeric(frmcurrentEntityID)>
			<CF_RelayNavMenuItem MenuItemText="phr_Sys_Actions_AddNewAction" CFTemplate="javaScript:addActionRecord('#frmEntityType#',#frmcurrentEntityID#)">
		</cfif>
		<CF_RelayNavMenuItem MenuItemText="phr_Sys_Actions_ShowMyActions" CFTemplate="/actions/ActionAutoSearch.cfm">
		<CF_RelayNavMenuItem MenuItemText="phr_Sys_Actions_MyRelatedActions" CFTemplate="/actions/ActionAutoSearch.cfm?srchUserGroupID=#request.relayCurrentUser.usergroupid#">
		<CF_RelayNavMenuItem MenuItemText="phr_Sys_Actions_SearchActions" CFTemplate="/actions/ActionSearch.cfm">
		<!--- <CF_RelayNavMenuItem MenuItemText="phr_Sys_Help" CFTemplate="javascript:getHelpID(169)"> --->
	</CFIF>

	<CFIF findNoCase("showEntityActions.cfm",SCRIPT_NAME) or findNoCase("showEntityActions.cfm",request.query_string)
			or findNoCase("entityFrameScreens.cfm",SCRIPT_NAME) or findNoCase("entityFrameScreens.cfm",request.query_string)>
    	<CFIF isDefined("frmEntityType") and frmEntityType neq "" and isDefined("frmcurrentEntityID") and isNumeric(frmcurrentEntityID)>
			<CF_RelayNavMenuItem MenuItemText="phr_Sys_Actions_AddNewAction" CFTemplate="javaScript:addActionRecord('#frmEntityType#',#frmcurrentEntityID#)">
		</cfif>
		<CF_RelayNavMenuItem MenuItemText="phr_Sys_Actions_ShowMyActions" CFTemplate="/actions/ActionAutoSearch.cfm">
	</CFIF>

	<CFIF findNoCase("ActionView.cfm",SCRIPT_NAME) or findNoCase("ActionAdd.cfm",request.query_string)>
		<CF_RelayNavMenuItem MenuItemText="phr_Sys_Back" CFTemplate="javascript:history.go(-1)">
		<!--- <CF_RelayNavMenuItem MenuItemText="phr_Sys_Help" CFTemplate="javascript:getHelpID(261)"> --->
	</CFIF>
	
	<CFIF findNoCase("ActionAdd.cfm",SCRIPT_NAME) or findNoCase("ActionAdd.cfm",request.query_string)>
		<CF_RelayNavMenuItem MenuItemText="phr_Sys_MyActions" CFTemplate="/actions/ActionAutoSearch.cfm">
	</CFIF>

	<CFIF findNoCase("reportActionTime.cfm",SCRIPT_NAME) or findNoCase("reportActionTime.cfm",request.query_string)>
		<CF_RelayNavMenuItem MenuItemText="phr_Sys_Actions_DailyReport" CFTemplate="/actions/reportDailyActions.cfm">
		<CF_RelayNavMenuItem MenuItemText="phr_Sys_Actions_ListByDate" CFTemplate="/actions/reportTimeDateDialog.cfm">
		<CF_RelayNavMenuItem MenuItemText="phr_Sys_Actions_TimeReport" CFTemplate="/actions/reportActionTime.cfm">
		<CF_RelayNavMenuItem MenuItemText="phr_Sys_Actions_TimeAnalysis" CFTemplate="/actions/reportTimeAnalysis.cfm">
		<CF_RelayNavMenuItem MenuItemText="phr_Sys_Actions_OpenInExcel" CFTemplate="/actions/reportActionTime.cfm?#queryString#&openAsExcel=true">
	</CFIF>
	
</CF_RelayNavMenu>