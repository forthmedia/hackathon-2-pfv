<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Amendment History
		
		actionsavenote.cfm


SWJ added type on 20-Apr-99 
	SWJ added the status section
2008/06/30     SSS      Added Nvarchar Support
2008/10/27		NJH		Bug Fix All Sites Issue 1206 - cfqueryParam the noteText and allow it through the sqlInjection checker
--->

<CFSET SaveTime = now()>
<cfparam name="actionNoteType" default="Note">
<cfif actionNoteType eq "">
	<cfset actionNoteType = "Note">
</cfif>

<CFQUERY NAME="InsertNote" DATASOURCE="#application.SiteDataSource#">
 	Insert into actionnote (actionid,creatorid,created,notetext,
	datestart,dateend,timetaken,cost,newowner,newstatus, type)
	Values(
		<cf_queryparam value="#qryrecordid#" CFSQLTYPE="CF_SQL_INTEGER" >,
		<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >,
		<cf_queryparam value="#SaveTime#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
		<cfqueryparam value="#form.noteText#" cfsqltype="cf_sql_varchar">, <!--- NJH 2008/10/27 was N'#form.notetext#', --->
		<CFIF NoteStartedDate IS NOT ''><cf_queryparam value="#NoteStartedDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" ><CFELSE>null</CFIF>,
		<CFIF NoteEndedDate IS NOT ''><cf_queryparam value="#NoteEndedDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" ><CFELSE>null</CFIF>,
		<CFIF NoteTimeSpent IS NOT ''><cf_queryparam value="#NoteTimeSpent#" CFSQLTYPE="CF_SQL_TIMESTAMP" ><CFELSE>null</CFIF>,
		0,
		<cf_queryparam value="#qryownerid#" CFSQLTYPE="CF_SQL_INTEGER" >,
		<cf_queryparam value="#new_status#" CFSQLTYPE="CF_SQL_INTEGER" >, 
		<cf_queryparam value="#actionNoteType#" CFSQLTYPE="CF_SQL_VARCHAR" >)
</CFQUERY>



<CFIF actionNoteType EQ 'Status'>
	<CFQUERY NAME="GetLastNote" dataSource="#application.siteDataSource#">
		select actionNoteID from actionNote 
		where created =  <cf_queryparam value="#SaveTime#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
			and actionid =  <cf_queryparam value="#qryRecordID#" CFSQLTYPE="CF_SQL_INTEGER" > 
			and creatorID = <cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</CFQUERY>
	<CFQUERY NAME="UpdateAction" dataSource="#application.siteDataSource#">
		Update actions set LatestNoteID =  <cf_queryparam value="#GetLastNote.actionNoteID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		where actionID =  <cf_queryparam value="#qryRecordID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</CFQUERY>
</CFIF>




