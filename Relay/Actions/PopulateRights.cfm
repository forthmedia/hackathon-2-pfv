<!--- �Relayware. All Rights Reserved 2014 --->
<!--- save permissions --->
<!--- assume the following variables exist
XX		#new_status# - the status from the form or, if applicable, the assigned status
XX		#strand_only# - the strand from the parsed deliverable form field
XX		#deliverable_only# - the deliverable (parsed) from the form
		#permission# = value is LetterNumber (see below)
		#qryPersonID#  - the ID of the owner, will usually be Cookie.USER BUT NOT ALWAYS
		#qryEntity# the Entity for which we are adding permissions
		#qryRecordID# = the record ID

--->


<!--- assume permission value is:
		LetterNumber

	  where letter is:
	  
	   P - Personal only 
	       Only the user's group has read and write access
	   G - My groups only
	       All the groups to which the user belongs
		   have access
	   A - All groups have access
	   
	   and number is the permission code
	   1 - read only
	   3 - read(1)  and write(2)
	   
	   We do not need to add records for usergroups with 
	   no permission to access (permission 0)
	   
	   If the owner is not the user, then the applicable groups
	   are the owner's groups
	   
	   But we need to check for sum(permission\2 MOD 2) GT 0
	   		or
			
  	   sum(permission\2 MOD 2) IS NOT 0 AND sum(permission\2 MOD 2) IS NOT ""
--->

<!--- delete all the RecordRights for the existing entity/record --->

<CFQUERY NAME="DeleteRights" DATASOURCE="#application.SiteDataSource#">
	DELETE FROM RecordRights
	 WHERE RecordID =  <cf_queryparam value="#qryRecordID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	   AND Entity =  <cf_queryparam value="#qryEntity#" CFSQLTYPE="CF_SQL_VARCHAR" > 
</CFQUERY>

<!--- owner's group ALWAYS has full permissions
	  this should add one record --->

<CFQUERY NAME="InsertUserRights" DATASOURCE="#application.SiteDataSource#" DBTYPE="ODBC">
INSERT INTO RecordRights (UserGroupID,Entity,RecordID,Permission)
		SELECT userGroup.UserGroupID,
				'#qryEntity#',
				#qryRecordID#,
				'3' AS Permission
		FROM userGroup
	   WHERE userGroup.PersonID = #qryOwnerID#</CFQUERY>

<CFIF Left(permission,1) IS "G">
	<!--- owner's groups only have RecordRights, other groups have none --->
	<CFQUERY NAME="InsertActionRightsG" DATASOURCE="#application.SiteDataSource#" DBTYPE="ODBC">
		INSERT INTO RecordRights (UserGroupID,Entity,RecordID,Permission)
			SELECT DISTINCT rg.UserGroupID,
			'#qryEntity#',#qryRecordID#,'#Mid(permission,2,5)#' AS Permission
		FROM RightsGroup rg inner join userGroup ug on rg.UserGroupID = ug.UserGroupID
		WHERE rg.PersonID = #qryOwnerID#</CFQUERY>
</CFIF>		
<!--- 	***************************************************************************	

This whole section needs think ing through as the new rights structure works differently


<CFIF #Left(permission,1)# IS "A">
	<!--- all other groups have RecordRights --->
	<CFQUERY NAME="InsertActionRightsA" DATASOURCE="#application.SiteDataSource#" DBTYPE="ODBC">
		INSERT INTO RecordRights (UserGroupID,Entity,RecordID,Permission)
			SELECT DISTINCT RightsGroup.UserGroupID,
					'#qryEntity#',
					#qryRecordID#,
					'#Mid(permission,2,5)#' AS Permission
			 FROM RightsGroup, userGroup
			 WHERE RightsGroup.UserGroupID <> userGroup.UserGroupID
			   AND userGroup.PersonID = #qryOwnerID#</CFQUERY>
</CFIF>		


<CFIF IsDefined("qryAdminOnly")>
	<!--- any groups defined as Administrator groups should have all RecordRights --->
	
	<!--- first update the administator groups that already exist --->
	<CFQUERY NAME="UpdateAdminRights" DATASOURCE="#application.SiteDataSource#" DBTYPE="ODBC">
		UPDATE RecordRights AS a SET a.Permission = #qryAdminOnly#
	 		 WHERE a.RecordID = #qryRecordID#
	 		   AND a.Entity = '#qryEntity#'
			   AND EXISTS (SELECT userGroup.UserGroupID
		                 FROM userGroup
						WHERE a.UserGroupID = userGroup.UserGroupID
						  AND userGroup.Administrator = true)</CFQUERY>

	
	<!--- then add the administrator groups that don't exist
	      chances are no records will be added since permissions was probably "all" --->	

	<CFQUERY NAME="InsertAdminRights" DATASOURCE="#application.SiteDataSource#" DBTYPE="ODBC">
		INSERT INTO RecordRights (UserGroupID,Entity,RecordID,Permission)
			SELECT DISTINCT userGroup.UserGroupID,
					'#qryEntity#',
					#qryRecordID#,
					#qryAdminOnly# AS Permission
			 FROM userGroup
			 WHERE userGroup.Administrator = true
			   AND NOT EXISTS (SELECT a.UserGroupID FROM
			   	RecordRights AS a
				WHERE a.RecordID = #qryRecordID#
 			      AND a.Entity = '#qryEntity#'
			   	  AND a.UserGroupID = userGroup.UserGroupID)</CFQUERY>
	
	
</CFIF>		
 --->	