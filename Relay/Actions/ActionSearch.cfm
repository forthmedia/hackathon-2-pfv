<!--- �Relayware. All Rights Reserved 2014 --->
<!---
2014-09-16	RPW		CORE-146 The Actions page has a search. Whatever criteria you use it returns a blank page - which looks very broken...
2015-09-16	ACPK	FIFTEEN-365 Replaced text input field for srchActionID with numeric RelayFormElement
2015-11-27  SB 		Bootstrap
--->


<cfset application.com.request.setDocumentH1(text="Search Actions")>
<CFINCLUDE TEMPLATE="PopulateLists.cfm">

<CFQUERY NAME="getSharers" dataSource="#application.siteDataSource#">
	SELECT DISTINCT u.usergroupid as UserGroupID,
		p.firstName+' '+p.lastName as fullname, o.OrganisationName
	FROM person p
	INNER JOIN organisation o ON p.OrganisationID = o.OrganisationID
	INNER JOIN usergroup u ON u.personid = p.personid
	INNER JOIN recordRights rr ON rr.userGroupID = u.userGroupID
	WHERE p.active = 1
AND rr.entity = 'actions'
		ORDER BY p.firstName+' '+p.lastName</CFQUERY>

<!--- <CFSET qryEntity = "actions"> --->

<!--- template has one query "getAddPermission" ---->
<!--- <CFINCLUDE TEMPLATE="../SharedCode/Perm_add.cfm"> --->

<!--- <cf_includeJavascriptOnce template="/javascript/Calendar.js"> --->

<cf_title>Action List</cf_title>

<CFINCLUDE TEMPLATE="actionTopHead.cfm">
<p>Phr_Sys_Pleaseselectthesearchcriteria</p>

	<!--- 2014-09-16	RPW	CORE-146 The Actions page has a search. Whatever criteria you use it returns a blank page - which looks very broken... --->
	<cfoutput><form action="/actions/ActionList.cfm" method="post" name="actionSearchForm"></cfoutput>

	<Cf_relayFormElementDisplay type="select" query="#getStatus#" display="Status" value="actionStatusID" label="Phr_Sys_Status" fieldname="srchStatusID" currentvalue="" showNull="true">
	<!--- <div class="form-group">
		<label for="srchStatusID">Phr_Sys_Status</label>
		<SELECT NAME="srchStatusID" id="srchStatusID" class="form-control">
			<OPTION VALUE="0">
			<CFOUTPUT QUERY="getStatus">
			<OPTION VALUE="#actionStatusID#">#HTMLEditFormat (Status)#
			</CFOUTPUT>
		 </SELECT>
	</div> --->

	<div class="form-group">
		<label>Phr_Sys_Owner</label>
		<cf_displayValidValues
			validFieldName = "organisation.InternalUsers"
			formFieldName =  "srchPersonID"
			keepOrig = "false"
			currentValue = "0"
		>
	</div>

	<Cf_relayFormElementDisplay type="select" query="#getSharers#" display="fullname" value="UserGroupID" label="Phr_Sys_Sharedby" fieldname="srchUserGroupID" currentvalue="" showNull="true">
	<!--- Added in by MDC to search for shared actions - Needs work - 2003-12-05	--->
	<!--- <div class="form-group">
		<label for="srchUserGroupID">Phr_Sys_Sharedby</label>

		<SELECT NAME="srchUserGroupID" id="srchUserGroupID" class="form-control">
		 	<OPTION VALUE=0>
			 <CFOUTPUT QUERY="getSharers">
			 	<OPTION VALUE="#UserGroupID#">#HTMLEditFormat(fullname)#
			 </CFOUTPUT>
	     </SELECT>
	</div> --->

	<cfif getUserGroups.recordCount gt 0>
<!--- 	<div class="form-group">
		<label for="srchUserGroupID">Phr_Sys_ActionsIhaverightstoEdit</label>
		<SELECT NAME="srchUserGroupID" id="srchUserGroupID" class="form-control">
		 	<OPTION VALUE=0>
			 <CFOUTPUT QUERY="getUserGroups">
			 	<OPTION VALUE="#UserGroupID#">#HTMLEditFormat(Name)#
			 </CFOUTPUT>
	     </SELECT>
	 </div> --->
		 <Cf_relayFormElementDisplay type="select" query="#getUserGroups#" display="Name" value="UserGroupID" label="Phr_Sys_ActionsIhaverightstoEdit" fieldname="srchUserGroupID" currentvalue="" showNull="true">
	 </cfif>
	 <cfif getLiveProjects.recordCount gt 0>
		<!--- <div class="form-group">
			<label for="srchProjectID">Project:</label>
				<SELECT NAME="srchProjectID" id="srchProjectID" class="form-control">
				     <OPTION VALUE="0">
					 <CFOUTPUT QUERY="getLiveProjects">
				       <OPTION VALUE="#ProjectID#">#HTMLEditFormat(ProjectName)#
					 </CFOUTPUT>
		     	</SELECT>
		 </div> --->
		  <Cf_relayFormElementDisplay type="select" query="#getLiveProjects#" display="ProjectName" value="ProjectID" label="Project" fieldname="srchProjectID" currentvalue="" showNull="true">
	 </cfif>
	<!--- <div class="form-group">
		<label for="srchActionTypeID">Phr_Sys_Type:</label>
		<SELECT NAME="srchActionTypeID" id="srchActionTypeID" class="form-control">
			<option value="0">Phr_Sys_Choosetype</option>
			<cfoutput query="getActionTypes">
			<option value="#actionTypeID#">#htmleditformat(actionType)#</option>
			</cfoutput>
		</SELECT>
	</div> --->
	 <Cf_relayFormElementDisplay type="select" query="#getActionTypes#" display="actionType" value="actionTypeID" label="Phr_Sys_Type" fieldname="srchActionTypeID" currentvalue="" nullText="Phr_Sys_Choosetype">

	<!--- <div class="form-group">
		<label for="srchActionID">Phr_Sys_ActionID</label> --->
		<!--- 2015-09-16	ACPK	 FIFTEEN-365 Replaced text input field for srchActionID with numeric RelayFormElement --->
		<cf_relayFormElementDisplay label="Phr_Sys_ActionID" Type="numeric" currentValue="" fieldname="srchActionID" id="srchActionID" required="no" size="4" maxlength="4">
	<!--- </div> --->

	<!--- <div class="form-group">
		<label for="srchword">PPhr_Sys_WordSearch*</label>
		<INPUT TYPE="text" NAME="srchword" id="srchword" VALUE="" SIZE=20 class="form-control">
	</div> --->
	<cf_relayFormElementDisplay label="PPhr_Sys_WordSearch*" Type="text" currentValue="" fieldname="srchword">

	<Cf_relayFormElementdisplay type="date" fieldname="srchDate" currentValue="" label="Phr_Sys_SelectaDate">

	<!--- <div class="form-group">
		<label for="srchDate">Phr_Sys_SelectaDate:</label>
		<CFOUTPUT><input type="text" name="srchDate" id="srchDate" size="11" readonly>
			<A HREF="javascript:show_calendar('actionSearchForm.srchDate',null,'','DD-Mon-YYYY',null,'AllowWeekends=No;CurrentDate=Today')" TITLE="Click to choose date" onMouseOver="window.status='Pop Calendar';return true;" onMouseOut="window.status='';return true;">
				<div class="fa fa-calendar"></div>
			</A>
		</cfoutput>
	</div> --->

	<!--- 			<CFSET loopdate=Now()>
				<SELECT NAME="srchDate">
				<OPTION VALUE="">Select a date
				<CFLOOP INDEX="LoopCount" FROM="1" TO="15" STEP="1">
					<CFOUTPUT><OPTION VALUE="#loopdate#">#DateFormat("#loopdate#")#</CFOUTPUT>
				<CFSET loopdate=#DateAdd('d', -1, "#loopdate#")#>
				</CFLOOP>
				</SELECT>
	<TR><TD ALIGN="right">or Enter a Date (dd/mm/yy): </TD><TD ALIGN="left">
	 	<INPUT TYPE="text" NAME="srchDate" VALUE="" SIZE=8>
	 --->

	<div class="form-group">
		<p><b>Phr_Sys_Specifywhichdatetouse:</b></p>
		<label class="radio-inline">
			<INPUT TYPE="Radio" NAME="SrchDateType" VALUE="Started" CHECKED> Phr_Sys_ActionsAddedsince
		</label>
		<label class="radio-inline">
			<INPUT TYPE="Radio" NAME="SrchDateType" VALUE="Completed"> Phr_Sys_ActionsCompletedsince
		</label>
		<!--- <INPUT TYPE="Checkbox" NAME="srchWeekEnding"> Week ending --->
	</div>

	<div class="form-group">
		<label class="checkbox">
			Phr_Sys_OnlyShowLiveActions:
			<INPUT TYPE="Checkbox" NAME="srchActionLive" CHECKED>
		</label>
	</div>
	<p>* Phr_Sys_Searchesdescription</p>


	<INPUT TYPE="submit" VALUE="Search" class="btn btn-primary">
	<INPUT TYPE="Reset" VALUE="Clear Contents" class="btn btn-primary">

	</FORM>
