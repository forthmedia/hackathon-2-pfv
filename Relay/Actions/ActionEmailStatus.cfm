<!--- �Relayware. All Rights Reserved 2014 --->
<!--- sends an email when the status has changed--->

<!---  Assume the following variables are defined
		#new_status# - the status from the form or, if applicable, the assigned status
		#new_status_text# - the description of the new status
		#old_status_text# - the description of the old status
		#strand_only# - the strand from the parsed deliverable form field
		#deliverable_only# - the deliverable (parsed) from the form
		#qryOwnerID# - the owner (not necessarily the Cookie.USERID)
		#qryCreatorID# - the creator (not necessarily the Cookie.USERID)
--->

<!--- 
2004-06-03  WAB: Added a message if the creator does not have an email address - probably unnecessary 'cause turned out that email was blank because query was wrong!
2009/05/21 SSS LH 2261 Get creator query is passed a usergroup so I have changed the query 
2009/07/27	NJH	LID 2383 - added actionID= to the link URL.
 --->


<CFIF NOT IsDefined("new_status_text")>

	<CFQUERY NAME="getNewStatusInfo" dataSource="#application.siteDataSource#">
		SELECT actionStatusID,Status
			FROM actionStatus
		WHERE actionStatusID =  <cf_queryparam value="#new_status#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</CFQUERY>
	<CFSET new_status_text = getNewStatusInfo.Status>
</CFIF>

<!--- SSS Start LH 2261 changed query for usergroup passed --->
<CFQUERY NAME="getCreator" dataSource="#application.siteDataSource#">
SELECT firstName+' '+lastname as FullName, Email
	 FROM person p inner join
	UserGroup ug on p.personID = ug.personID
	WHERE ug.usergroupID =  <cf_queryparam value="#qryCreatorID#" CFSQLTYPE="CF_SQL_INTEGER" > 
</CFQUERY>
<!--- SSS END LH 2261 chnaged query for usergroup passed --->

<CFQUERY NAME="getOwner" dataSource="#application.siteDataSource#">
SELECT firstName+' '+lastname as FullName, Email
	 FROM person
	WHERE PersonID=<cf_queryparam value="#qryOwnerID#" CFSQLTYPE="CF_SQL_INTEGER" > 
</CFQUERY>

<!--- NJH 2009/07/27 LID 2383 added actionID= to the link url --->
<CFIF getCreator.Email NEQ "">
	<!--- 2013-03-08 - RMB - 434140 - Changed 'CFMAIL' to 'CF_MAIL'  --->
	<CF_MAIL TO="#getCreator.fullname#<#trim(getCreator.Email)#>"
	        FROM="#getOwner.fullname#<#trim(getOwner.email)#>"
	        SUBJECT="Action status change"
	        TYPE="HTML">
	<cfoutput>
	<STYLE> 
	P {
	 	font-family : 'Verdana', 'Arial', 'Sans-Serif';
	 	font-size : 10pt;
	 	font-weight : normal;
	 	color : Navy;
	 }
	</STYLE>
	<P>The status of the following action has changed: </P>
	<P><B>Action:</B> #Form.Description#<BR>
	<B>Action ID:</B> #Form.ActionID#<BR>
	<B>Link:</B> <A HREF="#request.currentSite.protocolAndDomain#/actions/ActionEdit.cfm?actionID=#Form.ActionID#">#Form.Description#</A><BR>
	<B>Old Status</B>: #old_status_text#<BR> 
	<B>New Status: </B>#new_status_text#</P>
	<HR WIDTH="80" SIZE="1">
	<P><B>Owner:</B> #getOwner.FullName# <BR>
	<B>Deadline:</B> #DateFormat(Form.Deadline,"dd-mmm-yyyy")# <BR>
	<B>Priority:</B> #DateFormat(Form.Deadline,"dd-mmm-yyyy")# <BR>
	<HR WIDTH="80" SIZE="1">
	<P>Notes: #Form.Notes# </P>
	</cfoutput>
	</CF_MAIL>
	<!--- 2013-03-08 - RMB - 434140 - Changed 'CFMAIL' to 'CF_MAIL'  --->
	  

<CFELSE>

	<CFSET message = message & "#getCreator.fullname# could not be informed of the status change because they do not have an email address. <BR>">

</CFIF>	
	

