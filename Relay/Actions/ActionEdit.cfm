<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 

Amendment History:  	ActionEdit.cfm

Date 		Initials 	What was changed
2008-10-10	NYB  		CR-TND561-bug2 - Save & Return was saving but not returning
2011/01/13	MS		LID:4314 retrieving and campaignId if recorded against the action

Enhancement still to do:

 --->

<!--- flag set in actionview to display in read only format --->
<CFPARAM NAME="viewonly" DEFAULT="no">
<cfparam name="portalaction" default="0">


<cfinclude template="/actions/PopulateLists.cfm">

<CFQUERY NAME="getActionDetails" DATASOURCE="#application.SiteDataSource#">
    SELECT a.*
      FROM actions a
     WHERE a.actionid =  <cf_queryparam value="#actionid#" CFSQLTYPE="CF_SQL_INTEGER" > 
</CFQUERY>

<CFQUERY NAME="GetNotes" DATASOURCE="#application.SiteDataSource#">
	SELECT 	n.*,
		s.status as newStatusText,
		p.firstName+' '+p.lastName as newOwnerName,
		ug.name as initials
	FROM actionnote n 
		INNER JOIN userGroup ug ON ug.userGroupID=n.creatorid 
		INNER JOIN person p ON p.personid=n.newowner 
		INNER JOIN actionStatus s ON s.actionStatusid = n.newstatus
	WHERE actionid =  <cf_queryparam value="#actionid#" CFSQLTYPE="CF_SQL_INTEGER" > 
	ORDER BY n.created ASC

</CFQUERY>

<!--- NJH 2008/05/20 P-TND066 1.1.3 this query is moved from ActionForm.cfm as it was also needed in view only mode --->
<cfif isDefined("getActionDetails.entityTypeID") and getActionDetails.entityTypeID neq "" and getActionDetails.entityID neq "">
	<cfif getActionDetails.entityTypeID eq "0">
		<CFQUERY NAME="getEntityDetails" dataSource="#application.siteDataSource#">
			select 
			p.firstName+' '+p.lastName+' - '+ o.organisationName+' ' +
			case when isNUll(l.telephone,'') <> '' then '<br>Switch: '+ l.telephone else '' end +  
			case when isNUll(p.officephone,'') <> '' then '<br>DD: ' + p.officephone else '' end +
			case when isNUll(p.mobilephone,'') <> '' then '<br>Mobile: '+p.mobilephone else '' end
			as entityName, 
			p.personid as Data, 'frmsrchPersonID=' as Datatype
			from person p
			INNER JOIN organisation o ON o.organisationID = p.organisationID
			join location l on l.locationid = p.locationid
			where p.personID =  <cf_queryparam value="#getActionDetails.entityID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</CFQUERY>
	<cfelseif getActionDetails.entityTypeID eq "2">
		<CFQUERY NAME="getEntityDetails" dataSource="#application.siteDataSource#">
			select 'frmsrchOrgID=' as DataType, organisationid as Data, organisationName as entityName from organisation where organisationID =  <cf_queryparam value="#getActionDetails.entityID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</CFQUERY>
	<cfelseif getActionDetails.entityTypeID eq "23">
		<CFQUERY NAME="getEntityDetails" dataSource="#application.siteDataSource#">
			select entityid as Data, 'frmsrchOrgID=' as DataType, detail as entityName from opportunity where opportunityID =  <cf_queryparam value="#getActionDetails.entityID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</CFQUERY>
	<cfelse>
		<cfset getEntityDetails.entityName = "">
	</cfif>
<cfelse>
	<cfset getEntityDetails.entityName = "">
</CFIF>

<CFSET qryRecordID = getActionDetails.ActionID>
<CFSET qryEntity = "actions">
<CFSET qryDescription = getActionDetails.Description>
<CFSET qryDeadline = getActionDetails.Deadline>
<CFSET qryOwnerID = getActionDetails.ownerPersonID>
<CFSET qryCreatorID = getActionDetails.Createdby>
<CFSET qryStatusID = getActionDetails.actionStatusID>
<CFSET qryPriority = getActionDetails.Priority>
<CFSET qryActionTypeID = getActionDetails.actionTypeID>
<CFSET qryDeliverableID = getActionDetails.DeliverableID>
<CFSET qryProjectID = getActionDetails.ProjectID>
<CFSET qryNotes = getActionDetails.Notes>
<CFSET qryTimeSpent = getActionDetails.Timespent>
<CFSET qryDateEntered = getActionDetails.created>
<CFSET qryCompletedDate = getActionDetails.CompletedDate>
<CFSET qryRequestedBy = getActionDetails.RequestedBy>
<CFSET qryEntityName = getEntityDetails.entityName><!--- NJH 2008/05/20 P-TND066 1.1.3 --->
<CFSET qryCampaignID = getActionDetails.actionCampaignID><!--- MS  2011/01/13 LID4314 --->

<!---CFSET #qryPermission# = ""--->
		


<CFIF viewonly IS "yes">
	<CFSET page_header = "Action Record">
	<CFSET function = "view">
	<cfinclude template="/actions/ActionViewForm.cfm">
	<CF_ABORT>	
<CFELSE>
	<CFSET page_header = "Edit Action Record">
	<CFSET function = "edit">
	<!--- NYB 2008-10-10 CR-TND561-bug2 - added: --->
	<CFSET current = "ActionEdit.cfm">
	<!--- :NYB --->
	<cfinclude template="/actions/ActionForm.cfm">
	<CF_ABORT>
</CFIF>

