<!--- �Relayware. All Rights Reserved 2014 --->


<CFSET todayday=datepart("d", now())>
<CFSET todaymonth=datepart("m", now())>
<CFSET todayyear=datepart("yyyy", now())>




<CFPARAM name="frmtodate" default="#createdatetime(todayyear,todaymonth,todayday,23,59,59)#">
<CFPARAM name="frmfromdate" default="#dateadd('s',1,dateadd('d',-30,frmtodate))#">
<CFPARAM name="frmpersonid" default="#request.relayCurrentUser.personid#">



<CFQUERY NAME="GetFigures" DATASOURCE="#application.SiteDataSource#">
SELECT 	a.Description,
		a.actionid,
		d.Deliverable,
		d.Deliverableid,
		n.datestart,
		n.dateend,
		n.created,
		n.timetaken,
		p.fullname<!---,
		 IIf(DatePart('h',dateend)>18 Or DatePart('h',dateend)<9 Or DatePart('h',dateend-timetaken)<9 or DatePart('w',dateend-timetaken)=1 or DatePart('w',dateend-timetaken)=7,1,0 ) AS outofhours,
		IIf(n.DateEnd is not null,n.dateend, IIf(n.DateStart is not null,n.datestart, IIf(n.Created is not null,n.Created,''))) as actiondate 
		, <!--- datepart('y',actiondate - 0.125) as day    anything up to 3am counts as previous day! ---> --->
FROM person as p, actionnote as n, actions as a, deliverables as d
WHERE p.PersonID = n.creatorid
AND n.actionid = a.ActionID
AND a.DeliverableID = d.DeliverableID
AND n.creatorid =  <cf_queryparam value="#frmpersonid#" CFSQLTYPE="CF_SQL_INTEGER" > 
AND ((n.dateend >=  <cf_queryparam value="#frmfromdate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >  AND n.dateend <  <cf_queryparam value="#frmtodate#" CFSQLTYPE="CF_SQL_TIMESTAMP" > )
	or (n.datestart >=  <cf_queryparam value="#frmfromdate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >  AND n.datestart <  <cf_queryparam value="#frmtodate#" CFSQLTYPE="CF_SQL_TIMESTAMP" > )
	or (n.created >=  <cf_queryparam value="#frmfromdate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >  AND n.created <  <cf_queryparam value="#frmtodate#" CFSQLTYPE="CF_SQL_TIMESTAMP" > ))	
ORDER BY  		IIf(n.DateEnd is not null,n.dateend, IIf(n.DateStart is not null,n.datestart, IIf(n.Created is not null,n.Created,''))) 
</CFQUERY>

<CFQUERY NAME="getDeliverables" DATASOURCE="#application.SiteDataSource#">
SELECT deliverable, deliverableid
from deliverables
order by deliverableid
</CFQUERY>

<CFQUERY NAME="getPeople" DATASOURCE="#application.SiteDataSource#">
SELECT personid, fullname
from person
order by fullname
</CFQUERY>


<CFSET deliverablearray=arraynew(1)>
<CFLOOP query="getdeliverables">
	<CFSET deliverablearray[deliverableid] = deliverable>
	<CFSET maxdeliverableid=deliverableid>
</CFLOOP>


<cf_title>Time Report</cf_title>


<CFSET current ="ActionTimeReport">
<CFINCLUDE TEMPLATE="actionTopHead.cfm">

<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
<FORM name="thisForm" action="dailyreport.cfm" method="post">

<TR><TD ALIGN="left">Time Report for : </TD>
		<TD ALIGN="left">	
			<CFSET loopdate=Now()>
			<SELECT NAME="frmPersonid">
			<CFLOOP query="getpeople">
				<CFOUTPUT><OPTION VALUE="#personid#" #iif(personid is frmpersonid,DE(" Selected"), DE(""))#>#htmleditformat(fullname)#</CFOUTPUT>
			</CFLOOP>
			</SELECT>
</TD></TR>

<TR><TD ALIGN="left">From: </TD>
		<TD ALIGN="left">	
			<CFSET loopdate=Now()>
			<SELECT NAME="frmFromDate">
			<OPTION VALUE="">Select a date
			<CFLOOP INDEX="LoopCount" FROM="1" TO="60" STEP="1">
				<CFOUTPUT><OPTION VALUE="#loopdate#">#DateFormat("#htmleditformat(loopdate)#")#</CFOUTPUT>
			<CFSET loopdate=#DateAdd('d', -1, "#loopdate#")#>
			</CFLOOP>
			</SELECT>
	<TD ALIGN="left">or Enter a Date (dd/mm/yy): </TD><TD ALIGN="left">		
 	<INPUT TYPE="text" NAME="frmFromDate" VALUE="" SIZE=8>
 	<INPUT TYPE="hidden" NAME="frmFromDate_eurodate" VALUE="" SIZE=8>
</TD></TR>

<TR><TD ALIGN="left">To: </TD>
		<TD ALIGN="left">	
			<CFSET loopdate=Now()>
			<SELECT NAME="FrmToDate">
			<OPTION VALUE="">Select a date
			<CFLOOP INDEX="LoopCount" FROM="1" TO="30" STEP="1">
				<CFOUTPUT><OPTION VALUE="#loopdate#">#DateFormat("#htmleditformat(loopdate)#")#</CFOUTPUT>
			<CFSET loopdate=#DateAdd('d', -1, "#loopdate#")#>
			</CFLOOP>
			</SELECT>
	<TD ALIGN="left">or Enter a Date (dd/mm/yy): </TD><TD ALIGN="left">		
 	<INPUT TYPE="text" NAME="FrmToDate" VALUE="" SIZE=8>
 	<INPUT TYPE="hidden" NAME="FrmToDate_eurodate" VALUE="" SIZE=8>
</TD></TR>
<TR><TD ALIGN="left"><INPUT TYPE="submit" VALUE="Search"> </TD>
</TR>

</TABLE>
</FORM>



	<TABLE border=0 cellpadding=3 cellspacing=0>
	
		<TR bgcolor="cccccc">
		    <TD>ActionID</TD>  
		    <TD>Deliverable</TD>
		    <TD>Description</TD>
		    <TD>Start Time</TD>
			<TD>End Time</TD> 
			<TD>TotalTime</TD>
		</TR>

	<CFSET totaltimetoday=createtimespan(0,0,0,0)>
	<CFSET deliverabletimetoday=arraynew(1)>
	<CFSET x=arrayset(deliverabletimetoday, 1 , maxdeliverableid, createtimespan(0,0,0,0))>

	<CFSET currentday=0>
		
	<CFLOOP query="getfigures" >

	<CFIF currentday is not day>
	<!--- new day so output previous totals and new header--->
		<TR><TD colspan=6 align="Right"><B><CFOUTPUT>#iif(totaltimetoday is not 0,DE(timeformat(totaltimetoday,'HH:mm')),DE(""))#</CFOUTPUT></B></TD></TR>
		<CFLOOP index=I from="1" to ="#maxdeliverableid#"><CFIF deliverabletimetoday[I] is not 0><CFOUTPUT><TR><TD colspan=5 align="Right"> #deliverablearray[I]#: </TD><TD align=right>#timeformat(deliverabletimetoday[I],"HH:mm")#</TD> </CFOUTPUT></CFIF> </CFLOOP>

		<CFSET totaltimetoday=createtimespan(0,0,0,0)>
		<CFSET x=arrayset(deliverabletimetoday, 1 , maxdeliverableid, createtimespan(0,0,0,0))>

		<TR><TD colspan="6"><B><CFOUTPUT>#DateFormat(actiondate,"dddd, dd mmm yy")#</CFOUTPUT></B></TD></TR>

	</CFIF>

			
		<CFOUTPUT>
		<TR #iif(outofhours is 1,DE(' bgcolor="dddddd"'), DE(""))# >
		<TD><A HREF="ActionEdit.cfm?ActionID=#ActionID#">#htmleditformat(Actionid)#</A></TD>
		<TD>#htmleditformat(Deliverable)#</TD>
		<TD>#htmleditformat(Description)#</TD>
		<TD align="Right">#timeformat(datestart,"HH:mm")#</TD>
		<TD align="Right">#timeformat(dateend,"HH:mm")#</TD>
		<TD align="Right">#timeformat(timetaken,"HH:mm")#</TD>
		</TR>		
		<CFIF timetaken is not "">
			<CFSET totaltimetoday=totaltimetoday+timetaken>
			<CFSET deliverabletimetoday[deliverableid]= deliverabletimetoday[deliverableid]+timetaken>
		</CFIF>
		</CFOUTPUT>

		<CFSET currentday=day>
	</CFLOOP>

		
	<!--- final day--->
		<TR><TD colspan=6 align="Right"><B><CFOUTPUT>#iif(totaltimetoday is not 0,DE(timeformat(totaltimetoday,'HH:mm')),DE(""))#</CFOUTPUT></B></TD></TR>
		<CFLOOP index=I from="1" to ="#maxdeliverableid#"><CFIF deliverabletimetoday[I] is not 0><CFOUTPUT><TR><TD colspan=5 align="Right"> #deliverablearray[I]#: </TD><TD align=right>#timeformat(deliverabletimetoday[I],"HH:mm")#</TD> </CFOUTPUT></CFIF> </CFLOOP>

		<CFSET totaltimetoday=createtimespan(0,0,0,0)>
		<CFSET x=arrayset(deliverabletimetoday, 1 , maxdeliverableid, createtimespan(0,0,0,0))>
		

	</TABLE>
	


