<!--- �Relayware. All Rights Reserved 2014 --->
<!--- <CFINCLUDE TEMPLATE="../SharedCode/PopulateLists.cfm"> --->

<CFSET qryEntity = "actions">

<!--- template has one query "getAddPermission" ---->
<!--- <CFINCLUDE TEMPLATE="../SharedCode/Perm_add.cfm"> --->

<cf_title>Action List</cf_title>

<h2>Find actions by date</h2>

Please choose a date, choose to search by the date the action was added or when it was completed
 and just click on the search button.  If you want to see all records uncheck the 

 <HR>


<FORM ACTION="ActionList.CFM" METHOD="POST">

<!--- pass this value instead of re-executing the query --->
<!--- <INPUT TYPE="HIDDEN" NAME="frmAdd" VALUE="<CFOUTPUT>#addperm#</CFOUTPUT>"> --->

<CENTER>
<TABLE>


<TR><TD ALIGN="left">Select a date: </TD>
		<TD ALIGN="left">	
			<CFSET loopdate=Now()>
			<SELECT NAME="srchDate">
			<OPTION VALUE="">Select a date
			<CFLOOP INDEX="LoopCount" FROM="1" TO="15" STEP="1">
				<CFOUTPUT><OPTION VALUE="#loopdate#">#DateFormat("#htmleditformat(loopdate)#")#</CFOUTPUT>
			<CFSET loopdate=#DateAdd('d', -1, "#loopdate#")#>
			</CFLOOP>
			</SELECT>
<TR><TD ALIGN="left">or Enter a Date (dd/mm/yy): </TD><TD ALIGN="left">		
 	<INPUT TYPE="text" NAME="srchDate" VALUE="" SIZE=8>
</TD></TR>
<TR><TD ALIGN="left">Specify which date to use: </TD><TD ALIGN="left">
	<INPUT TYPE="Radio" NAME="SrchDateType" VALUE="Started" CHECKED> Actions Added since
	<INPUT TYPE="Radio" NAME="SrchDateType" VALUE="Completed"> Actions Completed since
	<!--- <INPUT TYPE="Checkbox" NAME="srchWeekEnding"> Week ending --->
</TD></TR>
<INPUT TYPE="hidden"    NAME="srchDate_Eurodate"
    VALUE="This is not a valid search date.">

<TR><TD ALIGN="left">Only Show Live Actions: </TD>
		<TD ALIGN="left"> <INPUT TYPE="Checkbox" NAME="srchActionLive" CHECKED> 
</TD></TR>

</TABLE>

<INPUT TYPE="submit" VALUE="Search">
<INPUT TYPE="Reset" VALUE="Clear Contents">
</CENTER>
</FORM>



<CFINCLUDE TEMPLATE="../SharedCode/Footer.cfm">



