<!--- �Relayware. All Rights Reserved 2014 --->

<!--- <CFQUERY NAME="UpdateTimeTaken" DATASOURCE="#application.siteDataSource#">
	UPDATE actionnote 
	SET actionnote.timetakenI = 
	(Minute([actionnote].[timetaken])/60)+(Hour([actionnote].[timetaken]))
</CFQUERY> --->

<cfparam name="srchStartDate" type="date" default="#dateAdd("m",-1,now())#">
<cfparam name="srchEndDate" default="">
<cfparam name="srchProjectID" default="0">

<CFQUERY NAME="AnalyseTime" DATASOURCE="#application.siteDataSource#">
SELECT isnull((datePart(Hour,[timetaken]) + (cast(datePart(Minute,timetaken) as decimal(5,2))/60)),0) as SumOftimetaken,  
Project.ProjectName, Actions.projectID
FROM Project 
	INNER JOIN actions ON Project.ProjectID = actions.ProjectID
	INNER JOIN actionnote ON actions.ActionID = actionnote.actionid
	
	WHERE actionnote.datestart >  <cf_queryparam value="#srchStartDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >  
	
    <CFIF srchEndDate IS NOT "">
       AND actionnote.datestart <  <cf_queryparam value="#srchEndDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >  
    </CFIF>
	
    <CFIF srchProjectID IS NOT "0">
       AND actions.ProjectID =  <cf_queryparam value="#srchProjectID#" CFSQLTYPE="CF_SQL_INTEGER" >  
    </CFIF>


	ORDER BY Project.ProjectName
</CFQUERY>

<CFIF srchProjectID IS NOT "0">
	<CFQUERY NAME="GetProjectName" DATASOURCE="#application.SiteDataSource#">
	Select ProjectName from project where projectid =  <cf_queryparam value="#SrchProjectID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</CFQUERY>
</CFIF>

<h2>Project Time Analysis <CFOUTPUT>#DateFormat(srchStartDate)# to #Dateformat(srchEndDate)#</CFOUTPUT>
    <CFIF srchProjectID IS NOT "0">
		<BR> <CFOUTPUT>#htmleditformat(GetProjectName.ProjectName)#</CFOUTPUT>
    </CFIF>
</h2>

<FORM ACTION="TimeDetail.cfm" METHOD="POST">
	<CF_INPUT TYPE="Hidden" NAME="srchEndDate" VALUE="#srchEndDate#">
	<CF_INPUT TYPE="Hidden" NAME="srchStartDate" VALUE="#srchStartDate#">
	<CFTABLE QUERY="AnalyseTime" COLSPACING="3" COLHEADERS HTMLTABLE>
			 <CFCOL TEXT="#ProjectName#" HEADER="Project Name" ALIGN="LEFT">
			 <CFCOL TEXT="#DecimalFormat(SumofTimeTaken)#" HEADER="Total Hours Spent" ALIGN="RIGHT">
<!--- 			 <INPUT TYPE="Submit" NAME="View" VALUE="View"> --->
<!--- 			 <INPUT TYPE="Hidden" NAME="projectID" VALUE="#projectID#"> --->
	</CFTABLE>
</FORM>



