<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			.cfm
Author:				SWJ
Date started:			/xx/02

Description:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
	30-APR-2008		NJH			Added max length to the text fields. Trend NABU bug issue 258.
	2015/02/10		NJH			If the queries don't return any results for topics/initiators/types, then don't show the empty select as it's confusing

Possible enhancements:


 --->

<cfparam name="actionNoteID" default="">
<cfparam name="actionID" default="">
<!--- workarround for default locale being en_US on www1 --->
<cfset x = setlocale("English (UK)")>
<!--- for uniquely naming new windows and thus allowing seperate popups. if this is a new note, increment the value--->
<cfif actionNoteID eq "">
	<cfif isDefined("request.windowIDIncr")>
		<cfset request.windowIDIncr = request.windowIDIncr + 1>
	</cfif>
</cfif>

<!--- save data --->
<cfif isDefined("form.bSubmit")>
	<cfif form.action eq "update">
		<cfscript>
			application.com.relayActions.updateActionNote(form);
		</cfscript>
	<cfelse>
		<cfscript>
			actionNoteID = application.com.relayActions.createActionNote(form);
		</cfscript>
	</cfif>
</cfif>

<!--- get topics, initiators and types --->
<cfscript>
	topics = application.com.relayActions.getTopics();
	initiators = application.com.relayActions.getInitiators();
	types = application.com.relayActions.getTypes();
	actionNote = application.com.relayActions.getActionNoteDetail(actionNoteID,actionID);
</cfscript>

<cfif not isQuery(actionNote) or (actionNote.actionID eq 0)>
	No actionID or actionNoteID specified.
	<CF_ABORT>
</cfif>

<cf_title>Action ID <cfoutput>#actionNote.actionID# - #actionNote.description#</cfoutput></cf_title>



<cfset request.displayType="HTML-table">
<cfform  method="POST" novalidate="true">
<cf_relayFormDisplay>

	<cfif actionNoteID neq "">
		<cf_input type="hidden" name="action" value="update">
	<cfelse>
		<cf_input type="hidden" name="action" value="create">
	</cfif>
	<cf_input type="hidden" name="actionID" value="#actionID#">
	<cf_input type="hidden" name="actionNoteID" value="#actionNoteID#">

	<cf_relayFormElementDisplay relayFormElementType="html" fieldname="frmHTML" currentValue="Action ID #htmleditformat(actionNote.actionID)# - #htmleditformat(actionNote.description)#" label="phr_ThisNoteRelatesTo">

	<cf_relayFormElementDisplay relayFormElementType="Text" fieldname="summary" required="true" currentValue="#actionNote.summary#" maxLength="80" message="phr_Ext_ActionNote_SummaryMessage" label="phr_Summary">

	<cf_relayFormElementDisplay relayFormElementType="html" fieldname="frmHTML" currentValue="" required="true" label="phr_Topic">
		<cfif topics.recordCount><cf_select name="topic_select" query="#topics#" display="topic" value="topic" currentValue="" width=150 onChange="$('topic').value = $F(this)" showNull="true" required="false"></cfif>
		<cfinput type="Text" id="topic" name="topic" width="150" label="" required="true" message="#application.com.relayTranslations.translatePhrase(phrase='phr_Ext_ActionNote_TopicMessage')#" visible="Yes" enabled="Yes" value="#actionNote.topic#" maxLength="20">
	</cf_relayFormElementDisplay>

	<cf_relayFormElementDisplay relayFormElementType="html" fieldname="frmHTML" currentValue="" label="phr_Initiator">
		<cfif initiators.recordCount><cf_select name="initiator_select" query="#initiators#" display="initiator" value="initiator" currentValue="" width=150 onChange="$('initiator').value = $F(this)" showNull="true" required="false"></cfif>
		<cfinput type="Text" id="initiator" name="initiator" width="150" label="" required="No" visible="Yes" enabled="Yes" value="#actionNote.initiator#" maxLength="50">
	</cf_relayFormElementDisplay>

	<cf_relayFormElementDisplay relayFormElementType="html" fieldname="frmHTML" currentValue="" label="phr_Category">
		<cfif types.recordCount><cf_select name="type_select" query="#types#" display="type" value="type" currentValue="" width=150 onChange="$('type').value = $F(this)" showNull="true" required="false"></cfif>
		<cfinput type="Text" id="type" name="type" width="150" label="" required="No" visible="Yes" enabled="Yes" value="#actionNote.type#" maxLength="15">
	</cf_relayFormElementDisplay>

	<cf_relayFormElementDisplay relayFormElementType="Text" fieldname="TimeTakenI" width="150" currentValue="#actionNote.timeTakenI#" validate="numeric" maxLength="80" message="phr_Ext_ActionNote_TimeMessage" label="phr_TotalTime (phr_Minutes)">

	<cf_relayFormElementDisplay relayFormElementType="html" fieldname="frmHTML" currentValue="" label="phr_Calls">
		<cfinput type="Text" name="Calls" width="100" label="" required="No" visible="Yes" enabled="Yes" value="#actionNote.calls#" validate="integer" message="phr_Ext_ActionNote_InvalidCallsMessage">
		&nbsp;<span class="label">phr_Emails:</span>&nbsp;
		<cfinput type="Text" name="Emails" width="100" label="" required="No" visible="Yes" enabled="Yes" value="#actionNote.emails#" validate="integer" message="phr_Ext_ActionNote_InvalidEmailsMessage">
	</cf_relayFormElementDisplay>

	<cf_relayFormElementDisplay relayFormElementType="TextArea" fieldname="NoteText" width="150" currentValue="#htmleditformat(actionNote.noteText)#" label="phr_Notes">

		<cfif actionNote.dateStart eq "">
			<cfset startDate = dateFormat(now(),'dd mmm yyyy')>
		<cfelse>
			<cfset startDate = dateFormat(actionNote.dateStart,'dd mmm yyyy')>
		</cfif>

	<cf_relayFormElementDisplay relayFormElementType="date" fieldname="dateStart" currentValue="#startDate#" label="phr_DateStarted">

	<cf_relayFormElementDisplay relayFormElementType="date" fieldname="dateEnd" currentValue="#actionNote.dateEnd#" label="phr_DateEnded">

	<cf_relayFormElementDisplay relayFormElementType="submit" fieldname="bSubmit" currentValue="Save" label="">

</cf_relayFormDisplay>
</cfform>



