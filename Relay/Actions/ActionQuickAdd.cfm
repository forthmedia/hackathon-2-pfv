<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File name:		ActionQuickAdd.cfm
Author:
Date created:

Amendment History:

YYYY/MM/YY	Initials 	What was changed
2011/01/13 				LID:4314 MS Dropdown to record active campaigns
2011/05/03	PPB			moved time next to due date
2011/05/03	PPB			populate the assignTo dropdown using the new way of populating validvalues with a cfc func
2011/10/05	WAB			LID 7936 Move form outside of table so that calendar popup works
2011/10/18	WAB			LID 7924 add ordering to getperson query
Enhancement still to do:

--->

<CFPARAM NAME="frmEntityType" DEFAULT="organisation">
<CFPARAM NAME="frmcurrentEntityID" DEFAULT="1">
<CFPARAM NAME="getPersonListType" DEFAULT="default">

<CFIF isDefined("frmAddAction") and frmAddAction eq "save">
<!--- <cfoutput>FORM.txtDate=#FORM.txtDate# FORM.frmNextActionTime=#FORM.frmNextActionTime#</cfoutput> --->
	<cfset myDate = dateadd("n",form.frmNextActionMinute,dateadd("h",frmNextActionHour,txtDate))>

<!--- 	<cfif isnumeric(left(txtDate,4)) eq "true">
		<cfset myDate = dateadd("n",frmNextActionMinute,dateadd("h", frmNextActionHour, txtDate))>
	<cfelse>
		<cfset myDate = CreateDateTime(mid(txtDate,7,4), mid(txtDate,4,2), left(txtDate,2), frmNextActionHour, frmNextActionMinute, 00)>
	</cfif>
 --->
	<!--- <cfset myDate = CreateDateTime(FORM.frmYear, FORM.frmMonth, FORM.frmDay,
	  mid(FORM.frmNextActionTime,5,2), mid(FORM.frmNextActionTime,8,2), 00)> --->
	<!--- Debug: <cfoutput>
	<UL>
	  <LI>Built with CreateDate:
	    #CreateDate(FORM.frmyear, FORM.frmmonth, FORM.frmday)#
	  <LI>Built with CreateDateTime:
	    #DateFormat(CreateDateTime(FORM.frmyear, FORM.frmmonth, FORM.frmday,
	      mid(FORM.frmNextActionTime,5,2), mid(FORM.frmNextActionTime,8,2), 00))#
	  <LI>Built with CreateODBCDate: #CreateODBCDate(myDate)#
	  <LI>Built with CreateODBCDateTime: #CreateODBCDateTime(myDate)#
	</UL>
	</cfoutput>
	<CF_ABORT> --->
<!--- <CFIF isDate("myDate") eq "no">it's not a date</CFIF> --->

	<CF_addAction
		ownerPersonID 	= #frmOwnerPersonID#
		entityID   		= #frmNactEntityID#
		entityTypeID 	= #frmEntityTypeID#
		description		= #frmDescription#
		priority		= #frmPriority#
		campaign		= #form.activeCampaigns#<!--- 12Jan2011 MS LID:4314 sending value of selected active capaing to be stored against the action--->
		notes			= #form.frmActionNotes#
		actionTypeID	= #form.frmActionTypeID#
		deadline		= #myDate#
		StatusID		= #form.StatusID#
		CompletedTime	= #form.CompletedTime#
		CompletedDate	= #form.CompletedDate#
	>
	<cfset message="Action added">
	<CFSET session.campaignID="#form.activeCampaigns#"><!--- MS  2011/01/13 LID4314 storing the selected active campaign as sessions variable for later use--->
</CFIF>

<CFQUERY NAME="getActionTypes" dataSource="#application.siteDataSource#">
	select actionTypeID, actionType from actionType
</CFQUERY>


<CFQUERY NAME="GetPerson" dataSource="#application.SiteDataSource#">
	Select firstname+' '+Lastname AS Name, PersonID
	FROM Person
	<cfswitch expression="#getPersonListType#">
		<cfcase value="default">
			<CFIF frmentityType IS "Location">
				WHERE locationId =  <cf_queryparam value="#frmcurrentEntityID#" CFSQLTYPE="CF_SQL_INTEGER" >
			<CFELSEIF frmEntityType eq "organisation">
				WHERE organisationID =  <cf_queryparam value="#frmcurrentEntityID#" CFSQLTYPE="CF_SQL_INTEGER" >
			<CFELSEIF frmentityType IS "Person">
				WHERE organisationID = (select organisationID from person
							where personID =  <cf_queryparam value="#frmcurrentEntityID#" CFSQLTYPE="CF_SQL_INTEGER" > )
			</CFIF>
	    </cfcase>
	</CFSWITCH>
	order by name
</CFQUERY>

<CFQUERY NAME="GetUserName" DATASOURCE="#application.SiteDataSource#">
	Select firstname+' '+Lastname AS Name, personid
	FROM Person
	WHERE personid = #request.relayCurrentUser.personid#
</CFQUERY>

<cfquery name="getEntityTypes" dataSource="#application.siteDataSource#">
	select entityTypeID from schemaTable where entityName =  <cf_queryparam value="#frmEntityType#" CFSQLTYPE="CF_SQL_VARCHAR" >
</cfquery>

<CFQUERY NAME="getStatus" dataSource="#application.siteDataSource#">
	SELECT actionStatus.actionStatusID, actionStatus.Status, actionStatus.status_order
		FROM actionStatus
		ORDER BY actionStatus.status_order
</CFQUERY>

<cfif getEntityTypes.recordCount eq 1>
	<CFSET thisEntityTypeID = getEntityTypes.entityTypeID>
<cfelse>
	<CFABORT showerror="This Entity type is not supported">
</CFIF>

<script>
	<!--  // hide from browsers
	function enterDate(form,field,datestring) {

		fieldtosearchfor=field+'Time'

		form = 	document.actionInput
			for (var i=0; i<form.length;i++) {
				if (form[i].name==fieldtosearchfor){
					form[i].value=datestring.getHours()+':'+datestring.getMinutes()
					break
				}
			}
		fieldtosearchfor=field+'Date'
			// loop continues where previous one left off, so assumes that time preceeds date
			for (var i=i; i<form.length;i++) {

				if (form[i].name==fieldtosearchfor && form[i].type == 'select-one' ){				// if there is a select box of the same name, then it is set to blank
					form[i].selectedIndex = 0
				}

				if (form[i].name==fieldtosearchfor && form[i].type == 'text' ){				// check for text ensures that we don't try and update the select box
					form[i].value=datestring.getDate()+'/'+(datestring.getMonth()+1)+'/'+datestring.getFullYear() // NJH 2009/08/28 LID 2565 use full year
					break
				}



			}

	return;
	}

	<!--- 2012-03-16	RMB KER002 --->
	function checkForCompleted() {
		form = 	document.actionInput

		if (form.StatusID.options[form.StatusID.selectedIndex].text == 'Complete') {
		 	if (form.CompletedTime.value == '' & form.CompletedDate.value == ''){
				enterDate('','Completed',new Date())
			 }

		}

		return;
	}

	function UpdateEntityTypeID() {
		form = 	document.actionInput

		if (form.frmNactEntityID.options[form.frmNactEntityID.selectedIndex].text == '') {
			form.frmEntityTypeID.value = 2;
		} else {
			form.frmEntityTypeID.value = 0;
		}

		return;
	}
	-->
</script>

<cf_title>Add Actions</cf_title>

<cfinclude template="/templates/relayFormJavaScripts.cfm">
<cfparam name="message" type="string" default="">
<CFPARAM NAME="request.actionDueDateDays" DEFAULT="7">   <!--- days in future to set due date of action --->
<CFPARAM NAME="txtDate" DEFAULT=#dateadd("d",request.actionDueDateDays,now())#>

<!--- 2011/05/03 PPB START - done for Lexmark to restrict AssignTo list in Valid Values --->
<CFIF IsDefined("frmentityType") and frmentityType eq "person" and IsDefined("frmCurrentEntityID") and frmCurrentEntityID neq 0>
	<CFSET countryIdOfPersonToContact = application.com.relayPLO.getPersonDetails(frmCurrentEntityID).countryID>
<cfelse>
	<CFSET countryIdOfPersonToContact = 0>
</CFIF>

<cfset validValueQuery = application.com.relayForms.getValidValues (fieldname = 'action.OwnerPersonID',countryID = countryIdOfPersonToContact) >
<!--- 2011/05/03 PPB END --->

<cf_RelayNavMenu pageTitle="Phr_Sys_Nextactiondetails" thisDir="actions">
	<cf_RelayNavMenuItem MenuItemText="Phr_Sys_Save" CFTemplate="javascript:document.actionInput.submit()">
</cf_RelayNavMenu>

<cfform action="#SCRIPT_NAME#?#request.query_string#" method="POST" name="actionInput">
	

	<div class="actionsForm">		
		<cfoutput>
			<CF_INPUT type="hidden" name="frmEntityTypeID" value="#thisEntityTypeID#"></cfoutput>
			<INPUT type="hidden" name="frmAddAction" value="save">
			
			<!--- <div class="row">
				<div class="col-sm-3 col-sx-4">
					<p>
						Phr_Sys_Nextactiondetails
					</p>
				</div>
				<div class="col-sm-9 col-sx-8">
					<a href="javascript:document.actionInput.submit()" class="Submenu" title="Click to save this action" onMouseOver="status='Click to save this action'; return true">Phr_Sys_Save</a>
				</div>
			</div> --->

			<div class="row">
				<div class="col-xs-12">
					<cfif message neq "">
						<p>
							<cfoutput>#application.com.relayUI.message(message)#</cfoutput>
						</p>
					</cfif>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-3 col-sx-4">
					<label class="label">Phr_Sys_Actiontype</label>
				</div>
				<div class="col-sm-9 col-sx-8">
					<SELECT NAME="frmActionTypeID" class="form-control">
						<!--- <OPTION VALUE="0">Select type of Action</OPTION> --->
						<CFOUTPUT QUERY="getActionTypes">
							<OPTION VALUE="#actionTypeId#" <CFIF 1 EQ actionTypeId>SELECTED</CFIF>>#htmleditformat(ActionType)#</OPTION>
						</CFOUTPUT>
					</SELECT>
				</div>
			</div>

			<!--- 2012/03/16	RMB KER002 - Added "organisation"--->
			<CFIF IsDefined("frmentityType") and (frmentityType eq "person" or frmentityType eq "organisation") and IsDefined("frmCurrentEntityID") and frmCurrentEntityID neq 0>
				<div class="row">
					<div class="col-sm-3 col-sx-4">
						<label class="label">Person next action is with</label>
					</div>
					<div class="col-sm-9 col-sx-8">
						<SELECT NAME="frmNactEntityID" onchange="UpdateEntityTypeID()" class="form-control">
							<!--- 2012/03/16	RMB KER002 - Added "organisation" Start--->
							<cfif frmentityType eq "organisation">
								<CFOUTPUT>
									<OPTION VALUE="#frmCurrentEntityID#" <CFIF IsDefined("frmentityType") and frmentityType eq "organisation" and IsDefined("frmCurrentEntityID") and frmCurrentEntityID neq 0>SELECTED</CFIF>></OPTION>
								</CFOUTPUT>
							</cfif>
							<!--- 2012/03/16	RMB KER002 - Added "organisation" END--->
							<CFOUTPUT QUERY="GetPerson">
								<OPTION VALUE="#PersonID#" <CFIF IsDefined("frmentityType") and frmentityType eq "person" and IsDefined("frmCurrentEntityID") and frmCurrentEntityID neq 0><CFIF frmCurrentEntityID EQ personid>SELECTED</CFIF></CFIF>>#htmleditformat(Name)#</OPTION>
							</CFOUTPUT>
						</SELECT>
					</div>
				</div>
				<cfelse>
				<cfoutput>
					<CF_INPUT type="Hidden" name="frmNactEntityID" value="#frmCurrentEntityID#">
				</cfoutput>
			</CFIF>
	
			<!--- <div class="row">
				<div class="col-sm-3 col-sx-4">
					<p>Phr_Sys_NextactionTimeHHMM</p>
				</div>
				<div class="col-sm-9 col-sx-8">
					<CF_relayTimeField hourFieldName="frmNextActionHour" minuteFieldName="frmNextActionMinute">
				</div>
			</div> --->

			<div class="row">
				<div class="col-sm-3 col-sx-4">
					<label class="label">Phr_Sys_Priority</label>
				</div>
				<div class="col-sm-9 col-sx-8">
					<SELECT name="frmPriority" class="form-control">
						<OPTION VALUE="0" SELECTED></OPTION>
						<OPTION VALUE="1">Low</OPTION>
						<OPTION VALUE="2">Med</OPTION>
						<OPTION VALUE="3">Hi</OPTION>
					</SELECT>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-3 col-sx-4">
					<label class="label">Phr_Sys_Assignto</label>
				</div>
				<div class="col-sm-9 col-sx-8">
					<!---<cf_displayValidValues validFieldName = "action.OwnerPersonID" formFieldName =  "frmOwnerPersonID"
									keepOrig = "false" showNull = "false"	<!--- NJH 2007/01/26 ---> currentValue = "#request.relayCurrentUser.personid#">--->
					<!--- 2011/05/03 PPB START --->
					<cfoutput>#application.com.relayForms.validValueDisplay (query = validValueQuery,name="frmOwnerPersonID",selected=request.relayCurrentUser.PersonID)#</cfoutput>
					<!--- 2011/05/03 PPB END --->
					<CFOUTPUT QUERY="GetUserName">
						<CF_INPUT TYPE="Hidden" NAME="LastUpdatedBy" VALUE="#PersonID#">
					</CFOUTPUT>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-3 col-sx-4">
					<label class="label">Phr_Sys_Duedate</label>
				</div>
				<div class="col-sm-9 col-sx-8">
					<!---<cfoutput>
						<input type="Text" name="txtDate" value="#txtDate#" readonly>
					</cfoutput>
					<A HREF="javascript:show_calendar('actionInput.txtDate',null,'','DD-MM-YYYY',null,'AllowWeekends=No;CurrentDate=Today')" TITLE="Click to choose date" onMouseOver="window.status='Pop Calendar';return true;" onMouseOut="window.status='';return true;">
						<cfoutput>
							<IMG SRC="/images/misc/IconCalendar.gif" ALIGN="ABSMIDDLE" BORDER="0">
						</cfoutput>
					</A> --->
			 		<!--- <cfif isnumeric(left(txtDate,4)) eq "true">
						<cfset popDate = txtDate>
					<cfelse>
						<cfset popDate = CreateODBCDate(CreateDate(mid(txtDate,7,4), mid(txtDate,4,2), left(txtDate,2)))>
 					</cfif>--->
	 				<cf_relayDateField divName="calDivTxtDate" currentValue="#txtdate#" thisFormName="actionInput"
						fieldName="txtDate" anchorName="anchor1X" helpText="Select the date">
					<div class="input-group">
						<CF_relayTimeField hourFieldName="frmNextActionHour" minuteFieldName="frmNextActionMinute">
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-3 col-sx-4">
					<label class="label">Status:</label>
				</div>
				<div class="col-sm-9 col-sx-8">
					<cfset StatusIDcurrentValue = 0>
					<cfloop query="getStatus">
						<cfif Status EQ 'Not-Started'>
							<cfset StatusIDcurrentValue = #actionStatusID#>
							<cfbreak>
						</cfif>
					</cfloop>
					<CF_displayValidValues formFieldName = "StatusID" currentValue = "#StatusIDcurrentValue#" displayValueColumn = "Status"
						dataValueColumn = "actionStatusID" validValues = #getStatus# onChange="checkForCompleted(this, this.form);"
						required="yes" RequiredLabel="Please select a Status"	showNull="false">
				</div>
			</div>

			<div class="row completedTime">
				<div class="col-sm-3 col-sx-4">
					<label class="label">Completed at:</label>
				</div>
				<div class="col-sm-9 col-sx-8">
					<div class="form-inline">
						<INPUT class="form-control" TYPE="text" NAME="CompletedTime" VALUE="" SIZE=4 placeholder="hh:mm"> 
						<!--- <p>hh:mm</p> --->
						<INPUT class="form-control" TYPE="text" NAME="CompletedDate" VALUE="" SIZE="12" placeholder="dd/mm/yyyy"> 
						<!--- <p>dd/mm/yyyy</p>  --->
						<!--- <p>(hh:mm dd/mm/yyyy)</p>  --->
						<A HREF="javascript:enterDate(this.form,'Completed',new Date());" class="btn btn-primary">Time Now</A>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-3 col-sx-4">
					<label class="label">Phr_Sys_About</label>
				</div>
				<div class="col-sm-9 col-sx-8">
					<INPUT class="form-control" TYPE="text" NAME="frmDescription" SIZE="50" MAXLENGTH="65">
				</div>
			</div>

			<!--- LID:4314 MS 13JAN2011 Dropdown to record active campaigns --->
			<CFIF application.com.settings.getSetting("campaigns.displayCampaigns")>
				<!--- getting all active campaigns --->
				<cfset getActiveCampaigns = application.com.relayCampaigns.GetCampaigns(currentvalue=structKeyExists(session,"campaignID")?session.campaignID:'')>
				<div class="row">
					<div class="col-sm-3 col-sx-4">
						<label class="label">Campaign</label>
					</div>
					<div class="col-sm-9 col-sx-8">
						<select name="activeCampaigns" class="form-control">
							<OPTION VALUE=0 <cfif not isdefined("session.campaignID")>selected="true"</cfif>>Please select a campaign</OPTION>
							<cfoutput query="getActiveCampaigns">
								<OPTION VALUE=#campaignId# <cfif isdefined("session.campaignID") and session.campaignID eq getActiveCampaigns.campaignId>selected="true"</cfif>>#htmleditformat(campaignName)#</OPTION>
							</cfoutput>
						</select>
					</div>
				</div>
			<cfelse>
				<input  type="hidden" name="activeCampaigns" value=0>
			</cfif>

			<div class="row">
				<div class="col-sm-3 col-sx-4">
					<label class="label">Phr_Sys_Notes</label>
				</div>
				<div class="col-sm-9 col-sx-8">
					<TEXTAREA NAME="frmActionNotes" class="form-control" ROWS="5" WRAP="VIRTUAL"></TEXTAREA>
				</div>
			</div>
		
		</div>
	</CFFORM>