<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 

Amendment History:

Date 		Initials 	What was changed
??-???-????	???			?
12-Nov-2003	KAP			Correction to 'Set the rights for this record:'

2005-02-28		WAB			Added hours and minutes to the dealine, linked in the relaydatefield tag
2008-10-09	NYB  		CR-TND561-bug1 - added void() to href as was returning [Object] to calling form
2008-10-10	NYB  		CR-TND561-bug2 - Save & Return was saving but not returning
2009-02-09 	NJH 		P-SNY047 changed from form to cfform as ugRecordmanager changed to using cfselect
2009-08-28	NJH			LID 2565 - changed date format from dd-mmm-yy to dd-mmm-yyyy.
2010-10-08	NAS			LID4280: Actions report is not showing the correct time.
2014-03-07	AXA			Case 439072: renamed file extension from CFM in all caps TO cfm in the form action.  Form was not submitting.
Enhancement still to do:


 --->

<CFSET qrynotetext="">
<CFSET qrynotedatestarted="">
<CFSET qrynotedateended="">
<CFSET qrynotetimespent="">
<CFPARAM Name="Message" Default="">

<!--- for uniquely naming new windows and thus allowing seperate popups. --->
<cfif not isDefined("request.windowIDIncr")>
	<cfset request.windowIDIncr = 1>
</cfif>

<!--- gets incremented on click, so ensures the window id is definately unique. --->
<cfoutput>
	<SCRIPT type="text/javasript">
		jsWinIncr = #jsStringFormat(request.windowIDIncr)#;
	</script>
</cfoutput>


<CFQUERY NAME="getAssigned" DATASOURCE="#application.SiteDataSource#">
	SELECT actionStatusID, status from actionStatus WHERE Assigned = 1
</CFQUERY>

<!--- NJH 2008-05-20 P-TND066 1.1.3 this query is moved to ActionEdit.cfm as it was also needed in view only mode --->
<!--- <cfif isDefined("getActionDetails.entityTypeID") and getActionDetails.entityTypeID neq "" and getActionDetails.entityID neq "">
	<cfif getActionDetails.entityTypeID eq "0">
		<CFQUERY NAME="getEntityDetails" dataSource="#application.siteDataSource#">
			select 
			p.firstName+' '+p.lastName+' - '+ o.organisationName+' ' +
			case when isNUll(l.telephone,'') <> '' then '<br>Switch: '+ l.telephone else '' end +  
			case when isNUll(p.officephone,'') <> '' then '<br>DD: ' + p.officephone else '' end +
			case when isNUll(p.mobilephone,'') <> '' then '<br>Mobile: '+p.mobilephone else '' end
			as entityName, 
			p.personid as Data, 'frmsrchPersonID=' as Datatype
			from person p
			INNER JOIN organisation o ON o.organisationID = p.organisationID
			join location l on l.locationid = p.locationid
			where p.personID = #getActionDetails.entityID#
		</CFQUERY>
	<cfelseif getActionDetails.entityTypeID eq "2">
		<CFQUERY NAME="getEntityDetails" dataSource="#application.siteDataSource#">
			select 'frmsrchOrgID=' as DataType, organisationid as Data, organisationName as entityName from organisation where organisationID = #getActionDetails.entityID#
		</CFQUERY>
	<cfelseif getActionDetails.entityTypeID eq "23">
		<CFQUERY NAME="getEntityDetails" dataSource="#application.siteDataSource#">
			select entityid as Data, 'frmsrchOrgID=' as DataType, detail as entityName from opportunity where opportunityID = #getActionDetails.entityID#
		</CFQUERY>
	<cfelse>
		<cfset getEntityDetails.entityName = "">
	</cfif>
<cfelse>
	<cfset getEntityDetails.entityName = "">
</CFIF> --->

<!--- assume that the following fields are defined (even if "")
		#qryDescription#
		#qryDeadline#
		#qryOwnerID#
		#qryCreatorID#
		#qryStatusID#
		#qryDeliverableID#
		#qryNotes#
		#qrytimespent#
		#qryDateEntered#
		#qryCompletedDate#
		#qryRequestedBy#

		#qryPermission#
		
		#page_header#
		#function#   "edit" or "add"
		--->
		



<cf_head>
<cf_title>Action entry form</cf_title>
<cfoutput>
	<SCRIPT type="text/javascript" SRC="/javascript/Calendar.js"></SCRIPT>
</cfoutput>
<cfoutput>
	<SCRIPT type="text/javascript"  src="/javascript/openWin.js"></script>
</cfoutput>

<SCRIPT type="text/javascript">
<!-- // hide from browsers

<!--- NYB 2008-10-10 CR-TND561-bug2 - added: --->
<CFIF not isDefined("function")>
	<cfset function="edit">
</CFIF>
<!--- :NYB --->

<CFIF #function# IS "add">	
	function checkStatus(owner,form) {
		var status_selected = form.StatusID.options[form.StatusID.selectedIndex];
		var owner_selected = owner.options[owner.selectedIndex]; 
		if (owner_selected.value != <CFOUTPUT>#request.relayCurrentUser.personid#</CFOUTPUT> && status_selected.value != <CFOUTPUT>#getAssigned.actionStatusID#</CFOUTPUT>) {
			var Msg = "\n\nYou may only assign a task to someone if the status of the task is '<CFOUTPUT>#getAssigned.Status#</CFOUTPUT>'.";
			alert(Msg);
			for (Count=0; Count < owner.length; Count++) {
				if (owner.options[Count].value == <CFOUTPUT>#request.relayCurrentUser.personid#</CFOUTPUT>) {
					owner.selectedIndex = Count;
					break;
				}
			}
		}
	}
	function checkOwner(status,form) {
		var status_selected = status.options[status.selectedIndex];
		var owner_selected = form.PersonID.options[form.PersonID.selectedIndex];
		if (owner_selected.value != <CFOUTPUT>#request.relayCurrentUser.personid#</CFOUTPUT> && status_selected.value != <CFOUTPUT>#getAssigned.actionStatusID#</CFOUTPUT>) {
			var Msg = "\n\nSince you are not the owner, status must be '<CFOUTPUT>#getAssigned.Status#</CFOUTPUT>'.";
			alert(Msg);
			for (Count=0; Count < status.length; Count++) {
				if (status.options[Count].value == <CFOUTPUT>#getAssigned.actionStatusID#</CFOUTPUT>) {
					status.selectedIndex = Count;
					break;
				}
			}
		}

	checkForCompleted()
		
	}
<CFELSE>

	function checkStatus(owner,form) {
		return;
	}	

	function checkOwner(status,form) {
		checkForCompleted()

		return;
	}


</CFIF>		

	function enterDate(form,field,datestring) {

		fieldtosearchfor=field+'Time'

		form = 	document.ActionForm
			for (var i=0; i<form.length;i++) {
				if (form[i].name==fieldtosearchfor){
					form[i].value=datestring.getHours()+':'+datestring.getMinutes()
					break
				}
			}
		fieldtosearchfor=field+'Date'
			// loop continues where previous one left off, so assumes that time preceeds date
			for (var i=i; i<form.length;i++) {

				if (form[i].name==fieldtosearchfor && form[i].type == 'select-one' ){				// if there is a select box of the same name, then it is set to blank
					form[i].selectedIndex = 0
				}

				if (form[i].name==fieldtosearchfor && form[i].type == 'text' ){				// check for text ensures that we don't try and update the select box
					form[i].value=datestring.getDate()+'/'+(datestring.getMonth()+1)+'/'+datestring.getFullYear() // NJH 2009-08-28 LID 2565 use full year
					break
				}

				
				
			}
		
	return;
	}


	function CalculateTimeSpent(form) {
		form = 	document.ActionForm		

		if (form.NoteStartedDate.value!=form.NoteEndedDate.value)
		{ alert('Cant calculate time spent over multiple days')
		
		}

	d='1990-01-01 '
	t1=	form.NoteStartedTime.value
	t2=	form.NoteEndedTime.value

	diff = Date.parse(d+t2) - Date.parse(d+t1)
	diffhours = (Math.floor(diff / 3600000)) 
	diffminutes = (Math.floor(diff / 60000))- (60*diffhours)
	form.NoteTimeSpent.value= diffhours+':'+diffminutes
		
	return;	
	}


	function checkForCompleted() {
		form = 	document.ActionForm		

		if (form.StatusID.options[form.StatusID.selectedIndex].text == 'Complete') {  
		 	if (form.CompletedTime.value == '' & form.CompletedDate.value == ''){
				enterDate('','Completed',new Date())					
			 }
				
		}

		return;
	}

	function save(action){
		form = document.ActionForm
		form.btnActionUpdate.value = action

		<!--- NYB 2008-10-10 CR-TND561-bug4 - added: --->
		if (document.forms["ActionForm"].NoteText.value.length > 0 && document.forms["ActionForm"].ActionNoteType.value == "") 
		{
			var resp=confirm("Type is a mandatory field, please review your note\n\nPress OK to retry or Cancel to close");
			if (resp==true) {
				document.getElementById('ActionNoteType').focus()
				
			}
			else {
				form.reset()
			}
		} else {
		<!--- :NYB --->
		/*elementName = "UG_actions_<cfoutput>#actionid#</cfoutput>_1";
		with ( document.forms["ActionForm"] )
		for ( optionIndex = 0; optionIndex < elements[elementName].options.length; optionIndex++ )
		{
			if ( elements[elementName].options[optionIndex].value != "" )
			elements[elementName].options[optionIndex].selected = true;
		}*/
		form.submit()
		}
	<!--- NYB 2008-10-10 CR-TND561-bug4 - added: --->
	}
	<!--- :NYB --->
//-->
</SCRIPT>

</cf_head>



<!--- NYB 2008-10-10 CR-TND561-bug2 - added if statment to NJHs addition: --->
<cfif not isDefined("current")>
	<CFSET current ="ActionForm.cfm"> <!--- NJH 2008-10-10 --->
</cfif>

<CFINCLUDE TEMPLATE="actionTopHead.cfm">
<!--- NJH 2009-02-09 P-SNY047 changed from form to cfform as ugRecordmanager changed to using cfselect --->
<CFFORM NAME="ActionForm" ACTION="/actions/ActionTask.cfm" METHOD="POST" ><!--- 2014-03-07 AXA Case 439072: renamed file extension from CFM in all caps TO cfm in the form action.  Form was not submitting. --->

<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">

<cfoutput>
	<!--- NYB 2008-10-10 CR-TND561-bug2 - added: --->
	<CF_INPUT TYPE="hidden" NAME="current" VALUE="#current#">
	<!--- :NYB --->
	<INPUT TYPE="hidden" NAME="btnActionUpdate" VALUE="0">
	<INPUT TYPE="hidden" NAME="StatusID_required" VALUE="You cannot leave the field (Status) empty.">
	<CF_INPUT TYPE="hidden" NAME="CreatorID" VALUE="#qryCreatorID#">
	<INPUT TYPE="hidden" NAME="TimeSpent_Time" VALUE="The value in the field (Time Spent) must be a valid time hour:minute.">
	<INPUT TYPE="hidden" NAME="CompletedTime_Time" VALUE="The value in the field (Time Completed) must be a valid time hour:minute.">
	</cfoutput>
<!--- 	<INPUT TYPE="hidden" NAME="CompletedDate_eurodate" VALUE="The value in the field (Date Completed) must be a valid date day-month-year."> --->
	<CFOUTPUT>
		<CFIF IsDefined("srchStatusID")><CF_INPUT TYPE="hidden" NAME="srchStatusID" VALUE="#srchStatusID#"></CFIF>
		<CFIF IsDefined("srchStrandID")><CF_INPUT TYPE="hidden" NAME="srchStrandID" VALUE="#srchStrandID#"></CFIF>
		<CFIF IsDefined("srchPersonID")><CF_INPUT TYPE="hidden" NAME="srchPersonID" VALUE="#srchPersonID#"></CFIF>
		<CFIF IsDefined("srchUserGroupID")><CF_INPUT TYPE="hidden" NAME="srchUserGroupID" VALUE="#srchUserGroupID#"></CFIF>
		<CFIF IsDefined("srchDeliverableID")><CF_INPUT TYPE="hidden" NAME="srchDeliverableID" VALUE="#srchDeliverableID#"></CFIF>
		<CFIF IsDefined("srchActionID")><CF_INPUT TYPE="hidden" NAME="srchActionID" VALUE="#srchActionID#"></CFIF>
		<CFIF IsDefined("srchProjectID")><CF_INPUT TYPE="hidden" NAME="srchProjectID" VALUE="#srchProjectID#"></CFIF>
		<CFIF IsDefined("srchActionLive")><CF_INPUT TYPE="hidden" NAME="srchActionLive" VALUE="#srchActionLive#"></CFIF>
		<CFIF IsDefined("srchDate")><CF_INPUT TYPE="hidden" NAME="srchdate" VALUE="#srchdate#"></CFIF>
		<CFIF IsDefined("srchDatetype")><CF_INPUT TYPE="hidden" NAME="srchdatetype" VALUE="#srchdatetype#"></CFIF>
		<CFIF IsDefined("srchword")><CF_INPUT TYPE="hidden" NAME="srchword" VALUE="#srchword#"></CFIF>
		<CFIF IsDefined("frmCurrentEntityID")><CF_INPUT TYPE="hidden" NAME="frmCurrentEntityID" VALUE="#frmCurrentEntityID#"></CFIF>
		<CFIF IsDefined("frmEntityType")><CF_INPUT TYPE="hidden" NAME="frmEntityType" VALUE="#frmEntityType#"></CFIF>
	</CFOUTPUT>
	<CFIF isdefined("actionid")>
	<tr>
		<td colspan="2"><CFOUTPUT><CFIF Message is not "">#application.com.relayUI.message(message)#</cfif></CFOUTPUT></td>
	</tr>
	<TR><TD VALIGN="top" CLASS="label">Action ID: </TD>
	     <TD><CFOUTPUT>#htmleditformat(actionid)#</CFOUTPUT></TD></TR>
	</cfif>
	<TR><TD VALIGN="top" CLASS="label">Action With: </TD>
	    <td align="left">
		    	<!--- use sanitiseHTML as getEntityDetails.entityName has <br> s in it --->
		     <CFOUTPUT><A TITLE="Goto Record" HREF="Javascript:void(openWin('#request.currentSite.protocolAndDomain#/data/dataframe.cfm?#getEntityDetails.DataType##getEntityDetails.Data#','NewDataWindow','width=780,height=580,toolbar=0,location=0,directories=0,status=1,menuBar=0,scrollBars=1,resizable=1'));">#application.com.security.sanitiseHTML(getEntityDetails.entityName)#</a></CFOUTPUT>
		</TD></TR>
	<TR><TD VALIGN="top" CLASS="label">Type: </TD>
		<TD>
		<SELECT NAME="ActionTypeID">
			<option value="0">Choose type</option>
			<cfoutput query="getActionTypes">
			<option value="#actionTypeID#" #IIf(actionTypeID IS qryActionTypeID,DE(" SELECTED"),DE(""))#>#htmleditformat(actionType)#</option>
			</cfoutput>
		</SELECT>
		</TD>
	</TR>

	<TR><TD VALIGN="top" CLASS="label">Description: </TD>
		<TD><CF_INPUT TYPE="text" NAME="Description" VALUE="#qryDescription#" SIZE="50" MAXLENGTH="100"></TD></TR>
	
	<!--- <CFIF qryDeliverableID IS 8 OR qryDeliverableID IS 0 OR qryOwnerID IS 18> --->
	<TR><TD VALIGN="top" CLASS="label">Requested By: </TD>
		<TD><CF_INPUT TYPE="text" NAME="RequestedBy" VALUE="#qryRequestedBy#" SIZE="50" MAXLENGTH="100"></TD></TR>
	<TR><TD VALIGN="top" CLASS="label">Action added: </TD>
	     <TD><CFOUTPUT>#DateFormat(qryDateEntered,"dd-mmm-yyyy")#</CFOUTPUT></TD></TR>
	<!--- </CFIF> --->
	<CFPARAM NAME="Deadline" DEFAULT="#dateformat(dateadd("d",7,now()),"dd-mm-yyyy")#">
	<TR><TD VALIGN="top" CLASS="label">Deadline: </TD>
		<TD>
<!--- 			<CFOUTPUT><INPUT TYPE="text" NAME="Deadline" VALUE="#DateFormat(qryDeadline,"dd-mmm-yyyy")#" SIZE=13 readonly></CFOUTPUT> 
			<A HREF="javascript:show_calendar('ActionForm.Deadline',null,'','DD-Mon-YYYY',null,'AllowWeekends=No;CurrentDate=Today')" TITLE="Click to choose date" onMouseOver="window.status='Pop Calendar';return true;" onMouseOut="window.status='';return true;">
			<cfoutput><IMG SRC="/images/misc/IconCalendar.gif" ALIGN="ABSMIDDLE" BORDER="0"></cfoutput></A>
 --->			
 			<cf_relaydatefield currentvalue = "#qryDeadline#" fieldname = "Deadline" thisformname="ActionForm" helpText="Click to choose date" anchorname="x">
			<cf_relaytimefield hourfieldname = "deadline_hr" minutefieldname = "deadline_min" currenthour = "#datepart("h",qryDeadline)#" currentminute = "#datepart("n",qryDeadline)#">
			Time estimate:	<CF_INPUT TYPE="text" NAME="TimeSpent" VALUE="#timeFormat(qryTimeSpent,"hh:mm")#" SIZE=6> 
			<FONT SIZE="2">(Format hh:mm)</FONT>
		</TD>
	</TR>
	<TR><TD VALIGN="top" CLASS="label">Status: </TD>
		<TD><SELECT NAME="StatusID" onChange="checkOwner(this, this.form);">
	     <OPTION VALUE="0">
		 <CFOUTPUT QUERY="getStatus">
		 	<OPTION VALUE="#actionStatusID#"#IIf(actionStatusID IS qryStatusID,DE(" SELECTED"),DE(""))#>#HTMLEditFormat(Status)#
		 </CFOUTPUT>
	     </SELECT>
		<CF_INPUT TYPE="hidden" NAME="OldStatusID" VALUE="#qryStatusID#">
		
		Completed at: <CF_INPUT TYPE="text" NAME="CompletedTime" VALUE="#TimeFormat(qryCompletedDate,"HH:mm")#" <!--- DISABLED="#iif(qryStatusID IS NOT 3,true,false)#" --->  SIZE=4> <FONT SIZE="2"></FONT>  <CF_INPUT TYPE="text" NAME="CompletedDate" VALUE="#DateFormat(qryCompletedDate,"dd-mmm-yyyy")#" SIZE="12"> <FONT SIZE="1">(hh:mm dd/mm/yyyy)</FONT> <A HREF="javascript:enterDate(this.form,'Completed',new Date());">Time Now</A></TD></TR> 
		
		 </td></TR>
		
	<TR><TD VALIGN="top" CLASS="label">Owner: (Change to re-allocate action)</TD>
		<TD>
			<cf_displayValidValues 
				validFieldName = "organisation.InternalUsers" 
				formFieldName =  "PersonID"
				keepOrig = "false"
				currentValue = "#qryOwnerID#"
				onChange="checkStatus(this,this.form);">

<!--- 		<SELECT NAME="PersonID" onChange="checkStatus(this,this.form);">
	     <OPTION VALUE="0">
		 <CFOUTPUT QUERY="getOwners">
		  	<OPTION VALUE="#ownerPersonID#"#IIf(ownerPersonID IS qryOwnerID,DE(" SELECTED"),DE(""))#>#HTMLEditFormat(FullName)# (#HTMLEditFormat(OrganisationName)#)
		 </CFOUTPUT>
	     </SELECT> --->
		 <INPUT TYPE="HIDDEN" NAME="oldOwnerID" VALUE="<CFOUTPUT>#IIf(qryOwnerID IS NOT "",qryOwnerID,DE("0"))#</CFOUTPUT>">
		 </TD>
	</TR>
		
	<TR>
		<TD class="label" ALIGN="left" VALIGN="top">Set the rights for this record: </TD>	
		<TD ALIGN="left">
			<CFIF IsDefined("actionid")>
	 			<CF_UGRecordManager entity="actions" entityid="#actionid#" Form="ActionForm" caption1="All Users" caption2="Users with rights">
			<CFELSE>	
	 			<CF_UGRecordManager entity="actions" entityid="NewRecord" Form="ActionForm" caption1="All Users" caption2="Users with rights">
			</CFIF>
		</TD>
	</TR>
		
	<cfoutput>
	 	<TR><TD VALIGN="top" CLASS="label">Priority: </TD>
			<TD><!--- #qryPriority# --->
				<SELECT NAME="priority">
					<OPTION VALUE="0"#IIf(qryPriority eq "0",DE(" SELECTED"),DE(""))#>
		       		<OPTION VALUE="1"#IIf(qryPriority eq "1",DE(" SELECTED"),DE(""))#>Low
					<OPTION VALUE="2"#IIf(qryPriority eq "2",DE(" SELECTED"),DE(""))#>Med
					<OPTION VALUE="3"#IIf(qryPriority eq "3",DE(" SELECTED"),DE(""))#>Hi
		     </SELECT>
			</TD>
		</TR>
	</cfoutput>

 	<cfif getLiveProjects.recordCount eq 0> 
		<input type="hidden" name="projectID" value="0">
	<cfelse>
		<TR><TD VALIGN="top" CLASS="label">Project: </TD><TD>
			<SELECT NAME="ProjectID">
		     <OPTION VALUE="0">
			 <CFOUTPUT QUERY="getLiveProjects">
		       <OPTION VALUE="#ProjectID#"#IIf(ProjectID IS qryProjectID,DE(" SELECTED"),DE(""))#>#HTMLEditFormat(ProjectName)#
			 </CFOUTPUT>
		     </SELECT>
			 </TD>
		</TR>
	</cfif>
	<TR><TD VALIGN="top" CLASS="label">Notes: </TD>
		<TD VALIGN="TOP"><TEXTAREA NAME="Notes" COLS="65" WRAP="VIRTUAL" ROWS="6"><CFOUTPUT>#HTMLEditFormat(qryNotes)#</CFOUTPUT></TEXTAREA></TD>
	</TR>



<CFIF function is not "add">

	<CFSET previousowner="">
	<CFSET previousstatus="">
	<tr>
		<th colspan="2" align="center">
			ACTION HISTORY
		</th>
	</tr>
	<cfif getNotes.recordcount eq 0>
		<tr>
			<td colspan="2" align="center">
				No History for this action
			</td>
		</tr>	
	<cfelse>
		<CFLOOP query="getnotes">
			<CFOUTPUT>	
			<TR> 
				<td align="right" valign="top" class="label">
					#htmleditformat(initials)# <BR> 
					#DateFormat(lastUpdated,"dd/mm/yyyy")#<BR>
					#htmleditformat(type)#
					<a href="Javascript:void(openWin('/actions/actionNoteEdit.cfm?actionNoteID=#getNotes.actionNoteID#','ActionNote#actionNoteID#','width=720,height=500,toolbar=0,location=0,directories=0,status=1,menuBar=0,scrollBars=1,resizable=1'));">Edit</a>
				</td>
				<TD BGCOLOR="FFFFCE" valign="top" >
				<CFIF summary IS NOT ""><i>#htmleditformat(summary)#</i><BR></CFIF>
				<CFIF len(notetext) gt 0>#replace(notetext, chr(13), "<BR>","ALL")#<BR></CFIF>
				<CFIF previousowner IS NOT newowner>Allocated to #htmleditformat(getNotes.newOwnerName)# by #htmleditformat(getNotes.initials)# <BR></CFIF>
				<CFIF previousstatus IS NOT newstatus>Status changed to #htmleditformat(getNotes.newStatusText)#<BR></CFIF>
				<!--- START 2010-10-08	NAS			LID4280: Actions report is not showing the correct time. --->
				<CFIF datestart IS NOT "">Started:  #TimeFormat(datestart,"HH:mm")# <CFIF topic IS "">&nbsp #DateFormat(datestart,"dd-mmm-yyyy")# </cfif> <BR></CFIF>
				<CFIF dateend IS NOT "">Finished: #TimeFormat(dateend,"HH:mm")# <CFIF topic IS "">&nbsp #DateFormat(dateend,"dd-mmm-yyyy")# </cfif> </CFIF>
				<!--- STOP 2010-10-08	NAS			LID4280: Actions report is not showing the correct time. --->
				<CFIF timetaken IS NOT "">TimeTaken:  #TimeFormat(timetaken,"HH:mm")#<CFELSEIF timetakenI IS NOT "">TimeTaken (in minutes):  #htmleditformat(timetakenI)#</CFIF>
				<CFIF topic IS NOT ""><BR>Topic: #htmleditformat(topic)#</CFIF>
				<CFIF initiator IS NOT ""><BR>Initiator: #htmleditformat(initiator)#</CFIF>
				<CFIF type IS NOT ""><BR>Category: #htmleditformat(type)#</CFIF>
				<CFIF calls IS NOT ""><BR>Amount of calls: #htmleditformat(calls)#</CFIF>
				<CFIF emails IS NOT ""><BR>Amount of emails: #htmleditformat(emails)#</CFIF>
				</TD>
			</TR>
			
			</CFOUTPUT>
		
		<CFSET previousowner=newowner>
		<CFSET previousstatus=newstatus>
		
		</CFLOOP>
	</cfif>
</cfif>
	<tr>
		<th colspan="2" align="center">
			<!--- NYB 2008-10-09 CR-TND561 - added void() to href --->
			ADD ACTION NOTE (<cfoutput><a href="javascript:void(openWin('/actions/actionNoteEdit.cfm?actionID=#actionID#','action#actionID#_Window#request.windowIDIncr#_'+jsWinIncr,'width=700,height=500,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1'));" onClick="jsWinIncr++;">ADD WITH NEW METHOD</a></cfoutput>)
		</th>
	</tr>

<TR><TD VALIGN="top" CLASS="label">Type</TD>
		<!--- NYB 2008-10-09 CR-TND561-bug4 - added ID --->
		<TD><SELECT NAME="ActionNoteType" ID="ActionNoteType">
		<OPTION VALUE="">Choose one</OPTION>
		<OPTION VALUE="Work done">Work done</OPTION>
	   	<OPTION VALUE="Comment">Comment</OPTION>
	   	<OPTION VALUE="Reply">Reply from Client</OPTION>
	   	<OPTION VALUE="Status">Status</OPTION>
	   	<OPTION VALUE="To do">To do</OPTION>
		</SELECT></TD></TR> 

<TR><TD VALIGN="top" CLASS="label">Add new item: </TD>
	<TD VALIGN="TOP"><TEXTAREA NAME="NoteText" WRAP="VIRTUAL" COLS="65" ROWS="6"><CFOUTPUT>#HTMLEditFormat(qryNoteText)#</CFOUTPUT></TEXTAREA></TD></TR>
<TR><TD VALIGN="top" CLASS="label">Started</TD>
		<TD> <INPUT TYPE="text" NAME="NoteStartedTime" VALUE="09:00" SIZE=4> <FONT SIZE="2"></FONT> 
			<!--- <CFSET loopdate=Now()>WAB:  this didn't work - had a time element which caused problems, next line is better, although not very elegant!--->
			<CFSET loopdate=createdate(year(Now()),month(now()),day(now())) >
			<SELECT NAME="NoteStartedDate">
 			<OPTION VALUE="">Select a date
			<CFLOOP INDEX="LoopCount" FROM="1" TO="30" STEP="1">
				<CFOUTPUT><OPTION VALUE="#loopdate#">#DateFormat("#htmleditformat(loopdate)#","ddd, dd-mmm-yyyy")#</CFOUTPUT>
			<CFSET loopdate=#DateAdd('d', -1, "#loopdate#")#>
			</CFLOOP>
			</SELECT>
	or enter a Date (dd/mm/yyyy):		
 <INPUT TYPE="text" NAME="NoteStartedDate" VALUE="" SIZE=7> <FONT SIZE="1">(hh:mm dd/mm/yyyy)</FONT> 	<A HREF="javascript:enterDate(this.form,'NoteStarted',new Date());">Time Now</A></TD></TR>

<TR><TD VALIGN="top" CLASS="label">Ended</TD>
		<TD> <INPUT TYPE="text" NAME="NoteEndedTime" VALUE="17:00" SIZE=4> <FONT SIZE="2"></FONT>  
<!--- 			<CFSET loopdate=Now()> --->
			<CFSET loopdate=createdate(year(Now()),month(now()),day(now())) >
			<SELECT NAME="NoteEndedDate">
 			<OPTION VALUE="">Select a date
			<CFLOOP INDEX="LoopCount" FROM="1" TO="30" STEP="1">
				<CFOUTPUT><OPTION VALUE="#loopdate#">#DateFormat("#htmleditformat(loopdate)#","ddd, dd-mmm-yyyy")#</CFOUTPUT>
			<CFSET loopdate=#DateAdd('d', -1, "#loopdate#")#>
			</CFLOOP>
			</SELECT>
	or enter a Date (dd/mm/yyyy):		
		<CF_INPUT TYPE="text" NAME="NoteEndedDate" VALUE="#DateFormat(qryNoteDateEnded,"dd-mmm-yyyy")#" SIZE=7> <FONT SIZE="1">(hh:mm dd/mm/yyyy)</FONT> <A HREF="javascript:enterDate(this.form,'NoteEnded',new Date());">Time Now</A></TD></TR> 
<TR><TD VALIGN="top" CLASS="label">Time Spent</TD>
		<TD><CF_INPUT TYPE="text" NAME="NoteTimeSpent" VALUE="#TimeFormat(qryNoteTimeSpent,"hh:mm")#" SIZE=4> <FONT SIZE="1">(hh:mm)</FONT>  <A HREF="javascript:CalculateTimeSpent(this.form);">Calc</A></TD></TR> 

<!--- NYB 2008-10-10 CR-TND561-bug4 - added, mainly for testng: ---
<CFIF function neq "add">
	<TR>
		<TD VALIGN="top" CLASS="label"> &nbsp; </TD>
		<TD style="text-align:right; padding-right: 120px;"><INPUT TYPE="button" NAME="btnActionNote" VALUE="Save Action Note" OnClick="javaScript:save(' Update & Return ')"></TD>
	</TR>
</CFIF>
!--- :NYB --->

<INPUT TYPE="hidden" NAME="NoteStartedTime_time" VALUE="Started Time Invalid">
<!--- <INPUT TYPE="hidden" NAME="NoteStartedDate_eurodate" VALUE="Started Date Invalid"> --->
<INPUT TYPE="hidden" NAME="NoteEndedTime_time" VALUE="Ended Time Invalid">
<!--- <INPUT TYPE="hidden" NAME="NoteEndeddate_eurodate" VALUE="Ended Date Invalid"> --->
<INPUT TYPE="hidden" NAME="NoteTimeSpent_time" VALUE="Time Spent on Note Invalid">

<!---CFSET #perm_recordID# = #ActionID#--->
<!--- <CFINCLUDE TEMPLATE="../SharedCode/Permissions.cfm"> --->


</TABLE>

<CFIF function IS "add">
	<CENTER>
	<INPUT TYPE="submit" NAME="btnActionSC" VALUE="Save & Continue">
<!--- 	<INPUT TYPE="submit" NAME="btnActionSE" VALUE="Save & Exit"> --->
	<INPUT TYPE="submit" NAME="btnActionCancel" VALUE="Cancel">
	<INPUT TYPE="Reset" VALUE="Clear Contents">
	</CENTER>
<CFELSE>
	<CF_INPUT TYPE="HIDDEN" NAME="ActionID" VALUE="#ActionID#">
	<CENTER>
<!--- 	<INPUT TYPE="submit" NAME="btnActionUpdate" VALUE=" Update & Return ">
	<INPUT TYPE="submit" NAME="btnActionUpdate" VALUE=" Update & Continue "> 
	<INPUT TYPE="Reset" VALUE=" Reset ">--->
	</CENTER>
</CFIF>
</CFFORM>

</CENTER>







