<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 

Amendment History:

Date 		Initials 	What was changed
2002-03-06	SWJ			Removed references to delievrableID
12-Nov-2003	KAP			Correction to 'Set the rights for this record:'
2004-06-04		WAB			Message variable now passed to next page
2008/06/30     SSS      Added Nvarchar Support
2008-10-10	NYB  		CR-TND561-bug2 - Save & Return was saving but not returning
2009/08/28	NJH			LID 2565 - create date/time by creating odbc datetime from form fields rather than adding the date/time fields together.
2009/09/03				LID 2565 - MAJOR HACK to get the javascript dates and the coldfusion dates to work correctly if anybody has a better idea please go ahead
2010/10/07	NAS			LID4280: Actions report is not showing the correct time.
2011-06-22	NYB 		LHID4127 - Save & Return was saving but not returning.  Not sure why/how/if CR-TND561-bug2 changed fix this when it appears to now be the cause
2012-03-20	RMB			KER002 - Added cfif to stop email when updating your own actions
2014-03-07	AXA			Case 439072: explicitly set qryOwnerID and qryRecordID to form variables passed in.
2015-07-28	PYW			P-TAT006. Do not change action status in any case
Enhancement still to do:


 --->
                                            
<CFIF IsDefined("btnActionCancel")>
		<CFLOCATION URL="actionstart.cfm"addToken="false">
		<CF_ABORT>
</CFIF>

<!---   This line added by WAB with his adjustments  ---> 
	<CFPARAM NAME="TimeSpent" DEFAULT="">
	<CFPARAM NAME="CompletedDate" DEFAULT="">
	<CFPARAM NAME="CompletedTime" DEFAULT=0>
	<CFPARAM NAME="RequestedBy" DEFAULT="">
	<CFPARAM NAME="NoteStartedDate" DEFAULT="">
	<CFPARAM NAME="NoteEndedDate" DEFAULT="">
	<CFPARAM NAME="Message" DEFAULT="">
	<!--- NYB 2008-10-10 CR-TND561-bug2 - added: --->
	<CFPARAM NAME="current" DEFAULT="ActionList.cfm">
	<!--- :NYB --->
	<!--- <CFPARAM NAME="frmCurrentEntityID" DEFAULT="">
	<CFPARAM NAME="frmEntityType" DEFAULT=""> --->
	<!--- can't work out exactly why I put these next two lines here! --->
	
	<!--- NJH 2009/08/28 LID 2565
	<CFIF NoteStartedDate IS NOT ''><CFSET NoteStartedDate=NoteStartedDate+NoteStartedTime></CFIF>
	<CFIF NoteEndedDate IS NOT ''><CFSET NoteEndedDate=NoteEndedDate+NoteEndedTime></CFIF> --->
	<!--- Start 2009/09/03 LID 2565 SSS This may look a little strange but but we are entring a english date on one screen the system converts it to American so the month is actually the day MAJOR HACK--->
	<CFIF NoteStartedDate IS NOT ''>
	<cfif #day(form.NoteStartedDate)# GT 12>
		<CFSET NoteStartedDateEnglish = createdate(year(form.NoteStartedDate),month(form.NoteStartedDate),day(form.NoteStartedDate))>
	<cfelse>
		<CFSET NoteStartedDateEnglish = createdate(year(form.NoteStartedDate),day(form.NoteStartedDate),month(form.NoteStartedDate))>
	</cfif>
		<!--- START 2010/10/07	NAS			LID4280: Actions report is not showing the correct time. --->
		<CFSET NoteStartedDate=(DateFormat(NoteStartedDateEnglish) &' '& timeformat(form.NoteStartedTime))>
	
		<CFSET NoteStartedDate=(createODBCDateTime(NoteStartedDate))>
		<!--- STOP 2010/10/07	NAS			LID4280: Actions report is not showing the correct time. --->
	</CFIF> 
	<!--- <cfdump var="#NoteStartedDate#">: <cfdump var="#form.NoteStartedTime#">
	<CF_ABORT> --->
	<CFIF NoteEndedDate IS NOT ''>
	<cfif #day(form.NoteStartedDate)# GT 12>
		<CFSET NoteEndedDateEnglish = createdate(year(form.NoteEndedDate),month(form.NoteEndedDate),day(form.NoteEndedDate))>
	<cfelse>
		<CFSET NoteEndedDateEnglish = createdate(year(form.NoteEndedDate),day(form.NoteEndedDate),month(form.NoteEndedDate))>
	</cfif>
		<!--- START 2010/10/07	NAS			LID4280: Actions report is not showing the correct time. --->
		<CFSET NoteEndedDate=(DateFormat(NoteEndedDateEnglish) &' '& timeformat(form.NoteEndedTime))>
	
		<CFSET NoteEndedDate=(createODBCDateTime(NoteEndedDate))>
		<!--- STOP 2010/10/07	NAS			LID4280: Actions report is not showing the correct time. --->
	</CFIF>
	<!--- END 2009/09/03 LID 2565 SSS This may look a little strange but but we are entring a english date on one screen the system converts it to American so the month is actually the day MAJOR HACK--->

	 

<CFIF IsDefined("btnActionSE") OR IsDefined("btnActionSC") OR IsDefined("btnActionUpdate")>
	<!--- for any UPDATE or ADD --->
				
	<!--- freeze a few variables --->

	<CFSET today = Now()>
	<CFSET qryOwnerID = Form.PersonID>
	<!--- 2015/07/28 PYW. P-TAT006. Do not change action status in any case --->
<!---	
	<CFIF qryOwnerID IS NOT #request.relayCurrentUser.personid#>
		<!--- if owner <> request.relayCurrentUser.personid
				1)	then status must be whatever status is checked
					as "assigned" in actionStatus
				 
					although this should be ensured before the form is submitted
					(via JavaScript), in necessary, overwrite form parameters
		--->
		<!--- NYB 2008-10-10 CR-TND561-bug3 - if someone other than the owner changes the Action then Status gets set to "Work in Progress". 
				PM confirmed this wasn't a problem.  Made no changes --->
		<CFQUERY NAME="getAssigned" DATASOURCE="#application.SiteDataSource#">
			SELECT actionStatusID, Status
			FROM actionStatus WHERE Assigned = 1
		</CFQUERY>
		
		<CFSET new_status = getAssigned.actionStatusID>		
		<CFSET new_status_text = getAssigned.Status>		<!--- used only for email --->
	</CFIF>
--->
	<CFPARAM NAME="new_status" DEFAULT="#Form.StatusID#">

	<!--- add hours and minutes to the deadline date --->
	<cfset form.Deadline = dateadd("n",form.Deadline_Min,dateadd("h",form.Deadline_hr,form.deadline))> 

</CFIF>


<CFIF IsDefined("btnActionSE") OR IsDefined("btnActionSC")>
	<!--- ================================== 
			Adding Action
		    2)	Add records to the actions table AND the rights table
		   =================================  ---->
	<CFTRANSACTION>
 
		<CFQUERY NAME="InsertAction" DATASOURCE="#application.SiteDataSource#">
			INSERT INTO actions (ownerPersonID,createdby,priority,ProjectID,Description,
				created,lastupdated,lastUpdatedBy,
				Deadline,Notes,actionStatusID,ActionTypeID,RequestedBy,
				TimeSpent,CompletedDate)
			VALUES (
					<cf_queryparam value="#qryOwnerID#" CFSQLTYPE="CF_SQL_INTEGER" >
					,<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >
					,<cf_queryparam value="#priority#" CFSQLTYPE="CF_SQL_INTEGER" >
					,<cf_queryparam value="#ProjectID#" CFSQLTYPE="CF_SQL_INTEGER" >
					,<cf_queryparam value="#Description#" CFSQLTYPE="CF_SQL_VARCHAR" >
					,<cf_queryparam value="#today#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
					,<cf_queryparam value="#today#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
					,<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >
					,<cf_queryparam value="#Deadline#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
					,<cf_queryparam value="#Notes#" CFSQLTYPE="CF_SQL_VARCHAR" >
					,<cf_queryparam value="#new_status#" CFSQLTYPE="CF_SQL_INTEGER" >
					,<cf_queryparam value="#ActionTypeID#" CFSQLTYPE="CF_SQL_INTEGER" >
					,<cf_queryparam value="#RequestedBy#" CFSQLTYPE="CF_SQL_VARCHAR" >
					,<CFIF TimeSpent IS NOT ""> <cf_queryparam value="#TimeSpent#" CFSQLTYPE="CF_SQL_TIMESTAMP" > <cfelse> null </cfif>
					,<CFIF CompletedDate IS NOT ""> <cf_queryparam value="#CompletedDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >+<cfif CompletedTime IS NOT ""><cf_queryparam value="#CompletedTime#" CFSQLTYPE="CF_SQL_TIMESTAMP" ><cfelse>0 </cfif><cfelse> null </cfif>
					)
		</CFQUERY>
	
		<CFQUERY NAME="GetActionID" DATASOURCE="#application.SiteDataSource#">
			SELECT ActionID
			  FROM actions
			 WHERE ownerPersonID =  <cf_queryparam value="#qryOwnerID#" CFSQLTYPE="CF_SQL_INTEGER" > 
			   AND createdby = <cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="CF_SQL_INTEGER" > 
			   AND priority = <cf_queryparam value="#priority#" CFSQLTYPE="CF_SQL_INTEGER" > 
			   AND ProjectID =  <cf_queryparam value="#ProjectID#" CFSQLTYPE="CF_SQL_INTEGER" > 
			   AND Description =  <cf_queryparam value="#Description#" CFSQLTYPE="CF_SQL_VARCHAR" > 
			   AND created =  <cf_queryparam value="#today#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
			   AND Deadline =  <cf_queryparam value="#Deadline#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
			   AND actionStatusID =  <cf_queryparam value="#new_status#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</CFQUERY>
	
		
		<CFIF GetActionID.RecordCount IS 1>
		
			<CFSET qryEntity = "actions">
			<CFSET qryRecordID = GetActionID.ActionID>
	
			<CFINCLUDE TEMPLATE="actionsavenote.cfm">
			<CFSET Message = Message & "Action "& qryrecordid &" added">		
	
			<cfset thisEntityID = GetActionID.ActionID>
			<CF_UGRecordManagerTask NewEntityID=#GetActionID.ActionID#>
			
			<CFIF Form.PersonID IS NOT #request.relayCurrentUser.personid#>
				
				<!--- email owner of new action assigned to him/her --->
				<cfset actionID = qryRecordID>
				<CFINCLUDE TEMPLATE="ActionEmail.cfm">			
			</CFIF>
	
		</CFIF>			

	</CFTRANSACTION>		
	
	<CFIF IsDefined("btnActionSE")>
		<CFLOCATION URL="actionstart.cfm"addToken="false">
		<CF_ABORT>
	</CFIF>
</CFIF>

<CFIF IsDefined("btnActionUpdate")>
	<!--- ================================== 
			Updating Action

		    1)	Add records to the actions table
			2)  Only change permissions if EITHER
				a) Form.Permission exists AND IS NOT ""
				b) If Owner changed 
		   =================================  ---->
		   
	<!--- define some variables used by the included templates --->
	<CFSET qryEntity = "actions">
	<!--- START: 2014-03-07	AXA	Case 439072: explicitly ste qryOwnerID and qryRecordID to form variables passed in. --->
	<cfif structKeyExists(form,'actionID')>
		<CFSET qryRecordID = val(trim(form.actionid)) />			   
	<cfelse>
		<CFSET qryRecordID = #ActionID# />			   
	</cfif>
	<!--- END: 2014-03-07	AXA	Case 439072: explicitly ste qryOwnerID and qryRecordID to form variables passed in. --->
	<CFTRANSACTION>
		<!---
			<CFQUERY NAME="InsertAction" DATASOURCE="#application.SiteDataSource#">
				INSERT INTO actions (PersonID,createdby,DeliverableID,Description,created,Deadline,Notes,StatusID)
				VALUES (#PersonID#,#request.relayCurrentUser.usergroupid#,#DeliverableID#,'#Description#',#today#,#Deadline#,'#Notes#',#new_status#)
			</CFQUERY>
		--->
	
		<CFQUERY NAME="OriginalAction" DATASOURCE="#application.SiteDataSource#">
			SELECT	actions.ownerPersonID,
				    actions.createdby,		
					actions.actionStatusID,
				   actionStatus.Status
			FROM actions, actionStatus
			WHERE actions.ActionID =  <cf_queryparam value="#qryRecordID#" CFSQLTYPE="CF_SQL_INTEGER" > 
			AND actions.actionStatusID = actionStatus.actionStatusID
		</CFQUERY>
	
		<CFSET old_status = OriginalAction.actionStatusID>
		<CFSET old_status_text = OriginalAction.Status>
		<CFSET qryCreatorID = OriginalAction.createdby>
		<CFSET qryOldOwnerID = OriginalAction.ownerPersonID>
	
	
	<!---	2003-09-15: modified to handle an error		  --->
	<CFIF CompletedDate IS NOT "" and CompletedTime is NOT ""> 
		<cfset CompletedDateTime = createodbcdatetime(CompletedDate+CompletedTime)>
	<cfelseif CompletedDate IS NOT "" and CompletedTime is "">
		<cfset CompletedDateTime = createodbcdatetime(#now#)>
	</cfif>
	<!--- START: 2014-03-07	AXA	Case 439072: explicitly ste qryOwnerID and qryRecordID to form variables passed in. --->
	<cfif structKeyExists(form,'personID')>
		<cfset qryOwnerID = val(trim(form.personID)) />	
	</cfif>
	<!--- END: 2014-03-07	AXA	Case 439072: explicitly ste qryOwnerID and qryRecordID to form variables passed in. --->
	<!--- <cfdump var="#form#">
	<cfoutput>#CompletedDateTime#</cfoutput>
	<CFABORT showerror="#CompletedDateTime#">  --->

		<CFQUERY NAME="UpdateAction" DATASOURCE="#application.SiteDataSource#">
			UPDATE actions
			SET ownerPersonID =  <cf_queryparam value="#val(qryOwnerID)#" CFSQLTYPE="CF_SQL_INTEGER" > ,
			    priority =  <cf_queryparam value="#priority#" CFSQLTYPE="CF_SQL_INTEGER" > ,
				ProjectID =  <cf_queryparam value="#ProjectID#" CFSQLTYPE="CF_SQL_INTEGER" > ,
			    Description =  <cf_queryparam value="#Description#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
			    Deadline =  <cf_queryparam value="#Deadline#" CFSQLTYPE="CF_SQL_TIMESTAMP" > ,
			    Notes =  <cf_queryparam value="#Notes#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
			    actionStatusID =  <cf_queryparam value="#new_status#" CFSQLTYPE="CF_SQL_INTEGER" > ,
				ActionTypeID =  <cf_queryparam value="#ActionTypeID#" CFSQLTYPE="CF_SQL_INTEGER" > ,
				lastupdated = getDate(),
				lastUpdatedBy = #request.relayCurrentUser.usergroupid#,
			    <CFIF TimeSpent IS NOT "">TimeSpent =   <cf_queryparam value="#TimeSpent#" CFSQLTYPE="CF_SQL_TIMESTAMP" >, </cfif>
			   	<CFIF isDefined("completedDateTime") and CompletedDateTime IS NOT "">CompletedDate=#CompletedDateTime#, </cfif>  
	   		    RequestedBy =  <cf_queryparam value="#RequestedBy#" CFSQLTYPE="CF_SQL_VARCHAR" > 
			WHERE actionID =  <cf_queryparam value="#val(qryRecordID)#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</CFQUERY>
	
		<CFIF form.actionNoteType IS NOT "" OR New_status is not form.oldstatusid or qryOwnerID IS NOT qryOldOwnerID>
			<CFINCLUDE TEMPLATE="actionsavenote.cfm">
		</cfif>	
	
		<!--- note because no record locking, need to 
				assume that Form.oldOwnerID = qryOldOwnerID --->
	
		<CFIF qryOwnerID IS NOT qryOldOwnerID>
			<!--- owner changed so old_owner's personal group
					should have all permissions removed
					and new owner should get all permissions --->
					
			<cfquery name="CheckNewOwnerExistingRights" datasource="#application.sitedatasource#">
					select rr.usergroupid as Usergroupid, rr.entity, rr.recordid, rr.permission, ug.personid 
					from UserGroup as ug, recordRights as rr
					where ug.UserGroupID = rr.UserGroupID
					and Entity =  <cf_queryparam value="#qryEntity#" CFSQLTYPE="CF_SQL_VARCHAR" > 
					and RecordID =  <cf_queryparam value="#qryRecordID#" CFSQLTYPE="CF_SQL_INTEGER" > 
					and PersonID =  <cf_queryparam value="#qryOwnerID#" CFSQLTYPE="CF_SQL_INTEGER" > 
					ORDER BY recordid
			</cfquery>
			
			<cfif CheckNewOwnerExistingRights.recordcount neq 0>
				<CFSET UGID = CheckNewOwnerExistingRights.Usergroupid>
				
				<cfquery name="UpdateRecordRights" datasource="#application.sitedatasource#">
						update recordrights
						set permission = 3
						where recordid =  <cf_queryparam value="#qryRecordID#" CFSQLTYPE="CF_SQL_INTEGER" > 
						and entity =  <cf_queryparam value="#qryEntity#" CFSQLTYPE="CF_SQL_VARCHAR" > 
						and usergroupid =  <cf_queryparam value="#UGID#" CFSQLTYPE="CF_SQL_INTEGER" > 
				</cfquery>
			</cfif>
			
	 		<CFQUERY NAME="InsertNewOwnerRights" dataSource="#application.siteDataSource#">
			INSERT INTO RecordRights (UserGroupID,Entity,RecordID,Permission)
					SELECT usergroup.UserGroupID,
							<cf_queryparam value="#qryEntity#" CFSQLTYPE="CF_SQL_VARCHAR" >,
							<cf_queryparam value="#qryRecordID#" CFSQLTYPE="CF_SQL_INTEGER" >,
							'3' AS Permission
					FROM usergroup
				   WHERE usergroup.PersonID =  <cf_queryparam value="#qryOwnerID#" CFSQLTYPE="CF_SQL_INTEGER" >  
				   AND NOT EXISTS (select distinct rr.UserGroupID
					FROM recordRights rr
					INNER JOIN userGroup ug ON ug.userGroupID = rr.userGroupID 
				   WHERE recordID =  <cf_queryparam value="#qryRecordID#" CFSQLTYPE="CF_SQL_INTEGER" > 
				   and entity =  <cf_queryparam value="#qryEntity#" CFSQLTYPE="CF_SQL_VARCHAR" > 
				   and ug.personid =  <cf_queryparam value="#qryOwnerID#" CFSQLTYPE="CF_SQL_INTEGER" > )
			</CFQUERY>

		
			<CFIF qryOwnerID IS NOT request.relayCurrentUser.personid>
				<!--- set actionID the call ActionEmail --->
				<cfset actionID = qryRecordID>
				<CFINCLUDE TEMPLATE="ActionEmail.cfm">		
			</CFIF>
			
		</CFIF>
	
		<CFIF qryOwnerID IS NOT qryCreatorID AND new_status IS NOT old_status and qryCreatorID neq "404">
			<!--- status has been changed so email action creator if
				  different from owner ---->
			<!----2012-03-20 - RMB - KER002 - Added cfif to stop email when updating your own actions--->
			<CFIF qryOwnerID IS NOT request.relayCurrentUser.personid>
			
			<CFINCLUDE TEMPLATE="ActionEmailStatus.cfm">
	
		</CFIF>
	
		</CFIF>
	
	
<!--- 	
		<CFIF IsDefined("Form.Permission")>
			<CFIF Form.Permission IS NOT "">
 --->
				<!--- permissions were changed so 
				
						delete all permission an insert new ones.
						
						NOTE:
						1) that is may reinsert the record
							added for a new owner above 
						
						2)	note that this does not really mean they
							have changed, only that they will be 
							set to something specifically defined (which could
							be what they already were
				 --->
	
				<CF_UGRecordManagerTask>
<!--- 
			</CFIF>
		</CFIF>		
 --->
	</CFTRANSACTION>		

	<!--- cannot use CFLOCATION here since we need the intial search 
			parameters passed --->
	<CFSET Message = message & "Action "&qryrecordid &" updated">

<!--- SWJ: Test added here to send back to ActionForm.cfm --->
	<CFIF btnActionUpdate IS " Update & Continue ">
		<CFPARAM NAME="ViewOnly" DEFAULT="No">
		<CFINCLUDE TEMPLATE="ActionEdit.cfm">
		<CF_ABORT>
	<CFELSE>
		<cfset queryString = "">	
		<cfloop collection="#form#" item="formItem">
			<cfif findNoCase("srch",formItem,0) gt 0>
				<cfset queryString = queryString & "&" & formItem & "=" & URLEncodedFormat( form[formItem] )>
			</cfif>
		</cfloop>
		<cfif isDefined("frmCurrentEntityID") and isDefined("frmEntityType")>
			<CFSET queryString = queryString & #IIf(frmCurrentEntityID IS NOT "",DE("&frmCurrentEntityID=" & frmCurrentEntityID),DE(""))#>
			<CFSET queryString = queryString & #IIf(frmEntityType IS NOT "",DE("&frmEntityType=" & frmEntityType),DE(""))#>
		</cfif>

		<cflocation url="ActionList.cfm?#queryString#&message=#message#" addtoken="No">
		<!--- NYB - with: ---   <-- returned to original NYB 2011-06-22 LHID4127
		<cfif current eq "ActionEdit.cfm">
			<CFSET queryString = queryString & "&ActionID=#form.ActionID#">
		</cfif>
		<cflocation url="#current#?#queryString#&message=#message#" addtoken="No">
		!--- :NYB --->
		<CF_ABORT>
	</CFIF>
</CFIF>

<CFINCLUDE TEMPLATE="PopulateLists.cfm">

<!--- define default values for new records --->
<CFSET qryDescription = "">
<CFSET qryDeadline = DateAdd('D',7,Now()) >
<CFSET qryOwnerID = "#request.relayCurrentUser.personid#">
<CFSET qryCreatorID = "#request.relayCurrentUser.usergroupid#">
<CFSET qryStatusID = "">
<CFSET qryStrandID = "">
<CFSET qryDeliverableID = "">
<CFSET qryNotes = "">
<CFSET qryRequestedBy = "">
<CFSET qryProjectID = "">
<CFSET qryTimeSpent = "">
<CFSET qryCompletedDate = "">
<CFSET qryDateEntered = "">


<!---CFSET qryPermission = ""--->
		
<CFSET page_header = "Action Entry Form">
<CFSET function = "add">


<CFINCLUDE TEMPLATE="ActionForm.cfm">

<CF_ABORT>


