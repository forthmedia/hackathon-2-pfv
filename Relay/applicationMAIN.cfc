<!--- �Relayware. All Rights Reserved 2014 --->
<cfcomponent output="false">
<!---


applicationMain.cfc

is extended by application.cfc

kept as a separate function to allow use of onrequest method when required, but allow cfcs to run without onrequet

NYB P-LEN024 2011-07-12 added the ability to pass suppressOutput as a url variable to the onRequestStart function
WAB 2011-11-15 Added enforcement of encryption based on security XML
WAB 2012-01 	Security Project - Add Session Token (and test for it)
				Add on RequestEnd processing (actually done in com.security)
WAB 2012-02-09 Moved setting of request.requestTime to constructor.  Now store a timeoffset between db and web server.  Use globalfunctions.extendRequestTimeout at beginning of onError

RMB 2012-01-12 RMB - Added 'output = "False"' on cfcomponent to remove white space for web services.
WAB 2012-03-19 Moved all script stuff which was in onRequestStart() into request.cfc, so done at end of request
WAB 2012-06-13 Major changes to security.  Testing for blind post, injection and encryption moved into testFileSecurity() function
				Also added a new parameter (hasOwnSecurity)	to the securityXML.  This allows files such as webservices\callWebervice.cfc to implement their own security
IH	2012-07-31	Case 429715 add code to rollback unfinished transactions
WAB 2012-09-20	Code to rollback unfinished transactions moved, must be after timeout has been extended
WAB 2012-09-20	delete processLocks onError
WAB 2013-10-09	Set an application wide siteDataSource - no longer need datasource attribute in CFQUERY
WAB 2014-03-17	CASE 438291 Mods relating to loadbalancer - use application.com.security.getRemoteAddress() instead of cgi.remote_addr
NJH 2014/11/04 - reinstate the deleting of process Locks in onError - see comments below
WAB 2014-12-18 CASE 443133 Changes to siteOffline code to support list of allowed IPAddress ranges
WAB 2015-06-22 FIFTEEN-384 added combineDateFormFields to doctorFormFields()
WAB 2015-11-04 Added server.locale so that we know in which format cfdirectory will return lastModified
WAB 2015-11-04 Reinstated warning messages when cfcs fail to load on dev sites and after flush on other sites (using relayUI.message)
WAB 2015-05-19 CASE 444754 support for Rotation of SessionID on Login.
WAB 2015-11-04 Added server.locale so that we know in which format cfdirectory will return lastModified
WAB 2016-01-22	Add wirebox to the system
NJH 2016/02/23 Added functionality to doctorFormFields which converts an email back to unicode if that's what it was originially. Seems to be an issue on Chrome.
WAB 2016-03-17	During 	PROD2016-118  Housekeeping Improvements. Use a new function getDBTime() to set the request time
--->

	<cftry>
		<cfif not structKeyExists (server,"serverInitialisationObj") or isdefined("url.flush")>
			<cftry>
				<cflock name="doServerInitialisation" timeout="0" throwOnTimeout="true">
					<cfset serverInitialisationObj = createObject("component","com.serverInitialisation")>
					<cfset serverInitialisationObj.initialise()>
					<cfset server.serverInitialisationObj = serverInitialisationObj>  <!--- only set server variable afterwards so that not created if there is a failure of some sort --->
					<cfset server.locale = getLocale()>
				</cflock>
				<cfcatch>
					<cfif cfcatch.type is "lock">
						<cfthrow type="ApplicationInitialising" >
					<cfelse>
						<cfrethrow>
					</cfif>
				</cfcatch>
			</cftry>
		</cfif>

		<cfset this.siteDetails = server.serverInitialisationObj.getSiteForThisDomain()>
		<cfset this.name = this.siteDetails.cfappname>
 		<cfset this.sessionManagement = true>
 		<cfset this.datasource = this.sitedetails.datasource>

		<cfset this.sessionTimeout = createTimeSpan(0,2,0,0)>
		<cfset this.mappings = structCopy(this.siteDetails.cf_mappings)>
		<cfset this.customtagpaths= this.sitedetails.paths.customtags>

		<!--- 	PROD2016-1249 Allow our cfcs to return WSDLs regardless of Axis setting in CFAdmin 
				Could have set our standard to Axis 2, but this would have required updates to underlying XML to deal with https 
		--->
		<cfset this.wssettings.version.publish = 1>
		<cfset this.wssettings.version.consume = 1>

		<cfcatch>
			<cfset errorHandlerObject = createObject ("component","com.errorHandler")>
			<cfset errorHandlerObject.handleError (exception = cfcatch)>
			<cfoutput>Boot Failure<BR></cfoutput>
			<CFABORT>
		</cfcatch>
	</cftry>

	<cfset request.requestTime = dateFormat(now(),"yyyy-mm-dd") & " " & timeFormat(now(),"HH:mm:ss")> <!--- will be overridden by a db version later --->

	<cfset request.requestTimeODBC = now()> <!--- will be overridden by a db version later --->
	<cfset request.requestID = createUUID()>

	<!--- 
	<CFQUERY NAME="TheTime" DATASOURCE="#this.sitedetails.datasource#">
	SELECT dateadd(ms,-datepart(ms,getdate()),GETDATE()) AS Now, getdate() [getdate]
	</CFQUERY>

	<cfset request.requestTime = TheTime.Now>
	<cfset request.requestTimeODBC = createODBCDateTime(request.requestTime)>
	--->

	<cffunction name="onApplicationStart" returnType="void">

		<cfsetting requesttimeout="300"> <!--- WAB 2012-10-08 added requesttimeout here - loading cfcs at boot up can take quite a long time --->
		<cfset doFlush(2)>

	</cffunction>

	<cffunction name="onApplicationEnd" returnType="void">
		<cfargument name="applicationScope">
		<cftry>

		<cfset applicationScope.com.clustermanagement.deRegisterInstance(applicationScope = applicationScope)>
		<cfset applicationScope.com.clustermanagement.updateserversInClusterVariable(applicationScope = applicationScope)>

			<cfcatch>
				<cflog file="Boot" text="Error During OnApplicationEnd #cfcatch.message#">
			</cfcatch>
		</cftry>
		<cflog file="Boot" text="Application #applicationScope.applicationName# ended">
	</cffunction>

	<cffunction name="doFlush">
		<cfargument name="flush" type="numeric" required="true">

		<cfparam name="flushDone" default = "false">
		<!--- deliberately not var'ed so will remember if doflush is called more than once (which it can be on server startup if there is a flush=1 in the URL [mainly likely on dev])--->

		<cfif not flushDone>

			<cftry>
				<cflock name="doFlush" timeout="0" throwOnTimeout="true">

					<cfset application.testsite = this.sitedetails.testsite >
					<cfset application.sitedatasource = this.sitedetails.datasource>
					<cfset application.cf_mappings = this.sitedetails.cf_mappings>  <!--- needed by com.componentUtils ---> <!--- the underscore was added so that it didn't appear in seaches for application. cfm when I was upgrading to application.cfc --->
					<cfset application.instance = this.siteDetails.instance>

					<cfset var wireboxInitProperties = {}>
					<cfif application.testsite is 2>
						<cfset wireboxInitProperties.checkSingletonsForReload = true>
					</cfif>
					<cfset var wirebox = createObject("component","wirebox.system.ioc.Injector").init(properties = wireboxInitProperties)>   <!---  --->
					<cfset wirebox.getBinder().scanLocations("code,code.com,code.cftemplates,com") >

					<cfset setPathAppVars()>

					<!--- Flushing of CFCs (flush = 1)--->

						<cfset applicationVariablesObj = createObject("component","com.ApplicationVariables")>
						<cfset applicationVariablesObj.reLoadAllVariables_BeforeCom()>  <!--- strictly only required to be done before com on first boot up --->
						<cfset loadResult = applicationVariablesObj.loadApplicationComponents (onlyIfChanged = iif(flush lt 1,true,false))>
						<!--- NJH 2015/12/21 - get connector components by new method --->
						<cfset createObject("component","com.objectFactory")>
						<!--- WAB 2009-10-05 moved locking into the cfc, so if locked give the message here --->
						<cfif loadResult.lockError>
								<cfthrow type="ApplicationInitialising" >
						</cfif>

						<cfif structKeyExists(url,"flush")>
							<!--- Message displayed later are in output = false here --->
							<cfset application.com.relayUI.setMessage (message = loadResult.message, type="warning", scope="request")>
						</cfif>

					<cfif flush gt 1>
						<!--- application variables - NJH 2011-02-21 - keep in this order as there are some application variables needed when loading settings. --->
						<cfset application.com.applicationVariables.reLoadAllVariables_AfterCom()>
						<!--- NJH 2011-02-28 P-FNL075 - moved this into reloadAllVariables as there were some variables in there that were dependant on settings being run first..
						<cfset application.com.settings.loadVariablesIntoApplicationScope ()> --->
						
						
						<cfset var successfulCompile=application.com.java.compile().isOk>
						<cfif successfulCompile>
							<!--- The call to compile should itself log any issues--->
							<cfset application.com.java.fullJavaLoadAndBoot()>
						</cfif>
					</cfif>
							

					
					

					<cfset flushDone = true>
				</cflock>

				<cfcatch>
					<cfif cfcatch.type is "lock">
						<cfthrow type="ApplicationInitialising" >
					<cfelse>
						<cfrethrow>
					</cfif>

				</cfcatch>
			</cftry>

		</cfif>


	</cffunction>

	<cffunction name="onSessionStart">

		<!--- WAB 2015-05-19 CASE 444754 support for Rotation of SessionID on Login.   --->
		<cfset application.com.request.checkForRotateSessionOnSessionStart()>

	</cffunction>


	<cffunction name="onSessionEnd" returnType="void">
		<cfargument name="sessionscope">
		<cfargument name="applicationscope">

		<!--- All functions which need to be run at the end of a session are defined in the housekeeping cfc
			The function needs to have take the arguments  sessionScope and applicationScope.
			Also needs to have the property runOnEach="sessionEnd"
		--->
		<cfset applicationScope.com.housekeeping.runSessionEndHouseKeeping(applicationscope = applicationscope,sessionScope=sessionScope)>

		<cfif structKeyExists (sessionscope,"relaycurrentUserstructure")>
			<!--- 2015-05-19 	WAB CASE 444754 Rotate Session - update sessionEndedDate based on sessionID rather than visitID --->
			<cfquery name="updateVisit" datasource = "#applicationscope.sitedatasource#">
			update visit
				set sessionEndedDate = getdate ()
			where
				jsessionID = '#sessionScope.SessionID#'
			</cfquery>
		</cfif>

	</cffunction>


	<cffunction name="onMissingTemplate" returnType="void">
		<cfargument name="scriptname" type="string" required=true/>

		<!--- The assumption must be that this is a cfm or cfc file, since IIS deals with all other missing templates (I also used to think it dealt with missing cf files as well, but apparently not)
			Possibilities are:
			i) It is a request to a stub file for a secure file, but we have now got rid of stubs
			ii) It is a request to a file in the root directory which actually is to be found in the defaultRootDirectory or content folder (like robots.txt, but a cfm file)
			iii) Something else which is an error.
		--->

		<!---  --->
		<cfif refindnocase("/content/.*\..*\.cfm",scriptname)>
				<!--- this looks like it might be a stub file
					we need to get request.relaycurrentuser into action, but needs a scriptname which won't be blocked by security

				 --->
				<cfset onrequestStart("/index.cfm")>
				<cfset fileDetails = application.com.filemanager.getFileIDFromScriptName (scriptName)>
				<cfif fileDetails.isOK>
					<cfset application.com.filemanager.manageSecuredFileSecurity(scriptName)>
					<CF_ABORT>
				<cfelse>
					<!--- need proper 404 here --->
					<cfoutput>#htmleditformat(scriptName)# not found</cfoutput>
				</cfif>

				<CF_ABORT>
		<cfelse>
			<!--- What else might it be!
				If it is looking for a file in the core directories which is not there then we could redirect to that same file in the content directory
			--->

			<cfif fileExists (application.paths.content & scriptname)>
				<cfset actualScriptPath = "\content\#scriptname#">
			<!--- NJH 2010-08-16 look in the code directory if the file is not available in content.. this is more for handling files in userfiles or scheduled task that may be looking at the old content directory, when in fact the code has now moved to 'code' --->
			<cfelseif fileExists(application.paths.code & scriptname)>
				<cfset actualScriptPath = "/code/#scriptname#">
			<cfelseif fileExists (application.paths.relay & "/defaultRootDirectory" & scriptname)>
				<cfset actualScriptPath = "/defaultRootDirectory/#scriptname#">
			<cfelse>
				<cfset actualScriptPath = 	"">
			</cfif>

			<cfif actualScriptPath is not "">
				<cfset onrequestStart(scriptName)>
				<cfinclude template="#actualScriptPath#">
			<cfelse>
				<!--- need proper 404 here --->
				<cfset application.com.errorHandler.recordRelayError_Warning (Type="Missing File",Message="Missing File: " & scriptName,addToDataStruct="")>
				<cfoutput>#htmleditformat(scriptName)# not found</cfoutput>

			</cfif>

		</cfif>


	</cffunction>


	<cffunction name="onError" returnType="void">
		<cfargument name="exception" >
		<cfargument name="eventname" >

		<cfif structKeyExists (exception,"stackTrace") and exception.stackTrace contains "coldfusion.filter.FormValidationException">
			<!--- This is a Form Validation Error, rethrow --->
			 <cfthrow object="#exception#">
			<CF_ABORT>
		</cfif>


		<!---
			NOTE - THIS MUST BE FIRST ACTION IN ONERROR
			Discovered that if the error was a request timeout then we ran out of time to process the error.
			So now I add some extra time
		 --->
		<cfif structKeyExists(application,"com") and structKeyExists(application.com,"request")>
			<cfset application.com.request.extendTimeOutIfNecessary(60)>
		</cfif>

		<!--- Both rolling back transactions and killing processLocks will in theory get done at the end of error handling by calling doOnRequestEnd() later, but we are doing them here as belt and braces --->
		<!--- Rollback any transactions created by CF_TRANSACTION --->
		<cfif structKeyExists(request,"openTransaction") and request.openTransaction is 1>
			<cfquery name="tran" datasource = "#application.sitedatasource#">
				IF @@TRANCOUNT > 0 ROLLBACK tran
			</cfquery>
		</cfif>

		<!---
		Kill any processlocks
		WAB 2012-01-? No don't do this, only on cf_abort and in error handler, otherwise we can't spawn fire and forget threads with locks in them
		NJH 2014/11/04	Re-instate this code. It doesn't look like the error handler is dealing with this and so we are left with open locks when an error occurs in code that is locked. This still won't deal with locks in threads(??)
			but will at least deal with errors in scheduled tasks, etc.
		--->
		<cfif structKeyExists(application,"com") and structKeyExists(application.com,"globalFunctions")>
			<cfset application.com.globalFunctions.deleteThisRequestsProcessLocks()>
		</cfif>

		<cfif structKeyExists(application,"com") and structKeyExists(application.com,"errorHandler")>
			<cfset errorHandlerObject = application.com.errorHandler>
		<cfelse>
					<!--- This try only during dev incase there are errors in the cfc --->
			<CFTRY>
				<cfset errorHandlerObject = createObject("component","com.errorHandler")>
				<CFCATCH>
					<CFDUMp VAR="#CFCATCH#">
					<CF_ABORT>
				</CFCATCH>
			</CFTRY>

		</cfif>

		<!---
			In all templates included using onRequest, the exception structure has an extra level
			with a key called rootcause.  We are only interested in the stuff within rootCause.
			We test for exception.name is "Event handler exception."
			Errors from webservice directory (or any template not using onRequest) don't have this extra layer. [Although they might have their own rootCause key so we can't just test for rootCause]
			--->

		<cfif exception.message is "Event handler exception." and structKeyExists (exception,"rootcause")>
			<cfset exception = exception.rootcause>
		</cfif>

		<cfset 	errorHandlerObject.handleError (exception = exception)	>

		<cftry>
			<!--- WAB 2012-05-15 Added call to request.OnrequestEnd(), because it appears as if onRequestEnd() itself is not called after onError() --->
			<cfset application.com.request.doOnRequestEnd()>
			<cfcatch>
			<!--- if there is an error at this point then we are just going to have to ignore it --->
			</cfcatch>
		</cftry>

	</cffunction>


	<cffunction name="setRequestTimeVariables" returnType="void" output="false">
		
		<!--- WAB BF-722 Rounding errors in SQL datetime causing issues (especially when inserting something and then searching for it by date)
				So make sure that requestTimeODBC does not have any milliseconds attached to it
		 --->
		<cfset var requestTimeODBCWithMS = application.com.dateFunctions.getDBTime()>
		<cfset request.requestTimeODBC = dateAdd ("l", - datepart("l", requestTimeODBCWithMS), requestTimeODBCWithMS)>
		<cfset request.requestTime = dateFormat (request.requestTimeODBC,"yyyy-mm-dd") & " " & timeFormat (request.requestTimeODBC, "HH:MM:ss")>

	</cffunction>


	<cffunction name="onRequestStart" returnType="void" output="false">
		<cfargument name="scriptName" default="#cgi.script_name#">
		<cfargument name="suppressOutput" default = "false">


		<cfif isdefined("url.flush") and isnumeric (url.flush)>
			<cfset doFlush(url.flush)>
		<cfelseif application.testsite is 2 and not (script_name is "/admin/reloadapplicationstructures.cfm" and QUERY_STRING contains "structure=ApplicationComponents")>
			<cfset loadResult = application.com.applicationVariables.loadApplicationComponents (onlyIfChanged = true, verbose=false)>
			<cfif loadResult.numberfailed>
				<!--- Message displayed later are in output = false here --->
				<cfset application.com.relayUI.setMessage (message = loadResult.message, type="error", scope="request")>
			</cfif>
		</cfif>

		<cfset setRequestTimeVariables()>

		

		<!--- WAB 2013-05-02 Moved make_cgi_query_String_safe up before decryptformstate, request.query_string now generated from URL structure, so must be done before url.formState is decrypted --->
		<!--- NJH 2013-09-04 Case 436757 - moved form decryption before setting of current user --->
		<cfset application.com.security.make_cgi_query_String_safe()>
		<cfset request.script_name = cgi.script_name>

		<!--- WAB 2009-07-14 automatically decrypt the formstate variables if they exist, will pass through this tag if they don't exist, but have added an if anyway
				can be either in form or url scope (or both)
		--->
		<cf_decryptFormStateString>

		<!--- Refresh the current user structure --->
		<cfset application.com.relayCurrentUser.refresh()>
		<cfset application.com.request.initialiseDocumentVars()>

		<cf_include template="/code/cftemplates/relayINI.cfm">

		<!--- WAB 2012-09-20  Run function to deal with LocaleSpecific Number Fields --->
		<cfset doctorformfields()> 	<!--- 2012-09-25 PPB LSNUMERIC INPUT CHANGES TEMPORARILY REVERTED --->

		<!--- WAB 2016-11-20  FormRenderer - General code to process anything --->
		<cfif isdefined("form") and structKeyExists (form,"rwFormProcessor")> <!---GCC 2017/02/16 - added check for form scope - webservice calls don't always have fomr scope --->
			<cfloop list = "#form.rwFormProcessor#" index="process">
				<cfset RegExp = "((?:(com)\.)?(.*?))\.(.*?)\((.*?)\)"><!--- this parses a string like com.wabtest.function({xx},{y2},'name') --->
				<cfset ReFindResult = application.com.regexp.reFindAllOccurrences(RegExp,process,{"1"="fullComponentPath", "2"="com", "3"="comItem", "4"="method", "5":"arguments"})>
				<cfif arrayLen(ReFindResult)>
					<cfif ReFindResult[1].com is not "">
						<cfset com = application.com[ReFindResult[1].comItem]>
						<cfinvoke component = #com# method = "#ReFindResult[1].method#">
					</cfif>
				</cfif>

			</cfloop>
		</cfif>
		

		<!--- 2005-11-29  WAB This if put in by to allow us to not output stuff to the HTML Head
			 2008-05-07  WAB rejigged, also looks for openAsExcel
			 2009-07-14  Added test for returnformat is JSON, useful when webservices not in webservices directory (mainly duri
		--->


		<!--- 	To take a site offline (i.e. internal or external) edit this in siteDef table (or via UI)
				WAB 2014-12-18 CASE 443133 improvements to allow setting list of IPAddresses which can by-pass the siteoffline message
				Relayware address and 127.0.0.1 are always allowed through, others can be set in sitedef table
		--->
		<cfset ShowSiteToTheseIPsRegExp = listAppend(application.com.settings.getSetting("relaywareInc.breakoutRegexp"),"127\.0\.0\.1","|")>
		<CFIF 	request.currentSite.siteOffline
				and reFindNoCase(ShowSiteToTheseIPsRegExp,application.com.security.getRemoteAddress()) eq 0
				and not application.com.security.checkIPAddressInRangeCIDR(application.com.security.getRemoteAddress(),request.currentsite.siteofflineAllowedIPs)
		>
				<!--- reload sitedefs (in case have been unable to access the site to switch it back on and had to update in database! --->
				<cfset application.com.relayCurrentSite.loadSiteDefinitions()>
				<cfset siteSpecificFile = "/code/cftemplates/siteOffline.cfm">
				<cf_include template="#siteSpecificFile#" checkIFExists="true" onNotExistsTemplate="\templates\siteoffline.cfm">
				<CF_ABORT>
		</CFIF>



		<!--- P_FNL017 GCC 2006-05-31 sets up template for handling server side validation errors --->
		<cferror type="VALIDATION" template="/errorHandler/errorhandler_validation.cfm">





			<!--- START: AJC 2008-08-11 P-SOP003 Req705 - Internal NTLM Autologin
				2009-06-23 WAB FNL 069 Moved from index.cfm
				2009-08-06 GCC Added correct checks and ensured that it is only applied to INTERNAL users...
			--->
		<cfparam name="request.internalAutoLogin" default="false">

		<cfif request.internalAutoLogin and not request.relaycurrentUser.isLoggedIn and request.relaycurrentuser.isinternal>
			<!--- 2009-09-02 GCC LID 2592 Must allow non-cf output here--->
			<CFSETTING enablecfoutputonly="NO">
			<cf_include template="/code/cftemplates/internalAutoLogin.cfm" checkIfExists="true">
			<CFSETTING enablecfoutputonly="YES">
		</cfif>


			<!--- ****************
				WAB 2009/06 FNL 069 RACE INTEGRATION START
				**************** --->

			<!--- WAB 2012-06-13 moved some security tests from here into testFileSecurity() --->
			<cfset securityObj = application.com.security>
			<cfset securityResult = securityObj.testFileSecurity(scriptname = scriptname)>

			<cfif not securityResult.isOK>
				<!---
				This section replicates most of the code in templates\applicationdirectorysecurity.cmf to decide what to do with various failure situations
				 --->

				<cfif securityResult.failureCode is "LoginRequired" or securityResult.failureCode is "FullLoginRequired" >
					<!---
					WAB I would like to be able to return the correct status Code, but can't until can figure out IIS
					<cfheader statusCode = "401"   statusText = "Access Forbidden">
					--->
					<cfif request.relaycurrentuser.isinternal>
						<!--- Internal Site --->
						<cfif request.relaycurrentuser.isLoggedInExpired>  <!--- isLoggedInExpired will be true if there has been a relay login since the browser was opened.  In this case we use the popup login --->
							<cfset message="Your session has expired.  Please login again.">
							<cfinclude template="login.cfm">
						<cfelse>
							<cfinclude template = "index.cfm">
						</cfif>
					<cfelse>
						<!--- WAB 2012-02-05 P-XI / CRM433659 --->
						<cfif isDefined("returnFormat") and returnFormat is "plain">
							<cfoutput>Login Required
							<script>window.location.href = window.location.href</script>
							</cfoutput>
						<cfelse>
						<!--- External Site --->
						<CFSETTING enablecfoutputonly="NO">
						<cfinclude template = "et.cfm">	<!--- will end up at the logged out home page of the portal --->
						<!--- <cfdump var="#securityResult#">				 --->
						</cfif>
					</cfif>
					<CF_ABORT>

				<cfelse>

					<cfif request.relaycurrentuser.isinternal and securityResult.failureCode is "InsufficientRights">
						<cfoutput><p>Sorry, but you do not have permission to access this function.</p></cfoutput>
					<cfelse>
						<!--- <cfheader statusCode = "403"   statusText = "Access Forbidden">  --->
						<cfoutput>#securityResult.message#<br /></cfoutput>
					</cfif>

					<!--- debugging on test sites --->
					<cfif application.testSite is 2 or (application.testSite is 1 and structKeyExists(url,"debug") and url.debug)>
						<cfif securityResult.failurecode is not "LoginRequired">
							<cfoutput>
								This dump only shown on Dev Sites and Staging Sites with ?debug=true.<br />
							</cfoutput>
							<cfdump var="#securityResult#">
						</cfif>
					</cfif>


				</cfif>
				<!--- vvvvvvvvv THIS IS THE CRITICAL ABORT STATEMENT DO NOT REMOVE --->
					  <CF_ABORT>
				<!--- ^^^^^^^^^ THIS IS THE CRITICAL ABORT STATEMENT DO NOT REMOVE --->
			<cfelse>
				<!--- We have passed the security, we now include any initialisation files and set a checkPermission query if required --->
				<cf_doCheckPermissionAndIncludes securityResultStructure = #securityResult#>
			</cfif>

			<!--- ****************
				WAB 2009/06 RACE INTEGRATION END
				**************** --->

			<cfif securityResult.nodebug>
				<cf_NoDebug>
			</cfif>


	</cffunction>

	<cffunction name ="onrequestend"> <!--- Note, has to have output=true so that generatedcontent can be updated --->

		<cfset application.com.request.doOnRequestEnd()>

		<!--- WAB 2011-02-01
				 considering having a function here which saves the generated content to relaycurrentuser
				This would allow us to track down Javascript errors more easily because when the error came we would be able to find actual line it appeared at
				would require each page request to be given a unique id written as a JS variable
				it would use getPageContext().getCFOutput().getBuffer()
		--->
		<!--- Do any housekeeping tasks --->
		<cfset application.com.housekeeping.checkHouseKeepingScheduledTask()>

	</cffunction>



	<cffunction name="setPathAppVars">
		<!--- for backwards compatibility --->
		<cfset application.path = this.sitedetails.paths["relay"]>
		<cfset application.userFilesAbsolutePath = this.sitedetails.paths["userfiles"] & "\">
		<!--- <cfset application. UserFilesSecurePath = this.sitedetails.paths["secure"]> now completely replaced--->
		<cfset application.userfilespath = ""><!--- not needed now, /content mapping  so cfinclude template"/content/..."> will still work --->

		<cfset application.paths=this.sitedetails.paths>

	</cffunction>


	<!---
	WAB 2012-09-20
	Code to support Locale Specific Numeric Input Boxes
	Look for field of form WXYZ_lsNumeric and then lsNumberFormat the field WXYZ
	Could be expanded to do other similar things in future (such as combining date fields)

	NJH 2013/02/19 - implement the combining of date fields
	NJH 2014/0425 - pass through the scope through to combineDateAndTimeFormFields function
	WAB 2015-06-22 FIFTEEN-384 Add time support to date flags, moved calls to combineDateFormFields to here
	 --->
	<cffunction name="doctorFormFields" returns="void">
			<!--- Code for dealing with LocaleSpecific Numbers using hidden fields --->
			<cfloop index = "scope" list = "url,form">
				<cfif isdefined(scope)>
					<cfset theCollection = evaluate(scope)>
					<cfloop item="field" collection="#theCollection#">

						<!---
						<cfif field contains  "_LSNumeric">
							<cfset otherField = replaceNoCase(field,"_LSNumeric","")>
							<cfif structKeyExists (thecollection,otherField) and thecollection[otherField] is not "">
								<cfset thecollection[otherField] = lsparsenumber(thecollection[otherField])>
							</cfif>
						</cfif>
						--->

						<cfif refindnocase ("Year\Z",field)>
							<cfset BaseField = reReplaceNoCase(field,"Year\Z","")>
							<cfset theCollection[BaseField] = application.com.dateFunctions.combineDateFormFields(baseVariableName = BaseField, scope = theCollection)>
							<cfset theCollection[BaseField] = application.com.dateFunctions.combineDateAndTimeFormFields(baseVariableName=baseField,scope=theCollection)>
						</cfif>

						<cfif refindnocase ("_(Euro)?Date\Z",field)>
							<cfset BaseField = reReplaceNoCase(field,"_(Euro)?Date\Z","")>
							<cfif structKeyExists (thecollection,BaseField) and theCollection[BaseField] is not "">
								<cfset theCollection[BaseField] = application.com.dateFunctions.combineDateAndTimeFormFields(baseVariableName=baseField,scope=theCollection)>
							</cfif>
						</cfif>

						<!--- NJH 2016/02/23 case when browser converts email domain with double byte characters into single byte characters. Seems to be specific to Chrome. If that happens, we want to convert the email address back to unicode --->
						<cfif findNoCase("email",field)>
							<cfset theCollection[field] = application.com.email.convertEmailToUnicode(email=theCollection[field])>
						</cfif>
					</cfloop>
				</cfif>
			</cfloop>
	</cffunction>

</cfcomponent>
