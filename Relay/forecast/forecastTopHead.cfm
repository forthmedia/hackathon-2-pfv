<!--- �Relayware. All Rights Reserved 2014 --->
<CFPARAM NAME="current" TYPE="string" DEFAULT="forecastList.cfm">

<CF_RelayNavMenu pageTitle="Forecast" thisDir="#thisDir#">
	<CFIF findnocase("forecastList.cfm",SCRIPT_NAME) gt 0>
		<CF_RelayNavMenuItem MenuItemText="New Forecast" CFTemplate="forecastList.cfm?action=newForecast">
	</CFIF> 
	<CFIF findnocase("forecastDetailList.cfm",SCRIPT_NAME) gt 0>
		<CF_RelayNavMenuItem MenuItemText="Populate Forecast" CFTemplate="forecastDetailList.cfm?action=populate&forecastID=#forecastID#">
		<CF_RelayNavMenuItem MenuItemText="Load Run Rate Data" CFTemplate="forecastDetailList.cfm?action=runRate&forecastID=#forecastID#">
	</CFIF> 

</CF_RelayNavMenu>