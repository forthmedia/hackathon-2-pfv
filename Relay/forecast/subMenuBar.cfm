<!--- �Relayware. All Rights Reserved 2014 --->
<CFPARAM NAME="current" TYPE="string" DEFAULT="profileEntityList.cfm">

<CF_RelayNavMenu pageTitle="Profile Manager" thisDir="#thisDir#">
	<CFIF findNoCase("subMenuBar.cfm",SCRIPT_NAME,1) >
		<CF_RelayNavMenuUpper MenuItemText="Define Profile" CFTemplate="profileEntityList.cfm" target="mainSub"  current="#current#">
		<CF_RelayNavMenuUpper MenuItemText="Analyse Profile" CFTemplate="../report/reportFrame.cfm" target="mainSub"  current="#current#">
	</CFIF>
	<CFIF findNoCase("profileList.cfm",SCRIPT_NAME,1) >
		<CF_RelayNavMenuItem MenuItemText="Back" CFTemplate="JavaScript:window.history.back();" target="mainSub">
		<CFIF checkPermission.AddOkay GT 0>
			<CF_RelayNavMenuItem MenuItemText="New Profile" CFTemplate="JavaScript:addProfile()" target="mainSub">
		</CFIF>	
	</CFIF> 
	<CFIF findNoCase("profileDetails.cfm",SCRIPT_NAME,1)>
		<CF_RelayNavMenuItem MenuItemText="Back" CFTemplate="profileList.cfm" target="mainSub">
		<CFIF checkPermission.AddOkay GT 0 and checkEdit.RecordCount IS NOT 0 and getFlagGroup.FlagTypeID IS NOT 1>
			<CF_RelayNavMenuItem MenuItemText="Add New Profile Attribute" CFTemplate="JavaScript:addFlag()" target="mainSub">
		</CFIF>	
	</CFIF> 
	<CFIF findNoCase("profileEdit.cfm",SCRIPT_NAME,1) >
		<CF_RelayNavMenuItem MenuItemText="Back" CFTemplate="JavaScript:window.history.back();" target="mainSub">
		<CF_RelayNavMenuItem MenuItemText="Profile List" CFTemplate="profileList.cfm" target="mainSub">
	</CFIF> 
	<CFIF findNoCase("FlagGroupList.cfm",SCRIPT_NAME,1) or findNoCase("FlagGroupCounts.cfm",SCRIPT_NAME,1)>
		<CF_RelayNavMenuItem MenuItemText="Back" CFTemplate="javascript:window.history.back()" target="mainSub">
		<CF_RelayNavMenuItem MenuItemText="Organisation Profiles" CFTemplate="FlagGroupList.cfm?frmEntityTypeID=2" target="mainSub">
		<CF_RelayNavMenuItem MenuItemText="Location Profiles" CFTemplate="FlagGroupList.cfm?frmEntityTypeID=1" target="mainSub">
		<CF_RelayNavMenuItem MenuItemText="Person Profiles" CFTemplate="FlagGroupList.cfm?frmEntityTypeID=0" target="mainSub">
	</CFIF> 
	<CFIF findNoCase("flagEdit.cfm",SCRIPT_NAME,1) >
		<CF_RelayNavMenuItem MenuItemText="Back" CFTemplate="JavaScript:window.history.back();" target="mainSub">
		<CF_RelayNavMenuItem MenuItemText="Profile List" CFTemplate="profileList.cfm" target="mainSub">
	</CFIF> 

</CF_RelayNavMenu>

