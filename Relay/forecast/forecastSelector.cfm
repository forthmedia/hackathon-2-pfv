<!--- �Relayware. All Rights Reserved 2014 --->



<cf_head>
<cf_title>Forecast Selector</cf_title>

<!--- --------------------------------------------------------------------- --->
<!--- example of excluding date filters which do not fall within a required range, see templates/filterDate.cfm --->
<!--- --------------------------------------------------------------------- --->
<cfquery name="qDateRange" datasource="#application["siteDataSource"]#">
	SELECT
		min( ship_month ) AS queryRangeStartMonth,
		max( ship_month ) AS queryRangeEndMonth
	FROM vForecastDetail
</cfquery>
<cfif qDateRange.RecordCount neq 0>
	<cfset queryRangeStartDateTime = CreateDate( Year( now() ), qDateRange.queryRangeStartMonth, 1 )>
	<cfset queryRangeEndDateTime = DateAdd( "d", -1, DateAdd( "m", 1, CreateDate( Year( now() ), qDateRange.queryRangeEndMonth, 1 ) ) )>
</cfif>
<!--- --------------------------------------------------------------------- --->

<SCRIPT type="text/javascript">
<!--
// copy the checked country values and product groups into the main form
function eventForecastFormOnSubmit( callerObject )
{
  checkboxMigration( document.forms["mainForm"]["insCountryID"], document.forms["frmForecast"]["frmRegionFilterList"] )
  checkboxMigration( document.forms["frmProductGroup"]["frmProductGroupID"], document.forms["frmForecast"]["frmProductGroupFilterList"] )
  if ( (document.forms["frmDateRangeForm"]) )
  {
    document.forms["frmForecast"]["frmRangeStartTimeStamp"].value = document.forms["frmDateRangeForm"]["frmRangeStartTimeStamp"].value;
    document.forms["frmForecast"]["frmRangeEndTimeStamp"].value = document.forms["frmDateRangeForm"]["frmRangeEndTimeStamp"].value;
// in addition to TimeStamp there are Year, Month, Day, Hour, Minute and Second, if required for validation or to be passed through
  }
  return true;
}

function checkboxMigration( sourceCheckbox, targetField )
{
  var checkboxIndex = 0;
  var listDelimiter = "";

  if ( !(sourceCheckbox) ) return false;
  
  if ( (sourceCheckbox.checked) )
  {
    targetField.value = sourceCheckbox.value;
  }
  else
  {
    targetField.value = "";
    for ( checkboxIndex = 0; checkboxIndex < sourceCheckbox.length; checkboxIndex++ )
    {
      if ( sourceCheckbox[checkboxIndex].checked )
      {
        targetField.value += listDelimiter;
        targetField.value += sourceCheckbox[checkboxIndex].value;
        listDelimiter = ",";
      }
    }
  }

  return true;
}

function eventForecastIDOnChange( callerObject )
{
  callerObject.form.submit();
  return true;
}

function toggleVisibility( divName, callerObject )
{
  document.all[divName].style.display = ( document.all[divName].style.display == "" ) ? "none" : "";
  if ( callerObject )
  {
//    callerObject.value = ( document.all[divName].style.display == "" ) ? String.fromCharCode( 171 ) : String.fromCharCode( 187 );
//    callerObject.blur();
    callerObject.src = ( document.all[divName].style.display == "" ) ? iconDoubleArrowUp.src : iconDoubleArrowDown.src;
  }
  return true;
}

function frameRefresh( callerObject )
{
  with ( document.forms["frmForecastSwitch"] )
  {
    submit();
  }
}

var iconDoubleArrowUp = new Image();
var iconDoubleArrowDown = new Image();
function eventWindowOnLoad( callerObject )
{
  iconDoubleArrowUp.src = "<cfoutput></cfoutput>/images/MISC/iconDoubleArrowUp.gif";
  iconDoubleArrowDown.src = "<cfoutput></cfoutput>/images/MISC/iconDoubleArrowDown.gif";
}
//-->
</script>

</cf_head>

<cf_body onload="eventWindowOnLoad(this);">

<cfparam name="entityID" type="numeric" default="0">
<cfparam name="frm_vendoraccountmanagerpersonid" type="numeric" default="0">
<cfparam name="form.forecastID" type="string" default="0">

<cfscript>
// create an instance of the userFiles relayForecast component
componentPath = "content.CFTemplates.relayForecast";
myForecast = createObject("component",componentPath);
myForecast.dataSource = application.siteDataSource;
myForecast.entityID = entityID;
// myopportunity.liveStatusList = liveStatusList;
myForecast.vendorAccountManagerPersonID = frm_vendoraccountmanagerpersonid;
// if form.sortorder is defined use it otherwise set it a default value
if (isdefined("form.sortOrder") and form.sortOrder neq "") {
	myForecast.sortOrder = form.sortOrder;} 
else {myForecast.sortOrder = "account";}
if (isdefined("FilterSelect") and FilterSelect neq ""
	and isdefined("FilterSelectValues") and FilterSelectValues neq "") {
	myForecast.FilterSelect = FilterSelect;
	myForecast.FilterSelectValues = FilterSelectValues;
	}
if (isdefined("radioFilterName") and radioFilterName neq "") {
	if (isDefined("form.radioFilterName")){
		myForecast.radioFilterName = radioFilterName;
		myForecast.radioFilterValue = form.radioFilterName;} 
	else {
		myForecast.radioFilterName = radioFilterName;
		myForecast.radioFilterName = radioFilterDefault;} 
	}
if (isdefined("FORM.showComplete") and FORM.showComplete eq "1") {
	myopportunity.showComplete = "1";
	} 
	else { myopportunity.showComplete = "0"; }
myForecast.getForecastLists();

</cfscript>

<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
	<TR>
		<TD ALIGN="LEFT" CLASS="SubMenu">Forecast selection</TD>
	</TR>
</TABLE>

<table width="100%" border="0" cellspacing="0" cellpadding="3" align="center" class="withBorder">
	<tr>
		<td colspan="2">
			<form method="post" name="frmForecastSwitch" action="<cfoutput>#cgi["script_name"]#</cfoutput>" target="_self">
				<select name="forecastID" onChange="eventForecastIDOnChange(this);">
					<cfloop query="myForecast.qGetForecastLists">
						<cfif form["forecastID"] eq 0>
							<cfset form["forecastID"] = myForecast.qGetForecastLists.forecastid>
						</cfif>
						<option value="<cfoutput>#myForecast.qGetForecastLists.forecastid#</cfoutput>"<cfoutput>#iif( myForecast.qGetForecastLists.forecastid eq form["forecastID"], de( " selected" ), de( "" ) )#</cfoutput>><cfoutput>#myForecast.qGetForecastLists.period# #myForecast.qGetForecastLists.title#</cfoutput></option>
					</cfloop>
				</select>
			</form>
		</td>
	</tr>
	<cfscript>
	// delay until a default forecastid is known
	myForecast.forecastID = form["forecastID"];
	myForecast.getProductGroupData();
	</cfscript>
	<tr>
		<td colspan="2">
			<form method="post" name="frmForecast" action="forecastDetailList.cfm" target="forecastDetail" onSubmit="return eventForecastFormOnSubmit(this);">
				<CF_INPUT type="hidden" name="forecastID" value="#form["forecastID"]#">
				<input type="hidden" name="frmRegionFilterList" value="">
				<input type="hidden" name="frmProductGroupFilterList" value="">
				<input type="hidden" name="frmRangeStartTimeStamp" value="">
				<input type="hidden" name="frmRangeEndTimeStamp" value="">
				<input type="submit" name="frmsubmit" value="Get Forecast data">
			</form>
		</td>
	</tr>
	<tr>
		<th align="left"><img src="<cfoutput></cfoutput>/images/MISC/iconDoubleArrowDown.gif" alt="" width="16" height="15" border="0" onClick="toggleVisibility( 'div-product', this );">
<!--- 
		<input type="button" name="frmExpandCollapse" value="&raquo;" onClick="toggleVisibility( 'div-product', this );" style="font-weight: bold;"></th>
 --->
		<th>Filter by Product Group</th>
	</tr>
	<tr>
		<td colspan="2">
			<div id="div-product" style="display: none;">
				<cfinclude template="/templates/filterProductGroup.cfm">
			</div>
		</td>
	</tr>
	<tr>
		<th align="left"><img src="<cfoutput></cfoutput>/images/MISC/iconDoubleArrowDown.gif" alt="" width="16" height="15" border="0" onClick="toggleVisibility( 'div-region', this );">
<!--- 
		<input type="button" name="frmExpandCollapse" value="&raquo;" onClick="toggleVisibility( 'div-region', this );" style="font-weight: bold;"></th>
 --->
		<th>Filter by Region/Country</th>
	</tr>
	<tr>
		<td colspan="2">
			<div id="div-region" style="display: none;">
				<cfinclude template="/templates/filterRegion.cfm">
			</div>
		</td>
	</tr>
	<tr>
		<th align="left"><img src="<cfoutput></cfoutput>/images/MISC/iconDoubleArrowDown.gif" alt="" width="16" height="15" border="0" onClick="toggleVisibility( 'div-date', this );">
		<th>Filter by date range</th>
	</tr>
	<tr>
		<td colspan="2">
			<div id="div-date" style="display: none;">
				<cfinclude template="/templates/filterDate.cfm">
			</div>
		</td>
	</tr>
</table>

<!--- 
<cfdump var="#myForecast.qProductGroupData#">
<a href="forecastList.cfm" target="forecastDetail">detail</a>
 --->


