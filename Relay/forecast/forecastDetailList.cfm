<!--- �Relayware. All Rights Reserved 2014 --->
<cfparam name="forecastID" default="1">
<!--- 
<cfparam name="url.frmRegionFilterList" type="string" default="">
<cfparam name="url.frmProductGroupFilterList" type="string" default="">

<cfparam name="url.frmRangeStartTimeStamp" type="string" default="">
<cfparam name="url.frmRangeEndTimeStamp" type="string" default="">

<cfparam name="form.frmRegionFilterList" type="string" default="#url["frmRegionFilterList"]#">
<cfparam name="form.frmProductGroupFilterList" type="string" default="#url["frmProductGroupFilterList"]#">

<cfparam name="form.frmRangeStartTimeStamp" type="string" default="#url["frmRangeStartTimeStamp"]#">
<cfparam name="form.frmRangeEndTimeStamp" type="string" default="#url["frmRangeEndTimeStamp"]#">
 --->

<cfparam name="form.frmRegionFilterList" type="string" default="">
<cfparam name="form.frmProductGroupFilterList" type="string" default="">

<cfparam name="form.frmRangeStartTimeStamp" type="string" default="">
<cfparam name="form.frmRangeEndTimeStamp" type="string" default="">

<cfparam name="url.action" type="string" default="">

<cfparam name="startrow" type="string" default="1">




<cf_head>
	<cf_title>Lead|Opportunity List</cf_title>

<SCRIPT type="text/javascript">
<!--
function eventWindowOnLoad( callerObject )
{
<cfif url["action"] eq "populate">
  if ( (parent.frames["forecastList"]) ) parent.frames["forecastList"].frameRefresh();
</cfif>
  return true;
}
//-->
</script>

</cf_head>

<cf_body onLoad="eventWindowOnLoad(this);">

<cfinclude template="forecastTopHead.cfm">

<cfparam name="sortOrder" default="forecastName">
<cfparam name="numRowsPerPage" default="20">
<!--- The keyColumn list group of params below control which columns show URL links
		where they link to and what key should be used in the URL --->
<cfparam name="keyColumnList" type="string" default="organisation"><!--- this can contain a list of columns that can be edited --->
<cfparam name="keyColumnURLList" type="string" default="../leadmanager/opportunityEdit.cfm?opportunityid="><!--- this can contain a list of matching columns that contain the URL of the editor template --->
<cfparam name="keyColumnKeyList" type="string" default="opportunityID"><!--- this can contain a list of matching key columns e.g. primary key --->

<cfparam name="radioFilterLabel" type="string" default=""><!--- this can contain a list of columns that can be edited --->
<cfparam name="radioFilterDefault" type="string" default=""><!--- this can contain a list of matching columns that contain the URL of the editor template --->
<cfparam name="radioFilterName" type="string" default=""><!--- this can contain a list of matching key columns e.g. primary key --->
<cfparam name="radioFilterValues" type="string" default=""><!--- this should contain a list of values for each radio button delimited by a minus sign --->

<cfparam name="FilterSelectFieldList" type="string" default="Organisation,Product_Code,Country,Ship_Month,SKU">

<cfparam name="dateFormat" type="string" default="last_Updated,Expected_Close_Date"><!--- this list should contain those fields you want in the date format dd-mmm-yy --->
<cfparam name="entityID" type="numeric" default="0">
<cfparam name="frm_vendoraccountmanagerpersonid" type="numeric" default="0">

<cfparam name="checkBoxFilterName" type="string" default="showComplete">
<cfparam name="checkBoxFilterLabel" type="string" default="Show Complete?">

<cfscript>
// create an instance of the opportunity component
myForecast = createObject("component","relay.com.relayForecast");
myForecast.dataSource = application.siteDataSource;
myForecast.forecastID = forecastID;

// if form.sortorder is defined use it otherwise set it a default value
if (isdefined("form.sortOrder") and form.sortOrder neq "") {
	myForecast.sortOrder = form.sortOrder;} 
else {myForecast.sortOrder = "countryID";}
if (isdefined("FilterSelect") and FilterSelect neq ""
	and isdefined("FilterSelectValues") and FilterSelectValues neq "") {
	myForecast.FilterSelect = FilterSelect;
	myForecast.FilterSelectValues = FilterSelectValues;
	}
if (isdefined("radioFilterName") and radioFilterName neq "") {
	if (isDefined("form.radioFilterName")){
		myForecast.radioFilterName = radioFilterName;
		myForecast.radioFilterValue = form.radioFilterName;} 
	else {
		myForecast.radioFilterName = radioFilterName;
		myForecast.radioFilterName = radioFilterDefault;} 
	}
if (isdefined("FORM.showComplete") and FORM.showComplete eq "1") {
	myopportunity.showComplete = "1";
	} 
	else { myopportunity.showComplete = "0"; }

switch (url["action"])
	{
	case "populate":
		myForecast.forecastID = forecastID;
		myForecast.insertCoutryForecastRecords();
		break;
	case "runRate":
		myForecast.populateRunRateData();
		break;
} //end switch

myForecast.countryFilter = form["frmRegionFilterList"];
myForecast.productGroupFilter = form["frmProductGroupFilterList"];
myForecast.rangeStartTimeStamp = form["frmRangeStartTimeStamp"];
myForecast.rangeEndTimeStamp = form["frmRangeEndTimeStamp"];
myForecast.GetForecastDetailList();
</cfscript>

<CF_tableFromQueryObject 
	passThroughVariablesStructure="#form#"
	queryObject="#myForecast.qGetForecastDetailList#"
	sortOrder = "#sortOrder#"
	numRowsPerPage="#numRowsPerPage#"
	keyColumnList="#keyColumnList#"
	keyColumnURLList="#keyColumnURLList#"
	keyColumnKeyList="#keyColumnKeyList#"
	hideTheseColumns="forecastDetailID,ID,countryID,forecastID,ProductGroupID,opportunityID"
	showTheseColumns="Organisation,SKU,Units,Ship_Month,Status,Product_Code,Country"
	dateFormat="#dateFormat#"
	FilterSelectFieldList="#FilterSelectFieldList#"
	radioFilterLabel="#radioFilterLabel#"
	radioFilterDefault="#radioFilterDefault#"
	radioFilterName="#radioFilterName#"
	radioFilterValues="#radioFilterValues#"
	checkBoxFilterName="#checkBoxFilterName#"
	checkBoxFilterLabel="#checkBoxFilterLabel#"
	allowColumnSorting="yes"
	currencyFormat="forecastRev,actualRev"
	startRow="#startRow#"
	useInclude="false"
>

<!--- 
<cfdump var="#form#">
<cfdump var="#myForecast.qGetForecastDetailList#">
 --->


