<!--- �Relayware. All Rights Reserved 2014 --->



<cf_head>
	<cf_title>Forecast List</cf_title>
</cf_head>


<cfinclude template="forecastTopHead.cfm">
<cfparam name="action" default="">
<!--- <cfinclude template="/code/cftemplates/opportunityINI.cfm"> --->

<cfparam name="sortOrder" default="forecastName">
<cfparam name="numRowsPerPage" default="20">
<!--- The keyColumn list group of params below control which columns show URL links
		where they link to and what key should be used in the URL --->
<cfparam name="keyColumnList" type="string" default="period,title"><!--- this can contain a list of columns that can be edited --->
<cfparam name="keyColumnURLList" type="string" default="../forecast/forecastFrame.cfm?forecastID=,../forecast/editForecast.cfm?forecastID="><!--- this can contain a list of matching columns that contain the URL of the editor template --->
<cfparam name="keyColumnKeyList" type="string" default="forecastID,forecastID"><!--- this can contain a list of matching key columns e.g. primary key --->

<cfparam name="radioFilterLabel" type="string" default=""><!--- this can contain a list of columns that can be edited --->
<cfparam name="radioFilterDefault" type="string" default=""><!--- this can contain a list of matching columns that contain the URL of the editor template --->
<cfparam name="radioFilterName" type="string" default=""><!--- this can contain a list of matching key columns e.g. primary key --->
<cfparam name="radioFilterValues" type="string" default=""><!--- this should contain a list of values for each radio button delimited by a minus sign --->

<cfparam name="dateFormat" type="string" default="last_Updated,Expected_Close_Date"><!--- this list should contain those fields you want in the date format dd-mmm-yy --->
<cfparam name="entityID" type="numeric" default="0">
<cfparam name="frm_vendoraccountmanagerpersonid" type="numeric" default="0">

<cfparam name="checkBoxFilterName" type="string" default="showComplete">
<cfparam name="checkBoxFilterLabel" type="string" default="Show Complete?">

<cfscript>
// create an instance of the userFiles relayForecast component
componentPath =  "content.CFTemplates.relayForecast";
myForecast = createObject("component",componentPath);
myForecast.dataSource = application.siteDataSource;
myForecast.entityID = entityID;
// myopportunity.liveStatusList = liveStatusList;
myForecast.vendorAccountManagerPersonID = frm_vendoraccountmanagerpersonid;
// if form.sortorder is defined use it otherwise set it a default value
if (isdefined("form.sortOrder") and form.sortOrder neq "") {
	myForecast.sortOrder = form.sortOrder;} 
else {myForecast.sortOrder = "account";}
if (isdefined("FilterSelect") and FilterSelect neq ""
	and isdefined("FilterSelectValues") and FilterSelectValues neq "") {
	myForecast.FilterSelect = FilterSelect;
	myForecast.FilterSelectValues = FilterSelectValues;
	}
if (isdefined("radioFilterName") and radioFilterName neq "") {
	if (isDefined("form.radioFilterName")){
		myForecast.radioFilterName = radioFilterName;
		myForecast.radioFilterValue = form.radioFilterName;} 
	else {
		myForecast.radioFilterName = radioFilterName;
		myForecast.radioFilterName = radioFilterDefault;} 
	}
if (isDefined("action") and action eq "newForecast"){
	myForecast.insertNewForecast();
	}
myForecast.getForecastLists();
//opportunityList = myForecast.qlistOpportunitiesInternal;
</cfscript>


<CF_tableFromQueryObject 
	queryObject="#myForecast.qGetForecastLists#"
	sortOrder = "#sortOrder#"
	numRowsPerPage="#numRowsPerPage#"
	keyColumnList="#keyColumnList#"
	keyColumnURLList="#keyColumnURLList#"
	keyColumnKeyList="#keyColumnKeyList#"
	hideTheseColumns="forecastID"
	dateFormat="#dateFormat#"
	FilterSelectFieldList=""
	radioFilterLabel="#radioFilterLabel#"
	radioFilterDefault="#radioFilterDefault#"
	radioFilterName="#radioFilterName#"
	radioFilterValues="#radioFilterValues#"
	checkBoxFilterName="#checkBoxFilterName#"
	checkBoxFilterLabel="#checkBoxFilterLabel#"
	allowColumnSorting="yes"
	currencyFormat="totalForecastRev,totalActualRev"
	useInclude="false"
>



