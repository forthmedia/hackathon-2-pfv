<!--- �Relayware. All Rights Reserved 2014 --->
<!---  	Template used within viewer for moving a person to another location
		Searches for locations defined in movePerson.cfm
		Also includes movePerson.cfm incase search has gone wrong
		When user selects a location, calls movePersonTask			
--->



<!--- get details of person to be moved --->
<CFQUERY NAME="Person" datasource="#application.siteDataSource#">
Select 	firstname,
		lastname,
		sitename
from 	person as p, location as l
where 	p.locationid = l.locationid
and		p.personid  in ( <cf_queryparam value="#frmMovePersonID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
</cfquery>



<!--- get list of locations matching search criteria --->
<CFQUERY NAME="getLocations" DATASOURCE="#application.SiteDataSource#" maxrows="30">
select 	* ,
		(select count(personid) from person where person.locationid = location.locationid) as people
from 	location 
where	
<CFIF srchSitename is not "">sitename like '#srchSitename#%'</cfif>
<CFIF srchLocationId is not "">locationid in (#srchLocationId#)</cfif>

</CFQUERY>

<!--- get SQL date, for checking whether records have been updated in the meantime --->
<CFQUERY NAME="getDate" datasource="#application.siteDataSource#">
select getDate() as date from dual 
</CFQUERY>

<SCRIPT>
function moveHere(locationid)  {

	form = document.movePerson

	form.frmMoveToLocationID.value = locationid	
	
	form.submit ()
		
	
}


</script>


<!--- <CFINCLUDE template="..\styles\defaultstyles.cfm"> --->

<CFOUTPUT>
<CENTER>
<!--- Display a list of matches --->
	<form action="movePersonTask.cfm" method="post" name="movePerson">
		<CF_INPUT type="hidden" name="frmMovePersonID" value="#frmMovePersonID#">
		<INPUT type="hidden" name="frmMoveToLocationID" value="">
		<CF_INPUT type="hidden" name="frmMoveDate" value="#getDate.Date#">
		<CFINCLUDE template="_formVariablesForViewer.cfm">
				
	<TABLE BORDER=0 >
		<TR>
			<TD colspan="3" align="center">
			Please select the location to move #htmleditformat(person.firstname)# #htmleditformat(person.lastname)# to
			</td>
		</tr>
		<TR>
			<TD colspan="3" align="center">
			If you have not found the correct location, please try searching again
			</td>
		</tr>
	
		<TR>
			<TD >
				<B>Site Name</B>
			</TD>
			<TD>
				<B>People</B>
			</TD>
			<TD >
				<B>Move <BR>Here</B>
			</TD>
		</TR>


		<CFLOOP QUERY="getLocations">
		<TR>
			<TD>#htmleditformat(Sitename)#</td>
			<TD>#htmleditformat(People)#</td>
			<TD><A HREF="JavaScript:moveHere('#LocationID#');"><IMG SRC="/images/MISC/greendot.gif" WIDTH=19 HEIGHT=19 BORDER=0></A></TD>		
		</tr>
		</CFLOOP>
		
	</TABLE>

	
	
</FORM>	
</center>
</cfoutput>


<CFINCLUDE template="movePerson.cfm">
	