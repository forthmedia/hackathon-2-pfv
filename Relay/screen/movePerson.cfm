<!--- �Relayware. All Rights Reserved 2014 --->
<!---  	Template used within viewer for moving a person to another location
		Asks user to enter the name/ id of the location to move the person to
		Calls movePersonList which then calls movePersonTask			
		
Ammendments required: 

1.  Needs to list current locations for this person's organisation.
2.  Needs to be clear where this is called from

--->



<CFIF not isDefined("frmCurrentEntityID") or not isDefined("frmEntityType") >
		<CFOUTPUT>This template can only be used with the viewer</cfoutput>
</cfif>

<CFIF frmEntityType is not "Person">
		<CFOUTPUT>This template can only be used with the person viewer</cfoutput>
</cfif>


<!--- <CFINCLUDE template="..\styles\defaultstyles.cfm"> --->

<CFSET frmMovePersonID = frmCurrentEntityID>

<!--- get details of person to be moved --->
<CFQUERY NAME="Person" datasource="#application.siteDataSource#">
Select 	firstname,
		lastname,
		sitename
from 	person as p, location as l
where 	p.locationid = l.locationid
and		p.personid  in ( <cf_queryparam value="#frmMovePersonID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
</cfquery>




<SCRIPT>
function jsSubmit()  {
	form = document.mainForm
	
	if (form.srchLocationId.value == '' && form.srchSitename.value == '' )  {
	
		alert ('Please enter a value in one of the input boxes')

	} else {
	
		form.submit ()
		
	}
	
	
}


</script>


<CFOUTPUT>
<CENTER>
<!--- Display a search dialogue with Sitename and LocationID --->
	<form action="movePersonList.cfm" method="post" name="mainForm">
		<CF_INPUT type="hidden" name="frmMovePersonID" value="#frmMovePersonID#">
	<!---The opening screen form--->
	<table class="Body">
		<TR>
			<TD colspan="2"  align="center">Move <CFLOOP query="person">#htmleditformat(person.firstname)# #htmleditformat(person.lastname)#, #htmleditformat(person.sitename)#<BR></cfloop></td>
		</tr>
		<tr>
			<TD colspan="2" align="center">Please enter either the name or ID of the location to move this person to</TD>
		</TR>
		<tr>
			<td>Site Name</td><td ><input type="text" name="srchSitename" size="30" maxlength="50" class="body" ></td>
		</tr>
		<tr>
			<td>Location ID</td><td ><input type="text" name="srchLocationId" size="8" maxlength="8" class="body" ></td>
		</tr>
		<tr>
			<td colspan="2" align="center"><A HREF="javascript:jsSubmit()">Continue</a></td>
		</tr>
	</TABLE>		
		<CFINCLUDE template="_formVariablesForViewer.cfm">
	
</FORM>	
</center>

</cfoutput>