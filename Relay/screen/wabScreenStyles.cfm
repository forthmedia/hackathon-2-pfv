<!--- �Relayware. All Rights Reserved 2014 --->
	<STYLE TYPE="text/css">    TR.heading  {
    	background-color : #000088;
    	font-size : 10pt;
    	color : #FFFFFF;
    	font-family : Arial;
    	font-style : italic;
    }
    
    TD.heading  {
    	background-color : #000099;
    	font-size : 10pt;
    	color : #FFFFFF;
    	font-family : Arial;
    }
    
    TABLE.screen  {
    	background-color : #FFFFCC;
    	font-size : 9pt;
    	font-family : Arial;
    }
    
    TR.screen  {
    	background-color : #FFFFCC;
    	font-size : 9pt;
    	font-family : Arial;
    }
    
        
    TD.screen  {
    	background-color : #FFFFCC;
    	font-size : 9pt;
    	font-family : Arial;
    	width : 400;
    }
    
    TD.centerbold  {
    	font-weight : bold;
    	text-align : center;
    }
    
    TD.largebold  {
    	font-weight : bold;
    	font-size : 13pt;
    }
    
    TD.largecenterbold  {
    	font-weight : bold;
    	font-size : 13pt;
    	text-align : center;
	}
	TR.body  {
    	background-color : #FFFFCC;
    	font-size : 9pt;
    	font-family : Arial;
    }
    
    TD.body  {
    	background-color : #FFFFCC;
    	font-size : 9pt;
    	font-family : Arial;
    }
    
    .required  {
    	color : #0000FF;
    }
    
    .smallfont  {
    	font-size : 8 pt;
    }
    
    Select.screen  {
    	font-size : 9 pt;
    	font-family : Arial;
    	font-weight : normal;
    	color : navy;
    }
    
    Input.screen  {
    	font-size : 8 pt;
    	font-family : sans-serif;
    	font-weight : normal;
    	color : navy;
    }
    
    .FlagGroupName  {
    	font-size : 9pt;
    	font-family : Arial;
    	line-height : 14pt;
    	color : navy;
    }
    
    .FlagGroupDescription  {
    	font-size : 9pt;
    	font-family : Arial;
    	line-height : 14pt;
    	font-style : italic;
    	color : navy;
    }
    
    TD.FlagGroup , P.FlagGroup  {
    	background-color : #FFFFcc;
    	font-size : 9pt;
    	font-family : Arial;
    	line-height : 14pt;
    	color : navy;
    }
    
    TD.FlagOption  {
    	background-color : #FFFFcc;
    	font-size : 8pt;
    	font-family : Arial;
    	color : black;
    }
    
    </style>
