<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 

2006-02-06   WAB  removed references to validvalue code in this directory and replaced with calls to customtags 

--->

<!--- has this flag record been read yet, if not run query --->
<CFIF not isdefined("#thisFieldSource#.recordcount")>
	<!--- get flag details --->
	<cfset thisFlagTextID = listgetat(thisFieldSource,2,"_")>
	<cFQUERY NAME="getParentFlagGroup" DATASOURcE="#application.SiteDataSource#">
		SELEcT fg.FlagGroupID, fg.FlagGroupTextID, fg.FlagTypeID, fg.EntityTypeID, 
		FlagID, F.Name, ft.datatable, fet.tablename
		FROM FlagGroup as fg, Flag as f, FlagType as ft, FlagEntityType as fet
		WHERE 
		<cfif isNumeric(thisFlagTextID)>
		f.FlagID =  <cf_queryparam value="#thisFlagTextID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		<cfelse>
		f.FlagTextID =  <cf_queryparam value="#thisFlagTextID#" CFSQLTYPE="CF_SQL_VARCHAR" > 
		</cfif>
		AND F.FlagGroupID = fg.FlagGroupID
		AND fg.flagtypeid = ft.flagtypeID
		AND fet.entityTypeID = fg.entityTypeID 
	</cFQUERY>
	<!--- workout thisEntity ID eg. frmOrganisationID --->
	<CFSET thisentityid = #evaluate("frm#getParentFlagGroup.tablename#id")#>
	<CFSET "#thisFieldSource#FieldList" = "">
	<CFSET "#thisFieldSource#_Parent" = getParentFlagGroup.FlagGroupTextID>
	<!--- <CF_ABORT> --->
	<!--- <cfoutput>thisFieldSource=#thisFieldSource#, thisEntityID=#thisEntityID#</cfoutput> --->
	<!--- get flagdata --->
	<cFQUERY NAME="#thisFieldSource#" DATASOURcE="#application.SiteDataSource#">					
		SELECT f.name as flagname, fd.* from 
		#getParentFlagGroup.datatable#flagdata as fd, flag as f
		where 	f.flagid = fd.flagid
		and 	f.flagid =  <cf_queryparam value="#getParentFlagGroup.flagid#" CFSQLTYPE="CF_SQL_INTEGER" > 
		<CFIF isdefined("session.flagProcessRowID")>
			and fd.id =  <cf_queryparam value="#session.flagProcessRowID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</CFIF>
		<CFIF isnumeric(thisentityid)>
			and fd.entityid =  <cf_queryparam value="#thisEntityID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		<CFELSE>
			and fd.entityid = 0 <!--- could be developed to get default values, see flag data stuff --->
		</CFIF>
	</CFQUERY>
	<CFSET "#thisFieldSource#_TableName" = getParentFlagGroup.tableName>
</cfif>



<cfset thislinedata = "">
<CFSET tableName = evaluate("#thisFieldSource#_TableName")>
<CFSET thislinedata = evaluate("#thisFieldSource#.#thisFieldtextID#")> 
<CFSET thislineorigdata = thislinedata> 
<cfif thisLineData is not "" >
	<cfset thislinedata = evaluate("#thisevalstring#") > 
</cfif>


<!--- <CFIF isdefined("#thisFieldSource#.recordcount") and evaluate("#thisFieldSource#.recordcount") gt 0>
	<CFSET thislinedata = evaluate("#thisFieldSource#.#thisfieldtextid#")>
</cfif>
 --->
<CFSET thislineformfield = "#lcase(thisFieldSource)#_#thisFieldTextID#_#thisEntityID#">

<cfif evaluate("#thisFieldSource#.recordcount") is 0>
	<!--- no current record, so check for default values --->						
	<cfif isdefined("#thisFieldSource#_#thisFieldTextID#_default")>
		<CFSET thislinedata = evaluate("#thisFieldSource#_#thisFieldTextID#_default")>
	</cfif>
</cfif>



<CFIF thislineMethod IS "Edit" AND ListFindNocase(evaluate("#thisFieldSource#FieldList"), '#thisFieldSource#_#thisFieldTextID#') IS 0 >

	<!--- add this flag to the flaglist if not already in it --->
	<CFIF listfind (evaluate("#tablename#flaglist"),#thisFieldSource# ) is 0 >
		<CFSET "#tablename#flaglist" = evaluate("#tablename#flaglist") & ",#thisFieldSource#"  >
	</cfif>

	<CFIF usevalidvalues IS  1 or lookup is 1>
<!--- 		<CFSET lookupvalue = "">
		<CFSET fieldname = "#thisFieldSource#_#thisfieldtextid#">
		<CFSET parentfieldname = "flaggroup_#evaluate("#thisFieldSource#_Parent")#_#thisfieldtextid#">
		<CFINCLUDE template="getvalidValueList.cfm">
 --->
		<cf_getValidValues validfieldname = "#thisFieldSource#_#fieldtextid#" parentvalidfieldname = "flaggroup_#evaluate("#thisFieldSource#_Parent")#_#thisfieldtextid#" shownull=false>
		<cfset "#thisfieldsource#_#fieldtextid#_validvalues" = validvalues>
		
	</CFIF>

	<CFIF not isdefined("#thisFieldSource#_#thisfieldtextid#_validvalues")> 
		<CFIF displayas is "memo">
			<CFPARAM name="cols" default="40">							
			<CFPARAM name="rows" default="3">
			<cFOUTPUT><TextArea NAME="#thislineformfield#" COLS="#cols#" ROWS="#rows#" WRAP="VIRTUAL">#HTMLEditFormat(trim(thislinedata))#</TEXTAREA></cFOUTPUT>
		<cfelseif displayas is "DateDropDowns">
			<cfif isDate(thislinedata)>
				<cfset thislinedata = createodbcdatetime(thislinedata)><!--- I do this so that the value stored in the _orig field is a timestamp - this means that I can easily to a comparison in update data --->
			</cfif>
			
			<CF_relayDateDropDowns
				currentValue="#thisLineData#"
				fieldName="#thislineformfield#"
			>
			
			<!--- even if not a required field, need to be sure that either all or none of the drop downs are selected --->
			<CFIF Required IS 0>
				<CFSET javascriptVerification = javascriptVerification & "if (numberofDateDropDownsSelected('#thisFormName#','#thislineformfield#') != 0 && numberofDateDropDownsSelected('#thisFormName#','#thislineformfield#') != 3) { msg = msg + '  enter a complete date for #requiredLabel#\n'};">
			</CFIF>


			
		<cfelseif displayas is "calendar">
			<cfif isDate(thislinedata)>
				<cfset thislinedata = createodbcdatetime(thislinedata)><!--- I do this so that the value stored in the _orig field is a timestamp - this means that I can easily to a comparison in update data --->
			</cfif>
			
			<CF_relayDateField	
					currentValue="#thisLineData#"
					thisFormName="#thisFormName#"
					fieldName="#thislineformfield#"
					anchorName="anchor1X"
					helpText=""
				>

		<CFELSE>
 			<cFOUTPUT><CF_INPUT TYPE="Text" NAME="#thislineformfield#" VALUE="#trim(thislinedata)#" SIZE="#size#" MAXLENGTH="#maxlength#"></cFOUTPUT>
		</CFIF>
		
		<cFOUTPUT><CF_INPUT TYPE="Hidden" NAME="#thislineformfield#_orig" VALUE="#trim(thisLineData)#" ></cFOUTPUT>


		<CFIF Required IS NOT 0>
			<cfif displayas is "DateDropDowns">
				<CFSET javascriptVerification = javascriptVerification & "if (numberofDateDropDownsSelected('#thisFormName#','#thislineformfield#') != 3) { msg = msg + '  phr_screen_enterAValueFor #requiredLabel#\n'};">
			<cfelse>
				<CFSET javascriptVerification = javascriptVerification & "msg = msg + verifyObjectv2(form.#thislineformfield#,'  phr_screen_enterAValueFor #requiredLabel#','#required#');" >
			</cfif>
		</CFIF>
		
		<CFIF trim(jsVerify) is not "">
			<CFSET javascriptVerification = javascriptVerification & "msg = msg + #evaluate("#jsVerify#")#;" >									
		</cfif>							
	<CFELSE>

			<cf_displayvalidvalues 
				formfieldname = #thislineformfield#
				validvalues = #evaluate('#thisfieldsource#_#fieldtextid#_validvalues')#					
				currentvalue = #thislinedata#
					parameters = #specialformatting#
				>

<!--- 		<CFSET formfield = thisLineFormField>
		<CFSET validValueList = evaluate('#thisFieldSource#_#thisfieldtextid#_validvalues')>
		<CFSET currentValue = thislinedata>
		<CFINCLUDE template="showValidValueList.cfm">						 --->
		
	</CFIF>

	<cFOUTPUT><CF_INPUT TYPE="Hidden" NAME="frm#thisFieldSource#FieldList" VALUE="#thisFieldTextID#" ></cFOUTPUT>
													
	

<cFELSEIF thislineMethod is "View"  or thislineMethod is "ViewWithHidden" >    

	<CFSET thislinedisplay = thislinedata> 
	<CFIF lookup is 1>
<!--- 		<CFSET lookupvalue = thislinedata>
		<CFSET fieldname = "#thisFieldSource#_#thisfieldtextid#">
		<CFINCLUDE template="doLookup.cfm">
 --->
			<cf_doValidValueLookup 
					validfieldname = "#thisFieldSource#.#fieldtextid#"
					lookupvalue = #thislinedata#
					countryid = #countryid#
				>	
		<CFSET thislinedisplay = lookupresult>
	</CFIF>
	<cFOUTPUT>#HTMLEditFormat(thisLineDisplay)#</cFOUTPUT>				  

	<cFIF thislineMethod is "ViewwithHidden" >    
		<cFOUTPUT>				  
		<CF_INPUT TYPE="Hidden" NAME="#thislineformfield#" VALUE="#trim(thislinedata)#">
		<CF_INPUT TYPE="Hidden" NAME="#thislineformfield#_orig" VALUE="#trim(thisLineData)#" >
		<CF_INPUT TYPE="Hidden" NAME="frm#thisFieldSource#FieldList" VALUE="#thisFieldTextID#" >
		</cFOUTPUT>				  
		<!--- add this flag to the flaglist if not already in it --->
		<CFIF listfind (evaluate("#tablename#flaglist"),#thisFieldSource# ) is 0 >
			<CFSET "#tablename#flaglist" = evaluate("#tablename#flaglist") & ",#thisFieldSource#"  >
		</cfif>
	</CFIF>


<cFELSEIF thislineMethod is "Hidden" >    
<!--- Allows parameters to be set in the background of forms
Uses the value in the evalstring column to set the value.  
(none of the other complex flags use the evalstring value--->
			<CFSET thisLineEval = evaluate(thisevalstring)>
			<cFOUTPUT>				  
			<CF_INPUT TYPE="Hidden" NAME="#thislineformfield#" VALUE="#trim(thislineEval)#">
			<CF_INPUT TYPE="Hidden" NAME="#thislineformfield#_orig" VALUE="#trim(thisLineData)#" >
			<CF_INPUT TYPE="Hidden" NAME="frm#thisFieldSource#FieldList" VALUE="#thisFieldTextID#" >
			</cFOUTPUT>				  
			<CFSET "#thisFieldSource#FieldList" = listAppend(evaluate("#thisFieldSource#FieldList"),"#thisFieldSource#_#thisFieldTextID#")>

			<!--- add this flag to the flaglist if not already in it --->
			<CFIF listfind (evaluate("#tablename#flaglist"),#thisFieldSource# ) is 0 >
				<CFSET "#tablename#flaglist" = evaluate("#tablename#flaglist") & ",#thisFieldSource#"  >
			</cfif>


<cFELSE>
	<cFOUTPUT>#HTMLEditFormat(thisLineData)#</cFOUTPUT>
</CFIF>
