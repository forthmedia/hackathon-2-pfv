<!--- �Relayware. All Rights Reserved 2014 --->
<!---	 Display dropdown of events
 	 expects either eventflagtetid or session.eventflagtextid
 	 returns eventflagtextid and session.eventflagtextid both set
	 if neither are set, then <CFABRT 

	2016-02-03	WAB Mass replace of displayValidValues using lists to use queries (may have touched unused files)
--->



<CFIF isDefined("eventFlagTextID")>
	<CFSET session.eventFlagTextID = eventFlagTextID>
<CFELSEIF isDefined("session.eventFlagTextID")>
	<CFSET eventFlagTextID = session.eventFlagTextID>
<CFELSE>
	<CFSET 	eventFlagTextID = "">
</CFIF>

<!--- currently an assumption that all event flags are person based --->	

<!--- Get list of events --->
<!--- need to take into account flag scope and eventscope (could conflict) --->

	<CFQUERY NAME="getEvents" datasource="#application.siteDataSource#">
	SELECT 	f.flagtextid as dataValue,
			f.name + ' (' + regstatus + ')' as displayValue,
			1 as sortorder1
	from 	EventDetail ed inner join Flag f on ed.flagid = f.flagid 
	inner join flagGroup fg on fg.flaggroupid = f.flaggroupid
	left outer join eventflagdata efd on f.flagid = efd.flagid
	where 	1=1
	and		efd.entityid =  <cf_queryparam value="#personid#" CFSQLTYPE="CF_SQL_INTEGER" >  

			UNION 

	SELECT 	
			f.flagtextid,
			f.name ,
			2 as sortorder1
	from 	EventDetail ed inner join Flag f on ed.flagid = f.flagid 
	inner join flagGroup fg on fg.flaggroupid = f.flaggroupid
	left outer join eventflagdata efd on f.flagid = efd.flagid
	where 	1=1 
	and		efd.entityid  is null
	order by sortorder1	
	</CFQUERY>


	<cf_displayValidValues FormFieldName="eventFlagTextID"
			DisplayAs = "select"
			validValues ="#getEvents#"
			currentvalue = "#eventFlagTextID#"
			showNull= True
			nullText = "Select an Event"
			>	
	


