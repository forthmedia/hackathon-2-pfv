<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 

peopleAtLocationInsert.cfm

Author:  WAB

Purpose:
	This file can be included in a screen definition and will display an editable list of people at a given location


Parameters:
thisLineMethod		= edit or view   (comes from screen code)
showRecords			number of records to show  (default 5)	
	
Various switches to turn on an off features (rather hard coded for specific uses, ought to be able to specify particular flags)
showPersonCommPreferences    0 (default)/ 1  - show flags for persons preferences
showLanguage				defined or not defined
showDeletionRequest			defined or not defined     shows flag

Version	 Date		By 		Description	
1
1.0		1999-04-13		WAB 	Added fields required for search by first and last name 
1.1		2001-08-12	SWJ		Modified layout so the table takes more room and edit fields 
							are easier to use/view

	2016-02-03	WAB Mass replace of displayValidValues using lists to use queries (may have touched unused files)
 --->


<CFSETTING enablecfoutputonly="yes"> 
<CFSET thisPageMethod = thislinemethod>
<CFPARAM name="showRecords" default= "5">

<CFPARAM NAME="frmPersonCheck" DEFAULT="">
<CFPARAM NAME="frmStartp" DEFAULT="1">
<CFPARAM NAME="showPersonCommPreferences" DEFAULT="0">

	<!--- define the start record --->
 	<CFIF IsDefined("FORM.PersonForward.x")>
		 <CFSET displaystart = frmStartp + Variables.ShowRecords>
 	<CFELSEIF IsDefined("FORM.PersonBack.x")>		 
		 <CFSET displaystart = frmStartp - Variables.ShowRecords>
	<CFELSEIF IsDefined("frmStartp")>
	 	 <CFSET displaystart = frmStartp>
	</CFIF>

<CFIF frmentitytype is "person">
	<CFQUERY NAME="getLocation" DATASOURCE=#application.SiteDataSource#>
		SELECT p.locationid 
		from person as p
		where p.personid =  <cf_queryparam value="#frmcurrententityid#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</CFQUERY>	

	<CFSET locationid = getlocation.locationid>
	
<CFELSEIF frmentitytype is "location">

	<CFSET locationid = #frmcurrententityid#>
	
<CFELSE>

	<CFOUTPUT> emtitytype not set </CFOUTPUT>
	<CF_ABORT>

</cfif>
		 


<CFQUERY NAME="person" DATASOURCE=#application.SiteDataSource#>
	SELECT p.PersonID,
		  p.Salutation,
		  p.FirstName,
		  p.LastName,
		  p.OfficePhone,
		  p.Email,
		  p.Language,
		  p.Emailstatus,
		  p.LastUpdated,
		  ug.Name AS LastUpdatedBy
	FROM Person AS p,
	     UserGroup AS ug
	WHERE 
	p.LocationID =  <cf_queryparam value="#locationid#" CFSQLTYPE="CF_SQL_INTEGER" > 
<!--- 	  AND l.Active <> 0 --->
	  AND p.LastUpdatedBy = ug.UserGroupID
	ORDER BY p.LastName, p.FirstName
</CFQUERY>	





<CFIF person.RecordCount GT variables.showrecords>
	
	<!--- define the start record --->
 	<CFIF IsDefined("FORM.PersonForward.x")>
		 <CFSET displaystart = frmStartp + Variables.ShowRecords>
 	<CFELSEIF IsDefined("FORM.PersonBack.x")>		 
		 <CFSET displaystart = frmStartp - Variables.ShowRecords>
	<CFELSEIF IsDefined("frmStartp")>
	 	 <CFSET displaystart = frmStartp>
	</CFIF>

</CFIF>


<CFOUTPUT>

	<cf_includejavascriptonce template = "/javascript/viewEntity.js">	
	<cf_includejavascriptonce template = "/javascript/dropDownFunctions.js">	
	<cf_includejavascriptonce template = "/javascript/checkboxFunctions.js">	

	<!--- add verification here if necessary --->
	<CFSET javascriptVerification = javascriptVerification >

<CF_INPUT TYPE="HIDDEN" NAME="frmStartp" VALUE="#variables.displayStart#">
</cfoutput> 
	
<CFIF person.RecordCount IS 0>
	<cfoutput>
	<table width="600" border="0" cellspacing="2" cellpadding="3" align="center" bgcolor="White">
	<tr>
		<td>
			No people were found at the above location.
		</td>
	</tr>
	</table>
	</cfoutput>
<CFELSE>
	<cfoutput>

		

	<table width="100%" border="0" cellspacing="0" cellpadding="1" align="center" bgcolor="White">
	<TR>
		<TH COLSPAN="7" ALIGN="left">People at this location</TH>
	</TR>
	<tr>
		<Th ALIGN="CENTER" VALIGN="BOTTOM" NOWRAP>
			Check
		</TD>
		<CFIF thisPageMethod IS NOT "edit">
			<TH VALIGN="BOTTOM">
				Name
			</TH>
		<CFELSE>
			<TH VALIGN="BOTTOM">
				Salutation
			</TH>
			<TH VALIGN="BOTTOM">
				First Name
			</TH>
			<TH VALIGN="BOTTOM">
				Last Name
			</TH>
		</cfif>
		<TH VALIGN="BOTTOM">
			Direct Line
		</TD>
		<TH VALIGN="BOTTOM">
			Email (& status)
		</TH>
		<CFIF isDefined("showLanguage")>
			<TH ALIGN="CENTER" VALIGN="BOTTOM">
				Language
			</TH>
		</cfif>
		<TH ALIGN="CENTER" VALIGN="BOTTOM">
			Edit<BR>Detail
		</TH>	
	</TR>
	</cfoutput>		
	
	<CFSET PeopleOnPage = "">	
	<CFPARAM name="PersonFlagList" default= "">	

	<CFSET endrow= min(Variables.ShowRecords+Variables.displaystart, person.recordcount)>

	<CFLOOP QUERY="person" STARTROW=#Variables.displaystart# ENDROW=#Endrow#>
		<CFSET fieldlist = "">
		<!--- gather a list of all the people displayed on this page --->
		<CFSET PeopleOnPage = ListAppend(PeopleOnPage,PersonID)>

	<CFOUTPUT>
	<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
		<TD ALIGN="CENTER" VALIGN="MIDDLE">
			<CF_INPUT TYPE="CHECKBOX" NAME="frmPersonCheck" VALUE="#PersonID#" CHECKED="#iif(ListFind(frmPersonCheck,PersonID) IS NOT 0,true,false)#"> 
		</TD>

		<TD>
			<CFIF thisPageMethod is not "edit">
				<CFIF Trim(Salutation & FirstName & LastName) IS "">
					&nbsp;
				<CFELSE>
					#Trim(Salutation & " " & FirstName & " " & LastName)#
				</CFIF>
			<CFELSE>
				<CF_INPUT TYPE="Text" Name="person_salutation_#personid#" Value="#trim(Salutation)#" SIZE="5" MAXLENGTH = "50">
				<CF_INPUT TYPE="Hidden" Name="person_salutation_#personid#_orig" Value="#trim(Salutation)#" >
				<CFSET fieldlist = listappend(fieldlist,"person_salutation")>
			</cfif>
		</TD>
		<CFIF thisPageMethod is "edit">
			<TD>
				<CF_INPUT TYPE="Text" Name="person_firstname_#personid#" Value="#trim(Firstname)#" SIZE="12" MAXLENGTH = "25">
				<CF_INPUT TYPE="Hidden" Name="person_firstname_#personid#_orig" Value="#trim(firstname)#" >
				<CFSET fieldlist = listappend(fieldlist,"person_firstname")>
			</TD>		
			<TD>
				<CF_INPUT TYPE="Text" Name="person_lastname_#personid#" Value="#trim(lastname)#" SIZE="12" MAXLENGTH = "25">
				<CF_INPUT TYPE="Hidden" Name="person_lastname_#personid#_orig" Value="#trim(lastname)#" >
				<CFSET fieldlist = listappend(fieldlist,"person_lastname")>
			</TD>		
		</CFIF>		
		<TD>
			<CFIF thisPageMethod is not "edit">
				<CFIF Trim(OfficePhone) IS "">&nbsp;<CFELSE>
				#OfficePhone#
				</CFIF>
			<CFELSE>
				<CF_INPUT TYPE="Text" Name="person_officePhone_#personid#" Value="#trim(officephone)#" SIZE="15" MAXLENGTH = "30">
				<CF_INPUT TYPE="Hidden" Name="person_officePhone_#personid#_orig" Value="#trim(officephone)#" >
				<CFSET fieldlist = listappend(fieldlist,"person_officephone")>			
			</CFIF>
		</TD>
		<TD>
			<CFIF thisPageMethod is not "edit">
				<CFIF Trim(Email) IS "">
					&nbsp;
				<CFELSE>
					#Email#
				</CFIF>
			<CFELSE>
				<CF_INPUT TYPE="Text" Name="person_email_#personid#" Value="#trim(email)#" SIZE="25" MAXLENGTH = "50">
				<CF_INPUT TYPE="HIdden" Name="person_email_#personid#_orig" Value="#trim(email)#" >
				<CFSET fieldlist = listappend(fieldlist,"person_email")>			
			</CFIF>
				<CFIF emailstatus LT 0> OK <CFELSEIF emailstatus IS 0 and trim(email) is not ""> ? <CFELSEIF emailstatus GT 0>#emailstatus# bad </cfif>	
		</TD>
		<CFIF isDefined("showLanguage")>
		<TD>
			<CFIF thisPageMethod is not "edit">
				<CFIF Trim(Language) IS "">&nbsp;<CFELSE>
				#Language#
				</CFIF>
			<CFELSE>
				
					<CFQUERY NAME="findLanguages" DATASOURCE=#application.SiteDataSource#>
					SELECT Language
					FROM Language
					ORDER BY Language
					</CFQUERY>
				
					<cf_displayValidValues 
						formFieldName = "person_language_#personid#" 
						validValues="#findLanguages#" 
						dataValueColumn = "language"
						displayValueColumn = "language"
						currentvalue="#language#"
						nullText="Choose a language"
					>	

				<CFSET fieldlist = listappend(fieldlist,"person_language")>
			
			</CFIF>
		</TD>
	</CFIF>
	<TD ALIGN="CENTER" VALIGN="MIDDLE">
		<A HREF="JavaScript:viewEntity_('Person',saveChecks(),'#personid#','','','');"><IMG SRC="/IMAGES/MISC/iconEditPencil.gif" BORDER=0></A>
	</TD>
</TR>
<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
	<TD>&nbsp;</td>
	<CFIF thisPageMethod is not "edit">
		<TD colspan="3">
	<CFELSE>
		<TD colspan="5">
	</CFIF>
	</CFOUTPUT>
		<!--- 	<FONT FACE="Arial, Chicago, Helvetica" SIZE="2"><CFSET flaggrouptextid="JobFunction"><CFSET thisentity=personid></cfoutput><CFINCLUDE TEMPLATE="../flags/listFlagGroup.cfm"><CFOUTPUT>Job Function: #result#   --->
			
			<CFSET thisfieldtextid = "JobFunction">
			<CFSET thisLineMethod = "edit">
			<CFSET specialFormatting = "">
			<CFSET maxLength = "">			
			<CFSET size = "4">			
			<CFSET required = 0>
			<CFSET displayas = "select">
			<CFSET noFlagGroupName = "1">
			<CFSET showSelectedFirst = "1">
						<cFSET jsverify = "">
<CFOUTPUT>Job Function<BR> </CFOUTPUT>
			<CFINCLUDE TEMPLATE="showScreenElement_flaggroup.cfm">


			<CFSET thisLineMethod = "edit">
			<CFSET specialFormatting = "">
			<CFSET maxLength = "">			
			<CFSET required = 0>
			<CFSET thisfieldtextid = "NoCommsTypeAll">
			<CFSET displayas = "checkbox">
			<CFINCLUDE TEMPLATE="showScreenElement_flag.cfm">

			
			<CFSET thisLineMethod = "edit">
			<CFSET specialFormatting = "">
			<CFSET maxLength = "">			
			<CFSET required = 0>
			<CFSET thisfieldtextid = "emailaddresswithheld">
			<CFSET displayas = "checkbox">
			<CFINCLUDE TEMPLATE="showScreenElement_flag.cfm">

		<CFIF isDefined("showDeletionRequest")>
			<CFSET thisLineMethod = "edit">
			<CFSET specialFormatting = "">
			<CFSET maxLength = "">			
			<CFSET required = 0>
			<CFSET thisfieldtextid = "DeletionRequest">
			<CFSET displayas = "checkbox">
			<CFINCLUDE TEMPLATE="showScreenElement_flag.cfm">
		</CFIF>


<CFIF showPersonCommPreferences is 1>
			<CFSET thisfieldtextid = "CommMediaPreference">
			<CFSET thisLineMethod = "edit">
			<CFSET specialFormatting = "">
			<CFSET maxLength = "">			
			<CFSET size = "4">			
			<CFSET required = 0>
			<CFSET displayas = "select">
			<CFSET noFlagGroupName = "1">
			<CFSET showSelectedFirst = "1">
			<CFOUTPUT><BR>Fax/Email Preference<BR> </CFOUTPUT>
			<CFINCLUDE TEMPLATE="showScreenElement_flaggroup.cfm"> 
			
			<CFSET thisfieldtextid = "CommEMailFormatPreference">
			<CFSET thisLineMethod = "edit">
			<CFSET specialFormatting = "">
			<CFSET maxLength = "">			
			<CFSET size = "4">			
			<CFSET required = 0>
			<CFSET displayas = "select">
			<CFSET noFlagGroupName = "1">
			<CFSET showSelectedFirst = "1">
			<CFOUTPUT><BR>Email Format Preference <BR></CFOUTPUT>
			<CFINCLUDE TEMPLATE="showScreenElement_flaggroup.cfm">


			<CFSET thisLineMethod = "edit">
			<CFSET specialFormatting = "">
			<CFSET maxLength = "">			
			<CFSET size = "4">			
			<CFSET required = 0>
			<CFSET nullText = "No Preference">

			<CFSET thisfieldtextid = "NoCommsTypeAll">
			<CFSET displayas = "checkbox">
			<CFINCLUDE TEMPLATE="showScreenElement_flag.cfm">

			
			
			<CFSET displayas = "radio">
			<CFSET thisfieldtextid = "SubscriptionNetAge">
			<CFINCLUDE TEMPLATE="showScreenElement_flaggroup.cfm">

			<CFSET thisfieldtextid = "SubscriptionBuyersGuide">
			<CFINCLUDE TEMPLATE="showScreenElement_flaggroup.cfm">

			<CFSET thisfieldtextid = "SubscriptionSMENewsLetter">
			<CFINCLUDE TEMPLATE="showScreenElement_flaggroup.cfm">

			<CFSET thisfieldtextid = "Subscription3Source">
			<CFINCLUDE TEMPLATE="showScreenElement_flaggroup.cfm">


</CFIF>
			
			
	<CFOUTPUT>
		</td>
		<TD>&nbsp;</td>
	</tr>
				<CFSET "caller.person_lastupdated_#personid#" = lastupdated>
	
	</cfoutput>		
	</CFLOOP>
	<cfoutput>
	</TABLE>

		<CF_INPUT TYPE="HIDDEN" NAME="frmPeopleOnPage" VALUE="#PeopleOnPage#">  <!--- this used by dropdown functions --->
		<CF_INPUT TYPE="HIDDEN" NAME="frmPeopleInReport" VALUE="#valuelist(person.personid)#">


	<CFIF isDefined("entitiesPassed") and not (entitiesPassed contains "person") >
		<!--- being called by custom tag, but custom tag isn't expecting any person fields, so set the caller variables here --->	
		<CFSET caller.personIdList	= peopleOnPage>
		<CFSET caller.personFieldList = fieldList>
		<CFSET caller.personFlagList = personFlagList>
	<CFELSEIF isDefined("entitiesPassed")  >
		<!--- being called by custom tag--->	
		<CFSET caller.personIdList = listappend(caller.personidlist,peopleonpage)>
		<CFSET caller.personFieldList = listappend(caller.personFieldList,fieldList)>
		<CFSET caller.personFlagList = listappend(caller.personFieldList,flagList)>
		
	<CFELSE>
			<CFSET personIdList = listappend(personidlist,peopleonpage)>
			<CFSET personFieldList = listappend(personFieldList,fieldList)>
	</cfif>

	
	<CFIF #person.RecordCount# GT #Variables.ShowRecords#>	
		<!--- if we only show a subset, then give next/previous arrows --->

		
	<br>
	<table width="600" border="0" cellspacing="2" cellpadding="3" align="center" bgcolor="White">	
	<TR>
		<TD>
			<CFIF #Variables.displaystart# GT 1>
				<INPUT TYPE="Image" NAME="PersonBack" SRC="/images/buttons/c_prev_e.gif" WIDTH=64 HEIGHT=21 BORDER=0 ALT="">
			</CFIF>
		</TD>
		<TD>
 			<CFIF (#Variables.displaystart# + #Variables.ShowRecords# - 1) LT #person.RecordCount#>
				Record(s) #htmleditformat(Variables.displaystart)# - #Evaluate("Variables.displaystart + Variables.ShowRecords - 1")#
			<CFELSE>
 				Record(s) #htmleditformat(Variables.displaystart)# - #person.RecordCount#
			</CFIF>
		</TD>
		<TD align='right'>
			<CFIF (#Variables.displaystart# + #Variables.ShowRecords# - 1) LT #person.RecordCount#>
				<INPUT TYPE="Image" NAME="PersonForward" SRC="/images/buttons/c_next_e.gif" WIDTH=64 HEIGHT=21 BORDER=0 ALT="Next">
			</cFIF>
		</TD>
	</TR>
	</TABLE>
</CFIF>
</cfoutput>	

	
 </CFIF> 

<CFOUTPUT>
</CENTER>


</cfoutput>

<CFSETTING enablecfoutputonly="no"> 
