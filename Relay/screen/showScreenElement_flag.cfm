<!--- �Relayware. All Rights Reserved 2014 --->
<!---
WAB 	2005-01-24   Mods to javascript
	  	2006-02-08  more mods to javascript to get translated messages  linked to a change to singleflag.cfm
WAB  	2007/11/05 pass size and maxlength to the flagcode via the specialformatting collection
WAB 	2007/01/17  changes to do with flagGroupFormatting Parameters
WAB 	2008/11/12 Bug 1327
WAB/ NJH 2009/02/26 Bug Fix All Sites Issue 1902 - set thisEntity to be variables.#getFlagGroup.entitytype.uniquekey# as when it was countryId that we
				were trying to set, countryID was always 0 as it was coming from the screenDef query.

2012-09-20 PPB Case 430402 integer flag required message
	2016-02-03	WAB Mass replace of displayValidValues using lists to use queries (may have touched unused files)
 --->


<CFSETTING enablecfoutputonly = "yes">

<!--- WAB edited this so that fieldtextid can actually be a list of flag ids  --->
<!--- could be altered to work even if from different flag groups, but currently assumes all in same group  --->


<CFSET quotedfieldTextID = replace(thisfieldtextid, "," , "','", "ALL")>

<cFQUERY NAME="getParentFlagGroup" DATASOURcE="#application.SiteDataSource#">
<!---
SELEcT distinct FlagGroup.FlagGroupID, FlagTypeID, EntityTypeID, FlagID, Flag.Name
FROM FlagGroup
--->
SELEcT distinct FlagGroupid, FlagTypeID, EntityTypeID, FlagID, Flag as Name, dataType as typeName, entityTypeID as entityType, entityTable as entityTypeName
FROM vFlagDef


WHERE
<cfif isNumeric(listfirst(thisfieldtextid))>
flagid  in ( <cf_queryparam value="#FieldTextID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
<cfelse>
FlagTextID  in ( <cf_queryparam value="#thisfieldtextid#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )
</cfif>


<CFIF listLen(thisfieldTextID)  LTE 1>AND FlagTypeID <> 3 </CFIF><!--- No single radio buttons.  WAB 28/9 relaxed this constraint if more than one flag chosen.  Very cheeky but hopefully user knows what he is doing, and we can fit a trigger to check integrity --->
</cFQUERY>

<CFIF getParentFlagGroup.Recordcount IS NOT 0>

	<CFSET flagList = "">
	<!---
	WAB 2010/09/09 replaced with fields from query
	<CFSET typeName = application. flagType[getParentFlagGroup.FlagTypeID].name>
	<CFSET entityTypeName =  application.entityType[getParentFlagGroup.EntityTypeID].tablename>
	--->
	<CFSET entityType = #getParentFlagGroup.EntityTypeID#>
	<CFSET flagmethod = thislinemethod>
	<CFSET flagformat=specialformatting>
	<!--- wab 2007/11/06 these added as part of the flagformatting project --->
	<cfif maxLength is not 0 and maxLength is not "">
		<cfset specialFormattingCollection.maxLength = maxLength>
	</cfif>
	<cfif size is not 0 and size is not "">
		<cfset specialFormattingCollection.size = size>
	</cfif>

	<CFSET flagfieldmaxlength=maxlength>
	<CFSET flagfieldsize=size>


	<!---  wab: test for special case with boolean/checkbox flag
			case of eg. a person flag where we want to display a dropdown of all people
			at this location which have that flag set
	--->

	<CFPARAM name="groupFlagBy" default="">
	<CFIF groupFlagBy is not "">
			<CFSET flagEntityTypeName = getParentFlagGroup.entityTypeName>
			<CFSET GroupByEntityTypeName = groupFlagBy>

			<!--- Find out about the group by entityType, ie what fields make up the name --->
			<cFQUERY NAME="getTableDetails" DATASOURcE="#application.SiteDataSource#">
			SELECT * from flagEntityType
			where tablename =  <cf_queryparam value="#flagEntityTypeName#" CFSQLTYPE="CF_SQL_VARCHAR" >
			</CFQUERY>

			<!--- get all entities (eg people) at this groupEntity (eg Location) --->
			<cfquery name="getEntityIds" datasource="#application.SiteDataSource#">
			select #flagEntityTypeName#id as entityid
			from #flagEntityTypeName#
			where #groupByEntityTypeName#id = #evaluate("#groupByEntityTypeName#id")#
			</CFQUERY>

			<CFIF getentityIDs.recordCount is 0>
				<CFSET EntityIDs = -1>
			<CFELSE>
				<CFSET EntityIDs = valueList(getentityIDs.entityId)>
			</cfif>



			<!--- now get all those entities which are flagged --->
			<cfquery name="getCurrentValues" datasource="#application.SiteDataSource#">select entityid
			from booleanFlagData
			where flagid =  <cf_queryparam value="#getParentFlagGroup.flagid#" CFSQLTYPE="CF_SQL_INTEGER" >
			and entityid  in ( <cf_queryparam value="#valuelist(getEntityIds.Entityid)#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			</cfquery>

			<CFIF getCurrentValues.recordCount is 0>
				<CFSET currentValues = -1>
			<CFELSE>
				<CFSET currentValues = valueList(getCurrentValues.entityId)>
			</cfif>

			<!--- now get all the names of those entities in order (with selected first) --->
			<!--- probably a better way of doing these 3 queries in one --->
			<cfquery name="getEntityNames" datasource="#application.SiteDataSource#">
			select
					#flagEntityTypeName#id as entityID, 
					#preserveSingleQuotes(getTableDetails.nameExpression)# as Name,
					#flagEntityTypeName#id,
					CASE WHEN #flagEntityTypeName#id  in ( <cf_queryparam value="#currentValues#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) THEN 1 ELSE 2 END as sortorder
			from
					#flagEntityTypeName#
			where
					#groupByEntityTypeName#id = #evaluate("#groupByEntityTypeName#id")#
					<CFIF flagMethod is not "edit">
					AND #flagEntityTypeName#id  in ( <cf_queryparam value="#currentValues#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
					</CFIF>
			order by
					 sortorder, name
			</CFQUERY>

			<CFOUTPUT>All #flagentityTypeName#(s) at this #groupByEntityTypeName# flagged as #getParentFlagGroup.name# <BR></CFOUTPUT>

			<CFIF flagMethod is "edit">
				<cf_displayValidValues
					formFieldName = "boolean_#getParentFlagGroup.flagid#_#evaluate("#GroupByEntityTypeName#id")#"
					validValues = "#getEntityNames#"
					dataValueColumn = "entityID"
					displayValueColumn = "Name"
					currentValue = "#valuelist(getCurrentValues.entityid)#"
					displayas = "multiselect"
					listsize = "5"
					nullText = " "
					inputOnNoValidValues = "false"
					 >

				<CFOUTPUT><CF_INPUT type="HIDDEn" NAME = "boolean_#getParentFlagGroup.flagid#_#evaluate("#groupByEntityTypeName#id")#_ORIG" value="#valuelist(getCurrentValues.entityid)#">						</CFOUTPUT>
				<CFSET "#groupByEntityTypeName#flagList" = evaluate("#groupByEntityTypeName#FlagList") & "," & "boolean_#getParentFlagGroup.flagid#">

			<CFELSE>
				<CFOUTPUT query="getEntityNames">
					#htmleditformat(Name)#<BR>
				</CFOUTPUT>
			</CFIF>

		<CFSET groupFlagBy="">    <!--- reset default --->
	<CFELSE>

		<CFSET thisentity = evaluate("variables.#getParentFlagGroup.entityTypeName#id")> <!--- Bug Fix All Sites Issue 1902 WAB/ NJH 2009/02/26 added "variables." - when entityTypeName is country we have to distinguish this countryid from the countryid in the encompassing screendefinition query --->

		<cFINcLUDE TEMPLATE="../flags/SingleFlag.cfm">


<!---
WAB 2008/11/12 Note
To fix another bug (1327) I moved the required code for radio and checkbox groups into the editFlagRadio and editFlagCheckBox templates
Eventually need to do other flag types as well, but in the meantime this code can live happily here because of the findnocase test

However did move the findnocase test to be around the single checkbox code as well
 --->


		<CFIF Required IS NOT 0 and thislinemethod is "edit">

			<cFLOOP LIST="#flagList#" INDEX="thisFlag">

				<!---  WAB 2004-09-29 discovered that the javascript verification was appearing twice if the flag in question was a text box with valid values (may have been other cases)
						required was already being dealt with by showvalidvaluelist.cfm
						therefore added a test here to see whether this item was already in the verification string
				--->
				<!--- 2004-10-13
					note that the message for a single checkbox isn't really quite right
					it talks about "select at least one item for .."  rather than saying "must select ..."
					2005-01-24  tried to sort out the message for a single checkbox
				--->
				<cfif findnocase ("#thisFlag#_#thisentity#",javascriptVerification) is 0>


					<CFIF (ListGetAt(thisFlag, 1, "_") IS "Checkbox" OR ListGetAt(thisFlag, 1, "_") IS "Radio") and getFlagGroup.recordCount is 1>
						<!--- this deals with single checkboxes or radios --->
						<!--- problem getting the name of the flag, 'cause the flaglist variable actually just give the name of the flaggroup, not the flag
								Also we should be able to handle a screen with two required flags on the same screen from the same flaggroup

								I am going to do a hack and refer the query getFlagGroup which has already run in singlflag..cfm

								If there is only one flag (usual) then we will do something slightly different
							 --->
						<cfif requiredMessage is "">
							<cfif requiredLabel is "">
								<cfset requiredLabel = "#getFlagGroup.flagname#">
							</cfif>

							<cfset requiredMessage = "Select #requiredLabel#">
						</cfif>

						<CFSET javascriptVerification = javascriptVerification & "if (! isItemInCheckboxGroupChecked (form.#thisFlag#_#thisentity#, #getFLagGroup.flagid#)) {msg = msg + '  #requiredMessage#\n';}" >

					<CFELSE>

							<cfif requiredMessage is "">
								<cfif requiredLabel is  "">
									<CFIF ListGetAt(thisFlag, 1, "_") IS "Checkbox" OR ListGetAt(thisFlag, 1, "_") IS "Radio">
										<!--- radio and checkbox use the flaggroup name --->
										<cfset requiredLabel = getFLagGroup.FlagGroupName>
									<cFELSE>
											<!--- getflaggroup might have brought back more than one record (it is possible to have more than one flag in 'singleflag!' -so have t do a quick query --->
										<cfquery name = "getdetail" dbtype="query">
										select flagname from getFLagGroup where flagid = #ListGetAt(thisFlag, 2, "_")#
										</cfquery>
										<cfset requiredLabel = getdetail.FlagName>
									</cfif>
								</cfif>

								<cfif getFlagGroup.flagtypeid is 2>
									<cfset requiredMessage = "phr_Sys_Screen_SelectAtLeastOneCheckboxFor #requiredLabel#">
								<cfelseif getFlagGroup.flagtypeid is 3 or usevalidvalues is 1> <!--- 2006-02-08 WAB added test for use valid values, if displaying valid values then will always need to select something from a dropdown or radio  --->
									<cfset requiredMessage = "phr_Sys_Screen_SelectAnOptionFor #requiredLabel#">
								<cFelseIF getFlagGroup.flagtypeid IS 6 AND getFlagGroup.linksToEntityTypeID EQ 0>
									<cfset requiredMessage = "phr_Screen_EnterAnOptionFor #requiredLabel# "><!--- CASE 430402: PJP 19/09/2012: Added in translation --->
								<!--- <cFelseIF getFlagGroup.flagtypeid IS 6  > --->	<!--- 2012-09-20 PPB Case 430402 let integer flags drop through to phr_Screen_EnterAValueFor --->
									<!--- NJH 2010/08/09 RW8.3 - replaced the typeList code --->
									<!--- <cfset flagGroupType = application. flagGroup[getFlagGroup.flagGroupId].flagType.name> --->
									<!--- <cfset requiredMessage = "phr_Screen_EnterAn #Lcase(getFlagGroup.TypeName)# phr_Screen_ValueFor #requiredLabel# "> ---><!--- CASE 430402: PJP 19/09/2012: Added in translation --->	<!--- 2012-09-20 PPB Case 430402 let integer flags drop through to phr_Screen_EnterAValueFor to avoid "japanese integer japanese" --->
									<!--- <cfset requiredMessage = "enter an #Lcase(ListGetAt('#application.typeList#',getFlagGroup.FlagTypeID))# value for #requiredLabel# "> --->
								<cFELSE>
									<cfset requiredMessage = "phr_Screen_EnterAValueFor #requiredLabel#">
								</CFIF>
							</cfif>

							<cfif displayas is "datedropdowns">
								<CFSET javascriptVerification = javascriptVerification & "if (numberofDateDropDownsSelected('#thisFormName#','#thisFlag#_#thisentity#') != 3) { msg = msg + '  phr_screen_enterAValueFor #requiredLabel#\n' };">
							<cfelse>
								<cFOUTPUT><CF_INPUT TYPE="HIDDEN" NAME="#thisFlag#_#thisentity#_required" VALUE="You must #requiredMessage#"></cFOUTPUT>
								<!--- <CFSET javascriptVerification = javascriptVerification & "msg = msg + verifyObject('#thisFlag#_#thisentity#',' #errmsg# for #getDetail.Name#','#required#');" > --->
								<CFSET javascriptVerification = javascriptVerification & "msg = msg + verifyObjectv2(form.#thisFlag#_#thisentity#,' #requiredMessage#','#required#');" >
							</cfif>

						</CFIF>

				</CFIF>

			</CFLOOP>

		</CFIF>

	</CFIF>

	<CFIF flagList is not "">
		<!--- adds to item to person/locationflaglist --->
		<CFSET "#getParentFlagGroup.entityTypeName#FlagList" = evaluate("#getParentFlagGroup.entityTypeName#FlagList") & "," & flagList>
	</CFIF>

	<cfif trim(jsverify) is not "">
		<cfloop list="#flagList#" index="thisFlag">
			<cfset thisLineFormField = "#thisFlag#_#thisentity#">
			<cfset javascriptverification = javascriptverification & "msg = msg + #evaluate("#jsverify#")#;" >
		</cfloop>
	</cfif>



<cFELSE>
	<cFOUTPUT><FONT cOLOR="##FF0000">#htmleditformat(thisFieldTextID)# is not a valid Flag or is a radio button.</FONT></cFOUTPUT>
</CFIF>


<CFSETTING enablecfoutputonly = "no">

