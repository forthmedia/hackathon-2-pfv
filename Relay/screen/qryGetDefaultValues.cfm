<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 	get default values for location and person fields
		used when adding new records 
 --->
 
<CFIF not isDefined("countryandregionlist")>
	<CFIF variables.countryID is 0 OR application.CountryGroupsBelongedTo[variables.countryid] is "">
		<CFSET regionlist = 0>
	<CFELSE>
		<CFSET regionlist = application.CountryGroupsBelongedTo[variables.countryid]> 
	</cfif>		

	<CFSET countryandregionlist = listappend(variables.countryid, regionlist)>
</cfif>	


 
 <cFQUERY NAME="getDefaultValues" datasource="#application.sitedatasource#" >
		select fieldname,
		CASE WHEN len(datavalue) is null THEN validvalue ELSE datavalue  END as thisvalidvalue
		from validfieldvalues as v1
		where (fieldname like 'person.%' or fieldname like 'location.%')
		AND countryID = 0
		AND defaultFieldValue=1
		AND NOT EXISTS (SELEcT 1 FROM validfieldvalues as v2
				WHERE v2.fieldname = v1.fieldname
				AND defaultFieldValue=1				
				and countryID  in ( <cf_queryparam value="#countryandregionlist#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) )
	

		<CFIF regionlist is not "">
		UNION			

		select fieldname,
		CASE WHEN len(datavalue) is null THEN validvalue ELSE datavalue  END as thisvalidvalue
		from validfieldvalues AS v1
		where (fieldname like 'person.%' or fieldname like 'location.%')
		AND countryID  in ( <cf_queryparam value="#regionlist#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )					
		AND defaultFieldValue=1
		
			AND NOT EXISTS (SELEcT 1 FROM validfieldvalues as v2
							WHERE v2.fieldname = v1.fieldname
							AND defaultFieldValue=1
							AND countryID  in ( <cf_queryparam value="#countryid#" CFSQLTYPE="cf_sql_integer"  list="true"> ))

		</CFIF>
		UNION
				
		select fieldname,
		CASE WHEN len(datavalue) is null THEN validvalue ELSE datavalue  END as thisvalidvalue
		from validfieldvalues as v1
		where (fieldname like 'person.%' or fieldname like 'location.%')
		AND defaultFieldValue=1
		AND countryID =  <cf_queryparam value="#countryid#" CFSQLTYPE="CF_SQL_INTEGER" > 

				
		</CFQUERY>

		
		<CFLOOP query="getDefaultValues">
			<CFSET thisFieldName = replace (fieldname, ".", "_","ALL")>
			<CFPARAM name="#thisFieldName#_DEFAULT" default="#thisValidValue#">
		</cfloop>