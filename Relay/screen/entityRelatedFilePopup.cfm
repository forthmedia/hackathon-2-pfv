<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File:			entityRelatedFilePopup.cfm
Author:			SWJ
Date created:	2005-05-08

Description:	Screen to allow files to be associated with any entity.  This is designed to be used
				as a popup screen.  To call see example link below:

				<a href="javascript:void(window.open('/screen/entityRelatedFilePopup.cfm?entityID=#getorderheader.orderid#&entityType=orders','UploadFiles','width=600,height=400,status,resizable,toolbar=0'))" onMouseOver="window.status='';return true" onMouseOut="window.status='';return true">phr_#phraseprefix#Order_UploadFilesToThisOrder</a>

Version history:
1
2009-03-02	NJH		CR-SNY671 - check if window.opener.relatedFileCountMessage exists before setting it.
2009-05-19	NJH		P-SNY069 - pass through a phrasePrefix
2009-07-17	NJH		P-FNL069 - check that url variables have been encrypted. Also default the params so that we don't get errors stating that the variables have not been defined.
2012/03/13 	PPB 	AVD001 added refreshOpener()
2014-10-28	RPW		CORE-666 OPP Edit Screen needs multiple XML Changes
2014-10-20 	PPB 	P-KAS040 added hook for displayRelatedFiles function in opener
2014-11-07 	PPB 	P-KAS049 allow customisation of File Category List 
--->

<cfparam name="url.entityID" type="numeric" default="0">
<cfparam name="url.entityType" type="string" default="">
<cfparam name="url.showFileCategoryDescription" type="boolean" default="false">
<cfparam name="url.FileCategory" type="string" default="">	 			<!--- 2014-11-07 PPB P-KAS049  --->
<cfparam name="url.phrasePrefix" type="string" default="">
<cfparam name="url.replaceallowed" type="boolean" default="true">
<cfparam name="url.uploadUUID" type="string" default=""><!--- 2014-10-28	RPW	CORE-666 OPP Edit Screen needs multiple XML Changes --->


 <!--- NJH 2009-05-19 P-SNY069 --->
<cf_displayBorder
	noborders=true
>
<!--- NJH 2009-07-16 P-FNL069 if the variables have come from the url scope, then check that they were encrypted --->
<cf_checkFieldEncryption fieldName = "entityID,entityType">

<CF_Translate>
<cf_head>
	<cf_title>phr_UploadFiles</cf_title>
</cf_head>

	<cfoutput>
	<h1>Phr_#htmleditformat(url.entityType)#_#htmleditformat(url.phrasePrefix)#UploadFileTitle</h1>
	<p>phr_#htmleditformat(url.entityType)#_#htmleditformat(url.phrasePrefix)#UploadFileExplanatoryText<br></p>
	</cfoutput>
	
	<!--- 2014-11-07 PPB P-KAS049 add a custom hook (which can change FileCategoryList)--->
	<cfset FileCategoryList = url.FileCategory>																
	<cf_include template="\code\cftemplates\entityRelatedFilePopup_Hook01.cfm" checkIfExists="true">		

	<!--- 2014-10-28	RPW	CORE-666 OPP Edit Screen needs multiple XML Changes --->
	<cf_relatedFile action="list,put" showFileCategoryDescription="#url.showFileCategoryDescription#" entity="#url.entityID#" entitytype="#url.entityType#" FileCategory="#FileCategoryList#" phrasePrefix="#url.phrasePrefix#" replaceallowed="#url.replaceallowed#" uploadUUID="#url.uploadUUID#"> 				<!--- 2014-11-07 PPB P-KAS049 --->

	
	<input type=button value="phr_CloseWindow" onClick="javascript:self.close();refreshOpener();">	<!--- 2012/03/13 PPB AVD001 call refreshOpener --->
	

	<!--- NJH 2009-03-02 CR-SNY671 Invoice Upload - added a check to see if window.opener.relatedFileCountMessage actually exists --->
	<cfoutput>
	<!--- 2014-10-28	RPW	CORE-666 OPP Edit Screen needs multiple XML Changes --->
	<script>
		// NB this only updates relatedFileCountMessage for added files not deleted ones (kaspersky refreshRelatedFilesList() does update it itself)
		if (parent.window.opener.relatedFileCountMessage) {																									//2014-10-20	PPB P-KAS040  use parent and innerHTML (for Firefox)
			parent.window.opener.relatedFileCountMessage.innerHTML = 'There are #relatedfile_FileList.recordCount# files associated with this record';		//2014-10-20	PPB P-KAS040  use parent and innerHTML (for Firefox)
		}

		//2012/03/13 PPB AVD001 added refreshOpener
		function refreshOpener() {
			
			if(typeof window.opener.displayRelatedFiles == "function"){
				window.opener.displayRelatedFiles();
			} else {
				if(typeof window.opener.refreshPage == "function"){
					window.opener.refreshPage();
				}
			    }
		    }
		             
				
		}



	</script>
	</cfoutput>

</CF_Translate>


