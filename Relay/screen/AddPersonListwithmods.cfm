<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
WAB 2010/10/13  removed get TextPhrases and replaced with CF_translate
--->


<CFPARAM name="frmlanguageid" default="#iif(isdefined('cookie.defaultlanguageid'),DE(cookie.defaultlanguageid),DE('1'))#">


<!--- <CFPARAM name="frmLanguageID" default="1">    ---> 
<CFSET listofvalidjobfunctions="ManagingDirector,FinancialManager,marketingManager,SalesContact,ServiceContact,PurchaseContact">

<!--- can't work out whether locationid or frmlocationid is set on coming into this template - so using both! --->
	<CFPARAM name="frmlocationid" default="">
	<CFPARAM name="locationid" default="#frmlocationid#">




<!--- had to add this query in to get the line beneath to run, 
but suspect that this query might already have been run in the calling template --->
<!--- get details of location--->
<CFQUERY NAME="getLocation" datasource="#application.siteDataSource#">
	select * from location where locationid =  <cf_queryparam value="#locationid#" CFSQLTYPE="CF_SQL_INTEGER" > 
</CFQUERY>

<CFPARAM NAME="Locname" DEFAULT="#GetLocation.sitename#">


<!--- get list of possible languages--->
<CFQUERY NAME="GetLangList" datasource="#application.siteDataSource#">
	select language from language order by language
</CFQUERY>

<!--- get name of current language --->
<CFQUERY NAME="GetLanguage" datasource="#application.siteDataSource#">
	SELECT language from language where LanguageID =  <cf_queryparam value="#frmlanguageID#" CFSQLTYPE="CF_SQL_INTEGER" > 
</CFQUERY>


<!--- get list of people at this location --->
<CFQUERY NAME="GetPeople" datasource="#application.siteDataSource#">
	Select p.personid, p.firstname, p.lastname, p.email, p.language, p.salutation, p.lastupdated
	from person as p
	where p.locationid =  <cf_queryparam value="#locationID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	and active = 1
</CFQUERY>

<cfquery name="Getcountry" datasource="#application.siteDataSource#">
Select countryid 
from location 
where location.locationid = #locationid#</cfquery>

<cfquery name="getvalidvaluesalutation" datasource="#application.siteDataSource#">
select ValidValue
from ValidFieldValues
where Fieldname like 'Person.Salutation'
AND CountryID =  <cf_queryparam value="#Getcountry.countryid#" CFSQLTYPE="cf_sql_integer" >
</cfquery>


<CFQUERY NAME="getalljobfunctions" datasource="#application.siteDataSource#">
		select f.name, f.flagid, f.namephrasetextid
		from flag as f, flaggroup as fg
		where f.flaggroupid= fg.flaggroupid
		and fg.flaggrouptextid='JobFunction'
		and f.active = 1
		and fg.active = 1
		and f.flagtextid  in ( <cf_queryparam value="#listofvalidjobfunctions#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )
		order by f.name
	</CFQUERY>


	<CFSET addoredit=iif(getpeople.recordcount is 0,DE("phr_addpeoplefor"),DE("phr_editaddpeoplefor"))>

	

<cf_translate encode="javascript">
<SCRIPT type="text/javascript">

<!--
	function doTranslation(phraseid) {
	//-----------------

	newWindow = window.open( '../report/utilities/language_detail.cfm?frmphraseid='+phraseid, 'translator','width=450,height=450,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1'  )
	newWindow.focus()
	}


	function verifyForm() {
		var form = document.frmmain;
		var msg = "";

		var userchecked = false
		for (j=0; j<form.elements.length; j++){
			thiselement = form.elements[j];
			if (thiselement.name == 'frmUser') {
					if (thiselement.checked==true)  {  
						userchecked = true
								}
			}
		}

		
		if (userchecked == false) {		
		
			msg += "* <CFOUTPUT>#replace(phr_whichrecordisyours,"<BR>", " ","ALL")#</CFOUTPUT>\n";

		}



		if (msg != "") {
				alert("\n<CFOUTPUT>phr_Pleaseprovidethefollowinginformation</CFOUTPUT>:\n\n" + msg);

		} else {

			form.submit();
		}
	}
//-->

</SCRIPT>
</cf_translate>

<cf_translate>

<cf_head>
	<cf_title>
	<CFOUTPUT>#htmleditformat(addoredit)#</cfoutput>
	</cf_title>
</cf_head>






<CFSET personfieldsonpage="Person_Email,Person_FirstName,Person_LastName,Person_Salutation,Person_Language">
<CFSET personidsonpage="">

<FORM ACTION="../partners/AddPersonListTask.cfm" METHOD="POST" NAME="frmmain">
<CFOUTPUT>
<CF_INPUT TYPE="Hidden" NAME="LocationID" VALUE="#LocationID#">
<CF_INPUT TYPE="Hidden" NAME="frmLocationID" VALUE="#LocationID#">
<CF_INPUT TYPE="HIDDEN" NAME="frmDate" VALUE="#createODBcDateTime(now())#">
<CFIF isdefined("frmProcessID")>
<CF_INPUT TYPE="HIDDEN" NAME="frmProcessID" VALUE="#frmProcessID#">
<CF_INPUT TYPE="HIDDEN" NAME="frmStepID" VALUE="#NextStepID#">
</cfif>

	
<CFIF isdefined("ScreenTitleA")>	<CF_INPUT TYPE="HIDDEN" NAME="ScreenTitleA" VALUE="#Screentitlea#">	</cfif>
<CFIF isdefined("partnerarea")>	<CF_INPUT TYPE="HIDDEN" NAME="partnerarea" VALUE="#partnerarea#"></cfif>
	
</CFOUTPUT>
<TABLE>
	<CFOUTPUT>
	<TR><TD WIDTH="85">&nbsp;</TD><TD COLSPAN="2"><H3>#htmleditformat(AddorEdit)# #htmleditformat(Locname)#</H3></TD></TR>
</CFOUTPUT>


 </TABLE>
		<CFOUTPUT><CFIF isdefined("email")><CF_INPUT TYPE="Hidden" NAME="email" VALUE="#email#"></CFIF></CFOUTPUT>

<TABLE>
		<TH WIDTH="85">&nbsp;</TH>
		<TH><CFOUTPUT><font face="arial narrow" color="blue">phr_Salutation</CFOUTPUT>:*</TH>
		<TH><CFOUTPUT><font face="arial narrow" color="blue">phr_FirstName</CFOUTPUT>:*</TH>
		<TH><CFOUTPUT><font face="arial narrow" color="blue">phr_LastName</CFOUTPUT>:*</TH>
		<TH><CFOUTPUT><font face="arial narrow" color="blue">phr_emailAddress</CFOUTPUT>:*</TH>
		<TH><CFOUTPUT><font face="arial narrow">phr_JobFunction</CFOUTPUT>:</TH>	
		<TH><CFOUTPUT><font face="arial narrow">phr_language</CFOUTPUT>:</TH>	
		<TH><CFOUTPUT><font face="arial narrow">phr_WhichRecordIsYours</CFOUTPUT></TH>


	<CFIF getpeople.recordcount is not 0>
	<TR><TD WIDTH="85">&nbsp;</TD><TD colspan=5><CFOUTPUT><B>phr_editthefollowingpeople:</b></cfoutput></td></tr>
	</cfif>

	<CFLOOP query="getpeople"> 
		<CFSET personidsonpage=listappend(personidsonpage,personid)>

		<!--- get list of all job functions, with field showing if this person has this job function. ordered with selected job functions first--->
		<CFQUERY NAME="getalljobfunctionsordered" datasource="#application.siteDataSource#">
			select f.name, f.flagid, f.namephrasetextid,
			case when exists (select * from booleanflagdata as bfd where entityid =  <cf_queryparam value="#personid#" CFSQLTYPE="CF_SQL_INTEGER" >  and bfd.flagid=f.flagid) THEN 1 ELSE 0 END	as selected
			from flag as f, flaggroup as fg
			where f.flaggroupid= fg.flaggroupid
			and fg.flaggrouptextid='JobFunction'
			and f.active = 1
			and fg.active = 1
			and f.flagtextid  in ( <cf_queryparam value="#listofvalidjobfunctions#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )
			order by selected DESC, f.orderingindex
		</CFQUERY>


		<!--- list of job function flags for this person  --->
		<CFQUERY NAME="getentityjobfunctions" datasource="#application.siteDataSource#">
			select f.flagid
			from flag as f, flaggroup as fg, booleanflagdata as bfd
			where f.flaggroupid= fg.flaggroupid
			and fg.flaggrouptextid='JobFunction'
			and bfd.flagid=f.flagid
			and bfd.entityid =  <cf_queryparam value="#personid#" CFSQLTYPE="CF_SQL_INTEGER" > 
			and f.active = 1
			and fg.active = 1
			and f.flagtextid  in ( <cf_queryparam value="#listofvalidjobfunctions#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )
			order by f.orderingindex
		</cfquery>

		


		<TR>
		
		<CFOUTPUT>

		<CF_INPUT TYPE="HIDDEN" NAME="person_lastupdated_#personid#" VALUE="#lastupdated#">			
		<TD WIDTH="85">&nbsp;</TD></cfoutput>
		
		<TD valign="Top">
			<cfif Getcountry.countryid IS 13>

				<CFSET validvaluelist=valuelist(getvalidvaluesalutation.validvalue)>
				<CFSET formfield = "Person_Salutation_#Personid#">
				<CFSET currentValue = salutation>
				<CFSET allowNull = false>
				<CFINCLUDE template= "showValidValueList.cfm">

			<cfelse>

				<cfoutput><CF_INPUT TYPE="Text" NAME="Person_Salutation_#Personid#" SIZE="10" VALUE="#trim(salutation)#"></cfoutput>
				<cfoutput><CF_INPUT TYPE="Hidden" NAME="Person_Salutation_#Personid#_orig" VALUE="#trim(salutation)#"></cfoutput>
			</cfif>


		</TD><cfoutput>
		
		<TD valign="Top">
		<CF_INPUT TYPE="Text" NAME="Person_FirstName_#Personid#" SIZE="15" VALUE="#trim(firstname)#"> 
		<CF_INPUT TYPE="Hidden" NAME="Person_FirstName_#Personid#_orig" VALUE="#trim(firstname)#">
		</TD>
		
		<TD valign="Top">
		<CF_INPUT TYPE="Text" NAME="Person_LastName_#Personid#" SIZE="15" VALUE="#trim(lastname)#">
		<CF_INPUT TYPE="Hidden" NAME="Person_LastName_#Personid#_orig" VALUE="#trim(Lastname)#">
		</TD>
	
		<TD valign="Top">
		<CF_INPUT TYPE="Text" NAME="Person_email_#Personid#" VALUE="#trim(email)#">
		<CF_INPUT TYPE="Hidden" NAME="Person_email_#Personid#_orig" VALUE="#trim(email)#">
		</TD>
		
		
		<TD>
		
		<SELECT multiple NAME="checkbox_jobfunction_#personid#" size=#max(getentityjobfunctions.recordcount+1,3)#>
		<OPTION VALUE="0" #IIF(getentityjobfunctions.recordcount is 0, DE(" SELECTED"), DE(""))#>phr_NoJobFunction</OPTION>
		</cfoutput>
		<CFOUTPUT QUERY="getalljobfunctionsordered">
		<OPTION VALUE="#Flagid#" #IIF(listfindnocase(valuelist(getentityjobfunctions.flagid),flagid) is not 0, DE(" SELECTED"), DE(""))#>#HTMLEditFormat(evaluate("phr_#htmleditformat(namephrasetextid)#"))#</OPTION>
		</CFOUTPUT>
		</SELECT>
		</TD>
		<cfoutput>
		<INPUT TYPE="Hidden" NAME="checkbox_jobfunction_#Personid#_orig" VALUE="#IIF(getentityjobfunctions.recordcount is not 0, DE("#valuelist(getentityjobfunctions.flagid)#"), DE("0") )#">	
		</cfoutput>



		<TD>

				<CFSET validvaluelist=valuelist(GetLangList.Language)>
				<CFSET formfield = "Person_Language_#Personid#">
				<CFSET currentValue = language>
				<CFSET allowNull = false>
				<CFINCLUDE template= "showValidValueList.cfm">

		</TD>


		<cfoutput>
		<TD valign="Top">
		<INPUT TYPE="Radio" NAME="frmUser"  value="#Personid#" <CFIF isDefined("request.relayCurrentUser.personid")><CFIF #personid# is #request.relayCurrentUser.personid#>CHECKED</CFIF></CFIF>>
		</TD>
		</cfoutput>

		</TR>

	
	</cfloop>
	
	


	
		<CFSET firstname="">
		<CFSET lastname="">
		<CFSET email="">
		<CFSET salutation="">
		<CFSET language="">
		<CFSET lastupdated="">
	


	<CFLOOP index="I" from="1" to="10">
		<CFSET personid="new"&I>
		<CFSET personidsonpage=listappend(personidsonpage,personid)>		
		
	
		
		<CFOUTPUT>
		<INPUT TYPE="HIDDEN" NAME="person_lastupdated_#personid#" VALUE="#lastupdated#">			
		<CF_INPUT TYPE="HIDDEN" NAME="person_locationid_#personid#" VALUE="#locationid#"></cfoutput>			<!--- this tells the task which location the person is to be added to --->

		<TD WIDTH="85">&nbsp;</TD>
				
		<TD valign="Top">
			<cfif Getcountry.countryid IS 13>

				<CFSET validvaluelist=valuelist(getvalidvaluesalutation.validvalue)>
				<CFSET formfield = "Person_Salutation_#Personid#">
				<CFSET currentValue = salutation>
				<CFSET allowNull = true>
				<CFSET nullText = "&nbsp">
				<CFINCLUDE template= "showValidValueList.cfm">

			<cfelse>

				<cfoutput><CF_INPUT TYPE="Text" NAME="Person_Salutation_#Personid#" SIZE="10" VALUE="#trim(salutation)#"></cfoutput>
				<cfoutput><CF_INPUT TYPE="Hidden" NAME="Person_Salutation_#Personid#_orig" VALUE="#trim(salutation)#"></cfoutput>

			</cfif>
		</TD>
		<cfoutput>
		<TD valign="Top"><CF_INPUT TYPE="Text" NAME="Person_FirstName_#Personid#" SIZE="15"> 
		<CF_INPUT TYPE="Hidden" NAME="Person_FirstName_#Personid#_orig" VALUE="">
		</TD>
		<TD valign="Top"><CF_INPUT TYPE="Text" NAME="Person_LastName_#Personid#" SIZE="15">
		<CF_INPUT TYPE="Hidden" NAME="Person_LastName_#Personid#_orig" VALUE="">
		</TD>
		<TD valign="Top"><CF_INPUT TYPE="Text" NAME="Person_email_#Personid#" VALUE="">
		<CF_INPUT TYPE="Hidden" NAME="Person_email_#Personid#_orig" VALUE="">
		</TD>

	
		<TD>
		<SELECT multiple NAME="checkbox_jobfunction_#personid#" size=3>

		<OPTION VALUE="0" SELECTED>phr_NoJobFunction</OPTION>
		</cfoutput>
		<CFOUTPUT QUERY="getalljobfunctions">
		<OPTION VALUE="#Flagid#" >#HTMLEditFormat(evaluate("phr_#htmleditformat(namephrasetextid)#"))#</OPTION>
		</CFOUTPUT>
		</SELECT>
		<cfoutput>
		<CF_INPUT TYPE="Hidden" NAME="checkbox_jobfunction_#Personid#_orig" VALUE="0">	
		</cfoutput>
		</TD>

		<TD>

				<CFSET validvaluelist=valuelist(GetLangList.Language)>
				<CFSET formfield = "Person_Language_#Personid#">
				<CFSET currentValue = GetLanguage.Language>
				<CFSET allowNull = false>
				<CFINCLUDE template= "showValidValueList.cfm">

<!---

		<cfoutput><SELECT NAME="Person_Language_#personid#"></cfoutput>
			<CFOUTPUT QUERY="GetLangList">
			<OPTION VALUE="#Language#" #IIF(GetLanguage.Language IS Language, DE(" SELECTED"), DE(""))#>#HTMLEditFormat(Language)#</OPTION>
			</CFOUTPUT>
			</SELECT>
			<cfoutput><INPUT TYPE="Hidden" NAME="Person_Language_#Personid#_orig" VALUE="#GetLanguage.Language#"></cfoutput>
 --->
		</TD>


		<TD valign="Top">
		<CFOUTPUT>
		<INPUT TYPE="Radio" NAME="frmUser"  value="#Personid#" <CFIF isDefined("request.relayCurrentUser.personid")><CFIF #personid# is #request.relayCurrentUser.personid#>CHECKED</CFIF></CFIF>>
		</cfoutput>
		</TD>

		

	</TR>


	</cfloop>
	

	<TR>
		<TD WIDTH="85">&nbsp;</TD>
		<TD colspan="3">* <CFOUTPUT>phr_RequiredFields</cfoutput>
		</TD>
	</TR>
	
	
	<TR><TD WIDTH="85"><P>&nbsp;</P></TD>
		<TD>&nbsp;</TD>
		<TD><P>&nbsp;</P><CFOUTPUT>
			<A HREF="javascript:verifyForm()">
			<IMG SRC="/images/buttons/c_continue_e.gif" WIDTH=105 HEIGHT=21 ALT="phr_continue" BORDER="0"></A>
		</CFOUTPUT>
		</TD>
	</TR>
</TABLE>

<CFOUTPUT>
<INPUT TYPE="Hidden" NAME="frmtablelist" VALUE="person">
<CF_INPUT TYPE="Hidden" NAME="frmpersonfieldlist" VALUE="#personfieldsonpage#">
<CF_INPUT TYPE="Hidden" NAME="frmpersonidlist" VALUE="#personidsonpage#">
<INPUT TYPE="Hidden" NAME="frmpersonflaglist" VALUE="checkbox_jobfunction">
</cfoutput>

</FORM>




</cf_translate>



