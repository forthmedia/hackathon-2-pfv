<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Requires the following inputs

frmpersonid		=	One person
or frmlocationid	= one location
frmnextpage		=	template to be included after update to database
partnerarea		=	Title text

and optional

frmsecure		=	test for this on 'frmnextpage' to deteremine whether to go to secure server
frmreadonlyfields  = list of fields which are readonly (form table_field) as single quoted list

frmHeadlinePhrase
frmFooterPhrase

frmDontShowrequired    = true/false   doesn't show the "reuired fields" bit at bottom


Mods

2000-05-08 		WAB doing some work to make this work with new screen tag
2001-07-02		WAB added frmDontShowrequired parameter
2001-07-09		WAB if styleSheet is null then no stylesheet loaded
2001-07-12  	WAB got rid of calls to getText Phrases

2004-09-28  	WAB cf_ascreen needed a formname parameter passed to it
2005-03-21    	WAB made form enctype="multipart/form-data"
2005-09-13	WAB moved preinclude code to ascreenitem.cfm
2009-02-26 	NYB		Sophos Stargate 2 - added ' class="buttonRow'
2011/05/25 	NYB		REL106 whitespace only changes
2012/04/17	PPB		P-REL112 added ids to buttons for css hooks
2014-08-15		RPW	Internal Approvals screen styling issues
2015-04-27		WAB add ID to <form> (in addition to existing name) so can be easily referenced by jquery
2016-03-09 	WAB	 PROD2016-684 Added divLayout property of a screen, used to set the overall class (and other things later on)
--->

<cfparam name="message" default="">
<cfparam name="frmFormName" default = "mainForm">  <!--- Allow separate form names so we can have multiple showsceen calls with forms on one page --->
<cf_param name="frmMethod" label="Method" default="edit" validvalues="edit,view"/>
<cf_param name="frmNoButton" label="No Buttons" default="no" type="boolean"/>   <!--- no save and reset buttons when in viewer --->
<cf_param name="nextScreenid" label="Next Screen ID" default="#frmcurrentscreenid#"/><!--- normally nextScreenID is set the currentscreenid, but is is possible to define the nextScreenID variable before calling showscreen --->
<cfparam name="frmShowScreenAction" default="/screen/updateData.cfm">
<cfparam name="styleSheet" default="">
<cf_param name="frmDontShowrequired" label="Dont Show Required Marker" default="no" type="boolean"/>
<cf_param name="frmParametersToHideOnForm" label="Parameters To Hide On Form" default=""/>
<cfparam name="protocol" default="#request.currentSite.httpProtocol#">
<cfhtmlhead text="<meta name='RelaySID' content='#frmCurrentScreenID#'>">
<cfparam name="ConfirmationMessage" default="">

<cfset partnerrestrictions = false><!--- if rights check finds that this is a Partner then this is set to True, can then only view own organisation's records --->

<!--- sort out page that is called after data update is done --->
<cf_param name="frmNextPage" label="Next Page" default=""/>

<cfif (isdefined("frmprocessid") or (isdefined("request.processAction") and request.processAction.continueButton.show)) and frmnextpage is "">
	<cfset frmnextpage="redirector.cfm">
<cfelseif frmnextpage is "">
	<cfset frmnextpage="screen.cfm">
</cfif>



<!--- If an entity has been added, then we come back and view it --->
<!--- probably need to change the logic here incase we want to do a series of additions--->
<cfif isdefined("personidsadded")>
	<cfset frmpersonid=listfirst(personidsadded)>
</cfif>
<cfif isdefined("locationidsadded")>
	<cfset frmlocationid=listfirst(locationidsadded)>
</cfif>

<!--- New code for getting a record and rights to it
	this tag returns querys named after the entity and its parents (eg person. , location. ...)
	also a query called rights with the rights to that record
--->
<cfif isdefined("frmentitytype")>
	<cfset "frm#frmEntityType#ID" = frmcurrententityid>
</cfif>

<cf_param name="frmpersonid" default="0" label="PersonID"/>
<cf_param name="frmLocationid" default="0" label="LocationID"/>
<cf_param name="frmOrganisationid" default="0" label="OrganisationID"/>

<cfif frmpersonid is not 0>
	<cf_getrecord entitytype="person" entityid = "#frmpersonid#" locationid = "#frmLocationID#" getparents getrights>
<cfelseif  frmlocationid is not 0>
	<cf_getrecord entitytype="location" entityid = "#frmLocationID#" getparents getrights>
<cfelseif frmorganisationid is not 0>
	<cf_getrecord entitytype="organisation" entityid = "#frmOrganisationID#" getparents getrights>
<cfelse>
	<cfoutput>Unsupported Relay Entity</cfoutput>
</cfif>


<cfoutput>
	<SCRIPT type="text/javascript">
			var focusChanged = false
		<CFIF isdefined("session.showScreenEditLink")>
			<CFINCLUDE template="..\admin\screens\jsEditScreenDef.cfm">
		</CFIF>

		function jsReset() {
		//----------------
		var form = document.mainForm;
		form.reset();
		}


		function jsSubmit() {
		//-----------------

			return verifyForm()

		}


		function moveback(){
			document.location ="Showscreen.cfm";
		}

		function verifyForm() {
			var form = window.document.mainForm;
			msg = "";
			<cfif isdefined("frmInViewer"	)>
				allowAbort = true
					<CFSET return_ok = 0>
					<CFSET return_notok = 1>
					<CFSET return_notokabort = 2>
			<CFELSE>

				allowAbort = false
					<CFSET return_ok = "">
					<CFSET 	return_notok = "">
					<CFSET return_notokabort = "">
			</cfif>
			var focusChanged = false
			msg = msg  +verifyInput()
			if (msg != "") {
				focusChanged = false
				if (allowAbort == false ) {
					alert("\nphr_Pleaseprovidethefollowinginformation :\n\n" + msg);
					return #jsStringFormat(return_notok)#
				} else {
					wantToContinue = confirm("\nphr_Pleaseprovidethefollowinginformation:\n\n" + msg + "\n\n Click Cancel to Abort the save operation");
					if (wantToContinue) {
						return #jsStringFormat(return_notok)#
					} else {
						return #jsStringFormat(return_notokabort)#
					}
				}

			} else {
				<!--- 2013-10-10	YMA	Case 435739 re-instate on submit check in such a way that does not break form validation. --->
				<cfif ConfirmationMessage neq "">
					if(confirm('#ConfirmationMessage#')){
						form.submit();
						return #jsStringFormat(return_ok)#
					}else{
						return #jsStringFormat(return_notokabort)#
					}
				<cfelse>
					form.submit();
					return #jsStringFormat(return_ok)#
				</cfif>
			}
		}



	//-->

	</script>

	<cfset javascriptverification="">

</cfoutput>

<!--- set a width for the table --->
<cfif isdefined("getScreen.tableWidth") and getscreen.tablewidth is not ""  and getscreen.tablewidth is not 0>
	<cfset tablewidth=getscreen.tablewidth>
<cfelse>
	<cfparam name="tableWidth" default="100%">
</cfif>

<!-- BEGIN: showScreenNew MainTable -->

<!--- start of form --->
<!--- WAB 2006-10-11 moved  form tag to outside of the table, otherwise firefox has problems with dynamically adding  html elements--->
<cfif request.relayFormDisplayStyle neq "HTML-div">
	<CFform  action="#frmShowScreenAction#" method="POST" name="#frmFormName#" id="#frmFormName#" target="_self" enctype="multipart/form-data" style="margin:0px;">
	<cfoutput>
	<!--- <form action="#frmShowScreenAction#" method="POST" name="#frmFormName#" target="_self" enctype="multipart/form-data" style="margin:0px;"> --->
	<!--- 2014-08-15		RPW	Internal Approvals screen styling issues --->
	<table width="#tableWidth#" border="0" cellspacing="0" cellpadding="0" class="screenTable" align=left id="#frmcurrentscreenid#">  <!--- wab removed align="left" 2004-10-12 'cause it really screwed up the debug output and I can't think that it was really important since tables normally align left by default.  Then put it back in 2006-06-28 because it has an effect on divs which surround it.  without the align left the divs don't always expand to fit the table (in firefox) --->
	<!--- Output a message if defined--->
		<cfif message is not "">
				<tr>
					<td colspan ="2">#application.com.relayUI.message(message=message)#</td>
				</tr>
		</cfif>

		<CF_INPUT type="HIDDEN" name="frmNextPage" value="#frmnextpage#">
		<cfif isdefined("request.processAction")>
			<cfif request.processAction.continueButton.show>
			<!--- WAB 2006-05-23 trying out a way of doing processes in  --->
			<CF_INPUT type="HIDDEN" name="frmProcessID" value="#request.processAction.ProcessID#|#request.processAction.nextStepID#">
			<CF_INPUT type="HIDDEN" name="frmProcessUUID" value="#request.processAction.UUID#">
			<cfset request.processAction.continueButton.show = false>  <!--- prevents two buttons being shown on page --->
			</cfif>
		<cfelseif isdefined("frmProcessID") and isdefined("NextStepID")>
			<CF_INPUT type="HIDDEN" name="frmProcessID" value="#frmProcessID#">
			<CF_INPUT type="HIDDEN" name="frmStepID" value="#NextStepID#">
		</cfif>
		<cfif isdefined("person")>
			<!--- this variable used when processing person additions to determine the location --->
			<cfif not isNumeric(person.locationid)>  <!--- WAB 2007/11/21 put this in for NABU because wanted person_locationid_#personid# to appear on a screen but don't know whether will break anything else.  If really is only used on additions, then it should work --->
				<CF_INPUT type="HIDDEN" name="person_locationid_#person.personid#" value="#person.locationid#">
			</cfif>
			<CF_INPUT type="HIDDEN" name="frmPersonId" value="#Person.personid#">
		</cfif>
		<cfif isdefined("location")>
			<!--- this variable used when processing location additions to determine the organisation --->
			<CF_INPUT type="HIDDEN" name="location_organisationid_#location.locationid#" value="#location.organisationid#">
			<!--- frmlocationid used mainly for adding new person and locations --->
			<CF_INPUT type="HIDDEN" name="frmLocationId" value="#location.locationid#">
		</cfif>
		<cfif isdefined("organisation")>
			<CF_INPUT type="HIDDEN" name="frmOrganisationId" value="#organisation.Organisationid#">
		</cfif>

		<CF_INPUT type="HIDDEN" name="frmCurrentScreenId" value="#nextscreenid#">
		<CF_INPUT type="HIDDEN" name="frmLoadedScreenId" value="#frmcurrentscreenid#">  <!--- WAb 2006-06-01 CR_SNY585 screentracking (frmCurrentScreenId can be changed by javascript to go to a new screen, so not a good thing to use!) --->

		<!--- NJH 2009/06/30 P-FNL069
		<cfparam name="frmGlobalParameters" default = "">
		<input type="HIDDEN" name="frmGlobalParameters" value="#frmGlobalParameters#"> --->
		<cfif isdefined("frmentitytype")>
			<CF_INPUT type="HIDDEN" name="frmentitytype"  value="#frmentitytype#">
			<CF_INPUT type="HIDDEN" name="frmCurrentEntityId"  value="#evaluate("#frmentitytype#.#frmentitytype#id")#">
		</cfif>
		<cfloop list = "#frmParametersToHideOnForm#" index="thisParameter">
			<CF_INPUT type="HIDDEN" name="#thisParameter#"  value="#evaluate(thisParameter)#">
		</cfloop>


		<cfif isdefined("setusercookie1")>
			<CF_INPUT type="HIDDEN" name="frmSetUserCookie1"  value="#SetUserCookie1#">
		</cfif>

	</cfoutput>


	<cfsetting enablecfoutputonly="yes">


	<!--- if a Column Width is defined then output a row with the width set
		(OK a bit of a hack, should be passed to cf_ascreen tag
		in fact it can be if form.col1Width is set
	--->

	<cfif isdefined("col1width") and isdefined("col2width")>
		<!--- removed because you can do maths when the default table width is set to 100%
		<cfset col2width=tablewidth - col1width> --->
		<cfoutput>
			<tr class="nobackground">
				<td width="#col1Width#">&nbsp;</td><td width="#col2Width#">&nbsp;</td>
			</tr>
		</cfoutput>
	</cfif>

		<cf_ascreen formname="#frmFormName#">
			<cfif isdefined("person")>
				<cf_ascreenitem screenid="#frmcurrentscreenid#"
						method="#frmMethod#"
						person=#person#
						location=#location#
						organisation=#organisation#
				>
			<cfelseif isdefined("location")>
				<cf_ascreenitem screenid="#frmcurrentscreenid#"
						method="#frmMethod#"
						location=#location#
						organisation=#organisation#
				>
			<cfelseif isdefined("organisation")>
				<cf_ascreenitem screenid="#frmcurrentscreenid#"
						method="#frmMethod#"
						organisation=#organisation#
				>
			</cfif>
		</cf_ascreen>

	<!--- 	<cfoutput></form></cfoutput> --->



		<!--- text to say "required fields in blue",if this is an updateable page --->
		<cfif updateablefields and not frmdontshowrequired  >
			<cfoutput><tr><td colspan="2"><p class="required">phr_requiredFields</p></td></tr></cfoutput>
		</cfif>



		<!--- =============================================================
		                BUTTON SECTION
		============================================================== --->
		<cfif not frmnobutton>
			<cfoutput>
				<!--- these variables can be set from outside in order to display different buttons --->
				<cf_param name="frmButtonAsLink" label="Display Button As Link" default="yes" type="boolean"/>
				<cf_param name="ResetButton" label="Reset Button Text" default="phr_reset"/>
				<cfparam name="ResetButtonAction" default = "JavaScript:jsReset();">
				<cf_param name="ContinueButton" label="Continue Button Text" default="phr_continue"/>
				<cf_param name="ContinueButtonAction" default = "JavaScript:verifyForm();"/>
				<cf_param name="Button3" default="" description="Optional 3rd Button"/>
				<cf_param name="Button3Action" default=""/>

				<cfif frmmethod is "view"  or not updateablefields >
					<!--- if no updating tables, then don't need reset button - probably need a better test --->
					<cfset resetbutton = "">
				</cfif>

			<cfif frmButtonAsLink>
				<!--- NYB 2009-02-26 Sophos Stargate 2 - added ' class="buttonRow'
					NJH 2012/11/07 - Case 431486 - change htmlEditFormat to sanitiseHTML
				 --->
				<tr align="center" class="buttonRow">
					<td colspan="2">
					<cfif trim(resetbutton) is not "" and resetButton neq "phr_blank">
						<a id="screenResetButton" href="#ResetButtonAction#">#application.com.security.sanitiseHTML(ResetButton)#</a>&nbsp;&nbsp;
					</cfif>

					<cfif continuebutton is not "">
						<a id="screenContinueButton" href="#ContinueButtonAction#">#application.com.security.sanitiseHTML(ContinueButton)#</a>&nbsp;&nbsp;
					</cfif>

					<cfif button3 is not "">
						<a id="screenButton3" href="#Button3Action#">#application.com.security.sanitiseHTML(button3)#</a>&nbsp;&nbsp;
					</cfif>
					</td>
				</tr>
			<cfelse>
				<!--- NYB 2009-02-26 Sophos Stargate 2 - added ' class="buttonRow' --->
				<tr class="buttonRow">
					<td colspan="2">
					<cfif resetbutton is not "">
						<CF_INPUT id="screenResetButton" type="Button" onclick="#ResetButtonAction#" value="#ResetButton#" class="screenButton">
					</cfif>

					<cfif continuebutton is not "">
						<CF_INPUT id="screenContinueButton" type="Button" onclick="#ContinueButtonAction#" value="#ContinueButton#" class="screenButton">
					</cfif>

					<cfif button3 is not "">
						<CF_INPUT id="screenButton3" type="Button" onclick="#Button3Action#" value="#button3#" class="screenButton">
					</cfif>
					</td>
				</tr>
				</cfif>
			</cfoutput>
		</cfif>


	<cfif isdefined("frmFooterPhrase") and frmfooterphrase is not "">
		<cfoutput><tr><td colspan="2"><p>phr_#htmleditformat(frmFooterPhrase)#</p></td></tr></cfoutput>
	</cfif>

	<cfoutput></table loc="showScreenNew ln 316"></cfoutput>
	</CFFORM>
<cfelse>

	<cfset relayformdisplayClass = "">
	<cfset screenParameters = application.com.regExp.convertNameValuePairStringToStructure (inputString=application.com.screens.getScreen(frmcurrentscreenid).parameters ,delimiter=",")>
	<cfif structKeyExists (screenParameters,"divlayout") and screenParameters.divlayout is "horizontal">
		<cfset relayformdisplayClass = "form-horizontal">
	</cfif>

	<!--- 2014/06/05	YMA		Responsive Forms --->
	<CFform  action="#frmShowScreenAction#" method="POST" name="#frmFormName#" target="_self" enctype="multipart/form-data" style="margin:0px;">
		<cf_relayformdisplay class = #relayformdisplayClass#>

		<cfoutput>
		<!--- Output a message if defined--->
			<cfif message is not "">
				#application.com.relayUI.message(message=message)#
			</cfif>

			<CF_INPUT type="HIDDEN" name="frmNextPage" value="#frmnextpage#">
			<cfif isdefined("request.processAction")>
				<cfif request.processAction.continueButton.show>
				<!--- WAB 2006-05-23 trying out a way of doing processes in  --->
				<CF_INPUT type="HIDDEN" name="frmProcessID" value="#request.processAction.ProcessID#|#request.processAction.nextStepID#">
				<CF_INPUT type="HIDDEN" name="frmProcessUUID" value="#request.processAction.UUID#">
				<cfset request.processAction.continueButton.show = false>  <!--- prevents two buttons being shown on page --->
				</cfif>
			<cfelseif isdefined("frmProcessID") and isdefined("NextStepID")>
				<CF_INPUT type="HIDDEN" name="frmProcessID" value="#frmProcessID#">
				<CF_INPUT type="HIDDEN" name="frmStepID" value="#NextStepID#">
			</cfif>
			<cfif isdefined("person")>
				<!--- this variable used when processing person additions to determine the location --->
				<cfif not isNumeric(person.locationid)>  <!--- WAB 2007/11/21 put this in for NABU because wanted person_locationid_#personid# to appear on a screen but don't know whether will break anything else.  If really is only used on additions, then it should work --->
					<CF_INPUT type="HIDDEN" name="person_locationid_#person.personid#" value="#person.locationid#">
				</cfif>
				<CF_INPUT type="HIDDEN" name="frmPersonId" value="#Person.personid#">
			</cfif>
			<cfif isdefined("location")>
				<!--- this variable used when processing location additions to determine the organisation --->
				<CF_INPUT type="HIDDEN" name="location_organisationid_#location.locationid#" value="#location.organisationid#">
				<!--- frmlocationid used mainly for adding new person and locations --->
				<CF_INPUT type="HIDDEN" name="frmLocationId" value="#location.locationid#">
			</cfif>
			<cfif isdefined("organisation")>
				<CF_INPUT type="HIDDEN" name="frmOrganisationId" value="#organisation.Organisationid#">
			</cfif>

			<CF_INPUT type="HIDDEN" name="frmCurrentScreenId" value="#nextscreenid#">
			<CF_INPUT type="HIDDEN" name="frmLoadedScreenId" value="#frmcurrentscreenid#">  <!--- WAb 2006-06-01 CR_SNY585 screentracking (frmCurrentScreenId can be changed by javascript to go to a new screen, so not a good thing to use!) --->

			<cfif isdefined("frmentitytype")>
				<CF_INPUT type="HIDDEN" name="frmentitytype"  value="#frmentitytype#">
				<CF_INPUT type="HIDDEN" name="frmCurrentEntityId"  value="#evaluate("#frmentitytype#.#frmentitytype#id")#">
			</cfif>
			<cfloop list = "#frmParametersToHideOnForm#" index="thisParameter">
				<CF_INPUT type="HIDDEN" name="#thisParameter#"  value="#evaluate(thisParameter)#">
			</cfloop>


			<cfif isdefined("setusercookie1")>
				<CF_INPUT type="HIDDEN" name="frmSetUserCookie1"  value="#SetUserCookie1#">
			</cfif>
		</cfoutput>

		<cf_ascreen formname="#frmFormName#">
			<cfif isdefined("person")>
				<cf_ascreenitem screenid="#frmcurrentscreenid#"
						method="#frmMethod#"
						person=#person#
						location=#location#
						organisation=#organisation#
				>
			<cfelseif isdefined("location")>
				<cf_ascreenitem screenid="#frmcurrentscreenid#"
						method="#frmMethod#"
						location=#location#
						organisation=#organisation#
				>
			<cfelseif isdefined("organisation")>
				<cf_ascreenitem screenid="#frmcurrentscreenid#"
						method="#frmMethod#"
						organisation=#organisation#
				>
			</cfif>
		</cf_ascreen>
		</cf_relayformdisplay>

		<!--- text to say "required fields in blue",if this is an updateable page --->
		<cfif updateablefields and not frmdontshowrequired  >
			<cfoutput>
				<div class="form-group">
				 	<div class="buttonContainer col-xs-12 col-sm-9 col-sm-offset-3">
						phr_requiredFields
					</div>
				</div>
			</cfoutput>
		</cfif>

		<!--- =============================================================
		                BUTTON SECTION
		============================================================== --->
		<cfif not frmnobutton>
			<cfoutput>
				<!--- these variables can be set from outside in order to display different buttons --->
				<cf_param name="frmButtonAsLink" label="Display Button As Link" default="yes" type="boolean"/>
				<cf_param name="ResetButton" label="Reset Button Text" default="phr_reset"/>
				<cfparam name="ResetButtonAction" default = "JavaScript:jsReset();">
				<cf_param name="ContinueButton" label="Continue Button Text" default="phr_continue"/>
				<cf_param name="ContinueButtonAction" default = "JavaScript:verifyForm();"/>
				<cf_param name="Button3" default="" description="Optional 3rd Button"/>
				<cf_param name="Button3Action" default=""/>

				<cfif frmmethod is "view"  or not updateablefields >
					<!--- if no updating tables, then don't need reset button - probably need a better test --->
					<cfset resetbutton = "">
				</cfif>

			<cfif frmButtonAsLink>
				<!--- NYB 2009-02-26 Sophos Stargate 2 - added ' class="buttonRow'
					NJH 2012/11/07 - Case 431486 - change htmlEditFormat to sanitiseHTML
				 --->
				 <div class="form-group">
				 	<div class="buttonContainer col-xs-12 col-sm-9 col-sm-offset-3">
						<cfif trim(resetbutton) is not "" and resetButton neq "phr_blank">
							<a id="screenResetButton" class="btn btn-primary" href="#ResetButtonAction#">#application.com.security.sanitiseHTML(ResetButton)#</a>&nbsp;&nbsp;
						</cfif>

						<cfif continuebutton is not "">
							<a id="screenContinueButton" class="btn btn-primary" href="#ContinueButtonAction#">#application.com.security.sanitiseHTML(ContinueButton)#</a>&nbsp;&nbsp;
						</cfif>

						<cfif button3 is not "">
							<a id="screenButton3" class="btn btn-primary" href="#Button3Action#">#application.com.security.sanitiseHTML(button3)#</a>&nbsp;&nbsp;
						</cfif>
					</div>
				</div>
			<cfelse>
				<!--- NYB 2009-02-26 Sophos Stargate 2 - added ' class="buttonRow' --->
				<div class="form-group">
					<div class="buttonContainer col-xs-12 col-sm-9 col-sm-offset-3">
						<cfif resetbutton is not "">
							<CF_INPUT id="screenResetButton" type="Button" onclick="#ResetButtonAction#" value="#ResetButton#" class="screenButton btn btn-primary">
						</cfif>

						<cfif continuebutton is not "">
							<CF_INPUT id="screenContinueButton" type="Button" onclick="#ContinueButtonAction#" value="#ContinueButton#" class="screenButton btn btn-primary">
						</cfif>

						<cfif button3 is not "">
							<CF_INPUT id="screenButton3" type="Button" onclick="#Button3Action#" value="#button3#" class="screenButton btn btn-primary">
						</cfif>
					</div>
				</div>
				</cfif>
			</cfoutput>
		</cfif>
	</CFFORM>
</cfif>
<!--- postinclude a file if required --->
<cfif getScreen.postinclude is not ""> <!--- getScreen is returned by cf_ascreenitem--->
		<cfinclude template = "/#getScreen.postinclude#">
</cfif>



<!-- END: showScreenMainTable -->

<!--- put this function at very end so that if the page errors,
	the viewer just naturally aborts the page --->
<!--- <SCRIPT type="text/javascript">
	function jsOkToAbort() {
	//-----------------
		return false
	//  no
	}
</script>
--->

<cfsetting enablecfoutputonly="no">







