<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Person Language --->

<CFIF thislineMethod IS "Edit">

	<!--- find all Languages --->
	<CFQUERY NAME="findLanguages" DATASOURCE=#application.SiteDataSource#>
	SELECT Language
	FROM Language
	ORDER BY Language
	</CFQUERY>

<CFELSEIF thisLineMethod IS "View">

	<!--- find this Person's language --->
	<CFQUERY NAME="findLanguage" DATASOURCE=#application.SiteDataSource#>
	SELECT Language
	FROM Language
	WHERE Language =  <cf_queryparam value="#person.Language#" CFSQLTYPE="CF_SQL_VARCHAR" > 
	</CFQUERY>

</CFIF>
	
<CFIF thislineMethod IS "Edit">

	<CFOUTPUT>
	<SELECT NAME="person_language_#frmpersonid#">
		<OPTION VALUE="" #IIF(getDetails.language IS "", DE(" SELECTED"), DE(""))#>Default</OPTION></CFOUTPUT>
		<CFOUTPUT QUERY="findLanguages">
		<OPTION VALUE="#Language#"#IIF(getDetails.language IS Language, DE(" SELECTED"), DE(""))#>#HTMLEditFormat(Language)#</OPTION>
		</CFOUTPUT>
		<CFOUTPUT>
		</SELECT> <!--- Default language is English --->
		<CF_INPUT TYPE="HIDDEN" NAME="person_language_#frmpersonid#_orig" VALUE="#getDetails.language#">	
		</cfoutput>
	<CFSET personFieldList = listAppend(personFieldList,"person_language")>

	<CFIF required is not 0>
		<CFSET javascriptVerification = javascriptVerification & "msg = msg + verifyObject('person_language_#frmpersonid#',' * choose a language','#required#');" >
	</cfif>
	
<CFELSEIF thisLineMethod IS "View">

 	<CFOUTPUT>#HTMLEditFormat(findLanguage.Language)#</CFOUTPUT>
		  
</CFIF>	  
			






