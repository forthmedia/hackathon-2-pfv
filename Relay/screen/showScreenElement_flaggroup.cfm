<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
WAB:  2005-01-24 	Modifications to javascript verification , can use variable requiredMessage to override the default message
WAB 2006-02-09 made sure that javascript verification phrases getthe translated flagname
WAB 2006-03-07   modified to use application. flaggroup structure

WAb 12-02-2007    problems with the javascript verification messages (got same message for each item)

WAB 2007/01/17  changes to do with flagGroupFormatting Parameters 
WAB/ NJH 2009/02/26 Bug Fix All Sites Issue 1902 - set thisEntity to be variables.#getFlagGroup.entitytype.uniquekey# as when it was countryId that we
				were trying to set, countryID was always 0 as it was coming from the screenDef query.
2014-08-26	REH	Case 441394 Spaces in email addresses
 --->
 
	 
<cfif application.com.flag.doesFlagGroupExist(thisFieldTextID)>

	<cfset getFlagGroup = application.com.flag.getFlagGroupStructure(thisFieldTextID)>
	<cfset flaglist = "">
	<cfset entitytypename =  getflaggroup.entitytype.tablename>  <!--- listgetat(application.typeentitytable,getflaggroup.entitytypeid+1) --->
	<cfset entitytype =  getflaggroup.entitytypeid>  <!--- listgetat(application.typeentitytable,getflaggroup.entitytypeid+1) --->
	<!--- START: 2008-10-28	AJC		P-SOP003	File Meta Data - Add abiltiy to assign Screen to a fileGroup --->
	<cfset thisentity = evaluate("variables.#getFlagGroup.entitytype.uniquekey#")>  <!--- Bug Fix All Sites Issue 1902 - WAB/ NJH 2009/02/26 added "variables." - when entityTypeName is country we have to distinguish this countryid from the countryid in the encompassing screendefinition query --->
	
	<!--- END: 2008-10-28	AJC		P-SOP003	File Meta Data - Add ability to assign Screen to a fileGroup --->
	<cfset flagmethod = #thislinemethod#>
	<cfset flagformat=specialformatting>	
	<!--- wab 2007/11/06 these added as part of the flagformatting project --->
	<cfif maxLength is not 0 and maxLength is not "">
		<cfset specialFormattingCollection.maxLength = maxLength>
	</cfif>
	<cfif size is not 0 and size is not "">
		<cfset specialFormattingCollection.size = size>
	</cfif>

	<cfset flagfieldmaxlength=maxlength> 
	<cfset flagfieldsize=size>

<cfinclude template="/flags/FlagGroup.cfm">

<!--- 
WAB 2008/11/12 Note
To fix another bug I moved the required code for radio and checkbox groups into the editFlagRadio and editFlagCheckBox templates
Eventually need to do other flag types as well, but in the meantime this code can live happily here because of the findnocase test
 --->

<cfif required is not 0 and thislinemethod is "edit">

	<cfloop list="#flagList#" index="thisFlagGroup">

		<cfif thisflaggroup is not "">			

				<!---  WAB 2004-09-29 discovered that the javascript verification was appearing twice if the flag in question was a text box with valid values (may have been other cases)
						required was already being dealt with by showvalidvaluelist.cfm
						therefore added a test here to see whether this item was already in the verification string
				--->
				<!--- 2004-10-13
					note that the message for a single checkbox isn't really quite right 
					it talks about "select at least one item for .."  rather than saying "must select ..."
				--->
			<cfif findnocase ("#thisFlagGroup#_#thisentity#",javascriptVerification) is 0 >

				<cfif listgetat(thisflaggroup, 1, "_") is "Checkbox" or listgetat(thisflaggroup, 1, "_") is "Radio">
 					<cfquery name="getDetail" datasource="#application.SiteDataSource#">
						SELEcT flagtypeid,
							case when isNull(flaggroup.namePhrasetextID,'') <> '' then 'phr_' + flaggroup.namePhrasetextID else flaggroup.name end as Name  <!--- WAB 2006-02-09 --->
						FROM FlagGroup 
						WHERE FlagGroupID =  <cf_queryparam value="#ListGetAt(thisFlagGroup, 2, "_")#" CFSQLTYPE="CF_SQL_INTEGER" >  
					</cfquery>

 				<cfelse>
					<cfquery name="getDetail" datasource="#application.SiteDataSource#">
						SELEcT fg.flagtypeid,
							case when isNull(f.namePhrasetextID,'') <> '' then 'phr_' + f.namePhrasetextID else f.name end as Name 
						FROM Flag f INNER JOIN FlagGroup fg on f.flagGroupID=fg.flagGroupID
						WHERE FlagID =  <cf_queryparam value="#ListGetAt(thisFlagGroup, 2, "_")#" CFSQLTYPE="CF_SQL_INTEGER" >  
					</cfquery>
				</cfif>
	
			
				<cfif requiredMessage is "">   
					<cfif requiredLabel is  "" >
						<cfset thisrequiredLabel = getDetail.Name>
					<cfelse>
						<cfset thisrequiredLabel = requiredLabel>
					</cfif>		

					<cfif getdetail.flagtypeid is 2>
						<cfset thisrequiredMessage = "phr_Sys_Screen_SelectAtLeastOneCheckboxFor #thisrequiredLabel#">
					<cfelseif getdetail.flagtypeid is 3 or usevalidvalues is 1>
						<cfset thisrequiredMessage = "phr_Sys_Screen_SelectAnOptionFor #thisrequiredLabel#">
					<cfelseif getdetail.flagtypeid is 6>
						<cfset thisrequiredMessage = "phr_Sys_Screen_EnterANumericValueFor #thisrequiredLabel#">
					<cfelse>
						<cfset thisrequiredMessage = "phr_Screen_EnterAValueFor #thisrequiredLabel#">
					</cfif>
				<cfelse>
					<cfset thisRequiredMessage = requiredMessage>
				</cfif>
	
				<cfoutput><CF_INPUT type="HIDDEN" name="#thisFlagGroup#_#thisentity#_required" value="#thisrequiredMessage#"></cfoutput>
	
	<!--- 			<cfset javascriptverification = javascriptverification & "msg = msg + verifyObject('#thisFlagGroup#_#thisentity#','#getDetail.Name#','#required#');" > --->
				<CFSET javascriptVerification = javascriptVerification & "msg = msg + verifyObjectv2(form.#thisFlagGroup#_#thisentity#,'  #thisrequiredMessage#','#required#');" >					

			</cfif>	
										
		</cfif>

	</cfloop>

</cfif>

<!--- added WAB 2004-12-15
rather assumes that only one flag group being used!
 --->
<cfif trim(jsverify) is not "">
	<cfloop list="#flagList#" index="thisFlagGroup">
		<cfset thisLineFormField = "#thisFlagGroup#_#thisentity#"><!--- 2014-08-26 REH Case 441394 Spaces in email addresses - added thisLineFormField to work with groupflag validation taken from showScreenElement_flag.cfm --->
		<cfset javascriptverification = javascriptverification & "msg = msg + #evaluate("#jsverify#")#;" >
	</cfloop>
</cfif>							

<cfif flaglist is not "">
	<cfset "#entityTypeName#FlagList" = evaluate("#entityTypeName#FlagList") & "," & flaglist>
</cfif>
				
<cfelse>
	<cfoutput><font color="##FF0000">#htmleditformat(thisFieldTextID)# is not a valid FlagGroup.</font></cfoutput>
</cfif>


