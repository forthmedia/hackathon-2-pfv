<!--- �Relayware. All Rights Reserved 2014 --->
<!---
addLocation.cfm

Template for adding a new location.  Also adds an organisation if necessary
Called from updatedata.cfm

Author:WAB




Mods 2000-12-21  	Added code to put a countryid into the organisation table.
				Also use stored proc to reserve a locationid
2009/05/14		GCC made consumate with locationMatchName - p-sop008
2009/09/23		WAB LID 2663 organisation id variable seemed to have to be location_organisationid_new.  Made it work off formentityid
2011/09/27		WAB	P-TND109 altered to deal with non numeric organisationid
2016/09/01		NJH JIRA PROD2016-1190 - replaced old matching/dataload code with call to new matching functions and call to relayEntity.
--->


<!--- all fields defined in frmlocationfieldsonpage--->

<!--- dummy id variable  --->
<CFSET updatetime=now()>


<CFPARAM name="location_active_#formentityid#" default="1">
<CFPARAM name="location_direct_#formentityid#" default="0">

<CFPARAM name="location_lastupdatedby_#formentityid#" default="#request.relayCurrentUser.userGroupId#">
<CFPARAM name="location_createdby_#formentityid#" default="#request.relayCurrentUser.userGroupId#">
<CFPARAM name="location_created_#formentityid#" default="#frmdate#">
<CFSET "location_lastupdated_#formentityid#" = updatetime>

<CFSET sitename = evaluate("location_sitename_#formentityid#")>
<CFSET address1 = evaluate("location_address1_#formentityid#")>
<CFSET address4 = evaluate("location_address4_#formentityid#")>
<CFSET postalcode = evaluate("location_postalcode_#formentityid#")>
<CFSET countryid = evaluate("location_countryid_#formentityid#")>
<CFSET organisationid = evaluate("location_organisationid_#formentityid#")>  <!--- WAB 2009/09/23 added --->
<cfif not isNumeric (organisationid)>
	<!--- newly added organisation --->
	<cfset organisationid = evaluate(organisationid)>
</cfif>

<CFSET entityadded=false>


	<!--- check for locations with same Sitename, Address1  --->
	<!--- bit feeble, but prevents multiple form submissions! --->

	<cfscript>
		//checkexisting = application.com.dbTools.checkLoc(SiteName=SiteName,Address1=Address1,Address4=Address4,PostalCode=PostalCode,orgID=organisationid,countryid=countryID);
		checkexisting = application.com.matching.getMatchesForEntityDetails(entityType="location",SiteName=SiteName,Address1=Address1,Address4=Address4,PostalCode=PostalCode,orgID=organisationid,countryid=countryID);
	</cfscript>

	<!---  --->
	<CFIF checkexisting.recordcount is 0>


			<!--- does this location already belong to an organisation --->
			<!--- Do we have an OrganisationID? --->
			<CFPARAM name="location_organisationid_#formentityid#" default = "">
			<CFSET orgID = evaluate("location_organisationid_#formentityid#")>


		<CFIF orgID is "" >

			<!--- add an organisation--->
			<cfset orgDetails = {organisationName=SiteName,countryID=countryID,Active=1}>
			<cfscript>
				NewLocOrgID = application.com.relayEntity.insertOrganisation(organsiationDetails=orgDetails);
			</cfscript>

			<!--- <CFQUERY NAME="insNewOrganisation" datasource="#application.siteDataSource#">
				INSERT INTO Organisation (OrganisationID, OrganisationName, countryID,
				Active, CreatedBy, Created, LastUpdatedBy, LastUpdated)
				VALUES
				(#NewLocOrgID#, '#SiteName#',  #countryID#, 1,
				 #request.relaycurrentuser.usergroupID#,  #updatetime#,  #request.relaycurrentuser.usergroupID#, #updatetime#)
			</CFQUERY> --->

			<!--- add to orgDataSource as
				 1 - manually input

			<CFQUERY NAME="insOrgDataSource" datasource="#application.siteDataSource#">
				INSERT INTO OrgDataSource(OrganisationID, DataSourceID, RemoteDataSourceID)
				VALUES(<cf_queryparam value="#NewLocOrgID#" CFSQLTYPE="CF_SQL_INTEGER" >,1,'0')
			</CFQUERY>--->

			<CFSET "location_organisationid_#formentityid#" = NewLocOrgID>

		<CFELSEIF not isNumeric(orgID) and isDefined(orgID)>

			<CFSET "location_organisationid_#formentityid#" = evaluate(orgID)>

		</CFIF>


			<!--- get definition of the table --->
			<CFSET tablerequired = "location">
			<CFINCLUDE template="qryGetTableDefinition.cfm">

			<!--- insert location --->
			<cfset paramSet = structNew()>
			<!--- <cfset paramSet.datasource = application.sitedatasource > --->

			<CFLOOP index="fieldname" list= "#textfields#">
				<CFIF structKeyExists(form,"location_#fieldname#_#formentityid#")>
					<CFSET paramSet[fieldname] = form["location_#fieldname#_#formentityid#"]>
				</CFIF>
			</CFLOOP>
			<CFLOOP index="fieldname" list= "#listappend(numericfields,datefields)#">
				<CFIF structKeyExists(form,"location_#fieldname#_#formentityid#")>
					<CFSET paramSet[fieldname] = form["location_#fieldname#_#formentityid#"]>
					
				</CFIF>
			</CFLOOP>

			<!---
<cfset locMatchKeyList=application.com.dbTools.getLocationMatchnameEntries()>
			<cfset locMatchName = "">
			<cfloop list="#locMatchKeyList#" delimiters="#application.delim1#" index="keyItem">
				<cfif locMatchName eq "">
					<cfset locMatchName = evaluate(keyItem)>
				<cfelse>
					<cfset locMatchName = locMatchName & " #application.delim1# " & evaluate(keyItem)>
				</cfif>
			</cfloop>
			<cfset paramSet['matchName']="#locMatchName#">
 --->

			<cfset NewLocID = application.com.relayEntity.insertLocation(locationDetails = paramset).entityID>

			<!--- <CFQUERY NAME="insNewLocation" datasource="#application.siteDataSource#">
			INSERT INTO Location (LocationID
				<CFLOOP index="fieldname" list= "#textfields#">
					<CFIF isdefined("location_#fieldname#_#formentityid#")>
						, #fieldname#
					</CFIF>
				</CFLOOP>
				<CFLOOP index="fieldname" list= "#listappend(numericfields,datefields)#">
					<CFIF isdefined("location_#fieldname#_#formentityid#")>
						, #fieldname#
					</CFIF>
				</CFLOOP>
				)
			VALUES (#NewLocOrgID#

				<CFLOOP index="fieldname" list= "#textfields#">
					<CFIF isdefined("location_#fieldname#_#formentityid#")>
						<CFSET fieldvalue = evaluate("location_#fieldname#_#formentityid#")>
						, '#fieldvalue#'
					</CFIF>
				</CFLOOP>
				<CFLOOP index="fieldname" list= "#listappend(numericfields,datefields)#">
					<CFIF isdefined("location_#fieldname#_#formentityid#")>
					, #evaluate("location_#fieldname#_#formentityid#")#
					</CFIF>
				</CFLOOP>

			)

			</CFQUERY>

			<!--- just check that record added --->
			<CFQUERY NAME="getLocID" datasource="#application.siteDataSource#">
				SELECT LocationID
				FROM Location
				WHERE CreatedBy = #request.relaycurrentuser.usergroupID#
				  AND Created = #frmdate#
				  AND LastUpdatedBy = #request.relaycurrentuser.usergroupID#
				  AND LastUpdated = #updatetime#
				  AND sitename = '#sitename#'
			</CFQUERY>
--->



			<!--- <CFQUERY NAME="insLocDataSource" datasource="#application.siteDataSource#">
				INSERT INTO LocationDataSource(LocationID, DataSourceID, RemoteDataSourceID)
				VALUES(<cf_queryparam value="#newLocID#" CFSQLTYPE="CF_SQL_INTEGER" >,1,'0')
			</CFQUERY> --->

			<CFSET newentityID=newLocID>
			<CFSET entityadded=true>
			<CFSET message = message & "'#Trim(SiteName)#' was added successfully.<BR>">

	<CFELSE>

		<CFSET entityadded=false>
		<CFSET message = message & "'#Trim(SiteName)#' already exists, not added<BR>">
		<CFSET newentityid = checkexisting.locationid>		<!--- set this anyway --->

	</CFIF>