<!--- �Relayware. All Rights Reserved 2014 --->
	<!--- get valid values for a given fieldname (variable of form fieldsource_fieldtextid) --->
	<!--- returned in variable of form fieldsource_fieldtextid_validValues --->

	<!--- also needs to have countryid, and countryandRegionList for current location --->

<!--- Mods:
2000-01-06	reworked the way that lookups work - query defined in the valid value database
2000-01-06  now returns additional lists  xx_validvaluedisplay and xx_validvaluedata
2000-09-18 WAB  altered way *lookup queries are defined in the database.  No longer needs quotes around select statement and can use a | separator between the query to get the list of values and the query to lookup an individual value
 --->		
	
	<!---  bit of a hack because the validValue table structure uses a . rather than a _ as its separator--->


		<CFPARAM name="translateValidValues" default="0"> 
<CFSET lookupvalue = "">	<!--- tells query to bring back list, not single value --->

		<CFINCLUDE template = "qryvalidvalue.cfm">

			<CFPARAM name="parentFieldName" default = "">

		<CFIF getValidValues.recordCount IS 0 and parentFieldName is not "">
			<!--- if no valid vlaues then, have a look for valid values inherited from a parent --->
			<CFSET tmpFieldName = fieldName> <!--- bit of a hack here to store current value of fieldName while running qryValidValue.cfm --->
			<CFSET FieldName = parentFieldName>

			<CFINCLUDE template = "qryvalidvalue.cfm">
	
			<CFSET FieldName = tmpfieldName>
			<CFSET parentFieldName = "">
		
		</cfif>

		<!---  runs query getvalidvalues--->	

		<CFIF getValidValues.displayvalue is "*lookup">		
			<!--- valid values need looking up --->
			<CFSET thisquery = listFirst(getValidValues.datavalue,"|") >
			<CFSET thisQuery = evaluate('"#thisquery#"')>
			<!---  runs another query getvalidvalues--->	
			<cFQUERY NAME="getValidValues" DATASOURcE="#application.SiteDataSource#">
				#preservesinglequotes(thisQuery)#
			</CFQUERY>	
		</CFIF>


		<CFIF getValidValues.recordcount is 0>
			<!--- no valid values found --->
	<CFSET "#fieldname#_validvalues"  = " " & application.delim1 & "No Valid Values">
			<CFSET "#fieldname#_validvaluesdisplay"  = "No Valid Values">
			<CFSET "#fieldname#_validvaluesdata"  = "">
		<cfelse>
			<CFSET "#fieldName#_validvalues" = valuelist(getValidValues.thisvalidvalue)>
			<CFSET "#fieldname#_validvaluesdisplay"  = valuelist(getValidValues.displayvalue)>
			<CFSET "#fieldname#_validvaluesdata"  = valuelist(getValidValues.datavalue)>
		</CFIF>
		
		<!--- variable invalidvalues has been set in template qryvalidvalue included earlier --->
		<CFIF invalidvalues is not "">
			<!--- need to remove values which aren't allowed --->
			<CFLOOP index="thisInvalidValue" list ="#invalidValues#" >
				<!--- check for match on displayValue and then dataValue--->
				<CFSET positionOfInvalidValue = listfindNoCase(valuelist(getValidValues.displayValue), thisInvalidValue) >
		<!--- 		<CFOUTPUT>#valuelist(getValidValues.displayValue)#.  #thisInvalidValue#</cfoutput>
		 --->		<CFIF positionOfInvalidValue IS 0>
					<CFSET positionOfInvalidValue = listfindNoCase(valuelist(getValidValues.dataValue), thisInvalidValue) >
				</cfif>
				<!--- If match on either, then delete from list --->
				<CFIF positionOfInvalidValue IS NOT 0>
					<CFSET "#fieldName#_validvalues" = listdeleteat(evaluate("#fieldName#_validvalues"),positionOfInvalidValue)>
					<CFSET "#fieldname#_validvaluesdisplay"  = listdeleteat(evaluate("#fieldName#_validvaluesdisplay"),positionOfInvalidValue)>
					<CFSET "#fieldname#_validvaluesdata"  = listdeleteat(evaluate("#fieldName#_validvaluesdata"),positionOfInvalidValue)>
				</CFIF>				
			</cfloop>
			
		</cfif>

		<CFSET translateValidValues = 0>  <!--- reset to default value --->
