<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 

Used to view a screen which has been defined in the Screen Table

It is the template which is always called by the viewer when a new screen is requested

It is also used in non - viewer applications 


Author WAB

Date: Sometime in 1999


Mods:
many many
2001-05-16		added CF_translate around outside
2002-03-18	added frmNoBorder


Parameters:
	frmCurrentScreenID   required

	Some other parameters will be used if they are defined
	If being called from the viewer then frmInViewer will be set
	will test for personidsadded  (a variable set in updatedata.cfm - in which case the new entities will be added to those in the viewer
	similarly will test for locationidsadded		
	
	
	Any number of other parameters might be passed through it ! (OK not very helpful)
	
	eg: frmEntityType & frmCurrentEntityID
	frmNoBorder=true turns borders off
	DAM well should have made sure there was a default value see below
	...

--->

<CFPARAM NAME="frmNoBorder" DEFAULT="false">

<CFSETTING enablecfoutputonly="yes"> 

<!--- decipher any parameters passed in frmGlobalParameters --->
<!--- NJH 2009/06/30 P-FNL069 - removed frmGlobalParameters as it's a security hole
<CFINCLUDE template="..\screen\setParameters.cfm"> --->


<!--- this bit of code is used when adding people/locations.  Allows definition of screen to go to afer the new record has been saved, by listing the two screen ids separated by *  (yes its a hack)--->
<!--- actually been superceded I think and no longer used--->
<CFIF listlen (frmCurrentScreenId,application.delim1) is not 1>
	<CFSET nextScreenId = listdeleteat(frmCurrentScreenId,1,application.delim1)>
	<CFSET frmCurrentScreenId = listfirst(frmCurrentScreenId,application.delim1)>
</cfif>



<!--- Check existence of frmCurrentScreenId and translates from textid if necessary --->
	<CFQUERY NAME="getScreen" datasource="#application.siteDataSource#">
		SELECT * from screens 
		where 
		<CFIF isnumeric(frmCurrentScreenId)>
		screenid =  <cf_queryparam value="#frmCurrentScreenId#" CFSQLTYPE="CF_SQL_INTEGER" > 
		<CFELSE>
		screentextid =  <cf_queryparam value="#frmCurrentScreenId#" CFSQLTYPE="CF_SQL_VARCHAR" > 
		</cfif>	
	</CFQUERY>
	
	<CFIF getScreen.recordcount is not 0>
		<CFSET frmcurrentscreenid=getscreen.screenid>
	<CFELSE>
		<!--- 2008-06-25 NYF - gave error a slightly more help error message  --->
		<CFOUTPUT>Invalid screen id: "#frmCurrentScreenId#"</cfoutput>
		<CF_ABORT>
	</CFIF>

<!--- Set parameters defined in Screens Table --->	
	<CF_EvaluatenameValuePair nameValuePairs = "#getScreen.Parameters#">
	
<!--- Check whether user has correct rights for this screen --->
<!--- don't need to worry if dataTask - ought already have been checked in application .cfm 
	WAB: Nov 00 this bit can be got rid of.  I now automatically load application .cfm if any files in other directories are included
	Need to get rid of this column and use usergroups to determine who sees which screens
--->

<CFIF getScreen.securityTask is "">
<!--- WAB 2008/05/28 don't worry about blank security anymore	<CFOUTPUT>No Security Context<CF_ABORT></cfoutput> --->
<CFELSEIF #getScreen.securityTask# is not "dataTask">

	<CFSET securityList = "#getScreen.securityTask#:read">
	<CFSET PartnerCheck = true>
	<CFINCLUDE template = "..\templates\qryCheckPerms.cfm">
</CFIF>

<!--- display a border set if required - can be set as a parameter in the screen or maybe in process actions 
2009/04/23 NJH/GCC LID:2135 enabled currentsite to be used if defined before defaulting to application. StandardBorderSet--->
<!--- <cfif structkeyexists(request,"currentSite")>
	<CFPARAM name="borderset" default="#request.currentSite.BorderSet#">
<cfelse>
	<CFPARAM name="borderset" default="#application. StandardBorderSet#">
</cfif> --->
 
<cf_DisplayBorder>
<!--- <cf_translate > --->
<!--- <CFIF frmNoBorder is true><CFELSE><cf_DisplayBorder BorderSet="#BorderSet#"></cfif> --->

	<!--- Include any files before the main form on a screen, useful if additional forms are required --->
	<CFLOOP index="filetoinclude" list="#getscreen.preinclude#">
		<CFINCLUDE template="#fileToInclude#">
	</cfloop>
	
	<!--- Decide what is used to view this screen (showscreen, redirector, or just a cfm page etc..) --->
	<CFIF getscreen.viewer is "showscreen" or getscreen.viewer is "">
	
			<CFINCLUDE template="showscreennew.cfm">

	<CFELSEIF getscreen.viewer is "application" >
		<!--- WAB: this bit never went live! --->
		<CFIF not isdefined("processid")>
	
			<CFSET frmprocessid=getScreen.ProcessID>
			<CFSET frmstepid=getScreen.StartStepID>
			<CFSET appname = getScreen.appname>
	
			<cFQUERY NAME="getDetails" DATASOURcE="#Application.SiteDataSource#">
				Select * from 
				<CFIF frmentitytype is "person">
				person as p, Location as l where p.locationid=l.locationid and p.personid =  <cf_queryparam value="#frmcurrententityid#" CFSQLTYPE="CF_SQL_INTEGER" > 
				<CFELSEIF frmentitytype is "location">
				Location as l where l.locationid =  <cf_queryparam value="#frmcurrententityid#" CFSQLTYPE="CF_SQL_INTEGER" > 
				</CFIF>
			</CFQUERY>
			
			<CFIF frmentitytype is "person">
				<CFSET frmpersonid=getDetails.Personid>
				<CFSET frmlocationid=getDetails.locationid>
			<CFELSEIF frmentitytype is "location">
				<CFSET frmlocationid=getDetails.locationid>
				<CFSET frmpersonid=0>	
			</CFIF>
			
		</cfif>
	
	
		<CFSETTING enablecfoutputonly="no"> 	
		<CFINCLUDE template="redirector.cfm">
		<CF_ABORT>
	
	
		
	<CFELSE>
		<!--- otherwise just include the file specified in the viewer field 
		however first test whether the file is in another directory and
		if so include application .cfm if there is one, this gets the security right
		--->

		<CFSETTING enablecfoutputonly="no"> 

			<CFSET fileToInclude = getscreen.viewer>
			
			<!--- slightly long winded way of working out the name of the application .cfm to test for --->
			<CFSET applicationFileRelative = replaceNoCase(fileToInclude,listLast(fileToInclude,"/\"),"application.cfm","ALL")>
			<CFSET applicationFile = expandpath(applicationFileRelative) >

			<!--- check whether it is in different directory --->
			<CFIF getDirectoryFromPath(applicationFile) is not getDirectoryFromPath(getTemplatePath())>
				<!--- different directory so test for application .cfm and include it --->
				<CFIF fileExists(applicationFile)>
	
					<CFINCLUDE template="#applicationFileRelative#">
	
				</cfif>

			</cfif> 
			
			<CFTRY>
				
				<CFINCLUDE template="#fileToInclude#">
								
				<CFCATCH type="template">
					<CFOUTPUT>Unable to find page #htmleditformat(getscreen.viewer)#</cfoutput>
				</CFCATCH>
					
			</CFTRY>	 

		
	
	</cfif>
</cf_DisplayBorder>
<!--- <CFIF frmNoBorder is true><CFELSE></cf_DisplayBorder></cfif> --->
<!--- </cf_translate> --->


