<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 	Template for adding a new location
 		all fields defined in frmlocationfieldsonpage
	 	dummy id variable


2011/09/27	WAB		P-TND109  Improved called to relayDataload.insertOrganisation, removed redundant stuff
2016/09/01		NJH JIRA PROD2016-1190 - replaced old matching/dataload code with call to new matching functions and call to relayEntity.

--->
<CFSET updatetime=now()>


<CFPARAM name="organisation_lastupdatedby_#formentityid#" default="#request.relaycurrentuser.usergroupID#">
<CFPARAM name="organisation_createdby_#formentityid#" default="#request.relaycurrentuser.usergroupID#">
<CFPARAM name="organisation_created_#formentityid#" default="#frmdate#">
<CFSET "organisation_lastupdated_#formentityid#" = updatetime>



<CFSET entityadded=false>


	<CFSET organisationname = evaluate("organisation_organisationname_#formentityid#")>
	<!--- check for organisations with same name, countryid --->
	<!--- bit feeble, but prevents multiple form submissions! --->
	<!--- <CFQUERY NAME="CheckExisting" datasource="#application.siteDataSource#">
	SELECT * from organisation as o
	WHERE o.organisationname =  <cf_queryparam value="#organisationName#" CFSQLTYPE="CF_SQL_VARCHAR" >
	and o.countryid =  <cf_queryparam value="#evaluate("organisation_countryid_#formentityid#")#" CFSQLTYPE="CF_SQL_VARCHAR" >
	and o.created =  <cf_queryparam value="#frmdate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
	</CFQUERY> --->

	<cfset checkexisting = application.com.matching.getMatchesForEntityDetails(entityType="organsiation",organisationName=organisationName,countryid=evaluate("organisation_countryid_#formentityid#"))>

	<!---  --->
	<CFIF checkexisting.recordcount is 0>
		<!--- organisation does not already exist --->

			<!--- get definition of the table --->
			<CFSET tablerequired = "organisation">
			<CFINCLUDE template="qryGetTableDefinition.cfm">

			<!--- insert organisation --->
			<cfset paramSet = {}>

			<CFLOOP index="fieldname" list= "#textfields#">
				<CFIF structKeyExists(form,"Organisation_#fieldname#_#formentityid#")>
					<CFSET paramSet[fieldname] = form["organisation_#fieldname#_#formentityid#"]>
					<!--- <cfset paramSet[fieldname] = "'#fieldvalue#'"> --->
				</CFIF>
			</CFLOOP>
			<CFLOOP index="fieldname" list= "#listappend(numericfields,datefields)#">
				<CFIF structKeyExists(form,"organisation_#fieldname#_#formentityid#")>
					<CFSET paramSet[fieldname] = form["organisation_#fieldname#_#formentityid#"]>
				</CFIF>
			</CFLOOP>

			<cfset newOrgID = application.com.relayEntity.insertOrganisation(organisationDetails = paramSet).entityID>

			<CFIF newOrgID is 0>

				<CFSET entityadded=false>
				<CFSET message = message & "There was a problem adding '#Trim(OrganisationName)#'.  Please try again.">

			<CFELSE>

				<CFSET newentityid = newOrgID>

			<!--- <CFQUERY NAME="insOrgDataSource" datasource="#application.siteDataSource#">
				INSERT INTO OrgDataSource(OrganisationID, DataSourceID, RemoteDataSourceID)
				VALUES(<cf_queryparam value="#newOrgID#" CFSQLTYPE="CF_SQL_INTEGER" >,1,'0')
			</CFQUERY> --->

				<CFSET entityadded=true>
				<CFSET message = message & "'#Trim(OrganisationName)#' was added successfully.<BR>">

			</CFIF>




	<CFELSE>

		<CFSET entityadded=false>
		<CFSET message = message & "'#Trim(OrganisationName)#' already exists, not added<BR>">
		<CFSET newentityid = checkexisting.Organisationid>		<!--- set this anyway --->

	</CFIF>