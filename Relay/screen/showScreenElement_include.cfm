<!--- �Relayware. All Rights Reserved 2014 --->

<!--- cannot include a template which requires login security --->
<cfif request.relayCurrentUser.IsInternal or (FindNocase("admin/", #FieldTextID#) IS 0
	AND FindNocase("flags/", #FieldTextID#) IS 0
	AND FindNocase("data/", #FieldTextID#) IS 0
	AND FindNocase("communicate/", #FieldTextID#) IS 0
	AND FindNocase("emailprocs/", #FieldTextID#) IS 0
	AND FindNocase("report/", #FieldTextID#) IS 0
	AND FindNocase("salesOutData/", #FieldTextID#) IS 0
	AND FindNocase("scheduled/", #FieldTextID#) IS 0
	AND FindNocase("selection/", #FieldTextID#) IS 0)<!--- --->
	<!--- AND FindNocase("templates/", #FieldTextID#) IS 0 --->
	>

	<!--- SWJ: 2006-05-03 This is Will's way of solving a problem where we have cfsetting tags that we've not 
	closed in a file included above this. --->
	<cfsetting enablecfoutputonly="no">
	<cfsetting enablecfoutputonly="no">
	<cfsetting enablecfoutputonly="no">	
	<cfsetting enablecfoutputonly="no"> 
	<cftry>
		<cfif fieldsource is "IncludeRelayFile">
			<cfinclude template="/relay/#fieldTextID#">
		<cfelseif fieldsource is "IncludeUserFile">
			<cfinclude template="/code/#fieldTextID#"><!---2010/03/25 GCC removed userfile path as part of 8.2 RACE--->
		<cfelse>
			<cfinclude template="#fieldTextID#">  <!--- this is what was on Sony --->
			<!--- 	<cfinclude template="/content/#fieldTextID#"> ---> <!--- this is what was on FNLWWW1--->
		</cfif>

		
		<cfcatch type="MissingInclude">
			<cfoutput>
				<p style="color:##FF0000">The included file '#htmleditformat(FieldTextID)#' could not be found.</p>
			</cfoutput>
		</cfcatch>
	</cftry>

	<cfsetting enablecfoutputonly="yes"> 				
	
<cfelse>
	<cfoutput><p style="color:##FF0000">'#htmleditformat(FieldTextID)#' is not a valid page to include.  It may compromise data security.</p></cfoutput>
</cfif>
	

