<!--- �Relayware. All Rights Reserved 2014 --->
<cfsetting enablecfoutputonly="yes">
<!---

Mods:

WAB 170200  Added support for labels which are defined in terms of variables and need translating
WAB 2005-05-10	put support for defining row and cell classes in (has been in anotehr version of this code in the custom tags directory which is no longer used
WAB 2007-03-12  put  <cfif showthisline is true> a little higher up the code

WAB 2007/01/17  changes to do with flagGroupFormatting Parameters
	and different label layouts
WAB 2009/09/24 altered specialformatting collection to allow dot notation
WAB 2008/11/12 Bug 1327 moved requiredLabel code to below flag label code
WAB 2009/02/03  altered flagRights code to have an application.enforceFlagsRights, and support for individual lines to have ignoreFlagRights
WAB 2009/03/10 added handling of parameters passed from screen
NJH	2009/08/25	LID 2558- try to evaluate line condition. If it falls over, fall over gracefully by outputing red error msg.
NJH 2010/02/12	LID 3082 - have set a few needed variables when encountering an error while evaluating... I'm not entirely sure if Will would approve, but it appears to work! :)
NYB 2011/08/26 demo project - have added a workaround, as the rights code was causing the new Location screen to not work
WAB 2011/09/27	P-TND109 added support for (NoFlagName is 2) - which puts the flagname in the label (was already an option in the screen editor)
				added support for LabelOnOwnRow
AXA 2014-11-19  removed span/required where they were nested inside label/required
NJH 2015/02/12 Jira Fifteen-93 - span cols when allColumns is true for included files
WAb 2016-02-03	Added Some missing <cfoutputs>.  Only noticeable when used outside of cfform
2016-03-09 	WAB	 PROD2016-684 Added support for divLayout and try to get flags/flaggroups and their names to display properly
					Still work TBD on flagGroups 
				Removed all trace of tables and re-indented
--->

<cfset showthisline = true>

<!--- NJH 2009/08/25 LID 2558 - try to evaluate lineCondition --->
<cfset errorMsg = "">
<!--- check anyCondition on this Line --->
<cfif trim(thislinecondition) is not "">
	<cftry>
		<cfset showthisline = evaluate(thislinecondition)>
		<cfcatch>
			<cfset showthisline = true>
			<cfset showlabel = false> <!--- NJH 2010/02/12 LID 3082 --->
			<cfset thisLineMethod = method> <!--- NJH 2010/02/12 LID 3082 --->
			<cfset errorMsg = cfcatch.Message>
		</cfcatch>
	</cftry>
</cfif>

<!--- NJH 2009/08/25 LID 2558 -  added 'and errorMsg eq ""' --->
<cfif showthisline is true and errorMsg eq "">


	<cfset thisLineMethod = method> <!--- WAB 2007-02-26 thisLineMethod used to be column name in query, but needed to be able to change it on the fly so easier as a variable. --->
	<cfparam name="hideLabels" default="0">  <!---  --->

	<!--- WAB 2009/03/10 added - also passed from aScreenItem.cfm --->
	<cfparam name="screenFormattingCollection" default = "#structNew()#">

	<!--- WAB 2006-12-19 moved from showscreenelementquick so that values can be used earlier on --->
	<CFPARAM name="displayas" default="">
	<CFSET column= "3">				<!--- this default value can be overwritten on a row by row basis from specialformatting --->
	<cfset requiredMessage = "">	<!--- can be set in specialformatting and overrides the who of the Javascript required message --->

	<cf_evaluateNameValuePair namevaluepairs = "#specialformatting#" collectionName="specialFormattingCollection" DotNotationToStructure=true>  <!--- WAB 2008/09/24 added dotnotation--->
	<cfset structappend(specialFormattingCollection,screenFormattingCollection,false)>

	<!--- decide what label to use --->
	<cfset showlabel = true>
	<cfset thislabel = fieldlabel>
	<!--- allow for labels to be defined in terms of variables --->
	<cfif refind ("##[[:graph:]]*##",thislabel) is not 0>
		<cfset thislabel = evaluate(thislabel)>
	</cfif>

	<cfif trim(thislabel) is "">
		<cfset showlabel = false>
	<cfelseif translatelabel is 1>
		<cfset thislabel = "phr_#thisLabel#"><!--- WAB 2004-10-12 all translation done by cf_translate round outside of pages --->
	</cfif>

	<cfset thisfieldsource = fieldsource>
	<cfset thisfieldtextid = fieldtextid>
	<cfset thisevalstring = evalstring>

	<CFIF refind ("##[[:graph:]]*##",thisFieldSource) is not 0 or refind ("##[[:graph:]]*##",thisFieldTextID) is not 0>

		<cfif refind ("##[[:graph:]]*##",thisfieldsource) is not 0>
			<cfset thisfieldsource = evaluate("'#thisFieldSource#'")>
		</cfif>

		<cfif refind ("##[[:graph:]]*##",thisfieldtextid) is not 0>   <!--- finds pairs of # with no spaces in between, ie cold fusion variables--->
			<cfset thisfieldtextid = evaluate("'#thisFieldTextID#'")>
			<cfif thisfieldtextid is ""><cfset showthisline=false></cfif>
		</cfif>

		<cfif refind ("##[[:graph:]]*##",thisevalstring) is not 0>
			  <cfset thisevalstring = evaluate("'#evalString#'")>
		</cfif>

	</cfif>

	<cfset required = required>

	<cfif hidelabels is not 0>
		<cfset showlabel = false>
	</cfif>


	<!---
	Test for rights to flags and flaggroups here
		WAB 2007-02-26

	WAB 2009/02/03
	Added support for ignoring flag rights on a line by line basis
	Added support for enforcing flag rights on an application by application basis
	Added better refreshing of the flagRights session cache (flag.cfc)
	Much of the complication here is to deal with handling backwards compatibilty and migration

	parameter for
	ignoreFlagRightsForBackwardsCompatibility
	ignoreFlagRights
	--->

	<cfif
		(thisFieldSource is "flag" or thisFieldSource is "flaggroup")
			and
		(thislinemethod is "edit" or thislinemethod is "view" )
	>

		<cfset hasRights = false>
		<cfif thisFieldSource is "flag">
			<cfif  application.com.flag.DoesFlagExist(listFirst(thisFieldTextID))>
				<cfset hasrights = application.com.flag.doesCurrentUserHaveRightsToFlag (flagID = listFirst(thisFieldTextID), type=thisLineMethod)>
			</cfif>
		<cfelse>
			<cfif application.com.flag.DoesFlagGroupExist(listFirst(thisFieldTextID))>
				<cfset hasrights = application.com.flag.doesCurrentUserHaveRightsToFlagGroup (flagGroupID = listFirst(thisFieldTextID), type=thisLineMethod)>
			</cfif>

		</cfif>

		<cfif structKeyExists (specialFormattingCollection,"ignoreFlagRights")>
			<cfset ignoreRights = specialFormattingCollection.ignoreFlagRights>
		<cfelseif structKeyExists (specialFormattingCollection,"ignoreFlagRightsForBackwardsCompatibility")>
			<cfset ignoreRights = specialFormattingCollection.ignoreFlagRightsForBackwardsCompatibility>
			<!--- If user does not have rights, but rights are not being enforced we make a log entry --->
			<cfif ignoreRights and not hasRights>
				<cfset warningStructure= {ScreenDefItem = itemid,fieldSource =fieldSource,Person =#request.relayCurrentUser.personid# }>
				<cfset application.com.errorHandler.recordRelayError_Warning (severity="Information",Type="Flag Rights Overriden",Message="Flag Rights Overriden: " & thisFieldTextID,warningStructure=warningStructure,TTL=7 )>
			</cfif>
		<cfelse>
			<cfset ignoreRights = false>
		</cfif>


		<cfif not hasRights and thisLineMethod is "edit">
			<!--- if doesn't have edit rights then check for view rights, if has them, then set this to a view only line --->
			<cfif thisFieldSource is "flag">
				<cfset hasrights = application.com.flag.doesCurrentUserHaveRightsToFlag (flagID = listFirst(thisFieldTextID), type="view")>
			<cfelse>
				<cfset hasrights = application.com.flag.doesCurrentUserHaveRightsToFlagGroup (flagGroupID = listFirst(thisFieldTextID), type="view")>
			</cfif>
			<cfif hasRights>
				<cfoutput><!-- no edit rights to flag #listFirst(thisFieldTextID)# --></cfoutput>
				<cfif not ignoreRights>
					<cfset thisLineMethod = "view">
				</cfif>
			</cfif>

		</cfif>

	<!--- If enforcing flag rights then set showThisLine,
		otherwise, if no rights then send an email to william --->
		<cfif not ignoreRights>
			<cfset showThisLine = hasRights>
		</cfif>

		<!--- <cfoutput>#enforcingrights#  #hasrights#</cfoutput>--->
		<cfif not hasRights>
			<cfoutput><!-- no rights to #thisFieldTextID# --></cfoutput>
		</cfif>

	<!--- NJH 2011/06/15 - added field level rights as part of security --->
	<cfelseif thislinemethod is "edit" or thislinemethod is "view">
		<cfif listFindNoCase("person,location,organisation,country",thisFieldSource)>
			<cfset rights = {edit=true,view=true}>
			<!--- START: NYB 2011/08/26 demo project - have added this workaround, as this rights code was causing the new Location screen to not work --->
			<cfset entityID=variables[application.entityType[application.entityTypeID[thisFieldSource]].uniqueKey]>
			<cfset entityType = thisFieldSource>
			<cfif entityID eq "new" and isDefined("location_organisationid_default")>
				<cfset entityID=location_organisationid_default>
				<cfif entityType eq "location">
					<cfset entityType = "organisation">
				</cfif>
			</cfif>
			<cfset hasRights = application.com.rights.doesUserHaveRightsToEntityField(entityID=entityID,entityType=thisFieldSource,permission=thisLineMethod,entityRights=rights,field=thisFieldTextID)>
			<!--- END: NYB 2011/08/26 demo project --->
			<cfif not hasRights and thisLineMethod eq "edit">
				<cfset hasRights = application.com.rights.doesUserHaveRightsToEntityField(entityID=variables[application.entityType[application.entityTypeID[thisFieldSource]].uniqueKey],entityType=thisFieldSource,permission="view",entityRights=rights,field=thisFieldTextID)>
				<cfset thisLineMethod = "view">
			</cfif>

			<cfset showThisLine = hasRights>

		</cfif>
	</cfif>

	<!--- WAB 2016-03-09 Added various logic to try to guarantee that Div layout would work in all ways 
			allColumns on flags/flaggroups is not compatible with Div Layout (and perhaps for other items as well
			Try to avoid flag/flaggroup labels being output by the flag code - do it all here
			If label is set make sure that flag/flagGroup names are not output by flag code
			Still may be issues outputting non boolean flag groups
	--->

	<cfif (thisFieldSource is "flag") OR (thisFieldSource is "flaggroup")>
		<cfset querySetCell (screenDefinitionQry,"allColumns",0,currentRow)>
	</cfif>
	
	<!--- If no label supplied then we can get automatically from the flags/flaggroup --->
	<cfif trim(thislabel) is "">
		<!--- WAB 2007-11-07 special case for showing flag name in the label column --->
		<cfif (thisFieldSource is "flag") and (not structKeyExists (specialFormattingCollection,"NoFlagName") OR specialFormattingCollection.NoFlagName is not 1)  and application.com.flag.DoesFlagExist(listFirst(thisFieldTextID))>
			<cfset thislabel = application.com.flag.getFlagStructure(listFirst(thisFieldTextID)).translatedName	>
			<cfset specialFormattingCollection.NoFlagName =  2> <!--- this shows that name has already been output --->
			<cfset showlabel = true>
		</cfif>
	
		<cfif (thisFieldSource is "flaggroup")  and (not structKeyExists (specialFormattingCollection,"NoFlagGroupName") OR specialFormattingCollection.NoFlagGroupName is not 1)  and application.com.flag.DoesFlagGroupExist(listFirst(thisFieldTextID))>
			<cfset flagGroupStructure = application.com.flag.getFlagGroupStructure(listFirst(thisFieldTextID))	>
			<cfset thislabel = flagGroupStructure.translatedName	>
			<cfset showlabel = true>
			<cfset specialFormattingCollection.NoFlagGroupName = 2>  <!--- this shows that name has already been output --->
		</cfif>
	<cfelse>
		<!--- If already a label then make sure that flag name not output as well --->
		<cfif (thisFieldSource is "flag")>
			<cfset specialFormattingCollection.NoFlagName =  1>
		</cfif>
		<!--- If already a label then make sure that flaggroup name not output as well --->
		<cfif (thisFieldSource is "flagGroup")>
			<cfset specialFormattingCollection.NoFlagGroupName =  1>
		</cfif>


	</cfif>

	<!--- NJH 2009/02/02 Moved this block of code inside the if statement Bug Fix All Sites Issue 1680 --->
	<!--- 2008/11/12 WAB moved this code to below code  which creates a label from a flag name --->
	<cfset requiredlabel = listfirst(thislabel,"<")> <!--- attempt to sort out problem of labels with html stuff in them (only get bit before opening html tag) --->

	<!--- special code for labels which aren't to be shown, but might be needed for 'required' text --->
	<cfif left(fieldlabel,1) is "^">
		<cfset showlabel = false>
		<cfset requiredlabel = mid(requiredlabel,2,len(requiredlabel)-1)>
	</cfif>

<!--- NJH 2009/08/25 LID 2558 - add new field source of type error. --->
<cfelseif errorMsg neq "">
	<cfset thisFieldSource = "error">
</cfif>


<cfif fieldSource IS "specialEffect" and fieldTextID IS "sectionHeader">
		<cfset sectionid = "section_#itemid#">
		<cfset thisLabel = "<A href=""javascript:void($('#sectionid#').show())"">+</A> / <A href=""javascript:void($('#sectionid#').hide())"">-</A> " & thisLabel >
</cfif>


<cfif showthisline is true>   <!--- note that showThisLine can be made false by code above - eg if no rights to a flag --->

	<cfif listfindnocase("include,IncludeRelayFile,IncludeUserFile,query,hiddenfield,ScreenPostProcess,set,cfparam",thisFieldSource) or thislinemethod is "hidden">  <!--- WAB 2006-06-27 query type shouldn't have a row at all (can be done with keep with next, but this is nicer, could be extended to other types) --->
		<cfinclude template="/screen/showScreenElementQuick.cfm">
	<cfelse>
		<cfif keepwithprevious is false >
			<!--- only start new row if not keep with previous --->
			<cfset allColumns = false>
			<cfif not listfindnocase("include,IncludeRelayFile,IncludeUserFile,html,SpecialEffect",thisFieldSource)>
				<!--- NJH 2015/09/11 - add an id to the div --->
				<cfoutput><div class="form-group showScreenTable1" <cfif listFindNoCase("flag,flagGroup",thisFieldSource)>id="control__#listFirst(thisFieldTextID)#"</cfif>></cfoutput>
			<cfelse>
				<cfoutput><div></cfoutput>
			</cfif>
		
			<!--- Output label bit --->
			<cfif allcolumns is false >
				<!--- label in single column--->
				<cfif showlabel is true>
							<cfoutput>
								<div <cfif divLayout is "horizontal">class="col-xs-12 col-sm-3 control-label"</cfif> >
									<label <cfif required is not 0 and thislinemethod is "edit">class="required"</cfif>>
							</cfoutput>
						<cfoutput>#application.com.security.sanitiseHTML(thislabel)#</cfoutput>
							<cfoutput></label></div></cfoutput>
				<cfelse>
					<cfif not listfindnocase("include,IncludeRelayFile,IncludeUserFile,flaggroup,html,SpecialEffect",thisFieldSource)>
						<cfoutput><div <cfif divLayout is "horizontal">class="col-xs-12 col-sm-3 control-label" </cfif> >&nbsp;</div></cfoutput>
					</cfif>
				</cfif>


			<cfelse>
				<!--- label on multiple columns, paragraph after --->
				<cfif showlabel is true>
						<cfoutput>
							<div  id="showScreenTableLabel" <cfif divLayout is "horizontal">class="col-xs-12 col-sm-3 control-label"</cfif>>
								<label <cfif required is not 0 and thislinemethod is "edit">class="required"</cfif>>
						</cfoutput>
						<cfoutput>#application.com.security.sanitiseHTML(thislabel)#</cfoutput>
						<cfoutput></label></div></cfoutput>
				</cfif>
			</cfif>
		<cfelse>

			<cfif showlabel is true>
				<div <cfif divLayout is "horizontal">class="col-xs-12 col-sm-3 control-label"</cfif>  ><label>
				<!--- just output field label --->
				<cfif required is not 0 and thislinemethod is "edit">
					<cfoutput><span class="required"></cfoutput>
				</cfif>
				<cfoutput>#application.com.security.sanitiseHTML(thislabel)#</cfoutput>
				<cfif required is not 0 and thislinemethod is "edit">
					<cfoutput><span class="required"></cfoutput>
				</cfif>
					</label></div>
			</cfif>
		</cfif>

		<cfif isdefined("session.showScreenEditLink")>
			<cfoutput><a href="javascript:editScreenDef(#itemid#)" > <img src="/images/buttons/editform.gif" width=13 height=12 border=0 alt="Edit record"></a>&nbsp;</cfoutput>
		</cfif>

		<cfinclude template="/screen/showScreenElementQuick.cfm">

		<!--- close off row if required --->
		<cfif breakln is not true>
				<cfoutput></div></cfoutput>
			<cfset keepwithprevious = false>
		<cfelse>
			<cfset keepwithprevious = true>
		</cfif>

	</cfif>

</cfif>

<cfsetting enablecfoutputonly="no">

