<!--- �Relayware. All Rights Reserved 2014 --->
<!--- WAB 2007-01-23   changed to a cfswitch from a cfif 
	WAB 2008/01/09    altered relatedFile
	NJH 2009/08/25		LID 2558 - added extra case value of 'error' when line condition could not be evaluated.
	2011/05/25 	NYB		REL106 added support for non PLO entities - to make screen code more generic
	2015-12-14 WAB 		BF-79 Fix in relatedFile section to deal with cases where uniqueKey field is not just #tablename#ID 
--->

<CFSETTING enablecfoutputonly="yes"> 


<!--- 
WAB 2006-12-19 moved to showscreentable so that values can be used earlier on
<CFPARAM name="displayas" default="">
<CFSET column= "3">				<!--- this default value can be overwritten on a row by row basis from specialformatting --->
<cfset requiredMessage = "">	<!--- can be set in specialformatting and overrides the who of the Javascript required message --->



 --->

<!--- any name value pairs defined in special formatting column are set --->
<!--- <CFLOOP list="#specialformatting#" index="namevaluepair">
	<CFIF listlen(namevaluepair,"=") is 2>   <!--- some older stuff doesn't have name value pairs --->
		<cfset variablename = trim(listfirst(namevaluepair,"="))>
		<cfset value = listgetat(namevaluepair,2,"=")>
		<CFSET "#variablename#" = value>
	</cfif>
</CFLOOP> 
 --->

<!--- outputs a single screen element defined in screendefinition
included from showscreen.cfm --->
<cfswitch expression = #thisFieldSource#>
	<!--- 2011/05/25 NYB REL106 added locatordef to the list --->
	<cfcase value="location,person,country,region,organisation,locatordef,activityType">
		<CFINCLUDE template="showScreenElement_main.cfm">
	</cfcase>
	<cfcase value="include,IncludeRelayFile,IncludeUserFile">
		<CFINCLUDE template="showScreenElement_include.cfm">
	</cfcase>		
	<cfcase value="flaggroup,flag,html,SpecialEffect">
		<CFINCLUDE template="showScreenElement_#thisFieldSource#.cfm">
	</cfcase>		
	<cfcase value="allpersonflags,alllocationflags,allelementflags,allorganisationflags">
		<CFINCLUDE template="showScreenElement_AllFlags.cfm">
	</cfcase>		
	<cfcase value="text">
		<cFOUTPUT>#htmleditformat(FieldTextID)#</cFOUTPUT>
	</cfcase>		
	<cfcase value="phrase">
		 <cFOUTPUT>phr_#htmleditformat(FieldTextID)#</cFOUTPUT>
	</cfcase>		
	<cfcase value="FullAddress">
		<CFOUTPUT>#application.com.screens.evaluateAddressFormat (location = location)#
		<!--- #evaluate(application.addressFormat[variables.Countryid])# --->
		</CFOUTPUT>		
	</cfcase>		
	<cfcase value="link">
		<!--- this code originally from reports! --->
		<CFIF findnocase("gif",fieldtextid)  is not 0>
			<CFSET anchortext = "<IMG SRC='/images/MISC/#fieldtextid#' BORDER=0>">
		<CFELSE>
			<CFSET anchortext = evaluate(fieldtextid) >
		</CFIF>		
		<CFOUTPUT><a HREF="#evaluate(thisevalstring)#">#htmleditformat(anchortext)#</A></CFOUTPUT>		
	</cfcase>		
	<cfcase value="flaglist">
		<CFSET flaggrouptextid=fieldtextid>
		<CFSET thisentity=""><!--- will be worked out when flag type is calculated --->
		<CFINCLUDE TEMPLATE="../flags/listFlagGroup.cfm">
		<CFOUTPUT>#htmleditformat(evaluate(thisevalstring))#</cfoutput>
	</cfcase>		
	<cfcase value="checkbox">
		<CFPARAM name="#fieldtextid#" default="">
		<CFOUTPUT><CF_INPUT TYPE="CHECKBOX" NAME="#fieldtextid#" VALUE="#evaluate(thisevalstring)#"  CHECKED="#iif(ListFind(evaluate(fieldtextid),evaluate(thisevalstring)) IS NOT 0,true,false)#"> </cfoutput>
	</cfcase>		
	<cfcase value="selection">
		<CFOUTPUT><CF_INPUT TYPE="CHECKBOX" NAME="#thisFieldSource#_#fieldtextid#" VALUE="#evaluate(thisevalstring)#"  CHECKED="#iif(ListFind(evaluate("thisFieldSource_fieldtextid"),evaluate(thisevalstring)) IS NOT 0,true,false)#"> </cfoutput>	
	</cfcase>		
	<cfcase value="value">
		<cftry><CFOUTPUT>#htmleditformat(evaluate(thisevalstring))#</cfoutput>						
			<cfcatch><!--- WAB added a catch so that errors don't appear - might be worth sending an email though.  Hidden Comment put in the output --->
				<cfoutput><!--- VALUE could not be evaluated: #cfcatch.message# #thisevalstring# ---></cfoutput>
			</cfcatch>
		</cftry>
	</cfcase>		
	<cfcase value="set">
		<CF_EvaluateNameValuePair nameValuePairs="#fieldTextID#">
	</cfcase>		
	<cfcase value="CFBREAK,CFSET,CFPARAM">
		<CFIF thisFieldSource is "CFBREAK">
			<CFSET breakScreen = true>
		<CFELSEIF thisFieldSource is "CFSET">
			<CF_EvaluateNameValuePair nameValuePairs="#thisfieldTextID#">
		<CFELSEIF thisFieldSource is "CFPARAM">
			<CF_EvaluateNameValuePair nameValuePairs="#thisfieldTextID#" mode="param">
		</cfif>
	</cfcase>		
	<cfcase value="HiddenField">
		<!--- 	<cfif refind ("##[[:graph:]]*##",thisevalstring) is not 0>
				  <cfset thisevalstring = evaluate("'#evalString#'")>
				</cfif>
 			--->	
		 <cfoutput><CF_INPUT type="hidden" name="#thisFieldTextID#" value="#evaluate("'#evalString#'")#"></cfoutput>
	</cfcase>		
	<cfcase value="ScreenPostProcess">
		<cfoutput><CF_INPUT type="hidden" name="frmScreenPostProcessID" value="#thisfieldTextID#"></cfoutput>
	</cfcase>		
	<cfcase value="Label">
		<!--- already will have been outputted --->
	</cfcase>		
	
	<!--- NJH 2009/08/25 LID 2558 - added another case of error when the lineCondition could not be evaluated. --->
	<cfcase value="error">
		<cfparam name="errorMsg" type="string" default="">
		<cfoutput><span class="required">#htmleditformat(errorMsg)#</span></cfoutput>
	</cfcase>	
	<cfcase value="query">
		<CFSET queryString = #evaluate(evalstring)#>
		<!--- check for disallowed bits of sql 
			regExp should allow words which contain these words
		--->
		<cfset re = "(\A|[^A-Za-z0-9])(drop|create|truncate|insert|update|delete)(\Z|[^A-Za-z0-9])">
		<cfif reFindNoCase(re,queryString) is not 0>
			<cfoutput>disallowed query #htmleditformat(queryString)#</cfoutput><CF_ABORT>

		<cfelse>
			<CFQUERY name="#fieldTextID#" dataSource="#application.sitedatasource#">
				#preserveSingleQuotes(queryString)#
			</cfquery>
		</cfif>	

	</cfcase>		
	<cfcase value="relatedFile">
			<!--- this functionality added by WAB 5/01
				room for lots more switches - this is a basic implementation
			 --->
			 <cfset filecategory = thisFieldTextID>
			 <cfquery name="getRelatedFileCategory" dataSource="#application.sitedatasource#">
				 select * from relatedFileCategory where
				 <cfif isNumeric(filecategory)>filecategoryID <cfelse>	 filecategory  	 </cfif>='#filecategory#'
			 </cfquery>
			<!--- WAB 2015-12-14 BF-79 use getEntityType to get uniqueKey field (for cases where uniqueKey is not just #tablename#ID) --->
			<cfset relatedFileEntityID = evaluate(application.com.relayentity.getEntityType(getRelatedFileCategory.fileCategoryEntity).uniqueKey)>
			<CFPARAM name="allowReplace" default= "true">
		
			<!--- NJH 2009/07/27 P-FNL069 changed from v2 to relatedFile which then calls V2 --->
			<CF_relatedfile query="test" action="variables" entityType="#getRelatedFileCategory.fileCategoryEntity#" entity = "#relatedFileEntityID#" filecategory="#getRelatedFileCategory.fileCategory#" > 

			<cfif relatedFileVariables.filelist.recordcount is not 0>
				<!--- <cfoutput>Existing file(s): <BR></cfoutput> --->
				<cfif displayas is "image">
					<CFPARAM name="height" default= "">
					<CFPARAM name="width" default= "">
					<cfoutput query="relatedFileVariables.filelist">
					<IMG <cfif variables.height is not "">height=#htmleditformat(variables.height)#</cfif> <cfif variables.width is not "">width=#htmleditformat(variables.width)#</cfif> src="#htmleditformat(webhandle)#"><BR>
					</cfoutput>
				<cfelse>
					<cfoutput query="relatedFileVariables.filelist">
					<A HREF="#webhandle#" target="_blank">#htmleditformat(filename)#</A><BR>
					</cfoutput>
				</cfif>
			</cfif>		

			<CFIF thislineMethod IS "Edit" and (allowreplace is true or relatedFileVariables.filelist.recordcount is 0)>
				<cfoutput><CF_INPUT type="file" name="#relatedFileVariables.formfieldname.filename#" size="30"> </cfoutput>
				<CFSET caller.updateableFields = true>
				<cfif required gt relatedFileVariables.filelist.recordcount> 
					<!--- WAB 2010/10/20 support for required message --->
					<cfif requiredMessage is "">
						<cfset requiredMessage = "Enter a file for #requiredLabel#">
					</cfif>
					<CFSET javascriptVerification = javascriptVerification & "msg = msg + verifyObjectv2(form.#relatedFileVariables.formfieldname.filename#,'  #requiredMessage#','1');" > 
				</cfif>
			</cfif>
			
			
			<cfset filecategory = "">			
			<cfset allowreplace = true>

	</cfcase>		
	<cfdefaultcase>
		<CFIF left(thisFieldSource,5) is "Flag_">	
			<CFINCLUDE template="showScreenElement_complexFlag.cfm"> 
		<cfelseif  structKeyExists(application.entityTypeID,thisFieldSource)>
			<CFINCLUDE template="showScreenElement_main.cfm">
		<CFELSE>
			<CFOUTPUT><FONT cOLOR="##FF0000">Error: Unknown thisFieldSource '#htmleditformat(thisFieldSource)#'.</FONT></CFOUTPUT>
		</CFIF>

	</cfdefaultcase>


</cfswitch>

<!--- any name value pairs defined in special formatting column are set back to ""--->
<cf_evaluateNameValuePair namevaluepairs = "#specialformatting#" mode = "null">

<CFSETTING enablecfoutputonly="no">

