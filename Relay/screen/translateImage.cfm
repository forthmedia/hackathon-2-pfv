<!--- �Relayware. All Rights Reserved 2014 --->
<!--- custom tag which displays an image in the requested language --->
<!--- however, checks for existence first and will default to a default image --->
<!--- also returns caller.imageFullPath if output is false --->

<CFPARAM name="attributes.imageName">
<CFPARAM name="attributes.imagePath">
<CFPARAM name="attributes.imageType" default = "gif">
<CFPARAM name="attributes.languageCode" default="e">
<CFPARAM name="attributes.fallbackLanguageCode" default="e">
<CFPARAM name="attributes.alt"  default = "">
<CFPARAM name="attributes.height"  default = "">
<CFPARAM name="attributes.width"  default = "">
<CFPARAM name="attributes.border" default ="0">
<CFPARAM name="attributes.output" default ="true">

<CFSET fileToLookFor = replace("#application.path##attributes.imagePath#\#attributes.imageName#_#attributes.LanguageCode#.#attributes.imageType#","/","\","ALL")>

<CFIF fileExists(fileToLookFor)>
	<CFSET languageCode = attributes.LanguageCode>
<CFELSE>
	<CFSET languageCode = attributes.fallbackLanguageCode>
</cfif>

<CFSET imageFullPath = "#attributes.imagePath#/#attributes.imageName#_#languageCode#.#attributes.imageType#">

<CFIF attributes.output>
	<CFOUTPUT><IMG 
			SRc="#imageFullPath#"
			<CFIF attributes.height is not "">
				HEIGHT= "#htmleditformat(attributes.height)#"
			</cfif>
			<CFIF attributes.height is not "">
				WIDTH= "#htmleditformat(attributes.width)#"
			</cfif>
			ALT="#htmleditformat(attributes.alt)#"
			BORDER="#htmleditformat(attributes.border)#"
		></cfoutput><CFELSE>
	<CFSET caller.imageFullPath = imageFullPath>
</cfif>