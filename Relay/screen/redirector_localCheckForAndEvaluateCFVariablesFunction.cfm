	<cffunction name="localCheckForAndEvaluateCFVariables">
			<cfargument name="phraseText" required="true">
			<cfargument name="phraseTextID" default = "">

			<cfset var result =  phraseText>
			<cfset var temp =  phraseText>
			
					<!--- WAB 2006-05-30 replaced original code (with twoo ifs, with a single version --->
					<cfif refind ("\[\[[[:graph:]]*\]\]",temp) is not 0 or refind ("##[a-zA-Z_][[:graph:]]*##",temp) is not 0>
								<!---  convert any hashed cf variables with [[ ]] notation, we can then escape any other hashes, before switching [[ ]] back to #
										- WAB 2007-05-01 actually this caused problems if we had a variable like #x[1]# because it became [[x[1]]] which then became #x[1#]
										  so now I do something unique with * and [
								--->
							<CFset temp =  rereplaceNoCase (temp,"##([a-zA-Z_][[:graph:]]*)##","*[[*\1*]]*"  ,"ALL") >    <!--- variable must start with a letter - means that french such as d&#039;aujourd&#039;hui is not considered a variable! --->
								 
							<CFTRY>
								<!--- replace " with "", # with ## then replace [[xxx]] with #xxx# and evaluate --->
								<cfset  temp = replaceNoCase(temp,'"','""',"ALL")>
								<cfset  temp = replaceNoCase(temp,'##',"####","ALL")>
								<cfset  temp = replaceNoCase(temp,'*[[*',"##","ALL")>
								<cfset  temp = replaceNoCase(temp,'*]]*',"##","ALL")>						
								<cfset  temp = replaceNoCase(temp,'[[',"##","ALL")>
								<cfset  temp = replaceNoCase(temp,']]',"##","ALL")>						
								<!--- prevent any cfcs in application.com being accessed - will most likely cause a crash, but never mind--->
								<cfset  temp = replaceNoCase(temp,'application.com',"","ALL")>						
								<CFSET  result = evaluate('"#temp#"')>
							<CFCATCH>
								<!--- if the evaluate errors then we just continue, the result will be the original phrase --->
								<cfmail from ="william@foundation-network.com" to = "william@foundation-network.com" subject =  "#request.relaycurrentuser.sitedomain#: Redirector Phrase/String Evaluation Error. " type="html">
									<cfif phraseTextID is not "">Phrase: #phraseTextID#</cfif>
									String:  #phraseText# <BR>
									Trying to process:  evaluate('"#temp#"') <BR>
									Problem : #cfcatch.message# <BR>
								</cfmail>


										<cfoutput>
											<table border="1" cellpadding="2">
												<tr><th>Caught an exception, type = #htmleditformat(cfcatch.type)#</th></tr>	
											 	<tr><td>ProcID: #htmleditformat(processid)# StepID: #htmleditformat(stepid)# SequenceID: #htmleditformat(sequenceID)#</td></tr> 
												<tr><td>#htmleditformat(jumpexpression)# : #htmleditformat(jumpExpression)#</td></tr> 
												<tr><td>JumpAction: #htmleditformat(jumpaction)# </td></tr>
												<tr><td>jumpName : #htmleditformat(jumpname)#</td></tr>
												<tr><td>jumpValue : #htmleditformat(jumpvalue)#</td></tr>
												<tr><td>#htmleditformat(cfcatch.message)#</td></tr>
												<tr><td>#htmleditformat(cfcatch.detail)#</td></tr>
												<tr><td><P>The contents of the tag stack are:</P>
													  <cfloop index = i from = 1 to = #ArrayLen(cfcatch.tagContext)#>
													     <cfset sCurrent = #cfcatch.tagContext[i]#>
													       <BR>#i# #sCurrent["ID"]# 
															(#sCurrent["LINE"]#,#sCurrent["COLUMN"]#) 
															#sCurrent["TEMPLATE"]#
													  </cfloop>
												</td></tr>
											</table>
										</cfoutput>
										<cfbreak>
								</CFCATCH>
							</CFTRY>

					</CFIF>
				<cfreturn result>
		</cffunction>