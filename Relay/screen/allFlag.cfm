<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Requires the following variables set (may be FORM or URL or VARIABLE)

	flagMethod  	== 	edit | view | search
	entityType		==	1 | 0
	thisEntity		==	(LongInteger)		ID if flagMethod is view or edit


Mods:  WAB 2004-10-12 - all label done with cf_translate in calling templates

	--->



<CFPARAM name="flagformat" default=""><!--- something wab trying out --->

<!--- Get USER countries --->
<CFINCLUDE TEMPLATE="../templates/qrygetcountries.cfm">

<CFIF flagMethod IS "search">

	<!--- Get country groups for this user to find which FlagGroups with Scope they can view --->
	
	<!--- Find all Country Groups for this Country --->
	<!--- Note: no User Country rights implemented for site currently --->
	<CFQUERY NAME="getCountryGroups" datasource="#application.siteDataSource#">
	SELECT DISTINCT b.CountryID
	 FROM Country AS a, Country AS b, CountryGroup AS c
	WHERE (b.ISOcode IS null OR b.ISOcode = '')
	  AND c.CountryGroupID = b.CountryID
	  AND c.CountryMemberID = a.CountryID
	  AND a.CountryID  IN ( <cf_queryparam value="#Variables.CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) <!--- User Country rights --->
	ORDER BY b.CountryID
	</CFQUERY>
	
	<CFSET UserCountryList = "0,#Variables.CountryList#">
	<CFIF getCountryGroups.CountryID IS NOT "">
		<!--- top record (or only record) is not null --->
		<CFSET UserCountryList =  UserCountryList & "," & ValueList(getCountryGroups.CountryID)>
	</CFIF>
	
<CFELSE>

	<CFIF thisEntity IS NOT 0>
	
		<!--- Find Country for this Entity --->
		<CFQUERY NAME="getCountry" datasource="#application.sitedatasource#">
			SELECT DISTINCT l.CountryID
			<CFIF entityType IS 0>
			FROM Person As p, Location As l
			WHERE p.PersonID =  <cf_queryparam value="#thisEntity#" CFSQLTYPE="CF_SQL_INTEGER" > 
			AND p.LocationID = l.LocationID
			<CFELSE>
			FROM Location As l
			WHERE l.LocationID =  <cf_queryparam value="#thisEntity#" CFSQLTYPE="CF_SQL_INTEGER" > 
			</CFIF>
			<!--- 2012-07-30 PPB P-SMA001 commented out
			AND l.CountryID  IN ( <cf_queryparam value="#Variables.CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) <!--- User Country rights --->
			 --->
			#application.com.rights.getRightsFilterWhereClause(entityType="person",alias="p").whereClause# 	<!--- 2012-07-25 PPB P-SMA001 ---> 
 		</CFQUERY>
		
		<CFSET eCountryID = getCountry.CountryID>
	
	<CFELSE>
	
		<CFSET eCountryID = frmCountryID>
		
	</CFIF>
		
	<!--- Find all Country Groups for this Country --->
	<!--- Note: no User Country rights implemented for site currently --->
	<CFQUERY NAME="getCountryGroups" datasource="#application.siteDataSource#">
	SELECT DISTINCT b.CountryID
	 FROM Country AS a, Country AS b, CountryGroup AS c
	WHERE a.CountryID =  <cf_queryparam value="#eCountryID#" CFSQLTYPE="CF_SQL_INTEGER" >  <!--- Entity Country --->
	  AND (b.ISOcode IS null OR b.ISOcode = '')
	  AND c.CountryGroupID = b.CountryID
	  AND c.CountryMemberID = a.CountryID
	ORDER BY b.CountryID
	</CFQUERY>

	<CFSET EntityCountryList = "0,#eCountryID#">
	
	<CFIF findCountries.RecordCount GT 0>
		<CFIF getCountryGroups.CountryID IS NOT "">
			<!--- top record (or only record) is not null --->
			<CFSET EntityCountryList = EntityCountryList & "," & ValueList(getCountryGroups.CountryID)>
		</CFIF>
	</CFIF>


</CFIF>

<!--- use tmpflagmethod in the sql queries, these use viewing where everything else uses view --->
<CFSET tmpflagmethod=iif(flagMethod is "view", DE("viewing"),DE("#flagmethod#"))  >

<CFQUERY NAME="getTopLevelFlags" datasource="#application.sitedatasource#">
SELECT DISTINCT FlagGroup.FlagGroupID AS ID, FlagTypeID, OrderingIndex, Name, Description, NamePhraseTextId, DescriptionPhraseTextId
<CFIF flagMethod IS "edit">, 1 AS edit_view</CFIF>
FROM FlagGroup
WHERE Active <> 0
AND Expiry > #CreateODBCDateTime(Now())#
AND ParentFlagGroupID = 0
AND EntityTypeID =  <cf_queryparam value="#entityType#" CFSQLTYPE="CF_SQL_INTEGER" > 
<CFIF flagMethod IS "search">
	AND Scope  IN ( <cf_queryparam value="#UserCountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
<CFELSE>
	AND Scope  IN ( <cf_queryparam value="#EntityCountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
</CFIF>
AND ( <!--- removed by WAB - doesn't make sense - creator is likely to just be an administrator, shouldn't have automatic rights FlagGroup.CreatedBy=#request.relayCurrentUser.usergroupid#OR --->
	 (#tmpFlagMethod#AccessRights = 0 AND FlagGroup.#tmpFlagMethod# <>0)
	OR (#tmpFlagMethod#AccessRights <> 0
		AND EXISTS (SELECT 1 FROM FlagGroupRights
			WHERE FlagGroup.FlagGroupID = FlagGroupRights.FlagGroupID
			AND FlagGroupRights.UserID in (#request.relaycurrentuser.usergroups# )
			AND FlagGroupRights.#tmpFlagMethod# <>0)))


<CFIF flagMethod IS "edit">
UNION
SELECT DISTINCT FlagGroup.FlagGroupID AS ID, FlagTypeID, OrderingIndex, Name, Description, NamePhraseTextId, DescriptionPhraseTextId,0
FROM FlagGroup
WHERE Active <> 0
AND Expiry > #CreateODBCDateTime(Now())#
AND ParentFlagGroupID = 0
AND EntityTypeID =  <cf_queryparam value="#entityType#" CFSQLTYPE="CF_SQL_INTEGER" > 
AND Scope  IN ( <cf_queryparam value="#EntityCountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
<!--- wab removed to match removal above AND FlagGroup.CreatedBy<>#request.relayCurrentUser.usergroupid# --->
AND ( (ViewingAccessRights = 0 AND FlagGroup.Viewing <>0)
	OR (ViewingAccessRights <> 0
		AND EXISTS (SELECT 1 FROM FlagGroupRights
			WHERE FlagGroup.FlagGroupID = FlagGroupRights.FlagGroupID
			AND FlagGroupRights.UserID in (#request.relaycurrentuser.usergroups# )
			AND FlagGroupRights.Viewing <>0)))
AND ( (EditAccessRights = 0 AND FlagGroup.Edit = 0)
<!--- 	WAB: I think that this is wrong, needs a NOT EXISTS as below 
	OR (EditAccessRights <> 0
		AND EXISTS (SELECT 1 FROM FlagGroupRights
			WHERE FlagGroup.FlagGroupID = FlagGroupRights.FlagGroupID
			AND FlagGroupRights.UserID in (#request.relaycurrentuser.usergroups# )
			AND FlagGroupRights.Edit =0)) --->
		OR (EditAccessRights <> 0
			AND NOT EXISTS (SELECT 1 FROM FlagGroupRights
				WHERE FlagGroup.FlagGroupID = FlagGroupRights.FlagGroupID
				AND FlagGroupRights.UserID in (#request.relaycurrentuser.usergroups# )
				AND FlagGroupRights.Edit <> 0))

			)
</CFIF>
ORDER BY OrderingIndex, Name
</CFQUERY> 

<CFIF #flagMethod# IS "edit">

	<CFOUTPUT>	<CF_INPUT TYPE="HIDDEN" NAME="frmDate" VALUE="#CreateODBCDateTime(now())#">
				<CF_INPUT TYPE="HIDDEN" NAME="frmEntityID" VALUE="#thisEntity#"></CFOUTPUT>
	<CFSET flagList = "Group_0"> <!--- List of flags displayed --->

<CFELSEIF #flagMethod# IS "search">

	<CFOUTPUT><CF_INPUT TYPE="HIDDEN" NAME="entityType" VALUE="#entityType#"></CFOUTPUT>
	<CFSET flagList = "Group_0"> <!--- List of flags displayed --->

</CFIF>

<CFSET tempMethod = "">


<CFLOOP QUERY="getTopLevelFlags">

<CFSET thisFlagGroup = ID>
<CFIF flagMethod IS "edit"><CFSET this_edit_view = edit_view></CFIF>
<CFSET typeName = #ListGetAt(typeList,FlagTypeID)#>
<CFSET typeData = #ListGetAt(typeDataList,FlagTypeID)#>


		<CFOUTPUT><P></CFOUTPUT>
		<CFIF trim(namephrasetextid) is not "">  <!--- if there isn't a namephrasetextid there won't be any phrases --->
			<CFOUTPUT>phr_#namephrasetextid# <CFIF #Trim(Description)# IS NOT "">(<I>#Description#</I>)</CFIF></CFOUTPUT>
		<CFELSE>
			<CFOUTPUT>#Name# <CFIF #Trim(Description)# IS NOT "">(<I>#Description#</I>)</CFIF></CFOUTPUT>
		</cfif>




	<CFQUERY NAME="getLowerLevelFlagGroups" datasource="#application.sitedatasource#">
	SELECT DISTINCT FlagGroup.FlagGroupID AS ID, FlagTypeID, OrderingIndex, Name, Description, NamePhraseTextId, DescriptionPhraseTextId
	<CFIF flagMethod IS "edit">, 1 AS lower_edit_view</CFIF>
	FROM FlagGroup
	WHERE Active <> 0
	AND Expiry > #CreateODBCDateTime(Now())#
	AND ParentFlagGroupID =  <cf_queryparam value="#thisFlagGroup#" CFSQLTYPE="CF_SQL_INTEGER" > 
	AND EntityTypeID =  <cf_queryparam value="#entityType#" CFSQLTYPE="CF_SQL_INTEGER" > 
	<CFIF flagMethod IS "search">
		AND Scope  IN ( <cf_queryparam value="#UserCountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	<CFELSE>
		AND Scope  IN ( <cf_queryparam value="#EntityCountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	</CFIF>
	AND ( <!--- wab removed FlagGroup.CreatedBy=#request.relayCurrentUser.usergroupid#
	OR  --->(#tmpflagMethod#AccessRights = 0 AND FlagGroup.#tmpflagMethod# <> 0)
	OR (#tmpflagMethod#AccessRights <> 0
		AND EXISTS (SELECT 1 FROM FlagGroupRights
			WHERE FlagGroup.FlagGroupID = FlagGroupRights.FlagGroupID
			AND FlagGroupRights.UserID in (#request.relaycurrentuser.usergroups#)
			AND FlagGroupRights.#tmpflagMethod# <> 0)))
<CFIF flagMethod IS "edit">
UNION
SELECT DISTINCT FlagGroup.FlagGroupID, FlagTypeID, OrderingIndex, Name, Description, NamePhraseTextId, DescriptionPhraseTextId, 0
FROM FlagGroup
WHERE Active <> 0
AND Expiry > #CreateODBCDateTime(Now())#
AND ParentFlagGroupID =  <cf_queryparam value="#thisFlagGroup#" CFSQLTYPE="CF_SQL_INTEGER" > 
AND EntityTypeID =  <cf_queryparam value="#entityType#" CFSQLTYPE="CF_SQL_INTEGER" > 
AND Scope  IN ( <cf_queryparam value="#EntityCountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
<!--- wab removed to match removal above AND FlagGroup.CreatedBy<>#request.relayCurrentUser.usergroupid# --->
AND ( (ViewingAccessRights = 0 AND FlagGroup.Viewing <>0)
	OR (ViewingAccessRights <> 0
		AND EXISTS (SELECT 1 FROM FlagGroupRights
			WHERE FlagGroup.FlagGroupID = FlagGroupRights.FlagGroupID
			AND FlagGroupRights.UserID in (#request.relaycurrentuser.usergroups# )
			AND FlagGroupRights.Viewing <>0)))
AND ( (EditAccessRights = 0 AND FlagGroup.Edit = 0)
<!--- 	WAB: I think that this is wrong, needs a NOT EXISTS as below 
	OR (EditAccessRights <> 0
		AND EXISTS (SELECT 1 FROM FlagGroupRights
			WHERE FlagGroup.FlagGroupID = FlagGroupRights.FlagGroupID
			AND FlagGroupRights.UserID in (#request.relaycurrentuser.usergroups# )
			AND FlagGroupRights.Edit =0)) --->
	OR (EditAccessRights <> 0
		AND NOT EXISTS (SELECT 1 FROM FlagGroupRights
			WHERE FlagGroup.FlagGroupID = FlagGroupRights.FlagGroupID
			AND FlagGroupRights.UserID in (#request.relaycurrentuser.usergroups# )
			AND FlagGroupRights.Edit  <>0))			
			)
</CFIF>
	ORDER BY OrderingIndex, Name
	</CFQUERY>
	

	
	<CFIF #getLowerLevelFlagGroups.RecordCount# IS NOT 0>

		<CFOUTPUT><UL></CFOUTPUT><CFLOOP QUERY="getLowerLevelFlagGroups">
		<CFSET thisFlagGroup = ID>
		<CFSET typeName = #ListGetAt(typeList,FlagTypeID)#>
		<CFSET typeData = #ListGetAt(typeDataList,FlagTypeID)#>
			
			<CFOUTPUT><P></CFOUTPUT>
			<CFIF trim(namephrasetextid) is not "">  <!--- if there isn't a namephrasetextid there won't be any phrases --->
				<CFOUTPUT>phr_#namephrasetextid# </CFOUTPUT>
			<CFELSE>
				<CFOUTPUT>#Name# </CFOUTPUT> 
			</cfif>

			<CFIF trim(descriptionphrasetextid) is not "">
				<CFOUTPUT>(<I>phr_#namephrasetextid# </I>)</CFOUTPUT> 
			<CFELSE>				
				<CFIF #Trim(Description)# IS NOT ""><CFOUTPUT>(<I>#Description#</I>)</CFOUTPUT> </CFIF>
			</CFIF>
			
			
			
			<CFIF flagMethod IS "edit">
				<CFIF lower_edit_view IS NOT 1 or this_edit_view is not 1>
					<CFSET flagMethod = "view">
					<CFSET tempMethod = "edit">
				</CFIF>
			</CFIF>


			<!--- typeName should not be 'Group' but page exists for this case --->
			<CFINCLUDE TEMPLATE="#flagMethod#Flag#typeName#.cfm">
			<CFIF tempMethod IS NOT "">
				<CFSET flagMethod = tempMethod>
				<CFSET tempMethod = "">
			</CFIF>

		</CFLOOP><CFOUTPUT></UL></CFOUTPUT>
	<CFELSE>
	
		<CFIF #typeName# IS "Group">
			<CFOUTPUT><UL>No flags available to #htmleditformat(flagmethod)#.</UL></CFOUTPUT>
		<CFELSE>
			<CFIF flagMethod IS "edit">
				<CFIF this_edit_view IS NOT 1>
					<CFSET flagMethod= "view">
					<CFSET tempMethod = "edit">
				</CFIF>
			</CFIF>
			<CFOUTPUT><UL></CFOUTPUT><CFINCLUDE TEMPLATE="#flagMethod#Flag#typeName#.cfm"><CFOUTPUT></UL></CFOUTPUT>
			<CFIF tempMethod IS NOT "">
				<CFSET flagMethod= tempMethod>
				<CFSET tempMethod = "">
			</CFIF>
		</CFIF>

	</CFIF>

</CFLOOP>

<CFIF #flagMethod# IS "edit">
	<CFOUTPUT><CF_INPUT TYPE="HIDDEN" NAME="flagList" VALUE="#flagList#"></CFOUTPUT>
<CFELSEIF #flagMethod# IS "search">
	<!--- <CFSET entityTable = #ListGetAt(application.typeEntityTable,entityType+1)#> --->
	<cfset entityTable = application.entityType[entityType].tablename>
	<CFOUTPUT><CF_INPUT TYPE="HIDDEN" NAME="flagList#entityTable#" VALUE="#flagList#"></CFOUTPUT>
</CFIF>

