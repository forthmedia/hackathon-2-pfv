<!--- �Relayware. All Rights Reserved 2014 --->
<!--
File name:		peopleAtLocation.cfm
Author:			Will
Date created:	ages ago

	Objective - to list the people at this location

	Syntax	  -	it is called within the viewer so should use viewer params

	Parameters - Viewer params

	Return Codes - none

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed

19 July 2001		SWJ		I removed all of the font calls and changed the row color logic
							to use the classes oddrow/even row

WAB                  changes to fit in new entity navigation

2012-06-25 			PPB 	Case 428352 paging error if list of persons is long
2017-02-22			DBB	RT-213: Changed getPeople query to return right number of people at the location

Enhancement still to do:

1/  We need to consider replacing the hard coded job function call to use some system parametr

--->


<CFPARAM NAME="message" DEFAULT="">
<CFPARAM NAME="frmPersonCheck" DEFAULT="">
<CFPARAM NAME="frmStartp" DEFAULT="1">
	<!--- define the start record --->
 	<CFIF structKeyExists(FORM,"PersonForward.x")>
		 <CFSET displaystart = frmStartp + application.showRecords>
 	<CFELSEIF structKeyExists(FORM,"PersonBack.x")>
		 <CFSET displaystart = frmStartp - application.showRecords>
	<CFELSEIF IsDefined("frmStartp")>
	 	 <CFSET displaystart = frmStartp>
	</CFIF>

<CFIF frmentitytype is "person" or frmentitytype is "0">
	<CFQUERY NAME="getLocation" datasource="#application.siteDataSource#">
		SELECT p.locationid
		from person as p
		where p.personid =  <cf_queryparam value="#frmcurrententityid#" CFSQLTYPE="CF_SQL_INTEGER" >
	</CFQUERY>

	<CFSET locationid = getlocation.locationid>

<CFELSEIF frmentitytype is "location" or frmentitytype is "1">

	<cfif isDefined("frmcurrententityid")>
		<cfset locationid = frmcurrententityid>
	<cfelseif structKeyExists(form,"frmLocationID")>
		<cfset locationid = form.frmLocationID>
	</cfif>

<CFELSE>

	<CFOUTPUT> entitytype not set </CFOUTPUT>
	<CF_ABORT>
</cfif>

<CFQUERY NAME="getPeople" datasource="#application.siteDataSource#">
	SELECT p.PersonID,
		  p.Salutation,
		  p.FirstName,
		  p.LastName,
		  p.OfficePhone,
		  p.Email,
		  p.LastUpdated,
		  ug.Name AS LastUpdatedBy
	<!---START: DBB RT-213--->
	FROM Person p 
	LEFT OUTER JOIN UserGroup ug ON p.LastUpdatedBy = ug.UserGroupID 
        WHERE p.LocationID =  <cf_queryparam value="#locationid#" CFSQLTYPE="CF_SQL_INTEGER" >
        AND p.Active = 1
	ORDER BY p.LastName, p.FirstName
	<!---END: DBB RT-213--->
</CFQUERY>

<CFIF getPeople.RecordCount GT application.showRecords>
	<!--- showrecords in application .cfm
			defines the number of records to display --->

	<!--- define the start record --->
 	<CFIF structKeyExists(FORM,"PersonForward.x")>
		 <CFSET displaystart = frmStartp + application.showRecords>
 	<CFELSEIF structKeyExists(FORM,"PersonBack.x")>
		 <CFSET displaystart = frmStartp - application.showRecords>
	<CFELSEIF IsDefined("frmStartp")>
	 	 <CFSET displaystart = frmStartp>
	</CFIF>
</CFIF>

<cfhtmlhead text="
		<SCRIPT type='text/javascript'>
		<!--

			function jsReset() {
			//----------------
			var form = document.mainForm;
			form.reset();
			}

			function doForm(FormName) {
		//----------------------------
				var form = eval('document.' + FormName);
				form.frmDupPeopleCheck.value = saveChecks();
				form.submit();
			}

			function jsSubmit() {
			//-----------------

			document.mainForm.submit()
			}
		//-->
		</SCRIPT>
">

<!--- includes the ViewEntityFunction --->
<cf_includeJavaScriptOnce template="/javascript/ViewEntity.js">

<CFOUTPUT>
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
<TR>
	<TD>&nbsp;</td><TD ALIGN="RIGHT" VALIGN="TOP">
	<CFINCLUDE template="\relay\data\persondropdown.cfm"></td>
</TR>
</TABLE>

<CENTER>

<CFIF Trim(message) IS NOT "">
#application.com.relayUI.message(message=message)#
</CFIF>
</CFOUTPUT>

<!---

<FORM METHOD="POST" ACTION="..\screen\updatedata.cfm" NAME="mainForm" target="_self">
	<INPUT TYPE="HIDDEN" NAME="frmStartp" VALUE="<CFOUTPUT>#variables.displayStart#</CFOUTPUT>">
	<INPUT TYPE="HIDDEN" NAME="frmCurrentScreenId" VALUE="<CFOUTPUT>#frmcurrentscreenid#</CFOUTPUT>">
	<input type="HIDDEN" name="frmEntityType"  value="<cfoutput>#frmEntityType#</cfoutput>">
	<INPUT TYPE="HIDDEN" NAME="frmCurrentEntityId" VALUE="<CFOUTPUT>#frmcurrententityid#</CFOUTPUT>">
	<INPUT TYPE="HIDDEN" NAME="frmLocationId" VALUE="<CFOUTPUT>#locationid#</CFOUTPUT>">
	<input type="HIDDEN" name="frmNextPage"  value="screen.cfm">
	<INPUT TYPE="HIDDEN" NAME="frmGlobalParameters" VALUE="<CFOUTPUT>#frmGlobalParameters#</CFOUTPUT>"> --->


	<!--- 2012/06/25 PPB Case 428352 I re-instated these 2 hidden variables which are needed for paging if list of persons is long --->
	<INPUT TYPE="HIDDEN" NAME="frmStartp" VALUE="<CFOUTPUT>#variables.displayStart#</CFOUTPUT>">
	<input type="HIDDEN" name="frmEntityType"  value="<cfoutput>#frmEntityType#</cfoutput>">

<CFIF getPeople.RecordCount IS 0>

	<CFOUTPUT><P>No people were found at the above location.</CFOUTPUT>

<CFELSE>


	<CFOUTPUT>
	<TABLE BORDER=0 WIDTH="100%" CELLPADDING=4 CELLSPACING=0>
	<TR>
		<Th ALIGN="CENTER" VALIGN="BOTTOM" NOWRAP>
			<B>Check</B>
		</Th>

		<Th VALIGN="BOTTOM">
			<B>Name (Click to edit)</B>
		</Th>

		<Th VALIGN="BOTTOM">
			<B>Direct Line</B>
		</Th>

		<Th VALIGN="BOTTOM">
			<B>Email</B>
		</Th>

<!--- 		<Th ALIGN="CENTER" VALIGN="BOTTOM">
			<B>Edit<BR>Detail</B>
		</Th>
 --->
	</TR>


	<CFSET frmPeopleOnPage = "">
	<CFSET endrow= min(application.showRecords+Variables.displaystart, getpeople.recordcount)>

<!--- 	<CFOUTPUT QUERY="getPeople" STARTROW=#Variables.displaystart# MAXROWS=#application.showRecords#> --->
	<CFLOOP QUERY="getPeople" STARTROW=#Variables.displaystart# ENDROW=#Endrow#>
		<!--- gather a list of all the people displayed on this page --->
		<CFSET frmPeopleOnPage = ListAppend(frmPeopleOnPage,PersonID)>

	<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
		<TD ALIGN="CENTER" VALIGN="MIDDLE">
			<CF_INPUT TYPE="CHECKBOX" NAME="frmPersonCheck" VALUE="#PersonID#" CHECKED="#iif(ListFind(frmPersonCheck,PersonID) IS NOT 0,true,false)#">
		</TD>

		<TD>
			<CFIF Trim(Salutation & FirstName & LastName) IS "">&nbsp;<CFELSE>
			<A HREF="JavaScript:viewPerson(#personid#);">#Trim(Salutation & " " & FirstName & " " & LastName)#</A>
			</CFIF>
		</TD>
		<TD>
			<CFIF Trim(OfficePhone) IS "">&nbsp;<CFELSE>
			#htmleditformat(OfficePhone)#
			</CFIF>
		</TD>
		<TD>
			<CFIF Trim(Email) IS "">&nbsp;<CFELSE>
			#htmleditformat(Email)#
			</CFIF>
		</TD>
<!--- 		<TD ALIGN="CENTER">

			#DateFormat(LastUpdated,"DD-MMM-YYYY")# #TimeFormat(LastUpdated,"HH:mm:ss")#<BR>#LastUpdatedBy#
		</TD> --->
<!--- 		<TD ALIGN="CENTER" VALIGN="MIDDLE"><A HREF="JavaScript:viewEntity_('Person',saveChecks(),'#personid#','','','');"><IMG SRC="/images/MISC/greendot.gif" WIDTH=19 HEIGHT=19 BORDER=0></A></TD> --->
	</TR>

	<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
		<TD>&nbsp</td>
		<TD colspan="3"><CFSET flaggrouptextid="JobFunction"><CFSET thisentity=personid><CFINCLUDE TEMPLATE="../flags/listFlagGroup.cfm"><B>Job Function:</B> #htmleditformat(result)#
		</td>
		<TD>&nbsp</td>
	</tr>
	</CFLOOP>
	</TABLE>

	<CF_INPUT TYPE="HIDDEN" NAME="frmPeopleOnPage" VALUE="#frmPeopleOnPage#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmPeopleInReport" VALUE="#valuelist(getpeople.personid)#">
	</CFOUTPUT>



	<CFIF #getPeople.RecordCount# GT #application.showRecords#>
		<!--- if we only show a subset, then give next/previous arrows --->

	<CFOUTPUT>
	<P><TABLE BORDER=0>

		<TR><TD>
				<CFIF #Variables.displaystart# GT 1>
					<input type="Image" name="PersonBack" src="/images/buttons/prev.png" BORDER=0 ALT="Previous">
				</CFIF>
				</TD>
			<TD>
	 				<CFIF (#Variables.displaystart# + #application.showRecords# - 1) LT #getPeople.RecordCount#>
						<CFOUTPUT>Record(s) #htmleditformat(Variables.displaystart)# - #Evaluate("Variables.displaystart + application.showRecords - 1")#</CFOUTPUT>
					<CFELSE>
	 					<CFOUTPUT>Record(s) #htmleditformat(Variables.displaystart)# - #getPeople.RecordCount#</cFOUTPUT>
					</CFIF>
				</TD>

			<TD>
				<CFIF (#Variables.displaystart# + #application.showRecords# - 1) LT #getPeople.RecordCount#>
					<input type="Image" name="PersonForward" src="/images/buttons/next.png" BORDER=0 ALT="Next">
				</cFIF>
				</TD>
		</TR>
		</TABLE>

		</CFOUTPUT>
	</CFIF>



</CFIF>
<!--- </FORM>	 --->
<CFOUtPUT></CENTER></CFOUtPUT>