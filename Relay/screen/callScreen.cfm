<!--- �Relayware. All Rights Reserved 2014 --->

<!---

File name:		CallScreen.cfm	
Author:			SWJ	
Date created:	21st Aug 2000

Description:	This screen is designed to call the quickScreen tag.  It queries the schemaTable 
				table to get the relevant information to set up quick screen correctly.

Date Tested:
Tested by:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed

Enhancement still to do:



--->
<CFPARAM NAME="criteria" DEFAULT="blank">

<cfif not isDefined("title")>  
	<!--- The entity attribute parameter is required --->
	<table border="2">
		<tr>
		<td><p><b>Error Processing CallScreen.cfm:</b></p> 
		<p>Parameter <b>title</b> which is required was not supplied.</p></td>
		</tr>
	</table>
	<cfexit>
<CFELSE>
<cf_head>
	<cf_title><CFOUTPUT>#htmleditformat(Title)#</CFOUTPUT></cf_title>
</cf_head>
<CFPARAM NAME="debug" DEFAULT="yes">
<H1><CFOUTPUT>#htmleditformat(Title)#</CFOUTPUT></H1>
</cfif>
<!--- Lets see what parameters are required and available --->
<cfif not isDefined("entity")>  
	<!--- The entity attribute parameter is required --->
	<table border="2">
		<tr>
		<td><p><b>Error Processing CallScreen.cfm:</b></p> 
		<p>Parameter <b>entity</b> which is required was not supplied.</p></td>
		</tr>
	</table>
	<cfexit>
</cfif>
<cfif not isDefined("action")>  
	<!--- The entity attribute parameter is required --->
	<table border="2">
		<tr>
		<td><p><b>Error Processing CallScreen.cfm:</b></p> 
		<p>Parameter <b>action</b> which is required was not supplied.</p></td>
		</tr>
	</table>
	<cfexit>
</cfif>
<CFPARAM NAME="frmEntityID" DEFAULT="0">

<CFIF criteria EQ 'Blank'>
	<CF_QuickScreen 
		mainEntity="#entity#" 
		action="#action#" 
		debug="#debug#"
		frmEntityID="#frmEntityID#"
		>
<CFELSE>
	<CF_QuickScreen 
		mainEntity="#entity#" 
		action="#action#" 
		debug="#debug#"
		criteria="#criteria#"
		frmEntityID="#frmEntityID#"
		>
</CFIF>




