<!--- �Relayware. All Rights Reserved 2014 --->
<!---

Example usage:

<FORM>
<CFINCLUDE template="..\templates\qrygetcountries.cfm">
<CFSET location_countryid_validvalues = countryList>
<CFSET thisLineMethod = "edit">
<CFSET thisLineFormField = "frmcountryid">
<CFSET thisLineData = "9">

<CFINCLUDE template= "..\screen\showcountrylist.cfm">

</form>

 --->

<CFPARAM name="javascriptVerification" default = "">
<CFPARAM name="locationFieldList" default = "">
<CFPARAM name="onChange" default = "">    <!--- notused within screens, but can be used if this element is used by anything else  --->
<CFPARAM name="required" default = "0">
<CFPARAM name = "thisLineFormField"  default= "frmcountryid">

<!--- Location Country --->

<CFIF thislineMethod is "Edit">

	<!--- find all Countries regardless of rights --->
	<CFQUERY NAME="findCountries" datasource="#application.siteDataSource#">
	SELECT CountryID, CountryDescription
 	FROM Country
	WHERE CountryDescription <> 'zzz - Deleted Locations'
	AND ISOCode <> ''
	AND ISOCode <> 'XS'		<!--- spain temporary --->
	<CFIF isdefined("location_countryid_validvalues")> AND countryid  in ( <cf_queryparam value="#evaluate("location_countryid_validvalues")#" CFSQLTYPE="cf_sql_integer"  list="true"> )</CFIF>
	ORDER BY CountryDescription
	</CFQUERY>

<CFELSE>

	<!--- find this Location's country as name --->
	<CFQUERY NAME="findCountry" datasource="#application.siteDataSource#">
	SELECT CountryDescription
 	FROM Country
	WHERE CountryID = <CFIF isNumeric(thisLineData)>#thisLineData#<CFELSE>0</cfif>
	</CFQUERY>

</CFIF>

<CFIF thisLineMethod IS "Edit">

		<CFOUTPUT>
		<SELECT NAME="#thisLineFormField#" onchange="#onChange#">
			<OPTION VALUE=""#IIF(thisLineData IS 0, DE(" SELECTED"), DE(""))#>Select a Country</OPTION>
			</CFOUTPUT>
				<CFOUTPUT QUERY="findCountries">
				<OPTION VALUE="#CountryID#"#IIF(thisLineData IS CountryID, DE(" SELECTED"), DE(""))#>#HTMLEditFormat(CountryDescription)#</OPTION>
				</CFOUTPUT>
		<CFOUTPUT>
		</SELECT>
		<CF_INPUT TYPE="HIDDEN" NAME="#thisLineFormField#_orig" VALUE="#thisLineData#">
		</cfoutput>
	<CFSET locationFieldList = listAppend(locationFieldList,"location_countryid")>
	<!--- country always required --->
	<CFSET javascriptVerification = javascriptVerification & "msg = msg + verifyObject($('#thisLineFormField#'),' * choose a country','#required#');" >


<CFELSEIF thisLineMethod IS "ViewWithHidden">
 	<CFOUTPUT>#HTMLEditFormat(findCountry.CountryDescription)#
	<CF_INPUT TYPE="HIDDEN" NAME="#thisLineFormField#" VALUE="#thisLineData#">
	<CF_INPUT TYPE="HIDDEN" NAME="#thisLineFormField#_orig" VALUE="#thisLineData#">
	</CFOUTPUT>
	<CFSET locationFieldList = listAppend(locationFieldList,"location_countryid")>


<CFELSEIF thisLineMethod IS "View">

 	<CFOUTPUT>#HTMLEditFormat(findCountry.CountryDescription)#</CFOUTPUT>

</CFIF>







