<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Modifications report for the current Record.
For use within the Viewer --->

<!---
Author   WAB
Date	Many moons ago

Comments:  A rough report which has never been developed any further

Mods:

2000-11-10:  	WAB Added ID into the order by clause.
			Added a rough search for dedupes.  Needs to be modified so that user
			can see mods on those records as well.
2001-08-13	SWJ	Modified layout slightly
2005-03-25	SWJ Tidied up the layout of the report.

2007-05-30	WAB added **** when the field is password

2008/05/13    WAB  have added a field modifiedByPerson to the modregister table, it won't always be populated (until we upgrade lots of code, but if it is then use the name of the person)
			Also  reformatted to show flaggroup name, and to show changes to radio buttons as a single change on one line
2010/11/09	WAB AJC LID 4722 altered so that always joins to the person table to get the name of the updater (deals with usergroup.name not keeping in synch with the person name)
2013-04-17 WAB  during CASE 434808, changed query to use new vModRegister and take advantage of the entityTypeID field
2015/02/27	NJH	Jira Task Fifteen-176 - converted report to use TFQO to support sorting.
 --->

<!--- 2008/05/13 WAB for new screen navigation needs to pick up type of the screen being displayed--->
<cfif isDefined("frmScreenEntityType")>
	<cfset frmEntityType = frmScreenEntityType>
	<cfset frmCurrentEntityID = frmScreenEntityID>
</cfif>


<cfparam name = "frmEntityType">
<cfparam name = "frmCurrentEntityID">
<cfparam name="sortOrder" default="moddate desc, id desc">

<!--- WAB 2005-02-07   trying to getthis to work and frmEntityType seems to be using numerics these days rather than labels as expected.
				Therefore  a hack to get this to work--->
<cfif frmentitytype is 0>
	<cfset frmentitytype = "person">
<cfelseif frmentitytype is 1>
	<cfset frmentitytype = "location">
<cfelseif frmentitytype is 2>
	<cfset frmentitytype = "organisation">
</cfif>


<cfif not isdefined("ModEntityId")>
	<cfset modentityid = frmcurrententityid>
</cfif>
<cfif not isdefined("ModEntityType")>
	<cfset modentitytype = frmentitytype>
 </cfif>


<!--- WAB 2013-04-17 CASE 434808, changed query to use new vModRegister and take advantage of the entityTypeID field --->
<cfquery name="getMods" datasource="#application.sitedatasource#">

SELECT mr.moddate, mr.id, actionByCF,
	oldval,
	newval,
	field as fieldname ,
	case
		when fullname is not null then fullname
		else 'Not Known' end
		as userName	,
	CASE
		WHEN action = 'FA' THEN 'Attribute Added'
		WHEN action = 'FD' THEN 'Attribute Deleted'
		WHEN action = 'FM' THEN 'Attribute modified'
	 	WHEN substring(action,2,1) = 'A' THEN 'Record Added'
	 	WHEN substring(action,2,1) ='M' THEN 'Record Modified'
		ELSE action END as translatedAction,
	action,
	f.flaggroupid,
	f.datatype,
	0 as hide
FROM vmodregister mr
		Left JOIN
	vflagDef f on  mr.flagid is not null and mr.flagid= f.flagid

WHERE
	mr.entityTypeID = <cf_queryparam value="#application.entityTypeID[frmEntityType]#" CFSQLTYPE="CF_SQL_INTEGER" >
	AND recordID =  <cf_queryparam value="#modEntityID#" CFSQLTYPE="CF_SQL_INTEGER" >

ORDER BY mr.moddate desc, mr.id desc
</cfquery>


	<!--- WAB
	This code loops through the mods looking for updates to radio flags and combines them into the same line
	have to be
		same flaggroup
		within 2 seconds
		same user (or the delete by user "not known")
	There is an assumption (probably accurate given the triggers) that the add has to have happened after the deletion (so appears first in the query which is sorted by moddate desc)
	--->

	<cfloop query="getMods">
		<cfif 	dataType is "Radio"
				and currentRow is not getMods.recordCount
				and flagGroupID is getMods.flagGroupid[currentRow+1]
				and abs(dateDiff ("s",moddate,getMods.moddate[currentRow+1])) lte 2
				and action is "FA" and getMods.action[currentRow+1] is "FD"
				and (username is getMods.username[currentRow+1]  or getMods.actionByCF[currentRow+1] is 0)
				>

			<cfset querysetcell(getMods,"translatedAction","Attribute Updated",currentRow)>
			<cfset querysetcell(getMods,"oldVal",getMods.oldval[currentRow+1] ,currentRow)>
			<cfset querysetcell(getMods,"hide",1,currentRow + 1)>

		</cfif>
	</cfloop>

	<cfquery name="getMods" dbtype="query">
	select  * from getMods where hide = 0
	ORDER BY <cf_queryObjectName value="#sortOrder#">
	</cfquery>


<!--- check whether this record has ever been deduplicated with another record --->
<cfquery name="getDedupes" datasource="#application.sitedatasource#">
select da.* , p.firstname + ' '  + p.lastname as fullname
from dedupeAudit da
left join person p on p.personid = da.createdByPersonID
where
(da.newId =  <cf_queryparam value="#ModEntityID#" CFSQLTYPE="CF_SQL_INTEGER" >  or da.archivedid =  <cf_queryparam value="#ModEntityID#" CFSQLTYPE="CF_SQL_INTEGER" > )
and da.RecordType =  <cf_queryparam value="#frmEntityType#" CFSQLTYPE="CF_SQL_VARCHAR" >
</cfquery>



<!--- Check whether anyone has been deleted from this location/organisation --->
<!--- NJH 2008/02/18 Bug Fix Trend 136 Modification History - changed p.* to p.personID, p.firstname and p.lastname
	as the output was using the 'username' field from the person table rather than the usergroup table--->
<cfif frmentitytype is "Location" or frmentitytype is "Organisation" >
	<cfquery name="getDeletes" datasource="#application.sitedatasource#">
		select 	persondel.personID, persondel.firstname, persondel.lastname, mr.moddate,
				case 	when p.personid is not null then  p.firstName + ' '  + p.lastname
						when pd.personid is not null then  pd.firstName + ' '  + pd.lastname
						when ug.usergroupid is not null then  ug.name
					else 'Not Known' end
					as userName

		from 	persondel  inner join
				modRegister as mr on persondel.personid = mr.recordid and mr.action='PD'
				left join person p on mr.ActionByPerson = p.personid
				left join persondel pd on mr.ActionByPerson = pd.personid
				left join usergroup ug on ug.usergroupid = mr.ActionByCF
		where persondel.#frmEntityType#id =  <cf_queryparam value="#ModEntityID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</cfquery>
</cfif>


<!--- <cfoutput>

<table width="100%" border="0" cellspacing="1" cellpadding="3" align="center" class="withBorder">
	<tr>
		<th colspan="6">List of Modifications made</th>
	</tr>
	<tr>
		<th>Date</th>
		<th>User</th>
		<th>Action</th>
		<th>Field</th>
		<th>Old Value</th>
		<th>New Value</th>
	</tr>
	<cfloop query="getMods">
			<tr<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
				<td>#dateformat(ModDate)#</td>
				<td>#htmleditformat(UserName)#</td>
				<td>#htmleditformat(translatedAction)#</td>
				<cfif oldval is "" and newval is "">
					<td colspan="3">#htmleditformat(fieldname)#</td>
				<cfelse>
					<td>#htmleditformat(fieldname)#</td>
					<td>#htmleditformat(oldval)#</td>
					<td>#htmleditformat(NewVal)#</td>
				</cfif>
			</tr>

	</cfloop>

</table>
</cfoutput> --->
<h2>List of Modifications made</h2>
<cf_tableFromQueryObject
	queryObject="#getMods#"
	showTheseColumns="modDate,UserName,translatedAction,Fieldname,Oldval,Newval"
	dateformat="modDate"
	sortOrder="#sortOrder#"
	useInclude ="false"
	columnHeadingList="Date,User,Action,Field,Old Value,New Value"
>

<cfif getdedupes.recordcount is not 0>
	<cfoutput>
	<p></p>

	<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" CLASS="withBorder">
	<tr><th>This record was once deduplicated with the following record(s):</th></tr> <br>

		<cfloop query = "getDedupes">
			<tr<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
			<td>#dateFormat(Created,"dd-mmm-yy")#

			<cfif modentityid is archivedid>
				<a href="/screen/modifications.cfm?frmCurrentEntityID=#frmCurrentEntityID#&frmEntityType=#frmEntityType#&modEntityID=#NewID#">#htmleditformat(NewID)#</a>  (which was kept)<br>
			<cfelse>
				<a href="/screen/modifications.cfm?frmCurrentEntityID=#frmCurrentEntityID#&frmEntityType=#frmEntityType#&modEntityID=#ArchivedID#">#htmleditformat(ArchivedID)#</a> (which was deleted)<br>
			</cfif>
			<cfif fullname is not "">
		by #htmleditformat(fullname)#
			</cfif>
			</td>
			</tr>
		</cfloop>
		</table>
	</cfoutput>
</cfif>


<cfset administrator = false>
<cfif listfind(request.relaycurrentuser.usergroups,12) is not 0>
	<cfset administrator = true>
</cfif>


<cfif isdefined("getDeletes") and getdeletes.recordcount is not 0>
	<cfoutput>
	<p></p>
		<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" CLASS="withBorder">
			<tr>
				<th>These people have been deleted from this #htmleditformat(frmEntityType)#: </th>
			</tr>

		<cfloop query = "getDeletes">
			<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
				<td>#dateFormat(ModDate,"dd-mmm-yy")# #htmleditformat(personid)# #htmleditformat(firstname)# #htmleditformat(lastname)# by #htmleditformat(username)#
				<cfif administrator><a href="../report/utilities/undeletePerson.cfm?frmPersonid=#personid#">undelete</a></cfif>
				</td>
			</tr>
		</cfloop>

		</table>
	</cfoutput>

</cfif>


