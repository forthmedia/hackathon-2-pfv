<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Template for adding a new person--->
<!--- all fields defined in frmpersonfieldsonpage--->
<!--- dummy id variable  --->

<!--- variable formentityid will be something like newperson --->

<!--- mods
2000-03-30  wab: amended check for existing person - if email is blank then have to allow addition anyway
2010/11/04	NAS		LID4530 - Existing Customers not appearing for other Partners
2016/09/01		NJH JIRA PROD2016-1190 - replaced old matching/dataload code with call to new matching functions and call to relayEntity.

 --->


<CFSET updatetime=createODBCDate(now())>

<CFPARAM name="person_passworddate_#formentityid#" default="#Now()#">
<CFPARAM name="person_loginexpires_#formentityid#" default="#CreateODBCDate("01-Jan-92")#">
<CFPARAM name="person_username_#formentityid#" default=" ">
<CFPARAM name="person_password_#formentityid#" default=" ">
<CFPARAM name="person_firsttimeuser_#formentityid#" default="1">
<CFPARAM name="person_salutation_#formentityid#" default=" ">

<CFPARAM name="person_homephone_#formentityid#" default="">
<CFPARAM name="person_officephone_#formentityid#" default="">
<CFPARAM name="person_mobilephone_#formentityid#" default="">
<CFPARAM name="person_faxphone_#formentityid#" default="">
<CFPARAM name="person_language_#formentityid#" default="">

<CFPARAM name="person_active_#formentityid#" default="1">

<CFPARAM name="person_lastupdatedby_#formentityid#" default="#request.relayCurrentUser.userGroupId#">
<CFPARAM name="person_createdby_#formentityid#" default="#request.relayCurrentUser.userGroupId#">
<CFPARAM name="person_created_#formentityid#" default="#frmdate#">
<CFSET "person_lastupdated_#formentityid#" = updatetime>

<CFSET locationid = evaluate("person_locationid_#formentityid#")>
<CFIF not isnumeric(locationid)>
	<CFSET locationid = evaluate(locationid)>
	<CFSET "person_locationid_#formentityid#" = locationid>
</CFIF>
<CFSET firstname = evaluate("person_firstname_#formentityid#")>
<CFSET lastname = evaluate("person_lastname_#formentityid#")>

<CFSET email = evaluate("person_email_#formentityid#")>

<cfquery name="getOrganisationID" datasource="#application.sitedatasource#">
SELECT OrganisationID FROM Location WHERE LocationID =  <cf_queryparam value="#LocationID#" CFSQLTYPE="CF_SQL_INTEGER" >
</cfquery>

<CFPARAM name="person_organisationid_#formentityid#" default="#getOrganisationID.OrganisationID#">

<CFSEt entityadded=false>



<!--- removed this because it doesn't work properly when multiple people are added in one go
<!--- check that this form hasn't already been submitted --->

	<CFQUERY NAME="Check" datasource="#application.siteDataSource#">
	SELECT * from person
	where created = #frmdate#
	and createdby= #request.relayCurrentUser.userGroupId#
	</CFQUERY>

	<CFIF check.recordcount is not 0>
	form already submitted <BR>
<!--- 	<CF_ABORT> --->
	</CFIF>
 --->



	<!--- check for people with same email, first and last name --->
	<!--- <CFQUERY NAME="CheckExisting" datasource="#application.siteDataSource#">
	SELECT
		p.personid,
		l.sitename,
		l.organisationid as existingOrganisationID,
		currentLocation.organisationid as thisOrganisationID
	from
		person as p,
		location as l,
		location as currentLocation
	where
			p.locationid = l.locationid
		and p.firstname =  <cf_queryparam value="#firstname#" CFSQLTYPE="CF_SQL_VARCHAR" >
		and p.lastname =  <cf_queryparam value="#lastname#" CFSQLTYPE="CF_SQL_VARCHAR" >
		and p.email =  <cf_queryparam value="#email#" CFSQLTYPE="CF_SQL_VARCHAR" >
		and l.countryid  = currentLocation.countryid
		and currentLocation.locationid =  <cf_queryparam value="#locationid#" CFSQLTYPE="CF_SQL_INTEGER" >
	</CFQUERY> --->

	<!--- get definition of the table --->
	<CFSET tablerequired = "person">
	<CFINCLUDE template="qryGetTableDefinition.cfm">

	<!--- insert people --->
	<cfset paramSet = structNew()>
	<!--- <cfset paramSet.datasource = application.sitedatasource > --->

	<CFLOOP index="fieldname" list= "#textfields#">
		<CFIF structKeyExists(form,"person_#fieldname#_#formentityid#")>
			<CFSET paramSet[fieldname] = form["person_#fieldname#_#formentityid#"]>
			<!--- <cfset paramSet[fieldname]="N'#fieldvalue#'"> --->
		</CFIF>
	</CFLOOP>
	<CFLOOP index="fieldname" list= "#listappend(numericfields,datefields)#">
		<CFIF structKeyExists(form,"person_#fieldname#_#formentityid#")>
			<CFSET fieldvalue = form["person_#fieldname#_#formentityid#"]>
			<!--- HACK!!! datetimestamps bugger this up! not a date - use it --->
			<cfif listfindnocase("PasswordDate,LoginExpires",fieldName,",") eq 0>
				<cfset paramSet[fieldname]=fieldvalue>
			</cfif>
		</CFIF>
	</CFLOOP>

	<cfset checkexisting = application.com.matching.getMatchesForEntityDetails(entityType="person",argumentCollection=paramSet)>

	<CFIF checkexisting.recordcount is not 0 <!--- and email is not "" and listFind(valueList(checkexisting.existingOrganisationID), checkexisting.thisOrganisationID) is not 0 --->>

		<CFSET entityadded=false>
		<!--- <CFSET rowNumber = listFind(valueList(checkexisting.existingOrganisationID), checkexisting.thisOrganisationID)>  ---> <!--- which item in the query has matching Orgs --->
		<!--- 2010/11/04	NAS		LID4530 - Existing Customers not appearing for other Partners --->
		<cfset session.useThisPersonID = CheckExisting.personID>
		<CFSET message = message & "Person '#Trim(FirstName & " " & LastName)#' already exists, not added<BR>">
		<CFSET newentityid = checkexisting.personid[1]>		<!--- set this anyway --->

	<CFELSE>

		<!--- person does not already exist --->
		<!--- note, if email is blank then the test is a bit draconian so we can't really do anything other than set a warning message
			WAB Mod: allow addition if the person is in a different organisation - just set a warning
		--->
		<!--- <CFIF checkexisting.recordcount is NOT 0 and (email is "" or checkexisting.existingOrganisationID is not checkexisting.thisOrganisationID)>
				<CFSET message = message & "Warning: Person '#Trim(FirstName & " " & LastName)#' already exists at location #Checkexisting.sitename#.<BR>">
		</CFIF> --->

			<cfset newPersonID = application.com.relayEntity.insertPerson(personDetails=paramSet,insertIfExists=true).entityID> <!--- insertIfExists because we've already done matching check --->

			<CFIF newPersonID IS 0>

				<CFSET entityadded=false>
				<CFSET message = message & "There was a problem adding Person '#Trim(FirstName & " " & LastName)#'.  Please try again.">

			<CFELSE>

				<CFSET newentityid = newPersonID>

				<!--- <CFQUERY NAME="insPersDataSource" datasource="#application.siteDataSource#">
					if not exists (select * from persondatasource where personid =  <cf_queryparam value="#newentityid#" CFSQLTYPE="CF_SQL_INTEGER" > )
					INSERT INTO PersonDataSource(PersonID, DataSourceID, RemoteDataSourceID)
					VALUES(<cf_queryparam value="#newentityid#" CFSQLTYPE="CF_SQL_INTEGER" >,1,'0')
				</CFQUERY> --->

				<CFSET entityadded=true>
				<CFSET message = message & "Person '#Trim(FirstName & " " & LastName)#' was added successfully.<BR>">

			</CFIF>
	</CFIF>