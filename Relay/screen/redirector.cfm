<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		redirector.cfm
Author:			
Date created:	Jun 99 in Upper Heyford

Objective - This files executes a process defined in process Actions
		
Syntax	  -	
	
Parameters - 
				
Return Codes - none 

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
WAB: 1999-12-04 no longer requires an entry in process details
WAB: 2000-02-14 added OUTPUT to output a variable
WAB: 2000-07-27 added a setFlag action
WAB:  2001-06-20  when doing cflocation added check for existing 	
				url variables before appending the processid and stepid
SWJ: 2002-05-12 started to develop new version of redirector
SWJ: 2003-05-05 modified value to satisfy MX

WAB 2006-05-23  work done during CR_SNY584 
				added a function to evaluate CF variables in the jump name and value - this replaced a bit of previous code which just did this when the action was "value"
				- now does it on every item in the loop, and creates variables thisJumpName and thisJumpValue which can be accessed within the cfswitch
					- it allows for the [[ ]] notation to be used, prevents application.com being accessed directly 
					- am not using this in every type of action because some of them assume that the jumpValue is a cfvariable and therefore don't require it to have ## around it

				- also changed the cfif to a switch 
				- added parameters field
				- added customtag and relay tag calls (under dev)
				- 
WAB 2006-07-18
WAB 2007-01-31	- changed sendemail to use com.email.sendemail
NYB 2009-04-16 	-	Sophos Bugzilla 2041 - changed sendemail in JumpValue=sendemail to use currentuser, or 404, where frmPersonid doesn't exist
NJH 2009/04/20	- 	Bug Fix All Sites Issue 2078 - stop if the processID does not exist or if it's not numeric.
WAB 2010/10/13  removed get TextPhrases and replaced with CF_translate

2012-09-19	PPB/WAB	Case 430647 add "abort" as a process action (as defined in processJumpAction)					
2015-12-03  WAB/DAN Case 446541 - fix for setting the theEntityID if the "EntityID" when form/url key is not available

Enhancement still to do:

New actionTypes:
create selection
set table.column
get table.column
send email
send email to selection

 --->

<cfset variables.jumpToURL = "">
<cfset variables.jumpAction = "">
<cfset variables.nextStepID = 1>

<cfset variables.frmItem = 1>
<CFSET fnfrmName = ArrayNew(1)>
<CFSET fnfrmValue = ArrayNew(1)>
<!--- 2007-03-07 WAB Lenovo Project - reordered the order we look for variables - basically so that form variables come before URL - prevented problems when a forms action contained cgi_query string (which contained frmprocessid) and also had form variable --->
<cfif structKeyExists(Form,"frmProcessID")>
  <cfset Variables.processID = trim(Form.frmProcessID)>
<cfelseif structKeyExists(URL,"frmProcessID")>
  <cfset Variables.processID = trim(URL.frmProcessID)>
<cfelseif structKeyExists(variables,"frmProcessID")>
  <cfset Variables.processID = trim(variables.frmProcessID)>
</cfif>

<cfif structKeyExists(Form,"frmStepID")>
  <cfset Variables.stepID = trim(Form.frmStepID)>
<cfelseif structKeyExists(URL,"frmStepID")>
  <cfset Variables.stepID = trim(URL.frmStepID)>
<cfelseif structKeyExists(Variables,"frmStepID")>
  <cfset Variables.stepID = trim(Variables.frmStepID)>
</cfif>


<!--- added feature so that can pass a textlabel in frmProcessID, rather than a frmProcessID/StepID pair 
	and WAB 2006-06-06 can pass frmProcessID with processid-stepid  
--->

<!--- NJH 2009/04/20 Bug Fix All Sites Issue 2078 - temp variable to hold processID --->
<cfset origProcessID = variables.ProcessID>

<CFIF not isnumeric(variables.ProcessID)>
	<cfif listLen(variables.ProcessID,"|") is 2 and isNumeric(listfirst(variables.ProcessID,"|")) and isNumeric(listlast(variables.ProcessID,"|"))>
		<cfset variables.stepid = listlast(variables.ProcessID,"|")>
		<cfset variables.processid = listfirst(variables.ProcessID,"|")>
	<cfelse>
		<cfquery Name="GetProcessActions" DataSource="#application.siteDatasource#">
		select processid, stepid
		  from ProcessActions a
		 where a.processsteptextid =  <cf_queryparam value="#variables.processid#" CFSQLTYPE="CF_SQL_VARCHAR" >  and active = 1
		</cfquery>
		<CFSET variables.stepid = getprocessactions.stepid>
		<CFSET variables.processid = getprocessactions.processid>
	</cfif>
	
	<CFSET variables.frmprocessid = variables.processid>
	
</cfif>

<!--- NJH 2009/04/20 Bug Fix All Sites Issue 2078 - check if the process ID does exist before continuing. If not, abort. --->
<cfif not isNumeric(variables.ProcessID)>
	<cfoutput>
	<cfif variables.ProcessID eq "">
		ProcessID #origProcessID# does not exist.	
	<cfelse>
		ProcessID #variables.ProcessID# is not numeric.
	</cfif>
	</cfoutput>
	<CF_ABORT>
</cfif>

<!--- Test for ProcessID = -1 --->
<!--- ProcessID set to this value by the viewer if a new screen or entity is chosen
		don't exectute the redirector, but goto frmNextPage --->
<CFIF processID is -1>
	<CFINCLUDE TEMPLATE="#frmnextpage#">
	<CF_ABORT>
</CFIF>

<!--- if just passed a processid with no step id then lets find the lowest step in the process --->

<cfif isDefined("processid") and (not isDefined("stepid") or stepid is 0)>
	<cfquery Name="GetFirstStep" DataSource="#application.siteDatasource#">
		select min(stepid) as firststep
		  from ProcessActions 
		 where processid =  <cf_queryparam value="#variables.ProcessID#" CFSQLTYPE="CF_SQL_INTEGER" >  and active = 1
	</cfquery>
	<cfset variables.stepid = GetFirstStep.firststep>
</cfif>

<!--- WAB June 2006 - added something to store process variables in a session variable, accessible as thisProcess. 
	any variables set in the course of the process are stuffed in here
--->
<cfif not isDefined("frmProcessUUID") or not structKeyExists(session,"Processes") or not structKeyExists(session.Processes,frmProcessUUID)>
	<cfset  frmProcessUUID = createUUID()>
	<cfparam name="session.Processes" default="#structNew()#">
	<cfset session.Processes[frmProcessUUID] = structNew()>
	<cfset thisProcess = session.Processes[frmProcessUUID]>
	<cfset thisProcess.x = 1>
	<cfset thisProcess.processid = variables.ProcessID  & "|" &  variables.stepID  >
	<!--- read in parameters from processheader --->
	<cfquery Name="GetHeader" DataSource="#application.siteDatasource#">
	select * from processHeader where processid =  <cf_queryparam value="#variables.ProcessID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</cfquery>
	<cfset headerparametersStruct = application.com.regexp.convertNameValuePairStringToStructure(GetHeader.parameters,",")>			
	<cfset thisProcess = application.com.structurefunctions.structMerge (thisProcess,headerParametersStruct)>
<cfelse>
	<cfset thisProcess = session.Processes[frmProcessUUID]>
</cfif>

	

<CFSET actionsCompleted = false>
<cfset request.processAction = structNew()>  <!--- WAB testing for CR_SNY584--->
<cfset request.processAction.uuid = frmProcessUUID>
<cfset request.processAction.continueCallingTemplate = false>
<cfset request.processAction.continueButton = structNew()>
<cfset request.processAction.continueButton.show = false>
<cfset request.processAction.continueButton.label = "phr_continue">
<cfset request.processAction.CancelButton.show = false>
<cfset request.processAction.CancelButton.label = "phr_cancel">

<CFSET previousprocessid = 0>
<CFSET previousstepid = 0>
<CFSET gotosequence = 0>   <!--- used to jump to another sequenceid in the same step --->
<CFSET loopcount=1>

<cfset fn = application.com.redirectorAllowedFunctions>   <!--- alias this cfc so all methods can be called --->

<!--- this function is actually the same as one in relaytranslations, but having it in this template seems to allow it to work with local variables in the calling page 
Since a function cannot be defined more than once and redirector.cfm sometimes gets called recursively we have to put it in a separate file which is only included once
WAB 2017-01-23  (For AHL Panduit Case) Changed the test for previous inclusion.  Was a case where redirector was included twice in the same request but in different scope (so testing a request variable gave the wrong answer)
A test for the function localCheckForAndEvaluateCFVariables already being defined is actually far more sensible
--->
<cfif not isDefined("localCheckForAndEvaluateCFVariables")>
	<cfinclude template="/relay/screen/redirector_localCheckForAndEvaluateCFVariablesFunction.cfm">
</cfif>

<!--- work out the entityType and entityID --->
<cfset theEntityType = "">
<cfset theEntityTypeID = "">
<cfset theEntityID = 0>
<cfset theUniqueKey = "">

<cfif structKeyExists(form,"entityType")>
	<cfset theEntityType = form.entityType>
<cfelseif structKeyExists(url,"entityType")>
	<cfset theEntityType = url.entityType>
<cfelseif structKeyExists(variables,"entityType")>
	<cfset theEntityType = variables.entityType>
<cfelseif structKeyExists(form,"frmTableList")>
	<cfset theEntityType = listFirst(form.frmTableList)> <!--- how do we deal with this when this is longer than 1 item?? --->
</cfif>

<cfif structKeyExists(form,"entityTypeID")>
  	<cfset theEntityTypeID = form.entityTypeID>
<cfelseif structKeyExists(url,"entityTypeID")>
	<cfset theEntityTypeID = url.entityTypeID>
</cfif>

<cfif theEntityType neq "" or theEntityTypeID neq "">
  	<cfif theEntityType neq "">
  		<cfset theUniqueKey = application.com.relayEntity.getEntityType(entityTypeID=theEntityType).uniqueKey>
  	<cfelse>
  		<cfset theUniqueKey = application.entityType[theEntityTypeID].uniqueKey>
		<cfset theEntityType = application.entityType[theEntityTypeID].tablename>
  	</cfif>
 	<cfif isNumeric(theEntityType)>
		<cfset theEntityTypeID = theEntityType>
		<cfset theEntityType = application.entityType[theEntityTypeID].tablename>
	</cfif>
	
	<cfif structKeyExists(form,"entityID")>
		<cfset theEntityID = form.entityID>
	<cfelseif structKeyExists(url,"entityID")>
		<cfset theEntityID = url.entityID>
	<cfelseif structKeyExists(variables,"entityID")>
		<cfset theEntityID = variables.entityID>
	<cfelseif structKeyExists(form,theUniqueKey)>
		<cfset theEntityID = form[theUniqueKey]>
    <cfelseif structKeyExists(form,"frm#theUniqueKey#")>
        <cfset theEntityID = form["frm#theUniqueKey#"]> <!--- 2015-12-03 WAB/DN Case 446541 --->
	<cfelseif structKeyExists(url,theUniqueKey)>
		<cfset theEntityID = url[theUniqueKey]>
	</cfif>
</cfif>

<cfparam name="variables.validJumpActions" default=""> <!--- NJH 2014/09/03 - added for the connector so that we only run certain jumpActions... protect the user and the connector from silly configuration --->
			
<CFLOOP condition="actionsCompleted is false and not (variables.processid is previousprocessid and variables.stepid is previousstepid)">
	<!--- to catch a never ending loop --->
	<CFSET loopcount=loopcount+1>
	<CFIF loopcount GT 20>
		<cfmail to="application.AdminEmail"
        from="application.AdminPostmaster"
        subject="Attention: Process action problem"
        >
		Processid = #variables.processid#<br>
		Stepid = #variables.stepid#<br>
		appname = #cookie.appname#<br>		
		User = #cookie.user#<br>
		Site = #apllication.sitename#		
		</cfmail>

		<CF_ABORT>
	</cfif>

	<cfquery Name="GetProcessActions" DataSource="#application.siteDatasource#">
		select jumpExpression, jumpAction, jumpName, JumpValue, parameters,NextStepID, sequenceID
		  from ProcessActions a
		 where a.processID =  <cf_queryparam value="#variables.processID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		   and a.stepID =  <cf_queryparam value="#variables.stepID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		   and active = 1
		   <cfif variables.validJumpActions neq "">
			and jumpAction in (<cf_queryparam value="#variables.validJumpActions#" cfsqltype="cf_sql_varchar" list="true">)
			</cfif>
		   order by a.sequenceID
	</cfquery>

	<CFSET previousProcessId=variables.processid>
	<CFSET previousStepId=variables.stepid>
	
	<cfloop query="GetProcessActions">
		
		<cfif gotosequence is GetProcessActions.sequenceid>	
			<!---  --->
			<CFSET gotosequence = 0>
		</CFIF>
	 
	 	<cfif jumpExpression contains "application.com">
			<cfoutput>Illegal function call: #jumpExpression#.  <CF_ABORT></cfoutput>
		</cfif>
	
		<!--- if jumpexpression is blank then we use the previous result, 
			if jumpexpression is "else" then we reverse the previous result
		 --->
		 
		<cftry>
		
			<cfif jumpExpression is "" and isDefined("jumpExpressionResult")>
				<!--- use previous result --->
			<cfelseif jumpExpression is "else" and isDefined("jumpExpressionResult")>
				<cfset jumpExpressionResult = not jumpExpressionResult>
		
			<cfelseif left(jumpExpression,7) is "elseif " and isDefined("jumpExpressionResult")>
				<cfset jumpExpressionResult = not jumpExpressionResult and Evaluate(right(jumpExpression,len(jumpExpression)-6))>
			<cfelse>
				<cfset jumpExpressionResult = Evaluate(jumpExpression)>
			</cfif>
	
			<cfcatch type="Any">
				<cfoutput>Error evaluating #htmleditformat(jumpExpression)#</cfoutput>
				<cfdump var="#CFCATCH#">
				<cfrethrow> 
				
			</cfcatch>
		</cftry>

		
		<CFIF isDefined("DebugProc")>
			<CFOUTPUT>
			<table border="1" cellpadding="2">
				<tr><th>DEBUG:</th></tr>	
			 	<tr><td>ProcID: #htmleditformat(processid)# StepID: #htmleditformat(stepid)# SequenceID: #htmleditformat(sequenceID)#</td></tr> 
				<tr><td>#htmleditformat(jumpexpression)# : #htmleditformat(jumpexpressionResult)#</td></tr> 
				<cfif jumpexpressionResult>
					<tr><td>JumpAction: #htmleditformat(jumpaction)# </td></tr>
					<tr><td>jumpName : #htmleditformat(jumpname)#</td></tr>
					<tr><td>jumpValue : #htmleditformat(jumpvalue)#</td></tr>
				<cfelse>
					<tr><td>No Action Taken</td></tr>
				</cfif>
			</table>
			</cfoutput>
		</CFIF>			


	
		<cfif jumpExpressionResult and gotosequence is 0>	

			<cfset thisJumpName = localCheckForAndEvaluateCFVariables(jumpname)>
			<cfset thisJumpValue = localCheckForAndEvaluateCFVariables(jumpValue)>
			<cfset thisParametersStruct = application.com.regexp.convertNameValuePairStringToStructure(parameters,",")>			

			<!--- WAB 2006-05-23 
					needed a way of getting continue buttons to show on elements included in a process flow
					so trying out the idea of a request structure which gets picked up in both screens and elements
					(bit awkward if a screen is included in an element, so would need to show the continue button on the screen and not at the bottom of the element)
			--->
			
			<cfif NextStepID is -1 >
				<!--- WAB trying a way of terminating a process --->
				<cfset request.processAction.continueButton.show = false> 
				<cfset request.processAction.processID = -1> 
				<cfset request.processAction.nextstepid = -1> 				
		
			<cfelseif (NextStepID is not 0 and NextStepID is not "") and jumpAction is not "newStep">
				<cfset request.processAction.continueButton.show = true> 
				<cfset request.processAction.processID = processid> 
				<cfset request.processAction.nextstepid = nextStepID> 				
			<cfelse>
				<cfset request.processAction.continueButton.show = false> 
			</cfif>


			<cfswitch expression = "#lcase(jumpAction)#">
			  	<cfcase value="value"> 
	
						<CFSET "#thisjumpName#" = thisjumpValue>
						<CFSET fnfrmName[#variables.frmItem#] = thisjumpName>
						<cfset fnfrmValue[#variables.frmItem#] = thisJumpValue>
						<cfset thisProcess[thisjumpName] = thisjumpValue>
						<cfset variables.frmItem = variables.frmItem + 1>
						
	
				</cfcase>
	
	
			  	<cfcase value="include"> 
					<cfset variables.NextStepID = getProcessActions.nextStepID>
		
					<CFTRY>
						<cfinclude template="#thisJumpName#">				
						<cfcatch type="MissingInclude">
							<CFOUTPUT>Unable to find page #htmleditformat(cfcatch.missingfilename)#</cfoutput> <!--- WAB 2007-04-17 changed the filename output from #thisJumpName# to cfcatch...  --->
						</CFCATCH>
					</CFTRY>	
					
					<CFIF (nextstepid is not 0)> 
						<CFSET actionsCompleted = true>
						<CFBREAK> 
					</CFIF>
	
				</cfcase>
	
				<!--- 2012-09-19 PPB/WAB Case 430647 add "abort" as a process action --->
			  	<cfcase value="abort">
					<cf_abort>
				</cfcase>
	
			  	<cfcase value="screen"> 
	
					<cfset variables.NextStepID = #getProcessActions.nextStepID#>
					<CFSET frmCurrentScreenID = thisJumpValue>
	
					<cfif structKeyExists(thisParametersStruct,'hiderightborder')>
						<cfset request.hiderightborder = thisParametersStruct.hiderightborder>
					</cfif>
	
					<CFIF not isDefined("frmPersonid") and not isDefined("frmLocationid") and not isDefined("frmOrganisationid") and not request.relaycurrentUser.isUnknownUser>  <!--- WAB 2006-05-22 added so that frmPersonID does not have to be defined everytime in the process --->
						<cfset frmPersonid = request.relaycurrentUser.personid>
					</CFIF>
					
					<cfset frmButtonAsLink = false>
					<CFINCLUDE template= "/screen/screen.cfm">		
					<CFIF (nextstepid is not 0)> 
						<CFSET actionsCompleted = true> 
						<CFBREAK> 
					</CFIF>
				</cfcase>
	
			  	<cfcase value="gotoElement"> 
	
					<cfset form.NextStepID = getProcessActions.nextStepID>
					<CFSET eid = thisJumpValue>
					<cfset standardborderset = "">   <!--- this line was fudged in for Lenovo release don't release to other boxes.  Needs some thought --->
					<CFINCLUDE template= "/elementTemplate.cfm">
					<CF_ABORT>  <!--- wab --->
					
				</cfcase>
				
			  	<cfcase value="message"> 
	
					<cfset #variables.NextStepID# = #getProcessActions.nextStepID#>
					<cfset message = thisJumpValue>
					<cfif structKeyExists(thisParametersStruct,'ShowCancelButton')>
						<cfset request.processAction.CancelButton.show = thisParametersStruct.ShowCancelButton>
					</cfif>
					<cfif structKeyExists(thisParametersStruct,'CancelButtonLabel')>
						<cfset request.processAction.CancelButton.label = thisParametersStruct.CancelButtonLabel>
					</cfif>
					<cfif structKeyExists(thisParametersStruct,'continueButtonLabel')>
						<cfset request.processAction.continueButton.label = thisParametersStruct.continueButtonLabel>
					</cfif>
					<cfif structKeyExists(thisParametersStruct,'hiderightborder')>
						<cfset request.hiderightborder = thisParametersStruct.hiderightborder>
					</cfif>
	
					
					<cfloop collection="#thisParametersStruct#" item="variablename">
						<CFSET "#variablename#" = thisParametersStruct[variableName]>
					</cfloop>
	
					<cfoutput>#application.com.relayUI.message(message=message,showClose=false)#</cfoutput>
					<!--- <CFINCLUDE template = "/templates/processmessage.cfm">	 --->	
					<CFSET actionsCompleted = true> 
					<CFBREAK>  <!--- wab --->
					
					
				</cfcase>
	
				
				<!--- this sends an email using a generic email sender --->	
			  	<cfcase value="sendemail"> 
				  	
				  	<!--- add support for merge fields. Try and get the entityID based on the entityType --->
				  	<cfset mergeStruct = {}>
	
					<cfset emailPersonID = 404>
					<cfif isDefined("frmPersonid")>
						<cfset emailPersonID = frmPersonid>
					<cfelseif theUniqueKey eq "personID">
						<cfset emailPersonID = theEntityID>
					<cfelseif isDefined("request.relaycurrentuser.personid")>
						<cfset emailPersonID = request.relaycurrentuser.personid>
					</cfif>	
	
					<cfif theEntityType neq "">
						<cfset mergeStruct[theUniqueKey] = theEntityID>
						<cfset application.com.email.sendEmail(emailtextID=thisJumpValue,personID=emailPersonID,mergeStruct=mergeStruct,entityID=mergeStruct[theUniqueKey],entityTypeID=application.entityTypeID[theEntityType])>
					<cfelse>
						<cfset application.com.email.sendEmail(emailtextID=thisJumpValue,personID=emailPersonID,mergeStruct=mergeStruct)>
					</cfif>
				</cfcase>
	
			  	<cfcase value="output"> 
					<!--- WAB 2012-03-27 HTMLEditFormat has been removed from here because sometimes javascript links are put here - but could be a security risk --->
					<CFOUTPUT>#application.com.security.sanitiseHTML(evaluate(jumpvalue))#</cfoutput>
	
				</cfcase>
	
			  	<cfcase value="DUMP"> 
					<CFDUMP VAR="#evaluate(jumpvalue)#">
					
			  	<!--- this looks up a phrase and creates the phr_ variable --->
				</cfcase>
	
			  	<cfcase value="getphrase"> 
	
					<CFSET textPhraseList = "'#thisJumpValue#'">
					<cf_translate phrases = "#textPhraseList#">
				
				</cfcase>
	
	
			  	<cfcase value="setphrase"> 
				<!--- this looks up a phrase and sets the #jumpname# variable equal to the phrase value--->
	
					<CFSET textPhraseList = "'#thisjumpValue#'">
					<cf_translate phrases = "#textPhraseList#">
					<CFSET "#jumpName#" = evaluate("phr_#thisjumpValue#")>		
					<CFSET fnfrmName[#variables.frmItem#] = #thisJumpName#>
					<CFSET fnfrmValue[#variables.frmItem#] = evaluate("phr_#thisjumpValue#")>
	
			  	<!--- run a bit of sql--->
				</cfcase>
	
	
			  	<cfcase value="sql"> 
	
					<cfset variables.theSQL = thisJumpvalue>
					<cfquery Name="#jumpName#" DataSource="#application.siteDatasource#">
						#preservesinglequotes(theSQL)#
					</cfquery>
	
	
				</cfcase>
	
			  	<!--- <cfcase value="predefinedSQL"> 
	
					<cfquery Name="getSQL" DataSource="#application.siteDatasource#">
						Select SQLStatement from processSQL where queryName =  <cf_queryparam value="#jumpname#" CFSQLTYPE="CF_SQL_VARCHAR" > 
					</cfquery>
					
					<cfquery Name="#jumpName#" DataSource="#application.siteDatasource#">
						#preservesinglequotes(getSQL.sqlstatement)#
					</cfquery>
	
	
				</cfcase> --->
	
			  	<cfcase value="getappdetails"> 
	
					<!--- run query to get appdetails --->
					<cfquery Name="getAppDetails" DataSource="#application.siteDatasource#">
						Select * from application where appName =  <cf_queryparam value="#cookie.appname#" CFSQLTYPE="CF_SQL_VARCHAR" > 
					</cfquery>
	
	
				</cfcase>
	
			  	<cfcase value="gotosequence"> 
	
					<CFSET gotosequence = thisjumpvalue>
	
				
				</cfcase>
	
				<!--- 
					WAB 2006-05-30  CR_SNY584
					allow for exiting from redirector and continuing processing in the calling template			
				
				 --->
			  	<cfcase value="exit"> 
	
						<cfset request.processAction.continueCallingTemplate = true>
						<CFSET actionsCompleted = true> 
						<cfset structDelete(session.Processes,frmProcessUUID)>
						<cfbreak>
						
				</cfcase>
	
	
			  	<cfcase value="newstep"> 
					<CFSET #Variables.stepID# = #getProcessActions.nextStepID#>
					<cfbreak>
	
	
				</cfcase>
	
			  	<cfcase value="newprocess"> 
	
					<CFSET variables.processID = getProcessActions.jumpvalue>
					<CFSET variables.frmprocessID = getProcessActions.jumpvalue>
					<cfif getProcessActions.nextStepID is 0>
						<cfquery Name="GetFirstStep" DataSource="#application.siteDatasource#">
							select min(stepid) as firststep
							  from ProcessActions 
							 where processid =  <cf_queryparam value="#variables.ProcessID#" CFSQLTYPE="CF_SQL_INTEGER" >  and active = 1
						</cfquery>
						<cfset variables.stepid = GetFirstStep.firststep>
					<cfelse>
						<CFSET variables.stepID = getProcessActions.nextStepID>
					</cfif>
					<CFSET variables.frmstepID = variables.stepID>
					
	
	
					<cfbreak>
	
	
				</cfcase>
	
			  	<cfcase value="getFlag"> 
	
					<CFSET flagtextid= thisjumpvalue>
	
						<!---  this query can handle multiple flagtextids, but does assume that they are all in the same flaggroup (or atleast are all have the same entitytype)--->
					<cfquery Name="getFlagGroup" DataSource="#application.siteDatasource#">		  
				  		Select fg.*, f.*, 
			  				flagType.Name as typename,flagType.dataTableFullName 
			  			from 	
				  			flaggroup as fg 
				  			inner join flag as f on fg.flaggroupid = f.flaggroupid
							inner join FlagType ft on fg.FlagTypeID = ft.FlagTypeID 
						where 
						f.flagtextid in (<cf_queryparam value="#flagtextid#" CFSQLTYPE="CF_SQL_INTEGER"  list="true">)
					</CFQUERY>		  
	
					
					<cfquery Name="#jumpName#" DataSource="#application.siteDatasource#">				
						select * 
						from <!--- #listgetat(application.typeDataList,getFlagGroup.flagtypeid)#FlagData --->
							#getFlagGroup.dataTableFullName#
						WHERE FlagID  in ( <cf_queryparam value="#valuelist(getFlagGroup.FlagID)#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
						<!--- NJH 2010/08/09 RW8.3 - replace this with below --AND EntityID = #evaluate("frm#listgetat(application.typeEntityTable,getFlagGroup.entitytypeid+1	)#id")# --->
						AND EntityID =  = #evaluate("frm#application.entityType[getFlagGroup.entitytypeid].uniqueKey#")#
					</CFQUERY>		
	
	
				</cfcase>
	
			  	<cfcase value="setFlag"> 
					<!--- just for single flagTextIDs --->
	
					<CFSET flagtextid= jumpName>		  
	
					<cfquery Name="getFlagGroup" DataSource="#application.siteDatasource#">		  
			  		Select *, ft.name as TypeName from flaggroup as fg, flag as f, flagEntityType as fet, flagType as ft
					where fg.flaggroupid = f.flaggroupid
					and	fg.entityTypeID = fet.entityTypeID
					and	fg.flagTypeID = ft.flagTypeID
					and f.flagtextid =  <cf_queryparam value="#flagtextid#" CFSQLTYPE="CF_SQL_VARCHAR" > 
					</CFQUERY>		  
					
				  	<cfset frmentityID = theEntityID>
				  	
				  	<cfif not isdefined("frmentityID") or not isNumeric("#frmentityID#") or frmentityID eq 0>
						<!--- work out entityType and ID  and set up variables to call flagtask--->
						<CFSET frmentityID = evaluate("frm#getFlagGroup.TableName#id")>
					</cfif>
					<!--- 2013-07-12	YMA	Case 436172 put variables in the form scope for reference in flagTask.cfm --->
					<CFIF getFlagGroup.flagTypeId is 2 or getFlagGroup.flagTypeId is 3>
						<CFSET flagField = "#getFlagGroup.TypeName#_#getFlagGroup.FlagGroupID#">
						<CFIF jumpValue is 1>
							<CFSET form["#flagField#_#frmEntityID#"] = getFlagGroup.flagid >
							<CFSET form["#flagField#_#frmEntityID#_Orig"] = "">
						<CFELSE>
							<CFSET form["#flagField#_#frmEntityID#"] =  "">
							<CFSET form["#flagField#_#frmEntityID#_Orig"] = getFlagGroup.flagid>
						</cfif>
					<CFELSE>
						<CFSET flagField = "#getFlagGroup.TypeName#_#getFlagGroup.FlagID#">
						<CFSET form["#flagField#_#frmEntityID#"] = thisjumpValue >
						<CFSET form["#flagField#_#frmEntityID#_Orig"] = "">
					</cfif> 
					
					<CFSET flaglist= flagField>
					<CFSET frmTask = "update">
					
					<CFINCLUDE template="..\flags\flagtask.cfm">			
	
					<CFIF (getProcessActions.nextStepID is not 0)> 
						<CFSET nextStepID  = getProcessActions.nextStepID> 
						<CFSET actionsCompleted = true> 
						<CFBREAK> 
					</CFIF>
	
	
				</cfcase>
	
			  	<cfcase value="customtag"> <!--- an idea of WAB June 2006 - not full implemented, but should work - note that dot notation is used and tag must be under the customtags directory--->
					<cfmodule name="#thisJumpValue#" attributeCollection="#thisParametersStruct#"> 
				</cfcase>
	
			  	<cfcase value="relaytag"> <!--- an idea of WAB June 2006 - not fully programmed yet, although might work on some relay tags - but most would need to know about frmprocessid --->
	
					<cfset tagName = thisJumpValue>
					<cfif structKeyExists(application.RelayTagKeyedByTagName,tagName) >
						<cfset matchfound = true>
						<cfset theTag = application.RelayTagKeyedByTagName[tagName]>
						<CFSET nSpecificValues = #theTag.SpecificValues#>
						<CFIF nSpecificValues>
							<CFINCLUDE TEMPLATE="TagTemplates/#theTag.TagTagTextID#_Tag.cfm">
						</CFIF>
	
						
						<cfif theTag.isCustomTag>
							<cfmodule template="#theTag.tagPath#"  attributecollection="#thisParametersStruct#">
						<cfelse>
							<cfloop collection="#thisParametersStruct#" item="variablename">
								<CFSET "#variablename#" = thisParametersStruct[variableName]>
							</cfloop>
							<cfinclude template="#theTag.tagPath#">
						</cfif>
					<cfelse>
							<cfoutput>|#htmleditformat(tagname)#| does not exist
						</cfoutput>
						
					</cfif>
	
				</cfcase>
	
			  	<cfcase value="submit,location"> 
					<!--- <cfelseif (LCase(jumpAction) IS "submit") OR (LCase(jumpAction) IS "location") or (#nextstepid# is not 0) >   --->				
					<!--- wab added next step bit --->
					<cfset variables.jumpToURL = #evaluate(DE(getProcessActions.jumpName))#>
					<cfset variables.jumpAction = #getProcessActions.jumpAction#>
					<cfset variables.NextStepID = #getProcessActions.nextStepID#>
					<CFSET actionsCompleted = true>
					<cfbreak>
				</cfcase>
				
				<cfdefaultcase>
					<cfif #nextstepid# is not 0>
						<!--- same as submit/location above --->
						<cfset variables.jumpToURL = #evaluate(DE(getProcessActions.jumpName))#>
						<cfset variables.jumpAction = #getProcessActions.jumpAction#>
						<cfset variables.NextStepID = #getProcessActions.nextStepID#>
						<CFSET actionsCompleted = true>
						<cfbreak>
					</cfif>
				</cfdefaultcase>
			
				
			</cfswitch>	

		</cfif>

	</cfloop>


	<!--- <cfif variables.frmJumpMode IS "direct">

		<cfset variables.jumpToURL = #getProcessActions.jumpName#>
		<cfset variables.jumpAction = #getProcessActions.jumpAction#>
		<cfset variables.NextStepID = #getProcessActions.nextStepID#>

		<cfset conjunction = "?">
		<cfif variables.jumpToURL contains "?">
			<cfset conjunction = "&">
		</cfif>

		<cflocation URL="#variables.jumpToURL##conjunction#frmProcessID=#variables.processID#&frmStepID=#variables.NextStepID#"addToken="false"> 
		<CF_ABORT>
	</cfif> --->


	
</CFLOOP>	
<cfif Variables.jumpAction IS "submit">
<!--- only need to create form for submit --->

	<cf_body onLoad="jsSubmitForm()">
	<SCRIPT type="text/javascript">
	<!--

	function jsSubmitForm() {
	//---------------------
		//alert("submitting form");
		var form = document.redirectForm;
		form.submit();	
	}

	// -->
	</script>
	<A HREF="javascript:jsSubmitForm()">Click here if this page does not submit automatically</a>

	<!--- 		 now create form to contain all parameters received from calling page --->
	<form name="redirectForm" method="post" action="<cfoutput>#htmleditformat(variables.jumpToURL)#</cfoutput>">

		<CF_INPUT type="hidden" name="frmProcessID" value="#variables.processID#">
		<CF_INPUT type="hidden" name="frmStepID" value="#variables.NextStepID#">	

		<CFIF structKeyExists(Form,"fieldnames")>
		<CFLOOP INDEX="j" 
			LIST="#Form.fieldnames#">
			<cfif ( UCase(#j#) IS NOT 'FRMPROCESSID') AND (UCase(#j#) IS NOT 'FRMSTEPID')>
				<CF_INPUT type="hidden" name="#j#" value="#Evaluate(j)#">
			</cfif>
		</CFLOOP>
		</CFIF>

		<CFSET Array_Length = ArrayLen(fnfrmName)>
		<cfif Array_Length>
			<CFLOOP INDEX="j" FROM="1" TO="#Array_Length#" Step="1">   
			  <CF_INPUT type="hidden" name="#fnfrmName[j]#" value="#fnfrmValue[j]#">
			</CFLOOP>
		</CFIF>

		<!--- pass through the entity information for email merge fields... I'm assuming we will need to submit them --->
		<cfif structKeyExists(url,"entityType")>
			<cfif not structKeyExists(form,"entityType")>
				 <CF_INPUT type="hidden" name="entityType" value="#url.entityType#">
			</cfif>
			<cfset uniqueKey = application.com.relayEntity.getEntityType(entityTypeID=url.entityType).uniqueKey>
	  		<cfif structKeyExists(url,uniqueKey) and not structKeyExists(form,uniqueKey)>
	  			 <CF_INPUT type="hidden" name="#uniqueKey#" value="#url[uniqueKey]#">
	  		</cfif>
	  		<cfif structKeyExists(url,"entityID") and not structKeyExists(form,"entityID")>
	  			 <CF_INPUT type="hidden" name="entityID" value="#url.entityID#">
	  		</cfif>
		</cfif>

	</form>

	
	
<!--- --------------------------------------------------------------------- --->
<!--- avoid client-side round trip --->
<!--- --------------------------------------------------------------------- --->
<!--- I think that the above cfif can be replaced by something like this code, to keep the interaction server-side --->
<!--- 
	<cfset url = StructNew()>
	<cfset form["frmProcessID"] = variables.processID>
	<cfset form["frmStepID"] = variables.NextStepID>
	<cfloop index="frmNameIndex" from="1" to="#ArrayLen( fnfrmName )#">
		<cfset form[fnfrmName[frmNameIndex]] = fnfrmValue[frmNameIndex]>
	</cfloop>
	<cfinclude template="#variables.jumpToURL#"> <!--- the url will have to be dressed up in order to turn it into a path --->
	<CF_ABORT>
 --->
<!--- --------------------------------------------------------------------- --->
</cfif>


<!--- 		debug email
	<cfmail to="william@foundation-network.com"
        from="postmaster@foundation-network.com"
        subject="redirector end"
        server="mailhost.foundation-network.com">
		Processid = #variables.processid#
		Stepid = #variables.stepid#
		appname = #cookie.appname#		
		<CFIF isDefined("request.relayCurrentUser.personid")>user = #cookie.user#		</cfif>
		</cfmail>
 --->
	

<cfif Variables.jumpAction IS "submit">
	
<!--- 		WAB: I have replaced this with an onLoad statement - 
			it seems to work with both IE and navigator 
			- although I thought that there was some problem with one of them
		<SCRIPT type="text/javascript">
			jsSubmitForm();
		</script>
 --->
		<!---  <A href="javascript:jsSubmitForm()">test</a> --->
<cfelseif Variables.jumpAction IS "location"> 
	
	<!-- jump to '<cfoutput>#htmleditformat(variables.jumpToURL)#</cfoutput>' -->
	<!--- don't pass processid unless one is defined --->
	<CFIF variables.NextStepID is NOT 0>

			<cfset conjunction = "?">
		<cfif variables.jumpToURL contains "?">
			<cfset conjunction = "&">
		</cfif>

		<cflocation URL="#variables.jumpToURL##conjunction#frmProcessID=#variables.processID#&frmStepID=#variables.NextStepID#" addToken="false">		
	<CFELSE>
		<cflocation URL="#variables.jumpToURL#" addToken="false">
	</CFIF>	


	

</cfif>



