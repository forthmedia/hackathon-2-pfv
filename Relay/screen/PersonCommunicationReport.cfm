<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Communications sent to a particular person 

NJH		2009/01/26	Bug Fix Trend Support Issue 1438 - moved the query to a function and changed the layout.
		This file is now getting included by relay/data/personCommSummary.cfm
--->

<CFIF frmentitytype is "person">
	<CFSET personid = frmcurrententityid>
<CFELSE>	 
	
	<CFSET message = "No Person ID set.">
	<!--- <cFINcLUDE TEMPLATE="../templates/message.cfm"> --->
	<cfoutput>#application.com.relayUI.message(message=message,type="warning")#</cfoutput>
	<cFINcLUDE template="errorform.cfm">
	<CF_ABORT>

</CFIF>	

<!--- <CFQUERY name="CommsSent" dataSource="#application.SiteDataSource#" >
SELECT 	c.Title, 
		c.commid, 
		--c.SendDate, NJH 2009/01/23 Bug Fix Trend Support Issue 1438 - use dateSent instead as sendDate is the same for every web visit and adhoc email
		cd.dateSent,
		LL1.ItemText as FormText, 
		LL.ItemText as TypeText,
		status.Name as statustext,
		cd.commStatusID as status,
		cd.feedback as feedback,
		cd.fax as addressused
/*FROM CommDetail as cd,  Communication as c, LookupList as LL, LookupList AS LL1, commdetailStatus as status
WHERE cd.CommID = c.CommID
AND c.CommTypeLID = LL.LookupID
AND cd.CommFormLID = LL1.LookupID
AND cd.commstatusid=status.commstatusid */
FROM CommDetail as cd with (noLock) inner join Communication as c with (noLock)
	on cd.commID = c.commID inner join LookupList as LL with (noLock)
	on c.CommTypeLID = LL.LookupID left join LookupList AS LL1 with (noLock)
	on cd.CommFormLID = LL1.LookupID inner join commdetailStatus as status with (noLock)
	on cd.commstatusid=status.commstatusid
AND cd.PersonID=#Personid#
AND c.Sent <>0  
AND c.CommFormLID <>4 
AND c.CommTypeLID <>50

ORDER BY cd.dateSent DESC
</CFQUERY> --->

<!--- NJH 2009/01/26 - Bug Fix Trend Support 1438 - in an effort to consolidate querys, made a function --->
<cfscript>
	commsSent = application.com.communications.getCommsSentForPerson(personId=Personid);
</cfscript>


<CFQUERY name="getperson" dataSource="#application.SiteDataSource#" DEBUG>
Select 	person.FirstName as firstname,
		person.lastName as lastname
From 	Person
Where	Personid =  <cf_queryparam value="#personid#" CFSQLTYPE="CF_SQL_INTEGER" > 
</cfquery>

<SCRIPT type="text/javascript">
<!--


	function jsSubmit() {
	//-----------------

	document.mainForm.submit()
	}

//-->
</SCRIPT>


<!--- <CFOUTPUT>
Communications Sent to <BR>
 #getperson.Firstname# #getperson.lastname#
</cfoutput> --->


<CFIF commssent.recordcount is not 0>

<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" CLASS="withBorder">
<tr><td colspan="6"><CFOUTPUT><strong>Communications Sent to #htmleditformat(getperson.Firstname)# #htmleditformat(getperson.lastname)#</strong></cfoutput></td></tr>
<TR bgcolor="cccccc">
<TH>Date</TH>
<TH>CommID</TH>
	<TH>Title</TH>
	<TH>By</TH>
	<TH>Type</TH>
	<TH>Address or Fax #</TH>
	<TH>Status</TH>
</TR>

<CFOUTPUT query="commssent" maxrows="70">
<TR <CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>

    <TD>#DateFormat(sendDate,"DD/MM/YY")#</TD>
	<TD>#htmleditformat(commid)#</TD>
	<TD>#htmleditformat(title)#</TD>
	<TD>#htmleditformat(formText)#</TD>
	<TD>#htmleditformat(TypeText)#</TD>
	<TD>#htmleditformat(Addressused)#</TD>
	<TD>#htmleditformat(statustext)# #iif(status LE 0, DE(". Feedback: " & feedback), DE(""))#</TD>
	
	
</TR>

</cfoutput>
</TABLE>

<CFELSE>

<P>	No Communications have been sent


</CFIF>
