<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
WAB 2010/10/13  removed get TextPhrases and replaced with CF_translate
--->


<CFIF isdefined('cookie.defaultlanguageid')>
	<CFPARAM name="frmlanguageid" default = "#cookie.defaultlanguageid#">
<CFELSE>
	<CFPARAM name="frmlanguageid" default = "1">
</cfif> 


<CFSET listofvalidjobfunctions="ManagingDirector,FinancialManager,marketingManager,SalesContact,ServiceContact,PurchaseContact">

<!--- can't work out whether locationid or frmlocationid is set on coming into this template - so using both! --->
	<CFPARAM name="frmlocationid" default="">
	<CFPARAM name="locationid" default="#frmlocationid#">
	<CFIF isdefined ("frmpersonid")>
		<CFSET personid = frmpersonid>
	</cfif>
	
	

<cFPARAM name="frmNoHeader" default="0">
<cFPARAM name="frmNoButton" default="0">
<cFPARAM name="frmnolookandfeel" default="0">
<cFPARAM name="TitleArea2" default="">	


<!--- get list of possible languages--->
<CFQUERY NAME="GetLangList" datasource="#application.siteDataSource#">
	select language from language order by language
</CFQUERY>

<!--- get name of current language --->
<CFQUERY NAME="GetLanguage" datasource="#application.siteDataSource#">
	SELECT language from language where LanguageID =  <cf_queryparam value="#frmlanguageID#" CFSQLTYPE="CF_SQL_INTEGER" > 
</CFQUERY>


<!--- get list of people at this location --->
<!---  hacked in max rows = 20.  really ought to have forward and back buttons etc.   ---->
<CFQUERY NAME="GetPeople" DATASOURCE="#application.SiteDataSource#" maxrows="20">
	Select p.personid, p.firstname, p.lastname, p.email, p.salutation, p.lastupdated, p.language, p.title
	from person as p 
	where p.locationid =  <cf_queryparam value="#locationID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	and active = 1
</CFQUERY>


<CFQUERY NAME="GetLocation" DATASOURCE="#application.SiteDataSource#" >
	Select SiteName, CountryID, countrygroupid
	From Location, countrygroup
	Where locationid =  <cf_queryparam value="#locationID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	and countrygroup.countrymemberid = location.countryid
</CFQUERY>


<CFPARAM NAME="Locname" DEFAULT="#GetLocation.sitename#">
<CFSET regions = valuelist (getLocation.countryGroupid)>


<cfquery name="getvalidvaluesalutation" datasource="#application.siteDataSource#">
select ValidValue
from ValidFieldValues
where Fieldname like 'Person.Salutation'
AND CountryID =  <cf_queryparam value="#GetLocation.countryid#" CFSQLTYPE="CF_SQL_INTEGER" > 
</cfquery>

<cfquery name="getvalidvalueTitle" datasource="#application.siteDataSource#">
select ValidValue
from ValidFieldValues
where Fieldname like 'Person.Title'
AND CountryID =  <cf_queryparam value="#GetLocation.countryid#" CFSQLTYPE="CF_SQL_INTEGER" > 
</cfquery>


	<CFQUERY NAME="getalljobfunctions" datasource="#application.siteDataSource#">
	select f.name, f.flagid, f.namephrasetextid
	from flag as f, flaggroup as fg
	where f.flaggroupid= fg.flaggroupid
	and fg.flaggrouptextid='JobFunction'
	and f.active = 1
	and fg.active = 1
	and f.flagtextid  in ( <cf_queryparam value="#listofvalidjobfunctions#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )
	order by f.name
	</CFQUERY>


	<CFQUERY NAME="getDeletionRequestFlagGroup" datasource="#application.siteDataSource#">
	select fg.flaggroupid, f.flagid
	from flag as f, flaggroup as fg
	where fg.flaggroupid = f.flaggroupid
	and f.flagtextid='DeletionRequest'
	</CFQUERY>


	<CFSET addoredit=iif(getpeople.recordcount is 0,DE("phr_addpeoplefor"),DE("phr_editaddpeoplefor"))>

	

<cf_translate encode="javascript">
<SCRIPT type="text/javascript">

<!--

	function verifyForm() {
		var form = document.frmmain;
		var msg = "";

		var userchecked = false
		for (j=0; j<form.elements.length; j++){
			thiselement = form.elements[j];
			if (thiselement.name == 'frmUser') {
					if (thiselement.checked==true)  {  
						userchecked = true
								}
			}
		}

		
		if (userchecked == false) {		
		
			msg += "* phr_whichrecordisyours\n";

		}



		if (msg != "") {
				alert("\nphr_Pleaseprovidethefollowinginformation:\n\n" + msg);

		} else {

			form.submit();
		}
	}
//-->

</SCRIPT>
</cf_translate>

	
<cf_translate>

<cf_head>
	<cf_title>
	<CFOUTPUT>#htmleditformat(addoredit)#</cfoutput>
	</cf_title>
</cf_head>



<CFINCLUDE TEMPLATE="../Styles/DefaultStyles.cfm"> 
<CFINCLUDE TEMPLATE="../Screen/WABScreenStyles.cfm">
<CFIF frmNoLookAndFeel is false>
	<CFINCLUDE TEMPLATE="#looknFeel#">
</cfif>


<cFIF frmNoHeader is 0>
<cFOUTPUT>
<CENTER>
	<TABLE BORDER=0 BGcOLOR="##FFFFcc" cELLPADDING="5" cELLSPAcING="0" WIDTH="500">
	
		<TR><TD cOLSPAN="2" ALIGN="cENTER" BGcOLOR="##000099"><FONT FAcE="Arial, chicago, Helvetica" SIZE="2" cOLOR="##FFFFFF">
		<B>#htmleditformat(ScreenTitleA)#</B></FONT></TD></TR>
		<TR><TD ALIGN="cENTER" cOLSPAN=2><FONT FAcE="Arial, chicago, Helvetica" SIZE=2>		
			#htmleditformat(PartnerArea)#
		<BR></FONT></TD>
		</TR>
			<TR><TD cOLSPAN="2" ALIGN="cENTER"><IMG SRc="/images/misc/rule.gif" WIDTH=230 HEIGHT=3 ALT="" BORDER="0"></TD></TR>
			<TR><TD ALIGN="cENTER" cOLSPAN=2><FONT FAcE="Arial, chicago, Helvetica" SIZE=2>
				<!--- Please complete the form below, amending any existing details if necessary. --->
				#htmleditformat(TitleArea2)#</FONT>
				</TD>
		</TR>
	</TABLE>
</center>
</cFOUTPUT>
</cFIF>

<cfif GetLocation.countryid IS 13 or GetLocation.countryid IS 1>
	<CFSET personfieldsonpage="Person_Email,Person_FirstName,Person_LastName,Person_Title,Person_Language">
<cfelse><CFSET personfieldsonpage="Person_Email,Person_FirstName,Person_LastName,Person_Salutation,Person_Language">
</cfif>
<CFSET personidsonpage="">

<FORM ACTION="../screen/updatedata.cfm" METHOD="POST" NAME="frmmain">
<CFOUTPUT>
	<CF_INPUT TYPE="Hidden" NAME="LocationID" VALUE="#LocationID#">
	<CF_INPUT TYPE="Hidden" NAME="frmLocationID" VALUE="#LocationID#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmDate" VALUE="#createODBcDateTime(now())#">
	<CFIF isdefined("frmProcessID")>
	<CF_INPUT TYPE="HIDDEN" NAME="frmProcessID" VALUE="#frmProcessID#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmStepID" VALUE="#NextStepID#">
	</cfif>
	<CFIF isdefined("ScreenTitleA")>	<CF_INPUT TYPE="HIDDEN" NAME="ScreenTitleA" VALUE="#Screentitlea#">	</cfif>
	<CFIF isdefined("partnerarea")>	<CF_INPUT TYPE="HIDDEN" NAME="partnerarea" VALUE="#partnerarea#"></cfif>
	<CFIF isdefined("email")><CF_INPUT TYPE="Hidden" NAME="email" VALUE="#email#"></CFIF>


<FONT FAcE="Arial, chicago, Helvetica" SIZE="3"><B>#htmleditformat(Locname)#</b></font>
<center>
<TABLE border="0" cellspacing="0">

	<TR class="label">
		<TD >&nbsp;</TD>
		<TD>Delete</TD>
		<TD><DIV class="required">phr_Salutation*</div></TD>
		<CFIF listfind(regions,35) is not 0>
		<TD><DIV class="required">phr_Initials*</div></TD>
		<CFELSE>
		<TD><DIV class="required">phr_FirstName*</div></TD>
		</cfif>
		<TD><DIV class="required">phr_LastName*</div></TD>
		<TD><DIV class="required">phr_emailAddress*</div></TD>
		<TD>phr_language</TD>	
		<TD >phr_JobFunction</TD>	
		<TD>phr_WhichRecordIsYours</TD>
	</tr>

	<CFIF getpeople.recordcount is not 0>
	<TR><TD >&nbsp;</TD><TD colspan=8>(phr_CheckDeleteToRemoveFromDatabase)</td></TR>
	</cfif>
</CFOUTPUT>

	<CFLOOP query="getpeople"> 
		<CFSET personidsonpage=listappend(personidsonpage,personid)>

		<!--- get list of all job functions, with field showing if this person has this job function. ordered with selected job functions first--->
		<CFQUERY NAME="getalljobfunctionsordered" datasource="#application.siteDataSource#">
			select f.name, f.flagid, f.namephrasetextid,
			case when exists (select * from booleanflagdata as bfd where entityid =  <cf_queryparam value="#personid#" CFSQLTYPE="CF_SQL_INTEGER" >  and bfd.flagid=f.flagid) THEN 1 ELSE 0 END	as selected
			from flag as f, flaggroup as fg
			where f.flaggroupid= fg.flaggroupid
			and fg.flaggrouptextid='JobFunction'
			and f.active = 1
			and fg.active = 1
			and f.flagtextid  in ( <cf_queryparam value="#listofvalidjobfunctions#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )
			order by selected DESC, f.orderingindex
		</CFQUERY>


		<!--- list of job function flags for this person  --->
		<CFQUERY NAME="getentityjobfunctions" datasource="#application.siteDataSource#">
			select f.flagid
			from flag as f, flaggroup as fg, booleanflagdata as bfd
			where f.flaggroupid= fg.flaggroupid
			and fg.flaggrouptextid='JobFunction'
			and bfd.flagid=f.flagid
			and bfd.entityid =  <cf_queryparam value="#personid#" CFSQLTYPE="CF_SQL_INTEGER" > 
			and f.active = 1
			and fg.active = 1
			and f.flagtextid  in ( <cf_queryparam value="#listofvalidjobfunctions#" CFSQLTYPE="CF_SQL_VARCHAR"  list="true"> )
			order by f.orderingindex
		</cfquery>

		<CFQUERY NAME="getDeletionRequestFlag" datasource="#application.siteDataSource#">
			select f.flagid
			from booleanflagdata as bfd, flag as f
			where bfd.flagid = f.flagid
			and f.flagtextid='DeletionRequest'
			and bfd.entityid =  <cf_queryparam value="#personid#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</CFQUERY>

		


		<TR class="screen">
		
		<CFOUTPUT>

		<CF_INPUT TYPE="HIDDEN" NAME="person_lastupdated_#personid#" VALUE="#lastupdated#">			
		<TD  class="screen">&nbsp;</TD>
		

		<TD valign="Top" class="screen">
		<CF_INPUT TYPE="Checkbox" NAME="Checkbox_#getDeletionRequestFlagGroup.flaggroupid#_#Personid#" VALUE="#getDeletionRequestFlagGroup.Flagid#" checked="#iif( getDeletionRequestFlag.recordcount is not 0,true,false)#" >
		<INPUT TYPE="Hidden" NAME="Checkbox_#getDeletionRequestFlagGroup.flaggroupid#_#Personid#_Orig" VALUE="<CFIF getDeletionRequestFlag.recordcount is not 0>#htmleditformat(getDeletionRequestFlagGroup.Flagid)#<CFELSE>0</cfif>">
		</TD>


		<TD valign="Top" class="screen"></cfoutput>
			<cfif GetLocation.countryid IS 13 or GetLocation.countryid IS 1>

				<CFSET validvaluelist=valuelist(getvalidvaluetitle.validvalue)>
				<CFSET formfield = "Person_Title_#Personid#">
				<CFSET currentValue = title>
				<CFSET allowNull = false>
				<CFSET nullText = "&nbsp">
				<CFSET required = 0>
				<CFINCLUDE template= "showValidValueList.cfm">

			<cfelse>

				<cfoutput><CF_INPUT TYPE="Text" NAME="Person_Salutation_#Personid#" SIZE="3" class="screen" MAXLENGTH = "25" VALUE="#trim(salutation)#" ></cfoutput>
				<cfoutput><CF_INPUT TYPE="Hidden" NAME="Person_Salutation_#Personid#_orig" VALUE="#trim(salutation)#"></cfoutput>
			</cfif>


		</TD><cfoutput>
		
		<TD valign="Top" class="screen">
		<CF_INPUT TYPE="Text" NAME="Person_FirstName_#Personid#" SIZE="6" maxlength = "25" class="screen" value="#trim(firstname)#"> 
		<CF_INPUT TYPE="Hidden" NAME="Person_FirstName_#Personid#_orig" VALUE="#trim(firstname)#">
		</TD>
		
		<TD valign="Top" class="screen">
		<CF_INPUT TYPE="Text" NAME="Person_LastName_#Personid#" SIZE="8" maxlength = "25" VALUE="#trim(lastname)#" class="screen">
		<CF_INPUT TYPE="Hidden" NAME="Person_LastName_#Personid#_orig" VALUE="#trim(Lastname)#">
		</TD>
	
		<TD valign="Top" class="screen">
		<CF_INPUT TYPE="Text" NAME="Person_email_#Personid#" VALUE="#trim(email)#" SIZE="10" maxlength = "50" class="screen">
		<CF_INPUT TYPE="Hidden" NAME="Person_email_#Personid#_orig" VALUE="#trim(email)#">
		</TD>
		
		
		<TD class="screen" valign="Top">

				<CFSET validvaluelist=valuelist(GetLangList.Language)>
				<CFSET formfield = "Person_Language_#Personid#">
				<CFSET currentValue = language>
				<CFSET allowNull = false>
				<CFSET required = 0>
				<CFINCLUDE template= "showValidValueList.cfm">

		</TD>


		<TD class="screen">
		
		<SELECT multiple NAME="checkbox_jobfunction_#personid#" size=#max(getentityjobfunctions.recordcount+1,3)#>
		<OPTION VALUE="0" #IIF(getentityjobfunctions.recordcount is 0, DE(" SELECTED"), DE(""))#>phr_NoJobFunction</OPTION>
		</cfoutput>
		<CFOUTPUT QUERY="getalljobfunctionsordered">
		<OPTION VALUE="#Flagid#" #IIF(listfindnocase(valuelist(getentityjobfunctions.flagid),flagid) is not 0, DE(" SELECTED"), DE(""))#>#HTMLEditFormat(evaluate("phr_#htmleditformat(namephrasetextid)#"))#</OPTION>
		</CFOUTPUT>
		</SELECT>
		<cfoutput>
		<INPUT TYPE="Hidden" NAME="checkbox_jobfunction_#Personid#_orig" VALUE="#IIF(getentityjobfunctions.recordcount is not 0, DE("#valuelist(getentityjobfunctions.flagid)#"), DE("0") )#">	
		</cfoutput>
		</TD>


		<cfoutput>
		<TD valign="Top" class="screen">
		<INPUT TYPE="Radio" NAME="frmUser"  value="#Personid#" <CFIF isDefined("request.relayCurrentUser.personid")><CFIF #personid# is #request.relayCurrentUser.personid#>CHECKED</CFIF></CFIF>>
		</TD>
		</cfoutput>

		</TR>

	
	</cfloop>
	
	


	
		<CFSET firstname="">
		<CFSET lastname="">
		<CFSET email="">
		<CFSET title="">
		<CFSET salutation="">
		<CFSET language="">
		<CFSET lastupdated="">
	


	<CFLOOP index="I" from="1" to="5">
		<CFSET personid="new"&I>
		<CFSET personidsonpage=listappend(personidsonpage,personid)>		
		
	
		
		<CFOUTPUT>
		<INPUT TYPE="HIDDEN" NAME="person_lastupdated_#personid#" VALUE="#lastupdated#">			
		<CF_INPUT TYPE="HIDDEN" NAME="person_locationid_#personid#" VALUE="#locationid#">		<!--- this tells the task which location the person is to be added to --->

		<TD  class="screen">&nbsp;</TD>

		<TD class="screen">&nbsp;		<CF_INPUT TYPE="Hidden" NAME="Checkbox_#getDeletionRequestFlagGroup.flaggroupid#_#Personid#_Orig" VALUE="0"></TD>								

		<TD valign="Top" class="screen"></cfoutput>	
			<cfif GetLocation.countryid IS 13 or GetLocation.countryid IS 1>

				<CFSET validvaluelist=valuelist(getvalidvaluetitle.validvalue)>
				<CFSET formfield = "Person_Title_#Personid#">
				<CFSET currentValue = title>
				<CFSET allowNull = true>
				<CFSET required = 0>
				<CFSET nullText = "&nbsp">
				<CFINCLUDE template= "showValidValueList.cfm">

			<cfelse>

				<cfoutput><CF_INPUT TYPE="Text" NAME="Person_Salutation_#Personid#" SIZE="3" MAXLENGTH = "25" VALUE="#trim(salutation)#" class="screen" ></cfoutput>
				<cfoutput><CF_INPUT TYPE="Hidden" NAME="Person_Salutation_#Personid#_orig" VALUE="#trim(salutation)#"></cfoutput>

			</cfif>
		</TD>
		<cfoutput>
		<TD valign="Top" class="screen"><CF_INPUT TYPE="Text" NAME="Person_FirstName_#Personid#" SIZE="6" maxlength = "25" class="screen" > 
		<CF_INPUT TYPE="Hidden" NAME="Person_FirstName_#Personid#_orig" VALUE="">
		</TD>
		<TD valign="Top" class="screen"><CF_INPUT TYPE="Text" NAME="Person_LastName_#Personid#" SIZE="8" maxlength = "25" class="screen" >
		<CF_INPUT TYPE="Hidden" NAME="Person_LastName_#Personid#_orig" VALUE="">
		</TD>
		<TD valign="Top" class="screen"><CF_INPUT TYPE="Text" NAME="Person_email_#Personid#" VALUE="" SIZE="10" maxlength = "50" class="screen" >
		<CF_INPUT TYPE="Hidden" NAME="Person_email_#Personid#_orig" VALUE="">
		</TD>

	
		<TD class="screen" valign="Top">

				<CFSET validvaluelist=valuelist(GetLangList.Language)>
				<CFSET formfield = "Person_Language_#Personid#">
				<CFSET currentValue = GetLanguage.Language>
				<CFSET allowNull = false>
				<CFSET required = 0>
				<CFINCLUDE template= "showValidValueList.cfm">


		</TD>


		<TD class="screen">
		<SELECT multiple NAME="checkbox_jobfunction_#personid#" size=3>

		<OPTION VALUE="0" SELECTED>phr_NoJobFunction</OPTION>
		</cfoutput>
		<CFOUTPUT QUERY="getalljobfunctions">
		<OPTION VALUE="#Flagid#" >#HTMLEditFormat(evaluate("phr_#htmleditformat(namephrasetextid)#"))#</OPTION>
		</CFOUTPUT>
		</SELECT>
		<cfoutput>
		<CF_INPUT TYPE="Hidden" NAME="checkbox_jobfunction_#Personid#_orig" VALUE="0">	
		</cfoutput>
		</TD>


		<TD valign="Top" class="screen">
		<CFOUTPUT>
		<INPUT TYPE="Radio" NAME="frmUser"  value="#Personid#" <CFIF isDefined("request.relayCurrentUser.personid")><CFIF #personid# is #request.relayCurrentUser.personid#>CHECKED</CFIF></CFIF>>
		</cfoutput>
		</TD>

		

	</TR>


	</cfloop>
	

	<TR>
		<TD class="screen">&nbsp;</TD>
		<TD colspan="8" class="screen">* <CFOUTPUT>phr_RequiredFields</cfoutput>
		</TD>
	</TR>
	
	
	<TR><TD>&nbsp;</TD>
		<TD colspan = "8" align="center">&nbsp;<P><CFOUTPUT>
			<A HREF="javascript:verifyForm()">
			<IMG SRC="/images/buttons/c_continue_e.gif" WIDTH=105 HEIGHT=21 ALT="phr_continue" BORDER="0"></A>
		</CFOUTPUT>
		</TD>
	</TR>
</TABLE></center>

<CFOUTPUT>
<INPUT TYPE="Hidden" NAME="frmtablelist" VALUE="person">
<CF_INPUT TYPE="Hidden" NAME="frmpersonfieldlist" VALUE="#personfieldsonpage#">
<CF_INPUT TYPE="Hidden" NAME="frmpersonidlist" VALUE="#personidsonpage#">
<CF_INPUT TYPE="Hidden" NAME="frmpersonflaglist" VALUE="checkbox_jobfunction,Checkbox_#getDeletionRequestFlagGroup.flaggroupid#">
</cfoutput>

</FORM>




</cf_translate>







