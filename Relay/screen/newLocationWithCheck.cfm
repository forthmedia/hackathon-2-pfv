<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
newLocationWithCheck.cfm

Authors WAB and AH

Purpose:

	Page is called when a new location needs to be added 
	User has to enter the name of the new location and the country and can indicate if the new location is part of an existing organisation.
		Allows us to:
			 test for duplicates 
			 get correct country for subsequent screen 
			 check that the countryID is allowed for the current organisation


Parameters

Expects frmEntityType and  frmCurrentEntityID from the viewer

Can be passed frmOrganisationID to indicate the current organisation

--->


<!--- set the textid of screen to go to after all the checking is done--->
<CFIF frmentitytype is "person">
	<CFSET newLocationScreen = "newperson_location">
<CFELSE>
	<CFSET newLocationScreen = "newlocation">  
</CFIF>
	
<cfparam NAME="Message" default="">
<cfparam NAME="frmTask" default="">


	<STYLE>
	.Text  {
                  	font-size : 10 pt;
                  	font-family : Arial, Chicago, Helvetica;
                  	font-style : normal;
                  	color : #000000;
                  }
                  
	</style>
		

<CFIF frmTask is "" >
<!--- first time into this page location_sitename_default will not be set  --->

	<!---Opening Screen--->

	<!--- Get details of current entity
		want to get a countryid and an organisationid
		if frmOrganisationID is defined then use that
	--->

	
		<cFQUERY NAME="getDetails" datasource="#application.sitedatasource#" >
		Select 
			o.organisationid, 
			o.countryid ,
			OrganisationName
		from 
			organisation o
			<CFIF isdefined("frmOrganisationID")>
				where organisationid =  <cf_queryparam value="#frmOrganisationID#" CFSQLTYPE="CF_SQL_INTEGER" > 
			<CFELSEIF frmentitytype is "person" and isNumeric(frmcurrententityid)>
				inner join person  p on p.organisationid=o.organisationid 
				where 
				p.personid =  <cf_queryparam value="#frmcurrententityid#" CFSQLTYPE="CF_SQL_INTEGER" > 
			<CFELSEIF frmentitytype is "location" and isNumeric(frmcurrententityid)>
				inner join Location as l on l.organisationid=o.organisationid  
				where l.locationid =  <cf_queryparam value="#frmcurrententityid#" CFSQLTYPE="CF_SQL_INTEGER" > 
			<CFELSE>
				where 1=0
			</CFIF>
		</CFQUERY>

		<!--- If there is a current record then get it's country, otherwise get the user's country --->
		<CFIF getDetails.RecordCount is NOT 0>
			<CFSET currentCountryID = getDetails.countryid>
		<CFELSE>
			<!--- set to country of the user --->
			<cFQUERY NAME="getUserCountry" datasource="#application.sitedatasource#" >			
			Select Countryid from person,location where person.locationid=location.locationid and personid = #request.relayCurrentUser.personid#
			</CFQUERY>
			<CFSET currentCountryID = getUserCountry.countryid>
		
		</cfif>
		
		
		
		<!---Get authorized countries--->
		<CFINCLUDE TEMPLATE="../templates/qrygetcountries.cfm">

		<!---Get the country names for the combo box display--->
		<CFQUERY NAME="getCountryCombo" datasource="#application.siteDataSource#">
			SELECT CountryDescription, CountryID
			FROM Country
			WHERE CountryID  IN ( <cf_queryparam value="#CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			ORDER BY CountryDescription
		</CFQUERY>
		


	<!---Verify that form fields are complete and submit the form--->	
	<SCRIPT>
	function jsSubmit() {
	
		form = window.document.Form1
		msg = ''
	
		if (form.location_sitename_default.value == '') {
			msg +=  'The Site Name is Required\n'
		}
	
		if (form.location_countryid_default[form.location_countryid_default.selectedIndex].value == '') {
			msg +=  'Please choose a Country\n'
		}	
	
		if (msg != '' ) {
			alert(msg)
		} else {
			form.submit()
		}	
	
	}
	
	</SCRIPT>

<CFOUTPUT>	
	
	<CENTER>

		<CFIF message is not "">
			#application.com.relayUI.message(message=message)#
		</CFIF>

	<form action="newLocationWithCheck.cfm" method="post" name="Form1">
		<CF_INPUT type="hidden" name="frmEntityType" value=#frmEntityType#>
		<CF_INPUT type="hidden" name="frmCurrentEntityId" value=#frmCurrentEntityID#>
		<input type="hidden" name="frmCurrentScreenId" value="">
		<input type="hidden" name="frmNextPage" value="">
		<input type="hidden" name="frmTask" value="check">
		<!--- <input type="hidden" name="frmGlobalParameters" value=#frmglobalparameters#> NJH 2009/06/30 P-FNL069 --->

	<!---The opening screen form--->
	<table class="Body">
		<tr>
			<TD colspan="2" align="center"><B>Add a New Location</b></td>
		</TR>
	
		<tr>
			<TD colspan="2" align="center">Please enter the name and country of the new location <BR>
								The system will check for possible duplicate entries<BR>
								and display the correct address format for the given country<BR>
								Please indicate if the location is part of the current organisation &nbsp</td>
		</TR>
	
		<tr>
			<td>Site Name</td><td ><input type="text" name="location_sitename_default" size="30" maxlength="50" class="body" ></td>
		</tr>
	
		<TR>
			<td>Country</td>
			<td>
			<SELECT name="location_countryid_default" class="body">
			<OPTION value="">Select a Country
			<cfloop query="getCountryCombo">
			<OPTION value="#CountryID#" <CFIF countryid IS currentCountryid>SELECTED</cfif>>#htmleditformat(CountryDescription)#
			</cfloop>
			</select>
			</td>
		</tr>
	
		<!--- If there is a current entity then ask whether this Location is in the same organisation --->
		<CFIF getdetails.recordcount is not 0>
			<tr>
				<td colspan="2"><CF_INPUT type="checkbox" name="location_organisationid_default" value="#getDetails.organisationid#" checked="#iif( isdefined("frmOrganisationID"),true,false)#" > New Location is part of the organisation <b>#htmleditformat(getDetails.OrganisationName)#</b></td>
			</tr>
		</cfif>
	
		<tr>
			<td colspan="2" align="center">
			<A HREF="javascript: jsSubmit()">Continue</A>
			</td>
		</tr>

	</table>


	</form>	

	</center>

	
</cfoutput>

<!---The Second Screen--->
<CFELSEIF frmTask is "check">

	<cfif isDefined("location_organisationid_default")>
		<!--- Check that if the location going into an existing organisation that
			 is in the same country (or countrygroup as the organisation) --->
		<CFQUERY NAME="checkCountries" datasource="#application.sitedatasource#" >
		select 
			* 
		from 
			organisation o
				inner join 
			countryGroup cg on o.countryid = cg.countrygroupid
		where 	
			organisationid =  <cf_queryparam value="#location_organisationid_default#" CFSQLTYPE="CF_SQL_INTEGER" > 
			and countrymemberid =  <cf_queryparam value="#location_countryid_default#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfquery>

		<cfif checkCountries.recordCount is 0>
			<cfset message = "The location you are adding must be in the same country as the parent location<BR>Please try again">
			<cfset frmTask = "">
			<cfinclude	template = "newLocationWithCheck.cfm">	
			<CF_ABORT>		
		</cfif>

	</cfif>


	<!---Check if any Similar Locations are already in the database--->
		<CFQUERY NAME="getDetails" datasource="#application.sitedatasource#" >
		SELECT 
			*, 
			countrydescription as countryname
		FROM 
			organisation o 
				inner join 
			Location l on o.organisationid = l.organisationid
				inner join 
			country c on l.countryid = c.countryid
		WHERE 
			l.Countryid =  <cf_queryparam value="#location_countryid_default#" CFSQLTYPE="CF_SQL_INTEGER" > 
		and (SiteName  LIKE  <cf_queryparam value="#location_sitename_default#%" CFSQLTYPE="CF_SQL_VARCHAR" > 
			or 
			OrganisationName  LIKE  <cf_queryparam value="#location_sitename_default#%" CFSQLTYPE="CF_SQL_VARCHAR" > )
		Order by 
			OrganisationName, o.Organisationid, Sitename
		</cfquery>
	
	<!---If no records proceed to the Add New Record screen--->
		<CFIF #getDetails.RecordCount# EQ 0>
		
	
			<!--- set various variables before including screen.cfm --->
			<CFSET frmcurrententityid="newlocation">
			<CFSET frmcurrentscreenid= #newlocationscreen#>
			<CFSET form.frmreadonlywithhiddenfields="'location_countryid','##addressTable##_countryid'">
	
			<CFIF frmentityType IS "person">
				<CFSET frmPersonID = "newPerson">
				<CFSET frmLocationID = "newLocation">
			</cfif>
	
			<CFINCLUDE template="screen.cfm">
			<CF_ABORT>
	
		</cfif>
	
	
	<!---Otherwise display a list of the locations with the similar names--->	


	<SCRIPT>
		function jsSubmit()  {
		
			window.document.newLocationForm.submit()
		
		}
	
		<CFINCLUDE template="jsViewEntity.cfm">
	</SCRIPT>
	<CENTER>

			
	<CFIF message is not "">
	<CFOUTPUT>#application.com.relayUI.message(message=message)#</cfoutput>
	</CFIF>


	<form action="screen.cfm" method="post" name="newLocationForm">	
		<table class="text" cellspacing="0" cellpadding="2">
			<tr>
				<TD colspan="4" align="center"><B>Add a New Location</b></td>
			</TR>
			<tr>
				<td colspan="4" align="center">Below is a list of possible matches for <CFOUTPUT>'<B>#htmleditformat(location_sitename_default)#</b>'</cfoutput>.
						<BR>Please check that you are not about to add a duplicate entry
						<BR>If the location you are adding is part of one of the organisations listed, then please select the radio button next to its name.
						<BR>Click continue to add a new record, or click on a red button to view an existing record</td>
			</tr>
			
			<tr>
				<td colspan="4" align="center"><cfoutput><a href="javascript:jsSubmit()">Continue</a></cfoutput></td>
			</tr>
		
			<tr>
				<td></td>
				<td colspan="2"><b>Organisation</b></td>
				<td align="center"></td>
			</tr>

			<tr>
				<td></td>
				<td></td>
				<td><b>Site Name</b></td>
				<td align="center"><b>Edit</b></td>
			</tr>
		
			<cfoutput query="getDetails" group="organisationid">
	
				<TR BGCOLOR="##FFFFCE">
					<td><CF_INPUT type="radio" name="location_organisationid_default" value="#organisationid#" checked="#iif( isDefined("location_organisationid_default") and location_organisationid_default is organisationid,true,false)#"   > </td>
					<td colspan="2">
						<B>#htmleditformat(organisationName)#</b>
					</td>
				</tr>
				<cfoutput>
					<tr >
						<td></td>
						<td></td>
						<td width="250">
						<B>#htmleditformat(SiteName)#</B><BR>
						#evaluate(application.addressFormat[countryid])#<BR>
						Telephone: #htmleditformat(Telephone)#<br> 
						Fax: #htmleditformat(Fax)# <BR>
						</td>
						
						<td align="center">
							<table class="text">
								<tr>
									<td align="center">
									<a href="javascript:viewEntity('Location',#locationid#, #locationid#, '')"><IMG SRC="/images/MISC/reddot.gif" WIDTH=19 HEIGHT=19 BORDER=0></a>
									</td>
								</tr>
							</table>
						</td>
					</tr>	
				</cfoutput>
			</cfoutput>
		</table>
		
	</center>

	<CFOUTPUT>

	<input type="hidden" name="frmEntityType" value="location">
	<input type="hidden" name="frmCurrentEntityID" value="newlocation">
	<CFIF frmentityType IS "person">
	<input type="hidden" name="frmPersonID" value="newperson">
	<input type="hidden" name="frmLocationID" value="newlocation">
	</CFIF>
	<CF_INPUT type="hidden" name="frmCurrentScreenID" value="#newlocationscreen#">
	<CF_INPUT type="hidden" name="location_countryid_default" value="#location_countryid_default#">
	<CF_INPUT type="hidden" name="location_sitename_default" value="#location_sitename_default#">
	<!--- <input type="hidden" name="frmglobalparameters" value="#frmglobalparameters#"> NJH 2009/06/30 P-FNL069 --->
	<CF_INPUT type="hidden" name="frmreadonlywithhiddenfields" value="location_countryid,##addressTable##_countryid">
	</FORM>
	</cfoutput>	
	
</cfif>


	
	
	


