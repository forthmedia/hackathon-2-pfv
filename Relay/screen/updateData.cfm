<!--- �Relayware. All Rights Reserved 2014 --->

<!--- Generic template for updating person, location (and with a few mods) organisation records and flags
usually called from showscreen.cfm, or from an updateable report page.

Parameters Expected:
frmTableList - list of tables to be updated eg:  Person,Location
frm#tableName#idList (table names as defined in frmTableList) a list of ids on the page for that table.  Eg frmLocationIdlist=1 frmPersonIDList=1,2,3,999067
frm#tablename#fieldlist  list of updateable fields for the given table
Form Fields of the form #tableName#_#fieldName#_#entityId# and hidden fields of the form #tableName#_#fieldName#_#entityId#_orig giving the original setting of each field
frm#tablename#flaglist  list of flags for each #tablename#

Only updates records if changes have been made

EntityIds which are nonNumeric are assumed to be new records and are added to the database

--->



<!--- mods
140100  WAB  Made test for changes to records, case sensitive 
2001-04-04	WAB		Correction for bug when a field is a radio button and nothing is selected (so variable not defined) - need to param it
28-Jan-2011		MS		LID:5437 using #request.CurrentSite.Title# to display proper title rather then a screen number follwoed by the step number cuz it was using frmprocessid in title
--->


<!--- WAB 2005-01-19 try replacing all of this with cf_updatedata --->
<cf_updatedata_Stub> 



<!--- 
<CFSETTING enablecfoutputonly="yes">
<CFSET updateTime = CreateODBCDateTime(Now())>
<CFPARAM NAME="CUserGroup" DEFAULT="0">
<CFPARAM NAME="Message" DEFAULT="">
<CFSET PartnerRestrictions = False>


<!--- decipher any parameters passed in frmGlobalParameters --->
<CFINCLUDE template="..\screen\setParameters.cfm">




<CFIF isdefined ("frmTableList")>
<!--- only do updates if this variable is defined --->
<CFTRANSACTION>
<!--- loop through tables on page --->
<CFLOOP index="tablename" list="#frmTableList#">
	<!--- WAB added 2001-03-19 --->
	<CFIF evaluate("frm#tableName#FieldList") is not "">
	<!--- get definition of the table --->
		<CFSET tablerequired = tablename>
		<CFINCLUDE template="qryGetTableDefinition.cfm">


	<!--- Loop through ids on page --->
	<CFLOOP index="formentityid" list="#evaluate('frm#tablename#idlist')#">
			<CFSET actualentityid = formentityid>   	<!--- used later - for a new record, formentityid might be 'newperson' , when new record is added, actualentityid is set to the new id --->
		<CFIF formentityid is 0>
			<CFBREAK>
		</cfif>

	<!---  get details of Current Record --->
	<!---  name used for messages, country used to check rights --->
			<CFIF isnumeric(formentityid)>
				<CFQUERY NAME="getcurrentrecord" datasource="#application.siteDataSource#">				
				Select 	#tablename#.lastupdated, organisation.organisationid,
						<CFIF tablename is "Person">
						FirstName +' ' + LastName as name, location.countryid,
						<CFELSEIF tablename is "Location">
						Sitename as name, location.countryid,
						<CFELSEIF tablename is "organisation">
						Organisationname as name, Organisation.countryid,
						</CFIF>
						(select name from usergroup where #tablename#.lastupdatedby = usergroupid) as username
				From 	Organisation
						<CFIF "Person,Location" contains tablename >		
						 inner join Location on Organisation.OrganisationId = Location.OrganisationID
						</CFIF>
						<CFIF tablename is "Person">		
						 inner join Person on Person.LocationId = Location.LocationID
						</CFIF>
				where	#tablename#.#tablename#id=#formentityid#
				</CFQUERY>
				<!--- 
				****  
				Note that there is an occasional bug here.  If the record has been deleted while the form has been open, then this query will 
				not bring back a result and will caused problems later on.  (this is quite possible while doing dedupe work)
				****
				 --->

				<CFSET reqdCountryID = getCurrentRecord.Countryid>  <!--- for rights --->

			
			
			<CFELSE>
				<CFIF tableName IS "Person">
					<CFSET thisLocationID = #evaluate("person_locationid_#formEntityID#")#>
					<CFIF NOT isNumeric("thisLocationID") >
						<!--- must be a person and location being added at same time --->
						<CFSET thislocationid = evaluate(thislocationid)>
					</cfif>
					<CFQUERY NAME="getPersonCountry" datasource="#application.siteDataSource#">				
						select countryid from location where locationid =#thisLocationID#
					</CFQUERY>
						<CFSET reqdCountryID = getPersonCountry.countryid>
				<CFELSEIF tableName IS "Location">
					<CFSET reqdCountryID = evaluate("Location_countryid_#formEntityID#")>
				</cfif>
				
			</CFIF>


			<!---  check for permissions to update or add--->
			<CFIF usertype is "internal">
				<CFIF isnumeric(formentityid)>
					<!---  check for update rights for this country--->
					<CFSET reqdCountryID = getCurrentRecord.Countryid>
					<CFSET partnerCheck =true>
					<CFINCLUDE template = "/templates/qryGetCountries.cfm">
					
					<CFIF findCountries.recordCount IS "0">
						<CFSET #message# = "Sorry, but you do not have permission to edit this record.">
						<CFBREAK>
					<CFELSEIF  findCountries.UpdateOkay IS 0 and findCountries.Level4 IS NOT 1> 
						<CFSET #message# = "Sorry, but you do not have permission to edit this record.">
						<CFBREAK>
					</CFIF>
			
	
					<CFIF findCountries.Level4 IS 1>
						<!--- partner rights, check that this record is from same organisation--->
						<CFQUERY NAME="getPartnerOrganisation" datasource="#application.siteDataSource#">
						select location.Organisationid from person, location where person.locationid = location.locationid and personid = #request.relayCurrentUser.personid#
						</CFQUERY>
	
						<CFIF getPartnerOrganisation.Organisationid IS NOT getCurrentRecord.Organisationid>
							<CFSET #message# = "Sorry, but you do not have permission to edit this record.">
							<CFBREAK>
						</cfif>
	
					</cfif>
					
				<CFELSE>
				
	 				<!---  check for add rights for this country--->
					<CFSET partnerCheck = true>
					<CFINCLUDE template = "/templates/qryGetCountries.cfm">
					
					<CFIF findCountries.recordCount IS "0"><CFOUTPUT>#isnumeric(formentityid)#bb<BR></CFOUTPUT>
						<CFSET message = "Sorry, but you do not have permission to add this record.">
						<CFBREAK>
					<CFELSEIF  findCountries.AddOkay IS 0 and findCountries.Level4 IS NOT 1> 
						<CFSET message = "Sorry, but you do not have permission to edit this record.">
						<CFBREAK>
					</CFIF>
				</CFIF>
			</CFIF>
			
			

		<!--- need to check for any date fields which might be on the page 
				either need the day/month/year concatenating 
				or mm-dd-yyyy input converting
				--->	
		<CFLOOP index="fieldname" list="#evaluate('frm#tablename#fieldlist')#">
			<cfif listFindNoCase(datefields,listgetat(fieldname,2,"_")) IS  not 0>
				<!--- this is a date field 
					check for existence of day, month, year fields
					actually I assume that all are there if the day is there
					then check for format mm-dd-yyyy
				--->
				<cfif isDefined("#fieldname#_#formentityid#Day") and evaluate("#fieldname#_#formentityid#Day") is not "">
					<cfset "#fieldname#_#formentityid#" = CreateDateTime (evaluate("#fieldname#_#formentityid#Year"),evaluate("#fieldname#_#formentityid#Month"),evaluate("#fieldname#_#formentityid#Day"),0,0,0)>
				<cfelseif isDefined("#fieldname#_#formentityid#") and listlen(evaluate("#fieldname#_#formentityid#"),"-") is 3 AND len(trim(evaluate("#fieldname#_#formentityid#"))) LTE 10>
					<cfset fieldValue = evaluate("#fieldname#_#formentityid#")>
					<cfset "#fieldname#_#formentityid#" = CreateDateTime (listgetat(fieldValue,3,"-"),listgetat(fieldValue,2,"-"),listgetat(fieldValue,1,"-"),0,0,0)> 
				</cfif>
			</cfif>
		</CFLOOP>
			
		<!--- Have there been any changes to this entity? --->
		<!--- Loop though fields for current table and id --->
		<CFSET recordchanged=false>
		
		<CFLOOP index="fieldname" list="#evaluate('frm#tablename#fieldlist')#">	<!--- list eg: person_firstname person_lastname person_email  --->	
			<cfif isDefined("#fieldname#_#formentityid#_orig")>	<!--- checks that this entity really is on the page - was having problems when we have person lists on person pages --->
				<CFPARAM name="#fieldname#_#formentityid#" default="">  <!--- need to param the form field in case it is a radio button which isn't set  --->
				<CFIF compare(trim(evaluate("#fieldname#_#formentityid#")), trim(evaluate("#fieldname#_#formentityid#_orig"))) IS NOT 0>
<!--- 				debug: <CFOUTPUT>#fieldname#_#formentityid#, #evaluate("#fieldname#_#formentityid#")# #evaluate("#fieldname#_#formentityid#_orig")#</cfoutput>				    --->
					<CFSET recordchanged=true>
					<CFBREAK>
				</cfif>
			</cfif>
		</cfloop>


	 	<CFIF recordchanged is  true>
			<!--- Have been some changes so need to do update --->

			<!--- check this isn't a new record --->
			<CFIF isnumeric(formentityid)>
				<!--- this is an update --->


				<!--- check that record hasn't been updated in meantime --->
				<CFIF datediff("s", getcurrentrecord.lastupdated, evaluate("#tablename#_lastupdated_#formentityid#")) GE 0>

					<!--- Update Entity table if there any changes --->
					<CFQUERY NAME="update#tablename#" DATASOURCE="#application.SiteDataSource#">
					UPDATE #tablename#
					SET
					<CFLOOP INDEX="FieldName" LIST="#evaluate('frm#tablename#fieldlist')#">
						<!--- bit of a hack here to test for numeric fields and for the two active fields which have the be distinguished between--->
						<cfif isdefined("#FieldName#_#formentityid#")>
							<cfset data = #Evaluate("#FieldName#_#formentityid#")#>
							<cfset field = listgetat(fieldname,2,"_")>
		
						 	<CFIF listFindNoCase("personactive,locationactive",field) IS NOT 0>
								active = #data#,
							<cfelse>
								#field# =
								<CFIF listFindNoCase(numericfields,field) IS 0 and listFindNoCase(datefields,field) IS  0>
									'#data#'
								<CFELSE>
									<cfif data is "">
										<CFIF listFindNoCase(nullablefields,field) IS not 0>
										null
										<cfelse>
										0	
										</CFIF> 
									<cfelse>
									#data# 
									</cfif>
								</cfif> 	
								,
							</cfif> 	
						</CFIF>
					</CFLOOP>
					LastUpdated = #updateTime#,
					LastUpdatedBy = #request.relayCurrentUser.usergroupid#
					WHERE #tablename#ID = #formentityid#
					</CFQUERY>
	
					<CFSET message=message& "#getcurrentrecord.name# record updated <BR>">
					
				<CFELSE>

					<CFSET message=message&  "#getcurrentrecord.name# record has been edited by #getcurrentrecord.username# (at #timeformat(getcurrentrecord.lastupdated,"HH:mm:ss")#) since you opened the record, no update saved<BR>">

				</CFIF>
			
			<cfelse>
				 <!--- this is a new record --->


				<CFINCLUDE template="..\screen\add#tablename#.cfm">					
				
					
				<!--- bit of a problem here - if form is resubmitted, we don't readd the person, but we still set these variables, not sure of knock on effects --->

				<CFSET "#actualentityid#" = newentityid>   <!--- eg newlocation = 27123 --->
				<CFSET actualentityid = newentityid>
								
				<CFPARAM name="#tablename#idsadded" default="">
				<CFSET "#tablename#idsadded" = listappend(evaluate("#tablename#idsadded"), newentityid)>
				
				<!--- this bit should set frmLocationID and frmPersonID which some templates use --->
				<CFSET "frm#tablename#id" = newentityid>				
				

				<!--- this complicated bit should help when a person is being added --->
				<!--- if there is a variable frmCurrentEntityID, which itself is the name of a variable (such as newPerson) then if newPerson is defined, evaluate it --->
				<!--- eg. (ie frmcurrenentityid = 'newpers' and newpers=125893)  --->

				<!--- this complicated bit should help when a person is being added --->
			
				<CFIF isDefined("frmcurrentEntityID") and not isNumeric(frmcurrentEntityID) and frmcurrentEntityID is not "" and isDefined(frmcurrentEntityID)>	
					<CFSET frmcurrentEntityID = evaluate(frmcurrentEntityID)>  <!--- actually set the current entity --->			
				</CFIF>

<!--- 			
				<CFIF  isDefined("frmcurrentEntityID")>	<!--- check that this variable exists --->
					<CFIF  not isNumeric(frmcurrentEntityID) and frmcurrentEntityID is not "">  <!--- mustn't be numeric if we are going to do the isdefined test below, should not be blank, but there have been some cases when it seems to have been --->
						<CFIF  isDefined(frmcurrentEntityID)> <!--- check whether this variable points to another variable (ie frmcurrenentityid = 'newpers' and newpers=125893) --->
							<CFSET frmcurrentEntityID = evaluate(frmcurrentEntityID)>  <!--- actually set the current entity --->
						</CFIF>
					</cfif>
				</CFIF>
 --->						
			


			<!--- end of adding new record --->
			</CFIF>

		<!--- end of recordchange=true --->
		</CFIF> 



	</CFLOOP>

	</CFIF>
</cfloop>
		

		
		
<!--- 	********************************************* 
	
		Start of processing flags

		********************************************* --->

<CFLOOP index="tablename" list="#frmTableList#">


	<!--- Loop through ids on page --->
	<CFLOOP index="formentityid" list="#evaluate('frm#tablename#idlist')#">
			<CFSET actualentityid = formentityid>   	<!--- used later - for a new record, formentityid might be 'newperson' , when new record is added, actualentityid is set to the new id --->
			<CFIF not isNumeric(actualentityid)>
				<CFIF not isDefined(actualentityid)>
					<CFBREAK>
				</cfif>
				<CFSET actualentityid = evaluate(actualentityid)>
			</cfif>
		
<!--- <CFOUTPUT>debug: #evaluate('frm#tablename#flaglist')#	 </CFOUTPUT>   --->
		
		<!--- loop through flags on page for this entity--->
		<CFLOOP index="flag" list="#evaluate('frm#tablename#flaglist')#">
	
			<!--- check that this isn't from a new record where the base record hasn't been added --->
			<!--- if base record has been added then acutal entity id will have been set --->
			<CFIF not isnumeric(actualentityid)>
					<CFBREAK>
					<!--- don't go through flags if this is an new item line and a new item has not been added --->	
			</CFIF>	
		

			<CFIF listfirst(flag,"_") is NOT "Flag">
				<!--- regular flags --->
	
				<cfif listfirst(flag,"_") is "Date">
					<!--- this is a date field 
						check for existence of day, month, year fields
						actually I assume that all are there if the day is there
						then check for format mm-dd-yyyy
					--->
					<cfif isDefined("#flag#_#formentityid#Day") and evaluate("#flag#_#formentityid#Day") is not "">
						<cfset "#flag#_#formentityid#" = CreateDateTime (evaluate("#flag#_#formentityid#Year"),evaluate("#flag#_#formentityid#Month"),evaluate("#flag#_#formentityid#Day"),0,0,0)>
					<cfelseif isDefined("#flag#_#formentityid#") and listlen(evaluate("#flag#_#formentityid#"),"-") is 3 AND len(trim(evaluate("#flag#_#formentityid#"))) LTE 10>
						<cfset fieldValue = evaluate("#flag#_#formentityid#")>
						<cfset "#flag#_#formentityid#" = CreateDateTime (listgetat(fieldValue,3,"-"),listgetat(fieldValue,2,"-"),listgetat(fieldValue,1,"-"),0,0,0)> 
					</cfif>
				</cfif>


				<CFPARAM name="#flag#_#formentityid#" default="">  <!--- if nothing selected then the field will not be defined --->		
				<!--- <cfoutput>DEBUG: #flag#_#formentityid# ; #evaluate("#flag#_#formentityid#")# ; #evaluate("#flag#_#formentityid#_orig")#</cfoutput>			   --->
				<CFIF isDefined("#flag#_#formentityid#_orig")>
					<CFIF compare(evaluate("#flag#_#formentityid#"), evaluate("#flag#_#formentityid#_orig")) IS NOT 0>
						<!--- need to update this flag--->
						<CFSET frmTask = "update">

							<CFSET frmentityid=actualentityid>

						<CFIF isnumeric(formentityid)>
							<CFSET flaglist=flag>
						<CFELSE> 
							 <!--- ie this is a newly added record --->
							<CFSET flaglist=replace(flag,formentityid,actualentityid,"All")>
							<CFSET "#flag#_#actualEntityID#" = evaluate("#flag#_#formentityid#") >
							<CFSET "#flag#_#actualEntityID#_Orig" = evaluate("#flag#_#formentityid#_Orig")>
						</CFIF>


						<CFINCLUDE template="..\flags\flagtask.cfm">

					</cfif>

				</cfif>

			<CFELSE>		<!--- complex flag --->

				<!--- ************************************** 
						complex flags
					 ************************************** --->
		
					<CFINCLUDE template="updateData_complexFlags.cfm">
			
			</CFIF> 
		</cfloop>


 	</cfloop>
</cfloop>
	
</CFTRANSACTION>
</CFIF>


<!--- this bit is used by william's dynamic reporting thingy  (not live at 1999-11-29) --->
<CFIF isdefined("frmSelectionsOnPage")>
	<CFLOOP index="thisSelection" list="#frmSelectionsOnPage#">

		<CFQUERY NAME="saveTagStatusNo" datasource="#application.siteDataSource#">
			UPDATE SelectionTag
			   SET Status = 0,
				   LastUpdated = #updatetime#,
				   LastUpdatedBY = #request.relayCurrentUser.usergroupid#
		   	 WHERE SelectionID = #thisSelection#
			   AND EntityID IN (#frmPersonIDList#)
			   <CFIF IsDefined("selection_#thisSelection#")> <!--- could be empty --->
			   AND EntityID NOT IN (#evaluate("selection_#thisSelection#")#)
			   </cFIF> 			   
		</CFQUERY>		  
		
		<CFIF IsDefined("selection_#thisSelection#")>

			<CFQUERY NAME="saveTagStatusYes" datasource="#application.siteDataSource#">
				UPDATE SelectionTag
				   SET Status = 1,
					   LastUpdated = #updateTime#,
					   LastUpdatedBY = #request.relayCurrentUser.usergroupid#
			   	 WHERE SelectionID = #thisSelection#
				  AND EntityID IN (#frmPersonIDList#)
				  AND EntityID IN (#evaluate("selection_#thisSelection#")#)
			</CFQUERY>		  
	   </CFIF>




	</cfloop>
</cfif>



<!--- special hack to set the user cookie when someone is adding themselves to the database --->
<!--- parameter frmsetusercookie isn't passed onto the next screen --->


<CFIF isdefined("frmsetusercookie1")>
	<CFSET frmPersonID = evaluate("#frmsetusercookie1#")>
	<CFSET part1 = frmPersonID>
	<CFSET part2 = request.relayCurrentUser.usergroupid>
	<CFSET part3 = request.relaycurrentuser.usergroups>

	<cfset application.com.globalFunctions.cfcookie(name="user" value="#part1#-#part2#-#part3#")>
		<!--- make log entry, trim HTTP_USER_AGENT to preven this insert from ever failing --->
		<CFQUERY NAME="insUsage" DATASOURCE="#application.SiteDataSource#">
			INSERT INTO Usage (LoginDate, PersonID, Browser, RemoteIP, App)
			VALUES (#Now()#, #request.relayCurrentUser.personid#,'#Left(CGI.HTTP_USER_AGENT,255)#', '#REMOTE_ADDR#', '<CFIF isdefined("cookie.appname")>#cookie.appname#</cfif>')
		</CFQUERY>	

</cfif>

<CFSETTING enablecfoutputonly="no"> 

<CFIF isdefined("frmscreenpostprocessid")>
	<CFIF isdefined("frmprocessid")><cfset rememberFrmProcessID = frmProcessID><cfelse><cfset rememberFrmProcessID = ""></cfif>
		<cfloop list="#frmscreenpostprocessid#" index="thisProcess">
		<cfset frmProcessid = thisprocess>
			<CFINCLUDE TEMPLATE="redirector.cfm">
		</cfloop>
	<cfset frmprocessid = rememberFrmProcessID>
</cfif>
 
--->


 
<cfset form.message = message>  <!--- hopefully allow it down into customtags --->

<cfif isdefined("frmprocessid") and frmprocessid is not "">

	<CFINCLUDE TEMPLATE="redirector.cfm">

	<CF_ABORT>
<cfelseif isdefined("frmnextpage") and frmnextpage is NOT "">
	<!--- 2014/02/13	YMA	Case 438869 Updatedata not working since change in getRelayScreen.cfm.  Updated to compensate.  --->
	<cfif fileExists(expandpath(frmnextpage))>
		<cfinclude template="#frmnextpage#">
		<CF_ABORT>
	<cfelse>
		<!--- if the frmNextPage variable is not part of the domain, then don't re-direct... picked up on acunetix scan as a problem with URL redirection --->
		<cfif left(frmNextPage,len(request.currentSite.protocolAndDomain)) eq request.currentSite.protocolAndDomain or findnocase('http',frmnextpage) is 0>
			<cflocation URL="#frmnextpage#" ADDTOKEN="No">
			<CF_ABORT>
		</cfif>
	</cfif>
</cfif>	
 			
