<!--- �Relayware. All Rights Reserved 2014 --->
<!--- user rights to access:
		SecurityTask:dataTask
		Permission: Update
---->

<CFIF NOT checkPermission.UpdateOkay GT 0>
	<!--- if frmLocationID is 0 then task is to update
	      but the user doesn't have update permissions --->

	<CFSET message = "You are not authorised to use this function">
	<CFINCLUDE TEMPLATE="datamenu.cfm">
	<CF_ABORT>

</CFIF>

	<CFQUERY NAME="getNewLoc" datasource="#application.siteDataSource#">
	SELECT 	l.sitename
	FROM 	Location AS l
	WHERE 	l.LocationID =  <cf_queryparam value="#frmMoveToLocationID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</CFQUERY>

	<CFSET message = "">

<CFLOOP INDEX="thisPersonID" LIST=#frmMovePersonID# >
		<!--- Check that record has not changed --->
		<CFQUERY NAME="getLocOrgID" datasource="#application.siteDataSource#">
		SELECT p.LastUpdated, l.OrganisationID, p.LocationID AS OldLocID,
				p.firstname, p.lastname
		  FROM Person AS p, Location AS l
		 WHERE p.PersonID =  <cf_queryparam value="#thisPersonID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		 AND 	p.locationid = l.locationid
		</CFQUERY>

		<CFIF getLocOrgID.RecordCount IS 1>
			<CFIF getLocOrgID.LastUpdated LT frmMoveDate>
				<CFTRANSACTION>
 					<!--- Update the LastUpdated(by) fields and Loc/org ID for the person --->
					<CFQUERY NAME="UpdatePerson" datasource="#application.siteDataSource#">
					UPDATE Person
					SET LocationID =  <cf_queryparam value="#frmMoveToLocationID#" CFSQLTYPE="CF_SQL_INTEGER" > ,
						OrganisationID =  <cf_queryparam value="#getLocOrgID.OrganisationID#" CFSQLTYPE="CF_SQL_INTEGER" > ,
						LastUpdatedBy = #request.relayCurrentUser.usergroupid#,
						LastUpdated =  <cf_queryparam value="#Now()#" CFSQLTYPE="CF_SQL_TIMESTAMP" >  
					 WHERE PersonID =  <cf_queryparam value="#thisPersonID#" CFSQLTYPE="CF_SQL_INTEGER" > 
					</CFQUERY>

					<CFSET message = message & "#getLocOrgID.firstName# #getLocOrgID.lastName# moved to #getnewLoc.sitename#">

				</CFTRANSACTION>

			<CFELSE>

					<CFSET message = message & "#getLocOrgID.firstName# #getLocOrgID.lastName# could not be moved (edited by someone else)">			

			</CFIF>

	
		</CFIF>
</CFLOOP>

<CFOUTPUT>#message#</cfoutput>

<SCRIPT>
parent.menuWinForm.message.value = <CFOUTPUT>"#jsStringFormat(message)#"</cfoutput> ;
</script>
<CFSET frmEntityId = frmMovePersonID>
<CFSET altermode = "addAndselect">
<CFINCLUDE template = "viewerAlterEntities.cfm">


<CF_ABORT>
