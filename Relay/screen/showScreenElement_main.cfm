<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Mods:
030200  WAB Added check for when adding a new record, the _orig form field is set to blank to make sure that the record is updated
2004-10-13

2006-02-06   WAB  removed references to validvalue code in this directrory and replaced with calls to customtags
2007-05-04    WAB made sure that validvalues were passed the correct countryid and entityid
2009/09/16   WAB added thislineformfieldObject variable for use in jsverify functions
2009/09/24		WAB CR-TND561 Req8 major changes to cfinput, see notes
2009/04/21    WAB BUG 2129 remove htmleditformat when passing in to cfinput
2009/06/03		WAB/NYB LID2300 required not being passed to displayValidValues
2011/05/25 	NYB		REL106 added support for non PLO entities - to make screen code more generic
2011/09/27		WAB	P-TND109 changed View mode to use disabled controls
2013-07-09 	NYB 	Case 
2016-02-03	WAB	Make sure that required attribute is passed to cf_input
2016-03-09 	WAB	 PROD2016-684 Added support for divLayout 
 --->


<CFSET thislinedata = evaluate("#thisFieldSource#.#thisFieldtextID#") >
<CFSET thislineorigdata = thislinedata >
<cfif thisLineData is not "" >
	<cfset thislinedata = evaluate("#thisevalstring#") > <!--- note that thislinedata has been set above, so the evalstring  can reference this variable--->
</cfif>

<!--- START: 2013-07-09 NYB Case  added --->
<cfif thisFieldtextID eq "countryid" and isdefined("frmCountryID")>
	<CFSET thislinedata = frmCountryID>
</cfif>
<!--- END: 2013-07-09 NYB Case  --->

<!--- 2011/05/25 NYB REL106 changed to be based on actual PK, not guesstimated PK --->
<cfset thislineentityid = evaluate("frm#application.entityType[application.entityTypeID[thisFieldSource]].UniqueKey#")>

<cfif not isnumeric(thislineentityid)>
	<cfset newentity = true>
<cfelse>
	<cfset newentity = false>
</cfif>
<cfset thislineformfield = lcase(thisfieldsource) & "_" & fieldtextid & "_" & thislineentityid >
<cfset thislineformfieldObject  =  thisFormName & "." & thislineformfield>   <!--- WAB 2009/09/16 --->
<cfif not request.relayFormDisplayStyle neq "HTML-div"><cfoutput><div <cfif divLayout is "horizontal">class="col-xs-12 col-sm-9"</cfif>></cfoutput></cfif>
<cfif listfindnocase(evaluate("#thisFieldSource#ForbiddenFields"), '#fieldtextid#') is 0>
	<cfif listfindnocase(evaluate("#thisFieldSource#IncludeFields"), fieldtextid) is not 0>
		<!--- these dealt with using includes --->
		<cfinclude template="#thisFieldSource#_#FieldTextID#.cfm">
	<cfelse>
		<cfif listfindnocase(variables["#thisFieldSource#FieldList"], '#thisfieldsource#_#fieldtextid#') is not 0 >
			<cfset thisLineMethod = "view">		
		</cfif>

		<cfif thislinemethod is "View"  or thislinemethod is "ViewwithHidden" >
			<cfset disabled = true>
			<cfset disabledAttribute = "disabled">
		<cfelse>
			<cfset disabledAttribute = "">
			<cfset disabled = false>
		</cfif>    

		<cfif usevalidvalues is  1 or lookup is 1>
				<cf_getValidValues validfieldname = "#thisFieldSource#_#fieldtextid#" shownull=false countryid = #variables.countryid# entityid = #thisLineEntityID#>
				<cfset "#thisfieldsource#_#fieldtextid#_validvalues" = validvalues>	
		</cfif>

			<cfif not isdefined("#thisFieldSource#_#fieldtextid#_validvalues")>
				<cfif displayas is "memo">
						<cfparam name="cols" default="40">							
						<cfparam name="rows" default="3">
		 				<cfoutput><textarea class="form-control" name="#thislineformfield#" cols="#cols#" rows="#rows#" wrap="VIRTUAL" #disabledAttribute#>#HTMLEditFormat(trim(thislinedata))#</textarea></cfoutput>
				<cfelseif displayas is "DateDropDowns">
					<cfif isDate(thislinedata)>
						<cfset thislinedata = createodbcdatetime(thislinedata)><!--- I do this so that the value stored in the _orig field is a timestamp - this means that I can easily to a comparison in update data --->
					</cfif>
					
					<cfif not disabled>
						<CF_relayDateDropDowns
							currentValue="#thisLineData#"
							fieldName="#thislineformfield#"
						>
	
						<!--- even if not a required field, need to be sure that either all or none of the drop downs are selected --->
						<CFIF Required IS 0>
							<CFSET javascriptVerification = javascriptVerification & "if (numberofDateDropDownsSelected('#thisFormName#','#thislineformfield#') != 0 && numberofDateDropDownsSelected('#thisFormName#','#thislineformfield#') != 3) { msg = msg + '  enter a complete date for #requiredLabel#\n'};">
						</CFIF>
					<cfelse>
						<cfoutput>#thisLineData#</cfoutput>	<!--- TBD relayDateDropDowns disabled --->
					</cfif>


				<cfelseif displayas is "calendar">
					<cfif isDate(thislinedata)>
						<cfset thislinedata = createodbcdatetime(thislinedata)><!--- I do this so that the value stored in the _orig field is a timestamp - this means that I can easily to a comparison in update data --->
					</cfif>
					
					<CF_relayDateField	
							currentValue="#thisLineData#"
							thisFormName="#thisFormName#"
							fieldName="#thislineformfield#"
							anchorName="anchor1X"
							helpText=""
							disabled = "#disabled#"	
						>

					
				<cfelse>
					<!--- 2015/01/28	YMA Case 442683 apply phone mask throughout table fields accross portal --->
					<!--- WAB 2008/09/24 modified so that all attributes passed in a collection
					start using default settings from application.fieldInfo Structure 
					and invented idea of having a substructure fieldAttributes in the speciaFormattingCollection --->
					
					<cfparam name="specialformattingCollection.fieldAttributes" default=#structNew()#>
					<cfset cfinputAttributes = specialformattingCollection.fieldAttributes>
					
					<!--- 2015/01/28	YMA Case 442683 apply phone mask throughout table fields accross portal --->
					<!--- WAB 2007-10-17 added phone masking for Trend NABU - rather hard coded at the moment --->
					<cfif not structKeyExists(cfinputAttributes,"mask") and (thisFieldtextID contains "phone" or thisFieldtextID  is "Fax") and thisFieldtextID does not contain "ext" and thisFieldtextID does not contain "format">
							<cfset cfinputAttributes.mask = application.com.commonQueries.getCountry(countryid=variables.countryid).telephoneformat>
					</cfif>
					<cfif structKeyExists(cfinputAttributes,"mask") and cfinputAttributes.mask is not "">
						<cfoutput>
						<script>
							jQuery(document).ready(function(){
								checkForMaskAttributeOnElementArray($("#thislineformfield#").up("form").getElements())
							});
						</script>
						</cfoutput>
					</cfif>
					
					<cfset cfinputAttributes.type = "TEXT">
					<cfset fieldInfoStruct = application.com.relayEntity.getTableFieldStructure(tablename=thisFieldSource,fieldname=thisFieldtextID)>
					<cfif fieldInfoStruct.isOK >
						<cfset structAppend(cfinputAttributes,fieldInfoStruct.cfformParameters,false)>
					</cfif>

					<cfset cfinputAttributes.name= thislineformfield>
					<!--- CASE 439002 WAB 2014-03-25 when disabled (and the name attribute is deleted) still want an id --->
					<cfset cfinputAttributes.id = cfinputAttributes.name>
					<cfset cfinputAttributes.required = required>

					<!--- 	
						WAB 2009/04/21 don't need to do the htmleditformat - this is done by cfinput
							noticed during bug 2129
						<cfset cfinputAttributes.value="#HTMLEditFormat(trim(thislinedata))#" > 
					--->
	
					<cfset cfinputAttributes.value="#trim(thislinedata)#" >
					<!--- NJH 2014/08/18 show as date/time --->
					<cfif fieldInfoStruct.isOK and fieldInfoStruct.data_type eq "datetime" and cfinputAttributes.value neq "" and isvalid("date",cfinputAttributes.value)>
						<cfset cfinputAttributes.value=application.com.dateFunctions.dateTimeFormat(date=cfinputAttributes.value,showIcon=false)>
					</cfif>
					
					<cfset cfinputAttributes.size="#iif(size is "",25,size)#" >
					<cfset cfinputAttributes.maxlength="#iif(maxlength is "",200,maxlength)#">
					<cfset cfinputAttributes.class="form-control">
			 		
					<cfif structKeyExists(cfinputAttributes,"onValidate") and cfinputAttributes.onValidate is "validateEMail">
						<cf_includejavascriptonce template = "/javascript/verifyemail.js">  <!--- WAB 2008/09/24  --->
					</cfif>
					
					<cfset cfinputAttributes = application.com.relayTranslations.translateStructure(structure = cfinputAttributes)>

					<cfif disabled>
						<cfset structDelete (cfinputAttributes,"onvalidate")>
						<cfset structDelete (cfinputAttributes,"validate")>
						<cfset structDelete (cfinputAttributes,"required")>
						<cfset cfinputAttributes.disabled = true>
						<cfset cfinputAttributes.name = "">
						<!--- CASE 439002 WAB 2014-03-25 when disabled still want the id, so don't delete --->
					</cfif>
					<CF_input attributecollection = #cfinputAttributes#>
				</cfif>
				
				<cfif listfindnocase("edit,viewWithHidden",thisLineMethod)>
					<cfoutput> <input type="Hidden" name="#thislineformfield#_orig" value="<CFIF not NewEntity>#HTMLEditFormat(trim(thisLineData))#</cfif>" ></cfoutput>
				</cfif>
		
				<cfif thislinemethod is "ViewWithHidden" >    
					<cfoutput><CF_INPUT type="Hidden" name="#thislineformfield#" value="#trim(thislinedata)#"></cfoutput>				  
				</cfif>

				<cfif listfindnocase("edit",thisLineMethod)>
					<cfif required is not 0>
						<cfif displayas is "DateDropDowns">
							<CFSET javascriptVerification = javascriptVerification & "if (numberofDateDropDownsSelected('#thisFormName#','#thislineformfield#') != 3) { msg = msg + '  phr_screen_enterAValueFor #requiredLabel#\n'};">
						<cfelse>
							<CFSET javascriptVerification = javascriptVerification & "msg = msg + verifyObjectv2(form.#thislineformfield#,'  phr_screen_enterAValueFor #requiredLabel#','#required#');" >
						</cfif>
					</cfif>
					
				</cfif>

			<cfelse>
				<cf_displayvalidvalues 
					formfieldname = #thislineformfield#
					validvalues = #evaluate('#thisfieldsource#_#fieldtextid#_validvalues')#					
					currentvalue = #thislinedata#
					parameters = #specialformatting#
					countryid = #variables.countryid# 
					entityid = #thisLineEntityID#
					required = #required#    <!--- WAB 2009/06/03 LID2300 added required stuff--->
					requiredLabel = #requiredLabel#    
					requiredMessage = #requiredMessage#    
					attributeCollection = #specialformattingCollection#
					disabled = #disabled#
					class="form-control"
					>
					<!--- note that this tag updates the javascriptverification variable --->
			</cfif>
	
			<cfif listfindnocase("edit,viewWithHidden",thisLineMethod)>
				<cfset variables["#thisFieldSource#FieldList"] = listappend(variables["#thisFieldSource#FieldList"],"#thisFieldSource#_#FieldTextID#")>
			</cfif>
			

			<cfif trim(jsverify) is not "" and listfindnocase("edit",thisLineMethod)>
				<cfset javascriptverification = javascriptverification & "msg = msg + #evaluate("#jsverify#")#;" >
			</cfif>							
			

	</cfif> <!--- end of cfif field dealt with by own template --->
<cfelse>
	<cfoutput><font color="##FF0000">'#htmleditformat(FieldTextID)#' cannot be displayed in this way.</font></cfoutput>
</cfif>  <!--- end of cfif forbidden fields --->
<cfif not request.relayFormDisplayStyle neq "HTML-div"><cfoutput></div></cfoutput></cfif>
