<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Selections containing a particular person --->

<CFIF frmentitytype is "person">
	<CFSET personid = frmcurrententityid>
<CFELSE>	 
	
	<CFSET message = "No Person ID set.">
	<cfoutput>#application.com.relayUI.message(message=message,type="warning")#</cfoutput>
	<!--- <cFINcLUDE TEMPLATE="../templates/message.cfm"> --->
	<cFINcLUDE template="errorform.cfm">
	<CF_ABORT>

</CFIF>	


<CFLOOP index="personid" list="#frmpersonid#">

<CFQUERY name="Selections" dataSource="#application.SiteDataSource#">
SELECT 	s.Title, 
		s.selectionid, 
		s.created,
		u.name
FROM 	Selection as s, SelectionTag as st, usergroup as u
WHERE 	s.selectionid=st.selectionid
		AND s.createdby=u.usergroupid
		AND st.entityid =  <cf_queryparam value="#personid#" CFSQLTYPE="CF_SQL_INTEGER" > 
ORDER BY s.selectionid DESC
</CFQUERY>

<CFQUERY name="getperson" dataSource="#application.SiteDataSource#">
Select 	person.FirstName as firstname,
		person.lastName as lastname
From 	Person
Where	Personid =  <cf_queryparam value="#personid#" CFSQLTYPE="CF_SQL_INTEGER" > 
</cfquery>

<CFOUTPUT>
Selections containing<BR>
 #htmleditformat(getperson.Firstname)# #htmleditformat(getperson.lastname)#
</cfoutput>


<TABLE border=0 cellpadding=3 cellspacing=0>

<TR bgcolor="cccccc">
<TD>Date</TD>
	<TD>Title</TD>
	<TD>Owner</TD>
</TR>

<CFOUTPUT query="Selections">
<TR bgcolor="#IIf(CurrentRow Mod 2, DE('ffffff'), DE('ffffcf'))#">

    <TD>#DateFormat(created,"DD/MM/YY")#</TD>
	<TD>#htmleditformat(title)# (#htmleditformat(selectionid)#)</TD>
	<TD>#htmleditformat(name)#</TD>
	
	
</TR>

</cfoutput>
</TABLE>

<p>
</CFLOOP>