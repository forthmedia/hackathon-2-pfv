<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
2006-11-27		WAB 	errored if blank email passed in.  Now redisplays opening page.  Also cf_translated
2008-01-30		PKP		Rework page to new Design.
2010/06/29		NJH		P-FNL069 - used relayHeaderFooter.. cleaned up page a bit
2010/10/31		NYB		P-FNL069 - stripped back to necessary code and inserted in defaultLOGINV3.cfm
 --->

	<!--- PKP: 2008-01-30: if the password generationmethod exists and is user include RelayTagTemplates/resendpassword.cfm template page else continue with existimgpage : and structkeyexists(URL,"spl") and url.spl is not ""--->
	<cfif application.com.settings.getSetting("security.passwordGenerationMethod") eq "user">
		<cfset tdClass="">
		<cfinclude template="RelayTagTemplates/ResendPassword.cfm">
	<cfelse>
				<!--- If there is no partneremail data display a form to prompt the user for it --->
				<CFIF not IsDefined("userEmail") or trim(userEmail) is "">	
							<table cellspacing="0" cellpadding="0" border="0" class="loginFormElements" align="center">
								<FORM NAME="GetEmail" METHOD="post">
									<TR>
										<td class="label">Phr_ResendPW_EmailAddress&nbsp;&nbsp;</td>
										<td class="value"><INPUT TYPE="Text" NAME="userEmail" size="50"></td>
									</TR>
									<TR>
										<td class="label">&nbsp;</td>
										<TD class="value"><A HREF="javascript:document.GetEmail.submit()">Phr_Continue</A></TD>
									</TR>
								</FORM>
							</TABLE>
				<CFELSE>
							<!--- Otherwise if useremail is present process it --->
							<!--- Verify Email Address --->
							<CFQUERY NAME="CheckEmail" DATASOURCE="#application.sitedatasource#">
								SELECT P.personid, p.firstname, p.lastname, l.sitename, p.username, p.password
								FROM person p
								inner join  UserGroup ug on UG.personid = P.personid
								inner join location l on l.locationid = p.locationid
								WHERE P.email =  <cf_queryparam value="#userEmail#" CFSQLTYPE="CF_SQL_VARCHAR" > 
								  AND p.Active <> 0 <!--- true --->
								  AND p.Password <> ''
								  AND p.LoginExpires >= #CreateODBCDateTime(Now())#
							</CFQUERY>
							<CFIF CheckEmail.recordcount EQ 1>
										<!--- Process Resend PW --->
										<cfparam name="frmUserName" default="">
								 		<cf_createUserNameAndPassword
											personid = "#CheckEmail.personID#"
											resolveDuplicates = false
											OverWritePassword = false
											OverWriteUserName = false
											UserName = "#frmUserName#"
										>
								
										<CFIF not UserNameAndPassword.OK >
											<!---If user name is not Unique --->
											phr_ResendPW_UserNotUnique
										<CFELSE>
											<cfset username = UserNameAndPassword.username>
											<cfset password = UserNameAndPassword.password>
											<cfset email = UserNameAndPassword.email>
											
											<cfset defexists = application.com.email.doesEmailDefExist(emailtextid="SendInternalUserPassword")>
											
											<cfif defexists>
												<cfset application.com.email.sendEmail(emailtextid="SendInternalUserPassword",personid=CheckEmail.personID)>
											<cfelse>
											
												<CF_MAIL TO="#Email#"
											  			FROM="#application.supportEmailName# <#application.supportEmailAddress#>" 
														BCC="#application.cBccEmail#" 
														SUBJECT="#request.CurrentSite.Title# Username and Password" 
														type="html">
													
													<P><FONT face=Verdana size=2>Dear #CheckEmail.firstname#, </FONT></P>
													<P><FONT face=Verdana size=2>This email is being sent to you because you requested to resend password for the #request.CurrentSite.Title# 
													system. </FONT></P>
													<P><FONT face=Verdana size=2>Your User name is: #CheckEmail.username# </FONT></P>
													<P><FONT face=Verdana size=2>Your Password is: #CheckEmail.password# </FONT></P>
													
													<P><FONT face=Verdana size=2><STRONG>#request.CurrentSite.Title# Background Information</STRONG></FONT></P>
													<P><FONT face=Verdana size=2>The URL address to access the <STRONG>internal</STRONG> applications: <A href="#getReciprocalInternalProtocolAndDomain()#">#getReciprocalInternalDomain()#</A></FONT></P>
													<P><FONT face=Verdana size=2>
													Please keep your password for this system secure and do not share your password with others. </FONT></P>
													<P><FONT face=Verdana size=2>         
													For further assistance a Helpdesk is manned from 9.00 a.m. until 5.30 p.m. (GMT) every weekday of the year, excluding UK public holidays.</FONT></P>
													<P><FONT face=Verdana size=2>To contact the Helpdesk: </FONT></P>
													<P><FONT face=Verdana size=2>Telephone number: +44 (0)870 24 11 412 </FONT></P>
													<P><FONT face=Verdana size=2>E-mail the Helpdesk at: #HelpAlertMailbox#</FONT></P>
												</CF_MAIL>
											</cfif>
											<cf_translate phrases = "phr_ResendPW_YouHaveBeenSentYourPassword" />
											<cfset message = phr_ResendPW_YouHaveBeenSentYourPassword>
										</CFIF>
										<cflocation url="/index.cfm?message=#message#" addtoken="No">
							<CFELSE>	
									<table cellspacing="0" cellpadding="0" border="0" class="loginFormElements" align="center">
										<TR>
											<td>phr_ResendPW_BadEmailNotInDBInternal</td>
										</TR>
										<TR>
											<td><CFOUTPUT><a href="mailto:#application.supportemailaddress#">phr_ResendPW_ContactAdmin</CFOUTPUT></a></td>
										</TR>
									</TABLE>
							</CFIF>
				</CFIF>						
	</cfif>

