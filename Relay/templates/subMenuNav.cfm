<!--- �Relayware. All Rights Reserved 2014 --->
<!----
	NJH 2007-09-19 file based from reports/reportList.cfm

	NJH 2010-08-05 - RW8.3 - code clean up. Didn't need menu file name.
2012-02-03		WAB		Sprint Menu Changes.  Rationalise menu XML code.  Client extensions dealt with automatically.
2015-12-21		SB      Bootstrap
2016-11-04		STCR	PROD2016-2672 CASE 449896 Translate the H1 on subMenuNav, using the existing sys_nav phrases.
2016-11-08		STCR	PROD2016-2672 CASE 449896 Allow translation of the SubMenuAction link text, without requiring new phrases to be added to retain the existing OOTB English.
--->


<!---
WAB 2011-01-17 this template has been removed, the variable that it returns is not used
<cfinclude template="/templates/getUserSecurityLevels.cfm">
--->
<div id="subMenuNavContainer">
<cf_includeJavascriptOnce template = "/javascript/myRelay.js">


<cfparam name="subMenuNavItem" type="string" default="reports">

<cfparam name="uniqueFrameID" default="subMenuNavItem#createUUID()#">

<cfset qrySubMenuNavItem = application.com.relayMenu.getSubMenuNavItems(module=module,subMenuNavItem=subMenuNavItem)>

<!--- for backwards compatibility as some reports are still in reportList.xml--->
<cfif subMenuNavItem eq "reports">
	<CFSET FileLocation=replace("#application.path#\xml\#subMenuNavItem#List.xml","/","\")>

	<CFIF not fileExists("#FileLocation#")>
		<CFSET AttributesError="No ReportList.xml file found; looking for #FileLocation#">
	</CFIF>

	<cftry>
		<cffile action="read"
	 			file="#application.paths.relayware#\xml\ReportList.xml"
	 			variable="myxml">
		<cfcatch type="Any">XML file not found</cfcatch>
	</cftry>
	<cfscript>
	  	myxmldoc = XmlParse(myxml);
	  	selectedElements = XmlSearch(myxmldoc, "//reportGroup[@name='#module#']/report");
	  	menuXML = StructNew();
	</cfscript>

	<cfset genericReports = queryNew("url,grouping,name","varchar,varchar,varchar")>
	<cfif arrayLen(selectedElements)>
		<cfloop array="#selectedElements#" index="report">
			<cfset queryAddRow(genericReports)>
			<cfset querySetCell(genericReports,"url","/report/reportXMLGeneric.cfm?reportName=#report.XmlAttributes.Name#")>
			<cfset querySetCell(genericReports,"name",report.XmlAttributes.Title)>
			<cfset querySetCell(genericReports,"grouping",module)>
		</cfloop>
	</cfif>

	<!--- User reports --->
	<cfset qryClientReports = application.com.relayReports.getClientReports(module=module)>

	<!--- Jasper Reports --->
	<cfset qryJasperReports = queryNew("label,ReportURI,grouping","varchar,varchar,varchar")>
	<cfif application.com.relayMenu.isModuleActive(moduleID="ReportManager")>
		<cfset qryJasperReports = application.com.jasperServer.getReportsQuery(module=module)>
	</cfif>

	<!--- NJH 2007/09/27 if we have client specific client reports, append them to the main reports --->
	<cfquery name="qryReports" dbtype="query">
		select name, url, grouping from qrySubMenuNavItem
		union
		<cfif qryClientReports.recordCount>
		select name, '/' + url, grouping from qryClientReports
		union
		</cfif>
		<cfif qryJasperReports.recordCount gt 0>
		select label as name, '/templates/displayJasperServerResource.cfm?resource=report&resourceURI=' + ReportURI as url, grouping from qryJasperReports
		union
		</cfif>
		select name,url,grouping from genericReports
		order by grouping
	</cfquery>
</cfif>

<cfset application.com.request.setDocumentH1(text='phr_sys_nav_'&SubMenuNavItem)>
<!--- <CF_RelayNavMenu pageTitle="#SubMenuNavItem#" thisDir="templates">
	<!--- <CF_RelayNavMenuItem description="Hide this panel" MenuItemText="<img src='/images/MISC/close_icon.gif' alt='Hide this panel' border='0'>" CFTemplate="javascript:parent.document.body.cols = '0,*';"> --->
</CF_RelayNavMenu> --->

<div id="configListDiv">
	<cfif subMenuNavItem neq "reports">
		<cfoutput query="qrySubMenuNavItem" group="grouping">
				<h3><cfif grouping neq "">#htmleditformat(grouping)#<cfelse>phr_mod_#htmleditformat(module)#</cfif></h3>
				<cfoutput>
				<ul>
					<li>
						<A HREF="#application.com.security.encryptURL(qrySubMenuNavItem.URL)#" target="#uniqueFrameID#" CLASS="smallLink"><cfif left(LTrim(qrySubMenuNavItem.Name),4) eq 'phr_'>#htmleditformat(qrySubMenuNavItem.Name)#<cfelse>phr_#htmleditformat(Replace(LTrim(qrySubMenuNavItem.Name)," ","_","ALL"))#</cfif></A>
					</li>
				</ul>
				</cfoutput>

		</cfoutput>
	<cfelse>

		<cfoutput query="qryReports" group="grouping">
			<h3><cfif grouping neq "">#htmleditformat(grouping)#<cfelse>phr_mod_#htmleditformat(module)#</cfif></h3>
				<cfoutput>
					<ul>
						<li>
							<cfset application.com.myRelay.displayLink(linklocation=URL,linkName=Name,linkAttribute1=grouping)>
						</li>
					</ul>
				</cfoutput>
		</cfoutput>
	</cfif>
</div>