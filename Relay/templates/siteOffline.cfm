<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 

	Modifications
	
	2008-07-23	NJH		CR-TND557 Modified so that if an element exists with textID 'siteOffline' (which should be a system element), then use it;
						Otherwise use the standard text.

 --->

<cf_translate>

<CFOUTPUT>

<cfquery name="getSiteOffLineElement" datasource="#application.siteDataSource#">
	select * from element where elementTextID = 'siteOffline'
</cfquery>

<cfif getSiteOffLineElement.recordCount eq 0>
	<CENTER>
	
	<P>We are performing system maintenance which means this service is temporarily unavailable.</P>
	
	<P>We estimate that the system should be available again by #dateformat(dateAdd("h",1,now()),"dd-mmm-yyyy")# #timeformat(dateAdd("h",1,now()),"HH:mm")#</P>
	
	<P>Please email <A HREF="#application.AdminEmail#">#htmleditformat(application.AdminEmail)#</A> for further information.</P>
	
	<P>We apologise for any inconvenience this may cause.</P>
	
	</CENTER>
	<p>#htmleditformat(CGI.REMOTE_ADDR)#</p>
<cfelse>
	<div class="siteOfflineDiv">
		<h1>phr_headline_element_#htmleditformat(getSiteOffLineElement.id)#</h1>
		<p>phr_summary_element_#htmleditformat(getSiteOffLineElement.id)#</p>
		<p>phr_detail_element_#htmleditformat(getSiteOffLineElement.id)#</p>
	</div>
</cfif>

</CFOUTPUT>

</cf_translate>

		
