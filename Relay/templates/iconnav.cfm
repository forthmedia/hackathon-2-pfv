<!--- �Relayware. All Rights Reserved 2014 --->
<cfsetting enablecfoutputonly="yes">

<cf_includeJavascriptOnce template = "/javascript/extExtension.js">
<cf_includeJavascriptOnce template = "/javascript/openwin.js">
<cf_IncludeJavascriptOnce template = "/javascript/navandsitesearch.js" translate="true">

<cf_head>
	<cfoutput><link rel="stylesheet" type="text/css" media="screen" href="/styles/relayui.css" /></cfoutput>
	<cfif fileExists("#application.paths.code#\styles\relayui.css")><cfoutput><link rel="stylesheet" type="text/css" media="screen" href="/code/styles/relayui.css" /></cfoutput></cfif>
</cf_head>

<cfparam name="url.module" default="eLearning">
<cfparam name="variables.phraseSuffix" default="">
<cfparam name="variables.itemCountStruct" default="#StructNew()#">

<cfset validMenuItemIDs = application.com.relayMenu.getValidMenuItemIDs()>

<cfif listFindNoCase(validMenuItemIDs,url.module)>

	<cfset menuOrderVariable = "ui.iconnav-#url.module#">
	<cfset navOrder = application.com.settings.getsetting("#menuOrderVariable#")>

	<cfset stNavOrder = {}>
	<cfset orderValue = 1>
	<cfloop list="#navOrder#" index="i">
		<cfset stNavOrder[i] = orderValue++>
	</cfloop>

	<!--- <cfset menuXML = application.com.relayMenu.getMenuXMLDoc()>
	<cfset aMenu = XmlSearch(menuXML, "//MenuItem[@Module = '#url.module#']")> --->
	<cfset aMenu = application.com.xmlFunctions.xmlSearchWithLock(xmlDoc=application.com.relayMenu.getMenuXMLDoc(),xPathString="//MenuItem[@Module = '#url.module#']",lockName="applicationMenu")>

	<cfif not arrayLen(aMenu)>
		<cfabort showerror="Menu module <em>#htmlEditFormat(url.module)#</em> does not exist.">
	</cfif>

	<cfset parentExtData = "">
	<cfif structKeyExists(aMenu[1].XmlAttributes,"extData")>
		<cfset parentExtData = aMenu[1].XmlAttributes.extData>
	</cfif>

	<cfset thisMenu = aMenu[1].XmlChildren>

	<cfset stXmlChildren = {}>
	<cfset basePos = 2000>
	<cfloop from="1" to="#arrayLen(thisMenu)#" index="i">
		<cfset xmlA = thisMenu[i].xmlAttributes>
		<cfif structKeyExists(xmlA,"fullID") and structKeyExists(xmlA,"SECURITY") and application.com.login.checkSecurityList(xmlA.SECURITY)>
			<cfset thisPos = 0>
			<cfif structKeyExists(stNavOrder,xmlA.fullID)>
				<cfset thisPos = stNavOrder[xmlA.fullID]>
			</cfif>
			<cfif !thisPos>
				<cfset thisPos = basePos++>
			</cfif>
			<cfset xmlA.sortOrder = thisPos>
			<cfset stXmlChildren[thisPos] = xmlA>
		</cfif>
	</cfloop>
	<cfset aSorted = structSort(stXmlChildren,"numeric","asc","sortOrder")>

	<cfoutput><ul id="iconNav" class="#lCase(url.module)# row">
	<cfloop array="#aSorted#" index="x">
		<cfset e = duplicate(stXmlChildren[x])>
		<cfset thisExtData = "">
		<cfif structKeyExists(e,"extData")>
			<cfset thisExtData = "," & e.extData>
		</cfif>
			<!--- 2014-08-21	RPW	Add to Home Page options to display Activity Stream and Metrics Charts --->
			<cfscript>
            variables.linkItem = Replace(LTrim(e.tabText)," ","_","ALL") & variables.phraseSuffix;
            menuPhraseTextID = "phr_sys_nav_" & Replace(LTrim(e.tabText)," ","_","ALL") & variables.phraseSuffix;
            viewText = "phr_sys_nav_" & Replace(LTrim(e.tabText)," ","_","ALL");
			variables.tmpStruct = structNew();

			thisClass = lCase(reReplaceNoCase(e.tabText,"[^[:alnum:]]","","all"));
			iconClass = lcase(url.module);

			if (StructCount(variables.itemCountStruct)) {
				if (StructKeyExists(variables.itemCountStruct,variables.linkItem)) {
					variables.tmpStruct = duplicate(variables.itemCountStruct[variables.linkItem]);
					variables.mergeField = listFirst(structKeyList(variables.tmpStruct));
					// NJH 2015/10/06 - fix merge errors
					if (variables.tmpStruct[variables.mergeField] != "" && variables.tmpStruct[variables.mergeField] != 0) {
						variables.viewTextDisplay = application.com.relayTranslations.translatePhrase(phrase=menuPhraseTextID,mergeStruct=variables.tmpStruct);
					} else {
						e.condition = false;
					}
				} else {
					e.condition = false;
				}
			}
			</cfscript>
		<!--- WAB 2013-04-16 added a span around the label - to help clive get an xpath handle on the element, needed to remove style for '#iconnav a span', but appeared not to be used anywhere --->
		<cfif (not structKeyExists(e,"condition") or evaluate(e.condition)) and (not structKeyExists(e,"module") or application.com.relayMenu.isModuleActive(moduleID=e.module))>
		<li id="#e.fullid#" class="col-xs-6 col-sm-6 col-md-4 col-lg-3"><div class="icon"><a href="" class="#thisClass#" <cfif e.URL is "">onclick="alert('Coming Soon.'); return false;"<cfelse>onclick="openNewTab('#e.fullid#','#viewText#','#application.com.relayTranslations.checkForAndEvaluateCFVariables(phraseText=e.URL)#',{iconClass:'#iconClass#'#thisExtData#}); return false"</cfif>><span>#variables.viewTextDisplay#</span></a></div></li>
		</cfif>
	</cfloop>
	</ul><br /><div id="iconNavUpdated"></div></cfoutput>

	<cf_head>
		<cfoutput><script>var menuOrderVariable = '#menuOrderVariable#';</script></cfoutput>
	</cf_head>

<cfelse>
	<cfoutput><p>Invalid menuID</p></cfoutput>
</cfif>
