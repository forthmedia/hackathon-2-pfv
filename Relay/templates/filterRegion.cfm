<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			RegionFilter.cfm
Author:				KAP
Date started:		14-Aug-2003
	
Description:			

Region/Country filter called by forecastSelector.cfm

It uses its own form, so there's a JavaScript example of migrating the selected countries into a master form (onsubmit event in forecastSelector.cfm).

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
14-Aug-2003			KAP			Initial version

Possible enhancements:


--->


<!--- 
<cfparam name="frmPersonid" type="string" default="1497496">
 --->
<cfparam name="Cookie.USER" type="string" default="0">
<cfset frmPersonid = ListFirst( Cookie.USER, "-" )>

<!--- --------------------------------------------------------------------- --->
<!--- the following code mainly lifted form useredit.cfm --->
<!--- --------------------------------------------------------------------- --->
<CFQUERY NAME="getPersonCountries" datasource="#application.siteDataSource#">
	SELECT 	r.CountryID,
			max(r.Permission) as permission
	FROM Rights AS r, UserGroup AS ug
	WHERE r.SecurityTypeID = (SELECT e.SecurityTypeID FROM SecurityType AS e
							 WHERE e.ShortName = 'RecordTask')
	AND r.UserGroupID = ug.UserGroupID
	AND ug.PersonID =  <cf_queryparam value="#frmPersonid#" CFSQLTYPE="CF_SQL_INTEGER" > 
	GROUP BY r.CountryID
</CFQUERY>
<CFSET personCountries = ListAppend(ValueList(getPersonCountries.CountryID),"0")>
<CFSET personCountryPermission = ListAppend(ValueList(getPersonCountries.Permission),"0")>

<cfinclude template="/templates/qrygetcountries.cfm">

<!--- NB qrygetcountries has a query called getCountries too --->
<CFQUERY NAME="getCountries" datasource="#application.siteDataSource#">
	<!--- all the countries to which the user has rights --->
SELECT		'1' AS permiss,
 			(select distinct a.Permission from PredefinedUGSecurity AS a, SecurityType AS f where  a.SecurityTypeID = f.SecurityTypeID and f.shortname = 'RecordTask') as defaultPermission, 
			c.CountryID,
			c.CountryDescription AS CountryDescription
	FROM Country AS c
	WHERE c.CountryID  IN ( <cf_queryparam value="#Variables.CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) <!--- from qrygetcountries.cfm --->
	  AND c.ISOCode IS NOT null
	
 
 <!--- all the countries to which the person belongs but not in the above list --->
 UNION

	SELECT	'0',
			(select distinct a.Permission from PredefinedUGSecurity AS a, SecurityType AS f where  a.SecurityTypeID = f.SecurityTypeID and f.shortname = 'RecordTask') , 
			c.CountryID,
			c.CountryDescription
	FROM Country AS c
	WHERE c.CountryID  NOT IN ( <cf_queryparam value="#Variables.CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) 
	  AND c.CountryID  IN ( <cf_queryparam value="#Variables.personCountries#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	  AND c.ISOCode IS NOT null
	ORDER BY CountryDescription	  
</CFQUERY>

<cfset RegionSelectHeight = "5">
<cfinclude template="/admin/userManagement/RegionSelect.cfm">
<!--- --------------------------------------------------------------------- --->

<form action="" name="mainForm">
<table border="0" cellspacing="0" cellpadding="0">
<cfloop query="getCountries">

<cfif BitAnd( getCountries.CurrentRow - 1, 1 ) eq 0>
</tr><tr class="<cfoutput>#iif( BitAnd( getCountries.CurrentRow - 1, 2 ) eq 0, de( "oddrow" ), de( "evenrow" ) )#</cfoutput>">
</cfif>

<td valign="top" class="smaller">
<CF_INPUT type="checkbox" name="insCountryID" value="#CountryID#"CHECKED="#iif(ListFind(personCountries,CountryID) IS NOT 0,true,false)#">
</td>
<td valign="top" class="smaller">
<cfoutput>#htmleditformat(CountryDescription)#</cfoutput>
</td>
</cfloop>

</table>
</form>


