<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
		form fields from selectmain.cfm
		returns variable "searchID" with the new selection's ID
	
		A call to this template must have CFTRANSACTION around it 
		therefore always put a closing CFTRANSACTION before any CFABORTs

		Populates temporary tables identical to SelectionTag and SelectionCriteria
--->

<!--- mods:
WAB 2000-03-02  Added code to handle sub queries in the personid and locationid fields - in particular does not escape the single quotes
WAB 2000-04-20 ++  Added code to loop around list of things passed from multi-select boxes  (eg postalcodes, valid values etc.)
WAB 2000-10-18  Added replacement for QuotedFieldData for non flag fields which I seemed to have missed out previously
WAB 2005-10-25  added a mod to allow searching for 0 in flags.  0 is currently filtered out as a 'null', but I can't see why this is necessary - none of the flags use 0 as null
WAB 2005-11-29  allow nvarchar criteria (Action 713)
WAB 2007-03-21  sort out single quote problem in searching  (say o'rourke)  - amazing that not found before!
 --->


<!--- use variable from calling template if it exists --->
<CFPARAM NAME="FreezeDate" DEFAULT=#Now()#>

	<!--- records stored with PersonID --->
	<CFQUERY NAME="delTags" datasource="#application.siteDataSource#">
		DELETE FROM TempSelectionTag
		WHERE SelectionID = #request.relayCurrentUser.personid#
	</CFQUERY>

	<CFQUERY NAME="delCriteria" datasource="#application.siteDataSource#">
		DELETE FROM TempSelectionCriteria
		WHERE SelectionID = #request.relayCurrentUser.personid#
	</CFQUERY>

	<CFSET searchID = request.relayCurrentUser.personid>
	
	<!--- save criteria
			NB can't save into SelectionTag until the criteria is saved and
			query run  --->


	<!---  Variables.validfields  = list of form fields that initiate a save
									in SelectionField: 
			Form.FORMFIELDS       = list of all the form fields coming in.  Will list
									 form elements like checkboxes more than once.

			for each Variables.validField in Form.FormFields initiate a save --->
			 
	<CFQUERY NAME="getValid" DATASOURCE="#application.siteDataSource#">
		SELECT SelectionField, CriteriaMask
		FROM SelectionField
	</CFQUERY>

	<CFIF IsDefined("frmCountryID")>
		<CFIF Find("*", frmCountryID) GT 0>
			<!--- get the user's countries --->
			<CFINCLUDE TEMPLATE="../templates/qrygetcountries.cfm">
			<!--- qrygetcountries creates a variable CountryList --->
			
			<CFQUERY NAME="getcountries" datasource="#application.siteDataSource#">
			SELECT DISTINCT a.CountryID
			FROM Country AS a, Country AS b, CountryGroup AS c
			WHERE b.CountryID  IN ( <cf_queryparam value="#Replace(frmCountryID, "*", "", "ALL")#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			AND c.CountryGroupID = b.CountryID AND c.CountryMemberID = a.CountryID
			AND a.CountryID  IN ( <cf_queryparam value="#Variables.CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			</CFQUERY>
			<CFIF getcountries.RecordCount GT 0>
				<CFSET frmCountryGroup = ListAppend(ValueList(getcountries.CountryID), Replace(frmCountryID, "*", "", "ALL"))>
				<CFSET fieldnames = ListAppend(FORM.fieldNames, "frmCountryGroup")>
			</CFIF>
		</CFIF>
	</CFIF>
	
	<CFSET validfields = ValueList(getValid.SelectionField)> <!--- fields which can be saved as search criteria --->
	
	<CFSET donefields = ""> <!--- Form.FormFields that we have already processed --->

	<!--- add each relevant form field and it's criteria to the 
			SelectionCriteria table --->

	<CFLOOP INDEX="frmField" LIST=#FIELDNAMES#>
		<CFSET finalcriteria = "">
		<!--- since fields such as checkboxes will be listed in FORMFIELDS more than once
				check that it is a valid field and has not already been processed --->
				
		<!--- New Flags --->
		<CFIF ListFirst(frmField,"_") IS "frmFlag">
			<!--- <CFSET frmFieldName = "frmFlag" & ListGetAt(frmField,2,"_")> --->
			<CFSET fieldvalue = trim(Evaluate("#frmField#"))>
			
			<CFIF (ListFindNoCase(donefields,frmField, ",") IS 0)
			  <!--- AND (fieldValue IS NOT 0 )  --->  <!--- WAB 2005-10-25 WAB removed this.  none of the flags use 0 as a null --->
			  AND (fieldValue IS NOT "")>
				
				<CFLOOP QUERY="getValid">

					<CFIF CompareNoCase(frmField,SelectionField) IS 0>
						<!--- get the CriteriaMask Format for the appropriate selection 
								field and substitue the value of the form
								field for "FieldData in CriteriaMask --->
						<!--- WAB added, some fields require a quoted list.
								In this case we replace the expression QuotedFieldData with
								value of the form with comma replaced with ',' .  The opening
								and closing quotes are supplied by the criteria mask --->

							<CFSET a =Evaluate("#frmField#")>
							<CFSET a = Replace(a, "'", "''''", "ALL")>   <!--- WAB 2007-03-21 added this replace.   '''' - once in selectionCriteria table this becomes ''  and when used in qrycreateselection it stays as ''  which is what is required.  It looks as if CF doesn't automatically escape '' to '''' (fair enough really)--->
							<CFSET quoteda = listQualify(a,"'")>  <!--- converts a comma delimited list from a multiselect into a quoted list--->
							
						<CFIF #ListGetAt(frmField,2,"_")# IS "Date">
							<CFSET a = #CreateODBCDateTime(Evaluate("#frmField#"))#>
						</CFIF>

						<!--- special code for true and false - needed because anything set to 0 is ignored, so need another code.  *t not strictly necessary  --->
 						<CFIF a IS "*t">
							<CFSET a = "1">
						<CFELSEIF a IS "*f">
							<CFSET a = "0">
						</CFIF>
						
						<!--- WAB altered --->						



						<cfset finalcriteria = CriteriaMask>
						<CFIF reFIND("\(\{.+}\)", finalcriteria ) is not 0 >
								<!--- looks for ({ ...... })   --->
						<!--- 
						This is probably a search on a text flag which can contain more than one valid value
						We might have been given a list of items to search for, so we need to loop through them 
						the { }  s indicates the extent of the statement which needs to be OR'ed is --->
							<!--- extract the string from between the {} s, this string will be repeated for each item to be searched for in the list --->
							<CFSET 	beginning = reFIND("{.*}", finalcriteria )>
							<CFSET 	end = reFIND("}", finalcriteria, beginning )>							
							<CFSET query = mid (finalcriteria, beginning+1, end - beginning -1) >
							
							<CFSET thisCriteria =  "">

							<CFLOOP index="FieldDataItem" list ="#a#">

								<CFSET thisCriteria =  thisCriteria & " OR " & query>							
								<CFSET thiscriteria = ReplaceNoCase(thisCriteria,"QuotedFieldDataItem","'#FieldDataItem#'","ALL")> 								
								<CFSET thiscriteria = ReplaceNoCase(thisCriteria,"FieldDataItem",FieldDataItem,"ALL")> 								
							</cfloop>
							
												
								<!--- take off the leading OR --->
								<CFSET thisCriteria = Replace(thisCriteria," OR ","","ONE")>
			
								<!--- pop thiscriteria back into the finalcriteria string --->					
								<CFSET finalCriteria = reReplace(finalCriteria, "(.*)\{(.*)\}(.*)", "\1#thiscriteria#\3")>

						</cfif>

						<CFSET finalcriteria = Replace(finalCriteria,"QuotedFieldData",quoteda,"ALL")> 
						<CFSET finalcriteria = Replace(finalCriteria,"FieldData",a,"ALL")>
						<!--- NJH 2010/08/09 RW8.3 - changed to use query so we can get rid of typedataList, etc --->
						<cfquery name="getFlagType" datasource="#application.siteDataSource#">
							select dataTableFullName from flagType where name =  <cf_queryparam value="#ListGetAt(frmField,2,"_")#" CFSQLTYPE="CF_SQL_VARCHAR" > 
						</cfquery>
						<CFSET finalcriteria = "#getFlagType.dataTableFullName# WHERE " & finalCriteria>
						<!--- <CFSET finalcriteria = "#ListGetAt(application.typeDataList,ListFindNoCase(application.typeList,ListGetAt(frmField,2,"_")))#FlagData WHERE " & finalCriteria> --->
						<CFSET finalcriteria = Replace(finalcriteria,"FID","#ListGetAt(frmField,3,"_")#","ALL")>

						<CFBREAK>
						
									
					</CFIF>
				</CFLOOP>
				
				<cfset frmFieldValue = 	evaluate(frmField)>		<!--- doing this means that single quotes will be automatically escaped in the query  --->
				<CFQUERY NAME="SC#frmField#" DATASOURCE="#application.siteDataSource#">			
					INSERT INTO TempSelectionCriteria (SelectionID,SelectionField,Criteria,Criteria1,CreatedBy,Created,LastUpdatedBy,LastUpdated) 
					 VALUES (#Variables.searchID#,'#frmField#','#Variables.finalcriteria#','#frmFieldValue#',#request.relayCurrentUser.usergroupid#,#FreezeDate#,#request.relayCurrentUser.usergroupid#,#FreezeDate#)
				</CFQUERY>
				<!--- add field to list of donefields --->
				<CFSET donefields = ListAppend(Variables.donefields,frmField,",")> 
			
			</CFIF>
		<CFELSE>
			
		<!--- 'various changes to accomodate extended SQL logic' [godfrey.smith]- March 17, 2008, 15:24:46 PM --->

		<CFIF (ListFindNoCase(validFields,frmField, ",") IS NOT 0)
		  AND (ListFindNoCase(donefields,frmField, ",") IS 0)
		  AND (Evaluate("#frmField#") IS NOT 0)
		  AND (Evaluate("#frmField#") IS NOT "CompanyName")
		  AND (Evaluate("#frmField#") IS NOT "Region")
		  AND (Trim(Evaluate("#frmField#")) IS NOT "")>

			<CFLOOP QUERY="getValid">
				<CFIF CompareNoCase(frmField,SelectionField) IS 0>

					<!--- 
					get the CriteriaMask Format for the appropriate selection field and substitue the value of the form
					field for "FieldData in CriteriaMask 
					--->
					
					<!--- 
					this replaces all single quote with double quotes, however occasionally you don't want this to happen, 
					so test for the special case of \' and does not do the replacement (infact it does it and then reverses it)
					--->
					
					<cfset frmFieldValue = Evaluate("#frmField#")>

					<!--- NJH 2008/09/19 - added the find to the condition, as a comma delimited list was classified as a date (eg. 5,4 was converted to '2008-05-04') --->
					<cfif isDate(frmFieldValue) and not find(",",frmFieldValue)>
						<cfset frmFieldValue = dateformat(frmFieldValue,"yyyy-mm-dd")>
					</cfif>

					
					<CFSET a = Replace(frmFieldValue, "'", "''''", "ALL")>   <!--- WAB 2007-03-21 changed from '' to '''' - once in selectionCriteria table this becomes ''  and when used in qrycreateselection it stays as ''  which is what is required.  It looks as if CF doesn't automatically escape '' to '''' (fair enough really)--->
				<CFSET a = Replace(a, "\''", "'", "ALL")>
				<CFSET quoteda = listQualify(a,"'")>  <!--- converts a comma delimited list from a multiselect into a quoted list--->


 						<CFIF a IS "*t">
							<CFSET a = "1">
						<CFELSEIF a IS "*f">
							<CFSET a = "0">
						</CFIF>

				<CFSET finalcriteria = CriteriaMask>
						
						<CFIF reFIND("\(\{(.)+}\)", finalcriteria ) is not 0 >
								<!--- looks for ({ ...... })   --->
						<!--- 
						This is probably a search on a text flag which can contain more than one valid value
						We might have been given a list of items to search for, so we need to loop through them 
						the { }  s indicates the extent of the statement which needs to be OR'ed is --->
							<!--- extract the string from between the {} s, this string will be repeated for each item to be searched for in the list --->
							<CFSET 	beginning = reFIND("{.*}", finalcriteria )>
							<CFSET 	end = reFIND("}", finalcriteria, beginning )>							
							<CFSET query = mid (finalcriteria, beginning+1, end - beginning -1) >

							<CFSET thisCriteria =  "">
							<CFLOOP index="FieldDataItem" list ="#a#" delimiters=",*">

								<CFSET thisCriteria =  thisCriteria & " OR " & query>							
								<CFSET thiscriteria = ReplaceNoCase(thisCriteria,"QuotedFieldDataItem","'#FieldDataItem#'","ALL")> 								
								<CFSET thiscriteria = ReplaceNoCase(thisCriteria,"FieldDataItem",FieldDataItem,"ALL")> 								
							</cfloop>
												
								<!--- take off the leading OR --->
								<CFSET thisCriteria = Replace(thisCriteria," OR ","","ONE")>
			
								<!--- pop thiscriteria back into the finalcriteria string --->					
								<CFSET finalCriteria = reReplace(finalCriteria, "(.*)\{(.*)\}(.*)", "\1#thiscriteria#\3")>

						</cfif>

					
					<CFSET finalcriteria = Replace(FinalCriteria,"QuotedFieldData",quoteda,"ALL")>
					<CFSET finalcriteria = Replace(FinalCriteria,"FieldData",a,"ALL")>

					<CFBREAK>
				</CFIF>
			</CFLOOP>

<!---
			<CF_ADMINEMAIL SUBJECT="Error in Code" MESSAGE="A criteria could not be located in SelectionFields for the form field: #frmField#.">
 --->
<!--- 				<CFOUTPUT>#finalcriteria# <BR></cfoutput>			 --->
 			<CFQUERY NAME="SC#frmField#" DATASOURCE="#application.siteDataSource#">			
				INSERT INTO TempSelectionCriteria (SelectionID,SelectionField,Criteria,Criteria1,CreatedBy,Created,LastUpdatedBy,LastUpdated) 
				 VALUES (<cf_queryparam value="#Variables.searchID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#frmField#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#Variables.finalcriteria#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#frmFieldValue#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >,<cf_queryparam value="#FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >,<cf_queryparam value="#FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >)
			</CFQUERY>
			<!--- add field to list of donefields --->
			<CFSET donefields = ListAppend(Variables.donefields,frmField,",")>
		</CFIF>
		
		</CFIF>

	</CFLOOP>
 <!--- info saved to Selection Tag after the query is run ---->
 

