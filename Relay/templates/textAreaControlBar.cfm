<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			textAreaControlBar.cfm	
Author:				SWJ
Date started:		2002-11-21
	
Description:		This provides a control bar when called in ie.

Usage:				Include this file where you want the table to appear.
					Must be in the form that contains the text area inside .
					<cfmodule template="/templates/textAreaControlBar.cfm" 
						textAreaName="Detail"
						formname="myForm"
						showRelayTags="true"
						showClickThrus="false"
						showSpecialFields="false"
						showFilesForDownload="false">

					Add the following javascript calls to the text area in your page like this:
						<textarea name="detail" 
							ONSELECT="markSelection(this);" 
							ONCLICK="markSelection(this);" 
							ONKEYUP="markSelection(this);">
						</textarea> 

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
Sept 2006	WAB			put tag info into a javascript array and altered javascript
Mar 2007	WAB			link to new relaytag picker
2012/10/25	NJH			Social CR - Changed taform to this.form, so that we don't have to pass in a form name
Enhancements still to do:


 --->
<cfparam name="attributes.showHTMLTags" default="true">
<cfparam name="attributes.showHTMLPageTags" default="true">
<cfparam name="attributes.showSpecialFields" default="false">
<cfparam name="attributes.showRelayTags" default="false"> 
<cfparam name="attributes.showMergeFields" default="false">
<cfparam name="attributes.context" default="email">


<cfparam name="attributes.showFilesForDownload" default="false"> 
<!--- <cfif attributes.showFilesForDownload>
		<cfset getFilesForDownload = application.com.filemanager.getFilesAPersonHasRightsTo (personid = #request.relayCurrentUser.personid#, filetypegroupid = 1)>
</cfif> --->



				<!--- 
				WAB 2006-02-14
				rework this code so that can handle more than one textbox on a page  (although can only handle one form name still!)
				took all cf variables out of the core js functions and stuffed them into separate .js file
				
				
				just left with some code to set the oncontextmenu event

				--->

			<cf_includejavascriptonce template="/javascript/relayTagEditor.js">
			<cf_includejavascriptonce template="/javascript/textAreaControlBar.js">

<cfoutput>
			<SCRIPT type="text/javascript">
				var menu;
				var keptSelection = "";
			</script>
			<cfset caller.textAreaControlBar.onContextMenuEvent = "showMenu(event,this)">
			
			<STYLE>
				.textAreaContextMenu {
					position: absolute;
					visibility: hidden;
					width: 120px;
					background-color: lightgrey;
					layer-background-color: lightgrey;
					border: 2px outset white;
					top:0;
					left:0;
				}
				A.menu {
					color: black;
					text-decoration: none;
					cursor: default; width: 100%;
					display: block
				}
				A.menuOn {
					color: white;
					background-color: darkblue;
					text-decoration: none;
					cursor: default; width: 100%;
				}
</STYLE>




	<table>
		<tr>
		<CFIF attributes.showHTMLTags>
		<td><a onClick="insertText ( this.form['#attributes.textAreaName#'], '&nbsp;')"> <img src="/devEdit/de_images/button_nb.gif" alt="Non-breaking space" border="0"></a></td>
		<td><a onClick="insertTag ( this.form['.#attributes.textAreaName#'], 'hr', false )"><img src="/devEdit/de_images/button_hr.gif" alt="Horizontal line" ></a></td>
		<td><a onClick="insertTag ( this.form['.#attributes.textAreaName#'], 'p', true )"><img src="/devEdit/de_images/button_para.gif" alt="Wrap in paragraph tags"></a></td>
		<td><a onClick="insertTag ( this.form['.#attributes.textAreaName#'], 'br', false )"><img src="/devEdit/de_images/button_br.gif" alt="Line break"></a></td>
		<td>&nbsp;|&nbsp;</td>
		<td><a onClick="insertTag ( this.form['.#attributes.textAreaName#'], 'strong', true )"><img  src="/devEdit/de_images/button_bold.gif" alt="Bold text"></a></td> 
		<td><a onClick="insertTag ( this.form['.#attributes.textAreaName#'], 'em', true )"><img src="/devEdit/de_images/button_italic.gif" alt="Italicise text"></a></td> 
		<td><a onClick="insertTag ( this.form['.#attributes.textAreaName#'], 'h1', true )"><img src="/devEdit/de_images/button_h1.gif" alt="Heading size 1" ></a></td>
		<td><a onClick="insertTag ( this.form['.#attributes.textAreaName#'], 'h2', true )"><img  src="/devEdit/de_images/button_h2.gif" alt="Heading size 2" ></a></td>
		<td><a onClick="insertTag ( this.form['.#attributes.textAreaName#'], 'h3', true )"><img  src="/devEdit/de_images/button_h3.gif" alt="Heading size 3" ></a></td>
		</CFIF>
		
		<cfif attributes.showMergeFields>
			<!--- 2005-08-23 SWJ: modified this to us the memory resident application.relayTagStruct.  Used to use a query. --->
			<cfset mergeFieldFunctionQry = application.com.relayTags.getMergeFieldsAndFunctions(context=attributes.context)>
			<script>
			var tags = new Object();
				<cfloop query = mergeFieldFunctionQry>
					var thisTag = new Array();				
						thisTag.editorExists = #jsStringFormat(editorExists)#
						thisTag.tag = '#jsStringFormat(tag)#' ;
						thisTag.name = '#replaceNoCase(CleanHTMLCode,"'","\'","ALL")#'
						thisTag.insertCode = '#replaceNoCase(CleanHTMLCode,"'","\'","ALL")#'
						thisTag.description = '#replaceNoCase(replaceNoCase(description,"'","\'","ALL"),chr(13)&chr(10),"\n","ALL")#'
						thisTag.editorPath = '#jsStringFormat(editorPath)#'
					tags[thisTag.name.toLowerCase()] = thisTag;
				</cfloop>
			</script>
			
			
		<td>
			

			<DIV class= "textAreaContextMenu" ID="contextMenu_#attributes.textAreaName#" ONMOUSEOUT="menu = this; this.tid = setTimeout('menu.style.visibility = \'hidden\'', 20);" ONMOUSEOVER="clearTimeout(this.tid);">
				<A HREF="##" onClick="editClick(this.form['#attributes.textAreaName#']);showHideMenu(event,'hide','#attributes.textAreaName#');" CLASS="menu" ONMOUSEOVER="this.className = 'menuOn'" ONMOUSEOUT="this.className = 'menu';">Edit Tag</A>
				<A HREF="##" onclick="javascript:manipulateItem('SelectAll');showHideMenu(event,'hide','#attributes.textAreaName#');" CLASS="menu" ONMOUSEOVER="this.className = 'menuOn'" ONMOUSEOUT="this.className = 'menu';">Select All</A>
				<A HREF="##" onclick="javascript:manipulateItem('Copy');showHideMenu(event,'hide','#attributes.textAreaName#');" CLASS="menu" ONMOUSEOVER="this.className = 'menuOn'" ONMOUSEOUT="this.className = 'menu';">Copy</A>
				<A HREF="##" onclick="javascript:manipulateItem('Cut');showHideMenu(event,'hide','#attributes.textAreaName#');" CLASS="menu" ONMOUSEOVER="this.className = 'menuOn'" ONMOUSEOUT="this.className = 'menu';">Cut</A>
				<A HREF="##" onclick="javascript:manipulateItem('Paste');showHideMenu(event,'hide','#attributes.textAreaName#');" CLASS="menu" ONMOUSEOVER="this.className = 'menuOn'" ONMOUSEOUT="this.className = 'menu';">Paste</A>
				<A HREF="##" onclick="javascript:showHideMenu(event,'hide','#attributes.textAreaName#');" CLASS="menu" ONMOUSEOVER="this.className = 'menuOn'" ONMOUSEOUT="this.className = 'menu';">Cancel</A>
			</DIV>

			<CF_INPUT type="Button" onClick="javascript:openRelayTagPicker(this.form['#attributes.textAreaName#'],'mergeFields','#attributes.context#')" value="Insert Merge Field" id="frmInsertMergeField">
			
		</td>
		<td></td>
		</cfif>
		
		<cfif attributes.showRelayTags>
			<!--- 2005-08-23 SWJ: modified this to us the memory resident application.relayTagStruct.  Used to use a query. --->

			<script>
			var tags = new Object();
				<cfloop query = application.activeRelayTagQuery>
					var thisTag = new Array();				
						thisTag.editorExists = #jsStringFormat(editorExists)#
						thisTag.tag = '#jsStringFormat(tag)#' ;
						thisTag.name = '#replaceNoCase(CleanHTMLCode,"'","\'","ALL")#'
						thisTag.insertCode = '#replaceNoCase(CleanHTMLCode,"'","\'","ALL")#'
						thisTag.description = '#replaceNoCase(replaceNoCase(description,"'","\'","ALL"),chr(13)&chr(10),"\n","ALL")#'
						thisTag.editorPath = '#jsStringFormat(editorPath)#'
						tags[thisTag.name.toLowerCase()] = thisTag;
				</cfloop>
			</script>
			
			
		<td>
			

			<DIV class= "textAreaContextMenu" ID="contextMenu_#attributes.textAreaName#" ONMOUSEOUT="menu = this; this.tid = setTimeout('menu.style.visibility = \'hidden\'', 20);" ONMOUSEOVER="clearTimeout(this.tid);">
				<A HREF="##" onClick="editClick(this.form['#attributes.textAreaName#']);showHideMenu(event,'hide','#attributes.textAreaName#');" CLASS="menu" ONMOUSEOVER="this.className = 'menuOn'" ONMOUSEOUT="this.className = 'menu';">Edit Tag</A>
				<A HREF="##" onclick="javascript:manipulateItem('SelectAll');showHideMenu(event,'hide','#attributes.textAreaName#');" CLASS="menu" ONMOUSEOVER="this.className = 'menuOn'" ONMOUSEOUT="this.className = 'menu';">Select All</A>
				<A HREF="##" onclick="javascript:manipulateItem('Copy');showHideMenu(event,'hide','#attributes.textAreaName#');" CLASS="menu" ONMOUSEOVER="this.className = 'menuOn'" ONMOUSEOUT="this.className = 'menu';">Copy</A>
				<A HREF="##" onclick="javascript:manipulateItem('Cut');showHideMenu(event,'hide','#attributes.textAreaName#');" CLASS="menu" ONMOUSEOVER="this.className = 'menuOn'" ONMOUSEOUT="this.className = 'menu';">Cut</A>
				<A HREF="##" onclick="javascript:manipulateItem('Paste');showHideMenu(event,'hide','#attributes.textAreaName#');" CLASS="menu" ONMOUSEOVER="this.className = 'menuOn'" ONMOUSEOUT="this.className = 'menu';">Paste</A>
				<A HREF="##" onclick="javascript:showHideMenu(event,'hide','#attributes.textAreaName#');" CLASS="menu" ONMOUSEOVER="this.className = 'menuOn'" ONMOUSEOUT="this.className = 'menu';">Cancel</A>
			</DIV>

		<!---	<select name="RelayTagSelect_#attributes.textAreaName#" size="1" style="font-family: arial, helvetica, sans-serif; font-size: 10px;" 
					onchange="manageRelayTag('#attributes.textAreaName#',this.value,this)">
				<option value="">Relay Tags
						<cfloop query = application.activeRelayTagQuery><option value="#lcase(CleanHTMLCode)#">#InsertName#</cfloop>
			</select>
--->
			<CF_INPUT type="Button" onclick="javascript:openRelayTagPicker(this.form['.#attributes.textAreaName#'],'relaytag')" value="Insert Relay Tag">

			
		</td>
		<td></td>
		</cfif>

		<!--- <cfif attributes.showFilesForDownload>
		NJH 2013/08/28 Case 436732 - removed this as it's now done as a merge function
		<td><select name="select1" size="1" style="font-family: arial, helvetica, sans-serif; font-size: 10px;" onchange="insertText(this.form['.#attributes.textAreaName#'],this.value)">
				<option value="">Files for Download
				<cfloop query="getFilesForDownload"><OPTION VALUE="<a href='<<<download>>>&f=#htmleditformat(fileid)#'>Download file #htmleditformat(name)#</a>">#htmleditformat(name)#</cfloop>
			</select>
		</td>
		<td></td>
		</cfif> --->

		<cfif attributes.showSpecialFields>
		<td><select name="select1" size="1" style="font-family: arial, helvetica, sans-serif; font-size: 10px;" onchange="insertText(this.form['#attributes.textAreaName#'],this.value)">
				<option value="">Special fields 
					<CFLOOP index = "I" from = "1" to = "#structcount(application.emailMergeFieldsTEXT)#">
						<option value="#application.emailMergeFieldsTEXT[i].insertcode#">#application.emailMergeFieldsTEXT[i].title#
					</CFLOOP>
			</select>
		</td>
		</cfif>
		</tr>
	</table>

	<CFIF attributes.showHTMLTags>
	<table>
		<tr>
			<td><a onClick="quickHTML ( this.form['.#attributes.textAreaName#'], 'quickTable' )"><img src="/devEdit/de_images/button_table_down.gif" alt="Insert a quick table"></a></td>		
			<td><a onClick="insertTag ( this.form['.#attributes.textAreaName#'], 'table', true )"><img src="/devEdit/de_images/button_tbl.gif" alt="Add table tag"></a></td>
			<td><a onClick="insertTag ( this.form['.#attributes.textAreaName#'], 'th', true )"><img src="/devEdit/de_images/button_th.gif" alt="Table heading"></a></td>
			<td><a onClick="insertTag ( this.form['.#attributes.textAreaName#'], 'tr', true )"><img src="/devEdit/de_images/button_tr.gif" alt="Table row" ></a></td>
			<td><a onClick="insertTag ( this.form['.#attributes.textAreaName#'], 'td', true )"><img src="/devEdit/de_images/button_td.gif" alt="Table data cell" ></a></td>		
			<td>&nbsp;|&nbsp;</td>
			<td><a onClick="quickHTML ( this.form['.#attributes.textAreaName#'], 'quickOL' )"><img src="/devEdit/de_images/button_numbers.gif" alt="Quick numbered bullet list"></a></td>
			<td><a onClick="quickHTML ( this.form['.#attributes.textAreaName#'], 'quickUL' )"><img src="/devEdit/de_images/button_bullets.gif" alt="Quick Bullet list"></a></td>
			<td><a onClick="insertTag ( this.form['.#attributes.textAreaName#'], 'ol', true )"><img src="/devEdit/de_images/button_ol.gif" alt="Numbered bullet list tag"></a></td>
			<td><a onClick="insertTag ( this.form['.#attributes.textAreaName#'], 'ul', true )"><img src="/devEdit/de_images/button_ul.gif" alt="Bullet list outer tag"></a></td>
			<td><a onClick="insertTag ( this.form['.#attributes.textAreaName#'], 'li', true )"><img src="/devEdit/de_images/button_li.gif" alt="List item" ></a></td>
			<td>&nbsp;|&nbsp;</td>
			<td><a onClick="quickHTML ( this.form['.#attributes.textAreaName#'], 'quickHref' )"><img src="/devEdit/de_images/button_link.gif" alt="Insert URL link"></a></td>
		</tr>
	</table>

	<table>
		<tr>
			<cfif attributes.showHTMLPageTags>
			<td><CF_INPUT type="button" value="HTML Page" onClick="quickHTML ( this.form['.#attributes.textAreaName#'], 'quickHTMLPage' )"></td>
			<td><CF_INPUT type="button" value="htm" onClick="insertTag(this.form['.#attributes.textAreaName#'], 'html', true )"></td>
			<td><CF_INPUT type="button" value="head" onClick="insertTag(this.form['.#attributes.textAreaName#'], 'head', true )"></td>
			<td><CF_INPUT type="button" value="body" onClick="insertTag(this.form['.#attributes.textAreaName#'], 'body', true )"></td>
			</cfif>
		</tr>
	</table>
	</CFIF>	 
	
</cfoutput>
