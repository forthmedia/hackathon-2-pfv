<!--- �Relayware. All Rights Reserved 2014 --->
<!---


2005-05-17   WAB  altered to use permissions correctly, similar to defaultLmenuXML.cfm
2008/09/30 WAB 	further altered to deal with AND and OR in permissions
2010-07-07	WAB	Changed URL in xml file so no longer a	relative paths realtive to templates directory - also change to configToolsList.XML
2011/06/01 WAB LID 6769  Security Scan - Encrypt All URLs
2012/03/30	IH	Case 427037 make frame names unique
03/10/2014 SB Change to tables to css layout
12/11/2015 SB Bootstrap
WAB	2016-06-27	PROD2016-1334 CallRelayRecordManager security.  Encrypt URLs with singleSession argument
 --->

<cfparam name="module" default="admin">
<cfparam name="URL.uniqueFrameID" default="configArea">

<!--- <cfinclude template="/templates/get UserSecurityLevels.cfm"> --->
<CFSET FileLocation=replace("#application.paths.relayware#\XML\configToolsList.xml","/","\")>

<CFIF not fileExists("#FileLocation#")>
	<CFSET AttributesError="No ReportList.xml file found; looking for #FileLocation#">
</CFIF>
<!--- Added for MX --->
<cftry>
	<cffile action="read"
 			file="#application.paths.relayware#\XML\configToolsList.xml"
 			variable="myxml">
	<cfcatch type="Any">XML file not found</cfcatch>
</cftry>
<cfscript>
  	myxmldoc = XmlParse(myxml);
  	selectedElements = XmlSearch(myxmldoc, "//RelayConfigTool[@RelayModules='#module#']");
  	menuXML = StructNew();
</cfscript>

<CFPARAM NAME="reportGroup" TYPE="string" DEFAULT="All Available">
<cfset application.com.request.setDocumentH1(text="#module# Phr_Sys_ConfigurationOptions",id="#module#ConfigH1")>
<div id="configListDiv">
	<!--- <CF_RelayNavMenu pageTitle="#htmleditformat(module)# Phr_Sys_ConfigurationOptions" thisDir="templates">
		<!--- <CF_RelayNavMenuItem description="Hide this panel" MenuItemText="<img src='/images/MISC/close_icon.gif' alt='Hide this panel' border='0'>" CFTemplate="javascript:parent.document.body.cols = '0,*';"> --->
	</CF_RelayNavMenu> --->
	<ul>
		<cfoutput>
			<cfloop index="i" from="1" to="#ArrayLen(selectedElements)#" step="1">
				<li>
					<cfset thisSecurity = selectedElements[i].XmlAttributes.SECURITY>
					<!--- this line is just for backwards compatability with old xml files, can be removed once all have been updated --->
					<cfset thisSecurity = replaceNoCase (replaceNoCase(replaceNoCase(thisSecurity,"(7)",":Level3"),"(3)",":Level2"),"(1)",":Level1")>
					<!--- WAB 2008/09/30 changed from checkPermissions to checkSecurityList to handle securities with ANDs and ORs --->
					<cfset hasCorrectSecurity = application.com.login.checkSecurityList (thissecurity)>

				<cfif hasCorrectSecurity>
					<a href="#application.com.security.encryptURL(url = selectedElements[i].XmlAttributes.URL, singleSession = true)#" target="#URL.uniqueFrameID#" class="smallLink" title="#selectedElements[i].XmlAttributes.description#">#selectedElements[i].XmlAttributes.configToolName#</a>
			<!--- 2006-02-19 SWJ - Put the description into the href title field as some config lists were getting very long. --->
				<cfelse>
					#selectedElements[i].XmlAttributes.configToolName# <BR>(No rights)
				</cfif>
				</li>
			</cfloop>
		</cfoutput>
	</ul>
</div>