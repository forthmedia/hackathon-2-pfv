<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File name: qryCountryGroups		
Author:	Chris Snape		
Date created: 02-Mar-01	

	Objective - adds country groups to the list of country ids that a user has access to
		
	Rationale - For each country that the user has access to (as defined in the variable
	            countrylist) a check is made to see if the user has access to the region
				in which that country is located. In order to have access to a region a 
				user must have access rights to every country within that region
	
Date Tested:
Tested by:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed

Enhancement still to do:



--->

<CFIF IsDefined("CountryList")>

<CFQUERY NAME="getCountryGroups" DATASOURCE="#application.siteDataSource#">
	SELECT distinct cgr.countryGroupId
	  FROM country cou
	      ,countryGroup cgr
	 WHERE cou.isoCode IS NULL
	   AND cgr.countryGroupId = cou.countryId	   
	   AND cgr.countryMemberId  IN ( <cf_queryparam value="#CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	 UNION
	SELECT countryGroupId
	  FROM countryGroup   
	 WHERE countryGroupId = 0
</CFQUERY>

<CFSET CountryGroupList = valueList(getCountryGroups.countryGroupId)>

<CFQUERY NAME="getCountryMembers" DATASOURCE="#application.siteDataSource#">
	SELECT cou.countryId
		  ,cgr.countryGroupId
	  FROM country cou
          ,countryGroup cgr
	 WHERE cou.isoCode IS NOT NULL
       AND cgr.countryMemberId = cou.countryId 
       AND cgr.countryMemberId != cgr.countryGroupId	
	   AND cgr.countryGroupId  IN ( <cf_queryparam value="#CountryGroupList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
</CFQUERY>

<CFSET CountryMemberArray = #ArrayNew(2)#>

<CFLOOP QUERY="getCountryMembers">
	<CFSET CountryGroupIndex = #ListFind(CountryGroupList, CountryGroupId)#>
	<CFSET tmpIgnore = #ArrayAppend(CountryMemberArray[CountryGroupIndex], countryId)#>
</CFLOOP>	

<CFLOOP INDEX="CountryGroupIndex" FROM="1" TO=#ListLen(CountryGroupList)#>
	<CFSET CountryMemberList = #ArrayToList(CountryMemberArray[CountryGroupIndex])#>	
    <CFSET ValidMember = True>
	<CFLOOP INDEX="CountryMemberIndex" LIST=#CountryMemberList#>
 		<CFIF ListFind(CountryList, CountryMemberIndex) EQ 0>
			<CFSET ValidMember = False>
			<CFBREAK>	
		</CFIF>
	</CFLOOP>	
	<CFIF ValidMember>
		<CFSET CountryList = ListAppend(CountryList,ListGetAt(CountryGroupList,CountryGroupIndex))> 
	</CFIF> 
</CFLOOP>

</CFIF>
