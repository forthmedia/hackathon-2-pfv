<!--- �Relayware. All Rights Reserved 2014 --->
<!--- comes up with a list of all
		countries to which a User
		has at least read rights

USAGE:
		<CFINCLUDE TEMPLATE="../templates/qryGetBorders.cfm">
		qryGetBorders creates a variable BorderList

		  AND l.CountryID IN (#Variables.CountryList#)
--->

<!--- mods
WAB 2000-03-02 added stuff to return a comma separated list containing both the ids and the countrynames
 --->

<CFPARAM name="partnerCheck" default = "false">

<CFQUERY NAME="getBorders" datasource="#application.siteDataSource#">
SELECT distinct BorderSet
FROM         dbo.Borders 
</CFQUERY>

<CFSET BorderList = 0>

<CFIF getBorders.RecordCount GT 0>
	<CFIF getBorders.BorderSet IS NOT "">
		<!--- top record (or only record) is not null --->
		<CFSET BorderList = ValueList(getBorders.BorderSet)>		
	</CFIF>
</CFIF>

<!--- reset defaults (in case called more than once in a single page --->
<CFSET partnerCheck=false>
		