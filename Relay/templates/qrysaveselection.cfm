<!--- �Relayware. All Rights Reserved 2014 --->
<!---	saves record in Selections
		copies records from tempSelection tables
		deletes the records in tempSelections tables
		
		returns variable "searchID" with the new selection's ID
		
		A call to this template must have CFTRANSACTION around it 
		therefore always put a closing CFTRANSACTION before any CFABORTs
	
2004-09-21   WAB added columns to count locations and organisations to the selection table - these need to updated here

--->

<!--- use variable from calling template if it exists --->
<CFPARAM NAME="FreezeDate" DEFAULT="#request.requestTimeODBC#">


<CFQUERY NAME="saveSelectionHead" datasource="#application.siteDataSource#">
	INSERT INTO Selection (Title, RecordCount, SelectedCount, RefreshRate, Description, Tablename, CreatedBy, Created, LastUpdatedBy, LastUpdated)
	VALUES (<cf_queryparam value="#FORM.frmTitle#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#FORM.frmRecordCount#" CFSQLTYPE="CF_SQL_Integer" >,<cf_queryparam value="#FORM.frmSelectedCount#" CFSQLTYPE="CF_SQL_Integer" >,<cf_queryparam value="#form.frmRefresh#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="#FORM.frmDesc#" CFSQLTYPE="CF_SQL_VARCHAR" >,'Person',<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >,<cf_queryparam value="#Variables.FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >,<cf_queryparam value="#Variables.FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >)
</CFQUERY>

 <!--- assume Selection is locked so can get max --->
<CFQUERY NAME="getSelectionHead" datasource="#application.siteDataSource#">
	SELECT Max(SelectionID) AS newID
	  FROM Selection
	 WHERE Created = #CreateODBCDateTime(FreezeDate)#
</CFQUERY>

<CFIF #getSelectionHead.RecordCount# IS NOT 1>
	<CFSET message = "There has been a problem and your search could not be completed.  ">
	<CFSET message = message & "Please try again or <A HREF=" & "#AdminEmail#" & ">email us</A> explaining what you were trying to do.">
	<cfoutput>#application.com.relayUI.message(message="message",type="error")#</cfoutput>
	<!--- <CFINCLUDE TEMPLATE="message.cfm"> --->
	<CF_ABORT>
</CFIF>		

<CFSET searchID = getSelectionHead.newID>

<!--- Insert owner record --->
<CFQUERY NAME="saveSelectionOwner" datasource="#application.siteDataSource#">
	INSERT INTO SelectionGroup (SelectionID, UserGroupID, created, createdBy, lastupdated, lastupdatedby)
	VALUES (<cf_queryparam value="#searchID#" CFSQLTYPE="CF_SQL_INTEGER" >,<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >,<cf_queryparam value="#Variables.FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >,<cf_queryparam value="#Variables.FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >)
</CFQUERY>

<CFQUERY NAME="TemptoPermCriteria" datasource="#application.siteDataSource#">			
	INSERT INTO SelectionCriteria (SelectionID,SelectionField,Criteria,Criteria1,CreatedBy,Created,LastUpdatedBy,LastUpdated) 
	 SELECT <cf_queryparam value="#searchID#" CFSQLTYPE="CF_SQL_INTEGER" >,
	 		SelectionField,
			Criteria,
			Criteria1,
			<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >,
			<cf_queryparam value="#FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
			<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >,
			<cf_queryparam value="#FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
	 FROM TempSelectionCriteria
	 WHERE SelectionID = #request.relayCurrentUser.personid#
	   AND CreatedBy = #request.relayCurrentUser.usergroupid#
</CFQUERY>
		
<CFQUERY NAME="TemptoPermTag" datasource="#application.siteDataSource#">			
	INSERT INTO SelectionTag (SelectionID,EntityID,Status, CreatedBy,Created,LastUpdatedBy,LastUpdated) 
	 SELECT <cf_queryparam value="#searchID#" CFSQLTYPE="CF_SQL_INTEGER" >,
	 		EntityID,
			1,
			<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >,
			<cf_queryparam value="#FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
			<cf_queryparam value="#request.relayCurrentUser.usergroupid#" CFSQLTYPE="cf_sql_integer" >,
			<cf_queryparam value="#FreezeDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
	 FROM TempSelectionTag
	 WHERE SelectionID = #request.relayCurrentUser.personid#
	   AND CreatedBy = #request.relayCurrentUser.usergroupid#
</CFQUERY>

<!--- WAB 2004-09-21
update the counts of locations and organisations
rather hacked in here - all the other counts seem to have been passed into this template, but anyhow this was the easiest place to put it in

 --->

<cfset frmselectionid = #searchid#>
<cfinclude template = "qrySelectionRecordCounts.cfm">

<CFQUERY NAME="setSelectionHeader" datasource="#application.siteDataSource#">
	UPDATE Selection SET
		SelectedCountLoc =  <cf_queryparam value="#recordCounts.locations#" CFSQLTYPE="CF_SQL_Integer" > ,
		SelectedCountOrg =  <cf_queryparam value="#recordCounts.organisations#" CFSQLTYPE="CF_SQL_Integer" > 
	WHERE SelectionID =  <cf_queryparam value="#searchid#" CFSQLTYPE="CF_SQL_INTEGER" > 
</CFQUERY>


<!--- records stored with PersonID --->
<CFQUERY NAME="delTags" datasource="#application.siteDataSource#">
	DELETE FROM TempSelectionTag
	WHERE SelectionID = #request.relayCurrentUser.personid#
</CFQUERY>


<CFQUERY NAME="delCriteria" datasource="#application.siteDataSource#">
	DELETE FROM TempSelectionCriteria
	WHERE SelectionID = #request.relayCurrentUser.personid#
</CFQUERY>