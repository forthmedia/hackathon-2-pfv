<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			viewTableColsandData.cfm	
Author:				SWJ
Date started:		2003-01-19
	
Description:		This can be called from a URL link and will return either the 
					columns from the information Schema tables or the actual
					for a given table specified in tableName
					
Usage: 				<cfmodule template="../templates/viewTableColsandData.cfm"
						hideHeader="#hideHeader#"
						tableName="#tableName#"
						colList="#colList#"
						action="#action#"
						numRowsToReturn="#numRowsToReturn#"
						keyColumnList="#keyColumnList#"
						keyColumnURLList="#keyColumnURLList#"
					>

					
Parameters:			tableName - required
					colList Optional - if you specifiy a colList you can constrain 
					which columns are returned.
					dataSource - optional defaults to application.siteDataSource
					action - optional 
					numRowsToReturn - optional defaults to all but can be supressed
					whereClause - optional if you provide a valid where clause 
					it will constrain the rows by the where clause

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2015-11-12          ACPK        Replaced table at top with bootstrap; Improved code used to assign oddrow/evenrow classes to table rows

Possible enhancements:


 --->

<cfparam name="attributes.hideHeader" type="boolean" default="no"> <!--- by default we show the header section --->
<cfparam name="attributes.tableName" default="">
<cfparam name="attributes.colList" default="">
<cfparam name="attributes.whereClause" default="">
<cfparam name="attributes.action" default="viewCols">
<cfparam name="datasource" default="#application.siteDataSource#">
<cfparam name="attributes.numRowsToReturn" default="20">
<cfparam name="attributes.keyColumnList" type="string" default=""><!--- this can contain a list of columns that can be edited --->
<cfparam name="attributes.keyColumnURLList" type="string" default=""><!--- this can contain a list of matching columns that contain the URL of the editor template --->
<cfparam name="attributes.keyColumnKeyList" type="string" default=""><!--- this can contain a list of matching key columns e.g. primary key --->

<cfif attributes.tableName eq "">
	<cfif isDefined("tableName")><cfset attributes.tablename = tablename></cfif>
</cfif>

<cfif attributes.action eq "viewCols">
	<cfif isDefined("action")><cfset attributes.action = action></cfif>
</cfif>

<cfif attributes.numRowsToReturn eq "">
	<cfif isDefined("numRowsToReturn")><cfset attributes.numRowsToReturn = numRowsToReturn></cfif>
</cfif>

<CFSCRIPT>
// Create an instance of the object
myObject = createObject("component","relay.com.dbInterface");
// set tablename
myObject.dataSource = datasource;
myObject.tableName = attributes.tableName;
myObject.colList = attributes.colList;
myObject.whereClause = attributes.whereClause;
myObject.numRowsToReturn = attributes.numRowsToReturn;
// get the details
if(attributes.action eq "viewCols") {
	myObject.colList = "column_name,data_Type,character_maximum_length,is_nullable,column_default,ordinal_position";
	myObject.getAllCols();
	thisData = myObject.qGetAllCols; // set variable this data to the query object returned
	headingText = "Column details"; // set the headingText variable
} else {
	myObject.getAllRows();	
	thisData = myObject.qGetAllRows; // set variable this data to the query object returned
	headingText = "Data"; // set the headingText variable
}
</CFSCRIPT>

<cfif attributes.hideHeader eq "no">
    <!--- 2015-11-12    ACPK    Replaced table with bootstrap --->
	<cfoutput>
	   <p>#htmleditformat(headingText)# for #htmleditformat(attributes.tableName)#</p>
	   <p><cfif attributes.action eq "viewCols">
			<a href="/templates/viewTableColsandData.cfm?action=viewData&tablename=#attributes.tableName#&numRowsToReturn=20" class="Submenu">View Top 20 Rows</a>&nbsp;|&nbsp;
		</cfif>
		<a href="javascript:history.back()" class="Submenu">Back</a></p>
	</cfoutput>
</cfif>

<TABLE CLASS="table table-hover">
	<tr>
	<cfoutput>
		<cfloop index="i" list="#myObject.displayCols#">
			<th nowrap><cfif i eq myObject.uniqueKey>#UCASE(replace(i,"_"," ","ALL"))#<SMALL>(key)</SMALL><cfelse>#UCASE(replace(i,"_"," ","ALL"))#</cfif></th> 
		</cfloop>
	</cfoutput>
	</tr>

	<cfoutput query="thisData">
		<!--- 2015-11-12  ACPK    Improved code used to assign oddrow/evenrow classes --->
		<cfif currentrow mod 2 is not 0>
	        <cfset rowclass="oddrow">
	    <cfelse>
	        <cfset rowclass="evenrow">
	    </cfif>
		<TR class="#rowclass#"> 
			<cfloop index="i" list="#myObject.displayCols#">
				<td>
					<cfif isDefined("attributes.keyColumnList") and isDefined("attributes.keyColumnURLList") 
					and (listLen(attributes.keyColumnList) eq listLen(attributes.keyColumnURLList))
					and listFindNoCase(attributes.keyColumnList,i) neq 0>
					    <CFSET thiskeyColumnURL=gettoken(attributes.keyColumnURLList,listFindNoCase(attributes.keyColumnList,i),",")>
					    <CFSET thiskeyColumnKey=gettoken(attributes.keyColumnKeyList,listFindNoCase(attributes.keyColumnList,i),",")>
					    <a href="#thiskeyColumnURL##evaluate(thiskeyColumnKey)#">#htmleditformat(evaluate(i))#</a>
					<cfelse>
						#htmleditformat(evaluate(i))#
					</cfif>
				</td>
			</cfloop>
		</tr>
	</cfoutput>
</table>

