<!--- �Relayware. All Rights Reserved 2014 --->
<!-- frames -->

<!--- NJH 2014/10/14 Task Core 834 a bit of a hack for an IE 11 problem where a page with framesets. For some reason, moving away from a page that had framesets
	caused the page to not be displayed when returning to it. Only after changing the height of the div containing the iframe which contained
	the frameset caused the page to display again. This can be removed once we get rid of the framesets.
	 --->
<!---
NJH 2015/02/10 Jira task fifteen-133 - moved this out of here into request.cfc, so that we can deal with all framesets where this might be a problem
<script>
	var pid = jQuery(window.frameElement).parent().parent().parent().attr('id');

	jQuery('#relayTabPanel__'+pid,parent.document).click(function() {
		var parentHeight = jQuery(window.frameElement).parent().height();
		jQuery(window.frameElement).parent().height(parentHeight-1);
		setTimeout(function() {jQuery(window.frameElement).parent().height(parentHeight);},100); <!--- resetting the height back to the original --->
	});
</script> --->

<cfset uniqueFrameID="configArea" & createUUID()>
<cfoutput>
<FRAMESET COLS="300,*">
    <frame src="/templates/configList.cfm?#request.query_string#&uniqueFrameID=#uniqueFrameID#" name="configList" frameborder="0" scrolling="Auto" marginwidth="0" marginheight="0">
    <frame src="/templates/blank.html" name="#uniqueFrameID#" frameborder="0" scrolling="Auto" marginwidth="0" marginheight="0">
</FRAMESET>
</cfoutput>