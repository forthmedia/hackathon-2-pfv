<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
	WAB June 2006
	
	A template included by redirector to display a message

	replaces/develops from partners\partnermessage.cfm

 --->



<cfparam name="message" default = "">
<cfparam name="headline" default = "">


<CF_DISPLAYBORDER borderset="#request.currentsite.borderset#" >

<CFOUTPUT>
<TABLE class="messageTable" border="1" cellpadding="5" cellspacing="0" width="500" height=100>
	<cfif headline neq "">
	<Tr>
		<Th >#htmleditformat(headline)#&nbsp;
		</Th>
	</tr>
	</cfif>
	<TR>
		<TD >
				<cfif message neq "">#application.com.relayUI.message(message=message,showClose=false)#</cfif>
				<!--- WAB added this parameter so that it was possible to have a second line
					this was useful for me so that I could have a translation in #message#, but a hardcoded link in messageLine2 which translators couldn't mess up
				 --->
				<cfif isDefined("messageLine2")>
					<cfset message_html = application.com.relayTranslations.checkForAndEvaluateCFVariables(messageLine2)>
							#message_html# 
				</cfif>
		</TD>
	</TR>
	
	

	<cfif isDefined("request.processAction") and request.processAction.continueButton.show>
	<TR>
		<TD >
			<FORM action="/proc.cfm" method="POST" name="mainForm" target="_self">
				<CF_INPUT type="HIDDEN" name="frmProcessID" value="#request.processAction.processid#|#request.processAction.nextStepid#">
				<CF_INPUT type="HIDDEN" name="frmProcessUUID" value="#request.processAction.UUID#">
				
<!--- 				<CFIF isdefined("Form.fieldnames")>
					<CFLOOP index="fieldname" list="#Form.fieldnames#">
						<CFIF listfindnocase("frmProcessID,frmstepid,frmContinue,frmCancel,frmProcessUUID", fieldname ) is 0>
							<INPUT type="hidden" name="#fieldname#" value="#Evaluate(fieldname)#">
						</cfif>	
					</CFLOOP>
				</CFIF>
 --->
					<cfif request.processAction.cancelButton.show>
						<CF_INPUT class="submitbutton"  type="submit" name="frmCancel" value="#request.processAction.cancelButton.Label#">
					</cfif>

					<CF_INPUT class="submitbutton"  type="submit" name="frmContinue" value="#request.processAction.continueButton.Label#">
			
			</FORM>
	</td>
	</tr>
	</cfif>	

</TABLE>

</CFOUTPUT>
</CF_DISPLAYBORDER>


