<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			RegionFilter.cfm
Author:				KAP
Date started:		14-Aug-2003
	
Description:			

Product group filter called by forecastSelector.cfm

Requires an input query (currently called myForecast.qProductGroupData)

It uses its own form, so there's a JavaScript example of migrating the selected product groups into a master form (onsubmit event in forecastSelector.cfm).

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
14-Aug-2003			KAP			Initial version

Possible enhancements:


--->

<form action="" name="frmProductGroup">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<cfloop query="myForecast.qProductGroupData">
		<tr class="<cfoutput>#iif( BitAnd( myForecast.qProductGroupData.CurrentRow - 1, 1 ) eq 0, de( "oddrow" ), de( "evenrow" ) )#</cfoutput>">
			<td valign="top" class="smaller"><CF_INPUT type="checkbox" name="frmProductGroupID" value="#myForecast.qProductGroupData.productgroupid#" checked></td>
			<td valign="top" class="smaller"><cfoutput>#htmleditformat(myForecast.qProductGroupData.description)#</cfoutput></td>
		</tr>
	</cfloop>
	</table>
</form>

