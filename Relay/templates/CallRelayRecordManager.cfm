<!--- �Relayware. All Rights Reserved 2014 

2016-06-27	WAB		PROD2016-1334 callRelayRecordManager security.  Table must be encrypted
--->

<cfparam name="table" type="string" default="person">
<cfparam name="screenTitle" type="string" default=" ">
<cfparam name="datasource" type="string" default="#application.siteDataSource#">
<cfparam name="onDelete" type="string" default="">

<cf_checkFieldEncryption fieldnames="table">

<!--- 2014-10-15	RPW	Core-829 The fund manager screens do not have screen titles so a user will not instinctively know what screen they are on --->
<cfscript>
	if (application.com.relayTranslations.doesPhraseTextIDExist("Sys_#table#")) {
		variables.pageTitle = "Phr_Sys_#table#";
	} else {
		variables.pageTitle = table;
	}
</cfscript>	

<CF_RelayNavMenu pagetitle="Phr_Sys_Managedatafor #variables.pageTitle#">
	<!--- <CF_RelayNavMenuItem MenuItemText="Phr_Sys_Back" CFTemplate="JavaScript:history.back();"> --->
</CF_RelayNavMenu>

<cfscript>
	//2014-09-10	RPW		Add Lead Screen on Portal
	//Supports multiple conditions in the queryParams array.
	param name="variables.queryParams" default=[];

	if (IsDefined("URL.table") && IsDefined("URL.fieldName") && IsDefined("URL.fieldValue")) {

		variables.tmpStruct = {};
		StructInsert(variables.tmpStruct,"fieldName",URL.fieldName);
		StructInsert(variables.tmpStruct,"fieldValue",URL.fieldValue);
		ArrayAppend(variables.queryParams,tmpStruct);

	}

</cfscript>

<CF_RelayRecordManager
	DATASOURCE="#datasource#"
	TABLE="#table#"
	screenTitle="#screenTitle#"
	onDelete="#onDelete#"
	queryParams="#variables.queryParams#"
>