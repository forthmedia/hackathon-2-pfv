<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		changePWForm.cfm
Author:			NJH
Date started:		2010/06/25

Description:		This file is a form used for updating/changing one's password. It is used
					for the portal (templates/resendPassword.cfm) and for the internal (home/changePass.cfm).
					It was an attempt to factorise all the code.

Usage:

Amendment History:

Date (YYYY-MM-DD)	Initials 	What was changed


Enhancements still to do:


 --->

<cf_includeJavascriptOnce template="/javascript/login.js" translate="true">


<cfparam name="message" type="string" default="">

<!--- <script>
	function isInternalValidate () {
		testForm = validate ();
		if (testForm) {
			document.LoginForm.submit();
		}
	}

	function validate () {
		form=$('LoginForm')
		return !validatePassword()) {
			return false;
		} else {
			return true;
		}
	}
</script> --->

<cfoutput>
<cfif not isDefined("outputForm") or (isDefined("outputForm") and outputForm)>
<form name="LoginForm" id="LoginForm" action="" method="post" target="_self" onSubmit="return validatePassword();">
</cfif>
<cf_relayFormDisplay>
	<cfset request.relayForm.useRelayValidation = true>

	<cfif message neq "">#application.com.security.sanitisehtml(message)#<br /></cfif>
	<p>Phr_CP_Please_Enter_Your_Password_Below</p>

	<cfif request.relayCurrentUser.isInternal and request.relayCurrentUser.isLoggedIn>
		<cf_encryptHiddenFields>
			<cfoutput>
				<CF_INPUT type="hidden" name="personID" value="#request.relayCurrentUser.personID#">
			</cfoutput>
		</cf_encryptHiddenFields>
		<div class="form-group">
			<div class="col-xs-12 col-sm-3 control-label required">
				<label for="oldPassword">phr_login_currentPassword</label>
			<div class="col-xs-12 col-sm-9"></div>
				<input type="password" name="oldPassword" size="25" maxlength="50" value="" id="oldPassword" class="password form-control">
			</div>
		</div>
	<cfelse>
		<!--- <div class="form-group">
			<div class="col-xs-12 col-sm-3 control-label required">
				<label for="#htmleditformat(displayDetail)#" class="required">phr_#htmleditformat(displayDetail)#</label>
			</div><div class="col-xs-12 col-sm-9">
				<div id="#htmleditformat(displayDetail)#" class="form-control">#Evaluate("checkLinkDetails.#htmleditformat(displayDetail)#")#</div>
			</div>
		</div> --->
		<Cf_relayFormElementDisplay type="html" currentValue="#checkLinkDetails[displayDetail]#" label="phr_#htmleditformat(displayDetail)#">
		<cf_encryptHiddenFields>
				<CF_INPUT type="hidden" name="personID" value="#link.personId#">
				<CF_INPUT type="hidden" name="oldPassword" value="#link.Password#">
		</cf_encryptHiddenFields>
	</cfif>

	<cf_relayFormElementDisplay type="password" class="password form-control" fieldname="frmNewPassword1" currentValue="" label="phr_login_newPassword" required="true" AUTOCOMPLETE="off" oncopy="return false" onKeyUp="validatePasswordStrength(this)">
	<!--- <div class="form-group">
		<div class="col-xs-12 col-sm-3 control-label required">
			<label for="frmNewPassword1" class="required">phr_login_newPassword</label>
		</div><div class="col-xs-12 col-sm-9">
			<INPUT TYPE="PASSWORD" class="password form-control" ID="frmNewPassword1" NAME="frmNewPassword1" VALUE="" SIZE="25" MAXLENGTH="50" AUTOCOMPLETE="off" oncopy="return false" onKeyUp="validatePasswordStrength(this)">
		</div>
	</div> --->

	<cf_relayFormElementDisplay type="password" class="password form-control" fieldname="frmNewPassword2" currentValue="" label="phr_login_repeatNewPassword" required="true" AUTOCOMPLETE="off">
	<!--- <div class="form-group">
		<div class="col-xs-12 col-sm-3 control-label required">
			<label for="frmNewPassword2" class="required">phr_login_repeatNewPassword</label>
		</div><div class="col-xs-12 col-sm-9">
			<INPUT TYPE="PASSWORD" class="password form-control" ID="frmNewPassword2" NAME="frmNewPassword2" VALUE="" SIZE="25" MAXLENGTH="50" AUTOCOMPLETE="off">
		</div>
	</div> --->

	<div class="form-group">
		<div class="col-xs-12 col-sm-3 control-label required">
			<input type="hidden" name="action" value="setPassword">
		</div><div class="col-xs-12 col-sm-9">
			<div id="passwordStrengthIndicator">#application.com.login.checkPasswordComplexity(password="").fullDescription#</div>
		</div>
	</div>

	<cf_relayFormElementDisplay type="submit" currentValue="phr_login_ChangePassword" class="btn btn-primary submitbutton">
	</cf_relayFormDisplay>
<cfif not isDefined("outputForm") or (isDefined("outputForm") and outputForm)>
</FORM>
</cfif>
</cfoutput>
