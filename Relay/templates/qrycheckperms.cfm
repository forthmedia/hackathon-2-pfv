<!--- �Relayware. All Rights Reserved 2014 --->
<!--- assume that there is a variable

	 securitylist which is SecurityTask:min permission
	 
	 WHERE min permission {read,update,add}
--->

<!--- Mods:
2001-05-11	WAB:  Added check for cookie.user being defined
2000-11-10 WAB: Altered code so that the second part of security list can be passed in using the names defined in permissionDefinition table
2000-09-29 WAB: Added dynamic generation of column names from the permissions defined in the permissionsDefinition Table
1999-11-26 WAB: Added "Levels" for more generic uses
1999-11-26 WAB: If parameter "partnercheck" is set, then the query is based entirely on usergroupid and not the personid
				Allows rights to be assigned to a single UserGroup to cover all partner log ins

 --->
 
<CFPARAM name="continue" default = "no">
<CFPARAM name="OverRide" default = "no">
<CFPARAM name="partnerCheck" default = "false">

<cfif not isdefined ("cookie.user")>
	<cfoutput>Sorry, you must be logged in to access this site  <BR>
				(qrycheckperms)
	</cfoutput>
	<CF_ABORT>
</cfif>


<CFIF IsDefined("checkPermission.RecordCount") AND OverRide EQ "no">
	<!--- if this query has already been run, then
	      run the query but don't analyse it --->
	<CFSET continue = "yes">		  

</CFIF>

<!--- 2000-09-29 WAB added code so that the permission query brings back column names which are the same as the definitions in th
Permissiondefinition Table
Unfortunately view and add are keywords!
--->

<cfset checkPermission = application.com.login.checkInternalPermissions (ListFirst(securitylist,":"),"",ListRest(securitylist,":"))>   

<!--- 
These queries now replaced with the session variables as defined above

 
<CFQUERY NAME="getPermissions" datasource="#application.siteDataSource#">
	Select PermissionLevel, 
		PermissionName ,
		CASE WHEN permissionName in ('view','add') THEN '_' + permissionName ELSE permissionName END as ModifiedPermissionName , <!--- unfortunately view and add are SQL reserved words so can't be used as column names later on --->
		CASE WHEN permissionName = '#ListRest(securitylist,":")#' THEN PermissionLevel ELSE 1 END AS MinPermissionLevelRequired
		from PermissionDefinition
	where SecurityTypeID = (SELECT c.SecurityTypeID FROM SecurityType AS c
	         					WHERE c.ShortName = '#ListFirst(securitylist,":")#')
	order by MinPermissionLevelRequired desc  <!--- this puts the minRequired one at the top so that I can reference it as #getPermissions.minPermissionLevelRequired# --->
</CFQUERY>


<CFQUERY NAME="checkPermission" datasource="#application.siteDataSource#">
SELECT r.SecurityTypeID,
	<CFLOOP query="getPermissions">convert(bit,sum(r.permission & #evaluate("2 ^ (#permissionLevel#-1)")#)) AS #replace(ModifiedPermissionName," ","","ALL")#,
	</cfloop>
	Convert(bit,sum(r.permission & 8)) AS Level4,
	Convert(bit,sum(r.permission & 4)) AS Level3,
	Convert(bit,sum(r.permission & 2)) AS Level2,
	Convert(bit,sum(r.permission & 1)) AS Level1,
	Convert(bit,sum(r.permission & 4)) AS AddOkay,
	Convert(bit,sum(r.permission & 2)) AS UpdateOkay
	FROM Rights AS r, RightsGroup AS rg
	WHERE r.SecurityTypeID = (SELECT c.SecurityTypeID FROM SecurityType AS c
         					WHERE c.ShortName = '#ListFirst(securitylist,":")#')
	AND r.UserGroupID = rg.UserGroupID
	<CFIF not PartnerCheck>
		AND rg.PersonID = #request.relayCurrentUser.personid#
	<CFELSE>
		AND rg.PersonID = (select personid from usergroup where usergroupid = #request.relayCurrentUser.usergroupid#)
	</CFIF>
	GROUP BY r.SecurityTypeID
	HAVING Sum(CONVERT(int,r.Permission/(power(2, (#getPermissions.minPermissionLevelRequired#-1)))) % 2) > 0
</CFQUERY>
--->


<CFIF checkPermission.RecordCount IS 0 AND continue IS "no">
	<CFSET message = "Sorry, but you do not have permission to access this function.">
	<cfoutput>#application.com.relayUI.message(message=message,type="warning")#</cfoutput>
	<!--- <cfinclude template="/templates/message.cfm"> --->
	<CF_ABORT>
</CFIF>

	<!--- reset defaults (in case called more than once in a single page --->
	<CFSET partnerCheck=false>
	
	<cfscript>
		//an exception in case of read rights for external users
		if( not compareNoCase(listLast(securitylist,":"), "read")){
			securitylist = listSetAt(securitylist, 2, "view", ":");
		}
		
		permissionGranted = application.com.login.checkInternalPermissions(listFirst(securitylist,":"),listLast(securitylist,":"));
	</cfscript>
	<CFIF not permissionGranted>
		<CFSET message = "Sorry, but you do not have permission to access this function.">
		<cfoutput>#application.com.relayUI.message(message=message,type="warning")#</cfoutput>
		<!--- <cfinclude template="/templates/message.cfm"> --->
		<CF_ABORT>
	</CFIF>
