<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		CustomerContacts.cfm	
Author:			NJH  
Date started:	14-05-2010
	
Description:	Wrapper file for lead manager customer contacts		

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->

<!--- 
<cfinclude template="/leadmanager/opportunityParams.cfm">
<cf_include template="\code\cftemplates\opportunityINI.cfm" checkIfExists="true">

<cfif fileexists("#application.paths.code#\cftemplates\opportunityINI.cfm")>
	<cfinclude template="/code/cftemplates/opportunityINI.cfm">
</cfif> --->

<cfinclude template="/leadmanager/customerContacts.cfm">