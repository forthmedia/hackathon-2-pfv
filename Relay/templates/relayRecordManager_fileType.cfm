<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			relayRecordManager_fileType.cfm	
Author:				WAB
Date started:			2009/07/13
	
Description:			

File to run after a file type update
Specifically for moving the location of files if the secureFile setting has been changed.

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
WAB 2011/11/14		after some work tidying up fileManager.cfc I changed the name of the function being caled

Possible enhancements:


 --->
 
 <cfif isdefined("pkstruct")>  <!--- WAB Added 2009/09/15 only defined for edits and deletes --->
	 <cfsetting requestTimeOut="600"> <!--- can take a very long time if zip files are involved --->
	 <cfset application.com.filemanager.checkFileInCorrectLocationSecureOrNot(fileTypeID = pkStruct.fileTypeID)>  <!--- NJH 2010/07/28 changed from createStubsForType to createOrRemoveSecureFile --->
</cfif>	 

 
 

