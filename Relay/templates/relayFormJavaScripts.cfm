<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			relayFormJavaScripts.cfm
Author:				SWJ
Date started:		08 March 2004

Description:		This calls a standard set of validation javascripts that are used with the
					relayFormField custom tags

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2004-10-20		WAB added an if around to prevent multiple loading
2011-11-28	NYB	LHID8224 changed to call in limitTextArea.cfm instead of limitTextArea.js - to translate js
2012-09-21	PPB	Case 430402 moved script to call restrictKeyStrokes.js outside the CFHTMLHEAD to use cf_includeJavascriptOnce to allow translations
2016-03-02	WAB	Add rwFormValidation.js because relayEditForm now has a dependency on it
Possible enhancements:


 --->


<cfparam name="request.relayFormJavaScriptsLoaded"  default= "no">

<cfif not request.relayFormJavaScriptsLoaded>

	<cf_includeJavascriptOnce template="/javascript/relayEditForm.js">
	<cf_includeJavascriptOnce template="/javascript/rwFormValidation.js" translate="true">
	<!---<cf_includeJavascriptOnce template="/javascript/AnchorPosition.js">--->

	<!--- NJH 2012/02/16 CASE 425074 include js file --->
	<cf_includeJavascriptOnce template="/javascript/limitTextArea.js" translate="true">
	<cf_includeJavascriptOnce template="/javascript/restrictKeyStrokes.js" translate="true">	<!--- 2012-09-21 PPB Case 430402 moved to use cf_includeJavascriptOnce to allow translations in restrictKeyStrokes.js --->

	<cfset request.relayFormJavaScriptsLoaded = "yes">
</cfif>


