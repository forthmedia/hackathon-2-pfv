<!--- �Relayware. All Rights Reserved 2014 --->
<!---

2012-02-03		WAB		Sprint Menu Changes.  Rationalise menu XML code.  Client extensions dealt with automatically.
2015-11-22		SB		Bootstrap
 --->
<cf_includeJavascriptOnce template = "/javascript/myRelay.js" translate="true">


<cfparam name="subMenuNavItem" type="string" default="reports">
<cfparam name="module" type="string" default="DataTools">

<!--- a dummy module to show my favourite reports --->
<cfif module neq "myFavourites">

	<cfset qrySubMenuNavItem = application.com.relayMenu.getSubMenuNavItems(module=module,subMenuNavItem=subMenuNavItem)>

	<!--- for backwards compatibility as some reports are still in reportList.xml--->
	<cfif subMenuNavItem eq "reports">
		<CFSET FileLocation=replace("#application.path#\xml\#subMenuNavItem#List.xml","/","\")>

		<CFIF not fileExists("#FileLocation#")>
			<CFSET AttributesError="No ReportList.xml file found; looking for #FileLocation#">
		</CFIF>

		<cftry>
			<cffile action="read"
		 			file="#application.paths.relayware#\xml\ReportList.xml"
		 			variable="myxml">
			<cfcatch type="Any">XML file not found</cfcatch>
		</cftry>
		<cfscript>
		  	myxmldoc = XmlParse(myxml);
		  	selectedElements = XmlSearch(myxmldoc, "//reportGroup[translate(@name,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='#lcase(module)#']/report");
		  	menuXML = StructNew();
		</cfscript>

		<cfset genericReports = queryNew("url,grouping,name","varchar,varchar,varchar")>
		<cfif arrayLen(selectedElements)>
			<cfloop array="#selectedElements#" index="report">
				<cfset queryAddRow(genericReports)>
				<cfset querySetCell(genericReports,"url","/report/reportXMLGeneric.cfm?reportName=#report.XmlAttributes.Name#")>
				<cfset querySetCell(genericReports,"name",report.XmlAttributes.Title)>
				<cfset querySetCell(genericReports,"grouping",module)>
			</cfloop>
		</cfif>

		<!--- User reports --->
		<cfset qryClientReports = application.com.relayReports.getClientReports(module=module)>

		<!--- Jasper Reports --->
		<cfset qryJasperReports = queryNew("label,ReportURI,grouping","varchar,varchar,varchar")>
		<cfif application.com.relayMenu.isModuleActive(moduleID="ReportManager")>
			<cfset qryJasperReports = application.com.jasperServer.getReportsQuery(module=module)>
		</cfif>

		<!--- NJH 2007/09/27 if we have client specific client reports, append them to the main reports --->
		<cfquery name="qryReports" dbtype="query">
			select name, url as url,grouping,'report' as linkType from qrySubMenuNavItem
			union
			<cfif qryClientReports.recordCount>
			select name, url,grouping,'report' as linkType from qryClientReports
			union
			</cfif>
			<cfif qryJasperReports.recordCount gt 0>
			select label as name, ReportURI as url,grouping,'jasperReport' as linkType from qryJasperReports
			union
			</cfif>
			select name,url,grouping,'report' as linkType from genericReports
			order by grouping
		</cfquery>
	</cfif>

	<cfset moduleName = "phr_mod_#htmleditformat(module)#">
<cfelse>
	<cfset qryReports = application.com.myrelay.getMyLinks(personID=request.relayCurrentUser.personID)>

	<cfquery name="qryReports" dbtype="query">
		select linkAttribute1 as grouping, linkDescription as name, linkLocation as URL, linkType, [module]
		from qryReports
	</cfquery>

	<cfset moduleName = "My Favourites">
</cfif>

<cf_includejavascriptOnce template = "/javascript/extExtension.js">
<cfset application.com.request.setDocumentH1(text="#moduleName# #application.com.regExp.camelCaseToSpaces(string=SubMenuNavItem)#")>

<div>
	<ul id="reports">
	<cfoutput query="qryReports" group="grouping">
		<li>
			<h4>
				<cfif moduleName eq "My Favourites" and module neq "">
					#htmlEditFormat("phr_mod_#module#")#<cfif grouping neq "">:</cfif>
				</cfif>
				<cfif grouping neq "">#htmleditformat(grouping)#<cfelseif moduleName neq "My Favourites">#htmleditformat(moduleName)#</cfif>
			</h4>
		</li>
		<cfoutput>
			<li class="reportsList">
				<cfset application.com.myRelay.displayLink(linklocation=URL,linkName=Name,linkAttribute1=grouping,linkType=linkType,module=module)>
			</li>
		</cfoutput>
		<!---SB Not sure what these are for, the generate blank list items? <li>&nbsp;</li> --->
	</cfoutput>
	<!--- SB Not sure what these are for, the generate blank list items? <li>&nbsp;</li> --->
</ul>
</div>