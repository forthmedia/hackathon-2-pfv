<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			recentSearches.cfm
Author:				RPW
Date started:		2014-08-21

Description:


Usage:

Amendment History:

Date (YYYY-MM-DD)	Initials 	What was changed


Enhancements still to do:


 --->

<cf_includeJavascriptOnce template = "/javascript/recentSearches.js">
<cf_head>
<link rel="stylesheet" href="/styles/masthead.css">
</cf_head>

<div id="homePageSearchResultsContainer">
	<div id="siteSearchResults" style="position:absolute;height:17em;overflow:auto;width:100%;top:0;left:0em;">
		<div id="siteSearchResultsClose" class="right">
			<a href="" onclick="jQuery('#siteSearchResults').hide();toggleRecentSearches('show');return false;"> x</a>
		</div>
		<div id="siteSearchResultsInner" onClick="toggleRecentSearches('show');"></div>
	</div>
	<div id="recentSearchs">

		<h2><span class="fa fa-search"></span>Phr_Sys_RecentSearches</h2>
		<form id="siteSearchForm">
			<input type="hidden" onblur="this.value=(this.value=='') ? 'Search' : this.value;" onfocus="this.value=(this.value=='Search') ? '' : this.value;" value="Search" name="searchTextString" class="searchBox" maxlength="50">
		</form>
		<div id="recentSearchList"></div>
	</div>
</div>