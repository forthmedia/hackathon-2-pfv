<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		subFrame.cfm
Author:			SWJ
Date created:	2002-02-02

	Objective - To provide a completely generic way of navigating the application.
				It works in close conjunction with subMenuBar.cfm and subHome.cfm
				calling both and passing the module parameter in the query string.

	Syntax	  -	This is called from the lMenuXML.cfm. and is expecting a URL
				query string.

	Parameters - module - this is passed in the query string and if it is not passed
				then there will be problems.


Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Enhancement still to do:

1. Handle the error of not being passed a query string containing module

--->
<!--- convert URL params to form variables --->

<CFIF isStruct(url) and NOT IsDefined("fieldNames")>
<!--- if a URL string is Defined (tested using isStruct) --->
	<CFSET Fields="">
	<CFLOOP COLLECTION="#URL#" ITEM="VarName">
		<!--- loop through the URL collection and create form variables for each url param --->
		<cfset "form.#VarName#" = #URL[VarName]#>
		<CFSET Fields = Fields & "#VarName#" & ",">
		<!--- set the fieldnames variable to a list of url 'fields' --->
		<cfset fieldNames = fields>
	</cfloop>
</cfif>
<CF_FormFieldsURLString>

<cfoutput>

<cfparam name="RelaySiteNamePrefix" default="Internal">
<cfparam name="RelayMainFrameName" default="Main Frame">
<!--- If the site is a dev site (testSite = 1) or a dev Site (testSite = 2) override RelaySiteNamePrefix --->
<cfif application.testSite eq 2>
	<cfset RelaySiteNamePrefix = "Dev Int">
<cfelseif application.testSite eq 1>
	<cfset RelaySiteNamePrefix = "Test Int">
</cfif>

<cfif structKeyExists(URL,"module")>
	<cfset RelayMainFrameName = URL.module>
</cfif>



<cf_head>
<cf_title>#request.CurrentSite.Title# | #RelaySiteNamePrefix# | #RelayMainFrameName#</cf_title>
</cf_head>
	<FRAMESET ROWS="35px,*" FRAMESPACING="0" FRAMEBORDER="0">
	    <frame src="/templates/subMenuBar.cfm?#request.query_string#" name="Menu" frameborder="0" scrolling="No" marginwidth="0" marginheight="0">
	    <CFIF isDefined("mainSubTemplate")>
			<frame src="/#mainSubTemplate#?#request.query_string#" name="mainSub" frameborder="0" scrolling="Auto" marginwidth="0" marginheight="0">
		<cfelse>
			<frame src="/templates/subHome.cfm?#request.query_string#" name="mainSub" frameborder="0" scrolling="Auto" marginwidth="0" marginheight="0">
		</CFIF>
	</FRAMESET>
</cfoutput>
