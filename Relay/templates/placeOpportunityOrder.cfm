<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		placeOpportunityOrder.cfm	
Author:			NJH  
Date started:	26-04-2010
	
Description:	A page to capture what happens when a partner places an opportunity order		

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
10-06-2010			PPB			this template allows the capture of shipping information (or displays distributor information for tier2 users) and then allows submission of credit-card details as required

Possible enhancements:


 --->

<cfif not request.relayCurrentUser.isInternal and not application.com.security.confirmFieldsHaveBeenEncrypted(fieldNames="opportunityID")>
     <CF_ABORT>
</cfif>

<cfparam name="opportunityID" type="numeric">

<cfset request.relayFormDisplayStyle = "HTML-table">
<cfset tier2=false>

<cfset opportunityDetails = application.com.opportunity.getOpportunityDetails(opportunityID=opportunityID)>

<!--- if the partner is a tier2 partner and the distributor is not set to online ordering, show link to distributor website --->
<cfif opportunityDetails.partnerSalesPersonID neq 0 and application.com.flag.isFlagSetForPerson(flagId="Tier1Tier2DistributorTier2",personId=opportunityDetails.partnerSalesPersonID)>
	<cfset tier2=true>
	
	<!--- we shouldn't be getting to this spot, as a tier 2 reseller should have a disit set...but set the disti to 0 if non set.
		I think the bug was thrown by a data issue...
	 --->
	<cfif opportunityDetails.distiSalesPersonID eq "">
		<cfset opportunityDetails.distiSalesPersonID = 0>
	</cfif>

	<cfif not structKeyExists(form,"nextStep")>		<!--- don't need to this again after we've just submitted the shipping or the ccpayment forms  --->
		<!--- if there isn't a pricebook for the distributor then get the name & website url of the distributor so that we can display a message for the user to go and order directly --->	
		<cfset qryDistiPricebook = application.com.opportunity.getPricebook(currency='#opportunityDetails.currency#',countryID=#opportunityDetails.countryID#,organisationID=#opportunityDetails.distiLocationID#)>
		<cfif qryDistiPricebook.recordcount eq 0>
			<cfquery name="getDistributorDetails" datasource="#application.siteDataSource#">
				select orgUrl, organisationName from organisation where organisationID =  <cf_queryparam value="#opportunityDetails.distiLocationID#" CFSQLTYPE="CF_SQL_INTEGER" > 
			</cfquery>
		</cfif>
	</cfif>
</cfif>

		<script>
			function closePopUp() {
				window.opener.submitOrder();
		        this.close();
		    }
		</script>
	
	<!--- HOW IT WORKS:
	when the page is first loaded form.nextStep does not exist. Firstly  we check to see if we need to display a message telling a tier2 partner to go to their distributor and then proceed to capture shipping info; 
	when the shipping info is submitted, form.nextStep = 'submitShipping' so we want the page to submit the Shipping Info and display the CC payment entry form ;
	when the cc payment is submitted, form.nextStep = 'submitOrder' (sent into payment gateway and returned as a form field) and so we exit;
	--->

	<cfset continueToNextStep = true>					
		
	<cfif not structKeyExists(form,"nextStep")>				<!--- don't need to this again after we've just submitted the shipping or the ccpayment forms  --->
		<cfif tier2 and isDefined("getDistributorDetails")>
			<cfform name="referToDistiForm" >
				<cf_relayFormDisplay>
					<cfset form.distributorURL = getDistributorDetails.orgUrl>
					<cfset form.distributor = getDistributorDetails.organisationName>
					
					<cfif left(form.distributorURL,7) neq "http://">
						<cfset form.distributorURL = "http://#form.distributorURL#">
					</cfif>
					
					<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="frmMessage" label="" currentValue="">
						phr_opp_YourSelectedDistributorIs. <br /><br /> phr_opp_toOrderGoToDistiURL.
					</cf_relayFormElementDisplay>
					
					<cf_relayFormElementDisplay relayFormElementType="button" fieldname="frmTask" label="" currentValue="phr_closeWindow" onClick="javascript:closePopUp();" spanCols="true" valueAlign="right">
				</cf_relayFormDisplay>
			</cfform>

			<cfset continueToNextStep = false>					
		</cfif>
	</cfif>

	<cfif continueToNextStep>

		<cfif not structKeyExists(form,"nextStep") OR form.nextStep eq "submitShipping">
			<cfif fileexists("#application.paths.code#\cftemplates\opportunityShipping.cfm")>
				<cfinclude template="/code/cftemplates/opportunityShipping.cfm">
			</cfif>
		</cfif>
					
		<cfif structKeyExists(form,"nextStep") AND form.nextStep eq "submitShipping">
			<cfset partnerOrganisationId = application.com.RelayPLO.GetLocationOrg(opportunityDetails.partnerLocationId).organisationID>
			<cfset captureCreditCardInfo = application.com.flag.getFlagData(flagId="PaymentMethodCreditCard",entityID=partnerOrganisationId).recordcount>
			<cfset orderApprovalRequired = application.com.flag.getFlagData(flagId="internalOrderApprovalReqYes",entityID=partnerOrganisationId).recordcount>
			<cfset currentUserIsApprover = application.com.flag.isFlagSetForCurrentUser("PartnerOrderingManager")>

			<cfset qryOppTotal = application.com.opportunity.getOppProductsTotals(opportunityID=url.opportunityID).query>
			<cfset currency = qryOppTotal.currency>
			<cfset orderValue = NumberFormat(qryOppTotal.total,"0.00")>		<!--- strip any extra dps, we want to show trailing 0's (upto 2dps); NB. don't reformat to include commas cos it causes problems --->

			<!--- if PaymentMethod=CreditCard but opp requires approval and this user isn't the approver don't show cc form --->
			<cfif orderApprovalRequired and not currentUserIsApprover>
				<cfset captureCreditCardInfo = false> 
			</cfif>
	
			<cfif orderValue eq 0>
				<cfset captureCreditCardInfo = false> 
			</cfif>

			<cfif currency neq "USD">			<!--- this is a Panda special which will be moved out of core - no-one else is using credit cards at mo so it doesn't affect them; their gateway Authorize doesn't seem to support other currencies --->
				<cfset captureCreditCardInfo = false> 
			</cfif>
	
<!--- DEV: set to false for now so the users can't access cc payment until we release phase 1b --->				
<!--- <cfset captureCreditCardInfo = false>  --->	
			
			<cfif captureCreditCardInfo and fileexists("#application.paths.userFiles#\code\cftemplates\ccpayment\cccall.cfm")>
				<cfinclude template="/code/cftemplates/ccpayment/cccall.cfm">
			<cfelse>
				<script>
					closePopUp();
				</script>
			</cfif>
		<cfelseif structKeyExists(form,"nextStep") AND form.nextStep eq "submitOrder">
			<script>
				closePopUp();
			</script>
		</cfif>
			
	</cfif>