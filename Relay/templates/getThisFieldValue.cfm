<!--- �Relayware. All Rights Reserved 2014 --->

<!---

	DAM 2000-05-30
	
	This is called from a number of areas in selections it starts by giving
	a value to the variable [thisFieldValue] so that when it is refered to later in the
	calling page it doesn't disrupt anything else that calls it.
	
	It will only run its algorithm if the conditions on the second line are met.

	Get the field value from the selectionfield name
	Needs to be preceded by the query getCriteriaDescriptions
	and the parameter thisField
	
	Works well with TextBoxes and RadioButtons
	
--->
<CFSET thisFieldValue = "">
<CFIF isDefined("getCriteriaDescriptions") AND isDefined("thisField")>
	<!---
			DAM 2000-05-30
			
			Added field value retrieval so that the current value of the event field in the 
			current selection can be posted to the custom tag DisplayValidValueList 
	--->
	
	<CFOUTPUT QUERY="getCriteriaDescriptions">
		<CFSET y = #criteria#>
		<CFIF #selectionfield# IS #thisField#>
			<CFSET thisFieldValue = y>
		</CFIF>
	</CFOUTPUT>
	<!---
		Get the absolute field value from the selectionfield name
		
		There are many different types of criteria statement that a set of
		criteria may be contained in. These all have to be tested for one at a time
		in expression order: "{d '" has to be tested before "'".
	--->
	<CFIF thisField  IS "frmUKPostalCode" OR thisField  IS "frmUKTerritory">

		<CFSET nMarker = 1>
		<CFSET nLoop = 1>
		<CFSET nTempFieldValue = "">
		<CFSET n=1>
		<CFLOOP CONDITION="nLoop GT 0">
			<CFSET startAt = findnocase("patIndex('", thisFieldValue, nMarker)>
			<CFSET nLoop = startAt>
			<CFSET startAt=startAt+10>
			<CFSET nMarker = startAt>
			<CFIF nLoop GT 0>
				<CFSET endAt = findnocase("[0-9 ]", thisFieldValue, nMarker)>
				<CFSET nValue = mid(thisfieldvalue,startAt,(endAt-startAt))>
				<CFSET nTempFieldValue = listappend(nTempFieldValue,nValue,",")>
			</CFIF>
			<CFSET n=n+1>
		</CFLOOP>
		<CFSET thisFieldValue = "#nTempFieldValue#">

	<CFELSEIF findnocase("Quoted", thisFieldValue, 1) GT 0>
		<CFSET nMarker = 1>
		<CFSET nLoop = 1>
		<CFSET nTempFieldValue = "">
		<CFSET n=1>
		<CFLOOP CONDITION="nLoop GT 0">
			<CFSET startAt = findnocase("Quoted", thisFieldValue, nMarker)>
			<CFSET nLoop = startAt>
			<CFSET startAt=startAt+6>
			<CFSET nMarker = startAt>
			<CFIF nLoop GT 0>
				<CFSET endAt = findnocase(",", thisFieldValue, nMarker)>
				<CFSET nValue = mid(thisfieldvalue,startAt,(endAt-startAt))>
				<CFSET nTempFieldValue = listappend(nTempFieldValue,nValue,",")>
			</CFIF>
			<CFSET n=n+1>
		</CFLOOP>
		<CFSET thisFieldValue = "#nTempFieldValue#">
		
	<CFELSEIF findnocase("{d '", thisFieldValue, 1) GT 0>
		<CFSET startAt = findnocase("{d '", thisFieldValue, 1) + 4>
		<CFSET thisFieldValue = mid(thisFieldValue,startAt,len(thisFieldValue)-startAt-1)>

	<CFELSEIF findnocase("IN (", thisFieldValue, 1) GT 0> 
		<CFSET startAt = findnocase("IN (", thisFieldValue, 1) + 4>
		<CFSET endAt = findnocase(")", thisFieldValue, 1)>
		<CFSET thisFieldValue = mid(thisFieldValue,startAt,endAt-startAt)>

	<CFELSEIF findnocase("'", thisFieldValue, 1) GT 0> 
		<CFSET startAt = findnocase("'", thisFieldValue, 1) + 1>
		<CFSET thisFieldValue = mid(thisFieldValue,startAt,len(thisFieldValue)-startAt)>

	<CFELSEIF findnocase("=", thisFieldValue, 1) GT 0> 
		<CFSET startAt = findnocase("=", thisFieldValue, 1) + 1>
		<CFSET thisFieldValue = mid(thisFieldValue,startAt,len(thisFieldValue)-startAt + 1)>

	</CFIF>
	
</CFIF>
