<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 

qryGetTableDefinition.cfm

Purpose:
	Returns lists of which fields are numeric and which are string  (variables: TextFields and NumericFields and datefields)
	Used when generating updtae and insert queries to deciding whether I need quotes around the value or not.
	I have subsequently discovered that infact you can put quotes around numeric values anyway, 
	but I think that I still need it for date values.
	
requires  TableRequired - name of table


Mods:
2000-12-21 returns variable All Fields as well.
2001-06-18	WAB added smalldatetime to the numericfields
2004-10-12 WAB added a separate string of datefields 
		WAB save details in an application variable
2006-04-26 WAB took the date fields out of the text fields list!
May 2006	WAB converted to use sp_columns
July 2006 WAb discovered a bug in his conversion!
--->


<!--- these details stored as an application variable so test for key existence --->
	<cfif (not isDefined("application.tableDefinition"))>
		<cfset 	application.tableDefinition = structNew()>
	</cfif>
	


<cfif  not StructKeyExists(application.tableDefinition ,TableRequired)>

		<CFQUERY NAME="getColumnDefinitions" DATASOURCE="#application.siteDataSource#">
			sp_columns '#TableRequired#'
		</CFQUERY>

		<CFSET SQLNumericFields = "'int','bit','smallint','real','decimal','money'">
		<CFSET SQLDateFields = "'datetime','smalldatetime'">
		
		<cfquery name="getNumericFields" dbtype="query">
		select 	column_name
			from 	getColumnDefinitions
			where  	type_name in (#preservesinglequotes(SQLNumericFields)#)
			and		column_name not in ('#TableRequired#id')
		</cfquery>
		
		<CFQUERY NAME="getDateFields" dbtype="query">
		select 	column_name
			from 	getColumnDefinitions
			where  	type_name  in (#preservesinglequotes(SQLDateFields)#)
			and		column_name  not in ('#TableRequired#id')
		</CFQUERY>
		
		
		<CFQUERY NAME="getTextFields" dbtype="query">
			select 	column_name
			from 	getColumnDefinitions
			where  	type_name  not in (#preservesinglequotes(SQLNumericFields)#)
			and 	type_name  not in (#preservesinglequotes(SQLDateFields)#)		
		</CFQUERY>
		
		<CFQUERY NAME="getNullableFields" dbtype="query">
			select 	column_name
			from 	getColumnDefinitions
			where  	nullable = 1
		</CFQUERY>
		
		
		<CFSCRIPT>
			application.tableDefinition[TableRequired] 	= structNew() ;
			application.tableDefinition[TableRequired].textfields = valuelist(getTextFields.column_name) ;
			application.tableDefinition[TableRequired].numericfields = valuelist(getNumericFields.column_name) ;
			application.tableDefinition[TableRequired].datefields = valuelist(getDateFields.column_name) ;
			application.tableDefinition[TableRequired].allfields = valuelist(getColumnDefinitions.column_name) ;
			application.tableDefinition[TableRequired].nullablefields = valuelist(getNullableFields.column_name) ;
			x = application.com.structureFunctions.queryToStruct(getColumnDefinitions,"column_Name") ;
			application.tableDefinition[TableRequired].columns = x ;

		</cfscript>

</cfif>		

		<cfset table = application.tableDefinition[tablerequired]>

		<CFSET TextFields = table.textfields>	
		<CFSET NumericFields = table.Numericfields>	
		<CFSET DateFields = table.Datefields>	
		<CFSET AllFields = table.AllFields>	
		<CFSET NullableFields = table.nullablefields>	

		
<CFSET "#TableRequired#Def" = table>
