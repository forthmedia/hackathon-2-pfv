<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
	
	qryGetDistributors.cfm 
	
	This template will return a list of distis defined in the database using
	a view called vDistiList.  This view creates a list of distributors and
	data related to them in text flags. 

	Parameters:
		frmCountryID (optional): this will constrain the list to the coutries
		defined in frmCountryID.
		frmOrgIDList (optional): this will constrain the list to the orgs
		defined in frmOrgIDList.  This is used when a reseller has a defined set of 
		distis they deal with exclusively.

Modified		
		
CPS 07-Mar-01 Amend query to select distinct organisations from the view to ensure that 
              when adding points the correct organisationId is supplied to RWAccruePoints
			  rather than the locationId
SWJ	25 Jan 02 Added frmOrgIDList filter to allow us to constrain the list 
			  by organisation ID as well
--->


<CFPARAM NAME="frmCountryIDList" DEFAULT="">
<CFPARAM NAME="frmOrgIDList" DEFAULT="">

<CFQUERY NAME="GetDistis" datasource="#application.siteDataSource#">
	SELECT DISTINCT organisationId, organisationName,ISOCode,
		CountryDescription, specificURL, telephone, Fax, logoFile, distiType, OrderingIndex
	FROM dbo.vDistiList2 
	where 1=1
	<CFIF frmCountryIDList NEQ "">
	and countryID  in ( <cf_queryparam value="#frmCountryIDList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	</CFIF>
	<CFIF frmOrgIDList NEQ "">
	and organisationId  in ( <cf_queryparam value="#frmOrgIDList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	</CFIF>
Order by countryDescription, OrderingIndex,organisationName	
</CFQUERY>
