<!--- �Relayware. All Rights Reserved 2014 --->
<!--- comes up with a list of all
		countries to which a User
		has at least read rights

Parameters:
ReqdCountryID  (required)
CreateCountryRightsTable  (optional) (false)


Returns:
#Variables.CountryList#   (comma separated list of countryids)
or 
countryRightsTableName   (name of temporary table containing countryids)
and
CountryIDandNameList (a comma separated list of countryIDs and 
					names delimited by application.delim1)


USAGE:
		<CFINCLUDE TEMPLATE="../templates/qrygetcountries.cfm">
		qrygetcountries creates a variable CountryList

		  AND l.CountryID IN (#Variables.CountryList#)
--->

<!--- mods
WAB 2000-03-02 added stuff to return a comma separated list containing both the ids and the countrynames
wab 2001-06-11  added switch which creates a temporary table of countryids rather than providing a comma separated list. This can easily be used in queries and is rather easier than including the whole of this query in your own queries
	2016-02-03	WAB Mass replace of displayValidValues using lists to use queries 

 --->

<CFPARAM name="ReqdCountryID" default = "0">
<CFPARAM name="partnerCheck" default = "false">
<CFPARAM name="CreateCountryRightsTable" default = "false">

<!--- if we are to create a temporary table then decide what to call it and make sure that it doesn't exist --->
<cfif CreateCountryRightsTable>
	<cfset countryRightsTableName = "##countryRights_#request.relayCurrentUser.personid#">
	<CFQUERY NAME="dropExisting" DATASOURCE="#application.SiteDataSource#">
		if exists (select * from tempdb..sysobjects where id = object_id(N'TEMPDB..#countryRightsTableName#') )
		drop table #countryRightsTableName#
	</cfquery>		
</cfif>

<!--- lists all Countries to which User has rights --->
<CFQUERY NAME="findCountries" DATASOURCE="#application.SiteDataSource#">
	SELECT r.CountryID
		<cfif not CreateCountryRightsTable>
			,
			countryDescription,
			convert(varchar(3),r.countryid) + '#application.delim1#' +  c.countryDescription as CountryIDandName, 
			Convert(bit,sum(r.permission & 8)) AS Level4,
			Convert(bit,sum(r.permission & 4)) AS Level3,
			Convert(bit,sum(r.permission & 2)) AS Level2,
			convert(bit,sum(r.permission & 1)) AS Level1,
			Convert(bit,sum(r.permission & 4)) AS AddOkay,
			Convert(bit,sum(r.permission & 2)) AS UpdateOkay
		<cfelse>
		   into #countryRightsTableName#
		</cfif>

	FROM Rights AS r, RightsGroup AS rg, Country as c
	WHERE r.SecurityTypeID = (SELECT e.SecurityTypeID FROM SecurityType AS e
							 WHERE e.ShortName = 'RecordTask')
	AND r.UserGroupID = rg.UserGroupID
	AND c.countryid = r.countryid
	<CFIF not PartnerCheck>
		AND rg.PersonID = #request.relayCurrentUser.personid#
	<CFELSE>
		AND rg.PersonID = (select personid from usergroup where usergroupid = #request.relayCurrentUser.usergroupid#)
	</CFIF>
	<CFIF ReqdCountryID is not 0>
		AND r.CountryID =  <cf_queryparam value="#ReqdCountryID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</CFIF>
	AND r.Permission > 0
	GROUP BY r.CountryID, c.countryDescription
	ORDER BY CountryDescription
<!--- 	HAVING Sum((r.Permission\1) MOD 2) > 0	<!--- at least read permissions ---> --->
</CFQUERY>

<cfif not CreateCountryRightsTable>
	<CFSET CountryList = 0>
	<CFIF findCountries.RecordCount GT 0>
		<CFIF findCountries.CountryID IS NOT "">
			<!---< top record (or only record) is not null --->
			<CFSET CountryList = ValueList(findCountries.CountryID)>
			<CFSET CountryIDandNameList = valueList(findCountries.CountryIDandName)>
		</CFIF>
	</CFIF>
</cfif>
	
<!--- reset defaults (in case called more than once in a single page --->
<CFSET partnerCheck=false>
<CFSET ReqdCountryID=0>
		
