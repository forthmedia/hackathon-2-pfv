<!--- �Relayware. All Rights Reserved 2014 --->
<html xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:x="urn:schemas-microsoft-com:office:excel"
xmlns="http://www.w3.org/TR/REC-html40">
<head>
<!--- 2008/06/09 GCC removed - works better with the codeset making its own mind up...
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<meta name=ProgId content=Excel.Sheet>
--->
<!---2008/11/17 GCC put back in conjunction with changes in doDownloadV2.cfm and now works for EMEA and Far East langiages!!!--->
<!--- 16-March-2011 added xlTime class to handle time --->
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<style>
<!--table
	{mso-displayed-decimal-separator:"\.";
	mso-displayed-thousand-separator:"\,";}
@page
	{margin:1.0in .75in 1.0in .75in;
	mso-header-margin:.5in;
	mso-footer-margin:.5in;}
tr
	{mso-height-source:auto;}
hr
	{mso-style-parent:style0;
	color:black;
	font-size:9.0pt;
	font-weight:400;
	font-family:Verdana, sans-serif;
	mso-font-charset:0;
	background:white;
	mso-pattern:auto none;}

col
	{mso-width-source:auto;}
br
	{mso-data-placement:same-cell;}
.style18
	{mso-number-format:"_-\0022\0022* \#\,\#\#0\.00_-\;\\-\0022\0022* \#\,\#\#0\.00_-\;_-\0022\0022* \0022-\0022??_-\;_-\@_-";
	mso-style-name:Currency;
	mso-style-id:4;}
.style17
	{mso-number-format:"\[$$-409\]\#\,\#\#0\.00\;\[Red\]\[$$-409\]\#\,\#\#0\.00";
	mso-style-name:Currency;
	mso-style-id:4;}
.style16
	{mso-number-format:"\[$\20AC-2\]\\ \#\,\#\#0\.00\;\[Red\]\[$\20AC-2\]\\ \#\,\#\#0\.00";
	mso-style-name:Currency;
	mso-style-id:4;}
.style0
	{mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	white-space:nowrap;
	mso-rotate:0;
	mso-background-source:auto;
	mso-pattern:auto;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial;
	mso-generic-font-family:auto;
	mso-font-charset:0;
	border:none;
	mso-protection:locked visible;
	mso-style-name:Normal;
	mso-style-id:0;}
td
	{mso-style-parent:style0;
	padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial;
	mso-generic-font-family:auto;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border:none;
	mso-background-source:auto;
	mso-pattern:auto;
	mso-protection:locked visible;
	white-space:nowrap;
	mso-rotate:0;}
.xlDate
	{mso-style-parent:style0;
	mso-number-format:<<CHR(34)>>mm\/dd\/yyyy<<CHR(34)>>;}
.xl24
	{mso-style-parent:style0;
	mso-number-format:"Short Date";}

.xl25
	{mso-style-parent:style18;
	mso-number-format:"_-\0022\0022* \#\,\#\#0\.00_-\;\\-\0022\0022* \#\,\#\#0\.00_-\;_-\0022\0022* \0022-\0022??_-\;_-\@_-";}
	
.xl26
	{mso-style-parent:style17;
	mso-number-format:"\[$$-409\]\#\,\#\#0\.00\;\[Red\]\[$$-409\]\#\,\#\#0\.00";
	}
.xl45
	{mso-style-parent:style16;
	mso-number-format:"\[$\20AC-2\]\\ \#\,\#\#0\.00\;\[Red\]\[$\20AC-2\]\\ \#\,\#\#0\.00";
	}
.xl46
	{mso-style-parent:style19;
	mso-number-format:"\[$\00A3-2\]\\ \#\,\#\#0\.00\;\[Red\]\[$\00A3-2\]\\ \#\,\#\#0\.00";
	}
.text /*used to display vatnumber and postcode without leading zero's being removed*/
	{mso-number-format:"\@";}

.xlTime
	{mso-style-parent:style0;
	mso-number-format:"\[$-F400\]h\:mm\:ss\\ AM\/PM";}
-->
</style>
</head>

<body>
