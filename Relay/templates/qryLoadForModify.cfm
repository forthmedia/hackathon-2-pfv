<!--- �Relayware. All Rights Reserved 2014 --->

<!---
		Get rid of any temp selection for the current user
		and load a selection into temp selection for that user
		given the ModifyID Parameter

--->
<CFQUERY NAME="DelTempTag" DATASOURCE="#application.siteDataSource#">
	DELETE FROM TempSelectionTag
        WHERE SelectionID = #request.relayCurrentUser.personid#
</CFQUERY>

<CFQUERY NAME="DelTempCrit" DATASOURCE="#application.siteDataSource#">
	DELETE FROM TempSelectionCriteria
        WHERE SelectionID = #request.relayCurrentUser.personid#
</CFQUERY>


<!---
		Get the Selection Header
--->

<CFQUERY NAME="getSelectionHead" DATASOURCE="#application.siteDataSource#">
	SELECT 
		Title,
		RefreshRate,
		Description
	FROM
		Selection 
	WHERE
		SelectionID =  <cf_queryparam value="#ModifyID#" CFSQLTYPE="CF_SQL_INTEGER" > 
</CFQUERY>

<CFQUERY NAME="ModCriteria" DATASOURCE="#application.siteDataSource#">
	INSERT INTO TempSelectionCriteria (SelectionID,SelectionField,Criteria, CreatedBy,Created,LastUpdatedBy,LastUpdated) 
	 SELECT #request.relayCurrentUser.personid#,
	 		SelectionField,
			Criteria,
			CreatedBy,
			Created,
			LastUpdatedBy,
			LastUpdated
	 FROM SelectionCriteria
	 WHERE SelectionID =  <cf_queryparam value="#ModifyID#" CFSQLTYPE="CF_SQL_INTEGER" > 
</CFQUERY>


