<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		subMenuBar.cfm
Author:			
Date created:	

	Objective - please describe the objective of this procedure
		
	Syntax	  -	ProcName [Var1]>,[Var2]>,[ReturnRecordsets]
	
	Parameters - Please describe any parameters that can be passed to the proc and
				their purpose and usage.
				
	Return Codes - Please describe the return codes are returned to let you know certain 

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2004-12-13	WAB		Altered the way security is handled in the xml file
2012-02-03	WAB		Sprint Menu Changes.  Rationalise menu XML code.  Access menus through getMenuXMLDoc().  Client extensions dealt with automatically.
					(not sure this file used)

Enhancement still to do:
1.  Requires security check on each item


--->
<!--- <cfinclude template="/templates/get UserSecurityLevels.cfm"> --->

<cfparam name="Module" default="certification">

		
	<!--- <cfset xmlDoc = application.com.relayMenu.getMenuXMLDoc()>
	<cfset menuActions = XmlSearch(xmlDoc, "//MenuItem[@Module='#Module#']/MenuAction")> --->
	<cfset menuActions = application.com.xmlFunctions.xmlSearchWithLock(xmlDoc=application.com.relayMenu.getMenuXMLDoc(),xPathString="//MenuItem[@Module='#Module#']/MenuAction",lockName="applicationMenu")>
	<cfset current = "tab1">


<cf_head>
	<cf_title>Relay Sub Menu Bar</cf_title>
</cf_head>


<CF_Translate processevenifnested=true>
<CF_RelayNavMenu thisDir="#Module#"> 

<cfset menuList = "">
	<cfloop index="i" from="1" to="#ArrayLen(menuActions)#" step="1">

			<!--- use new security structure ((with backwards compatability) see defaultlmenuxml.cfm for details --->
			<cfset thisSecurity = menuActions[i].XmlAttributes.SECURITY>
			<!--- this line is just for backwards compatability with old xml files, can be removed once all have been updated 
			<cfset thisSecurity = replaceNoCase (replaceNoCase(replaceNoCase(thisSecurity,"(7)",":Level3"),"(3)",":Level2"),"(1)",":Level1")>--->
			<!--- WAB 2007-01-29 added ability to have mulitple items in the security list - new checkSecurityList function deals
			<cfset hasCorrectSecurity = application.com.login.checkInternalPermissions (listfirst(thissecurity,":"), listlast(thissecurity,":"))>--->
			<cfset hasCorrectSecurity =  application.com.login.checkSecurityList(thisSecurity)>

<!--- 		<cfif listFindNoCase(session. securityLevels,menuActions[i].XmlAttributes.SECURITY) neq 0> --->
		<cfif hascorrectSecurity> 
			<CF_RelayNavMenuUpper MenuItemText="phr_sys_nav_#replace(menuActions[i].XmlAttributes.TABTEXT," ","_","ALL")#" CFTemplate="/#menuActions[i].XmlAttributes.URL#" target="mainSub" description="#menuActions[i].XmlAttributes.DESCRIPTION#" current="#current#">
			<!--- NJH 2007-09-19 create a list of menu items. When looping through the client specific menus, we then determine whether to show it or
				not based on whether it already is displayed. So, if the main menu contains it, don't display it again --->
			<cfset menuList = listAppend(menuList,menuActions[i].XmlAttributes.TABTEXT)>
		</cfif>
 	</cfloop>
	
	
	<cfif application.com.login.checkInternalPermissions ("AdminTask", "Level1") >
		<CF_RelayNavMenuUpper MenuItemText="phr_sys_Nav_Configure" CFTemplate="/templates/configFrame.cfm?module=#Module#" target="mainSub" description="Configure #Module#" current="#current#">
	</cfif>

		<!--- NJH 2006-07-11 Reports is now in the XML file --->
<!---   	<cfif application.com.login.checkInternalPermissions ("ReportTask", "Level1") >
		<CF_RelayNavMenuUpper MenuItemText="Phr_Sys_Reports" CFTemplate="/templates/reportFrame.cfm?rptGroup=#Module#" target="mainSub" description="#Module# Reports" current="#current#">
	</cfif>  --->
	<CF_RelayNavMenuUpper MenuItemText="Phr_Sys_Help" CFTemplate="http://relay.foundation-network.com/RelayAdmin/help/HelpFrame.cfm?id=#adminUser#&showheader=no&srchString=#module#" target="mainSub" description="#Module# Help" current="#current#">
</CF_RelayNavMenu>
</CF_Translate>



