<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		entityManager.cfm
Author:			?
Date created:	2000

	Objective - allows user to allocate users record rights to an entity

	Syntax	  -		<cfif not isdefined("form.RecordID")>
						<cfparam name="form.RecordID" default="#url.RecordID#">
						<cfparam name="form.Entity" default="#URL.Entity#">
						<cfparam name="form.ShortName" default="#URL.ShortName#">
					</cfif>

					<cfinclude template="/templates/EntityManager.cfm">


	Parameters - RecordID (mandatory) - this is the entityID


Used in events/eventManager.cfm

Amendment History:

Date		Initials 	What was changed
2012/05/14	IH			Case 428120 fix GetAvailable query to return correct permissions
2013-04-03 	NYB 		Case 434494 replaced Level value list with a calculation - so it doesn't fall over when level 4 as assigned but level 2 wasn't.  The highest Level assigned is the value displayed.

Enhancement still to do:



--->



<CFIF NOT (IsDefined("form.RecordID") OR IsDefined("form.Entity") OR IsDefined("form.ShortName"))>
	<SCRIPT type="text/javascript">
	<!--
	alert("An error has occured and this process will now terminate");
	// window.close();
	//-->
	</SCRIPT>
	<CF_ABORT>
</CFIF>

<!---
<CFSET Levels = "Level 1,Level 2,Level 3,Level 4">
<CFSET Permissions = "1,3,7,15">
--->
<!--- START: 2013-04-03 NYB Case 434494 - replaced:
<CFSET Levels = "Level 1,Level 2,Level 3,Level 4,Special 2, Special 4, Special 5,Special 6,Special 8,Special 9,Special 10,Special 11">   <!--- WAB: this level4 is a bit of a hack - not really right --->
<CFSET Permissions = "1,3,7,15,2,4,5,6,8,9,10,11">
 with: --->
<CFSET TopLevel = "5">
<!--- END: 2013-04-03 NYB Case 434494 --->

<CFSET FreezeDate = request.requestTimeODBC>

<CFQUERY NAME="getSecurityTypeID" datasource="#application.siteDataSource#">
	SELECT SecurityTypeID
	FROM SecurityType
	WHERE ShortName =  <cf_queryparam value="#ShortName#" CFSQLTYPE="CF_SQL_VARCHAR" >
</CFQUERY>

<CFSET SecurityTypeID = getSecurityTypeID.SecurityTypeID>

<!--- add records to the RecordRights table if user is adding managers --->
<CFIF IsDefined("Add") AND IsDefined("Managers")>
	<CFLOOP LIST="#Managers#" INDEX="theIndex">
		<!--- make sure this isn't a page refresh --->
		<CFQUERY NAME="CheckRecord" datasource="#application.siteDataSource#">
			SELECT *
			FROM RecordRights
			WHERE UserGroupID =  <cf_queryparam value="#theIndex#" CFSQLTYPE="CF_SQL_INTEGER" >
			AND Entity =  <cf_queryparam value="#Entity#" CFSQLTYPE="CF_SQL_VARCHAR" >
			AND RecordID =  <cf_queryparam value="#RecordID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</CFQUERY>

		<CFIF CheckRecord.RecordCount EQ 0>
			<CFQUERY NAME="UpdateManagers" datasource="#application.siteDataSource#">
				INSERT INTO RecordRights
				(UserGroupID, Entity, RecordID, Permission)
				VALUES
				(<cf_queryparam value="#theIndex#" CFSQLTYPE="CF_SQL_INTEGER" >, <cf_queryparam value="#Entity#" CFSQLTYPE="CF_SQL_VARCHAR" >, <cf_queryparam value="#RecordID#" CFSQLTYPE="CF_SQL_INTEGER" >, 0)
			</CFQUERY>
		</CFIF>
	</CFLOOP>
</CFIF>

<!--- delete records from the RecordRights table if user is removing managers --->
<CFIF IsDefined("Remove") AND IsDefined("Managers")>
	<CFQUERY NAME="DeleteManagers" datasource="#application.siteDataSource#">
		DELETE FROM RecordRights
		WHERE Entity =  <cf_queryparam value="#Entity#" CFSQLTYPE="CF_SQL_VARCHAR" >
		AND RecordID =  <cf_queryparam value="#RecordID#" CFSQLTYPE="CF_SQL_INTEGER" >
		AND UserGroupID  IN ( <cf_queryparam value="#Managers#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	</CFQUERY>
</CFIF>

<!--- get the names of people who are assigned as managers to the entity in question -
the entity is defined by the FlagID, which is the unique identifier,
and the Entity, which is the table the FlagID is in --->
<!--- <CFQUERY NAME="GetAssigned" datasource="#application.siteDataSource#">
	SELECT MAX(ug.Name) AS Name, rr.UserGroupID, MAX(r.Permission) AS Permission
	FROM RecordRights AS rr, UserGroup AS ug, Rights AS r
	WHERE rr.RecordID = #RecordID#
	AND rr.UserGroupID = ug.UserGroupID
	AND r.UserGroupID = ug.UserGroupID
	AND rr.Entity = '#Entity#'
	AND r.SecurityTypeID = #SecurityTypeID#
	GROUP BY rr.UserGroupID
	ORDER BY ug.Name
</CFQUERY> --->


<CFQUERY NAME="GetAssigned" datasource="#application.siteDataSource#">
	SELECT     rr.usergroupid, UserGroup.Name,
		Convert(bit,sum(r.permission & 16)) * 16 +
		Convert(bit,sum(r.permission & 8)) * 8 +
		Convert(bit,sum(r.permission & 4)) * 4 +
		Convert(bit,sum(r.permission & 2)) * 2 +
		Convert(bit,sum(r.permission & 1)) * 1 AS permission
	FROM         recordrights rr INNER JOIN
	                      UserGroup ON rr.usergroupid = UserGroup.UserGroupID INNER JOIN
	                      RightsGroup ON UserGroup.PersonID = RightsGroup.PersonID INNER JOIN
	                      Rights r ON RightsGroup.UserGroupID = r.UserGroupID
	WHERE     (rr.recordid =  <cf_queryparam value="#RecordID#" CFSQLTYPE="CF_SQL_INTEGER" > ) AND (rr.entity =  <cf_queryparam value="#Entity#" CFSQLTYPE="CF_SQL_VARCHAR" > ) AND (r.SecurityTypeID =  <cf_queryparam value="#SecurityTypeID#" CFSQLTYPE="CF_SQL_INTEGER" > )
	GROUP BY rr.usergroupid,UserGroup.Name
	ORDER BY UserGroup.Name
</CFQUERY>

<!--- make a list of the assigned managers - default is 0 so it can be used in an IN clause--->
<CFSET AssignedManagers = 0>
<CFIF GetAssigned.RecordCount GT 0>
	<CFSET AssignedManagers = ValueList(GetAssigned.UserGroupID)>
</CFIF>

<!--- 2007/05/04 GCC Changed to inherit rights from user groups people are in instead of having to have personal rights
This has probably introduced some bugs but lets see how we go...
 --->

<!---
<!--- if this is an event then we need to make a temporary table that contains the UserGroupID of all people
who have country rights to view Events,	the country rights are determined by the EventCountry table -
if the CountryID in the EventCountry table matches the CountryID of the person then that person is included --->
<CFIF ShortName IS "EventTask">
	<CFSET TempPersonTable = DateFormat(FreezeDate,"yyyymmdd") & TimeFormat(FreezeDate,"HHmmss") & "_" & request.relayCurrentUser.usergroupid>
	<CFQUERY NAME="CreateTempPeople" datasource="#application.siteDataSource#">
		SELECT DISTINCT r.UserGroupID AS UserGroupID
		INTO ###TempPersonTable#
		FROM Rights AS r, EventCountry AS ec
		WHERE ec.FlagID = #RecordID#
		AND ec.CountryID = r.CountryID
		AND r.SecurityTypeID = (SELECT SecurityTypeID FROM SecurityType WHERE ShortName = 'RecordTask')
		AND r.Permission > 0
		AND r.MenuID = 0
	</CFQUERY>
</CFIF>

<!--- now get people who have greater than view rights for this type of entity (level 2 or 3, ie permission 3 or 7)
and have not already been assigned as a manager to this particular entity --->
<CFQUERY NAME="GetAvailable" datasource="#application.siteDataSource#">
	SELECT ug.Name, r.UsergroupID, r.Permission
	FROM Rights AS r, UserGroup AS ug<CFIF ShortName IS "EventTask">, ###TempPersonTable# AS tt</CFIF>
	WHERE r.SecurityTypeID = #SecurityTypeID#
	AND r.Permission > 1
	AND r.MenuID = 0
	<CFIF ShortName IS "EventTask">AND r.UsergroupID = tt.UsergroupID</CFIF>
	AND r.UserGroupID = ug.UsergroupID
	AND r.UsergroupID NOT IN (#AssignedManagers#)
	ORDER BY ug.Name
</CFQUERY>

<CFIF ShortName IS "EventTask">
	<CFQUERY NAME="dropTempTable" datasource="#application.siteDataSource#">
		DROP TABLE ###TempPersonTable#
	</CFQUERY>
</CFIF>
 --->

<CFIF ShortName IS "EventTask">
	<CFQUERY NAME="GetAvailable" datasource="#application.siteDataSource#">
		SELECT     ISNULL(Person.FirstName, '') + ' ' + ISNULL(Person.LastName, '') AS Name, UserGroup.UserGroupID,
			Convert(bit,sum(r.permission & 16)) * 16 +
			Convert(bit,sum(r.permission & 8)) * 8 +
			Convert(bit,sum(r.permission & 4)) * 4 +
			Convert(bit,sum(r.permission & 2)) * 2 +
			Convert(bit,sum(r.permission & 1)) * 1 AS permission
		FROM         RightsGroup INNER JOIN
		                      Rights r ON RightsGroup.UserGroupID = r.UserGroupID INNER JOIN
		                      Person ON RightsGroup.PersonID = Person.PersonID INNER JOIN
		                      UserGroup ON Person.PersonID = UserGroup.PersonID
		WHERE     (r.SecurityTypeID =  <cf_queryparam value="#securityTypeID#" CFSQLTYPE="CF_SQL_INTEGER" > ) AND (RightsGroup.PersonID IN
		                          (SELECT DISTINCT UserGroup.PersonID
		                            FROM          Rights r INNER JOIN
		                                                   eventCountry ec ON r.CountryID = ec.CountryID INNER JOIN
		                                                   UserGroup ON r.UserGroupID = UserGroup.UserGroupID
		                            WHERE      (r.MenuID = 0) AND (ec.FlagID =  <cf_queryparam value="#RecordID#" CFSQLTYPE="CF_SQL_INTEGER" > ) AND (r.SecurityTypeID =
		                                                       (SELECT     SecurityTypeID
		                                                         FROM          SecurityType
		                                                         WHERE      ShortName = 'RecordTask')) AND (r.Permission > 0))) AND (NOT (UserGroup.UserGroupID  IN ( <cf_queryparam value="#AssignedManagers#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )))
		GROUP BY ISNULL(Person.FirstName, '') + ' ' + ISNULL(Person.LastName, ''), UserGroup.UserGroupID
		HAVING (MAX(r.Permission) > 1)
		ORDER BY Name
	</CFQUERY>
<cfelse>
	<!--- PKP 2008-03-03 TREND BUG 148 ORDER BY NAME  --->
	<CFQUERY NAME="GetAvailable" datasource="#application.siteDataSource#">
		SELECT ug.Name, r.UsergroupID, r.Permission
		FROM Rights AS r, UserGroup AS ug
		WHERE r.SecurityTypeID =  <cf_queryparam value="#SecurityTypeID#" CFSQLTYPE="CF_SQL_INTEGER" >
		AND r.Permission > 1
		AND r.MenuID = 0
		AND r.UserGroupID = ug.UsergroupID
		AND r.UsergroupID  NOT IN ( <cf_queryparam value="#AssignedManagers#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		ORDER BY ug.Name
	</CFQUERY>
</CFIF>
<div class="internalBodyPadding">
<div class="row">
	<div class="col-xs-12 col-sm-6">
		<div class="grey-box-top">
			<CFIF GetAvailable.RecordCount GT 0>
				<FORM NAME="Add" ACTION="entityManager.cfm" METHOD="post">
				<CFOUTPUT>
				<CF_INPUT TYPE="Hidden" NAME="RecordID" VALUE="#RecordID#">
				<CF_INPUT TYPE="Hidden" NAME="Entity" VALUE="#Entity#">
				<CF_INPUT TYPE="Hidden" NAME="ShortName" VALUE="#ShortName#">
				<INPUT TYPE="Hidden" NAME="Add" VALUE="Add">
				</CFOUTPUT>
				<label>Managers you can assign:</label>

				<SELECT NAME="Managers" class="form-control" MULTIPLE <CFIF GetAvailable.RecordCount LT 10> SIZE="<CFOUTPUT>#GetAvailable.RecordCount#</CFOUTPUT>" <CFELSE> SIZE="10" </CFIF> >
					<CFOUTPUT QUERY="GetAvailable">
						<!--- START: 2013-04-03 NYB Case 434494 replaced:
						<OPTION VALUE="#UsergroupID#">#ListGetAt(Levels, ListFind(Permissions, getavailable.Permission))#: #htmleditformat(Name)#
						with: --->
						<cfset Level = 0>
						<cfloop from="#TopLevel#" to="1" index="i" step="-1">
							<cfif Permission gte (2^(i-1)) and Permission lt (2^(i))>
								<cfset Level = i>
								<cfbreak>
							</cfif>
						</cfloop>
						<OPTION VALUE="#UsergroupID#">Level #Level#: #htmleditformat(Name)#
						<!--- END: 2013-04-03 NYB Case 434494 --->
					</CFOUTPUT>
				</SELECT>
				</FORM>
			<CFELSE>
				<p>Sorry, there are no Managers that can be assigned.</p>
			</CFIF>
			<p id="entityManagerAdd">
				<A HREF="javascript:document.Add.submit()" class="btn btn-primary">ADD</A>
			</p>
		</div>
	</div>
	<div class="col-xs-12 col-sm-6">
		<div class="grey-box-top">
			<CFIF GetAssigned.RecordCount GT 0>
				<FORM NAME="Remove" ACTION="entityManager.cfm" METHOD="post">
				<CFOUTPUT>
				<CF_INPUT TYPE="Hidden" NAME="RecordID" VALUE="#RecordID#">
				<CF_INPUT TYPE="Hidden" NAME="Entity" VALUE="#Entity#">
				<CF_INPUT TYPE="Hidden" NAME="ShortName" VALUE="#ShortName#">
				<INPUT TYPE="Hidden" NAME="Remove" VALUE="Remove">
				</CFOUTPUT>
				<label>Managers currently assigned:</label>

				<SELECT class="form-control" NAME="Managers" MULTIPLE <CFIF GetAssigned.RecordCount LT 10> SIZE="<CFOUTPUT>#GetAssigned.RecordCount#</CFOUTPUT>" <CFELSE> SIZE="10" </CFIF> >
					<CFOUTPUT QUERY="GetAssigned">
						<!--- START: 2013-04-03 NYB Case 434494 replaced:
						<OPTION VALUE="#UsergroupID#">#ListGetAt(Levels, ListFind(Permissions, Permission))#: #htmleditformat(Name)#<br>
						with: --->
						<cfset Level = 0>
						<cfloop from="#TopLevel#" to="1" index="i" step="-1">
							<cfif Permission gte (2^(i-1)) and Permission lt (2^(i))>
								<cfset Level = i>
								<cfbreak>
							</cfif>
						</cfloop>
						<OPTION VALUE="#UsergroupID#">Level #Level#: #htmleditformat(Name)#<br>
						<!--- END: 2013-04-03 NYB Case 434494 --->
					</CFOUTPUT>
				</SELECT>
				</FORM>
			<CFELSE>
				<p>There are no Managers currently assigned.</p>
			</CFIF>
			<p id="entityManagerRemove">
				<A HREF="javascript:document.Remove.submit()" class="btn btn-primary">REMOVE</A>
			</p>
		</div>
	</div>
</div>
<cfif not isDefined("session.eventStep")>
	<p><A HREF="javascript: window.close();">CLOSE WINDOW</A></p>
</cfif>
</DIV>
</div>
