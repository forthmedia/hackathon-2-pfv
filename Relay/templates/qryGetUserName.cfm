<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File name:			qryGetUserName.cfm
Author:				Simon WJ
Date created:		30 June 2000

Description:		This gets the username from the users cookie

Date Tested:		30 June 2000
Tested by:

Amendment History:	

Date (DD-MMM-YYYY)	Initials 	What was changed


--->

<CFQUERY NAME="GetUserName" datasource="#application.siteDataSource#">
	SELECT firstName, LastName, email, firstName+' '+lastName AS fullname
	FROM person 
	WHERE personid = #request.relayCurrentUser.personid#
</CFQUERY>