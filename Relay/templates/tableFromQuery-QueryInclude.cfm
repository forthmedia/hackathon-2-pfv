<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			tableFromQuery-QueryInclude.cfm
Author:				SWJ
Date started:		2003-11-17

Description:	 Standard filters for queries that can safely be included for any query

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
18-JAN-2007			AJC			Removed the ListFirst function around FilterSelect(x) and FilterSelectValues(x) (line 34,39,44).
								If a filter value had a comma it would break and not filter the results properley.

03-aug-2007			SSS			Add functionality to allow people to check the reports by day as well as month
17-mar-2008			SSS			Added team where clause
2008-04-30 			WAB			bug with 3rd select filter  (had  a 1 in it)
2009-01-16 			SSS			Bug 1600 scoping error in vars ment that september had 31 days
2009-02-05 			NYB			Trend NABU bug 1744 - replaced 'FilterSelectValues1' (2 & 3) with 'replace(replace(FilterSelectValues1,"'","''"),"''''","''")'
								as filter was throwing an error when an organisation had an apostrophe
2011/02/02			WAB			Needed support for Nvarchar, but adding N falls over if the code is used within a QoQ
								There are only 1/2 dozen places where it is used in QoQ.  It would be nice to get rid of them all, but needed a quick fix
								So solution is to have a variable #N# which is set to either "N" or ""
								By default NVarchar is supported.  To switch off you must set variable inQoQ to true
2012-10-23			IH			Case 431420 fix filtering
2014-07-24			NYB			Case 441019 added ,"all"
2014/01/10			DXC			Case 441110 Add param "useWholeWordsOnly" to allow searching by whole words or part words. Defaults to whole words
2015/09/30			NJH			PROD2015-35 - added support for single date filter
2015-10-14 			WAB 		PROD2015-165 Security Scan.  Protect sortOrder from injection
2015-10-26			RJT			PROD2015-250 Correct order of dates for filters to be lowest -> highest whichever way round the user enters them. Alter form itself so that UI updates too
2015-10-27			ACPK		PROD2015-261 Ensure FilterSelects only expect string datatype; Fixed typo on line 93
2015-11-17			WAB			PROD2015-405 Security/SQL Injection Implement cf_querparam and cf_queryobjectname throughout.
								Also Fixed problem with PROD2015-261 fix.  Changed casting so that all casting in sql queries is nvarchar(max) - used to have some (15) and some (255)


Possible enhancements:


 --->
<!--- This section turns off Nvarchar support if we are in QoQ --->
<cfparam name="inQoQ" default = "false">
<cfif inQoQ>
	<cfset N="">
	<cfset varcharCasting = "varchar">
<cfelse>
	<cfset N="N">
	<cfset varcharCasting = "nvarchar(max)">
</cfif>

<!--- WAB 2015-10-14 PROD2015-165 Security Scan. SortOrder parameter is globally vulnerable to injection.  This is a quick fix until I get the globalTypeChecker running --->
<cfif isDefined("sortOrder")>
	<cf_param name = "sortOrder" default="" type="queryObjectName">
</cfif>

<!--- START 2014/01/10			DXC			Case 441110 - Call this page with wholeWordsOnly=false to allow wildcard search against part words--->
<cfparam name="useWholeWordsOnly" default = "true">
<cfif useWholeWordsOnly eq "true">
	<cfset spaceCharacter=" ">
<Cfelse>
	<cfset spaceCharacter="">
</cfif>
<!--- END 2014/01/10			DXC			Case 441110 --->

<!--- used in simple queries --->
<cfoutput>
	<cfif isdefined( "form.frmAlphabeticalIndex" ) and form["frmAlphabeticalIndex"] neq ""
		and isDefined ( "alphabeticalIndexColumn" ) and alphabeticalIndexColumn neq ""
	>
		AND LEFT( UPPER( <cf_queryObjectName value="#alphabeticalIndexColumn#"> ), 1 ) = <cf_queryparam value="#form["frmAlphabeticalIndex"]#" CFSQLTYPE="CF_SQL_VARCHAR" isQoQ="#inQoQ#">
	</cfif>

	<cfif isDefined("FilterSelect") and FilterSelect neq "" and isDefined("FilterSelectValues")>
		<cfif FilterSelectValues eq "null">
			AND (CAST(<cf_queryObjectName value="#FilterSelect#"> as #varcharCasting#) = '' OR <cf_queryObjectName value="#FilterSelect#"> is null)
		<cfelseif FilterSelectValues neq "">
			AND CAST(<cf_queryObjectName value="#FilterSelect#"> as #varcharCasting#) = <cf_queryparam value="#FilterSelectValues#" CFSQLTYPE="CF_SQL_VARCHAR" isQoQ="#inQoQ#"> 
		</cfif>
	</cfif>

	<cfif isDefined("FilterSelect1") and FilterSelect1 neq "" and isDefined("FilterSelectValues1")>
		<cfif FilterSelectValues1 eq "null">
			AND (CAST(<cf_queryObjectName value="#FilterSelect1#"> as #varcharCasting#) = '' OR <cf_queryObjectName value="#FilterSelect1#"> is null)
		<cfelseif FilterSelectValues1 neq "">
			AND CAST(<cf_queryObjectName value="#FilterSelect1#"> as #varcharCasting#) = <cf_queryparam value="#FilterSelectValues1#" CFSQLTYPE="CF_SQL_VARCHAR" isQoQ="#inQoQ#"> 
		</cfif>
	</cfif>

	<cfif isDefined("FilterSelect2") and FilterSelect2 neq "" and isDefined("FilterSelectValues2")>
		<cfif FilterSelectValues2 eq "null">
			AND (CAST(<cf_queryObjectName value="#FilterSelect2#"> as #varcharCasting#) = '' OR <cf_queryObjectName value="#FilterSelect2#"> is null)
		<cfelseif FilterSelectValues2 neq "">
			AND CAST(<cf_queryObjectName value="#FilterSelect2#"> as #varcharCasting#) = <cf_queryparam value="#FilterSelectValues2#" CFSQLTYPE="CF_SQL_VARCHAR" isQoQ="#inQoQ#"> 
		</cfif>
	</cfif>

	<cfif isDefined("FilterSelect3") and FilterSelect3 neq "" and isDefined("FilterSelectValues3")>
		<cfif FilterSelectValues3 eq "null"> <!--- 2015-10-27	ACPK	Fixed typo --->
			AND (CAST(<cf_queryObjectName value="#FilterSelect3#"> as #varcharCasting#) = '' OR <cf_queryObjectName value="#FilterSelect3#"> is null)
		<cfelseif FilterSelectValues3 neq "">
			AND CAST(<cf_queryObjectName value="#FilterSelect3#"> as #varcharCasting#) =  <cf_queryparam value="#FilterSelectValues3#" CFSQLTYPE="CF_SQL_VARCHAR" isQoQ="#inQoQ#"> 
		</cfif>
	</cfif>

	<cfif isDefined("radioFilterName") and radioFilterName neq ""
		and isDefined("radioFilterValue") and radioFilterValue neq "">
		AND <cf_queryObjectName value="#ListFirst( radioFilterName )#"> = <cf_queryparam value="#evaluate(ListFirst( radioFilterValue ))#" CFSQLTYPE="CF_SQL_VARCHAR" isQoQ="#inQoQ#">
	</cfif>

	<cfif isDefined("checkBoxFilterName") and checkBoxFilterName neq "">
		AND <cf_queryObjectName value="#checkBoxFilterName#"> = <cf_queryparam value="#evaluate( checkBoxFilterName )#" CFSQLTYPE="CF_SQL_VARCHAR" isQoQ="#inQoQ#">
	</cfif>

	<cfif isDefined("useDateRange") and useDateRange>
		<cfinclude template="tableFromQuery-QueryInclude-DateRange.cfm">
	</cfif>


	<cfif isDefined("frmTeam") and frmTeam is not "">
		and  <cf_queryObjectName value="#frmTeamColumn#">  = <cf_queryparam value="#frmTeam#" CFSQLTYPE="CF_SQL_INTEGER" >
	</cfif>

	<!--- text search parameters --->
	<cfparam name="form.frmSearchText" type="string" default="">
	<cfparam name="form.frmSearchColumn" type="string" default="">
	<cfparam name="form.frmSearchComparitor" type="string" default="">
	<cfparam name="form.frmSearchColumnDataType" type="string" default="">

	<cfif form["frmSearchText"] neq "">
		<cfswitch expression="#form["frmSearchComparitor"]#">
			<cfcase value="1"> <!--- ignore --->
			</cfcase>
			<cfcase value="2"> <!--- equals --->
				<cfif form["frmSearchColumnDataType"] eq 1>
					AND <cf_queryObjectName value="#form["frmSearchColumn"]#"> = <cf_queryparam value="#iif(IsNumeric( form["frmSearchText"] ), form["frmSearchText"], 0 )#" CFSQLTYPE="CF_SQL_NUMERIC" > 
				<cfelse>
					AND LOWER( CAST( <cf_queryObjectName value="#form["frmSearchColumn"]#"> AS #varcharCasting# ) ) =  <cf_queryparam value="#LCase( form["frmSearchText"] )#" CFSQLTYPE="CF_SQL_VARCHAR" isQoQ="#inQoQ#">
				</cfif>
			</cfcase>
			<cfcase value="3"> <!--- like --->
					AND CAST( <cf_queryObjectName value="#form["frmSearchColumn"]#"> AS #varcharCasting# ) LIKE <cf_queryparam value="%#form["frmSearchText"]#%" CFSQLTYPE="CF_SQL_VARCHAR" isQoQ="#inQoQ#">
			</cfcase>
			<cfcase value="4"> <!--- greater --->
				<cfif form["frmSearchColumnDataType"] eq 1>
					AND <cf_queryObjectName value="#form["frmSearchColumn"]#"> > <cf_queryparam value="#iif(IsNumeric( form["frmSearchText"] ), form["frmSearchText"], 0 )#" CFSQLTYPE="CF_SQL_NUMERIC" >
				<cfelse>
					AND LOWER( CAST( <cf_queryObjectName value="#form["frmSearchColumn"]#"> AS #varcharCasting# ) ) > <cf_queryparam value="#LCase( form["frmSearchText"] )#" CFSQLTYPE="CF_SQL_VARCHAR" isQoQ="#inQoQ#">
				</cfif>
			</cfcase>
			<cfcase value="5"> <!--- less --->
				<cfif form["frmSearchColumnDataType"] eq 1>
					AND <cf_queryObjectName value="#form["frmSearchColumn"]#"> < <cf_queryparam value="#iif(IsNumeric( form["frmSearchText"] ), form["frmSearchText"], "2^31" )#" CFSQLTYPE="CF_SQL_NUMERIC" >
				<cfelse>
					AND LOWER( CAST( <cf_queryObjectName value="#form["frmSearchColumn"]#"> AS #varcharCasting# ) ) < <cf_queryparam value="#LCase( form["frmSearchText"] )#" CFSQLTYPE="CF_SQL_VARCHAR" isQoQ="#inQoQ#">
				</cfif>
			</cfcase>
			<cfdefaultcase> <!--- none --->

			</cfdefaultcase>
		</cfswitch>
	</cfif>

	<!--- NJH 2013/04/23 2013 Roadmap.. if a text search box has been used, loop over the columns.. only handles search for integer and varchars..
		NJH 2015/09/30 - reworked slightly as we had an extra OR statement when last datatype was timestamp
	 --->
	<cfif structKeyExists(form,"searchStringColumnsAndTypes") and form.searchString neq "">
		<cfset firstPass = true>
		and (
		<cfloop list="#form.searchStringColumnsAndTypes#" index="colData">
			<cfif firstPass is false>
				or
			</cfif>
			<cfif listLast(colData,"|") eq "integer" and isNumeric(form.searchString)>
				<cf_queryObjectName value="#listFirst(colData,"|")#"> = <cf_queryparam value="#form.searchString#" CFSQLTYPE="CF_SQL_NUMERIC" isQoQ="#inQoQ#">
			<cfelseif listLast(colData,"|") neq "timestamp">
				<!--- START 2014/01/10			DXC			Case 441110 --->
				<!--- 2014/06/02	YMA		CASE 435829 Change search to search part words but only full words (prevents indexing). --->
				<cfif not inQoQ>
				(<cf_queryObjectName value="#listFirst(colData,"|")#"> like <cf_queryparam value="%#form.searchString##spaceCharacter#%" CFSQLTYPE="CF_SQL_VARCHAR" isQoQ="#inQoQ#">  <cfif spaceCharacter neq "">OR <cf_queryObjectName value="#listFirst(colData,"|")#"> like <cf_queryparam value="%#spaceCharacter##form.searchString#%" CFSQLTYPE="CF_SQL_VARCHAR" isQoQ="#inQoQ#"></cfif>)
				<cfelse>
				((cast(lower(<cf_queryObjectName value="#listFirst(colData,"|")#">) as #varcharCasting#) like <cf_queryparam value="%#lcase(form.searchString)##spaceCharacter#%" CFSQLTYPE="CF_SQL_VARCHAR" isQoQ="#inQoQ#"> <cfif spaceCharacter neq "">OR cast(lower(<cf_queryObjectName value="#listFirst(colData,"|")#">) as varchar) like <cf_queryparam value="%#spaceCharacter##lcase(form.searchString)#%" CFSQLTYPE="CF_SQL_VARCHAR" isQoQ="#inQoQ#"></cfif>) and <cf_queryObjectName value="#listFirst(colData,"|")#"> is not null)
				</cfif>
				<!--- END 2014/01/10			DXC			Case 441110 --->
			<cfelse>
				1 = 0	<!--- by adding this we guarantee that there is some condition, which makes it much easier to deal with the OR --->
			</cfif>
			<cfset firstPass = false>
		</cfloop>
			)
	</cfif>

	<!--- NJH PROD2015-35 - added support for date filter --->
	<cfif structKeyExists(form,"dateFilter") and (form.frmFromDate neq "" or form.frmToDate neq "")>
		<!---PROD2015-250 Correct order of dates to be lowest -> highest whichever way round the user enters them. Alter form itself so that UI updates too - RJT --->
		<cfscript>
			if (form.frmFromDate neq "" and form.frmToDate neq ""){

				smallest=DateDiff("h",form.frmFromDate,form.frmToDate)>0?form.frmFromDate:form.frmToDate;
				largest=DateDiff("h",form.frmFromDate,form.frmToDate)>0?form.frmToDate:form.frmFromDate;

				form.frmFromDate=smallest;
				form.frmToDate=largest;

			}
		</cfscript>


		and (<cfif form.frmFromDate neq "">cast(<cf_queryObjectName value="#form.dateFilter#"> as date) >= <cf_queryparam value="#form.frmFromDate#" CFSQLTYPE="cf_sql_timestamp" >   <cfif form.frmToDate neq "">and </cfif></cfif>
			<cfif form.frmToDate neq "">cast(<cf_queryObjectName value="#form.dateFilter#"> as date) <= <cf_queryparam value="#form.frmToDate#" CFSQLTYPE="cf_sql_timestamp" ></cfif>
			)
	</cfif>

	<!--- table where clause --->
	<!--- <cfparam name="form.frmWhereClauseList" type="string" default="">
	<cfloop index="whereClause" list="#form["frmWhereClauseList"]#" delimiters="|">
		#PreserveSingleQuotes( whereClause )#
	</cfloop> --->
</cfoutput>
<cfset includeFileIncluded = true>
<cfset inQoQ = false>
