<!--- �Relayware. All Rights Reserved 2014 --->
<!--- purge the selections if selections are older than 3 hours and not saved --->

<CFTRANSACTION>
	<CFSET #purgedate# = #CreateODBCDateTime(DateAdd("h",-1,Now()))#>

	<CFQUERY NAME="purgeSelectionTag" datasource="#application.siteDataSource#">
		DELETE FROM SelectionTag
		 WHERE EXISTS (
		 SELECT Selection.SelectionID FROM Selection
		 WHERE Selection.LastUpdate <=  <cf_queryparam value="#purgedate#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
		   AND Selection.Saved = 0 <!--- false --->
		   AND Selection.SelectionID = SelectionTag.SelectionID
		   )
	</CFQUERY>

	<CFQUERY NAME="purgeSelectionCriteria" datasource="#application.siteDataSource#">
		DELETE FROM SelectionCriteria
		 WHERE EXISTS (
		 SELECT Selection.SelectionID FROM Selection
		 WHERE Selection.LastUpdate <=  <cf_queryparam value="#purgedate#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
		   AND Selection.Saved = 0 <!--- false --->
		   AND Selection.SelectionID = SelectionCriteria.SelectionID
		  )
	</CFQUERY>

	<CFQUERY NAME="purgeSelections" datasource="#application.siteDataSource#">
		DELETE FROM Selection
		 WHERE LastUpdate <=  <cf_queryparam value="#purgedate#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
		   AND Saved = 0 <!--- false --->
	</CFQUERY>

</CFTRANSACTION>

