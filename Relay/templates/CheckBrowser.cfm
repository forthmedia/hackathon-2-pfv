<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
NJH 2009/05/15 Bug Fix Sony1 Support Issue 2218 - check to see if MSIE version is greater than 3, rather than looking in hard-coded list.
 --->

<cfset browser="notgood">
<cfif http_user_agent contains "MSIE">
	<!--- internet explorer --->
	
	<!--- NJH 2009/05/15 - check if IE version is greater than 4 --->
	<cfset msieString = "MSIE ">
	<cfset msiePos=find(msieString,http_user_agent)+len(msieString)>
	<cfset dotPos = find(".",http_user_agent,msiePos)>
	<cfif msiePos and dotPos>
		<cfset MSIEVersion = mid(http_user_agent,msiePos,dotPos-msiePos)>
		<cfif MSIEVersion gte 4>
			<cfset browser = "good">
		</cfif>
	</cfif>
	
	<!--- <cfif 
		http_user_agent contains "MSIE 4" 
	OR  http_user_agent contains "MSIE 5"
	OR 	http_user_agent contains "MSIE 6"
	OR 	http_user_agent contains "MSIE 7">
		<cfset browser = "good">	
	</cfif> --->
<cfelse>
	<!--- navigator --->
	<cfif MID(HTTP_USER_AGENT,9,1) gte 4>
		<cfset browser = "good">				
	</cfif>

</cfif>
<!--- If we have no browser information assume it will work --->
<cfif http_user_agent is "">
	<cfset browser = "good">
</cfif>

<!--- DO a Check for browsers less than version 4 --->
<CFIF browser IS "notgood">
<TABLE>
	<TR>
		<TD>Sorry, you require one of the following browsers to view these pages.</TD>
	</TR>
	<TR>
		<TD>&nbsp;</TD></TR>
	<TR>
		<TD><B>Netscape Navigator Version 4 or above</B></TD>
	</TR>
	<TR>
		<TD>&nbsp;</TD>
	</TR>
	<TR>	
		<TD><B>Internet Explorer Version 4 or above</B></TD>
	</TR>
	<TR>
		<TD>You have: <cfoutput>#htmleditformat(http_user_agent)#</cfoutput></TD>
	</TR>
	<TR>
		<TD>&nbsp;</TD>
	</TR>

</TABLE>
<CF_ABORT>

</CFIF>
