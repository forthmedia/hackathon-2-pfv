<!--- �Relayware. All Rights Reserved 2014 --->
<!---
		This displays a series of checkboxes for each criteria of a
		temporary selection
--->

<CFQUERY NAME="getCriteriaDescriptions" datasource="#application.siteDataSource#">
	SELECT 
		selectionfield, criteria
	FROM
		TempSelectionCriteria
	WHERE
		SelectionID = #request.relayCurrentUser.personid#
</CFQUERY>

<FONT FACE="Verdana" size="-2">
	<CFIF getCriteriaDescriptions.recordcount GT 0>
		<CFOUTPUT QUERY="getCriteriaDescriptions">
			<CF_INPUT TYPE="Checkbox" NAME="CriteriaList" VALUE="#selectionfield#">#htmleditformat(selectionfield)# #htmleditformat(criteria)#<BR>
		</CFOUTPUT>
	<CFELSE>
		NONE
	</CFIF>
</FONT>
