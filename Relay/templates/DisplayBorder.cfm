﻿<!--- ©Relayware. All Rights Reserved 2014 --->
<!---
		>>>------------------<<<
			DisplayBorder_V9
		>>>------------------<<<

		Written By SWJ Nov/Dec 02
		A complete revision of the Borders Architecture

		--------------------------------------------------------------------------
		VERSION HISTORY

		Nov 02		SWJ did a complete re-write and made borders much more controled in
					CF templates and completely database independent particularly introducing
					border_INI file to control all of the attributes in a border set.
		04/02/03 	SWJ modified to test for attributes.eid and attributes.etid so that
				 	we can be sure that eid and etid are passed from the caller.
		08/04/04  	SWJ added a surrounding table
		18/10/04	WAB	discovered that query of queries is case sensitive so that checking for elementTextID didn't work
					need to do an upper, but this crashes when it hits null values of elementtextid.  isNull doesn't seem to work in QoQ
					therefore needed to update the stored procedure to return '' rather than null
		18/03/2005	RND modified to add content padding variables to allow client to define values.
		18/03/2005	RND modified to be able to set body bottom margin
		2005		WAB - must have made numerout changes which I don't seem to have documented	- in particular to do with cacheing trees and dealing with pages which aren't available
		08/03/2006  WAB - added function to translate all phrases associated with current element
		27/04/2006	SWJ - modified control content link so that it does not show to SWJ

		April 2006  WAB
			Major rewrite so that all the getting of the current element and tree is done elsewhere (in elementtemplateV2.cfm)
		28/01/2007 AJC - P-SNY041 Dealernet Integration, Sony requirement to not have inline styles "hideInlineStylesInDisplayBorder=yes/no(default)" was created
		31/01/2008 GCC - Moved DisplayContentTableBorder table into Display border from elementTEmplateV2 for v9+ to enable padding to apply to non et.cfm pages in bordersets
		19/02/2009 NJH	Bug Fix All Sites Issue 1855 - changes stylesLoaded variable to request.stylesLoaded
	2009/04/22 WAB LID 2169 problem with right border (and therefore login form) not showing when no element on page (eg when "Please login to view this page" is displayed)
		2009/06/22 NJH 	Bug Fix FNL Issue 2181 - removed the 50 character restriction from the page title. Not sure why it was there.
		2010/01/14 NJH  LID 2294 - use a valid docType if application.settings.UI.externalDocType is 2
		2011/05/12 STCR P-LEN030 CR024 allow override of doctype for a specific borderset without affecting every portal site. Required for Lenovo Support Center Portal.
		2011/08/04 PPB 	REL109 - changed the width from "auto" to contentWidth ON borderContentAreaCustomised
		2012-01-11 NYB Case#424839 added ##bordercontentDiv to style to force priority
		2012/03/07 NJH	CASE 426986 - changed way to check if border was loaded.. use request.borderLoaded rather than checking if cfdislpayBorder was in parent tags. cfDislpayBorder was not found if displayBorder was in a translation and therefore called from a function.
		2012/12/10 IH	Case 432198 use request.currentSite.httpProtocol instead of hard coded protocol to get LinkedIN and Facebook JS files
		2012/12/10 IH	Replace hard coded Facebook appId with the proper appId from DB
		2014-10-31	WAB	Deal with excess of whitespace being generated in odd places.  Removed the CFOUTPUT which wrapped most of the code (and therefore filtered down into included templates).  Added individual cfoutputs.
						Removed the enablecfoutputonly because this is usually inherited from the calling template and doubling it up causes issues when we want to set to false
						For backwards compatibility all the includes are wrapped in a enablecfoutputonly=false
		2014/08/12 RPW	Link the Control Content link to security task
		2014/11/28 SB Used for the mobile menu, this needs to be written differently when we get time to add a mobile condition in Coldfusion
		2015/03/11 NJH	Jira Fifteen-259 - only display cookie message if the users country is not in the list of country groups to hide it for
		2016/01/05	NJH	Remove milonic code
		2016/01/19 ESZ case 447522 Both Portal hang in IE9
		ATTRIBUTES

		BorderSet
			is the name of the borderset and will call files
			in the borders content area named with the convention

			#borderset#_Header, #borderset#_Footer, #Borderset_Left#, #Borderset_Right#

		BorderSections
			is the list of Border sections to include in any order (NB case sensitive)

--->

<!--- WAB 26/02/2007
	code to prevent two bordersets being displayed nested
 --->
<!---
 NJH 2012/03/07 CASE 426986 - commented this out. changed way to check if border was loaded.. as if done in a translation (ie a function), cfdisplayBorder is not in list of parent tags
<cfset parentTags = getbasetaglist()>
<!--- this removeds 1 cf_border from the tag list and allows us to test for another --->
<cfset tagListWithoutThisCF_Border = listDeleteAt (parentTags, listfindnocase(parentTags,"cf_displayborder"))>
<cfif listfindnocase(tagListWithoutThisCF_Border,"cf_displayborder")>
	<cfexit method = "exitTemplate">
</cfif> --->

<cfif structKeyExistS(attributes,'noborders')>
	<cfset noborders = attributes.noborders>
</cfif>

<CFPARAM NAME="Debug" DEFAULT="no">
<cfif not isDefined("request.displayBorder.Borderset")>  <!--- use this variable to remember the borderset for when the end tag is run - border set may have been defined by the element itself --->
	<cfif isdefined("attributes.BorderSet") and attributes.borderset neq "">
		<cfset borderset = attributes.borderset>
	<cfelse>
		<cfset borderset = request.currentsite.borderset>
		<!---
		<cfif structKeyExists(application, "borderSetStruct") and structKeyExists(application.borderSetStruct, cgi.HTTP_HOST)>
			<cfset borderSet = application.borderSetStruct[cgi.HTTP_HOST]>
		<cfelse>
			<cfset borderset = "#application. standardBorderSet#">
		</cfif>
 		--->
	 </cfif>
	<cfset request.displayBorder.Borderset = 	borderset >
<cfelse>
	<cfset borderset = 	request.displayBorder.Borderset>
</cfif>

<cfif structKeyExists(attributes,"element")>
 			<!--- SWJ: gets the name value pairs so that the border files can test for them --->
			<cf_evaluatenamevaluepair
				namevaluepairs = #attributes.element.parameters#
			>

			<!--- If a borderset is defined in the element tree then use it--->
			<cfif attributes.element.inheritedborderset is not "">
				<cfset borderset = attributes.element.inheritedborderset>
				<cfset request.displayBorder.Borderset = borderset >  <!--- remember for end tag --->
			</cfif>
			<cfset displayBordergetThisElement = attributes.element>

</cfif>
<cfif structKeyExists(attributes,"elementTree")>
		<cfset getElements = attributes.elementTree>
		<cfquery name="getVisibleElements" dbtype="query">
			select * from getelements
			where
			ShowOnMenu = 1
		</cfquery>
</cfif>

<!--- WAB 21/6/2007 put in to deal with border sets being called from places other than elementTemplate (which will haev already loaded the element tree
	still a hack, and needs thinking
	 --->
<cfif not structKeyExists(request,"CurrentElementTree") and isdefined("request.currentsite.elementTreeDef.TopID")>
	<cfset request.CurrentElementTree = application.com.relayElementTree.getTreeForCurrentUser(topElementID=request.currentsite.elementTreeDef.TopID )>
	<cfset request.CurrentElementTreeCacheID = application.com.relayElementTree.getElementTreeCacheIDForCurrentUser(topElementID= request.currentsite.elementTreeDef.TopID)>
	<cfset request.CurrentElement = application.com.structureFunctions.queryRowToStruct(request.CurrentElementTree,1)>
	<cfset request.CurrentElement.parentidlist = "0">
	<cfset request.CurrentElement.translations = structNew()>
</cfif>

<cfif isDefined("attributes.element.parameterstructure.classOverride")>
	<cfset classOverride = attributes.element.parameterstructure.classOverride>
</cfif>

<!--- ==============================================================================
       Set the main params to control the default settings for borders to show.
	   Doing them here means that you don't need them in the border files.
=============================================================================== --->
<cfparam name="classOverride" type="string" default="lowerlevel">
<cfparam name="noborders" type="boolean" default="false">
<cfparam name="BorderSections" type="string" default="left,top,right,bottom,subHeader">
<cfparam name="borderImagePath" default="/code/borders/images"> <!--- no trailing space --->
<cfparam name="hideHeaderBorder" type="boolean" default="no">
<cfparam name="hideLeftBorder" type="boolean" default="no">
<cfparam name="hideSubHeaderBorder" type="boolean" default="no">
<cfparam name="hideRightBorder" type="boolean" default="no">
<cfparam name="bootstrapColSpan" type="string" default="col-xs-12 col-sm-12 col-md-12 col-lg-12">

<cfif structKeyExists(request,"hideRightBorder") and request.hideRightBorder> <!--- WAB added 07/06/2006 to have control when not being called within an element --->
	<cfset hideRightBorder = true>
</cfif>
<cfparam name="hideFooterBorder" type="boolean" default="no">
<!--- 28/01/2007 AJC - P-SNY041 Dealernet Integration, Sony requirement to not have inline styles --->
<cfparam name="hideInlineStylesInDisplayBorder" type="boolean" default="no">
<cfparam name="print" type="boolean" default="no">
<cfparam name="styleSheet" type="string" default="display">
<cfparam name="request.websiteHeadTitle" default="#request.CurrentSite.Title#">

<!---DEPRECATED--->
<!--- <cfparam name="showOuterTable" type="boolean" default="no">
<cfparam name="outerTableWidth" type="string" default="auto">
<cfparam name="bodyMarginWidth" type="numeric" default="0">
<cfparam name="bodyMarginHeight" type="numeric" default="0">--->
<!---END DEPRECATED--->

<!---This is the alignment of the entire page--->
<cfparam name="MainAlign" type="string" default="left">
<!---This is used as maximum width of the outer DIV--->
<cfparam name="MainWidth" type="numeric" default="900">

<!---Outer DIV border--->
<cfparam name="DisplayBorderTableBorder" type="numeric" default="0">
<cfparam name="DisplayBorderTableBorderColour" type="string" default="##000000">

<cfparam name="request.defaultExternalElementETID" default="#application.defaultExternalElementETID#">

<!---BODY LEFT AND TOP MARGIN--->
<cfparam name="bodyLeftMargin" type="numeric" default="0">
<cfparam name="bodyTopMargin" type="numeric" default="0">
<cfparam name="bodyBottomMargin" type="numeric" default="0">

<!---HEADER PARAMETERS (DESCRIPTIONS HERE APPLY TO LEFT, RIGHT
AND FOOTER, TOO)--->
<!---Vertical alignment (not yet functional)--->
<cfparam name="HeaderValign" type="string" default="Top">
<!---Horizontal alignment of the header DIV inside content--->
<cfparam name="HeaderHalign" type="string" default="left">
<!---Height of the DIV--->
<cfparam name="HeaderHeight" type="string" default="auto">
<!---Left and Right Padding inside the DIV, HAS TO BE NUMERIC DUE TO CALCULATIONS
LATER IN THE CODE--->
<cfparam name="HeaderMargin" type="numeric" default="0">
<!---Width of the DIV, HAS TO BE NUMERIC DUE TO CALCULATIONS
LATER IN THE CODE--->
<cfparam name="HeaderWidth" type="numeric" default="0">
<cfparam name="HeaderTopMargin" type="numeric" default="0">
<!---Number, will be the thickness of the border--->
<cfparam name="HeaderBorder" type="numeric" default="0">
<!---Border colour--->
<cfparam name="HeaderBorderColour" type="string" default="##000000">



<!---LEFT BORDER PARAMETERS--->
<cfparam name="LeftValign" type="string" default="Top">
<cfparam name="LeftHalign" type="string" default="left">
<cfparam name="LeftHeight" type="string" default="auto">
<cfparam name="LeftWidth" type="numeric" default="250">
<cfparam name="LeftMargin" type="numeric" default="0">
<cfparam name="LeftTopMargin" type="numeric" default="0">
<cfparam name="LeftBorder" type="numeric" default="0">
<cfparam name="LeftBorderColour" type="string" default="##000000">
<!--- RND - Added to allow user to set padding, if is not defined then will pick-up settings --->
<cfparam name="LeftContentPadding" type="numeric" default="0">

<!---RIGHT BORDER PARAMETERS--->
<cfparam name="RightValign" type="string" default="Top">
<cfparam name="RightHalign" type="string" default="left">
<cfparam name="RightHeight" type="string" default="auto">
<cfparam name="RightWidth" type="numeric" default="150">
<cfparam name="RightMargin" type="numeric" default="0">
<cfparam name="RightTopMargin" type="numeric" default="0">
<cfparam name="RightBorder" type="numeric" default="0">
<cfparam name="RightBorderColour" type="string" default="##000000">
<!--- RND - Added to allow user to set padding, if is not defined then will pick-up settings --->
<cfparam name="RightContentPadding" type="numeric" default="0">


<!---CONTENT PARAMETERS--->
<cfparam name="ContentValign" type="string" default="Top">
<cfparam name="ContentHalign" type="string" default="Left">
<cfparam name="ContentHeight" type="string" default="auto">
<cfparam name="ContentWidth" type="numeric" default="100">
<cfparam name="ContentMargin" type="numeric" default="0">
<cfparam name="ContentTopMargin" type="numeric" default="0">
<cfparam name="ContentBorder" type="numeric" default="0">
<cfparam name="ContentBorderColour" type="string" default="##000000">
<cfparam name="leftMainContentPadding" type="numeric" default="0">
<cfparam name="rightMainContentPadding" type="numeric" default="0">

<!---SUB HEADER PARAMETERS--->
<cfparam name="SubHeaderValign" type="string" default="Top">
<cfparam name="SubHeaderHalign" type="string" default="Left">
<cfparam name="SubHeaderHeight" type="string" default="auto">
<cfparam name="SubHeaderWidth" type="numeric" default="0">
<cfparam name="SubHeaderMargin" type="numeric" default="0">
<cfparam name="SubHeaderTopMargin" type="numeric" default="0">
<cfparam name="SubHeaderBorder" type="numeric" default="0">
<cfparam name="SubHeaderBorderColour" type="string" default="##000000">

<!---CONTENT AREA PARAMETERS--->
<cfparam name="ContentAreaValign" type="string" default="Top">
<cfparam name="ContentAreaHalign" type="string" default="Left">
<cfparam name="ContentAreaHeight" type="string" default="auto">
<cfparam name="ContentAreaMargin" type="numeric" default="0">
<cfparam name="ContentAreaTopMargin" type="numeric" default="0">
<cfparam name="ContentAreaBorder" type="numeric" default="0">
<cfparam name="ContentAreaBorderColour" type="string" default="##FF0000">


<!---FOOTER PARAMETERS--->
<cfparam name="FooterValign" type="string" default="Top">
<cfparam name="FooterHalign" type="string" default="Left">
<cfparam name="footerHeight" type="string" default="auto">
<cfparam name="FooterWidth" type="numeric" default="0">
<cfparam name="FooterMargin" type="numeric" default="0">
<cfparam name="FooterTopMargin" type="numeric" default="0">
<cfparam name="FooterBorder" type="numeric" default="0">
<cfparam name="FooterBorderColour" type="string" default="##000000">

<!---THESE PARAMETERS ARE DEPRECATED, BUT THEY HAVE TO BE HERE FOR BACK-COMPATIBILITY--->
<cfparam name="DisplayContentTableBorder" type="string" default="0">

<CFIF not request.relaycurrentuser.isInternal> <!--- WAB 25/01/06 Remove form.userType    <CFIF form.UserType IS "External">--->

	<cfif thistag.executionmode is "START">

		<!--- NJH 2012/03/07 CASE 426986 - changed way to check if border was loaded.. as if done in a translation, cfdisplayBorder is not in list of parent tags --->
		<cfif structKeyExists(request,"borderLoaded")>
			<cfif request.borderLoaded lte 1>
			<cfelse>
				<cfset request.borderLoaded++>
			</cfif>

		<cfelse>

			<cfset request.borderLoaded = 1>
	 <cfset frmpersonid = request.relaycurrentuser.personid>



<cf_Translate processevenifnested=true> <!--- by translating at this point we guard against CF ABORTS in the main code --->
<!--- ==============================================================================
     6. INCLUDE borderset_INI to Set various default values
=============================================================================== --->

	<cftry>
		<cfinclude template="/code/borders/#BorderSet#_INI.cfm">
		<!--- We want to trap a missing BorderSet_INI file otherwise the borders fail --->

		<cfcatch type="MissingInclude">
			<cfoutput>
			<b>DisplayBorder.cfm information</b><br>
				<cfif isdefined("SCRIPT_NAME")>Called from: #htmleditformat(request.query_string_and_script_name)#<br></cfif>
				Cannot find template #htmleditformat(cfcatch.missingFileName)#.<br>
				This is most likely because the border set is currently defined as #htmleditformat(borderset)#
				<cfif isdefined("CGI.HTTP_REFERER")>Referrer: #htmleditformat(CGI.HTTP_REFERER)#</cfif>
			</cfoutput>
		</cfcatch>
	</cftry>

<!--- ==============================================================================
       Set various variables so they can be used in the caller scope
=============================================================================== --->
	<cfset caller.DisplayContentTableBorder = DisplayContentTableBorder>
	<cfset caller.ContentWidth = ContentWidth>
	<cfset caller.ContentMargin = ContentMargin>
	<cfset caller.ContentTopMargin = ContentTopMargin>

<!--- ==============================================================================
       Set up some styles values
=============================================================================== --->
<cfset styleContentWidth = contentWidth+leftWidth+RightWidth>

<!--- ==============================================================================
       Start main HTML page
=============================================================================== --->

<!--- NJH 2010/01/14 LID 2294 - use new valid docType if using externalDocType version 2. This is set in relayVar ---><!--- STCR 2011-05-12 P-LEN023 CR024 override doctype for SupportCenter Portal borderset without affecting portal sites which require quirks mode for the old borderset. --->
<!--- <cfif application.com.settings.getSetting ("UI.PortalDocType") is "Standards" or (isDefined("request.currentsite.forceStandardsDocType") and request.currentsite.forceStandardsDocType) >
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<cfelse>
	<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
</cfif>
 --->
<cfif not (application.com.settings.getSetting ("UI.PortalDocType") is "Standards" or (isDefined("request.currentsite.forceStandardsDocType") and request.currentsite.forceStandardsDocType))>
	<cfset request.document.docType = '<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">'>
</cfif>

<cfset socialMedia = application.com.settings.getSetting("socialMedia")>

<cfparam name="request.currentelement.translations.headline" default="">
<cfparam name="request.currentelement.translations.summary" default="">

<cf_head>
<cfoutput>
	<cf_title><cfif application.testSite and not application.com.relayCurrentSite.isEnvironment('Demo')>Test </cfif>#request.websiteHeadTitle# | <cfif isdefined('displaybordergetthiselement.translations.headline')>#DisplayBorderGetThisElement.translations.headline#<cfelseif structKeyExists(attributes,"pageTitle")>#attributes.pageTitle#</cfif></cf_title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1,maximum-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!--- NJH 2008/06/12 Lexmark Bug 130 - use the translated title rather than the normal title --->
	<!--- NJH 2009/06/22 Bug Fix FNL Issue 2181 - replaced #left(DisplayBorderGetThisElement.translations.headline,50)# with DisplayBorderGetThisElement.translations.headline --->
	<cfif socialMedia.enableSocialMedia>
		<cfset eidParam = "">
		<cfif structKeyExists(url,"eid")>
			<cfset eidParam = "/?eid=#url.eid#">
		</cfif>
		<cfset elementLinkFileName = "linkImage-" & request.currentElement.ID & "-thumb.jpg">
		<cfset linkImagePath=application.paths.content & "\elementLinkImages\#elementLinkFileName#">
		<cfset ogTitle = request.currentelement.translations.headline>
		<cfif not request.relayCurrentUser.isLoggedIn and structKeyExists(url,"epl")>
			<cfset ogTitle = "phr_social_partnerPortalTitle">
		</cfif>
		<meta property="og:title" content="#htmleditformat(ogTitle)#">
		<meta property="og:type" content="website">
		<meta property="og:url" content="#request.currentSite.protocolAndDomain##htmleditformat(eidParam)#">
		<cfset vendorLogoQry = application.com.relatedFile.getFileDetailsByEntityID(entityID=application.com.settings.getSetting("theClient.clientOrganisationID"),entityType="organisation",FileCategory="logos")>
		<cfif vendorLogoQry.recordCount and fileExists(vendorLogoQry.fileHandle[1])>
		<meta property="og:image" content="#vendorLogoQry.webHandle[1]#">
		</cfif>
		<cfif fileExists(linkImagePath)>
		<meta property="og:image" content="/content/elementLinkImages/#elementLinkFileName#"/>
		</cfif>
		<cfset ogDescription = request.currentelement.translations.summary>
		<cfif not request.relayCurrentUser.isLoggedIn and structKeyExists(url,"epl")>
			<cfset ogDescription = "phr_social_partnerPortalDescription">
		</cfif>
		<meta property="og:description" content="#htmleditformat(ogDescription)#">
	</cfif>

	<!---
	START	MS	2011/07/01	Adding a hook for Lenovo to force the page to render as properly-this was done to fix
	the look and feel of My Certifications page and also to make the collapsable rows work properly
	 --->
	<cf_include template="/code/Borders/portalCustomHeader.cfm" checkIfExists="true">
	<!--- End	MS	2011/07/01 --->

	<relayPage name="displayBorder"></relayPage>
	<cf_includeJavascriptOnce template = "/javascript/openWin.js">

	<cfparam name="bootstrapColClass" default="col-md-6">
	<cfif listFindNoCase(bordersections,"Left") and not listFindNoCase(bordersections,"right")>
		<cfset bootstrapColClass = "col-xs-12 col-sm-8 col-md-9 col-lg-9">
	</cfif>
	<cfif listFindNoCase(bordersections,"right") and not listFindNoCase(bordersections,"left")>
		<cfset bootstrapColClass = "col-xs-12 col-sm-8 col-md-9 col-lg-9">
	</cfif>
	<cfif listFindNoCase(bordersections,"Left") and listFindNoCase(bordersections,"right")>
		<cfset bootstrapColClass = "col-xs-12 col-sm-8 col-md-6 col-lg-6">
	</cfif>
	<cfif not listFindNoCase(bordersections,"Left") and not listFindNoCase(bordersections,"right") or noborders>
		<cfset bootstrapColClass = "col-xs-12 col-sm-12 col-md-12 col-lg-12">
	</cfif>

	<!--- CSS IE HACKS--->
	<!--[if IE 9]><link href="/code/styles/ie9.css" rel="stylesheet" type="text/css" media="screen"><![endif]-->
	<!--[if IE 8]><link href="/code/styles/ie8.css" rel="stylesheet" type="text/css" media="screen"><![endif]-->
	<!--[if IE 7]><link href="/code/styles/ie7.css" rel="stylesheet" type="text/css" media="screen"><![endif]-->
	<!--- <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300italic,400,400italic,600,600italic" rel="stylesheet" type="text/css"> --->
	<!--- Override for when borders are not being displayed e.g. in modal popup.  Use to remove styling componants that conflict with popup styling --->
	<cfif noborders>
		<cf_includeCssOnce template="/code/styles/noborders-override.css"/>
	</cfif>
	<!--- JQUERY MOBILE --->
	<cf_includeJavascriptOnce template="/javascript/lib/jquery/jquery.mobile.js"/> <!--- NJH 2016/01/28 - moved here as part of Case 447522. Order is important apparently, although I'm not sure what it needs to come before! --->
	<!--- RESPOND JS--->
	<cf_includeJavascriptOnce template="/javascript/lib/respond.js"/>
	<!---
	WAB 2014-12-03  Moved general.js into the core and renamed to bordersGeneral.s
					left general.js as a client specific extension --->
	<cf_includeJavascriptOnce template="/javascript/bordersGeneral.js"/>
	<!---NJH 2016/05/19 JIRA PROD2016-1148 - should be included in above statement <cf_includeJavascriptOnce template="/code/borders/script/general.js"/>--->
	<!--- COOKIES MESSAGE--->
	<cf_includeJavascriptOnce template="/javascript/lib/jquery/jquery.cookie.js"/>
	<!--- BOOTSTRAP
		2014-10-21 WAB moved order of bootstrap.js
		Although previously listed after respond.js it was not being loaded using htmlhead so was actually appearing in the HMTL source at this point
	 --->
	<cf_includeJavascriptOnce template="/javascript/bootstrap/js/bootstrap.js"/>

	<!--- NJH 2016/01/29 - BF-397 really don't want this in here. Ideally, calls should be made to cf_modalDialog which should then take care of these two lines. Have left
		in here for now as there are still calls done to fancy box outside of cf_modalDialog --->
	<cf_includeCssOnce template="/javascript/lib/fancybox/jquery.fancybox.css">
	<cf_includeJavascriptOnce template="/javascript/lib/fancybox/jquery.fancybox.pack.js"/>



	<!--- HTML5SHIV --->
	<!--[if lt IE 9]>
	<script src="/code/borders/script/html5shiv.js"></script>
	<![endif]-->
	<!--- 2014/05/20	YMA	Fix bootstrap incompatibility with prototype --->
	<script>
		(function() {
		    var isBootstrapEvent = false;
		    if (window.jQuery) {
		        var all = jQuery('*');
		        jQuery.each(['hide.bs.dropdown',
		            'hide.bs.collapse',
		            'hide.bs.modal',
		            'hide.bs.tooltip',
		            'hide.bs.popover'], function(index, eventName) {
		            all.on(eventName, function( event ) {
		                isBootstrapEvent = true;
		            });
		        });
		    }
		    var originalHide = Element.hide;
		    Element.addMethods({
		        hide: function(element) {
		            if(isBootstrapEvent) {
		                isBootstrapEvent = false;
		                return element;
		            }
				return originalHide(element);
				}
			});
		})();
	</script>
</cfoutput>
</cf_head>

<cfset classString = "#classOverride#">
<cfif structKeyExists(request.currentelement,'CONTENTID')>
 	<cfset classString = "element_#request.currentelement.contentid# #classOverride#">
</cfif>
<cf_body id="bodyLogged#request.relayCurrentUser.isLoggedIn?'In':'Out'#" class="#classString#">	 <!--- 13/06/08 JvdW - Added id to body tag to be able to use different logged-in and logged-out styles on the same selectors --->

<cfparam name="UseRelayElementTreeFunctions" type="boolean" default="No">

<cfset showOuterTable="0">

<!--- In the Start Mode Include the Top, Left and Right border sections if required --->
<!--- Begin: displayBorderBorder_outerTable --->
<cfoutput>
<!-- 28/11/14 SB Used for the mobile menu, this needs to be written differently when we get time to add a mobile condition in Coldfusion -->
<div id="navbarCollapse" class="hidden-sm hidden-md hidden-lg mobile-wrapper-sidebar">
	<cfinclude template="/code/borders/relayBorders_nav.cfm">
</div>
<div id="wrapper" class="mobile-wrapper notActive">

	<!--- NJH 2015/03/11 Jira Fifteen-259 - only display cookie message if the users country is not in the list of country groups to hide it for --->
	<cfif not noborders and not application.com.relayCountries.isCountryInCountryGroup(country=request.relayCurrentUser.content.showForCountryID,countryGroup=application.com.settings.getSetting("portal.hideCookieMessageForCountryGroups"))>
		<div id="cookiesContainer" style="display:none;">
			<div class="cc-cookies container row-no-padding"><div class="col-xs-12 col-sm-9 col-md-9 col-lg-10">phr_cookies_module_name</div><div class="col-xs-12 col-sm-3 col-md-3 col-lg-2"><button type="button" class="cookiesAccept btn btn-primary">phr_cookies_link_name</button></div></div>
		</div>
	</cfif>

	<div class="overlay"></div>
	<div id="headerContainer">
		<div id="headerContainerInner"></cfoutput>
	<cfif listfindnocase(bordersections,"Top",",") and hideHeaderBorder eq "no" and not noborders>
		<!--- Begin: displayBorderBorder_Header --->
			<cfsetting enablecfoutputonly="false" >
			<cfinclude template="/code/borders/#BorderSet#_Header.cfm">
			<cfsetting enablecfoutputonly="true" >
	</cfif>

	<cfoutput>
			</div>
		</div>
		<div id="contentContainer" class="container">
			<div id="contentWrapper" class="row"><!--- 10/06/08 JvdW - Added an extra wrapper around the left, content and right borders for Sophos --->
	</cfoutput>


	<cfif listfindnocase(bordersections,"Right",",") and hideRightBorder eq "no" and not noborders>
		<!--- RMC: 26/09/2014 - Prevent login box from displaying on redirect screens --->
		<cfif not structkeyexists(request,"elementnotfoundredirect") or not request.elementnotfoundredirect>
		<CFOUTPUT>
			<!--- <div class="hidden-md hidden-lg col-xs-12">
				<a href="##" data-toggle="collapse" data-target="##borderRightDiv">Toggle Right Border</a>
			</div> --->
			<aside id="contentRightWrapper" class="contentRightWrapper col-xs-12 col-sm-4 col-md-3 col-lg-3 pull-right">
				<div class="box box-shadow">
				</CFOUTPUT>
			<cfsetting enablecfoutputonly="false" >
			<cfinclude template="/code/borders/#BorderSet#_Right.cfm">
			<cfsetting enablecfoutputonly="true" >
		<CFOUTPUT></div></aside></CFOUTPUT>
	</cfif>
	</cfif>

	<cfif listfindnocase(bordersections,"Left",",") and hideLeftBorder eq "no" and not noborders>
		<cfoutput>
			<aside id="contentLeftWrapper" class="contentLeftWrapper hidden-xs col-xs-12 col-sm-4 col-md-3 col-lg-3">
				<div class="box box-shadow">
		</cfoutput>
			<cfsetting enablecfoutputonly="false" >
			<cfinclude template="/code/borders/#BorderSet#_Left.cfm">
			<cfsetting enablecfoutputonly="true" >
		<cfoutput></div></aside></cfoutput>
	</cfif>

	<cfoutput><section id="contentInnerWrapper" class="contentInnerWrapper #bootstrapColClass#"><div class="lowerLevelbox lowerLevelbox-shadow"></cfoutput>
	<cfoutput><div id="message">#application.com.relayUI.displayMessage()#</div></cfoutput>

	<cfif listfindnocase(bordersections,"subHeader",",") and hideSubHeaderBorder eq "no" and not noborders>
		<cfoutput><header id="innerContentHeader" class="innerContentHeader row"></cfoutput>
			<cfsetting enablecfoutputonly="false" >
			<cfinclude template="/code/borders/#BorderSet#_SubHeader.cfm">
			<cfsetting enablecfoutputonly="true" >
		<cfoutput></header></cfoutput>
	</cfif>

	<!--- 2006/02/07 GCC - bit Hacky but it works - if right border is being used and it is not hidden don't allow
	the content div to spill into it - much more graceful display of excessively wide HTML content pages--->
	<cfoutput>
	<div id="innerContentContainer" class="innerContentContainer row">			<!--- 2008/01/31 GCC moved to displayBorderV* as an experiment so it is included for allportal content not just et.cfm comtent (proc, orders etc) --->
		<div id="innerContentColumns" class="#bootstrapColSpan#">
			<div id="embedContent">
		<!--- Begin: Main content Table --->
	</CFOUTPUT>
	</cf_Translate>
</cfif>

<cfelseif thistag.executionmode is "END">

	<cfif request.borderLoaded eq 1>
		<cf_Translate processevenifnested=true>

		<!--- START process, rating and content editing link sections ---->
		<cfif request.currentelement.id neq "">
			<cfif isdefined("request.processAction") and request.processAction.continueButton.show>
				<cfoutput>
					<form action = "/proc.cfm" method="post"><!--- would be nice if this was et.cfm, but not up to it yet --->
						<CF_INPUT type="HIDDEN" name="frmProcessID" value="#request.processAction.ProcessID#|#request.processAction.nextStepID#">
						<CF_INPUT type="HIDDEN" name="frmProcessUUID" value="#request.processAction.UUID#">
						<CF_INPUT type="submit" name="frmContinue" value="#request.processAction.continueButton.Label#">
						<cfif request.processAction.cancelButton.show>
							<CF_INPUT type="submit" name="frmCancel" value="#request.processAction.cancelButton.Label#">
						</cfif>
					</form>
				</cfoutput>
			</cfif>
			<!--- rating --->
			<cfinclude template="/elements/ratingTask.cfm">
			<cfif request.currentelement.rate is 1>
				<cfoutput>
					<form >
						<!--- 2013-03-08 - RMB - 434141 - CHANGED VAR 'elementid' to 'request.currentElement.id' --->
						<CF_INPUT type="hidden" name="elementid" value="#request.currentElement.ID#">
						<CF_INPUT type="hidden" name="frmPersonid" value="#request.relayCurrentUser.personid#">
						<cfinclude template="/elements/Rating.cfm">
					</form>
				</cfoutput>
			</cfif>
			<!---	***************************************
						Show editing link
					***************************************--->

			<!--- 2014/08/12 RPW	Link the Control Content link to security task --->
			<cfscript>
				variables.isContentEditor = application.com.login.checkInternalPermissions(securityTask="ElementTask", securityLevel="LEVEL3");
			</cfscript>
			<cfif variables.isContentEditor and application.com.rights.Element(request.currentelement.id,2).hasrights>
				<cfset OKtoEdit = true>
			<cfelse>
				<cfset OKtoEdit = false>
			</cfif>
			<CFIF variables.isContentEditor and not noborders>
				<CFOUTPUT>
					<div class="row">
						<div class="col-xs-12">
							<ul id="editLinks">
								<cfif OKtoEdit><li><A HREF="javascript:editElement(#request.currentelement.ID#)" class="btn btn-default">Edit this page</a></li></cfif>
								<cfif request.relaycurrentUser.Content.ShowLinkToControlContent>
									<cfoutput><li><A HREF="javascript:void(newWindow = window.open ('\/open\/controlcontentDisplay.cfm', 'ControlContent','height=400,width=400,location=0,resizable=1,scrollbars=1')); newWindow.focus()" class="btn btn-default">Control content</A></li></cfoutput>
								</cfif>
							</ul>
						</div>
					</div>
				</CFOUTPUT>
			</cfif>

		</cfif>
		<!--- END processes and content editing link ---->

		<CFOUTPUT>
									</div><!--- embedContent --->
								</div><!--- innerContentColumns --->
							</div><!--- borderContentDiv --->
						</div>
					</section><!---  borderContentArea --->
				</div><!--- contentWrapper --->
		 	</div><!--- contenContainer --->
		</CFOUTPUT>
			<cfif listfindnocase(bordersections,"Bottom",",") and hideFooterBorder eq "no" and not noborders>
				<!--- Begin: Bottom Border table --->
				<CFOUTPUT><div id="footerContainer">
					<div id="footerContainerInner" class="container">
					<footer id="footerWrapper" class="footerWrapper"></CFOUTPUT>
					<cfsetting enablecfoutputonly="false" >
					<cfinclude template="/code/borders/#BorderSet#_Footer.cfm">
					<cfsetting enablecfoutputonly="true" >
				<CFOUTPUT></footer></div></div></CFOUTPUT>
			</cfif>
		<CFOUTPUT>
		</div><!--- Wrapper --->

		</CFOUTPUT>

	<!--- 2007/06/21 GCC - dump out our major request structures if url.requestDump is true and you are on dev or test or coming from this IP to live --->
	<cfif structkeyexists(url,"requestDump") and url.requestDump and (CGI.REMOTE_ADDR eq "62.232.221.162" or application.testSite neq 0)>
		current site<BR>
		<cfdump var="#request.currentsite#">
		current element<BR>
		<cfdump var="#request.currentelement#">
		current user<BR>
		<cfdump var="#request.relaycurrentuser#">
	</cfif>

		</cf_Translate>
</cfif>

		<cfset request.borderLoaded-->
</cfif>
</cfif>