<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 2012-08-10 PPB Case 428651 don't set the EndCustomerOppContacts flag for internal user --->

<cfif not application.com.security.confirmFieldsHaveBeenEncrypted(fieldNames="frmOrganisationID,personID")>
      <CF_ABORT>
</cfif>

<cfif personID neq 0>
	<!--- security measure: get the list of contacts at the organisation so that we can check whether the person we are editing is in the list and if so grant temporary rights  --->
	<cfset qryContacts = application.com.opportunity.getOpportunityEndCustomerContacts(organisationID=frmOrganisationID)>
</cfif>

<!--- <cfdump var="#qryContacts#"> --->

<!--- if we're adding a new person or if the person is in the list of contacts that the partner has had dealings with, then give temporary access --->
<cfif personID eq 0 or ListFind(ValueList(qryContacts.personID),personID) gt 0>
	<cfset application.com.rights.giveTemporaryRights("organisation",frmOrganisationID)>
	<cfset variables.giveTemporaryRights = true>
	<cfset variables.tempOrganisationID = frmOrganisationID>
<cfelse>
     <CF_ABORT>
</cfif>


	<!--- when called from opportunity edit, this function will refresh the contacts drop down --->
	<cfoutput>
		<script language="javascript">
		      function refreshParentDropDowns(organisationID,personID) {
		      		//PPB add on a test for 'object' to get it to work in IE 
		            if (!window.parent.closed && (typeof window.parent.refreshEntityContacts == 'function' || typeof window.parent.refreshEntityContacts == 'object')) {		
		                  window.parent.refreshEntityContacts(organisationID,personID)
		            }
		      }
		</script>
	</cfoutput>

	<cfparam name="url.personid" default="0">
      
    <cfinclude template="/relaytagtemplates/managePeopleEditScreenIframe.cfm">

 	<cfif structKeyExists(form,"doUpdate") and form.doUpdate>

		<cfif url.personid eq 0 and isdefined("PERSON_LOCATIONID_NEW")>
			<cfquery name="getInsertedPerson" datasource="#application.siteDataSource#">
					SELECT max(PersonID) as ID
					FROM Person
					WHERE LocationID =  <cf_queryparam value="#PERSON_LOCATIONID_NEW#" CFSQLTYPE="CF_SQL_INTEGER" > 
					  and email =  <cf_queryparam value="#PERSON_EMAIL_NEW#" CFSQLTYPE="CF_SQL_VARCHAR" > 
					  and firstname =  <cf_queryparam value="#PERSON_FIRSTNAME_NEW#" CFSQLTYPE="CF_SQL_VARCHAR" > 
					  and lastname =  <cf_queryparam value="#PERSON_LASTNAME_NEW#" CFSQLTYPE="CF_SQL_VARCHAR" > 
			</cfquery>
			<!--- 2012-08-10 PPB Case 428651 now we also set the EndCustomerOppContacts flag in opportunityTask.cfm 
			this one is still required for when the user clicks Add New Contact button against an existing opp on the Portal - we want a newly added contact to have the flag so that they appear in the contact dropdown on the opp (ie before we have saved the opp)
			it is not relevant on the internal because we want to associate the reseller sales person (not current user) to the new contact added
			 --->
			<cfif not request.relaycurrentuser.isInternal > <!--- 2012-08-10 PPB Case 428651 this condition added to avoid extra flags being created on the internal even tho they don't do any harm --->
				<cfset application.com.flag.setFlagData(flagId='EndCustomerOppContacts',entityid=getInsertedPerson.ID,data=request.relaycurrentuser.personid)>
			</cfif>
		</cfif>

		<cfif structKeyExists(url,"mode") and left(url.mode,6) eq "Portal" >
		<cfoutput>
			<script type="text/javascript">
	            if (!window.parent.closed && typeof window.parent.refreshPage == 'function') {
	           	window.parent.refreshPage();
	            }
				parent.jQuery.fancybox.close();
			</script>
		</cfoutput>
		<cfelse>
			<cfif isDefined("personIDsAdded")>
           		<cfoutput>
					<script type="text/javascript">
					      refreshParentDropDowns(#jsStringFormat(frmOrganisationid)#,#jsStringFormat(personIDsAdded)#);
						  parent.jQuery.fancybox.close();
					</script>
         		</cfoutput>
			</cfif>
		</cfif>	
	</cfif>

<!--- 
      <cfif structKeyExists(form,"doUpdate") and form.doUpdate and isDefined("personIDsAdded")>
            <cfoutput>
                  <script>
                        refreshParentDropDowns(#frmOrganisationid#,#personIDsAdded#);
                  </script>
            </cfoutput>
      </cfif>
 --->