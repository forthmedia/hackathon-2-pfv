﻿<!--- ©Relayware. All Rights Reserved 2014 --->
<!---  
	relay\templates\qryCreateSelectionV2.cfm

	this is the body of the query which generate selections
	you need to provide the SELECT bit.
	It is used by selections/selectsum.cfm selections/selectrefresh.cfm scheduled/refresh.cfm
 --->

<!--- Mods:
WAB  created feb 00
WAB 160200 		Changed postalcode range stuff  problem with trying to convert numeric postal code to sql_real
WAB 2000-04-20  	added handling of "exclude/include locations contained in these selections"
WAB 2000-04-20  	added handling of UK postal code regions"
WAB 2000-07-06  	added hack to allow search for blank postalCodes and email addresses.  Type 'null' in the search field
WAB 2000-09-14   	copied location flag code to create organisation flagcode
WAB 2000-10-18  	added handling of address5 (in this case for South African regions)
?? 2000-11-08		Added frmOrganisatioName 
WAB 2004-09-21  	Added include / exclude organisations in selection
WAB 2005-02-21 	Add frmOrganisationID and frmExOrganisationID
MDC 2005-08-19 	Changed the way Jobdescription is being called as it was being lost when selections were freshed.
WAB 2005-11-29 	Fully Added IntegerMultiple support (there were bits but wasn't done fully)
WAB 2007-02-28   	test for organisation.active added
WAB 2008/05/07 	changed aliasing on main query - com/selections.cfc; templates/qryCreateSelection.cfm; selections/selectSum.cfm must be updated at same time
NYF 2008/05/07 	added Address 4 (aka city) and SQL search for project P-TND009 Partner Profile Report  
SSS 2008/09/19 	bug 1095:- I have added the fact that in the selection critC is not required if critB is not between

NJH 2008/10/20  Created V2 to use inner joins rather than 'in' statements. This was spurred on by a lexmark bug (Lexmark Supprot Issue 1170)
NYB	2009-02-02	SNY-Bug1703 & Len-Bug1704
NJH 2009/02/13 	P-SNY047 - added quizzes to selections.
GCC 2009/04/15	LID: 1975 corrected issue introduced in V2 - further comments below.
GCC 2009/04/28  LID:2157 added a few missing with(nolock) comments for including / excluding other selections
WAB 2009/05/27  altered search on organisation created date.  Get rid of view
NJH 2009/06/25	Bug Fix Sony1 Support Issue 2214 - added frmOrganisationFrom and frmOrganisationTo to allow searching using the between functionality on org names 
NAS	2011/05/16	LID5949 added Creation Date for Person & Modified Date and Modified By for Org
PPB 2012/02/12	Case 426125 use rwListGetAt instead of ListGetAt to defend against empty strings in the list
PJP 2012-07-12	Case#429052 added new table inner join trngModuleCourse 
PPB 2012-11-28 Case 432223 use selection owner for rights snippet
WAB 2012-12-06 CASE 432390 Problems with selections corrupting during refresh.  Had to var function selections.refreshSelection() including all variables used in this template.  Could not var the explicit variables scope, so had to remove references to it.
NJH 2013/01/08 2013 Roadmap - add support for decimal and textMultiple flags.
--->

<!---
		These parameters are not always passed
--->
<CFPARAM name="flagListPerson" default="">
<CFPARAM name="flagListLocation" default="">
<CFPARAM name="flagListOrganisation" default="">

<CFIF ListContainsNoCase(savedFields,"frmInLocSelection",application.delim1) IS NOT 0>
	<CFSET crit = application.com.globalFunctions.rwListGetAt(savedCriteria,ListContainsNoCase(savedFields,"frmInLocSelection",application.delim1),application.delim1)>
	
	<cfquery name="incLoc" datasource="#application.siteDataSource#">
		if object_id('tempdb..##incLocSelection') is not null
			begin
			   drop table ##incLocSelection
			end
			
			select locationID into ##incLocSelection from (
				SELECT locationID FROM person p with (noLock) inner join SelectionTag st with (noLock) on st.entityid = p.personid 
					WHERE st.SelectionID #PreserveSingleQuotes(crit)# AND st.Status > 0
			) base 
	</cfquery>
</CFIF>

<CFIF ListContainsNoCase(savedFields,"frmExLocSelection",application.delim1) IS NOT 0>
	<CFSET crit = application.com.globalFunctions.rwListGetAt(savedCriteria,ListContainsNoCase(savedFields,"frmExLocSelection",application.delim1),application.delim1)>
	
	<cfquery name="excLoc" datasource="#application.siteDataSource#">
		if object_id('tempdb..##excLocSelection') is not null
			begin
			   drop table ##excLocSelection
			end
			
			select locationID into ##excLocSelection from (
				SELECT locationID FROM person p with (noLock) inner join SelectionTag st with (noLock) on st.entityid = p.personid 
				WHERE st.SelectionID #PreserveSingleQuotes(crit)# AND st.Status > 0
			) base
	</cfquery>
</CFIF>

<CFIF ListContainsNoCase(savedFields,"frmInOrgSelection",application.delim1) IS NOT 0>
	<CFSET crit = application.com.globalFunctions.rwListGetAt(savedCriteria,ListContainsNoCase(savedFields,"frmInOrgSelection",application.delim1),application.delim1)>

	<cfquery name="incOrg" datasource="#application.siteDataSource#">
		if object_id('tempdb..##incOrgSelection') is not null
			begin
			   drop table ##incOrgSelection
			end
			
			select organisationid into ##incOrgSelection from (
				SELECT organisationid FROM person p with (noLock) inner join SelectionTag st with (noLock) on st.entityid = p.personid 
				WHERE st.SelectionID #PreserveSingleQuotes(crit)# AND st.Status > 0
			) base
	</cfquery>
</CFIF>

<CFIF ListContainsNoCase(savedFields,"frmExOrgSelection",application.delim1) IS NOT 0>
	<CFSET crit = application.com.globalFunctions.rwListGetAt(savedCriteria,ListContainsNoCase(savedFields,"frmExorgSelection",application.delim1),application.delim1)>
	
	<cfquery name="excOrg" datasource="#application.siteDataSource#">
		if object_id('tempdb..##excOrgSelection') is not null
			begin
			   drop table ##excOrgSelection
			end
			
			select organisationid into ##excOrgSelection from (
				SELECT organisationid FROM person p with (noLock) inner join SelectionTag st with (noLock) on st.entityid = p.personid 
				WHERE st.SelectionID #PreserveSingleQuotes(crit)# AND st.Status > 0
			) base
	</cfquery>
</CFIF>


<cfprocessingdirective suppresswhitespace="yes">
<cfsavecontent variable="selectionQrySnippet_sql">
<CFOUTPUT>
 
 FROM 

			 Organisation as o with (noLock)
				inner join
 			 Location AS l with (noLock) on o.organisationid = l.organisationid
			 	inner join
			 Person AS p with (noLock) on l.locationid = p.locationid

			<!--- 
			WAB 2012-07-09 P-SMA001 Country (and custom) Rights now done using getRightsFilterQuerySnippet()
			--->
			#application.com.rights.getRightsFilterQuerySnippet(entityType="location",alias="l",personId=selectionOwnerPersonId,isinternal=1).join#		<!--- PPB 2012-11-28 Case 432223 add selectionOwnerPersonId --->
			
			<!--- 2010/08/20 SKP inner join critera SNY-100--->
			<CFIF ListContainsNoCase(savedFields,"frmSQLQueryJoin",application.delim1) IS NOT 0>
					<CFSET crit = application.com.globalFunctions.rwListGetAt(savedCriteria,ListContainsNoCase(savedFields,"frmSQLQueryJoin",application.delim1),application.delim1)>
					#PreserveSingleQuotes(crit)#
			</CFIF>
			
			 <CFIF ListContainsNoCase(savedFields,"frmDataSource",application.delim1) IS NOT 0>
					<CFSET crit = application.com.globalFunctions.rwListGetAt(savedCriteria,ListContainsNoCase(savedFields,"frmDataSource",application.delim1),application.delim1)>
				 inner join LocationDataSource AS j with (noLock) on l.LocationID = j.LocationID
					  AND j.DataSourceID #PreserveSingleQuotes(crit)#
			</CFIF>

			<!--- START	NAS	2011/05/16	LID5949 added Creation Date for Person & Modified Date and Modified By for Org --->
			<CFIF (ListContainsNoCase(savedFields,"frmOrganisationCreatedby",application.delim1) IS NOT 0) 
			    OR (ListContainsNoCase(savedFields,"frmPersCreatedby",application.delim1) IS NOT 0)>
				 inner join UserGroup AS u with (noLock) on u.UserGroupID = 
				
				<CFIF ListContainsNoCase(savedFields,"frmOrganisationCreatedby",application.delim1) IS NOT 0>
					o.CreatedBy  
					<CFSET crit = application.com.globalFunctions.rwListGetAt(savedCriteria,ListContainsNoCase(savedFields,"frmOrganisationCreatedby",application.delim1),application.delim1)>
				<cfelse>
					p.CreatedBy 
					<CFSET crit = application.com.globalFunctions.rwListGetAt(savedCriteria,ListContainsNoCase(savedFields,"frmPersCreatedby",application.delim1),application.delim1)>
				</CFIF>
				  AND u.Name #PreserveSingleQuotes(crit)#				
			 </CFIF>
			<!--- STOP	NAS	2011/05/16	LID5949 added Creation Date for Person & Modified Date and Modified By for Org --->	
				
			 <CFIF (ListContainsNoCase(savedFields,"frmOrganisationLastUpdatedby",application.delim1) IS NOT 0) 
			 	OR(ListContainsNoCase(savedFields,"frmLocationLastUpdatedby",application.delim1) IS NOT 0)
			    OR (ListContainsNoCase(savedFields,"frmPersLastUpdatedby",application.delim1) IS NOT 0)>
				 inner join UserGroup AS k with (noLock) on k.UserGroupID = 
				<!--- 2009/04/15 GCC added crit values for frmPersLastUpdatedby and frmLocationLastUpdatedby which had gone missing in conversion from V1 to V2 --->
				<!--- START	NAS	2011/05/16	LID5949 added Creation Date for Person & Modified Date and Modified By for Org --->
				<CFIF ListContainsNoCase(savedFields,"frmOrganisationLastUpdatedby",application.delim1) IS NOT 0>
					o.LastUpdatedBy  
					<CFSET crit = application.com.globalFunctions.rwListGetAt(savedCriteria,ListContainsNoCase(savedFields,"frmOrganisationLastUpdatedby",application.delim1),application.delim1)>
				<!--- STOP	NAS	2011/05/16	LID5949 added Creation Date for Person & Modified Date and Modified By for Org --->
				<CFelseIF ListContainsNoCase(savedFields,"frmLocationLastUpdatedby",application.delim1) IS NOT 0>
					l.LastUpdatedBy  
					<CFSET crit = application.com.globalFunctions.rwListGetAt(savedCriteria,ListContainsNoCase(savedFields,"frmLocationLastUpdatedby",application.delim1),application.delim1)>
				<cfelse>
					p.LastUpdatedBy 
					<CFSET crit = application.com.globalFunctions.rwListGetAt(savedCriteria,ListContainsNoCase(savedFields,"frmPersLastUpdatedby",application.delim1),application.delim1)>
				</CFIF>
				  AND k.Name #PreserveSingleQuotes(crit)#				
			 </CFIF>



<!--- 
WAB 2012-07-09 P-SMA001 
Country (and custom) Rights now done above using getRightsFilterQuerySnippet()
Don't need join to userGroupCountry
			<!--- link in the countryGroup table, except when frmCountryGroup is defined in which case it is doing an IN statement --->
			<CFIF ListContainsNoCase(savedFields,"frmCountryGroup",application.delim1) IS 0>
				 inner join usergroupcountry as ugc with (noLock) on l.countryid = ugc.countryid
			</cfif>
--->

			 <cfif isDefined("FRMPRODCONTRACTSTARTDATE") and len(FRMPRODCONTRACTSTARTDATE) OR isDefined("FRMPRODCONTRACTENDDATE") and len(FRMPRODCONTRACTENDDATE)>
			 	inner join ProdContract as pc with (noLock) AND pc.personID = p.personID
			 </cfif>
			 
			 <!--- NJH 2008/09/19 elearning 8.1 - added specialisations to selections --->
			<CFIF ListContainsNoCase(savedFields,"frmSpecialisationID",application.delim1) IS NOT 0>
				<CFSET crit = application.com.globalFunctions.rwListGetAt(savedCriteria,ListContainsNoCase(savedFields,"frmSpecialisationID",application.delim1),application.delim1)>
				inner join entitySpecialisation es with (noLock) on es.entityID = o.organisationID and es.entityTypeID = 2 and es.specialisationID #PreserveSingleQuotes(crit)#
			</CFIF>
			
			<!--- NJH 2007/11/12 - select people that have done particular training courses/modules --->
			<CFIF (ListContainsNoCase(savedFields,"frmCourseSeries",application.delim1) IS NOT 0) or (ListContainsNoCase(savedFields,"frmSolutionArea",application.delim1) IS NOT 0)>
				inner join trngUserModuleProgress tump with (noLock) on tump.personID = p.personID 
				inner join trngModule tm with (noLock) on tm.moduleID = tump.moduleID 
				inner join trngModuleCourse tmc with (noLock) on tmc.moduleID = tm.moduleID <!--- PJP 2012-07-11 Case 429052 added inner join to trngModuleCourse ---->
				inner join trngCourse tc with (noLock) on tmc.courseID = tc.courseID
				<CFIF ListContainsNoCase(savedFields,"frmCourseSeries",application.delim1) IS NOT 0>
					<CFSET crit = application.com.globalFunctions.rwListGetAt(savedCriteria,ListContainsNoCase(savedFields,"frmCourseSeries",application.delim1),application.delim1)>
					<cfif not findNoCase("null",crit)>
						and tc.courseSeriesID #PreserveSingleQuotes(crit)#
					</cfif>
				</CFIF>
				<cfif ListContainsNoCase(savedFields,"frmSolutionArea",application.delim1) IS NOT 0>
					<CFSET crit = application.com.globalFunctions.rwListGetAt(savedCriteria,ListContainsNoCase(savedFields,"frmSolutionArea",application.delim1),application.delim1)>
					<cfif not findNoCase("null",crit)>
						and tc.solutionAreaID #PreserveSingleQuotes(crit)#
					</cfif>
				</cfif>
			</CFIF>
		
			<!--- NJH 2008/09/19 elearning 8.1 - added certifications to selections --->
			<CFIF ListContainsNoCase(savedFields,"frmCertificationID",application.delim1) IS NOT 0>
				<CFSET crit = application.com.globalFunctions.rwListGetAt(savedCriteria,ListContainsNoCase(savedFields,"frmCertificationID",application.delim1),application.delim1)>
				
				<cfset statusCrit = "">
				<CFIF ListContainsNoCase(savedFields,"frmCertificationStatusID",application.delim1) IS NOT 0>
					<CFSET statusCrit = application.com.globalFunctions.rwListGetAt(savedCriteria,ListContainsNoCase(savedFields,"frmCertificationStatusID",application.delim1),application.delim1)>
				</CFIF>
				inner join trngPersonCertification tpc with (noLock) on p.personID = tpc.personID and tpc.certificationID #PreserveSingleQuotes(crit)#
				<cfif not findNoCase("null",statusCrit)>
					and statusID #PreserveSingleQuotes(statusCrit)#
				</cfif>
			</CFIF>
			
			<!--- NJH 2009/02/13 P-SNY047 - added quizzes--->
			<CFIF ListContainsNoCase(savedFields,"frmQuizDetailID",application.delim1) IS NOT 0>
				<CFSET crit = application.com.globalFunctions.rwListGetAt(savedCriteria,ListContainsNoCase(savedFields,"frmQuizDetailID",application.delim1),application.delim1)>
				inner join quizTaken qt with (noLock) on qt.personID = p.personID
				inner join quizDetail qd with (noLock) on qt.quizDetailID = qd.quizDetailID
				<cfif not findNoCase("null",crit)>
					and qd.quizDetailID #PreserveSingleQuotes(crit)#
				</cfif>
					and qt.completed is not null
				<!--- get only quizzes that have been passed --->
				<CFIF ListContainsNoCase(savedFields,"frmQuizPass",application.delim1) IS NOT 0 and ListContainsNoCase(savedFields,"frmQuizFail",application.delim1) IS 0>
					and qt.score >= qd.passMark
				<CFELSEIF ListContainsNoCase(savedFields,"frmQuizFail",application.delim1) IS NOT 0 and ListContainsNoCase(savedFields,"frmQuizPass",application.delim1) IS 0>
					and qt.score < qd.passMark
				</CFIF>
			</CFIF>
			
			<CFIF ListContainsNoCase(savedFields,"frmInSelection",application.delim1) IS NOT 0>
				<CFSET crit = application.com.globalFunctions.rwListGetAt(savedCriteria,ListContainsNoCase(savedFields,"frmInSelection",application.delim1),application.delim1)>
			  	inner join SelectionTag personSelection_inc with (noLock)on personSelection_inc.entityID =p.personID and personSelection_inc.SelectionID #PreserveSingleQuotes(crit)# AND personSelection_inc.Status > 0
			</CFIF>
			
			<CFIF ListContainsNoCase(savedFields,"frmExSelection",application.delim1) IS NOT 0>
				<CFSET crit = application.com.globalFunctions.rwListGetAt(savedCriteria,ListContainsNoCase(savedFields,"frmExSelection",application.delim1),application.delim1)>
				left join SelectionTag personSelection_exc with (noLock)on personSelection_exc.entityID =p.personID and personSelection_exc.SelectionID #PreserveSingleQuotes(crit)# AND personSelection_exc.Status > 0
			</CFIF>
			
			<CFIF ListContainsNoCase(savedFields,"frmInLocSelection",application.delim1) IS NOT 0>
			 	inner join ##incLocSelection locSelection_inc
					on locSelection_inc.locationID = p.locationID
			</CFIF>
			
			<CFIF ListContainsNoCase(savedFields,"frmExLocSelection",application.delim1) IS NOT 0>
			  	left join ##excLocSelection locSelection_exc
					on locSelection_exc.locationID = p.locationID
			</CFIF>
			
			<CFIF ListContainsNoCase(savedFields,"frmInOrgSelection",application.delim1) IS NOT 0>
				inner join ##incOrgSelection orgSelection_inc
					on orgSelection_inc.organisationID = p.organisationID
			</CFIF>
			
			<CFIF ListContainsNoCase(savedFields,"frmExOrgSelection",application.delim1) IS NOT 0>
				left join ##excOrgSelection orgSelection_exc
					on orgSelection_exc.organisationID = p.organisationID
			</CFIF>
			
			<CFIF ListFindNoCase(savedFields,"frmOrgLastContactDate",application.delim1) IS NOT 0>
			 	inner join vOrganisationsWeHaveCommunicatedWithSinceX vOrgComm with (noLock) on vOrgComm.organisationID = o.organisationID
			</CFIF>
			 
			<cfset eventFlagTableJoins="">

			<!--- START: NYB 2009-02-02 SNY-Bug1703 & Len-Bug1704 - added: --->
			<cfset delimiter = application.delim1>
			<!--- END: NYB 2009-02-02 SNY-Bug1703 & Len-Bug1704 --->

			<!--- NYB 2009-02-02 SNY-Bug1703 & Len-Bug1704 - and replaced application.delim1 with delimiter --->
			<cfloop index = "i"	from = 1 to = "#listLen(savedFields,delimiter)#">
				<cfset selectionField = application.com.globalFunctions.rwListGetAt(savedFields,i,delimiter)>
				<cfif selectionField contains "frmFlag">
					<cfset criteria = application.com.globalFunctions.rwListGetAt (savedCriteria1,i,delimiter)>
					<cfset flagType = application.com.globalFunctions.rwListGetAt(selectionField,2,"_")>	
					<cfif flagType  is "radio" or flagType  is "checkbox">
						<cfset flagGroupID = application.com.globalFunctions.rwListGetAt(selectionField,3,"_")>
						<cfset flagGroupStructure = application.com.flag.getflagGroupStructure(flagGroupId)>
						inner join booleanflagdata as bfd_#flagGroupID# on bfd_#flagGroupID#.entityId = #left(flagGroupStructure.entityType.tablename,1)#.#flagGroupStructure.entityType.uniquekey#   and bfd_#flagGroupID#.flagid in (#criteria#)
					<cfelse>
						<cfset joins = application.com.flag.getJoinInfoForFlag(flagid = application.com.globalFunctions.rwListGetAt(selectionField,3,"_"), joinType="Inner",returnLinkedEntity = false)>
						<cfif flagType contains "integer" or flagType eq "decimal">
							#joins.join# and  #joins.tableAlias#.data in (#criteria#)
						<cfelseif flagType contains "text">
							#joins.join# and  #joins.tableAlias#.data in (#listQualify(criteria,"'")#)
						<cfelseif flagType is "date">
							#joins.join# and  #joins.tableAlias#.data = #criteria#
						<cfelseif flagType is not "event">
							#joins.join# and  #joins.tableAlias#.data like '#criteria#%'
						<cfelse>
							<!--- events flag data... criteria held differently 
								there could be 2 or more fields per event... such as regstatus and wave.
								So, if we've joined on the eventflag table to get the regstatus, we don't want to join again to the table to get the wave.
								The form field that gets all the event data is 'FRMFLAG_EVENT_1759' and the others are 'FRMFLAG_EVENT_1759_REGSTATUS' and 'FRMFLAG_EVENT_1759_WAVE' for example
								WAB I think that this relies on the flags appearing one after each other in the list of criteria - likely but not guaranteed
							--->
							<cfif not listFindNoCase(eventFlagTableJoins,joins.tableAlias)>
								#joins.join#
								<cfset eventFlagTableJoins = listAppend(eventFlagTableJoins,joins.tableAlias)>
							</cfif>
								<cfif listLen(selectionField,"_") gte 4>
									 and  #joins.tableAlias#.#application.com.globalFunctions.rwListGetAt(selectionField,4,"_")# in ('#criteria#')
								 <!--- WAB 2012-10-29 CASE 431540 removed this section, because this join had already been added 5 lines higher up.
								 			Slightly odd that this had not cause an error previously 
								<cfelseif listLen(selectionField,"_") eq 3>
									#joins.join#
								  --->
								</cfif>
	
						</cfif>
					</cfif>					
				</cfif>
			</cfloop>
 
		WHERE	p.Active =1 AND l.Active =1 AND o.Active =1    <!--- organisation active test added WAB 2007-02-28 --->
		
			<!--- MDC 2005-08-19 removed see notes above
			<cfif isDefined("frmPersJobDesc") and frmPersJobDesc neq "">
				AND LOWER(p.JobDesc) like ('#lcase(frmPersJobDesc)#')
			</cfif> --->
			<CFIF ListContainsNoCase(savedFields,"frmPersJobDesc",application.delim1) IS NOT 0>
				<CFSET crit = application.com.globalFunctions.rwListGetAt(savedCriteria,ListContainsNoCase(savedFields,"frmPersJobDesc",application.delim1),application.delim1)>
				AND (p.JobDesc #PreserveSingleQuotes(crit)#  )
			</CFIF>
		

<!--- 		KT 1999-10-11 replaced with preceding function - we always want only active people and locations
		<cfif isDefined("frmpersinactive")>
			p.Active <> -1 AND l.Active <> -1
		<cfelse>
			p.Active <> 0 AND l.Active <> 0
		</cfif>
 --->
		
		<cfif isDefined("FRMPRODCONTRACTSTARTDATE") and len(FRMPRODCONTRACTSTARTDATE)>
			AND pc.ContractEndDate >= #CreateODBCDate(FRMPRODCONTRACTSTARTDATE)#
		</cfif>
		
		<cfif isDefined("FRMPRODCONTRACTENDDATE") and len(FRMPRODCONTRACTENDDATE)>
			AND pc.ContractEndDate <= #CreateODBCDate(FRMPRODCONTRACTENDDATE)#
		</cfif>
		
		<CFIF ListContainsNoCase(savedFields,"frmExSelection",application.delim1) IS NOT 0>
			AND personSelection_exc.entityID is null
		</CFIF>
		
		<CFIF ListContainsNoCase(savedFields,"frmExLocSelection",application.delim1) IS NOT 0>
			AND locSelection_exc.locationID is null
		</CFIF>
		
		<CFIF ListContainsNoCase(savedFields,"frmExOrgSelection",application.delim1) IS NOT 0>
			AND orgSelection_exc.organisationID is null
		</CFIF>
		
		<CFIF ListFindNoCase(savedFields,"frmOrgCreateDate",application.delim1) IS NOT 0>
			
			<CFSET crita = application.com.globalFunctions.rwListGetAt(savedCriteria,ListFindNoCase(savedFields,"frmOrgCreateDate",application.delim1),application.delim1)>
			<CFSET critb = application.com.globalFunctions.rwListGetAt(savedCriteria,ListFindNoCase(savedFields,"frmOrgCreateContactDateWhen",application.delim1),application.delim1)>
<!--- SSS 2008/09/19 bug 1095 --->
			<CFIF critb IS "between">
				<CFSET critC = application.com.globalFunctions.rwListGetAt(savedCriteria,ListFindNoCase(savedFields,"frmOrgCreateDateEnd",application.delim1),application.delim1)>
			</cfif>
<!--- SSS 2008/09/19 bug 1095 --->
			<!--- 
			WAB 2009/05/27 god knows why using a view here, it is just a field on the organisation table.  Think is a GADism
			replaced with simpler query below
			<CFIF critb IS "before">
				<CFSET op = chr(60)>
				AND (vocd.MaxCreateDate #op# CONVERT(DATETIME, '#PreserveSingleQuotes(crita)#', 102))
			<CFELSEIF critb IS "after">
				<CFSET op = chr(62) & "=">
				AND (vocd.MaxCreateDate #op# CONVERT(DATETIME, '#PreserveSingleQuotes(crita)#', 102))
			<CFELSEIF critb IS "between">
				AND (vocd.MaxCreateDate BETWEEN CONVERT(DATETIME, '#PreserveSingleQuotes(crita)#', 102) AND CONVERT(DATETIME, '#PreserveSingleQuotes(critc)# 23:59:59', 102))
			</CFIF> --->

			<CFIF critb IS "before">
				<CFSET op = chr(60)>
			<CFELSEIF critb IS "after">
				<CFSET op = chr(62) & "=">
			<CFELSEIF critb IS "between">
				<CFSET op = "between">
			</CFIF>
			
			AND o.created #op# CONVERT(DATETIME, '#PreserveSingleQuotes(crita)#', 102) 	<CFIF critb IS "between">AND CONVERT(DATETIME, '#PreserveSingleQuotes(critc)# 23:59:59', 102)</cfif> 

		</CFIF>
		
		<CFIF ListFindNoCase(savedFields,"frmOrgLastContactDate",application.delim1) IS NOT 0>
			<CFSET crita = application.com.globalFunctions.rwListGetAt(savedCriteria,ListFindNoCase(savedFields,"frmOrgLastContactDate",application.delim1),application.delim1)>
			<CFSET critb = application.com.globalFunctions.rwListGetAt(savedCriteria,ListFindNoCase(savedFields,"frmOrgLastContactDateWhen",application.delim1),application.delim1)>

			<CFIF critb IS "before"><CFSET op = chr(60)><CFELSEIF critb IS "after"><CFSET op = chr(62) & "="></CFIF>
			AND (vOrgComm.MaxCommDetailSentDate #op# CONVERT(DATETIME, '#PreserveSingleQuotes(crita)#', 102))
		</CFIF>
		
		<CFIF ListContainsNoCase(savedFields,"frmCountryGroup",application.delim1) IS NOT 0>
			<CFSET crit = application.com.globalFunctions.rwListGetAt(savedCriteria,ListContainsNoCase(savedFields,"frmCountryGroup",application.delim1),application.delim1)>
			AND l.CountryID #PreserveSingleQuotes(crit)#
		<CFELSEIF ListContainsNoCase(savedFields,"frmCountryID",application.delim1) IS NOT 0>
			AND l.CountryID #application.com.globalFunctions.rwListGetAt(savedCriteria,ListContainsNoCase(savedFields,"frmCountryID",application.delim1),application.delim1)#
<!---
WAB 2012-07-09 P-SMA001 
Country (and custom) Rights now done above using getRightsFilterQuerySnippet()
			<!--- WAB: replace with join to userGroupCountryTable AND l.CountryID IN (#Variables.CountryList#) --->
 			and ugc.usergroupid = #usergroupid# --->
	<!--- 	<CFELSE>
			<!--- WAB: replace with join to userGroupCountryTable AND l.CountryID IN (#Variables.CountryList#) --->
			and ugc.usergroupid = #usergroupid# 
--->			
		</CFIF>
		

		<CFIF ListContainsNoCase(savedFields,"frmOrganisationName",application.delim1) IS NOT 0>
			<CFSET crit = application.com.globalFunctions.rwListGetAt(savedCriteria,ListContainsNoCase(savedFields,"frmOrganisationName",application.delim1),application.delim1)>
			AND (o.OrganisationName #PreserveSingleQuotes(crit)#  )
		</CFIF>
		
		<!--- NJH 2009/06/24 Bug Fix Sony1 Support Issue 2214 - added frmOrganisationFrom and frmOrganisationTo --->
		<CFIF ListContainsNoCase(savedFields,"frmOrganisationFrom",application.delim1) IS NOT 0>
			<CFSET crit = application.com.globalFunctions.rwListGetAt(savedCriteria,ListContainsNoCase(savedFields,"frmOrganisationFrom",application.delim1),application.delim1)>
			AND (o.OrganisationName #PreserveSingleQuotes(crit)#)
		</CFIF>

		<CFIF ListContainsNoCase(savedFields,"frmOrganisationTo",application.delim1) IS NOT 0>
			<CFSET crit = application.com.globalFunctions.rwListGetAt(savedCriteria,ListContainsNoCase(savedFields,"frmOrganisationTo",application.delim1),application.delim1)>
			AND (o.OrganisationName #PreserveSingleQuotes(crit)#)
		</CFIF>
		
		<CFIF ListContainsNoCase(savedFields,"frmOrganisationTypeID",application.delim1) IS NOT 0>
			<CFSET crit = application.com.globalFunctions.rwListGetAt(savedCriteria,ListContainsNoCase(savedFields,"frmOrganisationTypeID",application.delim1),application.delim1)>
			AND (o.OrganisationTypeID #PreserveSingleQuotes(crit)#  )
		</CFIF>

		<CFIF ListContainsNoCase(savedFields,"frmorganisationID",application.delim1) IS NOT 0>
			<CFSET crit = application.com.globalFunctions.rwListGetAt(savedCriteria,ListContainsNoCase(savedFields,"frmorganisationID",application.delim1),application.delim1)>
			AND o.organisationid #PreserveSingleQuotes(crit)#
		</CFIF>

		<CFIF ListContainsNoCase(savedFields,"frmExorganisationID",application.delim1) IS NOT 0>
			<CFSET crit = application.com.globalFunctions.rwListGetAt(savedCriteria,ListContainsNoCase(savedFields,"frmExorganisationID",application.delim1),application.delim1)>
			AND o.organisationid #PreserveSingleQuotes(crit)#
		</CFIF>
		
		
		

		<CFIF ListContainsNoCase(savedFields,"frmRegionState",application.delim1) IS NOT 0>
			<CFSET crit = application.com.globalFunctions.rwListGetAt(savedCriteria,ListContainsNoCase(savedFields,"frmRegionState",application.delim1),application.delim1)>
			AND (l.Address3 #PreserveSingleQuotes(crit)#
			  	OR l.Address4 #PreserveSingleQuotes(crit)#
				OR l.Address5 #PreserveSingleQuotes(crit)#
				OR l.nAddress3 #PreserveSingleQuotes(crit)#
			  	OR l.nAddress4 #PreserveSingleQuotes(crit)#
				OR l.nAddress5 #PreserveSingleQuotes(crit)#
				)
		</CFIF>

		<!--- NYF 2008/05/07 P-TND009 Partner Profile Report  --->
		<CFIF ListContainsNoCase(savedFields,"frmAddress4",application.delim1) IS NOT 0>
			<CFSET crit = application.com.globalFunctions.rwListGetAt(savedCriteria,ListContainsNoCase(savedFields,"frmAddress4",application.delim1),application.delim1)>
		  AND l.Address4 #PreserveSingleQuotes(crit)#
		</CFIF>
		

		<CFIF ListContainsNoCase(savedFields,"frmAddress5",application.delim1) IS NOT 0>
			<CFSET crit = application.com.globalFunctions.rwListGetAt(savedCriteria,ListContainsNoCase(savedFields,"frmAddress5",application.delim1),application.delim1)>
			AND (l.Address5 #PreserveSingleQuotes(crit)# )
		</CFIF>


		<CFIF ListContainsNoCase(savedFields,"frmCompanyName",application.delim1) IS NOT 0>
			<CFSET crit = application.com.globalFunctions.rwListGetAt(savedCriteria,ListContainsNoCase(savedFields,"frmCompanyName",application.delim1),application.delim1)>
			AND (l.SiteName #PreserveSingleQuotes(crit)# or l.nSiteName #PreserveSingleQuotes(crit)# )
		</CFIF>
		
		<CFIF ListContainsNoCase(savedFields,"frmAccountTypeID",application.delim1) IS NOT 0>
			<CFSET crit = application.com.globalFunctions.rwListGetAt(savedCriteria,ListContainsNoCase(savedFields,"frmAccountTypeID",application.delim1),application.delim1)>
			AND (l.accountTypeID #PreserveSingleQuotes(crit)#  )
		</CFIF>
		
		<CFIF ListContainsNoCase(savedFields,"frmCompanyFrom",application.delim1) IS NOT 0>
			<CFSET crit = application.com.globalFunctions.rwListGetAt(savedCriteria,ListContainsNoCase(savedFields,"frmCompanyFrom",application.delim1),application.delim1)>
			AND (l.SiteName #PreserveSingleQuotes(crit)# or l.nSiteName #PreserveSingleQuotes(crit)# )
		</CFIF>

		<CFIF ListContainsNoCase(savedFields,"frmCompanyTo",application.delim1) IS NOT 0>
			<CFSET crit = application.com.globalFunctions.rwListGetAt(savedCriteria,ListContainsNoCase(savedFields,"frmCompanyTo",application.delim1),application.delim1)>
			AND (l.SiteName #PreserveSingleQuotes(crit)# or l.nSiteName #PreserveSingleQuotes(crit)# )
		</CFIF>

		<CFIF ListContainsNoCase(savedFields,"frmLocationID",application.delim1) IS NOT 0>
			<CFSET crit = application.com.globalFunctions.rwListGetAt(savedCriteria,ListContainsNoCase(savedFields,"frmLocationID",application.delim1),application.delim1)>
			AND l.Locationid #PreserveSingleQuotes(crit)#
		</CFIF>

		<CFIF ListContainsNoCase(savedFields,"frmExLocationID",application.delim1) IS NOT 0>
			<CFSET crit = application.com.globalFunctions.rwListGetAt(savedCriteria,ListContainsNoCase(savedFields,"frmExLocationID",application.delim1),application.delim1)>
			AND l.Locationid #PreserveSingleQuotes(crit)#
		</CFIF>

		
		<CFIF ListContainsNoCase(savedFields,"frmPartnerType",application.delim1) IS NOT 0>
			<CFSET crit = application.com.globalFunctions.rwListGetAt(savedCriteria,ListContainsNoCase(savedFields,"frmPartnerType",application.delim1),application.delim1)>
			AND l.LocationID = h.LocationID
			AND h.LookupID #PreserveSingleQuotes(crit)#
		</CFIF>


		<CFIF ListContainsNoCase(savedFields,"frmPersLanguage",application.delim1) IS NOT 0>
			<CFSET crit = application.com.globalFunctions.rwListGetAt(savedCriteria,ListContainsNoCase(savedFields,"frmPersLanguage",application.delim1),application.delim1)>
			<CFSET crit = replaceNoCase(crit,"'null'","''","ONE")>
		  AND p.Language #PreserveSingleQuotes(crit)#
		</CFIF>
		<CFIF ListContainsNoCase(savedFields,"frmPersEmail",application.delim1) IS NOT 0>
			<CFSET crit = application.com.globalFunctions.rwListGetAt(savedCriteria,ListContainsNoCase(savedFields,"frmPersEmail",application.delim1),application.delim1)>
			<CFSET crit = replaceNoCase(crit,"'null'","''","ONE")>
		  AND p.Email #PreserveSingleQuotes(crit)#
		</CFIF>
		<CFIF ListContainsNoCase(savedFields,"frmEmailStatus",application.delim1) IS NOT 0>
			<CFSET crit = application.com.globalFunctions.rwListGetAt(savedCriteria,ListContainsNoCase(savedFields,"frmEmailStatus",application.delim1),application.delim1)>
		  AND (isNull(p.EmailStatus,0)  #PreserveSingleQuotes(crit)# )
		</CFIF>

		<CFIF ListContainsNoCase(savedFields,"frmPostalCodetext",application.delim1) IS NOT 0>
			<CFSET crit = application.com.globalFunctions.rwListGetAt(savedCriteria,ListContainsNoCase(savedFields,"frmPostalCodetext",application.delim1),application.delim1)>
			<CFSET crit = replaceNoCase(crit,"'null'","''","ONE")>
		  AND l.PostalCode #PreserveSingleQuotes(crit)#
		<CFELSEIF (ListContainsNoCase(savedFields,"frmPostalCodefrom",application.delim1) IS NOT 0)
			AND (ListContainsNoCase(savedFields,"frmPostalCodeto",application.delim1) IS NOT 0)>
			<CFSET crita = application.com.globalFunctions.rwListGetAt(savedCriteria,ListContainsNoCase(savedFields,"frmPostalCodefrom",application.delim1),application.delim1)>
			<CFSET critb = application.com.globalFunctions.rwListGetAt(savedCriteria,ListContainsNoCase(savedFields,"frmPostalCodeto",application.delim1),application.delim1)>

<!--- 	WAB/PMMC removed this, the convert function doesn't seem right (ie it crashes )	
			<CFIF IsNumeric(crita) AND IsNumeric(critb)>
			  AND {fn CONVERT(l.PostalCode,sql_real)} BETWEEN #crita# AND #critb#
			<CFELSE>
			  AND l.PostalCode BETWEEN '#crita#' AND '#critb#'
			</CFIF>
 --->		
 			  AND l.PostalCode BETWEEN '#crita#' AND '#critb#'
		 </CFIF>
		<CFIF ListContainsNoCase(savedFields,"frmUKPostalCode",application.delim1) IS NOT 0>
			<CFSET crit = application.com.globalFunctions.rwListGetAt(savedCriteria,ListContainsNoCase(savedFields,"frmUKPostalCode",application.delim1),application.delim1)>
			AND #crit#
		 </CFIF>

		<CFIF ListContainsNoCase(savedFields,"frmUKTerritory",application.delim1) IS NOT 0>
			<CFSET crit = application.com.globalFunctions.rwListGetAt(savedCriteria,ListContainsNoCase(savedFields,"frmUKTerritory",application.delim1),application.delim1)>
			AND #crit#
		 </CFIF>

		<CFIF ListContainsNoCase(savedFields,"frmLocationFax",application.delim1) IS NOT 0>
			<CFSET crit = application.com.globalFunctions.rwListGetAt(savedCriteria,ListContainsNoCase(savedFields,"frmLocationFax",application.delim1),application.delim1)>
		  AND l.Fax #PreserveSingleQuotes(crit)#
		</CFIF>
		<CFIF ListContainsNoCase(savedFields,"frmDirectPartner",application.delim1) IS NOT 0>
			<CFSET crit = application.com.globalFunctions.rwListGetAt(savedCriteria,ListContainsNoCase(savedFields,"frmDirectPartner",application.delim1),application.delim1)>
		  AND l.Direct #PreserveSingleQuotes(crit)#
		</CFIF>
		<!--- START	NAS	2011/05/16	LID5949 added Creation Date for Person & Modified Date and Modified By for Org --->
		<CFIF ListContainsNoCase(savedFields,"frmOrganisationLastUpdateddate",application.delim1) IS NOT 0>
			<CFSET crita = application.com.globalFunctions.rwListGetAt(savedCriteria,ListContainsNoCase(savedFields,"frmOrganisationLastUpdateddate",application.delim1),application.delim1)>
			<CFSET critb = application.com.globalFunctions.rwListGetAt(savedCriteria,ListContainsNoCase(savedFields,"frmOrganisationLastUpdatedWhen",application.delim1),application.delim1)>
			<CFIF critb IS "before"><CFSET op = chr(60)><CFELSEIF critb IS "after"><CFSET op = chr(62)></CFIF>
		  AND o.LastUpdated #op# #CreateODBCDateTime(PreserveSingleQuotes(crita))#
		</CFIF>
		<!--- STOP	NAS	2011/05/16	LID5949 added Creation Date for Person & Modified Date and Modified By for Org --->
 		<CFIF ListContainsNoCase(savedFields,"frmPersLastUpdateddate",application.delim1) IS NOT 0>
			<CFSET crita = application.com.globalFunctions.rwListGetAt(savedCriteria,ListContainsNoCase(savedFields,"frmPersLastUpdateddate",application.delim1),application.delim1)>
			<CFSET critb = application.com.globalFunctions.rwListGetAt(savedCriteria,ListContainsNoCase(savedFields,"frmPersLastUpdatedWhen",application.delim1),application.delim1)>
			<CFIF critb IS "before"><CFSET op = chr(60)><CFELSEIF critb IS "after"><CFSET op = chr(62)></CFIF>
 		  AND p.LastUpdated #op# #CreateODBCDateTime(PreserveSingleQuotes(crita))#
		</CFIF>
	 	<CFIF ListContainsNoCase(savedFields,"frmLocationLastUpdateddate",application.delim1) IS NOT 0>
			<CFSET crita = application.com.globalFunctions.rwListGetAt(savedCriteria,ListContainsNoCase(savedFields,"frmLocationLastUpdateddate",application.delim1),application.delim1)>
			<CFSET critb = application.com.globalFunctions.rwListGetAt(savedCriteria,ListContainsNoCase(savedFields,"frmLocationLastUpdatedWhen",application.delim1),application.delim1)>
			<CFIF critb IS "before"><CFSET op = chr(60)><CFELSEIF critb IS "after"><CFSET op = chr(62)></CFIF>
		  AND l.LastUpdated #op# #CreateODBCDateTime(PreserveSingleQuotes(crita))#
		</CFIF>
		<CFIF ListFindNoCase(savedFields,"frmPersNametext",application.delim1) IS NOT 0>
			<CFSET crita = application.com.globalFunctions.rwListGetAt(savedCriteria,ListContainsNoCase(savedFields,"frmPersNametext","#application.delim1#"),"#application.delim1#")>
			<CFSET critb = application.com.globalFunctions.rwListGetAt(savedCriteria,ListContainsNoCase(savedFields,"frmPersNameType","#application.delim1#"),"#application.delim1#")>
		  AND
		   (<CFIF critb IS "first">p.FirstName<CFELSEIF critb IS "last">p.LastName<CFELSEIF critb IS "full">p.FirstName + ' ' + p.LastName<CFELSE>p.LastName</CFIF> #PreserveSingleQuotes(crita)#
		   OR <CFIF critb IS "first">p.nFirstName<CFELSEIF critb IS "last">p.nLastName<CFELSEIF critb IS "full">p.nFirstName + ' ' + p.nLastName<CFELSE>p.LastName</CFIF> #PreserveSingleQuotes(crita)#)
		</CFIF>
		<CFIF ListContainsNoCase(savedFields,"frmPersSex",application.delim1) IS NOT 0>
			<CFSET crit = application.com.globalFunctions.rwListGetAt(savedCriteria,ListContainsNoCase(savedFields,"frmPersSex","#application.delim1#"),"#application.delim1#")>
			<CFSET crit = replaceNoCase(crit,"'null'","''","ONE")>
			AND p.Sex #PreserveSingleQuotes(crit)#
		</CFIF>			

		<CFIF ListContainsNoCase(savedFields,"frmPointsBalanceFrom",application.delim1) IS NOT 0>
			<CFSET crit = application.com.globalFunctions.rwListGetAt(savedCriteria,ListContainsNoCase(savedFields,"frmPointsBalanceFrom","#application.delim1#"),"#application.delim1#")>
			AND dbo.fn_RWGetPointsBalancePersonal (p.organisationid,p.personid,getdate())  #PreserveSingleQuotes(crit)#
		</CFIF>			

		<CFIF ListContainsNoCase(savedFields,"frmPointsBalanceTo",application.delim1) IS NOT 0>
			<CFSET crit = application.com.globalFunctions.rwListGetAt(savedCriteria,ListContainsNoCase(savedFields,"frmPointsBalanceTo","#application.delim1#"),"#application.delim1#")>
			AND dbo.fn_RWGetPointsBalancePersonal (p.organisationid,p.personid,getdate())  #PreserveSingleQuotes(crit)#
		</CFIF>
		
<!--- SWJ 2001-07-25 Added to suppress blank emails --->
		<CFIF ListContainsNoCase(savedFields,"frmSupressBlankEmailAddress",application.delim1) IS NOT 0>
			AND email is not NULL and email <> ' '
		</CFIF>

		<CFIF ListContainsNoCase(savedFields,"frmPersonID",application.delim1) IS NOT 0>
			<CFSET crit = application.com.globalFunctions.rwListGetAt(savedCriteria,ListContainsNoCase(savedFields,"frmPersonID",application.delim1),application.delim1)>
			AND p.Personid #PreserveSingleQuotes(crit)#
		</CFIF>

		<CFIF ListContainsNoCase(savedFields,"frmExPersonID",application.delim1) IS NOT 0>
			<CFSET crit = application.com.globalFunctions.rwListGetAt(savedCriteria,ListContainsNoCase(savedFields,"frmExPersonID",application.delim1),application.delim1)>
			AND p.Personid #PreserveSingleQuotes(crit)#
		</CFIF>

		<!--- START	NAS	2011/05/16	LID5949 added Creation Date for Person & Modified Date and Modified By for Org --->
		<CFIF ListFindNoCase(savedFields,"frmPersCreateDate",application.delim1) IS NOT 0>
			
			<CFSET crita = application.com.globalFunctions.rwListGetAt(savedCriteria,ListFindNoCase(savedFields,"frmPersCreateDate",application.delim1),application.delim1)>
			<CFSET critb = application.com.globalFunctions.rwListGetAt(savedCriteria,ListFindNoCase(savedFields,"frmPersCreateContactDateWhen",application.delim1),application.delim1)>

			<CFIF critb IS "between">
				<CFSET critC = application.com.globalFunctions.rwListGetAt(savedCriteria,ListFindNoCase(savedFields,"frmPersCreateDateEnd",application.delim1),application.delim1)>
			</cfif>

			<CFIF critb IS "before">
				<CFSET op = chr(60)>
			<CFELSEIF critb IS "after">
				<CFSET op = chr(62) & "=">
			<CFELSEIF critb IS "between">
				<CFSET op = "between">
			</CFIF>
			
			AND p.created #op# CONVERT(DATETIME, '#PreserveSingleQuotes(crita)#', 102) 	<CFIF critb IS "between">AND CONVERT(DATETIME, '#PreserveSingleQuotes(critc)# 23:59:59', 102)</cfif> 

		</CFIF>
		<!--- STOP	NAS	2011/05/16	LID5949 added Creation Date for Person & Modified Date and Modified By for Org --->

		<!--- NYF 2008/05/07 P-TND009 Partner Profile Report  --->
		<CFIF ListContainsNoCase(savedFields,"FrmSQLQuery",application.delim1) IS NOT 0>				
			AND #PreserveSingleQuotes(frmSQLQuery)#
		</CFIF>
	</cfoutput>
</cfsavecontent>
</cfprocessingdirective>
