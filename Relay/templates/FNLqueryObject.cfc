<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 

queryObject.cfc

WAB  December 2005 - February 2006

This was my attempt to cache queries so that they could 
a) be refreshed easily
b) could tell you the time at which the data was cached


It also has the possibility of
i)	getting results from cache when only reordering is required
ii) being used to parse queries and apply extra where clauses - things which I developed for the clicking through on reports (in application.com.regexp.parsequery)




--->


<cfcomponent displayname="QueryObject" >
	<cfproperty name="SQL" type="string" >   <!--- sql statement passed in to object --->
	<cfproperty name="datasource" type="string" >
	<cfproperty name="def" type="string" >   <!--- definition which might be calculated  --->
	<cfproperty name="name" type="data" >
	
	<cfproperty name="CurrentSQL" type="string" >   <!--- not used yet --->

	<cfparam name="application.queryCache" default = #structNew()#>
	<cfparam name="application.queryCache.lastCleanUp" default = #now()#>		
	<cfparam name="application.queryCache.queries" default = #structNew()#>


	<cfset minutesBetweenCleanUp = 1>     <!--- how often the procedure to clear up all the old queries in the cache is run --->

	
	<!--- cleans up the query cache on a regular basis --->
	<cfif now () gt dateadd("n",minutesBetweenCleanUp,application.queryCache.lastCleanUp)>
		<cfset cleanUp()>
	</cfif>
	

	<!--- 
		run query without any caching
	 --->
	<cffunction name="run" returns="struct">
		<cfquery name = "#this.name#" datasource="#application.sitedataSource#">
			#this.sql#
		</cfquery>
		<cfreturn variables[this.name]>
	</cffunction>



	<!--- 
		run query with caching
	--->
	<cffunction name="runWithCache" returns="struct">
		<cfargument name="timespan" required = "no" default = "#createTimeSpan(0,1,0,0)#">   <!--- default 1 hour --->
		<cfargument name="refreshCacheNow" required = "no" default = false>

		<cfset var data = "">
		<cfset var queryies = "">
		<cfparam name="application.queryCache.queries[this.datasource]" default = #structNew()#>
		
		<cfset  queryies = application.querycache.queries[this.datasource]>
		
		
		<!--- remove the order by to get the key of the query 
			this allows us to reorder queries otherwise identical queries in memory without having to retrieve results from SQL again
			could fall over if order by fields are not in the original query!		
		--->
		
		<Cfset re = "(.*)(order by(.*))">
		<cfset x = refindnocase(re,this.sql,0,true)>
		<cfset key = this.sql>
		<cfset orderby = "">
		<cfif x.pos[1] is not 0>
			<cfset key = mid(this.sql,x.pos[2],x.len[2])>
			<cfset orderby = mid(this.sql,x.pos[3],x.len[3])>
		</cfif>


		
		<cfif not isDefined("updateCache") 
				and not refreshCacheNow
				and structKeyExists (queryies,key) 
				and queryies[key].cacheUntil gt now()
				and queryies[key].cachedAt + timespan gt now()
				>
			<!---  getting the query from cache--->
		
			<cfset cacheInfo = queryies[key]>	
			<cfset data = queryies[key].resultset>	
			
			<cfif cacheInfo.orderby is not  orderby>
				
				<cfquery name="#this.name#_reorder" dbtype="query">
				select * from data
				#orderby#
				</cfquery>
				
				<cfset data = variables["#this.name#_reorder"]>	
				
			</cfif>
			
			<cfif data.recordcount is not 0>
				<cfset querySetCell (data,"querycachedAt",queryies[key].cachedAt,1)>
				<cfset querySetCell (data,"querycachedAgo",application.com.dateFunctions.TimeSpanInWords(now() - queryies[key].cachedAt),1)>
			</cfif>
		
		
		<cfelse>

			<!---  not getting the query from cache--->		
			<cfquery name = "data" datasource="#this.datasource#">
			#preserveSingleQuotes(this.sql)#
			</cfquery>

			<cfset queryies[key] = structNew()>
			<cfset queryies[key].cacheUntil = now() + timespan>
			<cfset queryies[key].cachedAt = now()>
			<cfset queryies[key].orderby = orderby>
			<cfset queryies[key].resultSet = data>
		
			<cfset queryaddcolumn(data,"querycachedAt",arrayNew(1))>
			<cfset queryaddcolumn(data,"querycachedAgo",arrayNew(1))>

			<cfif data.recordcount is not 0>
				<cfset querySetCell (data,"querycachedAt","",1)>  <!--- if cache just created we set querycachedat to blank --->
				<cfset querySetCell (data,"querycachedAgo","",1)>
			</cfif>
			
		</cfif>
		
		<cfreturn data>
	</cffunction>



	<!--- parse a query into its constituent parts  (not used yet) --->
	<cffunction name="parse" returns="struct">
			<cfset this.def = application.com.regexp.parsequery	(this.sql)>
	</cffunction>
	
		

	<!--- clean up the queries in memory --->
	<cffunction name="cleanUp" >

		<cfloop item="thisdatasource" collection="#application.queryCache.queries#">

			<cfloop item="thisitem" collection="#application.queryCache.queries[thisdatasource]#">
		
				<cfif not structKeyExists (application.queryCache.queries[thisdatasource][thisitem],"cacheUntil") or  application.queryCache.queries[thisdatasource][thisitem].cacheUntil lt now()>
					<cfset structDelete (application.queryCache.queries[thisdatasource],thisitem)>
				</cfif>
		
			</cfloop>			
	
		</cfloop>			
	
		<cfset application.queryCache.lastCleanUp = now()>		
			
	</cffunction>
	
	
	

</cfcomponent>


