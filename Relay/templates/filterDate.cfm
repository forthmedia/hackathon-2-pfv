<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			filterDate.cfm
Author:				KAP
Date started:		14-Aug-2003
	
Description:			

Date range filter called by forecastSelector.cfm

It uses its own form, so there's a JavaScript example of migrating the selected range into a master form (onsubmit event in forecastSelector.cfm).

If queryRangeStartDateTime and queryRangeEndDateTime are initialised by the caller (typically minimum and maximum date values from a query) then date filters which do not fall within the range are not displayed

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
10-Sep-2003			KAP			Initial version

Possible enhancements:


--->

<cfparam name="filterDates" type="array" default="#ArrayNew( 1 )#">
<cfparam name="queryRangeStartDateTime" type="date" default="#CreateDate( 1899, 12, 31 )#">
<cfparam name="queryRangeEndDateTime" type="date" default="#CreateDate( 2037, 9, 5 )#">

<!--- the calling template can override this array otherwise the following is the default --->
<cfif ArrayLen( filterDates ) eq 0>
	<cfset filterDates[1] = "Any">
	<cfset filterDates[2] = "Last week,w,-1,1"> <!--- label followed by parameters to be passed to the fnDateRange() function --->
	<cfset filterDates[3] = "This week,w,0,1">
	<cfset filterDates[4] = "Next week,w,1,1">
	<cfset filterDates[5] = "Yesterday,d,-1,1">
	<cfset filterDates[6] = "Today,d,0,1">
	<cfset filterDates[7] = "Tomorrow,d,1,1">
	<cfset filterDates[7] = "Feb-Aug,m,0,7,#CreateDate( Year( now() ), 2, 1 )#"> <!--- a reference date can be added instead of defaulting to now --->

	<cfset filterDates[8] = "Jan,m,0,1,#CreateDate( Year( now() ), 1, 1 )#">
	<cfset filterDates[9] = "Feb,m,0,1,#CreateDate( Year( now() ), 2, 1 )#">
	<cfset filterDates[10] = "Mar,m,0,1,#CreateDate( Year( now() ), 3, 1 )#">
	<cfset filterDates[11] = "Apr,m,0,1,#CreateDate( Year( now() ), 4, 1 )#">
	<cfset filterDates[12] = "May,m,0,1,#CreateDate( Year( now() ), 5, 1 )#">
	<cfset filterDates[13] = "Jun,m,0,1,#CreateDate( Year( now() ), 6, 1 )#">
	<cfset filterDates[14] = "Jul,m,0,1,#CreateDate( Year( now() ), 7, 1 )#">
	<cfset filterDates[15] = "Aug,m,0,1,#CreateDate( Year( now() ), 8, 1 )#">
	<cfset filterDates[16] = "Sep,m,0,1,#CreateDate( Year( now() ), 9, 1 )#">
	<cfset filterDates[17] = "Oct,m,0,1,#CreateDate( Year( now() ), 10, 1 )#">
	<cfset filterDates[18] = "Nov,m,0,1,#CreateDate( Year( now() ), 11, 1 )#">
	<cfset filterDates[19] = "Dec,m,0,1,#CreateDate( Year( now() ), 12, 1 )#">
</cfif>

<!--- --------------------------------------------------------------------- --->
<!--- date range function: fnDateRange( datePart, deltaStart, duration [, Date ) --->
<!--- --------------------------------------------------------------------- --->
<cffunction name="fnDateRange" returntype="struct">
	<cfargument name="fnRangeDatePart" type="string" required="yes">
	<cfargument name="fnRangeDeltaStart" type="numeric" required="yes">
	<cfargument name="fnRangeDuration" type="numeric" required="yes">
	<cfargument name="fnComparisonDate" type="date" default="#now()#">

	<cfset fnReturnValue = StructNew()>
	<cfset fnReturnValue["start"] = StructNew()>
	<cfset fnReturnValue["end"] = StructNew()>
	
	<cfset comparisonDateZeroHundredHours = CreateDateTime( Year( arguments["fnComparisonDate"] ), Month( arguments["fnComparisonDate"] ), Day( arguments["fnComparisonDate"] ), 0, 0, 0 )>
	
	<cfswitch expression="#iif( ListFindNoCase( "d,w,m,q", arguments["fnRangeDatePart"] ) neq 0, 'arguments["fnRangeDatePart"]', de( "w" ) )#">
	
		<cfcase value="d"> <!--- day --->
			<cfset rangeStartDateTime = DateAdd( "d", arguments["fnRangeDeltaStart"], comparisonDateZeroHundredHours )>
			<cfset rangeEndDateTime = DateAdd( "s", -1, DateAdd( "d", arguments["fnRangeDuration"], rangeStartDateTime ) )>
		</cfcase>
		
		<cfcase value="w"> <!--- week --->
			<cfset rangeStartDateTime = DateAdd( "d", ( arguments["fnRangeDeltaStart"] * 7 ) - DayOfWeek( comparisonDateZeroHundredHours ) + 1, comparisonDateZeroHundredHours )>
			<cfset rangeEndDateTime = DateAdd( "s", -1, DateAdd( "d", arguments["fnRangeDuration"] * 7, rangeStartDateTime ) )>
		</cfcase>
		
		<cfcase value="m"> <!--- month --->
			<cfset rangeStartDateTime = DateAdd( "m", arguments["fnRangeDeltaStart"], comparisonDateZeroHundredHours )>
			<cfset rangeEndDateTime = DateAdd( "s", -1, DateAdd( "m", arguments["fnRangeDuration"], rangeStartDateTime ) )>
		</cfcase>
		
		<cfcase value="q"> <!--- quarter --->
			<cfset rangeStartDateTime = DateAdd( "q", arguments["fnRangeDeltaStart"], comparisonDateZeroHundredHours )>
			<cfset rangeEndDateTime = DateAdd( "s", -1, DateAdd( "q", arguments["fnRangeDuration"], rangeStartDateTime ) )>
		</cfcase>
	
	</cfswitch>
	
	<cfif fnRangeDatePart neq "x">
		<cfset fnReturnValue["start"]["timeStamp"] = CreateODBCDateTime( rangeStartDateTime )>
		<cfset fnReturnValue["start"]["year"] = Year( rangeStartDateTime )>
		<cfset fnReturnValue["start"]["month"] = Month( rangeStartDateTime )>
		<cfset fnReturnValue["start"]["day"] = Day( rangeStartDateTime )>
		<cfset fnReturnValue["start"]["hour"] = Hour( rangeStartDateTime )>
		<cfset fnReturnValue["start"]["minute"] = Minute( rangeStartDateTime )>
		<cfset fnReturnValue["start"]["second"] = Second( rangeStartDateTime )>
		
		<cfset fnReturnValue["end"]["timeStamp"] = CreateODBCDateTime( rangeEndDateTime )>
		<cfset fnReturnValue["end"]["year"] = Year( rangeEndDateTime )>
		<cfset fnReturnValue["end"]["month"] = Month( rangeEndDateTime )>
		<cfset fnReturnValue["end"]["day"] = Day( rangeEndDateTime )>
		<cfset fnReturnValue["end"]["hour"] = Hour( rangeEndDateTime )>
		<cfset fnReturnValue["end"]["minute"] = Minute( rangeEndDateTime )>
		<cfset fnReturnValue["end"]["second"] = Second( rangeEndDateTime )>
	<cfelse>
		<cfset fnReturnValue["start"]["timeStamp"] = "">
		<cfset fnReturnValue["start"]["year"] = "">
		<cfset fnReturnValue["start"]["month"] = "">
		<cfset fnReturnValue["start"]["day"] = "">
		<cfset fnReturnValue["start"]["hour"] = "">
		<cfset fnReturnValue["start"]["minute"] = "">
		<cfset fnReturnValue["start"]["second"] = "">
		
		<cfset fnReturnValue["end"]["timeStamp"] = "">
		<cfset fnReturnValue["end"]["year"] = "">
		<cfset fnReturnValue["end"]["month"] = "">
		<cfset fnReturnValue["end"]["day"] = "">
		<cfset fnReturnValue["end"]["hour"] = "">
		<cfset fnReturnValue["end"]["minute"] = "">
		<cfset fnReturnValue["end"]["second"] = "">
	</cfif>
	
	<cfreturn fnReturnValue>

</cffunction>
<!--- --------------------------------------------------------------------- --->

<cfset dateRangeArray = ArrayNew(1)>
<cfloop index="filterDateIndex" from="1" to="#ArrayLen( filterDates )#">
	<cfif ListLen( filterDates[filterDateIndex] ) gte 4>
		<cfset dateRangeArray[filterDateIndex] = fnDateRange( ListGetAt( filterDates[filterDateIndex], 2 ), ListGetAt( filterDates[filterDateIndex], 3 ), ListGetAt( filterDates[filterDateIndex], 4 ), iif( ListLen( filterDates[filterDateIndex] ) gte 5, "ListGetAt( filterDates[filterDateIndex], 5 )", "now()" ) )>
	<cfelse>
		<cfset dateRangeArray[filterDateIndex] = fnDateRange( "x", 0, 0 )>
	</cfif>
</cfloop>

<SCRIPT type="text/javascript">
<!--
var dateRangeObject = new Object();

<cfloop index="filterDateIndex" from="1" to="#ArrayLen( filterDates )#">
	dateRangeObject["<cfoutput>#filterDateIndex#</cfoutput>"] = new Object();
	
	dateRangeObject["<cfoutput>#filterDateIndex#</cfoutput>"]["start"] = new Object();
	dateRangeObject["<cfoutput>#filterDateIndex#</cfoutput>"]["start"]["timeStamp"] = "<cfoutput>#JSStringFormat( dateRangeArray[filterDateIndex]["start"]["timeStamp"] )#</cfoutput>";
	dateRangeObject["<cfoutput>#filterDateIndex#</cfoutput>"]["start"]["year"] = "<cfoutput>#JSStringFormat( dateRangeArray[filterDateIndex]["start"]["year"] )#</cfoutput>";
	dateRangeObject["<cfoutput>#filterDateIndex#</cfoutput>"]["start"]["month"] = "<cfoutput>#JSStringFormat( dateRangeArray[filterDateIndex]["start"]["month"] )#</cfoutput>";
	dateRangeObject["<cfoutput>#filterDateIndex#</cfoutput>"]["start"]["day"] = "<cfoutput>#JSStringFormat( dateRangeArray[filterDateIndex]["start"]["day"] )#</cfoutput>";
	dateRangeObject["<cfoutput>#filterDateIndex#</cfoutput>"]["start"]["hour"] = "<cfoutput>#JSStringFormat( dateRangeArray[filterDateIndex]["start"]["hour"] )#</cfoutput>";
	dateRangeObject["<cfoutput>#filterDateIndex#</cfoutput>"]["start"]["minute"] = "<cfoutput>#JSStringFormat( dateRangeArray[filterDateIndex]["start"]["minute"] )#</cfoutput>";
	dateRangeObject["<cfoutput>#filterDateIndex#</cfoutput>"]["start"]["second"] = "<cfoutput>#JSStringFormat( dateRangeArray[filterDateIndex]["start"]["second"] )#</cfoutput>";
		
	dateRangeObject["<cfoutput>#filterDateIndex#</cfoutput>"]["end"] = new Object();
	dateRangeObject["<cfoutput>#filterDateIndex#</cfoutput>"]["end"]["timeStamp"] = "<cfoutput>#JSStringFormat( dateRangeArray[filterDateIndex]["end"]["timeStamp"] )#</cfoutput>";
	dateRangeObject["<cfoutput>#filterDateIndex#</cfoutput>"]["end"]["year"] = "<cfoutput>#JSStringFormat( dateRangeArray[filterDateIndex]["end"]["year"] )#</cfoutput>";
	dateRangeObject["<cfoutput>#filterDateIndex#</cfoutput>"]["end"]["month"] = "<cfoutput>#JSStringFormat( dateRangeArray[filterDateIndex]["end"]["month"] )#</cfoutput>";
	dateRangeObject["<cfoutput>#filterDateIndex#</cfoutput>"]["end"]["day"] = "<cfoutput>#JSStringFormat( dateRangeArray[filterDateIndex]["end"]["day"] )#</cfoutput>";
	dateRangeObject["<cfoutput>#filterDateIndex#</cfoutput>"]["end"]["hour"] = "<cfoutput>#JSStringFormat( dateRangeArray[filterDateIndex]["end"]["hour"] )#</cfoutput>";
	dateRangeObject["<cfoutput>#filterDateIndex#</cfoutput>"]["end"]["minute"] = "<cfoutput>#JSStringFormat( dateRangeArray[filterDateIndex]["end"]["minute"] )#</cfoutput>";
	dateRangeObject["<cfoutput>#filterDateIndex#</cfoutput>"]["end"]["second"] = "<cfoutput>#JSStringFormat( dateRangeArray[filterDateIndex]["end"]["second"] )#</cfoutput>";
</cfloop>

function eventDateOnClick( callerObject )
{
  with ( callerObject.form )
  {
    elements["frmRangeStartTimeStamp"].value = dateRangeObject[callerObject.value]["start"]["timeStamp"];
    elements["frmRangeStartYear"].value = dateRangeObject[callerObject.value]["start"]["year"];
    elements["frmRangeStartMonth"].value = dateRangeObject[callerObject.value]["start"]["month"];
    elements["frmRangeStartDay"].value = dateRangeObject[callerObject.value]["start"]["day"];
    elements["frmRangeStartHour"].value = dateRangeObject[callerObject.value]["start"]["hour"];
    elements["frmRangeStartMinute"].value = dateRangeObject[callerObject.value]["start"]["minute"];
    elements["frmRangeStartSecond"].value = dateRangeObject[callerObject.value]["start"]["second"];
    elements["frmRangeEndTimeStamp"].value = dateRangeObject[callerObject.value]["end"]["timeStamp"];
    elements["frmRangeEndYear"].value = dateRangeObject[callerObject.value]["end"]["year"];
    elements["frmRangeEndMonth"].value = dateRangeObject[callerObject.value]["end"]["month"];
    elements["frmRangeEndDay"].value = dateRangeObject[callerObject.value]["end"]["day"];
    elements["frmRangeEndHour"].value = dateRangeObject[callerObject.value]["end"]["hour"];
    elements["frmRangeEndMinute"].value = dateRangeObject[callerObject.value]["end"]["minute"];
    elements["frmRangeEndSecond"].value = dateRangeObject[callerObject.value]["end"]["second"];
  }
  return true;
}
//-->
</script>

<form action="" name="frmDateRangeForm">
	<input type="hidden" name="frmRangeStartTimeStamp" value="">
	<input type="hidden" name="frmRangeStartYear" value="">
	<input type="hidden" name="frmRangeStartMonth" value="">
	<input type="hidden" name="frmRangeStartDay" value="">
	<input type="hidden" name="frmRangeStartHour" value="">
	<input type="hidden" name="frmRangeStartMinute" value="">
	<input type="hidden" name="frmRangeStartSecond" value="">
	<input type="hidden" name="frmRangeEndTimeStamp" value="">
	<input type="hidden" name="frmRangeEndYear" value="">
	<input type="hidden" name="frmRangeEndMonth" value="">
	<input type="hidden" name="frmRangeEndDay" value="">
	<input type="hidden" name="frmRangeEndHour" value="">
	<input type="hidden" name="frmRangeEndMinute" value="">
	<input type="hidden" name="frmRangeEndSecond" value="">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<cfloop index="filterDateIndex" from="1" to="#ArrayLen( filterDates )#">

<!--- either the filter start or end time must be bracketed by the interval queryRangeStartDateTime to queryRangeEndDateTime --->
	<cfif 
		dateRangeArray[filterDateIndex]["start"]["timeStamp"] eq ""
		or
		(
			(
				DateCompare( queryRangeStartDateTime, dateRangeArray[filterDateIndex]["start"]["timeStamp"] ) neq 1
				and
				DateCompare( queryRangeEndDateTime, dateRangeArray[filterDateIndex]["start"]["timeStamp"] ) neq -1
			)
			or
			(
				DateCompare( queryRangeStartDateTime, dateRangeArray[filterDateIndex]["end"]["timeStamp"] ) neq 1
				and
				DateCompare( queryRangeEndDateTime, dateRangeArray[filterDateIndex]["end"]["timeStamp"] ) neq -1
			)
		)>
	
			<tr class="<cfoutput>#iif( BitAnd( filterDateIndex - 1, 1 ) eq 0, de( "oddrow" ), de( "evenrow" ) )#</cfoutput>">
				<td valign="top" class="smaller"><CF_INPUT type="radio" name="frmDateRange" value="#filterDateIndex#" onClick="eventDateOnClick(this);" checked="#iif( ListLen( filterDates[filterDateIndex] ) eq 1,true,false)#"></td>
				<td valign="top" class="smaller"><cfoutput>#ListFirst( filterDates[filterDateIndex] )#</cfoutput></td>
<!--- 
	<cfif dateRangeArray[filterDateIndex]["start"]["timeStamp"] neq "">
	
	<cfset bool1 = DateCompare( queryRangeStartDateTime, dateRangeArray[filterDateIndex]["start"]["timeStamp"] ) neq 1>
	<td><cfoutput>#queryRangeStartDateTime#</cfoutput></td>
	<td><cfoutput>#dateRangeArray[filterDateIndex]["start"]["timeStamp"]#</cfoutput></td>
	<td><cfoutput>#DateCompare( queryRangeStartDateTime, dateRangeArray[filterDateIndex]["start"]["timeStamp"] )#</cfoutput></td>
	<td><cfoutput>#bool1#</cfoutput></td>
	
	<cfset bool2 = DateCompare( queryRangeEndDateTime, dateRangeArray[filterDateIndex]["end"]["timeStamp"] ) neq -1>
	<td><cfoutput>#queryRangeEndDateTime#</cfoutput></td>
	<td><cfoutput>#dateRangeArray[filterDateIndex]["end"]["timeStamp"]#</cfoutput></td>
	<td><cfoutput>#DateCompare( queryRangeEndDateTime, dateRangeArray[filterDateIndex]["end"]["timeStamp"] )#</cfoutput></td>
	<td><cfoutput>#bool2#</cfoutput></td>
	</cfif>
 --->
			</tr>
	</cfif>

	</cfloop>
	</table>
</form>

