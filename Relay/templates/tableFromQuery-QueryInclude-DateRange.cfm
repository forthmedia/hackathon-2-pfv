<!--- �Relayware. All Rights Reserved 2014 --->
<!--- PPB 2010-08-25 LEN022 I have split the date filtering out of the main body of tableFromQuery-QueryInclude.cfm so that it can be included independently;
it was done when I needed to filter on translated columns and dates; the main query is run then the columns are translated using cf_translateQueryColumn and then filtered in a QoQ, 
but as we can't refer to the SQLServer date functions in a QoQ, the dates are filtered directly in the main query using this include and then we set useDateRange=false 
and include tableFromQuery-QueryInclude.cfm in the QoQ (eg Lenovo contractReport.cfm) 

2015-11-17			WAB			PROD2015-405 Security/SQL Injection Implement cf_querparam and cf_queryobjectname throughout.
 --->
<cfoutput>
		<cf_param name="dateRangeFieldName" type="queryObjectName">
		
		<cfif isDefined("FRMWHERECLAUSEA") and FRMWHERECLAUSEA neq ""> 
			AND rtrim(datename(q, #dateRangeFieldName#)) = <cf_queryparam value="#right(listfirst(FRMWHERECLAUSEA,"-"),1)#" CFSQLTYPE="CF_SQL_INTEGER" > 
				and datepart(yy, #dateRangeFieldName#) = <cf_queryparam value="#listLast(FRMWHERECLAUSEA,"-")#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfif>
		
		<cfif isDefined("FRMWHERECLAUSEB") and FRMWHERECLAUSEB neq ""> 
			AND left(datename(m, #dateRangeFieldName#),3) = <cf_queryparam value="#listfirst(FRMWHERECLAUSEB,"-")#" CFSQLTYPE="CF_SQL_VARCHAR" >
				and datepart(yy, #dateRangeFieldName#) = <cf_queryparam value="#listLast(FRMWHERECLAUSEB,"-")#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfif>
		<cfif isdefined("dayend")>
			<cfif isDefined("FRMWHERECLAUSEC") and FRMWHERECLAUSEC neq "" and isDefined("FRMWHERECLAUSED") and FRMWHERECLAUSED eq "">
			<cfset correctFrmwhereclausec = listlast(FRMWHERECLAUSEC, ",")> 
				<cfset datetouse = '#listlast(daystart, ",")#-#listfirst(correctFrmwhereclausec,"-")#-#listLast(correctFrmwhereclausec,"-")#'>
				<cfif not IsDate(datetouse)>
					<cfset form.dayend = DaysInMonth("01-#listfirst(correctFrmwhereclausec,"-")#-#listLast(correctFrmwhereclausec,"-")#")>
					<cfset datetouse = '#listlast(dayend, ",")#-#listfirst(correctFrmwhereclausec,"-")#-#listLast(correctFrmwhereclausec,"-")#'>
				</cfif>
				AND #dateRangeFieldName# >= <cf_queryparam value="#dateadd('s', 0 , '#datetouse#')#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
			</cfif>
			<cfif isDefined("FRMWHERECLAUSED") and FRMWHERECLAUSED neq "" and isDefined("FRMWHERECLAUSEC") and FRMWHERECLAUSEC eq ""> 
				<cfset correctFrmwhereclaused = listlast(frmWhereClaused, ",")>
				<cfset datetouse = '#listlast(dayend, ",")#-#listfirst(correctFrmwhereclaused,"-")#-#listLast(correctFrmwhereclaused,"-")#'>
				<cfif not IsDate(datetouse)>
					<cfset form.dayend = DaysInMonth("01-#listfirst(correctFrmwhereclaused,"-")#-#listLast(correctFrmwhereclaused,"-")#")>
					<cfset datetouse = '#listlast(dayend, ",")#-#listfirst(correctFrmwhereclaused,"-")#-#listLast(correctFrmwhereclaused,"-")#'>
				</cfif>
				AND #dateRangeFieldName# < <cf_queryparam value="#dateadd('s', 86399 , '#datetouse#')#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
			</cfif>
			<cfif isDefined("FRMWHERECLAUSEC") and FRMWHERECLAUSEC neq "" and isDefined("FRMWHERECLAUSED") and FRMWHERECLAUSED neq "">
				<cfset correctFrmwhereclausec = listlast(FRMWHERECLAUSEC, ",")>
				<cfset correctFrmwhereclaused = listlast(FRMWHERECLAUSEd, ",")>
				<cfset datetouse = '#listlast(dayend , ",")#-#listfirst(correctFrmwhereclaused,"-")#-#listLast(correctFrmwhereclaused,"-")#'>
				<cfif not IsDate(datetouse)>
					<cfset form.dayend = DaysInMonth("01-#listfirst(correctFrmwhereclaused,"-")#-#listLast(correctFrmwhereclaused,"-")#")>
					<!---SSS 2009/01/16 var scoping error was caused so the dates where not working correctly Bug trendnabu support 1600 --->
					<cfset dayend = DaysInMonth("01-#listfirst(correctFrmwhereclaused,"-")#-#listLast(correctFrmwhereclaused,"-")#")>
					<cfset datetouse = '#listlast(dayend, ",")#-#listfirst(correctFrmwhereclaused,"-")#-#listLast(correctFrmwhereclaused,"-")#'>
				</cfif>
				AND #dateRangeFieldName# >= <cf_queryparam value="#listlast(daystart, ",")#-#listfirst(correctFrmwhereclausec,"-")#-#listLast(correctFrmwhereclausec,"-")#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
				AND #dateRangeFieldName# < <cf_queryparam value="#dateadd('s', 86399 , '#datetouse#')#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
			</cfif>
		<cfelse>
			<cfif isDefined("FRMWHERECLAUSEC") and FRMWHERECLAUSEC neq "" and isDefined("FRMWHERECLAUSED") and FRMWHERECLAUSED eq ""> 
				AND #dateRangeFieldName# >= <cf_queryparam value="01-#listfirst(FRMWHERECLAUSEC,"-")#-#listLast(FRMWHERECLAUSEC,"-")#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
			</cfif>
			
			<cfif isDefined("FRMWHERECLAUSED") and FRMWHERECLAUSED neq "" and isDefined("FRMWHERECLAUSEC") and FRMWHERECLAUSEC eq ""> 
				AND #dateRangeFieldName# < dateadd(m,1,<cf_queryparam value="01-#listfirst(FRMWHERECLAUSED,"-")#-#listLast(FRMWHERECLAUSED,"-")#" CFSQLTYPE="CF_SQL_TIMESTAMP" >)
			</cfif>
			
			<cfif isDefined("FRMWHERECLAUSEC") and FRMWHERECLAUSEC neq "" and isDefined("FRMWHERECLAUSED") and FRMWHERECLAUSED neq ""> 
				AND #dateRangeFieldName# >= <cf_queryparam value="01-#listfirst(FRMWHERECLAUSEC,"-")#-#listLast(FRMWHERECLAUSEC,"-")#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
				AND #dateRangeFieldName# < dateadd(m,1,<cf_queryparam value="01-#listfirst(FRMWHERECLAUSED,"-")#-#listLast(FRMWHERECLAUSED,"-")#" CFSQLTYPE="CF_SQL_TIMESTAMP" >)
			</cfif> 
		</cfif>
</cfoutput>
