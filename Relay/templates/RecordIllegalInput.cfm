<!--- �Relayware. All Rights Reserved 2014 --->

<CFIF isDefined("request.relayCurrentUser.personid") and listLen(Cookie.user,"-") GT 1>
	<CFQUERY NAME="getUsername" datasource="#application.siteDataSource#">
		SELECT Person.personID, Person.FirstName, Person.LastName, Person.Username, person.email
		FROM Person, usergroup
		WHERE Person.PersonID = #request.relayCurrentUser.personid#
		  AND Person.Active <> 0 
		  AND Person.Password <> ''
		  AND Person.Personid = usergroup.personid
		  AND usergroup.usergroupid = #request.relayCurrentUser.usergroupid#
	</CFQUERY>
	
	<CFIF #getUsername.RecordCount# IS 1>
		<CFSET UnKnown=false>
		<CFMAIL TYPE="HTML" TO="#getUsername.email#" 
		FROM="#application.AdminEmail#"
		CC = "relayHelp@foundation-network.com"
		SUBJECT="Illegal Input text notifcation">

	#getUsername.FirstName# #getUsername.LastName# entered illegal text at #HTTP_REFERER#<BR>
		<BR>
		The illegal text may be included in the following fields<BR>
		<CFLOOP LIST="#FIELDNAMES#" INDEX="x">
			<CFSET y= evaluate("#x#")>
			#x# = #y#<BR><BR>
		</CFLOOP>

	</CFMAIL>
	<CFELSE>
		<CFSET UnKnown=true>
	</CFIF>
<CFELSE>
	<CFSET UnKnown=true>

</CFIF>
<CFIF UnKnown IS true>
	<CFMAIL TYPE="HTML" TO="davemcl@foundation-network.com" 
		FROM="#application.AdminEmail#"
		SUBJECT="Illegal Input text notifcation">
	Unknown person entered illegal text at #HTTP_REFERER#<BR>
		<BR>
		The illegal text may be included in the following fields<BR>
		<CFLOOP LIST="#FIELDNAMES#" INDEX="x">
			<CFSET y= evaluate("#x#")>
			#x# = #y#<BR><BR>
		</CFLOOP>

	</CFMAIL>
</CFIF>

Illegal text was entered. This has been reported to the site administrator.

<CFIF url.NextTemplate IS NOT SCRIPT_NAME>
	<CFINCLUDE TEMPLATE="../..#url.NextTemplate#">
<CFELSE>
This is the second time you have performed this type of illegal operation.
You cannot use words such as Update, Insert, Delete, Create or Alter as these are SQL keywords that could cause damage to the database.
</CFIF>

