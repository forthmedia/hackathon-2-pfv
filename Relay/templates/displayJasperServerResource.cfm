<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		displayJasperServerResource.cfm	
Author:			NJH  
Date started:	2011/02/14
	
Description:	Template to show JasperServer dashboards and reports		

Amendment History:

Date 		Initials 	What was changed
2012/04/11 	PPB 		Case 427386 change viewReportFlow to viewAdhocReportFlow
2015-02-04	AHL		443498 Page asks to reload when putting a report on it

Possible enhancements:


 --->
<cfparam name="resourceURI" type="string" default="" > <!--- NOTE: this URI is case sensitive!!!! --->  <!--- 2015-02-04 AHL 443498 Page asks to reload when putting a report on it --->
<cfparam name="resource" type="string" default="dashboard">

<cfset resourceURI = replace(resourceURI," ","_","ALL")>
<cfset jasperServerPath = application.com.jasperServer.getJasperServerPath()>

<cfif resource eq "report">
<!--- 2012/04/11 PPB Case 427386 change viewReportFlow to viewAdhocReportFlow (requested by Yan)
	<cfset jasperserverUrl="#jasperServerPath#/flow.html?_flowId=viewReportFlow&reportUnit=#resourceURI#&decorate=no">
 --->
	<cfset jasperserverUrl="#jasperServerPath#/flow.html?_flowId=viewAdhocReportFlow&reportUnit=#resourceURI#&decorate=no">

	
<cfelseif resource eq "dashboard">
	<cfset jasperserverUrl="#jasperServerPath#/flow.html?_flowId=dashboardRuntimeFlow&dashboardResource=#resourceURI#&decorate=no">
</cfif>

<cfinclude template="/jasperServer/jasperServerLogin.cfm">
