<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
qrySelectionRecordCounts.cfm

query to count number of items in a selection


requires frmSelectionID


WAB 2004-09-21 
--->

			<CFQUERY NAME="recordCounts" datasource="#application.siteDataSource#">
			select 
				count(distinct personid) as People,
				count(distinct locationid) as Locations,
				count(distinct organisationid) as Organisations,
				(select count(1) from selectiontag where selectionid = s.selectionid) as AllPeopleSelectedorUnselected

			from
				selection s left join 
				(selectiontag st inner join person p on st.entityid = p.personid and st.status > 0) on s.selectionid = st.selectionid
			where 
				s.selectionid =  <cf_queryparam value="#frmselectionid#" CFSQLTYPE="CF_SQL_INTEGER" > 
			group By s.selectionid
			</cfquery>
			
