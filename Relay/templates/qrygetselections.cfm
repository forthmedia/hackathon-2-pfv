<!--- �Relayware. All Rights Reserved 2014 --->
<!--- qrygetselections.cfm   WAB 1999-02-11  
Gets a list of selections which the current user can add or remove people from  


WAB 2005-05-25 replaced with call to cfc
--->

<!--- <CFQUERY NAME="getSelections" DATASOURCE="#application.siteDataSource#">
	SELECT s.SelectionID,
		  ltrim(s.Title) as title,
		  s.Description,
		  s.TableName,
		  s.Created AS datecreated,
		  s.CreatedBy AS owner,
		  s.lastupdated
  	  FROM Selection AS s, SelectionGroup AS sg, Person AS p, UserGroup AS ug
	 WHERE sg.SelectionID = s.SelectionID
	   AND sg.UserGroupID = #request.relayCurrentUser.usergroupid#
	   AND ug.UserGroupID = s.CreatedBy
	   AND p.PersonID = ug.PersonID
   ORDER BY s.lastupdated desc , ltrim(s.Title) asc
</CFQUERY> --->

<cfset getSelections = application.com.selections.getSelections(orderby = "s.lastupdated desc, ltrim(s.title) asc")>
