﻿<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File name:		MagicCheckSum.cfm
Author:			Will/Chris
Date created:	Summer 2001

	Objective - To provide a method of validating that the personID and 
				magicNumber are a valid pair

				returns
				caller.validchecksum 		true/false
				
Date Tested:
Tested by:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed

WAB 2005-05-24    Turned out that the code checking the modregister for changed locationids was not working - it was lookign for tables called p and l not person and location
WAB 2005-05-25	Modified to handle p number belonging to a person who has been deduped.  
				now returns variable actualPersonID which is the personid still in the database

Enhancement still to do:



--->
<cfif isDefined("attributes.person")><cfset person=attributes.person></cfif>

<!--- This code taken from Wills Newsletter: personid and checksum passed, so decipher them --->	
<cfif person is not "">
	<!---  get Person's Record --->
	<!--- <CFOUTPUT> Checking GetPerson #listfirst(person,"-")# </cfoutput> --->

	<CFQUERY NAME="getPerson" DATASOURCE="#application.sitedatasource#">
	Select 	personid, locationid, personid as actualPersonID
	From 	Person	
	Where	personid =  <cf_queryparam value="#listfirst(Person,"-")#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</CFQUERY>
	
	<cfif getPerson.recordCount is 0 >
		<!--- 
		Note: WAB 2005-05-25
		If the personid is not found, then should check for dedupes in table dedupeAudit
		If we find one of these we can check the p-number, but should we pass back a different personid to the calling page??
		
		This is fine as far as it goes, but really should pass the correct personid back to the calling page
		 --->


		<CFQUERY NAME="getPerson" DATASOURCE="#application.sitedatasource#">
		Select 	pd.personid, pd.locationid , da.newid as actualPersonID
		From 	dedupeAudit da inner join PersonDel	pd on da.recordType = 'person' and da.archivedid = pd.personid
		Where	da.archivedid =  <cf_queryparam value="#listfirst(Person,"-")#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</CFQUERY>
	</cfif>



	<cfif getPerson.recordCount is 0 > 
		<CFSET caller.validchecksum = false>
		<CFSET caller.actualPersonID = 0>
	<cfelse>
		<!---  also going to get all locationid mods, just in case of dedupe --->
		<!--- 2015-02-16 GCC performance improvements - added nolock statements --->
		<CFQUERY NAME="getOldLocationIds" DATASOURCE="#application.sitedatasource#">
			Select 	distinct oldVal 
		From 	modRegister	with(nolock)
		Where	recordid =  <cf_queryparam value="#getPerson.personid#" CFSQLTYPE="CF_SQL_INTEGER" > 
		AND 	modentityid in (select modentityid from modentitydef with(nolock) where  fieldname= 'LocationID' and (TableName = 'Location' or TableName = 'Person' ))
		</CFQUERY>	
		
		<CFSET locationids = listappend (getPerson.locationid, getOldLocationIds.oldVal )>
		
		<!--- loop through all the valid location id until we get a match on the checksum/hash --->
		<CFSET validchecksum = false>
		<CFSET caller.validchecksum = false>
		<CFLOOP	index = thisLocationID list = #locationids#>
			<CFIF bitand(getPerson.personid, 1023) * bitand(thisLocationID, 1023) IS  #listlast(Person,"-")#>
				<CFSET validchecksum = true>
				<CFSET caller.validchecksum = true>
				<CFSET caller.actualPersonID = getPerson.actualPersonID>
				<!--- may need to set user cookie here - assume that the cookie will be set --->
				<CFBREAK>
			</cfif>
		</CFLOOP>


	</cfif>

</CFIF>	
