<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 2012/02/12 PPB Case 426125 use rwListGetAt instead of ListGetAt to defend against empty strings in the list --->
<!--- 2015-11-13    ACPK    Replace table with Bootstrap --->

<!--- this query returns a description, and the criteria text
	  in the order specified by each select's "position" field

 	 Assumes the following variables exist:
	 
		Variables.savedFields = list of SectionCriteria.SelectionFields for the search query
		Variables.savedCriteria	= list of the Criteria corresponding to savedFields 
			Note: blank or null Criteria is not allowed and will cause this logic to fail
				  since CF does not count empty list items
		  
	  NOTE: UNION implicitly removes duplicates

---->	  
<!--- There should always be 2 queries at the top of this union!!
		The first is to create aliases for the orderby and the second
		is to make sure there is always a union because aliases in
		orderby's only work with union queries
 --->

<!--- Get FieldNames and text for selected person flags --->
<!--- <CFSET #stringFlags# = "'#ReReplace(PersonFlags, ",", "','", "ALL")#'">
<CFIF IsDefined('PersonFlags')>
	<CFQUERY NAME="getFlagText" datasource="#application.siteDataSource#">
	SELECT ItemText,
	FieldName
	FROM LookUpList
	WHERE IsParent <> 0 <!---  true  --->
	AND FieldName IN (#PreserveSingleQuotes(stringFlags)#)
	</CFQUERY>
</CFIF>
--->
 
<CFQUERY NAME="getSearchText" datasource="#application.siteDataSource#">
	SELECT 0 as position,
 	      '' AS description,
		  '' As criteria
	 FROM FileType
	WHERE FileTypeID = 0
	UNION <!--- need this query so the ORDER BY will work (with aliases) --->
	SELECT 0,
 	      '',
		  ''
	 FROM Dual
	WHERE a = 0
	<CFIF #ListContainsNoCase(Variables.savedFields,"frmCountryGroup","#application.delim1#")# IS NOT 0>
	UNION
	SELECT 1,
		   'Country Group',
		   CountryDescription
	  FROM Country
     WHERE CountryID #application.com.globalFunctions.rwListGetAt(Variables.savedCriteria,ListContainsNoCase(Variables.savedFields,"frmCountryGroup","#application.delim1#"),"#application.delim1#")#
	 AND (ISOCode IS null OR ISOCode = '')
	 <!---  --->
	<CFELSEIF #ListContainsNoCase(Variables.savedFields,"frmCountryID","#application.delim1#")# IS NOT 0>
	UNION
	SELECT 1,
		   'Country',
		   CountryDescription
	  FROM Country
     WHERE CountryID #application.com.globalFunctions.rwListGetAt(Variables.savedCriteria,ListContainsNoCase(Variables.savedFields,"frmCountryID","#application.delim1#"),"#application.delim1#")#
	</CFIF>
	
	<CFIF #ListContainsNoCase(Variables.savedFields,"frmRegionState","#application.delim1#")# IS NOT 0>
	 	<CFSET crit = #application.com.globalFunctions.rwListGetAt(Variables.savedCriteria,ListContainsNoCase(Variables.savedFields,"frmRegionState","#application.delim1#"),"#application.delim1#")#>
	UNION
	SELECT 2,
		   'Locality',
		   '#crit#'
	 FROM Dual
	</CFIF>

<!--- 	<CFIF #ListContainsNoCase(Variables.savedFields,"frmPartnerType","#application.delim1#")# IS NOT 0>
	UNION
	SELECT 3,
		   'Partner Type',
		   ItemText
	  FROM LookupList
     WHERE LookupID #ListGetAt(Variables.savedCriteria,ListContainsNoCase(Variables.savedFields,"frmPartnerType","#application.delim1#"),"#application.delim1#")#
	</CFIF> --->

<!--- 	<CFLOOP QUERY="getFlagText">
		<CFIF #ListContainsNoCase(Variables.savedFields,"#FieldName#","#application.delim1#")# IS NOT 0>
		UNION
		SELECT 4,
			   '#ItemText#',
			   ItemText
		  FROM LookupList
	     WHERE LookupID #ListGetAt(Variables.savedCriteria,ListContainsNoCase(Variables.savedFields,"#FieldName#","#application.delim1#"),"#application.delim1#")#
		</CFIF>
	</CFLOOP> --->

	<CFIF #ListContainsNoCase(Variables.savedFields,"frmPersLanguage","#application.delim1#")# IS NOT 0>
	<!--- MDC 9th March 2004 - Commented out below as it was causing an extra lot of single quotes aroung the language.
	WAB 2007-10-19 but in fact then failed later on, had to do a replace to get rid of '' which appeared later on.  Change even though this code redundant on dev
--->
	<CFSET crit = application.com.globalFunctions.rwListGetAt(Variables.savedCriteria,ListContainsNoCase(Variables.savedFields,"frmPersLanguage","#application.delim1#"),"#application.delim1#")> 
<cfset crit = replace(crit,"''","'","ALL")>
	UNION
	SELECT 5,
		   'Person Language',
		   Language
	  FROM Language
     WHERE Language #PreserveSingleQuotes(crit)#
	</CFIF>

	<CFIF #ListContainsNoCase(Variables.savedFields,"frmPersNametext","#application.delim1#")# IS NOT 0>
		<CFSET crita = application.com.globalFunctions.rwListGetAt(Variables.savedCriteria,ListContainsNoCase(Variables.savedFields,"frmPersNametext","#application.delim1#"),"#application.delim1#")>
		<CFSET critb = application.com.globalFunctions.rwListGetAt(Variables.savedCriteria,ListContainsNoCase(Variables.savedFields,"frmPersNameType","#application.delim1#"),"#application.delim1#")>
	UNION
	SELECT 6,
		   'Person <CFIF critb IS "first">First<CFELSEIF critb IS "last">Last<CFELSEIF critb IS "full">Full</CFIF> Name',
		   '#crita#'
	 FROM Dual
	</CFIF>

	<CFIF #ListContainsNoCase(Variables.savedFields,"frmPersEmail","#application.delim1#")# IS NOT 0>
		<CFSET crit = application.com.globalFunctions.rwListGetAt(Variables.savedCriteria,ListContainsNoCase(Variables.savedFields,"frmPersEmail","#application.delim1#"),"#application.delim1#")>
	UNION
	SELECT 7,
		   'Person Email',
		   '#crit#'
	  FROM Dual
	</CFIF>

	<CFIF #ListContainsNoCase(Variables.savedFields,"frmPersonID","#application.delim1#")# IS NOT 0>
		<CFSET crit = application.com.globalFunctions.rwListGetAt(Variables.savedCriteria,ListContainsNoCase(Variables.savedFields,"frmPersonID","#application.delim1#"),"#application.delim1#")>
	UNION
	SELECT 7.5,
		   'Person ID',
		   '#crit#'
	  FROM Dual
	</CFIF>
	
	<CFIF #ListContainsNoCase(Variables.savedFields,"frmPersLastUpdateddate","#application.delim1#")# IS NOT 0>
		<CFSET crita = application.com.globalFunctions.rwListGetAt(Variables.savedCriteria,ListContainsNoCase(Variables.savedFields,"frmPersLastUpdateddate","#application.delim1#"),"#application.delim1#")>
		<CFSET critb = application.com.globalFunctions.rwListGetAt(Variables.savedCriteria,ListContainsNoCase(Variables.savedFields,"frmPersLastUpdatedWhen","#application.delim1#"),"#application.delim1#")>
			<CFIF critb IS "before"><CFSET op = "before"><CFELSEIF critb IS "after"><CFSET op = "on or after"></CFIF>
	UNION
	SELECT 8,
		   'Person Last Updated #op#',
		   '#DateFormat(crita, "dd-mmm-yyyy")#'
	  FROM Dual
	</CFIF>

	<CFIF #ListContainsNoCase(Variables.savedFields,"frmPersLastUpdatedby","#application.delim1#")# IS NOT 0>
		<CFSET crit = application.com.globalFunctions.rwListGetAt(Variables.savedCriteria,ListContainsNoCase(Variables.savedFields,"frmPersLastUpdatedby","#application.delim1#"),"#application.delim1#")>
	UNION
	SELECT 9,
		   'Person Last Updated By',
		   '#crit#'
	  FROM Dual
	</CFIF>


 	<CFIF #ListContainsNoCase(Variables.savedFields,"frmCompanyName","#application.delim1#")# IS NOT 0>
	 	<CFSET crit = #application.com.globalFunctions.rwListGetAt(Variables.savedCriteria,ListContainsNoCase(Variables.savedFields,"frmCompanyName","#application.delim1#"),"#application.delim1#")#>
	UNION
	SELECT 12,
		   'Company Name',
		   '#crit#'
	  FROM Dual
	</CFIF>
	
 	<CFIF #ListContainsNoCase(Variables.savedFields,"frmCompanyFrom","#application.delim1#")# IS NOT 0 or  #ListContainsNoCase(Variables.savedFields,"frmCompanyTo","#application.delim1#")# IS NOT 0>

		<CFIF #ListContainsNoCase(Variables.savedFields,"frmCompanyFrom","#application.delim1#")# IS NOT 0 >
		 	<CFSET crit1 = #replace(application.com.globalFunctions.rwListGetAt(Variables.savedCriteria,ListContainsNoCase(Variables.savedFields,"frmCompanyFrom","#application.delim1#"),"#application.delim1#"),">="," between ","ONE")#>		
		<CFELSE>
			<CFSET crit1 = " between A">
		</cfif>
		
		<CFIF #ListContainsNoCase(Variables.savedFields,"frmCompanyTo","#application.delim1#")# IS NOT 0 >
		 	<CFSET crit2 = #replace(application.com.globalFunctions.rwListGetAt(Variables.savedCriteria,ListContainsNoCase(Variables.savedFields,"frmCompanyTo","#application.delim1#"),"#application.delim1#"),"<"," and ","ONE" )#>		
		<CFELSE>
			<CFSET crit2 = " and end of alphabet">
		</cfif>


	UNION
	SELECT 12.5,
		   'Company Name ',
		   '#crit1#  #crit2#'
	  FROM Dual
	</CFIF>

	<CFIF #ListContainsNoCase(Variables.savedFields,"frmLocationID","#application.delim1#")# IS NOT 0>
		<CFSET crit = application.com.globalFunctions.rwListGetAt(Variables.savedCriteria,ListContainsNoCase(Variables.savedFields,"frmLocationID","#application.delim1#"),"#application.delim1#")>
	UNION
	SELECT 12.75,
		   'Location ID',
		   '#crit#'
	  FROM Dual
	</CFIF>

	<CFIF #ListContainsNoCase(Variables.savedFields,"frmAccountTypeID","#application.delim1#")# IS NOT 0>
	 	<CFSET crit = #application.com.globalFunctions.rwListGetAt(Variables.savedCriteria,ListContainsNoCase(Variables.savedFields,"frmAccountTypeID","#application.delim1#"),"#application.delim1#")#>
	UNION
	SELECT 12.8,
		   'Account Type',
		   typeTextID
	  FROM organisationType
	 WHERE organisationTypeID #PreserveSingleQuotes(crit)#
	</CFIF>
	

 	<CFIF #ListContainsNoCase(Variables.savedFields,"frmDataSource","#application.delim1#")# IS NOT 0>
	 	<CFSET crit = #application.com.globalFunctions.rwListGetAt(Variables.savedCriteria,ListContainsNoCase(Variables.savedFields,"frmDataSource","#application.delim1#"),"#application.delim1#")#>
	UNION
	SELECT 13,
		   'Location Data Source',
		   Description
	  FROM DataSource
	 WHERE DataSourceID #PreserveSingleQuotes(crit)#
	</CFIF>
	
	<CFIF #ListContainsNoCase(Variables.savedFields,"frmLocationLastUpdateddate","#application.delim1#")# IS NOT 0>
		<CFSET crita = application.com.globalFunctions.rwListGetAt(Variables.savedCriteria,ListContainsNoCase(Variables.savedFields,"frmLocationLastUpdateddate","#application.delim1#"),"#application.delim1#")>
		<CFSET critb = application.com.globalFunctions.rwListGetAt(Variables.savedCriteria,ListContainsNoCase(Variables.savedFields,"frmLocationLastUpdatedWhen","#application.delim1#"),"#application.delim1#")>
			<CFIF critb IS "before"><CFSET op = "before"><CFELSEIF critb IS "after"><CFSET op = "on or after"></CFIF>
	UNION
	SELECT 14,
		   'Location Last Updated #op#',
		   '#DateFormat(crita, "dd-mmm-yyyy")#'
	  FROM Dual
	</CFIF>

	<CFIF #ListContainsNoCase(Variables.savedFields,"frmLocationLastUpdatedby","#application.delim1#")# IS NOT 0>
		<CFSET crit = application.com.globalFunctions.rwListGetAt(Variables.savedCriteria,ListContainsNoCase(Variables.savedFields,"frmLocationLastUpdatedby","#application.delim1#"),"#application.delim1#")>
	UNION
	SELECT 15,
		   'Location Last Updated By',
		   '#crit#'
	  FROM Dual
	</CFIF>

	<CFIF #ListContainsNoCase(Variables.savedFields,"frmLocationFax","#application.delim1#")# IS NOT 0>
		<CFSET crit = application.com.globalFunctions.rwListGetAt(Variables.savedCriteria,ListContainsNoCase(Variables.savedFields,"frmLocationFax","#application.delim1#"),"#application.delim1#")>
	UNION
	SELECT 16,
		   'Location Fax',
		   '#crit#'
	  FROM Dual
	</CFIF>
	<CFIF #ListContainsNoCase(Variables.savedFields,"frmDirectPartner","#application.delim1#")# IS NOT 0>
		<CFSET crit = application.com.globalFunctions.rwListGetAt(Variables.savedCriteria,ListContainsNoCase(Variables.savedFields,"frmDirectPartner","#application.delim1#"),"#application.delim1#")>
	UNION
	SELECT 18,
		   'Location Direct Partner',
		   '<CFIF crit IS "= 1">Yes<CFELSEIF crit IS "= 0">No</CFIF>'
	  FROM Dual
	</CFIF>
	
	<CFIF #ListContainsNoCase(Variables.savedFields,"frmPostalCodetext","#application.delim1#")# IS NOT 0>
		<CFSET crit = application.com.globalFunctions.rwListGetAt(Variables.savedCriteria,ListContainsNoCase(Variables.savedFields,"frmPostalCodetext","#application.delim1#"),"#application.delim1#")>
	UNION
	SELECT 17,
		   'Location Postal Code',
		   '#crit#'
	  FROM Dual
	<CFELSEIF (#ListContainsNoCase(Variables.savedFields,"frmPostalCodefrom","#application.delim1#")# IS NOT 0)
			AND (#ListContainsNoCase(Variables.savedFields,"frmPostalCodeto","#application.delim1#")# IS NOT 0)>
		<CFSET crita = application.com.globalFunctions.rwListGetAt(Variables.savedCriteria,ListContainsNoCase(Variables.savedFields,"frmPostalCodefrom","#application.delim1#"),"#application.delim1#")>
		<CFSET critb = application.com.globalFunctions.rwListGetAt(Variables.savedCriteria,ListContainsNoCase(Variables.savedFields,"frmPostalCodeto","#application.delim1#"),"#application.delim1#")>
		UNION
	SELECT 17,
		   'Location Postal Code Range',
		   '#crita# - #critb#'
	  FROM Dual
	</CFIF>



	<!--- dups will be removed by union --->	
	ORDER BY position, description, criteria
	</CFQUERY>


<!--- print the standard search criteria table --->
<!--- <P>The database was searched based on the following criteria: --->
	<h4>SEARCH CRITERIA</h4>
		<CFOUTPUT QUERY="getSearchText" GROUP="description">
			<div class="col-xs-6">
    			<p>#htmleditformat(description)#:</p>
			</div>
			<div class="col-xs-6">
				<p>
					<CFSET cnt = 1>
					<!---- remove the LIKE stuff, The below is all on one line to ensure
						no space after each item before the comma, Single quotes are removed
						except where 2 are together in which case 1 is kept --->
					<CFOUTPUT><CFIF cnt GT 1>, </CFIF>#HTMLEditFormat(Trim(REReplace(REReplace(REReplace(criteria,"''","single#application.delim1#quote","ALL"),"'|LIKE N|LIKE ","","ALL"),"single#application.delim1#quote","'","ALL")))#<CFSET cnt = cnt + 1></CFOUTPUT>
				</p>
			</div>
			<div class="col-xs-12">
				<IMG SRC="/images/misc/rule.gif" WIDTH=230 HEIGHT=3 ALT="" BORDER="0">
			</div>
		</CFOUTPUT>


