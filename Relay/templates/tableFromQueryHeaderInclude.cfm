<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			tableFromQueryHeaderInclude.cfm	
Author:				KEP
Date started:		2003-11-17
	
Description:	 Recycles the URL parameters for tableFromQueryObject tag

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->
<!--- --------------------------------------------------------------------- --->
<!--- recycle url parameters into form structure for the record paging --->
<!--- --------------------------------------------------------------------- --->

<cfparam name="form.frmSearchText" type="string" default="#iif( isdefined( "url.frmSearchText" ), 'url["frmSearchText"]', de( "" ) )#">
<cfparam name="form.frmSearchColumn" type="string" default="#iif( isdefined( "url.frmSearchColumn" ), 'url["frmSearchColumn"]', de( "" ) )#">
<cfparam name="form.frmSearchComparitor" type="string" default="#iif( isdefined( "url.frmSearchComparitor" ), 'url["frmSearchComparitor"]', de( "" ) )#">
<cfparam name="form.frmSearchColumnDataType" type="string" default="#iif( isdefined( "url.frmSearchColumnDataType" ), 'url["frmSearchColumnDataType"]', de( "" ) )#">
<cfparam name="form.frmWhereClauseList" type="string" default="#iif( isdefined( "url.frmWhereClauseList" ), 'url["frmWhereClauseList"]', de( "" ) )#">

<cfparam name="form.frmAlphabeticalIndex" type="string" default="#iif( isdefined( "url.frmAlphabeticalIndex" ), 'url["frmAlphabeticalIndex"]', de( "" ) )#">

<!--- --------------------------------------------------------------------- --->
<!--- recycle url parameters except for startrow etc --->
<!--- --------------------------------------------------------------------- --->
<cfset queryString = "">
<cfloop collection="#url#" item="urlItem">
	<cfif ListFindNoCase( "startrow,numRowsPerPage,urlroot,delim1,sitedomain,go", urlItem ) eq 0>
		<cfset queryString = queryString & "&" & urlItem & "=" & URLEncodedFormat( url[urlItem] )>
	</cfif>
</cfloop>
<cfloop collection="#form#" item="formItem">
	<cfif ListFindNoCase( "startrow,numRowsPerPage,fieldnames,urlroot,delim1,sitedomain,go", formItem ) eq 0>
		<cfset queryString = queryString & "&" & formItem & "=" & URLEncodedFormat( form[formItem] )>
		<cfset application.com.request.setTopHead(queryString = queryString)>
	</cfif>
</cfloop>
<!--- --------------------------------------------------------------------- --->