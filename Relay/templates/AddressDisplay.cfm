<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
WAB 2010/10/13  removed get TextPhrases and replaced with CF_translate
--->
<CFQUERY NAME="GetCountryID" datasource="#application.siteDataSource#">
	SELECT CountryID from Location, Person 
	WHERE Location.LocationID = Person.LocationID
	AND personID = #request.relayCurrentUser.personid#
</CFQUERY>

<!--- get address format --->
<CFSET Countryid = getCountryID.countryID>
<CFSET screenid = 'standardaddressformat'>
<CFSET screenqueryname = "addressFormat">
<CFSET frmmethod = "edit">   
<cfinclude template="/screen/qryscreendefinition.cfm">



<CFQUERY NAME="GetRightCountries" DATASOURCE=#application.sitedatasource#>
		SELECT * FROM country
		WHERE countryid IN (SELECT CountryMemberID 
							from CountryGroup 
							where CountryGroupID IN (SELECT CountryID 
									from Country 
									where ISOCode IS NULL 
									and CountryDescription = 'Europe'))
		ORDER BY CountryDescription						
</CFQUERY>
<cf_translate>
<CFLOOP query="addressFormat">
	<CFOUTPUT>
		<TR>
			<td align="right" class="Label">		
				<CFIF #trim(fieldLabel)# is not "">phr_#htmleditformat(fieldLabel)#</cfif>
			</td>
	
			<TD>
				<CFIF #fieldTextID# is "Countryid">
					<CFSET thisLineData = Lead.CountryID>
					<CFSET thisLineFormField = "CountryID">
					<SELECT Name="CountryID">
					<CFLOOP QUERY="GetRightCountries">
					<OPTION value="#CountryID#" <CFIF CountryID EQ GetCountryID.CountryID>SELECTED</CFIF> >#htmleditformat(CountryDescription)#
					</CFLOOP>
					</SELECT>
				<CFELSE>
					<CFIF frmMethod IS "edit"><CF_INPUT TYPE="Text" NAME="#fieldTextId#" VALUE="#evaluate("Lead.#fieldTextID#")#" SIZE="20" MAXLENGTH="50"><CFelse>#evaluate("Lead.#htmleditformat(fieldTextID)#")#</cfif>
				</cfif>
			</TD>
		</TR>
	</CFOUTPUT>
</cfloop>
</cf_translate>
	


