<!--- Relayware. All Rights Reserved 2014 --->
<!--- 
This page is included in the border sets (of post 2016.1 customers), it pulls in settings and autostyles the portal.

It was primarily written by Steven Nettleingham then plugged in by Richard Tingle

--->

<cfoutput>

<!--- General Color Params --->
<cfparam name="primaryColor" default="#canonicaliseHexColour(application.com.settings.getSetting('portalStylingOptions.General.primaryColor'))#"> <!---Hex--->
<cfparam name="secondaryColor" default="#canonicaliseHexColour(application.com.settings.getSetting('portalStylingOptions.General.secondaryColor'))#"> <!---Hex--->

<cfparam name="linkColorOverride" default="#canonicaliseHexColour(application.com.settings.getSetting('portalStylingOptions.General.linkColorOverride'))#"> <!---Hex--->
<cfparam name="linkHoverColorOverride" default="#canonicaliseHexColour(application.com.settings.getSetting('portalStylingOptions.General.linkHoverColorOverride'))#"> <!---Hex--->

<cfparam name="buttonColorOverride" default="#canonicaliseHexColour(application.com.settings.getSetting('portalStylingOptions.General.buttonColorOverride'))#"> <!---Hex--->
<cfparam name="buttonHoverColorOverride" default="#canonicaliseHexColour(application.com.settings.getSetting('portalStylingOptions.General.buttonHoverColorOverride'))#"> <!---Hex--->

<cfparam name="headerOneColorOverride" default="#canonicaliseHexColour(application.com.settings.getSetting('portalStylingOptions.General.headerOneColorOverride'))#"> <!---Hex--->
<cfparam name="headerTwoColorOverride" default="#canonicaliseHexColour(application.com.settings.getSetting('portalStylingOptions.General.headerTwoColorOverride'))#"> <!---Hex--->
<cfparam name="headerThreeColorOverride" default="#canonicaliseHexColour(application.com.settings.getSetting('portalStylingOptions.General.headerThreeColorOverride'))#"> <!---Hex--->
<cfparam name="headerFourColorOverride" default="#canonicaliseHexColour(application.com.settings.getSetting('portalStylingOptions.General.headerFourColorOverride'))#"> <!---Hex--->

<cfparam name="textColorOverride" default="#canonicaliseHexColour(application.com.settings.getSetting('portalStylingOptions.General.textColorOverride'))#"> <!---Hex--->
<cfparam name="tableHeaderColorOverride" default="#canonicaliseHexColour(application.com.settings.getSetting('portalStylingOptions.General.tableHeaderColorOverride'))#"> <!---Hex--->

<!--- Header Params --->
<cfparam name="menuColorOverride" default="#canonicaliseHexColour(application.com.settings.getSetting('portalStylingOptions.Header.menuColorOverride'))#"> <!---Hex--->
<cfparam name="menuHoverColorOverride" default="#canonicaliseHexColour(application.com.settings.getSetting('portalStylingOptions.Header.menuHoverColorOverride'))#"> <!---Hex--->

<!--- SubHeader Params --->
<cfparam name="addSubheaderToHeader" default="#application.com.settings.getSetting('portalStylingOptions.SubHeader.addSubheaderToHeader')#"> <!---Boolean--->
<cfparam name="darkSubheader" default="#application.com.settings.getSetting('portalStylingOptions.SubHeader.darkSubheader')#"> <!---Boolean--->
<cfparam name="subheaderColorOverride" default="#canonicaliseHexColour(application.com.settings.getSetting('portalStylingOptions.SubHeader.subheaderColorOverride'))#"> <!---Hex--->
<cfparam name="subheaderTitleOverride" default="#canonicaliseHexColour(application.com.settings.getSetting('portalStylingOptions.SubHeader.subheaderTitleOverride'))#"> <!---Hex--->

<!--- Footer Params --->
<cfparam name="darkFooter" default="#application.com.settings.getSetting('portalStylingOptions.Footer.darkFooter')#"> <!---Boolean--->
<cfparam name="footerColorOverride" default="#canonicaliseHexColour(application.com.settings.getSetting('portalStylingOptions.Footer.footerColorOverride'))#"> <!---Hex--->
<cfparam name="footerLowerColorOverride" default="#canonicaliseHexColour(application.com.settings.getSetting('portalStylingOptions.Footer.footerLowerColorOverride'))#"> <!---Hex--->

<cfparam name="linkedInURL" default="#application.com.settings.getSetting('portalStylingOptions.Footer.linkedInURL')#"> <!---URL--->
<cfparam name="facebookURL" default="#application.com.settings.getSetting('portalStylingOptions.Footer.facebookURL')#"> <!---URL--->
<cfparam name="twitterURL" default="#application.com.settings.getSetting('portalStylingOptions.Footer.twitterURL')#"> <!---URL--->
<cfparam name="googleURL" default="#application.com.settings.getSetting('portalStylingOptions.Footer.googleURL')#"> <!---URL--->
<cfparam name="youtubeURL" default="#application.com.settings.getSetting('portalStylingOptions.Footer.youtubeURL')#"> <!---URL--->
<cfparam name="blogURL" default="#application.com.settings.getSetting('portalStylingOptions.Footer.blogURL')#"> <!---URL---> 

<!--- Left Border Params --->
<cfparam name="moveMenuToRight" default="#application.com.settings.getSetting('portalStylingOptions.LeftBorder.moveMenuToRight')#"> <!---Boolean--->
<cfparam name="leftMenuColorOverride" default="#canonicaliseHexColour(application.com.settings.getSetting('portalStylingOptions.LeftBorder.leftMenuColorOverride'))#"> <!---Hex--->
<cfparam name="leftMenuHoverColorOverride" default="#canonicaliseHexColour(application.com.settings.getSetting('portalStylingOptions.LeftBorder.leftMenuHoverColorOverride'))#"> <!---Hex--->

<!---set a default dark color for subheader if not specified --->
<cfif (darkSubheader eq true) and (subheaderColorOverride eq "")>
	<cfset subheaderColorOverride = "666666">
</cfif>
		
<!---set a default dark color for footer if not specified --->
<cfif (darkFooter eq true) and (footerColorOverride eq "")>
	<cfset footerColorOverride = "666666">
</cfif>
 
<style>
	<cfif primaryColor neq "">
		a.button,
		.btn-primary, 
		.cc-cookies button.cookiesAccept, 
		.jspDrag  { background:###primaryColor#; border:1px solid ###primaryColor#; }
		
		##cookiesContainer, 
		##leftNav li, 
		##footerContainer:before,
		.dropdown-menu > li.active,
		.dropdown-menu > li.active > a,
		##navbarCollapse .navbar-nav,
		##bannerRotator ul li,
		.cta:nth-child(odd) a{ background: ###primaryColor#;}
		
		.navbar-default .navbar-toggle{ background: ###primaryColor#;}
		
		a, 
		##subHeader h1,
		button.productsViewAllBtn,
		button.productsViewAllBtnSub {color: ###primaryColor#; }
		
		##headNavbarContainerWrapper {border-color:###primaryColor#;background: ###primaryColor#;}
		
		.btn-primary.disabled, .btn-primary[disabled], fieldset[disabled] .btn-primary, .btn-primary.disabled:hover, .btn-primary[disabled]:hover, fieldset[disabled] .btn-primary:hover, .btn-primary.disabled:focus, .btn-primary[disabled]:focus, fieldset[disabled] .btn-primary:focus, .btn-primary.disabled:active, .btn-primary[disabled]:active, fieldset[disabled] .btn-primary:active, .btn-primary.disabled.active, .btn-primary[disabled].active, fieldset[disabled] .btn-primary.active{
			background-color: ###primaryColor#;
			border-color: ###primaryColor#;
		}
		
	</cfif>
	
	<cfif secondaryColor neq "">
		a.button:hover,
		a.button:active,
		a.button:focus,
		.btn-primary:hover, 
		.btn-primary:active,
		.btn-primary:focus,
		.cc-cookies button:hover.cookiesAccept:hover, 
		.cc-cookies button:hover.cookiesAccept:active, 
		.cc-cookies button:hover.cookiesAccept:focus, 
		.nav .open > a, 
		.nav .open > a:hover, 
		.nav .open > a:focus { background:###secondaryColor#; border:1px solid ###secondaryColor#; }
		
		##leftNav li.selected, 
		##leftNav li a:hover, 
		##leftNav li a:focus, 
		##leftNav li a:active, 
		##innerContentContainer ul.filterLinks, 
		.navbar-default .navbar-nav>li:hover>a, 
		.navbar-default .navbar-nav > li.active,
		.navbar-default .navbar-nav > .active > a,
		.dropdown-menu,
		.login-header,
		ul##businessPlanTabs,
		ul##partnerLocatorTabs,
		##locatorContainer ul.viewLinks,
		table thead,
		##bannerRotator ##bannerNav a.active,
		.cta:nth-child(even) a{ background:###secondaryColor#;}
		
		.navbar-default .navbar-toggle:hover,
		.navbar-default .navbar-toggle:focus,
		.navbar-default .navbar-toggle:active,
		##navbarCollapse.hidden-sm ##mainNavigation li a:hover,
		##navbarCollapse.hidden-sm ##mainNavigation li a:focus,
		##navbarCollapse.hidden-sm ##mainNavigation li a:active{ background:###secondaryColor#;}	
		
		a:hover, 
		a:focus, 
		a:active,
		button.productsViewAllBtn:hover,
		button.productsViewAllBtnSub:hover {  color: ###secondaryColor#; }
		
		##activityStreamContainerSub, .login-form{
			border-color: ###secondaryColor#;
		}
	</cfif>
	
	<cfif linkColorOverride neq "">
		a {  color: ###linkColorOverride#; }
	</cfif>
	
	<cfif linkHoverColorOverride neq "">
		a:hover, 
		a:focus, 
		a:active{  color: ###linkHoverColorOverride#; }
	</cfif>
	
	<cfif buttonColorOverride neq "">
		.btn-primary, 
		.cc-cookies button.cookiesAccept{ background:###buttonColorOverride#; border:1px solid ###buttonColorOverride#; }
		
		.btn-primary.disabled, .btn-primary[disabled], fieldset[disabled] .btn-primary, .btn-primary.disabled:hover, .btn-primary[disabled]:hover, fieldset[disabled] .btn-primary:hover, .btn-primary.disabled:focus, .btn-primary[disabled]:focus, fieldset[disabled] .btn-primary:focus, .btn-primary.disabled:active, .btn-primary[disabled]:active, fieldset[disabled] .btn-primary:active, .btn-primary.disabled.active, .btn-primary[disabled].active, fieldset[disabled] .btn-primary.active{
			background-color: ###buttonColorOverride#;
			border-color: ###buttonColorOverride#;
		}
	</cfif>

	<cfif buttonHoverColorOverride neq "">
		.btn-primary:hover, 
		.btn-primary:active,
		.btn-primary:focus { background:###buttonHoverColorOverride#; border:1px solid ###buttonHoverColorOverride#; }
	</cfif>
	
	<cfif headerOneColorOverride neq "">
		h1, .h1 { color:###headerOneColorOverride#; }
	</cfif>
	
	<cfif headerTwoColorOverride neq "">
		h2, .h2 { color:###headerTwoColorOverride#; }
	</cfif>
	
	<cfif headerThreeColorOverride neq "">
		h3, .h3 { color:###headerThreeColorOverride#; }
	</cfif>
	
	<cfif headerFourColorOverride neq "">
		h4, .h4 { color:###headerFourColorOverride#; }
	</cfif>
	
	<cfif textColorOverride neq "">
		body { color:###textColorOverride#; }
	</cfif>

	
	<cfif subheaderColorOverride neq "">
			##subHeader > div, ##subheadContainerWrapper{ background: ###subheaderColorOverride#;}
	</cfif>
	
	<cfif subheaderTitleOverride neq "">
			##subHeader h1, ##subHeader.subheader-dark h1{ color: ###subheaderTitleOverride#;}
	</cfif>
		
	<cfif menuColorOverride neq "">
		.dropdown-menu > li.active,
		.dropdown-menu > li.active > a,
		##navbarCollapse .navbar-nav{ background: ###menuColorOverride#;}
		##headNavbarContainerWrapper {	border-color:###menuColorOverride#;	background: ###menuColorOverride#;	}
	</cfif>

	<cfif menuHoverColorOverride neq "">
		.nav .open > a, 
		.nav .open > a:hover, 
		.nav .open > a:focus { background:###menuHoverColorOverride#; border:1px solid ###menuHoverColorOverride#; }
		
		.navbar-default .navbar-nav>li:hover>a, 
		.navbar-default .navbar-nav > li.active,
		.navbar-default .navbar-nav > .active > a,
		.dropdown-menu	{ background:###menuHoverColorOverride#;}
		
		.navbar-default .navbar-toggle:hover,
		.navbar-default .navbar-toggle:focus,
		.navbar-default .navbar-toggle:active,
		##navbarCollapse.hidden-sm ##mainNavigation li a:hover,
		##navbarCollapse.hidden-sm ##mainNavigation li a:focus,
		##navbarCollapse.hidden-sm ##mainNavigation li a:active{ background:###menuHoverColorOverride#;}	
	</cfif>	
	
	<cfif footerColorOverride neq "">
		##footerContainer{ background-color: ###footerColorOverride#; }
		.footer-dark .socialDiv a .social-icon { fill: ###footerColorOverride#; }
	</cfif>
	
	<cfif footerLowerColorOverride neq "">
		##footerContainer:before{ background: ###footerLowerColorOverride#;}
	</cfif>

	<cfif (moveMenuToRight eq true)>
		##contentLeftWrapper{ float:right;}
	</cfif>

	<cfif leftMenuColorOverride neq "">
		##leftNav li{ background: ###leftMenuColorOverride#;}
	</cfif>

	<cfif leftMenuHoverColorOverride neq "">
		##leftNav li.selected, 
		##leftNav li a:hover, 
		##leftNav li a:focus, 
		##leftNav li a:active{ background:###leftMenuHoverColorOverride#;}
	</cfif>
	
	
</style>
</cfoutput>

<cfscript>
	private string function canonicaliseHexColour(string colour){ 
		//we don't want our colors with hashes on the front but people often put hashs on the front
		return Replace(colour,'##','','all');
		
	}
</cfscript>
