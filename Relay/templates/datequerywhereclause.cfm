<!--- �Relayware. All Rights Reserved 2014 --->
<!---
NAME:           DateQueryWhereClause
FILE:			relay\templates\DateQueryWhereClause.cfm
CREATED:		*********
LAST MODIFIED:	2007-08-03
VERSION:	    1.0
AUTHOR:
DESCRIPTION:    This file builds up date structures and passes them to query from table object in form of a structure
Usage:
Errors:


Mods
Note: The query needs to be changed to sort correctly
Note2: this needs turning into a custom tag but hear we go any way

SSS: 2007-08-02 I have added the ability for days to be added to this so that people can sort on days as well as months.
SSS: 2007-08-02 This file has a switch in it so that days can be truned on or off
SSS: 2007-09-17 Added locale switch to avoid dates being formatted incorrectly for SQL
SSS: 2007-12-13 I have added the ability to default dates varibles used is usePartialRange and 2 dates must be passed
			    PartialRangeStartDate and PartialRangeEndDate.
SSS: 2008-06-16 I have added a cfelse that defaults the endday to the last day in a month sent to it. I have tested it but if anybody sees a problem with this please send it back and I will rethink a solution.
SSS: 2009-02-20 I have changed the cfelse above to be more clever in how it lets through pre set dates if enddate or start date are greater then the days in the month
				the dates are automatically set to the last day in the month (ie 31-Feb-2009 would revert to 28 or 29 - FEB-2009
SSS: 2009/05/12	If select by day is not selected the end month should have the max days for that month selected
NYB 2009-08-25 P-ATI002 - made a few changes around the UsePartialRange functionality, as it was limiting the query results, but change was not reflecting in the filter dropdown options
SSS 2009/09/04 If the dates passed in are empty do not do any calculations of them
NYB 2009-10-01 LHID2703
NAS	2009-12-07 LID 2885 - Changed to stop the initialising of 'form.DayStart' and 'form.DayEnd'
NYB 2011-12-16 ST425136 added dates in clauseCDefault & clauseDDefault to getMonthDateRangeData (ie, month drop down) when they don't exist
				- to stop Month Start and Month End being selected despite the the filter being applied
YMA 2013/01/23 Case:432560 enforce default month field to use english locale when prodessing month
--->

<!--- 2005/11/22 GCC AID613 - these exist as url vars - picking up now for excel export --->
<cfscript>
oldlocal = GetLocale();
SetLocale("English (UK)");
</cfscript>

<!--- NJH 2008/12/19 Potential bug.. found as a result of trying to fix ATI Issue 1518. Url field were being passed as blank to excel
which meant that the form variables didn't get set which meant that the date fields became default where in reality we didn't want them.
This hasn't been tested at all although it seems like a sensible change and works for the issue I am currently dealing with. --->
<cfif isdefined("url.FRMWHERECLAUSEA") <!--- and len(url.FRMWHERECLAUSEA) gt 0 --->>
	<cfset form.FRMWHERECLAUSEA = url.FRMWHERECLAUSEA>
</cfif>
<cfif isdefined("url.FRMWHERECLAUSEB")<!---  and len(url.FRMWHERECLAUSEB) gt 0 --->>
	<cfset form.FRMWHERECLAUSEB = url.FRMWHERECLAUSEB>
</cfif>
<cfif isdefined("url.FRMWHERECLAUSEC") <!--- and len(url.FRMWHERECLAUSEC) gt 0 --->>
	<cfset form.FRMWHERECLAUSEC = url.FRMWHERECLAUSEC>
</cfif>
<cfif isdefined("url.FRMWHERECLAUSED") <!--- and len(url.FRMWHERECLAUSED) gt 0 --->>
	<cfset form.FRMWHERECLAUSED = url.FRMWHERECLAUSED>
</cfif>
<cfif isdefined("url.daystart") and len(url.daystart) gt 0>
	<cfset form.daystart = url.daystart>
</cfif>
<cfif isdefined("url.dayend") and len(url.dayend) gt 0>
	<cfset form.dayend = url.dayend>
</cfif>

<cfparam name="phrasePrefix" type="string" default=""> <!--- NJH 2007/05/29 --->
<cfparam name="clauseADefault" default="">
<cfparam name="clauseBDefault" default="">
<!--- YMA 2013/01/23 Case:432560 enforce default month field to use english locale when prodessing month --->
<cfparam name="clauseCDefault" default="#left(monthAsString(month(now()),'en'),3)#-#year(now())#">
<cfparam name="clauseDDefault" default="#left(monthAsString(month(dateadd("m",6,now())),'en'),3)#-#year(dateadd("m",6,now()))#">

<cfparam name="useFullRange" default="false">
<cfparam name="usePartialRange" default="false">

<cfparam name="dateField" default="forecastShipDate">
<cfparam name="tableName" default="opportunityProduct">

<cfscript>
queryWhereClause = StructNew();
</cfscript>

<!--- START: NYB 2009-08-25 P-ATI002 - added, because usePartialRange doesn't seem to work properly: --->
<cfif usePartialRange and (not useFullRange)>
	<cfif isdefined("PartialRangeStartDate")>
		<cfset PartialRangeQuarterStartDate = (datepart("yyyy", PartialRangeStartDate)*10)+datepart("q", PartialRangeStartDate)>
		<cfset PartialRangeMonthStartDate = (datepart("yyyy", PartialRangeStartDate)*100)+datepart("m", PartialRangeStartDate)>
	</cfif>
	<cfif isdefined("PartialRangeEndDate")>
		<cfset PartialRangeQuarterEndDate = (datepart("yyyy", PartialRangeEndDate)*10)+datepart("q", PartialRangeEndDate)>
		<cfset PartialRangeMonthEndDate = (datepart("yyyy", PartialRangeEndDate)*100)+datepart("m", PartialRangeEndDate)>
	</cfif>
</cfif>

<!--- END: NYB 2009-08-25 P-ATI002 --->

<cfquery name="getQuarterDateRangeData" datasource="#application.siteDataSource#">
select distinct
	'Q'+convert(varchar(3),rtrim(datename(q, #dateField#)))+'-'+convert(varchar(4),datepart(yyyy, #dateField#)) as Quarter_expected_close,
	datepart(yy, #dateField#), datepart(q, #dateField#)
	 as Quarter_expected_close
	from #PreserveSingleQuotes(tableName)# <cfif findnocase(" ",tablename) eq 0>with(nolock)</cfif>
	where #dateField# is not null
	<!--- START: NYB 2009-08-25 P-ATI002 - added, because usePartialRange doesn't seem to work properly: --->
	<cfif usePartialRange and not useFullRange>
		<cfif isdefined("PartialRangeStartDate")>
			and
			(datepart(yy, #dateField#)*10)+datepart(q, #dateField#) >=  <cf_queryparam value="#PartialRangeQuarterStartDate#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfif>
		<cfif isdefined("PartialRangeEndDate")>
			and
			(datepart(yy, #dateField#)*10)+datepart(q, #dateField#) <=  <cf_queryparam value="#PartialRangeQuarterEndDate#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfif>
	</cfif>
	<!--- END: NYB 2009-08-25 P-ATI002 --->
	order by datepart(yy, #dateField#), datepart(q, #dateField#)
</cfquery>

<cfset structKey = "A">
<cfset n = 1>
<cfscript>
	queryWhereClause[structKey] = StructNew();
	queryWhereClause[structKey][n]["title"] = "phr_sys_#phrasePrefix#Quarter";
	queryWhereClause[structKey][n]["sql"] = "";
</cfscript>

<cfoutput query="getQuarterDateRangeData">
	<cfscript>
		n = n + 1;
		queryWhereClause[structKey][n]["title"] = getQuarterDateRangeData.Quarter_expected_close;
		queryWhereClause[structKey][n]["sql"] = getQuarterDateRangeData.Quarter_expected_close;
	</cfscript>
</cfoutput>

<!--- ==============================================================================
      Month Date Range
=============================================================================== --->
<!--- 2011-12-16 NYB ST425136 added alias to unnamed fields in query: --->
<cfquery name="getMonthDateRangeData" datasource="#application.siteDataSource#">
select distinct
	convert(varchar(3),rtrim(datename(m, #dateField#))) +'-'+convert(varchar(4),datepart(yyyy, #dateField#)) as Month_expected_close,
	datepart(yy, #dateField#) as [year], datepart(m, #dateField#) as monthNum
	from #PreserveSingleQuotes(tableName)# <cfif findnocase(" ",tablename) eq 0>with(nolock)</cfif>
	where #dateField# is not null
	<!--- START: NYB 2009-08-25 P-ATI002 - added, because usePartialRange doesn't seem to work properly: --->
	<cfif usePartialRange and not useFullRange>
		<cfif isdefined("PartialRangeStartDate")>
			and
			(datepart(yy, #dateField#)*100)+datepart(m, #dateField#) >=  <cf_queryparam value="#PartialRangeMonthStartDate#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfif>
		<cfif isdefined("PartialRangeEndDate")>
			and
			(datepart(yy, #dateField#)*100)+datepart(m, #dateField#) <=  <cf_queryparam value="#PartialRangeMonthEndDate#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfif>
	</cfif>
	<!--- END: NYB 2009-08-25 P-ATI002 --->
	order by datepart(yy, #dateField#), datepart(m, #dateField#)
</cfquery>

<!--- START: 2011-12-16 NYB ST425136 added: --->
<cfif isDate(clauseCDefault)>
	<cfquery name="checkStartInMonthDateRangeData" dbtype="query">
		select * from getMonthDateRangeData where Month_expected_close = '#clauseCDefault#'
	</cfquery>
	<cfif checkStartInMonthDateRangeData.recordcount eq 0>
		<cfset newRecordCount = QueryAddRow(getMonthDateRangeData)>
		<cfset QuerySetCell(getMonthDateRangeData,'Month_expected_close',clauseCDefault,newRecordCount)>
		<cfset QuerySetCell(getMonthDateRangeData,'year',listlast(clauseCDefault,"-"),newRecordCount)>
		<cfset QuerySetCell(getMonthDateRangeData,'monthNum',Month(clauseCDefault),newRecordCount)>
	</cfif>
</cfif>

<cfif isDate(clauseDDefault)>
	<cfquery name="checkEndInMonthDateRangeData" dbtype="query">
		select * from getMonthDateRangeData where Month_expected_close = '#clauseDDefault#'
	</cfquery>
	<cfif checkEndInMonthDateRangeData.recordcount eq 0>
		<cfset newRecordCount = QueryAddRow(getMonthDateRangeData)>
		<cfset QuerySetCell(getMonthDateRangeData,'Month_expected_close',clauseDDefault,newRecordCount)>
		<cfset QuerySetCell(getMonthDateRangeData,'year',listlast(clauseDDefault,"-"),newRecordCount)>
		<cfset QuerySetCell(getMonthDateRangeData,'monthNum',Month(clauseDDefault),newRecordCount)>
	</cfif>
</cfif>
<!--- END: 2011-12-16 NYB ST425136 --->

<!--- <cfset structKey = "B">
<cfset n = 1>
<cfscript>
	queryWhereClause[structKey] = StructNew();
	queryWhereClause[structKey][n]["title"] = "Product Ship Month";
	queryWhereClause[structKey][n]["sql"] = "";
</cfscript>

<cfoutput query="getMonthDateRangeData">
	<cfscript>
		n = n + 1;
		queryWhereClause[structKey][n]["title"] = getMonthDateRangeData.Month_expected_close;
		queryWhereClause[structKey][n]["sql"] = getMonthDateRangeData.Month_expected_close;
	</cfscript>
</cfoutput> --->

<!--- ==============================================================================
      Ship Month Start from
=============================================================================== --->
<cfset structKey = "C">
<cfset n = 1>
<cfscript>
	queryWhereClause[structKey] = StructNew();
	queryWhereClause[structKey][n]["title"] = "phr_sys_#phrasePrefix#MonthStart";
	queryWhereClause[structKey][n]["sql"] = "";
	queryWhereClause[structKey][n]["daytitle"] = "DayStart";
	queryWhereClause[structKey][n]["day"] = "DayStart";
</cfscript>

<cfoutput query="getMonthDateRangeData">
	<cfscript>
		n = n + 1;
		queryWhereClause[structKey][n]["title"] = Month_expected_close;
		queryWhereClause[structKey][n]["sql"] = Month_expected_close;
		queryWhereClause[structKey][n]["day"] = "";
	</cfscript>
</cfoutput>

<cfif structKeyExists(queryWhereClause[structKey],2)>
	<cfset lowestDate = queryWhereClause[structKey][2]["sql"]>
<cfelse>
	<cfset lowestDate = queryWhereClause[structKey][1]["sql"]>
</cfif>

<!--- ==============================================================================
      Ship Month End
=============================================================================== --->
<cfset structKey = "D">
<cfset n = 1>
<cfscript>
	queryWhereClause[structKey] = StructNew();
	queryWhereClause[structKey][n]["title"] = "phr_sys_#phrasePrefix#MonthEnd";
	queryWhereClause[structKey][n]["sql"] = "";
	queryWhereClause[structKey][n]["daytitle"] = "DayEnd";
	queryWhereClause[structKey][n]["day"] = "DayEnd";
</cfscript>

<cfoutput query="getMonthDateRangeData">
	<cfscript>
		n = n + 1;
		queryWhereClause[structKey][n]["title"] = Month_expected_close;
		queryWhereClause[structKey][n]["sql"] = Month_expected_close;
		queryWhereClause[structKey][n]["day"] = "";
	</cfscript>
</cfoutput>

<cfset highestDate = queryWhereClause[structKey][n]["sql"]>

<cfset useDateRange = true>

<!--- 2008/05/08 GCC tweaked to override month partials with quarter dates if set - must be a more elegant way though...
Ideally we shoud get rid of quarter filtering completely as it only saves 1 drop down and gets rid of the Jan-Mar being Q1 or Q3 depending on financial vs calendar quarters... --->

<!--- START: NYB 2009-08-25 P-ATI002 - added cfif around params: --->
<cfparam name="form.FRMWHERECLAUSEA" default="#clauseADefault#">
<cfparam name="form.FRMWHERECLAUSEB" default="#clauseBDefault#">
<cfif usePartialRange>
	<cfparam name="form.FRMWHERECLAUSEC" default="#lowestDate#">
	<cfparam name="form.FRMWHERECLAUSED" default="#highestDate#">
<cfelse>
	<cfparam name="form.FRMWHERECLAUSEC" default="#clauseCDefault#">
	<cfparam name="form.FRMWHERECLAUSED" default="#clauseDDefault#">
</cfif>
<!--- END: NYB 2009-08-25 P-ATI002 --->

<cfif form.FRMWHERECLAUSEA neq "">
	<cfset usefullrange = true>
</cfif>

<cfif useFullRange>
	<!--- START: NYB 2009-08-25 P-ATI002 - added cfif around params: --->
	<!--- END: NYB 2009-08-25 P-ATI002 --->
	<cfset useFullRange = false>
	<cfset form.FRMWHERECLAUSEC = lowestDate>
	<cfset form.FRMWHERECLAUSED = highestDate>
	<cfset Enddatecalculation = highestDate>
	<!--- START: NYB 2009-08-25 P-ATI002 - moved to below: ---
	<cfset form.daystart = 1>
	<cfif highestDate gt 1> <!--- NJH 2007/10/11 --->
		<cfset form.dayend = DaysInMonth("1-#FRMWHERECLAUSED#")>
	<cfelse>
		<cfset form.dayend = 1>
	</cfif>
	!--- END: NYB 2009-08-25 P-ATI002 - moved to below: --->
<cfelseif usePartialRange>
	<!--- START: NYB 2009-08-25 P-ATI002 - replaced: ---
	<cfset usePartialRange = false>
	<cfset form.FRMWHERECLAUSEC = dateformat(PartialRangeStartDate, "mmm-yyyy")>
	<cfset form.FRMWHERECLAUSED = dateformat(PartialRangeEndDate, "mmm-yyyy")>
	!--- WITH: NYB 2009-08-25 P-ATI002 - --->

	<!--- put this in to prevent anyone trying to search from Feb-Jan (eg) - not sure if it's a desired feature --->
	<cfif form.FRMWHERECLAUSED lt form.FRMWHERECLAUSEC>
		<cfset form.FRMWHERECLAUSED = form.FRMWHERECLAUSEC>
	</cfif>

	<!--- END: NYB 2009-08-25 P-ATI002 --->
	<!--- START: NYB 2009-08-25 P-ATI002 - moved to below: ---
	<cfset form.daystart = 1>
	<cfif PartialRangeEndDate gt 1> <!--- NJH 2007/10/11 --->
		<cfset form.dayend = DaysInMonth("1-#FRMWHERECLAUSED#")>
	<cfelse>
		<cfset form.dayend = 1>
	</cfif>
	!--- END: NYB 2009-08-25 P-ATI002 - moved to below: --->
<!--- START: NYB 2009-08-25 P-ATI002 - moved following from above: --->
</cfif>

<!--- START: NAS	2009-12-07 LID 2885 - Changed to stop the initialising of 'form.DayStart' and 'form.DayEnd'  --->
<cfif  Not isdefined("form.daystart")>
<cfset form.daystart = 1>
</cfif>
<!--- 		END: NAS	2009-12-07 LID 2885 - Changed to stop the initialising of 'form.DayStart' and 'form.DayEnd' --->
<cfif highestDate gt 1> <!--- NJH 2007/10/11 --->
<!--- Start LHID 2582SSS 2009/09/04 if FRMWHERECLAUSED is empty do not attempt to get days in month --->
	<cfif FRMWHERECLAUSED NEQ "">
		<!--- START: NYB 2009-10-01 LHID2703 REPLACED: ---
		<cfset form.dayend = DaysInMonth("1-#FRMWHERECLAUSED#")>
		!--- NYB 2009-10-01 LHID2703 - WITH: --->
		<cfset form.dayend = DaysInMonth("#FRMWHERECLAUSED#")>
		<!--- END: NYB 2009-10-01 LHID2703--->
	</cfif>
<!--- End LHID 2582SSS 2009/09/04 if FRMWHERECLAUSED is empty do not attempt to get days in month --->
<cfelse>
	<cfset form.dayend = 1>
</cfif>

<!--- to guarentee we only have one search criteria for form.FRMWHERECLAUSEC --->
<cfif listlen(form.FRMWHERECLAUSEC,",") gt 1>
	<cfset form.FRMWHERECLAUSEC = listlast(form.FRMWHERECLAUSEC,",")>
</cfif>

<cfif listlen(form.FRMWHERECLAUSED,",") gt 1>
	<cfset form.FRMWHERECLAUSED = listlast(form.FRMWHERECLAUSED,",")>
</cfif>

<cfif not useFullRange and not usePartialRange>
<!--- END: NYB 2009-08-25 P-ATI002 --->
	<!--- SSS Bug 474 all sites I have tested this it works I have not yet found a way in which it does not yet if someone does please send it back to me for a rethink --->
	<cfif isdefined("form.daystart") and isdefined("form.dayend") and isDefined("FRMWHERECLAUSED") and FRMWHERECLAUSED neq "">

	<!--- SSS 2009-02-20 If startday and endday are passed run report on those days if the wrong days are passed in fix them --->
		<cfset form.daystart = listlast(form.daystart)>
		<cfset form.dayend = listlast(form.dayend)>
		<!--- START: NYB 2009-10-01 LHID2703 - removed: ---
		<cfset form.dayend = DaysInMonth("1-#FRMWHERECLAUSED#")>
		!--- END: NYB 2009-10-01 LHID2703--->
		<!--- SSS 2009-02-20 daystart is greater then the days in the month set day end to the last day in the month --->

		<!--- START: NYB 2009-10-01 LHID2703 - replaced: ---
		<cfset form.dayend = DaysInMonth("1-#FRMWHERECLAUSED#")>
		<cfif form.daystart GT form.DaysInMonth>
			<cfset form.daystart = form.DaysInMonth>
		</cfif>
		<!--- SSS 2009-02-20 dayend is greater then the days in the month set day end to the last day in the month --->
		<cfif form.dayend GT form.DaysInMonth>
			<cfset form.dayend = form.DaysInMonth>
		</cfif>
		<!--- SSS 2009/05/12 added this so that if select by day is not selected the max for the month is the correct day --->
		<cfif not isdefined("selectbyday") or selectbyday EQ 0>
			<cfset form.dayend = form.DaysInMonth>
		</cfif>
		!--- NYB 2009-10-01 LHID2703 - WITH: --->
		<!--- NAS	2009-12-07 LID 2885 - Changed to stop the initialising of 'form.DayStart' and 'form.DayEnd' Removed 'or not isdefined("selectbyday") or selectbyday EQ 0' --->
		<cfif form.dayend GT DaysInMonth("#FRMWHERECLAUSED#") <!--- or not isdefined("selectbyday") or selectbyday EQ 0 --->>
			<cfset form.dayend = DaysInMonth("#FRMWHERECLAUSED#")>
		</cfif>
		<cfif form.daystart GT form.dayend>
			<cfset form.daystart = form.dayend>
		</cfif>
		<!--- END: NYB 2009-10-01 LHID2703--->
	</cfif>
</cfif>

<cfscript>
// create a structure containing the fields below which we want to be reported when the form is filtered
if (not isDefined("passThruVars")) {
	passThruVars = StructNew();
}
if (isDefined("FRMWHERECLAUSEA")) {
	form.FRMWHERECLAUSEA = listlast(form.FRMWHERECLAUSEA);
	StructInsert(passthruVars, "FRMWHERECLAUSEA", form.FRMWHERECLAUSEA);
}
if (isDefined("FRMWHERECLAUSEB")) {
	StructInsert(passthruVars, "FRMWHERECLAUSEB", form.FRMWHERECLAUSEB);
}
if (isDefined("FRMWHERECLAUSEC")) {
	form.FRMWHERECLAUSEC = listlast(form.FRMWHERECLAUSEC);
	StructInsert(passthruVars, "FRMWHERECLAUSEC", form.FRMWHERECLAUSEC);
}
if (isDefined("FRMWHERECLAUSED")) {
	form.FRMWHERECLAUSED = listlast(form.FRMWHERECLAUSED);
	StructInsert(passthruVars, "FRMWHERECLAUSED", form.FRMWHERECLAUSED);
}
//MAJOR HACK The form vars are comming through is a comma delim listso we are picking the last element we should find out why this is
if (isDefined("daystart")) {
	form.daystart = listlast(form.daystart);
	StructInsert(passthruVars, "daystart", form.daystart);
}
if (isDefined("dayend")) {
	form.dayend = listlast(form.dayend);
	StructInsert(passthruVars, "dayend", form.dayend);
}
if (isDefined("useFullRange")) {
	StructInsert(passthruVars, "useFullRange", useFullRange);
}
if (isDefined("usePartialRange")) {
	StructInsert(passthruVars, "usePartialRange", usePartialRange);
}

// WAB/NJH 2008/10/01 Trend 1.1.3 passing frmWhereClauseList thru so that when a report is sorted, the value gets passed as well
if (isDefined("frmWhereClauseList")) {
	StructInsert(passthruVars, "frmWhereClauseList", frmWhereClauseList);
}


SetLocale(oldlocal);
</cfscript>


<!--- <cfdump var="#queryWhereClause#"> --->
<!--- <cfoutput>

<cfset StructSort( queryWhereClause, "numeric", "ASC")>

<!--- <cfset sortedWhereClause = StructSort( queryWhereClause, "numeric", "DESC", StructKeyList(queryWhereClause) )> --->
#StructKeyList(queryWhereClause)#<br>
</cfoutput> --->

<!--- <cfoutput>#ListToArray( StructKeyList( queryWhereClause[whereClauseStructureItem] ) )#</cfoutput> --->

