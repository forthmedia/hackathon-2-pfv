<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			RelayRecordManagerINI.cfm	
Author:				GCC
Date started:		2007/05/21
	
Description:		Creates initialisation data for the orders & tradeUp tools

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->

<cf_querySim>
	uneditableFields
	tableName,ColumnName
	fileType|secure
</cf_querySim>

<!--- any of the above variables can be over ridden in the INI file --->
<cf_include template="\code\cftemplates\RelayRecordManagerINI.cfm" checkIfExists="true">
<!--- <cfif fileexists("#application.paths.code#\cftemplates\RelayRecordManagerINI.cfm")>
	<cfinclude template="/code/cftemplates/RelayRecordManagerINI.cfm">
</cfif> --->
