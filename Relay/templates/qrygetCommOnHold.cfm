<!--- �Relayware. All Rights Reserved 2014 --->
<!--- this query IS VERY SIMILAR TO qrygetcommtosend.cfm
      except that it returns communications that are complete 
	  (defined by CommStatusID IS NOT NULL)
	  but are not queued.
---->	  

	<!--- CreateODBCDate(Now()) this is midnightof the day --->

	<CFPARAM NAME="usedate" DEFAULT="#Now()#">
	
	<!--- All communications that go out based on date--->
	<CFQUERY NAME="getCommOnHold" DATASOURCE="#application.siteDataSource#">
	SELECT c.CommID,
	  c.Title,
	  c.Description,
	  c.SendDate,
	  c.CommFormLID AS MainCommFormLID,
	  c.commtypelid,		<!--- added WAB 1999-02-16 --->
	  c.lastUpdated,
	  c.lastUpdatedBy,
	  c.CreatedBy,
	  cf.CommFormLID,
	  cf.CommFileID,
	  cf.Directory,
	  cf.FileName,
	  p.Email,
	  p.FirstName,
	  p.LastName,
	  c.replyto
	  FROM UserGroup AS ug, Person AS p, Communication AS c, CommFile AS cf
	 WHERE c.SendDate IS null
 	 AND c.CommStatusID > 0
	 AND cf.CommFileID = cf.IsParent
	 AND c.CommID = cf.CommID
	 AND c.TestFlag = 0 <!--- false --->
	 AND c.CommFormLID IN (2,3,5,6,7) <!--- exclude downloads (4) and 0 --->			
     AND c.sent = 0
	 AND p.PersonID = ug.PersonID
	 AND c.LastUpdatedBy = ug.UserGroupID
	ORDER BY c.CommID, cf.CommFormLID DESC
	</CFQUERY>
	

