<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		promotionAddForm.cfm	
Author:			NJH  
Date started:	20-05-2009
	
Description:	Stub file for promotionAddForm for cashback module		

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->

<cfset promotionGroupTextID=application.com.settings.getSetting("cashback.promotionGroupTextID")>
<cfinclude template="/admin/promotions/promotionAddForm.cfm">