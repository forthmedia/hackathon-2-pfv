<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		cashbackTopHead.cfm	
Author:			NJH  
Date started:	09-03-2009
	
Description:	Top Head for Cashback files		

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->


<cfparam name="current" type="string" default="index.cfm">
<cfparam name="attributes.templateType" type="string" default="list">
<cfparam name="attributes.pageTitle" type="string" default="">
<cfparam name="attributes.pagesBack" type="string" default="1">
<cfparam name="attributes.thisDir" type="string" default="">

<CF_RelayNavMenu pageTitle="#attributes.pageTitle#" thisDir="#attributes.thisDir#">
	<CFIF findNoCase("viewCashbackClaims.cfm",SCRIPT_NAME,1)>
		<CF_RelayNavMenuItem MenuItemText="Excel" CFTemplate="#cgi["SCRIPT_NAME"]#?#queryString#&openAsExcel=true" target="blank">
	</CFIF> 
</CF_RelayNavMenu>


