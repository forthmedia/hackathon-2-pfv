<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		incentiveClaimsProductList.cfm	
Author:			NJH  
Date started:	20-05-2009
	
Description:	Stub file for incentiveClaimsProductList.cfm for cashback module		

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->

<cfset defaultClaimsProductCatalogueCampaignID = application.com.settings.getSetting("cashback.claimsProductCatalogue")>
<cfinclude template="/products/incentiveClaimsProductList.cfm">