<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			validateCashbackClaims.cfm	- based on validateIncentiveClaim.cfm
Author:				NJH
Date started:		2009/03/05
	
Description:			

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2009/05/20			NJH			P-SNY069 - added extra column (campaign) and added it as a filter.
2009/11/06			NJH			P-SNY086 - added additionalReference to the child columns
2010/03/22			NAS			LID 3088 - added translatethesecolumns="translatedStatus" to tFQO
2012-10-04	WAB		Case 430963 Remove Excel Header 
2014-10-21			RPW			CORE-845 Not able to create Claim Cashback in portal side

Possible enhancements:

 --->

<cfparam name="viewAsStatement" type="boolean" default="false">

<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">

<cfparam name="form.approved" default="No">
<cfparam name="approved" default="#form.approved#">
<cfparam name="viewAsStatment" type="boolean" default="false">
<cfparam name="sortOrder" default="created">
<cfparam name="numRowsPerPage" default="50">
<cfparam name="startRow" default="1">

<cfparam name="keyColumnList" type="string" default="person_Name,_"><!--- this can contain a list of columns that can be edited --->
<cfparam name="keyColumnURLList" type="string" default="../data/dataFrame.cfm?frmSrchPersonID=, "> <!--- this can contain a list of matching columns that contain the URL of the editor template   incentivePointsClaimByProductFamily.cfm?readOnly=yes&rwtransactionid= --->
<cfparam name="keyColumnKeyList" type="string" default="personid, "><!--- this can contain a list of matching key columns e.g. primary key --->
<cfparam name="keyColumnOpenInWindowList" type="string" default="yes,no">
<cfparam name="dateFormat" type="string" default="Created">
<cfparam name="numberFormat" type="string" default="credit_amount">

<cfparam name="keyColumnOnClickList" type="string" default=" ,showCashbackClaimItems(this*comma##CashbackClaimID##);return false">
<!--- NJH 2009/05/19 P-SNY069 - added Campaign --->
<cfparam name="claimsReport_showTheseMasterColumns" type="string" default="Campaign,Created,Credit_Amount,Organisation_Name,PersonID,Person_Name,CashbackClaimID,translatedStatus,_">
<!--- NJH 2009/05/12 CR-SNY675-1 - changed default to be the list of columns, rather than an empty string --->
<cfparam name="claimsReport_showTheseChildColumns" type="string" default="ProductGroup,Product,SerialNumber,InvoiceNumber,EndUserCompanyName,UnitValue,Quantity,Value,AdditionalReference"> <!--- NJH 2009/11/06 - P-SNY086 - added additionalReference --->
<cfparam name="showInvoiceLink" type="boolean" default="true">

<cfparam name="openAsExcel" type="boolean" default="false">

<cf_title>phr_Cashback_Claims</cf_title>

<cfif not viewAsStatment and request.relayCurrentUser.isInternal>
	<cfif openAsExcel>
		<!--- if we're opening in excel, we don't want to show the image that shows the child records. --->
		<cfset claimsReport_showTheseMasterColumns = replace(claimsReport_showTheseMasterColumns,",_","")>
		<!--- NJH 2009/05/12 CR-SNY675-1 when exporting to excel, add child columns --->
		<cfset claimsReport_showTheseMasterColumns = listAppend(claimsReport_showTheseMasterColumns,claimsReport_showTheseChildColumns)>
	</cfif>

	<cfscript>
		if(structKeyExists(URL, "frmRowIdentity")){
			for(x = 1; x lte listLen(URL.frmRowIdentity); x = x + 1){
				if (URL.action eq "approveClaim") {
					v = application.com.relayCashback.approveCashbackClaim(CashbackClaimID=listGetAt(URL.frmRowIdentity,x));
				}
				else if (URL.action eq "rejectClaim") {
					c = application.com.relayCashback.rejectCashbackClaim(CashbackClaimID=listGetAt(URL.frmRowIdentity,x));
				}
			}
			// NJH 2008/12/09 CR-SNY670 clear the variables so that we don't re-verify claim when filters are being used.
			URL.frmRowIdentity = "";
			URL.action = "";
		}
	</cfscript>
</cfif>

<!--- 2009/04/14 GCC added person filter for portal view --->
<!--- NJH 2009/05/12 CR-SNY675-1 added a distinct as the view is now bringing back items as well and only bringing back specific columns rather than * --->
<cfquery name="vCashbackClaimList" datasource="#application.SiteDataSource#">
	select distinct #replace(claimsReport_showTheseMasterColumns,",_","")#
	<cfif not openAsExcel>
		,'<img src="/images/icons/bullet_arrow_down.png">' as _
	</cfif>
	from vCashbackClaimList
	where status <> 'Pending'
	<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
	<cfif approved>
		and status = 'Approved'
	</cfif>
	<cfif not request.relayCurrentUser.isInternal>
		and personID = #request.relayCurrentUser.personID#
	</cfif>
	order by <cf_queryObjectName value="#sortOrder#">	
</cfquery>


<cfif not openAsExcel>
	
	<cf_includeJavascriptOnce template = "/javascript/tableManipulation.js">
	
	<cfset columnToShowAjaxAt = listFindNoCase(vCashbackClaimList.columnList,"CashbackClaimID")>
	
	<script>
		<cfoutput>var columnToShowAjaxAt = #columnToShowAjaxAt#</cfoutput>
		function showCashbackClaimItems (obj,id) {
	
			currentRowObj = getParentOfParticularType (obj,'TR')
			currentTableObj = getParentOfParticularType (obj,'TABLE')
	
			totalColspan = 0
			for (i=0;i<currentRowObj.cells.length;i++) {
				totalColspan  += currentRowObj.cells[i].colSpan
			}
	
			infoRow =  id + '_info'
			infoRowObj = $(infoRow)
			
			
			if (!infoRowObj ) {
				page = '/webservices/relayCashbackWS.cfc?wsdl&method=getCashbackClaimItemsDisplay'
				page = page + '&CashbackClaimID='+id;
				<cfif claimsReport_showTheseChildColumns neq "">
				<cfoutput>page = page + '&showCols=#claimsReport_showTheseChildColumns#'</cfoutput>
				</cfif>
				<cfif showInvoiceLink>
					page = page + '&showInvoiceLink=true';
				</cfif>
	
				// changed to post to ensure proper refresh
				var myAjax = new Ajax.Request(
					page, 
					{
						method: 'post', 
						parameters: 'returnFormat=json', 
						evalJSON: 'force',
						debug : false,
						onComplete: function (requestObject,JSON) {
							json = requestObject.responseJSON
							rowStructure = new Array (2)
							rowStructure.row = new Array (1) 
							rowStructure.cells = new Array (1)
							rowStructure.cells[0] = new Array (1) 
							rowStructure.cells[0].content  = json.CONTENT 
							rowStructure.cells[0].colspan  = totalColspan
							rowStructure.cells[0].align  = 'right'
	
							regExp = new RegExp ('_info')
							deleteRowsByRegularExpression (currentTableObj,regExp)
							
							addRowToTable (currentTableObj,rowStructure,obj.parentNode.parentNode.rowIndex + 1,infoRow)
						
							}
					});
			} 
			else
			{
				regExp = new RegExp (infoRow)
				deleteRowsByRegularExpression (currentTableObj,regExp)
			}
			
			return 
		}
		
		function getParentOfParticularType (obj,tagName) {	
			if (obj.parentNode.tagName == tagName) 	{
				return obj.parentNode
			} else {
				return getParentOfParticularType (obj.parentNode,tagName)
			}
		}
	</script>
</cfif>


<cfif not viewAsStatement and request.relayCurrentUser.isInternal>

	<cfinclude template="claimApprovalFunctions.cfm">

	<CF_tableFromQueryObject
		queryObject="#vCashbackClaimList#"
		queryName="vCashbackClaimList"
		sortOrder = "#sortOrder#"
		numRowsPerPage="#numRowsPerPage#"
		startRow="#startRow#"
		openAsExcel="#openAsExcel#"	
		useInclude="true"
		
		keyColumnList="#keyColumnList#"
		keyColumnURLList="#keyColumnURLList#"
		keyColumnKeyList="#keyColumnKeyList#"
		keyColumnOnClickList="#keyColumnOnClickList#"
		keyColumnOpenInWindowList="yes,no"
		FilterSelectFieldList="Campaign,Organisation_Name,Person_Name" <!--- 2009/05/19 P-SNY069 - added campaign and org name --->
		FilterSelectFieldList2="Campaign,Organisation_Name,Person_Name" <!--- 2009/05/19 P-SNY069 - added filter 2 --->
		
		radioFilterLabel="Approved Only"
		radioFilterDefault="#form.approved#"
		radioFilterName="Approved"
		radioFilterValues="No-Yes"
		
		checkBoxFilterName=""
		checkBoxFilterLabel=""
		allowColumnSorting="yes"
		dateFormat="#dateFormat#"
		numberFormat="#numberFormat#"
		
		rowIdentityColumnName="CashbackClaimID"
		functionListQuery="#comTableFunction.qFunctionList#"
		columnTranslation="true"
		ColumnTranslationPrefix="phr_cashback_report_"
		<!--- 2010/03/22			NAS			LID 3088 - added translatethesecolumns="translatedStatus" to tFQO --->
		translatethesecolumns="translatedStatus"
		showTheseColumns="#claimsReport_showTheseMasterColumns#"
	>
	
<cfelse>

	<!--- NJH 2009/05/12 CR-SNY675-1 - added hideTheseColumns --->
	<!--- 2014-10-21			RPW			CORE-845 Not able to create Claim Cashback in portal side --->
	<cfif NOT IsDefined("form.frmTask")>
		<cfif vCashbackClaimList.recordCount>
			<CF_tableFromQueryObject
				queryObject="#vCashbackClaimList#"
				queryName="vCashbackClaimList"
				sortOrder = "#sortOrder#"
				HidePageControls="no" <!--- NJH 2009/05/22 P-SNY069 changed from yes --->
				useInclude="true"
				
				keyColumnList="#keyColumnList#"
				keyColumnURLList="#keyColumnURLList#"
				keyColumnKeyList="#keyColumnKeyList#"
				keyColumnOnClickList="#keyColumnOnClickList#"
				keyColumnOpenInWindowList="#keyColumnOpenInWindowList#"
				
				allowColumnSorting="yes"
				dateFormat="#dateFormat#"
				numberFormat="#numberFormat#"
				
				rowIdentityColumnName="CashbackClaimID"
				columnTranslation="true"
				ColumnTranslationPrefix="phr_cashback_report_"
				showTheseColumns="#claimsReport_showTheseMasterColumns#"
				hideTheseColumns="cashbackClaimID"
			>
		<cfelse>
			phr_cashback_NoCashbackClaimsHaveBeenMade.
		</cfif>
	</cfif>
</cfif>

<div id="dummy"></div>