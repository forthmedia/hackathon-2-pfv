	<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			incentiveCashbackClaimByProductFamily.cfm - based on incentivePointsClaimByProductFamily.cfm
Author:				NJH
Date started:		2009/03/03

Description:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2009/05/18			NJH			P-SNY069 - added phrase prefix. Also hide product Category and product Group dropdowns if they only contain one value.
2009/11/06			NJH			P-SNY086 - added additionalReference field
2012-03-26 			WAB 		Altered WebService functions to use returnFormat=json
2014-10-17			RPW			CORE-845 Not able to create Claim Cashback in portal side
2014-10-20			RPW			CORE-845 Not able to create Claim Cashback in portal side - Removed table
2014-10-21			RPW			CORE-845 Not able to create Claim Cashback in portal side
2015-02-18 			ACPK 		FIFTEEN-203 Fixed datepicker not allowing dates to be selectable

Possible enhancements:
TO DO: use structure to hold whether fields are required or not.

 --->

<!--- this include provides the logic for the Relay Form Custom Tags --->
<cfoutput>
	<cfinclude template="/templates/relayFormJavaScripts.cfm">
</cfoutput>


<cfparam name="form.CashbackClaimID" default="0">
<cfparam name="form.quantity" default="1">
<cfparam name="form.serialNo" default="">
<cfparam name="form.EndUserCompanyName" default="">
<cfparam name="form.productCategoryID" default="0">
<cfparam name="form.productFamilyID" default="0">
<cfparam name="form.DistiID" default="0">
<!--- NJH 2010/10/22 this has now been set in settings
<cfparam name="defaultClaimsProductCatalogueCampaignID" type="numeric"> ---> <!--- this should be set in the cashbackParams.cfm file --->
<cfparam name="form.frmTask" default="">
<cfparam name="phrasePrefix" type="string" default=""> <!--- NJH 2009/05/18 P-SNY069 - added phrasePrefix --->

<cfparam name="validClaimPendingStatuses" default="Pending">

<cfparam name="hideSerialNo" type="boolean" default="#application.com.settings.getSetting('cashback.hideSerialNo')#">
<cfparam name="hideInvoiceNo" type="boolean" default="false">
<cfparam name="hideInvoiceDate" type="boolean" default="false">
<cfparam name="hideEndUserCompanyName" type="boolean" default="false">
<cfparam name="hideDistiID" type="boolean" default="false">
<cfparam name="hideUploadClaimInvoice" type="boolean" default="false">
<cfparam name="hideAdditionalReference" type="boolean" default="true"> <!--- NJH 2009/11/06 P-SNY086 --->

<cfparam name="serialNoRequired" type="boolean" default="false">
<cfparam name="invoiceNoRequired" type="boolean" default="false">
<cfparam name="invoiceDateRequired" type="boolean" default="false">
<cfparam name="endUserCompanyNameRequired" type="boolean" default="false">
<cfparam name="distiIDRequired" type="boolean" default="false">
<cfparam name="claimInvoiceRequired" type="boolean" default="true">
<cfparam name="additionalReferenceRequired" type="boolean" default="false">

<cfparam name="claimFrom" type="date" default="2005-06-01">
<cfparam name="claimTo" type="date" default="#dateFormat(now(),'yyyy-mm-dd')#">
<cfparam name="claimWindowDays" type="numeric" default="28">
<cfparam name="constrainProductByFlag" type="boolean" default="true">
<cfparam name="serialNoLength" type="numeric" default="50">
<cfparam name="singleProductPerClaimItem" type="boolean" default="false"> <!--- force only one product per claim item --->
<cfparam name="singleClaimItemPerClaim" type="boolean" default="false">  <!--- force only one claim item per claim --->

<cfset defaultClaimsProductCatalogueCampaignID = application.com.settings.getSetting("cashback.claimsProductCatalogue")>

<!--- if we're uploading invoices, the invoice number needs to become mandatory. --->
<cfif not hideUploadClaimInvoice>
	<cfset hideInvoiceNo = "no">
	<cfset invoiceNoRequired = "yes">
</cfif>


<!--- NJH 2009/05/18 P-SNY069 - added phrasePrefix --->
<cf_translate phrases="phr_cashback_#phrasePrefix#NotAuthorisedToClaimAnyProducts,phr_cashback_claim_#phrasePrefix#JS_uploadInvoice"/>


<cf_translate>

<cfif not form.cashbackClaimID and form.frmTask eq "">
	<!--- 2014-10-20	RPW	CORE-845 Not able to create Claim Cashback in portal side - Removed table --->
	<cfoutput>phr_cashback_claim_#htmleditformat(phrasePrefix)#Introduction</cfoutput> <!--- NJH 2009/05/18 P-SNY069 - added phrasePrefix --->
	<form name="oppProductForm" method="post" enctype="multipart/form-data">

		<input type="hidden" id="frmTask" name="frmTask" value="createCashbackClaim">
		<cfoutput><button type="submit" class="btn btn-primary ui-link">phr_cashback_#phrasePrefix#CreateNewClaim</button></cfoutput>
	</form>
<cfelse>

	<cfscript>
		claimAlreadySubmitted = false;

		if(not len(form.productCategoryID)){
			form.productCategoryID = 0;
		}

		if(structKeyExists(url, "CashbackClaimID")){
			form.CashbackClaimID = url.CashbackClaimID;
		}

		CashbackClaimQry = application.com.relayCashback.getCashbackClaimDetails(form.CashbackClaimID);
		// Get the claim status.
		claimStatus = CashbackClaimQry.status;

		if (listFindNoCase(claimStatus,validClaimPendingStatuses) OR NOT LEN(form.frmTask) or form.cashbackClaimID eq 0) {

			if (listFindNoCase("submitClaim,submitAndStartNewClaim",frmTask)) {
				// Standard
				totalCashbackClaimed = application.com.relayCashback.submitCashbackClaim(CashbackClaimID = form.CashbackClaimID,distiID = form.distiID);
			}

			if (frmTask neq "submitClaim") {
				// GCC - 2005/09/29 - A_675 Show non-product promotions on the statement detail view

				if (structKeyExists(url, "readOnly")) {
					statementView = "true";
				} else {
					statementView = "false";
				}
				//get product group and products query
				qProductAndGroupList = application.com.relayIncentive.getProductAndGroupList(productCatalogueCampaignID=defaultClaimsProductCatalogueCampaignID, countryID=request.relayCurrentUser.countryID, productCategoryID=form.productCategoryID, constrainByFlag=variables.constrainProductByFlag, statementView=statementView);

				//if the two select query is limited by the category,
				//get the full one for the category query
				if(form.productCategoryID){
					qProductAndGroupListAll = application.com.relayIncentive.getProductAndGroupList(defaultClaimsProductCatalogueCampaignID, request.relayCurrentUser.countryID, form.productCategoryID, variables.constrainProductByFlag);
					//list of product groups from the group/product query
					rawGroupList = valueList(qProductAndGroupListAll.productGroupID);
				} else {
					//list of product groups from the group/product query
					rawGroupList = valueList(qProductAndGroupList.productGroupID);
				}

				distinctGroupList = "";
				//loop and dedupe
				for(i=1; i lte listLen(rawGroupList); i = i+1){
					if(not listFind(distinctGroupList, listGetAt(rawGroupList, i))){
						distinctGroupList = listAppend(distinctGroupList, listGetAt(rawGroupList, i));
					}
				}

				//If product categories present, get product categories (if any)
				if(listLen(variables.distinctGroupList) eq 0) {
					// NJH 2009/05/18 P-SNY069 - added phrasePrefix
					writeoutput (JSStringFormat(evaluate("phr_cashback_"&phrasePrefix&"NotAuthorisedToClaimAnyProducts")));
					application.com.email.sendEmail("NotAuthorisedToClaimAnyProducts",request.relayCurrentUser.personID);
					application.com.globalFunctions.abortIT();
				} else {
					productCategoriesQry = application.com.relayIncentive.getProductGroupCategory(variables.distinctGroupList, form.productFamilyID);
				}

				//If product categories present, get the query for three selects
				if(productCategoriesQry.recordCount){
					qProductAndGroupList = application.com.relayIncentive.getProductAndGroupAndCatList(defaultClaimsProductCatalogueCampaignID, request.relayCurrentUser.countryID,0, 0,'no',variables.constrainProductByFlag);
				} else {
					qProductAndGroupList = application.com.relayIncentive.getProductAndGroupList(defaultClaimsProductCatalogueCampaignID, request.relayCurrentUser.countryID, 0, variables.constrainProductByFlag);
				}

				//start cashback claim
				// NJH 2009/05/19 P-SNY069 - moved down here so that if you are not authorised to claim products, then no claim is made
				if (listFindNoCase("createCashbackClaim,submitAndStartNewClaim",frmTask)) {
					form.CashbackClaimID = application.com.relayCashback.createCashbackClaim();
				}
				//when transaction active, get all line items
				if(form.CashbackClaimID){
					//cashbackClaimItems = application.com.relayCashback.getCashbackClaimItems(CashbackClaimID = form.CashbackClaimID, statementView = statementView);
					qGetOrgDistis = application.com.commonQueries.getOrgDistis(application.siteDataSource,request.relayCurrentUser.organisationID);
				}

				/*whenever trasaction is active and some line items already in the basket,
					if productCategories present, set productCategoryID to the last product's category one
					so that if people are claiming from the same category, the select is always selected to the right one*/

				/* NJH not sure what we need here and if this is still being used with the ajax call.. commenting out for now
				if(form.CashbackClaimID and productCategoriesQry.recordCount and not form.productCategoryID and cashbackClaimItems.recordCount){
					//get the last item's in the trasanction category
					productCategoryLast = listLast(valueList(cashbackClaimItems.productGroupID));
					if(len(productCategoryLast)){
						form.productCategoryID = application.com.relayIncentive.getProductGroupCategory(productCategoryLast).productCategoryID;
					}
					else{
						form.productCategoryID = productCategoriesQry.productCategoryID[1];
					}
				}*/

				//if productFamilyID passed, do not limit it backwards
				if(form.productFamilyID){
					productCategoriesQryAll = application.com.relayIncentive.getProductGroupCategory(variables.distinctGroupList, 0);
					productCategoryList = valueList(productCategoriesQryAll.productCategoryID);
				}

				//if product categories present, see is product families also there
				/* NJH 2009/05/19 P-SNY069 - don't seem to be using productFamiliesQry so am commenting it out for now
				if(productCategoriesQry.recordCount){
					if(not structKeyExists(variables, "productCategoryList")){
						productCategoryList = valueList(productCategoriesQry.productCategoryID);
					}
					//get product families (if any)
					productFamiliesQry = application.com.relayIncentive.getProductFamily(productCategoryList);
				}*/
			}
		} else {
			claimAlreadySubmitted = true;
		}
	</cfscript>

	<cfif claimAlreadySubmitted>
		<cfoutput>phr_cashback_claim_#htmleditformat(phrasePrefix)#claimAlreadySubmitted</cfoutput> <!--- NJH 2009/05/18 P-SNY069 - added phrasePrefix --->

	<!--- if we've submitted a claim, output the message that the claim is being processed. If we're not starting a new one, then stop or
		continue to the postIncludeTemplate if it's defined. --->
	<cfelseif listFindNoCase("submitClaim,submitAndStartNewClaim",form.frmTask)>
		<cfoutput>phr_cashback_claim_#htmleditformat(phrasePrefix)#claimBeingProcessed</cfoutput>  <!--- NJH 2009/05/18 P-SNY069 - added phrasePrefix --->
		<cfif form.frmTask eq "submitClaim">
			<cfif isdefined('postIncludeTemplate')>
				<cfinclude template="#postIncludeTemplate#">
			<cfelse>
				<!--- 2014-10-21			RPW			CORE-845 Not able to create Claim Cashback in portal side --->
				<cflocation url="?eid=#URL.eid#" addtoken="false">
			</cfif>
		</cfif>
	</cfif>

	<!--- if we're not submitting the claim, then show the add to claim portion of the page --->
	<cfif form.frmTask neq "submitClaim">

		<cfsavecontent variable = "thejavascript_js">
			<cf_translate processEvenIfNested=true>
				<script >

				var listPriceArr = new Array();
				var productIDArr = new Array();
				var fieldsRequiredArr = new Array();


				<cfoutput query="qProductAndGroupList" group="productGroupID">
					<cfif len(#IncentiveClaimRequiredFields#)>
						fieldsRequiredArr[#jsStringFormat(productGroupID)#] = #jsStringFormat(IncentiveClaimRequiredFields)#;
					<cfelse>
						fieldsRequiredArr[#jsStringFormat(productGroupID)#] = '101111';
					</cfif>
					<cfoutput>productIDArr[#jsStringFormat(qProductAndGroupList.currentRow)#] = #jsStringFormat(productID)#;	listPriceArr[#jsStringFormat(qProductAndGroupList.currentRow)#] = #jsStringFormat(int(Len(discountprice) ? discountprice : 0))#;
					</cfoutput>
				</cfoutput>


				function addProduct() {
					var form = document.forms.oppProductForm;
					var errorMsg = '';

					if (form.thisProductID.value) {
						if (hasProductBeenClaimed(form.thisProductID.value,form.serialNo.value)) {
							<cfoutput>errorMsg = errorMsg + "\n phr_cashback_claim_#jsStringFormat(phrasePrefix)#JS_alreadyClaimedThisProduct";</cfoutput> <!--- NJH 2009/05/19 P-SNY069 - added phrase prefix --->
						}
					}

					if (errorMsg == '') {
						// force the form validation
						if (form.onsubmit()) {
							// NJH 2009/11/06 P-SNY086 - add additionalReference to list of fields
							addClaimItem(form.CashbackClaimID.value,form.thisProductID.value,form.quantity.value,form.totalCashbackClaimed.value,form.distiID.value,form.serialNo.value,form.invoiceNo.value,form.invoiceDate.value,form.endUserCompanyName.value,form.additionalReference.value);

							// reset the form variables
							if (form.productCategoryID) {form.productCategoryID.value="";}
							form.productGroupID.value="";
							form.thisProductID.value="";
							form.quantity.value = 1;
							form.invoiceNo.value="";
							form.serialNo.value="";
							form.distiID.value = 0;
							form.invoiceDate.value = "";
							form.endUserCompanyName.value="";
							form.additionalReference.value=""; // NJH 2009/11/06 P-SNY086
							var quantityCell = getRawObject('qntCell');
							//2014-10-17			RPW			CORE-845 Not able to create Claim Cashback in portal side
							quantityCell.innerHTML = '<input name="quantity1" id="quantity1" type="text" size="2" maxlength="3" value="1" onChange="javascript:setLineTotal(\'totalValue\');" <cfif singleProductPerClaimItem>readOnly</cfif> class="form-control">';
							resetTotal();
						}
					} else {
						alert(errorMsg);
					}
				}


				function submitClaim(action) {
					var errorMsg = '';
					var confirmCheck = getRawObject('confirmVeracity');

					if (action == 'submitAndStartNewClaim') {
						document.submitClaimForm.frmTask.value = action;
					} else {
						document.submitClaimForm.frmTask.value = 'submitClaim';
					}

					if (!confirmCheck.checked){
						<cfoutput>errorMsg = 'phr_cashback_claim_#jsStringFormat(phrasePrefix)#JS_confirmClaim';</cfoutput> <!--- NJH 2009/05/19 P-SNY069 --->
					}

					<cfif claimInvoiceRequired and not hideUploadClaimInvoice>
						if (!hasInvoiceBeenUploaded()) {
							<cfoutput>errorMsg = errorMsg + '\n phr_cashback_claim_#jsStringFormat(phrasePrefix)#JS_uploadInvoice';</cfoutput> <!--- NJH 2009/05/19 P-SNY069 --->
						}
					</cfif>

					if (errorMsg == '') {
						document.submitClaimForm.submit();
					} else {
						alert(errorMsg);
					}
				}


				function hasInvoiceBeenUploaded() {
					page = '/webservices/relayIncentiveWS.cfc?wsdl&method=getClaimInvoice&entityType=CashbackClaim&returnFormat=json'
					<cfoutput>page = page + '&entityID=#jsStringFormat(form.CashbackClaimID)#'</cfoutput>

					var myAjax = new Ajax.Request(
							page,
							{
								method: 'get',
								parameters: '',
								evalJSON: 'force',
								debug : false,
								asynchronous: false
							}
						);

					response = myAjax.transport.responseText.evalJSON();

					<!--- if we have a record returned, then an invoice has been uploaded --->
					if (response.length > 0) {
						return true;
					} else {
						return false;
					}
				}

				function hasProductBeenClaimed(productID,serialNo) {
					page = '/webservices/relayCashbackWS.cfc?wsdl&method=getCashbackClaimItems'
					<cfoutput>page = page + '&cashbackClaimID=#jsStringFormat(form.CashbackClaimID)#'</cfoutput>

					if (typeof(productID) != 'undefined'){
						page = page + '&productID=' + productID;
					}
					if (typeof(serialNo) != 'undefined') {
						page = page + '&serialNo=' + serialNo;
					}

					var myAjax = new Ajax.Request(
							page,
							{
								method: 'post',
								parameters: 'returnFormat=json&_cf_nodebug=true',
								evalJSON: 'force',
								debug : false,
								asynchronous: false
							}
						);

					response = myAjax.transport.responseText.evalJSON();

					<!--- if we have a record returned, then a product has already been claimed for this claim --->
					if (response.length > 0) {
						return true;
					} else {
						return false;
					}
				}

				<!--- NJH 2009/11/06 P-SNY086 - added additionalReference field --->
				function addClaimItem(cashbackClaimID,productID,quantity,value,distiID,serialNo,invoiceNo,invoiceDate,endUserCompanyName,additionalReference) {

					page = '/webservices/relayCashbackWS.cfc?wsdl&method=addCashbackClaimItem'
					page = page + '&CashbackClaimID='+cashbackClaimID
					page = page + '&productID='+productID
					page = page + '&quantity='+quantity
					page = page + '&value='+value
					page = page + '&distiID='+distiID
					page = page + '&serialNo='+serialNo
					page = page + '&invoiceNo='+invoiceNo
					page = page + '&invoiceDate='+invoiceDate
					page = page + '&endUserCompanyName='+endUserCompanyName
					page = page + '&additionalReference='+additionalReference

					var myAjax = new Ajax.Request(
						page,
						{
								method: 'post',
								parameters: '',
								evalJSON: 'force',
								debug : false,
								onComplete: function (requestObject,JSON) {
									json = requestObject.responseJSON
									//alert(json.CONTENT)
									updateClaimItemDiv(cashbackClaimID);
								}
						}
					)
				}

				function updateClaimItemDiv(cashbackClaimID) {
					var timeNow = new Date();
					page = '/webservices/relayCashbackWS.cfc?wsdl&method=getCashbackClaimItemsDisplayForClaimPage&time='+timeNow
					page = page + '&action=update&CashbackClaimID='+cashbackClaimID
					<!--- NJH 2009/05/19 P-SNY069 --->
					<cfif phrasePrefix neq "">
						<cfoutput>page = page + '&phrasePrefix=#jsStringFormat(phrasePrefix)#'</cfoutput>
					</cfif>

					div = 'cashbackClaimItemsDiv'
					var myAjax = new Ajax.Updater(
						div,
						page,
						{
							method: 'post',
							parameters: '' ,
							evalJSON: 'force',
							debug : false
						});

					<!--- NJH if we're allowing only a single item per claim, then disable the add product button once we have added one --->
					<cfif singleClaimItemPerClaim>
						var addProductButton = getRawObject('addProductBtn');

						if (hasProductBeenClaimed()) {
							addProductButton.disabled = true;
						} else {
							addProductButton.disabled = false;
						}
					</cfif>
				}

				function removeClaimItem(cashbackClaimID,cashbackClaimItemID) {

					page = '/webservices/relayCashbackWS.cfc?wsdl&method=removeCashbackClaimItem'
					page = page + '&CashbackClaimID='+cashbackClaimID
					page = page + '&cashbackClaimItemID='+cashbackClaimItemID

					var myAjax = new Ajax.Request(
						page,
						{
								method: 'post',
								parameters: '',
								evalJSON: 'force',
								debug : false,
								onComplete: function (requestObject,JSON) {
									json = requestObject.responseJSON
									//alert(json.CONTENT)
									updateClaimItemDiv(cashbackClaimID);
								}
						}
					)
				}

				function getRawObject(obj) {

					var theObj;

					if (typeof obj == "string") {
						theObj = document.getElementById(obj);
					} else {
						// pass through object reference
						theObj = obj;
					}
					return theObj;
				}

				function setSerialNo(){
					ser1 = document.getElementById('serialNo1');
					document.oppProductForm.serialNo.value = ser1.value;
				}

				function checkIfSerialRequired(pgid){

					if (pgid =='') {return}   <!--- Added WAB 2005-12-14 to prevent js errors further down.  Occurs if user selects the 'select a product group' value from drop down - Doesn't really make sense to do this so OK to just return --->

					var serialNumberObject = getRawObject('sno');
					var serialNumberLabel = getRawObject('snoLabel');
					var quantityCell = getRawObject('qntCell');
					var invoiceNo = getRawObject('invoiceNo');
					var endUserCompanyName = getRawObject('endUserCompanyName');
					var distiID = getRawObject('distiID');


					currentGroupSwitch = fieldsRequiredArr[pgid].toString();


					// 1. serial number
					if(currentGroupSwitch.charAt(1) == 1){
						document.oppProductForm.serialRequired.value = 1;
						serialNumberObject.innerHTML = '<input name="serialNo1" id="serialNo1" maxlength="50" type="text" onChange="javascript:setSerialNo();">';
						serialNumberLabel.innerHTML = 'Serial Number';
						quantityCell.innerHTML = '1';
					}
					else{
						document.oppProductForm.serialRequired.value = 0;
						if (serialNumberObject) {serialNumberObject.innerHTML = '';}
						if (serialNumberLabel) {serialNumberLabel.innerHTML = '';}
						//2014-10-17			RPW			CORE-845 Not able to create Claim Cashback in portal side
						quantityCell.innerHTML = '<input name="quantity1" id="quantity1" type="text" size="2" maxlength="3" value="1" onChange="javascript:setLineTotal(\'totalValue\');" <cfif singleProductPerClaimItem>readOnly</cfif> class="form-control">';

					}

					// 2. invoice number
					if(currentGroupSwitch.charAt(2) == 1){
						invoiceNo.disabled = false;
					}
					else{
						invoiceNo.disabled = true;
					}

					// 3. end user company name
					if(currentGroupSwitch.charAt(4) == 1){
						endUserCompanyName.disabled = false;
					}
					else{
						endUserCompanyName.disabled = true;
					}

					//4. distiID
					if(currentGroupSwitch.charAt(5) == 1){
						distiID.disabled = false;
					}
					else{
						distiID.disabled = true;
					}
				}


				function setInnerElement(element,productID){

					myObj = getRawObject(element);

					for (var i = 1; i <= productIDArr.length; i++) {
						if(productIDArr[i] == productID){
							myObj.innerHTML = listPriceArr[i];
							itemPrice = listPriceArr[i];
							document.oppProductForm.currentPointsValue.value = itemPrice;
						}
					}
					if(document.oppProductForm.quantity){
						totalPrice = itemPrice*document.oppProductForm.quantity.value;
					}
					else{
						totalPrice = itemPrice;
					}
					document.oppProductForm.totalCashbackClaimed.value = totalPrice;

					totalObj = document.getElementById('totalValue');
					totalObj.innerHTML = totalPrice;
				}

				function setLineTotal(element){

					qty1 = document.getElementById('quantity1');
					totalPrice = document.oppProductForm.currentPointsValue.value * qty1.value;
					document.oppProductForm.quantity.value = qty1.value;
					myObj = getRawObject(element);
					myObj.innerHTML = totalPrice;
					document.oppProductForm.totalCashbackClaimed.value = totalPrice;
				}

				function resetTotal(){

					singlePriceObj = getRawObject('discountPrice');
					totalObj =  getRawObject('totalValue');
					serialNumberObj =  getRawObject('sno');

					singlePriceObj.innerHTML = '';
					totalObj.innerHTML = '';
					serialNumberObj.innerHTML = '';
				}


				function eventDeleteOnClick(cashbackClaimID,cashbackClaimItemID){
					<!--- NJH 2009/05/19 P-SNY069 - added phrasePrefix --->
					<cfoutput>
					with(document.forms["oppProductForm"]){
				  		if(confirm("phr_cashback_claim_#jsStringFormat(phrasePrefix)#JS_ConfirmDeleteProduct")){
							elements["frmProductDeletionList"].value = cashbackClaimItemID;
							removeClaimItem(cashbackClaimID,cashbackClaimItemID);
						}
				  	}
				  	</cfoutput>
				}

				</script>
			</cf_translate>
		</cfsavecontent>
		<cfhtmlhead text = #thejavascript_js#>


		<cfset request.relayFormDisplayStyle = "HTML-div">

		<cfif not structKeyExists(url, "readOnly")>

			<!--- NJH 2009/05/19 P-SNY069 - added phrasePrefix's to labels in form fields --->
			<cfform name="oppProductForm" method="post" enctype="multipart/form-data">
				<cfajaximport>
				<cf_relayFormDisplay class="">

				<cfoutput>
					<input id="frmTask" type="hidden" name="frmTask" value="">
					<input id="currentPointsValue" type="hidden" name="currentPointsValue" value="0">
					<input id="totalCashbackClaimed" type="hidden" name="totalCashbackClaimed" value="0">
					<!--- added to guarantee existence - were getting destroyed in NS and FF form domains by innerHTML - not being picked up as form objects after then --->
					<input type="hidden" name="quantity" id="quantity" size="2" maxlength="3" value="1">
					<CF_INPUT id="CashbackClaimID" type="hidden" name="CashbackClaimID" value="#CashbackClaimID#">
					<CF_INPUT id="productFamilyID" type="hidden" name="productFamilyID" value="#form.productFamilyID#">
						<input id="serialRequired" name="serialRequired" type="hidden" value="0">
						<input id="frmProductDeletionList" name="frmProductDeletionList" type="hidden" value="0">

						<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="" label="" currentValue="phr_cashback_claim_#phrasePrefix#Instructions" spanCols="true">
						<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="" label="phr_cashback_claim_#phrasePrefix#PersonName" currentValue="#request.relayCurrentUser.person.firstname# #request.relayCurrentUser.person.lastname#">
						<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="" label="phr_cashback_claim_#phrasePrefix#companyName" currentValue="#request.relayCurrentUser.organisation.organisationName#">
						<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="" label="phr_cashback_claim_#phrasePrefix#address" currentValue="">
						<cfif len(request.relayCurrentUser.location.sitename)>#htmleditformat(request.relayCurrentUser.location.sitename)#<br></cfif>
						<cfif len(request.relayCurrentUser.location.address1)>#htmleditformat(request.relayCurrentUser.location.address1)#<br></cfif>
						<cfif len(request.relayCurrentUser.location.address4)>#htmleditformat(request.relayCurrentUser.location.address4)#<br></cfif>
						<cfif len(request.relayCurrentUser.location.postalCode)>#htmleditformat(request.relayCurrentUser.location.postalCode)#<br></cfif>
						#application.countryName[request.relayCurrentUser.countryID]#
					</cf_relayFormElementDisplay>

					<!--- NJH - a bit dirty, but Sony needs to show the SAP number. I have set all these variables in the cashbackINI.cfm, so normally won't be set. --->
					<cfif isDefined("showSAPNo") and showSAPNo and isDefined("SAPNo") and len(SAPNo)>
							<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="" label="phr_cashback_claim_#phrasePrefix#SAPNo" currentValue="#SAPNo#">
					</cfif>

				</cfoutput>

				<TR class="label" id="test1">
					<td valign="top">
						<cfset style="width:150px">

							<!--- show product categories --->
							<!--- if we have more than one product category, then show as select, else as a hidden field --->
							<cfif productCategoriesQry.recordCount gt 1>
								<cf_relayFormElement relayFormElementType="select" label="phr_cashback_#phrasePrefix#productCategoryID" currentValue="" fieldname="productCategoryID" query="#productCategoriesQry#" value="productCategoryID" display="productCategoryName" nullText="phr_cashback_Claim_#phrasePrefix#SelectProductCategory" nullValue="" onChange="javascript:resetTotal()" style="#style#" required="true"><br>
							<cfelse>
								<cfset currentProductCategoryID = "">
								<cfif productCategoriesQry.recordCount eq 1>
									<cfset currentProductCategoryID = productCategoriesQry.productCategoryID[1]>
								</cfif>

								<cfoutput>
									<CF_INPUT type="hidden" id="productCategoryID" value="#currentProductCategoryID#">
								</cfoutput>
							</cfif>

							<!--- show product groups --->
							<cfquery name="getDistinctProductGroups" dbType="query">
								select distinct productGroupID, productGroupDescription from qProductAndGroupList
								order by productGroupDescription
							</cfquery>

							<!--- if we have more than one product group, show a dropdown else a hidden field --->
							<cfif productCategoriesQry.recordCount gt 1 and getDistinctProductGroups.recordCount gt 1>
								<!--- use bind function --->
								<!--- 2014-10-17			RPW			CORE-845 Not able to create Claim Cashback in portal side --->
								<cf_relayFormElementDisplay relayFormElementType="select" label="phr_cashback_#phrasePrefix#ProductGroupID" currentValue="" fieldname="productGroupID" bindFunction="cfc:relay.webservices.relayIncentiveWS.getFlaggedProductGroupsForSelect({productCategoryID},#defaultClaimsProductCatalogueCampaignID#,#request.relayCurrentUser.countryID#,#constrainProductByFlag#,'cashback_Claim_#phrasePrefix#')" value="productGroupID" display="productGroupDescription" nullText="phr_cashback_Claim_#phrasePrefix#SelectProductGroup" nullValue="" bindOnLoad="true" onChange="javascript:checkIfSerialRequired(this.value);" required="true">
							<cfelseif getDistinctProductGroups.recordCount gt 1>
								<!--- use normal query --->
								<!--- 2014-10-17			RPW			CORE-845 Not able to create Claim Cashback in portal side --->
								<cf_relayFormElementDisplay relayFormElementType="select" label="phr_cashback_#phrasePrefix#ProductGroupID" currentValue="" fieldname="productGroupID" query="#getDistinctProductGroups#" value="productGroupID" display="productGroupDescription" nullText="phr_cashback_Claim_#phrasePrefix#SelectProductCategory" nullValue="" onChange="javascript:checkIfSerialRequired(this.value)">
							<cfelse>
								<!--- use hidden field --->
								<cfset currentProductGroupId = "">
								<cfif getDistinctProductGroups.recordCount eq 1>
									<cfset currentProductGroupId = getDistinctProductGroups.productGroupID[1]>
								</cfif>

								<cfoutput>
									<CF_INPUT type="hidden" id="productGroupID" value="#currentProductGroupId#">
								</cfoutput>
							</cfif>

							<!--- show products --->
							<!--- 2014-10-17			RPW			CORE-845 Not able to create Claim Cashback in portal side --->
							<cf_relayFormElementDisplay relayFormElementType="select" label="phr_cashback_#phrasePrefix#product" currentValue="" fieldname="thisProductID" bindFunction="cfc:relay.webservices.relayIncentiveWS.getFlaggedProductsForSelect(#defaultClaimsProductCatalogueCampaignID#,#request.relayCurrentUser.countryID#,#constrainProductByFlag#,'cashback_Claim_#phrasePrefix#',{productGroupID})" value="productID" display="productDescription" nullText="phr_cashback_#phrasePrefix#Claim_SelectProduct" nullValue="" bindOnLoad="true" onChange="javascript:setInnerElement('discountPrice',this.value)" required="true">

							<cfscript>
								if (singleProductPerClaimItem) {
									variables.readOnly = true;
								} else {
									variables.readOnly = false;
								}
							</cfscript>

					</td>
				</tr>
				<!--- 2014-10-17			RPW			CORE-845 Not able to create Claim Cashback in portal side --->
				<cf_relayFormElementDisplay relayFormElementType="HTML" label="phr_cashback_Claim_#htmleditformat(phrasePrefix)#Product">
					<cfoutput>
					<div id="cashBackClaimContainer3">
						<div id="cashBackClaimHeader3" class="row">
							<div class="col-xs-12 col-sm-3">
								<label>phr_cashback_Claim_#htmleditformat(phrasePrefix)#UnitPrice</label>
								<div id="discountPrice"></div><!--- Gets overwritten busing javascript by quantity1 or serialNo1 depending upon information capture requirements --->
							</div>
							<div class="col-xs-12 col-sm-3">
								<label>phr_cashback_Claim_#htmleditformat(phrasePrefix)#quantity</label>
								<div id="qntCell"><input type="Text" name="quantityDummy" id="quantityDummy" size="2" maxlength="3" value="1" onChange="javascript:setLineTotal();" <cfif singleProductPerClaimItem>readOnly</cfif> class="form-control"></div>
							</div>
							<div class="col-xs-12 col-sm-3">
								<label>phr_cashback_Claim_#htmleditformat(phrasePrefix)#TotalValue</label>
								<div id="totalValue"></div>
							</div>
							<div class="col-xs-12 col-sm-3">
								<div id="snoLabel"></div>
								<div id="sno"></div>
						    </div>
						</div>
					</div>

					</cfoutput>
				</cf_relayFormElementDisplay>

				<cfif not hideSerialNo>
						<cf_relayFormElementDisplay relayFormElementType="text" fieldname="serialNo" maxLength="#serialNoLength#" label="phr_cashback_Claim_#phrasePrefix#SerialNumber" currentValue="" required="#serialNoRequired#">
						<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="" label="" currentValue="phr_cashback_claim_#phrasePrefix#serialNoInstructions">
				<cfelse>
					<input id="serialNo" name="serialNo" type="hidden" value="">
				</cfif>

					<!--- NJH 2009/11/06 SNY086 - show additionalReference field --->
					<cfif not hideAdditionalReference>
						<cf_relayFormElementDisplay relayFormElementType="text" maxLength="40" label="phr_cashback_claim_#phrasePrefix#additionalReference" currentValue="" fieldname="additionalReference" required="#additionalReferenceRequired#">
					<cfelse>
						<input id="additionalReference" name="additionalReference" type="hidden" value="">
					</cfif>

				<cfif not hideInvoiceNo>
						<cf_relayFormElementDisplay relayFormElementType="text" fieldname="invoiceNo" maxLength="50" label="phr_cashback_Claim_#phrasePrefix#InvoiceNumber" currentValue="" required="#invoiceNoRequired#">
				<cfelse>
					<input id="invoiceNo" name="invoiceNo" type="hidden" value="">
				</cfif>
				<!--- 2015-02-18 ACPK FIFTEEN-203 fixed datepicker not allowing dates to be selectable by swapping disableToDate and disableFromDate variables--->
				<cfif not hideInvoiceDate>
					<cfset windowStartDate = dateFormat(dateadd ("d",-#claimWindowDays# +1,now()),'yyyy-mm-dd')>
					<cfif windowStartDate gt claimFrom>
						<cfset disableToDate = windowStartDate>
					<cfelse>
						<cfset disableToDate = claimFrom>
					</cfif>
					<cfset disableFromDate = claimTo>

						<cf_relayFormElementDisplay relayFormElementType="date" label="phr_cashback_Claim_#phrasePrefix#InvoiceDate" currentValue="" fieldname="invoiceDate" formName="oppProductForm" anchorName="anchorX1" helpText="phr_dateFieldHelpText" disableFromDate="#disableFromDate#" disableToDate="#disableToDate#" enableRange="true" required="#invoiceDateRequired#">
				<cfelse>
					<input id="invoiceDate" name="invoiceDate" type="hidden" value="">
				</cfif>
				<cfif not hideEndUserCompanyName>
						<cf_relayFormElementDisplay relayFormElementType="text" fieldname="endUserCompanyName" size="40" maxLength="160" label="phr_cashback_Claim_#phrasePrefix#EndUserCompanyName" currentValue="" required="#endUserCompanyNameRequired#">
						<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="" label="" currentValue="phr_cashback_claim_#phrasePrefix#endCustomerInstructions">
				<cfelse>
					<input id="endUserCompanyName" name="endUserCompanyName" type="hidden" value="">
				</cfif>

				<cfif not hideDistiID>
						<cf_relayFormElementDisplay relayFormElementType="select" label="phr_cashback_claim_#phrasePrefix#distributor" currentValue="" fieldname="distiID" query="#qGetOrgDistis#" value="OrganisationID" display="OrganisationName" required="#distiIDRequired#" nullText="phr_cashback_Claim_#phrasePrefix#mustSpecifyDistributor" nullValue="">
				<cfelse>
					<input id="distiID" name="distiID" type="hidden" value="0">
				</cfif>

				<!--- 2014-10-17			RPW			CORE-845 Not able to create Claim Cashback in portal side --->
				<cf_relayFormElementDisplay relayFormElementType="button" label="" fieldname="addProductBtn" currentValue="phr_cashback_Claim_#phrasePrefix#AddLineItem" onClick="javascript:addProduct()">


			<!--- ==============================================================================
				   Existing claim items
			=============================================================================== --->
				<cfif structKeyExists(url, "readOnly")>
					<tr>
						<td colspan="2">
							<cfif not compareNoCase(CashbackClaimQry.status, "submitted")>
									phr_cashback_Claim_#htmleditformat(phrasePrefix)#StatusAwaitingApproval
							<cfelseif not compareNoCase(CashbackClaimQry.status, "approved")>
									phr_cashback_Claim_#htmleditformat(phrasePrefix)#StatusApproved
							</cfif>
						</td>
					</tr>
				</cfif>

				<cfoutput>
					<script>
						updateClaimItemDiv(#jsStringFormat(form.cashbackClaimID)#);

							<!--- NJH 2009/05/22 P-SNY069 if we have only a single product category, run this script.. it's normally run on the onChange. --->
							<cfif isDefined("currentProductGroupId")>
								resetTotal();
							</cfif>
							<cfif isDefined("currentProductGroupId")>
								checkIfSerialRequired(#jsStringFormat(currentProductGroupId)#);
							</cfif>
					</script>
				</cfoutput>

				<div id="cashbackClaimItemsDiv"></div>


				</cf_relayFormDisplay>
			</cfform>

			<!--- This form is simply to submit the claim. Having the mainForm do the submit causes issues as the form validation
				causes certain fields to be required which we don't need/want when actually submitting the claim as the products that we want to claim
				have already been added.
			 --->
			<cfform name="submitClaimForm" >
				<cfoutput>
					<CF_INPUT id="CashbackClaimID" type="hidden" name="CashbackClaimID" value="#CashbackClaimID#">
					<input id="frmTask" type="hidden" name="frmTask" value="submitClaim">
				</cfoutput>
			</cfform>
		</cfif>

	<!--- <cfelse>
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" class="withBorder">
			<tr>
				<td>
					phr_cashback_claim_Introduction
				</td>
			</tr>
			<form name="oppProductForm" method="post" enctype="multipart/form-data">
				<input type="hidden" id="frmTask" name="frmTask" value="createCashbackClaim">
				<tr>
					<td>
						<input type="button" value="phr_cashback_CreateNewClaim" onClick="javascript:document.oppProductForm.submit();">
					</td>
				</tr>
			</form>
		</table> --->

	</cfif>

</cfif>

</cf_translate>

