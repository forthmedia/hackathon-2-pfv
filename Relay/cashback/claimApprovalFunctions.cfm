<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			eServiceFunctions.cfm
Author:				KAP
Date started:		2003-09-03
Description:			

creates functionList query for tableFromQueryObject custom tag

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
03-Sep-2003			KAP			Initial version

Possible enhancements:


 --->

<cfscript>
comTableFunction = CreateObject( "component", "relay.com.tableFunction" );

comTableFunction.functionName = "Phr_Sys_IncentiveFunctions";
comTableFunction.securityLevel = "";
comTableFunction.url = "";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();

comTableFunction.functionName = "--------------------";
comTableFunction.securityLevel = "";
comTableFunction.url = "";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();

comTableFunction.functionName = "Phr_Sys_CashbackClaims";
comTableFunction.securityLevel = "";
comTableFunction.url = "";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();

comTableFunction.functionName = "&nbsp;phr_sys_approveCheckedClaims";
comTableFunction.securityLevel = "";
comTableFunction.url = "/cashback/viewCashbackClaims.cfm?action=approveClaim&frmRowIdentity=";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();

comTableFunction.functionName = "&nbsp;phr_sys_rejectCheckedClaims";
comTableFunction.securityLevel = "";
comTableFunction.url = "/cashback/viewCashbackClaims.cfm?action=rejectClaim&frmRowIdentity=";  /*javascript:document.frmBogusForm.submit();*/
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();
</cfscript>

