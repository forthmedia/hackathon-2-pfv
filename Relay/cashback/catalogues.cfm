<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		catalogues.cfm	
Author:			NJH  
Date started:	20-05-2009
	
Description:	Manage Cashback Catalogues	- wrapper for catalogues.cfm	

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->

<cfset promotionGroupTextID=application.com.settings.getSetting("cashback.promotionGroupTextID")>
<cfset keyColumnURLList="incentiveClaimsProductList.cfm?campaignID=">
<cfset keyColumnList = "catalogue">
<cfset keyColumnKeyList = "campaignID">
<cfinclude template="/products/catalogues.cfm">