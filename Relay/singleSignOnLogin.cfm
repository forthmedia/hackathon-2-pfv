<!--- �Relayware. All Rights Reserved 2014 --->




<cfscript>
	loginBehaviour=application.com.singleSignOnSiteControl.getSiteSSOBehaviour(request.currentSite.siteDefID);
	
	

	idpManager=application.javaloader.create("singleSignOn.samlserviceprovider.idps.IDPManager").init();

	allSamlIDPs = application.com.singleSignOnSiteControl.getValidIdentityProvidersForASite();

	oauthserverManager = new singleSignOn.common.editorBackingBean();
	validOAuthServers=oauthserverManager.getValidServers();

</cfscript>

<cfif loginBehaviour.defaultAllowSingleSignOn>

	<cf_head>
		<cf_title><cfoutput>#request.CurrentSite.Title#</cfoutput> - Login</cf_title>

		<link href="/styles/login.css" rel="stylesheet" media="screen,print" type="text/css">
	</cf_head>


	<cfoutput>
		<cfif loginBehaviour.autostartSingleSignOn and (allSamlIDPs.recordCount + arrayLen(validOAuthServers)) EQ 1>
			<!--- An autostarting SSO --->
			<cfif allSamlIDPs.recordCount EQ 1>
				<CF_SAMLAuthnRequestLaunchPost SAMLIdentityProviderID="#allSamlIDPs.SAMLIdentityProviderID#" launchButtonText="Sign in with #htmleditFormat(allSamlIDPs.identityProviderName)#" idpManager="#idpManager#" autolaunch="true">
			<cfelse>
				<CF_OAuth2AuthRequestLaunchPost OAuth2ID=#validOAuthServers[1].getClientId()# launchButtonText="phr_Sign_in_with #htmleditFormat(validOAuthServers[1].getServerName())#" serverManager=#oauthserverManager# class="form-control" autolaunch="true">
			</cfif>
				

		<cfelse>
		
			
			<div id="outerDiv">
				<div id="loginDiv" class="row">
					<div id="loginLeftCol" class="col-xs-12 col-sm-6">
						<div id="loginOuterDiv">
							<div id="loginInputDiv" style="text-align: center;">
								<CF_relayFormDisplay>
									<cfloop query="allSamlIDPs">
										<CF_SAMLAuthnRequestLaunchPost SAMLIdentityProviderID="#SAMLIdentityProviderID#" launchButtonText="phr_Sign_in_with #htmleditFormat(identityProviderName)#" idpManager="#idpManager#" class="form-control">
									</cfloop>
									<cfloop array=#validOAuthServers# index="servers">
										<CF_OAuth2AuthRequestLaunchPost OAuth2ID=#servers.getClientId()# launchButtonText="phr_Sign_in_with #htmleditFormat(servers.getServerName())#" serverManager=#oauthserverManager# class="form-control">
									</cfloop>
									<cfif loginBehaviour.defaultAllowRegularLogin>
										<form action="/" method="post">
											<input type="hidden" value="1" name="forceRW">
											<!---Not CF_relayFormElement as we need the form-control class--->
											<input class="form-control btn btn-primary" name="signInWithRelayware" value="phr_Sign_in_with Relayware" id="signInWithRelayware" type="submit">
										</form>
									</cfif>
								</CF_relayFormDisplay>
							</div>
						</div>
					</div>
					<div id="loginRightCol" class="col-sm-6 hidden-xs">
						<img src="/images/login/partnerUp.png">
					</div>
				</div>
				<footer id="loginFooter" role="footer">
					<ul>
						<li id="footerLogo"><a href="http://www.relayware.com"><img src="/images/login/logo.png"></a></li>
					</ul>
				</footer>
			</div>
			<div id="loginCopyright">
				<p>Copyright &copy; <cfoutput>#DateFormat(Now(),"yyyy")#</cfoutput> relayware.com, inc. All rights reserved.</p>
			</div>
		
		</cfif>
	
	</cfoutput>
<cfelse>	
	Single sign on not permitted
</cfif>