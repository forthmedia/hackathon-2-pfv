<!--- �Relayware. All Rights Reserved 2014 --->
<!--- amendment History

Version	 Date		By 		Description
1
1.0?	1999-04-13		WAB 	Added fields required for search by first and last name
1.1		2001-03-02		WAB 	Added check for frmMovePeople being defined, which only allows searches for locations
2002-06-07		SWJ		Added the code to search by campaigns
2004-10-02	SWJ		Added flagGroupName to  getFlagCriteria
2005-05-17		WAB altered query bringing back selections to use selections.cfc
2007-04-17	AJC Added in new filter OrganisationTypeID
2008/07/15		WAB correct bug with JS used to opendrilldown (was taking behaviour from setting on left menu)
2010/04/14 GCC removed an incorrect alias in the order by clause which broke in sql 2005 but was ok in sql 2000
2011-03-03	NYB	LHID5635: added a FlagNameSizeLimit for the Profile attributes dropdown, because a flag with a name of 237 chars was making this page difficult to work with
2011/06/30	NAS		P-LEX053 - Add VAT Search
2013-09-17	IH	Allow multiple Organisation IDs to be entered
2015-12-24  SB	Bootstrap
2016-01-06  BF-159 None of the fields on this page are required but several show as if they are
2016/01/07	NJH	JIRA BF-200 - remove link to 'Show in 2 Pane view'
2016/06/09	NJH	JIRA PROD2016-342 - clean up advanced search page. Remove some unused fields. Changed orgType and country dropdown to support multiple values. Use relayFormElementDisplay. Replaced
				inline queries with function calls (such as to get countries,etc)
2017/02/21	SBW	JIRA RT-28 - Added check to determine Org or Loc Account manager; display different field label depending.
--->

<!--- user rights to access:
		SecurityTask:dataTask
		Permission: Read
---->

<!--- To add new search fields
1:	Create input boxes for each one in this template
2:	Amend Javascript which checks whether any criteria have been entered
3:  Add line of form "<CFPARAM NAME="frmAddress" DEFAULT="">
<CFPARAM NAME="frmPostalCode" DEFAULT="">" into (almost) every template in the data directory
4:  add line of form "	<INPUT TYPE="HIDDEN" NAME="frmLastName" VALUE="#frmLastName#">" into almost every template in the data directory
5:  add search criteria to the query in loclist.cfm
6:  add code for displaying criteria in the header of loclist.cfm
 --->

<cfset FlagNameSizeLimit = 75>

<!--- <cfinclude template="../templates/get UserSecurityLevels.cfm"> --->
<CFIF NOT checkPermission.Level1 GT 0>
	<CFSET message="Phr_Sys_YouAreNotAuthorised">
		<CFINCLUDE TEMPLATE="datamenu.cfm">
	<CF_ABORT>
</CFIF>

<CFINCLUDE TEMPLATE="SearchParamDefaults.cfm">
<!--- SearchParamDefaults sets up the list of Flags used to search by --->

<!--- AJC 2007-04-17 Query to populate organisation type drop down --->
<cfquery name="qry_get_organisationTypes" datasource="#application.siteDataSource#">
	SELECT organisationTypeID,'phr_sys_'+TypeTextID as TypeTextID
	FROM organisationType
	WHERE Active=1
	ORDER BY sortIndex
</cfquery>
<!--- this incl
ude provides the logic for the Relay Form Custom Tags --->
<cfinclude template="/templates/relayFormJavaScripts.cfm">

<!--- get a list of all live campaigns --->
<!--- <CFQUERY NAME="getCampaigns" datasource="#application.siteDataSource#">
	select commid,commTitle from phoneCommRecords where complete = 0
</CFQUERY> --->

<!--- 2010/04/14 GCC removed an incorrect alias in the order by clause which broke in sql 2005 but was ok in sql 2000 --->
<CFQUERY NAME="getFlagCriteria" datasource="#application.siteDataSource#">
	SELECT f.FlagID, fg.name + '-' + f.Name as name,
	left(fg.name,#FlagNameSizeLimit#)+case when len(fg.name) >  <cf_queryparam value="#FlagNameSizeLimit#" CFSQLTYPE="CF_SQL_INTEGER" >  then '...-' else '-' end + left(f.name,#FlagNameSizeLimit#) + case when len(f.name) >  <cf_queryparam value="#FlagNameSizeLimit#" CFSQLTYPE="CF_SQL_INTEGER" >  then '...' else '' end as ShortName,
	f.FlagGroupID, f.OrderingIndex
	      FROM Flag AS f inner join FlagGroup AS fg on f.flaggroupid=fg.flaggroupid
			WHERE f.Active <> 0
			AND fg.Expiry > getDate()
			AND fg.Active <> 0
		   	AND fg.UseInSearchScreen = 1
		   	AND f.FlagGroupID = fg.FlagGroupID

		UNION
		SELECT f.FlagID, f.Name as name,
	left(f.name,#FlagNameSizeLimit#) + case when len(f.name) >  <cf_queryparam value="#FlagNameSizeLimit#" CFSQLTYPE="CF_SQL_INTEGER" >  then '...' else '' end as ShortName,
	f.FlagGroupID, f.OrderingIndex
	      FROM Flag AS f
			WHERE f.Active <> 0
		   	AND f.UseInSearchScreen = 1
		ORDER BY  Name
</CFQUERY>


<CFIF IsDefined("frmDupPeople")>
	<CFIF frmDupPeople IS NOT 0>
		<CFSET frmMovePeople = frmDupPeople>
		<CFIF IsDefined("frmMoveFreeze")>

			<CFQUERY NAME="Checklist" datasource="#application.siteDataSource#">
			SELECT * FROM Person
			 WHERE PersonID  IN ( <cf_queryparam value="#frmMovePeople#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			</CFQUERY>
			<CFIF Checklist.RecordCount IS NOT 0>
				<CFQUERY NAME="CheckLastUpd" datasource="#application.siteDataSource#">
				SELECT max(Person.LastUpdated) AS LastUpdated  FROM Person
				 WHERE PersonID  IN ( <cf_queryparam value="#frmMovePeople#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
				</CFQUERY>

				<CFIF DateDiff("s", frmMoveFreeze, CheckLastUpd.LastUpdated) GT 0>
					<CFSET message = "Records have been changed since initial selection. You will need to search and make your selection again.">
					<CFINCLUDE TEMPLATE="datamenu.cfm">
					<CF_ABORT>
				</CFIF>
				<CFSET frmMoveFreeze = CreateODBCDateTime(Now())>
				<CFQUERY NAME="UpdatePerson" datasource="#application.siteDataSource#">
				UPDATE Person
				SET LastUpdatedBy = #request.relayCurrentUser.usergroupid#,
					LastUpdated =  <cf_queryparam value="#frmMoveFreeze#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
				 WHERE PersonID  IN ( <cf_queryparam value="#frmMovePeople#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
				</CFQUERY>
			</CFIF>
		</CFIF>
	</CFIF>
</CFIF>


<cf_head>
	<cf_includejavascriptonce template="/javascript/viewEntity.js">
	<cf_includejavascriptonce template="/javascript/extExtension.js">
	<SCRIPT type="text/javascript">

	<!--

		function doForm(nextForm,drillLevel,inTab) {
			var form = document.ThisForm;
			var found = "no";
			var ignore = "no";
			if (form.frmSiteName.value != "" && form.frmSiteName.value != " ") {
				found = "yes";
				//form.frmSearchType.value = "Loc";
			}
			// 2011/06/30	NAS		P-LEX053 - Add VAT Search
			if (form.frmVAT.value != "" && form.frmVAT.value != " ") {
				found = "yes";
				//form.frmSearchType.value = "Org";
			}

			if (jQuery('#frmCountryID').val() != null) {
				found = "yes";
				//form.frmSearchType.value = "Loc";
			}

			for (i=0; i<form.frmFlagIDs.length; i++) {
				mselect = form.frmFlagIDs.options[i];
				if (mselect.selected == true && mselect.value != 0) {
					found = "yes";
					//form.frmSearchType.value = "Org";
				}
			}
			for (i=0; i<form.frmAccountMngrID.length; i++) {
				mselect = form.frmAccountMngrID.options[i];
				if (mselect.selected == true && mselect.value != 0) {
					found = "yes";
					//form.frmSearchType.value = "Org";
				}
			}

			if (form.frmEmail.value != "" && form.frmEmail.value != " ") {
				found = "yes";
				//form.frmSearchType.value = "Per";
			}
			if (form.frmTelephone.value != "" && form.frmTelephone.value != " ") {
				found = "yes";
				//form.frmSearchType.value = "Loc";
				form.frmTelephone.value = form.frmTelephone.value.replace(/[^0-9%]/gi,""); // NJH 2007/10/03 CR-TNDNAB502 search only on the numbers
			}
			if (form.frmPostalCode.value != "" && form.frmPostalCode.value != " ") {
				found = "yes";
				//form.frmSearchType.value = "Loc";
			}

			if (form.frmLastName.value != "" && form.frmLastName.value != " ") {
				found = "yes";
				//form.frmSearchType.value = "Per";
			}
			if (form.frmFirstName.value != "" && form.frmFirstName.value != " ") {
				found = "yes";
				//form.frmSearchType.value = "Per";
			}

			// NJH 2007/10/03  - CR-TNDNAB502 - fax is being searched in the frmTelephone field
			if (form.frmOrgURL.value != "" && form.frmOrgURL.value != " ") {
				found = "yes";
				//form.frmSearchType.value = "Loc";
			}

			if (form.frmAddress.value != "" && form.frmAddress.value != " ") {
				found = "yes";
				//form.frmSearchType.value = "Loc";
			}

			if (form.frmSrchPersonID.value != "" && form.frmSrchPersonID.value != " ") {
				found = "yes";
				//form.frmSearchType.value = "Per";
			}

			if (form.frmSrchOrgID.value != "" && form.frmSrchOrgID.value != " ") {
				found = "yes";
				//form.frmSearchType.value = "Org";
			}

			if (form.frmSrchLocationID.value != "" && form.frmSrchLocationID.value != " ") {
				found = "yes";
				//form.frmSearchType.value = "Loc";
			}

			if (form.frmSelectionID.options[form.frmSelectionID.selectedIndex].value != 0) {
				found = "yes";
				//form.frmSearchType.value = "Loc";
			}

			//if (form.frmNoContacts.checked  && nextForm == "orglist.cfm") {
			//	alert("\nYou can only view organisations with 0 person record via the frame view.");
			//	return;
			//}

			//if (form.frmNoContacts.checked) {
			//	found = "yes";
			//	form.frmSearchType.value = "Per";
			//}
			//if (form.frmDupeLocs.checked) {
			//	found = "yes";
			//	form.frmSearchType.value = "Loc";
			//}

			//if (form.frmDupePers.checked) {
			//	found = "yes";
			//	form.frmSearchType.value = "Per";
			//}

			// added by NJH 2008/07/08. Should be a valid search criteria
			if (form.frmLastContactDate.value != '') {
				found = "yes";
				//form.frmSearchType.value = "Loc";
			}

			if (found == "no") {
				alert("\nYou must enter some criteria.");
			}
			else
			{

				// special code if running in Ext interface
				if (topWindow = getReferenceToUIExtWindow ()  ) {
					params = jQuery('#details').serialize();

					if (nextForm == 'dataFrame.cfm') {
						topWindow.Ext.addTab ('dataFrame','Results Frame','\/data\/dataFrame.cfm?'+ params,{iconClass:'search'})
						return false
					}
				}

				form.action = nextForm;
				//form.frmDrillLevel.value = drillLevel
				form.submit();
			}

			return;
		}


	//-->
	</SCRIPT>
</cf_head>

<div class="header">
	<h1>
		Phr_Sys_SearchOrgSitePeople
	</h1>
	<p>Phr_Sys_SearchAccountDatabase</p>
</div>

<CFIF IsDefined("frmMovePeople")>
	<CFIF Checklist.RecordCount IS 0>
		<p>Phr_Sys_NoSelectPersonMove</p>
		<CF_ABORT>
	</CFIF>
</CFIF>

<!--- SBW: check settings to find organisation or location, get appropriate phrase --->
<cfset whichAccMgr = (application.com.settings.getSetting('PLO.USELOCATIONASPRIMARYPARTNERACCOUNT') eq 1 ? "phr_flag_LocAccManager" : "phr_flag_AccManager" )>

<form action="loclist.cfm?RequestTimeout=500" method="post" name="ThisForm" id="details">

<CF_relayFormDisplay>

	<!--- <CF_relayFormElementDisplay relayFormElementType="hidden" currentvalue="" label="" fieldname="frmSearchType"> --->
	<CF_relayFormElementDisplay relayFormElementType="hidden" currentvalue="" label="" fieldname="frmDrillLevel">
	<CF_relayFormElementDisplay relayFormElementType="hidden" currentvalue="dataSearch" label="" fieldname="frmNext">
	<CF_relayFormElementDisplay relayFormElementType="hidden" currentvalue="main" label="" fieldname="frmNextTarget">

	<CFIF IsDefined("frmMovePeople")>
		Phr_Sys_SearchLoctoMovePeople
	</CFIF>

	<cf_relayFormElementDisplay type="numeric" name="frmSrchOrgID" label="Phr_Sys_OrganisationUID" value="">
	<cf_relayFormElementDisplay type="numeric" name="frmSrchLocationID" label="Phr_Sys_LocationUID" value="">
	<cf_relayFormElementDisplay type="numeric" name="frmSrchPersonID" label="Phr_Sys_PersonUID" value="">
	<cf_relayFormElementDisplay type="select" name="frmOrganisationTypeID" label="Phr_Sys_CompanyType" multiple="true" currentValue="" query="#qry_get_organisationTypes#" display="typeTextID" value="organisationTypeID">
	<cf_relayFormElementDisplay type="text" name="frmSiteName" label="Phr_Sys_CompanyName" value="" noteText="Phr_search_wildcardcomment Phr_search_Wildgenerator_at_end">
	<cf_relayFormElementDisplay type="text" name="frmVAT" label="Phr_Sys_VAT" value="" noteText="Phr_search_wildcardcomment">
	<cf_relayFormElementDisplay type="select" name="frmFlagIDs" label="Phr_Sys_ProfileAttributes" multiple="true" currentValue="" query="#getFlagCriteria#" display="ShortName" value="flagID" noteText="Phr_Sys_ThislistisenabledinProfileManager">
	<cf_relayFormElementDisplay type="select" name="frmAccountMngrID" label="#whichAccMgr#" multiple="true" currentValue="" query="#application.com.validValues.getExistingAccountManagers()#">
	<cf_relayFormElementDisplay type="text" name="frmFirstName" label="Phr_Sys_FirstName" value="" noteText="Phr_search_wildcardcomment">
	<cf_relayFormElementDisplay type="text" name="frmLastName" label="Phr_Sys_LastName" value="" noteText="Phr_search_wildcardcomment">
	<cf_relayFormElementDisplay type="text" name="frmEmail" label="Phr_Sys_Email" value="" noteText="Phr_search_wildcardcomment">
	<cf_relayFormElementDisplay type="text" name="frmTelephone" label="Phr_Sys_Telephone" value="" noteText="Phr_search_wildcardcomment Phr_Sys_PhoneFaxSearch">
	<cf_relayFormElementDisplay type="select" name="frmCountryID" label="Phr_Sys_Country" multiple="true" currentValue="" query="#application.com.relayCountries.getCountriesValidValues(liveCountriesOnly=false,showCurrentUsersCountriesOnly=true)#">
	<cf_relayFormElementDisplay type="text" name="frmAddress" label="Phr_sys_cityRegion" value="" noteText="Phr_search_wildcardcomment">
	<cf_relayFormElementDisplay type="text" name="frmPostalCode" label="Phr_Sys_PostalCode" value="" noteText="Phr_search_wildcardcomment">
	<cf_relayFormElementDisplay type="text" name="frmOrgURL" label="Phr_Sys_CompanyURL" value="" noteText="Phr_search_wildcardcomment">
	<CF_relayFormElementDisplay type="date" label="Phr_Sys_LastContactDateSince" currentValue="" thisFormName="ThisForm" name="frmLastContactDate" helpText="Click to choose date" showClearLink="true">

		<!--- PKP: 2008-02-08 hide select if no campaigns exist--->
		<!--- <cfif getCampaigns.recordcount gt 0>
	<div class="form-group">
			<label for="frmCommID">Phr_Sys_Campaigns:</label>
			<SELECT NAME="frmCommID" id="frmCommID" class="form-control">
				<OPTION VALUE="0">Phr_Sys_SelectCampaign</OPTION>
				<CFOUTPUT QUERY="getCampaigns">
					<OPTION VALUE="#commID#">#HTMLEditFormat(commTitle)#</OPTION>
				</CFOUTPUT>
			</SELECT>

		<cfelse>
			<INPUT TYPE="HIDDEN" NAME="frmCommID" VALUE="0">
		</cfif>
	</div> --->

	<!--- <div class="checkbox">
		<label>
			Phr_Sys_Organisationswithnocontacts
			<INPUT TYPE="CHECKBOX" NAME="frmNoContacts" id="frmNoContacts" VALUE="1"> Just show organisations with 0 person records
		</label>
	</div> --->

	<cf_relayFormElementDisplay type="select" name="frmSelectionID" label="Phr_Sys_Selections" currentValue="" query="#application.com.selections.getSelections()#" display="Title" value="SelectionID" showNull="true">

	<div class="checkbox">
		<label>
			<!--- Phr_Sys_DeletedPolData --->
			<INPUT TYPE="CHECKBOX" NAME="frmDeleted" id="frmDeleted" VALUE="1"> Phr_Sys_searchDeletedPolData
		</label>
	</div>
	<!--- <div class="form-group">
		<label class="radio-inline">
			<INPUT TYPE="radio" NAME="frmLooseMatch" VALUE="0"> Phr_Sys_LooseMatch
		</label>
		<label class="radio-inline">
			<INPUT Type="radio" name="frmLooseMatch" value="1" CHECKED> Phr_Sys_TightMatch
		</label>
	</div> --->

	<div class="form-group">
				<CFIF not IsDefined("frmMovePeople")>
					<input type="button" class="btn btn-primary" value="Phr_Sys_ShowDataFrameView" onClick="doForm('dataFrame.cfm', 'org')">
					<input type="button" class="btn btn-primary" value="Phr_Sys_SearchTextFields" onClick="window.location.href = 'textSearch.cfm'">
				</CFIF>
	</div>

	<CFIF IsDefined("frmMovePeople")>
	<!--- Insert form elements if this is to search locations whilst moving people --->
		<CF_INPUT TYPE="HIDDEN" NAME="frmMovePeople" VALUE="#frmMovePeople#">
		<CF_INPUT TYPE="HIDDEN" NAME="frmMoveFreeze" VALUE="#frmMoveFreeze#">
	</CFIF>

</CF_relayFormDisplay>

</form>