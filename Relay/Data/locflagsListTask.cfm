<!--- �Relayware. All Rights Reserved 2014 --->





<CFIF NOT #checkPermission.UpdateOkay# GT 0>
	<!--- if frmLocationID is not 0 then task is to update
	      but the user doesn't have update permissions --->
	<CFSET message="You are not authorised to use this function">
	<CFINCLUDE TEMPLATE="datamenu.cfm">
	<CF_ABORT>

</CFIF>

<!--- <cfinclude template="/templates/qrygetcountries.cfm"> --->

<CFPARAM NAME="frmBack" DEFAULT="datamenu">
<CFPARAM NAME="frmNext" DEFAULT="locdedupesum">
<CFPARAM NAME="frmLocationID" DEFAULT="0">

<CFPARAM NAME="message" DEFAULT="">

<CFTRANSACTION>
	<!--- the last updated flag used will be that of the Location table --->
	<CFSET updatetime = CreateODBCDateTime(Now())>
	
	<!--- check last updated date/time --->
	<CFQUERY NAME="checkLocation" DATASOURCE=#application.SiteDataSource#>
		SELECT LocationID, SiteName, LastUpdated
		FROM Location
		WHERE LocationID =  <cf_queryparam value="#frmLocationID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		AND LastUpdated =  <cf_queryparam value="#insLastUpdated#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
	</CFQUERY>
	
	<CFIF #checkLocation.RecordCount# IS 0>
		<!--- Removed to get round JIT compilation error </CFTRANSACTION> --->
		<CFSET #message# = "The locations's record could not be updated since it has been changed by another user since you last viewed it.">
		<CFINCLUDE TEMPLATE="#frmNext#.cfm">
		<CF_ABORT>
	</CFIF>

	<CFPARAM NAME="insPartnerType" DEFAULT="0">

	
	<!--- New Flags --->
	<CFINCLUDE TEMPLATE="../flags/flagTask.cfm">
	
	<CFQUERY NAME="updLocation" DATASOURCE=#application.SiteDataSource#>
		UPDATE Location SET
		 LastUpdatedBy = #request.relayCurrentUser.usergroupid#,
		 LastUpdated =  <cf_queryparam value="#Variables.updatetime#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
		WHERE LocationID =  <cf_queryparam value="#frmLocationID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</CFQUERY>

			
</CFTRANSACTION>




<CFSET message = "Flags for location #checkLocation.SiteName# (#checkLocation.LocationID#) have been updated successfully.">
<CFINCLUDE TEMPLATE="#frmNext#.cfm">
<CF_ABORT>
</FORM>

</CENTER>
</FONT>


