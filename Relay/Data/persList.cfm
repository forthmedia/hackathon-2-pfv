<!--- �Relayware. All Rights Reserved 2014 --->
<!---
	Amendment History
	Version	Date		By		Description
	1999-04-16 	WAB 	Added job function to list and direct/indirect to header
	1999-04-14		WAB 	Added fields required for more searches
	1999-04-13		WAB 	Added fields required for search by first and last name
	2		01 Jul 00 	SWJ		Added ShowLocDetails (line 14) param which turns of the loc details stuff
	2.10	2001-02-28 	WAB 	altered lcoation query to user left outer joins
	2.11	2001-02-03		WAB 	altered where the person move form points to
	2.12	2001-07-17		WAB		altered to use persondropdown (which has all the new flag things in it), new savechecks('entity') code used
	2.13	2001-07-17	SWJ		altered look and feel and added images to make it more user friendly
	2.14	2001-09-06	SWJ		altered the listing screen slightly to work better and include jobDesc column
	2.15	2001-10-12	SWJ		altered to support sorting
	2.16	2001-10-22	CPS		added personid and locationid when calling email template
	2.17	2002-02-03	SWJ		Modified calls to add contact, quickmail, notes and actions
	to popup a window
	2002-03-05	SWJ		Added popup function to add action
	2002-05-22	SWJ		Modified call to addActions to mak it generic
	2002-07-21	SWJ		Modified javascript loads to load javascript from relayware folders
	2006-03-16	SWJ		Introduced vPersList and showDeletedRecords to filter out records marked for deletion
	2007-08-09	SWJ		Imroved the dedupe function call and made it a popup
	2007-11-14	SSS		I have added a switch to turn on and off manual merge
	2008/02		wab		Mods to work with new EntityNavigation - new links for editing/adding people and locs
	2008-04-28 WAB  added handling of frmfullname
	2008-06-13      SSS     Merge tasks code has been added now these merge taks are required for a person to merge
	2008-10-08 	NJH		CR-TND561 1.1.3 -if the person is inactive, don't allow the sending of an email
	2008-10-16	NJH		T-10 Bug Fix Issue 863 - Refresh person list after person merge complete. Simply submit the form.
	2008-11-14	NJH		T-10 Bug Fix Issue 888 - When clicking on 'Add New Person', determine whether the org has any locations using an Ajax call.
	If there are locations, add the person else display a message to add a location first.
	2009-01-12		WAB		Speed up getting persList by getting rid of the lastcontact date (requires new view).
	Also changed email image
	2009-01-20    WAB BUG-12816 continued.  Added a switch to allow lastcontact to be shown if wanted. Ended up not using a new view, but selectivey getting columns of old view (see com.relayplo)
	2009-10-27			NAS		LID - 2755 'Add New Site' and 'Add New Person' removed when no Organisation found
	2010-10-05		WAB 	request.manual....On becomes a setting
	2011-06-03 		PPB 		REL106 added selectedRowColor
	2011-07-20 			PPB 	LEN024 switch to show firstname/lastname Separately; show phr_sys_InActive against inactive persons; filter to show Active/InActive records
	2011-10-26		RMB			LID7549 Missing Hidden value to pass sort option
	2012/01/12 		PPB 		P-REL112 connect to linkedIn
	2012-12-10 		IH			Case 432420 use request.currentSite.httpProtocol instead of hard coded protocol to get LinkedIN JS file
	2013/08/09		NJH			Case 436426 Removed the 'link to linkedIn' functionality. not implemented correctly.
	2014-08-19		RPW			Remove SMS link from Dashboard
	2014-09-08 		AXA 		CASE 441742  blank orgid passing through preventing dashboard from displaying for org
	2014-10-04		SB			Moved around column order and changed no contact text to a blank icon.
	2015-12-18      SB			Bootstrap
	2016/01/22		NJH			Jira BF-331 - moved showInactive to tophead menu
	2017-02-15		STCR		JIRA RT-175 - removed duplicated hidden fields within Previous/Next paging forms, which caused performance problems for Organisations with many People.
	2017-02-03		DBB			ENT-6: Hiding screen elements for read only users	
--->

<cfset dataTaskL2 = application.com.login.checkInternalPermissions("DataTask","Level2")><!---DBB ENT-6--->
<cfset frmSearchLocationID = ""> <!--- initialise list of locations --->

<!--- JIRA PROD2016-342 NJH 2016/06/09 - add locations to search results. Add location in switch below to get the proper organisation --->
<cfif isDefined("entitytypeID")>
	<cfswitch expression="#entitytypeID#">
		<cfcase value="0">
			<cfset form.frmsrchPersonID = entityID>
			<cfset form.frmOrganisationID = application.com.relayPLO.getPersonDetails(personID=entityID).organisationID>
		</cfcase>
		<cfcase value="1">
			<cfset frmSearchLocationID = entityID>
			<cfset form.frmOrganisationID = application.com.relayPLO.getLocDetails(locationID=entityID).organisationID>
		</cfcase>
		<cfcase value="2">
			<cfset form.frmOrganisationID = entityID>
		</cfcase>
	</cfswitch>
</cfif>

<!--- START 2014-09-08 AXA CASE 441742  blank orgid passing through preventing dashboard from displaying for org--->
<cfif isDefined('frmOrganisationID') AND len(trim(frmOrganisationID)) EQ 0>
	<cfset frmOrganisationID = 0 />
</cfif>
<!--- END 2014-09-08 AXA CASE 441742  --->
<cfset selectedRowColor = application.com.settings.getSetting("miscellaneous.selectedRowColor")>
<!--- 2011-06-03 PPB REL106 --->
<!--- redirect back to loclist if from Forward/Back buttons --->
<cfif isdefined("ListForward.x") or isdefined("ListBack.x")>
	<cfinclude template="loclist.cfm">
	<CF_ABORT>
</cfif>
<!--- manages the default sort order.  frmPersListSortOrder can be over-ridden in relayINI.cfm
	WAB 2010-10-19 moved to settings
	<cfparam name="frmPersListSortOrder" default="p.FirstName asc"> <!---  2009-01-12 WAB removed lastContactDate desc, when this column removed from view--->
--->
<cfset PersListSortOrder = application.com.settings.getSetting('screens.personList.SortOrder')>
<!--- Note: this dictates the sortorder 1st time in; it is not used once we start clicking on column headings --->
<cfset showLastContactDate = application.com.settings.getSetting('screens.personList.showLastContactDate')>
<cfset showFirstNameSeparately = application.com.settings.getSetting('screens.personList.showFirstNameSeparately')>
<!--- 2011-07-20 PPB LEN024 switch to show firstname/lastname Separately --->
<cfset LinkedInEnabled = application.com.settings.getSetting("socialMedia.enableSocialMedia") and (ListFindNoCase(application.com.settings.getSetting("socialMedia.services"),'LinkedIn') gt 0)>
<!--- 2012/01/12 PPB P-REL112 connect to linkedIn --->
<!--- 2006-03-16 SWJ suppresses deleted records from showing. Over-ride in relayINI --->
<cfparam name="showDeletedRecords" default="0">
<cfparam name="frmSortOrder" default="#PersListSortOrder#">
<cfparam name="getPersonCount" default="">
<cfparam name="ShowLocDetails" default="0">
<!--- This controls whether to show loc details or not --->
<cfparam name="message" default="">
<cfset showrecords =100>
<!--- overrides application .cfm setting --->
<cfset displaystart = 1>
<cfset frmback = "locmain">
<!--- where to go for a "return" --->
<cfset frmnext = "locmain">
<!--- next page to show --->
<cfset mergeok = 0>
<div id="personList">
	<cfif not checkpermission.level1 gt 0>
		<cfset message="Phr_Sys_YouAreNotAuthorised">
		<cfinclude template="datamenu.cfm">
		<CF_ABORT>
	</cfif>
	<!--- all of these should really be coming in as URL. OR FORM.
		but we can fool the system so we don't need to check
		for IsDefined
		These are stored so that we can go back to the orginal query
		--->
	<cfinclude template="searchparamdefaults.cfm">
	<cfparam name="Partners" default="">
	<cfparam name="frmStart" default="1">
	<cfparam name="frmStartp" default="1">
	<cfparam name="frmPeopleOnPage" default="0">
	<cfparam name="frmorganisationID" type="numeric" default="1">
	<!--- check to see if any criteria exists --->
	<cfparam name="AnyCriteria" default="no">
	<cfparam name="frmLocationID" default="0">
	<!--- is really a URL.variable --->
	<cfset frmShowInActiveRecords = structKeyExists(form,"frmShowInActiveRecords") and form.frmShowInActiveRecords>
	<!--- 2011-07-20 PPB LEN024 if it's defined in the form scope then it's checked --->
	<cfscript>
	// if there are any criteria that will filter the number of person records returns in getPersList then get totalPeopleInOrg
	// WAB 2008-04-28 added fullname
	IF ((isDefined("frmFirstName") and len(frmFirstName) gt 0)
		or (isDefined("frmLastName") and len(frmLastName) gt 0)
		or (isDefined("frmFullName") and len(frmFullName) gt 0)
		or (isDefined("frmsrchPersonID") and len(frmsrchPersonID) gt 0)
		or (isDefined("frmEmail") and len(frmEmail) gt 0)
		or (isDefined("frmLocationIDs") and len(frmLocationIDs) gt 0)
		or (isDefined("frmSelectionID") and frmSelectionID IS NOT 0)){
		//frmSortOrder ="p.firstName,p.lastName";			<!--- 2011-07-20 PPB LEN024 not sure why we would want to stomp over the setting --->
		totalPeopleInOrg = application.com.relayPLO.countPeopleInOrg(organisationID=frmOrganisationID);
		}

	// 2006-03-16 SWJ Moved to a view based query in relayPLO and introduced request.showDeletedRecords to supress deleted
	// NJH 2016/06/10 - pass through frmSearchLocationID to handle location-based searches
	getPersList = application.com.relayPLO.getPersList(organisationID=frmorganisationID,firstName=frmFirstName, fullName=frmFullName, email=frmEmail, lastName=frmLastName, personid=frmsrchPersonID, selectionID=frmSelectionID, sortOrder=frmSortOrder, getLastContactDate=showLastContactDate, showInActiveRecords=frmShowInActiveRecords,showDeletedRecords=showDeletedRecords,locationIDs=frmSearchLocationID);

	getOrgDetails = application.com.relayPLO.getOrgDetails(organisationID=frmOrganisationID);   // need org name to output a message

</cfscript>
	<cfif getperslist.recordcount gt application.showrecords>
		<!--- showrecords in application .cfm defines the number of records to display --->
		<!--- define the start record --->
		<cfif structKeyExists(FORM,"PersonForward.x")>
			<cfset displaystart = frmstartp + application.showrecords>
		<cfelseif structKeyExists(FORM,"PersonBack.x")>
			<cfset displaystart = frmstartp - application.showrecords>
		<cfelseif isdefined("frmStartp")>
			<cfset displaystart = frmstartp>
		</cfif>
	</cfif>

	<cfset application.com.request.setTophead(getOrgRecordCount=getOrgDetails.recordCount,pageTitle="",getPersonRecordCount=getperslist.recordcount,showInActiveRecords=frmShowInActiveRecords)>

	<cf_includeJavascriptOnce template = "/javascript/dropdownFunctions.js">
	<cf_includeJavascriptOnce template = "/javascript/checkBoxFunctions.js">
	<cf_includeJavascriptOnce template = "/javascript/viewEntity.js">
	<cf_includeJavascriptOnce template = "/javascript/openWin.js">
	<cf_includeJavascriptOnce template = "/javascript/addShowActions.js">
	<cfif LinkedInEnabled>
		<cfoutput>
			<script src="#request.currentSite.httpProtocol#platform.linkedin.com/in.js" type="text/javascript"></script>
		</cfoutput>
		<!--- 2012/01/12 PPB P-REL112  I haven't used cf_includeJavascriptOnce in case it breaches ts&cs --->
	</cfif>
	<cf_translate phrases="phr_Sys_pleaseAddLocationFirst"/>
	<!--- NJH 2008-11-14 T-10 Bug Fix Issue 888 end --->
	<cf_head>
	<SCRIPT type="text/javascript">
	function reSort(sortOrder) {
		var form = document.reSort;
		form.frmSortOrder.value = sortOrder;
		form.submit();
	}

	function addContactRecord(personID) {
		openWin('../data/actionEntryFrame.cfm?frmEntityType=person&frmcurrentEntityID='+personID,'AddContact','width=520,height=600,toolbar=0,location=0,directories=0,status=1,menuBar=0,scrollBars=1,resizable=1');
	}

	function newPerson(){
		var FormName = "mainForm";
		var form = eval('document.' + FormName);
		form.target="Detail";
		form.action="persondetail.cfm";
		form.frmPersonID.value=0;
		doForm(FormName);
	}

	function backToList(){
		var FormName = "LocForm";
		var form = eval('document.' + FormName);
		form.frmTask.value='back';
		form.frmNext.value='loclist';
		doForm(FormName);
	}


	function quickMail(personID, locationID, email){
		var qryStr = "quickMailClient.cfm?frmpersonID="+personID+"&frmlocationID="+locationID+"&frmMailto="+email
		openWin(qryStr,'QuickMail','width=650,height=550,toolbar=0,location=0,directories=0,status=1,menuBar=0,scrollBars=0,resizable=1');
		//form.frmPersonID.value = personID;
		//form.frmLocationID.value = locationID;
		//form.frmMailTo.value = email;
		//form.submit();
	}

	function quickSMS(personID){
		var qryStr = "quickSMS.cfm?personid="+personID
		openWin(qryStr,'QuickSMS','width=650,height=550,toolbar=0,location=0,directories=0,status=1,menuBar=0,scrollBars=0,resizable=1');
	}

	function doForm(FormName) {
		var form = eval('document.' + FormName);
		form.frmDupPeopleCheck.value = saveChecks('Person');
		form.submit();
	}

	//PPB/NJH: Bug Fix T-10 Issue 863 2008-10-02 refresh after person dedupe
	function personDedupeCompleted() {
		var FormName = "mainForm";
		var form = eval('document.' + FormName);
		form.submit();
	}

	// NJH 2008-11-14 T-10 Bug Fix Issue 888 - find out if the org has locations. If so, then add people. Else prompt them to add a location first.
	function addNewPerson(orgID) {
		numLocs = doesOrgHaveLocs(orgID);
		if (numLocs > 0) {
			viewOrganisation(orgID,'addNewPerson');
		} else {
			<cfoutput>alert('#phr_Sys_pleaseAddLocationFirst#')</cfoutput>
		}
	}

	function doesOrgHaveLocs(orgID) {
		page = '/WebServices/mxAjaxRelayPLO.cfc?wsdl'
		// NJH 2009-02-17 - added timestamp to force a refresh of the cache.
		timeStamp = new Date();
		parameters  = 'method=init&OrganisationID='+orgID+'&function=countLocsInOrg&timestamp='+timeStamp;

		var myAjax = new Ajax.Request(
								page,
									{method: 'get',
									asynchronous: false,
									evalJSON: 'force',
									parameters: parameters,
									debug:false}
						)
		response = myAjax.transport.responseText.evalJSON();

		return response;
	}
</script>
	</cf_head>

	<cfif trim(message) is not "">
		<cfoutput>
			#application.com.relayUI.message(message=message)#
		</cfoutput>
	</cfif>
	<form method="POST" action="perslist.cfm" name="mainForm" >
		<CF_INPUT type="hidden" name="frmShowInActiveRecords" value="#frmShowInActiveRecords#">
		<cfoutput>
			<div class="row">
				<!--- <div class="col-xs-4">
					<div classs="checkbox">
						<label>
							<CF_INPUT type="CHECKBOX" name="frmShowInActiveRecords" value="#frmShowInActiveRecords#" CHECKED="#frmShowInActiveRecords#" onclick="document.mainForm.submit()">
							phr_sys_ShowInactivePersons
						</label>
					</div>
				</div> --->
				<div class="col-xs-8">
					<!---START: DBB ENT-6--->
					<cfif dataTaskL2>
					<cfinclude template="/data/persondropdown.cfm">
					</cfif>
					<!---END: DBB ENT-6--->
				</div>
			</div>
		</cfoutput>
		<table class="table table-hover table-striped" id="persListTable">
			<tr>
				<th align="CENTER" valign="BOTTOM">
					<cf_input type="checkbox" id="tagAllPeople" name="tagAllPeople" onclick="tagCheckboxes('frmPersonCheck',this.checked)">
				</th>
				<cfif showFirstNameSeparately>
					<th align="Left" valign="BOTTOM">
						<a href="javaScript:reSort('p.FirstName,p.LastName')" class="th_link">
							Phr_Sys_FirstName
						</a>
					</th>
					<!--- 2011-07-20 PPB LEN024 show firtname/lastname Separately --->
					<th align="Left" valign="BOTTOM">
						<a href="javaScript:reSort('p.LastName,p.FirstName')" class="th_link">
							Phr_Sys_LastName
						</a>
					</th>
				<cfelse>
					<cfif frmSortOrder eq "p.LastName,p.FirstName">
						<!--- 2011-07-20 PPB LEN024 putting this toggle in to a large extent makes the setting/code to showFirstNameSeparately superfluous but I'll leave it in as it does save an extra click to sort on firstname --->
						<cfset personSortOrder = "p.FirstName,p.LastName">
					<cfelse>
						<cfset personSortOrder = "p.LastName,p.FirstName">
					</cfif>
					<th align="Left" valign="BOTTOM">
						<a href="javaScript:reSort('<cfoutput>#htmleditformat(personSortOrder)#</cfoutput>')" class="th_link">
							Phr_Sys_Person
						</a>
					</th>
					<!--- 2011-07-20 PPB LEN024 I've switched to main sort on Lastname not Firstname for default (VAB request); NB sort order in settings only operates first time in --->
				</cfif>
				<th align="Left" valign="BOTTOM">
					<a href="javaScript:reSort('p.locationID')" class="th_link"">
						Phr_Sys_Site
					</a>
				</th>
				<th class="alignCenter">
					Phr_Sys_Actions
				</th>
				<th class="alignCenter" colspan="2" width="80" valign="BOTTOM">
					<cfif showLastContactDate>
						<a href="javaScript:reSort('lastContactDate desc')" class="th_link">
							Phr_Sys_LastContact
						</a>
					<cfelse>
						phr_sys_contacts
					</cfif>
					<!--- 2009-01-12 WAB  --->
				</th>
				<th class="alignCenter" valign="BOTTOM">
					<a href="javaScript:reSort('email')" class="th_link"">
						Phr_Sys_Email
					</a>
				</th>
				<cfif LinkedInEnabled>
					<th class="alignCenter">
						phr_sys_Social
					</th>
					<!--- 2012/01/20 P-REL112 linkedin col --->
				</cfif>
				<!---START: DBB ENT-6--->
				<cfif dataTaskL2>
				<th class="alignCenter">
					phr_sys_Edit
				</th>
				</cfif>
				<!---END: DBB ENT-6--->
			</tr>
			<cfset frmpeopleonpage = "">
			<cfset endrow= min(application.showrecords+variables.displaystart-1, getperslist.recordcount)>
			<cfloop query="getPersList" startrow="#Variables.displaystart#" endrow="#Endrow#">
				<cfoutput>
					<!--- gather a list of all the people displayed on this page --->
					<cfset frmpeopleonpage = listappend(frmpeopleonpage,personid)>
					<cfif currentrow mod 2 is not 0>
						<cfset rowclass="oddrow">
					<cfelse>
						<cfset rowclass="evenrow">
					</cfif>
					<tr class="#rowClass#">
						<td align="left" valign="top">
							<CF_INPUT type="CHECKBOX" name="frmPersonCheck" value="#PersonID#" CHECKED="#iif(listfind(frmduppeople,personid) is not 0,true,false)#">
						</td>
						<!--- START: 2011-07-20 PPB LEN024 show firtname/lastname Separately --->
						<cfif showFirstNameSeparately>
							<td valign="top">
								<cfif trim(firstname) is "">
									&nbsp;
								<cfelse>
									<cfoutput>
										<a id="PersonID#PersonID#" href="javaScript:void(viewPerson(#personID#))" title="Phr_Sys_ClickToEdit" class="smallLink">
									</cfoutput>
									#left(Trim(FirstName),12)#
								</cfif>
							</td>
						</cfif>
						<!--- END: 2011-07-20 PPB LEN024 show firtname/lastname Separately --->
						<td valign="top"> <!--- START: 2011-07-20 PPB LEN024 show firtname/lastname Separately --->
							<cfif showFirstNameSeparately>
								<cfif trim(lastname) is "">&nbsp;<cfelse>
									<cfoutput>
										<a id="PersonID#PersonID#" href="javaScript:void(viewPerson(#personID#))" title="Phr_Sys_ClickToEdit" class="smallLink">
									</cfoutput> #left(Trim(LastName),20)#
								</cfif>
								<cfelse>
									<cfif trim(firstname & lastname) is "">&nbsp;<cfelse>
									<cfoutput>
										<a id="PersonID#PersonID#" href="javaScript:void(viewPerson(#personID#))" title="Phr_Sys_ClickToEdit" class="smallLink">
									</cfoutput> #left(Trim(FirstName & " " & LastName),20)# <br />
									</cfif>
							</cfif> <!--- END: 2011-07-20 PPB LEN024 show firtname/lastname Separately --->
							<cfif trim(jobdesc) neq "">#left(jobDesc,20)# <br></cfif> <cfif trim(officephone) is "">#htmleditformat(Trim(telephone))#<cfelse>#htmleditformat(OfficePhone)#</cfif></a> <b class="AttentionFont">#IIF(active is 0,DE("(phr_sys_Inactive)"),DE(""))#</b> <!--- 2011-07-20 PPB LEN024 show if Inactive ---> <!--- 			<CFSET flaggrouptextid="JobFunction"><CFSET thisentity=personid></cfoutput><CFINCLUDE TEMPLATE="../flags/listFlagGroup.cfm"><CFOUTPUT>#result# ---> </td>
						<td valign="top">
							<a href="javaScript:void(viewLocation(#locationID#))" class="smallLink">
								<b>#htmleditformat(left(SiteName,50))#<cfif len(sitename) gt 50>...</cfif></b>
							</a>,

							<cfif trim(address4) is not "">
								#htmleditformat(address4)#,
							</cfif>
							<cfif trim(address5) is not "">
								#htmleditformat(address5)#,
							</cfif>
							<cfif trim(postalcode) is not "">
								#htmleditformat(postalCode)#,
							</cfif>
							<cfif isocode is not "">
								#htmleditformat(ISOCode)#
							</cfif>
						</td>
						<td align="center" valign="top">
							<cfif actions eq 0>
								<a href="javascript:addActionRecord('person',#personid#)" title="Add Action for #left(Trim(FirstName & " " & LastName),20)#">
									<span id="orgListFlag" class="fa fa-flag"></span>
								</a>
								<!--- 	WAB 2008-04-25 changed to JS popup			<a href="../actions/actionQuickAdd.cfm?frmEntityType=person&frmcurrentEntityID=#personID#" target="Detail" title="Add Action for #left(Trim(FirstName & " " & LastName),20)#"><img src="/IMAGES/MISC/IconFlagGrey.gif" border="0"></a><br>  --->
							<cfelse>
								<a href="javascript:void(viewPerson(#personid#,'PersonActions'))" title="Show Actions for #left(Trim(FirstName & " " & LastName),20)#">
									<span id="orgListFlag" class="fa fa-flag"></span>
								</a>
								<!--- 	WAB 2008-04-25 changed to javascript to load entityViewer	<a href="../actions/showEntityActions.cfm?frmEntityType=person&frmcurrentEntityID=#personID#" target="Detail" title="Show Actions for #left(Trim(FirstName & " " & LastName),20)#"><img src="/IMAGES/MISC/IconFlag.gif" border="0"></a> --->
							</cfif>
						</td>
						<td valign="top" align="center">
							<!--- 		WAB 2009-01-12--->
							<cfif showLastContactDate>
								<cfif trim(lastcontactdate) is "">
									Phr_Sys_NoContact
								<cfelse>
									<a href="javaScript:void(viewPerson(#personid#,'personcontactHistory'))" title="Phr_Sys_ViewContactHistoryWith #FirstName# #lastName#" class="smallLink">
										#dateFormat(lastContactDate,'dd-mmm-yy')#
									</a>
								</cfif>
							<cfelse>
								<a href="javaScript:void(viewPerson(#personid#,'personcontactHistory'))" title="Phr_Sys_ViewContactHistoryWith #FirstName# #lastName#" class="smallLink">
									<cfif hasContactHistory is 1>
										<span id="orgListHistory" class="fa fa-history"></span>
									<CFELSE>
										<img src="/images/MISC/transparent.gif" width="16" height="16" border=0>
									</cfif>
								</a>
							</cfif>
						</td>
						<td valign="top" align="center">
							&nbsp;
							<a href="javaScript:addContactRecord(#personID#)" title="Phr_Sys_AddContactRecordFor #Trim(FirstName)# #Trim(LastName)#" class="smallLink">
								<span id="orgListPlus" class="fa fa-plus"></span>
							</a>
						</td>
						<td align="center" valign="top">
							<!--- CR-TND561 1.1.3 NJH 2008-10-08 if the person is inactive, don't allow the sending of an email --->
							<cfif trim(email) is "" or not active>
								&nbsp;
							<cfelse>
								<cfset nemail=replace(email,"'","\'","All")>
								<a href="javaScript:quickMail(#personID#, #locationID#, '#nEmail#')" title="Phr_Sys_ClickToSendEmailTo #FirstName# #LastName#">
									<span id="orgListEnvelope" class="fa fa-envelope"></span>
								</a>
							</cfif>

							<cfif trim(mobilePhone) is "">
								&nbsp;
							<cfelseif isNumeric(trim(mobilephone)) and request.relayCurrentUser.organisation.organisationID eq 1>
								<!--- 2014-08-19		RPW			Remove SMS link from Dashboard --->
							</cfif>
						</td>
						<cfif LinkedInEnabled>
							<td valign="top" align="center">
								<!--- <script type=IN/MemberProfile data-format="hover" data-id="http://www.linkedin.com/pub/nathaniel-hogeboom/40/b00/493"></script> --->
								<cfif linkedInProfileURL neq "">
									<script type=IN/MemberProfile data-format="click" data-id="#linkedInProfileURL#" data-related="false"></script>
									<!--- 2012/01/12 PPB P-REL112 Connect to LinkedIn --->
								<cfelse>
									<span id="orgListlinkedinDisabled" class="fa fa-linkedin"></span>
								</cfif>
							</td>
						</cfif>
						<!---START: DBB ENT-6--->
						<cfif dataTaskL2>
						<td valign="top" align="center" nowrap>
							<a href="javaScript:void(viewPerson(#personID#,'perProfileSummary'));" title="Phr_Sys_EditProfileDataFor #FirstName# #lastName#" class="smallLink">
								<span id="orgListPencil" class="fa fa-pencil"></span>
							</a>
						</td>
						</cfif>
						<!---END: DBB ENT-6--->
					</tr>
				</cfoutput>
			</cfloop>
			<cfif isdefined("totalPeopleInOrg") and totalpeopleinorg gt getperslist.recordcount>
				<tr>
					<td colspan="10">
						<cfoutput>
							<a href="perslist.cfm?frmorganisationID=#frmorganisationID#" title="Show more records for this organisation" class="smallLink">
								More&nbsp;
								<img src="/images/misc/tinyArrow.gif" alt="Arrow" border="0">
							</a>
						</cfoutput>
					</td>
				</tr>
			</cfif>
		</table>
		<cfif getperslist.recordcount gt application.showrecords>
			<!--- if we only show a subset, then give next/previous arrows --->
			<p>
			<table border=0>
				<tr>
					<td>
						<cfif variables.displaystart gt 1>
							<input type="Image" name="PersonBack" src="/images/buttons/prev.png" BORDER=0 ALT="">
						</cfif>
					</td>
					<td>
						<cfif (variables.displaystart + application.showrecords - 1) lt getperslist.recordcount>
							<cfoutput>
								Record(s) #htmleditformat(Variables.displaystart)# - #Evaluate("Variables.displaystart + application.showRecords-1")#
							</cfoutput>
						<cfelse>
							<cfoutput>
								Record(s) #htmleditformat(Variables.displaystart)# - #getPersList.RecordCount#
							</cfoutput>
						</cfif>
					</td>
					<td>
						<cfif (variables.displaystart + application.showrecords - 1) lt getperslist.recordcount>
							<input type="Image" name="PersonForward" src="/images/buttons/next.png" BORDER=0 ALT="Next">
						</cfif>
					</td>
				</tr>
			</table>
		</cfif>
		<cfif getperslist.recordcount is 0 and getOrgDetails.recordcount NEQ 0>
			<cfoutput>
				<table width="100%" border="0" cellspacing="1" cellpadding="3" align="center" bgcolor="White" class="withBorderSidesOnly">
					<tr>
						<td>
							<p>
								<cfif frmShowInActiveRecords >Phr_Sys_ThereAreNoInactivePeopleRecordsFor<cfelse>Phr_Sys_ThereAreNoPeopleRecordsFor</cfif> #htmleditformat(Trim(getOrgDetails.organisationName))# Phr_Sys_InTheDatabase.
								<!--- <br>
								<br>
								<a href="JavaScript:void(addNewPerson(#frmOrganisationID#));" class="Submenu">
									Phr_Sys_AddNewPerson
								</a> --->
							</p>
						</td>
					</tr>
					<!--- NJH 2008-11-14 T-10 Bug Fix Issue 888 - doing an Ajax call to determine whether we can add people or not.
						<cfif orgnumlocs gt 0>
						Phr_Sys_Click <a href="JavaScript:newPerson();" class="smallLink">Phr_Sys_AddNewPerson</a> Phr_Sys_ToAddPeople.
					<cfelse>
						Phr_Sys_Click <a href="JavaScript:alert('phr_Sys_pleaseAddLocationFirst');" class="smallLink">Phr_Sys_AddNewPerson</a> Phr_Sys_ToAddPeople.</p></td></tr>
						</cfif>--->
				</table>
			</cfoutput>
			<cfparam name="frmMoveFreeze" default=#createodbcdatetime(now())#>
			<CF_INPUT type="HIDDEN" name="frmMoveFreeze" value="#frmMoveFreeze#">
			<cfinclude template="searchparamform.cfm">
			<CF_INPUT type="HIDDEN" name="frmLocationID" value="#frmLocationID#">
			<CF_INPUT type="HIDDEN" name="frmStart" value="#frmStart#">
			<CF_INPUT type="HIDDEN" name="frmStartp" value="#Variables.displaystart#">
			<CF_INPUT type="HIDDEN" name="frmBack" value="#Variables.frmBack#">
			<input type="HIDDEN" name="frmPersonID" value="0">
			<input type="HIDDEN" name="frmTask" value="edit">
			<CF_INPUT type="HIDDEN" name="frmDupLocs" value="#frmDupLocs#">
			<CF_INPUT type="HIDDEN" name="frmDupPeople" value="#frmDupPeople#">
			<CF_INPUT type="HIDDEN" name="frmPeopleOnPage" value="#frmPeopleOnPage#">
			<input type="HIDDEN" name="frmDupPeopleCheck" value="0">
			<CF_INPUT type="HIDDEN" name="frmOrganisationID" value="#frmOrganisationID#">
		<cfelse>
			<cfparam name="frmMoveFreeze" default=#createodbcdatetime(now())#>
			<CF_INPUT type="HIDDEN" name="frmMoveFreeze" value="#frmMoveFreeze#">
			<cfinclude template="searchparamform.cfm">
			<CF_INPUT type="HIDDEN" name="frmLocationID" value="#frmLocationID#">
			<CF_INPUT type="HIDDEN" name="frmStart" value="#frmStart#">
			<CF_INPUT type="HIDDEN" name="frmStartp" value="#Variables.displaystart#">
			<CF_INPUT type="HIDDEN" name="frmBack" value="#Variables.frmBack#">
			<input type="HIDDEN" name="frmPersonID" value="0">
			<input type="HIDDEN" name="frmTask" value="edit">
			<CF_INPUT type="HIDDEN" name="frmDupLocs" value="#frmDupLocs#">
			<CF_INPUT type="HIDDEN" name="frmDupPeople" value="#frmDupPeople#">
			<input type="HIDDEN" name="frmDupPeopleCheck" value="0">
			<CF_INPUT type="HIDDEN" name="frmOrganisationID" value="#frmOrganisationID#">
		</cfif>
		<CF_INPUT type="HIDDEN" name="frmPeopleOnPage" value="#frmPeopleOnPage#">
		<CF_INPUT type="HIDDEN" name="frmPeopleInReport" value="#valuelist(getPersList.personid)#">
		<CF_INPUT type="HIDDEN" name="frmSortOrder" value="#frmSortOrder#">
		<!--- 2011-10-26 RMB LID7549 Missing Hidden value to pass sort option --->
		<!---  --->
	</form>
	<form method="POST" action="locdetail.cfm" name="LocForm">
		<CF_INPUT type="HIDDEN" name="frmMoveFreeze" value="#frmMoveFreeze#">
		<cfinclude template="searchparamform.cfm">
		<CF_INPUT type="HIDDEN" name="frmLocationID" value="#frmLocationID#">
		<CF_INPUT type="HIDDEN" name="frmNext" value="#Variables.frmNext#">
		<CF_INPUT type="HIDDEN" name="frmBack" value="#Variables.frmBack#">
		<CF_INPUT type="HIDDEN" name="frmStart" value="#frmStart#">
		<CF_INPUT type="HIDDEN" name="frmStartp" value="#Variables.displaystart#">
		<input type="HIDDEN" name="frmTask" value="edit">
		<CF_INPUT type="HIDDEN" name="frmDupLocs" value="#frmDupLocs#">
		<CF_INPUT type="HIDDEN" name="frmDupPeople" value="#frmDupPeople#">
		<CF_INPUT type="HIDDEN" name="frmPeopleOnPage" value="#frmPeopleOnPage#">
		<input type="HIDDEN" name="frmDupPeopleCheck" value="0">
	</form>
	<form method="POST" action="PersonDedupe.cfm" name="DedupeForm">
		<CF_INPUT type="HIDDEN" name="frmMoveFreeze" value="#frmMoveFreeze#">
		<cfinclude template="searchparamform.cfm">
		<CF_INPUT type="HIDDEN" name="frmLocationID" value="#frmLocationID#">
		<CF_INPUT type="HIDDEN" name="frmStart" value="#frmStart#">
		<CF_INPUT type="HIDDEN" name="frmStartp" value="#Variables.displaystart#">
		<CF_INPUT type="HIDDEN" name="frmBack" value="#Variables.frmBack#">
		<CF_INPUT type="HIDDEN" name="frmDupLocs" value="#frmDupLocs#">
		<CF_INPUT type="HIDDEN" name="frmDupPeople" value="#frmDupPeople#">
		<CF_INPUT type="HIDDEN" name="frmPeopleOnPage" value="#frmPeopleOnPage#">
		<input type="HIDDEN" name="frmDupPeopleCheck" value="0">
	</form>
	<!--- This form is for moving people to different locations and Organisations --->
	<form method="POST" action="datasearch.cfm" name="PersonMoveForm">
		<CF_INPUT type="HIDDEN" name="frmMoveFreeze" value="#frmMoveFreeze#">
		<cfinclude template="searchparamform.cfm">
		<CF_INPUT type="HIDDEN" name="frmLocationID" value="#frmLocationID#">
		<CF_INPUT type="HIDDEN" name="frmStart" value="#frmStart#">
		<CF_INPUT type="HIDDEN" name="frmStartp" value="#Variables.displaystart#">
		<CF_INPUT type="HIDDEN" name="frmBack" value="#Variables.frmBack#">
		<CF_INPUT type="HIDDEN" name="frmDupPeople" value="#frmDupPeople#">
		<CF_INPUT type="HIDDEN" name="frmPeopleOnPage" value="#frmPeopleOnPage#">
		<input type="HIDDEN" name="frmDupPeopleCheck" value="0">
	</form>
	<cfoutput>
		<form action="/phoning/addCallRecord.cfm" method="post" name="addContactHistoryForm" target="Detail">
			<input type="hidden" name="frmcurrentEntityID" value="0">
			<input type="hidden" name="frmEntityType" value="person">
		</form>
		<form action="quickMailClient.cfm" method="post" name="mailClient" target="Detail">
			<input type="hidden" name="frmPersonID" value="0">
			<input type="hidden" name="frmLocationID" value="0">
			<input type="hidden" name="frmMailTo" value="0">
		</form>
		<form action="persList.cfm" method="post" name="reSort">
			<CF_INPUT type="HIDDEN" name="frmSortOrder" value="#frmSortOrder#">
			<!--- WAB 2009-01-12 --->
			<CF_INPUT type="HIDDEN" name="frmOrganisationID" value="#frmOrganisationID#">
			<CF_INPUT type="CHECKBOX" name="frmShowInActiveRecords" value="#frmShowInActiveRecords#" style="display:none;">
			<!--- 2011-07-20 PPB LEN024 this is a fudge to move the current setting of frmShowInActiveRecords to the reSort form so that it exists in the form scope when the form is submitted when sorting; I use CHECKBOX (and hide it) so that isDefined() works as the toggle --->
		</form>
	</cfoutput>
	<cfset frmnexttarget = "Detail">
	<cfinclude template="_editMainEntities.cfm">
	<cfinclude template="_entityDetailForms.cfm">
	<cfset application.com.globalFunctions.cfcookie(name="PFLAGSCHECKED", value="no-0")>
	<SCRIPT type="text/javascript">
<!--
	//2011-06-03 PPB REL106 added selectedRowColor
	function viewPerson(personID, screenID) {
		isOK = viewEntity (0,personID,screenID,
			function (isOK) {
				if (isOK) {
					var anchorArray = document.getElementsByTagName('a');
					for (var i=0;i<anchorArray.length;i++) {
						anchorArray[i].style.fontWeight = '';
						anchorArray[i].style.color = '';
					}
					// Set the required one to bold.
					document.getElementById("PersonID"+personID).style.fontWeight = 'bold';
					document.getElementById("PersonID"+personID).style.color = '#<cfoutput>#selectedRowColor#</cfoutput>';
				}
			}
		)
	}
//-->
</SCRIPT>
</div>
