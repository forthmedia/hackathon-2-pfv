<!--- �Relayware. All Rights Reserved 2014 --->
<!---

Mods

2005-09-26		WAb 	altered search on selections to handle status 1 or 2  [2 being manually added people]
2006-03-02		SWJ		added the param field to param the name of the view to use and the order by
2007/03/07		NJH 	filter records to show deleted orgs only if request.showDeletedOrgs is set to true
2007-04-17		AJC		filter record by organisation type
2007-05-25		AJC		Fixed issue with adding new person from a basic search of an organisation
2007/10/03		WAB/NH	took query building out into plo.cfc and combined with query in orglistdrilldownv2
2008/04/10		WAB 	saving of search criteria modified to use results of createDataSearchQueryFromFormVariables
2008/05/30		NJH		Increase num of records returned if selection is part of search criteria
2012/01/12 		PPB 	P-REL112 connect to linkedIn
2016/06/09		NJH		JIRA PROD2016-342 - change getFirstPerson query to return eithe first location or person. Changed the name to getFirstEntity. Removed old commented-out code.
--->



<CFSET displaystart = 1>
<CFSET frmLocsOnPage = "">
<cfset mergeOK = 0>
<CFPARAM NAME="frmAccountMngrID" DEFAULT="0">
<CFPARAM NAME="commid" TYPE="numeric" DEFAULT="0">
<CFPARAM NAME="frmFlagIDfromFlagCount" DEFAULT="0">
<CFPARAM NAME="frmEntityType" DEFAULT="0">
<CFPARAM NAME="frmdataTableFullName" DEFAULT="0">
<cfparam name="frmNextTarget" default="mainSub">
<cfparam name="frmNextTarget" default="mainSub">
<cfparam name="showFlags " default="0">
<cfparam name="frmNext" default="">
<cfparam name="frmOrganisationTypeID" default="0">
<cfparam name="maxNumOrgRecords" type="numeric" default="500">  <!--- NJH 2008/05/30 --->

<!--- NJH 2008/05/30 Bug Fix All Sites 34.. Increase the num of records returned to a value set in the dataINI if we're running a selection
	Otherwise, set the num of records returned to 500--->
<CFIF (not isDefined("frmSelectionID")) or (isDefined("frmSelectionID") and frmSelectionID IS 0)>
	<cfset maxNumOrgRecords = 500>
</CFIF>


<!--- 2007-05-25 AJC Fixed issue with adding new person from a basic search of an organisation --->
<cfif frmOrganisationTypeID is "">
	<cfset frmOrganisationTypeID = 0>
</cfif>

<!--- 2006-03-02 SWJ added flexibility to use different views and default sort orders --->
<cfparam name="orgViewToUse" default="vOrgList3">
<cfparam name="defaultOrderBy" default="sortKey, OrganisationName">
<cfif orgViewToUse neq "vOrgList3">
	<cfset defaultOrderBy = "field1, OrganisationName">
</cfif>

<CFINCLUDE template = "searchparamdefaults.cfm"	>

<CFPARAM NAME="frmFlagIDs" DEFAULT="0">
<CFPARAM NAME="frmMoveFreeze" DEFAULT=#CreateODBCDateTime(Now())#>

<!--- check to see if any criteria exists --->
<CFPARAM NAME="AnyCriteria" DEFAULT="no">

<cfif (isDefined("runQuery") AND runQuery) OR (not structKeyExists(url,"fromFrame")) OR (not structKeyExists(session,"qryGetOrganisations"))>


	<cfset querydetail = application.com.relayplo.createDataSearchQueryFromFormVariables()>

	<cfset LinkedInEnabled = application.com.settings.getSetting("socialMedia.enableSocialMedia") and (ListFindNoCase(application.com.settings.getSetting("socialMedia.services"),'LinkedIn') gt 0)>	<!--- 2012/01/12 PPB P-REL112 connect to linkedIn --->

	<!--- #####################################################################################
			Main Query
	#######################################################################################--->
	<!--- GCC 2005/03/04 - restricted to 500 - server was crashing for large result sets - may need a more refined solution however --->
	<CFQUERY NAME="getOrganisations" datasource="#application.siteDataSource#">
			SELECT distinct TOP #maxNumOrgRecords# o.*,
				organisationName+'-'+cast(o.organisationID as varchar(150)) as sortKey,
			 	(select count(actionID) from actions a with (noLock)
	     			WHERE a.entityID = o.organisationID and a.actionStatusID <>3) as actions
				<cfif LinkedInEnabled>					<!--- 2012/01/12 PPB P-REL112 connect to linkedIn --->
					,se_li.serviceEntityID AS LinkedInOrgID
				</cfif>
				,<cfif queryDetail.hasPersonCriteria>p.personid<cfelseif queryDetail.hasLocationCriteria>l.locationID<cfelse>o.organisationID</cfif> as entityID
			 from #orgViewToUse# o with (noLock)
				#preserveSingleQuotes(queryDetail.additionaljoins_sql)#
				<cfif LinkedInEnabled>						<!--- 2012/01/12 PPB P-REL112 connect to linkedIn --->
					LEFT OUTER JOIN (dbo.serviceEntity se_li with (noLock) INNER JOIN dbo.service s_li ON s_li.serviceID = se_li.serviceID AND s_li.name='LinkedIn') ON se_li.entityTypeID=2 AND se_li.entityID = o.organisationID
				</cfif>

		<!--- 2012-07-18 PPB P-SMA001 		WHERE O.CountryID IN (#request.relayCurrentUser.countryList#) --->
		where (1=1 #application.com.rights.getRightsFilterWhereClause(entityType="organisation",alias="o").whereClause#)	<!--- 2012-07-18 PPB P-SMA001 --->
			<!--- NJH 2009/06/22 Bug Fix Security Project Issue 2236 - If query is bad for any reason, don't bring back any records --->
			<cfif not querydetail.ok>
				and 1=0
			</cfif>
			#preserveSingleQuotes(queryDetail.whereStatement_sql)#
			ORDER BY <cf_queryObjectName value="#defaultOrderBy#">
		</CFQUERY>

	<!---    WAB 2008/01 - if there are person criteria then get the first person so that can be loaded up in the bottom frame, slight issue with what ordering should be used
		JIRA PROD2016-342 NJH 2016/06/09 - changed query below to be qoq. Get first record regardless of criteria.
	--->
		<CFQUERY NAME="qryGetFirstEntity" dbtype="query" maxrows="1">
		select entityID,
			<cfif queryDetail.hasPersonCriteria>#application.entityTypeID.person#<cfelseif queryDetail.hasLocationCriteria>#application.entityTypeID.location#<cfelse>#application.entityTypeID.organisation#</cfif> as entityTypeID
		from getOrganisations
		WHERE
		organisationid = <cf_queryparam value="#getOrganisations.recordCount?getOrganisations.organisationid:0#" CFSQLTYPE="CF_SQL_INTEGER" >
		</CFQUERY>

	<cfset orgColList = listDeleteAt(getOrganisations.columnList,listFindNoCase(getOrganisations.columnList,"entityID"))>
	<CFQUERY NAME="getOrganisations" dbtype="query">
		select distinct #orgColList# from getOrganisations
	</CFQUERY>

	<cflock timeout="60" scope="session" type="readonly">
		<cfset session.qryGetOrganisations = getOrganisations>
	</cflock>

</cfif>

<CFIF frmCountryID IS NOT 0>
	<CFQUERY NAME="getCountryName" datasource="#application.siteDataSource#">
		SELECT CountryDescription
		FROM Country
		WHERE CountryID in (<cf_queryparam value="#frmCountryID#" CFSQLTYPE="CF_SQL_INTEGER" list="true">)
	</CFQUERY>
</CFIF>