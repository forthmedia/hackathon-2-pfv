<!--- �Relayware. All Rights Reserved 2014 --->
<!--- user rights to access:
		SecurityTask:dataTask
		Permission: Update
---->

<!--- wab 2000-03-02 removed archiving - done by mod register --->

<CFIF NOT checkPermission.UpdateOkay GT 0>
	<!--- if frmLocationID is 0 then task is to update
	      but the user doesn't have update permissions --->

	<CFSET message = "You are not authorised to use this function">
	<CFINCLUDE TEMPLATE="datamenu.cfm">
	<CF_ABORT>

</CFIF>

<CFSET good = "">
<CFSET bad = "">
<CFLOOP LIST=#frmMovePeople# INDEX="idxpersonID"><CFSET ok = 0>
	<CFIF idxpersonID IS NOT 0>
		<!--- Check that record has not changed --->
		<CFQUERY NAME="getLocOrgID" DATASOURCE=#application.SiteDataSource#>
		SELECT p.LastUpdated, l.OrganisationID, p.LocationID AS OldLocID
		  FROM Person AS p, Location AS l
		 WHERE p.PersonID =  <cf_queryparam value="#idxpersonID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		   AND l.LocationID =  <cf_queryparam value="#frmLocationID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</CFQUERY>

		<CFIF getLocOrgID.RecordCount IS 1>
			<CFIF getLocOrgID.LastUpdated IS frmMoveFreeze>
				<CFTRANSACTION>
<!--- 		not required, handled by mod register
			<!--- Archive the Person record --->
					<CFQUERY NAME="ArchivePerson" DATASOURCE=#application.SiteDataSource#>
					INSERT INTO xPerson
						(PersonID, Salutation, FirstName, LastName, Username, Password, PasswordDate, LoginExpires, FirstTimeUser, HomePhone, OfficePhone, MobilePhone, FaxPhone, Email, JobDesc, Language, Notes, LocationID, OrganisationID, Active, DupeChuck, DupeGroup, CreatedBy, Created, LastUpdatedBy, LastUpdated, ArchivedBy, Archived)
						SELECT p.PersonID, p.Salutation, p.FirstName, p.LastName, p.Username, p.Password, p.PasswordDate, p.LoginExpires, p.FirstTimeUser, p.HomePhone, p.OfficePhone, p.MobilePhone, p.FaxPhone, p.Email, p.JobDesc, p.Language, p.Notes, p.LocationID, p.OrganisationID, p.Active, p.DupeChuck, p.DupeGroup, p.CreatedBy, p.Created, p.LastUpdatedBy, p.LastUpdated, #request.relayCurrentUser.usergroupid#, #Now()#
						  FROM Person AS p
						 WHERE p.PersonID = #idxpersonID#
					</CFQUERY>
 --->
 					<!--- Update the LastUpdated(by) fields and Loc/org ID for the person --->
					<CFQUERY NAME="UpdatePerson" DATASOURCE=#application.SiteDataSource#>
					UPDATE Person
					SET LocationID =  <cf_queryparam value="#frmLocationID#" CFSQLTYPE="CF_SQL_INTEGER" > ,
						OrganisationID =  <cf_queryparam value="#getLocOrgID.OrganisationID#" CFSQLTYPE="CF_SQL_INTEGER" > ,
						LastUpdatedBy = #request.relayCurrentUser.usergroupid#,
						LastUpdated =  <cf_queryparam value="#Now()#" CFSQLTYPE="CF_SQL_TIMESTAMP" >  <!--- Put pound signs around the Now() and on line 35 --->
					 WHERE PersonID =  <cf_queryparam value="#idxpersonID#" CFSQLTYPE="CF_SQL_INTEGER" > 
					</CFQUERY>
					<!--- If the record has NOT changed put ID in good list --->
					<CFSET good = good & idxpersonID & ",">
					<CFSET ok = 1>
				</CFTRANSACTION>
			</CFIF>
		</CFIF>
		<!--- If the record has changed put ID in bad list --->
		<CFIF ok IS 0>
			<CFSET bad = bad & idxPersonID & ",">
		</CFIF>
	</CFIF>
</CFLOOP>

<CFIF bad IS NOT ""><!--- Strip of last comma --->
	<CFSET bad = Left(bad, Len(bad)-1)>
	<CFSET bad = Replace(bad, ",", ", ", "ALL")>
</CFIF>

<CFIF good IS NOT ""><!--- Strip of last comma --->
	<CFSET good = Left(good, Len(good)-1)>
	<CFSET good = Replace(good, ",", ", ", "ALL")>
</CFIF>

<CFINCLUDE TEMPLATE="movesum.cfm">
<CF_ABORT>
