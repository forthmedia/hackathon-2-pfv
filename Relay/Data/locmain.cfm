<!--- �Relayware. All Rights Reserved 2014 --->
<!---


Amendment History
Version	Date		By		Description
		1999-04-16 	WAB 	Added job function to list and direct/indirect to header
		1999-04-14		WAB 	Added fields required for more searches
		1999-04-13		WAB 	Added fields required for search by first and last name
2		01 Jul 00 	SWJ		Added ShowLocDetails (line 14) param which turns of the loc details stuff
2.10	2001-02-28 	WAB 	altered lcoation query to user left outer joins
2.11	2001-02-03		WAB 	altered where the person move form points to
2.12	2001-07-17		WAB		altered to use persondropdown (which has all the new flag things in it), new savechecks('entity') code used
2.13	2001-07-17	SWJ		altered look and feel and added images to make it more user friendly
		2008-07-15	NYF 	altered Contact History link to use viewPerson function
--->

<!--- redirect back to loclist if from Forward/Back buttons --->
<CFIF IsDefined("ListForward.x") OR IsDefined("ListBack.x")>
	<CFINCLUDE TEMPLATE="loclist.cfm">
	<CF_ABORT>
</CFIF>

<!--- This controls whether to show loc details or not --->
<CFPARAM NAME="ShowLocDetails" DEFAULT="0">

<CFSET ShowRecords =100>  <!--- overrides application .cfm setting --->
<CFSET displaystart = 1>
<CFSET frmBack = "locmain"> <!--- where to go for a "return" --->
<CFSET frmNext = "locmain"> <!--- next page to show --->

<CFPARAM NAME="message" DEFAULT="">

<CFIF NOT checkPermission.Level1 GT 0>
	<CFSET message="You are not authorised to use this function">
		<CFINCLUDE TEMPLATE="datamenu.cfm">
	<CF_ABORT>
</CFIF>

<!--- the application .cfm checks for read dataTask privleges
--->
<!--- 2012-07-25 PPB P-SMA001 commented out
<cfinclude template="/templates/qrygetcountries.cfm">
 --->

<!--- all of these should really be coming in as URL. OR FORM.
		but we can fool the system so we don't need to check
		for IsDefined

		These are stored so that we can go back to the orginal query
		--->

<CFINCLUDE template="searchparamdefaults.cfm">

<CFPARAM NAME="Partners" DEFAULT="">
<CFPARAM NAME="frmStart" DEFAULT="1">
<CFPARAM NAME="frmStartp" DEFAULT="1">
<CFPARAM NAME="frmPeopleOnPage" DEFAULT="0">
<!--- get the text relating to Partner Type if the frm Variable is not 0
		NB there can be many partner types
--->
<CFIF frmFlagIDs IS NOT 0>

	<cfquery name="getPartnerType" datasource="#application.SiteDataSource#">
		SELECT Name AS ItemText FROM Flag
		WHERE FlagID  IN ( <cf_queryparam value="#frmFlagIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		ORDER BY Name
	</cfquery>

<!--- 	<CFQUERY NAME="getPartnerType" DATASOURCE=#application.SiteDataSource#>
		SELECT ItemText FROM LookupList
		WHERE LookupID IN (#frmFlagIDs#)
		ORDER BY ItemText
	</CFQUERY> --->


<CFIF getPartnerType.RecordCount IS NOT 0>
		<!--- put a space after each comma in the list --->
		<CFSET Partners = Replace(ValueList(getPartnerType.ItemText),",",", ","ALL")>
	<CFELSE>
		<CFSET Partners = "#frmFlagIDs# [description unavailable]">
	</CFIF>
</CFIF>

<CFIF frmCountryID IS NOT 0>
	<CFQUERY NAME="getCountryName" DATASOURCE=#application.SiteDataSource#>
		SELECT CountryDescription
		FROM Country
		WHERE CountryID =  <cf_queryparam value="#frmCountryID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</CFQUERY>
</CFIF>

<CFIF frmSelectionID IS NOT 0>
	<CFQUERY NAME="getSelectionTitle" DATASOURCE=#application.SiteDataSource#>
		SELECT Title
		FROM Selection
		WHERE CreatedBy = #request.relayCurrentUser.usergroupid#
		AND SelectionID =  <cf_queryparam value="#frmSelectionID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</CFQUERY>
</CFIF>


<!--- check to see if any criteria exists --->
<CFPARAM NAME="AnyCriteria" DEFAULT="no">
<CFPARAM NAME="frmLocationID" DEFAULT="0"> <!--- is really a URL.variable --->

<!--- Test to see if we should show location details the screen loads a lot faster if not --->
<CFIF ShowLocdetails IS "1">
	<CFQUERY NAME="getLocations" DATASOURCE=#application.SiteDataSource#>
		SELECT l.LocationID,
			   l.SiteName,
			   l.PostalCode,
			   l.Telephone,
			   l.Fax,
			   l.LastUpdated,
			   l.Address1,
			   l.Address2,
			   l.Address3,
			   l.Address4,
			   l.Address5,
			   l.PostalCode,
	   		   l.Direct,
			   ug.Name AS LastUpdatedBy,
			   c.CountryDescription,
			   c.ISOCode,
			   {fn CONCAT(d.Description,{fn CONCAT('-',ld.RemoteDataSourceID)})} AS DataSource
		FROM Location AS l
				inner join
			Country AS c on c.CountryID = l.CountryID
				left outer join
			LocationDataSource AS ld on l.LocationID = ld.locationID
				left outer join
			DataSource AS d on ld.DataSourceID = d.DataSourceID
				left outer join
		     UserGroup AS ug on l.LastUpdatedBy = ug.UserGroupID
		<!--- 2012-07-25 PPB P-SMA001 commented out
		WHERE c.CountryID  IN ( <cf_queryparam value="#Variables.CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		 --->
		WHERE 1=1 #application.com.rights.getRightsFilterWhereClause(entityType="location",alias="l").whereClause# 	<!--- 2012-07-25 PPB P-SMA001 --->
	  	AND l.LocatioNID =  <cf_queryparam value="#frmLocationID#" CFSQLTYPE="CF_SQL_INTEGER" >
		ORDER BY d.Description, ld.RemoteDataSourceID
	</CFQUERY>


	<CFQUERY NAME="getLocflags" DATASOURCE=#application.SiteDataSource#>
	SELECT DISTINCT
			fgb.Name AS parent,
			f.Name AS ItemText,
			l.SiteName
	      FROM Flag AS f, FlagGroup AS fga, FlagGroup AS fgb , Location AS l, BooleanFlagData AS bfd
			WHERE f.Active <> 0
			AND fga.Expiry > #CreateODBCDateTime(Now())#
			AND fgb.Expiry > #CreateODBCDateTime(Now())#
			AND fga.ParentFlagGroupID = fgb.FlagGroupID
			AND fga.Active <> 0
		   	AND fgb.Name = 'Partner Type'
		   	AND fgb.Active <> 0
		   	AND f.FlagGroupID = fga.FlagGroupID
			AND (fga.FlagTypeID = 2 OR fga.FlagTypeID = 3)
			AND bfd.FlagID = f.FlagID
			AND bfd.EntityID = l.LocationID
			AND l.LocationID =  <cf_queryparam value="#frmLocationID#" CFSQLTYPE="CF_SQL_INTEGER" >
	ORDER BY SiteName
	</CFQUERY>

<CFELSE>
	<CFQUERY NAME="getLocations" DATASOURCE=#application.SiteDataSource#>
		SELECT l.LocationID,
			   l.SiteName
		FROM Location AS l
		WHERE  l.LocatioNID =  <cf_queryparam value="#frmLocationID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</CFQUERY>
</CFIF>
<!--- checking for a record count ensures country-level security
      on person --->

<CFQUERY NAME="getPeople" DATASOURCE=#application.SiteDataSource#>
	SELECT p.PersonID,
		  p.Salutation,
		  p.FirstName,
		  p.LastName,
		  p.OfficePhone,
		  p.Email,
		  p.LastUpdated,
		  ug.Name AS LastUpdatedBy,
		  (select max(dateSent) from commdetail WHERE personID = p.personid) as lastContactDate
	FROM Person AS p
				left outer join
	     UserGroup AS ug on p.LastUpdatedBy = ug.UserGroupID
	WHERE p.LocatioNID =  <cf_queryparam value="#frmLocationID#" CFSQLTYPE="CF_SQL_INTEGER" >
	ORDER BY p.LastName, p.FirstName
</CFQUERY>





<CFIF getPeople.RecordCount GT application.showRecords>
	<!--- showrecords in application .cfm
			defines the number of records to display --->

	<!--- define the start record --->
 	<CFIF IsDefined("FORM.PersonForward.x")>
		 <CFSET displaystart = frmStartp + application.showRecords>
 	<CFELSEIF IsDefined("FORM.PersonBack.x")>
		 <CFSET displaystart = frmStartp - application.showRecords>
	<CFELSEIF IsDefined("frmStartp")>
	 	 <CFSET displaystart = frmStartp>
	</CFIF>

</CFIF>





<cf_head>
<cf_title><cfoutput>#request.CurrentSite.Title#</cfoutput></cf_title>
	<cf_includejavascriptonce template = "/javascript/viewEntity.js">
	<cf_includejavascriptonce template = "/javascript/dropdownFunctions.js">
	<cf_includejavascriptonce template = "/javascript/checkBoxFunctions.js">
	<cf_includejavascriptonce template="/javascript/openWin.js">
<SCRIPT type="text/javascript">
<!--
	function editPerson(pid) {
		var form = document.mainForm;
		form.frmPersonID.value = pid;
		form.submit();

	}
	function newPerson(){
		var FormName = "mainForm";
		var form = eval('document.' + FormName);

		form.frmPersonID.value=0;
		//form.frmTask.value='newp';
		//form.frmNext.value='locmain';

		doForm(FormName);
	}
	function backToList(){
		var FormName = "LocForm";
		var form = eval('document.' + FormName);

		form.frmTask.value='back';
		form.frmNext.value='loclist';

		doForm(FormName);
	}

	function doForm(FormName) {
		var form = eval('document.' + FormName);
		form.frmDupPeopleCheck.value = saveChecks('Person');
		form.submit();
	}


<!---
nolonger needed, using .js
	function saveChecks() {
		// create a string of all the checked locations
		var form = document.mainForm;
		var checkedItems = "0";
		for (j=0; j<form.elements.length; j++){
			thiselement = form.elements[j];
			if (thiselement.name == "frmPersonCheck") {
				if (thiselement.checked) {
					if (checkedItems == "0") {
						checkedItems = thiselement.value;
					} else {
						checkedItems += "," + thiselement.value;
					}
				}
			}
		}
		return checkedItems;
	}
 --->
//-->
</SCRIPT>

</cf_head>

<CFOUTPUT>
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
	<TR>
		<TD ALIGN="LEFT" CLASS="submenu" VALIGN="top">
			People based at #htmleditformat(Trim(getLocations.SiteName))#
		</TD>
		<TD ALIGN="RIGHT" VALIGN="top" CLASS="submenu">
			<CFIF getPeople.RecordCount IS NOT 0>
				<A HREF="JavaScript:doForm('DedupeForm');" Class="Submenu">Merge duplicate records</A>
			</CFIF>
		</TD>
	</TR>
</TABLE>
</CFOUTPUT>



<CENTER>

<CFIF Trim(message) IS NOT "">
<P><CFOUTPUT>#application.com.security.sanitiseHTML(message)#</CFOUTPUT>
</CFIF>


<FORM METHOD="POST" ACTION="PersonDetail.cfm" NAME="mainForm">
<CFIF getPeople.RecordCount IS 0>

	<P>No people were found at the above location.
	<CFPARAM NAME="frmMoveFreeze" DEFAULT=#CreateODBCDateTime(Now())#>
	<CF_INPUT TYPE="HIDDEN" NAME="frmMoveFreeze" VALUE="#frmMoveFreeze#">

	<CF_INPUT TYPE="HIDDEN" NAME="frmDupLocs" VALUE="#frmDupLocs#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmDupPeople" VALUE="#frmDupPeople#">

	<CFINCLUDE template="searchparamform.cfm">

	<CF_INPUT TYPE="HIDDEN" NAME="frmLocationID" VALUE="#frmLocationID#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmStart" VALUE="#frmStart#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmStartp" VALUE="#Variables.displaystart#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmBack" VALUE="#Variables.frmBack#">

	<INPUT TYPE="HIDDEN" NAME="frmPersonID" VALUE="0">
			<INPUT TYPE="HIDDEN" NAME="frmTask" VALUE="edit">

			<CF_INPUT TYPE="HIDDEN" NAME="frmDupLocs" VALUE="#frmDupLocs#">
			<CF_INPUT TYPE="HIDDEN" NAME="frmDupPeople" VALUE="#frmDupPeople#">
			<CF_INPUT TYPE="HIDDEN" NAME="frmPeopleOnPage" VALUE="#frmPeopleOnPage#">
			<INPUT TYPE="HIDDEN" NAME="frmDupPeopleCheck" VALUE="0">

<CFELSE>


	<CFPARAM NAME="frmMoveFreeze" DEFAULT=#CreateODBCDateTime(Now())#>
	<CF_INPUT TYPE="HIDDEN" NAME="frmMoveFreeze" VALUE="#frmMoveFreeze#">

	<CF_INPUT TYPE="HIDDEN" NAME="frmDupLocs" VALUE="#frmDupLocs#">
<!--- 	<CF_INPUT TYPE="HIDDEN" NAME="frmDupPeople" VALUE="#frmDupPeople#"> --->

	<CFINCLUDE template="searchparamform.cfm">

	<CF_INPUT TYPE="HIDDEN" NAME="frmLocationID" VALUE="#frmLocationID#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmStart" VALUE="#frmStart#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmStartp" VALUE="#Variables.displaystart#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmBack" VALUE="#Variables.frmBack#">

	<INPUT TYPE="HIDDEN" NAME="frmPersonID" VALUE="0">
			<INPUT TYPE="HIDDEN" NAME="frmTask" VALUE="edit">

			<CF_INPUT TYPE="HIDDEN" NAME="frmDupLocs" VALUE="#frmDupLocs#">
			<CF_INPUT TYPE="HIDDEN" NAME="frmDupPeople" VALUE="#frmDupPeople#">
<!--- 			<CF_INPUT TYPE="HIDDEN" NAME="frmPeopleOnPage" VALUE="#frmPeopleOnPage#"> --->
			<INPUT TYPE="HIDDEN" NAME="frmDupPeopleCheck" VALUE="0">

<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" CLASS="withBorder">
	<TR>
		<TH ALIGN="CENTER" VALIGN="BOTTOM">Check</TH>
		<TH ALIGN="Left" VALIGN="BOTTOM">Name</TH>
		<TH ALIGN="Left" VALIGN="BOTTOM">Last Contact date</TH>
		<TH ALIGN="Left" VALIGN="BOTTOM">Office Phone</TH>
		<TH ALIGN="Left" VALIGN="BOTTOM">Email</TH>
	</TR>


	<CFSET frmPeopleOnPage = "">
	<CFSET endrow= min(application.showRecords+Variables.displaystart, getpeople.recordcount)>

<!--- 	<CFOUTPUT QUERY="getPeople" STARTROW=#Variables.displaystart# MAXROWS=#application.showRecords#> --->
	<CFLOOP QUERY="getPeople" STARTROW=#Variables.displaystart# ENDROW=#Endrow#>
	<CFOUTPUT>
		<!--- gather a list of all the people displayed on this page --->
		<CFSET frmPeopleOnPage = ListAppend(frmPeopleOnPage,PersonID)>

	<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
		<TD ALIGN="CENTER" VALIGN="MIDDLE">
			<CF_INPUT TYPE="CHECKBOX" NAME="frmPersonCheck" VALUE="#PersonID#" CHECKED="#iif(ListFind(frmDupPeople,PersonID) IS NOT 0,true,false)#">
		</TD>

		<TD>
			<CFIF Trim(Salutation & FirstName & LastName) IS "">&nbsp;<CFELSE>
			#Trim(Salutation & " " & FirstName & " " & LastName)#
			</CFIF>
			<SPAN CLASS="SmallFontDim">(ID: #htmleditformat(personID)#)</SPAN>

		</TD>

		<TD>
			<CFIF Trim(lastContactDate) IS "">no contact<CFELSE>
			<!--- NYF 2008-07-15 Bugs 666 - replaced: ->
			<a href="javascript:void(viewEntity('person',#personid#, #personid#, 'callhistory'))" TITLE="View contact history with #FirstName# #lastName#">#dateFormat(lastContactDate,'dd-mmm-yy')#</A>
			--- with: --->
			<a href="javascript:void(viewPerson(#personid#, 'personContactHistory'));" TITLE="View contact history with #FirstName# #lastName#">#dateFormat(lastContactDate,'dd-mmm-yy')#</A>
			<!--- <- NYF 2008-07-15 --->
			</CFIF>
		</TD>


		<TD>
			<CFIF Trim(OfficePhone) IS "">&nbsp;<CFELSE>
			#htmleditformat(OfficePhone)#
			</CFIF>
		</TD>
		<TD>
			<CFIF Trim(Email) IS "">&nbsp;<CFELSE>
			#htmleditformat(email)#
			</CFIF>
		</TD>
	</TR>
	</CFOUTPUT>
	</CFLOOP>
	</TABLE>

	<CF_INPUT TYPE="HIDDEN" NAME="frmPeopleOnPage" VALUE="#frmPeopleOnPage#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmPeopleInReport" VALUE="#valuelist(getPeople.personid)#">

	<CFIF #getPeople.RecordCount# GT #application.showRecords#>
		<!--- if we only show a subset, then give next/previous arrows --->


	<P><TABLE BORDER=0>

		<TR><TD>
				<CFIF #Variables.displaystart# GT 1>
					<INPUT TYPE="Image" NAME="PersonBack" SRC="<CFOUTPUT></CFOUTPUT>/images/buttons/c_prev_e.gif" WIDTH=64 HEIGHT=21 BORDER=0 ALT="">
				</CFIF>
				</TD>
			<TD>
	 				<CFIF (#Variables.displaystart# + #application.showRecords# - 1) LT #getPeople.RecordCount#>
						<CFOUTPUT>Record(s) #htmleditformat(Variables.displaystart)# - #Evaluate("Variables.displaystart + application.showRecords - 1")#</CFOUTPUT>
					<CFELSE>
	 					<CFOUTPUT>Record(s) #htmleditformat(Variables.displaystart)# - #getPeople.RecordCount#</cFOUTPUT>
					</CFIF>
				</TD>

			<TD>
				<CFIF (#Variables.displaystart# + #application.showRecords# - 1) LT #getPeople.RecordCount#>
					<INPUT TYPE="Image" NAME="PersonForward" SRC="<CFOUTPUT></CFOUTPUT>/images/buttons/c_next_e.gif" WIDTH=64 HEIGHT=21 BORDER=0 ALT="Next">
				</cFIF>
				</TD>
		</TR>
		</TABLE>

	</CFIF>



</CFIF>
</FORM>
<CFOUTPUT>
		<FORM METHOD="POST" ACTION="locdetail.cfm" NAME="LocForm">
			<CF_INPUT TYPE="HIDDEN" NAME="frmMoveFreeze" VALUE="#frmMoveFreeze#">

			<CFINCLUDE template="searchparamform.cfm">

			<CF_INPUT TYPE="HIDDEN" NAME="frmLocationID" VALUE="#frmLocationID#">
			<CF_INPUT TYPE="HIDDEN" NAME="frmNext" VALUE="#Variables.frmNext#">
			<CF_INPUT TYPE="HIDDEN" NAME="frmBack" VALUE="#Variables.frmBack#">

			<CF_INPUT TYPE="HIDDEN" NAME="frmStart" VALUE="#frmStart#">
			<CF_INPUT TYPE="HIDDEN" NAME="frmStartp" VALUE="#Variables.displaystart#">
			<INPUT TYPE="HIDDEN" NAME="frmTask" VALUE="edit">

			<CF_INPUT TYPE="HIDDEN" NAME="frmDupLocs" VALUE="#frmDupLocs#">
			<CF_INPUT TYPE="HIDDEN" NAME="frmDupPeople" VALUE="#frmDupPeople#">
			<CF_INPUT TYPE="HIDDEN" NAME="frmPeopleOnPage" VALUE="#frmPeopleOnPage#">
			<INPUT TYPE="HIDDEN" NAME="frmDupPeopleCheck" VALUE="0">
		</FORM>

		<FORM METHOD="POST" ACTION="PersonDedupe.cfm" NAME="DedupeForm">
			<CF_INPUT TYPE="HIDDEN" NAME="frmMoveFreeze" VALUE="#frmMoveFreeze#">
			<CFINCLUDE template="searchparamform.cfm">
			<CF_INPUT TYPE="HIDDEN" NAME="frmLocationID" VALUE="#frmLocationID#">
			<CF_INPUT TYPE="HIDDEN" NAME="frmStart" VALUE="#frmStart#">
			<CF_INPUT TYPE="HIDDEN" NAME="frmStartp" VALUE="#Variables.displaystart#">
			<CF_INPUT TYPE="HIDDEN" NAME="frmBack" VALUE="#Variables.frmBack#">

			<CF_INPUT TYPE="HIDDEN" NAME="frmDupLocs" VALUE="#frmDupLocs#">
			<CF_INPUT TYPE="HIDDEN" NAME="frmDupPeople" VALUE="#frmDupPeople#">
			<CF_INPUT TYPE="HIDDEN" NAME="frmPeopleOnPage" VALUE="#frmPeopleOnPage#">
			<INPUT TYPE="HIDDEN" NAME="frmDupPeopleCheck" VALUE="0">

		</FORM>
		<!--- This form is for moving people to different locations and Organisations --->
		<FORM METHOD="POST" ACTION="datasearch.cfm" NAME="PersonMoveForm">

			<CF_INPUT TYPE="HIDDEN" NAME="frmMoveFreeze" VALUE="#frmMoveFreeze#">

			<CFINCLUDE template="searchparamform.cfm">

			<CF_INPUT TYPE="HIDDEN" NAME="frmLocationID" VALUE="#frmLocationID#">
			<CF_INPUT TYPE="HIDDEN" NAME="frmStart" VALUE="#frmStart#">
			<CF_INPUT TYPE="HIDDEN" NAME="frmStartp" VALUE="#Variables.displaystart#">
			<CF_INPUT TYPE="HIDDEN" NAME="frmBack" VALUE="#Variables.frmBack#">

			<CF_INPUT TYPE="HIDDEN" NAME="frmDupPeople" VALUE="#frmDupPeople#">
			<CF_INPUT TYPE="HIDDEN" NAME="frmPeopleOnPage" VALUE="#frmPeopleOnPage#">
			<INPUT TYPE="HIDDEN" NAME="frmDupPeopleCheck" VALUE="0">

		</FORM>
	</CFOUTPUT>

<cfset application.com.globalFunctions.cfcookie(NAME="PFLAGSCHECKED", VALUE="no-0")>