<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			dataParams.cfm	
Author:				SWJ
Date started:		2004-09-10
	
Purpose:	central storage for data params

Usage:	

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->

<!--- 	this variable controls whether the person data screen adds the javaScript
		to autopopulate their email address--->
<cfparam name="autoPopulateEmailAddress" default="yes">
