<!--- �Relayware. All Rights Reserved 2014 --->
<!--- amendment History
	2001-10-24	SWJ created and developed this to work with per loc & org recs
	
	 --->
<CFIF isDefined('frmTask') and frmTask eq "update">
	<cfinclude template="/flags/flagTask.cfm">
	<CFIF frmNext neq "entityFlagList">
		<cfif form.frmEntityType eq 0><!--- i.e. a person record --->
			<CFSET frmPersonID = form.frmcurrentEntityID>
		</CFIF>
<!--- 		<CFInclude TEMPLATE="#frmNext#.cfm">
		<CF_ABORT>
 ---></CFIF>
</CFIF>	

<CFPARAM NAME="frmBack" DEFAULT="datamenu">
<CFPARAM NAME="frmNext" DEFAULT="entityFlagList">
<CFPARAM NAME="frmcurrentEntityID" DEFAULT="0">
<CFPARAM NAME="message" DEFAULT="">
<CFPARAM NAME="frmFlagGroupID" DEFAULT="">
<cfparam name="pagesBack" type="numeric" default="1">

<cfquery name="getFlagGroups" datasource="#application.siteDataSource#">
SELECT DISTINCT FlagGroupID as thisID, left(Name,50) as name, ParentFlagGroupID,'group' as groupType
	FROM FlagGroup
	WHERE Active = 1
	AND (Expiry > #CreateODBCDateTime(Now())# or expiry is NULL)
	AND EntityTypeID =  <cf_queryparam value="#frmEntityType#" CFSQLTYPE="CF_SQL_INTEGER" > 
	AND parentFlagGroupID = 0
	<!--- AND Scope IN (#EntityCountryList#) --->
	AND ( 
		 (EditAccessRights = 0 AND FlagGroup.Edit =1)
		OR (editAccessRights = 1
			AND EXISTS (SELECT 1 FROM FlagGroupRights
				WHERE FlagGroup.FlagGroupID = FlagGroupRights.FlagGroupID
				AND FlagGroupRights.UserID in (#request.relaycurrentuser.usergroups# )
				AND FlagGroupRights.edit <>0)))
UNION<!--- thisID is set to parent below because AllFlags does not return flag Groups with no children --->
SELECT DISTINCT parentFlagGroupID as thisID, ' - ' + left(Name,50) as Name, ParentFlagGroupID,'single' as groupType
	FROM FlagGroup
	WHERE Active = 1
	AND (Expiry > #CreateODBCDateTime(Now())# or expiry is NULL)
	AND EntityTypeID =  <cf_queryparam value="#frmEntityType#" CFSQLTYPE="CF_SQL_INTEGER" > 
	AND parentFlagGroupID <> 0
	<!--- AND Scope IN (#EntityCountryList#) --->
	AND ( 
		 (EditAccessRights = 0 AND FlagGroup.Edit =1)
		OR (editAccessRights = 1
			AND EXISTS (SELECT 1 FROM FlagGroupRights
				WHERE FlagGroup.FlagGroupID = FlagGroupRights.FlagGroupID
				AND FlagGroupRights.UserID in (#request.relaycurrentuser.usergroups# )
				AND FlagGroupRights.edit <>0)))
</cfquery>


<SCRIPT type="text/javascript">

<!--
	function verifyForm1(next) {
		var form = document.FlagForm;
		var msg = "";
		var found = "no";
		form.frmTask.value = "Update";
		if (msg != "") {
			alert("\nYou must provide the following information for this entity:\n\n" + msg);
		} else {
			form.submit();
		}
	}
	function editProfile() {
		var form = document.editProfileForm;
		form.submit();
	}
	function showAllProfiles() {
		document.FlagForm.frmFlagGroupID.value = "";
		document.FlagForm.submit();
	}

//-->

</SCRIPT>
<cfinvoke component="relay.com.profileManagement" method="getFlagType" 
	returnvariable="thisEntityType">
	<cfinvokeArgument name="datasource" value="#application.siteDataSource#">
	<cfinvokeArgument name="entityTypeID" value="#frmEntityType#">
</cfinvoke>


<cf_head>
	<cf_title><cfif isDefined("thisEntityType")>#htmleditformat(thisEntityType)#<cfelse>Entity</cfif> Profile Data</cf_title>
</cf_head>
<CFOUTPUT>

<TABLE BORDER=0 BGCOLOR="##FFFFCC" CELLPADDING="5" CELLSPACING="0" WIDTH="100%">
<TR>
	<TD ALIGN="left" VALIGN="top" CLASS="Submenu">Profile data for <cfif isDefined("thisEntityType")>#htmleditformat(thisEntityType)#<cfelse>Entity</cfif> EntityID</TD>
	<td align="right" valign="top" nowrap class="Submenu">
		<cfif isDefined("thisEntityType") and thisEntityType eq "organisation"> 
			<A HREF="/data/orgDetail.cfm?frmOrgID=#frmcurrentEntityID#" CLASS="Submenu">Organisation Details</A> |
		</cfif>
		<A HREF="JavaScript:editProfile();" CLASS="Submenu">Profile Definition</A> | 
		<A HREF="javascript:verifyForm1('entityFlagList');" CLASS="Submenu">Save</A> | 
		<A HREF="JavaScript:history.go(-#pagesBack#);" CLASS="Submenu">Back</A>
	</td>
</TR>
</TABLE>
</CFOUTPUT>


<CFIF Trim(message) IS NOT "">
<P><CFOUTPUT>#application.com.security.sanitiseHTML(message)#</CFOUTPUT>
</CFIF>


<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
	<TR>
		<TD>
		<CFFORM ACTION="entityFlagList.cfm" METHOD="POST" NAME="FlagForm">
			<INPUT TYPE="hidden" NAME="frmTask" VALUE="">
			<CFOUTPUT>
				<CF_INPUT TYPE="hidden" NAME="entityType" VALUE="#frmEntityType#">
				<CF_INPUT TYPE="hidden" NAME="thisEntity" VALUE="#frmcurrentEntityID#">
				<CF_INPUT TYPE="hidden" NAME="frmEntityType" VALUE="#frmEntityType#">
				<CF_INPUT TYPE="hidden" NAME="frmcurrentEntityID" VALUE="#frmcurrentEntityID#">
				<CF_INPUT TYPE="hidden" NAME="frmNext" VALUE="#frmNext#">
				<CF_INPUT type="hidden" name="pagesBack" value="#pagesBack#">
			</CFOUTPUT>
<!--- 			<CFSET displayas = "select"> --->
			<CFSET flagMethod = "edit">
			<CFSET entityType = frmEntityType>
			<CFSET thisEntity = frmcurrentEntityID>
			<CFIF isDefined("frmflagGroupID") and isNumeric(frmflagGroupID)>
				<CFSET GroupList = frmflagGroupID>
				<!--- GroupList is a list of flag groups that you want to 
				limit all flags to return. --->
			</CFIF>
			Choose the profile you want to edit:
			<cfselect name="frmFlagGroupID"
	          size="1"
			  onchange="javaScript:document.FlagForm.submit()"
	          query="getFlagGroups"
	          value="thisID"
	          display="name"
	          selected="#frmFlagGroupID#">
			  <option value = "">Show All Profile Data for this <cfif isDefined("thisEntityType")><cfoutput>#htmleditformat(thisEntityType)#</cfoutput><cfelse>Entity</cfif>
			  </cfselect>
			  <a href="javascript:showAllProfiles()" class="smallLink">Show all Profile Data for this <cfif isDefined("thisEntityType")><cfoutput>#htmleditformat(thisEntityType)#</cfoutput><cfelse>Entity</cfif></a>
			<BR><BR>
			<!--- <CFOUTPUT>frmFlagGroupID = #frmFlagGroupID# GroupList = #GroupList#</CFOUTPUT> --->
			<CFInclude TEMPLATE="../flags/allFlag.cfm">
		</CFFORM>
		</TD>
	</TR>
</TABLE>



<cfoutput>
<form action="/profileManager/profileList.cfm" method="post" name="editProfileForm" id="editProfileForm">
	<CF_INPUT TYPE="hidden" NAME="frmEntityTypeID" VALUE="#frmEntityType#">
	<CF_INPUT TYPE="hidden" NAME="entityType" VALUE="#frmEntityType#">
</form>
</cfoutput>





