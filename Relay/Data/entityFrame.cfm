<!--- �Relayware. All Rights Reserved 2014 --->

<!--- NJH 2012/02/15 CASE 425212: Create a unique name for each frame --->
<cfset entityDetailFrameName = "EntityDetail" & int(rand() * 1000)>

<!---
File name:		entityFrameV1.cfm
Author:
Date started:	15-02-2012

Description:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed

2012/02/15			NJH			CASE 425212: Create a unique name for each frame
2012/02/27			NJH			CASE 425212: added getOrgFrame js function.
Possible enhancements:


 --->
<script>

		function loadNewEntity(entityTypeID, entityID ,screenID, refresh, returnFunction) {

			return frames.entityNavigation.loadNewEntity(entityTypeID = entityTypeID, entityID = entityID , screenID = screenID, refresh = true,loadNewEntityCallbackFunction=returnFunction)

		}

		function loadNewEntityNoRefresh(entityTypeID, entityID ,screenID) {

			return frames.entityNavigation.loadNewEntity(entityTypeID = entityTypeID, entityID = entityID , screenID = screenID, refresh = false)
		}

		function blankCurrentEntityDetails() {
			return frames.entityNavigation.blankCurrentEntityDetails()
		}
		function getOrgFrame() {
			return parent.getOrgFrame();
		}

		function getEntityDetailFrame() {
			return <cfoutput>#entityDetailFrameName#</cfoutput>
		}

</script>

<cfoutput>
<cfset formvariables = application.com.structureFunctions.convertStructureToNameValuePairs (struct=form,delimiter="&")>
<cfset variables = application.com.structureFunctions.convertStructureToNameValuePairs (struct=form,delimiter="&")>

<FRAMESET ROWS="90,*" FRAMESPACING="0" FRAMEBORDER="0" id="entityNavigationFrameset">
    <frame src="EntityFrameNavigation.cfm?#request.query_string#&#formvariables#&entityDetailFrameName=#entityDetailFrameName#" name="entityNavigation" id="EntityNavigation" scrolling="no" noresize marginwidth="0" marginheight="0">
    <frame src="" name="#entityDetailFrameName#" id="#entityDetailFrameName#" frameborder="0" scrolling="Auto" marginwidth="0" marginheight="0">
</FRAMESET>
</cfoutput>