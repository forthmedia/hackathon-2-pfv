<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			flagOrgs.cfm	
Author:				SWJ
Date started:			/xx/02
	
Description:			

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2007-02-13	SWJ	Added facility to show by country
2007/02/27	NJH/WAB moved the orginal query into a base query so that references to country in the 
			table from query object filters would work. As a result, all the where statements
			moved into the base query as well, as some of them were referencing tables that no longer existed
			(such as f.flagID).

Possible enhancements:


 --->

<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">
<cfparam name="openAsExcel" type="boolean" default="false">

<cfset application.com.request.setTopHead(pageTitle = "Organisations")>
<cf_title>Organisation Flag List</cf_title>

<cfparam name="frmSortOrder" default="sortKey, Organisation">  <!--- NJH 2007/02/23 changed organisationName to organisation --->
<cfparam name="numRowsPerPage" default="200">
<cfparam name="startRow" default="1">
<cfparam name="filterSelectColumnList" default="Country">

<cfparam name="showDataTypeIDs" default="4,5,6">
<cfparam name="showFlagNameTypeIDs" default="2,3">


<CFQUERY NAME="getOrganisations" datasource="#application.siteDataSource#">
		select * from (
		SELECT distinct o.organisationName as ORGANISATION,  o.countryDescription as Country, o.countryID,o.organisationID,o.ISOCode,
			o.organisationName+'-'+cast(o.organisationID as varchar(150)) as sortKey,
			LEFT( UPPER( o.organisationName ), 1 ) as alphabeticalIndex
			<cfif frmdataTableFullName neq "0">
				,b.Created as FlagCreatedDate
				<cfif isDefined("frmFlagTypeID") and listFind(showDataTypeIDs,frmFlagTypeID)>
				, b.data
				</cfif>
				<cfif isDefined("frmFlagIDList") and frmProfileID neq 0 and listLen(frmFlagIDList) gt 0>
				, f.name as FlagName
				</cfif>
			</cfif>
			<!--- NJH 2008/06/06 show the person name if a person flag and the sitename if a location--->
			<cfif frmentityType eq "Person">
				, p.firstname + ' '+p.lastname as person
			<cfelseif frmentityType eq "Location">
				, l.sitename
			</cfif>
		 from vOrgList3 o
		<cfif frmdataTableFullName neq "0">
			<CFIF frmentityType eq "organisation">
				INNER JOIN #frmdataTableFullName# b with (noLock) ON b.entityID = o.organisationID
			<cfelseif frmentityType eq "Person">
				INNER JOIN person p with (noLock) on p.organisationID = o.organisationID
				INNER JOIN #frmdataTableFullName# b with (noLock) ON b.entityID = p.personID
			<cfelseif frmentityType eq "Location">
				INNER JOIN location l with (noLock) on l.organisationID = o.organisationID
				INNER JOIN #frmdataTableFullName# b with (noLock) ON b.entityID = l.locationID
			</cfif>
		INNER JOIN Flag f with (noLock) ON b.flagID = f.flagID and b.flagID in (#frmFlagIDfromFlagCount#<cfif isDefined("frmFlagIDList")>,#frmFlagIDList#</cfif>) <!--- NJH 2008/06/05 Bug Fix Lexmark Support 89. Added flagID to inner join as multiple rows for orgs were getting returned--->
		</cfif>
		WHERE 1=1 
		<cfif frmFlagIDfromFlagCount neq "0" and frmEntityType neq "0" and frmdataTableFullName neq "0" >
		<!--- SWJ: 2002-03-02 this section is designed to deal with queries coming from FlagGroupCounts.cfm report
				whcih can pass any flag entity Type to this template.  Currently we only support orgs, locs and people --->
			<CFIF frmentityType eq "organisation">
			AND o.ORGANISATIONid IN (select entityID from #frmdataTableFullName# with (noLock) where FlagID  IN ( <cf_queryparam value="#frmFlagIDfromFlagCount#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ))
			<cfelseif frmentityType eq "Person">
			AND o.ORGANISATIONid IN (select OrganisationID FROM Person with (noLock) Where Personid 
				IN (select entityID from #frmdataTableFullName# with (noLock) where FlagID  IN ( <cf_queryparam value="#frmFlagIDfromFlagCount#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )))
			<cfelseif frmentityType eq "Location">
			AND o.ORGANISATIONid IN (SELECT OrganisationID FROM Location with (noLock) Where Locationid 
				IN (select entityID from #frmdataTableFullName# with (noLock) where FlagID  IN ( <cf_queryparam value="#frmFlagIDfromFlagCount#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )))
			</CFIF>
		</cfif>
		<cfif isDefined("FRMALPHABETICALINDEX") and FRMALPHABETICALINDEX neq "">
			and left(organisationname,1) =  <cf_queryparam value="#FRMALPHABETICALINDEX#" CFSQLTYPE="CF_SQL_VARCHAR" >    
		</cfif>
		<cfif isDefined("frmFlagIDList") and frmProfileID neq "0" and listLen(frmFlagIDList) gt 0 and frmdataTableFullName neq "0">
			AND f.flagID  in ( <cf_queryparam value="#frmFlagIDList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			<cfset dontRun = true>
		</cfif>
		<cfif isDefined("frmFlagIDList") and listLen(frmFlagIDList) gt 0 and frmdataTableFullName neq "0" and not isDefined("dontRun")>
			AND f.flagID in (select flagID from #frmdataTableFullName# with (noLock) where FlagID  IN ( <cf_queryparam value="#frmFlagIDfromFlagCount#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ))
		</cfif>

		<!--- <cfif isDefined("countryScopeOrganisationRecords") and countryScopeOrganisationRecords eq 1> --->
		<cfif application.com.settings.getSetting("plo.countryScopeOrganisationRecords")>
			<!--- 11 July 2003 modified to show all orgs unless  countryScopeOrganisationRecords
			is set to 1 in content/CFTemplates/relayINI.cfm --->
		  	and o.countryID in (
			 SELECT r.CountryID FROM Rights AS r with (noLock), RightsGroup AS rg with (noLock), Country as c with (noLock)
				WHERE r.SecurityTypeID = (SELECT e.SecurityTypeID FROM SecurityType AS e with (noLock)
					WHERE e.ShortName = 'RecordTask')
				AND r.UserGroupID = rg.UserGroupID
				AND c.countryid = r.countryid
				AND rg.PersonID =  #request.relayCurrentUser.personid#
				AND r.Permission > 0
			 ) 
		 </cfif>

		) o       <!--- NJH 2007/02/23 created a base query so that references to country would be valid --->
		WHERE 1=1 

		<cfinclude template="../templates/tableFromQuery-QueryInclude.cfm">
	ORDER BY <cf_queryObjectName value="#frmSortOrder#">
</cfquery>

<cfscript>
// create a structure containing the fields below which we want to be reported when the form is filtered
passThruVars = StructNew();
StructInsert(passthruVars, "frmFlagIDfromFlagCount", frmFlagIDfromFlagCount);
StructInsert(passthruVars, "frmEntityType", frmEntityType);
StructInsert(passthruVars, "frmdataTableFullName", frmdataTableFullName);
if (isDefined("frmProfileID")) { StructInsert(passthruVars, "frmProfileID", frmProfileID); }
if (isDefined("frmFlagIDList")) { StructInsert(passthruVars, "frmFlagIDList", frmFlagIDList); }
</cfscript>

<cfif isDefined("getOrganisations.FlagName")>
	<cfset lShowTheseColumns = "ORGANISATION,COUNTRY,FLAGNAME,FLAGCREATEDDATE">
<cfelse>
	<cfset lShowTheseColumns = "ORGANISATION,COUNTRY,FLAGCREATEDDATE">
</cfif>

<!--- NJH 2008/06/06 show the person name if a person flag--->
<cfif frmentityType eq "Person">
	<cfset lShowTheseColumns = listPrepend(lShowTheseColumns,"PERSON")>
<cfelseif frmentityType eq "Location">
	<cfset lShowTheseColumns = listPrepend(lShowTheseColumns,"SITENAME")>
</cfif>

<cfif isDefined("getOrganisations.data")>
	<cfset lShowTheseColumns = listAppend(lShowTheseColumns,"DATA")>
</cfif>

<CF_tableFromQueryObject 
	queryObject="#getOrganisations#"
	sortOrder = "#frmSortOrder#"
	numRowsPerPage="#numRowsPerPage#"
	startRow="#startRow#"
	
	keyColumnList="organisation"
	keyColumnURLList="dataFrame.cfm?frmsrchOrgID="
	keyColumnKeyList="organisationID"
	keyColumnOpenInWindowList="yes"
	
	passThroughVariablesStructure = "#passThruVars#"

	allowColumnSorting="yes"
	showTheseColumns="#lShowTheseColumns#"
	dateFormat="FLAGCREATEDDATE"
	
	FilterSelectFieldList="#filterSelectColumnList#"
	
	alphabeticalIndexColumn="organisation"
	
	>