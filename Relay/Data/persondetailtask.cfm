<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Amendments
	2001-09-06 		SWJ		altered code to not call flagTask 
	2006-03-02		SWJ		Added the cf_aScreenUpdate function.
	2008/07/07 		NYF 	Bug 616 - added condition only show perPostAddScreenID if it's available to that OrgType
	2010/02/10		NJH		LID 3052 - check if person exists before adding them. (using location,firstname,lastname,email) If they exist, don't add them but show message stating that they've been found.
							This behaviour has been copied from remoteAddTask.cfm
	2015-01-14		RPW		JIRA 2015 Roadmap FIFTEEN 55 Modify Certifications to Register an Organization for Certifications
	--->
 
<!--- <cf_translate> WAB - removed these because there are cfaborts in this template so the closing tag never runs--->
<CFPARAM NAME="frmNext" DEFAULT="datamenu.cfm">
<CFPARAM NAME="insJobFunction" DEFAULT="0">
<CFPARAM NAME="insNotes" DEFAULT="">
<CFPARAM NAME="oldfrmLocationID" DEFAULT="">
<cfparam name="messageType" default="success">
 

<!--- WAB 2007/11/28 not sure what this is doing, but doesn't work when this file is called with just frmPreSelectCountryID 
2007/12/17 have now worked out that it is to do with changing the location of a person - this forces a page reload but not a save
added the oldfrmLocationID is not 0 because of problems with new locations
--->


<cfif oldfrmLocationID NEQ frmLocationID and NOT isdefined('form.Save') and oldfrmLocationID is not 0 >
	<cfset LocationIDChanged = frmLocationID>
	<CFINCLUDE TEMPLATE="./personDetail.cfm">
	
	<CF_ABORT>	
</cfif>



<CFIF IsDefined("FORM.frmtask")>
	<!--- 17-10-2007 Gad: this is here due to the CFFORM javascripts already in the 
	Form and the RelayForm tags mode of operations i resulted to this and with the approval of 
	Gawain it will serve the its purpose in the short run
	
	NJH2008/05/01 added a check on the length as we don't want this error if no phone details have been entered.
	--->
	<cfif form.phonemask neq "" and 
			( (len(form.insOfficePhone) gt 0 and len(form.insOfficePhone) neq len(phonemask)) or 
				(len(form.insMobilePhone) gt 0 and len(form.insMobilePhone) neq len(phonemask))or 
				(len(form.insHomePhone) gt 0 and len(form.insHomePhone) neq len(phonemask)) or 
				(len(form.insFaxPhone) gt 0 and len(form.insFaxPhone) neq len(phonemask) )) >
			<p align="center"  class="label">
			The following Phone(s)/Fax entry does not march the Telephone format length
			<br />
		
			<br />
			<cfif (len(form.insOfficePhone) gt 0) and (len(form.insOfficePhone) neq len(phonemask))  >
				Office Phone<br />
			</cfif>

			<cfif (len(form.insMobilePhone) gt 0) and (len(form.insMobilePhone) neq len(phonemask))  >
				Mobile Phone<br />
			</cfif>
			<cfif (len(form.insHomePhone) gt 0) and (len(form.insHomePhone) neq len(phonemask))  >
				Home Phone <br />
			</cfif>
			<cfif (len(form.insFaxPhone) gt 0) and (len(form.insFaxPhone) neq len(phonemask))  >
				Fax number <br />
			</cfif>
			<br />
			<a href="javascript:history.back(1)" >Return to form</a>
			</p>		
		<CF_ABORT>			
	</cfif>
	
	<!--- NJH 2010/02/10 LID 3052 - moved out of cftransaction so can be used if person exists already --->
	<CFQUERY NAME="findOrg" DATASOURCE=#application.SiteDataSource#>
		SELECT OrganisationID,countryID
		FROM Location
		WHERE LocationID =  <cf_queryparam value="#frmLocationID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</CFQUERY>

	<CFSET thisOrgID = findOrg.organisationID>
	
	<!--- NJH 2010/10/25 P-FNL079 - replaced the insert function with the below code in an effort to consolidate code --->
	<cfset personDetails.Salutation = insSalutation>
	<cfset personDetails.FirstName = insFirstName>
	<cfset personDetails.LastName = insLastName>
	<cfset personDetails.HomePhone = insHomePhone>
	<cfset personDetails.OfficePhone = insOfficePhone>
	<cfset personDetails.OfficePhoneExt = insOfficePhoneExt>
	<cfset personDetails.MobilePhone = insMobilePhone>
	<cfset personDetails.FaxPhone = insFaxPhone>
	<cfif structKeyExists(form,"insEmail")>
		<cfset personDetails.Email = insEmail>
	</cfif>
	<cfset personDetails.emailStatus = insEmailStatus>
	<cfset personDetails.JobDesc = insjobDesc>
	<cfset personDetails.Language = insLanguage>
	<cfset personDetails.LocationID = frmLocationID>
	<cfset personDetails.OrganisationID = thisOrgID>
	<cfset personDetails.Active = insActive>
	<cfset personDetails.countryID = findOrg.countryID>
	
	<CFIF FORM.frmtask IS "add">
		<!--- if Loc active false, person cannot be true --->

		<!--- verify the user has rights to these countries --->
		<cfset personDetails.Username = "">
		<cfset personDetails.Password = "">
		<cfset personDetails.PasswordDate = request.requestTime>
		<cfset personDetails.LoginExpires = "1992-01-01"> <!--- CreateODBCDate("01-Jan-92") --->
		<cfset personDetails.FirstTimeUser = 1>
	
		<cfset personInsResult = application.com.relayPLO.insertPerson(personDetails=personDetails,insertIfExists=false)>
		
		<cfset frmPersonID = personInsResult.entityID>
		<cfset frmEntityID = personInsResult.entityID>
		<cfif personInsResult.entityID neq 0 and personInsResult.isOK>
		
			<!--- 2015-01-14 RPW JIRA 2015 Roadmap FIFTEEN 55 Modify Certifications to Register an Organization for Certifications --->
			<cfscript>
				if (fileExists("#application.paths.code#\cftemplates\code_addcertifications.cfc")) {
					application.com.code_addcertifications.AddDefaultCertifications(personID=personInsResult.entityID);
				}
			</cfscript>


			<cfif personInsResult.exists and listLen(personInsResult.entitiesMatchedList) gte 1>
				<cfset message="#Trim(insFirstName & " " & insLastName)# phr_found. Phr_Sys_NewNotAdded">
				<cfset messageType = "info">
				<cfset frmFromTask = "yes">
			<cfelse>
				<cfset message = "#Trim(insFirstName & " " & insLastName)# Phr_Sys_WasAddedSuccessfully">
				<cfset frmFromTask = "yes">
			</cfif>
		<cfelse>
			<cfset message = "Phr_Sys_ProblemAddingRecord">
			<cfset messageType = "warning">
			<cfinclude template="datamenu.cfm">
			<CF_ABORT>
		</cfif>
		
		<!--- NJH 2010/02/10 LID 3052 - check if person exists. If not, don't add them. This is the same behaviour as remoteAddTask.cfm --->
		<!--- <cfset frmPersonID = application.com.dbTools.checkPer(FirstName=insFirstName,LastName=insLastName,Email=insEmail,LocID=frmLocationID)>

		<cfif checkPer.recordcount eq 1>
			<cfset frmPersonID = checkPer.personID>
			<cfset frmEntityID = checkPer.personID>
			<cfset message="#Trim(insFirstName & " " & insLastName)# phr_found.">
			<cfset frmFromTask = "yes">
			
		<cfelse>
			  
			<CFSET updatetime = CreateODBCDateTime(Now())>
			<CFTRANSACTION>
				
				<cfscript>
					application.com.relayDataload.insertPerson(
						Salutation="N'#insSalutation#'",
						FirstName="N'#insFirstName#'",
						LastName="N'#insLastName#'",
						Username="N''",
						Password="''",
						PasswordDate="#Now()#",
						LoginExpires="#CreateODBCDate("01-Jan-92")#",
						FirstTimeUser=1,
						HomePhone="'#insHomePhone#'",
						OfficePhone="'#insOfficePhone#'",
						OfficePhoneExt="'#insOfficePhoneExt#'",
						MobilePhone="'#insMobilePhone#'",
						FaxPhone="'#insFaxPhone#'",
						Email="'#insEmail#'",
						emailStatus="#insEmailStatus#",
						JobDesc="N'#insjobDesc#'",
						Language="'#insLanguage#'",
						LocationID="#frmLocationID#",
						OrganisationID="#thisOrgID#",
						Active="#insActive#",
						createdBy="#request.relaycurrentuser.usergroupID#",
						created="#UpdateTime#",	
						LastUpdatedBy="#request.relaycurrentuser.usergroupID#", 
						LastUpdated="#UpdateTime#"
					);
				</cfscript>
				<!--- <CFQUERY NAME="insNewPerson" DATASOURCE=#application.SiteDataSource#>
					INSERT INTO Person (PersonID, Salutation, FirstName, LastName, Username,
					Password, PasswordDate, LoginExpires, FirstTimeUser,
					HomePhone, OfficePhone, MobilePhone, FaxPhone, Email, emailStatus, JobDesc,
					Language, LocationID, OrganisationID, Active,
					CreatedBy, Created, LastUpdatedBy, LastUpdated)
					SELECT @Seed_PERID,
						  N'#insSalutation#',
						  N'#insFirstName#',
						  N'#insLastName#',
						  '',
						  '',
						  #Now()#,
						  #CreateODBCDate("01-Jan-92")#,
						  1, <!--- true --->
						  '#insHomePhone#',
						  '#insOfficePhone#',
						  '#insMobilePhone#',
						  '#insFaxPhone#',
						  '#insEmail#',
						  #insEmailStatus#,
						  '#insjobDesc#',
						  '#insLanguage#',
						  #frmLocationID#,
						  (SELECT OrganisationID FROM Location WHERE LocationID = #frmLocationID#),
						  #insActive#,
						  #request.relaycurrentuser.usergroupID#,
						  #UpdateTime#,
						  #request.relaycurrentuser.usergroupID#,
						  #UpdateTime#
					FROM Person AS p
				</CFQUERY> --->
							
		
				<!--- just check that record added --->
				<CFQUERY NAME="getPersonID" DATASOURCE=#application.SiteDataSource#>
					SELECT PersonID
					FROM Person
					WHERE CreatedBy = #request.relaycurrentuser.usergroupID#
					  AND Created = #updatetime#
					  AND LastUpdatedBy = #request.relaycurrentuser.usergroupID#
					  AND LastUpdated = #updatetime#
					  AND LocationID = #frmLocationID#
				</CFQUERY>
				
				<CFIF getPersonID.RecordCount IS NOT 1>
					<CFSET message = "Phr_Sys_ProblemAddingRecord">
					<CFINCLUDE TEMPLATE="datamenu.cfm">
					<CF_ABORT>
	
				</CFIF>
	
				<CFQUERY NAME="insLocDataSource" DATASOURCE=#application.SiteDataSource#>
					INSERT INTO PersonDataSource(PersonID, DataSourceID, RemoteDataSourceID)
					VALUES(#getPersonID.PersonID#,1,'0')
				</CFQUERY>
	
				
				<!--- New Flags --->
				<CFSET frmEntityID = getPersonID.PersonID>
	
				<!--- WAB: This creates variables of the form text_123_194324 from equal to text_123_0 which has come from the form--->
				<!--- required for new way of naming form fields --->
				<!--- removed by SWJ 2001-09-06
				<CFLOOP index="flagform" list="#flaglist#">
					<CFIF isdefined("#flagform#_0")>
						<CFSET x = setvariable("#flagform#_#frmEntityID#",evaluate("#flagform#_0") )>
					</cfif>
				</cfloop>
	
				
				<CFINCLUDE TEMPLATE="../flags/flagTask.cfm"> --->
		
			</CFTRANSACTION>
			
			<CFSET frmPersonID = getPersonID.PersonID>
			<CFSET message = "#Trim(insFirstName & " " & insLastName)# Phr_Sys_WasAddedSuccessfully">
			<CFSET frmFromTask = "yes">
		</cfif> --->
		
		<!--- NJH 2007/01/26 --->
		<!--- NYF 2008/07/07 Bug 616 - add and getScreenForOrgType condition --->
		<cfif isDefined("perPostAddScreenID") and perPostAddScreenID neq "" 
		   and len(application.com.screens.getScreenForOrgType(screentextID=perPostAddScreenID, organisaitonid=thisOrgID, organisationtypeID=application.com.RelayPLO.getEntityStructure(EntityTypeID=2, Entityid=thisOrgID).organisationtypeid)) gt 0>
			<cflocation url="entityFrameScreens.cfm?frmcurrentEntityID=#frmPersonID#&frmEntityTypeID=0&message=#message#&messageType=#messageType#&frmEntityScreen=#perPostAddScreenID#&frmWABEntityAdded" addtoken="No">
		</cfif>
	 
	
	<!--- WAB 2007-12-18 
		If we are in the entity Frame then this will reload the menus --->
	<cfoutput>
	<script>
		if (parent.loadNewEntityNoRefresh) {
			parent.loadNewEntityNoRefresh (entityTypeID = 0, entityID = #frmPersonID#, screenID = 'persondetail' )
		}		
	</script>
	</cfoutput>
		<CFINCLUDE TEMPLATE="#frmNext#">
		<CF_ABORT>
		
		
		
	<CFELSEIF FORM.frmtask IS "update">
		
		<!--- note that the last updated field used is for the person record --->

		<CFSET updatetime = CreateODBCDateTime(Now())>
		
<!--- 		<!--- check last updated date/time --->
		<CFQUERY NAME="checkPerson" DATASOURCE=#application.SiteDataSource#>
			SELECT LastUpdated, (select name from usergroup where person.lastupdatedby = usergroupid) as username
			FROM Person 
			WHERE PersonID = #frmPersonID#
			AND datediff(s,LastUpdated,  #insLastUpdated#) < 0   <!--- this syntax should get round problems associated with miiliseconds --->
			AND lastUpdatedby = #request.relaycurrentuser.usergroupID#
		</CFQUERY>

		
	
		<CFIF checkPerson.RecordCount IS NOT 0>

			<CFSET #message# = "The person's record could not be updated since it was changed at #insLastUpdated# by #checkperson.username# since you last viewed it.">
			<CFOUTPUT>#message#<BR><BR>
			#frmNext#</CFOUTPUT>
			<!--- <CFINCLUDE TEMPLATE="#frmNext#.cfm"> --->
			<CF_ABORT>
		</CFIF>
 --->

		<!--- <CFTRANSACTION>

			<CFQUERY NAME="findOrg" DATASOURCE=#application.SiteDataSource#>
				SELECT OrganisationID
				FROM Location
				WHERE LocationID = #frmLocationID#
			</CFQUERY>

			<CFSET thisOrgID = findOrg.organisationID>
			
			<CFQUERY NAME="updPerson" DATASOURCE="#application.SiteDataSource#">
			<!--- WAB 2009/10/05 see note below for explanation of this odd bit of query--->
			select case when datediff(s,LastUpdated,  #insLastUpdated#) >= 0 then 1 else 0 end as Oktoupdate from person where personID = #frmPersonID#  

			Update Person
			SET
			  	Person.Salutation = N'#insSalutation#',
				Person.FirstName = N'#insFirstName#',
				Person.LastName = N'#insLastName#',
				Person.jobDesc = N'#insjobDesc#',
				Person.HomePhone = '#insHomePhone#',
				Person.OfficePhone = '#insOfficePhone#',
				Person.OfficePhoneExt = '#insOfficePhoneExt#',
				Person.MobilePhone = '#insMobilePhone#',
				Person.FaxPhone = '#insFaxPhone#',
				Person.Email = '#insEmail#',
				Person.emailStatus = #insEmailStatus#,
				Person.Language = '#insLanguage#',
				Person.Notes = '#insNotes#',
				Person.LocationID = #frmLocationID#,
				Person.OrganisationID = #findOrg.OrganisationID#,
				Person.Active = #insActive#,
				Person.LastUpdatedBy = #request.relaycurrentuser.usergroupID#,
				Person.LastUpdated = #updatetime#
			WHERE Person.PersonID = #frmPersonID#
			AND datediff(s,LastUpdated,  #insLastUpdated#) >= 0 
			</CFQUERY>


			<!--- removed by SWJ 2001-09-06
			<CFINCLUDE TEMPLATE="../flags/flagTask.cfm"> --->
			
			
			
		</CFTRANSACTION> --->
			<cfset personDetails.notes = insNotes>
			<cfset personUpdateResult = application.com.relayEntity.updatePersonDetails(personDetails=personDetails,personID=frmPersonID)>

			<CFQUERY NAME="updPerson" DATASOURCE="#application.SiteDataSource#">
				<!--- WAB 2009/10/05 see note below for explanation of this odd bit of query--->
				select case when datediff(s,LastUpdated,  #insLastUpdated#) >= 0 then 1 else 0 end as Oktoupdate from person where personID =  <cf_queryparam value="#frmPersonID#" CFSQLTYPE="CF_SQL_INTEGER" >   
			</CFQUERY>
		
			<!--- 
				WAB 2007-06-06 (from orgdetailtask, only added here 2009/10/05
				Problem here if the screen includes another field from the organisation table, because updatedata notices that organisation record has already been updated 
				I have therefore had to add a check to the query above to see if the dates are OK, and if they are I am going to update the lastupdated variable
			--->
			<cfif updPerson.Oktoupdate>
				<cfset "form.person_lastupdated_#frmpersonID#" = updatetime>
			</cfif>


		<!--- 2006-03-03 SWJ added the ability to update perProfileSummary screen from here --->
		<cf_aScreenUpdate>

		<!--- CASE 426744: NJH 2012/02/23 output error message if record not updated successfully --->
		<cfif personUpdateResult.isOK>
			<CFSET message = "#Trim(insFirstName & " " & insLastName)# Phr_Sys_WasUpdatedSuccessfully">
		<cfelse>
			<cfset message = personUpdateResult.message>
		</cfif>
		<CFSET frmFromTask = "yes">
		
		<cfoutput>
		<!--- JC: transform incoming querystring to accessible variables
		as CFINCLUDE's do not support querystrings 
			altered by WAB to use cf_evaluatenamevaluepair
		--->

		<cfif Find('?',frmNext) GT 0>

			<cfset includeQueryString = listRest(frmNext,"?")>
			<cfset frmNext = listfirst(frmNext,"?")>			

			<cf_evaluateNameValuePair 
				nameValuePairs =  #includeQueryString#
				delimiter = "&"
				scope = "url">

<!--- 			<cfloop index=i list="#includeQueryString#" delimiters="&">
				<cfif #ListGetAt(i,1,'=')# NEQ "">
					<cfset "url.#ListGetAt(i,1,'=')#" = ListGetAt(i,2,'=')>
				</cfif>
			</cfloop>
 --->		</cfif>
		</cfoutput>


		<CFIF findNoCase("persList.cfm",frmNext) neq 0>
			<cfinclude template="/data/personDetail.cfm">
		<CFELSE>
		       <!--- <script>alert("Record saved");</script> --->
			<!--- include page stipped of query string --->
			<cfinclude template="#frmNext#">
			
			<!--- <CF_ABORT> --->
			<!--- <cfif findNocase("?", frmNext)>
				<CFLOCATION URL="#frmNext#&message=#message#&frmRedraw=no" ADDTOKEN="No">
			<cfelse>
				<CFLOCATION URL="#frmNext#?frmOrganisationID=#variables.thisOrgID#&frmPersonID=#frmPersonID#&message=#message#&frmRedraw=no" ADDTOKEN="No">
			</cfif> --->
		</CFIF>
		
		<CF_ABORT>
	</CFIF>
</CFIF>
<!--- </cf_translate> --->

