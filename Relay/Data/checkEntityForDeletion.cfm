<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			checkOrgForDeletion.cfm
Author:
Date started:			/xx/07

Description:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
	02-Jun-2008		NJH			Determine whether orgs/people can be deleted before presenting them to the user. If an org can't be
								deleted, inform the user.
	04-Mar-2009		NYB			Updated to be usable for People & Locations as well as Organisations.  Changes include:
									- renamed deleteOrg button to deleteEntity
									- in cfloop list="#frmEntityID#" rename orgID to entityID
									- renamed checkOrgDeletion to checkEntityDeletion
									- renamed getOrgFailureReasons to getEntityFailureReasons
									- renamed getPeopleInOrg to getPeopleInEntity
									- renamed getOrgPeople to getPeople
									- renamed result.OrganisationID to result.OrganisationID
									- renamed orgsOkForDeletionArray to entitiesOkForDeletionArray
									- renamed orgsNotOkForDeletionArray to entitiesNotOkForDeletionArray
									- renamed numOfOrgsOkToDelete to numOfEntitiesOkToDelete
									- renamed numOfOrgsNotOkToDelete to numOfEntitiesNotOkToDelete
									- renamed orgsToDelete to entitiesToDelete
									- renamed frmOrgsToDelete to frmEntitiesToDelete
	2009-08-21		NJH			LID 2538 - check if orgs has locations to delete first before trying to delete the locations. - Updated for version control
	2010-02-12		NJH			LID 3073 - run selection deletion through this cfm, so that people with protected flags, or internal users, etc. don't get deleted.
	2011-10-25  	NYB			LHID7095,7963 - added Delete Organisations in Checked Selections
	2014-10-08		WAB			P-NEX001 initial implementation - check if reason exists in Person Query
	2016/01/26		NJH			Changed from cfform to form. Also changed the way that the query was passed in. Added another form attribute to say what type frmEntityID was. If a selection, then we build the query string ourselves in here.
Possible enhancements:


 --->

<!--- Stop process at the start if the FlagTextID passed isn't one expected --->
<cfset message = "">
<cfif structKeyExists(form,"REASSIGNOPPORTUNITIES")>
	<cfloop collection=#form# item = "item">
		<cfif left(item,len("REASSIGNTO")) eq "REASSIGNTO">
			<cfif not structKeyExistS(form,"REASSIGNTO")><cfset form["REASSIGNTO"] = ""></cfif>
			<cfif not listFind(form.REASSIGNTO,FORM[item])>
				<cfset FORM.REASSIGNTO = FORM.REASSIGNTO & ',' & FORM[item]>
			</cfif>
		</cfif>
	</cfloop>
</cfif>

<cfif structKeyExists(form,"REASSIGNOPPORTUNITIES") and structKeyExistS(form,"REASSIGNTO")>
	<cfset message = application.com.opportunity.updateReAssignedOpportunities(form.REASSIGNTO)>
	<cfoutput>
		#application.com.relayui.message(message=message,type='success')#
	</cfoutput>
</cfif>

<cfif ListFindNoCase("DeletePerson,DeleteLocation,DeleteOrganisation", frmflagtextid) eq 0>
	<p>FlagTextID passed not recognised as valid.<br/></p>
	<p><input type="button" name="Close" value="phr_Close" onclick="javascript:window.close()"></p>
	<CF_ABORT>
</cfif>

<cfset entity = application.com.flag.getflagstructure(flagID=frmflagtextid).ENTITYTYPE>
<cfset entityType = entity.TABLENAME>
<cfset entityTypeID = entity.ENTITYTYPEID>

<!--- START: 2011-10-25 LHID7963 added: --->
<cfset frmflagright = "">
<cfset userHasPersonDeleteRights = application.com.rights.getPersonDeleteRights()>
<cfif entityType EQ "Person">
	<cfif userHasPersonDeleteRights>
		<cfset frmflagright = "SystemTask">
	</cfif>
<cfelse>
	<cfset userHasLocationDeleteRights = application.com.rights.getLocationDeleteRights()>
	<cfif entityType EQ "Location">
		<cfif userHasPersonDeleteRights and userHasLocationDeleteRights>
			<cfset frmflagright = "SystemTask">
		</cfif>
	<cfelseif entityType EQ "Organisation">
		<cfif userHasPersonDeleteRights and userHasLocationDeleteRights and application.com.rights.getOrganisationDeleteRights()>
			<cfset frmflagright = "SystemTask">
		</cfif>
	</cfif>
</cfif>
<!--- END: 2011-10-25 LHID7963 --->

<!--- NJH 2010-02-12 LID 3073 - deleting people in selections is now using this code. In this case, frmEntityID is a query. --->
<cfif structKeyExists(form,"deleteEntityIDType") and form.frmEntityID neq "">

	<cfif form.deleteEntityIDType eq "organisationSelection">
		<cfset querystring="select distinct p.organisationid as entityid from person p with(nolock) inner join selectiontag as st on p.personid=st.entityid where st.status <>0 and st.selectionid in (#application.com.security.queryParam(value=form.frmEntityID,cfsqltype='cf_sql_integer',list=true)#)">
	<cfelse>
		<cfset querystring="select entityid from selectiontag as st where st.status <>0 and st.selectionid in (#application.com.security.queryParam(value=form.frmEntityID,cfsqltype='cf_sql_integer',list=true)#)">
	</cfif>

	<cfquery name="getEntityIDs" datasource="#application.siteDataSource#">
		#querystring#
	</cfquery>

	<cfset frmEntityID = valueList(getEntityIDs.entityID)>
</cfif>

<cfquery name="getPeople" datasource="#application.SiteDataSource#">
	SELECT  organisation.OrganisationID, organisation.OrganisationName, Person.Salutation,
			Person.FirstName, Person.LastName, person.personID,
			Person.FirstName+' '+Person.LastName as PersonName,
			location.locationID,location.SiteName as LocationName
	FROM Person
		INNER JOIN location ON Person.locationID = location.locationID
		INNER JOIN organisation ON location.OrganisationID = organisation.OrganisationID
	WHERE (Person.#entityType#ID  IN ( <cf_queryparam value="#FRMENTITYID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ))
		AND (Person.PersonID NOT IN
                (
					SELECT entityid FROM booleanflagdata bfd
					INNER JOIN flag f ON bfd.flagid = f.flagid
                   	WHERE flagtextid = 'deleteperson'
				)
		)
	ORDER BY Person.OrganisationID, organisation.OrganisationName
</cfquery>

<!--- START:  NYB 2009-03-04 POL Deletion Process - added: --->
<cfif entityType neq "Person">
	<cfquery name="getLocations" datasource="#application.SiteDataSource#">
		SELECT * from location where #entityType#ID  IN ( <cf_queryparam value="#FRMENTITYID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) and LocationID NOT IN
             (SELECT entityid
              FROM   booleanflagdata bfd
              INNER JOIN flag f ON bfd.flagid = f.flagid
              WHERE  flagtextid = 'deleteLocation')
		ORDER BY OrganisationID, LocationID
	</cfquery>
</cfif>
<!--- END:  NYB 2009-03-04 POL Deletion Process --->

<cfif structKeyExists(form,"deleteEntity") and not structKeyExists(form,"reAssignOpportunities")>  <!--- 2nd/last Screen --->
	<!--- <cfoutput query="getOrgPeople">
		<cfset deletePeople = application.com.flag.setBooleanFlag(flagTextID='DeletePerson', entityID=getOrgPeople.personID)>
	</cfoutput>
	--->
	<!--- flag people for deletion --->

	<cfif isDefined("frmPeopleToDelete")>
		<cfloop list="#frmPeopleToDelete#" index="personID">
			<cfset deletePeople = application.com.flag.setBooleanFlag(flagTextID='DeletePerson', entityID=personID, useRequestTime=true)>
		</cfloop>
	</cfif>

	<cfif isDefined("frmLocationsToDelete")>
		<cfloop list="#frmLocationsToDelete#" index="locationID">
			<cfset deleteLocations = application.com.flag.setBooleanFlag(flagTextID='DeleteLocation', entityID=locationID, useRequestTime=true)>
		</cfloop>
	</cfif>

	<!--- flag orgs for deletion --->
	<!--- 2011-10-25 LHID7963 added frmflagright to url string --->
	<cflocation url="/flags/updateflag.cfm?FRMEntityID=#frmEntitiesToDelete#&FRMFlagTextID=#FRMFlagTextID#&FRMFlagvalue=#FRMFlagvalue#&FRMRunInPopup=#FRMRunInPopup#&frmflagright=#frmflagright#&message=#message#&FRMShowFeedBack=" addtoken="No">

<cfelse>  <!--- 1st Screen --->

	<cfset request.relayFormDisplayStyle = "html-table">

	<cfset entitiesOkForDeletionArray = arrayNew(1)>
	<cfset entitiesNotOkForDeletionArray = arrayNew(1)>
	<cfloop list="#frmEntityID#" index="entityID">
		<cfset result = structNew()>

		<!--- NJH Relayware 8 Enhancement check if org and people can be deleted --->
		<cfquery name="checkEntityDeletion" datasource="#application.siteDataSource#">
			exec deleteEntityCheck @entityTypeID =  <cf_queryparam value="#entityTypeID#" CFSQLTYPE="CF_SQL_INTEGER" > , @entityID =  <cf_queryparam value="#entityID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>

		<cfquery name="getPeopleInEntity" dbtype="query">
			select * from getPeople where #entityType#ID = #entityID#
		</cfquery>

		<cfset result.EntityID = entityID>

		<!--- START:  NYB 2009-03-04 POL Deletion Process -  --->
		<cfif entityType eq "Organisation">
			<cfquery name="getLocationsInEntity" dbtype="query">
				select * from getLocations where #entityType#ID = #entityID#
			</cfquery>
		</cfif>
		<!--- END:  NYB 2009-03-04 POL Deletion Process - --->

		<cfquery name="getEntityFailureReasons" dbtype="query">
			select reason, extendedInfo from checkEntityDeletion where reason <> '' order by reason
		</cfquery>

		<cfset result.reasonQry = getEntityFailureReasons>

		<!--- If the org has reasons why it can't be deleted, set the isOkToDelete flag to false --->
		<cfif getEntityFailureReasons.recordCount gt 0>
			<cfset result.isOkToDelete = false>
		<cfelse>
			<cfset result.isOkToDelete = true>
		</cfif>

		<cfif getPeopleInEntity.recordCount gt 0>
			<cfset result.EntityName = evaluate("getPeopleInEntity.#entityType#Name[1]")>
		<cfelse>
			<!--- we need to get the organisation for display purposes later, and if getPeopleInEntity had records, we could get it from
				that query, but seeing as it's empty, we need to go to the db again. --->
			<cfif entityType eq "Organisation">
				<cfquery name="getEntityName" datasource="#application.siteDataSource#">
					select organisationName as EntityName from organisation where organisationID =  <cf_queryparam value="#entityID#" CFSQLTYPE="CF_SQL_INTEGER" >
				</cfquery>
			<cfelseif entityType eq "Location">
				<cfquery name="getEntityName" datasource="#application.siteDataSource#">
					select SiteName as EntityName from location where locationID =  <cf_queryparam value="#entityID#" CFSQLTYPE="CF_SQL_INTEGER" >
				</cfquery>
			<cfelse>
				<cfquery name="getEntityName" datasource="#application.siteDataSource#">
					select FirstName+' '+LastName as EntityName from person where personID =  <cf_queryparam value="#entityID#" CFSQLTYPE="CF_SQL_INTEGER" >
				</cfquery>
			</cfif>
			<cfset result.EntityName = getEntityName.EntityName>
		</cfif>


		<!--- if there are people in the org, check if they can be deleted --->
		<cfif entityType neq "Person" and getPeopleInEntity.recordCount gt 0>
			<!--- find out if the people in the organisation can be deleted--->
			<cfquery name="checkPersonDeletion" datasource="#application.siteDataSource#">
				select personID as entityID into ##deleteEntityIDTable from person where personID  in ( <cf_queryparam value="#valueList(getPeopleInEntity.personID)#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )

				Create table ##NotDeletable(EntityID int,Reason varchar(50),extendedInfo varchar(max))

				exec deleteEntityCheck @entityTypeID=0

				select d.*, p.firstname + ' ' +p.lastname as PersonName
				from ##NotDeletable d inner join person p on d.entityID = p.personID
				order by PersonName, reason

				drop table ##NotDeletable
				drop table ##deleteEntityIDTable
			</cfquery>
		</cfif>

		<!--- START:  NYB 2009-03-04 POL Deletion Process -  --->
		<cfif entityType eq "Organisation">
			<!--- NJH 2009-08-21 LID 2538 - check if org has locations first. If not, setting the list of locationIDs to be 0 so that the query and the
				remaining process does not fall over. --->
			<cfset locationsInOrg = valueList(getLocationsInEntity.locationID)>
			<cfif getLocationsInEntity.recordCount eq 0>
				<cfset locationsInOrg = 0>
			</cfif>
			<cfquery name="checkLocationDeletion" datasource="#application.siteDataSource#">
				select locationID as entityID into ##deleteEntityIDTable from location where locationID  in ( <cf_queryparam value="#locationsInOrg#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )

				Create table ##NotDeletable(EntityID int,Reason varchar(50),extendedInfo varchar(max))

				exec deleteEntityCheck @entityTypeID=1

				select d.*, l.SiteName
				from ##NotDeletable d inner join location l on d.entityID = l.locationID
				order by SiteName, reason

				drop table ##NotDeletable
				drop table ##deleteEntityIDTable
			</cfquery>
		</cfif>
		<!--- END:  NYB 2009-03-04 POL Deletion Process - --->

		<!--- START:  NYB 2009-03-04 POL Deletion Process -  --->
		<cfif entityType neq "Person">
			<cfif getPeopleInEntity.recordCount gt 0>
				<cfif not checkPersonDeletion.recordCount gt 0 and result.isOkToDelete>
					<cfset result.people = getPeopleInEntity>
				<cfelse >
					<!--- the org has people that can't be deleted, then add the reason into the reason query --->
					<cfif checkPersonDeletion.recordCount gt 0>
						<cfset queryAddRow(result.reasonQry)>
						<cfset QuerySetCell(result.reasonQry,"reason","ContainsPeopleThatCannotBeDeleted",getEntityFailureReasons.recordCount)>
						<cfset result.people = checkPersonDeletion>
						<!--- if the org was ok to be deleted but some of the people aren't, we fail the org --->
						<cfif result.isOkToDelete>
							<cfset result.isOkToDelete = false>
						</cfif>
					</cfif>
				</cfif>
			</cfif>
		</cfif>

		<cfif entityType eq "Organisation">
			<!--- the org has locations that can't be deleted, then add the reason into the reason query --->
			<cfif checkLocationDeletion.recordCount gt 0>
				<cfset queryAddRow(result.reasonQry)>
				<cfset QuerySetCell(result.reasonQry,"reason","ContainsLocationsThatCannotBeDeleted",getEntityFailureReasons.recordCount)>
				<cfset result.locations = checkLocationDeletion>
				<!--- if the org was ok to be deleted but some of the people aren't, we fail the org --->
				<cfif result.isOkToDelete>
					<cfset result.isOkToDelete = false>
				</cfif>
			<cfelse>
				<cfif result.isOkToDelete>
					<cfset result.locations = getLocationsInEntity>
				</cfif>
			</cfif>
		</cfif>

		<cfif result.isOkToDelete>
			<cfset arrayAppend(entitiesOkForDeletionArray,result)>
		<!--- if the org is not ok to delete --->
		<cfelse>
			<cfset arrayAppend(entitiesNotOkForDeletionArray,result)>
		</cfif>
		<!--- END:  NYB 2009-03-04 POL Deletion Process - --->
	</cfloop>
	<cfset numOfEntitiesOkToDelete = arrayLen(entitiesOkForDeletionArray)>
	<cfset numOfEntitiesNotOkToDelete = arrayLen(entitiesNotOkForDeletionArray)>
	<!--- if we're ok to delete all the orgs and none of the orgs have people in them, just delete the organisations --->
	<cfif (entityType eq "Person" or (entityType eq "Location" and getPeople.recordCount eq 0)  or (entityType eq "Organisation" and getLocations.recordCount eq 0 and getPeople.recordCount eq 0)) and numOfEntitiesNotOkToDelete eq 0>
		<!--- 2011-10-25 LHID7963 added frmflagright to url string --->
		<cflocation url="../flags/updateflag.cfm?FRMEntityID=#FRMEntityID#&FRMFlagTextID=#FRMFlagTextID#&FRMFlagvalue=#FRMFlagvalue#&FRMRunInPopup=#FRMRunInPopup#&frmflagright=#frmflagright#&message=#message#&FRMShowFeedBack=" addtoken="No">
	<cfelse>
		<form method="POST" name="details" id="details">
			<!--- Display the organisations ok for deletion --->
			<!--- 2011-10-25 NYB LHID7963 added hidden fields --->
			<CF_INPUT TYPE="hidden" Name="FRMEntityID" Value="#FRMEntityID#">
			<CF_INPUT TYPE="hidden" Name="FrmFlagTextID" Value="#FrmFlagTextID#">
			<CF_INPUT TYPE="Hidden" Name="frmflagvalue" Value="#frmflagvalue#" >
			<CF_INPUT TYPE="Hidden" Name="FRMRUNINPOPUP" Value="#FRMRUNINPOPUP#" >

			<cfif numOfEntitiesOkToDelete gt 0>
				<cfif entityType neq "Person">
					<cfset peopleToDelete = ""> <!--- list of people that the form submits as people that are ok to delete --->
				</cfif>
				<cfif entityType eq "Organisation">
					<cfset locationsToDelete = ""> <!--- list of locations that the form submits as locations that are ok to delete --->
				</cfif>

				<cfset entitiesToDelete = ""> <!--- list of orgs that the form submits as orgs that are ok to delete --->

				<cf_relayFormDisplay>
					<CF_relayFormElementDisplay relayFormElementType="html" fieldname="" label="" spancols="yes" currentValue="#application.com.relayui.message(message='phr_Ext_Delete' & entityType & 'Message',type='info',showClose=false)#">
						<cfoutput>
							<cfloop from="1" to="#numOfEntitiesOkToDelete#" index="arrayIdx">
								<cfset entitiesToDelete = listAppend(entitiesToDelete,entitiesOkForDeletionArray[arrayIdx].EntityID)>
								<tr>
									<th align="right" width="5%"><cf_divOpenClose divID="#entitiesOkForDeletionArray[arrayIdx].EntityID#" startOpen="false" rememberSettings=false>
										<cfif structKeyExists(entitiesOkForDeletionArray[arrayIdx],"people") or structKeyExists(entitiesOkForDeletionArray[arrayIdx],"locations")>
											<cf_divOpenClose_Image>
										</cfif>
										#htmleditformat(arrayIdx)#.
										</cf_divOpenClose>
									</th>
									<th align="left" width="95%">#entitiesOkForDeletionArray[arrayIdx].EntityName# (#entitiesOkForDeletionArray[arrayIdx].EntityID#)</th>
								</tr>
								<cfif structKeyExists(entitiesOkForDeletionArray[arrayIdx],"people") or structKeyExists(entitiesOkForDeletionArray[arrayIdx],"locations")>
								<tr>
									<td></td>
									<td align="left"><div id="#entitiesOkForDeletionArray[arrayIdx].EntityID#" style="#divStyle#">
										<table class="entityListing" width="90%">
											<tr class="entityListing">
												<cfif structKeyExists(entitiesOkForDeletionArray[arrayIdx],"locations")>
													<td valign="top" width="50%">
														<table width="100%">
															<th>phr_sys_Locations</th>
															<!--- if we have people at the org, display them --->
																<cfset tmpQry = entitiesOkForDeletionArray[arrayIdx].locations>
																<cfset locationsToDelete = listAppend(locationsToDelete,valueList(tmpQry.locationID))>
																<!--- list the people at the organisation that will be deleted --->
																<cfloop query="tmpQry">
																	<cfset class="evenRow">
																	<cfif currentRow mod 2>
																		<cfset class="oddRow">
																	</cfif>
																	<tr class="#class#">
																		<td>#htmleditformat(SiteName)# (#htmleditformat(entityID)#)</td>
																	</tr>
																</cfloop>
															<!--- <tr class="#IIF(isDefined("class") and class eq "oddRow",DE('evenRow'),DE('oddRow'))#"><td colspan="2">&nbsp;</td></tr> --->
														</table>
													</td>
												</cfif>
												<cfif structKeyExists(entitiesOkForDeletionArray[arrayIdx],"people")>
													<td valign="top" width="50%">
														<table width="100%">
															<th>phr_sys_People</th>
															<!--- if we have people at the org, display them --->
																<cfset tmpQry = entitiesOkForDeletionArray[arrayIdx].people>
																<cfset peopleToDelete = listAppend(peopleToDelete,valueList(tmpQry.personID))>
																<!--- list the people at the organisation that will be deleted --->
																<cfloop query="tmpQry">
																	<cfset class="evenRow">
																	<cfif currentRow mod 2>
																		<cfset class="oddRow">
																	</cfif>
																	<tr class="#class#">
																		<td>#htmleditformat(firstname)# #htmleditformat(lastname)# (#htmleditformat(personID)#)</td>
																	</tr>
																</cfloop>
															<!--- <tr class="#IIF(isDefined("class") and class eq "oddRow",DE('evenRow'),DE('oddRow'))#"><td colspan="2">&nbsp;</td></tr> --->
														</table>
													</td>
												</cfif>
												<cfif (structKeyExists(entitiesOkForDeletionArray[arrayIdx],"locations") and not structKeyExists(entitiesOkForDeletionArray[arrayIdx],"people")) or (not structKeyExists(entitiesOkForDeletionArray[arrayIdx],"locations") and structKeyExists(entitiesOkForDeletionArray[arrayIdx],"people"))>
													<td valign="top" width="50%">&nbsp;</td>
												</cfif>
											</tr>
										</table>
									</div></td>
								</tr>
								</cfif>
							</cfloop>
						</cfoutput>

					<cfif entityType neq "Person">
						<cf_relayFormElement relayFormElementType="hidden" fieldname="frmPeopleToDelete" currentValue="#peopleToDelete#" label="">
					</cfif>
					<cfif entityType eq "Organisation">
						<cf_relayFormElement relayFormElementType="hidden" fieldname="frmLocationsToDelete" currentValue="#locationsToDelete#" label="">
					</cfif>

					<cf_relayFormElement relayFormElementType="hidden" fieldname="frmEntitiesToDelete" currentValue="#entitiesToDelete#" label="">

					<CF_relayFormElementDisplay relayFormElementType="html" fieldname="" valueAlign="center" label="" spancols="yes" currentValue="">
						<CF_relayFormElement relayFormElementType="button" fieldname="cancel" currentValue="phr_sys_cancel" label="" class="btn btn-primary" onclick="JavaScript: window.close();">
						<CF_relayFormElement relayFormElementType="submit" fieldname="deleteEntity" currentValue="phr_StdFormsubmitbutton" label="" class="">
					</CF_relayFormElementDisplay>
				</cf_relayFormDisplay>
			</cfif>


			<cfif numOfEntitiesNotOkToDelete gt 0>
				<!--- display the entities that cannot be deleted --->
				<cf_relayFormDisplay >
					<cfif entityType eq "Organisation">
						<cfset messagePhrase = "phr_Ext_CannotDeleteOrgMessage">
					<cfelseif entityType eq "Location">
						<cfset messagePhrase = "phr_Ext_CannotDeleteLocMessage">
					<cfelse>
						<cfset messagePhrase = "phr_Ext_CannotDeletePerMessage">
					</cfif>

					<CF_relayFormElementDisplay relayFormElementType="html" fieldname="" label="" spancols="yes" currentValue="#application.com.relayui.message(message=messagePhrase,type='error',showClose=false)#"> <!---a href='javascript:getRelayWareHelpID(371)' title='phr_ext_ClickHereToAccessHelp'>phr_ext_Help</a--->
					<cfloop from="1" to="#numOfEntitiesNotOkToDelete#" index="arrayIdx">
						<tr>
							<th align="right" width="5%">
								<cf_divOpenClose divID="#entitiesNotOkForDeletionArray[arrayIdx].EntityID#" startopen="true" rememberSettings=false>
								<cfif structKeyExists(entitiesNotOkForDeletionArray[arrayIdx],"people") or structKeyExists(entitiesNotOkForDeletionArray[arrayIdx],"locations")>
									<cf_divOpenClose_Image>
								</cfif>
								<cfoutput>#htmleditformat(arrayIdx)#.</cfoutput>
								</cf_divOpenClose>
							</th>
							<th align="left" width="95%">
								<cfoutput>#entitiesNotOkForDeletionArray[arrayIdx].EntityName# (#entitiesNotOkForDeletionArray[arrayIdx].EntityID#):</cfoutput>
								<cfset tmpOrgQry = entitiesNotOkForDeletionArray[arrayIdx].reasonQry>

								<cfquery dbtype="query" name="tmpOpportunitiesQry">
									select *
									from tmpOrgQry
									where reason like 'hasOpportunity%'
								</cfquery>

								<cfset isFirstReason = true>
								<!--- loop through the reasons why an org can't be deleted --->
								<cfoutput query="tmpOrgQry" group="reason">
									<cfif not isFirstReason>;<cfelse><cfset isFirstReason = false></cfif>
									<!--- if the reason doesn't have a flagID as part of the extendedInfo, just display the reason --->
									<cfif not listFindNoCase("hasProtectedFlag,hasProtectedLinkedEntityFlag",reason)>
										phr_ext_#reason# #extendedInfo#
									<cfelse>
										<cfset isFirstRow = true>
										<acronym title="<cfoutput><cfif not isFirstRow>;<cfelse><cfset isFirstRow = false></cfif>#application.com.flag.getFlagStructure(extendedInfo).translatedName#</cfoutput>">phr_ext_#htmleditformat(reason)#  #htmleditformat(extendedInfo)#</acronym>
									</cfif>
								</cfoutput>

								<cfif tmpOpportunitiesQry.recordcount gt 0>
									<cfset opportunitiesToReassign = true>
									<cfoutput>#application.com.opportunity.reAssignOpportunities(personID = entitiesNotOkForDeletionArray[arrayIdx].EntityID)#</cfoutput>
								</cfif>

							</th>
						</tr>
						<tr>
							<td></td>
							<td align="left"><cfoutput><div id="#entitiesNotOkForDeletionArray[arrayIdx].EntityID#" style="#divStyle#"></cfoutput>
								<table class="entityListing" width="90%">
									<tr class="entityListing">
										<cfif structKeyExists(entitiesNotOkForDeletionArray[arrayIdx],"locations")>
											<td valign="top" width="100%">
												<table width="100%">
													<th>phr_sys_Locations</th>
													<!--- if we have locations at the org, display them --->
														<cfset tmpQry = entitiesNotOkForDeletionArray[arrayIdx].locations>
															<cfoutput query="tmpQry" group="siteName">
																<cfset class="evenRow">
																<cfif currentRow mod 2 eq 1>
																	<cfset class="oddRow">
																</cfif>
																<tr class="#class#">
																	<td>#htmleditformat(SiteName)#
																		<cfif ListFindNoCase(ArrayToList(tmpQry.getColumnNames()),'Reason') eq 0>
																			(#htmleditformat(entityID)#)
																		<cfelse>
																			(#htmleditformat(entityID)#):<b>
																			<cfset isFirstReason = true>
																			<cfoutput group="reason">
																				<cfif not isFirstReason>;<cfelse><cfset isFirstReason = false></cfif>
																				<!--- if the reason doesn't have a flagID as part of the extendedInfo, just display the reason --->
																				<cfif not listFindNoCase("hasProtectedFlag,hasProtectedLinkedEntityFlag",reason)>
																					phr_ext_#reason# #extendedInfo#
																				<cfelse>
																					<cfset isFirstRow = true>
																					<acronym title="<cfoutput><cfif not isFirstRow>;<cfelse><cfset isFirstRow = false></cfif>#application.com.flag.getFlagStructure(extendedInfo).translatedName#</cfoutput>">phr_ext_#htmleditformat(reason)# #htmleditformat(extendedInfo)#</acronym>
																				</cfif>
																			</cfoutput>
																			</b>
																		</cfif>
																	</td>
																</tr>
															</cfoutput>
													<!--- <tr class="#IIF(isDefined("class") and class eq "oddRow",DE('evenRow'),DE('oddRow'))#"><td colspan="2">&nbsp;</td></tr> --->
												</table>
											</td>
										</cfif>


										<cfif structKeyExists(entitiesNotOkForDeletionArray[arrayIdx],"people")>
											<td valign="top" width="100%">
												<table>
													<cfoutput><tr class="#isDefined("class") and class eq 'oddRow'?'evenRow':'oddRow'#"></cfoutput><th width="100%">phr_sys_People</th></tr>
													<!--- if we have people at the org, display them --->
														<cfset tmpQry = entitiesNotOkForDeletionArray[arrayIdx].people>
														<cfoutput query="tmpQry" group="PersonName">
															<!--- START: 2014-10-08 WAB P-NEX001 initial implementation - check if reason exists in Person Query --->
															<cfif ListFindNoCase(ArrayToList(tmpQry.getColumnNames()),'Reason') neq 0>
															<cfquery dbtype="query" name="tmpOpportunitiesQry">
																select *
																from tmpQry
																where reason like 'hasOpportunity%'
																and entityID = #entityID#
															</cfquery>
															</cfif>
															<!--- END: 2014-10-08 WAB P-NEX001 initial implementation - check if reason exists in Person Query --->
															<cfset class= currentRow mod 2 eq 1?"oddRow":"evenRow">

															<tr class="#class#">
																<td>#htmleditformat(PersonName)#
																	<cfif ListFindNoCase(ArrayToList(tmpQry.getColumnNames()),'Reason') eq 0>
																		(#htmleditformat(entityID)#)
																	<cfelse>
																		(#htmleditformat(entityID)#):<b>
																		<cfset isFirstReason = true>
																		<cfoutput group="reason">
																			<cfif not isFirstReason>;<cfelse><cfset isFirstReason = false></cfif>
																			<!--- if the reason doesn't have a flagID as part of the extendedInfo, just display the reason --->
																			<cfif not listFindNoCase("hasProtectedFlag,hasProtectedLinkedEntityFlag",reason)>
																				phr_ext_#reason# #extendedInfo#
																			<cfelse>
																				<cfset isFirstRow = true>
																				<acronym title="<cfoutput><cfif not isFirstRow>;<cfelse><cfset isFirstRow = false></cfif>#application.com.flag.getFlagStructure(extendedInfo).translatedName#</cfoutput>">phr_ext_#htmleditformat(reason)#  #htmleditformat(extendedInfo)#</acronym>
																			</cfif>
																		</cfoutput>
																		</b>
																		<cfif tmpOpportunitiesQry.recordcount gt 0>
																			<cfset opportunitiesToReassign = true>
																			#application.com.opportunity.reAssignOpportunities(personID = EntityID,assignFrom=entityType)#
																		</cfif>
																	</cfif>
																</td>
															</tr>
														</cfoutput>
													<!--- <tr class="#IIF(isDefined("class") and class eq "oddRow",DE('evenRow'),DE('oddRow'))#"><td colspan="2">&nbsp;</td></tr> --->
												</table>
											</td>
										</cfif>
										<cfif (structKeyExists(entitiesNotOkForDeletionArray[arrayIdx],"locations") and not structKeyExists(entitiesNotOkForDeletionArray[arrayIdx],"people")) or (not structKeyExists(entitiesNotOkForDeletionArray[arrayIdx],"locations") and structKeyExists(entitiesNotOkForDeletionArray[arrayIdx],"people"))>
											<td valign="top" width="50%">&nbsp;</td>
										</cfif>
									</tr>
								</table>
								</div></td>
							</tr>
					</cfloop>

					<!--- if the orgs that we've display are ones that can't be deleted, then show a cancel button. --->
					<cfif numOfEntitiesNotOkToDelete gt 0>
						<CF_relayFormElementDisplay relayFormElementType="html" fieldname="" valueAlign="center" label="" spancols="yes" currentValue="">
							<CF_relayFormElement relayFormElementType="button" fieldname="cancel" currentValue="phr_sys_cancel" label="" class="btn btn-primary" onclick="JavaScript: window.close();">
							<cfif isDefined("opportunitiesToReassign") and opportunitiesToReassign>
								<CF_relayFormElement relayFormElementType="submit" fieldname="reAssignOpportunities" currentValue="phr_reAssignOpportunities" label="" class="">
							</cfif>
						</CF_relayFormElementDisplay>
					</cfif>

				</cf_relayFormDisplay>
			</cfif>

		</form>
	</cfif>
</cfif>