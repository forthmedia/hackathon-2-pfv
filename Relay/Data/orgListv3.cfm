<!--- �Relayware. All Rights Reserved 2014 --->
<!---
	File name:		orgListv3.cfm
	Author:		SWJ
	Date created:	July/2000
	Description:	This is a listing screen used to display organisational hierarchies.
	Date Tested:
	Tested by:
	Amendment History:
	Date (DD-MMM-YYYY)	Initials 	What was changed
	28-Jun-2000 	SWJ		Fixed the delete query to work with the new x-table structure
	2000-12-04   		WAB  	modified the drop down functions to use (almost) generic code and then added a location drop down
	2001-07-07		SWJ 	Changed the main query handling to support multiple
	2001-11-07		SWJ		Got the next next lead function working
	Removed viewer code
	2001-11-12		CPS		Problems when clicking on the next and previous buttons to retrieve the relevant sets of data
	2002-02-21		DAM		We show 30 orgs, when the user clicked next or prev to navigate between
	lists of 30 he was doing that fine but the other frames were refreshing to show
	record number one. I've modified it so that now it shows the record at the top
	of the current list of 30
	2002-02-21		DAM		Changed the search for flags. Previously it was only bringing back orgs with org flags
	now it checks for the flag against person and location too
	2002-03-02		SWJ		Changed the search for flags that comes from FlagGroupCounts report.  This noew uses a
	a full set of flag and entity data to execute the search correctly.  I have left the old
	flag search intact as this works when coming from searchData.cfm
	2002-03-05		SWJ		Added popup function to add action
	2002-03-26		DAM		Fixed the bug where flag searches were bringing back non flagged orgs
	see dated note below
	2002-05-22		SWJ		Added the generic action code.
	2002-06-07		SWJ		Added the code to search by campaigns
	2002-07-02		SWJ		Modified the frmSelectionID code on the main query as is was wrong
	2002-07-21		SWJ		Modified javascript loads to load javascript from relayware folders
	2005-03-22   		WAB 	changed invokation of commonqueries.tracksearch to use application.com notation
	2005-04-09		SWJ 	various modifications to speed up the screen
	2007-03-09		NJH 	changed next/prev images to submit buttons, form to cfform, and set the frmStart = displayStart
	at all times, as we were getting a negative start row due to frmStart being set the query.recordCount
	2007-11-14		SSS		I have added a switch to turn on and off manual org merging.
	2008-02-18		WAB		problem with paging going wrong (seemed to be some rogue code in there!)
	2008-05-15		SWJ		Modified merge switch so that it uses <cfif application.com.login.checkInternalPermissions("mergeTask","Level1")>
	2008-05-30		NJH		Added with (noLocks) on queries
	2008-06-12		SSS		have added merge tasks for switches also for merging.
	2008-09-25		PPB		make merge/dedupe organisations a popup
	2009-02-02		WAB		added code to allow this frame to be reloaded and then reload the perslist - reloadOrgListByOrgID(orgID)  - used when a new org is added
	2009-10-27		NAS		LID - 2755 'Add New Card' removed when no Organisation found
	2010-07-16		NAS		LID - 2755 This was an error 'Sorry' code removed
	2010-08-05		NJH		LID 3797 - removed Neal's code from above. (2755)
	2011-06-03		PPB		LEX052 - introduced selectedRowColor
	2012-02-27		NJH		CASE 425212: use the frameID that is passed in to get a pointer to the correct orgList and perList frames
	2012/02/27		NJH		CASE 425212: use the frameID that is passed in to get a pointer to the correct orgList and perList frames
	2012-12-10 		IH		Case 432420 use request.currentSite.httpProtocol instead of hard coded protocol to get LinkedIN JS file
	2014-03-11		SB		Removed the inline styling on .smallLink
    21/01/2016      DAN     apply fixes done for Case 447334 to fix icons alignment in case when linkedIn is not enabled
	2017-02-03		DBB		ENT-6: Hiding screen elements for read only users

	Enhancements still to do:
	1. Add the account manager
	2. Remove any old locList code
	2a. Optimise the queries
	3. Check the indexing and create a custom tag that checks for the existence of an index
	and if it's not there send an error email to the support desk
	4. Tidy up the columns
	5. Remove locFunctions or at least work out how they're going to work
	6. sort out the previous function --- Done DAM 21 Feb 2002
	7. sort out merge
	8. get Icons and remove drop down!!!
	2015-12-18 		SB		Bootstrap
	--->
<!--- 2005-04-09 : SWJ modified to use request.relayCurrentUser
	<cfinclude template="../templates/get UserSecurityLevels.cfm">
	--->
<!--- <cf_translate> --->
<cfparam name="ShowDebug" default="no">
<cfparam name="frmAccountMngrID" default="0">
<cfparam name="frameID" default="">
<!--- used to uniquely name frames in the 3 pane view, default to "" so that template can be used out of frameset --->
<cfparam name="showDeletedRecords" default="0">
<cfset selectedRowColor = application.com.settings.getSetting("miscellaneous.selectedRowColor")>
<cfset dataTaskL2 = application.com.login.checkInternalPermissions("DataTask","Level2")><!---DBB ENT-6--->
<!--- 2011-06-03 PPB LEX052 --->
<!--- DEBUG: frmFlagIDfromFlagCount
	<cfif frmFlagIDfromFlagCount is NOT "">
	<cfoutput>frmFlagIDfromFlagCount = #frmFlagIDfromFlagCount#<br>
	frmEntityType = #frmEntityType#<br>
	frmdataTableFullName = #frmdataTableFullName#</cfoutput>
	</cfif>
	--->
<div id="organisationList">
	<cf_showdebug templatename="orgListv3.cfm" section="top of file"
		variables="#frmAccountMngrID#"
		show="#ShowDebug#">
	<cfif not checkpermission.level1 gt 0>
		<cfset message="Phr_Sys_YouAreNotAuthorised">
		<cfinclude template="datamenu.cfm">
		<CF_ABORT>
	</cfif>
	<cfparam name = "application.showrecords" default="30">
	<!--- WAB 2008-02-18 changed from a cfset  --->
	<!--- Include query file. This file manages params and queries which lead to getting organisation details using all search criteria.
		The query will only be run if certain criteria is met.
		Query is stored in variables.localQryGetOrganisations
		When this page is called from dataFrame.cfm, qrySearchOrgs.cfm has already been run and the result saved as a session variable.
		A variable url.fromFrame is passed to tell qrySearchOrgs not to run the query again.
		--->
	<cfinclude template="qrySearchOrgs.cfm">
	<cfset variables.localqrygetorganisations = session.qrygetorganisations>
	<!--- WAB 2007-10-15
		Problems occur when organisations are deduped and a person refreshes the top left pane.
		Both organisations still appear because the query has been cached
		Therefore I am going to delete the session variable at this point.
		qrySearchOrgs recreates the query if session.qrygetorganisations does not exist
		--->
	<cfset structDelete (session, "qrygetorganisations")>
	<cfif variables.localqrygetorganisations.recordcount gt application.showrecords>
		<!--- showrecords in application .cfm
			defines the number of records to display --->
		<!--- define the start record --->
		<!--- <cfif isdefined("FORM.ListForward.x")> --->
		<cfif isdefined("frmListForward")>
			<cfset displaystart = frmstart + application.showrecords>
			<!--- <cfelseif isdefined("FORM.ListBack.x")> --->
		<cfelseif isdefined("frmListBack")>
			<cfset displaystart = frmstart - application.showrecords>
		<cfelseif isdefined("frmStart")>
			<cfset displaystart = frmstart>
		</cfif>
	</cfif>
	<!--- this will be used in the javascript function below
		DAM - 21 Feb 2002 amended this to make sure the other windows
		refreshed for the first org in the list
		--->
	<cfoutput query="variables.localQryGetOrganisations" startrow=#displaystart# maxrows=1>
		<cfset temporgid = #organisationid#>
		<cfset temporgname = #organisationname#>
	</cfoutput>
	<cfset application.com.request.setTopHead(getOrgRecordCount = variables.localqrygetorganisations.recordcount)>
	<!--- <cfinclude template="dataTopHead.cfm"> --->
	<!---
		<cf_head>
		<cf_title><cfoutput>#request.CurrentSite.Title#</cfoutput></cf_title>--->
	<cf_includeJavascriptOnce template = "/javascript/dropdownFunctions.js">
	<cf_includeJavascriptOnce template = "/javascript/checkBoxFunctions.js">
	<cf_includeJavascriptOnce template = "/javascript/viewEntity.js">
	<cf_includeJavascriptOnce template = "/javascript/openWin.js">
	<cfset LinkedInEnabled = application.com.settings.getSetting("socialMedia.enableSocialMedia") and (ListFindNoCase(application.com.settings.getSetting("socialMedia.services"),'LinkedIn') gt 0)>
	<!--- 2012/01/12 PPB P-REL112 connect to linkedIn --->
	<cfif LinkedInEnabled>
		<cfoutput>
			<script src="#request.currentSite.httpProtocol#platform.linkedin.com/in.js" type="text/javascript"></script>
		</cfoutput>
		<!--- 2012/01/12 PPB P-REL112  I haven't used cf_includeJavascriptOnce in case it breaches ts&cs --->
	</cfif>
	<SCRIPT type="text/javascript">
<!--
	var frameID = <cfoutput>#jsStringFormat(frameid)#;</cfoutput>

		function backForm(formName,target) {
			var form = document.mainForm;
			form.action = formName;
			form.target = target;
			form.submit();
		}

		function dedupeOrgs(FormName) {
			var form = eval('document.' + FormName);
			form.submit();
		}


		function viewOrg(orgID, screenID) {
			viewEntity (2,orgID,screenID,
				function (isOK) {

					if (isOK) {
						//PPB 2008-10-01 refresh the personlist regardless of whether the orgId has changed; done so after merging 2 orgs leaving 1 org the person list can be refreshed (no longer required for merge issue cos we force a refresh now but according to Alistair after adding a person to an org it doesn't refresh auto so I've left this in
						//if (form2.frmOrganisationID.value != orgID) {
							reloadPersList (orgID);
						//}

						//AWJR 2011-03-21 P-LEX052Selected Org Highlighted (2011-06-03 PPB LEX052 added selectedRowColor)
						var anchorArray = document.getElementsByTagName('a');
						//var searchRegex=new RegExp(": "+orgID+"$","i");
						for (var i=0;i<anchorArray.length;i++) {
							// The following was overly complicated, but we still need to 'reset' all links.
							//if (searchRegex.test(anchorArray[i].title) == true){
								//Match has been found. Now add the style.
							//	anchorArray[i].style.fontWeight = 'bold';
							//}
							//else{
							//	anchorArray[i].style.fontWeight = '';
							//}
							anchorArray[i].style.fontWeight = '';
							anchorArray[i].style.color = '';
						}
						// Set the required one to bold.
						document.getElementById("OrganisationID"+orgID).style.fontWeight = 'bold';
						document.getElementById("OrganisationID"+orgID).style.color = '#<cfoutput>#selectedRowColor#</cfoutput>';
					}
				}

			)
			// if the change of entity in the entityFrame has gone OK then reload the person frame if orgid has changed
		}

	<!--- WAB 2009-02-02 broke out of above function --->
		function reloadPersList (orgID) {

				var form2 = document.mainForm;
				form2.frmOrganisationID.value = orgID;
				form2.submit();

		}


		<!--- WAB 2009-02-02 function to reload this page when for example a new org is added
		Goes on to reload the person frame
		 --->
		function reloadOrgList (queryString,reloadPersList) {

			reloadPersList = (reloadPersList == undefined)? true : reloadPersList   // by default the perslist is reloaded
			url = 	window.location.href.split('?')[0] + '?' + queryString
			url += reloadPersList ? '&reloadPersList=true' : ''
			url += '&frameID='+frameID;
			window.location.href = url

		}

		function reloadOrgListByOrgID (orgID,reloadPersList) {
			reloadOrgList ('frmsrchOrgID='+orgID,reloadPersList)
		}


		//PPB/Will: 2008-10-01 refresh all panes after org dedupe
		function orgDedupeCompleted(orgid) {
			viewOrg(orgid)
			window.location.reload()
		}

		function newOrg(orgID) {
			viewEntity(2,0, 'organisationDetail')

		}

		function viewOrgActions(orgID) {
			viewOrg(orgID, 'ActionFrame')

		}

		function doForm(FormName) {
			var form = eval('document.' + FormName);
			<CFIF variables.localQryGetOrganisations.RecordCount IS NOT 0>
				<CFIF NOT IsDefined("frmMovePeople")>
					form.frmDupOrgCheck.value = saveChecks('Organisation');
					form.frmorgids.value = saveChecks('Organisation');
				</CFIF>
			</CFIF>
			form.submit();
		}

		function quickAdd() {
				viewOrganisation (0,'QuickCardAdd')
		}

		function getRecent(value) {
			var form = eval('document.' + FormName);
			form.submit();
		}

		// this function executes if records are found


		<!---
		WAB all initial loading done by the frame set
		<CFIF variables.localQryGetOrganisations.RecordCount gt 0>
			<CFOUTPUT>
			function loadCode(noSubmitMainForm) {
				<!--- WAB 2005-12-09 if criteria are person based then load the person detail screen, otherwise organisationdetail --->

				<CFIF (isDefined("frmsrchPersonID") and len(frmsrchPersonID) gt 0)>

					if (window.parent.frames.Detail.location)  {
						window.parent.frames.Detail.location = 'persondetail.cfm?frmPersonid=#frmsrchPersonID#&frmOrganisationID=#tempOrgid#'
					}


		<cfelse>

					if (!noSubmitMainForm) {   // WAB 2006-10-23 was (noSubmitMainForm == undefined)  -- but this caused error in IE5
						var form = document.mainForm;
						form.frmOrganisationID.value = #tempOrgid#;
						form.submit();
					}
					var form2 = document.MenuLeftForm;
					//only submit if we are running in a frame set and the target frame exists
					if ( top.frames[form2.target])  {
										form2.submit();
					}


				</CFIF>

			}
			</CFOUTPUT>
		</cfif>			 --->

		// this function is called if there are no records returned. It sets the other
		// screens to blank
		function noRecs() {
			var form = document.blankForm;
			form.submit();
			form.target = "Detail";
			form.submit();
			}


<!---
WAB 2009-02-02 This function appears not to be used (can't find calls to it or the objects it refers to)
		function reSearch(){
			if (document.selecter2.frmSiteName.options[document.selecter2.frmSiteName.selectedIndex].value != "none")
			{
			frmSiteName = document.selecter2.frmSiteName.options[document.selecter2.frmSiteName.selectedIndex].value
			}
			document.selecter2.submit()
		}
 --->
		// used to get the next lead from a phoning campaign
		function getNextLead(commid) {
			var form = document.getNextLeadForm;
			form.campaignCommID.value = commid;
			form.submit();
			}

		function createLinkToLinkedIn(encryptedUrlVariables){
			var qryStr = "../social/ConnectOrgToLinkedIn.cfm"+encryptedUrlVariables
			openWin(qryStr,'phr_social_LinkToLinkedIn','width=486,height=360,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1');		//nb  resized in ConnectOrgToLinkedIn.cfm so that when we return to it from LinkedIn (after authentication) it isn't smaller
		}

//-->
</script>
	<!--- </cf_head> --->
	<!-- #####################################################################################
		Body
		#######################################################################################--->
	<cfparam name="url.fromFrame" default="false">
	<cf_includeJavascriptOnce template="/javascript/popupDescription.js">
	<cfparam name="frmDupPer" default="0">
	<cfif variables.localqrygetorganisations.recordcount gt 0>
		<!--- 2012/01/20 P-REL112 extended colspan to cope with extra col; set to 99 to save next developer having to bother --->
		<div class="row margin-bottom-15">
			<div class="col-xs-8">
				<!---  WAB 2008-02-18 put an if statement aruond this code because dev site breaking 'cause searchTextString does not exist.
					This appears to be new code, don't know whose!!
					--->
				<cfif isDefined("searchTextString")>
					<cfoutput>
						<script>
					if (typeof(parent.parent.parent.addRowToItemSearch) != "undefined") {
						//addRowToItemSearch(thisForm.searchTextString.value+'|'+thisForm.searchFieldSelector[thisForm.searchFieldSelector.selectedIndex].value);
						parent.parent.parent.addRowToItemSearch('#searchTextString#','#searchFieldSelector#','#searchTextString#|#searchFieldSelector#');
					}
				</script>
					</cfoutput>
				</cfif>
				<cfif variables.localqrygetorganisations.recordcount gt 0>
					<!---START: DBB ENT-6--->
					<cfif dataTaskL2>
					<cfinclude template="/data/OrganisationDropDown.cfm">
				</cfif>
					<!---END: DBB ENT-6--->
				</cfif>
			</div>
		</div>
		<table class="orgListv3Tablerg table main-top-15">
			<cfoutput>
				<form name="mainForm" method="POST" action="persList.cfm" target="PersList#frameID#">
					<cf_input type="hidden" name="showDeletedRecords" value="#showDeletedRecords#">
			</cfoutput>
			<!--- GCC 2005-03-04 - Added copy to display when org result set has been restricted --->
			<tr>
				<th align="alignCenter">
					<cf_input type="checkbox" id="tagAllOrgs" name="tagAllOrgs" onclick="tagCheckboxes('frmOrganisationCheck',this.checked)">
				</th>
				<th align="left" valign="bottom">
					<cfoutput>#variables.localQryGetOrganisations.RecordCount#</cfoutput>&nbsp;Phr_Sys_OrgsFound<cfif variables.localqrygetorganisations.recordcount gte maxNumOrgRecords>&nbsp;(Phr_Sys_Restricted)</cfif>
				</th> <!--- 2012/01/20 P-REL112 extended colspan to cope with extra col; set to 99 to save next developer having to bother --->
				<th class="alignCenter">
					Phr_sys_Actions
				</th>
				<th class="alignCenter">
					phr_sys_sales
				</th>
				<th class="alignCenter">
					phr_sys_contacts
				</th>
				<th class="alignCenter">
					phr_sys_Social
				</th>
				<!---START: DBB ENT-6--->
				<cfif dataTaskL2>
				<th class="alignCenter">
					phr_sys_Edit
				</th>
				</cfif>
				<!---END: DBB ENT-6--->
			</tr>
			<cfset endrow= min(application.showrecords+variables.displaystart, variables.localqrygetorganisations.recordcount)> <!--- <cfset endrow = 100> WAB 2008-02-18 god knows why this was here - removed--->

			<cfoutput query="variables.localQryGetOrganisations" startrow=#variables.displaystart# maxrows="#endrow#"> <!--- gather a list of all the organisations displayed on this page --->
				<cfset frmlocsonpage = listappend(frmlocsonpage,organisationid)> <!--- get person actions for org and use to check whether to display red flag or not. --->

				<cfquery name="getPeopleActions" datasource="#application.SiteDataSource#">
					SELECT p.PersonID,
						(SELECT COUNT(*) AS Expr1
							FROM dbo.actions a with (noLock) INNER JOIN
				                      dbo.recordrights rr with (noLock) ON a.actionID = rr.recordid AND rr.entity = 'actions'
				     		WHERE rr.UserGroupID = #request.relayCurrentUser.usergroupid#
								and a.entityTypeID = 0 and a.entityID = p.personID and a.actionStatusID <>3) as actions
					FROM  dbo.Person p with (noLock)
						INNER JOIN dbo.Location L with (noLock) ON p.LocationID = L.LocationID
						INNER JOIN dbo.Country C with (noLock) ON L.CountryID = C.CountryID
						LEFT OUTER JOIN dbo.UserGroup ug with (noLock) ON p.LastUpdatedBy = ug.UserGroupID
					WHERE p.organisationID =  <cf_queryparam value="#frmorganisationID#" CFSQLTYPE="CF_SQL_INTEGER" >
					<cfif isdefined("lLocationIDs") and llocationids neq "">AND p.LocationID  in ( <cf_queryparam value="#lLocationIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )</cfif>
				</cfquery>

				<!--- 2007/03/01 GCC re-get org actions to avoid flag not changing colour when first org action is added due to cached session varibale org search query --->
				<cfquery name="reGetOrgActions" datasource="#application.SiteDataSource#">
					select actionID from actions a with (noLock)
				    WHERE a.entityID =  <cf_queryparam value="#variables.localqrygetorganisations.organisationID#" CFSQLTYPE="CF_SQL_INTEGER" >
					and a.entityTypeID = 2
					and a.actionStatusID <>3
				</cfquery>

				<cfif currentrow mod 2 is not 0>
					<cfset rowclass="oddrow">
				<cfelse>
					<cfset rowclass="evenrow">
				</cfif>

				<tr class="#rowClass#">
				<td valign="top">
					<CF_INPUT type="CHECKBOX" name="frmOrganisationCheck" value="#OrganisationID#" CHECKED="#iif(listfind(frmduplocs,organisationid) is not 0,true,false)#">
				</td>
				<td valign="top"> <!--- AWJR 2011-03-21 P-LEX052 Selected Org Highlighted (2011-06-03 PPB LEX052 added selectedRowColor) --->
					<a id="OrganisationID#OrganisationID#" href="JavaScript:viewOrg(#OrganisationID#);" class="smallLink" title="Phr_Sys_EditOrganisationDetails ID: #OrganisationID#">#htmleditformat(left(organisationName,50))#<cfif len(organisationName) gt 50>...</cfif> (#htmleditformat(ISOCode)#)</a> <!--- 2006-06-20 P_SNY039 GCC --->
					<cfif VATrequired>
						<cfquery name="getLocs" datasource="#application.siteDataSource#">
							SELECT    distinct address1,address5
							FROM         Location with (noLock)
							WHERE     (OrganisationID =  <cf_queryparam value="#organisationID#" CFSQLTYPE="CF_SQL_INTEGER" > )
							<!--- 2012-07-19 PPB P-SMA001 and countryID in (#request.relaycurrentuser.countrylist#) --->
							#application.com.rights.getRightsFilterWhereClause(entityType="location").whereClause#	<!--- 2012-07-19 PPB P-SMA001 --->
						</cfquery>
						<cfset info = "phr_VATnumber: " & VATnumber & "<BR>">
						<cfloop query="getLocs">
							<cfset info = info & "phr_site " & currentrow & ":" & address1 & "," & address5 & "<BR>">
						</cfloop> <!--- Wab 2008/01/14 removed " from pop info - causing js error, don't think anyone will miss --->
						<img src="/images/misc/iconInformation.gif" border="0" onmouseover="javascript:pop('#replaceNoCase(replaceNoCase(info,"'","\'","ALL"),'"','','ALL')#','ffffff',event)" onmouseout="javascript:kill();">
					</cfif>
					<cfif trim(accountnumber) is not ""> <br>&nbsp;&nbsp;#htmleditformat(ProgramName)# ID: #htmleditformat(AccountNumber)# </cfif>
					<b class="AttentionFont">#IIF(active is 0,DE("(phr_sys_Inactive)"),DE(""))#</b> <cfset currentorgid = #organisationid#><!--- This will be used as we loop thru the records below --->
				</td>
				<td align="center" valign="top">
					<cfif reGetOrgActions.recordcount eq 0 and getpeopleactions.recordcount eq 0>
						<a href="Javascript:viewOrgActions(#organisationID#);" title="phr_sys_ShowActionsFor #organisationName#">
							<span id="orgListFlag" class="fa fa-flag"></span>
						</a><br>
					<cfelse>
						<a href="javascript:viewOrgActions(#organisationID#);"  title="phr_sys_ShowActionsFor #organisationName#"><img src="/IMAGES/MISC/IconFlag.gif" border="0"></a>
					</cfif> </td> <td align="center" valign="top">
					<cfif application.com.relayMenu.isModuleActive(moduleID="LeadManager") or application.com.relayMenu.isModuleActive(moduleId="LeadManagerNoProducts")>
						<a href="javaScript:viewOrg(#OrganisationID#,'leadAndOppList');" class="Submenu" title="Phr_Sys_ViewOpportunities"><span id="orgListMoney" class="fa fa-money"></span></a>
					</cfif>
				</td>
                <td align="center"> <a href="JavaScript:viewOrg(#OrganisationID#,'OrganisationContactHistory');" title="Phr_Sys_ViewContactHistory" class="smallLink"><span id="orgListHistory" class="fa fa-history"></span></a> </td>
                <td valign="top" align="center">
                <cfif LinkedInEnabled>
                    <cfif LinkedInOrgID neq ""> <script type=IN/CompanyProfile data-format="click" data-id="#LinkedInOrgID#" data-related="false"></script> <!--- 2012/01/12 PPB P-REL112 Connect to LinkedIn --->
                    <cfelse>
                    <cfset encryptedUrlVariables = application.com.security.encryptQueryString("?serviceID=LinkedIn&entityTypeID=2&entityID=#OrganisationID#&entityName=#URLEncodedFormat(organisationName)#")><span id="orgListlinkedin" onclick="javascript:createLinkToLinkedIn('#encryptedUrlVariables#');" class="fa fa-linkedin"></span>
                    </cfif>
                </cfif>
                </td>
                <!---START: DBB ENT-6--->
				<cfif dataTaskL2>
                <td align="center" valign="top">
                    <a href="javaScript:viewOrg(#OrganisationID#,'orgProfileSummary');" title="Phr_Sys_EditProfileDataFor #organisationName#" class="smallLink">
                        <span id="orgListPencil" class="fa fa-pencil"></span></a>
                </td>
	            </cfif>
	            <!---END: DBB ENT-6--->
        </tr>
            </cfoutput>
        <CF_INPUT type="HIDDEN" name="frmMoveFreeze" value="#frmMoveFreeze#">
        <CF_INPUT type="HIDDEN" name="frmDupLocs" value="#frmDupLocs#">
        <CF_INPUT type="HIDDEN" name="frmDupPer" value="#frmDupPer#">
        <cfinclude template="searchparamform.cfm">
        <cfoutput><CF_INPUT type="HIDDEN" name="frmOrganisationID" value="#tempOrgid#"></cfoutput>
        <CF_INPUT type="HIDDEN" name="frmStart" value="#Variables.displaystart#">
        <input type="HIDDEN" name="frmBack" value="loclist">
        <!--- Start the main data table --->
        <cfoutput>
            <CF_INPUT type="HIDDEN" name="frmLocsOnPage" value="#frmLocsOnPage#">
            <CF_INPUT type="HIDDEN" name="frmLocsInReport" value="#valuelist(variables.localQryGetOrganisations.OrganisationID)#">
            <cfif isdefined("lLocationIDs") and llocationids neq "">
                <CF_INPUT type="hidden" name="frmLocationIDs" value="#lLocationIDs#">
            </cfif>
        </cfoutput>
        </form>
		</table>
		<cfif variables.localqrygetorganisations.recordcount gt application.showrecords>
			<!--- if we only show a subset, then give next/previous arrows --->
			<table width="100%" border="0" cellspacing="2" cellpadding="3" align="center" bgcolor="White">
				<cfform action="orgListv3.cfm" method="post" name="navForm" target="OrgList#frameID#">
					<cfinput type="hidden" name="frmDeleted" value="#showDeletedRecords#">
					<!--- used for search criteria --->
					<cfinput type="hidden" name="showDeletedRecords" value="#showDeletedRecords#">
					<!--- used to keep showDeletedRecords value consistent over posts --->
					<cfinput type="hidden" name="frameID" value="#frameID#">
					<!--- The following code commented out as it was producing fields containing lists of the same data that broke the SQL syntax of the form's queries --->
					<!--- 			<CFIF isdefined('FieldNames')>
						<CFLOOP INDEX="i" LIST="#FieldNames#">
						<CFOUTPUT><INPUT TYPE="hidden" NAME="#i#" VALUE="#evaluate(i)#"></CFOUTPUT>
						</CFLOOP>
						</CFIF>
						--->
					<cfinclude template="searchparamform.cfm">
					<!--- NJH 2007-03-09 we were getting a negative start row, because frmStart was set to the record count at the end. It should
						just be left to the display start --->
					<!--- <cfif (variables.displaystart + application.showrecords - 1) lt variables.localqrygetorganisations.recordcount> --->
					<CF_INPUT type="HIDDEN" name="frmStart" value="#Variables.displaystart#">
					<!--- <cfelse>
						<input type="HIDDEN" name="frmStart" value="<CFOUTPUT>#variables.localQryGetOrganisations.RecordCount#</cfoutput>">
						</cfif>
						--->
					<tr>
						<td align="left" width="20">
							<cfif variables.displaystart gt 1>
								<!--- <cfoutput><input type="Image" name="ListBack" src="/images/buttons/c_prev_e.gif" width=64 height=21 border=0 alt=""></cfoutput> --->
								<cfinput type="submit" name="frmListBack" border=0 alt="phr_Back" value=" <<< ">
							</cfif>
						</td>
						<td align="center">
							<cfif (variables.displaystart + application.showrecords - 1) lt variables.localqrygetorganisations.recordcount>
								<cfoutput>
									Record(s) #htmleditformat(Variables.displaystart)# - #Evaluate("Variables.displaystart + application.showRecords - 1")#
								</cfoutput>
							<cfelse>
								<cfoutput>
									Record(s) #htmleditformat(Variables.displaystart)# - #variables.localQryGetOrganisations.RecordCount#
								</cfoutput>
							</cfif>
						</td>
						<td align="right" width="20">
							<cfif (variables.displaystart + application.showrecords - 1) lt variables.localqrygetorganisations.recordcount>
								<!--- <cfoutput><input type="Image" name="ListForward" src="/images/buttons/c_next_e.gif" width=64 height=21 border=0 alt="Next"></cfoutput> --->
								<cfinput type="submit" name="frmListForward" border=0 alt="phr_Next" value=" >>> ">
							</cfif>
						</td>
					</tr>
				</cfform>
			</table>
		</cfif>
		<!---  --->
		<!-- #####################################################################################
			orgDetailForm
			#######################################################################################--->
		<cfoutput>
			<form name="orgDetailForm" method="POST" action="../phoning/commhistory.cfm" target="Detail">
				<input type="HIDDEN" name="frmEntityType" value="organisation">
				<CF_INPUT type="HIDDEN" name="frmcurrentEntityID" value="#tempOrgid#">
				<cfif isdefined('commID') and isnumeric(commid) and commid neq 0>
					<CF_INPUT type="hidden" name="commID" value="#commID#">
				</cfif>
			</form>
			<form name="MenuLeftForm" method="POST" action="/templates/LMenuXML.cfm?menu=AppsMenu" target="menuleft">
			</form>
			<form action="/phoning/getNextLeadv2.cfm" method="post" name="getNextLeadForm">
				<CF_INPUT type="hidden" name="campaignCommID" value="#commid#">
			</form>
		</cfoutput>
		<!--- form to pass search criteria to deduping --->
		<!--- PPB 2008-09-25 OrgDedupeForm form no longer used as we are now opening in a popup and passing frmorgids through; othe variables not used
			<form name="OrgDedupeForm" method="POST" action="orgDedupe.cfm" target="Detail">
			<cfparam name="frmMoveFreeze" default=#createodbcdatetime(now())#>
			<cfoutput>
			<input type="HIDDEN" name="frmMoveFreeze" value="#frmMoveFreeze#">
			<input type="HIDDEN" name="processContext" value="#frmMoveFreeze#">
			<input type="HIDDEN" name="frmDupOrgCheck" value="0">
			<input type="HIDDEN" name="frmorgids" value="">
			</cfoutput>
			<cfinclude template="searchparamform.cfm">
			</form>
			--->
	<cfelse>
		<!--- if the search returns no records --->
		<div class="row">
			<div class="col-xs-12">
				<p>
					Phr_Sys_NoOrgsFound
				</p>
			</div>
		</div>
		<!---  --->
	</cfif>
	<cfoutput>
		<form action="/phoning/phoninghome.cfm" method="post" name="nextForm">
		</form>
		<form action="/templates/blank.cfm" method="get" name="blankForm" target="PersList#frameID#">
		</form>
	</cfoutput>
	<cfinclude template="_entityDetailForms.cfm">
	<!---
		WAB 2009-02-02
		Some occasion when want this page to refresh the perlist frame on load.  [Namely when this frame has been refreshed from the bottom frame when a new org has been added]
		In this case a parameter reloadPersList is passed and we reload the perslist with the first org in the query
		--->
	<cfif isdefined("reloadPersList")>
		<script>
			<cfoutput>
			reloadPersList	(#localQryGetOrganisations.organisationid#)
			</cfoutput>
		</script>
	</cfif>
</div>
