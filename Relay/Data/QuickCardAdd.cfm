<!--- �Relayware. All Rights Reserved 2014 --->
<!--- amendment History

	1999-04-13	 WAB 	Added fields required for search by first and last name
	2001-09-06 SWJ	Removed the call to flags and set the screen up to work with styles not fonts
	2001-09-20 SWJ	Updated look and feel slightly and got update and add working properly
	2006-05-24 NJH	(P_FNL017) Altered code to display form elements with standard wrapper tags.
	2006/06/19 GCC	P_SNY039 Implemented extended match keys for VAT required companies and country first input to enable country data masks
	2007-04-12 AJC	Trend - Added Org Type for filtering/different displays etc
	2008-06-25 NYF	NABU bug 340 - added code around hideDirectLineExtension to facilitate adding it to internal New Card screen
	2008/07/03 NJH	made insAddress1 mandatory by default  - Bug Fix T-10 Issue 619.
	2015-11-06 ACPK    Added Bootstrap
	2015-11-11 ACPK    Commented out cutpaste area
	2015-11-13 ACPK    Removed table for header
	--->

<!--- user rights to access:
		SecurityTask:dataTask
		Permission: Read
--->


<CFIF NOT checkPermission.AddOkay GT 0>
	<CFSET message="You are not authorised to use this function">
		<CFINCLUDE TEMPLATE="datamenu.cfm">
	<CF_ABORT>
</CFIF>

<cfinclude template="/templates/qrygetcountries.cfm">

<!--- the following is needed whether we add/update --->
<cfquery name="getCountries" datasource="#application.siteDataSource#">
	SELECT DISTINCT c.CountryID, c.CountryDescription
	FROM Country AS c
	WHERE c.CountryID  IN ( <cf_queryparam value="#CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	ORDER BY c.CountryDescription ASC
</cfquery>

<!--- the following is needed whether we add/update --->
<cfquery name="qry_get_organisationTypes" datasource="#application.siteDataSource#">
	SELECT organisationTypeID,'phr_sys_'+TypeTextID as TypeTextID
	FROM organisationType
	WHERE Active=1
	ORDER BY sortIndex
</cfquery>

<cfparam name="showCols" default="insCountryID,insSalutation,insFirstName,insLastName,insJobDesc,insEmail,insOfficePhone,insMobilePhone,insSiteName,insVATnumber,insAddress1,insAddress2,insAddress3,insAddress4,insAddress5,insPostalCode,insTelephone,insFax,insOrgURL" type="string">
<!--- <cfparam name="mandatoryCols" default="insFirstName,insLastName,insEmail,insSiteName,insAddress1" type="string"> ---> <!--- NJH 2008/07/03 maded insAddress1 mandatory by default --->
<cfparam name="mandatoryCols" default="#application.com.settings.getSetting('plo.mandatoryCols')#">
<cfparam name="personDataOnly" default="0" type="numeric">
<cfparam name="initialiseUser" default="false">
<cfparam name="showRequiredSymbol" type="boolean" default="false">
<cfparam name="frmNext" default="return=this" type="string">
<cfparam name="frmReturnMessage" default="yes" type="boolean">
<cfparam name="fieldListQuery" default="relayFieldList" type="string">
<cfparam name="frmDebug" default="no" type="boolean">
<cfparam name="flagStructure" default="" type="string">
<cfparam name="form.selectedCountry" type="boolean" default="0">
<!--- 2007-04-12 AJC - Trend - Org Type --->
<cfparam name="insOrganisationTypeID" type="numeric" default="0">

<!--- NYF 2008-06-25 NABU bug 340 - added to add to internal New Card screen -> --->
<cfif structKeyExists(request,"hideDirectLineExtension")>
	<cfparam name="hideDirectLineExtension" default="#request.hideDirectLineExtension#">
<cfelse>
	<cfparam name="hideDirectLineExtension" default="true"><!--- pkp 2008-04-22 add switch to hide extension text box --->
</cfif>
<!--- <- NYF 2008-06-25 --->
<CFPARAM NAME="qryCountryID" DEFAULT="#request.relayCurrentUser.countryID#">

<cfif isDefined("insCountryID")>

	<cfif insCountryID eq 13>
		<cfset showCols = listAppend(showCols,"insAddress7,insAddress8,insAddress9")>
	</cfif>
	<cfif insCountryID eq 17>
		<cfif isDefined("mandatoryColsOverride")>
			<cfset mandatoryColsOverride = listAppend(mandatoryColsOverride,"insAddress5")>
		<cfelse>
			<cfset mandatoryCols = listAppend(mandatoryCols,"insAddress5")>
		</cfif>
	</cfif>
</cfif>

 <!--- 2005/07/06 - GCC - showAdditionalCols SUPLEMENTS the default showCols list --->
<cfif isDefined("showAdditionalCols") and len(showAdditionalCols) gt 0>
	<cfloop index="i" list="#showAdditionalCols#" delimiters="-">
		<cfset showCols = listAppend(showcols,i)>
	</cfloop>
</cfif>

<!--- 2005/07/06 - GCC - mandatoryColsOverride REPLACES default mandatory list --->
<cfif isDefined("mandatoryColsOverride") and len(mandatoryColsOverride) gt 0>
	<cfset mandatoryCols ="">
	<cfloop index="i" list="#mandatoryColsOverride#" delimiters="-">
		<cfset mandatoryCols = listAppend(mandatoryCols,i,",")>
	</cfloop>
<cfelse>
	<!--- NJH 2008/07/08 Bug Fix T-10 Issue 619 commented this out as the mandatoryCols param is being overridden which shouldn't be.
	<cfset mandatoryCols = "insFirstName,insLastName,insEmail,insSiteName"> --->
	<!--- NJH 2007/02/06 added address5 (state) to the mandatory columns if we're in the US --->
	<cfif isDefined("insCountryID") and insCountryID eq 17>
		<cfset mandatoryCols = listAppend(mandatoryCols,"insAddress5")>
	</cfif>
</cfif>

<!--- NJH 2007/10/02 make the org url required if the organisation matchname method used is the org url --->
<cfif isDefined("request.orgMatchNameMethodID") and request.orgMatchNameMethodID eq 2>
	<cfset mandatoryCols = listAppend(mandatoryCols,"insOrgURL")>
</cfif>

<!--- <CFPARAM NAME="qryFrmTask" DEFAULT="add">
<CFPARAM NAME="qrylocationID" DEFAULT="0">
<CFPARAM NAME="qryAddress1" DEFAULT="">
<CFPARAM NAME="qryAddress2" DEFAULT="">
<CFPARAM NAME="qryAddress3" DEFAULT="">
<CFPARAM NAME="qryAddress4" DEFAULT="">
<CFPARAM NAME="qryAddress5" DEFAULT="">
<CFPARAM NAME="qryPostalCode" DEFAULT=""> --->

<!--- <CFPARAM NAME="qryOrganisationID" DEFAULT="0">
<CFPARAM NAME="qrySitename" DEFAULT="">
<CFPARAM NAME="qryTelephone" DEFAULT="">
<CFPARAM NAME="qryFax" DEFAULT="">
<CFPARAM NAME="qrySpecificURL" DEFAULT="">
<CFPARAM NAME="qryActive" DEFAULT="yes">
<CFPARAM NAME="qryDirect" DEFAULT="no">
<CFPARAM NAME="qrySalutation" DEFAULT="">
<CFPARAM NAME="qryFirstName" DEFAULT="">
<CFPARAM NAME="qryLastName" DEFAULT="">
<cfparam name="qryJobdesc" default="">
<CFPARAM NAME="qryUsername" DEFAULT="">
<CFPARAM NAME="qryHomePhone" DEFAULT="">
<CFPARAM NAME="qryOfficePhone" DEFAULT="">
<CFPARAM NAME="qryFaxPhone" DEFAULT="">
<CFPARAM NAME="qryMobilePhone" DEFAULT="">
<CFPARAM NAME="qryEmail" DEFAULT="">
<CFPARAM NAME="qryLanguage" DEFAULT="English"> --->


<CFPARAM NAME="message" DEFAULT="">

<CFINCLUDE TEMPLATE="_entityDetailForms.cfm">


<h4>Phr_Sys_AddNewBusinessCard</h4>


<cfparam name="request.relayFormDisplayStyle" default="HTML-table">
<cfinclude template="/templates/relayFormJavaScripts.cfm">
<cf_translate phrases="Phr_Sys_Country"/> <!---, Phr_Sys_FirstName, Phr_Sys_LastName, Phr_Sys_Email,Phr_SYS_VATnumber"/> --->
<cf_translate phrases="Phr_Sys_OrganisationType"/>
<!--- 2006/06/19 P_SNY039 GCC --->
<cfif form.selectedCountry is 0 and not isDefined("addContactSaveButton")>

		<SCRIPT type="text/javascript">
		function submitPrimaryForm () {
			var form = document.details
			form.frmSave.click()
		}
		</SCRIPT>

	<cfform action="?#request.query_string#" method="post" name="details" id="details">
		<CF_relayFormDisplay>
			<CF_relayFormElementDisplay relayFormElementType="select" currentValue="#qryCountryID#" fieldName="insCountryID" query="#getCountries#" display="CountryDescription" value="CountryID" label="#Phr_Sys_Country#" nullText="phr_SelectACountry" nullValue="0" required="yes">
			<!--- 2007-04-12 - AJC - Trend OrgTypeID code --->
			<!--- If there is more that 1 Active Org Type then display Drop down --->
			<cfif qry_get_organisationTypes.RecordCount gt 1>
				<CF_relayFormElementDisplay relayFormElementType="select" fieldName="insOrganisationTypeID" currentValue="" query="#qry_get_organisationTypes#" display="typeTextID" value="organisationTypeID" label="#Phr_Sys_OrganisationType#" required="yes">
			<!--- If 1 Org Type set hidden value to it's ID --->
			<cfelseif qry_get_organisationTypes.RecordCount eq 1>
				<CF_relayFormElementDisplay relayFormElementType="hidden" fieldName="insOrganisationTypeID" currentValue="#qry_get_organisationTypes.organisationTypeID#" label="">
			<!--- If no org types then set to a default of 1 --->
			<cfelse>
				<CF_relayFormElementDisplay relayFormElementType="hidden" fieldName="insOrganisationTypeID" currentValue="1" label="">
			</cfif>
			<CF_relayFormElementDisplay relayFormElementType="hidden" fieldName="selectedCountry" currentValue="1" label="">
			<CF_relayFormElementDisplay relayFormElementType="hidden" fieldName="frmShowLanguageID" currentValue="1" label="">
			<CF_relayFormElementDisplay relayFormElementType="submit" fieldname="frmSave" currentValue="phr_continue" label="">
		</CF_relayFormDisplay>
	</cfform>

<cfelseif form.selectedCountry is not 0 or isDefined("addContactSaveButton")>

	<SCRIPT type="text/javascript">
		function submitPrimaryForm () {
			var form = document.details
			form.addContactSaveButton.click()
		}
	</SCRIPT>


      <!---SB Bootstrap 18/11/15  <div class="form-group">
	       	<div class="col-xs-12"> --->
	       		<cf_addRelayContactDataForm
	       			showCols = "#showCols#"
	       			mandatoryCols = "#mandatoryCols#"
	       			defaultCountryID = "#insCountryID#"
	       			insorganisationTypeID = "#insOrganisationTypeID#"
	       			frmNext = "#frmNext#"
	       			frmDebug = "#frmDebug#"
	       			showIntroText = "no"
	       			frmReturnMessage = "#frmReturnMessage#"
	       			urlRoot = ""
	       			flagStructure = "#flagStructure#"
	       			nextstep=6
	       			fieldListQuery = "#fieldListQuery#"
	       			personDataOnly = "#personDataOnly#"
	       			initialiseUser= #initialiseUser#
	       			hideDirectLineExtension = #hideDirectLineExtension#
	       			showRequiredSymbol=#showRequiredSymbol#
	       			>
	       		<cfif Trim(message) IS NOT "">
	       			<cfoutput>#application.com.relayUI.message(message=message)#</cfoutput>
	       		</cfif>

	       		<!--- 2015-11-11     ACPK    Commented out cutpaste area --->
	       		 <!--- <cfif not isDefined("addContactSaveButton")>
	       			<cfform >
	       				 <b>Phr_Sys_CutPasteArea</b><br>
	       					<CF_relayFormElement relayFormElementType="textArea" fieldname="textBox" currentValue="" label="" maxlength="2000" cols="35" rows="10" noteText="Phr_Sys_CutPasteArea" NoteTextPosition="top">
	       			</CFFORM>
	       		</cfif> --->
	 <!---       	</div>
       </div> --->


</cfif>