<!--- �Relayware. All Rights Reserved 2014 --->
<!--- amendment History
	1999-04-13	WAB Added fields required for search by first and last name --->

<CFIF NOT checkPermission.UpdateOkay GT 0>
	<!--- if frmPersonID is not 0 then task is to update
	      but the user doesn't have update permissions --->
	<CFSET message="You are not authorised to use this function">
	<CFINCLUDE TEMPLATE="datamenu.cfm">
	<CF_ABORT>

</CFIF>



<cfinclude template="/templates/qrygetcountries.cfm">

<CFPARAM NAME="frmBack" DEFAULT="datamenu">
<CFPARAM NAME="frmNext" DEFAULT="datamenu">
<CFPARAM NAME="frmLocationID" DEFAULT="0">
<CFPARAM NAME="message" DEFAULT="">

<CFQUERY NAME="getLocInfo" datasource="#application.siteDataSource#">
	SELECT l.LocationID,
		  l.SiteName,
		  l.Telephone,
		  l.PostalCode,
  		  l.LastUpdated,
		  c.CountryDescription,
		  c.ISOCode
	FROM Location AS l, Country AS c
	WHERE l.CountryID = c.CountryID
<!--- 	  AND l.CountryID IN (#Variables.CountryList#) <!--- from qrygetcountries.cfm ---> --->
<!--- 	  AND c.CountryID = l.CountryID<!--- AND c.CountryID IN (#Variables.CountryList#) ---> --->
	  AND l.LocationID =  <cf_queryparam value="#frmLocationID#" CFSQLTYPE="CF_SQL_INTEGER" > 
</CFQUERY>

<!--- <CFIF getLocInfo.RecordCount IS 0>

	<CFSET message = "You did not select a location, or the location could not be found.">
	<CFINCLUDE TEMPLATE="datamenu.cfm">
	<CF_ABORT>

</CFIF>
 --->
<!--- <CFQUERY NAME="getDataSources" datasource="#application.siteDataSource#">
	SELECT d.Description & '-' & pd.RemoteDataSourceID AS DataSource
	  FROM Location AS p, DataSource AS d, LocationDataSource AS ld
	 WHERE d.DataSourceID = ld.DataSourceID
	   AND ld.LocationID = l.LocationID
	   AND l.LocationID = #frmLocationID#
	 ORDER BY d.Description
</CFQUERY> --->

	
<!--- <CFPARAM NAME="qryCreatedBy" DEFAULT="">
<CFPARAM NAME="qryCreated" DEFAULT="">
<CFPARAM NAME="qryLastUpdatedBy" DEFAULT="">
<CFPARAM NAME="qryLastUpdated" DEFAULT="">

<CFPARAM NAME="qryLookupID" DEFAULT="0"> --->




<head/>
<cf_title><cfoutput>#request.CurrentSite.Title#</cfoutput></cf_title>
<SCRIPT type="text/javascript">

<!--
	function verifyForm(back,next) {
		var form = document.FlagForm;
//		form.frmTask.value = task;
//		form.frmNext.value = next;
//		form.frmBack.value = back;
		var msg = "";
		var found = "no";
				
		//for (j=0; j<form.elements.length; j++){
		//	thiselement = form.elements[j];
		//	if (thiselement.name == "insPartnerType") {
		//		if (thiselement.checked) {
		//			found = "yes";
		//		}
		//	}
		//}

		//if (found == "no") {
		//	msg += "* Partner Type (at least one)\n";
		//}

		if (msg != "") {
			alert("\nYou must provide the following information for this location:\n\n" + msg);
		} else {
			form.submit();
		}
}
//-->

</SCRIPT>

<cf_head>
	<cf_title>Location Flag List</cf_title>
</cf_head>




<CFOUTPUT>
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
<TR>
	<TD ALIGN="LEFT" VALIGN="TOP"><H1>Flags for this location</H1></TD>
	<TD ALIGN="RIGHT" VALIGN="TOP"></TD>
</TR>
</TABLE>
</CFOUTPUT>

<CENTER>

<CFIF #Trim(message)# IS NOT "">
<P><CFOUTPUT>#application.com.security.sanitiseHTML(message)#</CFOUTPUT>
</CFIF>

<P><TABLE BORDER=0 BGCOLOR="#FFFFCC" CELLPADDING="5" CELLSPACING="0" WIDTH="320">
	
	<TR><TD COLSPAN="2" ALIGN="CENTER" BGCOLOR="#000099">
	<FONT FACE="Arial, Chicago, Helvetica" SIZE="2" COLOR="#FFFFFF"><B>CURRENT RECORD SUMMARY</B></FONT></TD></TR>
	
	<CFIF #getLocInfo.RecordCount# IS NOT 0>

		<TR><TD VALIGN="TOP" ALIGN="CENTER" COLSPAN=2>
			<TABLE BORDER=0 CELLSPACING=0 CELLPADDING=2>
				<TR><TD VALIGN="TOP">
						Location ID:
					</TD>
					<TD VALIGN="TOP">
						<CFOUTPUT>#htmleditformat(getLocInfo.LocationID)#</CFOUTPUT>
					</TD>
				</TR>
		
				<TR><TD VALIGN="TOP">
						Organisation Name:
					</TD>
					<TD VALIGN="TOP">
						<CFIF #Trim(getLocInfo.SiteName)# IS "">&nbsp;<CFELSE>
						<CFOUTPUT>#HTMLEditFormat(Trim(getLocInfo.SiteName))#</CFOUTPUT>
						</CFIF>
					</TD>
				</TR>
				<TR><TD VALIGN="TOP">
						Postal Code:
					</TD>
					<TD VALIGN="TOP">
						<CFIF #Trim(getLocInfo.PostalCode)# IS "">&nbsp;<CFELSE>
						<CFOUTPUT>#HTMLEditFormat(Trim(getLocInfo.PostalCode))#</CFOUTPUT>
						</CFIF>
					</TD>
				</TR>
				<TR><TD VALIGN="TOP">
						Telephone:
					</TD>
					<TD VALIGN="TOP">
						<CFIF #Trim(getLocInfo.Telephone)# IS "">&nbsp;<CFELSE>
						<CFOUTPUT>#HTMLEditFormat(Trim(getLocInfo.Telephone))#</CFOUTPUT>
						</CFIF>
					</TD>
				</TR>
				<TR><TD VALIGN="TOP">
						Country (ISO Code):
					</TD>
					<TD VALIGN="TOP">
						<CFIF #Trim(getLocInfo.CountryDescription)# IS "">&nbsp;<CFELSE>
						<CFOUTPUT>#HTMLEditFormat(Trim(getLocInfo.CountryDescription))# (#HTMLEditFormat(Trim(getLocInfo.ISOCode))#)</CFOUTPUT>
						</CFIF>
					</TD>
				</TR>
		
				</TABLE>
			</TD>
		</TR>
		
	<CFELSE>
		<TR><TD ALIGN="CENTER" COLSPAN=2>
				The specified location could not be found.
			<BR></TD>
		</TR>
		</TABLE>
		</CENTER>
		
		
		
		<!--- messy end for messy situation --->
		<CF_ABORT>
	</CFIF>
</TABLE>


<P><CFOUTPUT><FORM ACTION="locflagstask.cfm" METHOD="POST" NAME="FlagForm"></CFOUTPUT>
	<CF_INPUT TYPE="HIDDEN" NAME="insLastUpdated" VALUE="#CreateODBCDateTime(getLocInfo.LastUpdated)#">	
	<CF_INPUT TYPE="HIDDEN" NAME="frmLocationID" VALUE="#frmLocationID#">	
	<INPUT TYPE="HIDDEN" NAME="frmTask" VALUE="update">
<!--- 	<INPUT TYPE="HIDDEN" NAME="frmDupPeople" VALUE="<CFOUTPUT>#frmDupPeople#</CFOUTPUT>">	
	<INPUT TYPE="HIDDEN" NAME="frmDupLocs" VALUE="<CFOUTPUT>#frmDupLocs#</CFOUTPUT>">
 --->	
<!--- 	<CFINCLUDE template="searchparamform.cfm"> --->
	
<!--- 	<INPUT TYPE="HIDDEN" NAME="frmStartp" VALUE="<CFOUTPUT>#frmStartp#</CFOUTPUT>">			
	<INPUT TYPE="HIDDEN" NAME="frmStart" VALUE="<CFOUTPUT>#frmStart#</CFOUTPUT>">
 --->	<CFIF IsDefined("frmPersonID")>
		<CF_INPUT TYPE="HIDDEN" NAME="frmPersonID" VALUE="#frmPersonID#">
	</CFIF>
	<CF_INPUT TYPE="HIDDEN" NAME="frmBack" VALUE="#frmBack#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmNext" VALUE="#frmNext#">

<P><TABLE BORDER=0>

<!--- New Flags --->
	<TR>
		<TD VALIGN="TOP" COLSPAN=2><BR>
		<CFSET flagMethod = "edit">
		<CFSET entityType = 1>
		<CFSET thisEntity = frmLocationID>
		<CFINCLUDE TEMPLATE="../flags/allFlag.cfm">
		<BR><BR></TD>
	</TR>

</TABLE>


<CFOUTPUT>
<P><A HREF="JavaScript:verifyForm('#frmBack#','#frmBack#');">Save and return</A>
</CFOUTPUT>



</FORM>

</CENTER>



