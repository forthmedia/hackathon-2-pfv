<!--- �Relayware. All Rights Reserved 2014 --->
<!--- amendment History

	1999-04-13	 		WAB 	Added fields required for search by first and last name
	2001-09-06 		SWJ	 	Removed the call to flags and set the screen up to work with styles not fonts
	2001-09-20 		SWJ	 	Updated look and feel slightly and got update and add working properly
	2005-11-14  		WAB	 	removed javascript function Alex() and reverted to verifyForm , added a changeScreem function
	2006-02-21		WAB  	changed encoding on frmNext in expression
							JavaScript:verifyForm('#qryFrmTask#','#frmBack#','#frmNext#')
						 	from urlencodedformat to just replacing single quotes.  because javascript errored even when single quotes were urlencoded
	2006-05-25 		NJH	 	(P_FNL017) Altered code to display form elements with standard wrapper tags.
	2007-04-23		AJC  	Organisation Type ID filter code
	2007-09-21		SSS  	I have added a query that allows states and provinces for countries to come from the database if the country does not exists in the database
						 	a filed will be shown for manual input. Also there is a switch in the database to show Abbreviation or the full name.
NJH 2008/01/08 - added address10 where the data is coming from the region table.
	2008/01 		WAB	 	Work to use with new entitynavigation
	2008/05/01 		NJH 	Added ability to set fields to required based on the mandatoryCols parameter
	2008/07/16		NYF		bug 790 - Added trID="Indented" to various <tr>'s	and removed the trailing &nbsp;&nbsp; in the label
	2009/05/06		SSS		P-LEN008Added lat long fields and a button to recalculate the lat longs also added a switch to only show the button for clients that need it
	2009/12/15		NAS		LID - 2910 amended code to use the Name of the person for Creation/Update Date
	2011/02/10		AJC		P-LEN023 - Increased maxlength of specificURL
	2014/05/14		NJH		Salesforce connector - added the accountTypeID field
	2014/09/18		DXC		Case 440298 Increased maxlength of copy paste area
	2015-03-02		RPW		Changed Confirm Changes on Exit to use custom Save, Discard and Cancel confimation box.
	2015/06/22		NJH		Show name rather than abbreviation for address5
	2016/01/26      SB		Removed copy and paste.
    2016/12/07      DAN     Case 453334 - include checkObject JS
	--->

<!--- user rights to access:
		SecurityTask:dataTask
		Permission: Read
---->

<cf_translate>

<cfparam name="frmTask" default="new">

<cfif frmtask is "newp">
	<cfinclude template="persondetail.cfm">
	<CF_ABORT>
<cfelseif frmtask is "back">
	<cfinclude template="loclist.cfm">
	<CF_ABORT>
</cfif>

<cf_includeJavascriptOnce template="/javascript/extExtension.js">

<cfparam name="frmLocationID" default="0">
<cfparam name="frmOrganisationID" default="0">
<cfparam name="frmOrganisationTypeID" default="0"> <!--- 2007-04-23 AJC Organisation Type ID filter code --->
<CFPARAM NAME="qryCrmID" DEFAULT="">
<cfparam name="frmBack" default="locdetail">
<cfparam name="frmnext" default="locdetail">
<cfparam name="message" default="">
<cfparam name="messageType" default="success">
<!--- <cfparam name="locDetailsScreenID" type="string" default=""> --->
<cfset locDetailsScreenID = application.com.settings.getSetting("screens.locDetailsScreenID")>

<cfparam name="request.showAddress6" default="no" type="boolean">	<!--- NJH 2006-12-05 added so that we could hide/display address 6 --->
<cfparam name="request.showAddress10" default="no" type="boolean">   <!--- NJH 2008/01/14 --->

<cfparam name = "phonemask" default="">
<cfparam name = "showlatlogbutton" default="false">

<!--- NJH 2008/05/01 --->
<!--- NJH 2010/10/22 replaced param with settings
<cfparam name = "mandatoryCols" default = "insSiteName,insAddress1,insPostalCode"> --->
<cfparam name = "mandatoryCols" default = "#application.com.settings.getSetting('plo.mandatoryCols')#">

<cfif not checkpermission.level1 gt 0>
	<cfset message="Phr_Sys_YouAreNotAuthorised">
	<cfset messageType="warning">
		<cfinclude template="datamenu.cfm">
	<CF_ABORT>
</cfif>

<cfscript>
	if(structKeyExists(url, "frmLocationID")){
		form.frmLocationID = url.frmLocationID;
	}
</cfscript>


<!--- NJH 2008/05/01
	Create a structure of the fields in the form and set whether they are required or not.
	The fields are defaulted to false. Use the mandatoryCols or the mandatoryLocationDetailCols
	param to set the values in the structure.

	When adding a new field to the form, remember to add the field to this structure.

 --->
<cfif isDefined("mandatoryColsOverride") and len(mandatoryColsOverride) gt 0>
	<cfset mandatoryCols ="">
	<cfloop index="i" list="#mandatoryColsOverride#" delimiters="-">
		<cfset mandatoryCols = listAppend(mandatoryCols,i,",")>
	</cfloop>
</cfif>

<cfparam name="mandatoryLocationDetailCols" default="#mandatoryCols#">

<cfscript>
	requiredFieldsStruct = structNew();
	StructInsert(requiredFieldsStruct, "insSiteName", "false");
	StructInsert(requiredFieldsStruct, "insAddress1", "false");
	StructInsert(requiredFieldsStruct, "insAddress2", "false");
	StructInsert(requiredFieldsStruct, "insAddress3", "false");
	StructInsert(requiredFieldsStruct, "insAddress4", "false");
	StructInsert(requiredFieldsStruct, "insAddress5", "false");
	StructInsert(requiredFieldsStruct, "insAddress6", "false");
	StructInsert(requiredFieldsStruct, "insAddress7", "false");
	StructInsert(requiredFieldsStruct, "insAddress8", "false");
	StructInsert(requiredFieldsStruct, "insAddress9", "false");
	StructInsert(requiredFieldsStruct, "insAddress10", "false");
	StructInsert(requiredFieldsStruct, "insPostalCode", "false");
	StructInsert(requiredFieldsStruct, "insTelephone", "false");
	StructInsert(requiredFieldsStruct, "insFax", "false");
	StructInsert(requiredFieldsStruct, "insLocEmail", "false");
	StructInsert(requiredFieldsStruct, "insSpecificUrl", "false");
	StructInsert(requiredFieldsStruct, "insCountryID", "true");
</cfscript>

<cfloop list="#mandatoryLocationDetailCols#" index="formField">
	<cfif structKeyExists(requiredFieldsStruct,"#formField#")>
		<cfset requiredFieldsStruct[formField] = "true">
	</cfif>
</cfloop>


<!--- <cfinclude template="/templates/qry getcountries.cfm"> --->

<!--- START: NYB 2009-10-02 LHID2719 - added: --->
<cfset getLocationTableDetails =  application.com.dbTools.getTableDetails(tableName="Location",returnColumns="true")>
<cfset rightsJoinInfo = application.com.rights.getRightsFilterQuerySnippet(entityType="location",alias="l")>
<!--- END: NYB 2009-10-02 LHID2719 --->

<cfif frmlocationid is not 0>
	<!--- Find Location record information to update --->
	<!--- 2007-04-23 AJC Organisation Type ID filter code --->
	<cfquery name="getLocationDets" datasource=#application.sitedatasource#>
	SELECT DISTINCT
		<!--- START REPLACED: NYB 2009-10-02 LHID2719 ---
        l.locationID,l.Address1, l.Address2, l.Address3, l.Address4, l.Address5, l.address6, l.address7,
		l.address8, l.address9, l.PostalCode, l.CountryID, l.SiteName, l.OrganisationID,
		l.Telephone, l.Fax, l.SpecificURL, l.Direct, l.Active, l.Created, l.LastUpdated,
		l.locEmail, l.address10, l.latitude, l.longitude,
		!--- WITH: NYB 2009-10-02 LHID2719 --->
		<cfoutput query="getLocationTableDetails">
			l.#column_name#,
		</cfoutput>
		<!--- END: NYB 2009-10-02 LHID2719 --->
		<!--- START 2009/12/15		NAS		LID - 2910 amended code to use the Name of the person for Creation/Update Date --->
		c.dialCode, ugb.Name AS LastUpdatedByName,
		uga.Name AS CreatedByName, o.organisationTypeID,
		case when hq.locationID = l.locationID then 1 else 0 end is_HQ,
		hq.locationID as HQLocationID, hq.sitename as hqSiteName, isNull(hq.address4+',','') +' '+isNull(hq.postalcode+',','')+ ' '+hqCountry.countryDescription as HQAddress,
		#rightsJoinInfo.RIGHTSTABLEALIAS#.level3
		<!--- END 2009/12/15		NAS		LID - 2910 amended code to use the Name of the person for Creation/Update Date --->
	FROM Location l
		LEFT OUTER JOIN UserGroup ugb ON l.LastUpdatedBy = ugb.UserGroupID
		LEFT OUTER JOIN UserGroup uga ON l.CreatedBy = uga.UserGroupID
		INNER JOIN country c ON l.countryID = c.countryID
		INNER JOIN organisation o on l.organisationID = o.organisationID
		left join integerFlagData ifd on ifd.entityID = l.organisationID and ifd.flagID = <cf_queryparam value="#application.com.flag.getFlagStructure(flagID='HQ').flagID#" CFSQLTYPE="CF_SQL_INTEGER" >
		left join (location hq inner join country hqCountry on hq.countryID = hqCountry.countryID) on hq.locationID = ifd.data
		#rightsJoinInfo.join#
	WHERE l.LocationID =  <cf_queryparam value="#frmLocationID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</cfquery>

	<cfset frmorganisationid = getlocationdets.organisationid>

	<cfquery name="getDataSources" datasource=#application.sitedatasource#>
	SELECT d.Description + '-' + ld.RemoteDataSourceID AS DataSource
	  FROM Location AS l, DataSource AS d, LocationDataSource AS ld
	 WHERE d.DataSourceID = ld.DataSourceID
	   AND ld.LocationID = l.LocationID
	   AND l.LocationID =  <cf_queryparam value="#frmLocationID#" CFSQLTYPE="CF_SQL_INTEGER" >
	 ORDER BY d.Description
	</cfquery>

	<cfif getlocationdets.recordcount is 1>
		<!--- user can at least read data for that country,
			but although the user can generally update,
			can the user update data for this specific country?? --->
		<cfif not getlocationdets.level3[1]>
			<cfset message="Phr_Sys_YouAreNotAuthorised (2).">
			<cfset messageType="warning">
			<cfinclude template="datamenu.cfm">
			<CF_ABORT>
		</cfif>

		<!--- if we got out of the loop, the user can update for this country --->

		<cfoutput query="getLocationDets">
			<cfset qryaddress1 = address1>
			<cfset qryaddress2 = address2>
			<cfset qryaddress3 = address3>
			<cfset qryaddress4 = address4>
			<cfset qryaddress5 = address5>
			<cfset qryaddress6 = address6>
			<cfset qryaddress7 = address7>
			<cfset qryaddress8 = address8>
			<cfset qryaddress9 = address9>
			<cfset qryaddress10 = address10>  <!--- NJH 2008/01/14 --->
			<cfset qrypostalcode = postalcode>
			<cfset qrycountryid = countryid>
			<cfset qrysitename = sitename>
			<cfset frmorganisationid = organisationid>
			<cfset qryorganisationTypeID = organisationTypeID><!--- 2007-04-23 AJC Organisation Type ID filter code --->
			<cfset qrytelephone = telephone>
			<cfset qryfax = fax>
			<cfset qryEmail = locEmail>
			<cfset dialCode = dialcode>
			<cfset qryspecificurl = specificurl>
			<cfset qrycreatedby = createdby>
			<cfset qrycreated = created>
			<cfset qrylastupdatedby = lastupdatedby>
			<cfset qrylastupdated = lastupdated>
			<cfset qryAccountTypeID = accountTypeID>
			<cfset qryIs_HQ = is_HQ>
			<cfset hqLocationID = hqLocationID>
			<cfset hqSitename = hqSitename>
			<cfset HQAddress = HQAddress>

			<cfset qryfrmtask = "update">
			<cfset qryCrmID = crmLocID>
			<cfset qryactive = #yesnoformat(active)#>
			<cfset qrydirect = #yesnoformat(direct)#>
		</cfoutput>

	<cfelse>

		<cfset message="Phr_Sys_LocDataNotFound">
		<cfset messageType="info">
		<cfinclude template="datamenu.cfm">
		<CF_ABORT>

	</cfif>

<cfelse>
	<!--- we're adding a record here --->
	<cfquery name="getBasicOrgDetails" datasource="#application.siteDataSOurce#">
		select organisationName, organisation.countryID , organisationTypeID,
			hq.locationID as HQLocationID, hq.sitename as hqSiteName, hq.sitename as hqSiteName, isNull(hq.address4+',','') +' '+isNull(hq.postalcode+',','')+ ' '+hqCountry.countryDescription as HQAddress
		from organisation
			left join integerFlagData ifd on ifd.entityID = organisation.organisationID and ifd.flagID = <cf_queryparam value="#application.com.flag.getFlagStructure(flagID='HQ').flagID#" CFSQLTYPE="CF_SQL_INTEGER" >
			left join (location hq inner join country hqCountry on hq.countryID = hqCountry.countryID) on hq.locationID = ifd.data
		where organisation.organisationID =  <cf_queryparam value="#frmOrganisationID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</cfquery>
	<cfset qrycountryid = getBasicOrgDetails.countryid>
	<cfset qryorganisationTypeID = getBasicOrgDetails.organisationTypeID><!--- 2007-04-23 AJC Organisation Type ID filter code --->
	<cfset qrysitename = getBasicOrgDetails.organisationName>
	<cfset hqLocationID = getBasicOrgDetails.hqLocationID>
	<cfset hqSitename = getBasicOrgDetails.hqSitename>
	<cfset HQAddress = getBasicOrgDetails.HQAddress>
</cfif>


<!--- the following is needed whether we add/update --->


<cfquery name="getCountries" datasource="#application.sitedatasource#">
	SELECT DISTINCT c.CountryID, c.CountryDescription
	  FROM Country AS c
	 WHERE c.CountryID  IN ( <cf_queryparam value="#request.relayCurrentUser.CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	ORDER BY c.CountryDescription ASC
</cfquery>


<cfparam name="task" default="add">

<cfparam name="qryFrmTask" default="add">
<cfparam name="qrylocationID" default="0">
<cfparam name="qryAddress1" default="">
<cfparam name="qryAddress2" default="">
<cfparam name="qryAddress3" default="">
<cfparam name="qryAddress4" default="">
<cfparam name="qryAddress5" default="">
<cfparam name="qryAddress6" default="">
<cfparam name="qryAddress7" default="">
<cfparam name="qryAddress8" default="">
<cfparam name="qryAddress9" default="">
<cfparam name="qryAddress10" default="">  <!--- NJH 2008/01/14 --->
<cfparam name="qryPostalCode" default="">
<cfparam name="qryCountryID" default="0">
<cfparam name="qryOrganisationID" default="0">
<cfparam name="qrySitename" default="">
<cfparam name="qryTelephone" default="">
<cfparam name="qryFax" default="">
<cfparam name="qryEmail" default=""> <!--- NJH 2008/01/08 --->
<cfparam name="dialcode" default="">
<cfparam name="qrySpecificURL" default="">
<cfparam name="qryActive" default="yes">
<cfparam name="qryDirect" default="no">
<cfparam name="qryAccountTypeID" default="1">
<cfparam name="qryLookupID" default="0"> <!--- a list of the selected ids --->
<CFPARAM NAME="thisEntityType" DEFAULT="location">
<cfparam name="qryIs_HQ" default="0">


<!--- 08-10-2007 GAD : get phone mask --->
<cfif val(frmOrganisationID) NEQ 0 and val(qryCountryID) EQ 0 >
	<cfset orgnCountryID =application.com.relayplo.getOrgDetails(frmOrganisationID).countryID>
	<cfset phoneMask = application.com.commonQueries.getCountry(countryid=orgnCountryID).telephoneformat>
<cfelse>
	<cfset phoneMask = application.com.commonQueries.getCountry(countryid=val(qryCountryID)).telephoneformat>
</cfif>

<cf_head>
	<cf_includejavascriptonce template = "/javascript/viewEntity.js">
	<cf_includejavascriptonce template = "/javascript/openWin.js">
	<cf_includejavascriptonce template = "/javascript/checkObject.js"> <!--- Case 453334 --->
	<CFOUTPUT>

	<!--- 2015-03-02	RPW	Added isPrimaryFormDirty(). --->
	<SCRIPT type="text/javascript">

		// WAB function which can be called from the entity navigation
		function submitPrimaryForm() {

			jQuery('##frmSubmit').click();
			return true;

		}

		function isPrimaryFormDirty() {

			var form = document.details;
			return isDirty (form)

		}

		// function changeScreen
		// WAB 2005-11-14 see comment in orgDetail.cfm

		function changeScreen (next,screenTextID) {
			viewentityFlags(#frmLocationID#,1,'',screenTextID)

		}

	</script>
	</cfoutput>
</cf_head>

<cfinclude template="_entityDetailForms.cfm">

<cfset request.relayFormDisplayStyle = "HTML-div">
<cfinclude template="/templates/relayFormJavaScripts.cfm">

<cfform action="locdetailtask.cfm" method="POST" name="details">
<cf_relayFormDisplay>

	<cf_relayFormDisplay class="">
		<!--- here are all the search parameters --->
		<cfinclude template="searchparamdefaults.cfm">
		<cfinclude template="searchparamform.cfm">
		<cfparam name="frmStart" default="1">
		<cfparam name="frmStartp" default="1">

		<cfif frmlocationid is not 0>
			<cfif getlocationdets.recordcount is 1>
				<CF_relayFormElementDisplay relayFormElementType="HTML" currentvalue="#frmLocationID#" fieldname=""  label="Phr_Sys_LocationID">
			</cfif>
		</cfif>

		<!--- NJH 2016/03/22 sugar 446863 - increase length of site name --->
		<CF_relayFormElementDisplay relayFormElementType="text" currentvalue="#qrySitename#" fieldname="insSiteName" label="Phr_Sys_SiteName" size="50" maxlength="255" required="#requiredFieldsStruct['insSiteName']#">

		<cfquery name="getAccountTypes">
			select organisationTypeID,'phr_sys_'+TypeTextID as TypeTextID from organisationType where active=1 order by sortIndex
		</cfquery>

		<CF_relayFormElementDisplay relayFormElementType="select" currentvalue="#qryAccountTypeID#" fieldname="accountTypeID" label="Phr_Sys_accountType" query="#getAccountTypes#" display="typeTextID" value="organisationTypeID">
		<cfset HQLocationText = "<a onclick='javascript:viewLocation(#hqLocationId#,null,{openInOwnTab:true});return false;'>#htmlEditFormat(HQSitename)# (#htmlEditFormat(HQAddress)#)</a>">
		<CF_relayFormElementDisplay relayFormElementType="checkbox" currentvalue="#qryis_HQ#" fieldname="is_hq" label="Phr_Sys_headquarters" value="1" noteText="#IIF(hqlocationID neq '' and hqlocationID neq frmLocationID,DE(HQLocationText),DE(''))#" noteTextPosition="right" noteTextImage="no">

		<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#hqLocationID#" fieldname="hqLocationID">

		<CF_relayFormElementDisplay relayFormElementType="text" currentvalue="#qryAddress1#" fieldname="insAddress1" label="Phr_Sys_Address" maxlength="160" required="#requiredFieldsStruct['insAddress1']#">
		<CF_relayFormElementDisplay relayFormElementType="text" currentvalue="#qryAddress2#" fieldname="insAddress2" label="" maxlength="160" required="#requiredFieldsStruct['insAddress2']#">
		<CF_relayFormElementDisplay relayFormElementType="text" currentvalue="#qryAddress3#" fieldname="insAddress3" label="" maxlength="160" required="#requiredFieldsStruct['insAddress3']#">
		<CF_relayFormElementDisplay relayFormElementType="text" currentvalue="#qryAddress4#" fieldname="insAddress4" trID="Indented" label="Phr_Sys_TownCity" maxlength="160" required="#requiredFieldsStruct['insAddress4']#">

		<cfquery name="getProvince" datasource="#application.siteDataSource#">
			select p.name, p.Abbreviation
			from Province p
			where countryID =  <cf_queryparam value="#val(qrycountryid)#" CFSQLTYPE="CF_SQL_INTEGER" >
			order by p.name
		</cfquery>
		<cfif getProvince.recordcount EQ 0>
			<CF_relayFormElementDisplay relayFormElementType="text" currentvalue="#qryAddress5#" fieldname="insAddress5" trID="Indented" label="Phr_Sys_Region" maxlength="160" required="#requiredFieldsStruct['insAddress5']#">
		<cfelse>
			<CF_relayFormElementDisplay relayFormElementType="select" currentValue="#qryAddress5#" fieldName="insAddress5" trID="Indented" label="Phr_Sys_State" query="#getProvince#" display="name" value="Abbreviation" nullText="phr_PleaseChooseOne" nullValue="" required="#requiredFieldsStruct['insAddress5']#">
		</cfif>
		<cfif request.showAddress6>
			<CF_relayFormElementDisplay relayFormElementType="text" currentvalue="#qryAddress6#" fieldname="insAddress6" trID="Indented" label="Phr_Sys_Province" required="#requiredFieldsStruct['insAddress6']#">
		</cfif>

		<cfif qrycountryid is 13>
			<CF_relayFormElementDisplay relayFormElementType="text" currentvalue="#qryAddress7#" fieldname="insAddress7" label="Phr_Sys_POBox" required="#requiredFieldsStruct['insAddress7']#">
			<CF_relayFormElementDisplay relayFormElementType="text" currentvalue="#qryAddress8#" fieldname="insAddress8" label="Phr_Sys_POBoxPostcode" required="#requiredFieldsStruct['insAddress8']#">
			<CF_relayFormElementDisplay relayFormElementType="text" currentvalue="#qryAddress9#" fieldname="insAddress9" label="Phr_Sys_POBoxTown" required="#requiredFieldsStruct['insAddress9']#">
		</cfif>
		<!--- SSS p-len008 2009/05/05 added this code for lat long recalculate BEGIN--->
		<cfif isdefined("getLocationDets")>
			<CF_relayFormElementDisplay relayFormElementType="text" currentvalue="#getLocationDets.latitude#" fieldname="insLatitude" label="Phr_Sys_latitude" disabled="true">
			<cf_relayFormElementDisplay relayFormElementType="html" fieldname="" label="Phr_Sys_longitude" currentValue="">
			<cf_relayFormElement relayFormElementType="text" currentvalue="#getLocationDets.longitude#" fieldname="insLongitude" label="Phr_Sys_longitude" disabled="true">
			<cfif showlatlogbutton>
			<cf_relayFormElement relayFormElementType="button" fieldname="frmSaveForm" label="" currentValue="reset Lat/Long" onclick="javascript:resetlatlong();">
			</cfif>
		</cf_relayFormElementDisplay>
		</cfif>
		<!--- p-len008 END--->
		<!--- NJH 2008/01/08 used currently for the Trend Locator. Store the region name in address10--->
		<cfif (structKeyExists(request,"showAddress10") and request.showAddress10)>
			<cfquery name="getRegion" datasource="#application.siteDataSource#">
				select r.name from region r	where countryID =  <cf_queryparam value="#val(qrycountryid)#" CFSQLTYPE="CF_SQL_INTEGER" >  order by r.name
			</cfquery>
			<cfif getRegion.recordcount eq 0>
				<cf_relayFormElementDisplay relayFormElementType="text" currentvalue="#qryAddress10#" fieldname="insAddress10" trID="Indented" label="Phr_Sys_GreaterRegion" required="#requiredFieldsStruct['insAddress10']#">
			<cfelse>
				<cf_relayFormElementDisplay relayFormElementType="select" currentValue="#qryAddress10#" fieldName="insAddress10" trID="Indented" label="Phr_Sys_GreaterRegion" query="#getRegion#" display="name" value="name" nullText="phr_PleaseChooseOne" nullValue="" required="#requiredFieldsStruct['insAddress10']#">
			</cfif>
		</cfif>

		<cfset mapNote="">
		<cfif qrypostalcode neq "" and qrycountryid eq 9>
			<cfset mapNote="<a href='http://uk.multimap.com/p/browse.cgi?pc=#Replace(qryPostalCode, ' ', '','all')#' target='multimap'>Phr_Sys_Map</a>">
		</cfif>

		<CF_relayFormElementDisplay relayFormElementType="text" currentvalue="#qryPostalCode#" fieldname="insPostalCode" label="Phr_Sys_PostalCodeZip" size="8" maxlength="20" noteText="#mapNote#" noteTextPosition="right" noteTextImage="no" required="#requiredFieldsStruct['insPostalCode']#">
		<CF_relayFormElementDisplay relayFormElementType="select" currentValue="#qryCountryID#" fieldName="insCountryID" label="Phr_Sys_Country" query="#getCountries#" display="CountryDescription" value="CountryID" nullText="Phr_Sys_SelectACountry" nullValue="" required="#requiredFieldsStruct['insCountryID']#" >

		<cfsavecontent variable="intlCodeLabel_html">
			<cfoutput>
				<cfif trim(#dialCode#) neq ''>: (Phr_Sys_IntlCode: +#dialCode#)</cfif>
			</cfoutput>
		</cfsavecontent>
		<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#phonemask#" fieldname="phonemask">
		<CF_relayFormElementDisplay relayFormElementType="text" currentvalue="#qryTelephone#" fieldname="insTelephone" label="Phr_Sys_Switchboard#intlCodeLabel_html#" mask="#phoneMask#" required="#requiredFieldsStruct['insTelephone']#">
		<CF_relayFormElementDisplay relayFormElementType="text" currentvalue="#qryFax#" fieldname="insFax" label="Phr_Sys_MainFaxNo" NoteText="Phr_Sys_PhoneNoFormat" NoteTextPosition="bottom" mask="#htmleditformat(phoneMask)#" required="#requiredFieldsStruct['insFax']#">
		<cf_relayFormElementDisplay relayFormElementType="text" currentvalue="#qryEmail#" fieldname="insLocEmail" label="Phr_Sys_Email" required="#requiredFieldsStruct['insLocEmail']#" validate="email">
		<CF_relayFormElementDisplay relayFormElementType="text" currentvalue="#qrySpecificURL#" fieldname="insSpecificURL" label="Phr_Sys_WebAddressNOHTTP" maxlength="500" required="#requiredFieldsStruct['insSpecificURL']#"> <!--- NJH 2009/02/17 verifyURL does not appear to be defined. Causing js errors.. onBlur="verifyURL" --->

		<cfif application.com.relayMenu.isModuleActive("connector")>
			<cfset synchingStatus = application.getObject("connector.com.connector").getEntitySynchingStatus(entityType="location",entityID=frmLocationID)>

			<CF_relayFormElementDisplay relayFormElementType="text" fieldName="crmID" currentValue="#qryCrmID#" label="phr_Sys_#application.com.settings.getSetting('connector.type')#ID" disabled="true" NoteTextImageSrc="#synchingStatus.statusImage#" NoteText="#synchingStatus.status#" noteTextPosition="right">
		</cfif>

		<cf_querySim>
			getStati
			display,value
			Phr_Sys_Yes|1
			Phr_Sys_No|0
		</cf_querySim>
		<!--- <CF_relayFormElementDisplay relayFormElementType="radio" currentValue="#IIF(YesNoFormat(qrydirect) IS 'yes',1,0)#" fieldName="insDirect" label="Phr_Sys_DirectPartner?" query="#getStati#" display="display" value="value">	 --->
		<CF_relayFormElementDisplay relayFormElementType="radio" currentValue="#IIF(YesNoFormat(qryactive) IS 'yes',1,0)#" fieldName="insActive" label="Phr_Sys_Active?" query="#getStati#" display="display" value="value">

	<!--- <!--- New Flags --->
			<tr>
				<td valign="TOP" colspan=2><br>
				<cfset flagmethod = "edit">
				<cfset entitytype = 1>
				<cfset thisentity = frmlocationid>
				<cfinclude template="../flags/allFlag.cfm">
				<br><br></td>
			</tr>
	 --->
	 	<cfif frmlocationid is not 0>
			<cfif getlocationdets.recordcount is 1>
				<CF_relayFormElementDisplay relayFormElementType="HTML" currentValue="#Replace(ValueList(getDataSources.DataSource), ',', '<br>', 'ALL')#" fieldName="" label="Phr_Sys_RemoteDataSources">
				<!--- START 2009/12/15		NAS		LID - 2910 amended code to use the Name of the person for Creation/Update Date --->
				<CF_relayFormElementDisplay relayFormElementType="HTML" currentValue="#getLocationDets.CreatedByName# - #DateFormat(getLocationDets.Created, 'dd mmm yyyy')# #TimeFormat(getLocationDets.LastUpdated, 'HH:mm:ss')#" fieldName="" label="Phr_Sys_CreatedBy">
				<CF_relayFormElementDisplay relayFormElementType="HTML" currentValue="#getLocationDets.LastUpdatedByName# - #DateFormat(getLocationDets.LastUpdated, 'dd mmm yyyy')# #TimeFormat(getLocationDets.LastUpdated, 'HH:mm:ss')#" fieldName="" label="Phr_Sys_LastUpdatedBy">
				<!--- END 2009/12/15		NAS		LID - 2910 amended code to use the Name of the person for Creation/Update Date --->
				<CF_relayFormElementDisplay relayFormElementType="hidden" currentValue="#CreateODBCDateTime(getLocationDets.LastUpdated)#" fieldName="insLastUpdated" label="">
			</cfif>
		</cfif>
	</cf_relayFormDisplay>

<!--- NJH 2007/01/26 --->
<cfif frmlocationID is not 0 and getLocationDets.recordCount>
	<cfif locDetailsScreenID neq "">
		<cf_getrecord entitytype="location" entityid = "#frmlocationID#" getparents getrights>
		<!--- 2007-05-02 AJC Check for organisation Type specific profile summary screen & if none, checks the past screen too --->
		<cfscript>
			locDetailsScreenID = application.com.screens.getScreenForOrgType("#locDetailsScreenID#",organisation.organisationID,"");
		</cfscript>

		<CF_relayFormElementDisplay relayFormElementType="SCREEN" currentvalue="">
			<cf_ascreen formName = "mainForm">
				<cf_ascreenitem screenid="#locDetailsScreenID#"
						method="edit"
						location="#getlocationdets#"
						organisation="#organisation#"
				>
			</cf_ascreen>
		</CF_relayFormElementDisplay>
	</cfif>
</cfif>

<cf_relayFormElementDisplay relayFormElementType="submit" fieldname="frmSubmit" currentValue="Save" label="">
<!--- all these fields moved to end of form because otherwise the  masking code done by cfform tries to focus on the first item in the field (which is hidden) and ends up focusing on one of the phon numbers and scrolling down--->
		<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#qryFrmTask#" fieldname="frmTask">
		<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#frmDupLocs#" fieldname="frmDupLocs">
		<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#frmDupPeople#" fieldname="frmDupPeople">
		<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#frmStart#" fieldname="frmStart">
		<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#frmStartp#" fieldname="frmStartp">
		<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#frmLocationID#" fieldname="frmLocationID">
		<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#frmOrganisationID#" fieldname="frmOrganisationID">
		<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#frmBack#" fieldname="frmBack">
		<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#frmNext#" fieldname="frmNext">
</cf_relayFormDisplay>

</cfform>



<cfoutput>
<script language="javascript">
function resetlatlong() {
	page = '/webservices/relayLocatorWS.cfc?wsdl&method=SetandGetLatlong'
    page = page + '&locationID=#frmLocationID#'

    var myAjax = new Ajax.Request(
    page,
    {
    	method: 'post',
        parameters: 'returnFormat=json&_cf_nodebug=true',
        evalJSON: 'force',
        debug : false,
        asynchronous: false
     }
     );
     //alert(myAjax.transport.responseText);
     //document.frmCompanySearch.frmShowLanguageID.value = parseInt(myAjax.transport.responseText);
     document.details.insLatitude.value=myAjax.transport.responseText.evalJSON().LATITUDE;
     document.details.insLongitude.value = myAjax.transport.responseText.evalJSON().LONGITUDE;
}
</script>

</cfoutput>
</cf_translate>