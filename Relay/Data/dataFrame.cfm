<!--- �Relayware. All Rights Reserved 2014 --->
<!--- NM 2005-05-27 - Speeding up results: Does Org search here in order to pass first org to persList.cfm
	WAB 2005-10-19  - bug fix where two frmOrganisationIDs get passed to the second frame

	WAB 2007/12     changed to use new bottom frame
	WAB 2008/01/08	if have a person criteria then show person in bottom frame
	WAB 2008/12/09  BUG-1440 added reload of recent searches after a successful search (New UI)
	WAB 2009/01/20  BUG-1492 (1440) added reload of recent searches after a successful search (OLD INTERFACE)
	WAB 2009/02/23 added test for window.parent.menuleft to prevent a JS error if running outside of our UI
	NJH 2012/02/27	CASE 425212: add a unique ID to each frameset instance
	2014-10-08	RPW	CORE-806 Refreshing the org screen in three pane view lists loads of other organisations.
	NJH 2016/06/09	JIRA PROD2016-342 deal with search for locations. Tidied up we set the entityFrame struct.
--->


<cfinclude template="qrySearchOrgs.cfm">

<!--- this will be used in the javascript function below
	DAM - 21 Feb 2002 amended this to make sure the other windows
	refreshed for the first org in the list
--->
	<CFSET tempOrgid = getOrganisations.organisationID>

	<!--- JIRA PROD2016-342 NJH 2016/06/09 - add locations to search results. Deal with both locations and people in first if statement --->
	<cfset entityFrame = {entityTypeID=qryGetFirstEntity.entityTypeID,entityID = qryGetFirstEntity.entityID}>
	<cfif not qryGetFirstEntity.recordCount>
		<cfset entityFrame = {entityTypeID=1,entityID = 0}>
	</cfif>

	<!--- <cfif isdefined("qrygetFirstEntity") and qrygetFirstEntity.recordCount gt 0 >
		<!--- in this case we have searched on a person criteria so we want to show the person details in the bottom frame  --->
		<cfset entityFrame.entityTypeID = qrygetFirstEntity.entityTypeID>
		<cfset entityFrame.entityID = qrygetFirstEntity.entityID>
	<cfelse>
		<cfset entityFrame.entityTypeID = 2>
		<cfset entityFrame.entityID = getOrganisations.organisationID>
	</cfif> --->


<CFPARAM NAME="frmDupPer" DEFAULT="0">

<!-- frames
2003-06-10 Modified parameter function to work with any URL variable
DAM 2001 December
Added artificial construction of fieldnames variable where the required
perameter is passed in the URL
2005-05-26 Add org query in order to pass it to perslist.cfm in hopes of speeding up the search.
-->





<!--- convert URL params to form variables --->
<CFIF isStruct(url) and NOT IsDefined("fieldNames")>
<!--- if a URL string is Defined (tested using isStruct) --->
	<CFSET Fields="">
	<CFLOOP COLLECTION="#URL#" ITEM="VarName">
		<!--- loop through the URL collection and create form variables for each url param --->
		<cfset form[VarName] = URL[VarName]>
		<CFSET Fields = listAppend(Fields,VarName)>
		<!--- set the fieldnames variable to a list of url 'fields' --->
		<cfset fieldNames = fields>
	</cfloop>
</cfif>

<!---
<CFIF IsDefined("fieldNames")>
	<cfset fieldNames = fieldNames & "&" & fields>
<cfelse>
	<cfset fieldNames = fields>
</CFIF>

<CFIF IsDefined("url.frmsrchOrgID") and NOT IsDefined("fieldNames") and not isdefined ("form.frmsrchOrgID")>
	<CFSET form.frmsrchOrgID = url.frmsrchOrgID>
	<CFSET fieldNames="frmsrchOrgID">
</CFIF> --->

<!--- <CF_checkMandatoryParams callingTemplate="dataFrame" relayParam="fieldNames"> --->

<!--- convert form variables to variable URLstring --->
<CF_FormFieldsURLString>

<cfset persListParams = "">

<CFIF session.qryGetOrganisations.RecordCount GT 0>

	<CFOUTPUT QUERY="session.qryGetOrganisations" STARTROW=1 MAXROWS=1>
		<!--- gather a list of all the organisations displayed on this page --->
		<CFSET frmLocsOnPage = ListAppend(frmLocsOnPage,OrganisationID)>
	</CFOUTPUT>

	<!--- WAB 2005-19-10 when this page is called from the selectionreview page using the form on _editMainEntities it passes frmOrganisationid in the URL string and then we add another, so we end up with two, so we need to remove the one in the urlstring --->
	<cfset URLString = reReplaceNoCase(URLString,"frmOrganisationID=[0-9]*[&|\Z]","")>

	<cfif Len(URLString) GT 0>
		<cfset persListParams = "#URLString#&frmMoveFreeze=#frmMoveFreeze#&frmDupLocs=#frmDupLocs#&frmDupPer=#frmDupPer#&frmOrganisationID=#getOrganisations.organisationID#&frmStart=#Variables.displaystart#&frmBack=loclist">
	<cfelse>
		<cfset persListParams = "frmMoveFreeze=#frmMoveFreeze#&frmDupLocs=#frmDupLocs#&frmDupPer=#frmDupPer#&frmOrganisationID=#getOrganisations.organisationID#&frmStart=#Variables.displaystart#&frmBack=loclist">
	</cfif>
	<cfif isDefined("lLocationIDs")>
		<cfset persListParams = "#persListParams#&frmLocationIDs=#lLocationIDs#">
	</cfif>

</CFIF>

<!---
	NJH 2012/02/27 CASE 425212: Added two new js functions getOrgFrame and getPerFrame
	We use a random number to generate a unique frameset it, as Chrome doesn't handle non-unique names/ids in different tabs in the same way that
	FF and IE. (ie. It will load the contents of the second tab into the first tab)
 --->
<cfset randNum = int(rand() * 1000)>

<cfoutput>
	<script>

		function toggleVisibility( divName, callerObject )
		{
		  document.all[divName].style.display = ( document.all[divName].style.display == "" ) ? "none" : "";
		  if ( callerObject )
		  {
			callerObject.src = ( document.all[divName].style.display == "" ) ? iconDoubleArrowUp.src : iconDoubleArrowDown.src;
		  }
		  return true;
		}
		var iconDoubleArrowUp = new Image();
		var iconDoubleArrowDown = new Image();

		function eventWindowOnLoad( callerObject )
		{
		  // 2008/01/24 WAB removed this line, there may be a reason for doing this with an on load function, but I can't think of it and it keeps going wrong, the frame never loads, have put url back into the frameset
		  // frames['OrgList'].window.location.href = "orgListv3.cfm?fromFrame=true&#URLString#";
		  iconDoubleArrowUp.src = "#request.currentsite.httpprotocol##jsStringFormat(cgi.SERVER_NAME)#/images/MISC/iconDoubleArrowUp.gif";
		  iconDoubleArrowDown.src = "#request.currentsite.httpprotocol##jsStringFormat(cgi.SERVER_NAME)#/images/MISC/iconDoubleArrowDown.gif";
		}

		function getOrgFrame() {
			return OrgList#jsStringFormat(randNum)#;
		}

		function getPerFrame() {
			return PersList#jsStringFormat(randNum)#;
		}

		function getEntityFrame() {
			return Detail#jsStringFormat(randNum)#;
		}
	</script>
</cfoutput>

<cf_includeJavascriptOnce template = "/javascript/extExtension.js">

<cfset showDeletedRecords = false>
<cfif isDefined("frmDeleted") and isBoolean(frmDeleted) and frmDeleted>
	<cfset showDeletedRecords = true>
</cfif>

<cfscript>
	//2014-10-08	RPW	CORE-806 Refreshing the org screen in three pane view lists loads of other organisations.
	if (structKeyExists(URL,"frmsrchOrgID") && IsNumeric(URL.frmsrchOrgID)) {
		variables.orgFrameSrc = "orgListv3.cfm?fromFrame=true&frameID=#randNum#&showDeletedRecords=#showDeletedRecords#&frmsrchOrgID=#URL.frmsrchOrgID#";
	} else {
		variables.orgFrameSrc = "orgListv3.cfm?fromFrame=true&frameID=#randNum#&showDeletedRecords=#showDeletedRecords#";
	}
</cfscript>

<CFOUTPUT>
<FRAMESET onLoad="eventWindowOnLoad(this);" ROWS="40%,*" FRAMESPACING="0" FRAMEBORDER="1">
	<FRAMESET cols="40%,*" FRAMEBORDER="1">
		<FRAME SRC="#variables.orgFrameSrc#" NAME="OrgList#randNum#" MARGINWIDTH="0" MARGINHEIGHT="0">
		<FRAME SRC="persList.cfm?entityTypeID=#entityFrame.entityTypeID#&entityID=#entityFrame.entityID#&frameID=#randNum#&showDeletedRecords=#showDeletedRecords#" NAME="PersList#randNum#" MARGINWIDTH="0" MARGINHEIGHT="0">
	</FRAMESET>
    <FRAME SRC="entityFrame.cfm?frmentityTypeID=#entityFrame.entityTypeID#&frmentityId=#entityFrame.entityID#&frameID=#randNum#" NAME="Detail#randNum#" MARGINWIDTH="0" MARGINHEIGHT="0">
</FRAMESET><noframes>You must enable frames to use this part of the system.</noframes>
</CFOUTPUT>