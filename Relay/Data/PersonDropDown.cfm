<!--- �Relayware. All Rights Reserved 2014 --->
<!---
	Drop Down box for use on person screens
	WAB 2000-11-29 Altered check for deletion rights to use a View (which used rights groups)
	WAB	2001-03-26		Added a few new functions
	WAB  2005-06-01	converted to cf_translate
	put rights into query for link to regprocesses
	WAB 2007-06-12	altered cf_selectboxscroll so that doesn't require the select box to be on a form of known name
	WAB 2007-09-24	  voided all openWin stuff
	SWJ 2008-03-27	changed rights management to use standard methods
	2008/06/13      SSS     Merge tasks code has been added now these merge taks are required for a person to merge
	WAB 2008/12/01
	NYB 2009-03-12 POL Deletion Process
	PPB	2011/07/19 LEN024 added permissions
	NYB	2011-10-25  LHID7095,7963 - replaced multiple if's call around Delete Check People to [new] function getPersonDeleteRights
	- to help achieve continuity throughout the system around delete rights
	NJH	2015/02/13	Jira Fifteen-165 - removed redundant selection option
	2017-02-13		DBB			ENT-6: Hiding screen elements for read only users
	--->

<cfset dataTaskL2 = application.com.login.checkInternalPermissions("DataTask","Level2")><!---DBB ENT-6--->
<!--- KT 1999-11-25 added to set event manager variable --->
<CFQUERY NAME="GetRecordRights" datasource="#application.siteDataSource#">
	SELECT rr.RecordID
	FROM RecordRights AS rr, EventDetail AS ed
	WHERE rr.Entity = 'Flag'
	AND rr.UserGroupID =  <cf_queryparam value="#ListGetAt(Cookie.user, 2, "-")#" CFSQLTYPE="CF_SQL_INTEGER" >
	AND rr.RecordID = ed.FlagID
</CFQUERY>
<CFSET EventManager = 0>
<CFIF GetRecordRights.RecordCount GT 0>
	<CFSET EventManager = 1>
</CFIF>
<cf_includeJavaScriptOnce template="/javascript/dropdownFunctions.js">
<cf_includeJavaScriptOnce template="/javascript/checkBoxFunctions.js">
<cf_includeJavaScriptOnce template="/javascript/openwin.js">
<cf_translate>
	<!---
		WAB 2007-06-12 removed form so that it can be used within other forms
		<form name="personFunctions">
		--->
	<CFOUTPUT>
		<div class="form-horizontal withBorder">
			<div class="form-group">
				<div class="col-xs-12 col-sm-6">
					<SELECT NAME="personDropDown" id="personDropDown" class="form-control" SIZE="1">
						<option value=none>
							Phr_Sys_PersonFunctions
						</option>
						<cfif application.com.login.checkInternalPermissions("selectTask","Level2")>
							<optGroup label="Phr_Sys_Selections">
								<option value="JavaScript: checks = saveChecks('Person'); if (checks!='0') {  void(newWindow = openWin( '../selection/selectalter.cfm?frmpersonids='+checks+'&frmtask=add&frmruninpopup=true' ,'PopUp', 'width=450,height=450,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1' )); } else {noChecks('Person')}">
									Phr_Sys_AddCheckedPeopleToASelection
								<option value="JavaScript: checks = saveChecks('Person'); if (checks!='0') {  void(newWindow = openWin( '../selection/selectalter.cfm?frmpersonids='+checks+'&frmtask=remove&frmruninpopup=true' ,'PopUp', 'width=450,height=450,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1' )); } else {noChecks('Person')}">
									Phr_Sys_RemoveCheckedPeopleFromASelection
								<option value="JavaScript: checks = saveChecks('Person'); if (checks!='0') {  void(newWindow = openWin( '../selection/selectalter.cfm?frmpersonids='+checks+'&frmtask=save&frmruninpopup=true','PopUp','width=450,height=450,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1' )); } else {noChecks('Person')}">
									Phr_Sys_SaveCheckedPeopleAsNewSelection
							</optGroup>
						</cfif>
						<cfif application.com.login.checkInternalPermissions("commtask","Level2")> <optGroup label="Phr_Sys_Communications"> <!--- PJP 13/11/2012: CASE 427937 : 'Removed send checked people a message' ----> <!---<option value="JavaScript: checks = saveChecks('Person'); if (checks!='0') {  void(newWindow = openWin( '/communicate/commheader.cfm?frmpersonids='+checks+'&frmselectid=0&frmruninpopup=true&qrycommformlid=2','PopUp','width=650,height=500,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1' )); } else {noChecks('Person')}">Phr_Sys_SendCheckedPeopleANewMessage---> <option value="JavaScript: checks = saveChecks('Person');  if (checks!='0')  {  void(newWindow = openWin( '../download/downheader.cfm?frmpersonids='+checks +'&frmselectid=0&frmruninpopup=true&qrycommformlid=4','PopUp','width=650,height=500,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1' )); } else {noChecks('Person')}">Phr_Sys_DownloadCheckedPeople <option value="JavaScript: checks = saveChecks('Person');if (checks!='0') {  void(newWindow = openWin( '/communicate/selectcomm.cfm?frmpersonids='+checks+'&frmselectid=0&frmruninpopup=true&qrycommformlid=2','PopUp','width=780,height=450,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1' )); } else {noChecks('Person')}">Phr_Sys_SendCheckedPeoplePreviousComm </optGroup> </cfif>
						<optGroup label="Phr_Sys_Utilities">
							<!--- 2011-10-25 NYB LHID7095,7963 - replaced multiple if's call to [new] function getPersonDeleteRights - to introduce continuity throughout the system around delete rights --->
							<cfif application.com.rights.getPersonDeleteRights()>
								<!--- START:  NYB 2009-03-12 POL Deletion Process - replaced: ---
									<option value="JavaScript: checks = saveChecks('Person');if (checks!='0') {  void(newWindow = openWin( '../flags/updateflag.cfm?frmentityid='+checks+'&frmflagtextid=DeletePerson&frmruninpopup=true&frmflagvalue=1&frmshowfeedback=','PopUp','width=450,height=450,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1'));  } else {noChecks('Person')}">Phr_Sys_DeleteCheckedPeople
									!--- WITH:  NYB 2009-03-12 POL Deletion Process --->
								<option value="JavaScript: if (saveChecks('Person')!='0') { void(newWindow = openWin( '../data/checkEntityForDeletion.cfm?frmentityid='+saveChecks('Person')+'&frmflagtextid=DeletePerson&frmruninpopup=true&frmflagvalue=1&frmshowfeedback=','PopUp','width=850,height=450,toolbar=0,Person=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1'));  } else {noChecks('Person')}">
									Phr_Sys_DeleteCheckedPeople
									<!--- END:  NYB 2009-03-12 POL Deletion Process --->
							</cfif>
							<cfif application.com.login.checkInternalPermissions("CreateTask").recordcount eq 0 OR application.com.login.checkInternalPermissions("CreateTask","Level3")> <!--- 2011/07/19 PPB LEN024 added permission; we only need to check it if the Task exists ---> 
								<!---START: DBB ENT-6--->
								<cfif dataTaskL2>
									<option value="JavaScript: checks = saveChecks('Person');if (checks!='0') {  void(newWindow = openWin( '../data/movePersonToDiffOrg.cfm?frmPersonIds='+checks,'PopUp','width=600,height=450,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1'));  } else {noChecks('Person')}">Phr_Sys_MovePeopleToDifferentOrg 
								</cfif>
								<!---END: DBB ENT-6--->
							</cfif> 
							<cfif (application.com.login.checkInternalPermissions("mergeTask","Level2") or application.com.login.checkInternalPermissions("mergePerTask","Level2")) and application.com.settings.getSetting ("plo.mergeAndDelete.manualMerge.person")> 
								<option value="JavaScript: checks = saveChecks('Person'); if (checks!='0') { void(openWin( '../data/personDedupe.cfm?frmDupPeople='+checks+'&frmruninpopup=true&refreshLocList=yes','PopUp','width=750,height=550,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1'));  } else {noChecks('Person')};">Phr_Sys_Merge 
							</cfif> <!--- LID 3759 NJH 2011/09/16 - removed this functionality from dropdown
								<cfif application.com.login.checkInternalPermissions("UserTask","Level1")>
								<!--- 		<option value="JavaScript: checks = saveChecks('Person');if (checks!='0')  {  void(newWindow = openWin( '../InFocusTools/MagicNumber.cfm?frmPersonId='+checks+'&frmruninpopup=true','PopUp','width=650,height=500,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1'));  } else {noChecks('Person')}">Phr_Sys_AccessCheckedPeoplesMagicNumber --->
							<option value="JavaScript: checks = saveChecks('Person');if (checks!='0') {  void(newWindow = openWin( '/utilities/createUserNamePassword.cfm?frmPersonid='+checks,'PopUp','width=450,height=450,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1'));  } else {noChecks('Person')}">
								Phr_Sys_CreateUsernameAndPassword </cfif>--->
						</optGroup>
						<optGroup label="Phr_Sys_Profiles">
							<cfif application.com.login.checkInternalPermissions("ProfileTask","Level2")>
								<!--- 2011/07/19 PPB LEN024 added permission --->
								<option value="JavaScript: checks=saveChecks('Person') ; if (checks!='0')  {  void(newWindow = openWin( '../flags/SetFlagsForIDList.cfm?frmentityid='+checks+'&frmEntityTypeID=0','PopUp','width=450,height=450,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1'));  } else {noChecks('People')}">
									Phr_Sys_SetProfileForCheckedPeople
							</cfif>
							<!--- MDC 2006-10-24 - This can now be easily accessed from the left menu.---> <cfif application.com.login.checkInternalPermissions("ApprovalTask","Level1")> <cfquery name="getRegProcesses" datasource="#application.siteDataSource#">
						select distinct case when charindex('_',flaggroupTextID) = 0 then '' else substring(flaggroupTextID,1,charindex('_',flaggroupTextID)) end as Prefix,
								 name
						 from
							 flagGroup fg left join (flaggrouprights fgr inner join rightsGroup rg on fgr.userid = rg.usergroupid and rg.personid = #request.relayCurrentUser.personid# AND (fgr.EDIT = 1 or fgr.viewing=1)) on fg.flaggroupid = fgr.flaggroupid
						where
							flaggroupTextID like '%orgApprovalStatus'
							and
							case when fg.editaccessrights = 0 and fg.edit = 1 then 1 when fg.editaccessrights = 1 and fgr.edit = 1 then 1 else 0 end  = 1
						</cfquery> <cfloop query="getRegProcesses"><!--- checks = saveChecks('Person');if (checks != '0' ) {  void(newWindow = openWin( '/approvals/approvalProcess.cfm?frmProcessid=#prefix#RegApproval&frmstepid=0&frmPersonID='+checks,'PopUp','width=600,height=660,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1')); } else if (checks ='0') {noChecks('Person');} else if (checks >'0') {multiChecks('Person')} ---> <option value="JavaScript:checks = saveChecks('Person');if (checks != '0' ) {  void(newWindow = openWin( '/approvals/approvalProcess.cfm?frmProcessid=#prefix#RegApproval&frmstepid=0&frmPersonID='+checks,'PopUp','width=600,height=660,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1')); } else  {noChecks('Person');} ">Phr_Sys_ApprovePer <cfif prefix neq "">(#htmleditformat(prefix)#)</cfif> </cfloop> </cfif>
						</optGroup>
						<cfif application.com.login.checkInternalPermissions("eventTask","Level2")> <optGroup label="Phr_Sys_Events"> <option value="JavaScript: checks = saveChecks('Person'); if (checks!='0') {  void(newWindow = openWin( '../events/eventAddPopup.cfm?frmpersonids='+checks+'&frmruninpopup=true','PopUp','width=450,height=450,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1' )); } else {noChecks('Person')}">Phr_Sys_AddCheckedPeopleToAnEvent <option value="JavaScript: checks = saveChecks('Person');  if (checks!='0') {  void(newWindow = openWin( '../events/eventAddPopup.cfm?frmpersonids='+checks+'&frmruninpopup=true&frmRegister=1','PopUp','width=650,height=500,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1' )); } else {noChecks()}">Phr_Sys_AddCheckedPeopleToAnEventAndRegister </optGroup> </cfif> <!--- TRD102 SKP 2010-11-16 ---> <cf_include template="\code\CFTemplates\CustomFunctionsPerson.cfm" checkIfExists="true"> <!--- MDC 2003-09-03 - You can now see these reports in the persondata screen.
							<option value=none>Phr_Sys_Reports
							<cfif isDefined("session. securityLevels") and listFindNoCase(session. securityLevels,"CommTask(3)") neq 0>
							<option value="JavaScript: checks = saveChecks('Person'); if (checks!='0') { newWindow = openWin( '/report/personComms.cfm?frmpersonid='+checks+'&frmruninpopup=true','PopUp','width=650,height=500,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1');  } else {noChecks('Person')}">Phr_Sys_ReportCommsCheckedPeopleSent
							</cfif>
							<option value="JavaScript: checks = saveChecks('Person');if (checks!='0') { newWindow = openWin( '/report/personSelection.cfm?frmpersonid='+checks+'&frmruninpopup=true','PopUp','width=650,height=500,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1');  } else {noChecks('Person')}">Phr_Sys_ReportSelectionsPeopleAreIn
							<cfif isDefined("session. securityLevels") and listFindNoCase(session. securityLevels,"OrderTask(1)") neq 0 and >
							<option value=none>Phr_Sys_ProductOrdering
							<option value="JavaScript: checks = saveChecks('Person');if (checks!='0')  { newWindow = openWin( '../Orders/orderstart.cfm?frmPersonId='+saveChecks()+'&frmruninpopup=true','PopUp','width=650,height=500,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1');  } else {noChecks('Person')}">Phr_Sys_PlaceOrder
							</cfif>		 --->
					</select>
				</div>
			</div>
		</div>
	</CFOUTPUT>
	<!--- 	<cf_selectScrollJS formName="personFunctions" selectBoxName="personDropDown" onChangeFunction="doDropDown(document.personFunctions.personDropDown)">  --->
	<cf_selectScrollJS formName="" selectBoxName="personDropDown" onChangeFunction="doDropDown(document.getElementById('personDropDown'))">
	<!--- </form>  --->
</cf_translate>
