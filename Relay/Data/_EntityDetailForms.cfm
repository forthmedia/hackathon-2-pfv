<!--- �Relayware. All Rights Reserved 2014 --->
<!--- This include file should be put just after the body of the main detail forms 

	NJH 2012/02/27 CASE 425212: altered the refreshPerList function to test the existence of PersList with the use of a function
--->


<SCRIPT>
	// used to add new location record
	function newLoc(orgID,selIndex){
		var form = document.newLocForm;
		form.frmOrganisationID.value = orgID;
		if(selIndex){
			form.selIndex.value = selIndex;
		}
		form.submit();
		}
		
	// used to add new location record
	function viewSites(orgID,selIndex){
		var form = document.viewLocForm;
		form.frmsrchOrgID.value = orgID;
		if(selIndex){
			form.selIndex.value = selIndex;
		}
		form.submit();
		}
		
	function refreshPerList(orgID,selIndex){
		/* WAB: when used within drilldown the perlist isn't there, so need to check for it otherwise opens a new window
		   can be a little tricky knowing exactly what the parentage is (different versions of the entitynavigation are different) so have put in a couple of checks
		 */
		//if (window.parent.parent.PersList || window.parent.PersList) {
		if (personFrame = (window.parent.parent.getPerFrame() || window.parent.getPerFrame())) {
			var form = document.persListForm;	
			form.target = personFrame.name;
			form.frmOrganisationID.value = orgID;
			if(selIndex){
				form.selIndex.value = selIndex;
			}
			form.submit();
		}
	}
		
</SCRIPT>


<FORM ACTION="locDetail.cfm" METHOD="post" NAME="newLocForm">
	<INPUT TYPE="hidden" NAME="frmOrganisationID" VALUE="0">
	<INPUT TYPE="HIDDEN" NAME="frmLocationID" VALUE="0">
	<INPUT TYPE="hidden" NAME="selIndex" VALUE="">
</FORM>

<FORM ACTION="locList.cfm" METHOD="post" NAME="viewLocForm">
	<INPUT TYPE="hidden" NAME="frmsrchOrgID" VALUE="0">
	<INPUT TYPE="HIDDEN" NAME="frmLocationID" VALUE="0">
	<INPUT TYPE="hidden" NAME="selIndex" VALUE="">
</FORM>

<FORM NAME="persListForm" METHOD="POST" ACTION="persList.cfm" target="PersList">
	<INPUT TYPE="hidden" NAME="frmOrganisationID" VALUE="0">
	<INPUT TYPE="hidden" NAME="selIndex" VALUE="">
</FORM>


