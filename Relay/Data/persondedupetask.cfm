<!--- �Relayware. All Rights Reserved 2014 --->
<!--- user rights to access:
		SecurityTask:dataTask
		Permission: Update

FileName:	personDedupeTask.cfm

Mod History

1.00     ?
1.01	2001-08-16	SWJ 	Modified to use Jules's new dedupe person stored procedure
1.02	2008-09-15	PPB		Added the requesttimeout to avoid a crash
1.03	2008-09-24	PPB		Allow user to choose which Flags to keep during dedupe
---->

<cfsetting requesttimeout="600"> 		<!--- PPB set to 10 minutes as dedupe can be a length process --->


<CFIF NOT checkPermission.UpdateOkay GT 0>
	<!--- if frmLocationID is 0 then task is to Update
	      but the user doesn't have Update permissions --->
 	<CFSET message="You are not authorised to use this function">
		<CFINCLUDE TEMPLATE="datamenu.cfm">
	<CF_ABORT>
</CFIF>


<CFTRANSACTION>
	<CFSET archivetime = Now()>

	<CFQUERY NAME="UpdatePerson" DATASOURCE=#application.SiteDataSource#>
	UPDATE Person
		SET Salutation =  <cf_queryparam value="#insSalutation#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
			FirstName =  <cf_queryparam value="#insFirstName#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
			LastName =  <cf_queryparam value="#insLastName#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
			HomePhone =  <cf_queryparam value="#insHomePhone#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
			OfficePhone =  <cf_queryparam value="#insOfficePhone#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
			MobilePhone =  <cf_queryparam value="#insMobilePhone#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
			FaxPhone =  <cf_queryparam value="#insFaxPhone#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
			Email =  <cf_queryparam value="#insEmail#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
			JobDesc =  <cf_queryparam value="#insJobDesc#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
			Language =  <cf_queryparam value="#insLanguage#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
			<!---
			Notes = N'#insNotes#',
			--->
			Active =  <cf_queryparam value="#insActive#" CFSQLTYPE="cf_sql_float" > ,
			LastUpdatedBy = #request.relayCurrentUser.usergroupid#,
			LastUpdated =  <cf_queryparam value="#archivetime#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
		WHERE PersonID =  <cf_queryparam value="#PersonIDsave#" CFSQLTYPE="CF_SQL_INTEGER" >
	</CFQUERY>


	<!--- 	PPB 2008-09-29: UPDATE THE FLAG TABLES --->
	<CFSET entityIdSave = PersonIDsave>
	<CFSET entityIdDel = PersonIDDel>
	<CFINCLUDE template="dedupe_SetFlags.cfm">

	<cfscript>
	companyAccount = application.com.dedupe.mergePerson(personIDSave,personIDDel);
	</cfscript>

</CFTRANSACTION>

<cfoutput>
	<cfif isDefined("frmRadioName")><script>opener.disableItem(#frmRadioName#,#frmRadioAmount#);</script></cfif>
</cfoutput>

<!--- remove the from the list --->
<CFSET a = ListFind(frmDupPeople,PersonIDdel)>
<CFIF a IS NOT 0>
	<CFSET frmDupPeople = ListDeleteAt(frmDupPeople,a)>
	<CFSET PersonsDel = ListAppend(PersonsDel, PersonIDdel)>
</CFIF>
<!--- if the last ID, then go to summary --->
<CFIF (ListLen(frmDupPeople) IS 1 AND ListFind(frmDupPeople,0) IS 0) OR
	(ListLen(frmDupPeople) IS 2 AND ListFind(frmDupPeople,0) IS NOT 0)>	<!--- should be the PersonIDsave --->
	<CFQUERY Name="getOrgID"  datasource="#application.siteDataSource#">
		select organisationID
		from person
		Where personID =  <cf_queryparam value="#personIDSave#" CFSQLTYPE="CF_SQL_INTEGER" >
	</CFQUERY>

	<cfset frmOrganisationID = getOrgID.organisationID>

	<CFINCLUDE TEMPLATE="persondedupesum.cfm">
	<CF_ABORT>
</CFIF>

<CFINCLUDE TEMPLATE="persondedupe.cfm">
<CF_ABORT>