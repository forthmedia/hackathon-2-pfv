<!--- �Relayware. All Rights Reserved 2014 --->

<!---

File name:		QuickMailClientTask.cfm
Author:			SWJ
Date created:	02 July 2000

	Objective - to provide a simple mail client which

	Rationale - Please describe why this was built and any useful contextual information
				about the conditions of use

	Syntax	  -	ProcName [Var1]>,[Var2]>,[ReturnRecordsets]

	Parameters - Please describe any parameters that can be passed to the proc and
				their purpose and usage.

	Return Codes - Please describe the return codes are returned to let you know certain


Date Tested:
Tested by:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
25 Oct 2001			CPS			send email as HTML if eWebEditPro editor is in use.
2001-11-15			swj			Added the attachment management
30-Jun-2008			NJH			(Bug Fix Lenovo Support 519) Added a 'TextAreaFormat' function to format the text from the textArea into a similar HTML format
27-Oct-2008 		NJH/WAB		Bug Fix Lenovo Issue 26 - start storing the subject line as part of the feedback to help clear some of the reporting issues. This will get displayed as the communication title.
2013-03-11			WAB			CASE 434140 change CFMAIL to CF_MAIL
2013-12-03 			WAB 		2013RoadMap2 Item 25 Contact History.  Change to CF_AddCommDetail to support Subject/Body
2014-01-09			WAB			and change commTypeID to 51

Enhancement still to do:

--->
<cfinclude template="/templates/qryGetUserName.cfm">
<!--- Make sure attached file exists --->
<CFPARAM NAME="CCList" default="">
<CFPARAM NAME="BCCList" default="">

<CFIF structKeyExists(form,"attachment") and Form.Attachment is not "">

	<CFSET attachementDir = "#application.paths.content#\EmailAttachments\">

	<CFIF directoryExists(attachementDir)>
		<!--- Directory OK. --->
	<CFELSE>
		Directory <CFOUTPUT>#htmleditformat(attachementDir)#</CFOUTPUT> does not exist.
		<CFDIRECTORY ACTION="CREATE" DIRECTORY="#attachementDir#">
		<CFIF directoryExists(#attachementDir#)>
			<BR>Directory <CFOUTPUT>#htmleditformat(attachementDir)#</CFOUTPUT> now exists.
		<CFELSE>
			<BR>Directory <CFOUTPUT>#htmleditformat(attachementDir)#</CFOUTPUT> was not created.
			<BR>This is a problem please speak to the administrator
			<CF_ABORT>
		</CFIF>
	</CFIF>

	<CFIF structKeyExists(form,"attachment") and Form.Attachment is not "">
		<CFFILE
			ACTION="UPLOAD"
			FILEFIELD="Form.Attachment"
			DESTINATION="#attachementDir#"
			NAMECONFLICT="MAKEUNIQUE">
	</CFIF>

<CFIF FileExists(Form.Attachment) is "No">

      <cf_head>
         <cf_title>Attachment Not Found</cf_title>
      </cf_head>

      <H2>Attachment Not Found</H2>
      The file you specifed as an attachment,
      <CFOUTPUT>
      <STRONG>#htmleditformat(Form.Attachment)#</STRONG>,
      </CFOUTPUT>
      could not be found.
      <P>You can only attach valid files to your e-mail. Use the
      Back button to go back and enter a valid file name or
      erase the Attachment name.


      <CF_ABORT>
    </CFIF>
</CFIF>

<!--- NJH/WAB 2008-10-27 Bug Fix Lenovo Issue 26 - start storing the subject line as part of the feedback to help
clear some of the reporting issues. This will get displayed as the communication title.  --->
<CFIF IsDefined("contactHistory")>
	<CF_addCommDetail
		personID 	 = #frmPersonID#
		locationID   = #frmLocationID#
		commTypeID	 = 51
		commStatusID = 2
		subject	 = #Form.Subject#
		body = 	#form.message#
	>

	<!--- CRM 427542 NJH 2012/05/24 clear the contact History cache by forcing a refresh of the contact history report. --->
	<cfset session.CommHistoryCurrentEntityID= 0>
</CFIF>

<!--- converts text to HTML format
first convert any urls to HREFs
then add paragraphs
 --->
<cfset messageConverted = application.com.regExp.convertAddressToHref (Form.Message)>
<!--- <cfset messageCOnverted = paragraphFormat(messageconverted)> --->

<!--- NJH 2008-06-30 Bug Fix Lenovo Support 519 - Added the textAreaFormat function
	Should probably be in a string.cfc somewhere.... --->
<cfscript>
function TextAreaFormat(formfield) {
	temp = replace(formfield,chr(13)&chr(10)&chr(13)&chr(10),"<p>","all");
	temp = replace(temp,chr(13)&chr(10),"<br>","all");
	temp = replace(temp,chr(32)&chr(32)&chr(32),"&nbsp;&nbsp;&nbsp;","all");
	temp = replace(temp,chr(9),"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;","all");
	return temp;
}
</cfscript>

<cfset messageConverted = TextAreaFormat(messageconverted)>

<!--- WAB 2013-11-04 CASE 437797 Needed to add cfoutputs to these cf_mails--->
<CFIF structKeyExists(form,"attachment") and Form.Attachment is not "">
   <!--- Send the email, using the comma delimted list and attachment --->
   <CF_MAIL TO="#Form.Mailto#"
        FROM="#GetUserName.fullname# <#GetUserName.email#>"
        SUBJECT="#Form.Subject#"
        CC="#CCList#"
		BCC="#BCCList#"
        MIMEATTACH="#attachementDir##CFFile.serverFileName#.#CFFIle.serverFileExt#"
        TYPE="HTML"><cfoutput><FONT FACE="Verdana,Geneva,Arial,Helvetica,sans-serif" SIZE="2">#messageConverted#</FONT></cfoutput>
<CFINCLUDE TEMPLATE="quickMailFooter.cfm">
   </CF_MAIL>
<CFELSE>
   <!--- Send the email, using the comma delimted list and NO attachment --->
	   <CF_MAIL
	      TO="#Form.Mailto#"
	      FROM="#GetUserName.fullname# <#GetUserName.email#>"
		  CC="#CCList#"
	      SUBJECT="#Form.Subject#"
		  TYPE="HTML"
		  BCC="#BCCList#"
	   ><cfoutput><FONT FACE="Verdana,Geneva,Arial,Helvetica,sans-serif" SIZE="2">#messageConverted#</FONT></cfoutput><CFINCLUDE TEMPLATE="quickMailFooter.cfm">
	   </CF_MAIL>
</CFIF>

<CFQUERY NAME="getOrgID" datasource="#application.siteDataSource#">
	select organisationID from person where personid =  <cf_queryparam value="#frmPersonID#" CFSQLTYPE="CF_SQL_INTEGER" >
</CFQUERY>


<cf_title>Email Sent</cf_title>
<!--- NJH 2016/02/09 - BF-43 removed this for now as causing problems in pane refresh. I suspect that it's not really needed anymore and the 3-pane view is going to get an overhaul anyways
<cf_body onload="javaScript:window.opener.location.href=window.opener.location.href;"/> --->

	<cf_includejavascriptonce template = "/javascript/openwin.js">
	<cf_includejavascriptonce template = "/javascript/addShowActions.js">

<CF_RelayNavMenu moduleTitle="" thisDir="/data">
<CFIF isDefined("frmPersonID") and isNumeric(frmPersonID)>
	<CF_RelayNavMenuItem MenuItemText="Add Action" CFTemplate="javaScript:addActionRecord('person',#frmPersonID#)">
</cfif>
</CF_RelayNavMenu>

<CFOUTPUT>
<table width="100%" border="0" cellspacing="1" cellpadding="3" align="center" class="withBorder">
	<TR><TD CLASS="label">&nbsp;</td><TD><p style="font: bold; color: Red;">phr_EmailSent</p></TD></TR>
	<TR><TD ALIGN="right" VALIGN="top" CLASS="label">phr_Subject : </TD><TD>#htmleditformat(Form.Subject)#</TD></TR>
	<TR><TD ALIGN="right" VALIGN="top" CLASS="label">phr_to : </TD><TD>#htmleditformat(Form.Mailto)#</TD></TR>
	<TR><TD ALIGN="right" VALIGN="top" CLASS="label">CC : </TD><TD>#htmleditformat(Form.CCList)#</TD></TR>
	<TR><TD ALIGN="right" VALIGN="top" CLASS="label">phr_BCC : </TD><TD>#htmleditformat(Form.BCCList)#</TD></TR>
	<tr><td colspan="2" CLASS="label"><hr width="100%" size="1"></td></tr>
	<TR><td colspan="2" bgcolor="White">#htmleditformat(messageConverted)#
	<CFINCLUDE TEMPLATE="quickMailFooter.cfm">
	</td>
	</TR>
</TABLE>
</CFOUTPUT>