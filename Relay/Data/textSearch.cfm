<!--- �Relayware. All Rights Reserved 2014 --->
<!---

Mods
WAB 2005-03-22   changed invokation of commonqueries.tracksearch to use application.com notation
NJH 2016/06/09	little code clean up. Use getRightsFilterQuerySnippet for country rights.
 --->

<cf_title>Text Search Screen</cf_title>

<cfparam name="form['frmGlobalSearch']" default="">
<cfparam name="table" type="string" default="datasource">
<cfparam name="screenTitle" default="">
<cfparam name="sortOrder" default="organisation">

<cfinclude template="datatophead.cfm">

	<div class="header">
		<h1>Search Text Fields</h1>
	</div>

	<form action="" method="post">
		<p>phr_sys_TextSearchIntroduction</p>
		<div class="form-group">
			<label for="frmGlobalSearch">phr_sys_enterTextToSearchFor</label>
			<input type="text" name="frmGlobalSearch" id="frmGlobalSearch" size="40" maxlength="40" class="form-control">
		</div>
		<div class="form-group">
			<input type="submit" class="btn btn-primary">
		</div>
	</form>


<cfif form["frmGlobalSearch"] neq "">

		<cfscript>
		// create an instance of the trackSearch component
		application.com.commonqueries.inserttracksearch (searchtype = "T",searchstring=#form["frmGlobalSearch"]#, personid = request.relayCurrentUser.personID );

		// start: NJH 2008-04-14 Bug Fix LexClient Issue 113 - added passThruVarsStruct as show all records was not working
		if (not isDefined("passThruVars")) {
			passThruVars = StructNew();
		}
		StructInsert(passthruVars, "frmGlobalSearch", form.frmGlobalSearch);
		// end
		</cfscript>

	<cfquery name="getTextData" datasource="#application.siteDataSource#">
		select distinct orgID, organisationName as organisation, vNoteText.personid, PERSON,
		left(note_text, 200) as note_text,note_type
		from organisation with(nolock)
			INNER JOIN  vNoteText with(nolock) ON organisationID = orgID
		<!--- <cfif isDefined("countryScopeOrganisationRecords") and countryScopeOrganisationRecords eq 1> --->
		<cfif application.com.settings.getSetting("plo.countryScopeOrganisationRecords")>
			<cfset countryRights = application.com.rights.getRightsFilterQuerySnippet(entityType="organisation").join>
			#preserveSingleQuotes(countryRights)#
			<!--- show all orgs unless countryScopeOrganisationRecords
			is set to 1 in content/CFTemplates/relayINI.cfm --->
		  	<!--- and countryID in (
			 SELECT r.CountryID FROM Rights AS r with(nolock), RightsGroup AS rg with(nolock), Country as c with(nolock)
				WHERE r.SecurityTypeID = (SELECT e.SecurityTypeID FROM SecurityType AS e with(nolock)
					WHERE e.ShortName = 'RecordTask')
				AND r.UserGroupID = rg.UserGroupID
				AND c.countryid = r.countryid
				AND rg.PersonID =  #request.relayCurrentUser.personID#
				AND r.Permission > 0
			 ) --->
		 </cfif>
		 where note_Text  LIKE  <cf_queryparam value="%#form["frmGlobalSearch"]#%" CFSQLTYPE="CF_SQL_VARCHAR" >
		order by <cf_queryObjectName value="#sortorder#">
	</cfquery>

	<CF_tableFromQueryObject
		queryObject="#getTextData#"

		keyColumnList="organisation,PERSON"
		keyColumnURLList="dataFrame.cfm?frmsrchOrgID=,dataFrame.cfm?frmsrchPersonID="
		keyColumnKeyList="orgID,personid"

		useInclude = "false"

		hideTheseColumns="orgID,personID"
		showTheseColumns="ORGANISATION,PERSON,NOTE_TEXT,NOTE_TYPE"

		dateFormat=""
		sortOrder="#sortorder#">
</cfif>