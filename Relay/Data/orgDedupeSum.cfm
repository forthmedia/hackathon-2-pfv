<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
	2008-09-26	PPB - new file: based on persondedupesum.cfm
	
Amendment History


 --->

<CFIF NOT checkPermission.AddOkay GT 0>
	<!--- if frmLocationID is 0 then task is to add
	      but the user doesn't have add permissions --->

	<CFSET message="You are not authorised to use this function">
		<CFINCLUDE TEMPLATE="datamenu.cfm">
	<CF_ABORT>

</CFIF>



<CFPARAM NAME="OrganisationsDel" Default="">


<CFIF NOT IsDefined("message")>

	<!--- remove any 0s from list --->
	
	<CFLOOP CONDITION="ListFind(frmOrgIDs,0) IS NOT 0">
		<CFSET frmOrgIDs = ListDeleteAt(frmOrgIDs,ListFind(frmOrgIDs,0))>
	</CFLOOP>

	<CFSET message = "All records previously linked to organisation record(s) #OrganisationsDel# are now linked to organisation record #frmOrgIDs#.">

</CFIF>




<cf_head>
<cf_title><CFOUTPUT>#request.CurrentSite.Title#</CFOUTPUT></cf_title>
<SCRIPT type="text/javascript">
<!--
	function goTo(a,b) {
		var form = document.DedupeFlags;
		form.frmGoTo.value = a;
		form.frmNext.value = b;
		form.submit();
	}

//-->
</SCRIPT>
</cf_head>

<cfset application.com.request.setTopHead(pageTitle="Organisation Records Merged")>

<CENTER>
	
<CFOUTPUT>#application.com.relayUI.message(message=message)#</CFOUTPUT>

<CFOUTPUT>
<TABLE BORDER="0" CELLSPACING="5" CELLPADDING="5">

	<TR>
		<TD VALIGN=TOP ALIGN="CENTER" colspan="2">
			<!---
			<A HREF="JavaScript:goTo('orgListv3','');" id="returnToOrgLink" style="visibility:visible">Return to organisation list</A>
			--->
			
			<!--- PPB: force a full org view for the org we have just merged into to refresh org/location/person ---> 
			<script type="text/javascript">
				window.opener.orgDedupeCompleted(#frmOrgIDs#)
			</script>

			<cfif not isDefined("frmRadioName")>
				<!--- PPB: now no need to reload because we fire off a full org view 
				<input type=button value="Close Window"  id="closeWindowButton" style="visibility:hidden" onClick="javascript:window.opener.location.reload(true); if (window.opener.progressWindow) { window.opener.progressWindow.close() } ;self.close();">
				--->
				<input type=button value="Close Window"  id="closeWindowButton" style="visibility:hidden" onClick="javascript:if (window.opener.progressWindow) { window.opener.progressWindow.close() } ;self.close();">
			<cfelse>
				<input type=button value="Close Window"  id="closeWindowButton" style="visibility:hidden" onClick="javascript:self.close();">
			</cfif>
		</TD>
	</TR>

</TABLE>
</CFOUTPUT>


<!--- COPIED FROM persondedupe: equivalent REQUIRED ?

<FORM ACTION="personchoice.cfm" METHOD="POST" NAME="DedupeFlags">
	<CFINCLUDE template="searchparamdefaults.cfm">
	<CFPARAM NAME="frmStart" DEFAULT="1">	
	<CFPARAM NAME="frmStartp" DEFAULT="1">	
	<CFOUTPUT>
	<INPUT TYPE="HIDDEN" NAME="frmDupLocs" VALUE="#frmDupLocs#">
	<INPUT TYPE="HIDDEN" NAME="frmDupPeople" VALUE="#frmDupPeople#">
	<CFIF IsDefined("frmLocationID")>
		<INPUT TYPE="HIDDEN" NAME="frmLocationID" VALUE="#frmLocationID#">
	<CFELSE>
		<INPUT TYPE="HIDDEN" NAME="frmLocationID" VALUE="0">
	</CFIF>
	
	<CFIF IsDefined("frmOrganisationID")>
		<INPUT TYPE="HIDDEN" NAME="frmOrganisationID" VALUE="#frmOrganisationID#">
	<CFELSE>
		<INPUT TYPE="HIDDEN" NAME="frmOrganisationID" VALUE="0">
	</CFIF>

	<CFINCLUDE template="searchparamform.cfm">
	
	<INPUT TYPE="HIDDEN" NAME="frmStart" VALUE="#frmStart#">
	<INPUT TYPE="HIDDEN" NAME="frmStartp" VALUE="#frmStartp#">
	<INPUT TYPE="HIDDEN" NAME="frmGoTo" VALUE="">
	<INPUT TYPE="HIDDEN" NAME="frmNext" VALUE="datamenu">
	</CFOUTPUT>
</FORM>
--->

</CENTER>




<script>
	// if this is called in a window hide the back button
	if(opener){ 
		var thisButtonVar = document.getElementById('closeWindowButton');
		thisButtonVar.style.visibility = "visible";
		
		//var thisLinkVar = document.getElementById('returnToOrgLink');
		//thisLinkVar.style.visibility = "hidden";
	}
</script>

<CF_ABORT>

