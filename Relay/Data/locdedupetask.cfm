<!--- �Relayware. All Rights Reserved 2014 --->
<!--- user rights to access:
		SecurityTask:dataTask
		Permission: Update

FileName:	locDedupeTask.cfm

Mod History

1.00     ?
1.01	2001-08-16	SWJ 	Modified to use Jules's new dedupe entity stored procedure
	2005-07-05	WAB		The update didn't have the
WAB    2007/07/18   added a bit of error handling to mergelocation needed for Trend (rather last minute)
	            this must be released with the changes to dedupe.cfc
		2008-09-24	PPB		Allow user to choose which Flags to keep during dedupe
		2009-04-22	NYB		Sophos - added concept of request.AdditionalLocDedupeFields to add additional fields to dedupe process

---->

<CFIF NOT checkPermission.UpdateOkay GT 0>
	<CFSET message = "You are not authorised to use this function">
	<CFINCLUDE TEMPLATE="datamenu.cfm">
	<CF_ABORT>
</CFIF>

<cfsetting requesttimeout="600"> 		<!--- PPB set to 10 minutes as dedupe can be a length process --->


<CFQUERY NAME="UpdateLocation" datasource="#application.siteDataSource#">
	UPDATE Location
		SET SiteName =  <cf_queryparam value="#insSiteName#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
			accountTypeID =  <cf_queryparam value="#insAccountTypeID#" CFSQLTYPE="CF_SQL_INTEGER" > ,
			Address1 =  <cf_queryparam value="#insAddress1#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
			Address2 =  <cf_queryparam value="#insAddress2#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
			Address3 =  <cf_queryparam value="#insAddress3#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
			Address4 =  <cf_queryparam value="#insAddress4#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
			Address5 =  <cf_queryparam value="#insAddress5#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
			PostalCode =  <cf_queryparam value="#insPostalCode#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
			CountryID =  <cf_queryparam value="#insCountryID#" CFSQLTYPE="CF_SQL_INTEGER" > ,
			Telephone =  <cf_queryparam value="#insTelephone#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
			Fax =  <cf_queryparam value="#insFax#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
			<!--- START: NYB 2009-04-22 - added cfloop to query --->
			<cfloop index="i" list="#application.com.settings.getSetting('plo.AdditionalLocDedupeFields')#">
				#i# =  <cf_queryparam value="#evaluate("ins#i#")#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
			</cfloop>
			lastupdated =  <cf_queryparam value="#now()#" CFSQLTYPE="CF_SQL_TIMESTAMP" > ,
			lastupdatedby = #request.relaycurrentUser.usergroupid#
		WHERE LocationID =  <cf_queryparam value="#LocationIDsave#" CFSQLTYPE="CF_SQL_INTEGER" >
</CFQUERY>


<!--- PPB 2008-09-29 --->
<CFSET entityIdSave = LocationIDsave>
<CFSET entityIdDel = LocationIDDel>
<CFINCLUDE template="dedupe_SetFlags.cfm">

<cfset mergeResult = application.com.dedupe.mergeLocation(locationIDSave,locationIDDel)>

<cfif mergeResult.isOK>

	<cfoutput>
		<cfif isDefined("frmRadioName")><script>opener.disableItem(#frmRadioName#,#frmRadioAmount#);</script></cfif>
	</cfoutput>

	<CFSET a = ListFind(frmDupLocs,LocationIDdel)>
	<CFIF a IS NOT 0>
		<CFSET frmDupLocs = ListDeleteAt(frmDupLocs,a)>
		<CFSET LocationsDel = ListAppend(LocationsDel, LocationIDdel)>
	</CFIF>

	<CFIF (ListLen(frmDupLocs) IS 1 AND ListFind(frmDupLocs,0) IS 0) OR	(ListLen(frmDupLocs) IS 2 AND ListFind(frmDupLocs,0) IS NOT 0)>	<!--- should be the LocationIDsave --->
		<CFQUERY Name="getOrgID"  datasource="#application.siteDataSource#">
			select organisationID
			from location
			Where locationid =  <cf_queryparam value="#locationIDSave#" CFSQLTYPE="CF_SQL_INTEGER" >
		</CFQUERY>
		<cfset frmsrchOrgID = getOrgID.organisationID>
		<CFINCLUDE TEMPLATE="locdedupesum.cfm">
		<CF_ABORT>
	</CFIF>

	<CFINCLUDE TEMPLATE="locdedupe.cfm">

<cfelse>
	<!--- dedupe failed --->
	<cfoutput>#htmleditformat(mergeresult.message)#</cfoutput>

</cfif>
<CF_ABORT>