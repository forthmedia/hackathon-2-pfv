<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Amendment History
1999-06-17 WAb added support for address 7,8,9
1990-02-20	WAB No longer always update Organisation name, only if only one location in organisatioion 
2001-09-06 SWJ Removed calls to FlagTask in the insert and update
2001-09-20 SWJ	removed the organisation insert in the code, removed all of the archive code
				and simplified the insert and update statements.
2008/01/08 NJH added location email.
NYB 	2009-05-12 added Location dedupe code
NJH 	2014/05/14	Connector - Added the saving of accountTypeID
--->
<cf_translate>
<CFPARAM NAME="frmNext" DEFAULT="datamenu">

<!---  added these defaults, 'cause not always passed --->
<CFPARAM NAME="insAddress6" DEFAULT="">
<CFPARAM NAME="insAddress7" DEFAULT="">
<CFPARAM NAME="insAddress8" DEFAULT="">
<CFPARAM NAME="insAddress9" DEFAULT="">
<CFPARAM NAME="insAddress10" DEFAULT=""> <!--- NJH 2008/01/14 - used to store the region field tied to the region table --->
<CFPARAM NAME="insDirect" DEFAULT="0"> <!--- SSS 2008-02-08 - it is taken out of the front end so set here in case it is but back --->
<CFPARAM NAME="insLocEmail" DEFAULT=""> <!--- NJH 2008/06/24 - Bug Fix Lexmark 569 - the location email --->

<cfif not structKeyExists(form,"is_HQ")>
	<cfset form.is_HQ = 0>
</cfif>

<cfset messageType = "success">

<CFIF structKeyExists(FORM,"frmtask")>

	<!--- NJH 2008/05/01 added this as the person detail task had the validation and thought it would be good to be consistent
	 	when validating phone numbers across people and locations --->
	<cfif form.phonemask neq "" and 
			( (len(form.insTelephone) gt 0 and len(form.insTelephone) neq len(phonemask)) or 
				(len(form.insFax) gt 0 and len(form.insFax) neq len(phonemask)))>
			<p align="center"  class="label">
			The following Phone(s)/Fax entry does not march the Telephone format length
			<br />
		
			<br />
			<cfif (len(form.insTelephone) gt 0) and (len(form.insTelephone) neq len(phonemask))  >
				Switchboard<br />
			</cfif>

			<cfif (len(form.insFax) gt 0) and (len(form.insFax) neq len(phonemask))  >
				Fax Number<br />
			</cfif>
			<br />
			<a href="javascript:history.back(1)" >Return to form</a>
			</p>		
		<CF_ABORT>			
	</cfif>
	
	<!--- <cfset locMatchKeyList=application.com.dbTools.getLocationMatchnameEntries()>
	<cfset locMatchName = "">
	<cfloop list="#locMatchKeyList#" delimiters="#application.delim1#" index="keyItem">
		<cfset keyItem = "ins" & keyItem>
		<cfif locMatchName eq "">
			<cfset locMatchName = evaluate(keyItem)>
		<cfelse>
			<cfset locMatchName = locMatchName & " #application.delim1# " & evaluate(keyItem)>
		</cfif>
	</cfloop> --->
	
	<cfset locationDetails.Address1 = insAddress1>
	<cfset locationDetails.Address2 = insAddress2>
	<cfset locationDetails.Address3 = insAddress3>
	<cfset locationDetails.Address4 = insAddress4>
	<cfset locationDetails.Address5 = insAddress5>
	<cfset locationDetails.Address6 = insAddress6>
	<cfset locationDetails.PostalCode = insPostalCode>
	<cfset locationDetails.CountryID = insCountryID>
	<cfset locationDetails.OrganisationID = frmOrganisationID>
	<cfset locationDetails.SiteName = insSiteName>
	<cfset locationDetails.Telephone = insTelephone>
	<cfset locationDetails.Fax = insFax>
	<cfset locationDetails.LocEmail = insLocEmail>
	<cfset locationDetails.Address10 = insAddress10>
	<cfset locationDetails.SpecificURL = insSpecificURL>
	<cfset locationDetails.Direct = insDirect>
	<cfset locationDetails.Active = insActive>
	<!--- <cfset locationDetails.Matchname = locMatchName> --->
	<cfset locationDetails.accountTypeID = accountTypeID>
	
	<CFIF FORM.frmtask IS "add">
		<!--- Org active IS ALWAYS true
			if we set if false then we just have more management
			which is not necessary with 1-1 and no user editing
		 --->
		<!--- verify user has rights to these countries --->

		<!--- NJH 2011/04/04 replaced the insert function with the below code in an effort to consolidate code --->
		<cfset locationInsResult = application.com.relayPLO.insertLocation(locationDetails=locationDetails,insertIfExists=false)>
		<cfset frmEntityID = locationInsResult.entityID>
		<cfset frmLocationID = frmEntityID>

		<!--- WAB 2007-12-18 
			If we are in the entity Frame then this will reload the menus with the new location--->
		<cfoutput>
		<script>
			if (parent.loadNewEntityNoRefresh) {
				parent.loadNewEntityNoRefresh (entityTypeID = 1, entityID = #frmLocationID#, screenID = 'locationdetail' )
			}		
		</script>
		</cfoutput>
		
		<cfif locationInsResult.entityID neq 0 and locationInsResult.isOK>
			<cfif locationInsResult.exists and listLen(locationInsResult.entitiesMatchedList) gte 1>
				<cfset message="Phr_Sys_Location #Trim(insSiteName)# phr_found. Phr_Sys_NewNotAdded">
				<cfset messageType = "info">
			<cfelse>
			
				<cfif is_hq>
					<cfset application.com.flag.setFlagData(entityID=frmOrganisationID,data=frmLocationID,flagID="HQ")>
				</cfif>
			
				<cfset message = "Phr_Sys_Location '#insSiteName#' Phr_Sys_WasAddedSuccessfully">
				<cfset messageType="success">
			</cfif>
		</cfif>
		
		<cfset application.com.relayUI.setMessage(message=message,messageType=messageType)>
		
		<cfset frmEntityType = "Location">
		<CFSET frmCurrentEntityID = frmLocationID>
		<CFINCLUDE TEMPLATE="#frmNext#.cfm">
		<CF_ABORT>
		
	<CFELSEIF FORM.frmtask IS "update">

		<!--- if the location can't be made inactive, do all other changes
				but include text with final success message --->
		<CFSET msginactive = ""> 
		
		<CFSET updatetime = CreateODBCDateTime(Now())>
		
		<!--- check last updated date/time --->
		<CFQUERY NAME="checkLocation" DATASOURCE="#application.SiteDataSource#">
			SELECT Active, SiteName, OrganisationID, LastUpdated, (select name from usergroup where location.lastupdatedby = usergroupid) as username
			FROM Location 
			WHERE LocationID =  <cf_queryparam value="#frmLocationID#" CFSQLTYPE="CF_SQL_INTEGER" > 
			AND datediff(s,LastUpdated,  #insLastUpdated#) < 0   <!--- this syntax should get round problems associated with miiliseconds --->
		</CFQUERY>
		
		<CFIF checkLocation.RecordCount IS NOT 0>
			<CFSET message = "Phr_Sys_LocRecordChangedBy #checklocation.username# (at #timeformat(checklocation.lastupdated,"HH:mm:ss")#) Phr_Sys_LastViewed  Debug: #insLastUpdated#">
			<cfset messageType="info">
			
			<cfset application.com.relayUI.setMessage(message=message,messageType=messageType)>
			<CFINCLUDE TEMPLATE="#frmNext#.cfm">
			<CF_ABORT>
		</CFIF>

		<CFIF YesNoFormat(insActive) IS "no">
		<!--- if the active on the form is "no" then check all the related
				people to see if any are users.  Users cannot be made inactive --->
				
			<CFQUERY NAME="checkForUsers" DATASOURCE=#application.SiteDataSource#>
				SELECT p.PersonID, p.FirstName, p.LastName
				FROM Person AS p, UserGroup AS ug
				WHERE p.LocationID =  <cf_queryparam value="#frmLocationID#" CFSQLTYPE="CF_SQL_INTEGER" > 
				  AND p.PersonID = ug.PersonID
			</CFQUERY>
				
			<CFIF checkForUsers.RecordCount IS NOT 0>
				<CFSET insActive = 1> <!--- override the false from the user's form --->
				<CFSET msginactive = " Phr_Sys_LocHasActiveUsers">
			</CFIF>
	

		</CFIF>
		<!--- check organisation date/time ??? --->
		<!--- Assume anytime Location Flags update, so is
				Location LastUpdated so no need to check
				the Flags date/time  --->
	
		<!--- <CFTRANSACTION>
			NJH 2012/07/10 RelayAPI  case 429325 - removed the transaction here as relayEntity now has a transaction within it, and can't have nested transactions.
				I don't think this transaction is important.. if so, we will need to address the issue for having nested transactions.
			 --->	

			<CFQUERY NAME="updLocation" DATASOURCE=#application.SiteDataSource#>
			<!--- WAB 2009/10/05 see note below for explanation of this odd bit of query--->
			select case when datediff(s,LastUpdated,  #insLastUpdated#) >= 0 then 1 else 0 end as Oktoupdate from location where locationID =  <cf_queryparam value="#frmLocationID#" CFSQLTYPE="CF_SQL_INTEGER" >
			</CFQUERY>
			
			<!--- NJH 2011/04/04 replaced the update function with the below code in an effort to consolidate code --->
			<cfset locationUpdateResult = application.com.relayPLO.updateLocationDetails(locationDetails=locationDetails,locationID=frmLocationID)>

			<!--- set Person to inactive if the Location
					has been changed to inactive
					(insActive is false and checkLocation.Active was true)
					
					NB since org <-> Location is one-to-one
						org is always true
			--->
			<CFIF (YesNoFormat(insActive) IS "no") AND (YesNoFormat(checkLocation.Active) IS "yes")>
				<CFQUERY NAME="updPersonActive" DATASOURCE=#application.SiteDataSource#>
					UPDATE Person
					   SET Active = 0,
					   		LastUpdatedBy = #request.relaycurrentuser.usergroupID#,
							LastUpdated =  <cf_queryparam value="#Variables.updatetime#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
					 WHERE LocationID =  <cf_queryparam value="#frmLocationID#" CFSQLTYPE="CF_SQL_INTEGER" > 
				</CFQUERY>
			</CFIF>
			
			<cfif is_hq and hqLocationID neq frmLocationID>
				<cfset application.com.flag.setFlagData(entityID=frmOrganisationID,data=frmLocationID,flagID="HQ")>
			<cfelseif not is_hq and hqLocationID eq frmLocationID>
				<cfset application.com.flag.deleteFlagData(entityID=frmOrganisationID,data=frmLocationID,flagID="HQ")>
			</cfif>
			
		<!--- </CFTRANSACTION> --->

			<!--- 
				WAB 2007-06-06 (from orgdetailtask, only added here 2009/10/05
				Problem here if the screen includes another field from the organisation table, because updatedata notices that organisation record has already been updated 
				I have therefore had to add a check to the query above to see if the dates are OK, and if they are I am going to update the lastupdated variable
			--->
			<cfif updLocation.Oktoupdate>
				<cfset form["Location_lastupdated_#frmLocationID#"] = updatetime>
			</cfif>

			<!--- WAB 2008/01/09 amazingly no screenupdate had been added - needs testing to see if this works!--->
			<cf_aScreenUpdate>

		<CFSET message = "Phr_Sys_Location '#insSiteName#' Phr_Sys_WasUpdatedSuccessfully#msginactive#.">
		<cfset messageType="success">
		<cfset application.com.relayUI.setMessage(message=message,messageType=messageType)>
		<CFIF findNoCase("persList.cfm",frmNext) neq 0>
			<!--- if we're going back to personDetail then do CFINCLUDE len(QUERY_ STRING) neq 0 and --->
			<cfinclude template="/data/locdetail.cfm">
		<CFELSE>
			<!--- 	
				WAB 2007-06-12 removed this bit of javascript to try and sort out problems with frmNext creating errors
			<script>alert("Record saved");history.go(-2)</script> --->
			<CFINCLUDE TEMPLATE="#frmNext#.cfm">

		</CFIF>

		<CF_ABORT>
	</CFIF>
	

	
</CFIF>
</cf_translate>