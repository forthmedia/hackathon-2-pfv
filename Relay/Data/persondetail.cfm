<!--- �Relayware. All Rights Reserved 2014 --->
<!--- amendment History
	1999-04-13	WAB 	Added fields required for search by first and last name
	1999-04-07	 WAB	Created Time was showing the last updated time.  Now corrected
	2001-03-01  WAB 	altered joins  on query so that lastupdatedby doesn't have to exist
	2001-09-06 SWJ	altered code to not show flags and modified frmNextScreen
	2001-09-19 SWJ	altered to manage locations and organisations properly
	2002-01-11 SWJ	altered the screen layout slightly to get more on the screen
	2002-03-08 SWJ	altered to refresh persList.cfm when saving a record
	2002-03-20 DAM	made sure that frmcountryid was not set to Location.Countryid if it has already been set
					this caused problems where a location was not defined.
	2004-09-10 SWJ	added a parameter to supress auto-populate Emails
	2005-03-11 SWJ	added a few extra columns to the getLocations drop down
	2006-03-03 SWJ	added the ability to update perProfileSummary screen from here
	2006-05-25 NJH	(P_FNL017) Altered code to display form elements with standard wrapper tags.
	2007-04-23	AJC Organisation Type ID filter code
	2007-10-03 Gad    Worked on the INPUT TELEPHONE MASK
	2008/01/28 		WAB tried to sort out code which runs when locationid is changed
	2008/02/18      WAB   removed second save button on new records because confusing my javasript and not needed within entityNavigation
	2008/03/25		godfrey smith There was an error when not variable 'autoPopulateEmailAddress' was not present. error when dataParams.cfm not referenced via application .cfm
	2008/05/101		NJH set required fields based on the mandatoryCols list in dataINI. This can be overriden by setting
	2008/06/24	AJC	Put ## around phr_sys_salutation
	2009-09-14	NYB	LHID2608 - removed Back to Top link.  Seems to be a bug with IE7 that skews the frames
	2010/06/01	NJH	LID3405 - trim the phone numbers
	2010/09/21	NJH	LID 3956 Check the org type when doing unique email validation. Skip the check for end customers
	2011/02/10 	AJC P-LEN023 - Increased maxlength of insEmail
	2011/06/01  	WAB LID 6769  Security Scan - Ensure that frmPersonID is encrypted
	2011/09/05	NYB	fixed error - added a default for qrySalesForceID
	2012/02/24	NJH	 CASE 426697: if one didn't have rights to the country that their organisation was in, this page would fall over.. We set the country to be that of the current user if they are editing their own
					details and they don't have rights to their country. Should never really be in this situation, but this is to help safeguard against it falling over.
	2012/02/24	NJH	 CASE 426697: if one didn't have rights to the country that their organisation was in, this page would fall over.. We set the country to be that of the current user if they are editing their own
					details and they don't have rights to their country. Should never really be in this situation, but this is to help safeguard against it falling over.
	2012/04/20 	PPB P-REL112 tweak to LinkedIn button
	2013/01/29	YMA	Add social profile image.
	2013-03-06 	NYB	Case 434039 removed locations flagged for deletion from select location dropdown
	2013-03-26	YMA	Case 430471 Stop location change submitting without form validation
	2016/01/06  ESZ case 446546 - Office Phone not being fully displayed on the text box replace all special symbols
	2016-02-08	WAB BF-409, arising from 446546.  Do not replace special symbols from the phone number if it matches the mask

To dos
1.  Redo layout to look more like outlook contact page

	--->
<!--- redirect back to personlist if from Forward/Back buttons --->
<cf_translate>
<CFIF IsDefined("PersonForward.x") OR IsDefined("PersonBack.x")>
	<CFINCLUDE TEMPLATE="locmain.cfm">
	<CF_ABORT>
</CFIF>

<CFIF NOT checkPermission.Level1 GT 0>
	<CFSET message="Phr_Sys_YouAreNotAuthorised">
		<CFINCLUDE TEMPLATE="datamenu.cfm">
	<CF_ABORT>
</CFIF>

<!---
<cfset application.com.request.setTopHead(topHeadcfm = "")>
 --->

<cfset perDetailsScreenID = application.com.settings.getSetting("screens.perDetailsScreenID")>
<cfset perPostAddScreenID = application.com.settings.getSetting("screens.perPostAddScreenID")>

<cfscript>
	if(structKeyExists(url, "frmPersonID")){
		form.frmPersonID = url.frmPersonID;
	}
</cfscript>

<!--- <cfinclude template="/templates/qrygetcountries.cfm"> --->
<cf_checkfieldEncryption fieldnames="frmpersonid">
<CFPARAM NAME="frmLocationID" TYPE="numeric" DEFAULT="0">
<CFPARAM NAME="frmBack" DEFAULT="personDetail.cfm">
<CFPARAM NAME="frmNext" DEFAULT="personDetail.cfm">
<cfparam name="frmPersonID" type="numeric" default="0">
<cfparam name="frmOrganisationTypeID" type="numeric" default="0"> <!--- 2007-04-23 AJC Organisation Type ID filter code --->
<CFPARAM NAME="message" DEFAULT="">
<CFPARAM NAME="messageType" DEFAULT="success">
<cfparam name = "phonemask" default="">
<cfparam name = "masknCountryID" default="0">

<!--- NJH 2008/05/01 --->
<!--- NJH 2010/10/22 get the override from settings
<cfparam name = "mandatoryCols" default = "insSalutation,insFirstName,insLastName,insEmail,insJobDesc,insMobilePhone,insOfficePhone">--->
<cfparam name = "mandatoryCols" default = "#application.com.settings.getSetting('plo.mandatoryCols')#">

<!--- 'error when dataParams.cfm not referenced via application .cfm fixed' [godfrey.smith]- March 25, 2008, 10:21:57 AM --->
<!--- dataParams sets up some default values for varaiables
		which can be overridden either by dataINI below or at run time --->
<cfinclude template="dataParams.cfm">

<!--- NJH 2008/07/03 removed this as it's already being included in the data\application .cfm .
<cfif fileexists("#application.paths.code#\cftemplates\dataINI.cfm")>
	<!--- dataINI can be used to over-ride default
		global application variables and params
			eg: request.showAddress6
		--->
	<cf_includeonce template="/code/cftemplates/dataINI.cfm">
</cfif> --->

<!--- NJH 2008/05/01
	Create a structure of the fields in the form and set whether they are required or not.
	The fields are defaulted to false. Use the mandatoryCols or the mandatoryPersonDetailCols
	param to set the values in the structure.

	When adding a new field to the form, remember to add the field to this structure.

	The only field that is hardcoded to be required is the location
 --->
<cfif isDefined("mandatoryColsOverride") and len(mandatoryColsOverride) gt 0>
	<cfset mandatoryCols ="">
	<cfloop index="i" list="#mandatoryColsOverride#" delimiters="-">
		<cfset mandatoryCols = listAppend(mandatoryCols,i,",")>
	</cfloop>
</cfif>

<cfparam name="mandatoryPersonDetailCols" default="#mandatoryCols#">

<cfscript>
	requiredFieldsStruct = structNew();
	StructInsert(requiredFieldsStruct, "insSalutation", "false");
	StructInsert(requiredFieldsStruct, "insFirstName", "false");
	StructInsert(requiredFieldsStruct, "insLastName", "false");
	StructInsert(requiredFieldsStruct, "insEmail", "false");
	StructInsert(requiredFieldsStruct, "insJobDesc", "false");
	StructInsert(requiredFieldsStruct, "insMobilePhone", "false");
	StructInsert(requiredFieldsStruct, "insOfficePhone", "false");
	StructInsert(requiredFieldsStruct, "insFaxPhone", "false");
	StructInsert(requiredFieldsStruct, "insLanguage", "false");
	StructInsert(requiredFieldsStruct, "insHomePhone", "false");
</cfscript>

<cfloop list="#mandatoryPersonDetailCols#" index="formField">
	<cfif structKeyExists(requiredFieldsStruct,"#formField#")>
		<cfset requiredFieldsStruct[formField] = "true">
	</cfif>
</cfloop>

<CFIF not IsDefined("frmOrganisationID") and (frmPersonID neq 0 or frmLocationID neq 0)>
	<CFQUERY NAME="GetOrgid" datasource="#application.siteDataSource#">
		SELECT OrganisationID FROM
		<cfif frmLocationID neq 0>
			Location WHERE LocationID =  <cf_queryparam value="#frmLocationID#" CFSQLTYPE="CF_SQL_INTEGER" >
		<cfelse>
			person WHERE personID =  <cf_queryparam value="#frmPersonID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfif>
	</CFQUERY>
	<CFIF GetOrgid.recordcount GT 0>
		<CFSET frmOrganisationID=GetOrgid.OrganisationID>
	</CFIF>
</CFIF>
<!--- 08-10-2007 GAD : get phone mask application.com.relayplo.getOrgDetails(frmOrganisationID).countryID--->
<cfif isdefined('form.frmPreSelectCountryID')>
	<cfset masknCountryID = form.frmPreSelectCountryID>
<cfelseif isdefined('form.frmLocationID') and val(form.frmLocationID) neq 0>
	<cfset masknCountryID = application.com.relayplo.getLocations(actionOrgs=frmOrganisationID,locationID=form.frmLocationID).countryID >
<cfelseif isdefined('frmOrganisationID') and val(frmOrganisationID) neq 0>
	<cfset masknCountryID = application.com.relayplo.getOrgDetails(frmOrganisationID).countryID> <!--- NJH LID 8098 2011/11/07 --->
</cfif>
<!--- 11-10-2007 GAD : get all countries for org and use this to determine if a Country Select is required; only run when creating new person --->
<CFIF frmPersonID EQ 0 and frmLocationID eq 0>
	<cfset orgLocationCountries =application.com.relayplo.getOrgCountries(frmOrganisationID)>
</CFIF>

<CFIF frmPersonID IS NOT 0 and isNumeric(frmPersonID)>
	<!--- Find Location record information to update --->
	<cfquery name="getPersonDets" datasource="#application.siteDataSource#">
		SELECT p.*,  <!--- WAB 2014-02-26 reverted a change which removed p.*.  Note, needs to be p.* because later passed to screens as the person query --->
			ug.Name AS LastUpdatedBy,l.countryID,
			L.SiteName, L.Address4, L.Address3, L.Address5,
			l.telephone,l.fax, l.postalCode, c.ISOCode, c.dialCode, o.organisationTypeID
		FROM Person p INNER JOIN Location L ON p.LocationID = L.LocationID
		INNER JOIN organisation o on l.organisationID = o.organisationID
			INNER JOIN country c ON l.countryID = c.countryID
			LEFT OUTER JOIN UserGroup ug ON p.LastUpdatedBy = ug.UserGroupID
		WHERE p.PersonID =  <cf_queryparam value="#frmPersonID#" CFSQLTYPE="CF_SQL_INTEGER" >  <!--- <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#frmPersonID#"> --->
	</cfquery>
	<CFSET frmCountryID = getPersonDets.countryID>

	<!--- <cfset phoneMask = application.com.commonQueries.getCountry(countryid=frmCountryID).telephoneformat> --->

	<CFQUERY NAME="getDataSources" datasource="#application.siteDataSource#">
		SELECT d.Description + '-' + pd.RemoteDataSourceID AS DataSource
		  FROM Person AS p, DataSource AS d, PersonDataSource AS pd
		 WHERE d.DataSourceID = pd.DataSourceID
		   AND pd.PersonID = p.PersonID
		   AND p.PersonID =  <cf_queryparam value="#frmPersonID#" CFSQLTYPE="CF_SQL_INTEGER" >
		 ORDER BY d.Description
	</CFQUERY>
	<cfset thisOrgID = getPersonDets.organisationID>

	<CFQUERY NAME="getNoteCount" datasource="#application.siteDataSource#">
		SELECT count(*) as noteCount from entityNote
			where entityID =  <cf_queryparam value="#frmPersonID#" CFSQLTYPE="CF_SQL_INTEGER" >  and entityTypeID=0
	</CFQUERY>

	<CFIF getPersonDets.RecordCount IS 1>
		<CFOUTPUT QUERY="getPersonDets">
			<CFSET qrySalutation = Salutation>
			<CFSET qryOrganisationID = OrganisationID>
			<CFSET qryOrganisationTypeID = OrganisationTypeID><!--- 2007-04-23 AJC Organisation Type ID filter code --->
			<!--- 2013/01/29	YMA		Add social profile image. --->
			<CFSET qryPictureURL = pictureURL>
			<CFSET qryFirstName = FirstName>
			<CFSET qryLastName = LastName>
			<cfset qryJobDesc = JobDesc>
			<CFSET qryUsername = Username>
			<CFSET qryHomePhone = HomePhone>
			<CFSET qryOfficePhone = OfficePhone>
			<CFSET qryOfficePhoneExt = OfficePhoneExt>
			<CFSET qryFaxPhone = FaxPhone>
			<CFSET qryMobilePhone = MobilePhone>
			<CFSET qryEmail = Email>
			<CFSET dialcode = dialcode>
			<CFSET qryeMailStatus = emailStatus>
			<CFSET qryLanguage = Language>
			<CFSET qryNotes = Notes>
			<CFSET qryNoteCount = getNoteCount.NoteCount>
			<CFSET qryCreatedBy = CreatedBy>
			<CFSET qryCreated = Created>
			<CFSET qryLastUpdatedBy = LastUpdatedBy>
			<CFSET qryLastUpdated = LastUpdated>
			<CFSET qryFrmTask = "update">
			<CFSET qryActive = #YesNoFormat(Active)#>
			<CFSET qryLocationID = locationID>
			<CFSET frmLocationID = locationID>
			<cfset qryCrmID = crmPerID>

		</CFOUTPUT>
		<!--- NJH 2008/02/18 Bug Fix - All Sites Issue 19 : added isNull so that we didn't have empty rows returned.
			NJH 2012/02/24 CASE 426697: added person's country to the list of countries if they are editing their own record should they not have rights to their country
		--->

		<!--- START: NYB 2013-03-06 Case 434039 replace query with function call - to minimise number of places that needs editing when display requirements change:
		<CFQUERY NAME="getLocations" datasource="#application.siteDataSource#">
			SELECT l.SiteName
				+ case WHEN len(ltrim(isNull(l.Address1,''))) = 0 then '' else ', ' + l.Address1 end
				+ case WHEN len(ltrim(isNull(l.Address4,''))) = 0 then '' else ', ' + l.Address4 end
				+ case WHEN len(ltrim(isNull(l.Address5,''))) = 0 then '' else ', ' + l.Address5 end
				+ case WHEN len(ltrim(isNull(l.postalCode,''))) = 0 then '' else ', ' + l.postalCode end
				+ case WHEN len(ltrim(isNull(c.ISOCode,''))) = 0 then '' else ', ' + c.ISOCode end
				+ case WHEN len(ltrim(isNull(l.locationID,''))) = 0 then '' else ', ID:' + cast(l.locationID as nvarchar(50)) end
			AS outputAddress,
				l.locationID, l.SiteName, l.countryID
			FROM Location L
				INNER JOIN country c ON l.countryID = c.countryID
			WHERE l.organisationID =  <cf_queryparam value="#getPersonDets.organisationID#" CFSQLTYPE="CF_SQL_INTEGER" >
				<!--- 2012-06-27 PPB P-SMA001		 and l.countryID in (#request.relayCurrentUser.countryList#<cfif request.relayCurrentUser.personID eq getPersonDets.personID>,#request.relayCurrentUser.countryID#</cfif>) --->
			AND	(1=1 #application.com.rights.getRightsFilterWhereClause(entityType="location",alias="l").whereClause# <cfif request.relayCurrentUser.personID eq getPersonDets.personID> OR l.countryID=#request.relayCurrentUser.countryID#</cfif> )    <!--- 2012-06-27 PPB P-SMA001 --->
		</CFQUERY>
		--->
		<cfset additionalFilter = 'AND	(1=1 #application.com.rights.getRightsFilterWhereClause(entityType="location",alias="l").whereClause#'>
		<cfif request.relayCurrentUser.personID eq getPersonDets.personID>
			<cfset additionalFilter = additionalFilter & 'OR l.countryID=#request.relayCurrentUser.countryID#'>
		 </cfif>
		<cfset additionalFilter = additionalFilter & ')'>

		<cfset getLocations = application.com.relayplo.getOrgLocations(OrganisationID=getPersonDets.organisationID,excludeFlaggedForDeletion="true",additionalFilter=additionalFilter)>
		<!--- END: NYB 2013-03-06 Case 434039 --->

	<CFELSE>
		<CFSET message = "Phr_Sys_NoRecordFound">
		<cfset messageType = "info">
		<CFINCLUDE TEMPLATE="datamenu.cfm">
		<CF_ABORT>
	</CFIF>

<CFELSE>
	<!--- should be a new record --->
	<cfif frmOrganisationID neq 0>

		<!--- START: NYB 2013-03-06 Case 434039 replace query with function call - to minimise number of places that needs editing when display requirements change:
		<CFQUERY NAME="getLocations" datasource="#application.siteDataSource#">
			SELECT isnull(l.SiteName,'')
				+ case WHEN ltrim(l.Address1) is null then '' else ', ' + l.Address1 end
				+ case WHEN ltrim(l.Address4) is null then '' else ', ' + l.Address4 end
				+ case WHEN ltrim(l.Address5) is null then '' else ', ' + l.Address5 end
				+ case WHEN ltrim(l.postalCode) is null then '' else ', ' + l.postalCode end
				+ case WHEN ltrim(c.ISOCode) is null then '' else ', ' + c.ISOCode end
				+ case WHEN ltrim(l.locationID) is null then '' else ', ID:' + cast(l.locationID as nvarchar(50)) end
			AS outputAddress,
				l.locationID, l.SiteName, l.countryID
			FROM Location L
				INNER JOIN country c ON l.countryID = c.countryID
				<!--- NYB 2013-03-06 Case 434039 added 1 line: --->
				left join booleanFlagData locDelete with (noLock) on locDelete.entityID = l.locationId and locDelete.flagID = #application.com.flag.getFlagStructure(flagID="deleteLocation").flagID#
			WHERE l.organisationID =  <cf_queryparam value="#frmOrganisationID#" CFSQLTYPE="CF_SQL_INTEGER" >
			<cfif isdefined('form.frmPreSelectCountryID') and isNumeric(form.frmPreSelectCountryID)>
			AND l.countryID =  <cf_queryparam value="#form.frmPreSelectCountryID#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfif>
			<!--- 2012-06-27 PPB P-SMA001		AND l.countryID in (#request.relayCurrentUser.CountryList#) --->
			<!--- NYB 2013-03-06 Case 434039 added 1 line: --->
			and locDelete.entityID is null
			#application.com.rights.getRightsFilterWhereClause(entityType="location",alias="l").whereClause# <!--- 2012-06-27 PPB P-SMA001 --->
		</CFQUERY>
		--->
		<cfset additionalFilter = "">
		<cfif isdefined('form.frmPreSelectCountryID') and isNumeric(form.frmPreSelectCountryID)>
			<cfset additionalFilter = additionalFilter & "AND l.countryID = #form.frmPreSelectCountryID# ">
		</cfif>
		<cfset additionalFilter = additionalFilter & '#application.com.rights.getRightsFilterWhereClause(entityType="location",alias="l").whereClause#'>

		<cfset getLocations = application.com.relayplo.getOrgLocations(OrganisationID=frmOrganisationID,excludeFlaggedForDeletion="true",additionalFilter=additionalFilter)>
		<!--- END: NYB 2013-03-06 Case 434039 --->

		<cfset thisOrgID = frmOrganisationID>
		<CFIF not IsDefined("frmCountryID") and getLocations.recordCount gt 0>
			<CFSET frmCountryID = getLocations.countryID>
		</CFIF>
	</CFIF>
</CFIF>

<!--- NJH 2012/02/24 CASE 426697: changed a bit of the logic here to try and handle every situation.. If all else fails, set the country to 0
	masknCountryID could be "" if they don't have rights to their organisation's country
 --->
<cfif masknCountryID eq 0 or masknCountryID eq "">
	<cfif isDefined("frmCountryID")>
		<cfset masknCountryID = frmCountryID>
	<cfelseif structKeyExists(form,"frmPersonID") and form.frmPersonID eq request.relayCurrentUser.personID>
		<cfset masknCountryID = request.relayCurrentUser.countryID>
	<cfelse>
		<cfset masknCountryID = 0>
	</cfif>
</cfif>
<cfset phoneMask = application.com.commonQueries.getCountry(countryid=masknCountryID).telephoneformat>

<!--- get the organisation type of the person org --->
<cfquery name="getOrgType" datasource="#application.siteDataSource#">
select organisationTypeID from organisation where organisationID =  <cf_queryparam value="#thisOrgID#" CFSQLTYPE="CF_SQL_INTEGER" >
</cfquery>
<cfset orgTypeID = getOrgType.organisationTypeID>

<CFQUERY NAME="getLanguages" datasource="#application.siteDataSource#">
	SELECT Language
	FROM Language
	ORDER BY Language
</CFQUERY>

<CFQUERY NAME="getDefaultEmailDomain" datasource="#application.siteDataSource#">
	SELECT DefaultDomainName from organisation where organisationID =  <cf_queryparam value="#thisOrgID#" CFSQLTYPE="CF_SQL_INTEGER" >
</CFQUERY>

<CFIF frmPersonID IS NOT 0 and getPersonDets.RecordCount IS 1>
	<CFIF getPersonDets.Email eq "">
		<CFSET qryEmail = #getDefaultEmailDomain.DefaultDomainName#>
	</CFIF>

	<!--- 2006-03-03 SWJ added the ability to update perProfileSummary screen from here --->
	<CFQUERY NAME="GetThisPerson" datasource="#application.siteDataSource#">
		Select * FROM Person WHERE personid =  <cf_queryparam value="#frmPersonID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</CFQUERY>
	<cf_getrecord entitytype="person" entityid = "#GetThisPerson.PersonID#" locationid = "#GetThisPerson.LocationID#" getparents>

</CFIF>


<CFPARAM NAME="task" DEFAULT="add">
<CFPARAM NAME="qryFormAction" DEFAULT="/data/persondetailtask.cfm">

<cfif isdefined("LocationIDChanged") AND LocationIDChanged IS NOT 0 >
	<cfset frmLocationid = LocationIDChanged>
	<cfset qryLocationid = LocationIDChanged>
	<cfset qrySalutation = insSalutation>
	<!--- 2013/01/29	YMA		Add social profile image. --->
	<CFSET qryPictureURL = getPersonDets.pictureURL[1]>
	<cfset qryFirstName = insFirstName>
	<cfset qryLastName = insLastName>
	<cfset qryJobdesc = insJobdesc>
	<!--- CRM 428115 - NJH 2012/05/25 - insEmail may not be defined if changing the location for an internal user, as the email will be readonly --->
	<cfif isDefined("insEmail")>
		<cfset qryemail = insEmail>
	</cfif>
	<cfset qryHomePhone = insHomePhone>
	<cfset qryOfficePhone = insOfficePhone >
	<!--- <cfset qryOfficePhoneExt = insOfficePhoneExt>	--->
	<cfset qryFaxPhone = insFaxPhone>
	<cfset qryMobilePhone = insMobilePhone>
	<cfset qryActive = insActive>
</cfif>


<CFPARAM NAME="qryFrmTask" DEFAULT="add">
<CFPARAM NAME="qryPersonID" DEFAULT="0">
<CFPARAM NAME="qrySalutation" DEFAULT="">
<!--- 2013/01/29	YMA		Add social profile image. --->
<CFPARAM NAME="qryPictureURL" DEFAULT="">
<CFPARAM NAME="qryFirstName" DEFAULT="">
<CFPARAM NAME="qryLastName" DEFAULT="">
<cfparam name="qryJobdesc" default="">
<CFPARAM NAME="qryUsername" DEFAULT="">
<CFPARAM NAME="qryHomePhone" DEFAULT="">
<CFPARAM NAME="qryOfficePhone" DEFAULT="">
<CFPARAM NAME="qryOfficePhoneExt" DEFAULT="">
<CFPARAM NAME="qryFaxPhone" DEFAULT="">
<CFPARAM NAME="qryMobilePhone" DEFAULT="">
<CFPARAM NAME="qryEmail" DEFAULT="@#getDefaultEmailDomain.DefaultDomainName#">
<CFPARAM NAME="qryeMailStatus" DEFAULT="0">
<CFPARAM NAME="dialCode" DEFAULT="">
<CFPARAM NAME="qryLanguage" DEFAULT="English">
<CFPARAM NAME="qryNotes" DEFAULT="">
<CFPARAM NAME="qryNoteCount" DEFAULT="0">
<CFPARAM NAME="qryCreatedBy" DEFAULT="">
<CFPARAM NAME="qryCreated" DEFAULT="">
<CFPARAM NAME="qryLastUpdatedBy" DEFAULT="">
<CFPARAM NAME="qryLastUpdated" DEFAULT="">
<CFPARAM NAME="qryActive" DEFAULT="yes">
<CFPARAM NAME="qryLookupID" DEFAULT="0">
<CFPARAM NAME="qryCrmID" DEFAULT="">



<cf_head>
<cf_title><cfoutput>#request.CurrentSite.Title#</cfoutput></cf_title>
	<cf_includeJavaScriptOnce template = "/javascript/checkObject.js"><!--- has isDirty function --->
	<cf_includeJavaScriptOnce template = "/javascript/openWin.js"><!--- has isDirty function --->




<SCRIPT type="text/javascript">

<!---
	<cfoutput> <!---SSS commented because it was causeing trouble gad oked commenting it  --->
	function validatePhone(objectName) {

		thisObject = eval('document.details.'+objectName+'.value.length');
	    if (thisObject = #len(phonemask)#)

			objectName.focus();
			objectName.select();

	    else {
	        alert('One or more Phone field is invalid; all phone number entries must meet format #phonemask# !');

--->


	function movePerson(personID){
		openWin('movePersonToDiffOrg.cfm?frmPersonIds='+personID,'MovePerson','width=520,height=338,toolbar=0,location=0,directories=0,status=1,menuBar=0,scrollBars=0,resizable=1');
		}

	function addNote(personID){
		openWin('entityNoteView.cfm?frmEntityType=Person&frmEntityTypeID=0&frmEntityID='+personID,'AddNote','width=700,height=450,toolbar=0,location=0,directories=0,status=1,menuBar=0,scrollBars=0,resizable=1');
		}

	function editUser(personID){
		var form = document.userEditForm;
		form.frmPersonID.value = personID;
		form.submit();
		}

	function insFirstName_onblur()
		{
		<cfif autoPopulateEmailAddress eq "yes">
		var form = document.details;
		// if the firt character of the email field is blank then we are probably adding a new email address
		if (form.insEmail.value.substring(0,1) == "@"){
			form.insEmail.value =  form.insFirstName.value + form.insEmail.value
			}
		</cfif>
		}

	function insLastName_onblur()
		{
		<cfif autoPopulateEmailAddress eq "yes">
		var form = document.details;
		// if the email field does not contain the lastName string add it
		if (form.insEmail.value.indexOf(form.insLastName.value) == "-1") {
			form.insEmail.value = form.insFirstName.value +"."+ form.insLastName.value + form.frmMailDomain.value
			}
		</cfif>
		}


// WAB function which can be called from the entity navigation
function submitPrimaryForm() {

	var form = document.details;
		okToSubmit = form.onsubmit()
	if (okToSubmit) {
		form.Save.click()
		return true
	} else {
		return false
	}
}

function isPrimaryFormDirty() {

	var form = document.details;
	return isDirty (form)

}


	function createLinkToLinkedIn(){

		window.open('/social/ServiceAccess.cfm?a=authorise&callback=#request.currentSite.protocolAndDomain#/#cgi.SCRIPT_NAME#?#cgi.QUERY_STRING#','LinkedInLogin','width=400px,height=280px')

	}


	function unlinkFromLinkedIn() {

		if (confirm('phr_social_PersonDetailUnlinkButtonAreYouSure') == true) {

			var page = '/webservices/serviceWS.cfc?wsdl&method=unlinkEntityFromService';
			var page = page + '';

			parameters = '';
			var myAjax = new Ajax.Request(
											page,
											{
												method: 'get',
												parameters:  parameters,
												asynchronous:false,
												onComplete: function(){ displayLinkedInButton('Link'); }
											}
										);
		}
	}

	function displayLinkedInButton(btnToShow) {
		if (btnToShow == 'Link') {
			$('btnLinkToLinkedIn').show();
			$('btnUnlinkFromLinkedIn').hide();
		} else {
			$('btnLinkToLinkedIn').hide();
			$('btnUnlinkFromLinkedIn').show();
		}
	}


</SCRIPT>
</cf_head>
<!--- This section refreshes the persList when the personDetail screen is saved.
		frmRedraw is set to no in persondetailtask.cfm when the personDetail is being called from other
		templates e.g. orgList.cfm and selectReview.cfm --->
<CFPARAM NAME="frmRedraw" DEFAULT="Yes">
<CFPARAM NAME="thisEntityType" DEFAULT="person">
<CFIF frmRedraw eq "yes" and findNoCase("personDetailTask.cfm",SCRIPT_NAME) neq 0>
	<CFOUTPUT><cf_body onLoad="javaScript:refreshPerList(#thisOrgID#)"></CFOUTPUT>
<cfelse>

</CFIF>

<CFINCLUDE TEMPLATE="_entityDetailForms.cfm">

<cfset request.relayFormDisplayStyle = "HTML-div">
<cfinclude template="/templates/relayFormJavaScripts.cfm">

<cfform action="#qryFormAction#" method="POST" name="details" noValidate="true">

<cfif isdefined('orgLocationCountries') AND orgLocationCountries.recordcount GT 1 AND NOT structKeyExists(form,"frmPreSelectCountryID") and frmPersonID EQ 0 >
<!--- 2007-10-11 Gad: START cfif orgLocationCountries get country : DO THIS ONLY IF ADDING NEW PERSON--->


		<CF_relayFormDisplay>
			<CF_relayFormElementDisplay relayFormElementType="select" currentValue="" fieldName="frmPreSelectCountryID" query="#orgLocationCountries#" display="CountryDescription" value="CountryID" label="Phr_Sys_Country" nullValue="0" required="yes">
			<!---  2007-10-11 Gad: get all form items and pass them back to form fields --->
			<cfif structKeyExists(form,"fieldnames")>
				<cfloop list="#form.fieldnames#" index="formItem">
					<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#evaluate(formItem)#" fieldname="#formItem#">
				</cfloop>
			<cfelse>
				<!--- WAB 2007/11/28 found that didn't work from new entityscreens because no form variables passed, needed to hack these in --->
					<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#frmOrganisationid#" fieldname="frmOrganisationID">
					<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#frmLocationid#" fieldname="frmLocationID">
			</cfif>
			<CF_relayFormElementDisplay relayFormElementType="submit" fieldname="" currentValue="phr_continue" label="">
		</CF_relayFormDisplay>


<cfelse>

<!--- If you would like to see the cut and paste area all of the time
		comment out the cfif below and the one starting on row 493  MDC 2004-01-09 --->

<cf_relayFormDisplay>
	<TR><TD WIDTH="60%">
	<cf_relayFormDisplay class="">
	<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="@#getDefaultEmailDomain.DefaultDomainName#" fieldname="frmMailDomain">

	<cf_encrypthiddenfields dontRemoveRegExp="frmPersonID">
	<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#qryFrmTask#" fieldname="frmTask">
	<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#frmPersonID#" fieldname="frmPersonID">
	<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#qryEmailStatus#" fieldname="insEmailStatus">
	<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#frmNext#" fieldname="frmNext">
	<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#frmBack#" fieldname="frmBack">
	<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#perPostAddScreenID#" fieldname="perPostAddScreenID">  <!--- NJH 2007/01/26  --->
	<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#frmLocationID#" fieldname="OLDfrmLocationID">
	</cf_encrypthiddenfields>

	<CFIF Trim(message) IS NOT "">
		<cfset application.com.relayUI.setMessage(message=message,messageType=messageType)>
	</CFIF>

	<!--- NJH 2008/05/01 start if salutation, firstname, or lastname is required, make the label = class='required' --->
	<cf_translate phrases= "phr_sys_salutation,phr_sys_firstname,phr_sys_lastname"/>
	<cfif requiredFieldsStruct['insSalutation']>
		<cfset required_salutation = "<span class='required'>#phr_sys_salutation#</span>">
	<cfelse>
		<cfset required_salutation = "#phr_sys_salutation#">
	</cfif>
	<!--- NJH 2008/05/01 end --->

    <!--- 2013/01/29    YMA     Add social profile image. --->
        <cfif frmPersonID gt 0>
            <cfif qryPictureURL eq "">
                <cfset imgSrc = "/images/social/icon_no_photo_80x80.png">
            <cfelse>
                <cfif fileExists(application.UserFilesAbsolutePath & qryPictureURL)>
                    <cfset imgSrc = request.currentsite.HTTPPROTOCOL & request.currentsite.DOMAIN & "/" & qryPictureURL>
                <cfelse>
                    <cfset imgSrc = qryPictureURL>
                </cfif>
            </cfif>
            <cfoutput><img src="#imgSrc#" class="socialImage" style="display:block; padding-bottom:.5em"></cfoutput>
        </cfif>

        <cfif frmPersonID eq request.relayCurrentUser.personId OR frmPersonID gt 0> <!--- 2012/01/31 PPB P-REL112 link to/unlink from linkedIn buttons --->

            <cfset LinkedInEnabled = application.com.settings.getSetting("socialMedia.enableSocialMedia") and (ListFindNoCase(application.com.settings.getSetting("socialMedia.services"),'LinkedIn') gt 0)>    <!--- 2012/01/12 PPB P-REL112 connect to linkedIn --->

            <cfif LinkedInEnabled>
                &nbsp;&nbsp;
                <img id="btnLinkToLinkedIn" src="/images/social/linkedin-notLinked.png" title="phr_social_PersonDetailLinkButtonHover" border="0" style="cursor: pointer;" onclick="javascript:createLinkToLinkedIn();" >
                <img id="btnUnlinkFromLinkedIn" src="/images/social/linkedin-unlink.png" title="phr_social_PersonDetailUnlinkButtonHover" border="0" style="cursor: pointer;" onclick="javascript:unlinkFromLinkedIn();" >

                <cfif application.com.service.hasEntityBeenLinked(serviceId='LinkedIn').linkEstablished>    <!--- 2012/04/20 PPB P-REL112 specifically serviceId='LinkedIn' --->
                    <script language="javascript">
                        displayLinkedInButton('Unlink');
                    </script>
                <cfelse>
                    <script language="javascript">
                        displayLinkedInButton('Link');
                    </script>
                </cfif>
            </cfif>
        </cfif>

	<CF_relayFormElementDisplay relayFormElementType="HTML" currentvalue="" label="#required_salutation#">  <!--- phr_sys_salutation --->

		<!--- AJC 2008/06/24 Put ## around phr_sys_salutation --->
		<CF_DisplayValidValues
			validFieldName = "person.salutation"
			formFieldName =  "insSalutation"
			displayas = "select"
			currentValue = #qrySalutation#
			countryID = #masknCountryID#
			entityID = #frmPersonID#
			required = #requiredFieldsStruct['insSalutation']#
			requiredLabel="#Phr_Sys_Salutation#"
			useCFFORM = true
			>

		</CF_relayFormElementDisplay>

    <CF_relayFormElementDisplay relayFormElementType="text" currentValue="#qryFirstName#" fieldName="insFirstName" label="phr_sys_firstname" maxlength="24" onBlur="if (document.details.frmPersonID.value==0){insFirstName_onblur()}" required="#requiredFieldsStruct['insFirstName']#" message="Phr_Sys_FirstName is a required field">
    <CF_relayFormElementDisplay relayFormElementType="text" currentValue="#qryLastName#" fieldName="insLastName" label="phr_sys_lastname" maxlength="25" onBlur="if (document.details.frmPersonID.value==0) {insLastName_onblur()}" required="#requiredFieldsStruct['insLastName']#" message="Phr_Sys_LastName is a required field" >


<!--- 	<!--- reset this value to set for new value updated persondetailTask ; actually the form has been submitted and the new value must be assigned
	take this out and the form will continue loading whatever value is stored in the DB for LocationID --->
	<cfif frmPersonID NEQ 0 and oldfrmLocationID neq 0>
		<cfset frmLocationID = oldfrmLocationID>
	</cfif>
 --->
	<cfif frmPersonID EQ 0>
		<CF_relayFormElementDisplay relayFormElementType="select" currentValue="#frmLocationID#" fieldname="frmLocationID" label="Phr_Sys_LocatedAt" query="#getLocations#" value="locationID" display="outputAddress" nullText="Phr_Sys_SelectLocation" nullValue="" required="yes" >
	<cfelse>
		<CF_relayFormElementDisplay relayFormElementType="select" currentValue="#frmLocationID#" fieldname="frmLocationID" label="Phr_Sys_LocatedAt" query="#getLocations#" value="locationID" display="outputAddress" nullText="Phr_Sys_SelectLocation" nullValue="" required="yes" onChange="if(_CF_checkdetails(document.getElementById('details'))){document.details.submit();}"><!--- 2013-03-26	YMA	Case 430471 Stop location change submitting without form validation --->
	</cfif>
	<!--- hidden field used for server side validation for the above select --->
	<!--- <CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="Invalid location value" fieldname="frmLocationID_required"> --->

	<CF_relayFormElementDisplay relayFormElementType="text" currentValue="#qryJobDesc#" fieldName="insJobDesc" label="Phr_Sys_JobDesc" size="50" maxlength="255" required="#requiredFieldsStruct['insJobDesc']#">

	<cfset entityRights = application.com.rights.getUserRightsForEntity(entityID=frmPersonID,entityType="person",entityTypeID=application.entityTypeID.person,countryID=masknCountryID)>

	<cfset rights = application.com.rights.getUserRightsToEntityField(entityType="person",field="email",entityRights=entityRights,entityId=frmPersonID)>

	<CF_relayFormElementDisplay relayFormElementType="#rights.edit?'text':'html'#" currentValue="#qryEmail#" fieldName="insEmail" label="Phr_Sys_Email" NoteText="Phr_Sys_EmailStatus: #qryEmailStatus#" NoteTextPosition="right" size="50" maxlength="200" validate="email" validateAt="onSubmit" onValidate="validateEmail" required="#requiredFieldsStruct['insEmail']#" personid = #frmPersonID# orgTypeID=#orgTypeID#>	<!--- WAB added personid attribute to allow us to validate unique email --->

	<cfif application.com.relayPLO.isUniqueEmailValidationOn().isOn>
		<CF_relayFormElementDisplay relayFormElementType="html" label= "" currentvalue = "phr_sys_formValidation_UniqueEmailOnScreenLabel" >
	</cfif>


	<cfsavecontent variable="intlCodeLabel_html">
		<cfoutput>
			<cfif trim(#dialCode#) neq ''>: (Phr_Sys_IntlCode: +#dialCode#)</cfif>
		</cfoutput>
	</cfsavecontent>
	<cfsavecontent variable="telephoneNoteText_html">
	  <cfoutput>
		  <CFIF frmPersonID IS NOT 0>Phr_Sys_Switchboard: #getpersonDets.Telephone#</CFIF>
	  </cfoutput>
	</cfsavecontent>

	<!--- 	ESZ 2016/01/06 case 446546 - Office Phone not being fully displayed on the text box replace all special symbols if phonemask is not null
			WAB 2016-02-08	BF-409, only replace special symbols if phone number does not match the mask
					If we replace the masking characters each time (and they are then re-added by the javascript), the form always thinks that it is dirty and asks the user to do a save, even though no changes have been made
	--->
	<cfif len(trim(phonemask)) and NOT application.com.regExp.doesStringMatchMask (string = qryOfficePhone, mask = phonemask)>
		<cfset qryOfficePhone = REReplace(qryOfficePhone,"[^0-9]","","all")>
	</cfif>

	<!--- NJH 2010/06/01 LID3405 - trim the phone numbers --->
	<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentvalue="#phonemask#" fieldname="phonemask">
	<CF_relayFormElementDisplay relayFormElementType="text" currentValue="#trim(qryOfficePhone)#" fieldName="insOfficePhone"  label="Phr_Sys_DirectLine#intlCodeLabel_html#"size="25" mask="#phonemask#" noTR="START" required="#requiredFieldsStruct['insOfficePhone']#" validate="telephone">
	<CF_relayFormElementDisplay relayFormElementType="text" currentValue="#trim(qryOfficePhoneExt)#" fieldName="insOfficePhoneExt" label="Phr_Sys_DirectLineExt"  size="7" noTR="END"   NoteText="#telephoneNoteText_html#" NoteTextPosition="right" NoteTextImage="no">
	<CF_relayFormElementDisplay relayFormElementType="text" currentValue="#trim(qryHomePhone)#" fieldName="insHomePhone" label="Phr_Sys_HomePhone" size="25" maxlength="30"   mask="#phonemask#" required="#requiredFieldsStruct['insHomePhone']#" validate="telephone">
	<CF_relayFormElementDisplay relayFormElementType="text" currentValue="#trim(qryMobilePhone)#" fieldName="insMobilePhone" label="Phr_Sys_Mobile" size="25" maxlength="30"   mask="#phonemask#"  required="#requiredFieldsStruct['insMobilePhone']#" validate="telephone">

	<cfsavecontent variable="faxNoteText_html">
		<cfoutput>
			<CFIF frmPersonID IS NOT 0>Phr_Sys_MainFaxNo: #getpersonDets.Fax#</CFIF>
		</cfoutput>
	</cfsavecontent>
	<CF_relayFormElementDisplay relayFormElementType="text" currentValue="#trim(qryFaxPhone)#" fieldName="insFaxPhone" label="Phr_Sys_Fax" size="25" maxlength="30" NoteText="#faxNoteText_html#" NoteTextPosition="right" NoteTextImage="no" mask="#phonemask#" required="#requiredFieldsStruct['insFaxPhone']#" validate="telephone">
	<CF_relayFormElementDisplay relayFormElementType="select" currentValue="#qryLanguage#" fieldName="insLanguage" label="Phr_Sys_Language" query="#getLanguages#" display="Language" value="Language" nullText="Phr_Sys_Default" nullValue="" NoteText="Phr_Sys_DefaultLangEnglish" NoteTextPosition="right" required="#requiredFieldsStruct['insLanguage']#">

	<cfset loginExpiresInDays = application.com.settings.getSetting("security.internalsites.passwords.expiresin_days ")>

	<!--- NJH 2010/08/10 RW8.3 - replaced application. PassValidFor with loginExpiresInDays --->
	<CFIF frmPersonID IS NOT 0 and (DateDiff("d", getPersonDets.PasswordDate, Now()) LTE loginExpiresInDays)
		AND (DateDiff("s",Now(),getPersonDets.LoginExpires) GT 0)
		AND (Trim(getPersonDets.Username) IS NOT "")
		AND (Trim(getPersonDets.Password) IS NOT "")>
		<cf_querySim>
			getStati
			display,value
			Phr_Sys_Yes|1
		</cf_querySim>
		<CF_relayFormElementDisplay relayFormElementType="radio" currentValue="#IIF(YesNoFormat(qryActive) IS 'yes',1,0)#" fieldName="insActive" label="Phr_Sys_Active" query="#getStati#" display="display" value="value" noteText="Phr_Sys_NotSetvalidUserInactive" noteTextPosition="right">
	<CFELSE>
		<!--- false --->
		<cf_querySim>
			getStati
			display,value
			Phr_Sys_Yes|1
			Phr_Sys_No|0
		</cf_querySim>
		<CF_relayFormElementDisplay relayFormElementType="radio" currentValue="#IIF(YesNoFormat(qryActive) IS 'yes',1,0)#" fieldName="insActive" label="Phr_Sys_Active" query="#getStati#" display="display" value="value">
	</CFIF>

	<cfif application.com.relayMenu.isModuleActive("connector")>
			<cfset synchingStatus = application.getObject("connector.com.connector").getEntitySynchingStatus(entityType="person",entityID=frmPersonID)>

		<CF_relayFormElementDisplay relayFormElementType="text" fieldName="crmID" currentValue="#qryCrmID#" label="phr_Sys_#application.com.settings.getSetting('connector.type')#ID" disabled="true" NoteTextImageSrc="#synchingStatus.statusImage#" NoteText="#synchingStatus.status#" noteTextPosition="right">
	</cfif>

<!--- Flags --->
<!--- 		<TR>
			<TD VALIGN="TOP" COLSPAN=7><BR>
			<CFSET flagMethod = "edit">
			<CFSET entityType = 0>
			<CFSET thisEntity = frmPersonID>
			<CFINCLUDE TEMPLATE="../flags/allFlag.cfm">
			<BR><BR></TD>
		</TR>
 --->
 	<CFIF frmPersonID IS NOT 0>
		<CF_relayFormElementDisplay relayFormElementType="HTML" currentvalue="#Replace(ValueList(getDataSources.DataSource), ',', '<BR>', 'ALL')#" label="Phr_Sys_RemoteDataSources">

		<CFIF getPersonDets.RecordCount IS 1>
				<cf_entityLastUpdatedInfo
				created="#getPersonDets.Created#"
				createdby="#getPersonDets.CreatedBy#"
				lastUpdated="#getPersonDets.LastUpdated#"
				lastUpdatedby="#getPersonDets.LastUpdatedBy#">

			<CF_relayFormElementDisplay relayFormElementType="hidden" fieldname="insLastUpdated" currentvalue="#CreateODBCDateTime(getPersonDets.LastUpdated)#" label="">
			<CF_relayFormElementDisplay relayFormElementType="HTML" currentvalue="#frmPersonID#" label="Phr_Sys_PersonID">

<!--- 	 		<cfsavecontent variable="notes">
				<a name="Notes"></a>
				<cfmodule template="/data/entityNoteList.cfm" frmEntityType="Person" frmEntityID="#frmPersonID#" frmEntityTypeID="0">
			 	<cf_relayFormElement relayFormElementType="hidden" fieldname="insNotes" currentValue="#qryNotes#">
			</cfsavecontent>
			<cf_relayFormElementDisplay relayFormElementType="HTML" currentvalue="#notes#" spanCols="yes">--->

			<cfif perDetailsScreenID neq "">

			<!--- 2007-05-02 AJC Check for organisation Type specific profile summary screen & if none, checks the past screen too --->
			<cfif not isnumeric(perDetailsScreenID)>
				<cfscript>
					perDetailsScreenID = application.com.screens.getScreenForOrgType("#perDetailsScreenID#",organisation.organisationID,"");
				</cfscript>
			</cfif>

				<CF_relayFormElementDisplay relayFormElementType="SCREEN" currentvalue="">
					<cf_ascreen formName = "details">
						<cf_ascreenitem screenid="#perDetailsScreenID#"
								method="edit"
								person="#getPersonDets#"
								location="#location#"
								organisation="#organisation#"
						>
					</cf_ascreen>
				</CF_relayFormElementDisplay>
			</cfif>

		</CFIF>
	</CFIF>
</cf_relayFormDisplay>
</TD>
<!--- <CFIF NOT frmPersonID>
		<TD valign="top" align="left">
		    Phr_Sys_CutPasteArea<br>
			<CF_relayFormElement relayFormElementType="textArea" fieldname="textBox" currentValue="" label="" cols="35" rows="10" noteText="Phr_Sys_CutPasteArea" NoteTextPosition="top">
		</TD>
</CFIF> --->
</TR>
		<cf_relayFormElementDisplay relayFormElementType="submit" fieldname="Save" currentValue="phr_save" label=""></TD>
</cf_relayFormDisplay>
<!--- END cfif orgLocationCountries --->
</cfif>

</cfform>

<form action="/admin/userManagement/userEdit.cfm" target="Detail" name="userEditForm">
	<CF_relayFormElement relayFormElementType="hidden" label="" fieldname="frmPersonID" currentvalue="0">
	<CF_relayFormElement relayFormElementType="hidden" label="" fieldname="frmType" currentvalue="Last">
	<CF_relayFormElement relayFormElementType="hidden" label="" fieldname="frmNameText" currentvalue="">
	<CF_relayFormElement relayFormElementType="hidden" label="" fieldname="frmCountryID" currentvalue="0">
	<CF_relayFormElement relayFormElementType="hidden" label="" fieldname="frmPersonType" currentvalue="Last">
</form>
</cf_translate>