<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			.cfm	
Author:				SWJ
Date started:			/xx/02
	
Purpose:	

Usage:	

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:
This will include:

1. List of comms this person's been sent
2. Usage history
3. When their password was sent.
4. When they first and last logged on 
5. What they looked at 

We also need
6. Record changes history
7. Person Transaction History


WAB 2007-04-25 did some work on query to make it a bit nicer - still a bit odd
WAB/NJH 2008-10-27 Bug Fix Lenovo Issue 26. Helped clear up some of the reporting issues. We have adopted only a temporary fix in the sense
					that web visits and ad-hoc emails are still being stored as the same communication type. Ideally, these types would need to 
					be separated and it appears that Will has already started this on dev on the ATI server.
NJH		2009-01-26	Bug Fix Trend Support Issue 1438 - now include relay/screen/personCommunicationReport.cfm for person communications to consolidate code.
 --->

<cfif not isDefined("pid") and isDefined("frmPersonid")>
	<!--- need to be able to be called from entityScreens --->
	<cfset pid = frmPersonid>

</cfif>

<cf_head>
	<cf_title>Person Communication Overview</cf_title>
</cf_head>



<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
	<TR>
		<TD ALIGN="LEFT" CLASS="Submenu">&nbsp;</TD>
		<TD COLSPAN="2" ALIGN="right" CLASS="Submenu">
			<A HREF="javascript:history.go(-1);" Class="Submenu">Back</A> &nbsp;&nbsp; 
		</TD>		
	</TR>
</TABLE>

<!--- <CFQUERY name="CommsSent" dataSource="#application.SiteDataSource#" maxrows = "50">
<!--- WAB 2007-05-16 got rid of references to wab status and wabcommstatuslookup --->
<!--- WAB/NJH 2008-10-27 Bug Fix Lenovo Issue 26 - Help clear up some of the 'discrepencies' for those
	comms of type 'Web Visit', although this really is only a temporary fix.
	
	We are now storing email subjects as part of the feedback. They are separated from the body with a '#application.delim1#' --->
	
SELECT 
	case when charindex('#application.delim1#',feedback) <> 0 then left (feedback,charindex('#application.delim1#',feedback)-1) when cd.commTypeID = 11 then 'Web Visit' else c.Title end as title,
<!--- c.Title, ---> 
			c.commid, 
<!--- 			c.SendDate,  --->
			cd.dateSent as sendDate, 
<!--- 			LL1.ItemText as FormText,  --->
			CDT.Name as FormText, 
			cd.commstatusid as status,
			case when cd.commTypeID = 11 then '' else LL.ItemText end as TypeText,
			<!--- LL.ItemText as TypeText, --->
			s.name as statustext,
			cd.commStatusID as feedback,
			cd.fax as addressused
	FROM CommDetail cd 
		INNER JOIN Communication c on  cd.CommID = c.CommID
		INNER JOIN LookupList LL on  c.CommTypeLID = LL.LookupID
		INNER JOIN commdetailType cdt on cd.CommTypeID = CDT.commtypeid
		INNER JOIN commDetailStatus s on cd.commStatusID=s.commStatusID
	WHERE cd.PersonID=#pid#
	AND c.CommFormLID <>4 
	AND c.CommTypeLID <>50
	ORDER BY cd.datesent desc
	</CFQUERY> --->
	
	<!--- NJH 2009-01-26 - Bug Fix Trend Support 1438 - in an effort to consolidate querys, made a function --->
<!--- 	<cfscript>
		commsSent = application.com.communications.getCommsSentForPerson(personId=pid);
	</cfscript>
	
	<CFQUERY name="getperson" dataSource="#application.siteDataSource#">
	Select 	person.FirstName as firstname,
			person.lastName as lastname
	From 	Person
	Where	Personid=#pid#
	</cfquery>
	
	
	<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
	<tr><td colspan="6"><CFOUTPUT><strong>Communications Sent to #getperson.Firstname# #getperson.lastname#</strong></cfoutput></td></tr>
	<TR>
		<TH>Date<BR>CommID</TH>
		<TH>Title</TH>
		<TH>By</TH>
		<TH>Type</TH>
		<TH>Address or Fax #</TH>
		<TH>Status</TH>
	</TR>
	
	<CFOUTPUT query="commssent" maxrows="70">
		<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
		    <TD>#DateFormat(senddate,"DD/MM/YY")#<BR>#commid#</TD>
			<TD>#title# (#commid#)</font></TD>
			<TD>#formText#</TD>
			<TD>#TypeText#</TD>
			<TD>#Addressused#</TD>
			<TD>#statustext# #iif(status LE 0, DE(". Feedback: " & feedback), DE(""))#</TD>
		</TR>
	</cfoutput>
	</TABLE> --->

<cfset frmentitytype = "person">
<cfset frmcurrententityid = pid>	
<cfinclude template="/screen/personCommunicationReport.cfm">

<CFQUERY name="Selections" dataSource="#application.siteDataSource#">
	SELECT 	s.Title, 
			s.selectionid, 
			s.created,
			u.name
	FROM 	Selection as s, SelectionTag as st, usergroup as u
	WHERE 	s.selectionid=st.selectionid
			AND s.createdby=u.usergroupid
			AND st.entityid =  <cf_queryparam value="#pid#" CFSQLTYPE="CF_SQL_INTEGER" > 
	ORDER BY s.selectionid DESC
</CFQUERY>
	

<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center"<!---  BGCOLOR="White"  --->CLASS="withBorder">
	<tr><td colspan="3"><CFOUTPUT><strong>Selections containing #htmleditformat(getperson.Firstname)# #htmleditformat(getperson.lastname)#</strong></cfoutput></td></tr>
	<TR bgcolor="cccccc">
		<TH>Date</TH>
		<TH>Title</TH>
		<TH>Owner</TH>
	</TR>
	
	<CFOUTPUT query="Selections">
		<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
		    <TD>#DateFormat(created,"DD/MM/YY")#</TD>
			<TD>#htmleditformat(title)# (#htmleditformat(selectionid)#)</font></TD>
			<TD>#htmleditformat(name)#</TD>
		</TR>
	
	</cfoutput>
</TABLE>

<cfset sortorder = "c.created desc">
	
<cfquery name="getDownloads" datasource="#application.SiteDataSource#">
	select distinct ug.name as downloaded_by, description, title as download_name, 
		(select count(1) from commdetail where commdetail.commid = c.commid) total_records_downloaded,
		c.created as date_downloaded
	from communication c 
	inner join commdetail cd on c.commid = cd.commid 
	inner join usergroup ug on ug.usergroupid = c.createdby 
	where cd.personid =  <cf_queryparam value="#pid#" CFSQLTYPE="CF_SQL_INTEGER" >  
	and feedback = 'downloaded'
	order by <cf_queryObjectName value="#sortOrder#">
</cfquery> 

<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
	<tr><td><CFOUTPUT><strong>Downloads containing #htmleditformat(getperson.Firstname)# #htmleditformat(getperson.lastname)#</strong></cfoutput></td></tr>
</TABLE>
<cfif IsQuery(getDownloads)>
	<CF_tableFromQueryObject queryObject="#getDownloads#" 
	
	HidePageControls="yes"
	useInclude = "false"
	dateFormat = "date_downloaded"
	
	sortorder="#sortOrder#">
</cfif>






