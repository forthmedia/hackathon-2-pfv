<!--- �Relayware. All Rights Reserved 2014 --->
<!--- amendment History
	1999-04-13	WAB Added fields required for search by first and last name --->

<!--- user rights to access:
		SecurityTask:dataTask
		Permission: Add
---->

<CFIF NOT #checkPermission.AddOkay# GT 0>
	<!--- if frmLocationID is 0 then task is to add
	      but the user doesn't have add permissions --->

	<CFSET message="You are not authorised to use this function">
		<CFINCLUDE TEMPLATE="datamenu.cfm">
	<CF_ABORT>

</CFIF>

<!--- if they don't already exist, create the cookies
		do not conditionally display buttons to
		give user the choice of re-checking --->

<cf_head>
<cf_title><CFOUTPUT>#request.CurrentSite.Title#</CFOUTPUT></cf_title>
</cf_head>

<FONT FACE="Arial, Chicago, Helvetica" SIZE=2>

<CFOUTPUT>
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
<TR>

<TD ALIGN="LEFT" VALIGN="TOP"><IMG SRC="/images/MISC/title_data_e.gif" WIDTH=180 HEIGHT=23 BORDER=0 ALT="Data"></TD>
<TD ALIGN="RIGHT" VALIGN="TOP">
	<A HREF="#thisdir#/locSearch.cfm"><IMG SRC="/images/BUTTONS/txt_search_e.gif" WIDTH=81 HEIGHT=18 BORDER=0 ALT="Search"></A>
	<!--- list the templates on which to include this --->
</TD>
</TR></CFOUTPUT>
</TABLE>

<CENTER>

<P><TABLE BORDER=0 BGCOLOR="#FFFFCC" CELLPADDING="5" CELLSPACING="0" WIDTH="320">
	
	<TR><TD COLSPAN="2" ALIGN="CENTER" BGCOLOR="#000099"><FONT FACE="Arial, Chicago, Helvetica" SIZE="2" COLOR="#FFFFFF"><B>DEDUPE LOCATION FEEDBACK</B></FONT></TD></TR>
	<TR><TD ALIGN="CENTER" COLSPAN=2><FONT FACE="Arial, Chicago, Helvetica" SIZE=2>		
			<TABLE BORDER=0 CELLSPACING=0 CELLPADDING=2>
			<TR><TD><FONT FACE="Arial, Chicago, Helvetica" SIZE="2">
			
			<CFOUTPUT>
			<CFIF good IS NOT "">
			<P>The following record#IIF(ListLen(good) IS 1, DE(" was"), DE("s were"))# moved: #htmleditformat(good)#
			<BR>#IIF(ListLen(good) IS 1, DE("This"), DE("These"))# record#IIF(ListLen(good) IS 1, DE(" is"), DE("s are"))# now linked to location: #htmleditformat(frmLocationID)#
			</CFIF>
			<CFIF bad IS NOT "">
			<P>The following record#IIF(ListLen(bad) IS 1, DE(" was"), DE("s were"))# NOT moved: #htmleditformat(bad)#
			<BR>#IIF(ListLen(bad) IS 1, DE("This"), DE("These"))# record#IIF(ListLen(bad) IS 1, DE(""), DE("s"))# have either been moved or updated since selection
			</CFIF>
			</CFOUTPUT>
				</FONT></TD>
			</TR>
			</TABLE>
		</FONT></TD>
	</TR>
</TABLE>
<CFOUTPUT>
<P><FORM NAME="ThisForm" METHOD="POST" ACTION="locmain.cfm">
<TABLE BORDER="0" CELLSPACING="5" CELLPADDING="5"><TR>
	<TD VALIGN=TOP>
	<A HREF="JavaScript:document.ThisForm.submit();"><IMG SRC="/images/buttons/c_checkloc_e.gif" WIDTH=145 HEIGHT=21 BORDER=0 ALT=""></A>
	</TD>
</TR></TABLE>
	<CF_INPUT TYPE="HIDDEN" NAME="frmDupLocs" VALUE="#frmDupLocs#">
</CFOUTPUT>
		<CFINCLUDE template="searchparamform.cfm">
<CFOUTPUT>		
		<CF_INPUT TYPE="HIDDEN" NAME="frmLocationID" VALUE="#frmLocationID#">
</FORM>
</CFOUTPUT>

</CENTER>
</FONT>




<CF_ABORT>

