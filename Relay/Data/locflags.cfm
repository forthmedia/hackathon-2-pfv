<!--- �Relayware. All Rights Reserved 2014 --->
<!--- amendment History
	1999-04-13	WAB Added fields required for search by first and last name 
	2008-10-21 NJH Lenovo Support Bug Fix 1196 - use cfform rather than form.
--->

<CFIF NOT checkPermission.UpdateOkay GT 0>
	<!--- if frmPersonID is not 0 then task is to update
	      but the user doesn't have update permissions --->
	<CFSET message="You are not authorised to use this function">
	<CFINCLUDE TEMPLATE="datamenu.cfm">
	<CF_ABORT>

</CFIF>


<!--- 2012-07-25 PPB P-SMA001 commented out
<cfinclude template="/templates/qrygetcountries.cfm">
--->

<CFPARAM NAME="frmBack" DEFAULT="datamenu">
<CFPARAM NAME="frmNext" DEFAULT="datamenu">
<CFPARAM NAME="frmLocationID" DEFAULT="0">
<CFPARAM NAME="message" DEFAULT="">

<CFQUERY NAME="getLocInfo" datasource="#application.siteDataSource#">
	SELECT l.LocationID,
		  l.SiteName,
		  l.Telephone,
		  l.PostalCode,
  		  l.LastUpdated,
		  c.CountryDescription,
		  c.ISOCode
	FROM Location AS l, Country AS c
	WHERE l.CountryID = c.CountryID
	  <!--- 2012-07-25 PPB P-SMA001 commented out
	  AND l.CountryID  IN ( <cf_queryparam value="#Variables.CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) <!--- from qrygetcountries.cfm --->
	   --->
	  #application.com.rights.getRightsFilterWhereClause(entityType="location",alias="l").whereClause#	<!--- 2012-07-25 PPB P-SMA001 --->	
	  AND c.CountryID = l.CountryID<!--- AND c.CountryID IN (#Variables.CountryList#) --->
	  AND l.LocationID =  <cf_queryparam value="#frmLocationID#" CFSQLTYPE="CF_SQL_INTEGER" > 
</CFQUERY>

<CFIF getLocInfo.RecordCount IS 0>

	<CFSET message = "You did not select a location, or the location could not be found.">
	<CFINCLUDE TEMPLATE="datamenu.cfm">
	<CF_ABORT>

</CFIF>

<!--- <CFQUERY NAME="getDataSources" datasource="#application.siteDataSource#">
	SELECT d.Description & '-' & pd.RemoteDataSourceID AS DataSource
	  FROM Location AS p, DataSource AS d, LocationDataSource AS ld
	 WHERE d.DataSourceID = ld.DataSourceID
	   AND ld.LocationID = l.LocationID
	   AND l.LocationID = #frmLocationID#
	 ORDER BY d.Description
</CFQUERY> --->

	
<!--- <CFPARAM NAME="qryCreatedBy" DEFAULT="">
<CFPARAM NAME="qryCreated" DEFAULT="">
<CFPARAM NAME="qryLastUpdatedBy" DEFAULT="">
<CFPARAM NAME="qryLastUpdated" DEFAULT="">

<CFPARAM NAME="qryLookupID" DEFAULT="0"> --->




<cf_head>
<cf_title><cfoutput>#request.CurrentSite.Title#</cfoutput></cf_title>
<SCRIPT type="text/javascript">

<!--
	function verifyForm(back,next) {
		var form = document.FlagForm;
//		form.frmTask.value = task;
//		form.frmNext.value = next;
//		form.frmBack.value = back;
		var msg = "";
		var found = "no";
				
		//for (j=0; j<form.elements.length; j++){
		//	thiselement = form.elements[j];
		//	if (thiselement.name == "insPartnerType") {
		//		if (thiselement.checked) {
		//			found = "yes";
		//		}
		//	}
		//}

		//if (found == "no") {
		//	msg += "* Partner Type (at least one)\n";
		//}

		if (msg != "") {
			alert("\nYou must provide the following information for this location:\n\n" + msg);
		} else {
			form.submit();
		}
}
//-->

</SCRIPT>
</cf_head>


<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
	<TR class="Submenu">
		<TD ALIGN="LEFT" VALIGN="TOP" class="Submenu">CURRENT RECORD SUMMARY</TD>
	</TR>
</TABLE>

<CFIF Trim(message) IS NOT "">
<P><CFOUTPUT>#application.com.security.sanitiseHTML(message)#</CFOUTPUT>
</CFIF>


<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" CLASS="withBorder">	

	
	<CFIF getLocInfo.RecordCount IS NOT 0>

		<TR><TD VALIGN="TOP" ALIGN="CENTER" COLSPAN=2>
			<TABLE BORDER=0 CELLSPACING=0 CELLPADDING=2>
				<TR><TD class="Label" VALIGN="TOP">
						Location ID:
					</TD>
					<TD VALIGN="TOP">
						<CFOUTPUT>#htmleditformat(getLocInfo.LocationID)#</CFOUTPUT>
					</TD>
				</TR>
		
				<TR><TD class="Label" VALIGN="TOP">
						Organisation Name:
					</TD>
					<TD VALIGN="TOP">
						<CFIF #Trim(getLocInfo.SiteName)# IS "">&nbsp;<CFELSE>
						<CFOUTPUT>#HTMLEditFormat(Trim(getLocInfo.SiteName))#</CFOUTPUT>
						</CFIF>
					</TD>
				</TR>
				<TR><TD class="Label" VALIGN="TOP">
						Postal Code:
					</TD>
					<TD VALIGN="TOP">
						<CFIF #Trim(getLocInfo.PostalCode)# IS "">&nbsp;<CFELSE>
						<CFOUTPUT>#HTMLEditFormat(Trim(getLocInfo.PostalCode))#</CFOUTPUT>
						</CFIF>
					</TD>
				</TR>
				<TR><TD class="Label" VALIGN="TOP">
						Telephone:
					</TD>
					<TD VALIGN="TOP">
						<CFIF #Trim(getLocInfo.Telephone)# IS "">&nbsp;<CFELSE>
						<CFOUTPUT>#HTMLEditFormat(Trim(getLocInfo.Telephone))#</CFOUTPUT>
						</CFIF>
					</TD>
				</TR>
				<TR><TD class="Label" VALIGN="TOP">
						Country (ISO Code):
					</TD>
					<TD VALIGN="TOP">
						<CFIF #Trim(getLocInfo.CountryDescription)# IS "">&nbsp;<CFELSE>
						<CFOUTPUT>#HTMLEditFormat(Trim(getLocInfo.CountryDescription))# (#HTMLEditFormat(Trim(getLocInfo.ISOCode))#)</CFOUTPUT>
						</CFIF>
					</TD>
				</TR>
		
				</TABLE>
			</TD>
		</TR>
		
	<CFELSE>
		<TR><TD ALIGN="CENTER" COLSPAN=2>
				The specified location could not be found.
			<BR></TD>
		</TR>
		</TABLE>
		</CENTER>
		
		
		
		<!--- messy end for messy situation --->
		<CF_ABORT>
	</CFIF>
</TABLE>

<!--- NJH 2008-10-21 Lenovo Support Bug Fix 1196 - use cfform as editFlagText now uses a cfinput for mask functionality --->
<P><CFFORM ACTION="locflagstask.cfm" METHOD="POST" NAME="FlagForm">
	<CF_INPUT TYPE="HIDDEN" NAME="insLastUpdated" VALUE="#CreateODBCDateTime(getLocInfo.LastUpdated)#">	
	<CF_INPUT TYPE="HIDDEN" NAME="frmLocationID" VALUE="#frmLocationID#">	
	<INPUT TYPE="HIDDEN" NAME="frmTask" VALUE="update">
	<CF_INPUT TYPE="HIDDEN" NAME="frmDupPeople" VALUE="#frmDupPeople#">	
	<CF_INPUT TYPE="HIDDEN" NAME="frmDupLocs" VALUE="#frmDupLocs#">
	
	<CFINCLUDE template="searchparamform.cfm">
	
	<CF_INPUT TYPE="HIDDEN" NAME="frmStartp" VALUE="#frmStartp#">			
	<CF_INPUT TYPE="HIDDEN" NAME="frmStart" VALUE="#frmStart#">
	<CFIF IsDefined("frmPersonID")>
		<CF_INPUT TYPE="HIDDEN" NAME="frmPersonID" VALUE="#frmPersonID#">
	</CFIF>
	<CF_INPUT TYPE="HIDDEN" NAME="frmBack" VALUE="#frmBack#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmNext" VALUE="#frmNext#">

<P><TABLE BORDER=0>

<!--- New Flags --->
	<TR>
		<TD VALIGN="TOP" COLSPAN=2><BR>
		<!--- NJH 2008-10-21 Lenovo Support Bug Fix 1196 get/set the countryID --->
		<cfquery name="getLocationCountry" datasource="#application.siteDataSource#">
			select countryID from location where locationID =  <cf_queryparam value="#frmLocationID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfquery>
		
		<CFSET countryID = getLocationCountry.countryID>
		<CFSET flagMethod = "edit">
		<CFSET entityType = 1>
		<CFSET thisEntity = frmLocationID>
		<cfset thisFormName = "FlagForm">		<!--- added WAB 2005-10-19  - needed by some flags with fancy valid value drop downs--->
		<CFINCLUDE TEMPLATE="../flags/allFlag.cfm">
		<BR><BR></TD>
	</TR>

</TABLE>


<CFOUTPUT>
<P><CF_INPUT type="button" id="verifyFormBtn" value="Save and Return" onClick="JavaScript:verifyForm('#frmBack#','#frmBack#');">
<!--- <A HREF="JavaScript:verifyForm('#frmBack#','#frmBack#');"><IMG SRC="/images/buttons/c_savereturn_e.gif" WIDTH=145 HEIGHT=22 ALT="Save and Return" BORDER="0"></A> --->
</CFOUTPUT>



</CFFORM>

</CENTER>



