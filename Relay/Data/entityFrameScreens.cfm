<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			entityFrameScreens.cfm
Author:				WAB from entityScreens.cfm by SWJ
Date started:		2007/12

Purpose:

Usage:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed

2001-10-24		SWJ created and developed this to work with per loc & org recs
2008-05			WAB new function to get the entity details
2008-05-14		WAB allow files to be "included" using CFLocation (set useCFLocation in the screen Parameters)
2008-05-27		WAB problem when called not in frame set with parameter frmCurrentEntityID needed to change to frmEntityID before reloading the parent frame
2008-07-08 		CR-SNY646 WAB dealing with refreshing perslist when adding new person
2008-07-09 		CR-SNY646 WAB dealing with openasexcel
2008-10-01		WAB Added frmPersonID, frmLocationID and frmOrganisationID to the screens form (code copied from entityScreens.cfm)>
2008-12-03 		WAB Problem when screenTextID is blank.  Changed thisScreenTextID to = GetScreen.screenid.
2009-02-02 		WAB problem reloading perslist in FF
2009-08-20		NJH	Look for an applicationINI file if it exists as the application .cfm files have been done away with.
2010-10-14		WAB	change method for looking for applicationINI file, use cf_include
2011-12-09		NYB	LHID8232 changed file to append url with suppressOutput - preventing the stylesheet from being included - when openAsWord is true, then added the contents of the css instead
2012-02-15		NJH	CASE 425212: Using the unique name given to the entitydetails frame.
2015-03-02		RPW	Changed Confirm Changes on Exit to use custom Save, Discard and Cancel confimation box.
2015-04-29		WAB	Added fancybox to this page - allows confirmation box to be displayed over this page if not within the main UIExt 'frameSet'
2015-06-11		PYW/GCC	P-Tat 005 PRM Release 1 - use open as word mode but will save as PDF related file so need to avoid word download step
Possible enhancements:


 --->
<cfset application.com.request.setTopHead(topHeadcfm = "")>

<cf_includeJavascriptOnce template="/javascript/checkObject.js">
<cf_includeJavascriptOnce template = "/javascript/lib/fancybox/jquery.fancybox.pack.js">
<cf_includeCssOnce template = "/javascript/lib/fancybox/jquery.fancybox.css">

<cfif isDefined("frmEntityType") and isNumeric(frmEntityType)>
	<cfset frmEntityTypeID = frmEntityType>
<cfelseif isDefined("frmEntityType") and not isNumeric(frmEntityType)>
	<cfset frmEntityTypeID = application.entityTypeID[frmEntityType]>
</cfif>
	<cfset thisEntityTypeID = frmEntityTypeID>
	<cfset thisEntityType = application.entityType[thisEntityTypeID].tablename>
<cfset form.frmEntityType = thisEntityType>  <!--- seems to be needed by some screens   --->

<cfparam name="frmEntityScreen" type="string" default="">
<cfparam name="form.thisScreenTextID" type="string" default="#frmEntityScreen#">
<cfparam name="openAsWord" type="boolean" default="false"><!--- NYB 2011-12-09 LHID8232 added --->
<cfparam name="openAsWordMode" type="string" default="Download"><!--- PYW/GCC 2015-06-11 P-Tat 005 PRM Release 1 - use open as word mode but will save as PDF related file so need to avoid word download step --->

<!--- Have to be able to deal with frmEntityID or frmCurrentEntityID, my fault for lack of standardisation--->
<cfif isDefined("frmEntityID") and not isDefined("frmCurrentEntityID")>
	<cfset frmCurrentEntityID = frmEntityID>
</cfif>

<!--- This template is supposed to run in a frameset,
	so run this some js to check that it is in a frame called entitydetail,
	if not in frame then reload the frame
	had to put in some nasty replaces because the parentframe expects frmEntityID and frmEntityTypeID whereas this template expects frmCurrentEntityID

	2008-07-09 WAB but if openasexcel is set then don't worry about frameset
2008-12-02 WAB openasexcel javascript code seems to be wrong, so only running if openAsExcel was in the querystring, removed a ! duh
	--->

<!--- NYB 2011-12-09 LHID8232 changed js:
	NJH 2012-02-15 CASE 425212: look for the substring 'entityDetail' in the window name rather than the actual string, as now it has a unique ID appended to it.
--->
<cf_head>
<script>
	var openAsWordVar = <cfoutput>'#jsStringFormat(openAsWord)#'</cfoutput>;
	if ((openAsWordVar == 'true' || openAsWordVar == 'yes') && window.location.href.indexOf('openAsWord') == -1) {
		window.location.href = window.location.href + '&suppressOutput=true&openAsWord=true'
	} else {
		if (openAsWordVar != 'true' && openAsWordVar != 'yes' && window.name.toLowerCase().substring(0,12) != 'entitydetail' && window.location.href.toLowerCase().search(/openasexcel/) == -1) {
			 window.location.href = window.location.href.toLowerCase().replace('entityframescreens','entityframe').replace('frmcurrententityid','frmEntityID')
		}
	}

	<!--- NJH add the slideHeader2 class for secondary level header menus --->
	jQuery(document).ready(function(){
		jQuery('#slideHeader').attr('class','slideHeader2');

		/*===================
		  SB 29/10/14: Slide out header nav
	    =====================*/
		menuSlideOut();

		function menuSlideOut(){
			//jQuery's animate() won't function correctly for width: "auto", so this workaround is required
			var autoWidth = jQuery('#slideHeader ul').width() + "px";
			//minimise nav menu
			jQuery('.headerCloseHeaderMenu').click(function(){
				jQuery('#slideHeader ul').animate({
					width: "0px"
					}, 500, function() {
					// Animation complete.
				});
				jQuery('.headerCloseHeaderMenu').hide();
				jQuery('.headerOpenHeaderMenu').show();
			});
			//restore nav menu to original width
			jQuery('.headerOpenHeaderMenu').click(function(){
				jQuery('#slideHeader ul').animate({
					width: autoWidth
					}, 500, function() {
					// Animation complete.
				});
				jQuery('.headerOpenHeaderMenu').hide();
				jQuery('.headerCloseHeaderMenu').show();
			});


		}
	});
</script>
</cf_head>

<cfif frmEntityScreen is "" or frmEntityScreen is 0>
	<cfset qryThisScreenTextID = application.com.screens.getDefaultScreen(thisEntityType)>
	<cfset thisscreentextid = qryThisScreenTextID.screenID>
</cfif>

	 <!--- WAB 2005-11-15 allow screenTextID to actually be a screenid
	 use this query to resolve
	 --->
	<CFQUERY NAME="GetScreen" datasource="#application.sitedatasource#">
		select ScreenName, screenid, screentextid, viewer, entityType, parameters
		from screens
		where
		<cfif isNumeric(thisscreentextid)>
			screenid =  <cf_queryparam value="#thisscreentextid#" CFSQLTYPE="CF_SQL_INTEGER" >
		<cfelse>
			screentextid =  <cf_queryparam value="#thisscreentextid#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</cfif>
	</cfquery>
	<cfset form.thisScreenTextID = getScreen.screentextID> <!--- WAB 2008-03-12 this line is bit useless because if screentextid is blank you end up with a blank form.thisScreenTextID.  Not sure whether this value is used anywhere --->

<CFIF GetScreen.recordCount eq 0>
	<cfoutput>A screen with the id #form.thisScreenTextID# cannot be found.</cfoutput>
	<cfexit method="EXITTEMPLATE">
</CFIF>


<!--- START: NYB 2011-12-09 LHID8232 added additional if conditions and pulling in css content --->
<cfif openAsWord AND openAsWord NEQ "GetContent" and structkeyexists(url,"openAsWord") and structkeyexists(url,"suppressOutput")>
	<!--- END: NYB 2011-12-09 LHID8232 added additional if conditions and pulling in css content --->
	<!--- get suggested filename --->
	<cfif openAsWordMode eq "Download"> <!--- PYW/GCC 2015-06-11 P-Tat 005 PRM Release 1 - use open as word mode but will save as PDF related file so need to avoid word download step --->
		<cfset filename = "#Replace(GetScreen.screenname,' ','','ALL')#">
		<cfcontent type="application/msword">
		<cfheader name="content-Disposition" value="filename=#filename#.doc">
		<cfinclude template="../templates/wordHeaderInclude.cfm">
	</cfif>

	<!---  set screen to view mode--->
	<cfset frmmethod = "view">
	 <BASE href="#request.currentsite.httpprotocol##htmleditformat(sitedomain)#">
</cfif>


<!---  phrases="phr_Sys_OrganisationDetails,Phr_Sys_Contact,Phr_Sys_ListLeads,Phr_Sys_Files,Phr_Sys_ViewSites,Phr_Sys_UserDetails,Phr_Sys_CommSummary,Phr_Sys_ContactHistory,Phr_Sys_Notes"/ --->

<!--- </cf_translate> --->

<CFIF isDefined('frmTask') and frmTask eq "update">
	<!--- include the custom tag for updating screen data --->
	<cf_aScreenUpdate>
</CFIF>



<CFPARAM NAME="frmMethod" DEFAULT="edit">
<CFPARAM NAME="frmcurrentEntityID" DEFAULT="0">

<CFPARAM NAME="message" DEFAULT="">
<cfparam name="messageType" type="string" default="success">

	<!--- a method of making sure that added entities are reflected in the navigation frame--->
<cfif isDefined("frmWABEntityAdded")>

	<cfoutput>
	<script>
		// alert ('something added entityTypeID = #application.entityTypeid[thisEntityType]#, entityID = #frmCurrentEntityID#, screenID = #GetScreen.screenid#')
		if (parent.loadNewEntityNoRefresh) {
			parent.loadNewEntityNoRefresh (entityTypeID = #thisEntityTypeID#, entityID = #frmCurrentEntityID#, screenID = '#GetScreen.screenid#' ) <!--- WAB 2008-03-12 changed from thisScreenTextID to GetScreen.screenid,  since thisScreenTextID can be blank--->
		}

		<!--- WAB 2008-07-08
		perlist wasn't refreshing if a screen was being displayed immediately after an add (perPostAddScreenID)
		if it is a person who has been added, need to check for the existence of person frame (3pane view) and reload
		we do this by submitting the existing form on the perlist page
		note that this means that any existing filters are retained and the new person may not necessarily show
		--->
		<cfif thisEntityTypeID is 0>
			if (window.parent.parent.getPerFrame) {persListFrame = window.parent.parent.getPerFrame()}
			else if  (window.parent.getPerFrame) {persListFrame = window.parent.getPerFrame()}
			else {persListFrame = null }

			if (persListFrame) {
				persListFrame.document.mainForm.submit();   <!--- WAB 2009-02-02 added .document for FF--->
			}

		</cfif>
	</script>
	</cfoutput>

</cfif>


<cfif getScreen.viewer is not "showScreen" and getScreen.viewer is not "" >
		<!--- Not a 'screen' type of screen, just include the file (may need to do application .cfm and security etc
			we set a frm variable depending upon the entityType of the screen
		--->

		<!--- WAB 2008-04-19 seem to always be needing the parent entityIds, so adding in a query here
			and passing a structure called EntityDetails - modelled on the one used in the parent prame - actually could haveused same quer y or even passed the structure arounf
			could add
		 --->


		 <cfset entityDetails = application.com.relayPLO.getEntityStructure(entityTypeID =thisEntityTypeID, entityID = frmcurrentEntityID )>

		<cfset frmScreenEntityType = getScreen.entityType>    <!--- useful for pages such as the actionframe which can deal with different entity types --->
		<cfset frmEntityType = thisEntityType>
		<cfset frmEntityTypeID = thisEntityTypeID>

		<cfset frmScreenEntityID = entityDetails.ID[getScreen.entityType] >
		<cfset  "frm#getScreen.entityType#ID" = frmScreenEntityID>

		<!--- <cfif getScreen.entityType is not thisEntityType>
			<cfquery name="getEntityID" datasource="#application.sitedatasource#">
			select #getScreen.entityType#id as entityid from #thisEntityType# where #thisEntityType#id = #frmcurrentEntityID#
			</cfquery>
			<cfset  "frm#getScreen.entityType#ID" = #getEntityID.entityid#>
			<cfset frmScreenEntityID = #getEntityID.entityid#>
		<cfelse>
				<cfset  "frm#thisEntityType#ID" = frmcurrentEntityID>
				<cfset frmScreenEntityID = frmcurrentEntityID>
		</cfif> --->

		<cfset parameterStructure = application.com.regExp.convertNameValuePairStringToStructure (inputString = getScreen.parameters,delimiter = ",")>

		<cfif structKeyExists(parameterStructure,"useCFLocation")>
			<cfset theurl = "#replace(getScreen.viewer,'\','/','ALL')#">
			<cfset theurl = theurl & "?frmScreenEntityType=#frmScreenEntityType#&frmScreenEntityID=#frmScreenEntityID#&frmEntityType=#frmEntityType#&frmEntityTypeID=#frmEntityTypeID#&frm#getScreen.entityType#ID=#frmScreenEntityID#">
			<cflocation url = #theurl# addToken="false">


		<cfelse>
			<!--- try to run an application .cfm --->
<!---
			<cfset filedetails = application.com.regexp.getFileDetailsFromPath (getScreen.viewer)>
			<cfset applicationCfmname  = "#filedetails.path#/application.cfm">
			<cftry>
				<cfinclude template = "#applicationcfmName#">
				<cfcatch>

					<!--- NJH 2009-08-20 Need to include the new applicationINI files --->
					<cfset DirName = replace(replace(filedetails.path,"\","","All"),"/","","All")>
					<cfset applicationINIfile = "application#DirName#Ini.cfm">
					<cftry>
						<cfinclude template = "#filedetails.path#/#applicationINIfile#">
						<cfcatch>
							<cfrethrow>
						</cfcatch>
					</cftry>

					<cfif cfcatch.type is "MissingInclude" and cfcatch.missingfilename is applicationcfmName>
						<!--- don't worry --->
					<cfelse>
						<cfrethrow>
					</cfif>
				</cfcatch>
			</cftry>
--->

			<!--- WAB 2008-06-30 variables get created from parameters and passed to template (note hasn't been done for the cflocation above)--->
			<cfloop collection="#parameterStructure#" item="variablename">
				<cfset variables[variablename] = parameterStructure[variablename]>
			</cfloop>

			<!--- This does an include and runs any initialisation files required of that directory --->
			<cf_include template = "#getScreen.viewer#" doDirectoryInitialisation=true>

	<!--- 		<cfinclude template = "#getScreen.viewer#"> --->
		</cfif>

	<CF_ABORT>
</cfif>


<cfif getScreen.viewer is "formRenderer">
		<cfset entity = application.com.entities.load (entityType = thisEntityType, entityID = frmcurrentEntityID)>

		<cfoutput>
		<script>

		function submitPrimaryForm() {
			triggerFormSubmit (document.mainForm)			
		}

		function isPrimaryFormDirty() {
			return isDirty (document.mainForm) ;    // this function in checkObject.js
		}

		</script>

		<form  action="entityFrameScreens.cfm" method="POST" name="mainForm" enctype="multipart/form-data">

					<div id="message"><cfif message neq ""><cfoutput>#application.com.relayUI.message(message=message,type=messageType)#</cfoutput></cfif></div>

					<cf_input type="hidden" name="entityType" value="#entity.getentityType()#" encrypt="true">
					<cf_input type="hidden" name="entityID" value="#entity.getPrimaryKey()#" encrypt="true">
					<input type="hidden" name="screenID" value="#getScreen.screenid#" encrypt="true">
					<cf_input type="hidden" name="rwFormProcessor" value="com.entities.processFormScope()" encrypt="true">

					<cfset formObject = new form.formelementsobject (entity = entity , screenid  = getScreen.screenid, method= frmMethod)>
		
					<cf_relayFormDisplay autoRefreshRequiredClass="true">
						#formObject.render()#
					</cf_relayFormDisplay>
		
		</form>
		</cfoutput>		


<cfelse>






	<cfif NOT openAsWord>
		<!--- SWJ 2006-11-12 Added openWin so we can call it from a screen item --->
		<cf_includejavascriptonce template = "/javascript/openWin.js">


		<!--- submitPrimaryForm_: main function for submitting the form which can be recognised by the navigation frame
			actually we don't want this function unless there are updateable fields on the page, so
			I will actually output a wrapper function (submitPrimaryForm) after the screen has been built
		 --->

		<!--- 2015-03-02		RPW	Changed Confirm Changes on Exit to use custom Save, Discard and Cancel confimation box. --->
		<cfhtmlhead text="
		<SCRIPT type='text/javascript'>

		<!--
		function submitPrimaryForm_() {

			var form = document.mainForm;
			okToSubmit = form.onsubmit()
			if (okToSubmit) {



				var msg = '';
				form.frmTask.value = 'Update';
				msg = verifyInput();
				if (msg != '') {
					alert('\nYou must provide the following information for this entity:\n\n' + msg);
					return false
				} else {
					form.submit();
					return true;
				}

			} else {
				return false;
			}

			}

			function isPrimaryFormDirty() {

				var form = document.mainForm;
				return isDirty (form)     // this function in checkObject.js

			}





			function openWordForm() {
				var form = document.openWordForm;
				form.submit() ;
			}

			//-->

		</SCRIPT>
		">
	</cfif>



	<!--- WAB 2008-05-29 moved _EntityDetailForms.cfm into body otherwise kills horizontal scrolling--->
	<cfif NOT openAsWord>
		<cfinclude template="_EntityDetailForms.cfm">
		<!--- this include provides the logic for the Relay Form Custom Tags --->
		<cfinclude template="/templates/relayFormJavaScripts.cfm">
	</cfif>

		<!--- WAB 2007-10-17 change to cfform to use masking --->
		<cfform  action="entityFrameScreens.cfm" method="POST" name="mainForm" enctype="multipart/form-data">

			<!--- extract content into var for processing in event of opening page as Word file --->
			<cfsavecontent variable="thecontentHTML">
				<!--- get the locationID for the person record required in the next section --->
				<cfif thisEntityType eq "person" and frmcurrentEntityID is not "0">
					<CFQUERY NAME="getEntityDetails" datasource="#application.siteDataSource#">
						SELECT *
							FROM #thisEntityType#
							WHERE #thisEntityType#ID =  <cf_queryparam value="#frmcurrentEntityID#" CFSQLTYPE="CF_SQL_INTEGER" >
					</CFQUERY>
				</cfif>

				<!--- get the record details required for cf_aScreen later in the template --->
				<cfset dataStruct = structNew()>

				<cfif thisEntityType eq "person" and frmcurrentEntityID is not "0">
					<cf_getrecord entitytype="person" entityid = "#frmcurrentEntityID#" locationid = "#getEntityDetails.LocationID#" getparents getrights>
					<cfset dataStruct.person = person><cfset dataStruct.location = location><cfset dataStruct.organisation = organisation>
				<cfelseif thisEntityType eq "location" and frmcurrentEntityID is not "0">
					<cf_getrecord entitytype="location" entityid = "#frmcurrentEntityID#" getparents getrights>
					<cfset dataStruct.location = location><cfset dataStruct.organisation = organisation>
				<cfelseif thisEntityType eq "organisation" and frmcurrentEntityID is not "0">
					<cf_getrecord entitytype="organisation" entityid = "#frmcurrentEntityID#" getparents getrights>
					<cfset dataStruct.organisation = organisation>
				<cfelse>
					<cfoutput>Phr_Sys_UnsuportedRelayEntity</cfoutput>
				</cfif>

			<div id="message"><cfif message neq ""><cfoutput>#application.com.relayUI.message(message=message,type=messageType)#</cfoutput></cfif></div>
				<table width="100%">
				<cf_ascreen formName = "mainForm">
						<cf_ascreenitem screenid="#GetScreen.screenid#"	  <!--- WAB 2008-03-12 changed from thisScreenTextID to GetScreen.screenid,  since thisScreenTextID can be blank--->
								method="#frmMethod#"
								attributeCollection = #dataStruct#
						>
				</cf_ascreen>
				</table>
		
			</cfsaveContent>


	<cfif NOT openAsWord>
		<CFOUTPUT>
		<!--- <table width="100%" border="0" cellspacing="0" cellpadding="3" align="center">  <!--- WAB 2006-10-11 moved inside the form to allow FF to do dynamic HTML ---> --->
			<INPUT TYPE="hidden" NAME="frmTask" VALUE="">
				<CF_INPUT TYPE="hidden" NAME="frmEntityTypeID" VALUE="#thisEntityTypeID#">
				<CF_INPUT TYPE="hidden" NAME="frmcurrentEntityID" VALUE="#frmcurrentEntityID#">
				<CF_INPUT TYPE="hidden" NAME="frmEntityScreen" VALUE="#GetScreen.screenid#"> <!--- WAB 2008-03-12 changed from thisScreenTextID to GetScreen.screenid,  since thisScreenTextID can be blank--->

				<!--- WAB 2008-10-01 these lines had not been brought from file entityScreens.cfm when entityFrameScreens.cfm was created--->
				<!--- WAB added these ids - often needed if a process is run after an update --->
				<cfif isdefined("person")>
				<CF_INPUT TYPE="hidden" NAME="frmPersonid" VALUE="#person.personid#">
				</cfif>
				<cfif isdefined("location")>
				<CF_INPUT TYPE="hidden" NAME="frmLocationid" VALUE="#location.locationid#">
				</cfif>
				<cfif isdefined("organisation")>
				<CF_INPUT TYPE="hidden" NAME="frmorganisationid" VALUE="#organisation.organisationid#">
				</cfif>



				<!--- <td width="20" class="label">&nbsp;</td>
				<td class="label"> --->
			</CFOUTPUT>
		</cfif>


	<cfif openAsWord>
		<!--- replace any hidden, radio and check fields --->
		<cfset theContentHTML = application.com.regExp.replaceCheckField(theContentHTML,"ALL")>
		<cfset theContentHTML = application.com.regExp.removeHiddenField(theContentHTML,"ALL")>
		<!--- START: NYB 2011-12-09 LHID8232 added: --->
		<cfset theContentHTML = application.com.regExp.replaceHTMLTagsInString(inputString=theContentHTML,htmlTag="script",replaceExp="",hasEndTag="yes")>
		<cfset theContentHTML = REreplaceNoCase(theContentHTML,"<img(.*?)src[ ]*?=[ ]*?[""'][^h]([^""']*?)[""']([^>]*?)>","<img\1src=""#request.currentsite.PROTOCOLANDDOMAIN#/\2""\3>", "ALL")>
		<!--- END: NYB 2011-12-09 LHID8232 --->
	</cfif>

	<cfoutput>#theContentHTML# </cfoutput>

		<cfif NOT openAsWord>

				<!--- </td>
				<td width="20" class="label">&nbsp;</td>
			</tr> --->
		</cfif>
		<!--- WAB 2007-10-17 change to cfform to use masking --->
		</cfform>


		<!--- </table> --->

		<cfif isDefined("getScreen.postinclude") and getScreen.postinclude is not ""> <!--- getScreen is returned by cf_ascreenitem--->
			<cfinclude template = "/#getScreen.postinclude#">
		</cfif>

	<cfif NOT openAsWord>  <!--- WAB 2011-08-03 moved this down a few lines so that openAsWord included the postInclude --->
		<cfoutput>

		<form action="" method="post" name="openWordForm" target="_blank">
			<INPUT TYPE="hidden" NAME="openAsWord" VALUE="true">
			<CF_INPUT TYPE="hidden" NAME="frmEntityType" VALUE="#thisEntityType#">
			<CF_INPUT TYPE="hidden" NAME="frmcurrentEntityID" VALUE="#frmcurrentEntityID#">
			<CF_INPUT TYPE="hidden" name="frmEntityScreen" value="#GetScreen.screenid#"> <!--- LID 2577 NJH 2009-09-22 --->
			<INPUT TYPE="hidden" NAME="suppressOutput" VALUE="true"><!--- NYB 2011-12-09 LHID8232 added --->
			<CF_INPUT TYPE="hidden" NAME="openAsWordMode" VALUE="#openAsWordMode#"> <!--- PYW/GCC 2015-06-11 P-Tat 005 PRM Release 1 - use open as word mode but will save as PDF related file so need to avoid word download step --->
		</form>
		</cfoutput>
	</cfif>


	<!---  updateableFields is returned by <cf_ascreen>--->
	<cfif updateableFields>
		<cfhtmlhead text = "
			<script>
				function submitPrimaryForm () {
					return submitPrimaryForm_()
				}
	
			</script>
		">
	
	</cfif>

</cfif>
