<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		_entityEditFunction.cfm
Author:			SWJ
Date created:	10 August 2002

	Objective - this provides a standard way to call a main entityEdit Window.
		
	Syntax	  -	To call the functions below 
	
	Parameters - frmNextTarget = the frame of the template that is including this code
				


Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Enhancement still to do:



--->

<cfif not isDefined("frmNextTarget")>
 <CFABORT SHOWERROR="DEBUG: _editMainEntities.cfm must have frmNextTarget set before it is included.">
</cfif>

<!--- 
WAB 2005-02-08
This is a complete and utter hack!!
We get an error by :	
	1) merging two people  (top right frame)
	2) clicking on the "return to people list"  (top right frame)
	3) clicking to edit a person   (come up in bottom frame)
	4) saving the person record		(bottom frame)
	
	basically at step 2 perslist is included from personchoice.cfm so script_name is personchoice.cfm 
	so frmNext is set to personchoice.cfm 	- which don't work
	so I am intercepting that and setting it to perslist.

	infact it's all rubbish 'cause later on (at the end of persondetailtask) if perslist.cfm is detected then as frmnext then it replaced with persondetail.cfm!!!!

 --->
<cfset thisScriptName = SCRIPT_NAME>
<cfif thisScriptName contains "personchoice">
	<cfset thisScriptName = "/data/perslist.cfm">
</cfif>

<cfset dataframeWindowSettings = "width=800,height=600,toolbar=0,location=0,directories=0,status=1,menuBar=0,scrollBars=1,resizable=1">


<cfset thisURL = "">
<!--- Create the URL String from fieldNames for the javaScript below --->
<cfoutput>
	<CFIF isdefined("FieldNames") and findNoCase("perList.cfm",thisScriptName) eq 0>
		<CFLOOP INDEX="i" LIST="#FieldNames#">
			<CFIF evaluate(i) neq "" and listFindNocase("frmNext,frmBack,FRMMOVEFREEZE,frmLocsInReport",#i#) eq 0>
				<cfset thisURL = thisURL & "&#i#=#evaluate(i)#">
			</CFIF>
		</CFLOOP>
	<cfelse>
		<cfset thisURL = "">
	</CFIF>
	
	
	<SCRIPT type="text/javascript">
		function editOrg(orgID) {
			viewOrganisation (orgID)
			
		    return;
	     	}
	
		function editLoc(orgID,locID) {
			viewLocation (locID)
	        return;
	     	}
		
		function editPerson(orgID,personID) {
			viewPerson (personID)
			return ;

				}


			
			

	</SCRIPT>
	
	<FORM ACTION="/data/dataFrame.cfm" METHOD="post" NAME="editMainEntityForm" target="#frmNextTarget#">
		<INPUT TYPE="hidden" NAME="frmsrchOrgID" VALUE="0">
		<INPUT TYPE="hidden" NAME="frmSrchPersonid" VALUE="">
		<INPUT TYPE="hidden" NAME="frmOrganisationID" VALUE="0">
		<INPUT TYPE="hidden" NAME="frmLocationID" VALUE="0">
		<INPUT TYPE="hidden" NAME="frmPersonID" VALUE="0">
		<INPUT TYPE="HIDDEN" NAME="frmNext" VALUE="">
		<CF_INPUT TYPE="HIDDEN" NAME="frmNextTarget" VALUE="#frmNextTarget#">
		<INPUT TYPE="HIDDEN" NAME="frmNext" VALUE="">

		<!--- WAB 2007/11/21  --->
		<INPUT TYPE="hidden" NAME="frmEntityTypeID" VALUE="">		
		<INPUT TYPE="hidden" NAME="frmEntityID" VALUE="">		
		
		
		
	</FORM>
</cfoutput>

