<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
	2008-09-29	PPB	CF functions related to the POL dedupe process
2009/06/02  WAB  	LID 2308  conflicting flags query not taking entityTypeID into account		
			restructured some of the queries in process
amendment History

---->
	
	<!--- as the query for IntegerFlagData and TextFlagData are the same, do it in a function --->
	<CFFUNCTION name="getConflictingFlags" returntype="query">
		<cfargument name="dataType" type="String" required="true">
		<cfargument name="EntityTypeId" type="numeric" required="true">
		<cfargument name="CurrentEntityId" type="String" required="true">
		<cfargument name="OtherEntityId" type="String" required="true">
		
		<CFSWITCH expression="#dataType#">
			<CFCASE value="Boolean">
				<!--- get all the flags for the currentEntity where the otherEntity has an entry for the same flagGroup but does not have the same flag;
				      exclude FlagType=2 (Checkboxes) which are always merged --->
				<CFQUERY NAME="qryConflictingFlags" DATASOURCE=#application.SiteDataSource#>
				SELECT     fd1.EntityID, fd1.FlagID, f1.Name AS FlagName, fg.FlagGroupID, fg.FlagGroupTextID, 
				                      fg.Name AS FlagGroupName
						from	
							flaggroup fg
								inner join
							(flag f1  
								inner join 
							booleanflagdata fd1 on f1.flagid = fd1.flagid )
								on fg.flaggroupid = f1.flaggroupid
								inner join
							(flag f2
								inner join 
							booleanflagdata fd2 on f2.flagid = fd2.flagid )
								on fg.flaggroupid = f2.flaggroupid
							
						where fg.flagTypeID = 3 -- radio
							and fg.entityTypeid =  <cf_queryparam value="#arguments.entityTypeID#" CFSQLTYPE="CF_SQL_INTEGER" > 	
							and	fd1.entityid =  <cf_queryparam value="#arguments.CurrentEntityId#" CFSQLTYPE="CF_SQL_INTEGER" > 
							and 	fd2.entityid =  <cf_queryparam value="#arguments.otherEntityId#" CFSQLTYPE="CF_SQL_INTEGER" > 
							and 	f1.flagid <> f2.flagid

				ORDER BY fg.FlagGroupId

<!---

				FROM         dbo.BooleanFlagData INNER JOIN
				                      dbo.flag ON dbo.BooleanFlagData.FlagID = dbo.flag.FlagID INNER JOIN
				                      dbo.FlagGroup ON dbo.flag.FlagGroupID = dbo.FlagGroup.FlagGroupID
				WHERE     (dbo.BooleanFlagData.EntityID = #arguments.CurrentEntityId#)	
				AND dbo.FlagGroup.FlagTypeId <> 2			
				AND (dbo.flag.FlagGroupID IN
				  (SELECT     flag.FlagGroupID
				    FROM          dbo.BooleanFlagData AS BooleanFlagData INNER JOIN
				                           dbo.flag AS flag ON BooleanFlagData.FlagID = flag.FlagID
				    WHERE      (BooleanFlagData.EntityID = #arguments.otherEntityId#)))
				AND (dbo.BooleanFlagData.FlagID NOT IN
				  (SELECT     FlagID
				    FROM          dbo.BooleanFlagData 
				    WHERE      (BooleanFlagData.EntityID = #arguments.otherEntityId#)))			
				ORDER BY dbo.flag.FlagGroupId

--->

				</CFQUERY>
			</CFCASE>

			<CFCASE value="Integer,Text,Decimal">
				<!--- get all the flags for the currentEntity where the otherEntity has an entry for the same flag but the data is different --->
				<CFQUERY NAME="qryConflictingFlags" DATASOURCE=#application.SiteDataSource#>
				SELECT     fd.EntityID, fd.FlagID, f.flag AS FlagName, fd.Data AS FlagData, f.FlagGroupID, f.FlagGroupTextID, 
				                      f.FlagGroup AS FlagGroupName, f.LinksToEntityTypeId, flag.useValidValues, f.FlagTextID
				FROM         
						vflagDef f
								INNER JOIN
						flag on flag.flagid = f.flagid  <!--- needed to join to flag becayse useValidValues not in the view! --->
								INNER JOIN
						dbo.#dataType#FlagData fd ON fd.FlagID = f.FlagID 
							INNER JOIN
                        dbo.#dataType#FlagData AS OtherFD ON fd.FlagID = OtherFD.FlagID AND fd.Data <> OtherFD.Data 
				WHERE     
						f.entityTypeID =  <cf_queryparam value="#arguments.entityTypeID#" CFSQLTYPE="CF_SQL_INTEGER" > 	
					AND f.DataType =  <cf_queryparam value="#dataType#" CFSQLTYPE="CF_SQL_VARCHAR" > 	
					AND	(FD.EntityID =  <cf_queryparam value="#arguments.CurrentEntityId#" CFSQLTYPE="CF_SQL_INTEGER" > ) 
					AND  (OtherFD.EntityID =  <cf_queryparam value="#arguments.OtherEntityId#" CFSQLTYPE="CF_SQL_INTEGER" > )		 		
				</CFQUERY>
			</CFCASE>
			
			<CFCASE value="Event">
				<CFQUERY NAME="qryConflictingFlags" DATASOURCE=#application.SiteDataSource#>
					SELECT     fd.EntityID, fd.FlagID, fd.RegStatus AS FlagData, f.FlagGroupID, 
					                      dbo.EventDetail.Title AS FlagGroupName
					FROM         
								vflagDef f
									INNER JOIN
				               dbo.EventDetail ON f.FlagID = EventDetail.FlagID 
									INNER JOIN
								dbo.EventFlagData AS fd ON fd.FlagID = f.FlagID 
									INNER JOIN
		                        dbo.EventFlagData AS OtherFD ON fd.FlagID = otherFD.FlagID AND fd.RegStatus <> OtherFD.RegStatus 
									                   	     
					WHERE     
						f.entityTypeID =  <cf_queryparam value="#arguments.entityTypeID#" CFSQLTYPE="CF_SQL_INTEGER" > 
						and f.dataType = 'Event'
						and	(fd.EntityID =  <cf_queryparam value="#arguments.CurrentEntityId#" CFSQLTYPE="CF_SQL_INTEGER" > ) AND  (otherFD.EntityID =  <cf_queryparam value="#arguments.OtherEntityId#" CFSQLTYPE="CF_SQL_INTEGER" > )		 		
				</CFQUERY>
			</CFCASE>
			
		</CFSWITCH>
	
		<CFRETURN qryConflictingFlags> 
	</CFFUNCTION>

