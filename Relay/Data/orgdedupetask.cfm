<!--- �Relayware. All Rights Reserved 2014 --->
<!---

orgDedupe.cfm

Author:

Date:

Purpose:
	Deduplicate Organisations

Mods:

Parameters:
		#frmorgids#  list of all the organisationids
		#frmorgtokeep#   - optional
		#frmnewOrgName#  - optional

Ver
1.00     ?
1.01	2001-03-02	WAB 	Modified to use new dedupe organisation stored procedure
1.02	2001-07-25	SWJ		Modified template to show a screen after the process finishes
1.03	2001-08-17	SWJ		Modified template to show locList after the process finishes
1.04	2008-09-23	PPB		Now user has form to choose which field values and flags to keep when merging orgs
--->

<cfsetting requesttimeout="600"> <!--- PPB set to 10 minutes as dedupe can be a length process --->

<CFTRANSACTION>
	<CFQUERY Name="UpdateOrgName"  datasource="#application.siteDataSource#">
	Update Organisation
	Set OrganisationName =  <cf_queryparam value="#insOrganisationName#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
		OrganisationTypeID =  <cf_queryparam value="#insOrganisationTypeID#" CFSQLTYPE="CF_SQL_INTEGER" > ,
		AKA =  <cf_queryparam value="#insAKA#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
		CountryId =  <cf_queryparam value="#insCountryId#" CFSQLTYPE="CF_SQL_INTEGER" > ,
		VATNumber =  <cf_queryparam value="#insVATNumber#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
		DefaultDomainName =  <cf_queryparam value="#insDefaultDomainName#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
		OrgUrl =  <cf_queryparam value="#insOrgUrl#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
		OrgEmail =  <cf_queryparam value="#insOrgEmail#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
		Notes =  <cf_queryparam value="#insNotes#" CFSQLTYPE="CF_SQL_VARCHAR" >
	Where organisationid =  <cf_queryparam value="#OrganisationIDsave#" CFSQLTYPE="CF_SQL_INTEGER" >
	</CFQUERY>

	<!--- UPDATE THE FLAG TABLES --->
	<CFSET entityIdSave = organisationIDSave>
	<CFSET entityIdDel = organisationIDDel>
	<CFINCLUDE template="dedupe_SetFlags.cfm">

	<cfscript>
	companyAccount = application.com.dedupe.mergeOrganisation(organisationIDSave,organisationIDDel);
	</cfscript>
</CFTRANSACTION>


<cfoutput>
	<cfif isDefined("frmRadioName")><script>opener.disableItem(#frmRadioName#,#frmRadioAmount#);</script></cfif>
</cfoutput>

<!--- remove the from the list --->
<CFSET a = ListFind(frmOrgIDs,OrganisationIDdel)>
<CFIF a IS NOT 0>
	<CFSET frmOrgIDs = ListDeleteAt(frmOrgIDs,a)>
	<CFSET OrganisationsDel = ListAppend(OrganisationsDel, OrganisationIDdel)>
</CFIF>
<!--- if the last ID, then go to summary --->
<CFIF (ListLen(frmOrgIDs) IS 1 AND ListFind(frmOrgIDs,0) IS 0) OR
	(ListLen(frmOrgIDs) IS 2 AND ListFind(frmOrgIDs,0) IS NOT 0)>	<!--- should be the OrganisationIDsave --->
	<CFINCLUDE TEMPLATE="orgdedupesum.cfm">
	<CF_ABORT>
</CFIF>

<CFINCLUDE TEMPLATE="orgdedupe.cfm">
<CF_ABORT>