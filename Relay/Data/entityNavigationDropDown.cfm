<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			entityNavigationDropDown.cfm	
Author:				AH
Date started:			/06/04
	
Description: An include of drop down list containing screens and other entity templates
			 requires: 
			 - thisEntityType variable to be set in the variable scope above the include itself
			 - 

Amendment History:

Date			Initials 	What was changed
2005-04-14		RND			Check for definition of frmOrganisationID param, if not present use EntityID instead, see line 147.
2005-04-18      RJP			Added Organisation entity type variable, see line 


Possible enhancements:

frmOrgID, frmPrsID, frmLocID should be passed simply as currentEntityID and read like that
by queries. That way we would not have to worry what variable should be passed to what screen! (08-Jun-2004 AH)


 --->
<cfparam name="qryOrganisationTypeID" default="0">
<script language="JavaScript">
 function jumpTo(urlToHit,selIndex) {
 		
		
		if(!selIndex){
			
			jumpToURL = urlToHit;
		}
		else{
			jumpToURL = urlToHit + "&selIndex=" + selIndex;
			
		}
		
		location.href=jumpToURL;
	}
</script>



<cfscript>
	if(structKeyExists(url, "selIndex")){
		form.selIndex = url.selIndex;
	}
</cfscript>
 <!---   phrases="Sys_OrganisationDetails,Sys_Contact,Sys_ListLeads,Sys_Files,Sys_ViewSites,sys_notes" --->
<cf_translate>

 
 <!--- get a list of active screens  --->
<cfscript>

	//MUST SET LOCAL VARIABLE thisEntityType above this document
	if(not structKeyExists(variables, "thisEntityType")){
		writeOutput("data/entityNavigationDropDown.cfm include requires thisEntityType variable to be set in the variable scope above the include itself!");
		application.com.globalFunctions.abortIt();
	}

	//What file are we actually browsing?
	currentDocument = listFirst(listLast(cgi.script_Name, "/"),"?");

	//query bringing back list of screenTextIDs which are used to 'flip' through profile screens
	getScreenList = application.com.screens.getScreenList(thisEntityType,qryOrganisationTypeID);
	
	//this list will hold the displays of all the links
	//will later be sorted alphabetically and looped through
	//in order to construct the select box' options
	screenNameList = "";
		
	//if we are inside 'organisation zone'
	if(not compareNocase(thisEntityType, "organisation")){
	
		if(isDefined("frmOrgID") and frmOrgID is not 0){          // WAB 2005-11-14 added  AND frmOrgID is not 0.  frmOrgId is param'ed to 0 so was always defined - hope ther isn't a case when it is supposed to be zero
			org_entityID = frmOrgID;
		}
		
		else{
			org_entityID = frmCurrentEntityID;
		}
	
		//set global vars
		entityDetailFiles = "orgdetail.cfm,orgdetailtask.cfm";
		currentEntityIDvariable = "frmOrgID";
		secondValueVariable = 2;
		thirdValueVariable = 'orgDetail';
		
		//loop through the query and set links and functions
		for(i = 1; i lte getScreenList.recordCount; i = i+1){
			if(listFindNoCase(entityDetailFiles, currentDocument)){
				linkStruct[getScreenList.screenName[i]] = 'viewentityFlags(' & evaluate(currentEntityIDvariable) & "," & secondValueVariable & ",'" & thirdValueVariable & "','" & getScreenList.screenTextID[i] & "', this.selectedIndex)";
			}
			else{
				linkStruct[getScreenList.screenName[i]] = 'verifyForm(' & "'','" & getScreenList.screenTextID[i] & "')";

			}
			screenNameList = listAppend(screenNameList, getScreenList.screenName[i]);
		}
		
		//ADDITIONAL LINKS
		
		//ORGANISATION DETAILS LINK
		linkStruct['Phr_Sys_OrganisationDetails'] = "javascript:jumpTo('/data/orgDetail.cfm?frmOrgID=" & org_entityID &"', this.selectedIndex)";
		screenNameList = listAppend(screenNameList, 'Phr_Sys_OrganisationDetails');
		
		//VIEW CONTACT HISTORY
		linkStruct['Phr_Sys_ContactHistory'] = "javascript:viewContactHistory(" & org_entityID & ",'organisation', this.selectedIndex)";
		screenNameList = listAppend(screenNameList, 'Phr_Sys_ContactHistory');
		
		//LIST FILES LINK
		linkStruct['Phr_Sys_Files'] = "javascript:jumpTo('/data/entityFiles.cfm?frmEntityType=organisation&frmEntityID=" & org_entityID &"', this.selectedIndex)";
		screenNameList = listAppend(screenNameList, 'Phr_Sys_Files');
		
		//LIST LEADS LINK
		//if ((listFindNocase(application. liverelayapps,39) or listfindnocase(application. liverelayapps,49)) and structKeyExists(session,"currentOrgLeadCount") and session.currentOrgLeadCount){
		if ((application.com.relayMenu.isModuleActive(moduleID="LeadManager") or application.com.relayMenu.isModuleActive(moduleId="LeadManagerNoProducts")) and structKeyExists(session,"currentOrgLeadCount") and session.currentOrgLeadCount){
			linkStruct['Phr_Sys_ListLeads'] = "javascript:jumpTo('/leadManager/leadAndOppList.cfm?entityID=" & org_entityID &"', this.selectedIndex)";
			screenNameList = listAppend(screenNameList, 'Phr_Sys_ListLeads');
		}
		
		//VIEW SITES
		if(structKeyExists(session,"currentOrgLocCount") and session.currentOrgLocCount){
			linkStruct['Phr_Sys_ViewSites'] = "javaScript:viewSites(" & org_entityID & ", this.selectedIndex)";
			screenNameList = listAppend(screenNameList, 'Phr_Sys_ViewSites');
		}
		
		//order the list of screen names, so that when we loop through the
		//structure, we loop in alphabetically sorted way
		screenNameList = listSort(screenNameList, "textnocase");
		
	}
	
	
	else if(not compareNocase(thisEntityType, "location")){
	
		if(isDefined("frmLocationID")){ 
			loc_entityID = frmLocationID;
		}
		
		else{
			loc_entityID = frmCurrentEntityID;
		}
		entityDetailFiles = "locdetail.cfm,locdetailtask.cfm";
		currentEntityIDvariable = "FRMLOCATIONID";
		secondValueVariable = 1;
		thirdValueVariable = 'locDetail';
	
		//loop through the query and set links and functions
		for(i = 1; i lte getScreenList.recordCount; i = i+1){
			if(listFindNoCase(entityDetailFiles, currentDocument)){
				linkStruct[getScreenList.screenName[i]] = 'viewentityFlags(' & evaluate(currentEntityIDvariable) & "," & secondValueVariable & ",'" & thirdValueVariable & "','" & getScreenList.screenTextID[i] & "', this.selectedIndex)";
			}
			else{
				linkStruct[getScreenList.screenName[i]] = 'verifyForm(' & "'','" & getScreenList.screenTextID[i] & "')";
			}
			screenNameList = listAppend(screenNameList, getScreenList.screenName[i]);
		}
		
		//ADDITIONAL LINKS
		
		//LOCATION DETAILS LINK
		linkStruct['Location Details'] = "javascript:jumpTo('/data/locdetail.cfm?frmLocationID=" & loc_entityID &"', this.selectedIndex)";
		screenNameList = listAppend(screenNameList, 'Location Details');
		

		//VIEW SITES
		// WAB 2005-11-14 this wasn't working because it was using the locationID instead of the organisationid
		// I think that frmOrganisationID will be defined - but there is a check to be sure
		if(isDefined("frmOrganisationID")){
			linkStruct['Phr_Sys_ViewSites'] = "javaScript:viewSites(" & frmOrganisationID & ", this.selectedIndex)";
			screenNameList = listAppend(screenNameList, 'Phr_Sys_ViewSites');
		}
		
		//order the list of screen names, so that when we loop through the
		//structure, we loop in alphabetically sorted way
		screenNameList = listSort(screenNameList, "textnocase");
		
	}
	
	
	else if(not compareNocase(thisEntityType, "person")){
	
		if(isDefined("frmPersonID")){ 
			pers_entityID = frmPersonID;
		}
	
		else{
			pers_entityID = frmCurrentEntityID;
		}
	
		entityDetailFiles = "persondetail.cfm,persondetailtask.cfm";
		currentEntityIDvariable = "frmPersonID";
		secondValueVariable = 0;
		thirdValueVariable = 'personDetail';
		
		//loop through the query and set links and functions
		for(i = 1; i lte getScreenList.recordCount; i = i+1){
			if(listFindNoCase(entityDetailFiles, currentDocument)){
				linkStruct[getScreenList.screenName[i]] = 'viewentityFlags(' & evaluate(currentEntityIDvariable) & "," & secondValueVariable & ",'" & thirdValueVariable & "','" & getScreenList.screenTextID[i] & "', this.selectedIndex)";
			}
			else{
				linkStruct[getScreenList.screenName[i]] = 'verifyForm(' & "'','" & getScreenList.screenTextID[i] & "')";
			}
			screenNameList = listAppend(screenNameList, getScreenList.screenName[i]);
		}
		
		
		//ADDITIONAL LINKS
		
		//PERSON DETAILS LINK
		linkStruct['Phr_Sys_PersonDetails'] = "javascript:jumpTo('/data/persondetail.cfm?frmPersonID=" & pers_entityID &"', this.selectedIndex)";
		screenNameList = listAppend(screenNameList, 'Phr_Sys_PersonDetails');
		
		if(pers_entityID and listFindNoCase(entityDetailFiles, currentDocument)){
		
			//COMMUNICATION SUMMARY LINK
			linkStruct['Phr_Sys_CommSummary'] = "javascript:jumpTo('/data/personCommSummary.cfm?pid=" & pers_entityID &"', this.selectedIndex)";
			screenNameList = listAppend(screenNameList, 'Phr_Sys_CommSummary');
		
			//CONTACT HISTORY LINK
			linkStruct['Phr_Sys_ContactHistory'] = "javaScript:viewContactHistory(" & pers_entityID & ",'person', this.selectedIndex)";
			screenNameList = listAppend(screenNameList, 'Phr_Sys_ContactHistory');
		
		
			//CONTACT HISTORY LINK
			linkStruct['Phr_Sys_Notes' &'(' & qryNoteCount & ' ' & 'Phr_Sys_Notes' & ')'] = "javascript:jumpTo('/data/persondetail.cfm?frmPersonID=" & pers_entityID & "##Notes')";
			screenNameList = listAppend(screenNameList, 'Phr_Sys_Notes' &'(' & qryNoteCount & ' ' & 'Phr_Sys_Notes' & ')');
		
		}
		
		//order the list of screen names, so that when we loop through the
		//structure, we loop in alphabetically sorted way
		screenNameList = listSort(screenNameList, "textnocase");
	
		
	}
	
	counter = 1;
</cfscript>


<cfoutput>


<!---IF IT IS NOT ORGANISATION DETAIL SCREEN (MEANS WE ARE IN THE 'PROFILE ZONE')--->
<!--- <cfif not listFindNoCase(entityDetailFiles, currentDocument )>
 --->
	<select name="frmEntityScreen" onChange="if(this.selectedIndex!=0)eval(this[this.selectedIndex].value)">

<!--- <cfelse>
	
	<select name="frmEntityScreen" onChange="if(this.selectedIndex!=0 && this.selectedIndex!=1)viewentityFlags(#form[currentEntityIDvariable]#,'#secondValueVariable#','#secondValueVariable#',this[this.selectedIndex].value)">
	
</cfif> --->
	
	<option value="0">phr_sys_EntityNavDropDownGoTo</option>
	
	<cfloop list="#screenNameList#" index="listItem">
		<option value="#linkStruct[listItem]#" <cfif structKeyExists(form,"selIndex") and form.selIndex eq variables.counter>selected</cfif> >#htmleditformat(listItem)#</option>
		<cfscript>
			counter = counter+1;
		</cfscript>
	</cfloop>
	</cfoutput>
</select>

</cf_translate>


