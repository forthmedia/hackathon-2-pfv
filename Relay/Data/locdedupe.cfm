<!--- �Relayware. All Rights Reserved 2014 --->
<!---
 user rights to access:
		SecurityTask:dataTask
		Permission: Update

amendment History
1999-04-13	WAB Added fields required for search by first and last name
1.01	2001-08-16	SWJ 	Modified to use new look n feel
2		2007-08-03		SWJ		Modified the screen so that the user can now choose either record to keep
2.01	2008-09-24	PPB		Allow user to choose which Flags to keep during dedupe
		2009-04-22	NYB		Sophos - added concept of request.AdditionalLocDedupeFields to add additional fields to dedupe process
		2009/04/30  WAB/SSS CR-LEX588   DedupeProtection .  See comments in orgdedupe.cfm
		2009/06/02  WAB  	LID 2308  conflicting flags query not taking entityTypeID into account

--->

<CFIF NOT checkPermission.UpdateOkay GT 0>
	<!--- if frmLocationID is 0 then task is to update
	      but the user doesn't have update permissions --->

	<CFSET message="You are not authorised to use this function">
		<CFINCLUDE TEMPLATE="datamenu.cfm">
	<CF_ABORT>
</CFIF>

<cfif listlen(#frmDupLocs#) lt 2>
	<P>You must select at least two locations to start the dedupe process.</p>
	<a href="JavaScript:window.close();">Close window</a>  <!--- SSS 2009/05/05--->
	<CF_ABORT>
</cfif>

<CFPARAM NAME="LocationsDel" Default="">
<CFPARAM NAME="refreshLocList" DEFAULT="no"><!--- used in dedupe task and set by locList.cfm --->
<!--- NYB 2009-04-22 Sophos - added this param --->
<!--- NJH 2010/10/22 - moved to settings <CFPARAM NAME="request.AdditionalLocDedupeFields" Default=""> --->

<!--- the application .cfm checks for read dataTask privleges
--->
<!--- <cfinclude template="/templates/qrygetcountries.cfm"> --->
<!--- qrygetcountries creates a variable CountryList --->


<!--- limits locations to those in allowable countries --->
<CFQUERY NAME="getLocs" datasource="#application.siteDataSource#">
SELECT l.*, l.SiteName AS qrySiteName, c.countryDescription FROM Location AS l
	inner join country c on l.countryID = c.countryID
WHERE l.LocationID  IN ( <cf_queryparam value="#frmDupLocs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
<!--- 2012-07-25 PPB P-SMA001 commented out
AND l.CountryID  IN ( <cf_queryparam value="#Variables.CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
 --->
#application.com.rights.getRightsFilterWhereClause(entityType="location",alias="l").whereClause#	<!--- 2012-07-25 PPB P-SMA001 --->
ORDER BY l.lastUpdated desc
</CFQUERY>

<CFQUERY NAME="getCountries" DATASOURCE="#application.SiteDataSource#">
	SELECT DISTINCT c.CountryID AS lCountryID, c.CountryDescription
	  FROM Country AS c
	 WHERE c.CountryID  IN ( <cf_queryparam value="#request.relayCurrentUser.countryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	ORDER BY c.CountryDescription ASC
</CFQUERY>

<!--- WAB 2009/04/30  dedupeProtection, These two functions will tell us if any or either of the elements are merge protected
--->
<cfset mergeCheck = arrayNew(1)>
<cfset mergeCheck[1] = application.com.dedupe.checkForMergeProtection(entityType=1,entityID =getLocs.locationID[1] )>
<cfset mergeCheck[2] = application.com.dedupe.checkForMergeProtection(entityType=1,entityID =getLocs.locationID[2] )>


<CFINCLUDE template="dedupe_functions.cfm">

<!--- NJH 21016/11/25 JIRA PROD-2190 - check that the locations are in the same country before merging --->
<cfif getLocs.countryID[1] neq getLocs.countryID[2]>
	<cfoutput>
	 	<div class="warningblock">
		<cfoutput>
		#getLocs.SiteName[1]# is located in #getLocs.countryDescription[1]#<BR><br>
		#getLocs.SiteName[2]# is located in #getLocs.countryDescription[2]#<BR><br>
		These Locations can not be merged</cfoutput>
	 	</div>
	 	<input type="button" value="Close Window" onClick="JavaScript:window.close();" class="btn btn-primary">
	 <CF_ABORT>
	 </cfoutput>
<!--- SSS 2009/05/01 this will stop the process running if all items selected are merge protected --->
<cfelseif mergeCheck[1].isMergeProtected and mergeCheck[2].isMergeProtected>
	 <cfoutput>
	 	<div class="warningblock">
		<cfoutput>
		#getLocs.SiteName[1]# has profile '#mergeCheck[1].detail.flagname[1]#' set<BR><br>
		#getLocs.SiteName[2]# has profile  '#mergeCheck[2].detail.flagname[1]#' set<BR><br>
		These Locations can not be merged</cfoutput>
	 	</div>
	 	<input type="button" value="Close Window" onClick="JavaScript:window.close();" class="btn btn-primary">
	 <CF_ABORT>
	 </cfoutput>
<cfelseif not mergeCheck[1].isMergeProtected and not mergeCheck[2].isMergeProtected>
	 <!--- 2009/05/01 SSS if neither item is merge protected do not disable the buttons--->
	<cfset noMergeProtectionSet = true>
</cfif>


<cf_title><CFOUTPUT>#request.CurrentSite.Title#</CFOUTPUT> phr_sys_merge</cf_title>

<cf_includejavascriptonce template = "/javascript/viewEntity.js">
<CFIF getLocs.RecordCount GTE 2>
	<cf_includejavascriptonce template = "/data/js/dedupe.js">
</CFIF>


<CENTER>

<cfset application.com.request.setTopHead(pageTitle="phr_sys_Merge phr_sys_Location phr_sys_records")>
<!--- <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
	<TR>
		<TD ALIGN="LEFT" VALIGN="top" CLASS="Submenu">
			phr_sys_Merge phr_sys_Location phr_sys_records
		</TD>
	</TR>
</table> --->

<FORM ACTION="loclist.cfm" METHOD="POST" NAME="BackForm">
	<!--- here are all the search parameters --->
	<CFINCLUDE template="searchparamdefaults.cfm">

	<CFPARAM NAME="frmStart" DEFAULT="1">
	<CFPARAM NAME="frmStartp" DEFAULT="1">

	<CFINCLUDE template="searchparamform.cfm">
	<CFOUTPUT>
		<CF_INPUT TYPE="HIDDEN" NAME="frmStart" VALUE="#frmStart#">
		<CF_INPUT TYPE="HIDDEN" NAME="frmStartp" VALUE="#frmStartp#">
	</CFOUTPUT>
</FORM>

<CFIF getLocs.RecordCount GTE 2>
	<CFSET strow = getLocs.RecordCount - 1>
	<CFSET enrow = getLocs.RecordCount>
	<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
		<TR><CFSET count = 0>
			<CFLOOP QUERY="getLocs" STARTROW=#strow# ENDROW=#enrow#>

			<CFSET count = count + 1>
			<!--- <cfdump var="#count#"> --->
			<!--- <CFIF count IS 2>
				<CFQUERY NAME="getDataSources" DATASOURCE="#application.SiteDataSource#">
					SELECT CONVERT(varchar(10),ld.DataSourceID) + '#application.delim1#' + ld.RemoteDataSourceID AS DataSource
					  FROM Location AS l, DataSource AS d, LocationDataSource AS ld
					 WHERE d.DataSourceID = ld.DataSourceID
					   AND ld.LocationID = l.LocationID
					   AND l.LocationID = #LocationID#<!--- 442074 --->
				  ORDER BY d.Description
				</CFQUERY>
				<CFSET allsources = "">
				<CFLOOP QUERY="getDataSources">
					<CFSET allsources = ListAppend(allsources, DataSource, Chr(135))>
				</CFLOOP>
			<CFELSE>
				<CFSET frmlocationID1 = LocationID>
			</CFIF> --->
			<CFOUTPUT>
			<TD VALIGN=TOP  <cfif count is 1>align="right"</cfif>> <!--- WAB/SSS 2009/05/05--->
			<FORM NAME="#IIF(count IS 1, DE("formA"), DE("formB"))#" ACTION="locdedupetask.cfm" METHOD="post">
				<TABLE BORDER="1">
					<TR>
						<TD NOWRAP align="right"><CFIF count IS 1>
						phr_sys_LocationID<CFELSE><BR></CFIF>
						</TD>
						<TD>
						#getlocs.LocationID[count]#
						</cfoutput>

						<!---
						PPB 2008-09-29
						the left side of the is screen is populated in the first loop (count=1) followed by the right hand side in the second loop (count=2);
						while populating the left hand side the currentEntity is that on the left and the otherEntity is that on the right... and vice versa;
						--->
						<CFIF count eq 1>
							<CFSET currentEntityId = getlocs.locationID[1]>
							<CFSET otherEntityId = getlocs.locationID[2]>
						<CFELSE>
							<CFSET currentEntityId = getlocs.locationID[2]>
							<CFSET otherEntityId = getlocs.locationID[1]>
						</CFIF>
							<CFSET EntityTypeId = 1>  <!--- WAB LID 2308 2009/06/02 needed for conflicting flags query --->

						<cfoutput>
							<CFINCLUDE template="searchparamform.cfm">
							<CF_INPUT TYPE="HIDDEN" NAME="frmDupLocs" VALUE="#ValueList(getLocs.LocationID)#">
							<CF_INPUT TYPE="HIDDEN" NAME="frmDupPeople" VALUE="#frmDupPeople#">
							<CF_INPUT TYPE="HIDDEN" NAME="frmStart" VALUE="#frmStart#">
							<CF_INPUT TYPE="HIDDEN" NAME="frmStartp" VALUE="#frmStartp#">
							<CF_INPUT TYPE="HIDDEN" NAME="refreshLocList" VALUE="#refreshLocList#">
	 						<CF_INPUT TYPE="HIDDEN" NAME="LocationsDel" VALUE="#Variables.LocationsDel#">

							<CF_INPUT TYPE="Hidden" NAME="LocationIDdel" VALUE="#otherEntityId#"><!--- Record to delete --->
							<CF_INPUT TYPE="Hidden" NAME="LocationIDsave" VALUE="#currentEntityId#"><!--- Record to save --->
							<CF_INPUT TYPE="Hidden" NAME="FreezeTime" VALUE="#CreateODBCDateTime(request.requestTime)#"><!--- Freeze Time --->
							<cfif isDefined("frmRadioName")>
								<CF_INPUT type="hidden" name="frmRadioName" value="#frmRadioName#">
								<CF_INPUT type="hidden" name="frmRadioAmount" value="#frmRadioAmount#">
							</cfif>
						</CFOUTPUT>
						</TD>
					</TR>

					<!--- ==============================================================================
					SWJ  03-08-2007  Added this query sim to control field list & attributes
					=============================================================================== --->
					<cfoutput>
					<!--- START: NYB 2009-04-22 - added cfloop to query --->
					<cf_querySim>
					getColumns
					label,name,fieldValue,size,maxlength
					phr_sys_siteName|insSiteName|getlocs.qrySitename[#htmleditformat(count)#]|30|50
					phr_sys_address|insAddress1|getlocs.Address1[#htmleditformat(count)#]|30|50
					phr_blank|insAddress2|getlocs.Address2[#htmleditformat(count)#]|30|50
					phr_blank|insAddress3|getlocs.Address3[#htmleditformat(count)#]|30|50
					phr_sys_towncity|insAddress4|getlocs.Address4[#htmleditformat(count)#]|30|50
					phr_sys_region|insAddress5|getlocs.Address5[#htmleditformat(count)#]|30|50
					phr_sys_postalCode|insPostalCode|getlocs.postalCode[#htmleditformat(count)#]|8|20
					phr_sys_switchboard|insTelephone|getlocs.telephone[#htmleditformat(count)#]|16|20
					phr_sys_Fax|insfax|getlocs.fax[#htmleditformat(count)#]|16|20
					<cfloop index="i" list="#application.com.settings.getSetting('plo.AdditionalLocDedupeFields')#">phr_sys_#htmleditformat(i)#|ins#htmleditformat(i)#|getlocs.#htmleditformat(i)#[#htmleditformat(count)#]|16|20
					</cfloop>
					</cf_querySim>
					</cfoutput>

					<cfoutput query="getColumns">
						<TR>
							<TD NOWRAP align="right">
							<CFIF count IS 1>
							#htmleditformat(label)#
							<cfelse>
								<CF_INPUT type="button" value=" >> " onClick="JavaScript:keepinfo('#htmleditformat(name)#')">
							</cfif>
							</TD>
							<TD><CF_INPUT TYPE="Text" NAME="#name#" VALUE="#evaluate(fieldValue)#" SIZE="#size#" MAXLENGTH="#maxlength#"></TD>
							<cfif count IS 1>
								<td align="right">
									<CF_INPUT type="button" value=" << " onClick="JavaScript:keepinfoLeft('#name#')">
								</td>
							</cfif>

						</TR>
					</cfoutput>

					<CFOUTPUT>

					<TR>
						<TD NOWRAP align="right"><CFIF count IS 1>
						phr_sys_accountType<CFELSE>
							<CF_INPUT type="button" value=" >> " onClick="JavaScript:keepinfo('insAccountTypeID')">
						</CFIF></TD>
						<TD>

						<SELECT NAME="insAccountTypeID"  class="form-control">
							<OPTION VALUE="0"#IIF(accountTypeID IS 0, DE(" SELECTED"), DE(""))#>Select an account type</OPTION>
							<CFSET ThisAccountType = accountTypeID>
							</CFOUTPUT>
							<cfloop collection="#application.organisationType#" item="orgType">
							<cfif isNumeric(orgType)>
							<cfoutput><OPTION VALUE="#application.organisationType[orgType].organisationTypeID#"#IIF(application.organisationType[orgType].organisationTypeID IS ThisAccountType, DE(" SELECTED"), DE(""))#>#htmleditformat(application.organisationType[orgType].name)#</OPTION></cfoutput>
							</cfif>
							</cfloop>
							<CFOUTPUT>
						</SELECT>

						</TD>
						<cfif count IS 1>
							<td align="right">
								<CF_INPUT type="button" value=" << " onClick="JavaScript:keepinfoLeft('insAccountTypeID')">
							</td>
						</cfif>

					</TR>

					<TR>
						<TD NOWRAP align="right"><CFIF count IS 1>
						phr_sys_country<CFELSE>
							<CF_INPUT type="button" value=" >> " onClick="JavaScript:keepinfo('insCountryID')">
						</CFIF></TD>
						<TD>

<!--- 						<SELECT NAME="insCountryID" class="form-control">
							<OPTION VALUE="0"#IIF(CountryID IS 0, DE(" SELECTED"), DE(""))#>Select a Country</OPTION>
							<CFSET ThisCountry = CountryID>
							</CFOUTPUT><CFOUTPUT QUERY="getCountries">
							<OPTION VALUE="#lCountryID#"#IIF(lCountryID IS ThisCountry, DE(" SELECTED"), DE(""))#>#htmleditformat(CountryDescription)#</OPTION></CFOUTPUT><CFOUTPUT>
						</SELECT> --->


						<CF_RELAYFORMELEMENT type="select" fieldName="insCountryID" query="#getCountries#" display="CountryDescription" value="lCountryID" currentvalue="#CountryID#" nulltext="Select a Country">


						</TD>
						<cfif count IS 1>
							<td align="right">
								<CF_INPUT type="button" value=" << " onClick="JavaScript:keepinfoLeft('insCountryID')">
							</td>
						</cfif>

					</TR>
<!---
					PPB 2008-09-29: not on spec for EMEA 1.1.3 so assumed no loger required
					<TR>
						<TD NOWRAP><CFIF count IS 1>
						More Details</CFIF></TD>
						<TD>
							<cfoutput><a href="javascript:viewentityFlags(#getlocs.LocationID[count]#,1,'locationDetail','CompleteLocationRecord');">View</a></cfoutput>
						</TD>

					</TR>
--->
					<TR><TD height="50"></TD></TR>		<!--- spacer row --->

					<!--- CONFLICTING FLAGS --->
					<CFINCLUDE template="dedupe_displayConfictingFlags.cfm">

					<TR>
						<TD NOWRAP>&nbsp;</TD>

						<TD ALIGN="LEFT">
							<INPUT TYPE="RESET" VALUE="phr_reset"><!--- <A HREF="JavaScript:document.formB.reset();"><IMG SRC="/images/buttons/c_resetrec_e.gif" WIDTH=146 HEIGHT=21 BORDER=0 ALT="Reset Record"></A> --->
							<br>
							<input name="KeepThisOne" type="button" value="phr_sys_Keep_This_One" onClick="savededup(#count#)" <cfif not isdefined("noMergeProtectionSet")><cfif mergeCheck[count].isMergeProtected EQ 0>disabled</cfif></cfif>>
						</TD>
					</TR>
					<tr>
					<td colspan ="2">
					<cfoutput>
					<!--- 2009/05/01 SSS message added to tell you why a button has been disabled and which flag is stopping you from mergeing this record--->
						 <cfif mergeCheck[count].isMergeProtected><div class="infoblock">
							 Profile '#mergeCheck[count].detail.flagname[1]#' is set.<BR>
							 This Location must be kept.
						</div>
						<cfelse>
						&nbsp;
						</cfif>
					</cfoutput>
					</td>
				</tr>
				</TABLE>
			</FORM></TD>
			</CFOUTPUT></CFLOOP>
		</TR>
	</TABLE>
</CFIF>
</CENTER>

<FORM ACTION="entityScreens.cfm" METHOD="post" NAME="flagListForm" TARGET="_blank">
	<INPUT TYPE="hidden" NAME="frmcurrentEntityID" VALUE="0">
	<INPUT TYPE="hidden" NAME="frmEntityType" VALUE="1">
	<INPUT TYPE="hidden" NAME="thisScreenTextID" VALUE="0">
	<INPUT TYPE="hidden" NAME="frmBack" VALUE="0">
	<INPUT TYPE="hidden" NAME="frmNext" VALUE="0">
	<INPUT TYPE="hidden" NAME="selIndex" VALUE="">
</FORM>

<CF_ABORT>