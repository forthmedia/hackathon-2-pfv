<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File name:		myAccounts.cfm	
Author:			SWJ
Date created:	7 Aug 2000

Description:	This code is a wrapper which calls the orglist.cfm screen 
				and passes one variable frmAccount

Date Tested:
Tested by:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2001-04-04		WAB				Changed so that doesn't CFABORT if no permissions.  This allows it to be included in the homepage without problems

--->


<CFSET displaystart = 1>
<CFSET frmLocsOnPage = "">	

<CFIF NOT checkPermission.Level1 GT 0>
	<CFSET message="You are not authorised to use this function">
		<CFINCLUDE TEMPLATE="datamenu.cfm">
	<CF_ABORT>
</CFIF>

	
	
	<!--- the application .cfm checks for read dataTask privleges
	--->
<!--- 2012-07-25 PPB P-SMA001 commented out
	<cfinclude template="/templates/qrygetcountries.cfm">
 --->	
	<CFQUERY NAME="getOrganisations" DATASOURCE=#application.SiteDataSource#>
		SELECT DISTINCT o.organisationName,
				o.OrganisationID,
				o.accountNumber,
				o.active,
			   o.LastUpdated,
			   c.CountryDescription,
			   c.ISOCode,
			   (SELECT count(LocationID) from Location WHERE OrganisationID = o.OrganisationID) AS pcount
		  FROM  Country AS c, 
		  organisation AS o
		<!---  2012-07-25 PPB P-SMA001 commented out 
		WHERE o.CountryID  IN ( <cf_queryparam value="#Variables.CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	     --->
	    WHERE 1=1 #application.com.rights.getRightsFilterWhereClause(entityType="organisation",alias="o").whereClause# 	<!--- 2012-07-25 PPB P-SMA001 --->
	    AND o.CountryID = c.CountryID 
		AND o.AccountMngrID = #request.relayCurrentUser.personid# 
		ORDER BY o.OrganisationName, c.CountryDescription, o.OrganisationID
	</CFQUERY>
	
	
	<TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="1">
		<TR>
			<TH ALIGN="left">My Accounts</TH>
			<TH>&nbsp;</TH>
		</TR>
	
		<TR>
			<TH ALIGN="left">Organisation (ID)</TH>
			<TH>Account Number</TH>
		</TR>
		<CFOUTPUT QUERY="getOrganisations">
		<TR>
				<TD><A HREF="JavaScript:viewEntity(2,#OrganisationID#);" CLASS="smallLink">#htmleditformat(organisationName)# (#htmleditformat(ISOCode)#)</A></TD>
				<TD><CFIF #AccountNumber# GT 0>Ac/No: #htmleditformat(AccountNumber)#</CFIF></TD> 
				<TD><B CLASS="AttentionFont">#IIF(active is 0,DE("(Inactive)"),DE(""))#</B></TD>
		</TR>
		</cfoutput>
	</TABLE>
	
