<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 	Code to put a drop down box on the organisation list screen

Amendment History
	WAB 1999-05-17		changes to parameters passed to updateflag
	WAB 2000-11-29 	Altered check for deletion rights to use a View (which used rights groups)
	MDC 2003-06-28 	Added in Approve Organisation
	SWJ 2005-04-13 	Removed old phrases code
	WAB 2005-12-05		altered javascript - didn't need newWindow.focus, 'cause this done in the openWin function (which also handles error conditions due to pop up blockers)
	SSS 2008-04-29	Give rights only the people that can edit flag AllocationSalesTechnical to be able to access account personal in the drop down.
	SSS 2008/07/02  organisation delete is now visible only if you have deletetask and deleteorgtask security tasks
	WAB	2008/12/01	Mods to make sure that merge functions appeared in the Ajax drilldown
	WAB	2008/12/01	Removed form around this drop down to improve formatting and bring in line with other drop downs
	NYB	2009-03-12	POL Deletion Process - changed checkOrgForDeletion.cfm to checkEntityForDeletion.cfm
	WAB 2010/10/05	request.manual....On becomes a setting
	PPB 2011/07/19  LEN024 added permissions
	NYB	2011-10-25  LHID7095,7963 - replaced multiple if's call around Delete Check Orgs to [new] function getOrganisationDeleteRights
					- to help achieve continuity throughout the system around delete rights
	2014-10-08	RPW	CORE-808 The organisation functions drop down list title is different to the pop-up window title.
--->


<cfparam name="mergeOK" default=0>  <!--- allows merge function to be hidden if say there is only one organisation on a page --->

	<select name="organisationDropDown" id="organisationDropDown" class="form-control" onchange="doDropDown(document.organisationFunctions.select1)">
	<option value="none" selected>Phr_Sys_OrganisationFunctions</option>

	<CFIF mergeOK>
		<optGroup label="Phr_Sys_Merge">
			<option value="JavaScript: checks = saveChecks('Organisation'); if (checks!='0') { void(newWindow = openWin( '../data/orgdedupe.cfm?frmOrgIds='+checks+'&frmruninpopup=true','PopUp','width=450,height=450,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1')); newWindow.focus() } else {noChecks('Organization')}"> Phr_Sys_MergeDuplicateOrganisations
		</optgroup>
 	</cfif>
	<CFOUTPUT>

	<optGroup label="Phr_Sys_Profiles">
		<cfif application.com.login.checkInternalPermissions("ProfileTask","Level2")>	<!--- 2011/07/19 PPB LEN024 added permission --->
			<option value="JavaScript: checks=saveChecks('Organisation') ; if (checks!='0')  { void(newWindow = openWin( '../data/moveOrgToDiffAccountManager.cfm?frmentityid='+checks+'&frmEntityTypeID=2','PopUp','width=450,height=450,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1'));  } else {noChecks('Organization')}"> Phr_Sys_MoveOrgsToNewAccountManager
			<option value="JavaScript: checks=saveChecks('Organisation') ; if (checks!='0')  { void(newWindow = openWin( '../flags/SetFlagsForIDList.cfm?frmentityid='+checks+'&frmEntityTypeID=2','PopUp','width=450,height=450,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1'));  } else {noChecks('Organization')}"> Phr_Sys_SetProfileAttributesOrgs

			<!--- SSS changed this AllocationSalesTechnical edit rights can access account personal --->
			<cfif application.com.flag.doesCurrentUserHaveRightsToFlagGroup(FlagGroupId="AllocationSalesTechnical", Type="edit")>
				<option value="JavaScript: checks=saveChecks('Organisation') ; if (checks!='0')  { void(newWindow = openWin( '../flags/SetFlagsForIDList.cfm?frmentityid='+checks+'&frmEntityTypeID=2&fgTextID=AllocationSalesTechnical','PopUp','width=450,height=450,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1')); } else {noChecks('Organization')}"> Phr_Sys_Allocate Phr_Sys_AccountPersonnel
			</cfif>
		</cfif>

		<!--- MDC - 2006-10-24 - This can now be easily accessed from the left menu and dosen't work well with the multiple approval processes

		<cfif application.com.login.checkInternalPermissions("ApprovalTask","Level1")>
				<option value="JavaScript: checks = saveChecks('Organisation');if (checks!='0') { void(openWin( '/profileManager/profileProcess.cfm?frmProcessid=RegApproval&frmstepid=0&frmOrganisationID='+checks,'PopUp','width=650,height=600,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1'));  } else {noChecks('Organisation')}">; Phr_Sys_ApproveOrgs
		</CFIF> --->
	<cfif application.com.login.checkInternalPermissions("ApprovalTask","Level1")>
			<cfquery name="getRegProcesses" datasource="#application.siteDataSource#">
			select distinct case when charindex('_',flaggroupTextID) = 0 then '' else substring(flaggroupTextID,1,charindex('_',flaggroupTextID)) end as Prefix,
					 name
			 from
				 flagGroup fg left join (flaggrouprights fgr inner join rightsGroup rg on fgr.userid = rg.usergroupid and rg.personid = #request.relayCurrentUser.personid# AND (fgr.EDIT = 1 or fgr.viewing=1)) on fg.flaggroupid = fgr.flaggroupid
			where
				flaggroupTextID like '%orgApprovalStatus'
				and
				case when fg.editaccessrights = 0 and fg.edit = 1 then 1 when fg.editaccessrights = 1 and fgr.edit = 1 then 1 else 0 end  = 1
			</cfquery>

			<cfloop query="getRegProcesses">
				<option value="JavaScript: checks = saveChecks('Organisation');if (checks!='0') { void(newWindow = openWin( '/approvals/approvalProcess.cfm?frmProcessid=#prefix#RegApproval&frmstepid=0&frmOrganisationID='+checks,'PopUp','width=600,height=660,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1')); } else {noChecks('Organization')}">Phr_Sys_ApproveOrgs (#htmleditformat(prefix)#)
			</cfloop>
		</cfif>

		<cfif application.com.login.checkInternalPermissions("CreateTask").recordcount eq 0 OR application.com.login.checkInternalPermissions("CreateTask","Level3")>	<!--- 2011/07/19 PPB LEN024 added permission; we only need to check it if the Task exists --->
		<option value="javaScript:newOrg(0)"> Phr_Sys_New phr_sys_organisation phr_sys_recordOnly
		</cfif>

	<cfif application.com.relayMenu.isModuleActive(moduleId="Funds") and application.com.login.checkInternalPermissions("fundTask","Level1")>
	<!--- 2014-10-08	RPW	CORE-808 The organisation functions drop down list title is different to the pop-up window title. --->
	<option value="JavaScript: if (saveChecks('Organisation')!='0') { void(newWindow = openWin( '../funds/createFundAccount.cfm?orgIDList='+saveChecks('Organisation')+'&t=1','PopUp','width=450,height=450,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1'));  } else {noChecks('Organization')}"> phr_sys_fund_CreateAccount
	</cfif>

	</optgroup>
		<cfif application.com.rights.getOrganisationDeleteRights()>
	<optGroup label="Phr_Sys_Deletion">
		<!--- NYB 2009-03-12 POL Deletion Process - changed checkOrgForDeletion.cfm to checkEntityForDeletion.cfm --->
		<option value="JavaScript: if (saveChecks('Organisation')!='0') { void(newWindow = openWin( '../data/checkEntityForDeletion.cfm?frmentityid='+saveChecks('Organisation')+'&frmflagtextid=DeleteOrganisation&frmruninpopup=true&frmflagvalue=1&frmshowfeedback=','PopUp','width=850,height=450,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1'));  } else {noChecks('Organization')}"> Phr_Sys_DeleteCheckedOrgs
	</optgroup>
	</cfif>
	<cf_include template="\code\CFTemplates\CustomFunctionsOrganisation.cfm" checkIfExists="true">
	</CFOUTPUT>
	</select>
	<cf_selectScrollJS selectBoxName="organisationDropDown" onChangeFunction="doDropDown(document.getElementById('organisationDropDown'))">