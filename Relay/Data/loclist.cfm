<!--- �Relayware. All Rights Reserved 2014 --->
<!---

loclist.cfm

Author:		Unknown

Date:		Unknown

Purpose:	Show's site listing.

Mods:

Date		Initial	Detail(s)
1999-04-16	WAB 	Added partner type and inactive and direct/indirect to listing
1999-04-13	WAB 	Added fields required for search by first and last name
2000-12-04 	WAB 	altered drop down so that it runs with generic javascript functions
2005-04-08 	RND		Extended sitename address details being output.
2005-05-17	WAB 	altered selection to hand selections shared with groups - rightsGroup
2008-06-12	SSS		have added merge tasks for switches also for merging.
2009-11-17	NAS		LID 2765 - HTMLEDITFORMAT added to allow Double Quotes ("") in Org Name.
2009-11-30 	NJH		LHID 2888 - pass through frmOrganisationID if it's defined.
2009-12-18	NAS		LID 2850 cfparam added as type numeric to stop 'or 1 = 1' sql injections.
2010-10-05	WAB 	request.manual....On becomes a setting
2011-07-21	PPB		use phrase phr_sys_inactive
2011-11-28	NYB 	LHID8224 changed to use phr_sys_recordfound if 1 record found, otherwise it uses sys_recordsfound
2012-02-27	NJH		CASE 425212: use getOrgFrame function to get the organisation frame, as it now has a unique ID appended to it, so
					can't call it with parent.orgList.
2014/06/03	NJH	Connector - Show Location HQ and changed what columns are displayed
2016/06/10	NJH		JIRA PROD2016-346 - showContactHistory link on location listing. Look at vContactHistory view to determine if location has a link to display.
2017-02-03	DBB		ENT-6: Hiding screen elements for read only users
--->

<!--- 	2009-12-18		NAS	LID 2850 cfparam added as type numeric to stop 'or 1 = 1' sql injections  --->
<cfparam name="frmSelectionID" type="numeric" default="0">

<cfset mergeOK = 0>
<CFSET displaystart = 1>
<CFSET frmLocsOnPage = "">
<cfset dataTaskL2 = application.com.login.checkInternalPermissions("DataTask","Level2")><!---DBB ENT-6--->

<CFIF NOT checkPermission.Level1 GT 0>
	<CFSET message="You are not authorised to use this function">
		<CFINCLUDE TEMPLATE="datamenu.cfm">
	<CF_ABORT>
</CFIF>


<!--- the application .cfm checks for read dataTask privleges
--->

<!--- all of these should really be coming in as URL. OR FORM.
		but we can fool the system so we don't need to check
		for IsDefined --->

<CFINCLUDE template = "searchparamdefaults.cfm"	>

<CFPARAM NAME="frmFlagIDs" DEFAULT="0">
<CFPARAM NAME="frmMoveFreeze" DEFAULT=#CreateODBCDateTime(Now())#>

<!--- check to see if any criteria exists --->
<CFPARAM NAME="AnyCriteria" DEFAULT="no">

<!--- <!---  add a % to the end of the sitename search if it is set--->
<CFIF right(trim(frmsitename),1) is NOT "%" and trim(frmsitename) is not "">
	<CFSET frmsitename=frmsitename&"%">
</cfif>
 --->

<!--- If the selection is a search criteria, then we need to limit
	the locations to the owner's countries as well
--->
<CFIF frmSelectionID IS NOT 0>

	<CFQUERY NAME="getOwnerCountries" datasource="#application.siteDataSource#">
		SELECT r.CountryID
		  FROM Rights AS r with (noLock),
			   RightsGroup AS rg with (noLock),
			   Selection AS s with (noLock),
			   UserGroup AS ug with (noLock)
		 WHERE s.SelectionID =  <cf_queryparam value="#frmSelectionID#" CFSQLTYPE="CF_SQL_INTEGER" >
		   AND rg.PersonID = ug.PersonID
		   AND ug.UserGroupID = s.CreatedBy
		   AND rg.UserGroupID = r.UserGroupID
		   AND r.Permission > 0
		   AND r.SecurityTypeID =
					(SELECT e.SecurityTypeID
					 FROM SecurityType AS e
					 WHERE e.ShortName = 'RecordTask')
		</CFQUERY>

		<CFSET OwnerCountries = 0>

		<CFIF getOwnerCountries.RecordCount GT 0>
			<CFIF getOwnerCountries.CountryID IS NOT "">
				<!--- top record (or only record) is not null --->
				<CFSET OwnerCountries = ValueList(getOwnerCountries.CountryID)>
			</CFIF>
		</CFIF>

<cfelse>
	<CFSET OwnerCountries = 0>
</CFIF>

<CFQUERY NAME="getLocations" datasource="#application.siteDataSource#">
	SELECT DISTINCT
	        l.LocationID,
			o.organisationName,
			o.OrganisationID,
		   l.SiteName,
		   l.Address1,
		   l.Address2,
		   l.Address3,
		   l.Address4,
		   l.Address5,
		   l.PostalCode,
		   l.Telephone,
		   l.Fax,
			l.active,
			l.direct,
		   l.LastUpdated,
			l.dupegroup,
		    <CFIF frmSelectionID IS NOT 0>s.Title,</CFIF>
		   c.CountryDescription,
		   c.ISOCode,
		   l.specificURL,
		   (select	max(dateSent) from commdetail with (noLock) WHERE LocationID = l.LocationID) as lastContactDate,
		   (select top 1 1 from vContactHistory where locationID = l.locationID and date > getdate() - 120) as hasContactHistory, <!--- JIRA PROD2016-346 - this date filter was taken from commHistory.cfm. If changed there, it needs changing here as well. Taking just last 3 months for performance, I assume --->
		   (SELECT count(PersonID) from Person with (noLock) WHERE LocationID = l.LocationID) AS pcount,
		   case when hq.data = l.locationID then 1 else 0 end as isHQ,
		   ot.typeTextId as accountType
	  FROM
	  		organisation AS o with (noLock)
				inner join
			 Location AS l with (noLock) on o.organisationID = l.organisationID
				inner join
		  Country AS c with (noLock) on l.CountryID = c.CountryID
		  	left join organisationType ot on ot.organisationTypeID = l.accountTypeID
	  		left join booleanFlagData locDelete with (noLock) on locDelete.entityID = l.locationId and locDelete.flagID = #application.com.flag.getFlagStructure(flagID="deleteLocation").flagID#
			left join integerFlagData HQ on HQ.entityID = o.organisationID and HQ.flagID =  #application.com.flag.getFlagStructure(flagID="HQ").flagID#


	  <CFIF Trim(frmEmail) IS NOT "" OR frmSelectionID IS NOT 0 or Trim(frmFirstName) IS NOT "" or Trim(frmLastName) IS NOT "" or trim(frmfax) is not "" or trim(frmtelephone) is not "" or (frmsrchPersonID) is not "">
			inner join Person AS p with (noLock) on l.LocationID = p.LocationID
		  <CFIF frmSelectionID IS NOT 0>
			inner join
			Selection AS s with (noLock) on s.TableName = 'Person'
			inner join
			SelectionTag AS st with (noLock) on s.SelectionID = st.SelectionID and st.EntityID = p.PersonID
			inner join
			SelectionGroup AS sg with (noLock) on s.SelectionID = sg.SelectionID
			inner join
			rightsGroup AS rg with (noLock) on sg.usergroupid = rg.usergroupid and rg.UserGroupID = #request.relayCurrentUser.personid#

		   </CFIF>
	  </CFIF>
	<!--- 2012-07-25 PPB P-SMA001 commented out
	WHERE l.CountryID  IN ( <cf_queryparam value="#Variables.CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	 --->
	WHERE 1=1 #application.com.rights.getRightsFilterWhereClause(entityType="location",alias="l").whereClause#	<!--- 2012-07-25 PPB P-SMA001 --->
		and locDelete.entityID is null
	<CFIF frmLastContactDate IS NOT "">
	and (select max(dateSent) from commdetail with (noLock) where LocationID=l.LocationID)  >  <cf_queryparam value="#frmLastContactDate#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
	</CFIF>
	<CFIF frmFlagIDs IS NOT 0>
		AND (
			l.LocationID IN (Select entityID FROM booleanflagdata with (noLock) where flagid  IN ( <cf_queryparam value="#frmFlagIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ))
			OR
			l.LocationID IN (select locationID from person with (noLock) where personid in (Select entityID FROM booleanflagdata where flagid  IN ( <cf_queryparam value="#frmFlagIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )))
			)
	</CFIF>
	<CFIF frmCountryID IS NOT 0>
	 	AND l.CountryID =  <cf_queryparam value="#frmCountryID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</CFIF>
	<CFIF frmSelectionID IS NOT 0>
		  <!--- also add SelectionTag to FROM --->
		    AND st.SelectionID =  <cf_queryparam value="#frmSelectionID#" CFSQLTYPE="CF_SQL_INTEGER" >
			AND c.CountryID  IN ( <cf_queryparam value="#Variables.OwnerCountries#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	</CFIF>

	<CFIF Trim(frmSiteName) IS NOT "">
		<CFIF frmLooseMatch IS NOT 1>
		<!--- The difference test below returns 0-4 where
			4 represents the closest matches and a DIFFERENCE of 3 indicates
			that the two character strings have a similar sound even
			though the differ in several characters but it returns strange results --->
			AND DIFFERENCE(l.SiteName, '#frmSiteName#') >= 4
		<CFELSE>
			AND l.SiteName  like  <cf_queryparam value="#frmSiteName#%" CFSQLTYPE="CF_SQL_VARCHAR" >
		</CFIF>
	 </CFIF>
	 <CFIF Trim(frmEmail) IS NOT "">
	  <!--- also added table to FROM --->
	    AND p.Email  LIKE  <cf_queryparam value="#frmEmail#" CFSQLTYPE="CF_SQL_VARCHAR" >
	 </CFIF>
	 <CFIF Trim(frmLastName) IS NOT "">
	    AND p.LastName  LIKE  <cf_queryparam value="#frmLastName#" CFSQLTYPE="CF_SQL_VARCHAR" >
	 </CFIF>
	 <CFIF Trim(frmFirstName) IS NOT "">
	    AND p.FirstName  LIKE  <cf_queryparam value="#frmFirstName#" CFSQLTYPE="CF_SQL_VARCHAR" >
	 </CFIF>
	 <CFIF Trim(frmFax) IS NOT "">
	    AND (l.Fax  like  <cf_queryparam value="#frmFax#" CFSQLTYPE="CF_SQL_VARCHAR" >  or p.faxphone  like  <cf_queryparam value="#frmFax#" CFSQLTYPE="CF_SQL_VARCHAR" > )
	 </CFIF>
	 <CFIF Trim(frmTelephone) IS NOT "">
	    AND (l.Telephone  like  <cf_queryparam value="#frmTelephone#" CFSQLTYPE="CF_SQL_VARCHAR" >  or p.officephone  like  <cf_queryparam value="#frmTelephone#" CFSQLTYPE="CF_SQL_VARCHAR" >  or p.homephone  like  <cf_queryparam value="#frmTelephone#" CFSQLTYPE="CF_SQL_VARCHAR" >  or p.mobilephone  like  <cf_queryparam value="#frmTelephone#" CFSQLTYPE="CF_SQL_VARCHAR" > )
	 </CFIF>
 	 <CFIF Trim(frmPostalCode) IS NOT "">
	    AND l.PostalCode  like  <cf_queryparam value="#frmPostalCode#" CFSQLTYPE="CF_SQL_VARCHAR" >
	 </CFIF>
 	 <CFIF Trim(frmAddress) IS NOT "">
	    AND (l.Address3  like  <cf_queryparam value="#frmAddress#" CFSQLTYPE="CF_SQL_VARCHAR" >  or l.Address4  like  <cf_queryparam value="#frmAddress#" CFSQLTYPE="CF_SQL_VARCHAR" >  or l.Address5  like  <cf_queryparam value="#frmAddress#" CFSQLTYPE="CF_SQL_VARCHAR" > )
	 </CFIF>
 	 <CFIF Trim(frmsrchlocationID) IS NOT "">
	 	AND l.locationid =  <cf_queryparam value="#frmsrchlocationID#" CFSQLTYPE="CF_SQL_INTEGER" >
	 </CFIF>
 	 <CFIF Trim(frmsrchOrgID) IS NOT "">
	 	AND l.organisationid =  <cf_queryparam value="#frmsrchOrgID#" CFSQLTYPE="CF_SQL_INTEGER" >

	<cfelseif isDefined("frmOrganisationid") >
		<!--- WAB 2007-11-21 added for calling this template within entityScreens.cfm  --->
			and l.organisationid =  <cf_queryparam value="#frmOrganisationid#" CFSQLTYPE="CF_SQL_INTEGER" >
	 </CFIF>
 	 <CFIF Trim(frmsrchpersonID) IS NOT "">
	 	AND p.personid =  <cf_queryparam value="#frmsrchpersonID#" CFSQLTYPE="CF_SQL_INTEGER" >
	 </CFIF>
 	 <CFIF Trim(frmNoContacts) IS "1">
	 	AND not exists (select 1 from person with (noLock) where locationid=l.locationid)
	 </CFIF>
  	 <CFIF Trim(frmDupeLocs) IS "1">
		AND l.dupeGroup <> 0
	 </CFIF>
  	 <CFIF Trim(frmDupePers) IS "1">
		AND p.dupeGroup <> 0
	 </CFIF>

	ORDER BY
	  	 <CFIF Trim(frmDupeLocs) IS "1">
		 l.dupeGroup,
		 </CFIF>
	  	 <CFIF Trim(frmDupePers) IS "1">
		 p.dupeGroup,
		 </CFIF>
		case when hq.data = l.locationID then 1 else 0 end desc,l.SiteName, c.CountryDescription, l.LocationID
</CFQUERY>

<!--- <CFIF getLocations.RecordCount IS NOT 0>
	<CFQUERY NAME="getPeople" datasource="#application.siteDataSource#">
	SELECT count(*) AS NumPeople FROM Person WHERE LocationID IN (#ValueList(getLocations.LocationID)#)
	</CFQUERY>
</CFIF> --->
<!--- get the text relating to Partner Type if the frm Variable is not 0
		NB there can be many partner types
--->
<CFIF frmFlagIDs IS NOT 0>
	<CFQUERY NAME="getPartnerType" datasource="#application.siteDataSource#">
		SELECT Name FROM Flag with (noLock)
		WHERE FlagID  IN ( <cf_queryparam value="#frmFlagIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		ORDER BY Name
	</CFQUERY>
	<CFIF getPartnerType.RecordCount IS NOT 0>
		<!--- put a space after each comma in the list --->
		<CFSET Partners = Replace(ValueList(getPartnerType.Name),",",", ","ALL")>
	<CFELSE>
		<CFSET Partners = "#frmFlagIDs# [description unavailable]">
	</CFIF>
</CFIF>

<CFIF frmCountryID IS NOT 0>
	<CFQUERY NAME="getCountryName" datasource="#application.siteDataSource#">
		SELECT CountryDescription with (noLock)
		FROM Country
		WHERE CountryID =  <cf_queryparam value="#frmCountryID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</CFQUERY>
</CFIF>

<CFIF frmSelectionID IS NOT 0>
	<CFQUERY NAME="getSelectionTitle" datasource="#application.siteDataSource#">
		SELECT s.Title,
				s.CreatedBy,
				p.FirstName,
				p.LastName
		FROM Selection AS s with (noLock), SelectionGroup AS sg with (noLock), UserGroup AS ug with (noLock), Person AS p with (noLock)
		WHERE sg.UserGroupID = #request.relayCurrentUser.usergroupid#
		AND s.SelectionID = sg.SelectionID
		AND s.SelectionID =  <cf_queryparam value="#frmSelectionID#" CFSQLTYPE="CF_SQL_INTEGER" >
		AND s.CreatedBy = ug.UserGroupID
		AND ug.PersonID = p.PersonID
	</CFQUERY>
</CFIF>


<CFIF getLocations.RecordCount GT application.showRecords>
	<!--- showrecords in application .cfm
			defines the number of records to display --->

	<!--- define the start record --->
 	<CFIF structKeyExists(FORM,"ListForward.x")>
		 <CFSET displaystart = frmStart + application.showRecords>
 	<CFELSEIF structKeyExists(FORM,"ListBack.x")>
		 <CFSET displaystart = frmStart - application.showRecords>
	<CFELSEIF IsDefined("frmStart")>
	 	 <CFSET displaystart = frmStart>
	</CFIF>

</CFIF>

<cf_includeJavaScriptOnce template="/javascript/dropdownFunctions.js">
<cf_includeJavaScriptOnce template="/javascript/checkBoxFunctions.js">
<!--- NJH 2007-04-25 included openWin.js --->
<cf_includeJavaScriptOnce template="/javascript/openWin.js">
<cf_includeJavaScriptOnce template="/javascript/viewEntity.js">

<cf_head>
<SCRIPT type="text/javascript">
<!--
		function backForm(FormName) {
			var form = eval('document.' + FormName);
			form.submit();
		}
		function moveThis(loc) {
			var form = document.moveForm;
			form.frmLocationID.value = loc;
			if(confirm("\nAre you sure you wish to move all selected people to this location?\n")) {
				form.submit();
			} else {
				form.frmLocationID.value = "0";
			}
		}
		function domainForm(loc){
			var form = document.mainForm;
			form.frmLocationID.value = loc;
			form.submit();
		}

		function doForm(FormName) {
			var form = eval('document.' + FormName);
<CFIF getLocations.RecordCount IS NOT 0>
<CFIF NOT IsDefined("frmMovePeople")>form.frmDupLocCheck.value = saveChecks('Location');</CFIF>
</CFIF>			form.submit();
		}

//-->
</SCRIPT>
</cf_head>


<!--- 2012-02-27	NJH	CASE 425212: use the getOrgFrame() function --->
<cfif isDefined("refreshOrgList") and refreshOrgList eq "yes" and isDefined("orgShortName") and orgShortName neq "">
	<cfoutput><cf_body onLoad="parent.getOrgFrame().location.href='/data/orglistv3.cfm?frmSiteName=#htmleditformat(orgShortName)#';"></cfoutput>
<cfelse>

</cfif>
<cfinclude template="_entityDetailForms.cfm">

<cf_translate>

<!--- Here starts the top table on the screen --->
<CFOUTPUT>

<CF_RelayNavMenu pageTitle="#getLocations.RecordCount# #IIF(getLocations.RecordCount eq 1,DE('phr_sys_recordFound'),DE('phr_sys_recordsFound'))#" thisDir="data">
	<cfif getLocations.RecordCount gt 1 and (application.com.login.checkInternalPermissions("mergeTask","Level2") or application.com.login.checkInternalPermissions("mergeLocTask","Level2")) and application.com.settings.getSetting ("plo.mergeAndDelete.manualMerge.location")>
		<CF_RelayNavMenuItem MenuItemText="Phr_Sys_Merge" CFTemplate="JavaScript: checks = saveChecks('Location'); if (checks!='0') { void(openWin( '../data/locdedupe.cfm?frmDupLocs='+checks+'&frmruninpopup=true&refreshLocList=yes','PopUp','width=750,height=800,left=50,top=50,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1'));  } else {noChecks('Locations')};">
	</cfif>
</CF_RelayNavMenu>

</CFOUTPUT>
<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
	<TR>
		<CFIF getLocations.RecordCount GT 1>
			<cfset mergeOK = 1>
		</CFIF>
		<!---START: DBB ENT-6--->
		<cfif dataTaskL2>
		<TD ALIGN="right" VALIGN="top"><CFINCLUDE template="locationDropDown.cfm"></TD>
		</cfif>
		<!---END: DBB ENT-6--->
	</TR>
</TABLE>


<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
<CFIF getLocations.RecordCount IS 0>

	<P>phr_sys_noRecordsFound

<CFELSE>
<CFIF IsDefined("frmMovePeople")>
	<CFOUTPUT>
	<FORM NAME="moveForm" METHOD="POST" ACTION="movetask.cfm">
	<CF_INPUT TYPE="HIDDEN" NAME="frmMovePeople" VALUE="#frmMovePeople#">

	</CFOUTPUT>
<CFELSE>
	<FORM NAME="mainForm" METHOD="POST" ACTION="locmain.cfm">
</CFIF>
	<CFPARAM NAME="frmMoveFreeze" DEFAULT=#CreateODBCDateTime(Now())#>
	<CFOUTPUT>
	<CF_INPUT TYPE="HIDDEN" NAME="frmMoveFreeze" VALUE="#frmMoveFreeze#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmDupLocs" VALUE="#frmDupLocs#">
	<CFINCLUDE template="searchparamform.cfm">

	<!--- NJH 2009-11-30 LHID 2888 - pass frmOrganisation if it's defined --->
	<cfif isDefined("frmOrganisationid") >
		<CF_INPUT TYPE="HIDDEN" NAME="frmOrganisationid" VALUE="#frmOrganisationid#">
	 </CFIF>
	<INPUT TYPE="HIDDEN" NAME="frmLocationID" VALUE="0">
	<CF_INPUT TYPE="HIDDEN" NAME="frmStart" VALUE="#Variables.displaystart#">
	<INPUT TYPE="HIDDEN" NAME="frmBack" VALUE="loclist">
	</CFOUTPUT>
<!--- Start the main data table --->


	<TR>
		<CFIF NOT IsDefined("frmMovePeople")>
		<TH ALIGN="CENTER" VALIGN="MIDDLE" style="text-align:center;">
			<cf_input type="checkbox" id="tagAllLocs" name="tagAllLocs" onclick="tagCheckboxes('frmLocationCheck',this.checked)">
		</TH>
		</CFIF>
		<TH ALIGN="left">
			Phr_Sys_SiteName
		</TH>
		<TH ALIGN="left">
			Phr_Sys_accountType
		</TH>
		<TH ALIGN="left">
			Phr_Sys_website
		</TH>
 		<TH>
			Phr_Sys_Switchboard
		</TH>
		<TH>
			Phr_Sys_Country
		</TH>
		<th>
			phr_sys_contacts
		</th>
	</TR>

		<CFSET endrow= min(application.showRecords+Variables.displaystart, getlocations.recordcount)>
<!--- 	<CFOUTPUT QUERY="getLocations" STARTROW=#Variables.displaystart# MAXROWS=#application.showRecords#> --->
		<CFLOOP QUERY="getLocations" STARTROW=#Variables.displaystart# ENDROW=#endrow#>
		<CFOUTPUT>
		<!--- gather a list of all the locations displayed on this page --->
		<CFSET frmLocsOnPage = ListAppend(frmLocsOnPage,LocationID)>

	<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
		<CFIF NOT IsDefined("frmMovePeople")>
		<TD ALIGN="CENTER" VALIGN="MIDDLE">
			<CF_INPUT TYPE="CHECKBOX" NAME="frmLocationCheck" VALUE="#LocationID#" CHECKED="#iif(ListFind(frmDupLocs,LocationID) IS NOT 0,true,false)#">
		</TD>
		</CFIF>
		<CFSET p1 = Trim(PostalCode)><CFSET a1 = Trim(Address1)><CFSET a2 = Trim(Address2)><CFSET a3 = Trim(Address3)><CFSET a4 = Trim(Address4)><CFSET a5 = Trim(Address5)>
		<CFSET c1 = Trim(CountryDescription)>
		<TD>
			<!--- 2009-11-17	NAS		LID 2765 - HTMLEDITFORMAT added to allow Double Quotes ("") in Org Name. --->
			<A HREF="JavaScript:void(viewLocation(#locationID#));" CLASS="smallLink" TITLE="Parent: #HTMLEDITFORMAT(organisationName)# (#OrganisationID#) ">#htmleditformat(SiteName)#</A><cfif isHQ>&nbsp;(<b>HQ</b>)</cfif>
			<B CLASS="AttentionFont">#IIF(active is 0,DE("(phr_sys_Inactive)"),DE(""))#</B>

			<BR>
			<I CLASS="SmallFontDim">
			<cfif len(address1) gt 0>#htmleditformat(address1)#, </cfif><cfif len(address4) gt 0>#htmleditformat(address4)#, </cfif><cfif len(address5) gt 0>#htmleditformat(address5)#, </cfif> <cfif len(postalcode) gt 0>#htmleditformat(postalcode)#, </cfif> #htmleditformat(ISOCode)#
			</I>

			<BR>
		</TD></cfoutput>



		<CFOUTPUT>
		<TD VALIGN="top">
			#htmleditformat(accountType)#
		</TD>
		<TD VALIGN="top">
			<a href="http://#htmleditformat(specificURL)#" target="_blank">#htmleditformat(specificURL)#</a>
		</TD>
		<TD VALIGN="top">
			#htmleditformat(Telephone)#
		</TD>
		<TD VALIGN="top">
			#htmleditformat(CountryDescription)#
		</TD>
		<td>
			<a href="javaScript:void(viewLocation(#locationID#,'locationContactHistory'))" title="Phr_Sys_ViewContactHistoryWith #sitename#" class="smallLink">
				<cfif hasContactHistory is 1>
					<span id="orgListHistory" class="fa fa-history"></span>
				<CFELSE>
					<img src="/images/MISC/transparent.gif" width="16" height="16" border=0>
				</cfif>
			</a>
		</td>
	</TR>
	</CFOUTPUT>
	</CFLOOP>

	</TABLE>
	<CF_INPUT TYPE="HIDDEN" NAME="frmLocsOnPage" VALUE="#frmLocsOnPage#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmLocsInReport" VALUE="#valuelist(getLocations.locationid)#">
	<CFIF getLocations.RecordCount GT application.showRecords>
		<!--- if we only show a subset, then give next/previous arrows --->


	<P><TABLE BORDER=0>

		<TR><TD>
				<CFIF Variables.displaystart GT 1>
					<INPUT TYPE="Image" NAME="ListBack" SRC="/images/buttons/c_prev_e.gif" WIDTH=64 HEIGHT=21 BORDER=0 ALT="">
				</CFIF>
				</TD>
			<TD>
	 				<CFIF (Variables.displaystart + application.showRecords - 1) LT getLocations.RecordCount>
						<CFOUTPUT>Record(s) #htmleditformat(Variables.displaystart)# - #Evaluate("Variables.displaystart + application.showRecords - 1")#</CFOUTPUT>
					<CFELSE>
	 					<CFOUTPUT>Record(s) #htmleditformat(Variables.displaystart)# - #getLocations.RecordCount#</cFOUTPUT>
					</CFIF>
				</TD>

			<TD>
				<CFIF (Variables.displaystart + application.showRecords - 1) LT getLocations.RecordCount>
					<INPUT TYPE="Image" NAME="ListForward" SRC="/images/buttons/c_next_e.gif" WIDTH=64 HEIGHT=21 BORDER=0 ALT="Next">
				</cFIF>
				</TD>
		</TR>
		</TABLE>


	</CFIF>
	</FORM>
	<P>
	<BR>

</CFIF>

</cf_translate>

<CFIF IsDefined("frmMovePeople")>
	<FORM NAME="BacktoSearch" METHOD="POST" ACTION="locsearch.cfm">
		<CFPARAM NAME="frmMoveFreeze" DEFAULT=#CreateODBCDateTime(Now())#>
	<CFOUTPUT>
		<CF_INPUT TYPE="HIDDEN" NAME="frmDupPeople" VALUE="#frmMovePeople#">
		<CF_INPUT TYPE="HIDDEN" NAME="frmMoveFreeze" VALUE="#frmMoveFreeze#">
	</CFOUTPUT>
	</FORM>
<CFELSE>

	<!--- form to pass search criteria to deduping --->
	<FORM NAME="DedupeForm" METHOD="POST" ACTION="locdedupe.cfm">
	<CFPARAM NAME="frmMoveFreeze" DEFAULT=#CreateODBCDateTime(Now())#>
	<CFOUTPUT>
	<CF_INPUT TYPE="HIDDEN" NAME="frmMoveFreeze" VALUE="#frmMoveFreeze#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmDupLocs" VALUE="#frmDupLocs#">
	<INPUT TYPE="HIDDEN" NAME="frmDupLocCheck" VALUE="0">
	<CF_INPUT TYPE="HIDDEN" NAME="frmLocsOnPage" VALUE="#frmLocsOnPage#">
	</CFOUTPUT>
	<CFINCLUDE template="searchparamform.cfm">

	</FORM>

</CFIF>

 	<!--- form to pass search criteria to new location  --->
	<FORM NAME="NewForm" METHOD="POST" ACTION="locdetail.cfm">

	<CFOUTPUT>
	<CF_INPUT TYPE="HIDDEN" NAME="frmMoveFreeze" VALUE="#frmMoveFreeze#">

	<CF_INPUT TYPE="HIDDEN" NAME="frmStart" VALUE="#Variables.displaystart#">
	<INPUT TYPE="HIDDEN" NAME="frmBack" VALUE="loclist">

	<CF_INPUT TYPE="HIDDEN" NAME="frmDupLocs" VALUE="#frmDupLocs#">
	<INPUT TYPE="HIDDEN" NAME="frmDupLocCheck" VALUE="0">
	<CF_INPUT TYPE="HIDDEN" NAME="frmLocsOnPage" VALUE="#frmLocsOnPage#">
	</CFOUTPUT>
	<CFINCLUDE template="searchparamform.cfm">

	</FORM>

</CENTER>
<CFSET frmNextTarget = "Detail">
<CFINCLUDE TEMPLATE="_editMainEntities.cfm">

<CFIF NOT IsDefined("frmMovePeople")>
	<cfset application.com.globalFunctions.cfcookie(NAME="LFLAGSCHECKED", VALUE="no-0")>
	<cfset application.com.globalFunctions.cfcookie(NAME="PFLAGSCHECKED", VALUE="no-0")>
</CFIF>