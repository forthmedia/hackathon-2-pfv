<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
createUserNamePassword.cfm

Author  WAB

Date 2000-03-08

Purpose

Can be called with a list of personids to create usernames and passwords

SWJ 2003-02-20 Added the layout and ability to pass in a selectionID
WAB 2005-05-04  moved to the data directory and added test for 
WAB 2005-05-17  added  rightsgroup to selection query

 --->
 
<cf_head>
	<cf_title>Create User Name and Passwords</cf_title>
</cf_head>


<cfif not application.com.login.checkInternalPermissions("UserTask","Level1")>
	Sorry you do not have rights to this function <BR>
	UserTask:Level1

<cfelse>	




	<h2>User Names and Passwords</h2>
	<cfif isDefined("frmSelectionID")>
		<!--- WAB 2005-05-17 altered to use rightsgroup table --->
		<CFQUERY NAME="getRecords" datasource="#application.siteDataSource#">
		SELECT p.PersonID
			FROM dbo.organisation o INNER JOIN
		         dbo.Location l ON o.OrganisationID = l.OrganisationID INNER JOIN
		         dbo.Selection s INNER JOIN
		         dbo.SelectionTag st ON s.SelectionID = st.SelectionID INNER JOIN
		         dbo.Person p ON st.EntityID = p.PersonID INNER JOIN
		         dbo.SelectionGroup sg ON s.SelectionID = sg.SelectionID 
				inner join RightsGroup rg on sg.usergroupid = rg.usergroupid
				 ON l.LocationID = p.LocationID INNER JOIN
		         dbo.Country c ON l.CountryID = c.CountryID
			WHERE s.SelectionID  in ( <cf_queryparam value="#frmSelectionID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			  AND s.Tablename = 'Person'
			  AND p.active = 1
			  AND rg.PersonID = #request.relayCurrentUser.personid#
			  AND c.CountryID IN (SELECT r.CountryID
				  FROM Rights AS r,
					   RightsGroup AS rg,
					   Selection AS s,
					   UserGroup AS ug
				 WHERE s.SelectionID  in ( <cf_queryparam value="#frmSelectionID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
				   AND rg.PersonID = ug.PersonID
				   AND ug.UserGroupID = s.CreatedBy
				   AND rg.UserGroupID = r.UserGroupID
				   AND r.Permission > 0
				   AND r.SecurityTypeID =
							(SELECT e.SecurityTypeID
							 FROM SecurityType AS e
							 WHERE e.ShortName = 'RecordTask'))
		</cfquery>
		<cfset frmPersonID = valuelist(getRecords.personid)>
		<cfset relayCounter = 0>
			<p>User name and passwords were created for: </p>
		<cfloop list="#frmPersonID#" index="personID">
			<!--- <cf_createUserNameAndPassword
				personid = "#personID#"
				resolveDuplicates = true	
				OverWritePassword = false
				OverWriteUserName = false
			> --->
			<cfscript>
				getUserName = application.com.login.createUserNamePassword(personid);
			</cfscript>
			<cfset relayCounter = relayCounter + 1>
			<cfoutput>#getUserName.fullname#</cfoutput><br/>
		</cfloop>
	
	
	<cfelseif isDefined("frmPersonID")>
		<p>User name and passwords were created for: </p>
		<cfloop list="#frmPersonID#" index="personID">
			<!--- <cf_createUserNameAndPassword
				personid = "#personID#"
				resolveDuplicates = true	
				OverWritePassword = false
				OverWriteUserName = false
			> --->
			<cfscript>
				getUserName = application.com.login.createUserNamePassword(personid);
			</cfscript>
			<cfoutput>#htmleditformat(getUserName.fullname)#</cfoutput><br/>
		</cfloop>
	<cfelse>
		<p>You must provide either frmPersonID or frmSelectionID for this to work</p>
	</cfif>
</cfif>
	
	
