<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		moveOrgToDiffAccountManager.cfm
Objective - to move one or more entities from one to a new organisation record.
				Steps involved are:
				a) receive a list of entityID's to move
				b) search for the org to move them to
				c) choose the one and update the database record

Amendment History:
Date (YYYY-MM-DD)	Initials 	What was changed

Enhancement still to do:
--->

<CFPARAM NAME="frmEntityID" TYPE="string" DEFAULT="0">

<CFQUERY NAME="qOrg" datasource="#application.siteDataSource#">
	select organisationName, organisationid, accountMngrID
	from organisation 
	where organisationid  in ( <cf_queryparam value="#frmEntityID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	order by organisationName
</CFQUERY>

<cfif structKeyExists(form,"frmSearch") eq "yes">	
	<cfset qAccountManager = application.com.validValues.getAccountManagers(searchString=form.frmSearchString)>	
</CFIF>

<cfif structKeyExists(form,"frmMove") and form.frmMove eq "Transfer">
	
	<!--- <CFQUERY NAME="updateOrganisations">
		update organisation 
		set accountMngrID =  <cf_queryparam value="#form.frmSelectedAccountManagerID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		where organisationid  in ( <cf_queryparam value="#form.frmEntityID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	</CFQUERY>	 --->
	
	<cfset application.com.flag.setFlagData(entityId=form.frmEntityID,flagId="AccManager",data=form.frmSelectedAccountManagerID)>
	
	<CFQUERY NAME="qNewAccountManager" datasource="#application.siteDataSource#">
		select firstname + ' ' + lastname as Name
		from person
		where personid = <cf_queryparam value="#form.frmSelectedAccountManagerID#" CFSQLTYPE="CF_SQL_INTEGER"> 
	</CFQUERY>
	<CFSET message = "#HTMLEditFormat(valuelist(qOrg.organisationName,", "))# moved to Account Manager #htmlEditFormat(qNewAccountManager.name)#">
	
</CFIF>

<cf_title>Transfer Organizations</cf_title>

<cfset application.com.request.setTopHead(PageTitle='Phr_Sys_MoveOrgsToNewAccountManager',topheadcfm='')>

<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
	
	<cfif not structKeyExists(form,"frmMove")>
		<TR>
			<TD>The following organizations will be transferred:<BR>
			<ol>
			<CFOUTPUT><cfloop query="qOrg" ><li>#htmlEditFormat(organisationName)# (#htmleditformat(organisationid)#)</li></cfloop></CFOUTPUT>
			</ol>
			</TD>
		</TR>
	</cfif>
	
	<CFIF frmEntityID eq 0>
		<form method="POST" name="personForm">
			<TR>
				<TD VALIGN="top" CLASS="label">OrganisationID</TD>
				<TD><CF_INPUT TYPE="Text" NAME="frmEntityID" MESSAGE="You must put a valid organisationid" REQUIRED="Yes" SIZE="15"></TD>
			</TR>
			<TR>
				<TD>&nbsp;</TD>
				<TD><INPUT TYPE="submit" VALUE="Search"></TD>
			</TR>
		</FORM>
	<CFELSEIF frmEntityID neq "0" and not isDefined('message')>
		<form method="POST" name="orgSearchForm">
			<TR>
				<TD VALIGN="top"><HR WIDTH="100%" SIZE="1" COLOR="steelblue"></TD>
			</TR>
			<TR>
				<TD NOWRAP>Search for an existing Account Manager: <CF_INPUT TYPE="Text" NAME="frmSearchString" MESSAGE="You must put a valid organisationid" REQUIRED="Yes" SIZE="15">
				<INPUT TYPE="submit" NAME="frmSearch" VALUE="Search"></TD>
			</TR>
			<TR>
				<TD></TD>
			</TR>
			<CFOUTPUT><CF_INPUT TYPE="hidden" NAME="frmEntityID" VALUE="#frmEntityID#"></CFOUTPUT>
		</FORM>
	</CFIF>
	
	<CFIF frmEntityID neq "0" and isDefined('qAccountManager.recordCount') and qAccountManager.recordCount gt 0 and not structKeyExists(form,"frmMove")>
		<form method="POST" name="orgSearchForm2">
			<TR>
				<TD VALIGN="top"><HR WIDTH="100%" SIZE="1" COLOR="steelblue"></TD>
			</TR>
			<TR>
				<TD>Select which Account Manager to move them to:<BR>				
				<CF_SELECT NAME="frmSelectedAccountManagerID"
			          SIZE="#min(qAccountManager.recordCount,15)#"
			          MESSAGE="You must select an Account Manager"
			          QUERY="#qAccountManager#"
			          VALUE="dataValue"
			          DISPLAY="displayValue"		         
			          REQUIRED="Yes">
			     </CF_SELECT>
				</TD>
			</TR>
			<TR>
				<TD ALIGN="right"><INPUT TYPE="submit" NAME="frmMove" VALUE="Transfer"></TD>
			</TR>
			<CFOUTPUT><CF_INPUT TYPE="hidden" NAME="frmEntityID" VALUE="#frmEntityID#"></CFOUTPUT>
		</FORM>
	</CFIF>
	
	<CFIF isDefined('message')>
		<TR><CFOUTPUT>
			<TD CLASS="messageText">The following organizations :<BR>
				<ol>
					<cfloop query="qOrg" ><li>#htmlEditFormat(organisationName)# (#htmleditformat(organisationid)#)</li></cfloop>
				</ol>
				have been linked to #htmleditformat(qNewAccountManager.name)#
			</TD>
			</CFOUTPUT>
		</TR>
		<tr><td><input type=button value="Close Window" onClick="self.close();"></td></tr>
	</CFIF>
	
</TABLE>
<script type="text/javascript">jQuery(document).ready(function(){
   document.body.scrollTop = 0;
});</script>