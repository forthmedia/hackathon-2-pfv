<!--- �Relayware. All Rights Reserved 2014 --->
<CFIF not isDefined("frmEntityID")>
	<cfabort showerror="frmEntityID must be defined">
</CFIF>
	
<CFIF not isDefined("frmEntityTypeID")>
	<cfabort showerror="frmEntityTypeID must be defined">
</CFIF>

<!--- NJH 2008-09-24 added entityTypeID 22 for elearning 8.1 --->
<CFSET supportedEntityTypes = "0,1,2,22">
<CFIF listFind(supportedEntityTypes,frmEntityTypeID) eq 0>
	<cfabort showerror="You cannot currently add a note to entityType #frmEntityTypeID#">
</CFIF>

	
<CFIF isDefined("frmSaveNote") and frmSaveNote eq "Save Note">
	<CFIF isDefined("noteText") and Trim(noteText) neq "">
		<cfinsert datasource="#application.siteDataSource#"
          tablename="entityNote"
          tableowner="dbo"
          formfields="EntityID,EntityTypeID,NoteType,NoteText,createdBy,created,lastUpdatedBy,lastUpdated">
		<cfset message="Note saved">
	</CFIF>
</CFIF>

<cfquery name="getNotes" datasource="#application.sitedatasource#">
	select NoteText, NoteType from entityNote 
	<cfif isDefined("NoteType")>
		WHERE noteType =  <cf_queryparam value="#NoteType#" CFSQLTYPE="CF_SQL_VARCHAR" > 
	</cfif>
</cfquery>
<cfparam name="noteText" type="string" default="#getNotes.noteText#">
<cfparam name="NoteType" type="string" default="#getNotes.NoteType#">
<script>
	function saveNote(){
	document.mainForm.frmSaveNote.value = "save note";
	document.mainForm.submit();
	}
	
	
	function submitPrimaryForm () {   // for calling from the entityNavigation
		saveNote()
	}

</script>


<cf_head>
	<cf_title><CFOUTPUT>#htmleditformat(frmEntityType)# Note</CFOUTPUT></cf_title>
</cf_head>


<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="2" WIDTH="100%">
	<TR>
		<TD ALIGN="LEFT" CLASS="Submenu"><CFOUTPUT>Add note to #htmleditformat(frmEntityType)#</CFOUTPUT></TD>
		<TD ALIGN="Right" CLASS="Submenu"><A HREF="javaScript: saveNote()" CLASS="Submenu">Save Note</A>&nbsp;&nbsp;&nbsp;</TD>
	</TR>
</TABLE>


<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
	<TR>
		<TD ALIGN="LEFT" VALIGN="top">

		</TD>
	</TR>
	<TR>	
		<TD ALIGN="Left" VALIGN="top">
			<CFFORM METHOD="post" NAME="mainForm">
				<cfselect name="NoteType" selected="#noteType#" required="Yes" onChange="saveNote()">
	<cfoutput>				<OPTION VALUE="">Please select type</OPTION>
					<OPTION VALUE="General" #IIF(NoteType eq "General",DE('SELECTED'),DE(''))#>General</OPTION>
					<OPTION VALUE="Information" #IIF(NoteType eq "Information",DE('SELECTED'),DE(''))#>Information</OPTION>
					<OPTION VALUE="Other" #IIF(NoteType eq "Other",DE('SELECTED'),DE(''))#>Other</OPTION>
</cfoutput>				</cfselect><br>
				<CFOUTPUT>
				<TEXTAREA COLS="80" ROWS="25" NAME="noteText">#noteText#</TEXTAREA>
				<INPUT TYPE="hidden" NAME="frmSaveNote" VALUE="">
				
					<CF_INPUT TYPE="hidden" NAME="EntityID" VALUE="#frmEntityID#">
					<CF_INPUT TYPE="hidden" NAME="EntityTypeID" VALUE="#frmEntityTypeID#">
					<CF_INPUT TYPE="hidden" NAME="created" VALUE="#now()#">
					<CF_INPUT TYPE="hidden" NAME="createdBy" VALUE="#request.relayCurrentUser.usergroupid#">
					<CF_INPUT TYPE="hidden" NAME="lastUpdated" VALUE="#now()#">
					<CF_INPUT TYPE="hidden" NAME="lastUpdatedBy" VALUE="#request.relayCurrentUser.usergroupid#">

				</CFOUTPUT>
			</CFFORM>
		</TD>
	</TR>
</table>



