<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File name:		movePersonToDiffOrg.cfm
Author:			SWJ
Date created:	2001-11-17

	Objective - to move one or mor people from one to a new organisation record.
				Steps involved are:
				a) receive a list of personID's
				b) 
	
	Rationale - Please describe why this was built and any useful contextual information
				about the conditions of use
	
	Syntax	  -	ProcName [Var1]>,[Var2]>,[ReturnRecordsets]
	
	Parameters - Please describe any parameters that can be passed to the proc and
				their purpose and usage.
				
	Return Codes - Please describe the return codes are returned to let you know certain 


Date Tested:
Tested by:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2006-06-20		WAB				in response to an action made some changes to the feedback (added names not just numbers) allowed search on organisationid
2007-02-23		SSS				I am checking for the frmSearchString being avaliable to do the serach instaed of the search button being pressed. This enables a person topress the enetr key to complete the search as well as the button
2009/01/28		NJH				Remove orgs flagged for deletion from the search list.
2011-05-20		AJC				Issue 6113: Search in Japanese
Enhancement still to do:



--->

<CFPARAM NAME="frmPersonIds" TYPE="string" DEFAULT="0">


<CFQUERY NAME="getPeople" datasource="#application.siteDataSource#">
select firstname, lastname, personid from person where personid  in ( <cf_queryparam value="#frmPersonIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
</CFQUERY>


<!--- *********************************************************************
	  we're searching for the organisation to link them to --->
<cfif structKeyExists(form,"frmSearchString") eq "yes">
	<CFQUERY NAME="getOrganisation" datasource="#application.siteDataSource#">
		select organisationName +' (ID:'+ convert(nvarchar(15),o.organisationID) +') ('+ISOCode+', '+postalCode+', '+l.sitename+')' as displayValue, 
		convert(nvarchar(15),o.organisationID)+'-'+convert(nvarchar(15),l.locationID) as IDValue 
		from organisation o 
		inner join location l on o.organisationID = l.organisationID
		inner join country c on l.countryID = c.countryID
		<!--- NJH 2009/01/28 Bug Fix All Sites Issue 1694 - do not show organsations that are flagged for deletion --->
		left join booleanFlagData bfd on bfd.entityID = o.organisationID and bfd.flagID =  <cf_queryparam value="#application.com.flag.getFlagStructure("deleteOrganisation").flagID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		<!--- START: 2011-05-20 AJC Issue 6113: Search in Japanese --->
		where (organisationName  like  <cf_queryparam value="#form.frmSearchString#%" CFSQLTYPE="CF_SQL_VARCHAR" > 
		<!--- END: >2011-05-20 AJC Issue 6113: Search in Japanese --->
		<cfif isNumeric(form.frmSearchString)>
		or o.organisationID =  <cf_queryparam value="#form.frmSearchString#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfif>
		) and bfd.entityID is null
	</CFQUERY>
</CFIF>

<!--- *********************************************************************
	  we're doing the update --->
<cfif structKeyExists(form,'frmMove') and form.frmMove eq "move">
	<!--- NJH 2009/01/29 Bug Fix All Sites Issue 1695 - added updated by and updated to the update statement --->
	<CFQUERY NAME="updatePerson" datasource="#application.siteDataSource#">
		update person 
		set organisationID =  <cf_queryparam value="#ListFirst(form.frmIDs,"-")#" CFSQLTYPE="CF_SQL_INTEGER" > ,
			locationID =  <cf_queryparam value="#ListLast(form.frmIDs,"-")#" CFSQLTYPE="CF_SQL_INTEGER" > ,
			LastUpdatedBy = #request.relaycurrentuser.usergroupID#,
			LastUpdated = #createODBCDateTime(request.requestTime)#
		where personid  in ( <cf_queryparam value="#form.frmPersonIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	</CFQUERY>
	<CFQUERY NAME="getMovedPeople" datasource="#application.siteDataSource#">
	select fullname, organisationname, sitename, personid from vpeople where personid  in ( <cf_queryparam value="#frmPersonIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	</CFQUERY>
	
	<!--- 2013/11/21	YMA	Case 437630 Move opportunities with people who change organisation --->
	<cfquery name="getOpportunities" datasource="#application.siteDataSource#">
		select opportunityID from opportunity where partnersalespersonID in ( <cf_queryparam value="#form.frmPersonIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	</cfquery>
	
	<cfloop query="getOpportunities">
		<cfquery name="updateopportunity" datasource="#application.siteDataSource#">
			UPDATE opportunity 
			SET partnerLocationID = <cf_queryparam value="#ListLast(form.frmIDs,"-")#" CFSQLTYPE="CF_SQL_INTEGER" >
			WHERE opportunityID =  <cf_queryparam value="#getOpportunities.opportunityID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfquery>
	</cfloop>
	
	<CFSET message = "">
	<cfloop query="getMovedPeople">
		<CFSET message = message & "#fullname#<BR>">
	</cfloop>
		<CFSET message = message & "moved to <BR>#getMovedPeople.organisationname# (#getMovedPeople.sitename#)<BR>">
<!--- 	<CFSET message = "#form.frmPersonIDs# moved to organisationID #ListFirst(form.frmIDs,'-')# and locationID #ListLast(form.frmIDs,'-')#"> --->

</CFIF>


<cf_title>Move Person</cf_title>
<cf_RelayNavMenu pageTitle="Move a Person to a Different Organisation" thisDir="data"/>

<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
<cfif not structKeyExists(form,'frmMove')>
	<TR>
		<TD>
		<cfif getPeople.recordCount is not 0>
			The following people will be moved:<BR>
			<CFOUTPUT query="getPeople">#htmleditformat(firstName)# #htmleditformat(lastName)#<BR></CFOUTPUT>
		</cfif>
		</TD>
	</TR>
	<!--- *********************************************************************
	  we don't have any personIDs so we first need to search for them --->
	<CFIF frmPersonIDs eq "0">
		<CFFORM ACTION="movePersontoDiffOrg.cfm" METHOD="POST" ENABLECAB="No" NAME="personForm">
			<TR>
				<TD VALIGN="top" CLASS="label">PersonID</TD>
				<TD><CFINPUT TYPE="Text" NAME="frmPersonIDs" MESSAGE="You must put a valid personID" REQUIRED="Yes" SIZE="15"></TD>
			</TR>
			<TR>
				<TD>&nbsp;</TD>
				<TD><INPUT TYPE="submit" VALUE="Search"></TD>
			</TR>
		</CFFORM>
		
	<!--- *********************************************************************
	  we've got personID's but the getOrganisations has not been executed 
	  so we need the user to put in an orgName --->
	<CFELSEIF frmPersonIDs neq "0">
		<CFFORM ACTION="movePersontoDiffOrg.cfm" METHOD="POST" ENABLECAB="No" NAME="orgSearchForm">
			<TR>
				<TD VALIGN="top"><HR WIDTH="100%" SIZE="1" COLOR="steelblue"></TD>
			</TR>
			<TR>
				<TD NOWRAP>Enter the ID or the Name of the organisation to move them to:&nbsp;<CFINPUT TYPE="Text" NAME="frmSearchString" MESSAGE="You must put a valid personID" REQUIRED="Yes" SIZE="15">
				<BR>(you may enter the first part of name)
				</td>
			</tr>

			<TR>
				<TD>
				<INPUT TYPE="submit" NAME="frmSearch" VALUE="Search"></TD>
			</TR>
			<TR>
				<TD></TD>
			</TR>
			<CFOUTPUT><CF_INPUT TYPE="hidden" NAME="frmPersonIDs" VALUE="#frmPersonIDs#"></CFOUTPUT>
		</CFFORM>
	</CFIF>
	<!--- *********************************************************************
	  we've got personID's but the getOrganisations has not been executed 
	  so we need the user to put in an orgName --->
	<CFIF frmPersonIDs neq "0" and isDefined('getOrganisation.recordCount') and not isDefined('form.frmMove')>
		<cfif getOrganisation.recordCount gt 0>
			<CFFORM ACTION="movePersontoDiffOrg.cfm" METHOD="POST" NAME="orgSearchForm">
				<TR>
					<TD VALIGN="top"><HR WIDTH="100%" SIZE="1" COLOR="steelblue"></TD>
				</TR>
				<TR>
					<TD>Select which organisation and address to move them to:<BR>
					<CFSELECT NAME="frmIDs"
				          SIZE="15"
				          MESSAGE="You must select an organisation"
				          QUERY="getOrganisation"
				          VALUE="IDValue"
				          DISPLAY="displayValue"
				          REQUIRED="Yes"></CFSELECT>
					</TD>
				</TR>
				<TR>
					<TD ALIGN="right"><INPUT TYPE="submit" NAME="frmMove" VALUE="move">&nbsp;&nbsp;&nbsp;</TD>
				</TR>
				<CFOUTPUT><CF_INPUT TYPE="hidden" NAME="frmPersonIDs" VALUE="#frmPersonIDs#"></CFOUTPUT>
			</CFFORM>
		<cfelse>
			<TR>
					<TD>
					No matching Organisations found
					</TD>
				</TR>
			
		</cfif>
	</CFIF>
</cfif>
	
	<CFIF isDefined('message')>
		<TR>
			<TD VALIGN="top"><HR WIDTH="100%" SIZE="1" COLOR="steelblue"></TD>
		</TR>
		<TR>
			<TD><CFOUTPUT>#application.com.relayUI.message(message=message)#</CFOUTPUT></TD>
		</TR>
	</CFIF>
	
</TABLE>