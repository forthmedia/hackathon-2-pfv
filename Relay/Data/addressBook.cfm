<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			addressBook.cfm
Author:				KAP
Date started:		06-Nov-2003

Description:			

Called by QuickMailClient.cfm

E-mail address book popup

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
06-Nov-2003			KAP			Initial version

Possible enhancements:

--->
<!--- 
<cftry>
 --->
<cfscript>
comCommonQueries = CreateObject( "component", "relay.com.commonQueries" );
comCommonQueries.dataSource = application.siteDataSource;
comCommonQueries.getCurrentSystemUsers();
qCurrentSystemUsers = comCommonQueries.qCurrentSystemUsers;
</cfscript>

<cffunction name="udfAddressSelect" access="private" returntype="string" output="yes">
	<cfargument name="udfUserQuery" type="query" required="yes">

	<select name="frmAddressBook" size="20" multiple style="width: 200;">
	<cfloop query="udfUserQuery">
		<option value="<cfoutput>#HTMLEditFormat( udfUserQuery.email )#</cfoutput>"><cfoutput>#htmleditformat(udfUserQuery.displayvalue)#</cfoutput></option>
	</cfloop>
	</select>

	<cfreturn>
</cffunction>

<cffunction name="udfRecipientSelect" access="private" returntype="string" output="yes">
	<cfargument name="udfElementName" type="string" required="yes">
	<cfargument name="udfRecipientList" type="string" required="yes">

	<select name="<cfoutput>#htmleditformat(udfElementName)#</cfoutput>" size="6" multiple style="width: 200;">
	<cfloop index="udfRecipient" list="#udfRecipientList#">
		<option value="<cfoutput>#HTMLEditFormat( udfRecipient )#</cfoutput>"><cfoutput>#htmleditformat(udfRecipient)#</cfoutput></option>
	</cfloop>
	</select>

	<cfreturn>
</cffunction>

<cfparam name="url.recipientTo" type="string" default="">
<cfparam name="url.recipientCc" type="string" default="">
<cfparam name="url.recipientBcc" type="string" default="">


<cf_head>
<cf_title>Address Book</cf_title>
</cf_head>

<cf_body onload="eventWindowOnLoad(this);">

<table width="100%" border="0" cellspacing="0" cellpadding="3" align="center" bgcolor="white" class="withBorder">
	<tr>	
		<tr class="subMenu">
			<td align="left" class="subMenu">Address Book</TD>
			<td align="right" class="subMenu">
				<a href="javascript:void(loadOpenerForm());" class="subMenu">Confirm</A>
			</td>
		</tr>
	</tr>
</table>

<table width="100%" class="withBorder">

<form action="<cfoutput>#cgi["script_name"]#</cfoutput>" method="post" name="frmAddressBookForm">

<tr>
<td rowspan="3"><cfoutput>#udfAddressSelect( qCurrentSystemUsers )#</cfoutput></td>
<td align="center">
	<input type="button" name="frmButtonTo" value="To: &nbsp;&raquo;" onClick="eventButtonOnClick(this);">
	<br>
	<input type="button" name="frmButtonToRemove" value="&laquo;" onClick="eventButtonOnClick(this);">
</td>
<td><cfoutput>#udfRecipientSelect( "frmRecipientTo", url["recipientTo"] )#</cfoutput></td>
</tr>

<tr>
<td align="center">
	<input type="button" name="frmButtonCc" value="Cc: &nbsp;&raquo;" onClick="eventButtonOnClick(this);">
	<br>
	<input type="button" name="frmButtonCcRemove" value="&laquo;" onClick="eventButtonOnClick(this);">
</td>
<td><cfoutput>#udfRecipientSelect( "frmRecipientCc", url["recipientCc"] )#</cfoutput></td>
</tr>

<tr>
<td align="center">
	<input type="button" name="frmButtonBcc" value="Bcc: &nbsp;&raquo;" onClick="eventButtonOnClick(this);">
	<br>
	<input type="button" name="frmButtonBccRemove" value="&laquo;" onClick="eventButtonOnClick(this);">
</td>
<td><cfoutput>#udfRecipientSelect( "frmRecipientBcc", url["recipientBcc"] )#</cfoutput></td>
</tr>

<tr><td>&nbsp;</td></tr>

</form>
</table>
<SCRIPT type="text/javascript">
<!--
function eventButtonOnClick( callerObject )
{
	var optionIndex = 0;
	var targetElement = "";
	var targetOption;
	var removeRecipient = false;

	switch ( callerObject.name )
	{
		case "frmButtonTo" :
			targetElement = "frmRecipientTo";
			removeRecipient = false;
			break;
		case "frmButtonCc" :
			targetElement = "frmRecipientCc";
			removeRecipient = false;
			break;
		case "frmButtonBcc" :
			targetElement = "frmRecipientBcc";
			removeRecipient = false;
			break;
		case "frmButtonToRemove" :
			targetElement = "frmRecipientTo";
			removeRecipient = true;
			break;
		case "frmButtonCcRemove" :
			targetElement = "frmRecipientCc";
			removeRecipient = true;
			break;
		case "frmButtonBccRemove" :
			targetElement = "frmRecipientBcc";
			removeRecipient = true;
			break;
		default :
			targetElement = "frmRecipientBcc";
	}

	with ( callerObject.form )
	{
		if ( removeRecipient )
		{
			for ( optionIndex = elements[targetElement].options.length - 1; optionIndex >= 0; optionIndex-- )
				if ( elements[targetElement].options[optionIndex].selected ) elements[targetElement].options[optionIndex] = null;
		}
		else
		{
			for ( optionIndex = 0; optionIndex < elements["frmAddressBook"].options.length; optionIndex++ )
			{
				if ( elements["frmAddressBook"].options[optionIndex].selected )
				{
					elements["frmAddressBook"].options[optionIndex].selected = false;
					targetOption = new Option();
					targetOption.value = elements["frmAddressBook"].options[optionIndex].value;
					targetOption.text = elements["frmAddressBook"].options[optionIndex].text;
					elements[targetElement].options[elements[targetElement].options.length] = targetOption;
				}
			}
		}
	}
	return true;
}

function loadOpenerForm()
{
	var argumentArray = new Array();
	var recipientType = new Array( "frmRecipientTo", "frmRecipientCc", "frmRecipientBcc" );
	var recipientIndex = 0;
	var optionIndex = 0;
	var listDelimiter = "";

	for( recipientIndex = 0; recipientIndex < recipientType.length; recipientIndex++ )
	{
		argumentArray[recipientIndex] = "";
		listDelimiter = "";
		with ( document.forms["frmAddressBookForm"] )
		for ( optionIndex = 0; optionIndex < elements[[recipientType[recipientIndex]]].options.length; optionIndex++ )
		{
			argumentArray[recipientIndex] += listDelimiter;
			argumentArray[recipientIndex] += elements[[recipientType[recipientIndex]]].options[optionIndex].value;
			listDelimiter = ",";
		}
	}
	
	opener.loadForm( argumentArray[0], argumentArray[1], argumentArray[2] );
	opener.focus();
	self.close();
	return true;
}

function eventWindowOnLoad( $_caller )
{
	var windowHeight = 320;
	var windowWidth = 500;
	with ( $_caller )
	{
		resizeTo( windowWidth, windowHeight );
		moveTo( ( screen.availWidth - windowWidth ) / 2, ( screen.availHeight - windowHeight ) / 2 );
		focus();
	}
	return true;
}

//-->
</script>



<!--- 
<cfcatch type="any">
<cfdump var="#cfcatch#">
</cfcatch>
</cftry>
 --->
