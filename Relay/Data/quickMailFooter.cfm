<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		quickMailFooter.cfm
Author:			Simon WJ
Date created:	

	Objective - provides a standard footer for quickMail
		
	Syntax	  -	should only be included by quickMail client
	
	Parameters - It requires addedCommDetailID (this is set by addCommDetail custom 
				tag in the QuickMailClient template
				
	Return Codes - none

Amendment History:

DD-MMM-YYYY			Initials 	What was changed
		SWJ
2012/03/07			RMB			Added phase "phr_sys_emailfooterHTML" to append an email footer for the current users county
2014-01-31	WAB 	removed references to et cfm, can always just use / or /?

Enhancement still to do:



--->
<cfif fileexists("#application.paths.code#\cftemplates\quickMailFooter.cfm")>
	<cfinclude template="/code/cftemplates/quickMailFooter.cfm">
<cfelse>

	<CFQUERY NAME="getPersonDetails" datasource="#application.siteDataSource#">
		select p.email, p.firstName, p.lastName, o.organisationName, 
			p.OfficePhone, p.mobilePhone, l.telephone
		from person p 
			inner join organisation o ON p.organisationID = o.organisationID 
		inner join location l ON p.locationID = l.locationID
		where personID = #request.relayCurrentUser.personid#
	</CFQUERY>

	<CFOUTPUT QUERY="getPersonDetails" MAXROWS=1>
	
		<HR ALIGN="left" WIDTH="400" SIZE="1">
		<font face="Tahoma" size="2">#htmleditformat(Firstname)# #htmleditformat(LastName)#<BR>
		<STRONG>#htmleditformat(organisationName)#</STRONG><BR>
		<CFIF Trim(OfficePhone) neq "">Direct: #htmleditformat(Trim(OfficePhone))#<BR></CFIF>
		<CFIF Trim(mobilePhone) neq "">Mobile: #htmleditformat(Trim(mobilePhone))#<BR></CFIF>
		<CFIF Trim(telephone) neq "">Switchboard: #htmleditformat(Trim(telephone))#<BR></CFIF>
		<CFIF Trim(email) neq "">Mailto: <A href="mailto:#email#">#htmleditformat(email)#</A><BR></CFIF>
		
		<!--- WAB 2010/10/12 removed reciprocal domain and replaced with relaycurrentsite function
		<CF_ReciprocalDomain userType="internal">
		--->
		
		<A HREF="#application.com.relayCurrentSite.getReciprocalExternalProtocolAndDomain()#/?eid=333&pid=#frmPersonID#">#htmleditformat(application.theClientURL)#</A></FONT>
		</FONT> <BR>
		<cfif isDefined('thisCommID')>
			<!--- this will only be defined if a contact history record is being added --->
			<IMG SRC="#application.com.relayCurrentSite.getReciprocalExternalProtocolAndDomain()#/calltemplate.cfm?template=relayTagTemplates/mailreadadHoc.cfm&cid=#addedCommDetailID#&pid=#frmPersonID#" WIDTH=1 HEIGHT=1>
		</cfif>
	</CFOUTPUT>
	
	<!--- 2012/03/07 - RMB - Added phase "phr_sys_emailfooterHTML" to append an email footer for the current users country --->
	<cf_translate onNullShowPhraseTextID=false>
	phr_sys_emailfooterHTML
	</cf_translate>
	
</cfif>

