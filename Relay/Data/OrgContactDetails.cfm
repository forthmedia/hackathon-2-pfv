<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			opportunityTelNumbers.cfm	
Author:				MDC
Date started:		2004-03-04
	
Description:			

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:
WAB 2010/10/13  removed get TextPhrases and replaced with CF_translate

 --->
 

<cfparam name="opportunityID" type="numeric" default="0">


<cfquery name="GetLocTelNumbers" datasource="#application.sitedatasource#">
	select l.siteName as siteName, c.countryDescription as countryDescription, 
	l.telephone as Telephone
	from organisation o
	join location l on l.organisationid = o.organisationid
	join country c on c.countryid = l.countryid
	where o.organisationid =  <cf_queryparam value="#organisationid#" CFSQLTYPE="CF_SQL_INTEGER" > 
	and l.telephone not in ('')
	order by c.countrydescription, l.locationid
</cfquery>

<cfquery name="GetPerTelNumbers" datasource="#application.sitedatasource#">
	select p.firstname + ' ' + p.lastname as Name, c.countryDescription as countryDescription, 
	p.officephone as Telephone
	from organisation o
	join person p on p.organisationid = o.organisationid
	join location l on l.locationid = p.locationid
	join country c on c.countryid = l.countryid
	where o.organisationid =  <cf_queryparam value="#organisationid#" CFSQLTYPE="CF_SQL_INTEGER" > 
	and p.officephone not in ('')
	order by c.countrydescription, name
</cfquery>

<cf_translate>


<cf_title><cfoutput>Phr_Sys_Organisation Phr_OrgContactDetails</cfoutput></cf_title>


<TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorderSidesOnly">
	<TR CLASS="Submenu"><td><font color="#FFFFFF"><b><cfoutput>Phr_OrgContactDetails</cfoutput></b></font></td></TR>
</table>				
<cfif GetLocTelNumbers.recordcount eq 0 and GetPerTelNumbers.recordcount eq 0>
	<cfoutput><p>Phr_Sys_NoContactDetails</p></cfoutput>
</cfif>


<cfif GetLocTelNumbers.recordcount gt 0>
	<table width="100%">
			<cfoutput><tr><th>Phr_Sys_Site</th><th>Phr_Sys_Country</th><th>Phr_Sys_Switchboard</th></tr></cfoutput>
		<cfoutput query="GetLocTelNumbers">
			<tr<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
				<td>
					#htmleditformat(siteName)#
				</td>
				<td>
					#htmleditformat(countryDescription)#
				</td>
				<td>
					#htmleditformat(Telephone)#
				</td>
			</tr>	
		</cfoutput>
	</table>
</cfif>

<cfif GetPerTelNumbers.recordcount gt 0>
	<table width="100%">
			<cfoutput><tr><th>Phr_Sys_Person</th><th>Phr_Sys_Country</th><th>Phr_Sys_DirectLine</th></tr></cfoutput>
		<cfoutput query="GetPerTelNumbers">
			<tr<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
				<td>
					#htmleditformat(Name)#
				</td>
				<td>
					#htmleditformat(countryDescription)#
				</td>
				<td>
					#htmleditformat(Telephone)#
				</td>
			</tr>	
		</cfoutput>
	</table>
</cfif>





</cf_translate>
