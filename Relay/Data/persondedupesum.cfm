<!--- �Relayware. All Rights Reserved 2014 --->
<!--- amendment History
	1999-04-13	WAB Added fields required for search by first and last name --->

<!--- user rights to access:
		SecurityTask:dataTask
		Permission: Add
---->

<!--- Amendment History
1999-04-08	WAB	Added link back to search results/loc list 

2008/10/16  NJH  T-10 Bug Fix Issue 863 Refresh the person list after the merge is complete
--->

<CFIF NOT checkPermission.AddOkay GT 0>
	<!--- if frmLocationID is 0 then task is to add
	      but the user doesn't have add permissions --->

	<CFSET message="You are not authorised to use this function">
		<CFINCLUDE TEMPLATE="datamenu.cfm">
	<CF_ABORT>

</CFIF>



<CFPARAM NAME="PersonssDel" Default="">


<CFIF NOT IsDefined("message")>

	<!--- remove any 0s from list --->
	
	<CFLOOP CONDITION="ListFind(frmDupPeople,0) IS NOT 0">
		<CFSET frmDupPeople = ListDeleteAt(frmDupPeople,ListFind(frmDupPeople,0))>
	</CFLOOP>

	<CFSET message = "All records (i.e. job function, selections) previously linked to person record(s) #PersonsDel# are now linked to person record #frmDupPeople#.<br>Please check person record #frmDupPeople# for inconsistent flags.">
</CFIF>




<cf_head>
<cf_title><CFOUTPUT>#request.CurrentSite.Title#</CFOUTPUT></cf_title>
<SCRIPT type="text/javascript">
<!--
	function goTo(a,b) {
		var form = document.DedupeFlags;
		form.frmGoTo.value = a;
		form.frmNext.value = b;
		form.submit();
	}

//-->
</SCRIPT>
</cf_head>

<cfset application.com.request.setTopHead(pageTitle="Person Records Merged")>

<CENTER>

<CFOUTPUT>
	
#application.com.relayUI.message(message=message)#
	
<TABLE BORDER="0" CELLSPACING="5" CELLPADDING="5">
<CFIF IsDefined("Cookie.LFLAGSCHECKED")>
	<CFIF ListFirst(Cookie.LFLAGSCHECKED,"-") IS "no" AND ListGetAt(Cookie.LFLAGSCHECKED, 2, "-") IS NOT 0>
	<TR>
		<TD VALIGN=TOP ALIGN="CENTER" colspan="2">
			<A HREF="JavaScript:goTo('locflags','persondedupesum');" CLASS="Button">Check Profile Data</A>
		</TD>
	</TR>
	</CFIF>
</CFIF>
<CFIF IsDefined("Cookie.PFLAGSCHECKED") and ListFirst(Cookie.PFLAGSCHECKED,"-") IS "no">
	<TR>
		<TD VALIGN=TOP ALIGN="CENTER" colspan="2">
			<A HREF="JavaScript:goTo('personflags','persondedupesum');">Check Profile Data</A>
		</TD>
	</TR>
</CFIF>

<!--- 	<TR>
		<TD VALIGN=TOP ALIGN="CENTER" colspan="2">
		<A HREF="JavaScript:goTo('loclist','');" CLASS="Button">Return to location list</A>
		</TD>
	</TR>
 --->
 	<CFIF IsDefined("frmLocationID")>
		<TR>
			<TD VALIGN=TOP ALIGN="CENTER" colspan="2">
				<A HREF="JavaScript:goTo('persList','');" id="returnToPeopleLink" style="visibility:visible">Return to people list</A>
				<cfif not isDefined("frmRadioName")>
					<input type=button value="Close Window"  id="closeWindowButton" style="visibility:hidden" onClick="javascript:window.opener.location.reload(true); if (window.opener.progressWindow) { window.opener.progressWindow.close() } ;self.close();">
				<cfelse>
					<input type=button value="Close Window"  id="closeWindowButton" style="visibility:hidden" onClick="javascript:self.close();">
				</cfif>
			</TD>
		</TR>
	</CFIF>

</TABLE>
</CFOUTPUT>

<FORM ACTION="personchoice.cfm" METHOD="POST" NAME="DedupeFlags">
	<CFINCLUDE template="searchparamdefaults.cfm">
	<CFPARAM NAME="frmStart" DEFAULT="1">	
	<CFPARAM NAME="frmStartp" DEFAULT="1">	
	<CFOUTPUT>
	<CF_INPUT TYPE="HIDDEN" NAME="frmDupLocs" VALUE="#frmDupLocs#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmDupPeople" VALUE="#frmDupPeople#">
	<CFIF IsDefined("frmLocationID")>
		<CF_INPUT TYPE="HIDDEN" NAME="frmLocationID" VALUE="#frmLocationID#">
	<CFELSE>
		<INPUT TYPE="HIDDEN" NAME="frmLocationID" VALUE="0">
	</CFIF>
	
	<CFIF IsDefined("frmOrganisationID")>
		<CF_INPUT TYPE="HIDDEN" NAME="frmOrganisationID" VALUE="#frmOrganisationID#">
	<CFELSE>
		<INPUT TYPE="HIDDEN" NAME="frmOrganisationID" VALUE="0">
	</CFIF>

	<CFINCLUDE template="searchparamform.cfm">
	
	<CF_INPUT TYPE="HIDDEN" NAME="frmStart" VALUE="#frmStart#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmStartp" VALUE="#frmStartp#">
	<INPUT TYPE="HIDDEN" NAME="frmGoTo" VALUE="">
	<INPUT TYPE="HIDDEN" NAME="frmNext" VALUE="datamenu">
	</CFOUTPUT>
</FORM>


<!--- NJH 2008/10/16 T-10 Bug Fix Issue 863 Refresh the person list after the merge is complete --->
<cfoutput>
	<script type="text/javascript">
		if (!window.opener.closed){
			window.opener.personDedupeCompleted();
		}
	</script>
</cfoutput>


</CENTER>

<script>
	// if this is called in a window hide the back button
	if(opener){ 
		var thisButtonVar = document.getElementById('closeWindowButton');
		thisButtonVar.style.visibility = "visible";
		var thisLinkVar = document.getElementById('returnToPeopleLink');
		thisLinkVar.style.visibility = "hidden";
	}
</script>

<CF_ABORT>