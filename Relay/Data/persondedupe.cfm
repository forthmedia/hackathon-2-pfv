<!--- �Relayware. All Rights Reserved 2014 --->
<!---

 	user rights to access:
		SecurityTask:dataTask
		Permission: Update

amendment History
1999-04-13	WAB Added fields required for search by first and last name
1.01	2001-08-16	SWJ 	Modified to use new look n feel
2004-08-26 NM Added frm defaults for users coming from new dedupe tool.
2007-06-01 WAB
2007-08-03		SWJ		Modified the screen so that the user can now choose either record to keep
2008-09-24	PPB		Allow user to choose which Flags to keep during dedupe
		2009/04/30  WAB/SSS CR-LEX588   DedupeProtection .  See comments in orgdedupe.cfm
2009/06/02  WAB  	LID 2308  conflicting flags query not taking entityTypeID into account
---->

<cfsetting requesttimeout="600" >


<cfif isDefined("frmRadioName")>
	<!--- WAB 2007-06-01 removed whole list of params here and included template which does the job (an allows new ones to be added easily), a few items not in the include were left --->
	<cfinclude template="searchParamDefaults.cfm">
	<cfparam NAME="frmStart" default="">
	<cfparam NAME="frmStartP" default="">
	<cfparam NAME="frmStop" default="">
	<cfparam NAME="frmLocationID" default="">
</cfif>


<CFIF NOT checkPermission.UpdateOkay GT 0>
	<!--- if frmLocationID is 0 then task is to Update but the user doesn't have Update permissions --->
	<CFSET message="You are not authorised to use this function">
	<CFINCLUDE TEMPLATE="datamenu.cfm">
	<CF_ABORT>
</CFIF>


<cfif listlen(#frmDupPeople#) lt 2>
	<P>You must select at least two people to start the dedupe process.</p>
	<a href="JavaScript:window.close();">Close window</a>
	<CF_ABORT>
</cfif>


<CFPARAM NAME="PersonsDel" Default="">
<!--- the application .cfm checks for read dataTask privleges--->
<cfinclude template="/templates/qrygetcountries.cfm">
<!--- qrygetcountries creates a variable CountryList --->


<!--- identify the internal system users so they can be excluded as we don't want to merge them --->
<CFQUERY NAME="getUsers" DATASOURCE=#application.SiteDataSource#>
	SELECT DISTINCT PersonID
  FROM UserGroup
	WHERE PersonID  IN ( <cf_queryparam value="#frmDupPeople#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
</CFQUERY>


<!--- Get list of users from query ABOVE "getNonUsers" --->
<CFQUERY NAME="getPers" DATASOURCE=#application.SiteDataSource#>
	SELECT *, isnull(firstname,' ') +   ' ' + isnull(lastname,' ') as fullname FROM Person  <!--- WAB 2009/05/05 added fullname for feedback --->
	WHERE PersonID  IN ( <cf_queryparam value="#frmDupPeople#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	<CFIF getUsers.RecordCount IS NOT 0>
		AND PersonID  NOT IN ( <cf_queryparam value="#ValueList(getUsers.PersonID)#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	</CFIF>
	ORDER BY personID ASC
</CFQUERY>


<!--- List of languages for drop down list --->
<CFQUERY NAME="getLanguages" DATASOURCE="#application.SiteDataSource#">
	SELECT Language AS qryLanguage
	FROM Language
	ORDER BY Language
</CFQUERY>


<CFIF getPers.RecordCount GTE 2>
	<!--- WAB 2009/04/30  dedupeProtection, These two functions will tell us if any or either of the elements are merge protected--->
	<cfset mergeCheck = arrayNew(1)>
	<cfset  mergeCheck[1]  = application.com.dedupe.checkForMergeProtection(entityType=0,entityID =getPers.personid[1] )>
	<cfset mergeCheck[2] = application.com.dedupe.checkForMergeProtection(entityType=0,entityID =getPers.personid[2] )>
</cfif>


<CFINCLUDE template="dedupe_functions.cfm">


<cf_title>
	<CFOUTPUT>#request.CurrentSite.Title#</CFOUTPUT>
</cf_title>


<CFIF getPers.RecordCount GTE 2>
	<cf_includejavascriptonce template = "/data/js/dedupe.js">
</CFIF>


<CFIF getPers.RecordCount GTE 2>
	<!--- SSS 2009/05/01 this will stop the process running if all items selected are merge protected --->
	<cfif mergeCheck[1].isMergeProtected and mergeCheck[2].isMergeProtected>

		<cfoutput>
			<FORM NAME="noDedupe" ACTION="" METHOD="post">
				<table>
					<tr>
						<td>
							<div class="errorblock">
								#getPers.fullname[1]# has profile called #mergeCheck[1].detail.flagname[1]# set<BR><br>
								#getPers.fullname[2]# has profile called #mergeCheck[2].detail.flagname[1]# set<BR><br>
								These people cannot be merged
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<input type="button" class="" value="Close Window" onClick="JavaScript:window.close();">
						</td>
					</tr>
				</table>
			</form>
			<CF_ABORT>
		</cfoutput>

		<cfelseif not mergeCheck[1].isMergeProtected and not mergeCheck[2].isMergeProtected>
			<!--- 2009/05/01 SSS if neither item is merge protected do not disable the buttons--->
			<cfset noMergeProtectionSet = true>
		</cfif>

		<cfset application.com.request.setTopHead(pageTitle="Merge Person Records")>

		<!--- <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
			<TR>
				<TD ALIGN="LEFT" CLASS="submenu"VALIGN="top">
					Merge Person Records</DIV>
				</TD>

				<TD ALIGN="RIGHT" VALIGN="top" CLASS="submenu">
					<A id="runInPopUp" style="visibility:visible" HREF="JavaScript:history.back();" Class="Submenu">Back</A>
				</TD>
			</TR>
		</table> --->

		<CFSET strow = getPers.RecordCount - 1>
		<CFSET enrow = getPers.RecordCount>

		<div class="container" id="mergePeople">
			<!--- MAIN ROW --->
			<div class="row">

				<CFSET count = 0>

				<CFLOOP QUERY="getPers" STARTROW=#strow# ENDROW=#enrow#>

					<CFSET count = count + 1>
					<CFSET personLang = Language>
					<CFIF count IS 2>
						<CFQUERY NAME="getDataSources" DATASOURCE=#application.SiteDataSource#>
							<!--- SELECT pd.DataSourceID & '#application.delim1#' & pd.RemoteDataSourceID AS DataSource  see line 170 for updated code --->
							SELECT pd.DataSourceID AS DataSourcea, pd.RemoteDataSourceID AS DataSourceb
							  FROM Person AS p, DataSource AS d, PersonDataSource AS pd
							 WHERE d.DataSourceID = pd.DataSourceID
							   AND pd.PersonID = p.PersonID
							   AND p.PersonID =  <cf_queryparam value="#PersonID#" CFSQLTYPE="CF_SQL_INTEGER" > <!--- 442074 --->
						  ORDER BY d.Description
						</CFQUERY>
						<CFSET allsources = "">
						<CFLOOP QUERY="getDataSources">
							<CFSET allsources = ListAppend(allsources, "#DataSourcea##application.delim1##DataSourceb#", Chr(135))>
						</CFLOOP>
					<CFELSE>
						<CFSET frmPersonID1 = PersonID>
					</CFIF>
					<CFOUTPUT>
							
						<CFIF count eq 1>
							<div class="col-xs-7">
						<CFELSE>
							<div class="col-xs-5">
						</CFIF>
							<FORM NAME="#IIF(count IS 1, DE("formA"), DE("formB"))#" ACTION="Persondedupetask.cfm" METHOD="post">
					</CFOUTPUT>

					<!---
					PPB 2008-09-29
					the left side of the is screen is populated in the first loop (count=1) followed by the right hand side in the second loop (count=2);
					while populating the left hand side the currentEntity is that on the left and the otherEntity is that on the right... and vice versa;
					--->
					<CFIF count eq 1>
						<CFSET currentEntityId = getPers.PersonID[1]>
						<CFSET otherEntityId = getPers.PersonID[2]>
					<CFELSE>
						<CFSET currentEntityId = getPers.PersonID[2]>
						<CFSET otherEntityId = getPers.PersonID[1]>
					</CFIF>
					<CFSET EntityTypeId = 0>  <!--- WAB LID 2308 2009/05/02 needed for conflicting flags query --->

					<!--- <CFINCLUDE template="searchparamform.cfm"> --->
					<CFOUTPUT>
						<CF_INPUT TYPE="HIDDEN" NAME="frmDupPeople" VALUE="#ValueList(getPers.PersonID)#">
						<!--- <INPUT TYPE="HIDDEN" NAME="frmStart" VALUE="#frmStart#">
						<INPUT TYPE="HIDDEN" NAME="frmStartp" VALUE="#frmStartp#">
						<INPUT TYPE="HIDDEN" NAME="frmLocationID" VALUE="#frmLocationID#"> --->
						<CF_INPUT TYPE="HIDDEN" NAME="PersonsDel" VALUE="#Variables.PersonsDel#">
						<CF_INPUT TYPE="Hidden" NAME="PersonIDdel" VALUE="#otherEntityId#"><!--- Record to delete --->
						<CF_INPUT TYPE="Hidden" NAME="PersonIDsave" VALUE="#currentEntityId#"><!--- Record to save --->
						<CF_INPUT TYPE="Hidden" NAME="FreezeTime" VALUE="#CreateODBCDateTime(request.requestTime)#"><!--- Freeze Time --->
						<cfif isDefined("frmRadioName")>
							<CF_INPUT type="hidden" name="frmRadioName" value="#frmRadioName#">
							<CF_INPUT type="hidden" name="frmRadioAmount" value="#frmRadioAmount#">
						</cfif>
					</CFOUTPUT>

					<div class="row">
						<CFIF count IS 1>
							<div class="col-xs-3">
								<p class="label">Person ID:</p>
							</div>
							<div class="col-xs-9">
								<CFOUTPUT>
									<p class="">#getPers.PersonID[count]#</p>
								</CFOUTPUT>
							</div>
						<CFELSE>
							<div class="col-xs-12">
								<CFOUTPUT>
									<p class="secondPersonId">#getPers.PersonID[count]#</p>
								</CFOUTPUT>
							</div>
						</CFIF>
					</div>


					<!--- ==============================================================================
								SWJ  03-08-2007  Added this query sim to control field list & attributes
					=============================================================================== --->
					<cfoutput>
						<cf_querySim>
							getColumns
							label,name,fieldValue,size,maxlength
							phr_sys_Salutation|insSalutation|getPers.salutation[#htmleditformat(count)#]|30|30
							phr_sys_Firstname|insFirstName|getPers.firstName[#htmleditformat(count)#]|30|30
							phr_sys_Lastname|insLastName|getPers.LastName[#htmleditformat(count)#]|30|30
							phr_sys_HomePhone|insHomePhone|getPers.HomePhone[#htmleditformat(count)#]|30|30
							phr_sys_directLine|insOfficePhone|getPers.OfficePhone[#htmleditformat(count)#]|30|30
							phr_sys_Mobile|insMobilePhone|getPers.MobilePhone[#htmleditformat(count)#]|30|50
							phr_sys_Fax|insFaxPhone|getPers.FaxPhone[#htmleditformat(count)#]|30|30
							phr_sys_Email|insEmail|getPers.Email[#htmleditformat(count)#]|30|50
							phr_sys_JobDesc|insJobDesc|getPers.JobDesc[#htmleditformat(count)#]|30|50
						</cf_querySim>
					</cfoutput>

					<cfoutput query="getColumns">
						
						<div class="row">

								<CFIF count IS 1>
									<div class="col-xs-3">
										<p class="label">#htmleditformat(label)#</p>
								<cfelse>
									<div class="col-xs-3">
										<CF_INPUT type="button" class="btn btn-primary" value=" >> " onClick="JavaScript:keepinfo('#htmleditformat(name)#')">
								</cfif>
									</div>

								<CFIF count IS 1>
									<div class="col-xs-7">
								<cfelse>
									<div class="col-xs-9">
								</cfif>
										<CF_INPUT class="form-control" TYPE="Text" NAME="#name#" VALUE="#evaluate(fieldValue)#" SIZE="#size#" MAXLENGTH="#maxlength#">
							</div>
							<cfif count IS 1>
								<div class="col-xs-2">
									<CF_INPUT type="button" class=" form-control" value=" << " onClick="JavaScript:keepinfoLeft('#name#')">
								</div>
							</cfif>
						</div>
					</cfoutput>

					<cfoutput>

						<div class="row">
								<CFIF count IS 1>
									<div class="col-xs-3">
									<p class="label">Language:</p>
								<CFELSE>
									<div class="col-xs-3">
									<input type="button" class="btn btn-primary" value=" >> " onClick="JavaScript:keepinfo('insLanguage')">
								</CFIF>
							</div>
							<CFIF count IS 1>
								<div class="col-xs-7">
							<cfelse>
								<div class="col-xs-9">
							</cfif>
								<SELECT NAME="insLanguage" class="form-control">
									<OPTION VALUE="">Default
									<!--- saved the person's language as a different variables since #Language#
									comes up as "e".  I think it has to do with Language being a field in
									getLanguages and getPerson
									NJH 2016/09/22 JIRA PROD2016-2362 - changed from insCountryId to insLanguageID
									--->
									<cfloop QUERY="getLanguages">
										<OPTION VALUE="#qryLanguage#"#IIf(CompareNoCase(qryLanguage,Variables.personLang) IS 0,DE(" SELECTED"),DE(""))#>#htmleditformat(qryLanguage)#
									</cfloop>
								</SELECT>
							</div>
							<cfif count IS 1>
								<div class="col-xs-2">
									<input type="button" class="btn btn-primary" value=" << " onClick="JavaScript:keepinfoLeft('insLanguage')">
								</div>
							</cfif>
						</div>

						<!--- <div class="row">
							<div class="col-xs-3">
								<CFIF count IS 1>
									<p>Notes:</p>
								<CFELSE>
									<A class="Button" HREF="JavaScript:keepinfo('insNotes');"></A>
								</CFIF>
							</div>
							<div class="col-xs-8">
								<TEXTAREA NAME="insNotes" COLS="24" ROWS="4" WRAP="VIRTUAL">#HTMLEditFormat(Notes)#</TEXTAREA>
							</div>
						</div> --->

						<div class="row">
							

								<CFIF count IS 1>
									<div class="col-xs-3">
										<p class="label">Active?</p>
									</div>
									<div class="col-xs-9">
										<div class="radio">
											<label class="radio-inline">
												<CF_INPUT TYPE="radio" NAME="insActive" VALUE="1"  CHECKED="#Active#" class=""> Yes
											</label>
										</div>
										<div class="radio">
											<label class="radio-inline radiovalno">
												<CF_INPUT TYPE="radio" NAME="insActive" VALUE="0"  CHECKED="#iif(not Active,true,false)#" class=""> No
											</label>
										</div>
									</div>

								<cfelse>


									<div class="col-xs-3">
										
									</div>
									<div class="col-xs-9">
										<div class="radio">
											<label class="radio-inline">
												<CF_INPUT TYPE="radio" NAME="insActive" VALUE="1"  CHECKED="#Active#" class=""> Yes
											</label>
										</div>
										<div class="radio">
											<label class="radio-inline radiovalno">
												<CF_INPUT TYPE="radio" NAME="insActive" VALUE="0"  CHECKED="#iif(not Active,true,false)#" class=""> No
											</label>
										</div>
									</div>

								</CFIF>
													
						</div>

						<!--- PPB 2008-09-29: not on spec for EMEA 1.1.3 so assumed no loger required --->
						<!--- <div class="row">
							<div class="col-xs-3">
								<CFIF count IS 1>
									<p>More Details</p>
								</CFIF>
							</div>
							<div class="col-xs-8">
								<cfoutput>
									<a href="javascript:viewentityFlags(#getPers.PersonID[count]#,0,'personDetail','perProfileSummary');">View</a>
								</cfoutput>
							</div>
						</div> --->


						<!--- 	PPB 2008-09-29: CONFLICTING FLAGS --->
						<CFINCLUDE template="dedupe_displayConfictingFlags.cfm">

						<div class="row buttonsRow">


							<CFIF count IS 1>
							<div class="col-xs-3">
							</div>
								<div class="col-xs-9">
									<INPUT TYPE="RESET" VALUE="phr_reset" class="btn btn-primary">
									<!--- <A HREF="JavaScript:document.formB.reset();"><IMG SRC="/images/buttons/c_resetrec_e.gif" WIDTH=146 HEIGHT=21 BORDER=0 ALT="Reset Record"></A> --->
									<input name="KeepThisOne" type="button" class="btn btn-primary" value="phr_sys_Keep_This_One" onClick="savededup(#count#)" <cfif not isdefined("noMergeProtectionSet")><cfif mergeCheck[#count#].isMergeProtected EQ 0>disabled</cfif></cfif>>
								</div>
							<cfelse>
								<div class="col-xs-3">
								</div>
								<div class="col-xs-9">
									<INPUT TYPE="RESET" VALUE="phr_reset" class="btn btn-primary">
									<!--- <A HREF="JavaScript:document.formB.reset();"><IMG SRC="/images/buttons/c_resetrec_e.gif" WIDTH=146 HEIGHT=21 BORDER=0 ALT="Reset Record"></A> --->
									<input name="KeepThisOne" type="button" class="btn btn-primary" value="phr_sys_Keep_This_One" onClick="savededup(#count#)" <cfif not isdefined("noMergeProtectionSet")><cfif mergeCheck[#count#].isMergeProtected EQ 0>disabled</cfif></cfif>>
								</div>
							</cfif>


							<!--- <div class="col-xs-9">
								<INPUT TYPE="RESET" VALUE="phr_reset" class="btn btn-primary">
								<!--- <A HREF="JavaScript:document.formB.reset();"><IMG SRC="/images/buttons/c_resetrec_e.gif" WIDTH=146 HEIGHT=21 BORDER=0 ALT="Reset Record"></A> --->
								<input name="KeepThisOne" type="button" class="btn btn-primary" value="phr_sys_Keep_This_One" onClick="savededup(#count#)" <cfif not isdefined("noMergeProtectionSet")><cfif mergeCheck[#count#].isMergeProtected EQ 0>disabled</cfif></cfif>>
							</div> --->

						</div>

						<div class="row">
							<div class="col-xs-12">
								<cfoutput>
									<!--- 2009/05/01 SSS message added to tell you why a button has been disabled and which flag is stopping you from mergeing this record--->
								  <cfif mergeCheck[#count#].isMergeProtected>
									 	<div class="infoblock">
											Profile '#mergeCheck[count].detail.flagname[1]#' is set.<BR>
										 	This person must be kept.
										</div>
									</cfif>
								</cfoutput>
							</div>
						</div>
					</FORM>
				</div> <!--- / MAIN COL(s) --->
			</cfoutput>
		</CFLOOP>
	</div> <!--- / MAIN ROW --->
</div> <!--- / CONTAINER --->

<CFELSE>
	<!--- less than 2 non-users --->
	<CFIF IsDefined("dedupdone")>
		<P>There are no more person records to dedupe from your selection.</p>
	<CFELSE>
		<BR>
		<P>You must select at least two people who are not users to start the dedupe process.   You either selected less than two people or there are less than two people in your selection who are not users.</p>
	</CFIF>
</CFIF>


<script>
	// if this is called in a window hide the back button
	if(opener){
		var thisVar = document.getElementById('runInPopUp');
		thisVar.style.visibility = "hidden";
	}
</script>

<FORM ACTION="entityScreens.cfm" METHOD="post" NAME="flagListForm" TARGET="_blank">
	<INPUT TYPE="hidden" NAME="frmcurrentEntityID" VALUE="0">
	<INPUT TYPE="hidden" NAME="frmEntityType" VALUE="0">
	<INPUT TYPE="hidden" NAME="thisScreenTextID" VALUE="0">
	<INPUT TYPE="hidden" NAME="frmBack" VALUE="0">
	<INPUT TYPE="hidden" NAME="frmNext" VALUE="0">
	<INPUT TYPE="hidden" NAME="selIndex" VALUE="">
</FORM>

<CF_ABORT>