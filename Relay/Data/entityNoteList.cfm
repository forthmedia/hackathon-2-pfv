<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			entityNoteList.cfm	
Author:				SWJ
Date started:		2003-05-24
	
Purpose:	To provide an inline list of notes

Usage:	

<cfmodule template="/data/entityNoteList.cfm" frmEntityType="Organisation" frmEntityID="#frmOrgID#" frmEntityTypeID="2">

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->
<cf_translate>
<CFIF not isDefined("attributes.frmEntityID")>
	<cfexit method="EXITTAG">
	<cfset caller.message = "frmEntityID must be defined">
</CFIF>
	
<CFIF not isDefined("attributes.frmEntityTypeID")>
	<cfexit method="EXITTAG">
	<cfset caller.message = "frmEntityTypeID must be defined">
</CFIF>

<CFSET supportedEntityTypes = "0,1,2,23">
<CFIF listFind(supportedEntityTypes,attributes.frmEntityTypeID) eq 0>
	<cfexit method="EXITTAG">
	<cfset caller.message = "You cannot currently add a note to entityType #frmEntityTypeID#">
</CFIF>

<cfparam name="entityNoteID" type="numeric" default="0">
	
<CFQUERY NAME="getNotes" datasource="#application.siteDataSource#">
	select * from entityNote
	Where entityid =  <cf_queryparam value="#attributes.frmentityid#" CFSQLTYPE="CF_SQL_INTEGER" > 
	and entitytypeid =  <cf_queryparam value="#attributes.frmentitytypeid#" CFSQLTYPE="CF_SQL_INTEGER" > 
	order by created desc
</CFQUERY>

<cfif isDefined("entityNoteID") and entityNoteID gt 0>
	<CFQUERY NAME="loadNote" datasource="#application.siteDataSource#">
		select * from entityNote where entityNoteID =  <cf_queryparam value="#entityNoteID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</CFQUERY>
<cfelse>
	<CFQUERY NAME="loadNote" datasource="#application.siteDataSource#">
		select 0 as entityNoteID, '' as notetext, #attributes.frmEntityID# as entityID, #attributes.frmEntityTypeID# as entityTypeID, '' as noteType
	</CFQUERY>
</cfif>
<cfoutput>
<script>
function openNotes(entityNoteID,orgID){
	newWindow = window.open('/data/entityNoteView.cfm?frmEntityType=#attributes.frmEntityType#&frmEntityTypeID=#attributes.frmEntityTypeID#&frmEntityID='+orgID,'AddNote','width=700,height=450,toolbar=0,location=0,directories=0,status=1,menuBar=0,scrollBars=0,resizable=1');
	newWindow.focus();
	}
	
function addNote(orgID){
	openWin('/data/entityNoteView.cfm?frmEntityType=#attributes.frmEntityType#&frmEntityTypeID=#attributes.frmEntityTypeID#&frmEntityID='+orgID,'AddNote','width=700,height=450,toolbar=0,location=0,directories=0,status=1,menuBar=0,scrollBars=0,resizable=1');
	}
</script>
</cfoutput>

<!--- <cfif getNotes.recordcount gt 0>  I have moved this down so that you can add new notes
when there are no previous notes added - MDC 2004-08-09--->
	
	<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="2" BGCOLOR="White" CLASS="withBorder">
	
	<tr>
		<td colspan="2" class="label">Phr_Sys_Notes: <cfoutput>(<A HREF="javaScript:addNote(#attributes.frmentityid#)" TITLE="Phr_Sys_AddNotesToThisRecord">Phr_Sys_Notes</A>)</td></cfoutput>
	</tr>
	
<cfif getNotes.recordcount gt 0>
		<TR>
			<td valign="top" class="label">
				Phr_Sys_DateAddedClickEdit
			</td>
			<td valign="top" class="label">
				Phr_Sys_Notetext
			</td>
		</TR>
		<CFOUTPUT QUERY="getNotes">
			<TR>	
				<td valign="top" class="label">
					<a href="javascript:openNotes(#entityNoteID#,#attributes.frmentityid#)">#dateFormat(getNotes.created,"dd-mmm-yy")#</a>
				</td>
				<td class="label">
					#left(Getnotes.notetext,250)#
				</td>
			</TR>
		</CFOUTPUT>
	</table>
</cfif>

</cf_translate>