<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			entityScreens.cfm	
Author:				SWJ
Date started:		2003-07-26
	
Purpose:	To provide a single template that when passed various variables will submit
			to itself to draw screens for the entity and screens passed.
			
			It also gets a list of active screens

Usage:	

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
	21-Aug-2003		KAP			entity screen links changed to a select

	2004-10-11		WAB   sorting out javascript - wasn't coming back to correct page - problem from when Alex worked on drop down
	2005-08-23 		SWJ 	Added the call to preinclude a file so we can set variables to test later
	2005-11-14  		WAB	removed javascript function Alex() and reverted to verifyForm 

	
Possible enhancements:


 --->

<!--- amendment History
	2001-10-24	SWJ created and developed this to work with per loc & org recs
	 --->
<CFIF Not IsDefined("frmEntityScreen") and Not IsDefined("form.thisScreenTextID")>
	You must pass frmEntityScreen to data/entityScreens.cfm
	<cfexit method="EXITTEMPLATE">
</CFIF>

<CFIF Not IsDefined("frmEntityType")>
	You must pass frmEntityType to data/entityScreens.cfm
	<cfexit method="EXITTEMPLATE">
</CFIF>

	 
<cfparam name="frmEntityScreen" type="string" default="">
<cfparam name="form.thisScreenTextID" type="string" default="#frmEntityScreen#">
	 
	 
	 <!--- WAB 2005-11-15 allow screenTextID to actually be a screenid
	 use this query to resolve
	 --->
	<CFQUERY NAME="GetScreenID" datasource="#application.sitedatasource#">
		select ScreenName, screenid, screentextid
		from screens
		where 
		<cfif isNumeric(thisscreentextid)>  
			screenid =  <cf_queryparam value="#thisscreentextid#" CFSQLTYPE="CF_SQL_INTEGER" > 
		<cfelse>
			screentextid =  <cf_queryparam value="#thisscreentextid#" CFSQLTYPE="CF_SQL_VARCHAR" > 
		</cfif>
	</cfquery>
	<cfset form.thisScreenTextID = getScreenID.screentextID>

<CFIF GetScreenID.recordCount eq 0>
	<cfoutput>A screen with the id #form.thisScreenTextID# cannot be found.</cfoutput> <!--- NJH 2009-02-03 Bug Fix Demo Issue 1719 - added cfoutput --->
	<cfexit method="EXITTEMPLATE">
</CFIF>

<cf_translate>
<cfparam name="openAsWord" type="boolean" default="false">
<cfif openAsWord AND openAsWord NEQ "GetContent">
<!--- get suggested filename --->
	<cfset filename = "#Replace(GetScreenID.screenname,' ','','ALL')#">
	<cfcontent type="application/msword">
	<cfheader name="content-Disposition" value="filename=#filename#.doc">
	<cfinclude template="../templates/wordHeaderInclude.cfm">


	<!---  set screen to view mode--->
	<cfset frmmethod = "view">
	 <BASE href="#request.currentsite.httpprotocol##htmleditformat(sitedomain)#">
</cfif>

<CFPARAM name="message" default="">

<!---  phrases="phr_Sys_OrganisationDetails,Phr_Sys_Contact,Phr_Sys_ListLeads,Phr_Sys_Files,Phr_Sys_ViewSites,Phr_Sys_UserDetails,Phr_Sys_CommSummary,Phr_Sys_ContactHistory,Phr_Sys_Notes"/ --->

<!--- </cf_translate> --->

<CFIF isDefined('frmTask') and frmTask eq "update">
	<!--- include the custom tag for updating screen data --->
	<cf_aScreenUpdate>
	<CFIF frmNext neq "entityScreens">
		<cfif form.frmEntityType eq 0><!--- i.e. a person record --->
			<CFSET frmPersonID = form.frmcurrentEntityID>
		</CFIF>
		<cfset message = message & "Profile details updated successfully.">
<!--- 		<CFInclude TEMPLATE="#frmNext#.cfm">
		<CF_ABORT>
 ---></CFIF>
</CFIF>	

<cfinvoke component="relay.com.profileManagement" method="getFlagType" returnvariable="thisEntityType">
	<cfinvokeArgument name="datasource" value="#application.siteDataSource#">
	<cfinvokeArgument name="entityTypeID" value="#frmEntityType#">
</cfinvoke>

<CFPARAM NAME="frmBack" DEFAULT="datamenu">
<CFPARAM NAME="frmNext" DEFAULT="entityScreens">
<CFPARAM NAME="frmMethod" DEFAULT="edit">
<CFPARAM NAME="frmcurrentEntityID" DEFAULT="0">

<CFPARAM NAME="frmorgID" DEFAULT="0">
<CFPARAM NAME="message" DEFAULT="">

<!--- set the first as a default param --->
<!--- <cfset firstScreenTextID = getSreenList.screenTextID[1]> --->
<!--- <cfparam name="thisScreenTextID" default="#firstScreenTextID#"> --->
<cfset thisEntityTypeID = "frm#thisEntityType#">

<!--- get the locationID for the person record required in the next section --->
<cfif thisEntityType eq "person" and frmcurrentEntityID is not "0">
	<CFQUERY NAME="getEntityDetails" datasource="#application.siteDataSource#">
		SELECT *
			FROM #thisEntityType# 
			WHERE #thisEntityType#ID =  <cf_queryparam value="#frmcurrentEntityID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</CFQUERY>
</cfif>

<!--- get the record details required for cf_aScreen later in the template --->
<cfif thisEntityType eq "person" and frmcurrentEntityID is not "0">
	<cf_getrecord entitytype="person" entityid = "#frmcurrentEntityID#" locationid = "#getEntityDetails.LocationID#" getparents getrights>
<cfelseif thisEntityType eq "location" and frmcurrentEntityID is not "0">
	<cf_getrecord entitytype="location" entityid = "#frmcurrentEntityID#" getparents getrights>
<cfelseif thisEntityType eq "organisation" and frmcurrentEntityID is not "0">
	<cf_getrecord entitytype="organisation" entityid = "#frmcurrentEntityID#" getparents getrights>
<cfelse>
	<cfoutput>Phr_Sys_UnsuportedRelayEntity</cfoutput>
</cfif>	

<cfif NOT openAsWord>
	<!--- SWJ 2006-11-12 Added openWin so we can call it from a screen item --->
	<cf_includejavascriptonce template = "/javascript/openWin.js">

	<SCRIPT type="text/javascript">
	
	<!--

	<!--- WAB sorted out verifyForm and Alex Functions --->
	function verifyForm(next,screenTextID) {
			var form = document.mainForm;
			var msg = "";
			var found = "no";
			if (screenTextID) {   // sometimes called without all the parameters
				form.thisScreenTextID.value = screenTextID;
			}
			form.frmTask.value = "Update";
			msg = verifyInput();
			if (msg != "") {
				alert("\nYou must provide the following information for this entity:\n\n" + msg);
			} else {
				form.submit();
			}
		}
		

	// function changeScreen 
	// WAB 2005-11-14
	// this is a wrapper function so that we can have the same function on this page and on the orgDetail.cfm page
	// here is calls verifyForm, elsewhere it calls viewentityFlags
	// the 'next' parameter isn't used, but all the other functions seem to have it!!

	function changeScreen (next,screenTextID) {
		verifyForm('',screenTextID)
	
	}


		function editProfile() {
			var form = document.editProfileForm;
			form.submit();
		}
		function showAllProfiles() {
			document.mainForm.frmFlagGroupID.value = "";
			document.mainForm.submit();
		}

		function openWordForm() {
			var form = document.openWordForm;
			form.submit() ; 		
		}

		
		
		
//-->

	</SCRIPT>
</cfif>

<CFOUTPUT>

<cf_head>
	<cf_title><cfif isDefined("thisEntityType")>#htmleditformat(thisEntityType)#<cfelse>Phr_Sys_Entity</cfif> Phr_Sys_ProfileData</cf_title>
</cf_head>
</CFOUTPUT>




<!--- WAB 2008-05-29 moved _EntityDetailForms.cfm into body otherwise kills horizontal scrolling--->
<cfif NOT openAsWord>
<cfinclude template="_EntityDetailForms.cfm">
<!--- this include provides the logic for the Relay Form Custom Tags ---> 
	<cfinclude template="/templates/relayFormJavaScripts.cfm">
</cfif>

<cfif NOT openAsWord>
<CFOUTPUT>
<TABLE BORDER=0 BGCOLOR="##FFFFCC" CELLPADDING="5" CELLSPACING="0" WIDTH="100%">
<TR>
	<TD ALIGN="left" VALIGN="top" CLASS="Submenu">Phr_Sys_ProfileDataFor <cfif isDefined("thisEntityType")>#htmleditformat(thisEntityType)#<cfelse>Phr_Sys_Entity</cfif> #htmleditformat(frmcurrentEntityID)#</TD>
	<td align="right" valign="top" nowrap class="Submenu">
		<!--- <cfif isDefined("thisEntityType") and thisEntityType eq "organisation"> 
			<A HREF="/data/orgDetail.cfm?frmOrgID=#frmcurrentEntityID#" CLASS="Submenu">Phr_Sys_OrganisationDetails</A> |
		</cfif> --->
		<!--- <cfif isDefined("session. securityLevels") and listFindNoCase(session. securityLevels,"profileTask(1)") neq 0>  --->
		<cfif application.com.login.checkInternalPermissions("profileTask","Level1")> 
		<A HREF="/templates/subframe.cfm?module=Workflow&goTo=profileManager/profileFrame.cfm?frmEntityTypeID=#frmEntityType#&entityType=#frmEntityType#" target="_blank" CLASS="Submenu">Phr_Sys_ProfileDefinition</A> | 
		</cfif>
		<!--- <cfif isDefined("session. securityLevels") and listFindNoCase(session. securityLevels,"AdminTask(3)") neq 0>  --->
		<cfif application.com.login.checkInternalPermissions("AdminTask","Level2")> 
		<a href="/templates/subframe.cfm?module=Workflow&goTo=admin/screens/screenAdminFrame.cfm?frmScreenTextID=#form.thisScreenTextID#" target="_blank" class="Submenu">Phr_Sys_EditScreen</a> | 
		</cfif>
		<!--- build available vars into querystring --->
		<cfparam name="attributes.pageTitle" type="string" default="">
		<cfparam name="attributes.pagesBack" type="string" default="1">
		<cfparam name="attributes.thisDir" type="string" default="">
		<cfset querystring="REFRESHCACHE=No&FLUSH=0">
		<cfif #cgi.querystring# NEQ "">
			<cfset querystring="#querystring#&#cgi.querystring#">
		</cfif>
		<cfif #IsDefined("form.fieldnames")#>
			<cfloop index="i" list="#form.fieldnames#">
				<cfset querystring="#querystring#&#i#=#evaluate(i)#">
			</cfloop>
		</cfif>
		<A HREF="javascript:openWordForm()" CLASS="Submenu" >Word</A> | 
		<A HREF="JavaScript:verifyForm('','#thisScreenTextID#');" CLASS="Submenu">Phr_Sys_SaveAndReturn</A><!--- WAb removed Alex() function --->
	</td>
</TR>
</TABLE>
</CFOUTPUT>
</cfif>


<cfif NOT openAsWord>
<CFOUTPUT>
<table width="100%" border="0">
	<tr>
	<td align="right">
		
		<cfif isDefined("session.functionDDDisplayType")>
			<cfset displayType = session.functionDDDisplayType>
			<cfset navTopOffset = 2>
		</cfif>
		<cfinclude template="/data/entityNavigationDropDown.cfm">
	</td>
<!--- 
	<cfoutput query="GetScreenList">
		<td><a href="javascript:verifyForm('entityScreens','#screenTextID#');">#ScreenName#</a></td>
	</cfoutput>
 --->
	</tr>
	<CFIF Trim(message) IS NOT ""><TR><TD CLASS="messageText"><cfoutput>#application.com.security.sanitiseHTML(message)#</cfoutput></TD></TR></CFIF>
	<cfset URL.message="">
</table>
</CFOUTPUT>
</cfif>

<cfif NOT openAsWord>

<!--- 	
	WAB moved to ascreenitem.cfm - it will be called a little later
	<!--- SWJ: 2005-08-23 Added the call to preinclude a file so we can set variables to test later --->
	<cfset qCurrentScreen = application.com.screens.getScreen(CurrentScreenId=thisScreenTextID)>
	<CFLOOP index="filetoinclude" list="#valueList(qCurrentScreen.preinclude)#">
		<CFINCLUDE template="#fileToInclude#">
	</cfloop>
 --->

</cfif>

	<!--- WAB 2007-10-17 change to cfform to use masking --->
	<cfform  action="entityScreens.cfm" method="POST" name="mainForm" enctype="multipart/form-data" style="margin:0px;">

<cfif NOT openAsWord>
	<CFOUTPUT>
	<table width="100%" border="0" cellspacing="0" cellpadding="3" align="center">  <!--- WAB 2006-10-11 moved inside the form to allow FF to do dynamic HTML --->
		<INPUT TYPE="hidden" NAME="frmTask" VALUE="">
			<CF_INPUT TYPE="hidden" NAME="entityType" VALUE="#frmEntityType#">
			<CF_INPUT TYPE="hidden" NAME="thisEntity" VALUE="#frmcurrentEntityID#">
			<CF_INPUT TYPE="hidden" NAME="thisScreenTextID" VALUE="#thisScreenTextID#">
			<CF_INPUT TYPE="hidden" NAME="frmEntityType" VALUE="#frmEntityType#">
			<CF_INPUT TYPE="hidden" NAME="frmcurrentEntityID" VALUE="#frmcurrentEntityID#">
			<CF_INPUT TYPE="hidden" NAME="frmNext" VALUE="#frmNext#">
			<INPUT TYPE="hidden" NAME="message" VALUE="">

			<!--- WAB added these ids - often needed if a process is run after an update --->
			<cfif isdefined("person")>
			<CF_INPUT TYPE="hidden" NAME="frmPersonid" VALUE="#person.personid#">
			</cfif>
			<cfif isdefined("location")>
			<CF_INPUT TYPE="hidden" NAME="frmLocationid" VALUE="#location.locationid#">
			</cfif>
			<cfif isdefined("organisation")>
			<CF_INPUT TYPE="hidden" NAME="frmorganisationid" VALUE="#organisation.organisationid#">
			</cfif>

			

		<tr><td width="100%" colspan="3" class="label">&nbsp;</td></tr>
<!--- 		<CFIF frmcurrentEntityID IS NOT 0 and getEntityDetails.RecordCount IS 1>
			<cf_ascreen>
				<cf_ascreenitem screenid="#thisScreenTextID#"	
						method="edit" 
						organisation="#getEntityDetails#" 
				>
			</cf_ascreen>				
		</cfif> --->
		<tr>
			<td width="20" class="label">&nbsp;</td>
			<td class="label">
		</CFOUTPUT>
	</cfif>
			<table width="100%">

<!--- extract content into var for processing in event of opening page as Word file --->
<cfsavecontent variable="thecontentHMTL">
			<cf_ascreen formName = "mainForm">
				<cfif isdefined("person")>
					<cf_ascreenitem screenid="#thisScreenTextID#"	
							method="#frmMethod#" 
							person=#person#
							location=#location#
							organisation=#organisation# 
					>				
				<cfelseif isdefined("location")>
					<cf_ascreenitem screenid="#thisScreenTextID#"	
							method="#frmMethod#" 
							location=#location#
							organisation=#organisation# 
					>
				<cfelseif isdefined("organisation")>

					<cf_ascreenitem screenid="#thisScreenTextID#"	
							method="#frmMethod#" 
							organisation=#organisation# 
					>
				</cfif>	
			</cf_ascreen>	
</cfsaveContent>

<cfif openAsWord>
	<!--- replace any hidden, radio and check fields --->
	<cfset theContentHTML = application.com.regExp.replaceCheckField(theContentHTML,"ALL")>
	<cfset theContentHTML = application.com.regExp.removeHiddenField(theContentHTML,"ALL")>
</cfif>

<cfoutput>#theContentHTML# </cfoutput>

	<cfif NOT openAsWord>
			</table>
			</td>
			<td width="20" class="label">&nbsp;</td>
		</tr>
	</cfif>
	<!--- WAB 2007-10-17 change to cfform to use masking --->
	</cfform>
<cfif NOT openAsWord>
	</table>
	
	<cfif isDefined("getScreen.postinclude") and getScreen.postinclude is not ""> <!--- getScreen is returned by cf_ascreenitem--->
		<cfinclude template = "/#getScreen.postinclude#">
	</cfif>
	
	
	
	<cfoutput>
	<form action="/profileManager/profileFrame.cfm" method="post" name="editProfileForm" id="editProfileForm" target="_blank" >
		<CF_INPUT TYPE="hidden" NAME="frmEntityTypeID" VALUE="#frmEntityType#">
		<CF_INPUT TYPE="hidden" NAME="entityType" VALUE="#frmEntityType#">
	</form>

	<form action="/data/entityscreens.cfm" method="post" name="openWordForm" target="_blank">
			<INPUT TYPE="hidden" NAME="openAsWord" VALUE="True">
			<CF_INPUT TYPE="hidden" NAME="entityType" VALUE="#frmEntityType#">
			<CF_INPUT TYPE="hidden" NAME="thisEntity" VALUE="#frmcurrentEntityID#">
			<CF_INPUT TYPE="hidden" NAME="thisScreenTextID" VALUE="#thisScreenTextID#">
			<CF_INPUT TYPE="hidden" NAME="frmEntityType" VALUE="#frmEntityType#">
			<CF_INPUT TYPE="hidden" NAME="frmcurrentEntityID" VALUE="#frmcurrentEntityID#">
			
	</form>




	</cfoutput>
</cfif>
</cf_translate>



