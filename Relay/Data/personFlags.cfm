<!--- �Relayware. All Rights Reserved 2014 --->
<!--- amendment History
	1999-04-13	WAB Added fields required for search by first and last name
	2008-01-15 WAB fixed problem with country id not being set when checking flags
--->

<!--- <CFSET frmPersonID = 431156> --->

<CFIF NOT #checkPermission.UpdateOkay# GT 0>
	<!--- if frmPersonID is not 0 then task is to update
	      but the user doesn't have update permissions --->
	<CFSET message="You are not authorised to use this function">
	<CFINCLUDE TEMPLATE="datamenu.cfm">
	<CF_ABORT>

</CFIF>
<!--- 2012-07-25 PPB P-SMA001 commented out
<cfinclude template="/templates/qrygetcountries.cfm">
 --->
<CFPARAM NAME="frmBack" DEFAULT="datamenu">
<CFPARAM NAME="frmNext" DEFAULT="datamenu">
<CFPARAM NAME="frmPersonID" DEFAULT="0">
<CFPARAM NAME="message" DEFAULT="">

<CFQUERY NAME="getLocInfo" datasource="#application.siteDataSource#">
	SELECT l.LocationID,
		  l.SiteName,
		  l.Telephone,
		  l.PostalCode,
		  c.CountryDescription,
		  c.ISOCode,
		  c.countryid,
		  p.PersonID,
		  p.Salutation,
		  p.FirstName,
		  p.LastName,
		  p.OfficePhone,
		  p.Email,
		  p.LastUpdated
	FROM Location AS l, Country AS c, Person AS p
	WHERE l.CountryID = c.CountryID
	<!--- 2012-07-25 PPB P-SMA001 commented out
	  AND l.CountryID  IN ( <cf_queryparam value="#Variables.CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) <!--- from qrygetcountries.cfm --->
	  AND c.CountryID  IN ( <cf_queryparam value="#Variables.CountryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	 --->
	  #application.com.rights.getRightsFilterWhereClause(entityType="person",alias="p").whereClause# 	<!--- 2012-07-25 PPB P-SMA001 --->

	  AND p.LocationID = l.LocationID
	  AND p.PersonID =  <cf_queryparam value="#frmPersonID#" CFSQLTYPE="CF_SQL_INTEGER" >
</CFQUERY>

<CFIF getLocInfo.RecordCount IS 0>

	<CFSET message = "You did not select a person, or the person could not be found.">
	<CFINCLUDE TEMPLATE="datamenu.cfm">
	<CF_ABORT>

</CFIF>


<!--- <CFQUERY NAME="getDataSources" datasource="#application.siteDataSource#">
	SELECT d.Description & '-' & pd.RemoteDataSourceID AS DataSource
	  FROM Person AS p, DataSource AS d, PersonDataSource AS pd
	 WHERE d.DataSourceID = pd.DataSourceID
	   AND pd.PersonID = p.PersonID
	   AND p.PersonID = #frmPersonID#
	 ORDER BY d.Description
</CFQUERY> --->


<!--- <CFPARAM NAME="qryCreatedBy" DEFAULT="">
<CFPARAM NAME="qryCreated" DEFAULT="">
<CFPARAM NAME="qryLastUpdatedBy" DEFAULT="">
<CFPARAM NAME="qryLastUpdated" DEFAULT="">

<CFPARAM NAME="qryLookupID" DEFAULT="0"> --->




<cf_head>
<cf_title><cfoutput>#request.CurrentSite.Title#</cfoutput></cf_title>
<SCRIPT type="text/javascript">

<!--
	function verifyForm() {
		var form = document.FlagForm;
		var msg = "";
		var found = "no";

		//for (j=0; j<form.elements.length; j++){
		//	thiselement = form.elements[j];
		//	if (thiselement.name == "insJobFunction") {
		//		if (thiselement.checked) {
		//			found = "yes";
		//		}
		//	}
		//}

		//if (found == "no") {
		//	msg += "* Job Function (at least one)\n";
		//}

		if (msg != "") {
			alert("\nYou must provide the following information for this person:\n\n" + msg);
		} else {
			form.submit();
		}
}
//-->

</SCRIPT>
</cf_head>



<CFOUTPUT>
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
	<TR class="Submenu">
		<TD ALIGN="LEFT" VALIGN="TOP" class="Submenu">Merge Summary</TD>
	</TR>
</TABLE>
</CFOUTPUT>

<CENTER>

<CFIF #Trim(message)# IS NOT "">
<P><CFOUTPUT>#application.com.security.sanitiseHTML(message)#</CFOUTPUT>
</CFIF>

<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" CLASS="withBorder">

	<CFIF #getLocInfo.RecordCount# IS NOT 0>

		<TR><TD VALIGN="TOP" ALIGN="CENTER" COLSPAN=2>
			<TABLE BORDER=0 CELLSPACING=0 CELLPADDING=2>
				<TR><TD class="Label" VALIGN="TOP">
						Person ID:
					</TD>
					<TD VALIGN="TOP">
						<CFOUTPUT>#htmleditformat(getLocInfo.PersonID)#</CFOUTPUT>
					</TD>
				</TR>

				<TR><TD class="Label" VALIGN="TOP">
						Name:
					</TD>
					<TD VALIGN="TOP">
						<CFIF #Trim(getLocInfo.LastName)# IS "">&nbsp;<CFELSE>
						<CFOUTPUT>#HTMLEditFormat(Trim(getLocInfo.FirstName))# #HTMLEditFormat(Trim(getLocInfo.LastName))#</CFOUTPUT>
						</CFIF>
					</TD>
				</TR>
				<TR><TD class="Label" VALIGN="TOP">
						Office Phone:
					</TD>
					<TD VALIGN="TOP">
						<CFIF #Trim(getLocInfo.OfficePhone)# IS "">&nbsp;<CFELSE>
						<CFOUTPUT>#HTMLEditFormat(Trim(getLocInfo.OfficePhone))#</CFOUTPUT>
						</CFIF>
					</TD>
				</TR>
				<TR><TD class="Label" VALIGN="TOP">
						Email:
					</TD>
					<TD VALIGN="TOP">
						<CFIF #Trim(getLocInfo.Email)# IS "">&nbsp;<CFELSE>
						<CFOUTPUT>#HTMLEditFormat(Trim(getLocInfo.Email))#</CFOUTPUT>
						</CFIF>
					</TD>
				</TR>

				<TR><TD COLSPAN="2" ALIGN="CENTER"><CFOUTPUT><IMG SRC="/images/misc/rule.gif" WIDTH=280 HEIGHT=3 ALT="" BORDER="0"><BR></CFOUTPUT></TD></TR>

				<TR><TD class="Label" VALIGN="TOP">
						Location ID:
					</TD>
					<TD VALIGN="TOP">
						<CFOUTPUT>#htmleditformat(getLocInfo.LocationID)#</CFOUTPUT>
					</TD>
				</TR>

				<TR><TD class="Label" VALIGN="TOP">
						Organisation Name:
					</TD>
					<TD VALIGN="TOP">
						<CFIF #Trim(getLocInfo.SiteName)# IS "">&nbsp;<CFELSE>
						<CFOUTPUT>#HTMLEditFormat(Trim(getLocInfo.SiteName))#</CFOUTPUT>
						</CFIF>
					</TD>
				</TR>
				<TR><TD class="Label" VALIGN="TOP">
						Postal Code:
					</TD>
					<TD VALIGN="TOP">
						<CFIF #Trim(getLocInfo.PostalCode)# IS "">&nbsp;<CFELSE>
						<CFOUTPUT>#HTMLEditFormat(Trim(getLocInfo.PostalCode))#</CFOUTPUT>
						</CFIF>
					</TD>
				</TR>
				<TR><TD class="Label" VALIGN="TOP">
						Telephone:
					</TD>
					<TD VALIGN="TOP">
						<CFIF #Trim(getLocInfo.Telephone)# IS "">&nbsp;<CFELSE>
						<CFOUTPUT>#HTMLEditFormat(Trim(getLocInfo.Telephone))#</CFOUTPUT>
						</CFIF>
					</TD>
				</TR>
				<TR><TD class="Label" VALIGN="TOP">
						Country (ISO Code):
					</TD>
					<TD VALIGN="TOP">
						<CFIF #Trim(getLocInfo.CountryDescription)# IS "">&nbsp;<CFELSE>
						<CFOUTPUT>#HTMLEditFormat(Trim(getLocInfo.CountryDescription))# (#HTMLEditFormat(Trim(getLocInfo.ISOCode))#)</CFOUTPUT>
						</CFIF>
					</TD>
				</TR>

				</TABLE>
			</TD>
		</TR>

	<CFELSE>
		<TR><TD ALIGN="CENTER" COLSPAN=2>
				The specified person could not be found.
			<BR></TD>
		</TR>
		</TABLE>
		</CENTER>



		<!--- messy end for messy situation --->
		<CF_ABORT>
	</CFIF>
</TABLE>


<P><cfFORM ACTION="personflagstask.cfm" METHOD="POST" NAME="FlagForm">
	<CF_INPUT TYPE="HIDDEN" NAME="insLastUpdated" VALUE="#CreateODBCDateTime(getLocInfo.LastUpdated)#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmPersonID" VALUE="#frmPersonID#">
	<INPUT TYPE="HIDDEN" NAME="frmTask" VALUE="update">
	<CF_INPUT TYPE="HIDDEN" NAME="frmDupPeople" VALUE="#frmDupPeople#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmDupLocs" VALUE="#frmDupLocs#">

	<CFINCLUDE template="searchparamform.cfm">

	<CF_INPUT TYPE="HIDDEN" NAME="frmLocationID" VALUE="#frmLocationID#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmStartp" VALUE="#frmStartp#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmStart" VALUE="#frmStart#">

	<CF_INPUT TYPE="HIDDEN" NAME="frmBack" VALUE="#frmBack#">

	<CF_INPUT TYPE="HIDDEN" NAME="frmNext" VALUE="#frmNext#">

<TABLE BORDER=0 WIDTH="90%">
<!--- New Flags --->
<TR>
	<TD VALIGN="TOP" COLSPAN=7><BR>
	<CFSET flagMethod = "edit">
	<CFSET entityType = 0>
	<CFSET thisEntity = frmPersonID>
	<CFSET variables.countryid= getLocInfo.countryid> <!--- WAB 2008-01-15 --->
	<cfset thisFormName = "FlagForm">  <!--- added WAB 2005-10-19  - needed by some flags with fancy valid value drop downs--->
	<CFINCLUDE TEMPLATE="../flags/allFlag.cfm">
	<BR><BR></TD>
</TR>

</TABLE>


<CFOUTPUT>
<P><A HREF="JavaScript:verifyForm();"><IMG SRC="/images/buttons/c_savereturn_e.gif" WIDTH=145 HEIGHT=22 ALT="Save and Return" BORDER="0"></A>
</CFOUTPUT>



</cfFORM>

</CENTER>