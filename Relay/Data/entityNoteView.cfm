<!--- �Relayware. All Rights Reserved 2014 --->
<!---


Mods:

WAB 2007-11-27    Mods to work in entityNavigation frame; also
NJH 2008-09-24	  Added entityTypeID 22 for elearning 8.1
SSS 2009-08-13    If this is loaded in a screen do not display close window
NJH	2015/02/24		Jira ??? Also remove the 'save' if in a screen. Set message as well.
NJH 2015/02/26		Jira FIFTEEN-262. Check if form has onsubmit... (not sure how this worked before....)
PYW 2015/06/11    Case 444953. Fix bug when editing existing notes. Bug was introduced in 17653.
 --->

<cf_translate>

<CFIF not isDefined("frmEntityID")>
	<cfabort showerror="frmEntityID must be defined">
</CFIF>

<CFIF not isDefined("frmEntityTypeID")>
	<cfabort showerror="frmEntityTypeID must be defined">
</CFIF>

<!--- NJH 2008-09-24 added entityTypeID 22 for elearning 8.1 --->
<CFSET supportedEntityTypes = "0,1,2,22,23">
<CFIF listFind(supportedEntityTypes,frmEntityTypeID) eq 0>
	<cfabort showerror="You cannot currently add a note to entityType #frmEntityTypeID#">
</CFIF>

<cfparam name="entityNoteID" type="numeric" default="0">

<CFIF isDefined("frmSaveNote") and frmSaveNote eq "Save Note">
	<CFIF isDefined("noteText") and Trim(noteText) neq "">
		<!--- 2008-04-30 - GCC - Lenovo Bugs Issue 80 - Removed cfupdate/cfinsert because they don't support unicode into nText fields --->
		<cfif structKeyExists(form,"entityNoteID") and form.entityNoteID eq 0>
			<cfquery name="createNote" datasource="#application.siteDataSource#">
				insert into entityNote (EntityID,EntityTypeID,NoteType,NoteText,createdBy,created,lastUpdatedBy,lastUpdated)
				values (<cf_queryparam value="#form.EntityID#" CFSQLTYPE="CF_SQL_INTEGER" >,
				<cf_queryparam value="#form.EntityTypeID#" CFSQLTYPE="CF_SQL_INTEGER" >,
				<cf_queryparam value="#form.NoteType#" CFSQLTYPE="CF_SQL_VARCHAR" >,
				<cf_queryparam value="#form.NoteText#" CFSQLTYPE="CF_SQL_VARCHAR" >,
				<cf_queryparam value="#form.createdBy#" CFSQLTYPE="CF_SQL_INTEGER" >,
				<cf_queryparam value="#form.created#" CFSQLTYPE="CF_SQL_TIMESTAMP" >,
				<cf_queryparam value="#form.lastUpdatedBy#" CFSQLTYPE="CF_SQL_INTEGER" >,
				<cf_queryparam value="#form.lastUpdated#" CFSQLTYPE="CF_SQL_TIMESTAMP" >)
			</cfquery>

			<cfset application.com.relayUI.setMessage(message="Note Added")>
		<cfelse>
			<cfquery name="editNote" datasource="#application.siteDataSource#">
				update entityNote
				set EntityID =  <cf_queryparam value="#form.EntityID#" CFSQLTYPE="CF_SQL_INTEGER" > ,
					EntityTypeID =  <cf_queryparam value="#form.EntityTypeID#" CFSQLTYPE="CF_SQL_INTEGER" > ,
					NoteType =  <cf_queryparam value="#form.NoteType#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
					NoteText =  <cf_queryparam value="#form.NoteText#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
					lastUpdatedBy =  <cf_queryparam value="#form.lastUpdatedBy#" CFSQLTYPE="CF_SQL_INTEGER" > ,
					lastUpdated =  <cf_queryparam value="#form.lastUpdated#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
				where
					entityNoteID =  <cf_queryparam value="#form.entityNoteID#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfquery>

			<cfset application.com.relayUI.setMessage(message="Note Updated")>
		</cfif>
	</CFIF>
</CFIF>

<CFQUERY NAME="getNotes" datasource="#application.siteDataSource#">
	select * from entityNote
	Where entityid =  <cf_queryparam value="#frmentityid#" CFSQLTYPE="CF_SQL_INTEGER" >
	and entitytypeid =  <cf_queryparam value="#frmentitytypeid#" CFSQLTYPE="CF_SQL_INTEGER" >
	order by created desc
</CFQUERY>

<!--- PYW 2015/06/11 Case 444953. Fix bug when editing existing notes. Bug was introduced in 17653. --->
<cfif structKeyExists(form,"entityNoteID") and form.entityNoteID gt 0>
	<CFQUERY NAME="loadNote" datasource="#application.siteDataSource#">
		select * from entityNote where entityNoteID =  <cf_queryparam value="#entityNoteID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</CFQUERY>
<cfelse>
	<CFQUERY NAME="loadNote" datasource="#application.siteDataSource#">
		select 0 as entityNoteID, '' as notetext, #frmEntityID# as entityID, #frmEntityTypeID# as entityTypeID, '' as noteType
	</CFQUERY>
</cfif>

<script>
function loadNote(entityNoteID){
	var thisNote=document.mainForm;
	if ((thisNote.dirty.value == 0)) {
		// if the current note has not been changed
		thisNote.entityNoteID.value = entityNoteID;
		thisNote.frmSaveNote.value = "do not save";
		thisNote.submit();
		}
	else { // if we're loading a new note and the existing one has been changed
			if (confirm("Save changes?"))
				//alert("save");
				thisNote.submit();
			else {//alert("do not save");
				thisNote.entityNoteID.value = entityNoteID;
				thisNote.frmSaveNote.value = "do not save";
				thisNote.submit();
				}
		}

	}


	function saveNote(){

		form = document.mainForm
		okToSubmit = true;

		if (document.mainForm.onsubmit) {
			okToSubmit = document.mainForm.onsubmit ()
		}

		if (okToSubmit) {
			form.submit();
			return true
		} else {
			return false
		}
	}

	function submitPrimaryForm(){
		return saveNote()
	}

</script>


	<cf_title><CFOUTPUT>#htmleditformat(frmEntityType)# Phr_Sys_Notes</CFOUTPUT></cf_title>


<CF_RelayNavMenu pageTitle="Phr_Sys_NotesForThis #htmleditformat(frmEntityType)#" thisDir="data">
		<cfif not structKeyExists(form,"THISSCREENTEXTID")>
			<CF_RelayNavMenuItem MenuItemText="Phr_Sys_CloseWindow" CFTemplate="javascript:window.close();">
		</cfif>
		<CF_RelayNavMenuItem MenuItemText="Phr_Sys_NewNote" CFTemplate="javaScript:javascript:loadNote(0);">
		<cfif not structKeyExists(form,"THISSCREENTEXTID")>
			<CF_RelayNavMenuItem MenuItemText="Phr_Sys_SaveNote" CFTemplate="javaScript:void(saveNote());">
		</cfif>
</CF_RelayNavMenu>


<cfif getNotes.recordcount gt 0>
<table class="table table-hover table-bordered">
	<TR>
		<TH ALIGN="LEFT" VALIGN="top">
			<cfoutput>Phr_Sys_DateAddedClickEdit</cfoutput>
		</TH>
		<TH ALIGN="LEFT" VALIGN="top">
			<cfoutput>Phr_Sys_Notetext</cfoutput>
		</TH>
	</TR>
	<CFOUTPUT QUERY="getNotes">
		<TR>
			<td>
				<a href="javascript:loadNote(#entityNoteID#)">#dateFormat(getNotes.created,"dd-mmm-yy")#</a>
			</td>
			<TD>
				#left(Getnotes.notetext,250)#
			</TD>
		</TR>
	</CFOUTPUT>
</table>
</cfif>
	<form  method="POST" name="mainForm" id="mainForm">
		<div class="form-group">
			<label><cfoutput>Phr_Sys_Type</cfoutput>:</label>
			<SELECT NAME="NoteType" REQUIRED="Yes" onChange="dirty.value=1" class="form-control">
				<cfoutput>
				<OPTION VALUE="" #IIF(loadNote.NoteType eq '',DE('SELECTED'),DE(''))#>Phr_Sys_PleaseSelectType</OPTION>
				<OPTION VALUE="General" #IIF(loadNote.NoteType eq 'General',DE('SELECTED'),DE(''))#>Phr_Sys_General</OPTION>
				<OPTION VALUE="Information" #IIF(loadNote.NoteType eq 'Information',DE('SELECTED'),DE(''))#>Phr_Sys_Information</OPTION>
				<OPTION VALUE="Other" #IIF(loadNote.NoteType eq 'other',DE('SELECTED'),DE(''))#>Phr_Sys_Other</OPTION>
				</cfoutput>
			</SELECT>
		</div>
		<div class="form-group">
			<label><cfoutput>(Phr_Sys_NoteID #htmleditformat(loadNote.entityNoteID)#)</cfoutput></label>

			<cfoutput>
				<TEXTAREA COLS="80" ROWS="10" NAME="noteText" onChange="dirty.value=1" REQUIRED="Yes" class="form-control">#htmleditformat(loadNote.notetext)#</TEXTAREA>
				<input type="hidden" name="frmSaveNote" value="Save Note">
				<input type="hidden" name="dirty" value="0">
				<CF_INPUT TYPE="hidden" NAME="EntityID" VALUE="#loadNote.entityID#">
				<CF_INPUT TYPE="hidden" NAME="EntityTypeID" VALUE="#loadNote.entityTypeID#">
				<CF_INPUT TYPE="hidden" NAME="created" VALUE="#now()#">
				<CF_INPUT TYPE="hidden" NAME="createdBy" VALUE="#request.relayCurrentUser.usergroupid#">
				<CF_INPUT TYPE="hidden" NAME="lastUpdated" VALUE="#now()#">
				<CF_INPUT TYPE="hidden" NAME="lastUpdatedBy" VALUE="#request.relayCurrentUser.usergroupid#">
				<CF_INPUT TYPE="hidden" NAME="entityNoteID" VALUE="#loadNote.entityNoteID#">
			</cfoutput>
		</div>
	</form>
</cf_translate>