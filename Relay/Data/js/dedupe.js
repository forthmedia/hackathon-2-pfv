/* �Relayware. All Rights Reserved 2014 */
//	PPB 29/09/2008 New file (with some existing functions included)

	function toggleRadios(objRadioIdClicked) {
		// the radio buttons for the left entity are on a different form to those for the right entity, hence they do not work together and we need to explicitly toggle them
		// the radio button id is "1_" plus the relevant flagGroupId on FormA (LHS) and  "2_" plus the relevant flagGroupId on FormB (RHS)
		
		var strFlagGroupId = objRadioIdClicked.id.substring(2);
		
		if (objRadioIdClicked.id.substring(0,1) == "1") {
			strRadioIdToUpdate = "2_" + strFlagGroupId;
		} else {
			strRadioIdToUpdate = "1_" + strFlagGroupId;
		}
		
		document.getElementById(strRadioIdToUpdate).checked = !document.getElementById(objRadioIdClicked.id).checked;
	}

	function setAllRadios(columnIndex) {
		var loopIndex;
		
		if (columnIndex == 1) {
			thisForm = 'formA';
			otherForm = 'formB';
		} else {
			thisForm = 'formB';
			otherForm = 'formA';
		}

	
		for (loopIndex=0;loopIndex < document.forms[thisForm].elements.length;loopIndex++) {
			objField = document.forms[thisForm].elements[loopIndex];
			
			//set all the flags on this side		
			if (objField.name.substr(0,9) == 'RadioFlag') {
				document.forms[thisForm].elements[loopIndex].checked = true;
			}

			//reset the radio we've just clicked while we're at it so after further UI it can be clicked again if necessary (this should really be a button/link but is a radio for aesthetics 
			if (objField.name.substr(0,7) == 'KeepAll') {
				document.forms[thisForm].elements[loopIndex].checked = false;			
			}
		}

		//unset all the flags on the other form
		for (loopIndex=0;loopIndex < document.forms[otherForm].elements.length;loopIndex++) {
			objField = document.forms[otherForm].elements[loopIndex];
			if (objField.name.substr(0,9) == 'RadioFlag') {
				document.forms[otherForm].elements[loopIndex].checked = false;
			}
		}	
		
	}



	// Elements on forms will have the same name as the appropriate opposite
	function keepinfo(field) {
		// field is the name of the form field to move over
		var counta = 0;
		var countb = 0;
		// loop through formA elements to find element to move
		while ((document.formA.elements.length != counta) && (counta < 100)) {
			if (document.formA.elements[counta].name == field) {
				// loop through formB elements to find element to move to
				while ((document.formB.elements.length != countb) && (countb < 100)) {
					if (document.formB.elements[countb].name == field) {
						document.formB.elements[countb].value = document.formA.elements[counta].value;
						//document.formA.elements[counta].value = "";
						// Ensure this is the last iteration of all loops
						counta = 99;
						countb = 99;
					}
					countb++;
				}
			}
			counta++;
		}
	}

	// Elements on forms will have the same name as the appropriate opposite
	function keepinfoLeft(field) {
		// field is the name of the form field to move over
		var countA = 0;
		var countB = 0;
		// loop through formB elements to find element to move
		while ((document.formB.elements.length != countB) && (countB < 100)) {
			if (document.formB.elements[countB].name == field) {
				// loop through formA elements to find element to move to
				while ((document.formA.elements.length != countA) && (countA < 100)) {
					if (document.formA.elements[countA].name == field) {
						document.formA.elements[countA].value = document.formB.elements[countB].value;
						//document.formB.elements[countB].value = "";
						// Ensure this is the last iteration of all loops
						countA = 99;
						countB = 99;
					}
					countA++;
				}
			}
			countB++;
		}
	}
	
	// Elements on forms will have the same name as the appropriate opposite
	function keepselect(field) {
		// field is the name of the form field to move over (a selection list ID)
		var counta = 0;
		var countb = 0;
		// loop through formA elements to find element to move
		while ((document.formA.elements.length != counta) && (counta < 100)) {
			if (document.formA.elements[counta].name == field) {
				// loop through formB elements to find element to move to
				while ((document.formB.elements.length != countb) && (countb < 100)) {
					if (document.formB.elements[countb].name == field) {
						document.formB.elements[countb].selectedIndex = document.formA.elements[counta].selectedIndex;
						// document.formA.elements[counta].selectedIndex = 0;
						// Ensure this is the last iteration of all loops
						counta = 99;
						countb = 99;
					}
					countb++;
				}
			}
			counta++;
		}
	}

	
	function savededup(columnToSaveIndex) {
		
		var formNameToSubmit;
		var formNameNotSubmitted;
		var objField;
		var loopIndex = 0;
		var newBooleanFlagsList = '';
		var delBooleanFlagsList = '';
		var updateIntegerFlagsList = '';
		var updateTextFlagsList = '';
		var updateDecimalFlagsList = '';
		var updateEventFlagsList = '';
		var flagType;
		var flagId;
		var columnToDelIndex;
		var messageText = '';

		if (columnToSaveIndex == 1) {
			formNameToSubmit = 'formA';
			formNameNotSubmitted = 'formB';
			columnToDelIndex = '2';
		} else {
			formNameToSubmit = 'formB';
			formNameNotSubmitted = 'formA';
			columnToDelIndex = '1';
		}

		// the radio button have NAME: 'RadioFlag' + datatype + relevant flagId (where datatype is B for boolean, I for Integer, T for Text, E for Event) 
		// the radio button have ID:   ('1' for LHS or '2' for RHS) + relevant flagGroupId 
		
		// FORM VALIDATION: loop through the flag data to check at least one side has been clicked
		for (loopIndex=0;loopIndex < document.forms[formNameToSubmit].elements.length;loopIndex++) {
			objField = document.forms[formNameToSubmit].elements[loopIndex];
			
			
			if (objField.name.substr(0,9) == 'RadioFlag') {
				if (!objField.checked) {
					if (!document.getElementById(columnToDelIndex + '_' + objField.id.substr(2)).checked) {
						messageText = messageText + document.getElementById('FlagGroup_' + objField.id.substr(2)).innerHTML + '\n';
					}
				}
			}
		}


		if (messageText != '') {
			alert('You must select a value for the following profiles:\n\n' + messageText); 
		} else {
			saveCursorStyle = document.body.style.cursor;
			document.body.style.cursor = 'wait';				//Set cursor to hourglass
			
			document.formA.KeepThisOne.disabled = 'disabled';	//disable both submit buttons to prevent the user from clicking them both or clicking one more than once
			document.formB.KeepThisOne.disabled = 'disabled';
		
			// loop through the side we are not saving and copy the relevant flagId to the newBooleanFlagsList/updateIntegerFlagsList/updateTextFlagsList fields on side we are saving
			for (loopIndex=0;loopIndex < document.forms[formNameNotSubmitted].elements.length;loopIndex++) {
														
				objField = document.forms[formNameNotSubmitted].elements[loopIndex];
				if (objField.name.substr(0,9) == 'RadioFlag') {
					if (objField.checked) {
							
						flagType = objField.name.substr(9,1); 				// B=Boolean, I=Integer, T=Text, E=Event, D=Decimal
						flagId = objField.name.substring(11);				// strip off the prefix to leave the flagId
						if (flagType == 'B') {		
							newBooleanFlagsList = newBooleanFlagsList + flagId + ',';
						} else if (flagType == 'D') {		
							updateDecimalFlagsList = updateDecimalFlagsList + flagId + ',';			
						} else if (flagType == 'I') {		
							updateIntegerFlagsList = updateIntegerFlagsList + flagId + ',';		
						} else if (flagType == 'T') {		
							updateTextFlagsList = updateTextFlagsList + flagId + ',';			
						} else if (flagType == 'E') {		
							updateEventFlagsList = updateEventFlagsList + flagId + ',';			
						}
					}
				}
			}

			document.forms[formNameToSubmit].newBooleanFlagsList.value = newBooleanFlagsList; 	
			document.forms[formNameToSubmit].updateIntegerFlagsList.value = updateIntegerFlagsList; 			
			document.forms[formNameToSubmit].updateTextFlagsList.value = updateTextFlagsList; 			
			document.forms[formNameToSubmit].updateEventFlagsList.value = updateEventFlagsList;
			document.forms[formNameToSubmit].updateDecimalFlagsList.value = updateDecimalFlagsList;  			
	
			// loop through the side we are saving and copy the relevant flagId to the delBooleanFlagsList fields on side we are saving
			for (loopIndex=0;loopIndex < document.forms[formNameToSubmit].elements.length;loopIndex++) {
														
				objField = document.forms[formNameToSubmit].elements[loopIndex];
				
				if (objField.name.substr(0,9) == 'RadioFlag') {
					if (!objField.checked) {
						flagType = objField.name.substr(9,1); 				// B=Boolean, I=Integer, T=Text, E=Event
						flagId = objField.name.substring(11);				// strip off the prefix to leave the flagId
						if (flagType == 'B') {		
							delBooleanFlagsList = delBooleanFlagsList + flagId + ',';			
						}
					}
				}
			}
	
			document.forms[formNameToSubmit].delBooleanFlagsList.value = delBooleanFlagsList; 			
			
			document.body.style.cursor = 'auto';		//Turn hourglass off
								
			document.forms[formNameToSubmit].submit();
		}

	}
				
	
	function viewentityFlags(entityID,entityTypeID,frmBack,thisScreenTextID,selIndex){ 
		var form = document.flagListForm;
		form.frmcurrentEntityID.value = entityID;
		form.frmEntityType.value = entityTypeID;
		form.thisScreenTextID.value = thisScreenTextID;
		form.frmBack.value = frmBack;
		form.frmNext.value = frmBack;
		if(selIndex){
			form.selIndex.value = selIndex;
		}
		form.submit();	
	}
