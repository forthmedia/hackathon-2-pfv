<!--- �Relayware. All Rights Reserved 2014 --->
	<!---
	PPB 2008-09-29
	for the boolean flags we typically are deleting 1 flagID and inserting a different one 
	for the integer and text the flagID is the same and if we are updating it we delete it for the entityToSave and insert a new row for the entityToSave based on the data in the entityToDelete row
	--->
	
	<!--- do the deletes first to avoid any potential duplicate key errors ---> 
	<cfloop list="#delBooleanFlagsList#" index = "flagID">
		
		<cfset application.com.flag.unsetBooleanFlag(entityID=EntityIdSave,flagID=flagID)>
		<!--- <cfset delFlagId = listGetAt(delBooleanFlagsList, loopCount)>
		<cfquery name="delNewBooleanFlags" datasource="#application.SiteDataSource#">
			DELETE FROM booleanflagdata WHERE entityid =  <cf_queryparam value="#EntityIdSave#" CFSQLTYPE="CF_SQL_INTEGER" >  AND flagid =  <cf_queryparam value="#delFlagId#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>  ---> 
	</cfloop>
	
	<cfloop list="#newBooleanFlagsList#" index = "flagID">
		<cfset application.com.flag.setBooleanFlag(entityID=EntityIdSave,flagTextID=flagID,deleteOtherRadiosInGroup=true)>
		<!--- <cfquery name="addNewBooleanFlags" datasource="#application.SiteDataSource#">
			INSERT INTO booleanflagdata (entityid, flagid, createdBy, lastUpdatedBy) VALUES (<cf_queryparam value="#EntityIdSave#" CFSQLTYPE="CF_SQL_INTEGER" >, <cf_queryparam value="#newFlagId#" CFSQLTYPE="CF_SQL_INTEGER" >, #request.relaycurrentUser.personId#, #request.relaycurrentUser.personId#)
		</cfquery>  ---> 
	</cfloop>
	
	<cfloop list="#updateDecimalFlagsList#" index="flagID">
		<cfset application.com.flag.deleteFlagData(entityID=EntityIdSave,flagId=flagID)>

		<cfquery name="addNewDecimalFlags" datasource="#application.SiteDataSource#">
			INSERT INTO decimalflagdata (entityid, flagid, Data, createdBy, lastUpdatedBy,lastUpdatedByPerson) 
			SELECT <cf_queryparam value="#EntityIdSave#" CFSQLTYPE="CF_SQL_INTEGER" > , FlagId, Data, <cf_queryparam value="#request.relaycurrentUser.userGroupID#" CFSQLTYPE="cf_sql_integer" >, <cf_queryparam value="#request.relaycurrentUser.userGroupID#" CFSQLTYPE="cf_sql_integer" >, <cf_queryparam value="#request.relaycurrentUser.personID#" CFSQLTYPE="cf_sql_integer" > as lastUpdatedByPerson FROM decimalflagdata WHERE entityid =  <cf_queryparam value="#EntityIdDel#" CFSQLTYPE="CF_SQL_INTEGER" >  AND flagId =  <cf_queryparam value="#flagId#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfquery> 
	</cfloop>
	
	<cfloop list="#updateIntegerFlagsList#" index="flagID">
		<cfset application.com.flag.deleteFlagData(entityID=EntityIdSave,flagId=flagID)>
		
		<!--- <cfset newFlagId = #ListGetAt(updateIntegerFlagsList, loopCount)#>
		
		<cfquery name="delIntegerFlags" datasource="#application.SiteDataSource#">
			DELETE FROM integerflagdata WHERE entityid =  <cf_queryparam value="#EntityIdSave#" CFSQLTYPE="CF_SQL_INTEGER" >  AND flagId =  <cf_queryparam value="#newFlagId#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfquery>  --->

		<cfquery name="addNewIntegerFlags" datasource="#application.SiteDataSource#">
			INSERT INTO integerflagdata (entityid, flagid, Data, createdBy, lastUpdatedBy,lastUpdatedByPerson) 
			SELECT <cf_queryparam value="#EntityIdSave#" CFSQLTYPE="CF_SQL_INTEGER" > , FlagId, Data, <cf_queryparam value="#request.relaycurrentUser.userGroupID#" CFSQLTYPE="cf_sql_integer" >, <cf_queryparam value="#request.relaycurrentUser.userGroupID#" CFSQLTYPE="cf_sql_integer" >,<cf_queryparam value="#request.relaycurrentUser.personID#" CFSQLTYPE="cf_sql_integer" > as lastUpdatedByPerson FROM integerflagdata WHERE entityid =  <cf_queryparam value="#EntityIdDel#" CFSQLTYPE="CF_SQL_INTEGER" >  AND flagId =  <cf_queryparam value="#flagId#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfquery> 
	</cfloop>

	<cfloop list="#updateTextFlagsList#" index = "flagID">
		<cfset application.com.flag.deleteFlagData(entityID=EntityIdSave,flagId=flagID)>
		<!--- <cfset newFlagId = #ListGetAt(updateTextFlagsList, loopCount)#>
		
		<cfquery name="delTextFlags" datasource="#application.SiteDataSource#">
			DELETE FROM textflagdata WHERE entityid =  <cf_queryparam value="#EntityIdSave#" CFSQLTYPE="CF_SQL_INTEGER" >  AND flagId =  <cf_queryparam value="#newFlagId#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfquery>  --->

		<cfquery name="addNewTextFlags" datasource="#application.SiteDataSource#">
			INSERT INTO textflagdata (entityid, flagid, Data, createdBy, lastUpdatedBy,lastUpdatedByPerson) 
			SELECT <cf_queryparam value="#EntityIdSave#" CFSQLTYPE="CF_SQL_INTEGER" > , FlagId, Data, <cf_queryparam value="#request.relaycurrentUser.userGroupId#" CFSQLTYPE="cf_sql_integer" >, <cf_queryparam value="#request.relaycurrentUser.userGroupId#" CFSQLTYPE="cf_sql_integer" >,<cf_queryparam value="#request.relaycurrentUser.personID#" CFSQLTYPE="cf_sql_integer" > AS lastUpdatedByPerson FROM textflagdata WHERE entityid =  <cf_queryparam value="#EntityIdDel#" CFSQLTYPE="CF_SQL_INTEGER" >  AND flagId =  <cf_queryparam value="#flagId#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfquery> 
	</cfloop>

	<cfloop list="#updateEventFlagsList#" index = "flagID">
		<cfset application.com.flag.deleteFlagData(entityID=EntityIdSave,flagId=flagID)>
		<!--- <cfset newFlagId = #ListGetAt(updateEventFlagsList, loopCount)#>
		
		<cfquery name="delEventFlags" datasource="#application.SiteDataSource#">
			DELETE FROM eventflagdata WHERE entityid =  <cf_queryparam value="#EntityIdSave#" CFSQLTYPE="CF_SQL_INTEGER" >  AND flagId =  <cf_queryparam value="#newFlagId#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfquery>  --->

		<cfquery name="addNewEventFlags" datasource="#application.SiteDataSource#">
			INSERT INTO eventflagdata (entityid, flagid, RegStatus, createdBy, lastUpdatedBy,lastUpdatedByPerson) 
			SELECT <cf_queryparam value="#EntityIdSave#" CFSQLTYPE="CF_SQL_INTEGER" > , FlagId, RegStatus, <cf_queryparam value="#request.relaycurrentUser.userGroupID#" CFSQLTYPE="cf_sql_integer" >, <cf_queryparam value="#request.relaycurrentUser.userGroupID#" CFSQLTYPE="cf_sql_integer" >,<cf_queryparam value="#request.relaycurrentUser.personID#" CFSQLTYPE="cf_sql_integer" > AS lastUpdatedByPerson FROM eventflagdata WHERE entityid =  <cf_queryparam value="#EntityIdDel#" CFSQLTYPE="CF_SQL_INTEGER" >  AND flagId =  <cf_queryparam value="#flagId#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfquery> 
	</cfloop>
