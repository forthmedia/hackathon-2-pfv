<!--- �Relayware. All Rights Reserved 2014 --->
<!--- amendment History
	1999-04-13	WAB Added fields required for search by first and last name --->

<!--- user rights to access:
		SecurityTask:dataTask
		Permission: Add
---->
<!--- Amendment History
1999-04-09	WAB	Added link back to search results --->

<CFIF NOT checkPermission.AddOkay GT 0>
	<!--- if frmLocationID is 0 then task is to add but the user doesn't have add permissions --->
	<CFSET message="You are not authorised to use this function">
		<CFINCLUDE TEMPLATE="datamenu.cfm">
	<CF_ABORT>
</CFIF>

<CFPARAM NAME="LocationsDel" Default="">

<CFIF NOT IsDefined("message")>
	<!--- remove any 0s from list --->
	<CFLOOP CONDITION="ListFind(frmDupLocs,0) IS NOT 0">
		<CFSET frmDupLocs = ListDeleteAt(frmDupLocs,ListFind(frmDupLocs,0))>
	</CFLOOP>

	<CFSET message = "All records (i.e. people, partner types) previously linked to location record(s) #LocationsDel# are now linked to location record #frmDupLocs#.<br>Please check location record #frmDupLocs# for duplicate people.">
</CFIF>


<!--- if they don't already exist, create the cookies do not conditionally display buttons to give user the choice of re-checking --->
<CFIF NOT structKeyExists(Cookie,"LFLAGSCHECKED")>
	<cfset application.com.globalFunctions.cfcookie(NAME="LFlagsChecked", VALUE="no-#LocationIDsave#")>
</CFIF>

<cf_head>
<SCRIPT type="text/javascript">
<!--
	function goTo(a,b) {
		var form = document.DedupeFlags;
		form.frmGoTo.value = a;
		form.frmNext.value = b;
		form.submit();
	}
//-->
</SCRIPT>
</cf_head>


<cfif isDefined("frmsrchOrgID") and isNumeric(frmsrchOrgID)>
	<!--- are reloading at an org level, as the location may have been deleted that we were previously on, which then causes an error --->
	<cfoutput>
		<script>
			window.opener.parent.loadNewEntity(2,#frmsrchOrgID#,'LocationList',true)
		</script>
	</cfoutput>
<cfelse>
	<cf_body onLoad="window.opener.location.href = window.opener.location.href">
</cfif>

<cfset application.com.request.setTopHead(pageTitle="Location Records Merged")>

<CENTER>

<CFOUTPUT>
#application.com.relayUI.message(message=message)#

<CFSET cols=0>
<TABLE BORDER="0" CELLSPACING="5" CELLPADDING="5">
	<TR>
		<CFIF structKeyExists(Cookie,"LFLAGSCHECKED") and ListFirst(Cookie.LFLAGSCHECKED,"-") IS "no">
			<CFSET cols=cols+1>
			<TD VALIGN=TOP>
			<A HREF="JavaScript:goTo('locflags','locdedupesum');">Check Profile Data</A>
			</TD>
		</CFIF>
		<CFIF structKeyExists(Cookie,"PFLAGSCHECKED") and ListFirst(Cookie.PFLAGSCHECKED,"-") IS "no">
			<CFSET cols=cols+1>
			<TD VALIGN=TOP>
			<A HREF="JavaScript:goTo('locmain','sum');">Check People</A>
			</TD>
		</CFIF>
	</TR>
	<TR>
			<TD VALIGN=TOP align="center" colspan="#cols#">
			<A HREF="JavaScript:window.close();">Finish</A>
			</TD>
	</TR>
</TABLE>
</CFOUTPUT>

<FORM ACTION="locchoice.cfm" METHOD="POST" NAME="DedupeFlags">
	<CFINCLUDE template="searchparamdefaults.cfm">
	<CFPARAM NAME="frmStart" DEFAULT="1">
	<CFPARAM NAME="frmStartp" DEFAULT="1">
	<CFPARAM NAME="frmLocationID" DEFAULT="0">
	<CFOUTPUT>
		<CFIF frmLocationID IS 0>
			<CF_INPUT TYPE="HIDDEN" NAME="frmLocationID" VALUE="#frmDupLocs#">
		<CFELSE>
			<CF_INPUT TYPE="HIDDEN" NAME="frmLocationID" VALUE="#frmLocationID#">
		</CFIF>
	</CFOUTPUT>
	<CFINCLUDE template="searchparamform.cfm">

	<CF_INPUT TYPE="HIDDEN" NAME="frmStart" VALUE="#frmStart#">
	<CF_INPUT TYPE="HIDDEN" NAME="frmStartp" VALUE="#frmStartp#">
	<INPUT TYPE="HIDDEN" NAME="frmGoTo" VALUE="">
	<INPUT TYPE="HIDDEN" NAME="frmNext" VALUE="datamenu">
</FORM>
</CENTER>