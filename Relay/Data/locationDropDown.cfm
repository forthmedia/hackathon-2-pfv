<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 	Code to put a drop down box on the location list screen

Amendment History
		WAB 1999-05-17		changes to parameters passed to updateflag
		WAB 2000-11-29 	Altered check for deletion rights to use a View (which used rights groups)
		WAB 2005-12-05		altered javascript - didn't need newWindow. focus, 'cause this done in the openWin function (which also handles error conditions due to pop up blockers)
		NYF 2008-06-04 	added "mergeTask","Level1" to hide merging from those without rights to do so.  request. ManualLocationMergeOn also added for backwards compatibility
		2008/06/12		SSS		have added merge tasks for switches also for merging.
		2008/12/01		WAB		Mods to make sure that merge functions appeared in the Ajax drilldown
		2008/12/01		WAB		mods to make sure optgroups work (need to close first option tag), also set null value to "None" - which is what is required in dodropdown.js
		2008/12/01		WAB		got rid of form around the select, no longer needed (removed from person and location drop downs some time ago), improves formatting.
		2009-03-12		NYB		changed Deletion Process to use checkEntityForDeletion.cfm
		2010/10/05		WAB 	request.manual....On becomes a setting
		2011-10-25  	NYB		LHID7095,7963 - replaced multiple if's call around Delete Check People to [new] function getPersonDeleteRights
								- to help achieve continuity throughout the system around delete rights

			 --->

<cfparam name="mergeOK" default=0>  <!--- allows merge function to be hidden if say there is only one location on a page --->

<cf_includeJavaScriptOnce template="/javascript/dropdownFunctions.js">
<cf_includeJavaScriptOnce template="/javascript/checkBoxFunctions.js">

<!--- WAB 2007-06-12 so that can be included on other forms <form name="locationFunctions"> --->
	<select name="locationDropDown" onchange="doDropDown(this)" class="form-control">
	<option value=none>phr_sys_location phr_sys_Functions</option>

	<CFOUTPUT>
	<!--- NYF 2008-06-04 added  application.com.login.checkInternalPermissions("mergeTask","Level1") to hide merge when user doesn't have rights
			and request. ManualLocationMergeOn check for backwards compatibility ---->
	<CFIF (application.com.login.checkInternalPermissions("mergeTask","Level2") or application.com.login.checkInternalPermissions("mergeLocTask","Level2")) and application.com.settings.getSetting ("plo.mergeAndDelete.manualMerge.location") and isDefined("mergeOK") and mergeOK>
	<optGroup label="phr_sys_Merge">
		<option value="JavaScript: checks = saveChecks('Location'); if (checks!='0') { void(newWindow = openWin( '../data/locdedupe.cfm?frmDupLocs='+checks+'&frmruninpopup=true','PopUp','width=850,height=650,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1'));  } else {noChecks('Locations')}"> Merge Checked Locations
	</optgroup>
	</cfif>
	<optGroup label="phr_sys_Utilities">
		<!--- 2011-10-25 NYB LHID7095,7963 - replaced multiple if's call to [new] function getLocationDeleteRights - to introduce continuity throughout the system around delete rights --->
		<cfif application.com.rights.getLocationDeleteRights()>
			<!--- NYB 2009-03-12 POL Deletion Process --->
			<option value="JavaScript: if (saveChecks('Location')!='0') { void(newWindow = openWin( '../data/checkEntityForDeletion.cfm?frmentityid='+saveChecks('Location')+'&frmflagtextid=DeleteLocation&frmruninpopup=true&frmflagvalue=1&frmshowfeedback=','PopUp','width=850,height=450,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1'));  } else {noChecks('Location')}"> Phr_Sys_DeleteCheckedLocations
		</cfif>
		<option value="JavaScript: checks = saveChecks('Location'); cntchecks = countLocations('Location'); if (checks!='0') { if (cntchecks == 1) { void(newWindow = openWin( '../data/moveLocationtoNewOrganisation.cfm?frmLocationID='+checks,'PopUp','width=450,height=450,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1')); } else {multiChecks('Locations')} } else {noChecks('Locations')}"> Move Location to a new Organisation
	</optgroup>

	<cfif application.com.login.checkInternalPermissions("ProfileTask","Level2")>	<!--- 2011/07/19 PPB LEN024 added permission --->
	<optGroup label="phr_sys_Profiles">

		<option value="JavaScript: checks=saveChecks('Location') ; if (checks!='0')  { void(newWindow = openWin( '../flags/SetFlagsForIDList.cfm?frmentityid='+checks+'&frmEntityTypeID=1','PopUp','width=450,height=450,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1'));  } else {noChecks('Locations')}"> Set profile attributes for Checked Locations
	</cfif>
	</optgroup>
	</CFOUTPUT>
	</select>