<!--- �Relayware. All Rights Reserved 2014 --->

<cfif structkeyExists(url,"createVcard") and url.createVcard>
	<!--- this if put in by WAB 2005-11-29 to try something out. Used to suppress  automaticcfscript header inserts --->
	<!--- NJH 2006/06/19  - used to create a clean file so MS Outlook will recognise it as a valid .vcf file --->
	<!--- note WAB 2009/10/20 this probably won't work with application.cfc, use clearbuffer instead--->
	<cfset request.suppressApplicationCfmOutput="true">
</cfif>


<CFPARAM NAME="frmDupLocs" DEFAULT="0">	<!--- a list of all the checked locations --->
<!--- add the checked duplicates (if any) to the frmDupLocs list --->
<!--- this code in Loccmain as well --->
<CFIF IsDefined("frmLocsOnPage") >
	<!--- remove all locations on page from List --->
	<CFLOOP INDEX="ID" LIST="#frmLocsOnPage#">
		<CFIF ListFind(frmDupLocs,ID) IS NOT 0>
			<CFSET frmDupLocs = ListDeleteAt(frmDupLocs,ListFind(frmDupLocs,ID))>
		</CFIF>
	</CFLOOP>

	<!--- Add checkboxes (if any) to list --->
	<CFIF IsDefined("frmDupLocCheck") >
		<CFSET frmDupLocs = ListAppend(frmDupLocs,frmDupLocCheck)>
	</CFIF>

</CFIF>

<CFPARAM NAME="frmDupPeople" DEFAULT="0">	<!--- a list of all the checked people --->
<!--- add the checked duplicates (if any) to the frmDupLocs list --->
<!--- this code in Loccmain as well --->

<CFIF IsDefined("frmPeopleOnPage")>
	<!--- remove all locations on page from List --->
	<CFLOOP INDEX="ID" LIST="#frmPeopleOnPage#">
		<CFIF ListFind(frmDupPeople,ID) IS NOT 0>
			<CFSET frmDupPeople = ListDeleteAt(frmDupPeople,ListFind(frmDupPeople,ID))>
		</CFIF>
	</CFLOOP>

	<!--- Add checkboxes (if any) to list --->
	<CFIF IsDefined("frmDupPeopleCheck") >
		<CFSET frmDupPeople = ListAppend(frmDupPeople,frmDupPeopleCheck)>
	</CFIF>

</CFIF>

<cfset application.com.request.setDocumentH1(show=false)>
<!--- dataParams sets up some default values for varaiables
		which can be overridden either by dataINI below or at run time --->
<CFINCLUDE TEMPLATE="dataParams.cfm">

<cf_include template="\code\cftemplates\dataINI.cfm" checkIfExists="true">
<!--- <cfif fileexists("#application.paths.code#\cftemplates\dataINI.cfm")>
	<!--- dataINI can be used to over-ride default
		global application variables and params
			eg: request.showAddress6
		--->
	<cfinclude template="/code/cftemplates/dataINI.cfm">
</cfif> --->


