<!--- �Relayware. All Rights Reserved 2014 --->
<!--- amendment History

	1999-04-13	 WAB 	Added fields required for search by first and last name
	2001-09-06 SWJ	Removed the call to flags and set the screen up to work with styles not fonts
	2001-09-20 SWJ	Updated look and feel slightly and got update and add working properly
	2005-04-07 RND	Inclusion of javascript functions for div toggle
	2005-11-14  WAB	changed the javascript function verifyThisForm to verifyForm
	2006-03-22 SWJ	Reintroduced org.notes so allow a user to capture some notes on an org
		??			Converted to CFFORM and relayFORM elements
	2007-02-20 WAB  resurrected some javascript code which is needed if a screen is included and needs some verification - fits alongside the CF stuff - not very neatly!
					this code seemed to disappear when conversion to cfform done
	2007-04-19 AJC	Add in organisation type ID
	2008/05/01 NJH	Add the ability to set fields to required based on the mandatoryCols parameter.
	2008/06/16   WAb added scriptsrc to one of the cfforms
	2009/05/19 SSS p-TND088 Added a default of false to lock the orgtype field
	2011/02/10 AJC P-LEN023 - Increased maxlength of orgURL
	2011/03/25	PPB	LID6049 - Extend the editable/display size of the Org Name field
	2011/10/28	RMB LID7057 -  changed addNote() JS and openNotes() so scroll bars are on
	2015-03-02	RPW	Changed Confirm Changes on Exit to use custom Save, Discard and Cancel confimation box.
	--->

<!--- user rights to access:
		SecurityTask:dataTask
		Permission: Read
---->

<CFPARAM NAME="frmTask" DEFAULT="add">
<!--- <cfparam name="orgDetailsScreenID" type="string" default="orgProfileSummary"> --->  <!--- NJH 2007/01/26 param the screenIDs --->
<cfset orgDetailsScreenID = application.com.settings.getSetting("screens.orgDetailsScreenID")>


<CFIF frmTask IS "newp">
	<CFINCLUDE TEMPLATE="persondetail.cfm">
	<CF_ABORT>
<CFELSEIF frmTask IS "back">
	<CFINCLUDE TEMPLATE="loclist.cfm">
	<CF_ABORT>
</CFIF>

<CFIF not isDefined("frmOrgID")>
	<cfif isDefined("frmOrganisationID")>
		<cfset frmOrgID = frmOrganisationID>
	<CFelseIF isDefined("frmcurrentEntityID")>
		<cfset frmOrgID = frmcurrentEntityID>
	<CFELSE>
		<CFSET message="Please provide a frmOrgID">
		<CFINCLUDE TEMPLATE="datamenu.cfm">
	<CF_ABORT>
	</CFIF>
</CFIF>


<cfscript>
	if(structKeyExists(url, "frmOrgID")){
		form.frmOrgID = url.frmOrgID;
	}
</cfscript>

<CFPARAM NAME="frmLocationID" DEFAULT="0">
<CFPARAM NAME="frmOrganisationID" DEFAULT="0">
<CFPARAM NAME="qryOrganisationTypeID" DEFAULT="0"> <!--- AJC 2007-04-19 add in organisation type ID --->
<CFPARAM NAME="frmBack" DEFAULT="locdetail">
<CFPARAM NAME="message" DEFAULT="">
<CFPARAM NAME="messageType" DEFAULT="success">
<cfparam name="frmEntityType" default="organisation">
<cfparam name="frmEntityTypeID" default="2">  <!--- WAB 2005-11-14 was 0, ut surely 2 for organisation --->
<CFPARAM NAME="frmOrganisationID" DEFAULT="0">
<CFPARAM NAME="thisEntityType" DEFAULT="organisation">
<CFPARAM NAME="qryOrgName" DEFAULT="">
<CFPARAM NAME="qryAKA" DEFAULT="">
<CFPARAM NAME="qryNotes" DEFAULT="">
<CFPARAM NAME="qryNoteCount" DEFAULT="0">
<CFPARAM NAME="qryDefaultDomainName" DEFAULT="">
<CFPARAM NAME="qryOrgURL" DEFAULT="">
<CFPARAM NAME="qryCountryID" DEFAULT="9">
<CFPARAM NAME="qryCommID" DEFAULT="0">
<CFPARAM NAME="qryAccountMngrID" DEFAULT="0">
<CFPARAM NAME="qryAccountNumber" DEFAULT="">
<CFPARAM NAME="qryActive" DEFAULT="Yes">
<CFPARAM NAME="qryVATnumber" DEFAULT="">
<CFPARAM NAME="qryCrmID" DEFAULT="">
<!---Start p-TND088 SSS 2009/05/19 some clients want orgtype locked default is here override in dataini.cfm --->
<CFPARAM NAME="lockorgtype" DEFAULT="false">
<!---End p-TND088 SSS 2009/05/19 some clients want orgtype locked default is here override in dataini.cfm --->
<cfif frmOrgID is "">
<cfset frmOrgID  = 0>
</cfif>

<!--- NJH 2008/05/01 --->
<!--- NJH 2010/10/22 replaced param with settings
<cfparam name = "mandatoryCols" default = ""> --->
<cfparam name = "mandatoryCols" default = "#application.com.settings.getSetting('plo.mandatoryCols')#">

<!--- NJH 2008/07/03 removed this as it's redundant.
<cfif fileexists("#application.paths.code#\cftemplates\dataINI.cfm")>
	<!--- we can override showAddress6 in the dataINI file --->
	<cfinclude template="/code/cftemplates/dataINI.cfm">
</cfif> --->

<CFIF NOT checkPermission.Level1 GT 0>
	<CFSET message="Phr_Sys_YouAreNotAuthorised">
	<cfset messageType="warning">
		<CFINCLUDE TEMPLATE="datamenu.cfm">
	<CF_ABORT>
</CFIF>


<!--- 2007-05-02 AJC Check for organisation Type specific profile summary screen & if none, checks the past screen too --->
<cfscript>
	orgDetailsScreenID = application.com.screens.getScreenForOrgType("#orgDetailsScreenID#",frmOrgID,"");
</cfscript>

<cfinclude template="/templates/qrygetcountries.cfm">

<!--- NJH 2008/05/01
	Create a structure of the fields in the form and set whether they are required or not.
	The fields are defaulted to false. Use the mandatoryCols or the mandatoryOrgDetailCols
	param to set the values in the structure.

	When adding a new field to the form, remember to add the field to this structure.
 --->
<cfif isDefined("mandatoryColsOverride") and len(mandatoryColsOverride) gt 0>
	<cfset mandatoryCols ="">
	<cfloop index="i" list="#mandatoryColsOverride#" delimiters="-">
		<cfset mandatoryCols = listAppend(mandatoryCols,i,",")>
	</cfloop>
</cfif>

<cfparam name="mandatoryOrgDetailCols" default="#mandatoryCols#">

<cfscript>
	requiredFieldsStruct = structNew();
	StructInsert(requiredFieldsStruct, "insOrgName", "true");
	StructInsert(requiredFieldsStruct, "insOrganisationTypeID", "true");
	StructInsert(requiredFieldsStruct, "insAKA", "false");
	StructInsert(requiredFieldsStruct, "insCountryID", "true");
	StructInsert(requiredFieldsStruct, "insVATnumber", "false");
	StructInsert(requiredFieldsStruct, "insOrgURL", "false");
	StructInsert(requiredFieldsStruct, "insNotes", "false");
	StructInsert(requiredFieldsStruct, "insDefaultDomainName", "false");
</cfscript>

<cfloop list="#mandatoryOrgDetailCols#" index="formField">
	<cfif structKeyExists(requiredFieldsStruct,"#formField#")>
		<cfset requiredFieldsStruct[formField] = "true">
	</cfif>
</cfloop>


<CFIF frmOrgID neq 0>
<cfscript>
	// 2006-03-18 SWJ started to migrate to a cfc based approach
	// Find record information to update
	getOrgDetails = application.com.relayPLO.getOrgDetails(organisationID=frmOrgID);
	session.currentOrgLocCount = application.com.relayPLO.countLocsInOrg(organisationID=frmOrgID);
</cfscript>

	<CFSET frmOrganisationID = getOrgDetails.OrganisationID>

<!--- 	<cfset = countLocs.numLocs> --->

	<CFQUERY NAME="getDataSources" datasource="#application.siteDataSource#">
SELECT TOP 100 PERCENT d.Description + '-' + od.RemoteDataSourceID AS DataSource
		FROM dbo.OrgDataSource od INNER JOIN
		     dbo.DataSource d ON od.DataSourceID = d.DataSourceID RIGHT OUTER JOIN
		     dbo.organisation o ON od.OrganisationID = o.OrganisationID
		WHERE     (o.OrganisationID =  <cf_queryparam value="#frmOrgID#" CFSQLTYPE="CF_SQL_INTEGER" > )
		ORDER BY d.Description
	</CFQUERY>
	<CFIF getOrgDetails.RecordCount IS 1>
		<!--- user can at least read data for that country,
			but although the user can generally update,
			can the user update data for this specific country?? --->

		<CFLOOP QUERY="findcountries">
			<!--- query in qrygetcountries.cfm --->
			<CFIF getOrgDetails.CountryID eq countryID AND NOT (UpdateOkay GT 0)>
				<!--- rights to update can be country specific --->
				<CFSET message="Phr_Sys_YouAreNotAuthorised">
				<cfset messageType="warning">
				<CFINCLUDE TEMPLATE="datamenu.cfm">
				<CF_ABORT>
			</CFIF>

		</CFLOOP>

		<CFQUERY NAME="getNoteCount" datasource="#application.siteDataSource#">
			SELECT count(*) as noteCount from entityNote
				where entityID =  <cf_queryparam value="#frmOrgID#" CFSQLTYPE="CF_SQL_INTEGER" >  and entityTypeID=2
		</CFQUERY>

		<!--- get a list of all live campaigns --->
		<CFQUERY NAME="getCampaigns" datasource="#application.siteDataSource#">
			select commid,commTitle from phoneCommRecords where complete = 0 order by commTitle
		</CFQUERY>
		<!--- find out which campaign this company is in --->
		<cfquery name="getThisCoCamp" datasource="#application.SiteDataSource#" maxrows=1>
			select commID from phoneAction where organisationID =  <cf_queryparam value="#frmOrgID#" CFSQLTYPE="CF_SQL_INTEGER" >  and commID in (
				select commid from phoneCommRecords where complete = 0)
		</cfquery>
		<CFIF getThisCoCamp.recordCount eq 1>
			<CFSET qryCommID = getThisCoCamp.commID>
		<CFELSE>
			<CFSET qryCommID = 0>
		</CFIF>
		<!--- if we got out of the loop, the user can update for this country --->
<!---
	o.OrganisationID, o.OrganisationName, o.AKA, o.DefaultDomainName, o.OrgURL,
	o.Notes, o.MatchName, o.Confidence, o.Active, o.countryID,
    o.AccountMngrID, o.AccountNumber, o.ParentOrgID, o.Created,
	ugb.Name AS LastUpdatedBy, o.LastUpdated, uga.Name AS CreatedBy
 --->
		<CFOUTPUT QUERY="getOrgDetails">
			<CFSET frmOrganisationID = OrganisationID>
			<CFSET frmEntitTypeID = OrganisationID>
			<CFSET qryOrgName = OrganisationName>
			<CFSET qryAKA = AKA>
			<CFSET qryVATnumber = VATnumber>
			<CFSET qryDefaultDomainName = DefaultDomainName>
			<CFSET qryOrgURL = OrgURL>
			<CFSET qryNotes = Notes>
			<cfset qryNoteCount = getNoteCount.NoteCount>
			<CFSET qryAccountMngrID = AccountMngrID>
			<CFSET qryAccountNumber = AccountNumber>
			<CFSET qryCountryID = CountryID>
			<CFSET qryCreatedBy = CreatedBy>
			<CFSET qryCreated = Created>
			<CFSET qryLastUpdatedBy = LastUpdatedBy>
			<CFSET qryLastUpdated = LastUpdated>
			<CFSET frmTask = "update">
			<CFSET qryActive = #YesNoFormat(Active)#>
			<CFSET qryOrganisationTypeID = organisationtypeID><!--- AJC 2007-04-19 add in organisation type ID --->
			<cfset qryCrmID = crmOrgID>
			<cfset qryLinkedInOrgID = LinkedInOrgID>
		</CFOUTPUT>

	<CFELSE>

		<CFSET message="Phr_Sys_DataReqOrgCouldNotBeFound">
		<cfset messageType="info">
		<CF_ABORT>

	</CFIF>


	<!--- finished gathering data for update specific tasks --->
</CFIF>

<cf_includeJavascriptOnce template="/javascript/viewEntity.js">
<cf_includeJavascriptOnce template="/javascript/openWin.js">

<!--- the following is needed whether we add/update --->
<cf_head>
<cf_title>Organisation Detail</cf_title>

<!--- 2015-03-02	RPW	Added isPrimaryFormDirty(). --->
<SCRIPT type="text/javascript">
<cfoutput>

	function deleteEntity(entityID){
		newWindow = window.open('../flags/updateflag.cfm?frmentityid=' + entityID + '&frmflagtextid=DeleteOrganisation&frmruninpopup=true&frmflagvalue=1&frmshowfeedback=1','PopUp','width=450,height=450,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1');
		newWindow.focus();
	}

	function openNotes(entityNoteID,orgID){
	newWindow = window.open('/data/entityNoteView.cfm?frmEntityType=#frmEntityType#&frmEntityTypeID=#frmEntityTypeID#&frmEntityID='+orgID,'AddNote','width=700,height=450,toolbar=0,location=0,directories=0,status=1,menuBar=0,scrollBars=1,resizable=1');
	newWindow.focus();
	}

	function addNote(orgID){
		openWin('/data/entityNoteView.cfm?frmEntityType=#frmEntityType#&frmEntityTypeID=#frmEntityTypeID#&frmEntityID='+orgID,'AddNote','width=750,height=450,toolbar=0,location=0,directories=0,status=1,menuBar=0,scrollBars=1,resizable=1');
		}



	// this function is recognised by the navigation frame  - runs the cold fusion on submit code
	function submitPrimaryForm() {


			var form = document.mainForm;

			okToSubmit = form.onsubmit()
			if (okToSubmit) {
				form.submit()
				return true
			} else {
				return false
			}

	}

	function isPrimaryFormDirty() {

		var form = document.mainForm;
		return isDirty (form)

	}

	<!---
	// WAB 2007-02-20
	// this used to be the main function for verifying the form
	// however now CFFORM has been added to do most of the stuff, but we still need to run anything which is required for an included screen
	// changed code to return true/false where is used to do form.submit (or not)
	--->
	function verifyForm(task,next) {
		var form = document.mainForm;
		form.frmTask.value = task;
		form.frmNext.value = next;

		var msg = "";

		// check for a function called verifyInput, if exists then run it
		// WAB 2008/06/16 had to put window. infront

		if (window.verifyInput) {
			msg = verifyInput ()
		}

		if (msg != "") {
			alert("\nYou must provide the following information:\n\n" + msg);
			return false
		} else {
			return true
		}
	}


		function editLinkToLinkedIn(encryptedUrlVariables){
			var qryStr = "../social/ConnectOrgToLinkedIn.cfm" + encryptedUrlVariables
			openWin(qryStr,'phr_social_LinkToLinkedIn','width=486,height=360,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1');
		}

</cfoutput>
</script>
</cf_head>

<CFINCLUDE TEMPLATE="_entityDetailForms.cfm">
<cfinclude template="/templates/relayFormJavaScripts.cfm">
<cfset request.relayFormDisplayStyle = "HTML-div">

<CFQUERY NAME="HasLeads" datasource="#application.siteDataSource#">
	SELECT 1 FROM Opportunity WHERE entityid =  <cf_queryparam value="#frmOrgID#" CFSQLTYPE="CF_SQL_INTEGER" >
</CFQUERY>

<CFQUERY NAME="getNoteCount" datasource="#application.siteDataSource#">
	SELECT count(*) as noteCount from entityNote
		where entityID =  <cf_queryparam value="#frmOrgID#" CFSQLTYPE="CF_SQL_INTEGER" >  and entityTypeID=2
</CFQUERY>

<cfset session.currentOrgLeadCount = HasLeads.recordCount>

<cfparam name="url.functionDisplay" default="6">

<cfif message neq "">
	<cfset application.com.relayUI.setMessage(message=message,messageType=messageType)>
</cfif>

<cfset application.com.request.setTopHead(topheadCfm="/relay/data/dataTophead.cfm",organisationID=frmOrgID,noteCount=getNoteCount.noteCount,HasLeads=HasLeads.recordCount,screenCfm="orgDetail")>

<cfif url.functionDisplay is 3>
	<cfif frmOrgID>
		<cfset displayType = 3>
		<cfinclude template="/data/entityNavigationDropDown.cfm">
	</cfif>
</cfif>

<cfform action="OrgDetailtask.cfm" method="POST" name="mainForm" onSubmit = "return verifyForm('#frmTask#','orgDetail')">
	<cf_relayFormDisplay>

	<!--- here are all the search parameters --->
		<CFINCLUDE template="searchparamdefaults.cfm">
		<CFINCLUDE template="searchparamform.cfm">
		<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentValue="#frmTask#" fieldName="frmTask">
		<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentValue="orgDetail" fieldName="frmNext">
		<CF_relayFormElementDisplay relayFormElementType="hidden" label="" currentValue="#frmOrganisationID#" fieldName="frmOrganisationID">
		<cfif url.functionDisplay is 1>
			<cfif frmOrgID>
				<cfsavecontent variable="dropDown_html">
					<cfinclude template="/data/entityNavigationDropDown.cfm">
				</cfsavecontent>
				<CF_relayFormElementDisplay relayFormElementType="HTML" currentvalue="#dropDown_html#" fieldname="" spanCols="yes" valueAlign="right" label="">
			</cfif>
		</cfif>


	<cfif url.functionDisplay is 4>
		<cfif frmOrgID>
			<cfset displayType = 4>
			<cfinclude template="/data/entityNavigationDropDown.cfm">
		</cfif>
	</cfif>


		<cfif requiredFieldsStruct['insOrgName']>
			<cfset orgNameLabel = "<span class='required'>Phr_Sys_OrgName (Phr_Sys_OrganisationID :#frmOrganisationID#)</span>">
		<cfelse>
			<cfset orgNameLabel = "Phr_Sys_OrgName (Phr_Sys_OrganisationID :#frmOrganisationID#)">
		</cfif>

		<cf_relayFormElementDisplay relayFormElementType="html" currentValue="" fieldname="frmHTML" label="#orgNameLabel#">
			<!--- NJH 2016/03/22 sugar 446863 - increase length of org name --->
			<cf_relayFormElement relayFormElementType="text" currentValue="#qryOrgname#" fieldName="insOrgName"  maxLength="255" size="80" style="vertical-align:top;" >
			<cfif isDefined("qryLinkedInOrgId") and qryLinkedInOrgId neq "">
				<cfset encryptedUrlVariables = application.com.security.encryptQueryString("?serviceID=LinkedIn&entityTypeID=2&entityID=#frmOrgID#&entityName=#URLEncodedFormat(qryOrgName)#&ServiceEntityID=#qryLinkedInOrgId#")>
		 		<cfoutput><img src="/images/social/linkedin-editLink.png" border="0" style="cursor: pointer;" onclick="javascript:editLinkToLinkedIn('#encryptedUrlVariables#');" ></cfoutput>
			</cfif>
		</cf_relayFormElementDisplay>

		<!--- AJC 2007-04-19 add in organisation type ID --->
		<cfquery name="qry_get_organisationTypes" datasource="#application.siteDataSource#">
			SELECT organisationTypeID,'phr_sys_'+TypeTextID as TypeTextID
			FROM organisationType
			WHERE Active=1
			ORDER BY sortIndex
		</cfquery>
		<cfif qry_get_organisationTypes.RecordCount gt 1>
			<!---Start p-TND088 SSS 2009/05/19 if the locktype is set to true and qryOrgname is not blank disable the field sometimes you can add new record
			from here so the field in this case would have to be live in order to set the new record--->
			<cfif lockorgtype EQ "true" and qryOrgname NEQ "">
				<cfset organisationTypeIDdisabled = true>
			<cfelse>
				<cfset organisationTypeIDdisabled = false>
			</cfif>
			<CF_relayFormElementDisplay relayFormElementType="select" fieldName="insOrganisationTypeID" currentValue="#qryOrganisationTypeID#" query="#qry_get_organisationTypes#" display="typeTextID" value="organisationTypeID" label="Phr_Sys_OrganisationType" required="#requiredFieldsStruct['insOrganisationTypeID']#" disabled="#organisationTypeIDdisabled#">
			<!--- if the orgtype is diabled a hidden field carries the value--->
			<cfif organisationTypeIDdisabled>
				<CF_relayFormElementDisplay relayFormElementType="hidden" fieldName="insOrganisationTypeID" currentValue="#qryOrganisationTypeID#" label="">
			</cfif>
			<!--- If 1 Org Type set hidden value to it's ID --->
		<cfelse>
			<CF_relayFormElementDisplay relayFormElementType="hidden" fieldName="insOrganisationTypeID" currentValue="#qryOrganisationTypeID#" label="">
		</cfif>

		<CF_relayFormElementDisplay relayFormElementType="text"	currentValue="#qryAKA#" fieldName="insAKA" label="Phr_Sys_AKA" required="#requiredFieldsStruct['insAKA']#">

		<!--- 2006/06/19 GCC P_SNY039 --->
		<cfset countryDetails = application.com.commonQueries.getCountry(countryid=qryCountryID)>
		<cfset VATvalidate = "">
		<cfset VATpattern = "">
		<!--- <cfset VATrequired = "No"> --->
		<cfif countryDetails.VATformat neq "">
			<cfset VATvalidate = "regex">
			<cfset VATpattern = countryDetails.VATformat>
		</cfif>
		<cfif countryDetails.VATrequired>
			<cfset requiredFieldsStruct['insVATnumber'] = "true">
			<!--- <cfset VATrequired = "Yes"> --->
		</cfif>
		<CF_relayFormElementDisplay relayFormElementType="text" fieldName="insVATnumber" currentValue="#qryVATnumber#" label="Phr_Sys_VATnumber" required="#requiredFieldsStruct['insVATnumber']#" validate="#VATvalidate#" pattern="#VATpattern#">

		<cfscript>
			getCountries = application.com.RelayPLO.getCountries(countryIds=request.relayCurrentUser.CountryList);
		</cfscript>
		<CF_relayFormElementDisplay relayFormElementType="select" currentValue="#qryCountryID#" fieldName="insCountryID" label="Phr_Sys_Country" query="#getCountries#" display="Country" value="CountryID" nullText="Phr_Sys_SelectACountry" nullValue="" required="#requiredFieldsStruct['insCountryID']#">

		<!--- NJH 2007/10/02 if org url is the organisation matchname method used, make the org url required. This request variable is set in the relayINI.cfm --->
		<cfif structKeyExists(request,"orgMatchNameMethodID") and request.orgMatchNameMethodID eq 2>
			<cfset requiredFieldsStruct['insOrgURL'] = "true">
		</cfif>

		<CF_relayFormElementDisplay relayFormElementType="text"	currentValue="#qryOrgURL#" fieldName="insOrgURL" label="Phr_Sys_WebAddressNOHTTP" noteText="<CFIF trim(qryOrgURL) neq '' and trim(qryOrgURL) neq ' '>&nbsp;<A HREF='http://#replacenocase(qryOrgURL,'http://','','ONE')#' TARGET='_blank'>#htmleditformat(qryOrgURL)#</A></CFIF>" noteTextPosition="bottom" maxlength="500" required="#requiredFieldsStruct['insOrgURL']#">
		<CF_relayFormElementDisplay relayFormElementType="textArea" currentValue="#qrynotes#" fieldName="insNotes" maxlength="2000" label="Phr_Sys_Description">

		<cfif application.com.relayMenu.isModuleActive("connector")>
			<cfset synchingStatus = application.getObject("connector.com.connector").getEntitySynchingStatus(entityType="organisation",entityID=frmOrgID)>
			<CF_relayFormElementDisplay relayFormElementType="text" fieldName="crmID" currentValue="#qryCrmID#" label="phr_Sys_#application.com.settings.getSetting('connector.type')#ID" disabled="true" NoteTextImageSrc="#synchingStatus.statusImage#" NoteText="#synchingStatus.status#" noteTextPosition="right">
		</cfif>

		<CFIF frmOrgID IS NOT 0 and getOrgDetails.RecordCount IS 1>
			<CF_relayFormElementDisplay relayFormElementType="SCREEN" currentvalue="" fieldname="" label="">
				<cf_ascreen formName = "mainForm">
					<cf_ascreenitem screenid="#orgDetailsScreenID#"
							method="edit"
							organisation="#getOrgDetails#"
					>
				</cf_ascreen>
			</CF_relayFormElementDisplay>
		</cfif>

		<CF_relayFormElementDisplay relayFormElementType="hidden" currentValue="#qryCommID#" fieldName="qryCommID" label="">
		<CF_relayFormElementDisplay relayFormElementType="hidden" currentValue="" fieldName="insAccountmngrID" label="">
		<CF_relayFormElementDisplay relayFormElementType="text" currentValue="#qryDefaultDomainName#" fieldName="insDefaultDomainName" label="Phr_Sys_DefaultEmailDomain">

		<CFIF frmOrgID IS NOT 0>

			<cf_querySim>
				getStati
				display,value
				Phr_Sys_Yes|1
				Phr_Sys_No|0
			</cf_querySim>

			<CF_relayFormElementDisplay relayFormElementType="radio" currentValue="#IIF(YesNoFormat(qryActive) IS 'yes',1,0)#" fieldName="insActive" label="Phr_Sys_Active?" query="#getStati#" display="display" value="value">

			<CFIF getOrgDetails.RecordCount IS 1 and getDataSources.DataSource neq "">
				<CF_relayFormElementDisplay relayFormElementType="HTML" currentValue="" fieldName="" label="Phr_Sys_RemoteDataSources" noteText="#Replace(ValueList(getDataSources.DataSource), ',', '<BR>', 'ALL')#" noteTextPosition="top">
			</CFIF>
			<CFIF getOrgDetails.RecordCount IS 1>
				<cf_entityLastUpdatedInfo created="#getOrgDetails.Created#" createdby="#getOrgDetails.CreatedBy#" lastUpdated="#getOrgDetails.LastUpdated#" lastUpdatedby="#getOrgDetails.LastUpdatedBy#">
				<CF_relayFormElementDisplay relayFormElementType="hidden" currentValue="#CreateODBCDateTime(getOrgDetails.LastUpdated)#" fieldName="insLastUpdated" label="">
			</CFIF>
		</CFIF>

	</cf_relayFormDisplay>
</cfform>