<!--- �Relayware. All Rights Reserved 2014 --->
<!---

 	user rights to access:
		SecurityTask:dataTask
		Permission: Update

amendment History
1999-04-13		WAB 	Added fields required for search by first and last name
2001-08-16	SWJ 	Modified to use new look n feel
2004-08-26 	NM 		Added frm defaults for users coming from new dedupe tool.
2007-06-01 	WAB
2007-08-03	SWJ		Modified the screen so that the user can now choose either record to keep
2008-09-23	PPB		Now user has form to choose which field values and flags to keep when merging orgs
2009/04/30  WAB/SSS CR-LEX588   DedupeProtection
				Added code to check for dedupe protection of items.
				Give appropriate warnings and disable one of the "Keep this One" buttons if one item protected.  Abort with warning if both items protected.
				Altered table layout so that always lined up (had to move the << button to the left hand table
				Identical changes made to loc and per dedupe
2009/06/02  WAB  	LID 2308  conflicting flags query not taking entityTypeID into account
--->

<!--- this line is for test purposes as the process was timing out --->
<cfsetting requesttimeout="600">


<cfif isDefined("frmRadioName")>
	<!--- WAB 2007-06-01 removed whole list of params here and included template which does the job (an allows new ones to be added easily), a few items not in the include were left --->
	<cfinclude template="searchParamDefaults.cfm">

	<cfparam NAME="frmStart" default="">
	<cfparam NAME="frmStartP" default="">
	<cfparam NAME="frmStop" default="">
	<cfparam NAME="frmLocationID" default="">
</cfif>

<CFIF NOT checkPermission.UpdateOkay GT 0>
	<!--- if frmLocationID is 0 then task is to Update
	      but the user doesn't have Update permissions --->
	<CFSET message="You are not authorised to use this function">
		<CFINCLUDE TEMPLATE="datamenu.cfm">
	<CF_ABORT>
</CFIF>

<cfif listlen(#frmorgids#) lt 2>
	<P>You must select at least TWO organisations to start the dedupe process.</p>
	<a href="JavaScript:window.close();">Close window</a>   <!--- SSS 2008/05/01 added close window --->
	<CF_ABORT>
</cfif>

<CFPARAM NAME="OrganisationsDel" Default="">

<!--- list of organisations in frmorgids --->
<CFQUERY NAME="getOrgs" datasource="#application.siteDataSource#">
	select
		o.OrganisationName,
		o.OrganisationID,
		o.OrganisationTypeID,
		o.AKA,
		o.VATNumber,
		o.DefaultDomainName,
		o.OrgUrl,
		o.OrgEmail,
		o.Notes,
		o.CountryId,
		o.LastUpdated,
		o.LastUpdatedBy
	from dbo.organisation o
	where o.organisationid  in ( <cf_queryparam value="#frmorgids#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
</CFQUERY>


<CFIF getOrgs.RecordCount GTE 2>
	<!--- WAB 2009/04/30  dedupeProtection, These two functions will tell us if any or either of the elements are merge protected--->
	<cfset mergeCheck = arrayNew(1)>
	<cfset mergeCheck[1] = application.com.dedupe.checkForMergeProtection(entityType=2,entityID =getOrgs.organisationid[1] )>
	<cfset mergeCheck[2] = application.com.dedupe.checkForMergeProtection(entityType=2,entityID =getOrgs.organisationid[2] )>
</cfif>

<!--- Organisation Types for drop down list --->
<CFQUERY NAME="getOrgTypes" DATASOURCE="#application.SiteDataSource#">
	SELECT organisationTypeId, TypeTextId
	FROM organisationType
	ORDER BY TypeTextId
</CFQUERY>

<!--- List of languages for drop down list --->
<CFSET getCountries = application.com.relayPLO.getCountries()>

<CFINCLUDE template="dedupe_functions.cfm">

<cf_title><CFOUTPUT>#request.CurrentSite.Title#</CFOUTPUT></cf_title>

<cf_includeJavascriptOnce template="/data/js/dedupe.js">

<CENTER>

<CFIF getOrgs.RecordCount GTE 2>

	<!--- SSS 2009/05/01 this will stop the process running if all items selected are merge protected --->
	<cfif mergeCheck[1].isMergeProtected and mergeCheck[2].isMergeProtected>
		<cfoutput>
		<FORM NAME="noDedupe" ACTION="" METHOD="post">
		<table>
		<tr>
			<td>
			<div class="errorblock">
				#getOrgs.organisationname[1]# has profile '#mergeCheck[1].detail.flagname#' set<BR><br>
				#getOrgs.organisationname[2]# has profile '#mergeCheck[2].detail.flagname#' set<BR><br>
				These Organizations cannot be merged
			</div>
			</td>
		</tr>
		<tr>
			<td>
				<input type="button" value="Close Window" onClick="JavaScript:window.close();">
			</td>
		</tr>
		</table>
		</form>
		</cfoutput>
		<CF_ABORT>

	<cfelseif not mergeCheck[1].isMergeProtected and not mergeCheck[2].isMergeProtected>
		<!--- 2009/05/01 SSS if nither item is merge protected do not disable the buttons --->
		<cfset noMergeProtectionSet = true>
	</cfif>

<cfset application.com.request.setTopHead(pageTitle="Merge Organization Records")>
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
	<TR>
		<TD ALIGN="RIGHT" VALIGN="top" CLASS="submenu">
			<A id="runInPopUp" style="visibility:visible" HREF="JavaScript:history.back();" Class="Submenu">Back</A>
		</TD>
	</TR>
</table>


<CFSET strow = getOrgs.RecordCount - 1>
<CFSET enrow = getOrgs.RecordCount>

<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
	<TR><CFSET count = 0>
		<CFLOOP QUERY="getOrgs" STARTROW=#strow# ENDROW=#enrow#><CFSET count = count + 1>

			<CFOUTPUT>
			<TD VALIGN=TOP <cfif count is 1>align="right"</cfif>><FORM NAME="#IIF(count IS 1, DE("formA"), DE("formB"))#" ACTION="orgdedupetask.cfm" METHOD="post"> <!--- WAB/SSS 2009/05/05 add right align --->
				<TABLE BORDER="0" CELLPADDING="3" >

			</CFOUTPUT>


			<!---
			the left side of the is screen is populated in the first loop (count=1) followed by the right hand side in the second loop (count=2);
			while populating the left hand side the currentEntity is that on the left and the otherEntity is that on the right... and vice versa;
			--->
			<CFIF count eq 1>
				<CFSET currentEntityId = getOrgs.OrganisationID[1]>
				<CFSET otherEntityId = getOrgs.OrganisationID[2]>
			<CFELSE>
				<CFSET currentEntityId = getOrgs.OrganisationID[2]>
				<CFSET otherEntityId = getOrgs.OrganisationID[1]>
			</CFIF>
				<CFSET EntityTypeId = 2>  <!--- WAB LID 2308 2009/06/02 needed for conflicting flags query --->
			<!--- <CFINCLUDE template="searchparamform.cfm"> --->

			<CFOUTPUT>
			<CF_INPUT TYPE="HIDDEN" NAME="frmOrgIDs" VALUE="#ValueList(getOrgs.OrganisationID)#">

			<CF_INPUT TYPE="HIDDEN" NAME="OrganisationsDel" VALUE="#Variables.OrganisationsDel#">
			<CF_INPUT TYPE="Hidden" NAME="OrganisationIDdel" VALUE="#otherEntityId#"><!--- Record to delete --->
			<CF_INPUT TYPE="Hidden" NAME="OrganisationIDsave" VALUE="#currentEntityId#"><!--- Record to save --->
			<CF_INPUT TYPE="Hidden" NAME="FreezeTime" VALUE="#CreateODBCDateTime(request.requestTime)#"><!--- Freeze Time --->
			<cfif isDefined("frmRadioName")><CF_INPUT type="hidden" name="frmRadioName" value="#frmRadioName#"><CF_INPUT type="hidden" name="frmRadioAmount" value="#frmRadioAmount#"></cfif>
			</CFOUTPUT>

				<TR>
					<TD NOWRAP align="left">
						<CFIF count IS 1>OrganizationID:&nbsp;
					<!--- <CFELSE>Click to move WAB/SSS 2009/05/05 removed when changed layout--->
						</CFIF>
					</TD>
					<TD>
					<CFOUTPUT>#getOrgs.OrganisationID[count]#</CFOUTPUT></TD>
				</TR>

			<!--- ==============================================================================
			SWJ  03-08-2007  Added this query sim to control field list & attributes
			=============================================================================== --->

			<cfoutput>
			<cf_querySim>
			getColumns
			label,name,fieldValue,size,maxlength
			OrganizationName|insOrganisationName|getOrgs.OrganisationName[#htmleditformat(count)#]|30|160
			Type|insOrganisationTypeID|getOrgs.OrganisationTypeID[#htmleditformat(count)#]|30|30
			AKA|insAKA|getOrgs.AKA[#htmleditformat(count)#]|30|160
			Country|insCountryId|getOrgs.CountryId[#htmleditformat(count)#]|30|30
			VATNumber|insVATNumber|getOrgs.VATNumber[#htmleditformat(count)#]|30|50
			DefaultDomainName|insDefaultDomainName|getOrgs.DefaultDomainName[#htmleditformat(count)#]|40|160
			OrgUrl|insOrgUrl|getOrgs.OrgUrl[#htmleditformat(count)#]|40|200
			OrgEmail|insOrgEmail|getOrgs.OrgEmail[#htmleditformat(count)#]|40|200
			Notes|insNotes|getOrgs.Notes[#htmleditformat(count)#]|40|1000
			</cf_querySim>
			</cfoutput>

			<cfoutput query="getColumns">
				<TR>
					<TD NOWRAP align="left">
					<CFIF count IS 1>
					#htmleditformat(label)#
					<cfelse>
						<!--- input type="button" value=" << " onClick="JavaScript:keepinfoLeft('#name#')">&nbsp;&nbsp;&nbsp;&nbsp; WAB/SSS 2009/05/05 moved to right hand table--->
						<CF_INPUT type="button" value=" >> " onClick="JavaScript:keepinfo('#htmleditformat(name)#')">&nbsp;&nbsp;
					</cfif>
					</TD>
					<TD>

					<CFIF label eq "Type">
						<SELECT NAME="#name#">
							<cfloop QUERY="getOrgTypes">
								<OPTION VALUE="#getOrgTypes.OrganisationTypeID#" #IIf(getOrgTypes.OrganisationTypeID EQ evaluate(getColumns.fieldValue),DE("SELECTED"),DE(""))#>#htmleditformat(getOrgTypes.TypeTextId)#</OPTION>
							</cfloop>
						</SELECT>

					<CFELSEIF label eq "Country">
						<SELECT NAME="#name#">
							<cfloop QUERY="getCountries">
								<OPTION VALUE="#getCountries.CountryID#" #IIf(getCountries.CountryID EQ evaluate(getColumns.fieldValue),DE("SELECTED"),DE(""))#>#htmleditformat(getCountries.Country)#</OPTION>
							</cfloop>
						</SELECT>

					<CFELSEIF label eq "Notes">
						<TEXTAREA NAME="insNotes" COLS="24" ROWS="4" WRAP="VIRTUAL">#HTMLEditFormat(evaluate(fieldValue))#</TEXTAREA>

					<CFELSE>
						<CF_INPUT TYPE="Text" NAME="#name#" VALUE="#evaluate(fieldValue)#" SIZE="#size#" MAXLENGTH="#maxlength#">
					</CFIF>
					</TD>
					<!--- WAB/SSS 2009/05/05 moved from other table--->
					<TD align="right">
						<CFIF count IS 1>
							<CF_INPUT type="button" value=" << " onClick="JavaScript:keepinfoLeft('#name#')">
						</cfif>
					</TD>

				</TR>

			</cfoutput>

				<TR><TD height="50"></TD></TR>		<!--- spacer row --->

				<!--- PPB 2008-09-29: CONFLICTING FLAGS --->
				<CFINCLUDE template="dedupe_displayConfictingFlags.cfm">

				<TR>
					<TD NOWRAP>
					&nbsp;
					</TD>

						<TD ALIGN="LEFT">
							<INPUT TYPE="RESET" VALUE="phr_reset">
							<br>
							<input name="KeepThisOne" type="button" value="phr_sys_Keep_This_One" onClick="savededup(<CFOUTPUT>#htmleditformat(count)#</CFOUTPUT>)" <cfif not isdefined("noMergeProtectionSet")><cfif mergeCheck[count].isMergeProtected EQ 0>disabled</cfif></cfif>>
						</TD>
				</TR>
				<tr>
					<td colspan ="2">
					<cfoutput>
					<!--- 2009/05/01 SSS message added to tell you why a button has been disabled and which flag is stopping you from mergeing this record--->
						 <cfif mergeCheck[count].isMergeProtected>
							 <div class="infoblock">
								 Profile '#mergeCheck[count].detail.flagname[1]#' is set.<BR>
								 This Location must be kept.
							</div>
						</cfif>
					</cfoutput>
					</td>
				</tr>
			</TABLE>
		</FORM></TD>
		</CFLOOP>
	</TR>
</TABLE>
</CFIF>
</CENTER>




<script>
	// if this is called in a window hide the back button
	if(opener){
		var thisVar = document.getElementById('runInPopUp');
		thisVar.style.visibility = "hidden";
	}
</script>

<FORM ACTION="entityScreens.cfm" METHOD="post" NAME="flagListForm" TARGET="_blank">
	<INPUT TYPE="hidden" NAME="frmcurrentEntityID" VALUE="0">
	<INPUT TYPE="hidden" NAME="frmEntityType" VALUE="0">
	<INPUT TYPE="hidden" NAME="thisScreenTextID" VALUE="0">
	<INPUT TYPE="hidden" NAME="frmBack" VALUE="0">
	<INPUT TYPE="hidden" NAME="frmNext" VALUE="0">
	<INPUT TYPE="hidden" NAME="selIndex" VALUE="">
</FORM>

<CF_ABORT>


