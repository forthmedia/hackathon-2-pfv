<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
2012-10-04	WAB		Case 430963 Remove Excel Header 
 --->

<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">
<!--- PKP : 2008-04-14 BUG 161 TREND QUERY WAS TIMING OUT  --->
<cfsetting requesttimeout="3600"> 
<cfparam name="openAsExcel" type="boolean" default="false">



<cf_head>
	<cf_title>#htmleditformat(form.FRMENTITYTYPE)# Flag List Page</cf_title>
</cf_head>



<cfoutput>
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
	<TR class="Submenu">
		<TD ALIGN="LEFT" VALIGN="TOP" class="Submenu">#htmleditformat(form.FRMENTITYTYPE)#</TD>
 		<TD ALIGN="right" VALIGN="TOP" class="Submenu">
			<a href="#cgi['SCRIPT_NAME']#?#queryString#&openAsExcel=true" target="_blank" class="Submenu">Excel</a> &nbsp;
		</TD>
	</TR>
</TABLE>
</cfoutput>

<cfparam name="SortOrder" default="Name">
<cfparam name="numRowsPerPage" default="200">
<cfparam name="startRow" default="1">
<cfparam name="FilterSelectFieldList" default="Sitename,ISOcode">
<cfparam name="frmProfileID" default="0">

<cfparam name="showDataTypeIDs" default="4,5,6">
<cfparam name="showFlagNameTypeIDs" default="2,3">

<CFQUERY NAME="getPeople" datasource="#application.siteDataSource#">
		SELECT p.PersonID, p.Salutation, p.FirstName + ' ' + p.LastName as Name, 
			p.OfficePhone as Telephone, l.Telephone as Switchboard, p.Email, p.LastUpdated, l.Sitename,
		    (SELECT MAX(dateSent) FROM commdetail WHERE personID = p.personid
				and commformlid in (select commtypeid from commdetailtype 
				where showincommhistory = 1)) AS lastContactDate, 
			p.locationID,p.jobDesc,c.ISOCode, LEFT( UPPER(p.FirstName), 1 ) as alphabeticalIndex
			<cfif frmdataTableFullName neq "0">
				,b.Created
				<cfif isDefined("frmFlagTypeID") and listFind(showDataTypeIDs,frmFlagTypeID)>
				, b.data
				</cfif>
				<cfif isDefined("frmFlagIDList") and listLen(frmFlagIDList) gt 0 and isDefined("frmFlagTypeID") and listFind(showFlagNameTypeIDs,frmFlagTypeID)>
				, f.name as FlagName
				</cfif>
			</cfif>
		FROM  
			dbo.Person p 
			INNER JOIN dbo.Location L ON p.LocationID = L.LocationID  
			INNER JOIN dbo.Country C ON L.CountryID = C.CountryID
			LEFT OUTER JOIN dbo.UserGroup ug ON p.LastUpdatedBy = ug.UserGroupID
			<cfif frmdataTableFullName neq "0">
			JOIN #frmdataTableFullName# b ON b.entityID = p.personID
			JOIN Flag f ON b.flagID = f.flagID
			</cfif>
			INNER JOIN dbo.#form.FRMENTITYTYPE# e ON p.personid = e.personid
		WHERE 1=1
		<cfif isdefined( "form.frmAlphabeticalIndex" ) and form["frmAlphabeticalIndex"] neq "">
			AND LEFT( UPPER(FirstName), 1 ) =  <cf_queryparam value="#form["frmAlphabeticalIndex"]#" CFSQLTYPE="CF_SQL_VARCHAR" > 
		</cfif>
		<cfif isDefined("frmFlagIDList") and frmProfileID neq "0" and listLen(frmFlagIDList) gt 0 and frmdataTableFullName neq "0">
			AND b.flagID  in ( <cf_queryparam value="#frmFlagIDList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			<cfset dontRun = true>
		</cfif>
		<cfif isDefined("frmFlagIDList") and listLen(frmFlagIDList) gt 0 and frmdataTableFullName neq "0" and not isDefined("dontRun")>
			AND b.flagID in (select flagID from #frmdataTableFullName# where FlagID  IN ( <cf_queryparam value="#frmFlagIDfromFlagCount#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ))
		</cfif>
		<cfinclude template="../templates/tableFromQuery-QueryInclude.cfm">	
	ORDER BY <cf_queryObjectName value="#SortOrder#">
</cfquery>


<cfscript>
// create a structure containing the fields below which we want to be reported when the form is filtered
passThruVars = StructNew();
StructInsert(passthruVars, "frmFlagIDfromFlagCount", frmFlagIDfromFlagCount);
StructInsert(passthruVars, "frmEntityType", frmEntityType);
StructInsert(passthruVars, "frmdataTableFullName", frmdataTableFullName);
if (isDefined("frmProfileID")) { StructInsert(passthruVars, "frmProfileID", frmProfileID); }
if (isDefined("frmFlagIDList")) { StructInsert(passthruVars, "frmFlagIDList", frmFlagIDList); }
</cfscript>

<cfif isDefined("getPeople.FlagName")>
	<cfset lShowTheseColumns = "SALUTATION,NAME,EMAIL,TELEPHONE,SWITCHBOARD,SITENAME,ISOCODE,FLAGNAME,CREATED">
<cfelse>
	<cfset lShowTheseColumns = "SALUTATION,NAME,EMAIL,TELEPHONE,SWITCHBOARD,SITENAME,ISOCODE,CREATED">
</cfif>

<cfif isDefined("getPeople.data")>
	<cfset lShowTheseColumns = listAppend(lShowTheseColumns,"DATA")>
</cfif>

<CF_tableFromQueryObject 
	queryObject="#getPeople#"
	sortOrder = "#SortOrder#"
	numRowsPerPage="#numRowsPerPage#"
	startRow="#startRow#"
	
	keyColumnList="Name" <!--- NJH 2008-07-15 Bug Fix T-10 Issue 752. Change the link from sitename to person name for a person flag --->
	keyColumnURLList="dataFrame.cfm?frmsrchpersonID="
	keyColumnKeyList="personID"
	keyColumnOpenInWindowList="yes"
	
	passThroughVariablesStructure = "#passThruVars#"

	allowColumnSorting="yes"
	showTheseColumns="#lShowTheseColumns#"
	dateFormat="CREATED"
	
	FilterSelectFieldList="#FilterSelectFieldList#"
	
	alphabeticalIndexColumn="Sitename"

	
	>




