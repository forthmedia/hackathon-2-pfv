<!--- �Relayware. All Rights Reserved 2014 --->
<CFIF NOT #checkPermission.UpdateOkay# GT 0>
	<!--- if frmPersonID is not 0 then task is to update
	      but the user doesn't have update permissions --->
	<CFSET message="You are not authorised to use this function">
	<CFINCLUDE TEMPLATE="datamenu.cfm">
	<CF_ABORT>

</CFIF>

<cfinclude template="/templates/qrygetcountries.cfm">

<CFPARAM NAME="frmBack" DEFAULT="datamenu">
<CFPARAM NAME="frmNext" DEFAULT="persondedupesum">
<CFPARAM NAME="frmPersonID" DEFAULT="0">
<CFPARAM NAME="message" DEFAULT="">

<CFTRANSACTION>
	<!--- the last updated flag used will be that of the person table --->
	<CFSET updatetime = CreateODBCDateTime(Now())>

	<!--- check last updated date/time --->
	<CFQUERY NAME="checkPerson" DATASOURCE=#application.SiteDataSource#>
		SELECT PersonID, FirstName, LastName, LastUpdated
		FROM Person
		WHERE PersonID =  <cf_queryparam value="#frmPersonID#" CFSQLTYPE="CF_SQL_INTEGER" >
		AND LastUpdated =  <cf_queryparam value="#insLastUpdated#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
	</CFQUERY>

	<CFIF #checkPerson.RecordCount# IS 0>
		<!--- Removed to get round JIT compilation error </CFTRANSACTION> --->
		<CFSET #message# = "The person's record could not be updated since it has been changed by another user since you last viewed it.">
		<CFINCLUDE TEMPLATE="#frmNext#.cfm">
		<CF_ABORT>
	</CFIF>


	<!--- New Flags --->
	<CFINCLUDE TEMPLATE="../flags/flagTask.cfm">

	<!--- we don't bother archiving for a LastUpdated change --->
	<CFQUERY NAME="updPerson" DATASOURCE=#application.SiteDataSource#>
		UPDATE Person SET
		 LastUpdatedBy = #request.relayCurrentUser.usergroupid#,
		 LastUpdated =  <cf_queryparam value="#Variables.updatetime#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
		WHERE PersonID =  <cf_queryparam value="#frmPersonID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</CFQUERY>


</CFTRANSACTION>

<CFCOOKIE NAME="PFLAGSCHECKED" VALUE="yes-#frmPersonID#">


<!--- <CFSET message = "JobFunction for Person '#Trim(insSalutation & " "  & insFirstName & " " & insLastName)#' was updated successfully."> --->
<CFSET message = "Flags for person #checkPerson.FirstName# #checkPerson.LastName# (#checkPerson.PersonID#) have been updated successfully.">
<CFQUERY Name="getOrgID"  datasource="#application.siteDataSource#">
	select organisationID
	from person
	Where personID =  <cf_queryparam value="#frmPersonID#" CFSQLTYPE="CF_SQL_INTEGER" >
</CFQUERY>

<cfset frmOrganisationID = getOrgID.organisationID>

<CFINCLUDE TEMPLATE="persondedupesum.cfm">
<CF_ABORT>
