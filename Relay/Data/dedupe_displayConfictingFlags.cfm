<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
PPB 2008-09-29: for a technical overview as to how Conflicting Flags are dealt with see fnl-usa2/Relay Documentation/POL Merge Conflicting Flags.doc 
2009/06/02  WAB  	LID 2308  conflicting flags query not taking entityTypeID into account		
  	  	  	Added names of text flags 
---> 

<cfset qryConflictingBooleanFlags = getConflictingFlags(dataType="Boolean", entityTypeID = entityTypeID, currentEntityId= currentEntityId, otherEntityId =otherEntityId)>
<cfset qryConflictingIntegerFlags = getConflictingFlags(dataType="Integer", entityTypeID = entityTypeID, currentEntityId = currentEntityId, otherEntityId =otherEntityId)>
<cfset qryConflictingTextFlags = getConflictingFlags(dataType="Text", entityTypeID = entityTypeID, currentEntityId = currentEntityId, otherEntityId =otherEntityId)>
<cfset qryConflictingEventFlags = getConflictingFlags(dataType="Event", entityTypeID = entityTypeID, currentEntityId = currentEntityId, otherEntityId = otherEntityId)>
<cfset qryConflictingDecimalFlags = getConflictingFlags(dataType="decimal", entityTypeID = entityTypeID, currentEntityId = currentEntityId, otherEntityId = otherEntityId)>

<cfset numConflictingFlags = qryConflictingBooleanFlags.recordcount + qryConflictingIntegerFlags.recordcount + qryConflictingTextFlags.recordcount + qryConflictingEventFlags.recordcount + qryConflictingDecimalFlags.recordcount>

<cfoutput>
	<CFIF numConflictingFlags gt 0> 
		<TR>
			<TD colspan="2" height="40">
				<CFIF count IS 1>
					<B><U>Conflicting Profiles:</U></B>
				</CFIF>
			</TD>
		</TR>
	<!---
		<TR>
			<TD COLSPAN="2">
					Check the radio button for the profiles you would like to keep against the newly merged person
	
			</TD>
		</TR>
	--->			
	
		<TR>
			<TD colspan="2" height="40">
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<CF_INPUT TYPE="RADIO" ID="#count#_KeepAll" NAME="KeepAll" onclick="setAllRadios(#count#)"> <B>phr_sys_Keep_All_Profiles_Below</B>
			</TD>
		</TR>
					
		<!--- DISPLAY BOOLEAN FLAGS --->
		<cfloop query="qryConflictingBooleanFlags">
		<TR>
			<TD colspan="2" height="20">
				<CFIF count IS 1>
					<B><SPAN id="FlagGroup_#FlagGroupId#">#htmleditformat(FlagGroupName)#</SPAN></B>
				</CFIF>
			</TD>
		</TR>
		<TR>
			<TD colspan="2">
				<!--- 
				the radio button id is "1_" plus the relevant flagGroupId on FormA (LHS) and  "2_" plus the relevant flagGroupId on FormB (RHS) 
				the radio button name is RadioFlagB_ plus the relevant flagId on both forms for booleans (RadioFlagI_ for integers and RadioFlagT_ for Text)
				--->
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<CF_INPUT TYPE="RADIO" ID="#count#_#FlagGroupID#" NAME="RadioFlagB_#FlagID#" onclick="toggleRadios(this)"> #htmleditformat(FlagName)# 
			</TD>
		</TR>
		</cfloop>
	
	
		<!--- DISPLAY INTEGER FLAGS --->
		<cfloop query="qryConflictingIntegerFlags">
		<TR>
			<TD colspan="2" height="20">
				<CFIF count IS 1>
					<B><SPAN id="FlagGroup_#FlagGroupId#">#htmleditformat(FlagGroupName)#  - #htmleditformat(FlagName)#</SPAN></B> <!--- WAB 2009/06/02 added in the flag Name --->
				</CFIF>
			</TD>
		</TR>
		<TR>
			<TD colspan="2">
				
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<CF_INPUT TYPE="RADIO" ID="#count#_#FlagGroupID#" NAME="RadioFlagI_#FlagID#" onclick="toggleRadios(this)"> 
			<CFSET valueToDisplay = #FlagData#>
			<CFIF #useValidValues#>
				<CFQUERY name="getValidValueString" datasource="#application.SiteDataSource#">
					select datavalue from validfieldvalues where FieldName =  <cf_queryparam value="flag.#FlagTextId#" CFSQLTYPE="CF_SQL_VARCHAR" >  and ValidValue='*lookup'
				</CFQUERY>
				<CFIF getValidValueString.recordcount gt 0>
					<!--- once we know that this is an integer flag based on a lookup, we can get the Entity Name directly not via the ValidValues --->
					<CFSWITCH expression="#LinksToEntityTypeId#">
						<CFCASE value="0">
							<CFQUERY name="getPerson" datasource="#application.SiteDataSource#">
								SELECT FirstName + ' ' + LastName AS FullName FROM Person WHERE PersonId =  <cf_queryparam value="#FlagData#" CFSQLTYPE="CF_SQL_INTEGER" > 
							</CFQUERY>
							<CFSET valueToDisplay = getPerson.FullName>
						</CFCASE>
						<CFCASE value="1">
							<CFQUERY name="getLocation" datasource="#application.SiteDataSource#">
								SELECT SiteName FROM Location WHERE LocationId =  <cf_queryparam value="#FlagData#" CFSQLTYPE="CF_SQL_INTEGER" > 
							</CFQUERY>
							<CFSET valueToDisplay = getLocation.SiteName>
						</CFCASE>
						<CFCASE value="2">
							<CFQUERY name="getOrganisation" datasource="#application.SiteDataSource#">
								SELECT OrganisationName FROM Organisation WHERE OrganisationId =  <cf_queryparam value="#FlagData#" CFSQLTYPE="CF_SQL_INTEGER" > 
							</CFQUERY>
							<CFSET valueToDisplay = getOrganisation.OrganisationName>
						</CFCASE>
						
					</CFSWITCH>
				</CFIF>
				
				<!--- PPB The code below would allow you to display a dropdown using the appropriate ValidValues query; it is overkill to get the Entity Name via ValidValues to simply display the Name
				<!--- the ValidValid string could potentially refer to a variable that isn't defined at this point hence I trap the error and show the FlagData instead --->
				<CFTRY>
					<CFSET thisQuery = evaluate('"#getValidValueString.datavalue#"')>
					<CFQUERY name="getValidValues" datasource="#application.SiteDataSource#">
						#preservesinglequotes(thisQuery)#
					</CFQUERY>
					<CFQUERY name="getValidValue" dbtype="query" >
						SELECT DisplayValue FROM getValidValues WHERE DataValue = #FlagData#
					</CFQUERY>
					<CFSET valueToDisplay =  #getValidValue.DisplayValue#>
				
					<CFCATCH>
						<CFSET valueToDisplay = #FlagData#>
					</CFCATCH>
				</CFTRY>
				
				FlagData=#FlagData#<br/>
				<SELECT name="validValuesSelect">
					<CFLOOP query="getValidValues">
						<OPTION value="#dataValue#" <CFIF dataValue EQ #FlagData# >SELECTED</CFIF> >#displayValue#</OPTION>
					</CFLOOP>
				</SELECT>
				--->
			</CFIF>
			#valueToDisplay#
			</TD>
		</TR>
		</cfloop>
		
		
		<!--- DISPLAY Decimal FLAGS --->
		<cfloop query="qryConflictingDecimalFlags">
		<TR>
			<TD colspan="2" height="20">
				<CFIF count IS 1>
					<B><SPAN id="FlagGroup_#FlagGroupId#">#htmleditformat(FlagGroupName)# - #htmleditformat(FlagName)#</SPAN></B>
				</CFIF>
			</TD>
		</TR>
		<TR>
			<TD colspan="2">
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<CF_INPUT TYPE="RADIO" ID="#count#_#FlagGroupID#" NAME="RadioFlagD_#FlagID#" onclick="toggleRadios(this)"> #htmleditformat(FlagData)#
			</TD>
		</TR>
		</cfloop>
	
	
		<!--- DISPLAY TEXT FLAGS --->
		<cfloop query="qryConflictingTextFlags">
		<TR>
			<TD colspan="2" height="20">
				<CFIF count IS 1>
					<B><SPAN id="FlagGroup_#FlagGroupId#">#htmleditformat(FlagGroupName)# - #htmleditformat(FlagName)#</SPAN></B>   <!--- WAB 2009/06/02 added in the flag Name --->
				</CFIF>
			</TD>
		</TR>
		<TR>
			<TD colspan="2">
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<CF_INPUT TYPE="RADIO" ID="#count#_#FlagGroupID#" NAME="RadioFlagT_#FlagID#" onclick="toggleRadios(this)"> #htmleditformat(FlagData)#
			</TD>
		</TR>
		</cfloop>
	
	
		<!--- 
		NB. alas the data structure for Events differs from the others; we do not use the FlagGroup info (the event title is held on the eventflagdata).
		because we are matching on FlagId and not FlagGroupId, I have stuck an X_ in the IDs below just in case we were unlucky and we had a FlagGroup and a Flag with the same ID on the form 
		the actual FlagGroupID/FlagID held on the field IDs are only used to keep the fields unique with a prefix (eg 1_6789 and 2_6789 refer to the two instances of the same flag)   
		--->
		
		<!--- DISPLAY EVENT FLAGS --->
		<cfloop query="qryConflictingEventFlags">
		<TR>
			<TD colspan="2" height="20">
				<CFIF count IS 1>
					<B><SPAN id="FlagGroup_X_#FlagId#">#htmleditformat(FlagGroupName)#</SPAN></B>
				</CFIF>
			</TD>
		</TR>
		<TR>
			<TD colspan="2">
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<CF_INPUT TYPE="RADIO" ID="#count#_X_#FlagID#" NAME="RadioFlagE_#FlagID#" onclick="toggleRadios(this)"> #htmleditformat(FlagData)#
			</TD>
		</TR>
		</cfloop>
	</CFIF>		
		<INPUT TYPE="hidden" NAME="newBooleanFlagsList"> 		<!--- this is used to temporarily store the NEW Boolean Flags (ie those checked on formA if we are keeping formB and vice versa) ---> 
		<INPUT TYPE="hidden" NAME="delBooleanFlagsList"> 		<!--- this is used to temporarily store the Boolean Flags to DELETE (ie those not checked on formB if we are keeping formB and vice versa) ---> 
		<INPUT TYPE="hidden" NAME="updateIntegerFlagsList"> 	<!--- this is used to temporarily store the CHANGED Integer Flags (ie those checked on formA if we are keeping formB and vice versa) ---> 
		<INPUT TYPE="hidden" NAME="updateTextFlagsList"> 		<!--- this is used to temporarily store the CHANGED Text Flags (ie those checked on formA if we are keeping formB and vice versa) ---> 
		<INPUT TYPE="hidden" NAME="updateEventFlagsList"> 		<!--- this is used to temporarily store the CHANGED Event Flags (ie those checked on formA if we are keeping formB and vice versa) ---> 
		<INPUT TYPE="hidden" NAME="updateDecimalFlagsList"> 		<!--- this is used to temporarily store the CHANGED Text Flags (ie those checked on formA if we are keeping formB and vice versa) ---> 
</cfoutput>

