<!--- �Relayware. All Rights Reserved 2014 --->
<!--- orgDetailTask.cfm

Manages the adding/updting of organisation Details

Amendment History
2001-10-13	SWJ	Created first version
2001-11-22	SWJ Added the checkOrgExists test and added phoneAction addition
2006-03-22	SWJ	Re-introduced notes
2007-04-19	AJC Add in organisation type ID
2007-06-06 	WAB problems with cf_ascreenupdate and fields on orgTable
2008-06-16  SSS Bug 79 insOrganisationTypeID had to be added to the insertOrganisation function to fix.
2009-02-02	WAB sorted problems reloading 3 pane view when adding orgs
 2010-10-13 WAB removed get TextPhrases
2012-02-27	NJH CASE 425212: use new function to get pointer to the orgList frame
2012/02/27	NJH CASE 425212: use new function to get pointer to the orgList frame
2016/09/01	NJH	JIRA PROD2016-1190 - replaced all dataload matching/insert code with new code
Enhancements
1.  We should use matchName to check for duplicates in checkOrgExists
2.  This has hard coded the rule that you can only have one org in the whole DB.  The
	may need to be changed to cater for companies who want to have one org per country.
--->

<CFPARAM NAME="frmNext" DEFAULT="orgDetail">
<cfparam name="insorganisationtypeID" default="1"> <!--- AJC 2007-04-19 add in organisation type ID --->
<CFIF insAccountmngrID is "">
	<CFSET insAccountmngrID = "NULL">
</CFIF>
<cfset messageType = "success">

<CF_checkMandatoryParams callingTemplate="dataFrame" relayParam="frmTask,frmNext">
<CFIF FORM.frmtask IS "add">

	<!--- verify user has rights to these countries --->

	<CFSET updatetime = CreateODBCDateTime(Now())>

	<!--- first check that we are not going to add a duplicate --->
	<!--- NJH 2016/09/01 JIRA PROD2016-1190 - replaced old dataload code with newer insertOrganisation. That deals with matching also --->
	<!--- <CFQUERY NAME="checkOrgExists" DATASOURCE="#application.SiteDataSource#" MAXROWS="1">
		select organisationID from organisation
		where organisationName =  <cf_queryparam value="#insOrgName#" CFSQLTYPE="CF_SQL_VARCHAR" >
		and countryID =  <cf_queryparam value="#insCountryID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</CFQUERY> --->

	<cfset orgDetails = {CountryID=insCountryID,
					organisationName=insOrgName,
					organisationTypeID=insOrganisationTypeID,
					orgURL=replace(insOrgURL,"http://","","all"),
					AKA=insAKA,
					VATnumber=insVATnumber,
					Notes=insNotes,
					DefaultDomainName=replace(insDefaultDomainName,"www.","","all"),
					AccountMngrID=frmAccountmngrID,
					Active=1}>

		<!--- WAB 2012-04-19
			removed code which checked for inserted organisation by running a query (which stopped working under cfqueryparam)
		--->

		<!---<CFTRANSACTION>
			 <cfscript>
			insertedOrganisationID=	application.com.relayDataload.insertOrganisation(
					CountryID="#insCountryID#",
					organisationName="N'#insOrgName#'",
					organisationTypeID="N'#insOrganisationTypeID#'",
					orgURL="'#replace(insOrgURL,"http://","","all")#'",
					AKA="N'#insAKA#'",
					VATnumber="'#insVATnumber#'",
					Notes="N'#insNotes#'",
					DefaultDomainName="'#replace(insDefaultDomainName,"www.","","all")#'",
					AccountMngrID="#frmAccountmngrID#",
					Active=1,
					createdBy="#request.relaycurrentuser.usergroupID#",
					created="#updatetime#",
					LastUpdatedBy="#request.relaycurrentuser.usergroupID#",
					LastUpdated="#updatetime#"
				);
			</cfscript> --->
			<cfset insertedOrgResult = application.com.relayEntity.insertOrganisation(organisationDetails=orgDetails)>
			<cfset insertedOrganisationID = insertedOrgResult.entityID>
			<CFSET frmOrgID = insertedOrganisationID>

			<!--- if we find a duplicate load that instead --->
			<CFIF insertedOrgResult.errorCode eq "ENTITY_EXISTS">
				<CFSET message = "Phr_Sys_AlreadyHaveOrg '#insOrgName#'. Phr_Sys_NewNotAdded">
				<cfset messageType = "info">
			<CFELSEIF insertedOrganisationID IS 0>
				<CFSET message = "Phr_Sys_ProblemAddingRecord">
				<cfset messageType = "warning">
			<cfelse>

			<!--- <CFQUERY NAME="insOrgDataSource" datasource="#application.siteDataSource#">
				INSERT INTO OrgDataSource(organisationID, DataSourceID, RemoteDataSourceID)
				VALUES(<cf_queryparam value="#insertedOrganisationID#" CFSQLTYPE="CF_SQL_INTEGER" >,1,'0')</CFQUERY>
		</CFTRANSACTION> --->


				<CFOUTPUT>
						<SCRIPT type="text/javascript">
						<!--

							<!--- WAB 2009-02-02  new function to reload the orglist and perslist
								NJH 2012/02/27 CASE 425212: use new function to get pointer to the orgList frame
							--->
				//			if (parent.parent.OrgList.reloadOrgListByOrgID) {
							if (parent.parent.getOrgFrame().reloadOrgListByOrgID) {
								parent.parent.getOrgFrame().reloadOrgListByOrgID(#insertedOrganisationID#)
							}


							<!--- WAB 2009-02-02  If we are in the entity Frame then this will reload the menus --->
							if (parent.loadNewEntityNoRefresh) {
								parent.loadNewEntityNoRefresh (entityTypeID = 2, entityID = #insertedOrganisationID#, screenID = 'organisationdetail' )
							}

						//-->
						</script>
				</CFOUTPUT>


				<CFSET message = "Phr_Sys_TheOrganisation '#insOrgName#' Phr_Sys_WasAddedSuccessfully">
				<cfset messageType = "success">
			</CFIF>

		<CFINCLUDE TEMPLATE="#frmNext#.cfm">
		<CF_ABORT>
	<!---</CFIF>--->


<CFELSEIF FORM.frmtask IS "update">
	<CFSET updatetime = CreateODBCDateTime(Now())>
	<CF_TRANSACTION>

		<cfset orgDetails = {countryID=form.insCountryID,organisationName=form.insOrgName,organisationTypeID=form.insOrganisationTypeID,orgURL=form.insOrgURL,AKA=form.insAKA,VATnumber=form.insVATnumber,notes=form.insNotes,DefaultDomainName=form.insDefaultDomainName,AccountMngrID=form.insAccountmngrID,Active=form.insActive}>
		<cfset updOrg = application.com.relayEntity.updateOrganisationDetails(organisationDetails=orgDetails,organisationID=frmOrganisationID)>

		<!---<CFQUERY NAME="updOrg" DATASOURCE="#application.SiteDataSource#">
		<!--- WAB 2007-06-06 see note below for explanation of this odd bit of query--->
		 select case when datediff(s,LastUpdated,  #insLastUpdated#) >= 0 then 1 else 0 end as Oktoupdate from organisation where OrganisationID =  <cf_queryparam value="#frmOrganisationID#" CFSQLTYPE="CF_SQL_INTEGER" >

		Update organisation
		SET	CountryID =  <cf_queryparam value="#insCountryID#" CFSQLTYPE="CF_SQL_INTEGER" > ,
			organisationName =  <cf_queryparam value="#insOrgName#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
			organisationTypeID =  <cf_queryparam value="#insOrganisationTypeID#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
			orgURL =  <cf_queryparam value="#insOrgURL#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
			AKA =  <cf_queryparam value="#insAKA#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
			VATnumber =  <cf_queryparam value="#insVATnumber#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
			Notes =  <cf_queryparam value="#insNotes#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
			DefaultDomainName =  <cf_queryparam value="#insDefaultDomainName#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
			AccountMngrID =  <cf_queryparam value="#insAccountmngrID#" CFSQLTYPE="CF_SQL_INTEGER" > ,
			Active =  <cf_queryparam value="#insActive?"1":"0"#" CFSQLTYPE="cf_sql_float" > ,
			LastUpdatedBy = #request.relaycurrentuser.usergroupID#,
			LastUpdated =  <cf_queryparam value="#updatetime#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
		WHERE OrganisationID =  <cf_queryparam value="#frmOrganisationID#" CFSQLTYPE="CF_SQL_INTEGER" >
		AND datediff(s,LastUpdated,  #insLastUpdated#) >= 0
		</CFQUERY> --->
		<CFIF isDefined ('insCommID') and insCommID neq 0>
			<CFQUERY NAME="CheckPhoneActions" datasource="#application.siteDataSource#">
				select distinct PhoneActionID
					 from phoneAction pa inner join phonecommRecords c on pa.commid=c.commid
					where c.complete = 0 and organisationID =  <cf_queryparam value="#frmOrganisationID#" CFSQLTYPE="CF_SQL_INTEGER" >
			</CFQUERY>

			<CFIF CheckPhoneActions.RecordCount GT 0>
				<!--- if this org is in a phone campaign remove it and then insert phoneAction record --->
				<CFQUERY NAME="InsertPhoneActions" datasource="#application.siteDataSource#">
					delete from PhoneAction where organisationID =  <cf_queryparam value="#frmOrganisationID#" CFSQLTYPE="CF_SQL_INTEGER" >
				</CFQUERY>
			</CFIF>
			<CFQUERY NAME="InsertPhoneActions" datasource="#application.siteDataSource#">
				INSERT INTO PhoneAction (organisationID, locationID, CommID)
				VALUES (<cf_queryparam value="#frmOrganisationID#" CFSQLTYPE="CF_SQL_INTEGER" >, 0,<cf_queryparam value="#insCommID#" CFSQLTYPE="CF_SQL_INTEGER" >)
			</CFQUERY>

		</CFIF>
	</CF_TRANSACTION>
	<!--- include the custom tag for updating screen data --->

	<!---
		WAB 2007-06-06
		Problem here if the screen includes another field from the organisation table, because updatedata notices that organisation record has already been updated
		I have therefore had to add a check to the query above to see if the dates are OK, and if they are I am going to update the lastupdated variable
	--->
	<cfif updorg.isOK and updorg.entityID neq 0>
		<cfset form["Organisation_lastupdated_#frmOrganisationID#"] = updatetime>
	</cfif>

	<cf_aScreenUpdate>

	<CFSET message = "Phr_Sys_Organisation '#insOrgName#' Phr_Sys_WasUpdatedSuccessfully">
	<cfset messageType = "success">
	<CFSET frmOrgID = frmOrganisationID>
	<CFINCLUDE TEMPLATE="#frmNext#.cfm">
	<CF_ABORT>
</CFIF>