<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Amendment History

Version	 Date		By 		Description
2008-07-21			NYF		Changed Advanced Search & RelayNavMenu pagetitle to use phrases,
							changed the Adv Search target from main to _self so it opens in same window
2013-06-25			RMB		CASE 435923 Add ID attribute to NewCard, AddNewSite and AddNewPerson
2016/01/22			NJH		Jira BF-331 - added inactive/active links
2017-02-03			DBB		ENT-6: Hiding screen elements for read only users
--->

<CFPARAM NAME="current" TYPE="string" DEFAULT="dataSearch.cfm">

<cfparam name="pageTitle" default="">

<cfset dataTaskL2 = application.com.login.checkInternalPermissions("DataTask","Level2")><!---DBB ENT-6--->
<CF_RelayNavMenu pageTitle="#pageTitle#" thisDir="data">

	<cfif findNoCase("entityList.cfm",SCRIPT_NAME)>
    	<CF_RelayNavMenuItem MenuItemText="Dashboard" CFTemplate="javascript:jQuery('form[name=searchStringForDashboard]').submit()" target="_self">
    </cfif>

	<cfif findNoCase("textSearch.cfm",SCRIPT_NAME) or findNoCase("dataMenu.cfm",SCRIPT_NAME)>
		<CF_RelayNavMenuItem MenuItemText="phr_sys_datatophead_AdvancedSearch" CFTemplate="dataSearch.cfm" target="_self">
	</cfif>

	<CFIF findNoCase("orgListV3.cfm",SCRIPT_NAME) or findNoCase("orgListV3.cfm",request.query_string)>
		<cfif isdefined('commID') and isnumeric(commid) and commid neq 0>
			<CF_RelayNavMenuItem MenuItemText="Phr_Sys_NextCall" CFTemplate="Javascript:getNextLead('#commID#')">
		</cfif>
		<cfif application.com.login.checkInternalPermissions("CreateTask").recordcount eq 0 OR application.com.login.checkInternalPermissions("CreateTask","Level3")>
			<!---START: DBB ENT-6--->
			<cfif dataTaskL2>
			<CF_RelayNavMenuItem MenuItemText="Phr_Sys_NewCard" id="AddNewCardTopLink" CFTemplate="JavaScript:quickAdd(0);">
		</cfif>
			<!---END: DBB ENT-6--->
		</cfif>
		<cfif (application.com.login.checkInternalPermissions("mergeTask","Level2") or application.com.login.checkInternalPermissions("mergeorgTask","Level2")) and application.com.settings.getSetting ("plo.mergeAndDelete.manualMerge.organisation")>
			<cfif getOrgRecordCount gt 1>
				<CF_RelayNavMenuItem MenuItemText="Phr_Sys_Merge" CFTemplate="JavaScript: checks=saveChecks('Organisation'); if (checks!='0') { void(openWin( '../data/orgDedupe.cfm?frmorgids='+checks+'&frmruninpopup=true','PopUp','width=750,height=800,left=50,top=50,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1'));  } else {noChecks('Organisation')};">
			</cfif>
		</cfif>
	</CFIF>

	<CFIF findNoCase("persList.cfm",SCRIPT_NAME) or findNoCase("persList.cfm",request.query_string)>
		<cfif getOrgRecordCount NEQ 0>
			<!---START: DBB ENT-6--->
			<cfif dataTaskL2>
			<CF_RelayNavMenuItem MenuItemText="Phr_Sys_AddNewSite" id="AddNewSiteTopLink" CFTemplate="JavaScript:void(viewOrganisation(#frmOrganisationID#,'addNewLocation'));">
			<CF_RelayNavMenuItem MenuItemText="Phr_Sys_AddNewPerson" id="AddNewPersonTopLink"  CFTemplate="JavaScript:void(addNewPerson(#frmOrganisationID#,'viewOrganisation'));">
			</cfif>&nbsp;
			<!---END: DBB ENT-6--->
			<cfif showInActiveRecords>
				<CF_RelayNavMenuItem MenuItemText="phr_sys_ShowActivePersons" id="showActivePersons"  CFTemplate="javascript: document.mainForm.frmShowInActiveRecords.value=0;document.mainForm.submit();">
			<cfelse>
				<CF_RelayNavMenuItem MenuItemText="phr_sys_ShowInactivePersons" id="showInactivePersons"  CFTemplate="javascript: document.mainForm.frmShowInActiveRecords.value=1;document.mainForm.submit();">
			</cfif>
		</cfif>
		<cfif (application.com.login.checkInternalPermissions("mergeTask","Level2") or application.com.login.checkInternalPermissions("mergePerTask","Level2")) and application.com.settings.getSetting ("plo.mergeAndDelete.manualMerge.person")>
			<cfif getPersonRecordCount gt 1>
				<CF_RelayNavMenuItem MenuItemText="Phr_Sys_Merge" CFTemplate="JavaScript: checks = saveChecks('Person'); if (checks!='0') { void(openWin( '../data/personDedupe.cfm?frmDupPeople='+checks+'&frmruninpopup=true&refreshLocList=yes','PopUp','width=750,height=800,left=50,top=50,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1'));  } else {noChecks('Person')};">
			</cfif>
		</cfif>
	</CFIF>

	<cfparam name="organisationID" default=0>
	<cfparam name="screenCfm" default="">
	<CFIF screenCfm eq "orgDetail" and organisationID neq 0>
			<CF_RelayNavMenuItem MenuItemText="Phr_Sys_Notes (#noteCount#)" CFTemplate="javascript:addNote(#organisationID#)">
			<cfif application.com.relayMenu.isModuleActive(moduleId="LeadManager") or application.com.relayMenu.isModuleActive(moduleId="LeadManagerNoProducts")>
				<CFIF HasLeads GT 0>
					<CF_RelayNavMenuItem MenuItemText="Phr_Sys_ListLeads" CFTemplate="/leadManager/leadAndOppList.cfm?entityid=#organisationID#">
				</cfif>
			</cfif>
			<CF_RelayNavMenuItem MenuItemText="Phr_Sys_ViewSites" CFTemplate="javaScript:void(viewOrganisation(#organisationID#,'LocationList'))">
			<CF_RelayNavMenuItem MenuItemText="Phr_Sys_AddNewSite" CFTemplate="javaScript:void(viewOrganisation(#organisationID#,'addNewLocation'))">
	</cfif>
</CF_RelayNavMenu>