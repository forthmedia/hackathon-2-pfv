<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File:			entityFiles.cfm
Author:			SWJ
Date created:	2003-07-29

Description:	Template to link files to an entity

Version history:
1


Expected vars:
frmEntityType e.g. person, location etc. i.e. the table the file is linked to
frmEntityID the id of the entity this is linked to
--->





<cf_head>
	<cf_title>Related Files</cf_title>
</cf_head>




<cfif isdefined("getorganisationID")>
	<cfset frmEntityID = getorganisationID.organisationID>
</cfif>
<!--- WAB 2008-05-12 
	When we went to the new entity viewer, this page went a bit funny because it wasn't obvious what the entityType was
	use this new parameter frmScreenEntityType
 --->
<cfif isDefined("frmScreenEntityType")>
	<cfset frmEntityType = frmScreenEntityType>
	<cfset frmEntityID = frmScreenEntityID>
</cfif>

<cfparam name="defaultFileaction" default="list,put">
<cfparam name="fileCategory" default="">
<cfparam name="showFileCategoryDescription" default="false">
<!--- RMB - P-MIC002 - 2013-04-11 - Added 'showLanguage' and 'phrasePrefix' and 'listPhrasePrefix' and 'showSize' and 'showLoadedBy' --->
<cfparam name="showLanguage" default="false">
<cfparam name="phrasePrefix" type="string" default="">
<cfparam name="listPhrasePrefix" type="string" default="">
<cfparam name="showSize" default="true">
<cfparam name="showLoadedBy" default="true">


<!--- <cfoutput>
<TABLE BORDER=0 BGCOLOR="##FFFFCC" CELLPADDING="5" CELLSPACING="0" WIDTH="100%">
<TR>
	<TD ALIGN="left" VALIGN="top" CLASS="Submenu">phr_files <cfif isDefined("frmEntityType")>#htmleditformat(frmEntityType)#<cfelse>Entity</cfif> #htmleditformat(frmEntityID)#</TD>
	<td align="right" valign="top" nowrap class="Submenu">
		<A HREF="javascript:history.go(-1);" Class="Submenu">Back</A>&nbsp;&nbsp;
	</td>
</TR>
</TABLE>
</CFOUTPUT> --->
<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" CLASS="withBorder">
	
<cfif isDefined("frmEntityType") and frmEntityType eq "organisation">
	<tr><th align="left">Organisation Files</th></tr>
	<!--- RMB - P-MIC002 - 2013-04-11 - Added 'showLanguage' and 'phrasePrefix' and 'listPhrasePrefix' TO 'cf_relatedFile' and 'showSize' --->
	<tr><td><cf_relatedFile action="list" entity="#frmEntityID#" entitytype="#frmEntityType#" fileCategory="#fileCategory#" showFileCategoryDescription="#showFileCategoryDescription#" showLanguage="#showLanguage#" phrasePrefix="#phrasePrefix#" listPhrasePrefix="#listPhrasePrefix#" showSize="#showSize#" showLoadedBy="#showLoadedBy#"></td></tr>
	<!--- SSS 2008-05-12 someone has added this but it does not work this will need a very big redesign to make it work this way --->
	<!--- <tr><th align="left">Opportunity Files</th></tr>
	<tr><td><cf_relatedFile action="list" entity="#frmEntityID#" entitytype="Opportunity"></td></tr>
	 --->
	<cfset defaultFileaction="put">
</cfif>

	<tr><th align="left">Specify files to upload</th></tr>

<!--- RMB - P-MIC002 - 2013-04-11 - Added 'showLanguage' and 'phrasePrefix' and 'listPhrasePrefix' TO 'cf_relatedFile' and 'showSize' --->
<tr><td><cf_relatedFile action="#defaultFileaction#" entity="#frmEntityID#" entitytype="#frmEntityType#" fileCategory="#fileCategory#" showLanguage="#showLanguage#" phrasePrefix="#phrasePrefix#" listPhrasePrefix="#listPhrasePrefix#" showSize="#showSize#" showLoadedBy="#showLoadedBy#" showFileCategoryDescription=#showFileCategoryDescription#></td></tr>
</table>