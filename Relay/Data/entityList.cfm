<!--- �Relayware. All Rights Reserved 2014
Edit History:
2015-12-18 SB Bootstrap
2016/06/09	NJH	JIRA PROD2016-342 - add locations to search results. Did some refactoring of existing code.
--->
<cfsetting enablecfoutputonly="true">
<cfparam name="URL.tablesToSearch" default="">
<cfparam name="URL.entityID" default="">
<cfparam name="URL.orderby" default="">
<cfparam name="URL.orderByDirection" default="asc">
<cf_includejavascriptonce template = "/javascript/openwin.js">
<cf_includeJavascriptOnce template = "/javascript/viewEntity.js">

<!--- an array that holds structure containing field information, such as what the label is, whether the field can be ordered by --->
<cfset columnArray = arrayNew(1)>

<cfswitch expression="#URL.tablesToSearch#">
	<cfcase value="Organisation">
		<cfset extraColumns="o.AKA,o.countryID,o.organisationTypeID">
		<cfif url.orderBy eq "">
			<cfset URL.orderby = "entityName,AKA,entityLink">
		</cfif>
		<cfset searchVariable = "frmSrchOrgID">

		<cfset columnArray[1] = {column="entityName",label="Organization",orderby=true,linkToEntityName=true}>
		<cfset columnArray[2] = {column="AKA",label="AKA",orderby=true}>
		<cfset columnArray[3] = {column="organisationTypeID",label="Org. Type"}>
		<cfset columnArray[4] = {column="countryID",label="Country"}>
		<cfset columnArray[5] = {column="entityLink",label="Website",orderby=true}>
		<cfset columnArray[6] = {column="",label="Add Organization",href="",onclick="viewOrganisation(0,'QuickCardAdd',{openInOwnTab:true, tabTitle:'Add Organization',iconClass:'accounts'}); return false;"}>
	</cfcase>

	<cfcase value="Location">
		<cfset extraColumns="l.address1,l.address2,l.address3,l.address4,l.address5,postalCode,l.countryID">
		<cfif url.orderBy eq "">
			<cfset URL.orderby = "entityName">
		</cfif>
		<cfset searchVariable = "frmSrchLocationID">

		<cfset columnArray[1] = {column="entityName",label="Location",orderby=true,linkToEntityName=true}>
		<cfset columnArray[2] = {column="address",label="Address"}>
		<cfset columnArray[3] = {column="countryId",label="Country"}>
	</cfcase>

	<cfcase value="person">
		<cfset extraColumns="o.OrganisationID,o.organisationTypeID,p.JobDesc,p.FirstName,p.LastName">
		<cfif url.orderBy eq "">
			<cfset URL.orderby = "lastName,firstName,organisationID">
		</cfif>
		<cfset searchVariable = "frmSearchPersonID">

		<cfset columnArray[1] = {column="lastname",label="Last Name",orderby=true,linkToEntityName=true}>
		<cfset columnArray[2] = {column="firstname",label="First Name",orderby=true}>
		<cfset columnArray[3] = {column="JobDesc",label="Job Description"}>
		<cfset columnArray[4] = {column="organisationTypeID",label="Org. Type"}>
		<cfset columnArray[5] = {column="entityLink",label="Email",orderby=true}>
	</cfcase>
	<cfdefaultcase>
		<!--- invalid table name passed --->
		<cf_abort showerror="Invalid tablesToSearch passed.">
	</cfdefaultcase>
</cfswitch>

<!--- <cfswitch expression="#URL.tablesToSearch#">
	<cfcase value="Organisation"> --->
		<cfset listCount = 0>
		<cfset qSearch=application.com.search.getSiteSearchResults(searchTextString=URL.searchTextString,tablesToSearch=URL.tablesToSearch,maxReturnedRows=500,extraColumns=extraColumns)>
		<cfif qSearch.RecordCount>

			<!--- to make ordering case insenstive, lower all the fields used in the order by clause --->
			<cfquery name="qSearch" dbtype="query">
				SELECT *
					<cfloop list="#url.orderBy#" index="orderByCol">,lower(#orderByCol#) as lower_#orderByCol#</cfloop>
				FROM qSearch
				ORDER BY <cfloop list="#url.orderBy#" index="orderByCol"><cfset listCount++>lower_#orderByCol#<cfif listCount lt listLen(url.orderBy)>,</cfif></cfloop> #URL.orderByDirection#
			</cfquery>

			<cfoutput>
				<div class="header">
					<h1>
						Search Results
					</h1>
				</div>
				<table class="entityListTable table table-striped table-hover">
					<tr>
						<cfloop array="#columnArray#" index="heading">
							<th>
								<cfoutput>
									<cfif structKeyExists(heading,"orderby")>
										<cfset heading.href = "?#getQueryString()#&orderBy=#heading.column#">
									</cfif>
									<cfif structKeyExists(heading,"href")>
										<a href="#heading.href#" <cfif structKeyExists(heading,"onclick")>onClick="#heading.onclick#"</cfif>>
									</cfif>
										#heading.label#
									<cfif structKeyExists(heading,"href")>
										</a>
									</cfif>
								</cfoutput>
							</th>
						</cfloop>
					</tr>
			</cfoutput>
			<cfloop query="qSearch">
				<cfset rowClass = currentrow mod 2 is not 0?"oddrow":"evenrow">
				<cfoutput>
					<tr class="#rowclass#">
						<cfloop array="#columnArray#" index="heading">
							<td>
								<cfset value = "">

								<!--- work out output for certain fields. By default, just output value from query --->
								<cfif heading.column neq "">
									<cfif heading.column eq "organisationTypeID">
										<cfset orgTypeIDValue = qSearch[heading.column]>
										<cfset value = application.organisationType[orgTypeIDValue].TypeTextID>
									<cfelseif heading.column eq "countryID">
										<cfset countryIDValue = qSearch[heading.column]>
										<cfset value = application.countryName[countryIDValue]>
									<cfelseif heading.column eq "address">
										<cfset value = application.com.screens.evaluateAddressFormat(location=qSearch,row=currentRow,countryID=countryID,showCountry=false)>
									<cfelse>
										<cfset value = qSearch[heading.column]>
									</cfif>
								</cfif>

								<cfif structKeyExists(heading,"linkToEntityName") and heading.linkToEntityName>
									<a href="" title="Edit #HTMLEditFormat(entityName)#" onclick="view#URL.tablesToSearch#(#entityID#,'',{openInOwnTab:true, tabTitle:'#JSStringFormat(entityName)#',iconClass:'accounts'});  return false;">#HTMLEditFormat(value)#</a></td>
								<cfelse>
									#HTMLEditFormat(value)#
								</cfif>
							</td>
						</cfloop>
					</tr>
				</cfoutput>
			</cfloop>
			<cfoutput>
				<tr>
					<th colspan="6">#qSearch.RecordCount#&nbsp; record<cfif qSearch.RecordCount is not 1>s</cfif> found</th>
				</tr>
			</table>
			<form name="searchStringForDashboard" action="dataFrame.cfm" method="post"> <input type="hidden" name="#searchVariable#" value="#valueList(qSearch.entityID)#"> </form>
			</cfoutput>
		<cfelse>
			<p>
				Can't find any records.
			</p>
		</cfif>
	<!--- </cfcase>

	<cfcase value="Person">
		<cfset qSearch=application.com.search.getSiteSearchResults(searchTextString=URL.searchTextString,tablesToSearch='Person',maxReturnedRows=500,extraColumns="o.OrganisationID,p.JobDesc,p.FirstName,p.LastName")>
		<cfif qSearch.RecordCount>
			<cfif listFindNoCase("firstname,lastname,entityLink",URL.orderby)>
				<cfif !listFindNoCase("asc,desc",URL.orderByDirection)>
					<cfset URL.orderByDirection = "asc">
				</cfif>
			<cfelse>
				<cfset URL.orderby = "lastName, firstName, organisationid">
				<cfset URL.orderByDirection = "">
			</cfif>
			<cfquery name="qSearch" dbtype="query">
				SELECT *
				FROM qSearch
				ORDER BY #URL.orderby# #URL.orderByDirection#
			</cfquery>
			<cfquery name="qOrgDetails" datasource="#application.siteDataSource#">
				SELECT organisationid, organisationName, organisationTypeID
				FROM organisation
				WHERE organisationID in (<cf_queryparam value="#valueList(qSearch.organisationid)#" cfsqltype="CF_SQL_INTEGER" list="true">)
			</cfquery>
			<cfset stOrgDetails = {}>
			<cfloop query="qOrgDetails">
				<cfset stOrgDetails[organisationid] = {}>
				<cfset stOrgDetails[organisationid].organisationName=organisationName>
				<cfset stOrgDetails[organisationid].organisationTypeID=organisationTypeID>
			</cfloop>
			<cfset queryString = getQueryString()>
			<cfoutput>
				<table class="entityListTable">
					<tr>
						<th>
							<a href="?#queryString#&orderby=lastName">
								Last Name
							</a>
						</th>
						<th>
							<a href="?#queryString#&orderby=firstName">
								First Name
							</a>
						</th>
						<th>
							Job Description
						</th>
						<th>
							Organisation
						</th>
						<th>
							Org. Type
						</th>
						<th>
							<a href="?#queryString#&orderby=entityLink">
								Email
							</a>
						</th>
					</tr>
			</cfoutput>
			<cfloop query="qSearch"> <cfif currentrow mod 2 is not 0><cfset rowclass="oddrow"><cfelse><cfset rowclass="evenrow"></cfif> <cfoutput> <tr class="#rowclass#"> <td><a href="" title="Edit #HTMLEditFormat(entityName)#" onclick="viewPerson(#entityID#,'',{openInOwnTab:true, tabTitle:'#JSStringFormat(entityName)#',iconClass:'accounts'}); return false">#HTMLEditFormat(lastName)#</a></td> <td>#HTMLEditFormat(firstName)#</td> <td>#HTMLEditFormat(JobDesc)#</td> <td><cfif structKeyExists(stOrgDetails,organisationID)>#htmlEditFormat(stOrgDetails[organisationid].organisationName)#<cfelse>N/A</cfif></td> <td><cfif structKeyExists(stOrgDetails,organisationID) and structKeyExists(application.organisationType,stOrgDetails[organisationid].organisationTypeID)>#HTMLEditFormat(application.organisationType[stOrgDetails[organisationid].organisationTypeID].TypeTextID)#</a><cfelse>N/A</cfif></td> <td>#HTMLEditFormat(entityLink)#</td> </tr> </cfoutput> </cfloop> <cfoutput> <tr><th colspan="6">#qSearch.RecordCount#&nbsp; record<cfif qSearch.RecordCount is not 1>s</cfif> found</th></tr> </table> <form name="searchStringForDashboard" action="dataFrame.cfm" method="post"> <input type="hidden" name="frmsrchPersonID" value="#valueList(qSearch.entityID)#"> </form> </cfoutput>
		<cfelse>
			<p>
				Can't find any records.
			</p>
		</cfif>
	</cfcase>
	<cfdefaultcase>
		<!--- invalid table name passed --->
		<cf_abort showerror="Invalid tablesToSearch passed.">
	</cfdefaultcase>
</cfswitch> --->
<cfset URL.pageTitle = 'Search Results: #HTMLEditFormat(URL.searchTextString)#'>
<cfoutput>
	<!--- make the whole tr clickable --->
	<script type="text/javascript">
	jQuery(document).ready(function() {
	    jQuery('table.entityListTable tr.oddrow,table.entityListTable tr.evenrow').click(function() {
	        jQuery(this).find("td > a").triggerHandler('click');
	    }).css('cursor','pointer').find("td > a").click(function(event){event.stopPropagation();})
	});
</script>
</cfoutput>

<cffunction name="getQueryString">
	<cfreturn "searchTextString=#URLEncodedFormat(URL.searchTextString)#&tablesToSearch=#URLEncodedFormat(URL.tablesToSearch)#&orderByDirection=#iif(URL.orderByDirection is 'asc',de('desc'),de('asc'))#">
</cffunction>
