<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			flagLocs.cfm	
Author:				NJH (based on flagOrgs.cfm)
Date started:		2011/06/04
	
Description:			

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed

Possible enhancements:


 --->

<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">
<cfparam name="openAsExcel" type="boolean" default="false">

<cfset application.com.request.setTopHead(pageTitle = "Locations")>
<cf_title>Location Flag List</cf_title>

<cfparam name="frmSortOrder" default="sortKey, Location">
<cfparam name="numRowsPerPage" default="200">
<cfparam name="startRow" default="1">
<cfparam name="filterSelectColumnList" default="Country">

<cfparam name="showDataTypeIDs" default="4,5,6">
<cfparam name="showFlagNameTypeIDs" default="2,3">


<CFQUERY NAME="getLocations" datasource="#application.siteDataSource#">
		select * from (
		SELECT distinct l.siteName as Location,  c.countryDescription as Country, l.countryID,l.locationID,c.ISOCode,
			l.siteName+'-'+cast(l.locationID as varchar(150)) as sortKey,
			LEFT( UPPER(l.siteName), 1 ) as alphabeticalIndex
			<cfif frmdataTableFullName neq "0">
				,b.Created as FlagCreatedDate
				<cfif isDefined("frmFlagTypeID") and listFind(showDataTypeIDs,frmFlagTypeID)>
				, b.data
				</cfif>
				<cfif isDefined("frmFlagIDList") and frmProfileID neq 0 and listLen(frmFlagIDList) gt 0>
				, f.name as FlagName
				</cfif>
			</cfif>
			<!--- NJH 2008/06/06 show the person name if a person flag and the sitename if a location--->
			<cfif frmentityType eq "Person">
				, p.firstname + ' '+p.lastname as person
			</cfif>
		 from location l with (noLock)
		 	inner join country c with (noLock) on l.countryId = c.countryID
		<cfif frmdataTableFullName neq "0">
			<CFIF frmentityType eq "organisation">
				INNER JOIN #frmdataTableFullName# b with (noLock) ON b.entityID = l.organisationID
			<cfelseif frmentityType eq "Person">
				INNER JOIN person p with (noLock) on p.organisationID = l.organisationID
				INNER JOIN #frmdataTableFullName# b with (noLock) ON b.entityID = p.personID
			<cfelseif frmentityType eq "Location">
				INNER JOIN #frmdataTableFullName# b with (noLock) ON b.entityID = l.locationID
			</cfif>
		INNER JOIN Flag f with (noLock) ON b.flagID = f.flagID and b.flagID in (#frmFlagIDfromFlagCount#<cfif isDefined("frmFlagIDList")>,#frmFlagIDList#</cfif>)
		</cfif>
		WHERE 1=1 
		<cfif frmFlagIDfromFlagCount neq "0" and frmEntityType neq "0" and frmdataTableFullName neq "0" >
		<!--- SWJ: 2002-03-02 this section is designed to deal with queries coming from FlagGroupCounts.cfm report
				whcih can pass any flag entity Type to this template.  Currently we only support orgs, locs and people --->
			<CFIF frmentityType eq "organisation">
			AND l.ORGANISATIONid IN (select entityID from #frmdataTableFullName# with (noLock) where FlagID  IN ( <cf_queryparam value="#frmFlagIDfromFlagCount#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ))
			<cfelseif frmentityType eq "Person">
			AND l.ORGANISATIONid IN (select OrganisationID FROM Person with (noLock) Where Personid 
				IN (select entityID from #frmdataTableFullName# with (noLock) where FlagID  IN ( <cf_queryparam value="#frmFlagIDfromFlagCount#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )))
			<cfelseif frmentityType eq "Location">
			AND l.ORGANISATIONid IN (SELECT OrganisationID FROM Location with (noLock) Where Locationid 
				IN (select entityID from #frmdataTableFullName# with (noLock) where FlagID  IN ( <cf_queryparam value="#frmFlagIDfromFlagCount#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )))
			</CFIF>
		</cfif>
		<cfif isDefined("FRMALPHABETICALINDEX") and FRMALPHABETICALINDEX neq "">
			and left(l.siteName,1) =  <cf_queryparam value="#FRMALPHABETICALINDEX#" CFSQLTYPE="CF_SQL_VARCHAR" >    
		</cfif>
		<cfif isDefined("frmFlagIDList") and frmProfileID neq "0" and listLen(frmFlagIDList) gt 0 and frmdataTableFullName neq "0">
			AND f.flagID  in ( <cf_queryparam value="#frmFlagIDList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			<cfset dontRun = true>
		</cfif>
		<cfif isDefined("frmFlagIDList") and listLen(frmFlagIDList) gt 0 and frmdataTableFullName neq "0" and not isDefined("dontRun")>
			AND f.flagID in (select flagID from #frmdataTableFullName# with (noLock) where FlagID  IN ( <cf_queryparam value="#frmFlagIDfromFlagCount#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ))
		</cfif>

		<cfif application.com.settings.getSetting("plo.countryScopeOrganisationRecords")>
		  	and l.countryID in (
			 SELECT r.CountryID FROM Rights AS r with (noLock), RightsGroup AS rg with (noLock), Country as c with (noLock)
				WHERE r.SecurityTypeID = (SELECT e.SecurityTypeID FROM SecurityType AS e with (noLock)
					WHERE e.ShortName = 'RecordTask')
				AND r.UserGroupID = rg.UserGroupID
				AND c.countryid = r.countryid
				AND rg.PersonID =  #request.relayCurrentUser.personID#
				AND r.Permission > 0
			 ) 
		 </cfif>

		) l
		WHERE 1=1 

		<cfinclude template="../templates/tableFromQuery-QueryInclude.cfm">
	ORDER BY <cf_queryObjectName value="#frmSortOrder#">
</cfquery>

<cfscript>
// create a structure containing the fields below which we want to be reported when the form is filtered
passThruVars = StructNew();
StructInsert(passthruVars, "frmFlagIDfromFlagCount", frmFlagIDfromFlagCount);
StructInsert(passthruVars, "frmEntityType", frmEntityType);
StructInsert(passthruVars, "frmdataTableFullName", frmdataTableFullName);
if (isDefined("frmProfileID")) { StructInsert(passthruVars, "frmProfileID", frmProfileID); }
if (isDefined("frmFlagIDList")) { StructInsert(passthruVars, "frmFlagIDList", frmFlagIDList); }
</cfscript>

<cfif isDefined("getLocations.FlagName")>
	<cfset lShowTheseColumns = "LOCATION,COUNTRY,FLAGNAME,FLAGCREATEDDATE">
<cfelse>
	<cfset lShowTheseColumns = "LOCATION,COUNTRY,FLAGCREATEDDATE">
</cfif>

<!--- NJH 2008/06/06 show the person name if a person flag--->
<cfif frmentityType eq "Person">
	<cfset lShowTheseColumns = listPrepend(lShowTheseColumns,"PERSON")>
</cfif>

<cfif isDefined("getLocations.data")>
	<cfset lShowTheseColumns = listAppend(lShowTheseColumns,"DATA")>
</cfif>

<CF_tableFromQueryObject 
	queryObject="#getLocations#"
	sortOrder = "#frmSortOrder#"
	numRowsPerPage="#numRowsPerPage#"
	startRow="#startRow#"
	
	keyColumnList="location"
	keyColumnURLList="javascript:void(viewLocation(thisUrlKey*comma91*comma{openInOwnTab:true}))"
	keyColumnKeyList="locationID"
	keyColumnOpenInWindowList="no"
	
	passThroughVariablesStructure = "#passThruVars#"

	allowColumnSorting="yes"
	showTheseColumns="#lShowTheseColumns#"
	dateFormat="FLAGCREATEDDATE"
	
	FilterSelectFieldList="#filterSelectColumnList#"
	
	alphabeticalIndexColumn="location"
	
	>
