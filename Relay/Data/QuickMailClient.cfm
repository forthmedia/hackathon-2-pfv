<!--- �Relayware. All Rights Reserved 2014 --->
<cf_title>Mail Client</cf_title>
<cf_body onLoad="javascript:document.mainForm.mailTo.focus()"/>

<!---

File name:		QuickMailClient.cfm
Author:			SWJ
Date created:	02 July 2000

	Objective - to provide a simple mail client which 

Date Tested:
Tested by:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
31 May 2001			SWJ			Made the application much more generic allowing you to specify 
								a variety of optional swithches e.g. showBCC, etc.
25 Oct 2001			CPS			Incorporate call to eWebEditPro if this is the default editor.
06-Nov-2003			KAP			popup address book
2006-03-04			SWJ			Changed phrases and buttons
2011/11/11 			PPB 		added a hook for Kerio so they can have a checkbox to control whether the signature should be printed at the bottom of the email 
2012/03/07			RMB			Added phase "phr_sys_emailfooterHTML" to append an email footer for the current users country
Enhancement still to do:

Javascript email address validation

--->


<cfsavecontent variable="headerScript">
<cfoutput>
<SCRIPT>
	function sendMessage()
	{
		var form = document.mainForm;
		var mailToVar = form.mailTo;
		var varFormOK = IsFormComplete('mainForm');
		var mailMsg = ""
		//alert(formOK);
		mailMsg = verifyEmail('mailTo');
		if (mailMsg != "") 
			{alert(mailMsg)};
		
		if (varFormOK == true && mailMsg == "")
			{form.submit()};
	}

function IsFormComplete(FormName){
	var x       = 0
	var FormOk  = true
	
	while ((x < document.forms[FormName].elements.length) && (FormOk))
	   {
	     if ((document.forms[FormName].elements[x].value == '') &&
		 	((document.forms[FormName].elements[x].name == 'mailTo') ||
		 	(document.forms[FormName].elements[x].name == 'Subject')))
	     { 
	        alert('phr_PleaseEnterThe '+document.forms[FormName].elements[x].name +' phr_andTryAgain.')
	        document.forms[FormName].elements[x].focus()
	        FormOk = false 
	     }
	     x ++
	   }
	return FormOk
}



// -->


function verifyEmail(objectName)  {
	
	thisObject = eval('document.mainForm.'+objectName) 
	thisValue = thisObject.value
	msg = ''	
	
	if (thisValue != '') {
		<!--- regular expression for testing Email --->
		NumberFormatRe = /^[\w_\.\-']+@[\w_\.\-']+\.[\w_\.\-']+$/
		<!---  test match with regular expression and --->
		if (NumberFormatRe.test(thisValue) == false) {
			msg = 'phr_pleaseEnterAnEmailAddressOfTheFormAbc@def.ghi \n'
		} 
	} 	

	return msg
}

function loadForm( recipientTo, recipientCc, recipientBcc )
{
	with ( document.forms["mainForm"] )
	{
		elements["mailTo"].value = recipientTo;
		elements["CCList"].value = recipientCc;
		elements["BCCList"].value = recipientBcc;
	}
}
	</SCRIPT>
<!--- --------------------------------------------------------------------- --->
<!--- popup address book --->
<!--- --------------------------------------------------------------------- --->
<SCRIPT type="text/javascript">
<!--
function addressBookPopUp()
{
	var windowUrl = "";
	var windowFeatures = "";
	windowFeatures += "alwaysLowered=0,";
	windowFeatures += "alwaysRaised=0,";
	windowFeatures += "dependent=1,";
	windowFeatures += "directories=0,";
	windowFeatures += "height=320,";
	windowFeatures += "hotkeys=0,";
	windowFeatures += "innerHeight=0,";
	windowFeatures += "innerWidth=0,";
	windowFeatures += "location=0,";
	windowFeatures += "menubar=0,";
	windowFeatures += "outerHeight=0,";
	windowFeatures += "personalbar=0,";
	windowFeatures += "resizable=0,";
	windowFeatures += "screenX=0,";
	windowFeatures += "screenY=0,";
	windowFeatures += "scrollbars=0,";
	windowFeatures += "status=0,";
	windowFeatures += "titlebar=0,";
	windowFeatures += "toolbar=0,";
	windowFeatures += "width=500,";
	windowFeatures += "z-lock=0,";

	windowUrl = "addressBook.cfm";
	windowUrl += "?recipientTo=";
	windowUrl += escape( document.forms["mainForm"]["mailTo"].value );
	windowUrl += "&recipientCc=";
	windowUrl += escape( document.forms["mainForm"]["CCList"].value );
	windowUrl += "&recipientBcc=";
	windowUrl += escape( document.forms["mainForm"]["BCCList"].value );

	window.open( windowUrl, "addressBook", windowFeatures );
}
//-->
</script>
<!--- --------------------------------------------------------------------- --->
</cfoutput>
</cfsavecontent>

<cfhtmlhead text="#headerScript#">

<CFPARAM NAME="frmMailTo" DEFAULT="">
<CFPARAM NAME="ShowBCC" DEFAULT="yes">
<CFPARAM NAME="ShowCC" DEFAULT="yes">
<CFPARAM NAME="ShowAttachment" DEFAULT="yes">

<CFPARAM NAME="dataCCList" DEFAULT="">
<CFPARAM NAME="dataBCCList" DEFAULT="">
<cfinclude template="/templates/qryGetUserName.cfm">





	
<CF_RelayNavMenu pageTitle="Mail Message" thisDir="/data">
	<CF_RelayNavMenuItem MenuItemText="phr_Send" CFTemplate="JavaScript:sendMessage();">
</CF_RelayNavMenu>

<CFFORM ACTION="QuickMailClientTask.cfm" METHOD="post" enctype="multipart/form-data" NAME="mainForm">
<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
		
		<TR>
		   	<TD ALIGN="right" CLASS="Label"><input type="button" value="phr_sys_to" onClick="javascript:void(addressBookPopUp());"></TD>
		   	<TD CLASS="Label"><CFOUTPUT><CF_INPUT TYPE="text" NAME="mailTo" VALUE="#frmMailTo#" SIZE="80" MAXLENGTH="100"></CFOUTPUT></TD>			
		</TR>
		<CFIF showCC NEQ "NO">
			<TR>
			   <TD ALIGN="right" CLASS="Label"><input type="button" value="phr_sys_cc" onClick="javascript:void(addressBookPopUp());"></TD>
			   <TD CLASS="Label"><INPUT TYPE="text" NAME="CCList" SIZE="80" MAXLENGTH="100"></TD>
			</TR>
		<CFELSEIF dataCCList neq "">
			<CFOUTPUT><CF_INPUT TYPE="hidden" NAME="CCList" VALUE="#dataCCList#"></CFOUTPUT>
		</CFIF>
		
		<CFIF showBCC NEQ "NO">
			<CFIF GetUserName.email neq frmMailTo>
				<CFSET dataBCCList = GetUserName.email>
			</CFIF>
			<TR>
			   <TD ALIGN="right" CLASS="Label"><input type="button" value="phr_sys_bcc" onClick="javascript:void(addressBookPopUp());"></TD>
			   <TD CLASS="Label"><cfoutput><CF_INPUT type="text" name="BCCList" value="#dataBCCList#" size="80" maxlength="100"></cfoutput></TD>
			</TR>
		<CFELSEIF dataBCCList neq "">
			<CFOUTPUT><CF_INPUT TYPE="hidden" NAME="BCCList" VALUE="#dataBCCList#"></CFOUTPUT>
		</CFIF>
	
		<TR>
		   <TD ALIGN="right" CLASS="Label">phr_sys_Subject:</TD>
		   <TD CLASS="Label"><CFINPUT TYPE="Text" NAME="Subject" MESSAGE="phr_pleaseIncludeASubject" REQUIRED="Yes" SIZE="80" MAXLENGTH="100"></TD>
		</TR>
		
		<CFIF ShowAttachment NEQ "no">
		<TR>
		   <TD ALIGN="right" CLASS="Label">phr_sys_Attachment:</TD>
		   <TD CLASS="Label"><INPUT type="File" name="Attachment" size="40"></TD>
		</TR>

		</CFIF>
		
		<TR>
		   <TD ALIGN="right" CLASS="Label">phr_sys_addToContactHistory:</TD>
		   <CFPARAM NAME="checkedVar" TYPE="string" DEFAULT="yes">
		   <CFIF GetUserName.email eq frmMailTo><CFSET checkedVar = "no"></CFIF>
		   <TD CLASS="Label"><CFINPUT TYPE="CheckBox" NAME="ContactHistory" MESSAGE="phr_createContactHistory" REQUIRED="Yes" CHECKED="#checkedVar#"></TD>
		</TR>	

		<!--- 2011/11/11 PPB START added a hook for Kerio so they can have a checkbox to control whether the signature should be printed at the bottom of the email --->
		<cfif fileexists("#application.paths.code#\cftemplates\quickMailClientFormExtension.cfm")>
			<cfinclude template="/code/cftemplates/quickMailClientFormExtension.cfm">
		</cfif>
		<!--- 2011/11/11 PPB END --->
		<CFOUTPUT>
			<CF_INPUT TYPE="hidden" NAME="frmPersonId" VALUE="#frmPersonId#">				
			<CF_INPUT TYPE="hidden" NAME="frmLocationId" VALUE="#frmLocationId#">
		</CFOUTPUT>	
		<!--- <CFIF NOT application. eWebEditProAsEditor> --->
		<TR>
		   	<TD COLSPAN="2" ALIGN="center">
				<TEXTAREA NAME="Message" ROWS="20" COLS="80"></TEXTAREA>
			</TD>
		</TR>
   		<!--- </CFIF> --->

		<TR>
		   	<TD COLSPAN="2" ALIGN="center">
	phr_sys_emailfooterHTML
			</TD>
		</TR>
	</TABLE>


</CFFORM>