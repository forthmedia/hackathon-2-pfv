<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
2012-10-04	WAB		Case 430963 Remove Excel Header 
 --->

<cfinclude template="../templates/tableFromQueryHeaderInclude.cfm">
<cfparam name="openAsExcel" type="boolean" default="false">

<cf_head>
	<cf_title>Lead|Opportunity List</cf_title>
</cf_head>


<cfinclude template="LeadManagerTopHead.cfm">

<cfparam name="sortOrder" default="Account_Manager">
<cfparam name="numRowsPerPage" default="250">
<!--- The keyColumn list group of params below control which columns show URL links
		where they link to and what key should be used in the URL --->
<cfparam name="keyColumnList" type="string" default=""><!--- this can contain a list of columns that can be edited --->
<cfparam name="keyColumnURLList" type="string" default=""><!--- this can contain a list of matching columns that contain the URL of the editor template --->
<cfparam name="keyColumnKeyList" type="string" default=""><!--- this can contain a list of matching key columns e.g. primary key --->

<cfparam name="radioFilterLabel" type="string" default=""><!--- this can contain a list of columns that can be edited --->
<cfparam name="radioFilterDefault" type="string" default=""><!--- this can contain a list of matching columns that contain the URL of the editor template --->
<cfparam name="radioFilterName" type="string" default=""><!--- this can contain a list of matching key columns e.g. primary key --->
<cfparam name="radioFilterValues" type="string" default=""><!--- this should contain a list of values for each radio button delimited by a minus sign --->

<cfparam name="dateFormat" type="string" default=""><!--- this list should contain those fields you want in the date format dd-mmm-yy --->
<cfparam name="entityID" type="numeric" default="0">
<cfparam name="frm_vendoraccountmanagerpersonid" type="numeric" default="0">

<cfparam name="checkBoxFilterName" type="string" default="">
<cfparam name="checkBoxFilterLabel" type="string" default="">

<cfparam name="pipelineCountryShowTheseColumns" type="string" default="Project_Stage,Opportunity_Stage,calculated_budget_this_quarter,calculated_budget_next_quarter,calculated_budget_quarter_plus2">
<cfparam name="pipelineAMOppStageIDs" type="string" default="1,2,3,4,5">

<cfset thisQuarter = quarter(now())>
<cf_DateRange type="quarter" value="current" year="#year(now())#">
<cf_CalcWeek CheckDate="#now()#" BeginDay="2" EndDay="6">

<cfquery name="getPipeline" datasource="#application.SiteDataSource#">
select 	PersonID, Account_Manager, Project_Stage, calculated_budget_this_quarter, 
		calculated_budget_next_quarter, calculated_budget_quarter_plus2,Opportunity_Stage,
		TwentyFive_Percent, TwentyFive_PercentN, TwentyFive_PercentN2,
		Fifty_Percent, Fifty_PercentN, Fifty_PercentN2,
		SeventyFive_Percent, SeventyFive_PercentN, SeventyFive_PercentN2,
		Ninety_Percent, Ninety_PercentN, Ninety_PercentN2,
		OneHundred_Percent, OneHundred_PercentN, OneHundred_PercentN2
from	vLeadAMPipeline
where 	OpportunityStageID  in ( <cf_queryparam value="#pipelineAMOppStageIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) 

<!--- 2012-07-23 PPB P-SMA001 commented out 
<cfif application.com.settings.getSetting("leadManager.countryScopeOpportunityRecords")>	
	and countryid in (#request.relaycurrentuser.countrylist#)
</cfif>
--->
	#application.com.rights.getRightsFilterWhereClause(entityType="opportunity",alias="vLeadAMPipeline").whereClause#		<!--- 2012-07-23 PPB P-SMA001 note: setting checked inside getRightsFilterWhereClause() --->

	<cfinclude template="../templates/tableFromQuery-QueryInclude.cfm">

	order by <cf_queryObjectName value="#sortOrder#">
</cfquery>

<cf_translate>
<cfoutput>
<table width="100%" cellpadding="3">
	<tr>
		<td><strong>Q#htmleditformat(thisQuarter)# #htmleditformat(year(now()))# Account Manager Pipeline Report</strong></td>
		<td align="right">Week #htmleditformat(week(now()))# (Week commencing #dateFormat(StartWeek,"dd-mmm-yy")#)</td>
	</tr>
</table>
<table>
	<tr>
		<td>
			This reports shows revenue for an Account Manager over the next three quarters. 
			You can view revenue based on Project Stage and expected % probability of the revenue coming in. 
		</td>
</tr>
</table>
</cfoutput>


<CF_tableFromQueryObject 
	queryObject="#getPipeline#"
	queryName="getPipeline"
	sortOrder = "#sortOrder#"
	numRowsPerPage="#numRowsPerPage#"
	keyColumnList="#keyColumnList#"
	keyColumnURLList="#keyColumnURLList#"
	keyColumnKeyList="#keyColumnKeyList#"
	showTheseColumns="Project_Stage,TwentyFive_Percent,Fifty_Percent,SeventyFive_Percent,Ninety_Percent,OneHundred_Percent,calculated_budget_this_quarter,TwentyFive_PercentN,Fifty_PercentN,SeventyFive_PercentN,Ninety_PercentN,OneHundred_PercentN,calculated_budget_next_quarter,TwentyFive_PercentN2,Fifty_PercentN2,SeventyFive_PercentN2,Ninety_PercentN2,OneHundred_PercentN2,calculated_budget_quarter_plus2"
	hideTheseColumns="PersonID,Account_Manager,Opportunity_Stage"
	dateFormat="#dateFormat#"
	columnTranslation="true"
	FilterSelectFieldList="Account_Manager"
	totalTheseColumns="TwentyFive_Percent,Fifty_Percent,SeventyFive_Percent,Ninety_Percent,OneHundred_Percent,calculated_budget_this_quarter,TwentyFive_PercentN,Fifty_PercentN,SeventyFive_PercentN,Ninety_PercentN,OneHundred_PercentN,calculated_budget_next_quarter,TwentyFive_PercentN2,Fifty_PercentN2,SeventyFive_PercentN2,Ninety_PercentN2,OneHundred_PercentN2,calculated_budget_quarter_plus2"
	GroupByColumns="Account_Manager"
	radioFilterLabel="#radioFilterLabel#"
	radioFilterDefault="#radioFilterDefault#"
	radioFilterName="#radioFilterName#"
	radioFilterValues="#radioFilterValues#"
	checkBoxFilterName="#checkBoxFilterName#"
	checkBoxFilterLabel="#checkBoxFilterLabel#"
	allowColumnSorting="no"
	currencyFormat="TwentyFive_Percent,Fifty_Percent,SeventyFive_Percent,Ninety_Percent,OneHundred_Percent,calculated_budget_this_quarter,TwentyFive_PercentN,Fifty_PercentN,SeventyFive_PercentN,Ninety_PercentN,OneHundred_PercentN,calculated_budget_next_quarter,TwentyFive_PercentN2,Fifty_PercentN2,SeventyFive_PercentN2,Ninety_PercentN2,OneHundred_PercentN2,calculated_budget_quarter_plus2"
>
</cf_translate>





		
		
