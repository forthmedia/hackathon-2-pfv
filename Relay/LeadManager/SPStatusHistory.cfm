<!--- �Relayware. All Rights Reserved 2014 --->



<cf_head>
	<cf_title>Special Pricing Status History</cf_title>
</cf_head>
<cfquery name="getOppPricingStatusHistory" datasource="#application.siteDataSource#">
	select opsh.comments, opsh.created, oppPricingStatus, p.firstname +' '+p.lastname as fullname 
	from oppPricingStatusHistory opsh
	inner join person p on p.personID = opsh.personid
	inner join oppPricingStatus ops ON ops.oppPricingStatusID = opsh.oppPricingStatusID
	where opportunityID =  <cf_queryparam value="#opportunityID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	order by opsh.created desc
</cfquery>


<cf_translate>
<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White">
	<tr>
		<th colspan="4" align="left">
			phr_SpecialPricingStatusHistorySection
		</th>
	</tr>
	<TR>
		<TH>phr_Added</TH>
		<TH>phr_Status</TH>
		<TH>phr_Person</TH>
		<TH>phr_Comments</TH>
	</TR>
	<CFOUTPUT QUERY="getOppPricingStatusHistory">
		<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
			<TD>#dateformat(created,"dd-mmm-yy")# #timeformat(created,"HH:mm:sss")#</TD>
			<TD>#htmleditformat(oppPricingStatus)#</TD>
			<TD>#htmleditformat(fullname)#</TD>
			<TD>#htmleditformat(comments)#</TD>
		</TR>
	</CFOUTPUT>
</TABLE>
</cf_translate>



