<!--- �Relayware. All Rights Reserved 2014 --->
<!---
Amendment History:
Date (YYYY-MM-DD)	Initials 	What was changed
2008-07-03			NYF			Bug 625
2010-02-25			NAS			P-LEX039 - Demo checkbox addition for 'Deal Registration'
2010-04-23			NJH			P-PAN002 Added multi-currency display to TFQO
2010-08-04			NJH			P-PAN002 - fixed rollup issues.
2010-08-26			NJH			8.3 - opened links in new tabs
2011-04-08			NYB			LHID6095 in smallLink href changed from 'if len eq 0' to 'if len gt 0' include request.query_string
2011-06-01 			PPB 		REL106 use countryScopeOpportunityRecords setting
2012/03/12 			IH	 		Case 426033 Remove duplicate content-Disposition header, set meaningful default Excel filenames
2012/03/27 			PPB 		P-LEX070 sortOrder is a setting
2012/03/28 			PPB 		P-LEX070 type-specific heading on listing
2012/04/17			IH			Case 425697 changed CFEXIT to showList=false. CFEXIT doesn't work with cf_relayHeaderFooter
2014-10-16 			PPB 		P-KAS040 add setting for searchColumnList
2015/03/02			NJH			Jira Fifteen-270- Show all oppTypes. Set oppType to 0 if not set, which then gets ignored in the filter.
2015/11/23			NJH			PROD2015-442 - removed the ship date filters, as they didn't appear to be working and looks like they hadn't for sometime. Removed as filters now more noticeable.
2016/03/23			NJH			Sugar 448762 - added created as a date filter
2016-06-27			DCC			case 450069 made setting and default 7 days originally limited/hardcoded to 1 day due to kaspersky timing out

Possible enhancements:

 --->

<cfsetting requesttimeout="1000">
<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">
<cfparam name="openAsExcel" type="boolean" default="false">

<cfset showList=true>

<!--- 2012-07-26 PPB P-SMA001 commented out
<cfif application.com.settings.getSetting("leadManager.countryScopeOpportunityRecords")>	<!--- 2011-06-01 PPB REL106 use countryScopeOpportunityRecords setting --->
	<cfinclude template="/templates/qryGetCountries.cfm">
</cfif>
--->

<cfparam name="sortOrder" default="#application.com.settings.getSetting('leadManager.leadAndOppList.sortOrder')#">		<!--- 2012/03/27 PPB P-LEX070 sortOrder is a setting --->
<cfparam name="opportunityFilterDaysShown" default="#application.com.settings.getSetting('leadManager.opportunityFilterDaysShown')#"> <!--- 2016-06-27 DCC case 450069 --->
<cfparam name="numRowsPerPage" default="50">
<cfparam name="startRow" default="1">

<CFPARAM NAME="ShowTheseOppStageIDs" DEFAULT="">
<!--- The keyColumn list group of params below control which columns show URL links
		where they link to and what key should be used in the URL --->
<cfparam name="keyColumnList" type="string" default="detail,account"><!--- this can contain a list of columns that can be edited --->
<!--- Start 2010-02-25			NAS			P-LEX039 - Demo checkbox addition for 'Deal Registration' --->
<cfparam name="keyColumnURLList" type="string" default=" , ">
<cfif isdefined("url.frmOppTypeID")>
	<!--- <cfparam name="keyColumnURLList" type="string" default="/leadManager/opportunityEdit.cfm?frmOppTypeID=#url.frmOppTypeID#&opportunityid=,../data/dataframe.cfm?frmsrchOrgID="> ---><!--- this can contain a list of matching columns that contain the URL of the editor template --->
	<cfparam name="keyColumnOnClickList" type="string" default="javascript:openNewTab('Opportunity Edit ##opportunity_ID##'*comma'Opportunity ##opportunity_ID##'*comma'/leadManager/opportunityEdit.cfm?frmOppTypeID=#url.frmOppTypeID#&opportunityid=##opportunity_ID##'*comma{iconClass:'leadmanager'});return false,javascript:openNewTab('Account Details ##entityID##'*comma'Account Details'*comma'/data/dataframe.cfm?frmsrchOrgID=##entityID##'*comma{iconClass:'accounts'});return false">
<cfelse>
	<!--- <cfparam name="keyColumnURLList" type="string" default="/leadManager/opportunityEdit.cfm?opportunityid=,../data/dataframe.cfm?frmsrchOrgID="> ---><!--- this can contain a list of matching columns that contain the URL of the editor template --->
	<cfparam name="keyColumnOnClickList" type="string" default="javascript:openNewTab('Opportunity Edit ##opportunity_ID##'*comma'Opportunity ##opportunity_ID##'*comma'/leadManager/opportunityEdit.cfm?opportunityid=##opportunity_ID##'*comma{iconClass:'leadmanager'});return false,javascript:openNewTab('Account Details ##entityID##'*comma'Account Details'*comma'/data/dataframe.cfm?frmsrchOrgID=##entityID##'*comma{iconClass:'accounts'});return false">
</cfif>
<!--- End 2010-02-25			NAS			P-LEX039 - Demo checkbox addition for 'Deal Registration' --->
<cfparam name="keyColumnKeyList" type="string" default="opportunity_ID,entityID"><!--- this can contain a list of matching key columns e.g. primary key --->

<cfparam name="radioFilterLabel" type="string" default=""><!--- this can contain a list of columns that can be edited --->
<cfparam name="radioFilterDefault" type="string" default=""><!--- this can contain a list of matching columns that contain the URL of the editor template --->
<cfparam name="radioFilterName" type="string" default=""><!--- this can contain a list of matching key columns e.g. primary key --->
<cfparam name="radioFilterValues" type="string" default=""><!--- this should contain a list of values for each radio button delimited by a minus sign --->

<cfparam name="dateFormat" type="string" default="last_Updated,Expected_Close_Date"><!--- this list should contain those fields you want in the date format dd-mmm-yy --->
<cfparam name="entityID" type="numeric" default="0">
<cfparam name="frm_vendoraccountmanagerpersonid" type="numeric" default="0">

<cfparam name="checkBoxFilterName" type="string" default="showComplete">
<cfparam name="checkBoxFilterLabel" type="string" default="Phr_Sys_Opp_ShowComplete">

<cfparam name="alphabeticalIndexColumn" type="string" default="Account">

<cfset excelFileName="opportunities.xls">

<!--- WAB 2008-04-30 added so that can work within entityFrame --->
<cfif (entityID is 0 or entityID is -1)>
	<cfif isDefined("frmOrganisationid")>
		<cfset entityID = frmOrganisationid>
	<cfelseif isDefined("frmEntityID")>
		<cfif isDefined("frmEntityTypeID") and listFind("0,1",frmEntityTypeID)>
			<cfquery name="getOrganisationID" datasource="#application.siteDataSource#">
				select organisationID from #application.entitytype[frmEntityTypeID].tablename# where #application.entitytype[frmEntityTypeID].uniqueKey# =  <cf_queryparam value="#frmEntityID#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfquery>
			<cfset entityID = getOrganisationID.organisationID>
		<cfelse>
			<cfset entityID = frmEntityID>
		</cfif>
	</cfif>
</cfif>

<cfparam name="queryString" type="string" default="">

<cfset application.com.request.setTopHead(queryString = queryString,topHeadCfm="/relay/leadmanager/leadManagerTophead.cfm")>

<!---
<cfset opptypes = application.com.opportunity.getOppTypes()>

<cfif isdefined("frmOppTypeID")>
	<cfquery name="getOppType" datasource="#application.siteDataSource#">
		select distinct * from opptype where opptypeID =  <cf_queryparam value="#frmOppTypeID#" CFSQLTYPE="CF_SQL_INTEGER" >
	</cfquery>
	<cfset topHeadVars.pageTitle = getOppType.OppTypeTextID>

	<!--- 2012/03/12 IH Case 426033 set meaningful default Excel filenames --->
	<cfquery dbtype="query" name="qThisOppType">
		SELECT OppTypeTextID
		FROM opptypes
		WHERE oppTypeID = <cfqueryparam value="#frmOppTypeID#" cfsqltype="cf_sql_integer">
	</cfquery>
	<cfif qThisOppType.recordCount>
		<cfset excelFileName=lCase(qThisOppType.OppTypeTextID) & "_opportunities.xls">
		<!--- START: 2012/03/28 PPB P-LEX070 type-specific heading on listing --->
		<cf_translate>
			<cfoutput>
				<cfif application.com.relayTranslations.doesPhraseTextIDExist('opp_LeadAndOppListHeading_#qThisOppType.oppTypeTextId#')>
						<h1 style="padding:10px;">phr_opp_LeadAndOppListHeading_#htmleditformat(qThisOppType.oppTypeTextId)#</h1></td>
				</cfif>
			</cfoutput>
		</cf_translate>
		<!--- END: 2012/03/28 PPB P-LEX070 --->
	<cfelse>
		<cfthrow message="Invalid #frmOppTypeID#">
	</cfif>
<cfelse>
	<cfif opptypes.recordcount GT 1 and !openAsExcel>
		<cfset request.relayFormDisplayStyle = "HTML-table">
		<cfinclude template="/templates/relayFormJavaScripts.cfm">


		<table class="withborder" align="center" style="margin: 0 auto">
		<tr>
			<th>phr_OpportunityType</th>
		</tr>
		<cfloop query="opptypes">
			<tr>
				<td>
					<cfoutput><a class="smallLink" href="/leadManager/leadAndOppList.cfm?<cfif len(request.query_string) gt 0>#htmleditformat(request.query_string)#</cfif>&frmOppTypeID=#htmleditformat(OpptypeID)#">#htmleditformat(OppType)#</a></cfoutput>
				</td>
			</tr>
		</cfloop>
		</table>

		<cfset showList=false>
	<cfelse>
		<cfset frmOppTypeID = opptypes.OpptypeID>
	</cfif>
</cfif> --->

<cfif showList>
	<!--- <cfinclude template="shipDateQueryWhereClause.cfm"> NJH 2007-05-29 --->
	<cfset phrasePrefix="ship">
	<!--- <cfinclude template="/relay/templates/DateQueryWhereClause.cfm">  NJH 2015/11/23 - PROD2015-442 - removed as this didn't ever appear to be working--->

	<cfscript>
	// call the opportunity component either directly or via a stub in userfiles/content/
	if (fileexists(application.paths.code & "\cftemplates\relayOpportunity.cfc")) {
		componentPath = "code.CFTemplates.relayOpportunity";
		myopportunity = createObject("component",componentPath);}
	else {myopportunity = createObject("component","relay.com.opportunity");}

	myopportunity.dataSource = application.siteDataSource;
	myopportunity.entityID = entityID;
	myopportunity.opportunityView = application.com.settings.getSetting("leadManager.opportunityView");
	/* 2012-07-26 PPB P-SMA001 commented out object.countryIDList is now redundant
	if (application.com.settings.getSetting("leadManager.countryScopeOpportunityRecords")) {
		myopportunity.countryIDList = CountryList;}
	 */
	// myopportunity.liveStatusList = liveStatusList;
	myopportunity.vendorAccountManagerPersonID = frm_vendoraccountmanagerpersonid;
	// if form.sortorder is defined use it otherwise set it a default value
	if (isdefined("form.sortOrder") and form.sortOrder neq "") {
		myopportunity.sortOrder = form.sortOrder;}
	else if	(isdefined("url.sortOrder") and url.sortOrder neq "") {
		myopportunity.sortOrder = url.sortOrder;}
	else {myopportunity.sortOrder = "#sortOrder#";}						//2012/03/27 PPB P-LEX070 sortOrder is a setting
	if (isdefined("FilterSelect") and FilterSelect neq ""
		and isdefined("FilterSelectValues") and FilterSelectValues neq "") {
		myopportunity.FilterSelect = FilterSelect;
		myopportunity.FilterSelectValues = FilterSelectValues;
		}
	if (isdefined("FilterSelect2") and FilterSelect2 neq ""
		and isdefined("FilterSelectValues2") and FilterSelectValues2 neq "") {
		myopportunity.FilterSelect2 = FilterSelect2;
		myopportunity.FilterSelectValues2 = FilterSelectValues2;
		}
	if (isdefined("radioFilterName") and radioFilterName neq "") {
		if (isDefined("form.radioFilterName")){
			myopportunity.radioFilterName = radioFilterName;
			myopportunity.radioFilterValue = form.radioFilterName;}
		else {
			myopportunity.radioFilterName = radioFilterName;
			myopportunity.radioFilterName = radioFilterDefault;}
		}
	if (isdefined("FORM.showComplete") and FORM.showComplete eq "1") {
		myopportunity.showComplete = "1";
		}
		else { myopportunity.showComplete = "0"; }
	if (isdefined("FORM.FRMWHERECLAUSEA")) {
		myopportunity.FRMWHERECLAUSE1 = FORM.FRMWHERECLAUSEA;
		}
	if (isdefined("FORM.FRMWHERECLAUSEB")) {
		myopportunity.FRMWHERECLAUSE2 = FORM.FRMWHERECLAUSEB;
		}
	if (isdefined("FORM.FRMWHERECLAUSEC")) {
		myopportunity.FRMWHERECLAUSE3 = FORM.FRMWHERECLAUSEC;
		}
	if (isdefined("FORM.FRMWHERECLAUSED")) {
		myopportunity.FRMWHERECLAUSE4 = FORM.FRMWHERECLAUSED;
		}
	myopportunity.alphabeticalIndexColumn = alphabeticalIndexColumn;
	myopportunity.alphabeticalIndex = form["frmAlphabeticalIndex"];
	myopportunity.ShowTheseOppStageIDs = ShowTheseOppStageIDs;
	if (isDefined("frmOppTypeID")) {
	myopportunity.oppTypeID = frmOppTypeID;
	} else {
		myopportunity.oppTypeID = 0;
	}
	if (not structKeyExists(form,"frmFromDate")) {
		form.dateFilter = "created";
		// MRE 2017-01-10 (ymd) RT-66 Fixing an overwrite of query filter with default date when exporting to Excel
		if (structKeyExists(url,"frmFromDate") and url.frmFromDate neq "") {
			form.frmFromDate=url.frmFromDate;
		} else {
			form.frmFromDate=dateAdd("d",-#opportunityFilterDaysShown#,request.requestTime);/*DCC case 450069 made setting and default 7 days*/
		}
	}
	if (not structKeyExists(form,"frmToDate")) {
		form.dateFilter = "created";
		if (structKeyExists(url,"frmToDate") and url.frmToDate neq "") {
			form.frmToDate=url.frmToDate;
		} else {
			form.frmToDate = request.requestTimeODBC;
		}
	}
	myopportunity.listOpportunitiesInternal();
	opportunityList = myOpportunity.qlistOpportunitiesInternal;
	</cfscript>

	<cfinclude template="opportunityFunctions.cfm">
	<cfinclude template="OpportunityListScreenFunctions.cfm">

	<cfparam name="IllustrativePriceColumns" default="">
	<cfif structkeyexists(request,"UseIllustrativePrice")>
		<cfset IllustrativePriceColumns = "CALCULATED_BUDGET">
	</cfif>

	<cfset leadAndOppListFilterSelectColumnList = application.com.settings.getSetting("leadManager.leadAndOppList.filterSelectColumnList")>
	<cfloop list="#leadAndOppListFilterSelectColumnList#" index="col">
		<cfif not listfindNoCase(opportunityList.columnList,col)>
			<cfset leadAndOppListFilterSelectColumnList = listDeleteAt(leadAndOppListFilterSelectColumnList,listFindNoCase(leadAndOppListFilterSelectColumnList,col))>
		</cfif>
	</cfloop>

	<!--- NJH 2015/03/04 - Jira Fifteen-268 - filter out any columns that are not in the actual query --->
	<cfset searchDisplayList = "">
	<cfset searchColumnDataTypeList = "">
	<cfset searchColumnList = application.com.settings.getSetting('leadManager.leadAndOppList.searchColumnList')>

	<cfloop list="#searchColumnList#" index="col">
		<cfif not listfindNoCase(opportunityList.columnList,col)>
			<cfset searchColumnList = listDeleteAt(searchColumnList,listFindNoCase(searchColumnList,col))>
		<cfelse>
			<cfset searchDisplayList = listAppend(searchDisplayList,"phr_sys_report_"&col)>
			<cfset dataType = 0>
			<cfif col eq "opportunity_id">
				<cfset dataType = 1>
			</cfif>
			<cfset searchColumnDataTypeList = listAppend(searchColumnDataTypeList,dataType)>
		</cfif>
	</cfloop>
	
	<CF_tableFromQueryObject
		queryObject="#opportunityList#"
		sortOrder = "#sortOrder#"
		numRowsPerPage="#numRowsPerPage#"
		keyColumnList="#keyColumnList#"
		keyColumnURLList="#keyColumnURLList#"
		keyColumnKeyList="#keyColumnKeyList#"
		keyColumnOpenInWindowList="no,no"
		keyColumnOnClickList="#keyColumnOnClickList#"
		dateFilter="created"
		hideTheseColumns="#application.com.settings.getSetting('leadManager.leadAndOppList.hideTheseColumns')#"
		showTheseColumns="#application.com.settings.getSetting('leadManager.leadAndOppList.showTheseColumns')#"
		dateFormat="#dateFormat#"
		columnTranslation="true"
		booleanFormat="Forecast"<!--- YMA 2013/01/06 2013 Roadmap - 108 - Forecast Opportunity --->

		<!--- queryWhereClauseStructure="#queryWhereClause#" --->

		rollUpCurrency="true" <!--- NJH 2010-04-23 P-PAN002 --->

		FilterSelectFieldList1="#leadAndOppListFilterSelectColumnList#"
		FilterSelectFieldList2="#leadAndOppListFilterSelectColumnList#"
		radioFilterLabel="#radioFilterLabel#"
		radioFilterDefault="#radioFilterDefault#"
		radioFilterName="#radioFilterName#"
		radioFilterValues="#radioFilterValues#"
		checkBoxFilterName="#checkBoxFilterName#"
		checkBoxFilterLabel="#checkBoxFilterLabel#"
		allowColumnSorting="yes"
		currencyFormat="#application.com.settings.getSetting('leadManager.leadAndOppList.currencyFormatColumnList')#"
		startRow="#startRow#"

		searchColumnList=#searchColumnList#        <!--- 2014-10-16 PPB P-KAS040 add setting for searchColumnList was "opportunity_id,account,product" --->
		searchColumnDataTypeList="#searchColumnDataTypeList#"
		searchColumnDisplayList="#searchDisplayList#"

		rowIdentityColumnName="opportunity_id"
		functionListQuery="#comTableFunction.qFunctionList#"

		alphabeticalIndexColumn="#alphabeticalIndexColumn#"
		useInclude = "False"
		openAsExcel = "#openAsExcel#"
		excelFileName="#excelFileName#"
		>
</cfif>