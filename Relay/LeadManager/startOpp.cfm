<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		startOpp.cfm	
Author:			SWJ  
Date started:	21-08-2007
	
Description:	This tag is used to start to create a new opportunity record		

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2008-05-19			AJC			If distilocation and country is left blank then causes an error 
2008-07-23			AJC			Issue 675: Problem with Leads on affinity 
Possible enhancements:


 --->
 
<cf_head>
	<cf_title>Add Project</cf_title>
</cf_head>



<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
	<TR class="Submenu">
		<TD ALIGN="LEFT" VALIGN="TOP" class="Submenu">Adding a project</TD>
	</TR>
	<tr><td>&nbsp;</td></tr>
</TABLE>

	
<cfset request.relayFormDisplayStyle = "HTML-table">
<cf_relayFormDisplay>
<!--- START: 2008-05-19 AJC If distilocation and country is left blank then causes an error --->
<cfif isDefined("form.distiLocationID") and (form.distiLocationID neq "" and form.countryID neq "")>
<!--- END: 2008-05-19 AJC If distilocation and country is left blank then causes an error --->
	
		<cfquery name="checkOpportunity" datasource="#application.siteDataSource#">
			select opportunityID 
			from opportunity 
			where countryID =  <cf_queryparam value="#form.countryID#" CFSQLTYPE="CF_SQL_INTEGER" >  
			and entityID =  <cf_queryparam value="#form.entityID#" CFSQLTYPE="CF_SQL_INTEGER" > 
			and distiLocationID =  <cf_queryparam value="#form.distiLocationID#" CFSQLTYPE="CF_SQL_INTEGER" > 
			and created =  <cf_queryparam value="#form.created#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
			and createdBy =  <cf_queryparam value="#form.createdBy#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfquery>
		
		<cfif checkOpportunity.recordCount eq 0>
			<cfinsert datasource="#application.siteDatasource#" tablename="opportunity" 
				formFields="countryID,entityID,distiLocationID,statusID,stageID,oppTypeID,vendorAccountManagerPersonID,partnerLocationID,lastUpdatedBy,lastUpdated,createdBY,created"> 
			<cfquery name="checkOpportunity" datasource="#application.siteDataSource#">
				select opportunityID 
				from opportunity 
				where countryID =  <cf_queryparam value="#form.countryID#" CFSQLTYPE="CF_SQL_INTEGER" >  
				and entityID =  <cf_queryparam value="#form.entityID#" CFSQLTYPE="CF_SQL_INTEGER" > 
				and distiLocationID =  <cf_queryparam value="#form.distiLocationID#" CFSQLTYPE="CF_SQL_INTEGER" > 
				and created =  <cf_queryparam value="#form.created#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
				and createdBy =  <cf_queryparam value="#form.createdBy#" CFSQLTYPE="CF_SQL_INTEGER" > 
			</cfquery>
		</cfif>
		
		
		<cfform  method="POST" name="mainForm"  >
			<h3>Is there an Associated Reseller?</h3>
			<cf_relayFormElementDisplay relayFormElementType="text" fieldname="resellerName" label="Enter the name of reseller  " currentValue="">
			<CF_relayFormElementDisplay relayFormElementType="hidden" currentValue="#checkOpportunity.opportunityID#" fieldName="opportunityID" label="">
			<cf_relayFormElementDisplay relayFormElementType="submit" fieldname="Search" label="" currentValue="Search for them">
			<cf_relayFormElementDisplay relayFormElementType="submit" fieldname="goToOppEdit" label="" currentValue="No Reseller - Next">
		</cfform>
	
	
<cfelseif isDefined("form.resellerName") and isDefined("form.opportunityID") and isDefined("form.search")>

	<cfquery datasource="#application.siteDataSource#" name="getResellers">
		select l.locationID, l.sitename +' '+ l.address4 +' '+ l.postalCode +' '+ c.countryDescription as resellerName
		from location l
		inner join country c ON c.countryID = l.countryID
		where sitename  like  <cf_queryparam value="#form.resellerName#%" CFSQLTYPE="CF_SQL_VARCHAR" > 
		order by sitename
	</cfquery>

	<h3>Choose the reseller associated with this project</h3>

	<CFFORM  METHOD="POST" NAME="SearchForm">
		
		
			<CFSELECT NAME="partnerLocationID"
		          SIZE="15"
		          MESSAGE="You must select an organisation"
		          QUERY="getResellers"
		          VALUE="locationID"
		          DISPLAY="resellerName"
		          REQUIRED="Yes">
			</CFSELECT>

		<INPUT TYPE="submit" NAME="goToOppEdit" VALUE="Next">

		<CFOUTPUT>
			<CF_INPUT TYPE="hidden" NAME="opportunityID" VALUE="#form.opportunityID#">
		</CFOUTPUT>
	</CFFORM>

<cfelseif isDefined("form.opportunityID") and isDefined("form.goToOppEdit")>
		<cfif isdefined('form.partnerLocationID') and form.partnerLocationID neq "">
		<cfupdate datasource="#application.siteDataSource#" tablename="opportunity" 
				formFields="partnerLocationID,opportunityID">
		</cfif>
		<cflocation url="opportunityEdit.cfm?opportunityID=#form.opportunityID#"addToken="false">

<cfelse>


	<cfquery datasource="#application.siteDataSource#" name="getDistis">
		select countryDescription as country, c.countryID,
		l.locationID, l.sitename +' '+ l.address4 +' '+ l.postalCode as distiName
		from location l
		inner join country c ON c.countryID = l.countryID
		where locationID in (select locationID from location 
			where organisationID in 
			(select entityId from booleanFlagData bfd
				where flagID  in ( <cf_queryparam value="#application.com.settings.getSetting("leadManager.distiLocFlagList")#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
			) 
		order by countryDescription,sitename
	</cfquery>
	
	
	
	<cfscript>
		defaultEntityID = "1";
		// create an instance of the opportunity component
if (fileexists(application.paths.code & "\cftemplates\relayOpportunity.cfc")) {
	componentPath = "code.CFTemplates.relayOpportunity";
	myopportunity = createObject("component",componentPath);}
else {myopportunity = createObject("component","relay.com.opportunity");}
		myopportunity.dataSource = application.siteDataSource;
		myopportunity.getDistiLocationID();
	</cfscript>
	
	<h3>Choose country and distributor for this project</h3>
	<!--- START: 2008-05-19 AJC If distilocation and country is left blank then causes an error --->
	<cfif isDefined("form.distiLocationID") and (form.distiLocationID eq "" or form.countryID eq "")>
		<div class="errorblock">A country and distributor must be selected in order to continue</span>
	</cfif>
	<!--- END: 2008-05-19 AJC If distilocation and country is left blank then causes an error --->
	
	<cfform  method="POST" name="mainForm"  >
		
			<!--- <cfscript>
				getCountries = application.com.RelayPLO.getCountries(countryIds=request.relayCurrentUser.CountryList);
			</cfscript> --->		
			
		<cf_twoselectsRelated
			query="getDistis"
			name1="countryID"
			name2="distilocationID"
			display1="Country"
			display2="distiName"
			value1="countryID"
			value2="LocationID"
			forcewidth1="30"
			forcewidth2="40"
			size1="1"
			size2="auto"
			autoselectfirst="no"
			emptytext1="(Select Country)"
			emptytext2="(Select Disti)"
			forcelength2 = "6"
			formname = "mainForm">	
			<!--- <CF_relayFormElementDisplay relayFormElementType="select" currentValue="#request.relayCurrentUser.countryID#" 	
					fieldName="CountryID" label="Phr_Sys_Country" query="#getCountries#" 
					display="Country" value="CountryID" nullText="Phr_Sys_SelectACountry" nullValue="" required = true>
			
			<cfif isDefined("showDisti") and showDisti eq "yes">
				<CF_relayFormElementDisplay relayFormElementType="select" currentValue="" 	
						fieldName="" label="phr_distiLocationID" query="#myopportunity.qDistiLocationID#" 
						display="itemText" value="ID" nullText="Phr_Sys_SelectACountry" nullValue="" required = true>
			</cfif> --->
				
			<CF_relayFormElementDisplay relayFormElementType="hidden" currentValue="#defaultEntityID#" fieldName="entityID" label="">
			<CF_relayFormElementDisplay relayFormElementType="hidden" currentValue="1" fieldName="statusID" label="">
			<CF_relayFormElementDisplay relayFormElementType="hidden" currentValue="1" fieldName="stageID" label="">
			<!--- oppTypeID needs to be set when startOpp is called initially --->
			<CF_relayFormElementDisplay relayFormElementType="hidden" currentValue="1" fieldName="oppTypeID" label="">
			<CF_relayFormElementDisplay relayFormElementType="hidden" currentValue="0" fieldName="partnerLocationID" label="">
			<CF_relayFormElementDisplay relayFormElementType="hidden" currentValue="#request.relayCurrentUser.personID#" fieldName="vendorAccountManagerPersonID" label="">
			<CF_relayFormElementDisplay relayFormElementType="hidden" currentValue="#request.relayCurrentUser.userGroupID#" fieldName="lastUpdatedBy" label="">
			<CF_relayFormElementDisplay relayFormElementType="hidden" currentValue="#now()#" fieldName="lastUpdated" label="">
			<CF_relayFormElementDisplay relayFormElementType="hidden" currentValue="#request.relayCurrentUser.userGroupID#" fieldName="createdBy" label="">
			<CF_relayFormElementDisplay relayFormElementType="hidden" currentValue="#now()#" fieldName="created" label="">
		
			<cf_relayFormElementDisplay relayFormElementType="submit" fieldname="" label="" currentValue="Next">
		
		</cfform>
	
</cfif>
</cf_relayFormDisplay>



