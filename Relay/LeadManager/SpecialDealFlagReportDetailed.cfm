<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 

2010-07-30	NJH	P-PAN002 - change to use rollUp reporting
2011-06-01 	PPB P-REL106 use countryScopeOpportunityRecords setting 
2012-10-04	WAB		Case 430963 Remove Excel Header 

 --->

<cfinclude template="../templates/tableFromQueryHeaderInclude.cfm">
<cfparam name="openAsExcel" type="boolean" default="false">

	<cf_head>
		<cf_title>Special Deal Report</cf_title>
	</cf_head>

<!--- 2012-07-23 PPB P-SMA001 commented out
<cfif application.com.settings.getSetting("leadManager.countryScopeOpportunityRecords")>	<!--- 2011-06-01 PPB REL106 use countryScopeOpportunityRecords setting --->
	<cfinclude template="/templates/qryGetCountries.cfm">
</cfif>
--->

<cfparam name="sortOrder" default="OPPORTUNITY_STATUS">
<cfparam name="numRowsPerPage" default="50">
<!--- The keyColumn list group of params below control which columns show URL links
		where they link to and what key should be used in the URL --->
<cfparam name="keyColumnList" type="string" default="detail,account"><!--- this can contain a list of columns that can be edited --->
<cfparam name="keyColumnURLList" type="string" default="opportunityEdit.cfm?opportunityid=,../data/dataframe.cfm?frmsrchOrgID="><!--- this can contain a list of matching columns that contain the URL of the editor template --->
<cfparam name="keyColumnKeyList" type="string" default="opportunity_ID,entityID"><!--- this can contain a list of matching key columns e.g. primary key --->

<cfparam name="radioFilterLabel" type="string" default=""><!--- this can contain a list of columns that can be edited --->
<cfparam name="radioFilterDefault" type="string" default=""><!--- this can contain a list of matching columns that contain the URL of the editor template --->
<cfparam name="radioFilterName" type="string" default=""><!--- this can contain a list of matching key columns e.g. primary key --->
<cfparam name="radioFilterValues" type="string" default=""><!--- this should contain a list of values for each radio button delimited by a minus sign --->

<cfparam name="dateFormat" type="string" default=""><!--- this list should contain those fields you want in the date format dd-mmm-yy --->
<cfparam name="entityID" type="numeric" default="0"> 
<cfparam name="frm_vendoraccountmanagerpersonid" type="numeric" default="0">

<cfparam name="productFilterSelectFieldList" type="string" default="SKU,Country,Account_Manager,Month_Due,Product_Group">

<cfparam name="checkBoxFilterName" type="string" default="">
<cfparam name="checkBoxFilterLabel" type="string" default="">

<cfparam name="alphabeticalIndexColumn" type="string" default="Account">

<!--- <cfinclude template="shipDateQueryWhereClause.cfm"> NJH 2007-05-29 --->
<cfset phrasePrefix="">
<cfset tableName="opportunity">
<cfset dateField="expectedCloseDate">
<cfinclude template="/relay/templates/DateQueryWhereClause.cfm">

<cfquery name="getProductReport" datasource="#application.SiteDataSource#">
select SKU, QUANTITY, VALUE, ACCOUNT, DETAIL, ENTITYID, ACCOUNT_MANAGER, COUNTRY, Product_Group,
OPPORTUNITY_STATUS, PRODUCT_PROBABILITY, FORECAST_SHIP_DATE, OPPORTUNITY_ID, Month_due,Countryid,
currency,
LEFT( UPPER( #alphabeticalIndexColumn# ), 1 ) as alphabeticalIndex
from vOppSpecialDealFlag
Where 1=1
	<!--- 2012-07-23 PPB P-SMA001 commented out
	<cfif application.com.settings.getSetting("leadManager.countryScopeOpportunityRecords")>	<!--- 2011-06-01 PPB REL106 use countryScopeOpportunityRecords setting --->
	and countryid  in ( <cf_queryparam value="#countryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	</cfif>
	 --->	
	#application.com.rights.getRightsFilterWhereClause(entityType="opportunity",alias="vOppSpecialDealFlag").whereClause#		<!--- 2012-07-23 PPB P-SMA001 note: countryScopeOpportunityRecords setting checked inside getRightsFilterWhereClause() --->
	
	<cfif isDefined("frm_vendoraccountmanagerpersonid") and frm_vendoraccountmanagerpersonid neq "0">
		and vendorAccountManagerPersonID =  <cf_queryparam value="#frm_vendoraccountmanagerpersonid#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</cfif> 
			<!--- <cfif isDefined("FRMWHERECLAUSEA") and FRMWHERECLAUSEA neq ""> 
				AND rtrim(datename(q, forecast_Ship_Date)) = #right(listfirst(FRMWHERECLAUSEA,"-"),1)#
					and datepart(yy, forecast_Ship_Date) = #listLast(FRMWHERECLAUSEA,"-")#
			</cfif>
			
			<cfif isDefined("FRMWHERECLAUSEB") and FRMWHERECLAUSEB neq ""> 
				AND left(datename(m, forecast_Ship_Date),3) = '#listfirst(FRMWHERECLAUSEB,"-")#'
					and datepart(yy, forecast_Ship_Date) = #listLast(FRMWHERECLAUSEB,"-")#
			</cfif>
			
			<cfif isDefined("FRMWHERECLAUSEC") and FRMWHERECLAUSEC neq "" and isDefined("FRMWHERECLAUSED") and FRMWHERECLAUSED eq ""> 
				AND forecast_Ship_Date >= '01-#listfirst(FRMWHERECLAUSEC,"-")#-#listLast(FRMWHERECLAUSEC,"-")#'
					
			</cfif>
			
			<cfif isDefined("FRMWHERECLAUSED") and FRMWHERECLAUSED neq "" and isDefined("FRMWHERECLAUSEC") and FRMWHERECLAUSEC eq ""> 
				AND forecast_Ship_Date < dateadd(m,1,'01-#listfirst(FRMWHERECLAUSED,"-")#-#listLast(FRMWHERECLAUSED,"-")#')
			</cfif>
			<cfif isDefined("FRMWHERECLAUSEC") and FRMWHERECLAUSEC neq "" and isDefined("FRMWHERECLAUSED") and FRMWHERECLAUSED neq ""> 
				AND forecast_Ship_Date >= '01-#listfirst(FRMWHERECLAUSEC,"-")#-#listLast(FRMWHERECLAUSEC,"-")#'
				AND	forecast_Ship_Date < dateadd(m,1,'01-#listfirst(FRMWHERECLAUSED,"-")#-#listLast(FRMWHERECLAUSED,"-")#')
			</cfif>  --->
						
	<cfinclude template="../templates/tableFromQuery-QueryInclude.cfm">
	
	order by <cf_queryObjectName value="#sortOrder#"> 
</cfquery>

<cfoutput>
<table width="100%" cellpadding="3">
	<tr>
		<td><strong>Special Deal Report</strong></td>
		<td align="right"></td>
	</tr>
	<tr><td colspan="2">
		</td></tr>
</table>
</cfoutput>

<cfinclude template="opportunityListScreenFunctions.cfm">

<cf_translate>
<CF_tableFromQueryObject 
	queryObject="#getProductReport#"
	queryName="getProductReport"
	sortOrder = "#sortOrder#"
	numRowsPerPage="#numRowsPerPage#"
	
	keyColumnList="#keyColumnList#"
	keyColumnURLList="#keyColumnURLList#"
	keyColumnKeyList="#keyColumnKeyList#"
	
	hideTheseColumns="opportunity_ID,ENTITYID,month_due,Countryid,Product_Group"
	showTheseColumns="SKU,QUANTITY,VALUE,ACCOUNT,DETAIL,ACCOUNT_MANAGER,COUNTRY,OPPORTUNITY_STATUS,PRODUCT_PROBABILITY,FORECAST_SHIP_DATE"
	dateFormat="FORECAST_SHIP_DATE"
	<!--- columnTranslation="#translateReportColumns#" --->
	
	FilterSelectFieldList="#productFilterSelectFieldList#"
	FilterSelectFieldList2="#productFilterSelectFieldList#"
	
	GroupByColumns="OPPORTUNITY_STATUS"
	
	radioFilterLabel="#radioFilterLabel#"
	radioFilterDefault="#radioFilterDefault#"
	radioFilterName="#radioFilterName#"
	radioFilterValues="#radioFilterValues#"
	checkBoxFilterName="#checkBoxFilterName#"
	checkBoxFilterLabel="#checkBoxFilterLabel#"
	allowColumnSorting="no"
	currencyFormat="value"
	
	<!--- alphabeticalIndexColumn="#alphabeticalIndexColumn#" --->

	queryWhereClauseStructure="#queryWhereClause#"
	rollUpCurrency="true"
	startRow="#iif( isdefined( "startRow" ), "startRow", 1 )#"
>
</cf_translate>



