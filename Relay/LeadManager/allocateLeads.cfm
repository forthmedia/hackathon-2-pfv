<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
2012-10-04	WAB		Case 430963 Remove Excel Header 
 --->

<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">
<cfparam name="openAsExcel" type="boolean" default="false">

	<cf_head>
		<cf_title>Allocate Leads</cf_title>
	</cf_head>
	
	
	<cfinclude template="LeadManagerTopHead.cfm">


<cfif isDefined("URL.prid")>
	<cfparam name="prid" default="autoAllocateLeads">
	<cfparam name="stid" type="numeric" default="0">
	
	<cfparam name="frmPersonID" default="#request.relayCurrentUser.personid#">
	<cfparam name="frmProcessID" default="#prid#">
	<cfparam name="frmStepID" default="#stid#"> 
	
	<!--- NJH 2009-06-30 P-FNL069 - commented out as file is removed due to security reasons
	<cfinclude template="/screen/setParameters.cfm"> --->
	<cfinclude template="/screen/redirector.cfm">

</cfif>


<cfparam name="sortOrder" default="account">
<cfparam name="numRowsPerPage" default="20">
<cfparam name="startRow" default="1">
<!--- The keyColumn list group of params below control which columns show URL links
		where they link to and what key should be used in the URL --->
<cfparam name="keyColumnList" type="string" default="detail,account"><!--- this can contain a list of columns that can be edited --->
<cfparam name="keyColumnURLList" type="string" default="opportunityEdit.cfm?opportunityid=,../data/dataframe.cfm?frmsrchOrgID="><!--- this can contain a list of matching columns that contain the URL of the editor template --->
<cfparam name="keyColumnKeyList" type="string" default="opportunity_ID,entityID"><!--- this can contain a list of matching key columns e.g. primary key --->

<cfparam name="radioFilterLabel" type="string" default=""><!--- this can contain a list of columns that can be edited --->
<cfparam name="radioFilterDefault" type="string" default=""><!--- this can contain a list of matching columns that contain the URL of the editor template --->
<cfparam name="radioFilterName" type="string" default=""><!--- this can contain a list of matching key columns e.g. primary key --->
<cfparam name="radioFilterValues" type="string" default=""><!--- this should contain a list of values for each radio button delimited by a minus sign --->

<cfparam name="dateFormat" type="string" default="last_Updated,Expected_Close_Date"><!--- this list should contain those fields you want in the date format dd-mmm-yy --->
<cfparam name="entityID" type="numeric" default="0">
<cfparam name="frm_vendoraccountmanagerpersonid" type="numeric" default="0">

<cfparam name="form.showUnalocated" default="1">
<cfparam name="checkBoxFilterName" type="string" default="showUnalocated">
<cfparam name="checkBoxFilterLabel" type="string" default="Show Unalocated?">

<cfparam name="alphabeticalIndexColumn" type="string" default="Account">
<cfscript>
// create an instance of the opportunity component
if (fileexists(application.paths.code & "\cftemplates\relayOpportunity.cfc")) {
	componentPath = "code.CFTemplates.relayOpportunity";
	myopportunity = createObject("component",componentPath);}
else {myopportunity = createObject("component","relay.com.opportunity");}
myopportunity.dataSource = application.siteDataSource;
myopportunity.entityID = entityID;
myopportunity.opportunityView = application.com.settings.getSetting("leadManager.opportunityView");
// myopportunity.liveStatusList = liveStatusList;
myopportunity.vendorAccountManagerPersonID = frm_vendoraccountmanagerpersonid;
// if form.sortorder is defined use it otherwise set it a default value
if (isdefined("form.sortOrder") and form.sortOrder neq "") {
	myopportunity.sortOrder = form.sortOrder;} 
else {myopportunity.sortOrder = "account";}
if (isdefined("FilterSelect") and FilterSelect neq ""
	and isdefined("FilterSelectValues") and FilterSelectValues neq "") {
	myopportunity.FilterSelect = FilterSelect;
	myopportunity.FilterSelectValues = FilterSelectValues;
	}
if (isdefined("radioFilterName") and radioFilterName neq "") {
	if (isDefined("form.radioFilterName")){
		myopportunity.radioFilterName = radioFilterName;
		myopportunity.radioFilterValue = form.radioFilterName;} 
	else {
		myopportunity.radioFilterName = radioFilterName;
		myopportunity.radioFilterName = radioFilterDefault;} 
	}
if (isdefined("FORM.showUnalocated") and FORM.showUnalocated eq "1") {
	myopportunity.showUnalocated = "1";
	} 
	else { myopportunity.showUnalocated = "0"; }
myopportunity.alphabeticalIndexColumn = alphabeticalIndexColumn;
myopportunity.alphabeticalIndex = form["frmAlphabeticalIndex"];
myopportunity.listOpportunitiesInternal();
opportunityList = myOpportunity.qlistOpportunitiesInternal;
</cfscript>


<CF_tableFromQueryObject 
	queryObject="#myopportunity.qlistOpportunitiesInternal#"
	sortOrder = "#sortOrder#"
	numRowsPerPage="#numRowsPerPage#"
	keyColumnList="#keyColumnList#"
	keyColumnURLList="#keyColumnURLList#"
	keyColumnKeyList="#keyColumnKeyList#"
	
	hideTheseColumns="entityID,vendorAccountManagerPersonID,opportunity_id,stageID,OPPPRICINGSTATUSID,SPREQUIREDAPPROVALLEVEL,SPCURRENTAPPROVALLEVEL,alphabeticalIndex"
	dateFormat="#dateFormat#"
	FilterSelectFieldList="Account,Partner_Name,Account_Manager,Stage,Region,status"
	
	radioFilterLabel="#radioFilterLabel#"
	radioFilterDefault="#radioFilterDefault#"
	radioFilterName="#radioFilterName#"
	radioFilterValues="#radioFilterValues#"
	checkBoxFilterName="#checkBoxFilterName#"
	checkBoxFilterLabel="#checkBoxFilterLabel#"
	
	allowColumnSorting="yes"
	currencyFormat="budget"
	startRow="#startRow#"
	
	alphabeticalIndexColumn="#alphabeticalIndexColumn#"
	
	useInclude="false"
>



