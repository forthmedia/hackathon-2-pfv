<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		pipelineReportDetailed.cfm
Author:			SWJ
Date started:	26-06-2008

Description:	This is a more detailed pipeline summary that lists deals based on closed date and subtotals by status

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
26-06-2008 	SWJ  	The report was erroring because it was sharing pipelineShow/hideTheseColumns and FilterSelectColumnList as variable to
				    control which columns should show at runtime with another report.  If this value included columns not in the report it errored.
					I have changed it to use it's own vars instead.  These can be overidden in the OpportunityINI.cfm
2009-08-19	NYB 	LHID2541
2009-08-25	NYB 	P-ATI002 - restrict results to a year ago
2009-09-15	NJH		P-ATI002 - Tidied up the queries as special was not a column name that could be used in the where statement when filtering.
2010-07-09	PPB		P-PAN002 - display and subtotal for multi-currency
2010-08-04	NJH		P-PAN002 - Fixed rollup issues.
2010-08-26	NJH		8.3 - open links in new tabs
2011-04-08 	NYB 	LHID6207 added copy of lines to second query that had incorrectly only been added to 1st query (in UNION)
2011-06-01 	PPB 	REL106 use countryScopeOpportunityRecords setting
2012/03/12 	IH	 	Case 426033 Remove duplicate content-Disposition header and set default filename for Excel

Possible enhancements:


 --->

<cfinclude template="../templates/tableFromQueryHeaderInclude.cfm">
<cfparam name="openAsExcel" type="boolean" default="false">
<cfset excelFileName="pipeLineDetailed.xls">

<!--- 2012-07-23 PPB P-SMA001 commented out
<cfif application.com.settings.getSetting("leadManager.countryScopeOpportunityRecords")>
	<cfinclude template="/templates/qryGetCountries.cfm">
</cfif>
--->

<cfparam name="sortOrder" default="account">
<cfparam name="numRowsPerPage" default="50">
<!--- The keyColumn list group of params below control which columns show URL links
		where they link to and what key should be used in the URL --->
<cfparam name="keyColumnList" type="string" default="detail,account"><!--- this can contain a list of columns that can be edited --->
<cfparam name="keyColumnURLList" type="string" default=" , "><!--- this can contain a list of matching columns that contain the URL of the editor template --->
<cfparam name="keyColumnOnClickList" type="string" default="javascript:openNewTab('Opportunity Edit ##opportunity_ID##'*comma'Opportunity ##opportunity_ID##'*comma'/leadManager/opportunityEdit.cfm?opportunityid=##opportunity_ID##'*comma{iconClass:'leadmanager'});return false,javascript:openNewTab('Account Details ##entityID##'*comma'Account Details'*comma'/data/dataframe.cfm?frmsrchOrgID=##entityID##'*comma{iconClass:'accounts'});return false">
<cfparam name="keyColumnKeyList" type="string" default="opportunity_ID,entityID"><!--- this can contain a list of matching key columns e.g. primary key --->

<cfparam name="radioFilterLabel" type="string" default=""><!--- this can contain a list of columns that can be edited --->
<cfparam name="radioFilterDefault" type="string" default=""><!--- this can contain a list of matching columns that contain the URL of the editor template --->
<cfparam name="radioFilterName" type="string" default=""><!--- this can contain a list of matching key columns e.g. primary key --->
<cfparam name="radioFilterValues" type="string" default=""><!--- this should contain a list of values for each radio button delimited by a minus sign --->

<cfparam name="dateFormat" type="string" default=""><!--- this list should contain those fields you want in the date format dd-mmm-yy --->
<cfparam name="entityID" type="numeric" default="0">
<cfparam name="frm_vendoraccountmanagerpersonid" type="numeric" default="0">

<cfparam name="checkBoxFilterName" type="string" default="">
<cfparam name="checkBoxFilterLabel" type="string" default="">

<!--- START: NYB 2009-08-19 LHID2541 - added: --->
<cfparam name="pipelineReportDetailedFilterSelectColumnList" default="Account,Account_Manager,Country,Status,Special">
<!--- END: NYB 2009-08-19 LHID2541 --->
<cfparam name="alphabeticalIndexColumn" type="string" default="Account">

<cfparam name="pipelinetotalTheseColumns" type="string" default="calculated_budget_this_quarter">

<cfparam name="ShowTheseOppStageIDs" type="string" default="1,2,3,4,5,7,9,10">

<cfparam name="ShowLiveOppStageIDs" type="string" default="1">

<cfset filterPipeLineBy = application.com.settings.getSetting("leadManager.pipelineReportDetailed.filterBy")>
<cfset opportunityView = application.com.settings.getSetting("leadManager.opportunityView")>

<cfset thisQuarter = quarter(now())>
<cf_DateRange type="quarter" value="current" year="#year(now())#">
<cf_CalcWeek CheckDate="#now()#" BeginDay="2" EndDay="6">

<!--- <cfset clauseCDefault = dateFormat(startdate,"mmm-yyyy")>
<cfset clauseDDefault = dateFormat(enddate,"mmm-yyyy")> --->

<!--- START: NYB 2009-08-25 P-ATI002 - removed: ---
<cfparam name="useFullRange" default="true">
!--- END: NYB 2009-08-25 P-ATI002 --->

<cfset dateField = "expectedCloseDate">


<cfset tableName = opportunityView>
<!--- <cfinclude template="shipDateQueryWhereClause.cfm"> NJH 2007-05-29 --->
<cfset phrasePrefix="ship">

<!--- START: NYB 2009-08-25 P-ATI002 - added: --->
<cfset usePartialRange="true">
<cfset PartialRangeStartDate = DateAdd("yyyy", -1, now())>
<!--- END: NYB 2009-08-25 P-ATI002 --->

<cfinclude template="/relay/templates/DateQueryWhereClause.cfm">

<cfset dateRangeFieldName="expectedCloseDate">

<!---
	P-ATI002 NJH 2009-09-15 wrapped the queries in selects so that special pricing is recognised as a column by TFQO.
	Also added expectedCloseDate, liveStatus, onlyShowInQuarter, countryID as columns to the inside select statements
	so that the where clause had columns that were available.
--->
<cfquery name="getPipeline" datasource="#application.SiteDataSource#">
	select * from (
		select expectedCloseDate, liveStatus, onlyShowInQuarter, countryID,
		account, detail, region, country, status,OppStageID, entityID, statusSortOrder, probability,
		expectedCloseDate as expected_close_date, opportunity_ID,unapproved_special_price,
		month_expected_close, Quarter_expected_close, Account_Manager,
		calculated_budget_this_quarter,currency,OVERALL_CUSTOMER_BUDGET,
		<!--- START: NYB 2009-08-19 LHID2541 - added: --->
		isNULL(Partner_Name,'&nbsp;') as Partner_Name,
		<!--- END: NYB 2009-08-19 LHID2541 --->
		LEFT( UPPER( #alphabeticalIndexColumn# ), 1 ) as alphabeticalIndex,
		<!--- START: PPB 2010-07-14 LID3663 defend against #opportunityView# being set to a view that doesn't return these columns --->
		<cfif opportunityView eq "vLeadListingCalculatedBudget">
		partnerLocationId,distiLocationId,
		</cfif>
		<!--- END: PPB 2010-07-14 LID3663 --->
		<!--- START: NYB 2009-08-25 P-ATI002 - added: --->
		'special' = case
			when pricing_status like 'spq%' then 'special price'
		else
			'standard price'
		end

		<!--- END: NYB 2009-08-25 P-ATI002 --->
		FROM #opportunityView#) as base1
		where onlyShowInQuarter = 1
		<!--- <cfif filterPipeLineBy eq "calculated_budget_this_quarter">
			and calculated_budget_this_quarter > 0
		</cfif> --->

		<cfif filterPipeLineBy eq "ExpectedCloseDate">
			and expectedCloseDate >=  <cf_queryparam value="#dateformat(StartDate,"mm-dd-yyyy")#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
			and expectedCloseDate <=  <cf_queryparam value="#dateformat(EndDate,"mm-dd-yyyy")#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
		<cfelseif filterPipeLineBy eq "calculated_budget_this_quarter">
			<!--- START: NYB 2009-08-25 P-ATI002 - added IsNULL around value: --->
			and isNULL(calculated_budget_this_quarter,0) > 0
			<!--- END: NYB 2009-08-25 P-ATI002 --->
		</cfif>

		<cfif isdefined( "form.frmAlphabeticalIndex" ) and form["frmAlphabeticalIndex"] neq "">
			AND LEFT( UPPER( #alphabeticalIndexColumn# ), 1 ) =  <cf_queryparam value="#form["frmAlphabeticalIndex"]#" CFSQLTYPE="CF_SQL_VARCHAR" >
		</cfif>

		<cfif isDefined("entityID") and entityID neq "0">
			and (entityID =  <cf_queryparam value="#entityID#" CFSQLTYPE="CF_SQL_INTEGER" >
			or partnerLocationID in (select locationID from location where organisationID =  <cf_queryparam value="#entityID#" CFSQLTYPE="CF_SQL_INTEGER" > )
			or distiLocationID in (select locationID from location where organisationID =  <cf_queryparam value="#entityID#" CFSQLTYPE="CF_SQL_INTEGER" > ))
		</cfif>

		<!--- 2012-07-23 PPB P-SMA001 commented out
		<cfif application.com.settings.getSetting("leadManager.countryScopeOpportunityRecords")>
			and countryid  in ( <cf_queryparam value="#countryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		</cfif>
		--->
		#application.com.rights.getRightsFilterWhereClause(entityType="opportunity",alias="base1").whereClause#		<!--- 2012-07-23 PPB P-SMA001 note: countryScopeOpportunityRecords setting checked inside getRightsFilterWhereClause() --->


		<cfif isDefined("frm_vendoraccountmanagerpersonid") and frm_vendoraccountmanagerpersonid neq "0">
			and vendorAccountManagerPersonID =  <cf_queryparam value="#frm_vendoraccountmanagerpersonid#" CFSQLTYPE="CF_SQL_INTEGER" >
					or opportunity_id in (SELECT r.recordid
											FROM recordRights r
											JOIN opportunity o on o.opportunityid = r.recordid
											WHERE r.UserGroupID = <cf_queryparam value="#request.relayCurrentUser.usergroupid#" cfsqltype="cf_sql_integer">
											AND r.Entity = 'opportunity'
											<cfif filterPipeLineBy eq "ExpectedCloseDate">
												and expectedCloseDate >=  <cf_queryparam value="#dateformat(StartDate,"mm-dd-yyyy")#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
												and expectedCloseDate <=  <cf_queryparam value="#dateformat(EndDate,"mm-dd-yyyy")#" CFSQLTYPE="CF_SQL_TIMESTAMP" >
											<cfelseif filterPipeLineBy eq "calculated_budget_this_quarter">
												<!--- START: NYB 2009-08-25 P-ATI002 - added IsNULL around value: --->
												and isNULL(calculated_budget_this_quarter,0) > 0
												<!--- END: NYB 2009-08-25 P-ATI002 --->
											</cfif>
		</cfif>

		<cfif isDefined("ShowTheseOppStageIDs")>
			and oppstageid  in ( <cf_queryparam value="#ShowTheseOppStageIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		</cfif>

		<cfinclude template="../templates/tableFromQuery-QueryInclude.cfm">

		<cfif isDefined("ShowLiveOppStageIDs")>
			and liveStatus  in ( <cf_queryparam value="#ShowLiveOppStageIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		</cfif>

		<cfif filterPipeLineBy eq "ExpectedCloseDate">
			UNION
				select * from (
				select expectedCloseDate, liveStatus, onlyShowInQuarter, countryID,
				account, detail, region, country, status, OppStageID, entityID, statusSortOrder, probability,
				expectedCloseDate as expected_close_date, opportunity_ID,unapproved_special_price,
				month_expected_close, Quarter_expected_close,Account_Manager, calculated_budget_this_quarter,currency,
					OVERALL_CUSTOMER_BUDGET,
				<!--- START: NYB 2009-08-19 LHID2541 - added: --->
				isNULL(Partner_Name,'&nbsp;') as Partner_Name,
				<!--- END: NYB 2009-08-19 LHID2541 --->
				LEFT( UPPER( #alphabeticalIndexColumn# ), 1 ) as alphabeticalIndex,
				<!--- START: NYB 2009-08-25 P-ATI002 - added: --->
				<!--- START: NYB 2011-04-08 LHID6207 added copy of lines added to first query by PPB 2010-07-14 LID3663 defend against #opportunityView# being set to a view that doesn't return these columns --->
				<cfif opportunityView eq "vLeadListingCalculatedBudget">
				partnerLocationId,distiLocationId,
				</cfif>
				<!--- END: NYB 2011-04-08 LHID6207 --->
				'special' = case
					when pricing_status like 'spq%' then 'special price'
				else
					'standard price'
				end
				<!--- END: NYB 2009-08-25 P-ATI002 --->
				FROM #opportunityView#) as base2
				WHERE onlyShowInQuarter = 0
				<cfif isdefined( "form.frmAlphabeticalIndex" ) and form["frmAlphabeticalIndex"] neq "">
					AND LEFT( UPPER( #alphabeticalIndexColumn# ), 1 ) =  <cf_queryparam value="#form["frmAlphabeticalIndex"]#" CFSQLTYPE="CF_SQL_VARCHAR" >
				</cfif>
				<cfif isDefined("entityID") and entityID neq "0">
					and (entityID =  <cf_queryparam value="#entityID#" CFSQLTYPE="CF_SQL_INTEGER" >
					or partnerLocationID in (select locationID from location where organisationID =  <cf_queryparam value="#entityID#" CFSQLTYPE="CF_SQL_INTEGER" > )
					or distiLocationID in (select locationID from location where organisationID =  <cf_queryparam value="#entityID#" CFSQLTYPE="CF_SQL_INTEGER" > ))
				</cfif>

				<!--- 2012-07-23 PPB P-SMA001 commented out
				<cfif application.com.settings.getSetting("leadManager.countryScopeOpportunityRecords")>	<!--- 2011-06-01 PPB REL106 use countryScopeOpportunityRecords setting --->
					and countryID in (#request.relaycurrentuser.countryList#)
				</cfif>
				 --->
				#application.com.rights.getRightsFilterWhereClause(entityType="opportunity",alias="base2").whereClause#		<!--- 2012-07-23 PPB P-SMA001 note: countryScopeOpportunityRecords setting checked inside getRightsFilterWhereClause() --->


				<cfif isDefined("frm_vendoraccountmanagerpersonid") and frm_vendoraccountmanagerpersonid neq "0">
					and vendorAccountManagerPersonID =  <cf_queryparam value="#frm_vendoraccountmanagerpersonid#" CFSQLTYPE="CF_SQL_INTEGER" >
				</cfif>
				<cfif isDefined("showComplete") and showComplete neq 0>
					AND liveStatus = 1
				</cfif>

			<cfinclude template="../templates/tableFromQuery-QueryInclude.cfm">

			<!--- and liveStatus = 1 --->
			</cfif>
			order by statusSortOrder, #sortOrder#
</cfquery>

<cfoutput>

		<h3>Q#htmleditformat(thisQuarter)# #htmleditformat(year(now()))# Detailed Pipeline Report - Week #htmleditformat(week(now()))# (Week commencing #dateFormat(StartWeek,"dd-mmm-yy")#)</h3>

		<cfif filterPipeLineBy eq "ExpectedCloseDate">
			<p>Opportunities with expected close dates from #dateformat(StartDate,"dd-mmm-yyyy")# - #dateformat(EndDate,"dd-mmm-yyyy")# inclusive</p>
		<cfelseif filterPipeLineBy eq "calculated_budget_this_quarter">
			<p>Opportunities with line items budgeted for this quarter i.e. between #dateformat(StartDate,"dd-mmm-yyyy")# and #dateformat(EndDate,"dd-mmm-yyyy")# inclusive</p>
		</cfif>
</cfoutput>

<cfinclude template="opportunityListScreenFunctions.cfm">

<!--- NYB 2009-08-19 LHID2541 - in CF_tableFromQueryObject replaced FilterSelectFieldList of
	leadAndOppListFilterSelectColumnList with pipelineReportDetailedFilterSelectColumnList --->
<CF_tableFromQueryObject
	queryObject="#getPipeline#"
	queryName="getPipeline"
	sortOrder = "#sortOrder#"
	numRowsPerPage="#numRowsPerPage#"

	keyColumnList="#keyColumnList#"
	keyColumnURLList="#keyColumnURLList#"
	keyColumnKeyList="#keyColumnKeyList#"
	keyColumnOnClickList="#keyColumnOnClickList#"

	hideTheseColumns="#application.com.settings.getSetting('leadManager.pipelineReportDetailed.hideTheseColumns')#"
	showTheseColumns="#application.com.settings.getSetting('leadManager.pipelineReportDetailed.showTheseColumns')#"
	dateFormat="expected_close_date"
	columnTranslation="true"

	FilterSelectFieldList="#pipelineReportDetailedFilterSelectColumnList#"
	FilterSelectFieldList2="#pipelineReportDetailedFilterSelectColumnList#"
	totalTheseColumns="#pipelinetotalTheseColumns#"
	GroupByColumns="status"

	radioFilterLabel="#radioFilterLabel#"
	radioFilterDefault="#radioFilterDefault#"
	radioFilterName="#radioFilterName#"
	radioFilterValues="#radioFilterValues#"
	checkBoxFilterName="#checkBoxFilterName#"
	checkBoxFilterLabel="#checkBoxFilterLabel#"
	allowColumnSorting="yes"
 	currencyFormat="CALCULATED_BUDGET_THIS_QUARTER"
	rollUpCurrency="true"
	alphabeticalIndexColumn="#alphabeticalIndexColumn#"
    useInclude = "False"
	startRow="#iif( isdefined( "startRow" ), "startRow", 1 )#"
	openAsExcel = "#openAsExcel#"
	excelFileName="#excelFileName#"
>