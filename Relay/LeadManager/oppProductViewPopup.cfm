<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 	Code to put a drop down box on the product list screen

		WAB 1999-04-19	

Amendment History
		WAB 1999-04-19 moved the delete selections function from a button to this menu
					added a function to create a selection of blank emails

2001-03-22		WAB		added items to dropdowns and at same time standardised on the 'checkboxFunctions' javascript					
					made some mods to form name and things

03-MAR-2005		AJC		CR_ATI003_currentprices

			 --->
	<cf_includejavascriptonce template = "/javascript/checkBoxFunctions.js">		
<SCRIPT type="text/javascript">
<!--
// original code by Bill Trefzger 1996-12-12
function go()	{

	if (document.selecter.select1.options[document.selecter.select1.selectedIndex].value != "none") 
		{
		location = document.selecter.select1.options[document.selecter.select1.selectedIndex].value
		document.selecter.select1.selectedIndex=0     //resets menu to top item
		}
}

function openWin( windowURL,  windowName, windowFeatures ) { 

	return window.open( windowURL, windowName, windowFeatures ) ; 
}

 function goOption(checksRequired, frmAction, myform, entityType)
{
	var form = eval('document.'+ myform);
	if(checksRequired && saveChecksNew(entityType, myform)=='0')
		noChecks(entityType)
	else
	{ 		
		newWindow = openWin( 'about:blank' ,'PopUp', 'left=' + parseInt(screen.availWidth * 0.4)  + ', width=' + parseInt(screen.availWidth * 0.4)  + ', top=' + parseInt(screen.availHeight * 0.3)  + ', height=' + parseInt(screen.availHeight * 0.3) + ',	toolbar=0,  location=0,	directories=0, status=0, menuBar=0,	scrollBars=1, resizable=1'); 
		form.frmAction.value = frmAction; 
		// Removed by GC - only submitting one page
		//if (checksRequired) form.frmTotalChecks.value=saveChecksNew(entityType, myform); 
		form.target='PopUp'; 
		form.action='oppProductUpdateTask.cfm';
		form.submit(); 
		<CFIF FindNoCase("msie", cgi.http_user_agent) GT 0>
		//temp	resetForm();
		<CFELSE>
		//temp  setInterval('resetForm()', 1000);
		</CFIF> 
		newWindow.focus();
	}
}
//-->
</SCRIPT>
<form name="selecter">
	<select name="select1" size=1 onchange="go()">
	<option value=none>Select a function
	<option value=none>--------------------
	<CFOUTPUT>
<option value=none>Product Updates
	<option value="javascript:goOption(true, 'Date', 'oppProductForm', 'Product');">&nbsp;Update checked product shipping date
	<option value="javascript:goOption(true, 'probability', 'oppProductForm', 'Product');">&nbsp;Update checked product probability
	<option value="javascript:goOption(true, 'integer', 'oppProductForm', 'Product');">&nbsp;Update checked product quantity
	<option value="javascript:goOption(true, 'latest', 'oppProductForm', 'Product');">&nbsp;Update to latest pricing
<option value=none>Checks
	<option value="JavaScript:doChecksNew('Product', true, 'oppProductForm');">&nbsp;Check all products on page
	<option value="JavaScript:doChecksNew('Product', false, 'oppProductForm');">&nbsp;Uncheck all products on page
<option value=none>Product Removal
	<option value="javascript:goOption(true, 'delete', 'oppProductForm', 'Product');">&nbsp;Remove checked products
	</CFOUTPUT>
	</select>
</form>
