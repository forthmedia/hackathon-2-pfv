<!--- �Relayware. All Rights Reserved 2014 --->
<!---
NJH 2010/07/31	P-PAN002 - converted report to use rollup reporting
2011/06/01 		PPB 		REL106 use countryScopeOpportunityRecords setting
 --->

<cfinclude template="../templates/tableFromQueryHeaderInclude.cfm">
<cfparam name="openAsExcel" type="boolean" default="false">

<cf_title>Opportunity Products by Opportunity</cf_title>

<!--- 2012-07-23 PPB P-SMA001 commented out
<cfif application.com.settings.getSetting("leadManager.countryScopeOpportunityRecords")>
	<cfinclude template="/templates/qryGetCountries.cfm">
</cfif>
--->

<cfparam name="sortOrder" default="opportunity_Name">
<cfparam name="numRowsPerPage" default="50">
<cfparam name="startRow" default="1">
<cfparam name="frm_vendoraccountmanagerpersonid" type="numeric" default="0">

<cfparam name="checkBoxFilterName" type="string" default="">
<cfparam name="checkBoxFilterLabel" type="string" default="">

<cfset thisQuarter = quarter(now())>
<cf_DateRange type="quarter" value="current" year="#year(now())#">
<cf_CalcWeek CheckDate="#now()#" BeginDay="2" EndDay="6">

<cfset clauseCDefault = dateFormat(startdate,"mmm-yyyy")>
<cfset clauseDDefault = dateFormat(enddate,"mmm-yyyy")>
<!--- <cfinclude template="shipDateQueryWhereClause.cfm"> NJH 2007/05/29 --->
<cfset phrasePrefix="ship">
<cfinclude template="/relay/templates/DateQueryWhereClause.cfm">

<cfset dateRangeFieldName="forecastShipDate">

<cfquery name="getOppProductsByOpp" datasource="#application.SiteDataSource#">
SELECT productid,countryID,forecastShipDate,forecast_Quarter,opportunity_Name,opportunityid,
Sub_Total,qnty_Products,product_Group, SKU,ISOCode,forecast_month,Product_Probability,status,
Account_Manager,Account,currency
from vOppProductsByOpp
	WHERE 1=1

	<!--- 2012-07-23 PPB P-SMA001 commented out
	<cfif application.com.settings.getSetting("leadManager.countryScopeOpportunityRecords")>	<!--- 2011/06/01 PPB REL106 use countryScopeOpportunityRecords setting --->
	and countryid  in ( <cf_queryparam value="#countryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	</cfif>
	--->
	#application.com.rights.getRightsFilterWhereClause(entityType="opportunityProduct",alias="vOppProductsByOpp").whereClause#		<!--- 2012-07-23 PPB P-SMA001 note: countryScopeOpportunityRecords setting checked inside getRightsFilterWhereClause() --->

	<cfif isDefined("frm_vendoraccountmanagerpersonid") and frm_vendoraccountmanagerpersonid neq "0">
		and opportunityid in (select opportunityID from opportunity where vendoraccountmanagerpersonid =  <cf_queryparam value="#frm_vendoraccountmanagerpersonid#" CFSQLTYPE="CF_SQL_INTEGER" > )
	</cfif>
	<cfinclude template="../templates/tableFromQuery-QueryInclude.cfm">
	order by <cf_queryObjectName value="#sortOrder#">
</cfquery>

<cfoutput>
<table width="100%" cellpadding="3">
	<tr>
		<td><strong>Q#htmleditformat(thisQuarter)# #htmleditformat(year(now()))# Opportunities by product</strong></td>
		<td align="right">Week #htmleditformat(week(now()))# (Week commencing #dateFormat(StartWeek,"dd-mmm-yy")#)</td>
	</tr>
	<tr><td colspan="2">Product from opportunities with forecast ship dates from #dateformat(StartDate,"dd-mmm-yy")# - #dateformat(EndDate,"dd-mmm-yy")# inclusive</td></tr>
</table>
</cfoutput>

<CF_tableFromQueryObject
	queryObject="#getOppProductsByOpp#"
	queryName="getOppProductsByOpp"
	sortOrder = "#sortOrder#"
	numRowsPerPage="#numRowsPerPage#"
	startRow="#startRow#"

	keyColumnList="opportunity_name"
	keyColumnURLList="opportunityEdit.cfm?opportunityid="
	keyColumnKeyList="opportunityid"

	showTheseColumns="Account,Account_Manager,forecast_month,ISOCode,Product_Probability,qnty_Products,Sku,status,Sub_Total"
	hideTheseColumns="opportunityid,product_Group,productid,countryID,forecastShipDate,forecast_Quarter,opportunity_Name"
	dateFormat=""
	columnTranslation="true"

	FilterSelectFieldList="Opportunity_Name,Product_Group,SKU,ISOCode,Forecast_month,Forecast_Quarter,Product_Probability,Status,Account_Manager"
	FilterSelectFieldList2="Opportunity_Name,Product_Group,SKU,ISOCode,Forecast_month,Forecast_Quarter,Product_Probability,Status,Account_Manager"
	totalTheseColumns="sub_total"
	GroupByColumns=""
	radioFilterLabel=""
	radioFilterDefault=""
	radioFilterName=""
	radioFilterValues=""
	checkBoxFilterName=""
	checkBoxFilterLabel=""
	allowColumnSorting="yes"
	currencyFormat="sub_total"
	rollUpCurrency="true"
>