<!--- �Relayware. All Rights Reserved 2014 --->
<!---
2012/05/31	PPB P-MIC001 show the url message only ONCE (not js and cf) and when the list has loaded
2014-01-31	WAB	removed references to et cfm, can always just use / or /?
2014-03-10	PPB Case 439121 clicking Add New from within a CustomerController tab should create opp of the relevant OppType
2014-10-22	RPW	Check if starttab already exists in the query string before adding it.
2016/12/12	NJH JIRA PROD2016-2937 - remove the reseller OrgID parameter. Now done in rights.
--->

	<cfset request.document.addHTMLDocumentTags = false>
	<cfif not application.com.security.confirmFieldsHaveBeenEncrypted(fieldNames="orgid")>
	      <CF_ABORT>
	</cfif>


	<cfparam name="inIframe" default=0>			<!--- if called from a CustomerController Tab will be 1 --->

	<cf_translate>
		<cfif inIFrame eq 1>
			<cfscript>
				//2014-10-22	RPW	Check if starttab already exists in the query string before adding it.
				variables.httpReferer = CGI.HTTP_REFERER;
				variables.refererPath = ListFirst(variables.httpReferer,"?");
				variables.qString = ListLast(variables.httpReferer,"?");

				variables.cleanQString = "";
				for (variables.i=1;variables.i <= ListLen(variables.qString,"&");variables.i++) {
					if (ListFirst(ListGetAt(variables.qString,variables.i,"&"),"=") != "starttab") {
						variables.cleanQString = ListAppend(variables.cleanQString,ListGetAt(variables.qString,variables.i,"&"),"&");
					}
				}

				if (Len(variables.cleanQString)) {
					variables.returnURL = variables.refererPath & "?" & variables.cleanQString & "&starttab=" & oppType;
				} else {
					variables.returnURL = variables.refererPath & "?starttab=" & oppType;
				}
			</cfscript>
		<cfelse>
			<cfset returnURL = "#cgi.script_name#?#request.query_string#">
		</cfif>

		<table width="100%">
			<tr>
<!--- 2012/05/31 PPB P-MIC001 instead of showing the message twice (js and cf) now only show js so put it at the bottom so list is filled first
				<td>
					<cfif StructKeyExists(URL,"message") and message neq "">
						<cfoutput>
							<script type="text/javascript">
								alert('#jsStringFormat(message)#');
							</script>
							#htmleditformat(url.message)#
						</cfoutput>

						<cfset returnURL = application.com.globalFunctions.removeURLToken(returnURL,"message")>		<!--- having displayed the message strip it out of the returnURL so that we don't pass it through and see it again when we return to this page --->
					</cfif>
				</td>
--->
<!--- 2012/05/31 PPB P-MIC001 we still want to remove message from the returnURL before using it --->
				<cfif StructKeyExists(URL,"message") and message neq "">
					<cfset returnURL = application.com.globalFunctions.removeURLToken(returnURL,"message")>		<!--- having displayed the message strip it out of the returnURL so that we don't pass it through and see it again when we return to this page --->
				</cfif>


				<td align="right">
					<!--- START:	2012/08/09	MS		P-KAS002 	Hide the Add new Button for my Customers New Opportunities Tab when SHOWADDNEWENDCUSTBUTTON argument passed in the RELAY_OPPCUSTOMERLIST RelayTag set as false --->
					<cfif ShowAddNewEndCustButton eq true>

					<cfset urlString = "#application.com.security.encryptURL(url='/et.cfm?eid=oppEdit&opportunityID=0&entityid=#session.customerOrgID#&oppTypeId=#oppType#&returnURL=#urlencodedformat(returnURL)#')#">		<!--- 2014-03-10 PPB Case 439121 add oppTypeId --->

					<CF_INPUT type="button" onClick="javascript:parent.location.href='#urlString#';" value="phr_AddNew"/>

					<cfelse>
						&nbsp;
					</cfif>
					<!--- END:	2012/08/09	MS		P-KAS002 	Hide the Add new Button for my Customers New Opportunities Tab --->
				</td>
			</tr>
		</table>

		<cfparam name="startRow" default="1">
		<cf_param name="numRowsPerPage" default="100"/>
		<cfparam name="oppType" default="Deals">

		<cf_param name="sortOrder" default="o.opportunityID"/>
		<cfset qryOpportunities = application.com.opportunity.getCustomerControllerOpportunities(organisationID=session.customerOrgID,opportunityTypes=oppType,sortOrder=sortOrder)>

		<cfoutput>
		<script>
			function editOpp (params) {
				parent.location.href='/?eid=oppEdit&' + params
			}
		</script>
		</cfoutput>

		<cfif application.com.settings.getSetting("leadManager.products.useOppProductTotals")>
			<cfparam name="showColumns" default="opportunityDescription,TotalValue,ExpectedCloseDate,EditButton">
			<!--- <cfset columnHeadings = "Description,Value,ExpectedCloseDate,blank">
			<cfset showColumns = "opportunityDescription,TotalValue,ExpectedCloseDate,EditButton"> --->
		<cfelse>
			<cfparam name="showColumns" default="opportunityDescription,ExpectedCloseDate,EditButton">
			<!--- <cfset columnHeadings = "Description,ExpectedCloseDate,blank">
			<cfset showColumns = "opportunityDescription,ExpectedCloseDate,EditButton"> --->
		</cfif>

		<cfif qryOpportunities.recordCount gt 0>
			<CF_tableFromQueryObject
				queryObject="#qryOpportunities#"
				sortOrder = "#sortOrder#"
				startRow = "#startRow#"
				openAsExcel = "false"
				numRowsPerPage="#numRowsPerPage#"
				totalTheseColumns=""
				GroupByColumns=""
				ColumnTranslation="true"
			    ColumnTranslationPrefix="phr_opp_"

				showTheseColumns="#showColumns#"
				FilterSelectFieldList=""
				FilterSelectFieldList2=""
				hideTheseColumns=""
				numberFormat=""
				dateFormat="ExpectedCloseDate"
				showCellColumnHeadings="no"
				keyColumnURLList=" "
				keyColumnOnClickList="editOpp('##application.com.security.encryptQueryString('returnURL=#urlencodedformat(returnURL)#&opportunityid=##opportunityid##')##')"
				keyColumnKeyList="opportunityID"
				keyColumnTargetList=""
				keyColumnList="EditButton"
				keyColumnOpenInWindowList="no"
				OpenWinSettingsList=""
				useInclude = "false"
				allowColumnSorting = "true"
				currencyFormat="TotalValue"
				CurrencyISOColumn="currency"
				KeyColumnLinkDisplayList="button"
			>
		<cfelse>
			phr_opp_NoOpportunitiesToList
		</cfif>

		<!--- 2012/05/31 PPB P-MIC001 only show js message so put it at the bottom so list is filled first (previously displayed higher) --->
		<!--- <cfif StructKeyExists(URL,"message") and message neq "">
			<cfoutput>
				<script type="text/javascript">
					alert('#jsStringFormat(message)#');
				</script>
			</cfoutput>
		</cfif> --->


	</cf_translate>
