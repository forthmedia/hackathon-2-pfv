<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		setOpportunityDisplay.cfm
Author:			NJH
Date started:	01-04-2010

Description:	A file to work out what fields should be displayed and how.

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2010-05-20			AJC			LID 3444 - Account Manager was not being set by default
2010-06-08			NAS			P-CLS003 - Changed this field to a param so it can be included into the oppini.cfm file
2011/05/23 			PPB 		P-REL106 - add a level to opportunity xml so that each oppType can have a different configuration
2012-05-22 			PPB 		P-LEX070 - add countryISOCode to displayStruct
2012-08-24 			PPB 		ENP001 - disableToDate removed
2012-11-09       	IH       	P-LEN081 CR101 - Lenovo Opportunity App add set AccSalesRep, FieldRep as vendorAccountManagerPersonID if AccManager is blank
2013-02-06			IH			Case 433487 make distiLocationID editable
2013-12-05 			NYB 		Case 437286 added line that existed on the production version but was missing on repository version
2014-02-28 			PPB 		Case 438969 changed a reference from opp.oppType to opp.oppTypeTextId
2014-03-14 			PPB 		Case 439060 CountryId is EndCustomer country not CurrentUser country
2014-04-28	                MS	        Case. 439476 Need to "lock down" opportunity on RW side if the stage is "Closed/Won"
2014-10-28			RPW			CORE-666 OPP Edit Screen needs multiple XML Changes
2014-11-24 			AXA 		CORE-117 added check for new settings to switch flag for vendoraccountmanager
Possible enhancements:


 --->

<cfset displayStruct = structNew()>
<cfset oppReadOnly = false> <!--- lock down the details of opportunity if... --->
<cfset partnerTier = "">
<cfset displayStruct.showSystemStages = false>
<cfset displayStruct.liveStatus = true><!--- 2014-10-28	RPW	CORE-666 OPP Edit Screen needs multiple XML Changes --->
<cfset displayStruct.currentEntity.entityID=form.opportunityID>
<cfset displayStruct.currentEntity.entityTypeID = application.entityTypeID["opportunity"]>


<!--- 2010-06-08			NAS			P-CLS003 - Changed this field to a param so it can be included into the oppini.cfm file --->
<cfparam name="ShowDealRegistration" default="true">
<cfparam name="showBtnSaveOpp" default="true"> <!--- START: 2014-04-28	MS	Case. 439476 Need to "lock down" opportunity on RW side if the stage is "Closed/Won" --->
<cfparam name="showBtnApplyForSpecialPricing" default="true"> <!--- START: 2014-04-28	MS	Case. 439476 Need to "lock down" opportunity on RW side if the stage is "Closed/Won" --->

<!--- initialising the display structure with values from the DB --->
<cfloop list="#structKeyList(oppDetailsStruct)#" index="fieldname">
	<cfset displayStruct[fieldname] = structNew()>
	<cfset displayStruct[fieldname].currentValue = oppDetailsStruct[fieldname]>
	<cfset displayStruct[fieldname].readonly = oppReadOnly>
</cfloop>

<!--- values to set if the opportunity is new --->
<cfif form.opportunityID eq 0>
	<cfset displayStruct.opportunityID.currentValue = 0>
	<cfset displayStruct.oppPricingStatusID.currentValue = 1>
	<cfset displayStruct.distiLocationID.currentValue = "">
	<cfset displayStruct.stageID.currentValue = "">
	<cfset displayStruct.statusID.currentValue = "">
	<cfset displayStruct.countryID.currentValue = "">

	<cfif isDefined("entityID")>
		<cfset displayStruct.entityID.currentValue = entityID>
		<cfquery name="getEntityCountry" datasource="#application.siteDataSource#">
			select countryId from organisation where organisationId =  <cf_queryparam value="#entityID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>
		<cfset displayStruct.countryID.currentValue = getEntityCountry.countryID>
	</cfif>

	<cfif isDefined("contactPersonID")>  <!--- this should only be defined coming from the addContactForm. If so, then we want to set the contact to be the person we have just added. --->
		<cfset displayStruct.contactPersonID.currentValue=contactPersonID>
	</cfif>

	<!--- if opptypeid is passed in use it - low risk but this should be encrypted --->
	<cfif structkeyexists(URL,"oppTypeID")>
		<cfif not isNumeric(url.oppTypeID)>
			<cfset url.oppTypeId=application.com.opportunity.getOppTypes(oppTypeTextID=url.oppTypeID).oppTypeId>
		</cfif>
		<cfset displayStruct.oppTypeID.currentValue = url.oppTypeID>
	<cfelseif structkeyexists(form,"oppTypeID")>
		<cfset displayStruct.oppTypeID.currentValue = form.oppTypeID>
	<cfelse>
	<!--- default to lead on internal, deal on the portal --->
		<cfquery name="getOppType" datasource="#application.siteDataSource#">
			select oppTypeID from oppType where oppTypeTextID =  'Deals'
		</cfquery>
		<cfset displayStruct.oppTypeID.currentValue = getOppType.oppTypeID>
	</cfif>

	<!--- set some default variables --->
	<cfif not request.relayCurrentUser.isInternal>
		<!--- <cfset displayStruct.countryID.currentValue = request.relayCurrentUser.countryID> ---> <!--- 2014-03-14 PPB Case 439060 commented out this line; CountryId is EndCustomer country not CurrentUser country --->
		<cfset displayStruct.countryID.relayFormElementType="hidden">
		<cfset displayStruct.partnerLocationID.currentValue = request.relayCurrentUser.person.locationID>
		<cfset displayStruct.partnerSalesPersonID.currentValue = request.relayCurrentUser.personID>
		<!--- START: 2010-05-20 AJC LID 3444 - Account Manager was not being set by default --->
		<!--- 2012-11-09 IH P-LEN081 CR101 - Lenovo Opportunity App add set AccSalesRep, FieldRep as vendorAccountManagerPersonID if AccManager is blank --->

		<cfset partnerAccountManager = application.com.opportunity.getPartnerAccountManager()>

		<cfif partnerAccountManager neq 0>
			<cfset displayStruct.vendorAccountManagerPersonID.currentValue = partnerAccountManager><!--- 2014-11-24 AXA CORE-117 added check for new settings to switch flag for vendoraccountmanager --->
		</cfif>

		<!--- END: 2010-05-20 AJC LID 3444 - Account Manager was not being set by default --->
	<cfelse>
		<cfset displayStruct.partnerLocationID.currentValue = "">
		<cfset displayStruct.partnerSalesPersonID.currentValue="">
	</cfif>
</cfif>

<!--- get the special pricing status --->
<cfset oppSPQStatusTextID = application.com.opportunity.getOppPricingStatusID(oppPricingStatusID=displayStruct["oppPricingStatusID"].currentValue).statusTextID>
<cfset oppStatusTextID = application.com.opportunity.getStatusID(statusID=displayStruct["statusID"].currentValue).statusTextID>
<cfset oppStageTextID = application.com.opportunity.getStageID(stageID=displayStruct["stageID"].currentValue,showSystemStages=true).stageTextID>

<!--- values to set if the opportunity is not a new opportunity --->
<cfif form.opportunityID neq 0>

	<cfif request.relayCurrentUser.isInternal>

		<!--- show the special pricing editor button on the internal if not standard pricing --->
		<cfif oppSPQStatusTextID neq "" and oppSPQStatusTextID neq "StandardPricing">
			<cfset displayStruct["oppPricingStatusID"].noteText='<input type="button" onClick="javascript:void(openNewTab(''Special Pricing (#form.opportunityID#)'',''Special Pricing (#form.opportunityID#)'',''/leadManager/specialPriceEdit.cfm?entityID=#form.opportunityID#'',{iconClass:''leadmanager''}))" value="phr_SpecialPricing">'>
			<cfset displayStruct["oppPricingStatusID"].noteTextPosition = "right">
			<cfset displayStruct["oppPricingStatusID"].noteTextImage="false">
		</cfif>
	</cfif>

	<cfset displayStruct["currency"].relayFormElementType = "HTML">

	<!---
		NJH 2010/10/25 - commented out as opportunities can be synched while being edited.. the read-only should probably come from the opp stage.
	<cfif request.UseSalesForce>
		<!--- if an opportunity has been set to  'Synch with Sales Force', then make it readonly --->
	<cfif application.com.flag.getFlagData(flagID='oppSalesForceSynching',entityId=form.opportunityID).recordCount>
			<cfset oppReadOnly = true>
		</cfif>
	</cfif> --->

	<cfquery name="isOppARenewalOrDemo" datasource="#application.siteDataSource#">
		SELECT 	o.opportunityID
		FROM 	opportunity o
			INNER JOIN opptype opp ON o.oppTypeID = opp.oppTypeID
		WHERE 	o.opportunityID =  <cf_queryparam value="#form.opportunityID#" CFSQLTYPE="CF_SQL_INTEGER" >  AND opp.oppTypeTextId IN ('Renewal','Demo','Leads')		<!--- 2014-02-28 PPB Case 438969 changed opp.oppType to opp.oppTypeTextId --->
	</cfquery>

	<cfif isOppARenewalOrDemo.recordCount GT 0>
		<!--- checking for renewals--->
		<cfset ShowDealRegistration = "false">
	</cfif>
</cfif>

<!--- values to set for the portal user --->
<cfif not request.relayCurrentUser.isInternal>

	<!--- if we're the primary contact, the partner sales person and the account manager should be dropdowns; otherwise it's a hidden field. --->

	<!--- 2013-05-16 - 435316 - RMB Changed condtion to use 'NOT application.com.opportunity.isUserPortalLeadManager()' --->
	<cfif NOT application.com.opportunity.isUserPortalLeadManager()>
	<!--- cfif not application.com.flag.isFlagSetForCurrentUser(flagid="KeyContacts Primary") --->
		<cfset displayStruct["partnerSalesPersonID"].relayFormElementType = "hidden">
	</cfif>

	<!--- partner has applied for deal reg and it's not yet approved or declined --->
	<cfif oppStatusTextID eq "DealRegApplied">
		<cfset oppReadOnly = true>

	<!--- if in special pricing and special pricing has been requested --->
	<cfelseif oppSPQStatusTextID neq "" and oppSPQStatusTextID neq "StandardPricing">
		<cfset oppReadOnly = true>
	</cfif>

<cfelse>
	<!--- if deal reg has not been applied for, then don't provide a drop down on the internal site --->
	<!--- 2012-06-22 - RMB - re-instated checkInternalPermissions condtion to chack LeadApproveTask --->
	<cfif application.com.login.checkInternalPermissions(securityTask='LeadApproveTask',securityLevel='level3')>
		<cfif oppStatusTextID eq "">
			<cfset displayStruct["statusID"].relayFormElementType="HTML">
			<cfset displayStruct["statusID"].makeHidden=true>
		</cfif>
	<cfelse>
		<cfset displayStruct["statusID"].relayFormElementType="HTML">
		<cfset displayStruct["statusID"].makeHidden=true>
	</cfif>

</cfif>

<!--- P-PAN004 --->
<cfif isNumeric(displayStruct.partnerSalesPersonID.currentValue) and displayStruct.partnerSalesPersonID.currentValue neq 0>
	<cfset getFlagGroupDataForPersonQry = application.com.flag.getFlagGroupDataForPerson(flagGroupId="Tier1Tier2Distributor",personId=displayStruct.partnerSalesPersonID.currentValue)>
	<cfif IsQuery(getFlagGroupDataForPersonQry)>
		<cfset partnerTier = getFlagGroupDataForPersonQry.flagTextID>
	</cfif>
</cfif>

<cfset displayStruct.partnerTier = partnerTier>
<!--- values to set for tier2 partners --->
<cfif displayStruct.partnerTier eq "Tier1Tier2DistributorTier2">

	<cfif form.opportunityID neq 0>
		<!--- find out if tier2 opp has a distributor opportunity created --->
		<cfquery name="getDistiOpp" datasource="#application.siteDataSource#">
			select 1 from opportunity where resellerOppID =  <cf_queryparam value="#form.opportunityID#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>

		<cfif getDistiOpp.recordCount>
			<cfset oppReadOnly = true>
		</cfif>

		<!--- 2013-02-06 IH	Case 433487 make distiLocationID editable
		<cfset displayStruct.distiLocationID.relayFormElementType = "HTML">
		--->
	<cfelseif form.opportunityID eq 0 and not request.relayCurrentUser.isInternal>

		<cfset getDistiDetails = application.com.opportunity.getOppDistiLocations(locationID=displayStruct.partnerLocationID.currentValue,countryID=displayStruct.countryID.currentValue)>

		<!--- if it's an assigned Distributor, then we want to make it read-only... otherwise show a dropdown --->
		<cfif application.com.settings.getSetting("leadManager.distiLocFlagList") eq "AssignDistributorOrgID">
			<cfset displayStruct.distiLocationID.relayFormElementType = "HTML">
			<cfset displayStruct.distiLocationID.makeHidden=true>
			<cfset displayStruct["distiLocationID"].currentValue=getDistiDetails.ID>
		<cfelse>
			<cfset displayStruct.distiLocationID.relayFormElementType = "select">
		</cfif>
	</cfif>

<cfelseif displayStruct.partnerTier eq "Tier1Tier2DistributorTier1">
	<!--- LID 5253 NJH 2011/04/05 hide the distributor field if the parter is a tier 1 partner --->
	<cfset displayStruct.distiLocationID.relayFormElementType = "hidden">
	<cfset displayStruct.distiSalesPersonID.relayFormElementType = "hidden">	<!--- 2011/06/01 PPB REL106 hide distiSalesPersonID too --->
</cfif>

<!--- if the opportunity is a closed opportunity, then make it read only --->
<cfif listFindNoCase(application.com.settings.getSetting("leadManager.closedOppStages"),oppStageTextID)>
	<cfset oppReadOnly = true>
	<cfset displayStruct.showSystemStages = true>
</cfif>

<!--- if the opportunity is read only based on the rules above, then set the fields to readonly --->
<cfif oppReadOnly>
	<cfloop list="#structKeyList(oppDetailsStruct)#" index="fieldname">
		<cfset displayStruct[fieldname].readOnly = oppReadOnly>

		<!--- if opportunity is read-only, don't have to make fields required. --->
		<cfset displayStruct[fieldname].required = false>
	</cfloop>
</cfif>

<cfset displayStruct["opportunityID"].encrypt = true> <!--- encrypt the opportunityID --->
<!--- 2012-08-24 PPB ENP001 this wasn't working as expected; you need to set disableToDate in the XML for it to kick-in so you may as well set it to yesterday in the XML if that's what you want (we have checked all client source)
<cfset displayStruct["expectedCloseDate"].disableToDate="#dateAdd('d',-1,now())#">
 --->

<!--- 2012-05-22 PPB P-LEX070 countryISOCode is useful to conditionally control visibility of fields in opportunityDisplay.xml based on country without using ids --->
<cfset displayStruct.countryISOCode.currentValue = application.com.relayCountries.getCountryDetails(countryId=displayStruct.countryID.currentValue).ISOCode>

<!--- START: 2011/05/23 PPB P-REL106 add a level to opportunity xml so that each oppType can have a different configuration --->
<cfquery name="qryOppType" datasource="#application.siteDataSource#">
	select oppTypeTextID from oppType where oppTypeID =  <cf_queryparam value="#displayStruct["oppTypeID"].currentValue#" CFSQLTYPE="CF_SQL_INTEGER" >
</cfquery>
<cfset oppTypeTextID = qryOppType.oppTypeTextID>

<cfset oppTypeNodePath = "opportunity">
<cfif request.relayCurrentUser.isInternal>
	<cfset oppTypeNodePath = oppTypeNodePath & ".internal">
<cfelse>
	<cfset oppTypeNodePath = oppTypeNodePath & ".external">
</cfif>
<cfif StructKeyExists(StructGet("oppFieldsXML." & oppTypeNodePath),oppTypeTextID)>
	<cfset oppTypeNodePath = oppTypeNodePath & "." & oppTypeTextID>
<cfelse>
	<cfset oppTypeNodePath = oppTypeNodePath & ".defaultOppType">
</cfif>
<!--- at this point oppTypeNodePath should be eg. "opportunity.internal.defaultOppType" OR "opportunity.external.deals"  --->
<!--- END: 2011/05/23 PPB P-REL106 --->

<!--- START: 2013-12-05 NYB Case 437286 - this needs to be added for bug: --->
<!--- *** 2013-02-25	MS 	P-KAS005 Make Disti Editable and Visible even for Tier1 Orgs *** --->
<cf_include template="/code/cftemplates/setOpportunityDisplayExtension.cfm" checkifexists="true">
<!--- END: 2013-12-05 NYB Case 437286 --->

<!--- now assign these display attributes to the field nodes in the xml structure --->
<cftry>
	<!--- START: 2011/05/23 PPB P-REL106 add a level to opportunity xml so that each oppType can have a different configuration  --->
	<cfset oppNodes = StructGet("oppFieldsXML." & oppTypeNodePath)>
<!---
	<cfif request.relayCurrentUser.isInternal>
		<cfset oppNodes = oppFieldsXML.opportunity.internal>
	<cfelse>
		<cfset oppNodes = oppFieldsXML.opportunity.external>
	</cfif>
 --->
	<!--- END: 2011/05/23 PPB P-REL106 --->

	<cfset application.com.relayDisplay.setFieldAttributesForDisplay(node=oppNodes,displayStruct=displayStruct)>
	<cfcatch><cfdump var="#cfcatch#"><br><cfdump var="#displayStruct#">
		<CF_ABORT>
	</cfcatch>
</cftry>

