<!--- �Relayware. All Rights Reserved 2014 --->


<!--- 
File name:			oppProductAddMultipleProducts.cfm
Author:				ICS
Date started:		26.10.2004
	
Description:			

called by oppProductList to add identical multiple products

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
26.10.2004			ICS			initial version
2006-06-29      WAB			restrictKeystrokes function altered to handle firefox
Possible enhancements:

 --->

 
<cfparam name="url.countryID" type="string" default="0">
<cfparam name="url.opportunityID" type="string" default="0">
<cfparam name="form.frmApprove" type="string" default="0">
<cfparam name="cookie.user" type="string" default="0">
<cfparam name="form.stage" type="string" default="1">
<cfparam name="form.probability" type="string" default="25">
<cfparam name="form.sortorder" type="string" default="1">
<cfparam name="form.frmTask" type="string" default="">

<cfif form.stage neq 3>
	<cfinvoke component="relay.com.opportunity" method="OppProductGetSKUFromProductID" returnvariable="oppProduct">
		<cfif isdefined("url.oppProductID")><cfinvokeargument name="productID" value="#url.oppProductID#"></cfif>
		<cfif isdefined("form.oppProductID")><cfinvokeargument name="productID" value="#form.oppProductID#"></cfif>
	</cfinvoke>
</cfif>

<cfif form.stage neq 1>
	<cfscript>
	// create an instance of the opportunity component
	myopportunity = createObject("component","relay.com.opportunity");
	myopportunity.dataSource = application.siteDataSource;
	myopportunity.opportunityID = opportunityID;
	//myopportunity.productCatalogueCampaignID = productCatalogueCampaignID;
	myopportunity.countryID = url["countryID"];
	myopportunity.sortOrder = form["sortOrder"];
	//#myopportunity.datasource#
	</cfscript>
</cfif>

<cfif form.stage eq 2>
	<cfscript>
	myopportunity.getOppProducts();
	// probability selects
	myopportunity.OppProductProbabilitySelectName = "frmProbabilityEdit";
	myopportunity.OppProductProbabilitySelectDefault = form["probability"];
	myopportunity.OppProductProbabilitySelectOnchange = "eventOppProductEditOnChange(this);";
	myopportunity.OppProductProbability();
	OppProductProbabilitySelectEdit = myopportunity.OppProductProbabilitySelect;

	//myopportunity.OppProductProbabilitySelectName = "Probability";
	//myopportunity.OppProductProbabilitySelectDefault = form["probability"];
	//myopportunity.OppProductProbabilitySelectOnchange = "";
	//myopportunity.OppProductProbability();
	//OppProductProbabilitySelectAdd  = myopportunity.OppProductProbabilitySelect;
	</cfscript> 
</cfif>

<cfif form.frmTask eq "addproduct">
	<cfscript>
	myopportunity.productID = form.productID;
	myopportunity.opportunityID = form.opportunityID;
	
	for(i=1;i LT (form.oppProductMultiple + 1);i = i + 1)
	{
	myopportunity.quantity = form["quantity" & i];
	myopportunity.createdBy = request.relayCurrentUser.usergroupid;
	myopportunity.lastUpdatedBy = request.relayCurrentUser.usergroupid;
//	if ( form["forecastShipDate" & i] neq "" )
//		myopportunity.forecastShipDate = CreateDate( mid( form["forecastShipDate" & i], 7, 4 ), mid( form["forecastShipDate" & i], 4, 2 ), left( form["forecastShipDate" & i], 2 ) );
//	else
//	myopportunity.forecastShipDate = "";
	myopportunity.forecastShipDate = form["forecastShipDate" & i];
	myopportunity.probability = form["probability" & i];
	myopportunity.specialPrice = form["specialPrice" & i];
	myopportunity.stockLocation = form["frmStockLocation" & i];
	//myopportunity.SPManualEdit = form["frmSPManualEdit"];
	myopportunity.addOppProduct();
	}
	</cfscript>
</cfif>

<cf_translate>

<cf_head>
	<cf_title>Add Multiple Products</cf_title>
	<cfinclude template="/templates/relayFormJavaScripts.cfm">
</cf_head>


<table width="100%" height="100%" cellspacing="0" cellpadding="0" class="withBorder">
<tr>
<td valign="top">

<cfif form.stage eq 1>
	<SCRIPT type="text/javascript">
	function confirm() {
		var errorMessage = "";
		var form = document.frmAddMultipleProducts;
		if(form.oppProductMultiple.value == "" || form.oppProductMultiple.value > 20){
			errorMessage = "phr_sys_oppProductMultipleProductQuantityError";
		}
		
		if (errorMessage != "") {
			alert(errorMessage);
			form.oppProductMultiple.value = "";
			form.oppProductMultiple.focus();
		}
		else {
			form.submit();
		}
	}
	</script>
	
	<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
		<TR>
			<TD ALIGN="LEFT" CLASS="Submenu">phr_sys_oppProductAddMultipleProducts</TD>
		</TR>
	</TABLE>

	<br><br><br>
	<form name="frmAddMultipleProducts" action="oppProductAddMultipleProducts.cfm" method="post">
	<cfoutput>
	<CF_INPUT type="hidden" name="opportunityID" value="#url.opportunityID#">
	<CF_INPUT type="hidden" name="oppProductID" value="#url.oppProductID#">
	<input type="hidden" name="stage" value="2">
	</cfoutput>
	<table border="0" cellspacing="1" cellpadding="3" align="center" class="withBorder">
	<tr>
		<td class="label">phr_productGroupID</td>
		<td class="null" colspan="2"><cfoutput>#htmleditformat(oppProduct)#</cfoutput></td>
	</tr>
	<tr>
		<td class="label">phr_oppProductquantity</td>
		<td class="null"><input type="text" name="oppProductMultiple" size="4" onKeyPress="restrictKeyStrokes(event,keybNumeric)"></td>
		<td class="null" align="right"><input type="button" value="phr_sys_save" onclick="confirm()"></td>
	</tr>
	<table>
	</form>
	
<cfelseif form.stage eq 2>

<SCRIPT type="text/javascript">
	//prefills all the quantity fields with the value entered in the first one
	function prefill(q,n) {
		for (i = 1; i <= n; i++)
		{
		form = document.forms["frmAddMultipleProducts2"]["quantity"+i];
		form.value = q.value;
		}
	}

	function addproduct(q) {
		var errorMessage = "";

		<cfif MakeForecastShipDateMandatory>
		for(i=1;i <= q; i++){
				if ( document.forms["frmAddMultipleProducts2"]["forecastShipDate"+i].value == "" )
				{
					errorMessage = "The Ship Date field number " + i + " is mandatory";
				}
				if ( document.forms["frmAddMultipleProducts2"]["quantity"+i].value == "" )
				{
					errorMessage = "The Quantity field number " + i + " is mandatory";
				}
			}
		</cfif>

		if ( errorMessage == "" )
		{
			document.frmAddMultipleProducts2.frmTask.value = "addproduct";
			document.frmAddMultipleProducts2.submit();
		} else {
			alert( errorMessage );
		}
	}
</script>

<cfoutput>
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
	<TR>
		<TD ALIGN="LEFT" CLASS="Submenu">phr_sys_oppProductAddMultipleProducts</TD>
		<TD COLSPAN="2" ALIGN="right" CLASS="Submenu">
			<A HREF="javascript:addproduct(#form.oppProductMultiple#)" CLASS="Submenu">Save</a>
		</TD>		
	</TR>
</TABLE>
<table align="center" class="withBorder">
<tr>
	<td>&nbsp;</td>
</tr>
<tr>
	<td class="label">
		<strong>#htmleditformat(oppProduct)#</strong>
	</td>
</tr>
</table>

	<table align="center" class="withBorder" width="100%">
	      <form name="frmAddMultipleProducts2" method="post" action="oppProductAddMultipleProducts.cfm">
	      <CF_INPUT type="hidden" name="opportunityID" value="#form.opportunityID#">
	      <CF_INPUT type="hidden" name="productID" value="#form.oppProductID#">
	      <CF_INPUT type="hidden" name="oppProductMultiple" value="#form.oppProductMultiple#">
		  <input type="hidden" name="frmTask" value="">
		  <input type="hidden" name="stage" value="3">
			
			<cfset n = form.oppProductMultiple>
	   		
			<cfloop from="1" to="#form.oppProductMultiple#" index="i">
	   			<!--- ==============================================================================
	   			       Add new oppProduct Row         
	   			=============================================================================== --->
	   			<TR>
	   				<td>
	   					<TABLE border="0" CELLSPACING="3" CELLPADDING="0" class="withBorder" width="100%">
	   						<tr>
								<th nowrap width="20px">#htmleditformat(i)#</th>
								<th nowrap>phr_oppProductquantity
	   							</th>
	   							<th nowrap>	   									
	   								<cfif useOppProductShipDate>phr_oppProductShipDate
									<cfelse>&nbsp;</cfif>
	   							</th>
	   							<th nowrap>	   									
	   								<cfif useOppProductProbability>phr_oppProductProbability
	   								<cfelse>&nbsp;</cfif>
	   							</th>
	   							<th>	   									
	   								<cfif useOppProductSpecialPricing>phr_oppProductSpecialPrice
	   								<cfelse>&nbsp;</cfif>
	   							</th>
	   							<th>	   									
	   								<cfif useOppProductStockLocation>phr_oppProductStockLocation
	   								<cfelse>&nbsp;</cfif>
	   							</th>
	   						</tr>
	   						<tr>
	   							<td valign="top">&nbsp;</td>
	   							<td valign="top">
	   							   <input name="quantity#i#" size="5" onKeyPress="restrictKeyStrokes(event,keybNumeric)" <cfif i eq 1>onblur="prefill(this,#htmleditformat(n)#)"</cfif>>
	   							</td>
	   							<td valign="top">	   									
								<cfif useOppProductShipDate>
									<cf_relayDateField	
										currentValue=""
										thisFormName="frmAddMultipleProducts2"
										fieldName="forecastShipDate#i#"
										anchorName="anchorX"
										helpText="phr_dateFieldHelpText"
									>
								<cfelse>
									<CF_INPUT type="hidden" name="forecastShipDate#i#">&nbsp;
								</cfif>
								</td>
	   				<td valign="top">&nbsp;		   						
	   					<cfif useOppProductProbability>
							<cfscript>
							myopportunity.OppProductProbabilitySelectName = "Probability#i#";
							myopportunity.OppProductProbabilitySelectDefault = form["probability"];
							myopportunity.OppProductProbabilitySelectOnchange = "";
							myopportunity.OppProductProbability();
							OppProductProbabilitySelectAdd  = myopportunity.OppProductProbabilitySelect;
							</cfscript>
	   						<cfoutput>
	   							#htmleditformat(OppProductProbabilitySelectAdd)#
	   						</cfoutput>
	   					<cfelse>
	   					   <CF_INPUT type="hidden" name="Probability#i#" value="#url[" probability "]#">&nbsp;
	   					</cfif>
	   				</td>	   					
	   				<cfif useOppProductSpecialPricing>
	   					<td valign="top">
	   					   <CF_INPUT name="specialPrice#i#" size="5" onKeyPress="restrictKeyStrokes(event,keybDecimal)">
	   					</td>
	   				<cfelse>
	   				   <input type="hidden" name="specialPrice">
	   				</cfif>
	   				<cfif isDefined ("qValidStockLocations") and useOppProductStockLocation and isQuery(qValidStockLocations)>
	   					<td valign="top">
	   						<SELECT NAME="frmStockLocation#i#">	<!--- this is defined in opportunityINI.cfm --->
	   							<cfloop query="qValidStockLocations">
	   								<option value="#optionValue#">#htmleditformat(optionDescription)#</option>
	   							</cfloop>
	   						</SELECT>
	   					</td>
	   				<cfelse>
	   				   <CF_INPUT type="hidden" name="frmStockLocation#i#">
	   				</cfif>
	   			</tr>
	</table>
</td>
</tr>
</cfloop>
</form>
</cfoutput>
<cfelseif form.stage eq 3>
	<SCRIPT type="text/javascript">
	opener.location.href = opener.location.href;
	self.close();
	</script>
</cfif>
</td>
</tr>
</table>


</cf_translate>
