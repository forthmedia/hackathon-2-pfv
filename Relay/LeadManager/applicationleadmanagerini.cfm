<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			.cfm	
Author:				
Date started:			/xx/07
	
Description:			
This file is run automatically by the security system every time a file in LeadManager is accessed

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2010-05-17			AJC			Issue 3359: My Opportunities falls over when there are no opportunities

Possible enhancements:


 --->


<CFSET thisDir = "/LeadManager">

<!--- START: 2010-05-17 AJC Issue 3359: My Opportunities falls over when there are no opportunities --->
<cfif checkPermission.recordCount>
	<cfset SuperUser = checkPermission.Level4 />
<cfelse>
	<cfset SuperUser = false />
</cfif>
<!--- END: 2010-05-17 AJC Issue 3359: My Opportunities falls over when there are no opportunities --->	
	

<CFSET LeadSystemName = "Lead & Opportunity Manager">

<!--- any of the above variables can be over ridden in the INI file --->
<cfif fileexists("#application.paths.code#\cftemplates\opportunityINI.cfm")>
	<cfinclude template="/code/cftemplates/opportunityINI.cfm">
	<!--- 	SSS 2009/05/18 testing 
		NJH 2009/08/21
		Changing it back to including template, rather than including once. The idea of including the INI file once is nice, but doesn't quite work if the variables are not scoped as 'request' as the template
		is being called in a custom tag. So, we will need to change the variables to request scope if we are going to use this.
	 --->
	<!--- <cf_includeonce template="/code/cftemplates/opportunityINI.cfm"> --->
</cfif>

<!--- <cfif isDefined("siteLocale")>
	<cfset thislocale = SetLocale("#siteLocale#")>
</cfif> --->

<!--- this switches on role based scoping of opportunity records and stops users with 
	leadManagerTask(1) see anything but what they own.  It is controlled by adding
	roleScopeOpportunityRecords to the opportunityINI.cfm file --->
<!--- <cfif isdefined("roleScopeOpportunityRecords") and roleScopeOpportunityRecords 
	and structKeyExists(session,"securitylevels") and listfindnocase(session.securitylevels,"leadmanagertask(3)") eq 0> --->
<cfif application.com.settings.getSetting("leadManager.roleScopeOpportunityRecords") 
		and not application.com.login.checkInternalPermissions("leadmanagertask","Level2")>
	<cfset frm_vendorAccountManagerPersonID = request.relayCurrentUser.personid>
</cfif>

<!--- this switches on role based scoping of SPQ records only and stops users with 
	leadManagerTask(1) see anything but what they own.  It is controlled by adding
	roleScopeOpportunityRecords to the opportunityINI.cfm file --->
<!--- <cfif isdefined("roleScopeSPQRecords") and roleScopeSPQRecords 
	and structKeyExists(session,"securitylevels") and listfindnocase(session.securitylevels,"leadmanagertask(3)") eq 0> --->
<cfif isdefined("roleScopeSPQRecords") and roleScopeSPQRecords 
	and not application.com.login.checkInternalPermissions("leadmanagertask","Level2")>
	<cfset frm_vendorAccountManagerPersonID = request.relayCurrentUser.personid>
</cfif>

