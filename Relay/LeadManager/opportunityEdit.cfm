﻿<!--- ©Relayware. All Rights Reserved 2014 --->
<!---
File name:		opportunityEditV2.cfm
Author:			NJH
Date started:	03-03-2010

Description:	New version of opportunity Edit. Built by domain and display xml files

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2010-06-14			NAS			P-CLS002 Fields added as switches to turn fields/functionality on/off
2011-01-10 			NYB			LHID5257 - added output
2011/05/23 			PPB 		P-REL106 - add a level to opportunity xml so that each oppType can have a different configuration
2011/11/24			PPB			P-REL106/NET009 show a heading specific to oppType if the phrase exists
2012/03/13 			PPB 		AVD001 added relatedFiles
2012/04/12			GCC			added a "mode" so that a switch can be put inside an included file
2012/05/15 			PPB 		P-LEX070 facility to switch off rendering of a group/headings so we can conditionally switch in XML
2012/05/25 			PPB 		P-LEX070 return to the list of opps if the user starts entering a new opp and clicks cancel (rather than crash)
2012/05/31 			PPB 		P-MIC001 added CFOUTPUTs around refreshPage() javascript
2012/06/07			IH			Case 428643 set deal status to "Not yet Submitted for Approval" when saved
2012/06/07 			IH 			Case 428643 enable apply for deal reg button after the deal have been approved
2012/05/31 			PPB 		P-LEX070 moved re-direction from opportunityTask.cfm
2012-08-07 			PPB			Case 429506 now HIDE ApplyForDealReg and Save buttons depending on circumstance
2012-08-22			IH			Case 425816 Disable phr_opp_ApplyForSpecialPricing if there are no Opp. Approvers
2012-09-28 			PPB 		Case 429630 disable the save button after clicking it to prevent 2+ opps being created
2012-10-08			IH			Case 431126 moved disabling save button to oppEdit.js > submitOpportunityForm()
2013-08-23 			PPB 		Case 436754 don't refreshPromotions() on page load
2014-01-22			WAB/MS		P-SAF001 Added support for render true/false on all fields
2014-07-17 			PPB 		Case 439159 added DontClickBack message
2014-10-20			RPW			CORE-861 Need improvements when trying to add a new opp for a Partner via the 3 Pane View
2014-10-28			RPW			CORE-666 OPP Edit Screen needs multiple XML Changes
2015/03/16			NJH			Removed isInternal as now done in request.cfc
2015-04-22			WAB			PGI-001-004 Added bookmarks to next to the <H3> tags to allow jumping directly to Products area
2014/09/22			NJH			Jira Prod2015-34 - add screens to opp display capabilities.
2015-12-15			RJT		Case 446669 allow opportunities to not be locked down based on setting
2016/03/15			NJH			BF-571 changed stage_lost JS variable from closedLost to oppStageLost as oppStageLost was a standard opp stage. We seem to have duplicates at the moment which need cleaning up.
								Other code references oppStageLost so that is why I've gone with this one where as nothing referenced closedLost.
2016-06-29			WAB			During PROD2016-876.  Removed reference to request. encryption. encryptedfields.  Should not be necessary ('private variable')
Possible enhancements:


 --->


<cfif not request.relayCurrentUser.isInternal and not application.com.security.confirmFieldsHaveBeenEncrypted(fieldNames="opportunityID")>
     <CF_ABORT>
</cfif>

<cfsetting enablecfoutputonly="true">

<cf_param name="opportunityID" type="numeric" default="0"/>
<cf_param name="returnURL" type="string" default="#cgi.HTTP_REFERER#"/>
<!--- 2014-10-20			RPW			CORE-861 Need improvements when trying to add a new opp for a Partner via the 3 Pane View --->
<cf_param name="variables.approvalViewLinkDisplay" type="string" default=""/>
<cf_param name="variables.pageTitle" type="string" default="phr_opp_customerOpportunity"/><!--- 2014-10-28	RPW	CORE-666 OPP Edit Screen needs multiple XML Changes --->
<cfscript>
	if (!opportunityID) {
		variables.uploadUUID = application.com.dbInterface.GetDBUUID();
	} else {
		variables.uploadUUID = "";
	}
</cfscript>

<!--- the following functionality was put in for the ccpayment popup so that when the maximum CC Attempts is reached we can set the stage to awaitingPayment and refresh the opportunity form to "lock" the opp;
without this: when doing the refresh, returnURL would be set to cgi.HTTP_REFERER and would refer to the popup such that when the cancel opp button was pressed the popup page would be loaded instead of going back to an opp list (saves passing returnURL thro js functions into popup and back) --->
<cfif StructKeyExists(url,'useSavedReturnURL') and url.useSavedReturnURL and not request.relayCurrentUser.isInternal>
	<cfset returnURL = session.savedReturnURL>
<cfelse>
	<cfset session.savedReturnURL = returnURL>
</cfif>

<cfset returnURL = urldecode(returnURL)>

<cfif not structKeyExists(form,"opportunityID")>
	<!--- 2016-06-29	WAB	During PROD2016-876.  Removed reference to request. encryption. encryptedfields.  Did not seem to be serving much purpose and it's a 'private variable'
	<cfif structKeyExists(request. encryption. encryptedFields,"opportunityID")>
		<cfset form.opportunityID = request. encryption. encryptedFields.opportunityID>
	<cfelse>
		<cfset form.opportunityID = opportunityID>
	</cfif>
	--->

	<cfset form.opportunityID = opportunityID>
</cfif>

<cfset oppSettings = application.com.settings.getSetting("leadManager")>
<cfset useOppProducts = oppSettings.products.useOpportunityProducts>
<cfset disableDealRegWhenNoProducts = oppSettings.dealRegistration.disableDealRegWhenNoProducts>

<!--- If on the intneral referrer is 'broken' by opening in new tab - will always be opportunityedit.cfm --->
<cfif request.currentsite.isinternal and opportunityID neq 0>	<!--- 2012/05/25 PPB P-LEX070 I added the 'opportunityID neq 0' condition so a new opp returns to the list of opps if the user clicks cancel (rather than crash) --->
	<cfset returnURL= application.com.security.encryptURL(url="/leadmanager/opportunityedit.cfm?opportunityid=#form.opportunityid#")>
</cfif>

<cfif not application.com.rights.doesUserHaveRightsForEntity(entityType="opportunity",entityID=form.opportunityID,permission="view")>
	<!--- NYB 2011-01-10 LHID5257 - added output: --->
	<cfoutput>
		<p>phr_opp_youDoNotHaveRightsToViewThisOpportunity</p>
	</cfoutput>
	<CF_ABORT>
</cfif>

<!--- The top head was appearing twice on the internal due to the call above. Never seen due to styling, but since there were styling changes to the header, it showed up. Remove the second include --->
<cfset application.com.request.setTopHead(topheadCFM="")>

<cf_includeJavascriptOnce template = "/javascript/openWin.js">
<cf_includeJavascriptOnce template="/javascript/tableManipulation.js">
<cf_includeJavascriptOnce template="/leadManager/js/oppEdit.js" translate="true">
<cf_includeJavascriptOnce template="/javascript/checkObject.js">
<cf_includeJavascriptOnce template = "/javascript/fnlajax.js">

<!--- START 2016-04-15 AHL Product Combined/Select Box --->
<cf_includeCssOnce template="/javascript/lib/select2/css/select2.min.css" checkIfExists="false">
<cf_includeJavascriptOnce template = "/javascript/lib/select2/js/select2.min.js">
<!--- END 2016-04-15 AHL Product Combined/Select Box --->

<cf_includeCssOnce template="/styles/opportunity.css" checkIfExists="true">

<!--- 2012/03/13 PPB AVD001 added refreshPage function which can be called from a pop-up (entityRelatedFilePopup.cfm)  --->
<cfoutput>		<!--- 2012/05/31 PPB MIC001 added CFOUTPUTs because enablecfoutputonly="true" --->
<script type="text/javascript">
	function refreshPage() {
		window.location.href=window.location.href
	}
</script>
</cfoutput>
<cfinclude template="/templates/relayFormJavaScripts.cfm">

<!--- 2014-10-28	RPW	CORE-666 OPP Edit Screen needs multiple XML Changes
	2016/03/15 - NJH BF-571 changed from closedLost to oppStageLost as oppStageLost was a standard opp stage. We seem to have duplicates at the moment which need cleaning up.
	Other code references oppStageLost so that is why I've gone with this one where as nothing referenced closedLost.
--->
<cfset stageLostID = application.com.opportunity.getStageID(stageTextID="OppStageLost").ID>

<!--- <cfset request.relayFormDisplayStyle = "HTML-table"> --->

<!--- if the opportunity display xml is not loaded into memory (it will get deleted when application variables are loaded), load it --->
<cfif not structKeyExists(application,"opportunityXML")>
	<cfset application.com.relayDisplay.loadDisplayXML(tablename="opportunity")>
</cfif>

<!--- 2012/04/12 PPB any include files should test for mode to output to screen if mode eq "display" and run post-processing code if mode eq "save" --->
<cfif structKeyExists(form,"frmTask")>
	<cfset request.mode="save">					<!--- 2012/04/12 GCC added mode --->
	<!---2012-11-14 - PJP - P-XIR001 / 431945 - Moved To Opportuntity Task - This should be checking the old status first
	<cfif (not structKeyExists(form,"statusID")) and form.frmTask is "Save">
		<cfset form.statusID = 0>
	</cfif>	--->
	<cfinclude template="opportunityTask.cfm">
	<!--- 2014-10-20	RPW	CORE-861 Need improvements when trying to add a new opp for a Partner via the 3 Pane View - Removed included files --->
	<cfparam name="message" default="">

	<cfif frmTask eq "Save">
		<cfset message="phr_Record_Updated">
	</cfif>

	<cfset application.com.relayUI.setMessage(message=message)>

	<cfif returnURL neq "" and frmTask neq "SaveAndShowSpecialPrice">
		<!--- if we're saving for the first time, then continue on; otherwise re-direct if url has been defined --->
		<cfif not (frmTask eq "save" and (oldOpportunityDetails.opportunityID eq 0 or oldOpportunityDetails.opportunityID eq ""))>
			<!--- remove the message field if it exists --->

			<!--- <cfset returnURL = application.com.regExp.removeItemFromNameValuePairString(inputString=returnURL,itemToDelete="formState",delimiter="&")> --->

			<!--- <cfif isDefined("message")>
				<cfset returnURL = returnURL&"&message="&message>
			</cfif> --->

			<cflocation url="#returnURL#" addToken="false">
		</cfif>
	</cfif>
</cfif>
<cfset request.mode="display">					<!--- 2012/04/12 GCC added mode --->

<!--- go to the db to get the opportunity values--->
<cfset opportunityDetails = application.com.opportunity.getOpportunityDetails(opportunityID=form.opportunityID)>
<cfset oppDetailsStruct = application.com.structureFunctions.queryRowToStruct(query=opportunityDetails,row=opportunityDetails.recordCount)>

<!--- if the opportunity exists, then we need to do such things as re-calculate promotions that have been applied --->
<!---
2013-08-23 PPB Case 436754 I have commented this out cos it re-populates oppProducts which kicks a synch to salesforce and can stomp over changes on sf end
if we need to do a refresh as the result of a change to a promo we should setup a post-process on change of the promo to do it

<cfif form.opportunityID neq 0>
	<cfset application.com.opportunity.refreshPromotions(opportunityID=form.opportunityID)>
</cfif>
 --->

<cfset oppFieldsXML = duplicate(application.opportunityXML)>

<!--- set the various field attributes for display... things such as readonly, make hidden, etc. --->
<cfinclude template="setOpportunityDisplay.cfm">

<!--- wanted to use cf_abort, but that was displaying the page twice for some reason.. so, until that gets fixed, this is the way to output the message.. use ifOkToProceed --->
<cfset okToProceed = true>
<cfif (displayStruct.entityID.currentValue eq 0 or not isNumeric(displayStruct.entityID.currentValue)) and not request.relayCurrentUser.isInternal>
	<cfoutput>
		<p>phr_opp_thisRelayTagMustBeUsedInConjuctionWithOtherTags</p>
	</cfoutput>
	<cfset okToProceed = false>
</cfif>

<cfif okToProceed>

<!--- some javascript constants --->
<!--- 2014-10-28	RPW	CORE-666 OPP Edit Screen needs multiple XML Changes --->
<cfoutput>
	<script>
		var STAGE_LOST = #jsStringFormat(stageLostID)#;
		var disableDealRegWhenNoProducts = #jsStringFormat(disableDealRegWhenNoProducts)#;
		var oppStatusTextID = '#jsStringFormat(oppStatusTextID)#';
		var opportunityID = #FORM.opportunityID#;
		var uploadUUID = "#variables.uploadUUID#";
	</script>
</cfoutput>


<cfform  name="opportunityForm" method="post" onSubmit="return verifyOpportunityForm();" novalidate="true" >		<!--- 2012/03/13 PPB AVD001 added novalidate to prevent red outlines around required fields in FF if required="false" --->

	<cf_relayFormElement relayFormElementType="hidden" currentValue="Save" fieldname="frmTask" label="">
	<cf_relayFormElement relayFormElementType="hidden" currentValue="#partnerTier#" fieldname="partnerTier" label="">
	<cf_relayFormElement relayFormElementType="hidden" currentValue="#returnURL#" fieldname="returnURL" label="">
	<cf_relayFormElement relayFormElementType="hidden" currentValue="#stageLostID#" fieldname="lostVal" label="" ID="lost">
	<cf_relayFormElement relayFormElementType="hidden" currentValue="#variables.uploadUUID#" fieldname="uploadUUID"><!--- 2014-10-28	RPW	CORE-666 OPP Edit Screen needs multiple XML Changes --->

	<cfif isDefined("frmNext") and frmNext eq "returnLeadScreen">
		<!--- if we are being called from addContactForm and thisSupresses the relayware/relayTagTemplates//addContactForm from showing again --->
		<cf_relayFormElement relayFormElementType="hidden" currentValue="no" fieldname="showForm" label="">
		<cf_relayFormElement relayFormElementType="hidden" currentValue="no" fieldname="showIntroText" label="">
		<cf_relayFormElement relayFormElementType="hidden" currentValue="returnLeadScreen" fieldname="frmNext" label="">
		<cf_relayFormElement relayFormElementType="hidden" currentValue="#entityID#" fieldname="thisOrgID" label="">
	</cfif>

	<!--- 2014-10-20			RPW			CORE-861 Need improvements when trying to add a new opp for a Partner via the 3 Pane View --->
	<cfif ListFind("oppStagePlaceOrder,oppStageClosed,oppStageOrderViaDisti",oppStageTextID)>
		<cfset variables.pageTitle = "phr_opp_OpportunityOrdered">
	<cfelseif oppSPQStatusTextID neq "Standardpricing">
		<cfset variables.pageTitle = "phr_opp_OpportunitySpecialPricing">
	<cfelseif opportunityDetails.StatusID gt 0>
		<cfset variables.pageTitle = "phr_opp_OpportunityDealRegistration">
	<cfelseif form.opportunityID eq 0>
		<cfif application.com.relayTranslations.doesPhraseTextIDExist('opp_Opportunity#oppTypeTextId#New')>
			<cfset UseThisHeadingByOppType = "phr_opp_Opportunity#oppTypeTextId#New">
			<cfoutput>#htmleditformat(UseThisHeadingByOppType)#</cfoutput>
		<cfelse>
		<cfset variables.pageTitle = "phr_opp_OpportunityNew">
		</cfif>

	<cfelse>

		<cfif application.com.relayTranslations.doesPhraseTextIDExist('opp_OpportunityEdit#oppTypeTextId#')>
			<cfset UseHeadingByOppType = "phr_opp_OpportunityEdit#oppTypeTextId#">
			<cfoutput>#htmleditformat(UseHeadingByOppType)#</cfoutput>
		<cfelse>
		<cfset variables.pageTitle = "phr_opp_OpportunityEdit">
	</cfif>

</cfif>

<!--- 2014-10-20			RPW			CORE-861 Need improvements when trying to add a new opp for a Partner via the 3 Pane View --->
<cf_leadManagerTopHead
templateType = "edit"
thisDir="/leadManager"
entityID = form.opportunityID
showBack="false"
showPrint="false"
showActions="false"
showNotes="false"
showSave="false"
pageTitle="#variables.pageTitle#"
>
	<!--- NJH/RT 2016/03/03 - set class to empty string as form-horizonal for the portal was messing up some styling. --->
	<cf_relayFormDisplay id="opportunityTable" showValidationErrorsInline="true" class="">

		<cfquery name="getOrgName" datasource="#application.siteDataSource#">
			select organisationName from organisation where organisationID =  <cf_queryparam value="#displayStruct.entityID.currentValue#" CFSQLTYPE="CF_SQL_INTEGER" >
		</cfquery>

		<cfset form.endCustomerName = getOrgName.organisationName>

		<cfif form.opportunityID eq 0>
			<!--- 2011/11/24 PPB REL106/NET009 show a heading specific to oppType if the phrase exists --->
			<cfif application.com.relayTranslations.doesPhraseTextIDExist('opp_customerNew#oppTypeTextId#')>
				<cfset headingPhrase = "phr_opp_customerNew#oppTypeTextId#">
			<cfelse>
				<cfset headingPhrase = "phr_opp_customerNewOpportunity">
			</cfif>
		<cfelse>
			<!--- 2011/11/24 PPB REL106/NET009 show a heading specific to oppType if the phrase exists --->
			<cfif application.com.relayTranslations.doesPhraseTextIDExist('opp_customer#oppTypeTextId#')>
				<cfset headingPhrase = "phr_opp_customer#oppTypeTextId#">
			<cfelse>
				<cfset headingPhrase = "phr_opp_customerOpportunity">
			</cfif>

		</cfif>

		<cfoutput>
		<cfif request.relayFormDisplayStyle eq "HTML-div">
			<tr>
				<td colspan="2" style="text-align:center">
		</cfif>
			<!--- 2014-10-28	RPW	CORE-666 OPP Edit Screen needs multiple XML Changes --->
			<h3>#htmleditformat(headingPhrase)#</h3>
		<cfif request.relayFormDisplayStyle eq "HTML-div">
				</td>
			</tr>
		</cfif>
		</cfoutput>

		<!--- <cfif isDefined("message")>
			<cf_relayFormElementDisplay relayFormElementType="message" currentValue="#application.com.relayUI.getmessage()#" fieldname="frmMessage" label="">
		 </cfif> --->

<!--- START: 2011/05/23 PPB P-REL106 add a level to opportunity xml so that each oppType can have a different configuration  --->
<!---
		<!--- loop through the groups/fields --->
		<cfif request.relayCurrentUser.isInternal>
			<cfset fieldItemsArray = oppFieldsXML.opportunity.internal.xmlChildren>
		<cfelse>
			<cfset fieldItemsArray = oppFieldsXML.opportunity.external.xmlChildren>
		</cfif>
 --->

	<cfset fieldItemsArray = StructGet("oppFieldsXML." & oppTypeNodePath & ".xmlChildren")>

<!--- END: 2011/05/23 PPB P-REL106 --->

		<cfloop from=1 to="#arrayLen(fieldItemsArray)#" step="1" index="idx">
			<cfset node = fieldItemsArray[idx]>

		<!--- 2014-01-22	WAB/MS	P-SAF001 Moved this outside the cfif check for group so that it gets applied to node.xmlName field --->
		<cfif not StructKeyExists(node.xmlAttributes,"render") or evaluate(node.xmlAttributes.render)> 	<!--- 2012/05/15 PPB LEX070 facility to suppress rendering of a group so we can conditionally switch in XML --->

			<cfif node.xmlName eq "group">


					<cfif (node.xmlAttributes.name eq "Products" and form.opportunityID neq 0) or (node.xmlAttributes.name neq "Products")>
						<cfif not StructKeyExists(node.xmlAttributes,"renderHeading") or node.xmlAttributes.renderHeading>					<!--- 2012/05/15 PPB LEX070 facility to suppress rendering of a group heading --->
							<cfif request.relayFormDisplayStyle eq "HTML-div">
								<tr>
									<th colspan="2" style="text-align:center">
							</cfif>
								<!--- 2015-04-22	WAB	PGI-001-004 Added bookmark --->
								<h3><cfoutput><a name="#lcase(replace(node.xmlAttributes.name," ",""))#"></a>phr_opp_#replace(node.xmlAttributes.name," ","")#</cfoutput></h3>
							<cfif request.relayFormDisplayStyle eq "HTML-div">
									</th>
								</tr>
							</cfif>
						</cfif>
						<cfif not StructKeyExists(node.xmlAttributes,"renderHeadingDesc") or node.xmlAttributes.renderHeadingDesc>			<!--- 2012/05/15 PPB LEX070 facility to suppress rendering of a group heading description --->
							<cfif application.com.relayTranslations.doesPhraseTextIDExist('opp_#replace(node.xmlAttributes.name," ","")##oppTypeTextId#Desc')>
								<cfset DescPhrase = "phr_opp_#replace(node.xmlAttributes.name," ","")##oppTypeTextId#Desc">
							<cfelse>
								<cfset DescPhrase = "phr_opp_#replace(node.xmlAttributes.name," ","")#Desc">
							</cfif>
							<cfif request.relayFormDisplayStyle eq "HTML-div">
								<tr>
									<td colspan="2">
							</cfif>
							<p><cfoutput>#htmleditformat(DescPhrase)#</cfoutput></p>
							<cfif request.relayFormDisplayStyle eq "HTML-div">
									</td>
								</tr>
							</cfif>
						</cfif>
					</cfif>

					<!--- product picker --->
					<cfif node.xmlAttributes.name eq "Products" and useOppProducts>
						<cfif structKeyExists(form,"opportunityID") and form.opportunityID neq 0>

							<!--- if deal registration has been approved, then we shouldn't be able to add/delete products from the opportunity
								NJH 2012/02/13 CASE 426152: Added a setting to enable opportunity products to be editable after deal reg approval. So, make the opp readonly if this setting is false (which it is by default)
							 --->
							<cfset productDisplayReadOnly = oppReadOnly>
							<cfif oppStatusTextID eq "DealRegApproved" and not application.com.settings.getSetting("leadManager.dealRegistration.enableProductEditWhenDealRegApproved")>
								<cfset productDisplayReadOnly = true>
							</cfif>

							<cfset argumentString = "#form.opportunityID#,#productDisplayReadOnly#">
							<cfif structKeyExists(form,"frmTask") and form.frmTask eq "SaveAndShowSpecialPrice">
								<cfset argumentString = argumentString&",'getOppSpecialPricingDisplay'">
							</cfif>
							<cfif request.relayFormDisplayStyle eq "HTML-div">
								<tr>
									<td colspan="2">
							</cfif>
							<div id="oppProductsDiv"></div>
							<cfif request.relayFormDisplayStyle eq "HTML-div">
								</td></tr>
							</cfif>
							<script>
								<cfoutput>updateOppProductsDiv(#jsStringFormat(argumentString)#);</cfoutput>
							</script>
						</cfif>

					<cfelseif node.xmlAttributes.name eq "leadManagerRights">

						<cfif request.relaycurrentUser.isInternal and form.opportunityID neq 0>
							<tr>
								<td valign="top" class="label">phr_managerRights</td>
								<td>
									<CF_UGRecordManager
										entity = "opportunity"
										entityid = "#form.opportunityID#"
										form = "opportunityForm"
										caption1 = "People"
										caption2 = "Selected"
										suppressGroups = "true">
								</td>
							</tr>
						</cfif>

					<cfelseif node.xmlAttributes.name eq "RelatedFiles">			<!--- 2012/03/13 PPB AVD001 added relatedFiles --->
						<!--- 2014-10-28	RPW	CORE-666 OPP Edit Screen needs multiple XML Changes --->
						<cfif request.relayFormDisplayStyle eq "HTML-div"><tr><td colspan="2"></cfif>
						<div id="relatedFiles"></div>
						<cfif request.relayFormDisplayStyle eq "HTML-div"></td></tr></cfif>
					<cfelse>
						<cfif arrayLen(node.xmlChildren) gt 0>
							<cfset fieldsArray = node.xmlChildren>
							<cfloop from=1 to="#arrayLen(fieldsArray)#" step="1" index="fieldIdx">

								<cfset node = fieldsArray[fieldIdx]>
								<cfset args = duplicate (node.xmlattributes)>

								<!--- START: 2014-01-22	WAB/MS	P-SAF001 Check for render attribute on xmlChildren for fields as well --->
								 <cfif not StructKeyExists(node.xmlAttributes,"render") or evaluate(node.xmlAttributes.render)> 	<!--- 2012/05/15 PPB LEX070 facility to suppress rendering of a group so we can conditionally switch in XML --->

									<cfif node.xmlName eq "field">
										<cfset fieldAttributes = application.com.relayDisplay.getQueryAttributeForSelect(fieldAttributes=node.xmlattributes)>
										<cfset application.com.relayDisplay.displayFormField(fieldAttributes=fieldAttributes,entityID=form.opportunityID,countryID=displayStruct.countryID.currentValue)> <!--- 2013-06-06	MS Case. 435616 - Pass in the countryID so that it becomes available for valid Values Query  --->

								 	<cfelseif node.xmlName eq "space">

										<cfif request.relayFormDisplayStyle eq "HTML-DIV">
											<tr><td colspan="2">&nbsp;</td></tr>
										<cfelse>
											<span>&nbsp;</span>
										</cfif>

									<!--- NJH PROD2015-34 2015/09/22 - include a screen in the opp display --->
									<cfelseif node.xmlName eq "screen">
										<cfset useScreenByName = structKeyExists(node.xmlAttributes,"screenID")?node.xmlAttributes.screenID:"OpportunityEdit_Custom">
										<cfset application.com.relayDisplay.showScreenElementsInDisplayXML(screenID=useScreenByName,entityID=form.opportunityID,countryID=displayStruct.countryID.currentValue neq ''?displayStruct.countryID.currentValue:0,entityDetails=oppDetailsStruct)>

								 	<!--- include custom file --->
									<cfelseif node.xmlName eq "includeFile">
										<cfset application.com.relayDisplay.includeFileForDisplay(node=node)>
									</cfif>

								</cfif>
								<!--- END: 2014-01-22	WAB/MS	P-SAF001 Check for render attribute on xmlChildren for fields as well --->

							</cfloop>
						</cfif>
					</cfif>
			<cfelseif node.xmlName eq "field">

				<cfset fieldAttributes = application.com.relayDisplay.getQueryAttributeForSelect(fieldAttributes=node.xmlattributes)>
	 			<cfset application.com.relayDisplay.displayFormField(fieldAttributes=fieldAttributes,entityID=form.opportunityID,countryID=displayStruct.countryID.currentValue)> <!--- 2013-06-06	MS Case. 435616 - Pass in the countryID so that it becomes available for valid Values Query  --->

			<!--- NJH PROD2015-34 2015/09/22 - include a screen in the opp display --->
			<cfelseif node.xmlname eq "screen">
				<cfset useScreenByName = structKeyExists(node.xmlAttributes,"screenID")?node.xmlAttributes.screenID:"OpportunityEdit_Custom">
				<cfset application.com.relayDisplay.showScreenElementsInDisplayXML(screenID=useScreenByName,entityID=form.opportunityID,countryID=displayStruct.countryID.currentValue neq ''?displayStruct.countryID.currentValue:0,entityDetails=oppDetailsStruct)>

			<!--- include custom file --->
			<cfelseif node.xmlName eq "includeFile">
				<cfif form.opportunityID>
					<!--- 2014-10-20			RPW			CORE-861 Need improvements when trying to add a new opp for a Partner via the 3 Pane View --->
					<cfsavecontent variable="approvalViewLinkDisplay">
						<cfset application.com.relayDisplay.includeFileForDisplay(node=node)>
					</cfsavecontent>
				</cfif>
			</cfif>

		</cfif>
		<!--- END:2014-01-22 	WAB/MS	P-SAF001 Moved this outside the cfif check for group so that it gets applied to node.xmlName field --->
		</cfloop>

		<cfif request.relayFormDisplayStyle eq "HTML-div">
			<tr>
				<td colspan="2" align="center">
		</cfif>
				<cfoutput>
				<div id="oppButtonDiv" style="display:#IIF(structKeyExists(form,'frmTask') and form.frmTask eq 'SaveAndShowSpecialPrice',DE('none'),DE(''))#">
					<!--- 2014-10-20			RPW			CORE-861 Need improvements when trying to add a new opp for a Partner via the 3 Pane View --->
					<cfif Len(variables.approvalViewLinkDisplay)>
						#variables.approvalViewLinkDisplay#
					</cfif>
					<span id="opp_message_DontClickBack" style="display:none">phr_opp_DontClickBack</span>			<!--- 2014-07-17 PPB Case 439159 added DontClickBack message --->
					<cf_relayFormElement relayFormElementType="button" class="" currentValue="phr_opp_Cancel" fieldname="frmCancel" label="" onClick="javascript:window.location.href='#application.com.regExp.removeItemFromNameValuePairString(inputString=returnURL,itemToDelete="message",delimiter="&")#'">

					<cfset currentUserIsApprover = application.com.flag.isFlagSetForCurrentUser("PartnerOrderingManager")>
					<cfset okToApprove = (oppStageTextID eq 'oppStageAwaitingApproval') and currentUserIsApprover>	<!--- if the opp is awaiting approval and this is the approver we want to show the Place Order (and other) buttons  --->


					<cfset closedOppStages = application.com.settings.getSetting("leadManager.closedOppStages")>
					<!---Case 446669 RJT allow setting to override lockdown behaviour --->
					<cfset overrideReadOnlyBehaviour=request.relayCurrentUser.isInternal AND application.com.settings.getSetting("leadManager.allowInternalOpportunityEditingUnderAllCircumstances")>

					<!--- if the opportunity is readOnly, then don't show any of the buttons EXCEPT if special pricing has been Fully Approved --->
					<cfif overrideReadOnlyBehaviour or not oppReadOnly or (oppSPQStatusTextID eq "SPQFullyApproved") or okToApprove>					<!--- PPB 2010-05-20 added SPQFullyApproved check --->
						<cfif form.opportunityID neq 0>

							<cfset oppProductCount = application.com.opportunity.getOpportunityProductCount(opportunityID=form.opportunityID)>
							<cfif oppSPQStatusTextID neq "SPQFullyApproved">

								<cfif ShowDealRegistration>
									<cfset dealRegAttributes = structNew()>
									<!--- if Deal Reg has been applied for or approved, then disable the deal reg button; if we're using oppProducts and we have no products, disable button --->
									<!--- 2012/06/07 IH Case 428643 enable apply for deal reg button after the deal have been approved --->
									<!--- 2012/07/26 IH	Case 429506 disable ApplyForDealReg when deal status is approved/applied/rejected --->
									<!---
									<cfif listFindNoCase("DealRegApplied,DealRegApproved,DealRegRejected",oppStatusTextID) or ((disableDealRegWhenNoProducts and useOppProducts and not oppProductCount) or not useOppProducts)>
										<cfset dealRegAttributes.disabled=true>
									</cfif>
									 --->
									<cfif listFindNoCase("DealRegApplied,DealRegApproved",oppStatusTextID) or (disableDealRegWhenNoProducts and not oppProductCount)>
										<cfset dealRegAttributes.disabled="true">
									</cfif>

									<cfset dealRegAttributes.relayFormElementType="button">
									<cfset dealRegAttributes.onClick="ApplyForDealReg();">
									<cfset dealRegAttributes.currentValue="phr_opp_ApplyForDealReg">
									<cfif application.com.relayTranslations.doesPhraseTextIDExist('opp_ApplyForDealReg#oppTypeTextId#')>
										<cfset dealRegAttributes.currentValue = "phr_opp_ApplyForDealReg#oppTypeTextId#">
									</cfif>
									<cfset dealRegAttributes.fieldname="frmApplyForDealReg">
									<cfset dealRegAttributes.label="">
									<cf_relayFormElement attributeCollection=#dealRegAttributes#>
								</cfif>

								<!--- 2010-06-14			NAS			P-CLS002 Fields added as switches to turn fields/functionality on/off --->
								<cfif (partnerTier neq "Tier1Tier2DistributorTier2" and ((application.com.settings.getSetting("leadManager.specialPricing.useSpecialPricingOnInternal") and  request.relayCurrentUser.isInternal) OR application.com.settings.getSetting("leadManager.specialPricing.useSpecialPricingOnExternal"))) AND showBtnApplyForSpecialPricing>  <!--- START: 2014-04-28	MS	Case. 439476 Need to "lock down" opportunity on RW side if the stage is "Closed/Won" --->
									<!--- 2012-08-22 IH Case 425816 Disable phr_opp_ApplyForSpecialPricing if there are no Opp. Approvers --->
									<cfquery datasource="#application.siteDataSource#" name="qApprovers">
										SELECT 1 FROM oppSPApprover
										WHERE countryID = 0 or countryID = #request.relayCurrentUser.countryID#
									</cfquery>
									<cfset spAttributes = structNew()>
									<!--- if the opportunity is read only or if there are no products, then disable special pricing button --->
									<cfif oppReadOnly or not oppProductCount or oppSPQStatusTextID neq "standardPricing" or !qApprovers.recordCount>
										<cfset spAttributes.disabled=true>
									</cfif>
									<cfset spAttributes.onClick="showSpecialPricingScreen(#form.opportunityID#);">
									<!--- if the partner deals with a disti, then applying for special pricing creates a copy of the opportunity for the disti --->
									<cfif partnerTier eq "Tier1Tier2DistributorTier2">
										<cfset spAttributes.onClick="">
									</cfif>
									<cfset spAttributes.relayFormElementType="button">
									<cfset spAttributes.currentValue="phr_opp_ApplyForSpecialPricing">
									<cfif application.com.relayTranslations.doesPhraseTextIDExist('opp_ApplyForSpecialPricing#oppTypeTextId#')>
										<cfset spAttributes.currentValue = "phr_opp_ApplyForSpecialPricing#oppTypeTextId#">
									</cfif>
									<cfset spAttributes.fieldname="frmApplyForSpecialPricing">
									<cfset spAttributes.label="">
									<cf_relayFormElement attributeCollection=#spAttributes#>
								</cfif>
							</cfif>

							<!--- 2010-06-14			NAS			P-CLS002 Fields added as switches to turn fields/functionality on/off --->
							<cfif (not listFindNoCase(closedOppStages,oppStageTextID) or okToApprove) and application.com.settings.getSetting("leadManager.showPlaceOrderButton")>	<!--- PPB 2010-05-20 don't show the order button if the opp is ordered/closed (we wouldn't get here normally cos the opp would be read-only but do get here after special pricing has been approved) --->

								<!--- NJH 2010/06/04 - LID3549 --->
								<cfset placeOrderAttributes = structNew()>
								<cfset placeOrderAttributes.relayFormElementType = "button">
								<cfset placeOrderAttributes.currentValue = "phr_opp_PlaceOrder">
								<cfset placeOrderAttributes.fieldname = "frmPlaceOrder">
								<cfset placeOrderAttributes.label = "">

								<cfset encryptedQueryString = application.com.security.encryptQueryString('opportunityID=#opportunityID#')>
								<cfset placeOrderAttributes.onClick="javascript:placeOrder('#encryptedQueryString#');">

								<cfif not oppProductCount>
									<cfset placeOrderAttributes.disabled=true>
								</cfif>
								<cf_relayFormElement attributeCollection=#placeOrderAttributes#>
							</cfif>
						</cfif>
						<!---Case 446669 RJT allow setting to override lockdown behaviour --->
						<cfif overrideReadOnlyBehaviour or not listFindNoCase(closedOppStages,oppStageTextID)>	<!--- PPB 2010-05-20 don't show the save button if the opp is ordered/closed (we wouldn't get here normally cos the opp would be read-only but do get here after special pricing has been approved) --->
							<cfif application.com.relayTranslations.doesPhraseTextIDExist('opp_Save#oppTypeTextId#')>
								<cfset savePhrase = "phr_opp_Save#oppTypeTextId#">
							<cfelse>
								<cfset savePhrase = "phr_opp_Save">
							</cfif>
							<!--- START: 2014-04-28	MS	Case. 439476 Need to "lock down" opportunity on RW side if the stage is "Closed/Won" --->
							<cfif overrideReadOnlyBehaviour or showBtnSaveOpp>
								<cf_relayFormElement relayFormElementType="button" class="" currentValue="#savePhrase#" fieldname="frmSave" label="" id="frmSaveOpportunity" onClick="submitOpportunityForm();">		<!--- 2012-09-28 PPB Case 429630 disable the save button after clicking it to prevent 2+ opps being created, 2012-10-08 IH Case 431126 moved disabling save button to oppEdit.js > submitOpportunityForm() --->
							</cfif>
							<!--- END  : 2014-04-28	MS	Case. 439476 Need to "lock down" opportunity on RW side if the stage is "Closed/Won" --->

						</cfif>
					</cfif>
				</div>


				<div id="spButtonDiv" style="display:#IIF(structKeyExists(form,'frmTask') and form.frmTask eq 'SaveAndShowSpecialPrice',DE(''),DE('none'))#">
					<cf_relayFormElement relayFormElementType="button" currentValue="phr_opp_Cancel" fieldname="frmCancel" label="" onClick="javascript:window.location.href='#returnURL#'">
					<cf_relayFormElement relayFormElementType="button" currentValue="phr_opp_ExitSpecialPricing" fieldname="frmExitSpecialPricing" label="" onClick="updateOppProductsDiv(#opportunityID#,#oppReadOnly#);toggleSPButton(#opportunityID#);">
					<cf_relayFormElement relayFormElementType="button" currentValue="phr_opp_RequestSpecialPricing" fieldname="frmRequestSpecialPricing" label="" onClick="javascript:setCurrentTask('ApplyForSpecialPrice');submitOpportunityForm();">
				</div>
				</cfoutput>

		<cfif request.relayFormDisplayStyle eq "HTML-div">
				</td>
			</tr>
		</cfif>
	</cf_relayFormDisplay>
</cfform>
</cfif> <!--- ifOkToProceed --->

<cfsetting enablecfoutputonly="false">

