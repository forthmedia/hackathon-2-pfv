<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		moveOppToDiffAccountManager.cfm
Objective - to move one or more entities from one to a new organisation record.
				Steps involved are:
				a) receive a list of entityID's to move
				b) search for the org to move them to
				c) choose the one and update the database record

Amendment History:
Date (YYYY-MM-DD)	Initials 	What was changed

Enhancement still to do:
--->

<CFPARAM NAME="frmopportunityIDs" TYPE="string" DEFAULT="0">

<CFQUERY NAME="getOppDetails" datasource="#application.siteDataSource#">
	select detail, opportunityID, vendorAccountManagerPersonID
	from opportunity 
	where opportunityID  in ( <cf_queryparam value="#frmopportunityIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	order by detail
</CFQUERY>

<cfif structKeyExists(form,"frmSearch") eq "yes">	
	<cfset qAccountManager = application.com.validValues.getAccountManagers(searchString=form.frmSearchString)>	
</CFIF>

<cfif structKeyExists(form,"frmMove") and form.frmMove eq "Transfer">
	
	<CFQUERY NAME="updateOpportunity" datasource="#application.siteDataSource#">
		update opportunity 
		set vendorAccountManagerPersonID =  <cf_queryparam value="#form.frmSelectedAccountManagerID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		where opportunityID  in ( <cf_queryparam value="#form.frmopportunityIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	</CFQUERY>	
	
	<CFQUERY NAME="qNewAccountManager" datasource="#application.siteDataSource#">
		select firstname + ' ' + lastname as Name
		from person
		where personid = <cf_queryparam value="#form.frmSelectedAccountManagerID#" CFSQLTYPE="CF_SQL_INTEGER"> 
	</CFQUERY>
	
	<cfsavecontent variable="listOfOpportunities">
		<cfoutput><ul><cfloop query="getOppDetails" ><li>#detail# (#htmleditformat(opportunityID)#)</li></cfloop></ul></cfoutput>
	</cfsavecontent>
	
	<cfset mergeStruct = {listOfOpportunities=listOfOpportunities}>	
	<cfset application.com.email.sendEmail(personID=form.frmSelectedAccountManagerID,emailTextID="emailAccMngrOppAccMngrChange",mergeStruct=mergeStruct)>	
	<cfset application.com.email.sendEmail(personID=getOppDetails.vendorAccountManagerPersonID,emailTextID="oppOldAccountManagerNotification",mergeStruct=mergeStruct)>

	<CFSET message = "#valuelist(getOppDetails.detail,", ")# moved to Account Manager #htmlEditFormat(qNewAccountManager.name)#">
	
</CFIF>

<cf_head>
	<cf_title>Transfer Opportunity</cf_title>
</cf_head>

<cfset application.com.request.setTopHead(PageTitle='phr_Sys_MoveOppsToNewAccountManager',showTopHead="false")>

<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
	
	<cfif not structKeyExists(form,"frmMove")>
		<TR>
			<TD>The following opportunities will be transferred:<BR>
			<ol>
			<CFOUTPUT><cfloop query="getOppDetails" ><li>#detail# (#htmleditformat(opportunityID)#)</li></cfloop></CFOUTPUT>
			</ol>
			</TD>
		</TR>
	</cfif>
	
	<CFIF frmopportunityIDs eq "0">
		<cfform action="#cgi.script_name#" method="POST" name="personForm">
			<TR>
				<TD VALIGN="top" CLASS="label">opportunityID</TD>
				<TD><CFINPUT TYPE="Text" NAME="frmopportunityIDs" MESSAGE="You must put a valid opportunityID" REQUIRED="Yes" SIZE="15"></TD>
			</TR>
			<TR>
				<TD>&nbsp;</TD>
				<TD><INPUT TYPE="submit" VALUE="Search"></TD>
			</TR>
		</CFFORM>
	<CFELSEIF frmopportunityIDs neq "0" and not isDefined('message')>
		<cfform action="#cgi.script_name#" method="POST" name="orgSearchForm">
			<TR>
				<TD VALIGN="top"><HR WIDTH="100%" SIZE="1" COLOR="steelblue"></TD>
			</TR>
			<TR>
				<TD NOWRAP>Search for an existing Account Manager:&nbsp;<CFINPUT TYPE="Text" NAME="frmSearchString" SIZE="32">
				<INPUT TYPE="submit" NAME="frmSearch" VALUE="Search"></TD>
			</TR>
			<TR>
				<TD></TD>
			</TR>
			<CFOUTPUT><CF_INPUT TYPE="hidden" NAME="frmopportunityIDs" VALUE="#frmopportunityIDs#"></CFOUTPUT>
		</CFFORM>
	</CFIF>
	
	<CFIF frmopportunityIDs neq "0" and isDefined('qAccountManager.recordCount') and qAccountManager.recordCount gt 0 and not structKeyExists(form,"frmMove")>
		<cfform action="#cgi.script_name#" method="POST" name="orgSearchForm2">
			<TR>
				<TD VALIGN="top"><HR WIDTH="100%" SIZE="1" COLOR="steelblue"></TD>
			</TR>
			<TR>
				<TD>Select which Account Manager to move them to:<BR>
				<CFSELECT NAME="frmSelectedAccountManagerID"
			          SIZE="#min(qAccountManager.recordCount,15)#"
			          MESSAGE="You must select an Account Manager"
			          QUERY="qAccountManager"
			          VALUE="dataValue"
			          DISPLAY="displayValue"		         
			          REQUIRED="Yes">
				</CFSELECT>
				</TD>
			</TR>
			<TR>
				<TD ALIGN="right"><INPUT TYPE="submit" NAME="frmMove" VALUE="Transfer">&nbsp;&nbsp;&nbsp;</TD>
			</TR>
			<CFOUTPUT><CF_INPUT TYPE="hidden" NAME="frmopportunityIDs" VALUE="#frmopportunityIDs#"></CFOUTPUT>
		</CFFORM>
	</CFIF>
	
	<CFIF isDefined('message')>
		<TR>
			<TD VALIGN="top"><HR WIDTH="100%" SIZE="1" COLOR="steelblue"></TD>
		</TR>
		<TR><CFOUTPUT>
			<TD CLASS="messageText">The following opportunities :<BR>
				<ol>
					<cfloop query="getOppDetails" ><li>#detail# (#htmleditformat(opportunityID)#)</li></cfloop>
				</ol>
				have been linked to #htmleditformat(qNewAccountManager.name)#
			</TD>
			</CFOUTPUT>
		</TR>
		<tr><td><input type=button value="Close Window" onClick="javascript:window.opener.location.reload(true); if (window.opener.progressWindow) { window.opener.progressWindow.close() } ;self.close();"></td></tr>
	</CFIF>
	
</TABLE>
<script type="text/javascript">jQuery(document).ready(function(){
   document.body.scrollTop = 0;
});</script>