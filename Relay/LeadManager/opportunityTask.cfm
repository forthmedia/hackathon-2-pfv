<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		opportunityTask.cfm
Author:			NJH
Date started:	01-04-2010

Description:	Tasks to perform when opportunityEdit is submitted

Amendment History:

Date (DD/MM/YYYY)	Initials 	What was changed
2010-05-18 			AJC 		LID 3350 & LID 3355 - Using wrong field to get account manager
2010-05-20			AJC			LID 3444 - Account Manager was not being set by default
2010-05-21			AJC			LID 3399 - Opportunity Creation and Deal Reg email Notification - Missing Merge fields
2010-05-25			NAS			LID 3396 - Special Pricing Request - no email
2010-06-28			NAS			P-CLS002
2012-08-10 			PPB 		Case 428651 set the EndCustomerOppContacts flag
2013-08-23 			PPB 		Case 436754 refreshPromotions() on save rather than on page load
2014-07-22          REH         Case 440898 declared mergeStruct at top of script and appending vars as we go along (so custom merge fields can be added in customOpportunityTaskExtension.cfm)
2014-08-27 			PPB 		Case 441463 defend against non-existence of form.vendorAccountManagerPersonID throughout
2014-10-28			RPW			CORE-666 OPP Edit Screen needs multiple XML Changes
2014-10-14 			PPB 		P-KAS040 hook for post process customOpportunityTaskExtensionPostProcess.cfm
2014-10-29			RPW			CORE-901 Error seen while  i request for special Pricing(Opportunities) in internal site
2015-04-22			WAB			PGI-001-004 Added a bookmark (#products) to the cflocation after first save of record.
								Added an addToken=false

Possible enhancements:


 --->

<cfif structKeyExists(form,"frmTask")>

	<cfif listFindNoCase("Save,PlaceOrder,ApplyForDealReg,SaveAndShowSpecialPrice",form.frmTask)>

		<cfset oldOpportunityDetails = application.com.opportunity.getOpportunityDetails(opportunityID=form.opportunityID)>
		<!--- 2012-11-14 - PJP - P-XIR001 / 431945 - Moved From Opportuntity Edit - This should be checking the old status first - we may want to add a setting --->
		<cfif (not structKeyExists(form,"statusID")) and form.frmTask is "Save">

			<cfif IsNumeric(oldOpportunityDetails.statusID)>
				<cfset form.statusID = oldOpportunityDetails.statusID>
			<cfelse>
				<cfset form.statusID = "">
			</cfif>

		</cfif>
		<!--- 2012-11-14 - PJP - P-XIR001 - End --->

		<!--- if partner is applying for deal registration, set the deal reg value so it gets set --->
		<cfif form.frmTask eq "ApplyForDealReg">
			<cfset form.dealReg = 1>
			<cfset form.statusID = 1>

		<cfelseif form.frmTask eq "PlaceOrder">

			<!--- PPB 2010-07-06 was checking flag based on partnerSalesPersonID (but it can be left unassigned)  --->
			<cfset partnerOrganisationId = application.com.RelayPLO.GetLocationOrg(oldOpportunityDetails.partnerLocationId).organisationID>
			<cfset orderApprovalRequired = application.com.flag.getFlagData(flagId="internalOrderApprovalReqYes",entityID=partnerOrganisationId).recordcount>
			<cfset currentUserIsApprover = application.com.flag.isFlagSetForCurrentUser("PartnerOrderingManager")>
			<cfif orderApprovalRequired and not currentUserIsApprover>
				<cfset oppStage = "oppStageAwaitingApproval">
			<cfelse>
				<cfset oppStage = "oppStagePlaceOrder">
			</cfif>

			<!--- if tier2 partner, if the disti is not "order in PRM", then set oppportunity stage to closed --->
			<cfif partnerTier eq "Tier1Tier2DistributorTier2">
				<cfif not application.com.flag.isFlagSetForPerson(flagID="OnLineOrderingYes",personId=form.distiSalesPersonID)>
					<cfset oppStage = "oppStageClosed">
				<cfelse>
					<cfset oppStage = "oppStageOrderViaDisti">
				</cfif>
			</cfif>

			<cfquery name="getPlaceOrderStage" datasource="#application.siteDataSource#">
				select opportunityStageId from oppStage where stageTextID =  <cf_queryparam value="#oppStage#" CFSQLTYPE="CF_SQL_VARCHAR" >
			</cfquery>

			<cfif getPlaceOrderStage.recordCount>
				<cfset form.StageID = getPlaceOrderStage.opportunityStageId>		<!--- PPB 2010-05-20 changed oppStageID to StageID --->
			</cfif>
		</cfif>

		<cfset mergeStruct = {} />				<!--- 2014-07-22 REH case 440898 setting var at top of script so it can be shared --->

		<cfif form.opportunityID eq 0>
			<cfset result = application.com.relayEntity.insertOpportunity(opportunityDetails=form)>
			<cfif result.isOK>
				<cfset form.opportunityID = result.entityID>
				<cfset opportunityID = form.opportunityID>
				<cfset application.com.flag.processFlagForm (entityid=form.opportunityID,entityType="opportunity",formEntityID=0)>

				<!---START: 2011/11/18	MS	P-NET009	Requesting Demo Products Via Opportunity ie handeling OPPTYPEID=4 and oppTypeTextID=Demo--->
				<cf_include template ="/Code/CFTemplates/customOpportunityTaskExtension.cfm" checkIfExists="true">

				<!---START: 2014-10-14 PPB P-KAS040	hook for post process --->
				<!---
					NB customOpportunityTaskExtensionPostProcess.cfm is included here for compatibility with the UPDATE functionality below
					The code STILL includes customOpportunityTaskExtension.cfm AFTER the insert but BEFORE the update, which is incompatible
					I would like to move the customOpportunityTaskExtension.cfm to before the INSERT so that we have a PRE and a POST PROCESS (but that must be done in conjunction with looking at each customisation and potentially splitting code)
				 --->
				<cfif fileexists("#application.paths.code#\CFTemplates\customOpportunityTaskExtensionPostProcess.cfm")>
					<cfinclude template ="/Code/CFTemplates/customOpportunityTaskExtensionPostProcess.cfm">
				</cfif>
				<!---END: 2014-10-14 PPB P-KAS040 --->

				<!--- 2012-08-10 PPB Case 428651 set the EndCustomerOppContacts here in case the opp is added in the internal (in which case the newly added end contact WAS being associated to the current user (not the reseller sales person);
				also if an endcustomer has been added to the opp but a reseller not specified on creation this will establish the link on edit of opp;
				this flag is also set in addContactForm.cfm and editEndCustomer.cfm because we want a newly added contact to have the flag so that they appear in the contact dropdown on the opp (ie before we have saved the opp)
				 --->
				<!---
					NJH moved to insert opp function
				<cfif structKeyExists(form,"ContactPersonId") and form.ContactPersonId neq 0 and isNumeric(form.ContactPersonId) and structKeyExists(form,"PartnerSalesPersonId") and form.PartnerSalesPersonId neq 0 and isNumeric(form.PartnerSalesPersonId)>
					<cfset application.com.flag.setFlagData(flagId='EndCustomerOppContacts',entityid=form.ContactPersonId,data=form.PartnerSalesPersonId)>
				</cfif>	 --->


				<!--- Send email to CAM. --->
				<cfset structInsert(mergeStruct, 'opportunityID', form.opportunityID)>			<!--- 2014-07-22 REH case 440898 inserting var into struct --->
				<cfif StructKeyExists(form,"vendorAccountManagerPersonID") and form.vendorAccountManagerPersonID neq 0>		<!--- 2014-08-27 PPB Case 441463 defend against non-existence of form field --->
					<cfset application.com.email.sendEmail(personID=form.vendorAccountManagerPersonID,emailTextID="oppAccountManagerNotification",mergeStruct=mergeStruct)>
				</cfif>
				<!--- NJH 2017/01/03 PROD2016-3082 - send email to partner --->
				<cfif StructKeyExists(form,"partnerSalesPersonID") and form.partnerSalesPersonID neq 0>
					<cfset application.com.email.sendEmail(personID=form.partnerSalesPersonID,emailTextID="OpportunityEmail",mergeStruct=mergeStruct)>
				</cfif>

				<!--- 2014-10-28	RPW	CORE-666 OPP Edit Screen needs multiple XML Changes --->
				<cfscript>
					if (IsDefined("FORM.uploadUUID") AND Len(FORM.uploadUUID) EQ 36) {
						application.com.relatedFile.reallocateRelatedFiles(
							uploadUUID = FORM.uploadUUID,
							entityID = result.entityID
						);
					}
				</cfscript>
				<!---
				 the following re-direction is to 're-initialise' the page so that it can't be re-submitted
				 previously if the user hit page refresh during product entry (of a new opp) a duplicate opp was created
				 --->
				<cfif not request.relayCurrentUser.isInternal>
					<cfset encryptedQueryString = application.com.security.encryptQueryString('eid=oppEdit&opportunityID=#opportunityID#&useSavedReturnURL=1')>
					<!--- 2014-01-31	WAB 	removed references to et cfm, can always just use / or /? --->
					<!--- 2015-04-22	WAB		PGI-001-004 #products bookmark so that we jump to the add Products area after the first save.  Doesn't worry whether system is using products!  --->
					<cflocation url="\?#encryptedQueryString###products" addtoken="false">
				</cfif>
			</cfif>
		<cfelse>

			<!---START: 2012/02/06	MS	P-NET009	Sending out email on Demo Opp Submit--->
			<cf_include template ="/Code/CFTemplates/customOpportunityTaskExtension.cfm" checkIfExists="true">

			<!--- 2013/02/14	YMA	Fix issue where checkboxes weren't saving in Opp edit. --->
			<cfloop collection="#form#" item="formField">
				<cfset origFormField = replaceNoCase(formField,"_checkbox","")>
				<cfif right(formField,9) eq "_checkbox" and not structKeyExists(form,origFormField)>
					<cfset form[origFormField] = 0>
				</cfif>
			</cfloop>

			<cfset result = application.com.relayEntity.updateOpportunityDetails(opportunityID=form.opportunityID,opportunityDetails=form)>
			<cfset application.com.flag.processFlagForm (entityid=form.opportunityID,entityType="opportunity")>

			<!--- 2012-08-10 PPB Case 428651 set the EndCustomerOppContacts here in case the opp is added in the internal (in which case the newly added end contact WAS being associated to the current user (not the reseller sales person);
			also if an endcustomer has been added to the opp but a reseller not specified on creation this will establish the link on edit of opp;
			this flag is also set in addContactForm.cfm and editEndCustomer.cfm because we want a newly added contact to have the flag so that they appear in the contact dropdown on the opp (ie before we have saved the opp)
			 --->
			<!--- NJH  - moved to updateOpportunity function
			<cfif structKeyExists(form,"ContactPersonId") and form.ContactPersonId neq 0 and isNumeric(form.ContactPersonId) and structKeyExists(form,"PartnerSalesPersonId") and form.PartnerSalesPersonId neq 0 and isNumeric(form.PartnerSalesPersonId)>
				<cfset application.com.flag.setFlagData(flagId='EndCustomerOppContacts',entityid=form.ContactPersonId,data=form.PartnerSalesPersonId)>
			</cfif> --->


			<cfset structInsert(mergeStruct, 'opportunityID', form.opportunityID)>			<!--- 2014-07-22 REH case 440898 inserting var into struct --->

			<!---START: 2014-10-14 PPB P-KAS040	hook for post process so emails can be sent only AFTER the update has succeeded --->
			<cfif fileexists("#application.paths.code#\CFTemplates\customOpportunityTaskExtensionPostProcess.cfm")>
				<cfinclude template ="/Code/CFTemplates/customOpportunityTaskExtensionPostProcess.cfm">
			</cfif>
			<!---END: 2014-10-14 PPB P-KAS040 --->

			<!--- if the stage changes, email the account manager --->
			<cfif structKeyExists(form,"stageID") and form.stageID neq oldOpportunityDetails.stageID>
				<cfif StructKeyExists(form,"vendorAccountManagerPersonID") and form.vendorAccountManagerPersonID neq 0>		<!--- 2014-08-27 PPB Case 441463 defend against non-existence of form field --->
					<cfset application.com.email.sendEmail(personID=form.vendorAccountManagerPersonID,emailTextID="emailAccMngrOppStageChange",mergeStruct=mergeStruct)>
				</cfif>
			</cfif>

			<!--- if the account manager changes, email the new account manager --->
			<cfif structKeyExists(form,"vendorAccountManagerPersonID") and form.vendorAccountManagerPersonID neq oldOpportunityDetails.vendorAccountManagerPersonID>
				<cfif isNumeric(form.vendorAccountManagerPersonID) and form.vendorAccountManagerPersonID neq 0>
					<cfset application.com.email.sendEmail(personID=form.vendorAccountManagerPersonID,emailTextID="emailAccMngrOppAccMngrChange",mergeStruct=mergeStruct)> <!--- 2013-05-22	YMA	Case 435382 Account manager change is not notifying new account manager --->
				</cfif>
			</cfif>

			<CF_UGRecordManagerTask>
		</cfif>

		<cfif form.frmTask eq "ApplyForDealReg">
			<!--- if the reseller has applied for deal registration, then send email to the Channel Sales Rep, notifying them of the application --->
			<cfif StructKeyExists(form,"vendorAccountManagerPersonID") and form.vendorAccountManagerPersonID neq 0>		<!--- 2014-08-27 PPB Case 441463 defend against non-existence of form field --->
				<cfset application.com.email.sendEmail(personID=form.vendorAccountManagerPersonID,emailTextID="dealAccountManagerApproval",mergeStruct=mergeStruct)>
			</cfif>																										<!--- 2014-08-27 PPB Case 441463 --->

			<!--- 2012-06-08 PPB MIC001/LEX070 now pop up the message (oppedit.js) and dont pass it thro in the message variable on the returnURL --->
			<!--- 2012/11/20	YMA		CASE: 432143 Undone  2012-06-08 PPB MIC001/LEX070 and moved deal reg submitted message to occur after form validation takes place. --->
			<cfset message = "phr_opp_ThankYouYourDealRegApplicationHasBeenSubmitted">
		<cfelseif form.frmTask eq "PlaceOrder">

			<cfset placeOrder = true>

			<!--- if tier2 and disti is not "order in PRM", then email account manager --->
			<cfif partnerTier eq "Tier1Tier2DistributorTier2">
				<cfif not application.com.flag.isFlagSetForPerson(flagID="OnLineOrderingYes",personId=form.distiSalesPersonID)>
					<cfif StructKeyExists(form,"vendorAccountManagerPersonID") and form.vendorAccountManagerPersonID neq 0>		<!--- 2014-08-27 PPB Case 441463 defend against non-existence of form field --->
						<cfset application.com.email.sendEmail(personID=form.vendorAccountManagerPersonID,emailTextID="",mergeStruct=mergeStruct)>
					</cfif>																										<!--- 2014-08-27 PPB Case 441463 defend against non-existence of form field --->

					<cfset placeOrder = false>
				</cfif>
			</cfif>

			<cfif placeOrder>
				<!--- a flag that determines whether approval is needed when placing an order --->
				<cfif not orderApprovalRequired OR currentUserIsApprover>

					<!--- sending the confirmation email whether or not approval is required  --->
					<cfif currentUserIsApprover>
						<cfset application.com.opportunity.approveOpportunity(opportunityID=form.opportunityID)>
					</cfif>
					<cfset application.com.opportunity.processOpportunity(opportunityID=form.opportunityID)>

<!---
					<cfset application.com.email.sendEmail(personID=form.partnerSalesPersonID,emailTextID="OrderConfirmationEmail",mergeStruct=mergeStruct)>
					<cfset application.com.flag.setBooleanFlag(entityID=form.opportunityID,flagTextID="SalesForceTransferOK")>
 --->
					<cfset message = "phr_opp_ThankYouYourOrderHasBeenPlaced">
				<cfelse>

					<cfquery name="getPartnerOrganisation" datasource="#application.siteDataSource#">
						select organisationID from person where personID =  <cf_queryparam value="#form.partnerSalesPersonID#" CFSQLTYPE="CF_SQL_INTEGER" >
					</cfquery>

					<cfset oppApproverDetails = application.com.flag.getFlagData(flagID="PartnerOrderingManager",entityID=getPartnerOrganisation.organisationID)>

					<cfquery name="qryEndCustomerMainContact" datasource="#application.siteDataSource#">
						SELECT firstname + ' ' + lastname as maincontact
						FROM Person WHERE PersonID =  <cf_queryparam value="#form.contactPersonID#" CFSQLTYPE="CF_SQL_INTEGER" >
					</cfquery>


					<!--- i set up a seperate mergeStruct for this email because the [[firstname]] was being output incorrectly (the firstname for the person sent in was being stomped over by that in mergestruct)... with heinsight i think it would work by using the original mergestruct and changing the email definition to output Dear [[mergestruct.partner.firstname]] --->
					<cfset emailMergeStruct = StructNew()>
					<cfset emailMergeStruct.maincontact = qryEndCustomerMainContact.maincontact>
					<cfset emailMergeStruct.oppID = form.opportunityID>			<!--- defined cos reference to [[opportunityID]] didn't work in the email subject --->
					<cfset emailMergeStruct.EndCustomer = mergeStruct.EndCustomer>

					<cfset application.com.email.sendEmail(personID=oppApproverDetails.data,emailTextID="partnerOrderApproval",mergeStruct=emailMergeStruct)>
					<cfset message = "phr_opp_ThankYouYourOrderHasBeenSentForApproval">
				</cfif>

			</cfif>
		</cfif>

		<!--- if the deal registration has been approved --->
		<cfif structKeyExists(form,"statusID") and oldOpportunityDetails.statusID neq "">  <!--- it will be "" if the opportunity is a new one --->
			<cfset oldOppStatusTextID = application.com.opportunity.getStatusID(statusID=oldOpportunityDetails.statusID).statusTextID>
			<cfset oppStatusTextID = application.com.opportunity.getStatusID(statusID=form.statusID).statusTextID>

			<!--- if deal reg has been approved ---><!--- 2016/07/19 GCC 449849 added listlast as there are examples of partners getting emails more than once and can't find a better explanation --->
			<cfif oppStatusTextID eq "DealRegApproved" and oldOppStatusTextID neq "DealRegApproved">
				<cfset application.com.email.sendEmail(emailTextID="dealApprovedEmail",personID=listlast(form.partnerSalesPersonID,","),mergeStruct=mergeStruct)>
				<cfset application.com.opportunity.addOppPromotion(opportunityID=form.opportunityID,promotionID="DealRegPromo")>
			<cfelseif oppStatusTextID eq "DealRegRejected">
				<cfset application.com.email.sendEmail(emailTextID="dealDeclinedEmail",personID=listlast(form.partnerSalesPersonID,","),mergeStruct=mergeStruct)>
			</cfif>
		</cfif>

		<cfset application.com.opportunity.refreshPromotions(opportunityID=form.opportunityID)>		<!--- 2013-08-23 PPB Case 436754 was previously done on every page load, now on save only --->
	</cfif>

	<!--- Applying for special pricing --->
	<cfif form.frmTask eq "ApplyForSpecialPrice" and ((application.com.settings.getSetting("leadManager.specialPricing.useSpecialPricingOnInternal") and  request.relayCurrentUser.isInternal) OR application.com.settings.getSetting("leadManager.specialPricing.useSpecialPricingOnExternal"))>

		<cfif partnerTier neq "Tier1Tier2DistributorTier2">
			<cfset oppSpecialPricingStruct = structNew()>

			<cfloop collection="#form#" item="fieldname">
				<cfif findNoCase("frmSpecialPrice_",fieldName)>
					<cfset productID = listLast(fieldname,"_")>
					<cfset quantityFieldName = "frmSpecialPriceQuantity_" & #productID#>

					<cfif form[fieldname] eq "">
						<cfset oppSpecialPricingStruct[productID] = "">
					<cfelse>
						<cfset oppSpecialPricingStruct[productID] = form[fieldname]/form[quantityFieldName]>
					</cfif>
				</cfif>
			</cfloop>

			<!--- 2014-10-29			RPW			CORE-901 Error seen while  i request for special Pricing(Opportunities) in internal site --->
			<cfset application.com.oppSpecialPricing.updateSpecialPricing(opportunityID=form.opportunityID,oppSpecialPricingStruct=oppSpecialPricingStruct,specialPricingStatusMethod="SP_Request",personID=request.relayCurrentUser.personID)>
			<!--- 2010-05-25	START		NAS			LID 3396 - Special Pricing Request - no email --->
			<cfset spApproverLevel = application.com.oppSpecialPricing.checkApprover(opportunityID=form.opportunityID,personID=request.relayCurrentUser.personId,countryID=form.countryID)>
			<cfset SPApproverLevelRequired = application.com.oppSpecialPricing.checkApproverLevelRequired(opportunityId=form.opportunityID)>
			<cfset SPApproverLevelRequired = "#listFirst(SPApproverLevelRequired,"|")#">
			<!--- append country-specific recipients to cc list --->
			<cfset getCountryEmail = application.com.oppSpecialPricing.getCountrySPQEmailRecipients(opportunityID=form.opportunityID,SPApproverLevel=SPApproverLevel)>
			<cfset SPQCountryCCEmailList = valueList(getCountryEmail.email)>
			<!--- append users with view rights to the cc list --->
			<cfset getViewRightsEmail = application.com.oppSpecialPricing.getPeopleWithSPViewRights(opportunityID=form.opportunityID)>
			<cfset SPQViewCCEmailList = valueList(getViewRightsEmail.email)>
			<!--- 2011/05/27 PPB REL106 get email lists from settings --->
			<cfset SPQLevel1and2CCEmailList = application.com.settings.getSetting("leadManager.SPQLevel1and2CCEmailList")>
			<cfset SPQLevel2CCEmailList = application.com.settings.getSetting("leadManager.SPQLevel2CCEmailList")>
			<cfset SPQLevel3CCEmailList = application.com.settings.getSetting("leadManager.SPQLevel3CCEmailList")>
			<cfset SPQLevel4CCEmailList = application.com.settings.getSetting("leadManager.SPQLevel4CCEmailList")>
			<cfset SPQLevel5CCEmailList = application.com.settings.getSetting("leadManager.SPQLevel5CCEmailList")>
			<cfset application.com.oppSpecialPricing.progressSpecialPricing(opportunityID=form.opportunityID,specialPricingStatusMethod="SP_Request",personID=request.relayCurrentUser.personID,comments="Special Pricing Requested",SPApproverLevel=spApproverLevel,SPApproverLevelRequired=SPApproverLevelRequired,SPQCountryCCEmailList=SPQCountryCCEmailList,SPQLevel2CCEmailList=SPQLevel2CCEmailList,SPQLevel3CCEmailList=SPQLevel3CCEmailList,SPQLevel4CCEmailList=SPQLevel4CCEmailList,SPQLevel5CCEmailList=SPQLevel5CCEmailList,SPQViewCCEmailList=SPQViewCCEmailList)>
			<!--- 2010-05-25	END		NAS			LID 3396 - Special Pricing Request - no email --->

		<!--- if the partner is a tier2 partner, then create copy of the opportunity --->
		<cfelse>
			<cfset form.resellerOppID = form.opportunityID>
			<cfset form.partnerLocationID = form.distiLocationID>

			<!--- 2012/06/06 IH changed CFC name from relayPLO to relayEntity --->
			<cfset result = application.com.relayEntity.insertOpportunity(opportunityDetails=form,insertIfExists=false,matchOpportunityColumns="resellerOppID")>
			<!--- if there is a problem creating a new opportunity for the disti... --->
			<cfif not result.isOK>
			<cfelse>
				<!--- email the disti and copy the products --->
				<cfset oppProductDetails = application.com.dbTools.getTableDetails(tableName="opportunityProduct",returnColumns=true)>
				<cfset oppProductColumns = valueList(oppProductDetails.column_name)>
				<!--- remove unwanted columns --->
				<cfset oppProductColumns = listDeleteAt(oppProductColumns,listFindNoCase(oppProductColumns,"subtotal"))>
				<cfset oppProductColumns = listDeleteAt(oppProductColumns,listFindNoCase(oppProductColumns,"oppProductID"))>
				<cfset oppProductColumns = listDeleteAt(oppProductColumns,listFindNoCase(oppProductColumns,"opportunityID"))>

				<cfquery name="copyOppProducts" datasource="#application.siteDataSource#">
					insert into opportunityProduct (opportunityID,#oppProductColumns#)
					select #result.entityID#,#oppProductColumns# from opportunityProduct where opportunityID = #form.opportunityID#
				</cfquery>
			</cfif>

		</cfif>
	</cfif>
</cfif>