<!--- �Relayware. All Rights Reserved 2014

2014-01-31	WAB 	removed references to et cfm, can always just use / or /?
--->
	<cfif not application.com.security.confirmFieldsHaveBeenEncrypted(fieldNames="orgid")>
	      <CF_ABORT>
	</cfif>
	<cfparam name="message" default="">
	<cfparam name="createOpp" default="false">
	<cfparam name="renew" default="false">
	<cfparam name="startRow" default="1">
	<cfparam name="numRowsPerPage" default="100">
	<cfparam name="inIframe" default=0>			<!--- if called from a CustomerController Tab will be 1 --->
	<cfparam name="sortOrder" default="EndDate">

	<cfif inIframe>
		<cfset request.document.addHTMLDocumentTags = false>
	</cfif>

	<cfif request.relayCurrentUser.isInternal>
		<cfset organisationType = application.com.relayPLO.getOrganisationType(frmOrganisationID)>

		<cfif organisationType eq "EndCustomer">
			<cfset resellerOrgID = 0>
			<cfset customerOrgID = frmOrganisationID>
		<cfelse>
			<cfset resellerOrgID = frmOrganisationID>
			<cfset customerOrgID = 0>
		</cfif>

		<cfset columnHeadingList= "Customer,Contact,Description,Renewal Date,Value">
		<cfset showTheseColumns = "organisationName,PersonFullName,Description,EndDate,Price">
	<cfelse>
		<cfset resellerOrgID = request.relaycurrentuser.organisationID>
		<cfset customerOrgID = session.customerOrgID>
		<cfset columnHeadingList= "Description,Renewal Date,Value,blank">
		<cfset showTheseColumns = "Description,EndDate,Price,RenewButton">
	</cfif>

		<cfif structkeyexists(url,"assetid")>
			<cfif not application.com.security.confirmFieldsHaveBeenEncrypted(fieldNames="assetid")>
				<CF_ABORT>
			</cfif>
			<cfset Asset = application.com.asset.getAssets(assetID=url.assetID)>
			<cfif asset.recordcount gt 0 and asset.renewaloppid eq "">
				<cfset createOpp = true>
			</cfif>
		</cfif>

		<cfif renew and createOpp>

			<cfset returnURL=application.com.security.encryptURL("/?eid=customerController&startingTab=renewals&starttab=SalesHistory")>

			<cfset Asset = application.com.asset.getAssets(assetID=URL.assetID)>
			<cfset AssetInfo = application.com.structureFunctions.queryRowToStruct(query=Asset,row=1)>
			<cfset RenewAsset = application.com.asset.RenewAsset(asset=AssetInfo)>
 			<cfset redirectURL = application.com.security.encryptQueryString("/?eid=oppEdit&opportunityID=#RenewAsset#&returnURL=#returnURL#")>

			<script type="text/javascript">
				parent.location.href = '<cfoutput>#jsStringFormat(redirectURL)#</cfoutput>';
			</script>
		</cfif>

		<cfset qryAssets = application.com.asset.getAssets(resellerOrgID=resellerOrgID,organisationID=customerOrgID,sortOrder=sortOrder,includeRenewed=true)>
		<cfif qryAssets.recordCount gt 0>

		<cfoutput>
			<script>
				function renewOpp (params) {
					parent.location.href='#jsStringFormat(cgi.SCRIPT_NAME)#?renew=true&' + params
				}
			</script>
		</cfoutput>

		<CF_tableFromQueryObject
			queryObject="#qryAssets#"
			sortOrder = "#sortOrder#"
			startRow = "#startRow#"
			openAsExcel = "false"
			numRowsPerPage="#numRowsPerPage#"
			totalTheseColumns=""
			GroupByColumns=""
			ColumnTranslation="true"
		    ColumnTranslationPrefix="phr_"
			ColumnHeadingList="#columnHeadingList#"
			showTheseColumns="#showTheseColumns#"
			FilterSelectFieldList=""
			FilterSelectFieldList2=""
			hideTheseColumns=""
			numberFormat=""
			dateFormat="EndDate"
			showCellColumnHeadings="no"
			keyColumnURLList=" "
			keyColumnKeyList="assetID"
			keyColumnTargetList=""
			keyColumnOnClickList="renewOpp('##application.com.security.encryptQueryString('assetID=##assetID##')##')"
			keyColumnList="RenewButton"
			keyColumnOpenInWindowList="no"
			OpenWinSettingsList=""
			useInclude = "false"
			allowColumnSorting = "true"
			CurrencyISOColumn="currencyISOCode"
			currencyFormat="Price"
			KeyColumnLinkDisplayList="button"
		>
	<cfelse>
		phr_opp_NoAssetsToList
	</cfif>