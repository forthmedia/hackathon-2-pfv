<!--- �Relayware. All Rights Reserved 2014 --->
<cfparam name="textLength" default="35">
<cfparam name="countryID" default="#request.relaycurrentuser.countryID#">

<SCRIPT type="text/javascript">
	function moveItemTo(itemObj,amountObj,copyToObj) {
		if (itemObj.selectedIndex == -1 || itemObj.value == "" || itemObj.value == 0) {
			alert("You must select a product");
			itemObj.focus();
		} else if (amountObj.value == "" || isNaN(amountObj.value)) {
			alert("You must specify an amount.");
			amountObj.focus();
		} else {
			var objToCopy = itemObj[itemObj.selectedIndex];
			var checkIndex = checkForDupe(copyToObj,objToCopy.value);

			if (isNaN(checkIndex)) {
				var useIndex = copyToObj.options.length;
				copyToObj.options.length = copyToObj.options.length + 1;
			} else {
				var useIndex = checkIndex;
			}
			var newTextValue = objToCopy.text;
			<cfoutput>
			if (objToCopy.text.length >= #jsStringFormat(textLength)#) {
				newTextValue = objToCopy.text.slice(0,#jsStringFormat(textLength)#)+"...";
			}
			</cfoutput>
				
			copyToObj.options[useIndex].value = objToCopy.value+"~"+amountObj.value;
			copyToObj.options[useIndex].text = newTextValue+" ("+amountObj.value+")";
			//amountObj.value = "1";
		}
	}
	
	function removeItem(selectObj) {
			if (selectObj.selectedIndex == -1 || selectObj.value == "" || selectObj.value == 0) {
			alert("You must select a product to remove");
			selectObj.focus();
			} else {
			for (var i=selectObj.options.length-1;i>=0;i--) {
				if (selectObj.options[i].selected) {
					selectObj.options[i] = null;
				}
			}
		}
	}
	
	function checkForDupe(selectItem,itemValue) {
		for (var i=0;i<selectItem.length;i++) {
			var valueSplit = selectItem[i].value.split("~");
			if (valueSplit[0] == itemValue) {
				return i;
			}
		}
		return "goahead";
	}
</script>

<table class="withBorder">
	<tr>
		<td valign="bottom">
			
			<cfscript>
			// create an instance of the opportunity component
if (fileexists(application.paths.code & "\cftemplates\relayOpportunity.cfc")) {
	componentPath = "code.CFTemplates.relayOpportunity";
	myopportunity = createObject("component",componentPath);}
else {myopportunity = createObject("component","relay.com.opportunity");}
			myopportunity.dataSource = application.siteDataSource;
			myopportunity.opportunityID = opportunityID;
			myopportunity.productCatalogueCampaignID = application.com.settings.getSetting("leadManager.products.productCatalogueCampaignID");
			myopportunity.countryID = countryID;
			</cfscript>
			<cfscript>
				myopportunity.getProductAndGroupList();
			</cfscript>
			<cf_twoselectsrelated
				query="myopportunity.qProductAndGroupList"
				name1="productGroupID"
				name2="thisProductID"
				display1="productGroupDescription"
				display2="productDescription"
				value1="productGroupID"
				value2="productID"
				forcewidth1="30"
				forcewidth2="40"
				size1="1"
				size2="6"
				autoselectfirst="Yes"
				emptytext1="(Select Product Group)"
				emptytext2="(Optional Product)"
				forcelength2 = "6"
				formname = "mainProducts"
				HTMLBetween="<br>">
		</td>
		<td align="center">Qty<br>
			<input type="text" name="txtAmount" value="1" size="5"><br>
			<input type="button" value=">>" name="bAdd" onClick="moveItemTo(thisProductID,txtAmount,productsSelected);">
		</td>
		<td valign="bottom">
			<SELECT NAME="productsSelected" SIZE="6" STYLE="width:300px" MULTIPLE>
				<cfif isDefined("qrySelectedProducts")>
					<cfoutput query="qrySelectedProducts">
						<cfset optionLabel = SKU&' '&replaceNoCase(replaceNoCase(replaceNoCase(description,CHR(34),' inch','all'),CHR(39),'','all'),CHR(47),'','all')>
						<OPTION value="#productID#~#quantity#">
						#left(optionLabel,textLength)#<cfif len(optionLabel) gte textLength>...</cfif> (#htmleditformat(quantity)#)
						</OPTION>
					</cfoutput>
				</cfif>
			</SELECT>
		</td>
		<td valign="bottom"><input type="button" value="Remove" name="bDelete" onClick="removeItem(productsSelected);"></td>
	</tr>
</table>



