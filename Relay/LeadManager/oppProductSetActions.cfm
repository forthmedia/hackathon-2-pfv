<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			oppProductSetActions.cfm
Author:				NM
Date started:		02 August 2005

Description:

- Actions for Product Sets for lead and opportunity. A product set is a grouping of products.
- Create
- Edit
- Delete
- User Rights

Amendment History:

Date (DD-MMM-YYYY)	Initials	Code	What was changed
2009/02/09 			NJH			P-SNY047 changed from form to cfform as ugRecordmanager changed to using cfselect
WAB 2016-01-29	Removed call to Sel All Function and cfform

Possible enhancements:

--->
<cfparam name="psAction" default="">
<cf_translate>


<cf_head>
<cf_title><cfoutput>#htmleditformat(psAction)#</cfoutput> Product Set</cf_title>
<SCRIPT type="text/javascript">
	function doProductSubmit(productForm) {

		if (productForm.newProductSetName.value == "") {
			alert("Please give your Product Set a name.");
			productForm.newProductSetName.focus();
		} else {
			for (var i=0;i<productForm.productsSelected.options.length;i++) {
				productForm.productsSelected.options[i].selected = true;
			}
			productForm.submit();
		}
	}
</script>
</cf_head>



<table width="100%" class="withBorder">
	<tr>
		<td align="center">
<cfswitch expression="#psAction#">
	<cfcase value="Create">
		<cfif isDefined("newProductSetName")>
			<cfscript>
				application.com.oppProductSets.createProductSetWithUser(newProductSetName,productsSelected,3);
			</cfscript>
			<cfoutput>Product Set #htmleditformat(newProductSetName)# has been created.</cfoutput>
			<br><br>
			<a href="JavaScript:window.close()">close</a>
			<SCRIPT type="text/javascript">
				opener.location.reload(false);
			</script>
		<cfelse>

			<form name="mainProducts" action="" method="post">
			<cfoutput><CF_INPUT type="hidden" name="psAction" value="#psAction#"></cfoutput>
			<table class="withBorder">
				<tr>
					<td align="left">New Product Set Name : <input type="text" name="newProductSetName" value=""></td>
				</tr>
				<tr>
					<td align="left"><cfinclude template="oppProductSetProductSelect.cfm"></td>
				</tr>
				<tr>
					<td align="right"><input type="button" value="Create Product Set" name="cSubmit" onClick="doProductSubmit(document.mainProducts)"></td>
				</tr>
			</table>
			</form>
		</cfif>
	</cfcase>
	<cfcase value="Edit">
		<cfif isDefined("productSetID")>
			<cfif isDefined("newProductSetName")>
				<cfscript>
					application.com.oppProductSets.updateProductSet(productSetID,newProductSetName,productsSelected);
				</cfscript>
				<cfoutput>Product Set #htmleditformat(newProductSetName)# has been updated.</cfoutput>
				<br><br>
				<a href="JavaScript:window.close()">close</a>
				<SCRIPT type="text/javascript">
					opener.location.reload(false);
				</script>
			<cfelse>
				<cfscript>
					qrySelectedProducts=application.com.oppProductSets.getProductSetItems(productSetID,'');
					qryProductSetDetail=application.com.oppProductSets.get(ProductSetID);
				</cfscript>
				<cfif qryProductSetDetail.recordCount gt 0>
					<form name="mainProducts" action="" method="post">
					<cfoutput>
						<CF_INPUT type="hidden" name="psAction" value="#psAction#">
						<CF_INPUT type="hidden" name="productSetID" value="#productSetID#">
					</cfoutput>
						<table class="withBorder">
							<tr>
								<td align="left">Product Set Name : <cfoutput><CF_INPUT type="text" name="newProductSetName" value="#qryProductSetDetail.ProductSetName#"></cfoutput></td>
							</tr>
							<tr>
								<td align="left"><cfinclude template="oppProductSetProductSelect.cfm"></td>
							</tr>
							<tr>
								<td align="right"><input type="button" value="Update Product Set" name="eSubmit" onClick="doProductSubmit(document.mainProducts);"></td>
							</tr>
						</table>
					</form>
				<cfelse>
					Your Product Set does not appear to exist.<br>
					<a href="javascript:window.close()">close</a>
				</cfif>
			</cfif>
		<cfelse>
			You have not specified a productSetID.<br><br>
			<a href="JavaScript:window.close()">close</a>
		</cfif>
	</cfcase>
	<!--- DELETE A PRODUCT SET FROM THE SYSTEM. --->
	<cfcase value="Delete">
		<cfif not IsDefined("productSetID")>
			Cannot delete Product Set without a productSetID
			<CF_ABORT>
		<cfelse>
			<cfif isDefined("dSubmit")>
				<cfscript>
					application.com.oppProductSets.deleteProductSet(ProductSetID);
				</cfscript>
				Product Set Deleted.<br><br>
				<a href="JavaScript:window.close()">close</a>
				<SCRIPT type="text/javascript">
					opener.location.reload(false);
				</script>
			<cfelse>
				<cfscript>
				qryProductSetDetail=application.com.oppProductSets.get(productSetID);
				</cfscript>
				<cfoutput>
					<cfif qryProductSetDetail.recordcount gt 0>
						Are you sure you want to delete this product set:<br>
						#htmleditformat(qryProductSetDetail.ProductSetName)# (ProductSetID:#htmleditformat(productSetID)#)<br><br>

						<form name="doDelete" action="" method="post">
							<input type="submit" name="dSubmit" value="Delete">&nbsp;<input type="button" name="noDelete" value="Cancel" onClick="window.close();">
							<CF_INPUT type="hidden" name="ProductSetID" value="#ProductSetID#">
							<CF_INPUT type="hidden" name="psAction" value="#psAction#">
						</form>
					<cfelse>
						The selected product set no longer exists in the database.
					</cfif>
				</cfoutput>
			</cfif>
		</cfif>
	</cfcase>
	<cfcase value="Users">
		<cfif isDefined("nextAction")>
			<CF_UGRecordManagerTask>
			User rights for the Product Set have been updated.
		<cfelse>
			<cfoutput>
			<form name="users"  method="post">
				<CF_UGRecordManager entity="productSet" entityid="#productSetID#" Form="users">
				<input type="hidden" name="nextAction" value="true">
				<input type="button" name="uSubmit" value="Submit" onClick="form.submit();">
			</form>
			</cfoutput>
		</cfif>

	</cfcase>
	<cfcase value="Remove">
		<cfif not IsDefined("productSetID")>
			Cannot remove Product Set from your list without a productSetID.
			<CF_ABORT>
		<cfelse>
			<cfif isDefined("rSubmit")>
				<cfscript>
					application.com.oppProductSets.removeProductSetFromUser(ProductSetID);
				</cfscript>
				Product Set Removed from your list.<br><br>
				<a href="JavaScript:window.close()">close</a>
				<SCRIPT type="text/javascript">
					opener.location.reload(false);
				</script>
			<cfelse>
				<cfscript>
				qryProductSetDetail=application.com.oppProductSets.get(productSetID);
				</cfscript>
				<cfoutput>
					<cfif qryProductSetDetail.recordcount gt 0>
						Are you sure you want to remove this product set from your list:<br>
						#htmleditformat(qryProductSetDetail.ProductSetName)# (ProductSetID:#htmleditformat(productSetID)#)<br><br>

						<form name="doRemove" action="" method="post">
							<input type="submit" name="rSubmit" value="Remove">&nbsp;<input type="button" name="noRemove" value="Cancel" onClick="window.close();">
							<CF_INPUT type="hidden" name="ProductSetID" value="#ProductSetID#">
							<CF_INPUT type="hidden" name="psAction" value="#psAction#">
						</form>
					<cfelse>
						The selected product set no longer exists in the database.
					</cfif>
				</cfoutput>
			</cfif>
		</cfif>
	</cfcase>
	<cfcase value="">
		No action specified.
	</cfcase>
</cfswitch>
		</td>
	</tr>
</table>



</cf_translate>


