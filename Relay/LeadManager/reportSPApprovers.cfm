<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
2012-10-04	WAB		Case 430963 Remove Excel Header 
 --->

<cfinclude template="../templates/tableFromQueryHeaderInclude.cfm">
<cfparam name="openAsExcel" type="boolean" default="false">

	<cf_head>
		<cf_title>SPQ Approver Report</cf_title>
	</cf_head>

<cfparam name="sortOrder" default="countrydescription,approverlevel">
<cfparam name="numRowsPerPage" default="50">
<cfparam name="startRow" default="1">

<cfparam name="checkBoxFilterName" type="string" default="">
<cfparam name="checkBoxFilterLabel" type="string" default="">

<cfparam name="keyColumnURLList" type="string" default="">

<cfquery name="getSPQApproverIDs" datasource="#application.SiteDataSource#">
	select b.entityid 
	from booleanflagdata b
	join flag f on f.flagid = b.flagid
	where f.flagtextid like 'SPApprover'
	and entityid = #request.relayCurrentUser.personid#
</cfquery>

<cfif getSPQApproverIDs.recordcount eq 1>
	<cfset keyColumnURLList = "/templates/editXMLGeneric.cfm?editor=SPApprovers&oppSPApproverID=">
</cfif>

<cfquery name="getSPQApprovers" datasource="#application.SiteDataSource#">
	select Approver, ApproverLevel, Countrydescription, personid, oppSPapproverID, ProductApproverLevel 
from voppSPApprover		
	where 1=1
	
	<cfinclude template="../templates/tableFromQuery-QueryInclude.cfm">
	order by <cf_queryObjectName value="#sortOrder#"> 
</cfquery>

<cfoutput>
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
	<TR class="Submenu"> 
		<TD ALIGN="right" VALIGN="TOP" class="Submenu">
			<a href="javascript:history.back()" class="Submenu">Back</a> |  
			<a href="../templates/editXMLGeneric.cfm?add=Yes&Editor=SPApprovers" class="Submenu">Add New Approver&nbsp; </a>
		</TD>
	</TR>

	<tr>
		<td><h2>Special Pricing Approvers</h2></td>
	</tr>
	
	<tr>
		<td><p>#request.CurrentSite.Title# users who have user rights to approve Special Pricing.  
		To change an approver click their name below.  &nbsp;
		To add a new approver for a country choose Add New Approver from the menu above.<br>
		</p></td>
	</tr>
</table>
</cfoutput>

<cf_translate>
<CF_tableFromQueryObject 
	queryObject="#getSPQApprovers#"
	queryName="getSPQApprovers"
	sortOrder = "#sortOrder#"
	numRowsPerPage="#numRowsPerPage#"
	startRow="#startRow#"
	
	keyColumnList="Approver"
	keyColumnURLList="#keyColumnURLList#"
	keyColumnKeyList="oppSPApproverID"
	
	hideTheseColumns="oppSPApproverID"
	showTheseColumns="Approver,ApproverLevel,ProductApproverLevel,Countrydescription,PersonID"
	dateFormat=""
	columnTranslation="true"
	
	FilterSelectFieldList="ApproverLevel,Countrydescription"
	FilterSelectFieldList2="ApproverLevel,Countrydescription"
	totalTheseColumns=""
	GroupByColumns=""
	radioFilterLabel=""
	radioFilterDefault=""
	radioFilterName=""
	radioFilterValues=""
	checkBoxFilterName=""
	checkBoxFilterLabel=""
	allowColumnSorting="yes"
	currencyFormat=""
>
</cf_translate>



