<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			reportLeadCapture.cfm	
Author:				SS
Date started:		2007/05/22
	
Description:		

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2012-10-04	WAB		Case 430963 Remove Excel Header 

Possible enhancements:


 --->
<cfparam name="openAsExcel" type="boolean" default="false">


<cf_head>
	<cf_title>Lead Capture Report</cf_title>
</cf_head>



<cf_translate>

<cfparam name="countryGroupTypeID" type="numeric" default="6">
<cfparam name="numRowsPerPage" default="99999">
<cfparam name="sortOrder" default="leadID desc">



<cfset useDateRange ="true">
<cfset dateField="created">
<cfset dateRangeFieldName="created">
<cfset tableName="Lead">
<cfparam name="useFullRange" type="boolean" default="false">
<cfinclude template="/relay/templates/DateQueryWhereClause.cfm">


<cfscript>
// create a structure containing the fields below which we want to be reported when the form is filtered
passThruVars = StructNew();
StructInsert(passthruVars, "useFullRange", useFullRange);
</cfscript>
<cfinclude template="../templates/tableFromQueryHeaderInclude.cfm">

<cfquery name="getLeads" datasource="#application.siteDataSource#">
	select * from (
	select cg.countrygrouptypeid,cg.countryDescription as countryGroup,c.countrydescription as country, c.IsoCode as country_isocode, l.leadID, l.salutation,
	l.firstname, l.lastname, l.Email, l.telephone as phone, l.company as companyname, l.address1 as company_address1, 
	l.postalcode as zipcode, address4 as city, l.address5 as region, l.custom3 as company_size,l. custom2 as system, l.created 
	from lead l inner join country c on c.countryid = l.countryid inner join 
	countrygroup cm on c.countryid = cm.countryMemberID inner join 
	country cg on cg.countryID = cm.countryGroupID
	) base
	where countrygrouptypeid = 6
	<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
	order by <cf_queryObjectName value="#sortOrder#">
</cfquery>
<cfif not openAsExcel>
	<CF_RelayNavMenu pageTitle="" thisDir="">
		<CF_RelayNavMenuItem MenuItemText="Excel" CFTemplate="#cgi["SCRIPT_NAME"]#?#queryString#&openAsExcel=true" target="blank">
	</CF_RelayNavMenu>
</cfif>

	<CF_tableFromQueryObject queryObject="#getLeads#" 
		FilterSelectFieldList = "CountryGroup,Country"
		showTheseColumns="leadID,salutation,firstname,lastname,Email,phone,companyname,company_address1,zipcode,city,region,country_isocode,company_size,system,created"
		sortOrder="#sortOrder#"
		allowColumnSorting="yes"
		queryWhereClauseStructure="#queryWhereClause#"
		passThroughVariablesStructure="#passThruVars#"
		numberFormat=""
		dateformat="created"
		numRowsPerPage="#numRowsPerPage#"
		openAsExcel="#openAsExcel#"
	>
</cf_translate>
<cfif not openAsExcel>


</cfif>



