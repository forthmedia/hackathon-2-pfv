<!--- �Relayware. All Rights Reserved 2014 --->
<!---
WAB 2009/06/24 removed, RACE Security, now done automatically
2011-12-12 NYB LHID8175 changed leadOppPartnerColumnHeadingList & leadOppPartnerShowTheseColumns
2012-05-31 PPB P-MIC001 if there is a message sent in (eg after user applied for deal reg in oppEdit) then show it once we've displayed the list
2014-07-17	MS	Case. 440329 - Show heading on listing screen but not on Opp Edit Form
2015/09/30	NJH		PROD2015-35 - added param for column filters and added text search
2015/10/27	RJT	PROD2015-248 - Now that the filters can be used on the TFQO it makes sense to continue displaying the TFWQO even it it currently displays nothing
 --->


<!--- PPB uncommented the inclusion of opportunityINI to allow access to leadOppPartnerShowTheseColumns etc --->
<cf_include template="/code/cftemplates/opportunityINI.cfm" checkIfExists="true">

<!--- START 2013-07-12 MS P-KAS015 CR003 Japan Changes - Item No 4 Japanese Values should not show decimals just the separators --->
<cfif fileexists("#application.paths.code#\cftemplates\customLeadAndOppListPartner.cfm")>
	<cfinclude template="/code/cftemplates/customLeadAndOppListPartner.cfm">
<cfelse>

	<!---used to set the ADD NEW reocrd for Opportunity--->
	<cfparam name="showAdd" default="true">

	<!--- <cfparam name="url.startRow" default="1"> --->
	<cfparam name="sortOrder" default="account">
	<cfparam name="numRowsPerPage" default="50">
	<cfparam name="filterBy" default="">
	<!--- The keyColumn list group of params below control which columns show URL links
			where they link to and what key should be used in the URL --->
	<cfparam name="keyColumnList" type="string" default="detail"><!--- this can contain a list of columns that can be edited --->
	<!--- 2012/11/20	YMA		CASE: 432143 Clear down message once on opportunity list so it doesnt display again when entering next opportunity. --->

	<cfset structDelete(url,"formState")>
	<cfparam name="keyColumnURLList" type="string" default="#cgi.SCRIPT_NAME#?#application.com.structureFunctions.convertStructureToNameValuePairs(struct = URL,delimiter='&',encode='URL')#&frmTask=edit&opportunityid="><!--- this can contain a list of matching columns that contain the URL of the editor template --->
	<cfparam name="keyColumnKeyList" type="string" default="opportunity_ID"><!--- this can contain a list of matching key columns e.g. primary key --->
	<cfparam name="keyColumnOpenInWindowList" type="string" default="no">
	<cfparam name="OpenWinSettingsList" type="string" default="no">
	<cfparam name="dateFormat" type="string" default="last_Updated,Expected_Close_Date"><!--- this list should contain those fields you want in the date format dd-mmm-yy --->
	<cfparam name="entityID" type="numeric" default="0">
	<cfparam name="partnerLocationID" type="numeric" default="0">

	<!--- 2011-12-12 NYB LHID8175 changed leadOppPartnerColumnHeadingList & leadOppPartnerShowTheseColumns --->
	<cfparam name="leadOppPartnerColumnHeadingList" type="string" default="Description,Last_Updated,Stage">
	<cfparam name="leadOppPartnerShowTheseColumns" type="string" default="DETAIL,LAST_UPDATED,StagePhraseTextID">
	<!--- 2012-06-19 RMB P-LEX070 removed opportunity_id from hidden columns --->
	<cfparam name="leadOppPartnerHideTheseColumns" type="string" default="entityID,vendorAccountManagerPersonID,partnerSalesPersonID,stageID">	<!--- PPB 29/06/10 these were hidden in the original so have left them tho not needed for My Opportunities list --->
	<cfparam name="columnTranslation" default="true">
	<cfparam name="columnTranslationPrefix" default="phr_opp_">
	<cfparam name="filterSelectFieldList" default="stage">
	<cfparam name="dateFilter" default="Expected_Close_Date">

	<!--- 2012/07/18 NAS P-KAS002 added leadEditTemplate so a custom opp edit screen can be declared in the ini file --->
	<cfparam name="leadEditTemplate" default="/leadManager/opportunityEdit.cfm">

	<cfparam name="createOrder" default="false">
	<cfparam name="pricingSupport" default="false">
	<cfparam name="showRebate" default="false">
	<cfparam name="showBudget" default="false">
	<cfparam name="showCurrentMonth" default="false">
	<cfparam name="showAllPartnerLeads" default="false">
	<cfparam name="filterType" default="">						<!--- "pipeline", "forecast" or "" --->
	<cfparam name="filterToForecast" default="false">

	<cfif partnerLocationID eq "0">
		<!--- <cfquery name="getThisPartnerdetails" datasource="#application.sitedatasource#">
			select personID,organisationID,locationID from person
			where personID = #request.relayCurrentUser.personid#
		</cfquery>
		<cfset partnerLocationID = getThisPartnerdetails.locationID> --->
		<cfset partnerLocationID = request.relayCurrentUser.locationID>
		<cfif showAllPartnerLeads>
			<cfset partnerSalesPersonID = 0>
		<cfelse>
			<cfset partnerSalesPersonID = request.relayCurrentUser.personID>
		</cfif>
	</cfif>

	<CFIF isDefined("frmTask") and frmTask eq "edit">
		<!--- <cfinclude template="/leadManager/opportunityEdit.cfm"> --->
		<!--- 2012/07/18 NAS P-KAS002 added leadEditTemplate so a custom opp edit screen can be declared in the ini file --->
		<cfinclude template="#leadEditTemplate#">

	<cfelse>
		<!--- <cfquery name="test" datasource="" --->
		<cfscript>
		// create an instance of the opportunity component
		// call the opportunity component either directly or via a stub in userfiles/content/
	if (fileexists(application.paths.code & "\cftemplates\relayOpportunity.cfc")) {
		componentPath = "code.CFTemplates.relayOpportunity";
			myopportunity = createObject("component",componentPath);}
		else {myopportunity = createObject("component","relay.com.opportunity");}
			myopportunity.dataSource = application.siteDataSource;
			myopportunity.entityID = entityID;
			myopportunity.opportunityView = application.com.settings.getSetting("leadManager.opportunityView");
			myopportunity.partnerLocationID = partnerLocationID;
			myopportunity.partnerSalesPersonID = partnerSalesPersonID;
			// if form.sortorder is defined use it otherwise set it a default value
			if (structKeyExists(form,"sortOrder") and form.sortOrder neq "") {
				myopportunity.sortOrder = form.sortOrder;}
			// 2012-06-26 - RMB - P-LEX070 - Changed from hard coded "account" to pick up variable sortOrder
			else if	(structKeyExists(url,"sortOrder") and url.sortOrder neq "") {
			myopportunity.sortOrder = url.sortOrder;}
			else {myopportunity.sortOrder = sortOrder;}
			sortOrder=myopportunity.sortOrder;
			if (isdefined("showOppTypes")) {
				myopportunity.showOppTypes = showOppTypes;
			}
			myopportunity.listPartnerOpportunities(showCurrentMonth=showCurrentMonth, filterType=filterType, filterToForecast=filterToForecast);
		</cfscript>

		<cfset currencyFormatList = "CALCULATED_BUDGET,Overall_Customer_Budget">
		<cfset totalTheseColumnsList = "">

		<cfif createOrder>
			<cfset leadOppPartnerShowTheseColumns = #leadOppPartnerShowTheseColumns# & ",CREATE_ORDER">
			<cfset keyColumnList = #keyColumnList# & ",CREATE_ORDER">
			<cfset keyColumnURLList = #keyColumnURLList# & ",/orders/addOrderFromOpp.cfm?opportunityID=">
			<cfset keyColumnKeyList = #keyColumnKeyList# & ",opportunity_ID">
			<cfset keyColumnOpenInWindowList = #keyColumnOpenInWindowList# & ",yes">
			<cfset OpenWinSettingsList = #OpenWinSettingsList# & ";width=400,height=150,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=0,resizable=1">
		</cfif>

		<cfif pricingSupport>
			<cfset leadOppPartnerShowTheseColumns = #leadOppPartnerShowTheseColumns# & ",PRICING_SUPPORT">
			<cfset keyColumnList = #keyColumnList# & ",PRICING_SUPPORT">
			<cfset keyColumnURLList = #keyColumnURLList# & ",/relayTagTemplates/opportunityPricingSupport.cfm?opportunityID=">
			<cfset keyColumnKeyList = #keyColumnKeyList# & ",opportunity_ID">
			<cfset keyColumnOpenInWindowList = #keyColumnOpenInWindowList# & ",yes">
			<cfset OpenWinSettingsList = #OpenWinSettingsList# & ";width=800,height=500,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1">
		</cfif>

		<cfif showBudget>
			<cfset leadOppPartnerShowTheseColumns = #leadOppPartnerShowTheseColumns# & ",CALCULATED_BUDGET">
			<cfset totalTheseColumnsList = #totalTheseColumnsList# & ",calculated_budget">
		</cfif>

		<cfif showRebate>
			<cfset leadOppPartnerShowTheseColumns = #leadOppPartnerShowTheseColumns# & ",REBATE">
			<cfset currencyFormatList = #currencyFormatList# & ",rebate">
			<cfset totalTheseColumnsList = #totalTheseColumnsList# & ",rebate">
		</cfif>

	<!---
	Added by Gawain

	2007-08-21 : commented by gad, not working for the required results

		<cfif showAdd>
			<cfform method="post" name="FormY"  action="/leadManager/opportunityEdit.cfm">
				<cfset request.relayFormDisplayStyle ="HTML-Form" >
				<cf_relayFormDisplay>
					<CF_relayFormElement  relayFormElementType="button" fieldname="add_new" currentValue="phr_Ext_Add_new" label="" spanCols="No" valueAlign="left" class="button" onclick="document.FormY.submit()"   />

				</cf_relayformdisplay>
			</cfform>
		</cfif>
	--->

		<!--- START: 2014-07-17	MS	Case. 440329 - Show heading on listing screen but not on Opp Edit Form --->
		<cfif isDefined('showHeadingOnListingScreen') and showHeadingOnListingScreen and isDefined('listingScreenHeadingPhraseToUse') and listingScreenHeadingPhraseToUse neq ''>
			<cfoutput>#listingScreenHeadingPhraseToUse#</cfoutput>
		</cfif>
		<!--- END  : 2014-07-17	MS	Case. 440329 - Show heading on listing screen but not on Opp Edit Form --->


		<cfif myopportunity.qListPartnerOpportunities.recordcount eq 0 >
			<cfif isDefined("showOppTypes") and showOppTypes eq "Renewal">
				phr_opp_NoRenewalsToList
			<cfelse>
				phr_opp_NoOpportunitiesToList
			</cfif>
		</cfif>


		<cfloop list="#filterSelectFieldList#" index="filter">
			<cfif not listFindNoCase(leadOppPartnerShowTheseColumns,filter)>
				<cfset filterSelectFieldList = listDeleteAt(filterSelectFieldList,listFindNoCase(filterSelectFieldList,filter))>
			</cfif>
		</cfloop>

		<CF_tableFromQueryObject
			queryObject="#myopportunity.qListPartnerOpportunities#"
			sortOrder = "#sortOrder#"
			numRowsPerPage="#numRowsPerPage#"

			useInclude = "false"

			keyColumnList="#keyColumnList#"
			keyColumnURLList="#keyColumnURLList#"
			keyColumnKeyList="#keyColumnKeyList#"
			keyColumnOpenInWindowList="#keyColumnOpenInWindowList#"
			OpenWinSettingsList="#OpenWinSettingsList#"

			<!--- ColumnHeadingList="#leadOppPartnerColumnHeadingList#" --->
			showTheseColumns="#leadOppPartnerShowTheseColumns#"
			hideTheseColumns="#leadOppPartnerHideTheseColumns#"
			dateFormat="#dateFormat#"
			allowColumnSorting="yes"
			CurrencyISOColumn="currency,currency"
			currencyFormat="#currencyFormatList#"

			columnTranslation="#columnTranslation#"
			columnTranslationPrefix="#columnTranslationPrefix#"
			FilterSelectFieldList="#filterSelectFieldList#"
			FilterSelectFieldList2="#listLen(filterSelectFieldList) gt 1?filterSelectFieldList:''#"
			showTextSearch="true"
			dateFilter="#listFindNoCase(leadOppPartnerShowTheseColumns,dateFilter)?dateFilter:''#"

			totalTheseColumns="#totalTheseColumnsList#"
			>


	</CFIF>

	<!--- 2012-05-31 PPB P-MIC001 if there is a message sent in (eg after user applied for deal reg in oppEdit) then show it once we've displayed the list--->
	<!--- <cfif StructKeyExists(URL,"message") and message neq "">
		<cfoutput>
			<script type="text/javascript">
				alert('#jsStringFormat(message)#');
			</script>
		</cfoutput>
	</cfif> --->
</cfif>
<!--- END 2013-07-12 MS P-KAS015 CR003 --->