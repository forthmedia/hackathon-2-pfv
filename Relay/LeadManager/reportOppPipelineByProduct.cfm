<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			reportOppPipelineByProduct.cfm
Author:				Unknown
Date started:		Unknown

Description:		This is used to provide a page for listing people records

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2006-05-12			AJC			Add Sales Manager to the report
2009-10-01			NJH			LID 2697
2010-07-30			NJH			P-PAN002 - use rollup reporting
2011-06-01 			PPB 		REL106 use countryScopeOpportunityRecords setting
2012-10-04	WAB		Case 430963 Remove Excel Header

Possible enhancements:


 --->
<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">
<cfparam name="openAsExcel" type="boolean" default="false">

<!--- 2012-07-23 PPB P-SMA001 commented out
<cfif application.com.settings.getSetting("leadManager.countryScopeOpportunityRecords")>	<!--- 2011-06-01 PPB REL106 use countryScopeOpportunityRecords setting --->
	<cfinclude template="/templates/qryGetCountries.cfm">
</cfif>
--->

<cfparam name="startrow" default="1">
<cfparam name="sortOrder" default="OPPORTUNITY_STATUS">
<cfparam name="numRowsPerPage" default="50">
<!--- The keyColumn list group of params below control which columns show URL links
		where they link to and what key should be used in the URL --->
<cfparam name="keyColumnList" type="string" default="detail,account"><!--- this can contain a list of columns that can be edited --->
<cfparam name="keyColumnURLList" type="string" default="opportunityEdit.cfm?opportunityid=,../data/dataframe.cfm?frmsrchOrgID="><!--- this can contain a list of matching columns that contain the URL of the editor template --->
<cfparam name="keyColumnKeyList" type="string" default="opportunityID,entityID"><!--- this can contain a list of matching key columns e.g. primary key --->

<cfparam name="radioFilterLabel" type="string" default=""><!--- this can contain a list of columns that can be edited --->
<cfparam name="radioFilterDefault" type="string" default=""><!--- this can contain a list of matching columns that contain the URL of the editor template --->
<cfparam name="radioFilterName" type="string" default=""><!--- this can contain a list of matching key columns e.g. primary key --->
<cfparam name="radioFilterValues" type="string" default=""><!--- this should contain a list of values for each radio button delimited by a minus sign --->

<cfparam name="dateFormat" type="string" default=""><!--- this list should contain those fields you want in the date format dd-mmm-yy --->
<cfparam name="entityID" type="numeric" default="0">
<cfparam name="frm_vendoraccountmanagerpersonid" type="numeric" default="0">
<cfparam name="productFilterSelectFieldList" type="string" default="Country,Account_Manager,SALES_MANAGER,Opportunity_Status">
<cfparam name="checkBoxFilterName" type="string" default="">
<cfparam name="checkBoxFilterLabel" type="string" default="Phr_Sys_Opp_ShowComplete">
<cfparam name="form.showcomplete" default="0">
<cfparam name="alphabeticalIndexColumn" type="string" default="Account">
<cfparam name="pipelineProductOppStageIDs" type="string" default="1,2,3,4,5">

<cfscript>
// create a structure containing the fields below which we want to be reported when the form is filtered
passThruVars = StructNew();
StructInsert(passthruVars, "showComplete", form.showComplete);
</cfscript>

<cfset phrasePrefix="">
<cfset tableName="opportunity">
<cfset dateField="expectedCloseDate">
<cfset dateRangeFieldName = "expected_close_date">
<cfinclude template="/relay/templates/DateQueryWhereClause.cfm">
<cfset form.dateFilter = "expected_close_date">

<cfif not structKeyExists(form,"frmFromDate")>
	<cfset form.frmToDate = request.requestTimeODBC>
	<cfset form.frmFromDate=dateAdd("d",-1,request.requestTime)>
</cfif>

<CFQUERY NAME="oppFlags" DATASOURCE="#application.SiteDataSource#">
	select * from flagGroup where entityTypeID =  <cf_queryparam value="#application.entityTypeID["opportunity"]#" CFSQLTYPE="CF_SQL_INTEGER" >  and parentflaggroupid = 0
</CFQUERY>


<cfset flagjoins = application.com.flag.getJoinInfoForListOfFlagGroups(valueList(oppFlags.flagGroupID))>

<!--- NJH 2009-10-01 LID 2697 - added o. in front of some of the column names as some columns were ambiguous. Also added countryID --->
<cfquery name="getProductReport" datasource="#application.SiteDataSource#">
select distinct o.opportunityID,
	<cfloop index = i from = "1" to = "#arrayLen(flagjoins)#">
		#flagjoins[i].selectField# as  #flagjoins[i].alias# ,
	</cfloop>
 Account, ACCOUNT_MANAGER, SALES_MANAGER, o.Detail, COUNTRY, o.countryID,
OPPORTUNITY_STATUS, sum(subtotal) as Product_Value, o.entityID, opp_StatusID,
LEFT( UPPER( #alphabeticalIndexColumn# ), 1 ) as alphabeticalIndex, LAST_UPDATED, CURRENCY
from vOppProductsPipeline as o
<cfloop index = i from = "1" to = "#arrayLen(flagjoins)#">
	#flagjoins[i].join#
</cfloop>
Where 1=1

	and opp_StatusID  in ( <cf_queryparam value="#pipelineProductOppStageIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
<!--- 2012-07-23 PPB P-SMA001 commented out
	<cfif application.com.settings.getSetting("leadManager.countryScopeOpportunityRecords")>
	and o.countryid  in ( <cf_queryparam value="#countryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	</cfif>
 --->
	#application.com.rights.getRightsFilterWhereClause(entityType="opportunity",alias="o").whereClause#		<!--- 2012-07-23 PPB P-SMA001 note: countryScopeOpportunityRecords setting checked inside getRightsFilterWhereClause() --->

	 <cfif isDefined("frm_vendoraccountmanagerpersonid") and frm_vendoraccountmanagerpersonid neq "0">
		and ACCOUNT_MANAGER = (select per.FirstName + ' ' + per.LastName from person per where personid =  <cf_queryparam value="#frm_vendoraccountmanagerpersonid#" CFSQLTYPE="CF_SQL_INTEGER" > )
	</cfif>

	<cfinclude template="../templates/tableFromQuery-QueryInclude.cfm">
	group by  o.opportunityid, Account, o.detail, ACCOUNT_MANAGER, SALES_MANAGER, COUNTRY, o.countryID, OPPORTUNITY_STATUS, o.ENTITYID, opp_StatusID, LAST_UPDATED, CURRENCY
	<!--- WAB 2009-10-02 LID 2697 added these flags to the group statement - since otherwise did not work --->
	<cfloop index = i from = "1" to = "#arrayLen(flagjoins)#">
		, #flagjoins[i].selectField#
	</cfloop>
	order by <cf_queryObjectName value="#sortOrder#">

</cfquery>

<!--- get all the groups of eventTeam flags --->

		<!--- get names of the flags/groups for doing column names --->
		<cfset flagcolumnNameList = "">
		<cfset flagcolumnAliasList = "">
		<cfloop index = i from = "1" to = "#arrayLen(flagjoins)#">
			<cfset flagcolumnNameList = listappend(flagcolumnNameList,flagjoins[i].name)>
			<cfset flagcolumnAliasList = listappend(flagcolumnAliasList,flagjoins[i].alias)>
		</cfloop>


<cfset application.com.request.setDocumentH1(text="Revenue Pipeline Report")>
<cfinclude template="opportunityListScreenFunctions.cfm">

<cfparam name="IllustrativePriceColumns" default="">
<cfif structkeyexists(request,"UseIllustrativePrice")>
	<cfset IllustrativePriceColumns = "PRODUCT_VALUE">
</cfif>

<CF_tableFromQueryObject
	openAsExcel="#openAsExcel#"
	queryObject="#getProductReport#"
	queryName="getProductReport"
	sortOrder = "#sortOrder#"
	numRowsPerPage="#numRowsPerPage#"
	startrow="#startrow#"
	dateFilter="expected_close_date"
	passThroughVariablesStructure="#passThruVars#"

	keyColumnList="#keyColumnList#"
	keyColumnURLList="#keyColumnURLList#"
	keyColumnKeyList="#keyColumnKeyList#"

	hideTheseColumns="opportunityID,ENTITYID,opp_StatusID"
	showTheseColumns="ACCOUNT,ACCOUNT_MANAGER,SALES_MANAGER,DETAIL,COUNTRY,OPPORTUNITY_STATUS,PRODUCT_VALUE,#flagcolumnAliasList#"
	ColumnHeadingList="ACCOUNT,ACCOUNT MANAGER,SALES MANAGER,DETAIL,COUNTRY,OPPORTUNITY STATUS,PRODUCT VALUE,#flagcolumnNameList#"
	dateFormat=""
	<!--- columnTranslation="#translateReportColumns#" --->

	FilterSelectFieldList="#productFilterSelectFieldList#"
	FilterSelectFieldList2="#productFilterSelectFieldList#"

	<!--- IllustrativePriceFromCurrencyColumn="CURRENCY"
	IllustrativePriceToCurrency="#IllustrativePriceToCurrency#"
	IllustrativePriceDateColumn="LAST_UPDATED"
	IllustrativePriceColumns="#IllustrativePriceColumns#" --->

	rollUpCurrency="true"
	GroupByColumns="OPPORTUNITY_STATUS"
	totalTheseColumns="Product_Value"

	radioFilterLabel="#radioFilterLabel#"
	radioFilterDefault="#radioFilterDefault#"
	radioFilterName="#radioFilterName#"
	radioFilterValues="#radioFilterValues#"
	checkBoxFilterName="#checkBoxFilterName#"
	checkBoxFilterLabel="#checkBoxFilterLabel#"
	allowColumnSorting="no"
	currencyFormat="Product_Value"
	queryWhereClauseStructure="#queryWhereClause#"
>