<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		CustomerRenewals.cfm
Author:			PPB
Date started:	14-05-2010

Description:	Tab for customer Renewal opportunities

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2010/11/04			NAS			LID3927: Template Unavailable Error - Cancel button on Renewal
2014-01-31	WAB 	removed references to et cfm, can always just use / or /?
2016/12/12	NJH JIRA PROD2016-2937 - remove the reseller OrgID parameter. Now done in rights.

Possible enhancements:


 --->

	<cfif not application.com.security.confirmFieldsHaveBeenEncrypted(fieldNames="orgid")>
	      <CF_ABORT>
	</cfif>

	<cfparam name="inIframe" default=0>			<!--- if called from a CustomerController Tab will be 1 --->

	<cf_translate>

		<cfif inIFrame eq 1>
			<cfset returnURL = "#cgi.http_referer#">
		<cfelse>
			<cfset returnURL = "#cgi.script_name#?#request.query_string#">
		</cfif>

		<cfoutput>
		<script>
			function editOpp (params) {
//				parent.location.href='/?eid=oppEdit&returnURL=#urlencodedformat(application.com.security.encryptURL(returnURL))#&' + params
				parent.location.href='/?eid=oppEdit&' + params

			}
		</script>
		</cfoutput>

		<cfif application.com.settings.getSetting("leadManager.products.useOppProductTotals")>
			<cfset columnHeadings = "Description,Value,ExpectedCloseDate,blank">
			<cfset showColumns = "opportunityDescription,TotalValue,ExpectedCloseDate,EditButton">
		<cfelse>
			<cfset columnHeadings = "Description,ExpectedCloseDate,blank">
			<cfset showColumns = "opportunityDescription,ExpectedCloseDate,EditButton">
		</cfif>


		<cfparam name="startRow" default="1">
		<cfparam name="numRowsPerPage" default="100">
		<cfparam name="sortOrder" default="o.opportunityID">


		<cfset qryOpportunities = application.com.opportunity.getCustomerControllerOpportunities(organisationID=session.customerOrgID,opportunityTypes='RENEWAL',sortOrder=sortOrder)>

		<cfif qryOpportunities.recordCount gt 0>
			<CF_tableFromQueryObject
				queryObject="#qryOpportunities#"
				sortOrder = "#sortOrder#"
				startRow = "#startRow#"
				openAsExcel = "false"
				numRowsPerPage="#numRowsPerPage#"
				totalTheseColumns=""
				GroupByColumns=""
				ColumnTranslation="true"
			    ColumnTranslationPrefix="phr_"
				ColumnHeadingList="#columnHeadings#"
				showTheseColumns="#showColumns#"
				FilterSelectFieldList=""
				FilterSelectFieldList2=""
				hideTheseColumns=""
				numberFormat=""
				dateFormat="ExpectedCloseDate"
				showCellColumnHeadings="no"
				keyColumnURLList=" "
				keyColumnKeyList="opportunityID"
				keyColumnTargetList=""
				<!--- 2010/11/04			NAS			LID3927: Template Unavailable Error - Cancel button on Renewal --->
				keyColumnOnClickList="editOpp('##application.com.security.encryptQueryString('returnURL=#urlencodedformat(returnURL)#&opportunityid=##opportunityid##')##')"
				keyColumnList="EditButton"
				keyColumnOpenInWindowList="no"
				OpenWinSettingsList=""
				useInclude = "false"
				allowColumnSorting = "true"
				CurrencyISOColumn="currency"
				currencyFormat="TotalValue"
				KeyColumnLinkDisplayList="button"
			>
		<cfelse>
			phr_opp_NoRenewalsToList
		</cfif>

	</cf_translate>

