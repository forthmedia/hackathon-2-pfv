<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		LeadManager.cfm/SPQMyApprovals.cfm
Author:
Date created:

Objective - Display list of Approvals accessible to current user

Syntax	  -

Parameters -

Return Codes - none

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2004-12-20			JC			Additional 'Approvers' array created to extend
								existing getSPQOpps query
2004-12-23			JC			Mod to getSPQOpps to properly support rules for display to Approvers
2010-02-12			NJH			LID 3029 - sort the query based on the sortOrder variable.. was hardcoded to be account.
2011-06-01 			PPB 		P-REL106 use countryScopeOpportunityRecords setting
2012-10-04	WAB		Case 430963 Remove Excel Header

--->

<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">
<cfparam name="openAsExcel" type="boolean" default="false">

<CFQUERY name="GetApprovers" datasource="#application.sitedatasource#">
	select 	approverlevel, (approverlevel - 1) as ApproverMinus1,
	productapproverlevel,(productapproverlevel - 1) as productApproverMinus1,
	countryID
	from 	oppSPApprover
	where 	personid =  #request.relaycurrentuser.personid#
</CFQUERY>

<cfif GetApprovers.recordcount GT 0>
	<cfset Approvers = ArrayNew(2)>
	<cfoutput query="GetApprovers">
		<cfset Approvers[currentrow][1] = countryid>
		<cfset Approvers[currentrow][2] = ApproverMinus1><!--- approverlevel --->
		<cfset Approvers[currentrow][3] = productApproverMinus1><!--- productApproverlevel --->
	</cfoutput>
</cfif>

<!--- 2012-07-23 PPB P-SMA001 commented out
<cfif application.com.settings.getSetting("leadManager.countryScopeOpportunityRecords")>	<!--- 2011-06-01 PPB REL106 use countryScopeOpportunityRecords setting --->
	<cfinclude template="/templates/qryGetCountries.cfm">
</cfif>
--->

<cfparam name="sortOrder" default="account">
<cfparam name="numRowsPerPage" default="50">
<cfparam name="startRow" default="1">
<!--- The keyColumn list group of params below control which columns show URL links
		where they link to and what key should be used in the URL --->
<cfparam name="keyColumnList" type="string" default="detail,account,PRICING_STATUS,SPApprovalNumber"><!--- this can contain a list of columns that can be edited --->
<cfparam name="keyColumnURLList" type="string" default="opportunityEdit.cfm?opportunityid=,../data/dataframe.cfm?frmsrchOrgID=,specialPriceEdit.cfm?entityID=,specialPricePrint.cfm?openAsWord=true&entityID="><!--- this can contain a list of matching columns that contain the URL of the editor template --->
<cfparam name="keyColumnKeyList" type="string" default="opportunity_ID,entityID,opportunity_ID,opportunity_ID"><!--- this can contain a list of matching key columns e.g. primary key --->

<cfparam name="radioFilterLabel" type="string" default=""><!--- this can contain a list of columns that can be edited --->
<cfparam name="radioFilterDefault" type="string" default=""><!--- this can contain a list of matching columns that contain the URL of the editor template --->
<cfparam name="radioFilterName" type="string" default=""><!--- this can contain a list of matching key columns e.g. primary key --->
<cfparam name="radioFilterValues" type="string" default=""><!--- this should contain a list of values for each radio button delimited by a minus sign --->

<cfparam name="dateFormat" type="string" default="last_Updated,Expected_Close_Date"><!--- this list should contain those fields you want in the date format dd-mmm-yy --->
<cfparam name="entityID" type="numeric" default="0">
<cfparam name="frm_vendoraccountmanagerpersonid" type="numeric" default="0">

<cfparam name="checkBoxFilterName" type="string" default="showSPComplete">
<cfparam name="checkBoxFilterLabel" type="string" default="Phr_Sys_Opp_ShowSPQComplete">

<cfparam name="alphabeticalIndexColumn" type="string" default="Account">

<cfparam name="SPQReport" type="string" default="yes">

<cfparam name="SPQleadAndOppListFilterSelectColumnList" type="string" default="Pricing_Status,Account,Month_Expected_Close,Quarter_Expected_Close,Status,Country,Account_Manager">
<cfparam name="SPQleadAndOppListShowColumns" type="string" default="ACCOUNT,DETAIL,PRICING_STATUS,ACCOUNT_MANAGER,COUNTRY,CALCULATED_BUDGET,SPApprovalNumber">

<cfparam name="SPQsearchColumnList" type="string" default="opportunity_id,account,product">
<cfparam name="SPQsearchColumnDisplayList" type="string" default="Phr_opportunityID,Account,Product">

<cfquery name="qGetSPQOpps" datasource="#application.SiteDataSource#">
		SELECT 	opportunity_ID, Account, Stage,detail,expectedCloseDate as expected_Close_Date,
				calculated_budget, overall_customer_budget, unapproved_special_price,
				last_Updated, Account_Manager, status, Month_expected_close,
				Quarter_expected_close, probability, Partner_Name, entityid,
				vendorAccountManagerPersonID, country, region, SPRequiredApprovalLevel,
				SPCurrentApprovalLevel, SPApprovalNumber, pricing_status,
		 		oppPricingStatusID, distributor, SPApproverPersonIDs,SPExpiryDate,
				LEFT( UPPER( Account ), 1 ) as alphabeticalIndex,
				countryID,SPQ_Last_Updated
		FROM 	vLeadListingCalculatedBudget
		WHERE 	1=1
		#application.com.rights.getRightsFilterWhereClause(entityType="opportunity",alias="vLeadListingCalculatedBudget").whereClause#		<!--- 2012-07-23 PPB P-SMA001 note: countryScopeOpportunityRecords setting checked inside getRightsFilterWhereClause() --->
		AND 	oppPricingStatusID not in (2,7,8,9)
		AND 	OppPricingStatusID > 1
		and oppstageid <> 7
		order by <cf_queryObjectName value="#sortOrder#">
</cfquery>
<cfset getSPQOpps = QueryNew("opportunity_ID,Account,Stage,detail,expected_Close_Date,calculated_budget,overall_customer_budget,unapproved_special_price,last_Updated,Account_Manager,status,Month_expected_close,Quarter_expected_close,probability,Partner_Name,entityid,vendorAccountManagerPersonID,country,region,SPRequiredApprovalLevel,SPCurrentApprovalLevel,SPApprovalNumber,pricing_status,oppPricingStatusID,distributor,SPApproverPersonIDs,alphabeticalIndex,SPQ_Last_Updated")>

<cfif IsDefined("Approvers") AND (#ArrayLen(Approvers)# GTE 1)>
	<cfloop query="qGetSPQOpps">
		<cfset keep = "false">
		<cfscript>
			productApprovalRequired = application.com.oppSpecialPricing.productCheck(opportunityID = qGetSPQOpps.opportunity_ID);
		</cfscript>
		<cfquery name="getApproverCountries" datasource="#application.sitedatasource#">
			Select countryID from oppSPApprover where 1=1
			<cfif productApprovalRequired>
				and (productapproverlevel - 1) = <cf_queryparam value="#Approvers[1][3]#" CFSQLTYPE="CF_SQL_INTEGER" >
			<cfelse>
				and (approverlevel - 1) = <cf_queryparam value="#Approvers[1][2]#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfif>
				and personID = #request.relaycurrentuser.personid#
		</cfquery>
		<cfquery name="getOtherApproverCountries" datasource="#application.sitedatasource#">
			Select countryID from oppSPApprover where 1=1
			<cfif productApprovalRequired>
				and (productapproverlevel - 1) = <cf_queryparam value="#Approvers[1][3]#" CFSQLTYPE="CF_SQL_INTEGER" >
			<cfelse>
				and (approverlevel - 1) = <cf_queryparam value="#Approvers[1][2]#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfif>
		</cfquery>
		<cfif productApprovalRequired and qGetSPQOpps.SPCurrentApprovalLevel eq Approvers[1][3]>
			<!--- if country matches approver country or default approver without a country specific --->
			<cfif (listfind(valuelist(getApproverCountries.countryID),qGetSPQOpps.countryID) neq 0) or (Approvers[1][1] eq 0 and listfind(valuelist(getOtherApproverCountries.countryID),qGetSPQOpps.countryID) eq 0)>
				<cfset keep = "true">
			</cfif>
		<cfelseif productApprovalRequired eq "false" and qGetSPQOpps.SPCurrentApprovalLevel eq Approvers[1][2]>
			<!--- if country matches approver country or default approver without a country specific --->
			<cfif (listfind(valuelist(getApproverCountries.countryID),qGetSPQOpps.countryID) neq 0) or (Approvers[1][1] eq 0 and listfind(valuelist(getOtherApproverCountries.countryID),qGetSPQOpps.countryID) eq 0)>
				<cfset keep = "true">
			</cfif>
		</cfif>
		<cfif keep>
			<cfscript>
				QueryAddRow( getSPQOpps,1);
				QuerySetCell(getSPQOpps,"opportunity_ID",qGetSPQOpps.opportunity_ID);
				QuerySetCell(getSPQOpps,"Account",qGetSPQOpps.Account);
				QuerySetCell(getSPQOpps,"Stage",qGetSPQOpps.Stage);
				QuerySetCell(getSPQOpps,"detail",qGetSPQOpps.detail);
				QuerySetCell(getSPQOpps,"expected_Close_Date",qGetSPQOpps.expected_Close_Date);
				QuerySetCell(getSPQOpps,"calculated_budget",qGetSPQOpps.calculated_budget);
				QuerySetCell(getSPQOpps,"overall_customer_budget",qGetSPQOpps.overall_customer_budget);
				QuerySetCell(getSPQOpps,"unapproved_special_price",qGetSPQOpps.unapproved_special_price);
				QuerySetCell(getSPQOpps,"last_Updated",qGetSPQOpps.last_Updated);
				QuerySetCell(getSPQOpps,"status",qGetSPQOpps.status);
				QuerySetCell(getSPQOpps,"Month_expected_close",qGetSPQOpps.Month_expected_close);
				QuerySetCell(getSPQOpps,"Quarter_expected_close",qGetSPQOpps.Quarter_expected_close);
				QuerySetCell(getSPQOpps,"probability",qGetSPQOpps.probability);
				QuerySetCell(getSPQOpps,"Partner_Name",qGetSPQOpps.Partner_Name);
				QuerySetCell(getSPQOpps,"entityid",qGetSPQOpps.entityid);
				QuerySetCell(getSPQOpps,"vendorAccountManagerPersonID",qGetSPQOpps.vendorAccountManagerPersonID);
				QuerySetCell(getSPQOpps,"country",qGetSPQOpps.country);
				QuerySetCell(getSPQOpps,"region",qGetSPQOpps.region);
				QuerySetCell(getSPQOpps,"SPRequiredApprovalLevel",qGetSPQOpps.SPRequiredApprovalLevel);
				QuerySetCell(getSPQOpps,"SPCurrentApprovalLevel",qGetSPQOpps.SPCurrentApprovalLevel);
				QuerySetCell(getSPQOpps,"SPApprovalNumber",qGetSPQOpps.SPApprovalNumber);
				QuerySetCell(getSPQOpps,"pricing_status",qGetSPQOpps.pricing_status);
				QuerySetCell(getSPQOpps,"oppPricingStatusID",qGetSPQOpps.oppPricingStatusID);
				QuerySetCell(getSPQOpps,"distributor",qGetSPQOpps.distributor);
				QuerySetCell(getSPQOpps,"SPApproverPersonIDs",qGetSPQOpps.SPApproverPersonIDs);
				QuerySetCell(getSPQOpps,"alphabeticalIndex",qGetSPQOpps.alphabeticalIndex);
			</cfscript>
		</cfif>
	</cfloop>
</cfif>

	<!--- approvers are associated with a country and approver level --->
	<!--- getApprovers.recordcount eq 1 and --->
<!--- 	<cfif #IsDefined("Approvers")# AND (#ArrayLen(Approvers)# GTE 1)>
		and (
		  <cfloop index=i from=1 to="#ArrayLen(Approvers)#">
				(
					<cfif #Approvers[i][1]# NEQ 0>
					<!--- only where SPQ countryid matches current user countryid --->
						countryid = #Approvers[i][1]# AND
					<cfelseif #Approvers[i][1]# EQ 0>
					<!--- only where no country-specific approver for current SPQ --->
						countryid NOT IN (
						select countryid
						from oppspapprover
						where approverLevel - 1 = #Approvers[i][2]#) AND
					</cfif>
						SPCurrentApprovalLevel = #Approvers[i][2]#
				)
				<cfif i NEQ #ArrayLen(Approvers)#> OR </cfif>
		  </cfloop>
		)
	</cfif> --->
<cfif getSPQOpps.recordcount eq 0>
	<cfoutput>#application.com.relayUI.message(message="You currently do not have any deals to approve",type="info",showClose=false)#</cfoutput>

<cfelse>


<cf_translate>

<cfparam name="IllustrativePriceColumns" default="">
<cfif structkeyexists(request,"UseIllustrativePrice")>
	<cfset IllustrativePriceColumns = "CALCULATED_BUDGET">
</cfif>

<CF_tableFromQueryObject
	queryObject="#getSPQOpps#"
	queryName="getSPQOpps"
	sortOrder = "#sortOrder#"
	openAsExcel = "#openAsExcel#"
	numRowsPerPage="#numRowsPerPage#"

	keyColumnList="#keyColumnList#"
	keyColumnURLList="#keyColumnURLList#"
	keyColumnKeyList="#keyColumnKeyList#"

	hideTheseColumns="OVERALL_CUSTOMER_BUDGET,EXPECTED_CLOSE_DATE,LAST_UPDATED,oppPricingStatusID,SPRequiredApprovalLevel,SPCurrentApprovalLevel,PROBABILITY"
	showTheseColumns="#SPQleadAndOppListShowColumns#"
	dateFormat="#dateFormat#"
	columnTranslation="true"

	IllustrativePriceFromCurrency="USD"
	IllustrativePriceToCurrency="EUR"
	IllustrativePriceDateColumn="last_updated"
	IllustrativePriceColumns="#IllustrativePriceColumns#"

	allowColumnSorting="yes"
	currencyFormat="#application.com.settings.getSetting('leadManager.SPQMyApprovals.currencyFormatColumnList')#"
	startRow="#startRow#"


	rowIdentityColumnName="opportunity_id"


	useInclude="false"
>
</cf_translate>
</cfif>




