<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			oppProductSetDelete.cfm
Author:				NM
Date started:		02 August 2005

Description:			

- Deletion of Product Sets for lead and opportunity. A product set is a grouping of products.

Amendment History:

Date (DD-MMM-YYYY)	Initials	Code	What was changed

Possible enhancements:

--->

<cf_translate>


<cf_head>
<cf_title>Delete Product Set</cf_title>
</cf_head>



<table>
	<tr>
		<td align="center">
<cfif not IsDefined("productSetID")>
	Cannot delete Product Set without a productSetID
	<CF_ABORT>
<cfelse>
	<cfif isDefined("goDelete")>
		<cfscript>
			application.com.oppProductSets.deleteProductSet(ProductSetID);
		</cfscript>
		Product Set Deleted.
		<SCRIPT type="text/javascript">
			opener.location.reload(false);
		</script>
	<cfelse>
		<cfscript>
		qry_getProductSet=application.com.oppProductSets.get(productSetID);
		</cfscript>
		<cfoutput>
			<cfif qry_getProductSet.recordcount gt 0>
				Are you sure you want to delete this product set:<br>
				#htmleditformat(qry_getProductSet.ProductSetName)# (ProductSetID - #htmleditformat(productSetID)#)<br><br>
				
				<form name="doDelete" action="" method="post">
					<input type="submit" name="goDelete" value="Delete">&nbsp;<input type="button" name="noDelete" value="Cancel" onClick="window.close();">
					<CF_INPUT type="hidden" name="ProductSetID" value="#ProductSetID#">
				</form>
			<cfelse>
				The selected product set no longer exists in the database.
			</cfif>
		</cfoutput>
	</cfif>
</cfif>
		</td>
	</tr>
</table>



</cf_translate>
<!--- 
<cfdump var="#myopportunity.getOppProducts#">
<cfdump var="#myopportunity.qProductAndGroupList#">
 --->
<!--- 
<cfdump var="#myopportunity.qproductGroupID#">
 --->

<!--- 
<cfcatch type="any">
<cfdump var="#cfcatch#">
</cfcatch>
</cftry>
 --->


