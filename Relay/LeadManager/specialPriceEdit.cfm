<!--- �Relayware. All Rights Reserved 2014 --->

<!--- 
File name:			specialPriceEdit.cfm
Author:				SWJ
Date started:		2004-02-15
	
Description:			

called by oppProductList.cfm as a pop up for special price reasons

Amendment History:

Date (DD-MMM-YYYY)	Initials	Code	What was changed
15-02-2004			SWJ					Initial version
02-MAR-2005			AJC			CR_ATI002_superAM	- Added ability for a super account manager to see
the SPQ request form when they are not the vendorAccountManager
11-SEP-2008			GCC	Extended getCountryApprovers method calls to include productCheck call and then productApprovalRequired passed in. BT ID 2451
24-08-2009			NJH	P-ATI005 - added two new approval levels. Display the two new approvers for these levels.
2009-10-22			NAS LID - 2759 - Add an error message when the Query 'getOppDetails' does not return a record because the STAGE = 'Delete'
2010/01/29			NJH			P-ATI007 - changed checkApproverLevelRequired call so that we could take advantage of over-riding the function
2010/06/04			NJH	LID3396 - added entity to record rights filter
2010/11/23			NJH	LID 4908 - used the cfquery function in globalFunctions, rather than the local version.

Possible enhancements:
 --->

 
<cfparam name="message" default="">
<cfparam name="url.description" type="string" default="Unknown product">
<cfparam name="entityID" type="string" default="1">
<cfparam name="form.pagesBack" type="string" default="1">
<cfparam name="form.sortOrder" type="string" default="SKU ASC">
<cfparam name="SPQbccList" type="string" default="relayhelpcc@foundation-network.com">

<cfparam name="useProductStatus" default="true">
<cfparam name="ShowProductDescription" default="true">
<cfparam name="ShowSpecialDealFlag" default="false">
<cfparam name="ShowSpecialDealFlagID" default="1">

<!--- 2011/05/27 PPB REL106 get email lists from settings --->
<cfset SPQLevel1and2CCEmailList = application.com.settings.getSetting("leadManager.SPQLevel1and2CCEmailList")>			
<cfset SPQLevel2CCEmailList = application.com.settings.getSetting("leadManager.SPQLevel2CCEmailList")>			
<cfset SPQLevel3CCEmailList = application.com.settings.getSetting("leadManager.SPQLevel3CCEmailList")>			
<cfset SPQLevel4CCEmailList = application.com.settings.getSetting("leadManager.SPQLevel4CCEmailList")>			
<cfset SPQLevel5CCEmailList = application.com.settings.getSetting("leadManager.SPQLevel5CCEmailList")>			


<cfscript>
	productApprovalRequired = application.com.oppSpecialPricing.productCheck(opportunityID = entityID);
</cfscript>

<cfinvoke component="relay.com.oppSpecialPricing" method="getCountryApprovers" returnvariable="SPApprovers">
	<cfinvokeargument name="dataSource" value="#application.siteDataSource#"/>
	<cfinvokeargument name="opportunityID" value="#entityID#"/>
	<cfinvokeargument name ="productApprovalRequired" value="#productApprovalRequired#">
</cfinvoke>

<cfif isDefined("form.specialPricingStatusMethod") and isDefined("form.processProductsShipped") and processProductsShipped eq "">
	<cfswitch expression="#form.specialPricingStatusMethod#">
		<cfcase value="SP_Calculate">
			<cfparam name="desiredDiscount" default="">
			<cfparam name="desiredBudget" default="">
			<cfinvoke component="relay.com.oppSpecialPricing" method="updateSpecialPricing" returnvariable="message">
				<cfinvokeargument name="dataSource" value="#application.siteDataSource#"/>
				<cfinvokeargument name="opportunityID" value="#entityID#"/>
				<cfinvokeargument name="specialPricingStatusMethod" value="#form.specialPricingStatusMethod#"/>
				<cfinvokeargument name="personID" value="#request.relayCurrentUser.personid#"/>
				<cfinvokeargument name="desiredDiscount" value="#desiredDiscount#"/>
				<cfinvokeargument name="desiredBudget" value="#desiredBudget#"/>
				<cfinvokeargument name="SPExpiryDate" value="#SPExpiryDate#"/>
				<cfinvokeargument name="preservedOppPricingStatus" value="#FORM.preservedOppPricingStatus#"/>
			</cfinvoke>
	
			<cfset message = message>
		</cfcase>
	
		<cfdefaultcase>
		 <!--- Build CFQUERY function to enable in-cfscript query calls --->
		<!--- <cffunction name="CFQUERY" access="public" returntype="query">
		  <cfargument name="SQLString" type="string" required="yes">
		  <cfargument name="Datasource" type="string" required="yes">
		  <cfargument name="dbType" type="string" default="">		
		  <cfquery name="RecordSet" datasource="#arguments.Datasource#" 
		    dbtype="#arguments.dbType#">
			#preserveSingleQuotes(arguments.SQLString)#
		 </cfquery>
		 <cfreturn RecordSet>
		</cffunction>		 --->
		<cfscript>
			// JC 2004-12-17 append country-specific recipients to cc list
			if (IsDefined("SPAPPROVERLEVEL") AND IsDefined("entityid")){
			SQLSTRING="
				select p.email
				from flag f, integerMultipleFlagData iMFD, Person p
				where (f.flagtextid = 'SPQEmailCC'";
					if (SPApproverLevel EQ 1 OR SPApproverLevel EQ 2){
						SQLSTRING = SQLSTRING & " or f.flagtextid = 'SPQEmailCCApp1and2'";
					}
				SQLSTRING = SQLSTRING & ") and f.flagid = imfd.flagid
				and imfd.entityid = 
					(select countryID from vLeadListingCalculatedBudget where opportunity_ID = #entityid#)and imfd.data = p.personid";
			DATASOURCE="#application.siteDataSource#";
			getCountryEmail=application.com.globalFunctions.cfQuery(SQLSTRING:SQLSTRING,DATASOURCE:DATASOURCE);
			}
			
			SPQCountryCCEmailList = "";
			
			if (IsDefined("getCountryEmail.email") and #getCountryEmail.email# NEQ ""){
					SPQCountryCCEmailList = "#getCountryEmail.email#";
			}
			
			// JC 2004-12-17 append users with view rights to the cc list
			// NJH 2010/06/04 P-PAN002 - added the entity filter
			if (IsDefined("SPAPPROVERLEVEL") AND IsDefined("entityid")){
			SQLSTRING="select p.personid,email
				from usergroup ug, person p
				where ug.usergroupid IN (select usergroupid
				FROM RecordRights 
				WHERE RecordID = #entityid# and entity='opportunity')
				and ug.personid = p.personid";
			theDATASOURCE="#application.siteDataSource#";
			getViewerEmails=application.com.globalFunctions.cfQuery(SQLSTRING:SQLSTRING,DATASOURCE:theDATASOURCE);
			}
			
			SPQViewCCEmailList = "";
			
			if (IsDefined("getViewerEmails.email") and #getViewerEmails.email# NEQ ""){
					/*SPQViewCCEmailList = "#getViewerEmails.email#";*/
			}
		</cfscript>
		<cfset SPQViewCCEmailList = "">
		<cfoutput query="getViewerEmails">
			<cfset SPQViewCCEmailList = listappend(SPQViewCCEmailList,getViewerEmails.email)>
		</cfoutput>
		
			<cfinvoke component="relay.com.oppSpecialPricing" method="progressSpecialPricing" returnvariable="message">
				<cfinvokeargument name="dataSource" value="#application.siteDataSource#"/>
				<cfinvokeargument name="opportunityID" value="#entityID#"/>
				<cfinvokeargument name="specialPricingStatusMethod" value="#form.specialPricingStatusMethod#"/>
				<cfinvokeargument name="personID" value="#request.relayCurrentUser.personid#"/>
				<cfinvokeargument name="comments" value="#frmComments#"/>
				<cfinvokeargument name="SPApproverLevel" value="#SPApproverLevel#"/>
				<cfinvokeargument name="SPApproverLevelRequired" value="#SPApproverLevelRequired#"/>
				<cfinvokeargument name="SPQLevel1and2CCEmailList" value="#SPQLevel1and2CCEmailList#"/>
				<cfinvokeargument name="SPQLevel2CCEmailList" value="#SPQLevel2CCEmailList#"/>
				<cfinvokeargument name="SPQLevel3CCEmailList" value="#SPQLevel3CCEmailList#"/>
				<cfinvokeargument name="SPQLevel4CCEmailList" value="#SPQLevel4CCEmailList#"/> <!--- NJH 2009/08/24 P-ATI005 --->
				<cfinvokeargument name="SPQLevel5CCEmailList" value="#SPQLevel5CCEmailList#"/> <!--- NJH 2009/08/24 P-ATI005 --->
				<cfinvokeargument name="SPQCountryCCEmailList" value="#SPQCountryCCEmailList#"/>
				<cfinvokeargument name="SPQViewCCEmailList" value="#SPQViewCCEmailList#" />
				<cfif isDefined("SPapprovers")>
					<cfinvokeargument name="Level3Approvers" value="#evaluate(StructCount(SPapprovers.approverPersonID)-2)#" /> <!--- discount the 2 lev 1 and 2 approvers --->
				</cfif>
				
			</cfinvoke>
		</cfdefaultcase>
	</cfswitch>
<!--- update shipments --->
<cfelseif isDefined("form.processProductsShipped") and processProductsShipped eq "y">
	
	<cfinvoke component="relay.com.oppSpecialPricing" method="setProductShipping" returnvariable="message">
		<cfinvokeargument name="dataSource" value="#application.siteDataSource#"/>
		<cfinvokeargument name="formFields" value="#form.FieldNames#"/>
		<cfinvokeargument name="personID" value="#request.relayCurrentUser.personid#"/>
		<cfinvokeargument name="opportunityID" value="#entityID#"/>
	</cfinvoke>

</cfif>

<cfquery name="getOppDetails" datasource="#application.siteDataSource#">
	select * from vLeadListingCalculatedBudget where opportunity_ID =  <cf_queryparam value="#entityID#" CFSQLTYPE="CF_SQL_INTEGER" > 
</cfquery>

<!--- Start 2009-11-02			NAS LID - 2759 - Add an error message when the Query 'getOppDetails' does not return a record because the STAGE = 'Delete' --->
<cfif getOppDetails.recordcount EQ 0>
	<cfoutput>
		The Opportunity Stage is currently set to 'DELETE', please change this to access the 'Special Pricing Editor' <A HREF="javascript:history.go(-#form.pagesBack#);" Class="Submenu">Back</A> &nbsp;
	</cfoutput>
	<CF_ABORT>
</cfif>
<!--- End 	2009-11-02			NAS LID - 2759 - Add an error message when the Query 'getOppDetails' does not return a record because the STAGE = 'Delete' --->

<cfparam name="SPapproverLevel" default="0">
<cfinvoke component="relay.com.oppSpecialPricing" method="checkApprover" returnvariable="SPApproverLevel">
	<cfinvokeargument name="dataSource" value="#application.siteDataSource#"/>
	<cfinvokeargument name="personID" value="#request.relayCurrentUser.personid#"/>
	<cfinvokeargument name="countryID" value="#getOppDetails.countryID#"/>
	<cfinvokeargument name="opportunityID" value="#entityID#"/>
</cfinvoke>

<cfif getOppDetails.SPRequiredApprovalLevel neq "" and isNumeric(getOppDetails.SPRequiredApprovalLevel)>
	<cfset SPApproverLevelRequired = getOppDetails.SPRequiredApprovalLevel>
<cfelse>
	<!--- NJH 2010/01/29 P-ATI007 - changed call to that of below so that I could take advantage of over-riding functions. --->
	<!--- <cfinvoke component="relay.com.oppSpecialPricing" method="checkApproverLevelRequired" returnvariable="SPApproverLevelRequired">
		<cfinvokeargument name="dataSource" value="#application.siteDataSource#"/>
		<cfinvokeargument name="opportunityID" value="#entityID#"/>
	</cfinvoke> --->

	<cfset SPApproverLevelRequired = application.com.oppSpecialPricing.checkApproverLevelRequired(dataSource=application.siteDataSource,opportunityID=entityID)>
</cfif>

<cfif listLen(SPApproverLevel) gt 1>
	<!--- if this person holds more than one approver levels you need to decide which to use --->
	<cfif listfind(SPapproverLevel, listfirst(SPApproverLevelRequired,"|")) gt 0>
		<!--- first try to use the SPapproverLevelRequired --->
		<cfset SPApproverLevel = listfind(SPapproverLevel, listfirst(SPApproverLevelRequired,"|"))>
	<cfelse>
		<!--- otherwise use the first in the list --->
		<cfset SPApproverLevel = listFirst(SPApproverLevel)>
	</cfif>
</cfif>

<cfinvoke component="relay.com.oppSpecialPricing" method="getOppPricingReason" returnvariable="qOppPricingReason">
	<cfinvokeargument name="dataSource" value="#application.siteDataSource#"/>
</cfinvoke>

<cfinvoke component="relay.com.oppSpecialPricing" method="getOppPricing" returnvariable="qOppPricing">
	<cfinvokeargument name="dataSource" value="#application.siteDataSource#"/>
	<cfinvokeargument name="opportunityID" value="#entityID#"/>
</cfinvoke>

<cfscript>
// create an instance of the opportunity component
if (fileexists(application.paths.code & "\cftemplates\relayOpportunity.cfc")) {
	componentPath = "code.CFTemplates.relayOpportunity";
	myopportunity = createObject("component",componentPath);}
else {myopportunity = createObject("component","relay.com.opportunity");}
myopportunity.dataSource = application.siteDataSource;
myopportunity.opportunityID = entityID;
myopportunity.productCatalogueCampaignID = application.com.settings.getSetting("leadManager.products.productCatalogueCampaignID");
myopportunity.countryID = getOppDetails.countryID;
myopportunity.currencysign = application.com.currency.getCurrencyByISOCode(getOppDetails.currency).currencysign;
myopportunity.sortOrder = form["sortOrder"];
myopportunity.getOppProducts();
</cfscript>

<cf_translate>
<!--- Phr_Sys_oppProductSpecialPriceRequest --->

<cf_head>
<cf_title>Phr_Sys_oppProductSpecialPricePopUpTitle</cf_title>
<cfinclude template="/templates/relayFormJavaScripts.cfm">
<SCRIPT type="text/javascript">
<!--
function saveSpecialPricing(action)
{
	//alert(action);
  document.frmReasonForm.specialPricingStatusMethod.value = action;
  var doThis = "yes";
  //alert(document.frmReasonForm.specialPricingStatusMethod.value);
  if (action == "SP_AddComment" && document.frmReasonForm.frmComments.value == "")
  	{
	alert ("Please add in your comments");
	doThis = "no";
	}
// START ALEX
  if (action == "SP_InfoResponse" && document.frmReasonForm.frmComments.value == "")
  	{
	alert ("Please add in your further information");
	doThis = "no";
	}
// END ALEX
  if (action == "SP_Calculate")
  	{
	document.frmReasonForm.frmComments.value = "Budget changed to " + document.frmReasonForm.newBudget.value
	}
  if (action == "SP_Request" && document.frmReasonForm.frmComments.value == "")
  	{
	alert ("You must provide a reson for the special pricing");
	doThis = "no";
	}
  if (action == "SP_FullyApprove" && document.frmReasonForm.frmComments.value == "")
  	{
	alert ("Please add in your comments");
	doThis = "no";
	}
  if (action == "SP_MoreInfo" && document.frmReasonForm.frmComments.value == "")
  	{
	alert ("Please describe the information you are looking for in the comments field.");
	doThis = "no";
	}
  if (action == "SP_Reject" && document.frmReasonForm.frmComments.value == "")
  	{
	alert ("Please describe why you are rejecting this request in the comments field.");
	doThis = "no";
	}
  if (action == "SP_Complete")
    {
	var messageStr = "Are you sure? \n"
        messageStr += "If you click yes you will not be able to change the \n"
        messageStr += "Shipped Quantity again for any line item.\n\n" ; 
	var proceed = confirm(messageStr); 
	if (proceed == false)
		doThis = "no" 
	}
  if (doThis == "yes" )
  	document.frmReasonForm.submit();
}

function CheckIfFrmCommentsEmpty(){
	if (document.frmReasonForm.frmComments.value == "")
		document.frmReasonForm.specialPriceRequest.disabled=true
	else
		document.frmReasonForm.specialPriceRequest.disabled=false
}

function updateProductsShipped(){
	document.frmReasonForm.processProductsShipped.value = "y";
	document.frmReasonForm.submit();
}
//-->
</script>

</cf_head>


<cfoutput>
<table width="100%" border="0" cellspacing="0" cellpadding="3" align="center" bgcolor="white" class="withBorder">
	<tr>
		<td colspan="2" align="left" class="Submenu"><b>Phr_Sys_oppProductSpecialPricing</b></td>
		<td align="right" class="submenu">				
			<A HREF="javascript:print();" Class="Submenu">Print</A> &nbsp;|&nbsp;
			<A HREF="javascript:history.go(-#form.pagesBack#);" Class="Submenu">Back</A> &nbsp;
		</td>
	</tr>
</table>

<table width="100%" border="0" cellspacing="1" cellpadding="3" align="center" bgcolor="white" class="withBorder">	
<cfform  method="post" name="frmReasonForm" id="frmReasonForm">
	<input type="hidden" name="specialPricingStatusMethod" value="">
	<CF_INPUT type="hidden" name="preservedOppPricingStatus" value="#getOppDetails.Pricing_Status#">
	<CF_INPUT type="hidden" name="entityID" value="#entityID#">
	<input type="hidden" name="processProductsShipped" value="">
	<!--- <cfif getOppDetails.oppPricingStatusID is 6>
		<input type="hidden" name="SPApproverLevel" value="#getOppDetails.SPCurrentApprovalLevel#">
	<cfelse> --->
		<CF_INPUT type="hidden" name="SPApproverLevel" value="#SPApproverLevel#">
	<!--- </cfif> --->
	<CF_INPUT type="hidden" name="SPApproverLevelRequired" value="#listFirst(SPApproverLevelRequired,"|")#">
	<CF_INPUT type="hidden" name="pagesBack" value="#IncrementValue(form.pagesBack)#">
	<tr>
		<td colspan="3" align="center" nowrap style="color: Red;">#application.com.security.sanitiseHTML(message)#&nbsp;</td>
	</tr>
	
	
	<tr>
		<td class="label">Project:</td>
		<td><a href="opportunityEdit.cfm?opportunityid=#entityID#">#htmleditformat(getOppDetails.detail)#</a> (#htmleditformat(entityID)#)</td>
		<td>&nbsp;</td>
	</tr>
	
<!--- Checks for special deal flag MDC- 2006-01-06 ATI NSP projects controlled in opportunityINI --->	
	<cfif ShowSpecialDealFlag eq "true">

		<cfquery name="getSPflagID" datasource="#application.siteDataSource#">
			select name 
			from flag f
			join booleanflagdata b on b.flagid = f.flagid
			where f.flagid  in ( <cf_queryparam value="#ShowSpecialDealFlagID#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) 
			and b.entityid =  <cf_queryparam value="#entityID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfquery>
		
		<cfif getSPflagID.recordcount neq 0>
		<tr>
			<td valign="top" class="label">Deal Type:</td>
			<td>
				<cfloop query="getSPFlagID">
					#htmleditformat(getSPflagID.Name)#<cfif getSPFlagID.currentrow lt getSPflagID.recordcount>,</cfif>
				</cfloop>
			</td>
		</tr>
		</cfif>
	</cfif> 

	
	<cfif isNumeric(SPApproverLevelRequired) and SPApproverLevelRequired neq 0>
	<tr>
		<td valign="top" class="label">Approver Level Required :</td>
		<td>#htmleditformat(SPApproverLevelRequired)#</td>
		<td>&nbsp;</td>
	</tr> 
</cfif>

<cfif isNumeric(getOppDetails.SPCurrentApprovalLevel)>
	<tr>
		<td valign="top" class="label">Current Approval Level:</td>
		<td>#htmleditformat(getOppDetails.SPCurrentApprovalLevel)#</td>
		<td>&nbsp;</td>
	</tr> 
</cfif>

<cfif isDefined("SPapprovers") and isNumeric(getOppDetails.SPRequiredApprovalLevel) and getOppDetails.SPRequiredApprovalLevel gt 0>
	<tr>
		<td valign="top" class="label">The approvers are: </td>
		<td>
		Level 1: #listlast(SPapprovers.approverPersonID.level1,"|")#<cfif listFind(getOppDetails.SPApproverPersonIDs,listfirst(SPapprovers.approverPersonID.level1,"|")) neq 0> <strong>Approved</strong></cfif><br>
		<cfif getOppDetails.SPRequiredApprovalLevel gt 1>
		Level 2: #listlast(SPapprovers.approverPersonID.level2,"|")#<cfif listFind(getOppDetails.SPApproverPersonIDs,listfirst(SPapprovers.approverPersonID.level2,"|")) neq 0> <strong>Approved</strong></cfif><br>
		</cfif>
		<cfif getOppDetails.SPRequiredApprovalLevel gt 2>
		Level 3: #listlast(SPapprovers.approverPersonID.level3,"|")#<cfif listFind(getOppDetails.SPApproverPersonIDs,listfirst(SPapprovers.approverPersonID.level3,"|")) neq 0> <strong>Approved</strong></cfif><br>
		</cfif>
		<!--- NJH 2009/08/24 P-ATI005 - added two extra levels of approval --->
		<cfif getOppDetails.SPRequiredApprovalLevel gt 3>
		Level 4: #listlast(SPapprovers.approverPersonID.level4,"|")#<cfif listFind(getOppDetails.SPApproverPersonIDs,listfirst(SPapprovers.approverPersonID.level4,"|")) neq 0> <strong>Approved</strong></cfif><br>
		</cfif>
		<cfif getOppDetails.SPRequiredApprovalLevel gt 4>
		Level 5: #listlast(SPapprovers.approverPersonID.level5,"|")#<cfif listFind(getOppDetails.SPApproverPersonIDs,listfirst(SPapprovers.approverPersonID.level5,"|")) neq 0> <strong>Approved</strong></cfif><br>
		</cfif>
		</td>
		<td>&nbsp;</td>
	</tr> 
</cfif>

<cfif isNumeric(SPapproverLevel) and SPapproverLevel neq 0>
	<tr>
		<td valign="top" class="label">You are an approver level:</td>
		<td>#htmleditformat(SPapproverLevel)#</td>
		<td>&nbsp;</td>
	</tr> 
</cfif>
	<!--- CR_ATI019 2005/08/11 GCC Show margin analysis to any approver --->
	<!--- check if they are an approver full stop - country ignorant --->
	<cfquery name="getAnyApprover" datasource="#application.sitedatasource#">
	--select 1 from voppSPApprover where personid = #request.relayCurrentUser.personid#
	Select 1 from booleanflagdata bfd inner join flag f on bfd.flagid = f.flagid
	where flagtextid = 'SPApprover' and entityid = #request.relayCurrentUser.personid#
	</cfquery>
	
	<cfif getOppDetails.vendorAccountManagerPersonID eq request.relayCurrentUser.personid>	
	<tr>
		<td colspan="3" align="center">
			<iframe src="oppProductList.cfm?opportunityID=#htmleditformat(entityID)#&probability=#htmleditformat(getOppDetails.Probability)#&countryID=#htmleditformat(getOppDetails.countryID)#" width="100%" height="360">
			</iframe>
		</td>
	</tr>
	<cfelse>
	
		<cfset IllustrativePriceToCurrency = application.com.settings.getSetting("leadManager.IllustrativePriceToCurrency")>
	
	
	<tr>
		<td colspan="3" class="label">phr_oppSPProductTextArea</td>
	</tr>
	<tr>
		<td colspan="3" align="center">
			<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White">
				<cfparam name="theColSpan" default="6">
				<cfparam name="theColSpan2" default="4">
				<cfif structkeyExists(request,"illustrativePriceMethod")>
					<cfset theColSpan = 7>
					<cfset theColSpan2 = 5>
				</cfif>
				<tr>
					<th colspan="#theColSpan#" align="left">
						phr_oppProductSectionHeading
					</th>
					<!--- CR_ATI019 2005/08/11 GCC Show margin analysis to any approver --->
					<cfif getAnyApprover.recordcount gt 0>
						<th colspan="#htmleditformat(theColSpan2)#" align="left">
							Margin analysis
						</th>
					</cfif>
					<th></th>
				</tr>
				<TR>
					<cfif ShowProductDescription>
					<TH>phr_oppProductDescription</TH>
					</cfif>
					<TH>phr_oppProductSKU</TH>
					<TH>phr_oppProductquantity</TH>
					<cfif useProductStatus>
					<th nowrap>phr_oppProductStatus</th>
					</cfif>
					<TH>phr_oppProductUnitPrice</TH>
					<TH>phr_oppProductSpecialPrice</TH>
					<th nowrap>phr_oppProductSubTotal <cfif structkeyexists(request,"IllustrativePriceFromCurrency")>(#htmleditformat(request.IllustrativePriceFromCurrency)#)</cfif></th>
					<!--- CR_ATI031 - GCC - 2005/12/19 - euros --->
					<cfif structkeyExists(request,"illustrativePriceMethod")>						
						<cfswitch expression="#request.illustrativePriceMethod#">
							<cfcase value="2">
								<th>phr_oppProductSubTotal (#htmleditformat(IllustrativePriceToCurrency)#) phr_IllUSTRATIVE</th>
							</cfcase>
						</cfswitch>						
					</cfif>
					<!--- CR_ATI019 2005/08/11 GCC Show margin analysis to any approver --->
					<cfif getAnyApprover.recordcount gt 0>
					<!--- <cfif isNumeric(getOppDetails.SPCurrentApprovalLevel) 
							and SPapproverLevel gt 0> ---><!--- eq evaluate(getOppDetails.SPCurrentApprovalLevel + 1)> --->
					<TH>phr_oppProductInitialCOGS</TH>
					<TH>phr_oppProductCOGS</TH>
					<th nowrap>phr_oppProductMargin <cfif structkeyexists(request,"IllustrativePriceFromCurrency")>(#htmleditformat(request.IllustrativePriceFromCurrency)#)</cfif></th>
					<!--- CR_ATI031 - GCC - 2005/12/19 - euros --->
					<cfif structkeyExists(request,"illustrativePriceMethod")>						
						<cfswitch expression="#request.illustrativePriceMethod#">
							<cfcase value="2">
								<th>phr_oppProductMargin (#htmleditformat(IllustrativePriceToCurrency)#) phr_IllUSTRATIVE</th>
							</cfcase>
						</cfswitch>						
					</cfif>
					<th>% Margin</th>
					</cfif>
					<TH>Shipped</TH>
				</TR>
				
				<CFLOOP QUERY="myopportunity.getOppProducts">
					<TR<CFIF CurrentRow MOD 2 IS NOT 0> class="oddrow"<CFELSE> class="evenRow"</CFIF>>
						<cfif ShowProductDescription>
						<td>#htmleditformat(description)#</td>
						</cfif>
						<td>#htmleditformat(sku)#</td>
						<td align="right">#htmleditformat(quantity)#</td>
						<cfif useProductStatus>
						<td align="right">#htmleditformat(status)#</td>
						</cfif>			
						<td align="right">#htmleditformat(myopportunity.currencysign)##DecimalFormat(unitprice)#</td>
						<td align="right"><cfif specialPrice neq "">#htmleditformat(myopportunity.currencysign)##DecimalFormat(specialPrice)#</cfif></td>
						<td align="right">#htmleditformat(myopportunity.currencysign)##DecimalFormat(subtotal)#</td>
						<!--- CR_ATI031 - GCC - 2005/12/19 - euros --->
						<cfif structkeyExists(request,"illustrativePriceMethod")>						
							<cfswitch expression="#request.illustrativePriceMethod#">
								<cfcase value="2">
									<cfscript>
										IllustrativePrice = application.com.currency.convert(
											method = #request.IllustrativePriceMethod#,
											fromValue = subtotal,
											fromCurrency = request.IllustrativePriceFromCurrency,
											toCurrency = IllustrativePriceToCurrency,
											date = getOppDetails.last_Updated
											);
									</cfscript>
									<cfset IllustrativePriceOutput = IllustrativePrice.toSign & IllustrativePrice.toValue>
									<td align="right">#htmleditformat(IllustrativePriceOutput)#</td>
								</cfcase>
							</cfswitch>						
						</cfif>
						<cfif getAnyApprover.recordcount gt 0><!--- eq evaluate(getOppDetails.SPCurrentApprovalLevel + 1)> --->
						<td align="right">#htmleditformat(myopportunity.currencysign)##DecimalFormat(initialCOGS)#</td>
						<td align="right">#htmleditformat(myopportunity.currencysign)##DecimalFormat(COGS)#</td>
						<td align="right">#htmleditformat(myopportunity.currencysign)##DecimalFormat(SPQMargin)#</td>
						<!--- CR_ATI031 - GCC - 2005/12/19 - euros --->
						<cfif structkeyExists(request,"illustrativePriceMethod")>						
							<cfswitch expression="#request.illustrativePriceMethod#">
								<cfcase value="2">
									<cfscript>
										IllustrativePrice = application.com.currency.convert(
											method = #request.IllustrativePriceMethod#,
											fromValue = SPQMargin,
											fromCurrency = request.IllustrativePriceFromCurrency,
											toCurrency = IllustrativePriceToCurrency,
											date = getOppDetails.last_Updated
											);
									</cfscript>
									<cfif IllustrativePrice.toValue lt 0>
										<cfset IllustrativePriceOutput = "(" & IllustrativePrice.toSign & replace(IllustrativePrice.toValue,"-","","one") & ")">
									<cfelse>
										<cfset IllustrativePriceOutput = IllustrativePrice.toSign & IllustrativePrice.toValue>
									</cfif>
									
									<td align="right">#htmleditformat(IllustrativePriceOutput)#</td>
								</cfcase>
							</cfswitch>						
						</cfif>
						<td align="right">#decimalFormat(SPQPercentageMargin)#%</td>
						</cfif>
						<TD>
						<cfif isNumeric(getOppDetails.SPCurrentApprovalLevel) and getOppDetails.oppPricingStatusID eq 8
							and SPapproverLevel eq 10 and shippingComplete eq 0>
								<cf_relayValidatedField	
									fieldName="shipped_#myopportunity.getOppProducts.oppProductID#"
									charType="numeric"
									currentValue="#myopportunity.getOppProducts.productsShipped#"
									helpText="phr_dateFieldHelpText"
								> <CF_INPUT type="checkbox" name="shippingComplete_#oppProductID#" value="#shippingComplete#" checked="#iif(myopportunity.getOppProducts.shippingComplete eq 1,true,false)#">
						<cfelse>	
							#htmleditformat(productsShipped)#
						</cfif>
						</TD>
					</TR>
				</CFLOOP>
				<cfif isNumeric(getOppDetails.SPCurrentApprovalLevel) and getOppDetails.oppPricingStatusID eq 8
					and SPapproverLevel eq 10>
					<tr>
						<td colspan="9" align="right">
							<input type="button" value="Update products shipped" onClick="updateProductsShipped()"><br>
							<input type="button" name="specialPriceRequest" value="Special Price Complete" onClick="saveSpecialPricing('SP_Complete')">
							<input type="hidden" name="frmComments" value="">
						</td>
					</tr>
				</cfif>
			</TABLE>
		</td>
	</tr>
</cfif>
	
<!--- 	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
 --->	
 
<cfif getOppDetails.oppPricingStatusID eq 6>
	<cfset commentsLabelTxt = "Please provide further justification of this price.">
	<cfset commentsButtonTxt = "Send more information">
<cfelse>
	<cfset commentsLabelTxt = "If you require special pricing please give your reasons:">
	<cfset commentsButtonTxt = "Request Special Price">
</cfif>

	<cfif SPapproverLevel eq 10>
		<cfset commentsLabelTxt = "Finance comments:">
		<cfset commentsButtonTxt = "Save commments">
	</cfif>

<cfif isdefined("superAcctManagerIDs")>
	<!--- set the isSuperAM flag to false --->
	<cfset variables.isSuperAM=false>
	<cfoutput>
	<!--- loop through the superAcctManagerIDs stored in oppotunityINI.cfm --->
		<cfloop index="i" list="#superAcctManagerIDs#">
			<!--- see if superAcctManagerID exists in the users usergroups --->
			<cfif listfind(request.relaycurrentuser.usergroups, i) neq 0>
				<!--- if it does then set insert flag to true --->
				<cfset variables.isSuperAM=true>
			</cfif>
		</cfloop>
		
	</cfoutput>
</cfif>
	
<cfif (getOppDetails.vendorAccountManagerPersonID eq request.relayCurrentUser.personid) 
or (SPapproverLevel eq 10 and getOppDetails.oppPricingStatusID neq 7)
or (isdefined("variables.isSuperAM") and variables.isSuperAM is true)>

	<tr>
		<td valign="top" class="label">
			#htmleditformat(commentsLabelTxt)#
		</td>
		<td colspan="2">
		<SELECT NAME="frmSPreason">
			<cfloop QUERY="qOppPricingReason">
				<OPTION VALUE="#OppPricingReasonID#">#htmleditformat(reason)#</OPTION>
			</cfloop>
		</SELECT>
		<!--- ICS 25.10.04 - commented out code which reset currentApprovedLevel to 0 and approvePersonIDs to <NULL> --->
	<!--- <cfif (SPapproverLevel eq 10)> --->
		
		<!--- 12.11.04 ICS - commented out textfield and replaced it with a character limited relay text field --->
		<br><!--- <textarea cols="60" rows="3" name="frmComments" onkeyup="CheckIfFrmCommentsEmpty()"></textarea> --->
		<cf_relayTextField
				maxChars="4000"
				currentValue=""
				thisFormName="frmReasonForm"
				fieldName="frmComments"
				helpText=""
		><input type="button" name="specialPriceRequest" value="#commentsButtonTxt#" onClick="<cfif commentsButtonTxt eq 'Send more information'>saveSpecialPricing('SP_InfoResponse')<cfelseif commentsButtonTxt neq 'Request Special Price'>saveSpecialPricing('SP_AddComment')<cfelseif commentsButtonTxt eq "Request Special Price">saveSpecialPricing('SP_Request')</cfif>">

	<!--- <cfelse>
		<br><textarea cols="60" rows="3" name="frmComments" onkeyup="CheckIfFrmCommentsEmpty()"></textarea><input type="button" name="specialPriceRequest" value="#commentsButtonTxt#" onClick="saveSpecialPricing('SP_Request')">
	</cfif> --->
		
		</td>
	</tr>

	<!--- 2006-06-21 AJC CR_ATI035 Expiry date form field no longer required as this will now be automatic, changed to hidden field --->
	<!--- 
	<tr>
		<td valign="top" class="label">
			Special Price Expiry Date
		</td>
		<td colspan="2">
			<cf_relayDateField	
				divName="uniqueNameforThisDiv"
				currentValue="#getOppDetails.SPExpiryDate#"
				thisFormName="frmReasonForm"
				fieldName="SPExpiryDate"
				anchorName="anchorX"
				helpText="phr_dateFieldHelpText"
			>
		</td>
	</tr>
	--->
	<CF_INPUT type="hidden" name="SPExpiryDate" value="#getOppDetails.SPExpiryDate#">
</cfif>


<!--- CR_ATI019 2005/08/11 GCC Show margin analysis to any approver --->

<cfif getAnyApprover.recordcount gt 0>
	<tr>
		<td valign="top" class="label">SPQ Margin analysis:</td>
		<td colspan="2">
			<TABLE WIDTH="75%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="left" BGCOLOR="White">
				<tr><td align="right" valign="top">Project full price (Total disti sell in list price)</td><td colspan="2" align="left" valign="top">#htmleditformat(myopportunity.currencysign)##DecimalFormat(qOppPricing.fullPrice)#</td></tr>
				<tr><td align="right" valign="top">Requested Discount Special Price</td><td colspan="2" align="left" valign="top">#htmleditformat(myopportunity.currencysign)##DecimalFormat(qOppPricing.discountedSpecialPrice)#</td></tr>
				<cfif qOppPricing.discountedSpecialPrice gt 0><tr><td align="right" valign="top">Percentage discount requested</td><td colspan="2" align="left" valign="top"><cfset percentageDiscount = (1-(#qOppPricing.discountedSpecialPrice# / #qOppPricing.fullPrice#)) * 100>#decimalFormat(percentageDiscount)#%</td></tr></cfif>				
				<tr><td colspan="2" align="right" valign="top">&nbsp;</td><td>Percentage</td></tr>
				<tr><td align="right" valign="top">Total margin normal pricing</td><td align="left" valign="top">#htmleditformat(myopportunity.currencysign)##DecimalFormat(qOppPricing.totalMarginStandardPrice)#</td><td align="left" valign="top"><cfif qOppPricing.discountedSpecialPrice gt 0 and len(qOppPricing.totalMarginStandardPrice) gt 0 and len(qOppPricing.fullPrice)>#decimalFormat((qOppPricing.totalMarginStandardPrice/qOppPricing.fullPrice)*100)#%</cfif></td></tr>
				<tr><td align="right" valign="top">Total margin with Special pricing</td><td align="left" valign="top">#htmleditformat(myopportunity.currencysign)##DecimalFormat(qOppPricing.totalSPMargin)#</td><td align="left" valign="top"><cfif qOppPricing.discountedSpecialPrice gt 0 and len(qOppPricing.totalSPMargin) gt 0 and len(qOppPricing.discountedSpecialPrice) gt 0>#decimalFormat((qOppPricing.totalSPMargin/qOppPricing.discountedSpecialPrice)*100)#%</cfif></td></tr>					
			</TABLE>
		</td>
	</tr>
	<!--- if the currentApproverLevel does not equal SPCurrentApprovalLevel then we should be able to approve this  --->
</cfif>
<cfif getOppDetails.SPCurrentApprovalLevel neq "">
	<cfset NeededApproverLevel = evaluate(getOppDetails.SPCurrentApprovalLevel + 1)>
<cfelse>
	<cfset NeededApproverLevel = 1>
</cfif>

<!--- ICS 25.10.2004 Added OR logic statement in CFIF for multi level 3 approvers --->
<cfif isNumeric(getOppDetails.SPCurrentApprovalLevel) 
	and SPapproverLevel eq NeededApproverLevel and getOppDetails.SPRequiredApprovalLevel neq getOppDetails.SPCurrentApprovalLevel> 
		<tr>
		<td valign="top" class="label">Comments:</td>
		<td>
		<br><!--- <textarea cols="60" rows="3" name="frmComments"></textarea> --->
		<cf_relayTextField
			maxChars="4000"
			currentValue=""
			thisFormName="frmReasonForm"
			fieldName="frmComments"
			helpText=""
			multiple="2"
		>
		</td>
			<td>&nbsp;</td>
		</tr>
	
		<tr>
			<td class="label">Special Pricing Approval</td>
			<td bgcolor="white">
			<input type="button" name="specialPriceRequest" value="Approve Special Price" onClick="saveSpecialPricing('SP_Approve')">
			<input type="button" name="specialPriceRequest" value="More Info Required" onClick="saveSpecialPricing('SP_MoreInfo')">
			<input type="button" name="specialPriceRequest" value="Reject Special Price" onClick="saveSpecialPricing('SP_Reject')">
			</td>
			<td>&nbsp;</td>
		</tr>
	</cfif>

	<cfquery name="checkSPStatusHistory" datasource="#application.sitedatasource#">
		select created	from oppPricingStatusHistory where opportunityID =  <cf_queryparam value="#entityID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</cfquery>
	<cfif checkSPStatusHistory.recordCount gt 0>

	<tr>
		<td colspan="3" align="center">
		
		<iframe src="SPStatusHistory.cfm?opportunityID=#htmleditformat(entityID)#" width="100%" height="180"></iframe>	

		</td>
	</tr>
	</cfif>

</cfform>
</cfoutput>
</table>
<!--- 16.11.2004 ICS commented out file upload check if person is the account manager --->
<!--- <cfif getOppDetails.vendorAccountManagerPersonID eq request.relayCurrentUser.personid>	 --->
	<table border="0" cellspacing="1" cellpadding="3" align="center" class="withBorder">
		<tr>
			<td>
				<cf_relatedFile action="put,list" entity="#url["entityID"]#" entitytype="opportunity">
			</td>
		</tr>
	</table>
<!--- <cfelse>
	<table border="0" cellspacing="1" cellpadding="3" align="center" class="withBorder">
			<tr>
				<td>
					<cf_relatedFile action="list" entity="#url["entityID"]#" entitytype="opportunity">
				</td>
			</tr>
	</table>
</cfif> --->

 

<!--- temporary code for reset as part of CR_ATI031 2/3 level approver - uncomment me after December 1 --->
<cfif structkeyexists(session,"opportunityIDlastDone") and structkeyexists(form,"autoReset")>
	<cflocation url="#request.currentSite.httpProtocol##cgi.server_name#/leadmanager/resetAllActiveSpecialPricing.cfm"addToken="false"> 
</cfif>




</cf_translate>



