<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			oppProductSets.cfm	
Author:				AJC
Date started:		2005-06-08
	
Description:		Product Set functions for opportunities	

Orginal Code:		CR_ATI010

Amendment History:

Date (DD-MMM-YYYY)	Initials	Code	What was changed

Possible enhancements:
 --->
<!--- set local variables --->

<cfparam name="action" default="">
<cfparam name="cboProductSetID" default="">
<cfparam name="opportunityID" default="">
<!--- get product sets --->
<cfscript>
	// Add product set to opportunity
	if(action is "AddToOpportunity")
	{
		application.com.oppProductSets.addProductSetToOpportunity(form.opportunityid,form.cboProductSetID);
		
	}
	// get the product sets
	//qry_getProductSets=application.com.oppProductSets.get();
	qry_getProductSets=application.com.oppProductSets.getProductSetsWithRights();
</cfscript>
<!--- if the product set has been add to the opportunity --->
<cfif action is "AddToOpportunity">
	<script>
		window.opener.location.reload();
		window.close();
	</script>
</cfif>

	<cf_head>
		<cf_title>Add Product Set</cf_title>
		<!--- <LINK REL="stylesheet" HREF="DefaultPartnerStyles.css"> --->
<style>
.popupdivs{
	position:absolute;
	left:0;
	top:197;
	visibility:hidden;
	background-color:#D0D0D0;
}
</style>
<SCRIPT type="text/javascript">
function toggleBox(szDiv, iState) // 1 visible, 0 hidden
{
	var szDivArr = szDiv.split('~');
	var szDivID = szDivArr[0];
	if (szDivID != "") {
		if(document.layers)	   //NN4+
		{
		   document.layers[szDivID].visibility = iState ? "show" : "hide";
		}
		else if(document.getElementById)	  //gecko(NN6) + IE 5+
		{
			var obj = document.getElementById(szDivID);
			obj.style.visibility = iState ? "visible" : "hidden";
		}
		else if(document.all)	// IE 4
		{
			document.all[szDivID].style.visibility = iState ? "visible" : "hidden";
		}
	}
}
function changeDiv(nDiv){
	<cfoutput query="qry_getProductSets">
		toggleBox("#jsStringFormat(ProductSetID)#", 0);
	</cfoutput>
	if (nDiv != 0) {
		toggleBox(nDiv, 1);
	}
}

function eventOppProductEditOnChange( callerObject )
{
  if ( callerObject.name == "frmAddProductSetToOpportunity" && callerObject.value != "" ) touchedSpecialPrice = true;
  callerObject.style["color"] = "red";
  dirtyOppProductsEdit = true;
  document.forms["oppProductForm"].elements["frmSPManualEdit"].value = 1;
  return true;
}

function manageProductSetRights() {
	var functions = document.frmAddProductSetToOpportunity.psFunctions;
	var oppProductSetValue = document.frmAddProductSetToOpportunity.cboProductSetID.value;
	var oppProductSetValueArr = oppProductSetValue.split('~');
	var oppProductSetRights = "";
	if (oppProductSetValueArr.length > 1) {
		oppProductSetRights = oppProductSetValueArr[1];		
		if (oppProductSetRights == 3) {
			functions.options.length = 5;
			functions.options[2].value = "2";
			functions.options[2].text = "Edit selected Product Set";
			functions.options[3].value = "3";
			functions.options[3].text = "Delete selected Product Set";
			functions.options[4].value = "4";
			functions.options[4].text = "Set Product Set users";
		} else if (oppProductSetRights == 2) {
			functions.options.length = 4;
			functions.options[2].value = "2";
			functions.options[2].text = "Edit selected Product Set";
			functions.options[3].value = "5";
			functions.options[3].text = "Remove Product Set from my list";
			functions.options[4] = null;
		} else {
			functions.options.length = 3;
			functions.options[2].value = "5";
			functions.options[2].text = "Remove Product Set from my list";
			functions.options[3] = null;
			functions.options[4] = null;
		}
	} else {
		functions.options[2] = null;
		functions.options[3] = null;
		functions.options[4] = null;
	}
}

function psChange(objValue) {
	var urlToOpen = "";
	var widthAndheight = "width=400,height=500";
	var oppProductSetValue = document.frmAddProductSetToOpportunity.cboProductSetID.value;
	var oppProductSetValueArr = oppProductSetValue.split('~');
	var oppProductSetID = "";
	var oppProductSetRights = "";
	if (oppProductSetValueArr.length > 1) {
		oppProductSetID = oppProductSetValueArr[0];
		oppProductSetRights = oppProductSetValueArr[1];
	}
	<cfoutput>
	switch (objValue) {
		case "1":
			urlToOpen = "oppProductSetActions.cfm?psAction=Create&opportunityid=#opportunityID#&productCatalogueCampaignID=#application.com.settings.getSetting('leadManager.products.productCatalogueCampaignID')#";
			widthAndHeight = "width=750,height=300";
			break;
		case "2":
			if (oppProductSetID != "" && oppProductSetID != 0) {
				urlToOpen = "oppProductSetActions.cfm?psAction=Edit&opportunityid=#opportunityID#&productSetID="+oppProductSetID;
				widthAndHeight = "width=750,height=300";
			} else {
				alert("You must select a product set to edit.");
			}
			break;
		case "3":
			if (oppProductSetID != "" && oppProductSetID != 0) {
				urlToOpen = "oppProductSetActions.cfm?psAction=Delete&productSetID="+oppProductSetID;
			} else {
				alert("You must select a product set to delete.");
			}
			break;
		case "4":
			if (oppProductSetID != "" && oppProductSetID != 0) {
				urlToOpen = "oppProductSetActions.cfm?psAction=Users&productSetID="+oppProductSetID;
				widthAndHeight = "width=500,height=400";
			} else {
				alert("You must select a product set to edit its user rights.");
			}
			break;
		case "5":
			if (oppProductSetID != "" && oppProductSetID != 0) {
				urlToOpen = "oppProductSetActions.cfm?psAction=Remove&productSetID="+oppProductSetID;
				widthAndHeight = "width=500,height=400";
			} else {
				alert("You must select a product set to remove from your list.");
			}
			break;
	}
	</cfoutput>
	if (urlToOpen != "") {
		window.open(urlToOpen,'oppProductSetAction','scrollbars=yes,menubar=no,toolbar=no,resizable=yes,status=yes,'+widthAndHeight);
		//alert(document.frmAddProductSetToOpportunity.cboProductSetID.selectedIndex);
	}
	document.frmAddProductSetToOpportunity.psFunctions.options.selectedIndex = 0;
}

function doSubmit() {
	var theForm = document.frmAddProductSetToOpportunity;
	//alert(theForm.txtforecastShipDate.value);
	if (theForm.cboProductSetID.value == "" || theForm.cboProductSetID.value == 0) {
		alert("You must select a Product Set you would like to add to this opportunity.");
		theForm.cboProductSetID.focus();
	} else if (theForm.quantity.value == "" || isNaN(theForm.quantity.value)) {
		alert("You must specify the quantity of Product Sets you would like to add to this opportunity.");
		theForm.quantity.focus();
	} else if (theForm.txtforecastShipDate.value == undefined || theForm.txtforecastShipDate.value == "") {
		alert("You must specify a date for shipment.");
		theForm.txtForecastShipDate.focus();
	} else if (theForm.quantity.value > 1 && theForm.txtFrequency.value == "") {
		alert("You must specify frequency with multiple drops.");
		theForm.txtFrequency.focus();
	} else if (theForm.quantity.value > 1 && !(theForm.direction[0].checked) && !(theForm.direction[1].checked)) {
		alert("You must specify whether the shipment date is the start or end date with multiple drops.");
	} else {
		theForm.submit();
	}
}
</script>
		</cf_head>
	<cf_body onLoad="manageProductSetRights();changeDiv(document.frmAddProductSetToOpportunity.cboProductSetID.value);">
		<form action="oppProductSetReviewSchedule.cfm" method="post" name="frmAddProductSetToOpportunity">
			<table border="0" height="100%" class="withBorder" width="300"><tr><td valign="top">
			<table border="0" cellspacing="1" cellpadding="3" align="left" bgcolor="White" class="withBorder" width="300">
				<cfoutput>
					<tr>
						<td colspan="2" align="right">
							<select name="psFunctions" onchange="psChange(this.value)" style="font-family:arial;font-size:11">
								<option value="">--- Product Set Options ---</option>
								<option value="1">Create new Product Set</option>
							</select>
						</td>
					</tr>
					<tr>
						<td valign="top" width="1">
							<SELECT style="font-family:arial;font-size:11" NAME="cboProductSetID" onchange="javascript:manageProductSetRights();changeDiv(this.value);"> 
								<option value="0" selected>--- Product Set to ship ---</option>
								<cfloop query="qry_getProductSets">
									<option value="#ProductSetID#~#Permission#">#htmleditformat(ProductSetName)#</option>
								</cfloop>
							</SELECT>
						</td>
						<td valign="top" align="left">No. of drops <input type="text" name="quantity" size="5" value="1"><IMG width="19" SRC="/images/misc/blank.gif" ALIGN="ABSMIDDLE" BORDER="0"></td>
					</tr>
					<tr>
						<td colspan="1" align="left">
							Shipment Date<br>
							<cf_relayDateField	
								thisFormName="frmAddProductSetToOpportunity"
								fieldName="txtforecastShipDate"
								anchorName="anchorX1"
								helpText="phr_dateFieldHelpText"
								>
							<input type="hidden" name="txtforecastShipDate_eurodate" value="">
						</td>
						<td colspan="1" align="left">
							Frequency of shipments<br>
							<select name="txtFrequency">
								<option value="">Please Select</option>
								<option value="Daily">Daily</option>
								<option value="Weekly">Weekly</option>
								<option value="Monthly">Monthly</option>
								<option value="Yearly">Yearly</option>
							</select>
						</td>
					</tr>
					<tr>
						<td colspan="2" align="left">
							<input type="radio" name="direction" value="start" checked> Shipments to start from the date above<br>
							<input type="radio" name="direction" value="end"> Shipments end at the date above
						</td>
					</tr>
					<!--- <tr>
						<td colspan="2" align="center"><a href="##" onclick="window.open('review.htm','review','menubar=no,toolbar=no,resizable=yes,width=300,height=320')">ReviewSchedule</A></td>
					</tr> --->
					<tr>
						<td colspan="2" align="right">
							<input type="button" value="Continue" onClick="doSubmit()">
						</td>
					</tr>
					<CF_INPUT type="hidden" name="opportunityid" value="#opportunityid#">
					<input type="hidden" name="action" value="AddToOpportunity">
				</cfoutput>
			</table>
			</td></tr></table>
		</form>
		<cfoutput query="qry_GetProductSets">
			<cfscript>
				qry_getProductSetItems="";
				qry_getProductSetItems=application.com.oppProductSets.getProductSetItems(productsetID,'');
			</cfscript>
			<div id="#ProductSetID#" class="popupdivs">
				<TABLE border="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" class="withBorder" width="300">
					<tr>
						<th colspan="3">#htmleditformat(ProductSetName)#</th>
					</tr>
					<tr>
						<th>Qty</th><th>SKU</th><th>Description</th>
					</tr>
					<cfloop query="qry_getProductSetItems">
						<tr>
							<td>#htmleditformat(Quantity)#</td><td>#htmleditformat(SKU)#</td><td>#htmleditformat(Description)#</td>
						</tr>
					</cfloop>
				</table>
			</div>
		</cfoutput>
	

