<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			opportunityFunctions.cfm
Author:				KAP
Date started:		2003-09-03
	
Description:			

creates functionList query for tableFromQueryObject custom tag

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
03-Sep-2003			KAP			Initial version

Possible enhancements:


 --->

<cfscript>
comTableFunction = CreateObject( "component", "relay.com.tableFunction" );

comTableFunction.functionName = "Phr_Sys_OpportunityFunctions";
comTableFunction.securityLevel = "";
comTableFunction.url = "";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();

comTableFunction.functionName = "--------------------";
comTableFunction.securityLevel = "";
comTableFunction.url = "";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();

//comTableFunction.functionName = "&nbsp;Opportunity Letter";
//comTableFunction.securityLevel = "";
//comTableFunction.url = "/content/mergeDocs/MSWordLetterTemplate.cfm?personid=2&ignoreExtraneousParameter=";
//comTableFunction.windowFeatures = "width=1024,height=800,toolbar=1,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1";
//comTableFunction.functionListAddRow();

comTableFunction.functionName = "&nbsp;Transfer checked opportunities to a different organisation";
comTableFunction.securityLevel = "";
comTableFunction.url = "/leadManager/moveOppToDiffOrg.cfm?frmOpportunityIDs=";
comTableFunction.windowFeatures = "width=500,height=600,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1";
comTableFunction.functionListAddRow();

comTableFunction.functionName = "&nbsp;phr_Sys_MoveOppsToNewAccountManager";
comTableFunction.securityLevel = "";
comTableFunction.url = "/leadManager/moveOppToDiffAccountManager.cfm?frmOpportunityIDs=";
comTableFunction.windowFeatures = "width=500,height=600,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1";
comTableFunction.functionListAddRow();

// NJH 2011/10/26 - moved as not currently supporting this. Didn't even know it existed!!!
//comTableFunction.functionName = "&nbsp;Merge checked opportunities";
//comTableFunction.securityLevel = "";
//comTableFunction.url = "/leadManager/mergeOpportunities.cfm?frmOpportunityIDs=";
//comTableFunction.windowFeatures = "";
//comTableFunction.functionListAddRow();

// IH 2013-12-17 - Commented out per Michael's request
//comTableFunction.functionName = "Phr_Sys_Tagging";
//comTableFunction.securityLevel = "";
//comTableFunction.url = "";
//comTableFunction.windowFeatures = "";
//comTableFunction.functionListAddRow();

/*comTableFunction.functionName = "&nbsp;Phr_Sys_TagAllOpportunitiesOnPage";
comTableFunction.securityLevel = "";
comTableFunction.url = "javascript:void(checkboxSetAll(true));//";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();

comTableFunction.functionName = "&nbsp;Phr_Sys_UntagAllOpportunitiesOnPage";
comTableFunction.securityLevel = "";
comTableFunction.url = "javascript:void(checkboxSetAll(false));//";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();*/

// YMA 2013/01/06 2013 Roadmap - 108 - Forecast Opportunity
comTableFunction.functionName = "Phr_Sys_report_Forecast";
comTableFunction.securityLevel = "";
comTableFunction.url = "";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();

comTableFunction.functionName = "&nbsp;Phr_Sys_SetTaggedOpportunitiesToForecast";
comTableFunction.securityLevel = "";
comTableFunction.url = "/leadManager/setOppForecast.cfm?action=add&frmOpportunityIDs=";
comTableFunction.windowFeatures = "width=500,height=600,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1";
comTableFunction.functionListAddRow();

comTableFunction.functionName = "&nbsp;Phr_Sys_SetTaggedOpportunitiesToNotForecast";
comTableFunction.securityLevel = "";
comTableFunction.url = "/leadManager/setOppForecast.cfm?action=remove&frmOpportunityIDs=";
comTableFunction.windowFeatures = "width=500,height=600,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1";
comTableFunction.functionListAddRow();
</cfscript>


