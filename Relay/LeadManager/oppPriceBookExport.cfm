<!--- �Relayware. All Rights Reserved 2014 --->
<!---
	oppPriceBookExport.cfm 
	
	This template exports a pricebook to excel for a specified pricebookid
	

	Parameters:
	
	Author: Peter Barron 2010-04-06

Amendment History:


--->

<cfif not application.com.security.confirmFieldsHaveBeenEncrypted("pricebookID,Organisation,Country,Currency")>
      <CF_ABORT>
</cfif>


<!--- NB. there is a similar export from the internal system in pricebookEdit.cfm; any changes here may need doing there too! perhaps they should be merged !!! --->

<cfset organisationName = URL.Organisation>
<cfset organisationName = replace(organisationName,' ','','all')>
<cfset organisationName = replace(organisationName,',','','all')>
<cfset organisationName = replace(organisationName,'(','','all')>
<cfset organisationName = replace(organisationName,')','','all')>
<cfset organisationName = replace(organisationName,'&','','all')>

<cfset countryName = URL.Country>
<cfset countryName = replace(countryName,' ','','all')>
<cfset countryName = replace(countryName,',','','all')>
<cfset countryName = replace(countryName,'(','','all')>
<cfset countryName = replace(countryName,')','','all')>
<cfset countryName = replace(countryName,'&','','all')>

<cfset excelFileName = "Pricebook_#organisationName#_#countryName#_#URL.Currency#.xls">		<!--- i'm not using countryISO cos for "groups" eg Benelux it is set to null; i'm not using countryId cos country is easier to read --->

<cfquery name="getOppPriceBookEntries" datasource="#application.siteDataSource#">
	SELECT     Product.SKU, Product.Description, PricebookEntry.unitPrice
	FROM         PricebookEntry INNER JOIN
	                      Product ON PricebookEntry.productID = Product.ProductID	
	WHERE     (PricebookEntry.PricebookID =  <cf_queryparam value="#url.PricebookID#" CFSQLTYPE="CF_SQL_INTEGER" > )
</cfquery>

<CF_tableFromQueryObject  
	queryObject="#getOppPriceBookEntries#"
	sortOrder = "SKU"
	openAsExcel = "true"
	excelFileName = "#excelFileName#"
	totalTheseColumns=""
	GroupByColumns=""
	ColumnTranslation="true"
    ColumnTranslationPrefix="phr_"
	ColumnHeadingList="SKU, Description, Price"
	showTheseColumns="SKU, Description, unitPrice"
	FilterSelectFieldList=""
	FilterSelectFieldList2=""
	hideTheseColumns=""
	numberFormat=""
	currencyformat="unitPrice"
	dateFormat=""
	showCellColumnHeadings="no" 
	keyColumnURLList=""
	keyColumnKeyList=""
	keyColumnList=""
	keyColumnOpenInWindowList="no"
	OpenWinSettingsList="" 
	useInclude = "false" 
	allowColumnSorting = "false" 
>
