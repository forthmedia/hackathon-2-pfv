<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			specialPricePrint.cfm
Author:				SWJ
Date started:		2004-06-23
	
Description:			

called by oppProductList.cfm as a pop up for special price reasons
this page was orginally specialpriceedit.cfm
Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
15-02-2004			SWJ			Initial version
2004-06-23			psf
2010-01-29			NJH			P-ATI007 - changed checkApproverLevelRequired call so that we could take advantage of over-riding the function

Possible enhancements:


 --->
 
 
 
<cfparam name="message" default="">
<cfparam name="url.description" type="string" default="Unknown product">
<cfparam name="entityID" type="string" default="1">
<cfparam name="form.pagesBack" type="string" default="1">
<cfparam name="form.sortOrder" type="string" default="SKU ASC">

<cfif isDefined("form.specialPricingStatusMethod") and isDefined("form.processProductsShipped") and processProductsShipped eq "">

	<cfswitch expression="#form.specialPricingStatusMethod#">
		<cfcase value="SP_Calculate">
			<cfparam name="desiredDiscount" default="">
			<cfparam name="desiredBudget" default="">
			<cfinvoke component="relay.com.oppSpecialPricing" method="updateSpecialPricing" returnvariable="message">
				<cfinvokeargument name="dataSource" value="#application.siteDataSource#"/>
				<cfinvokeargument name="opportunityID" value="#entityID#"/>
				<cfinvokeargument name="specialPricingStatusMethod" value="#form.specialPricingStatusMethod#"/>
				<cfinvokeargument name="personID" value="#request.relayCurrentUser.personid#"/>
				<cfinvokeargument name="desiredDiscount" value="#desiredDiscount#"/>
				<cfinvokeargument name="desiredBudget" value="#desiredBudget#"/>
				<cfinvokeargument name="SPExpiryDate" value="#SPExpiryDate#"/>
			</cfinvoke>
	
			<cfset message = message>
		</cfcase>
	
		<cfdefaultcase>
			<cfinvoke component="relay.com.oppSpecialPricing" method="progressSpecialPricing" returnvariable="message">
				<cfinvokeargument name="dataSource" value="#application.siteDataSource#"/>
				<cfinvokeargument name="opportunityID" value="#entityID#"/>
				<cfinvokeargument name="specialPricingStatusMethod" value="#form.specialPricingStatusMethod#"/>
				<cfinvokeargument name="personID" value="#request.relayCurrentUser.personid#"/>
				<cfinvokeargument name="comments" value="#frmComments#"/>
				<cfinvokeargument name="SPApproverLevel" value="#SPApproverLevel#"/>
				<cfinvokeargument name="SPApproverLevelRequired" value="#SPApproverLevelRequired#"/>
			</cfinvoke>
		</cfdefaultcase>
	</cfswitch>
<!--- update shipments --->
<cfelseif isDefined("form.processProductsShipped") and processProductsShipped eq "y">
	<cfinvoke component="relay.com.oppSpecialPricing" method="setProductShipping" returnvariable="message">
		<cfinvokeargument name="dataSource" value="#application.siteDataSource#"/>
		<cfinvokeargument name="formFields" value="#form.FieldNames#"/>
		<cfinvokeargument name="personID" value="#request.relayCurrentUser.personid#"/>
		<cfinvokeargument name="opportunityID" value="#entityID#"/>
	</cfinvoke>

</cfif>

<cfquery name="getOppDetails" datasource="#application.siteDataSource#">
	select * from vLeadListingCalculatedBudget where opportunity_ID =  <cf_queryparam value="#entityID#" CFSQLTYPE="CF_SQL_INTEGER" > 
</cfquery>

<cfparam name="SPapproverLevel" default="0">
<cfinvoke component="relay.com.oppSpecialPricing" method="checkApprover" returnvariable="SPApproverLevel">
	<cfinvokeargument name="dataSource" value="#application.siteDataSource#"/>
	<cfinvokeargument name="personID" value="#request.relayCurrentUser.personid#"/>
	<cfinvokeargument name="countryID" value="#getOppDetails.countryID#"/>
</cfinvoke>

<cfif getOppDetails.SPRequiredApprovalLevel neq "" and isNumeric(getOppDetails.SPRequiredApprovalLevel)>
	<cfset SPApproverLevelRequired = getOppDetails.SPRequiredApprovalLevel>
<cfelse>
	<!--- NJH 2010-01-29 P-ATI007 - changed call to that of below so that I could take advantage of over-riding functions. --->
	<!--- <cfinvoke component="relay.com.oppSpecialPricing" method="checkApproverLevelRequired" returnvariable="SPApproverLevelRequired">
		<cfinvokeargument name="dataSource" value="#application.siteDataSource#"/>
		<cfinvokeargument name="opportunityID" value="#entityID#"/>
	</cfinvoke> --->
	
	<cfset SPApproverLevelRequired = application.com.oppSpecialPricing.checkApproverLevelRequired(dataSource=application.siteDataSource,opportunityID=entityID)>
</cfif>

<cfif listLen(SPApproverLevel) gt 1>
	<!--- if this person holds more than one approver levels you need to decide which to use --->
	<cfif listfind(SPapproverLevel, listfirst(SPApproverLevelRequired,"|")) gt 0>
		<!--- first try to use the SPapproverLevelRequired --->
		<cfset thisApproverLevel = listfind(SPapproverLevel, listfirst(SPApproverLevelRequired,"|"))>
	<cfelse>
		<!--- otherwise use the first in the list --->
		<cfset thisApproverLevel = listFirst(SPApproverLevel)>
	</cfif>
<cfelse>
	<cfset thisApproverLevel = SPApproverLevel>
</cfif>


<cfinvoke component="relay.com.oppSpecialPricing" method="getCountryApprovers" returnvariable="SPApprovers">
	<cfinvokeargument name="dataSource" value="#application.siteDataSource#"/>
	<cfinvokeargument name="opportunityID" value="#entityID#"/>
</cfinvoke>

<cfinvoke component="relay.com.oppSpecialPricing" method="getOppPricingReason" returnvariable="qOppPricingReason">
	<cfinvokeargument name="dataSource" value="#application.siteDataSource#"/>
</cfinvoke>

<cfinvoke component="relay.com.oppSpecialPricing" method="getOppPricing" returnvariable="qOppPricing">
	<cfinvokeargument name="dataSource" value="#application.siteDataSource#"/>
	<cfinvokeargument name="opportunityID" value="#entityID#"/>
</cfinvoke>

<cfscript>
// create an instance of the opportunity component
if (fileexists(application.paths.code & "\cftemplates\relayOpportunity.cfc")) {
	componentPath = "code.CFTemplates.relayOpportunity";
	myopportunity = createObject("component",componentPath);}
else {myopportunity = createObject("component","relay.com.opportunity");}
myopportunity.dataSource = application.siteDataSource;
myopportunity.opportunityID = entityID;
myopportunity.productCatalogueCampaignID = application.com.settings.getSetting("leadManager.products.productCatalogueCampaignID");
myopportunity.countryID = getOppDetails.countryID;
myopportunity.sortOrder = form["sortOrder"];
myopportunity.getOppProducts();
</cfscript>







<cfparam name="openAsWord" type="boolean" default="false">
<cfif openAsWord>
	<cfcontent type="application/msword">
	<cfheader name="content-Disposition" value="filename=SpecialPrice.doc">
	<!--- <cfinclude template="../templates/ProformaInvoice.cfm"> --->
<cfelse>
<!--- Phr_Sys_oppProductSpecialPriceRequest --->

	<cf_head>
		<cf_title>Special Price Report</cf_title>
	</cf_head>
	
</cfif>
<cf_translate>
<cfoutput>
<html xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:w="urn:schemas-microsoft-com:office:word"
xmlns:dt="uuid:C2F41010-65B3-11d1-A29F-00AA00C14882"
xmlns="http://www.w3.org/TR/REC-html40">

<cf_head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=ProgId content=Word.Document>
<!-- <link rel=File-List href="ProformaInvioce_files/filelist.xml"> -->

<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:"Arial Black";
	panose-1:2 11 10 4 2 1 2 2 2 4;
	mso-font-charset:0;
	mso-generic-font-family:swiss;
	mso-font-pitch:variable;
	mso-font-signature:647 0 0 0 159 0;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{mso-style-parent:"";
	margin:0in;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:10.0pt;
	mso-bidi-font-size:12.0pt;
	font-family:Arial;
	mso-fareast-font-family:"Times New Roman";
	mso-bidi-font-family:"Times New Roman";
	mso-fareast-language:EN-US;}
@page Section1
	{size:8.5in 11.0in;
	margin:.5in .5in .5in .5in;
	mso-header-margin:.5in;
	mso-footer-margin:.5in;
	mso-paper-source:0;}
div.Section1
	{page:Section1;}
 /* List Definitions */
 @list l0
	{mso-list-id:161624349;
	mso-list-type:hybrid;
	mso-list-template-ids:-1236383228 67698703 67698713 67698715 67698703 67698713 67698715 67698703 67698713 67698715;}
@list l0:level1
	{mso-level-tab-stop:.25in;
	mso-level-number-position:left;
	margin-left:.25in;
	text-indent:-.25in;}
ol
	{margin-bottom:0in;}
ul
	{margin-bottom:0in;}
	
td.specialPricePrint
	{background-color: White;
	text-align: center;}
-->
</style>
</cf_head>



<div class=Section1>

<p class=MsoNormal style='tab-stops:right 7.5in'><cfoutput>#request.CurrentSite.Title#</cfoutput><b><span style='font-size:18.0pt;
mso-bidi-font-size:12.0pt'><span style='mso-tab-count:1'>���������������������������� </span></span></b><span
style='font-size:28.0pt;mso-bidi-font-size:12.0pt;font-family:"Arial Black";
mso-bidi-font-family:Arial;color:gray'>Special Price</span><b><span style='font-size:18.0pt;mso-bidi-font-size:12.0pt'></span></b></p>



<p class=MsoNormal><o:p>&nbsp;</o:p></p>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='border-collapse:collapse;mso-padding-alt:0in 5.4pt 0in 5.4pt'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes'>
  <td width=547 valign=top style='width:5.7in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal>Address</p>
  <p class=MsoNormal>Address</p>
  <p class=MsoNormal>Phone  <span style='mso-no-proof:yes'>Fax</span></p>
  </td>
  <td width=187 valign=top style='width:1.95in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><b>Special Price:</b>&nbsp;<cfoutput>#htmleditformat(getOppDetails.SPApprovalNumber)#</cfoutput></p>
  <p class=MsoNormal><o:p>&nbsp;</o:p></p> 
  </td>
 </tr>
</table>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>
<p class=MsoNormal><span style='mso-bidi-font-family:Arial'><o:p>&nbsp;</o:p></span></p>
<p class=MsoNormal><span style='mso-bidi-font-family:Arial'><o:p>&nbsp;</o:p></span></p>
<p class=MsoNormal><span style='mso-bidi-font-family:Arial'><o:p>&nbsp;</o:p></span></p>
<p class=MsoNormal><b><span style='mso-bidi-font-family:Arial'>Comments or special instructions:</span></b><span style='mso-bidi-font-family:Arial'> <b><o:p></o:p></b></span></p>
<p class=MsoNormal><span style='mso-bidi-font-family:Arial'><o:p>&nbsp;</o:p></span></p>
<p class=MsoNormal><span style='mso-bidi-font-family:Arial'><o:p>&nbsp;</o:p></span></p>

<div align=center>
<!-- 
<table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0 width=737
 style='width:552.6pt;margin-left:9.0pt;border-collapse:collapse;border:none;
 mso-border-alt:solid windowtext .5pt;mso-padding-alt:.05in 5.75pt .05in 5.75pt;
 mso-border-insideh:.5pt solid windowtext;mso-border-insidev:.5pt solid windowtext'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;page-break-inside:avoid'>
  <td width=123 valign=top style='width:92.1pt;border:solid windowtext 1.0pt;
  border-top:solid windowtext 1.5pt;mso-border-alt:solid windowtext .5pt;
  mso-border-top-alt:solid windowtext 1.5pt;padding:.05in 5.75pt .05in 5.75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  style='mso-bidi-font-family:Arial'>SALESPERSON<o:p></o:p></span></b></p>
  </td>
  <td width=123 valign=top style='width:92.1pt;border-top:solid windowtext 1.5pt;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  mso-border-top-alt:solid windowtext 1.5pt;padding:.05in 5.75pt .05in 5.75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  style='mso-bidi-font-family:Arial'>P.O. NUMBER<o:p></o:p></span></b></p>
  </td>
  <td width=123 valign=top style='width:92.1pt;border-top:solid windowtext 1.5pt;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  mso-border-top-alt:solid windowtext 1.5pt;padding:.05in 5.75pt .05in 5.75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  style='mso-bidi-font-family:Arial'>SHIP DATE<o:p></o:p></span></b></p>
  </td>
  <td width=123 valign=top style='width:92.1pt;border-top:solid windowtext 1.5pt;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  mso-border-top-alt:solid windowtext 1.5pt;padding:.05in 5.75pt .05in 5.75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  style='mso-bidi-font-family:Arial'>SHIP VIA<o:p></o:p></span></b></p>
  </td>
  <td width=123 valign=top style='width:92.1pt;border-top:solid windowtext 1.5pt;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  mso-border-top-alt:solid windowtext 1.5pt;padding:.05in 5.75pt .05in 5.75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  style='mso-bidi-font-family:Arial'>F.O.B. POINT<o:p></o:p></span></b></p>
  </td>
  <td width=123 valign=top style='width:92.1pt;border-top:solid windowtext 1.5pt;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  mso-border-top-alt:solid windowtext 1.5pt;padding:.05in 5.75pt .05in 5.75pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  style='mso-bidi-font-family:Arial'>TERMS<o:p></o:p></span></b></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes;
  page-break-inside:avoid'>
  <td width=123 valign=top style='width:92.1pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:.05in 5.75pt .05in 5.75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='mso-bidi-font-family:Arial'><o:p>&nbsp;</o:p></span></p>
  </td>
  <td width=123 valign=top style='width:92.1pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:.05in 5.75pt .05in 5.75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='mso-bidi-font-family:Arial'><o:p>&nbsp;</o:p></span></p>
  </td>
  <td width=123 valign=top style='width:92.1pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:.05in 5.75pt .05in 5.75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='mso-bidi-font-family:Arial'><o:p>&nbsp;</o:p></span></p>
  </td>
  <td width=123 valign=top style='width:92.1pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:.05in 5.75pt .05in 5.75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='mso-bidi-font-family:Arial'><o:p>&nbsp;</o:p></span></p>
  </td>
  <td width=123 valign=top style='width:92.1pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:.05in 5.75pt .05in 5.75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='mso-bidi-font-family:Arial'><o:p>&nbsp;</o:p></span></p>
  </td>
  <td width=123 valign=top style='width:92.1pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:.05in 5.75pt .05in 5.75pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='mso-bidi-font-family:Arial'>Due on receipt<o:p></o:p></span></p>
  </td>
 </tr>
</table>
-->
</div>

<p class=MsoNormal><span style='mso-bidi-font-family:Arial'><o:p>&nbsp;</o:p></span></p>
<p class=MsoNormal><span style='mso-bidi-font-family:Arial'><o:p>&nbsp;</o:p></span></p>

<table width="100%" border="0" cellspacing="1" cellpadding="3" align="center" bgcolor="white">	
	<tr>
		<td colspan="6">Project:&nbsp;<cfoutput>#htmleditformat(getOppDetails.detail)#</cfoutput></td>
	</tr>
	<tr>
		<td class="specialPricePrint">phr_oppProductDescription</Td>
		<td class="specialPricePrint">phr_oppProductSKU</Td>
		<td class="specialPricePrint">phr_oppProductquantity</Td>
		<td class="specialPricePrint">phr_oppProductUnitPrice</Td>
		<td class="specialPricePrint">phr_oppProductSpecialPrice</Td>
		<td class="specialPricePrint">phr_oppProductSubTotal</Td>
	</tr>
	<CFLOOP QUERY="myopportunity.getOppProducts">
	<TR>
		<td align="left">#htmleditformat(description)#</td>
		<td align="center">#htmleditformat(sku)#</td>
		<td align="center">#htmleditformat(quantity)#</td>
		<td align="right">#LSCurrencyFormat(unitprice,"local")#</td>
		<td align="right"><cfif specialPrice neq "">#LSCurrencyFormat(specialPrice,"local")#</cfif></td>
		<td align="right">#LSCurrencyFormat(subtotal,"local")#</td>
	</TR>
	</CFLOOP>
</table>
</cfoutput>
</cf_translate>

<!---  --->






