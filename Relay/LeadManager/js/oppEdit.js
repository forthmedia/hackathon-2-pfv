/* �Relayware. All Rights Reserved 2014 */
/*
oppEdit.js

2012/06/18 PPB P-LEX070 added some _cf_nodebug=true params

2012-09-28 PPB/IH Case 429630 disable the save button after clicking it to prevent 2+ opps being created
2012-10-15 PPB Case 430062 after setting the opp stage to Lost only test for an oppReason if there is a oppReason on the form
2013-01-28 PPB Case 433390 don't try to re-populate the contact select box if it isn't there
2013-04-23 PPB Case 434558 disable the save button before issuing the Form.submit() because dups still occurring  
2014-07-17 PPB Case 439159 added DontClickBack message 
*/

jQuery(document).ready(function() {
	pageOnLoad();
	if (jQuery('#relatedFiles').length > 0) {
		displayRelatedFiles();
	}
});

function getContacts(organisationID) {
	var timeNow = new Date();
	page = '/webservices/relayOpportunityWS.cfc?wsdl&method=getOpportunityEndCustomerContacts&time='+timeNow
	parameters = 'organisationID='+organisationID+'&returnFormat=json&_cf_nodebug=true';
	
	var myAjax = new Ajax.Request(
						page,
						{
							method: 'get', 
							parameters: parameters, 
							evalJSON: 'force',
							debug : false,
							asynchronous: false
						}
					);
	return myAjax.transport.responseText.evalJSON();
}
	
	
function refreshEntityContacts(organisationID,personID) {
	contactPersonIdObj = document.getElementById('contactPersonID');
	
	if (contactPersonIdObj != null) {				//2013-01-28 PPB Case 433390 don't try to re-populate the select box if it isn't there  
	    if (contactPersonIdObj != undefined && contactPersonIdObj.tagName == 'SELECT') {
		    ColdFusion.Bind.assignValue('contactPersonID','value', getContacts(organisationID,personID),{'nullText':phr.opp_PleaseSpecifyMainContact})
	    }
	    
	    /* set the select with the new personID */
	    for (i=0;i<=contactPersonIdObj.options.length-1;i++) {
		    if (contactPersonIdObj.options[i].value == personID) {
			    contactPersonIdObj.options[i].selected=true;
		    }
	    }
	}			//2013-01-28 PPB Case 433390
	
	sponsorPersonIdObj = document.getElementById('sponsorPersonID');
	if (sponsorPersonIdObj != null) {				//2013-01-28 PPB Case 433390 don't try to re-populate the select box if it isn't there
	    currentSponsorID = sponsorPersonIdObj.value;
	    if (sponsorPersonIdObj != undefined && sponsorPersonIdObj.tagName == 'SELECT') {
		    ColdFusion.Bind.assignValue('sponsorPersonID','value', getContacts(organisationID,personID),{'nullText':phr.opp_PleaseSpecifySponsor})
	    }
	    
	    /* we have to reset the current value after the refresh */
	    for (i=0;i<=sponsorPersonIdObj.options.length-1;i++) {
		    if (sponsorPersonIdObj.options[i].value == currentSponsorID) {
			    sponsorPersonIdObj.options[i].selected=true;
		    }
	    }
	}			//2013-01-28 PPB Case 433390
}

function updateOppProductsDiv(opportunityID,readOnly,method) {
	var timeNow = new Date();
	
	if (method == undefined) {
		method = 'getOppProductsDisplay';
	}
	
	page = '/webservices/callWebService.cfc?'
	parameters = 'method=callWebService&webServiceName=relayOpportunityWS&methodName='+method+'&opportunityID='+opportunityID+'&returnFormat=plain&readOnly='+readOnly+'&time='+timeNow+'&_cf_nodebug=true';
	
	div = 'oppProductsDiv';
	var myAjax = new Ajax.Updater(
		div,
		page, 
		{
			method: 'get', 
			parameters: parameters, 
			evalJSON: 'force',
			evalScripts: true,
			debug : false,
			onComplete: function () {
				// 2014-06-17	YMA	Re-initialize responsive table on AJAX content
				jQuery('#opportunityTable').trigger('create');
			}
		});

	
	if (method == 'getOppProductsDisplay') {
		var oppProductCount = getOppProductCount(opportunityID);
					
		specialPriceBtnObj = $('frmApplyForSpecialPricing');
		if (specialPriceBtnObj) {
			if (oppProductCount > 0) {
				specialPriceBtnObj.disabled = false;
			} else {
				specialPriceBtnObj.disabled = true;
			}
		}
		
		placeOrderBtnObj = $('frmPlaceOrder');
		if (placeOrderBtnObj) {
			if (oppProductCount > 0) {
				placeOrderBtnObj.disabled = false;
			} else {
				placeOrderBtnObj.disabled = true;
			}
		}
		
		if (typeof disableDealRegWhenNoProducts != 'undefined') {
			if (disableDealRegWhenNoProducts) {
				dealRegBtnObj = $('frmApplyForDealReg');
				if (dealRegBtnObj) {
					if (oppProductCount > 0) {
						dealRegBtnObj.disabled = false;
					} else {
						dealRegBtnObj.disabled = true;
					}
				}
			}
		}
		
		if (typeof oppStatusTextID != 'undefined') {
			if (oppStatusTextID == 'DealRegApplied' || oppStatusTextID == 'DealRegApproved') {
				dealRegBtnObj = $('frmApplyForDealReg');
				if (dealRegBtnObj) {
					dealRegBtnObj.disabled = true;
				}
			}
		}
		
	}
}

function getOppProductCount(opportunityID) {
	
	page = '/webservices/relayOpportunityWS.cfc?wsdl&method=getOpportunityProductCount';
	parameters = 'opportunityID='+opportunityID+'&returnFormat=json&_cf_nodebug=true';

	var myAjax = new Ajax.Request(
			page,
			{
				method: 'get', 
				parameters: parameters, 
				evalJSON: 'force',
				debug : false,
				asynchronous: false
			}
		);

	return myAjax.transport.responseText.evalJSON();
}

function validateQuantity(productID,quantity) {

	var page = '/webservices/relayOpportunityWS.cfc?wsdl&method=validateQuantityField&returnFormat=json&_cf_nodebug=true';
	var parameters = 'productID='+productID+'&quantity='+quantity;
	
	var myAjax = new Ajax.Request(
		page,
		{
				method: 'get', 
				parameters: parameters, 
				evalJSON: 'force',
				debug : false,
				asynchronous: false
		}
	)
	
	return myAjax.transport.responseText.evalJSON();
}


function addOppProduct(opportunityID,productID,quantity) {

	if (quantity == undefined || quantity == '') {
		alert(phr.oppPleaseEnterValidQuantity);
		return;
	}

	if (validateQuantity(productID,quantity)) {
		var page = '/webservices/relayOpportunityWS.cfc?wsdl&method=insertOppProduct&returnFormat=json&_cf_nodebug=true';
		var parameters = 'productID='+productID+'&opportunityID='+opportunityID+'&quantity='+quantity;
		
		var myAjax = new Ajax.Request(
			page,
			{
					method: 'post', 
					parameters: parameters, 
					evalJSON: 'force',
					debug : false,
					asynchronous:false
			}
		)
		if (myAjax.transport.responseText.evalJSON().ILLEGALDUPLICATE) {
			alert(phr.opp_IllegalDuplicateProduct);
		} else {
			updateOppProductsDiv(opportunityID,false);
		};
	} else {
		alert(phr.oppPleaseEnterValidQuantity);
	}
}


function deleteOppProduct(opportunityID,oppProductID) {

	page = '/webservices/relayOpportunityWS.cfc?wsdl&method=removeOppProduct&_cf_nodebug=true';
	parameters = 'opportunityID='+opportunityID;
	if (oppProductID != undefined) {
		parameters = parameters+'&oppProductID='+oppProductID;
	}
	
	var myAjax = new Ajax.Request(
		page,
		{
				method: 'post', 
				parameters: parameters, 
				evalJSON: 'force',
				debug : false,
				onComplete: '',
				asynchronous:false
		}
	)
	updateOppProductsDiv(opportunityID,false);
}


function placeOrder(encryptedQueryString) {
	if (confirm(phr.opp_PleaseConfirmThatYouWantToPlaceOrder)) {
			openWin('/templates/placeOpportunityOrder.cfm?'+encryptedQueryString,'PlaceOpportunityOrder','width=600,height=400');
	} else {
		return false;
	}
}

function submitOrder(opportunityID) {
	setCurrentTask('PlaceOrder');
	submitOpportunityForm();
}


function ApplyForDealReg() {
	oppForm = document.getElementById('opportunityForm');
	if (isDirty(oppForm)) {
		if (!confirm(phr.opp_DoYouWantToSaveOppChanges)) {
			oppForm.reset();
			/* 2012/11/20	YMA		CASE: 432143 Form still submitted even if user clicked cancel. */
			return(false);
		}
	}
	/* 2012/11/20	YMA		CASE: 432143 Undone  2012-06-08 PPB MIC001/LEX070 and moved deal reg submitted message to occur after form validation takes place. */
	/* 2012-06-08 PPB MIC001/LEX070 now pop up the message and dont pass it thro in the message variable on the returnURL 
	alert(phr.opp_ThankYouYourDealRegApplicationHasBeenSubmitted);*/

	setCurrentTask('ApplyForDealReg');
	submitOpportunityForm();
}

function toggleSPButton(opportunityID) {

	$('spButtonDiv').toggle();
	$('oppButtonDiv').toggle();
	
}

function submitOpportunityForm() {
	if (document.opportunityForm.onsubmit()) {
		document.getElementById("frmSaveOpportunity").disabled=true;	/* 2012-09-28 PPB Case 429630 disable the save button after clicking it to prevent 2+ opps being created */
		document.getElementById('opp_message_DontClickBack').style.display = 'block';	/* 2014-07-17 PPB Case 439159 added DontClickBack message */
		document.opportunityForm.submit();								/* 2013-04-23 PPB Case 434558 disable the save button before issuing the Form.submit() */
	}
}

function setCurrentTask(task) {
	currentTask = document.getElementById('frmTask');
	currentTask.value = task;
}

function showSpecialPricingScreen(opportunityID) {

	saveChanges = false;
	
	if (isDirty(document.opportunityForm)) {
		if (confirm(phr.opp_DoYouWantToSaveOppChanges)) {
			saveChanges = true;
		}
	}
	
	if (saveChanges) {
		setCurrentTask('SaveAndShowSpecialPrice');
		submitOpportunityForm();
	} else {
		document.opportunityForm.reset();
		updateOppProductsDiv(opportunityID,false,'getOppSpecialPricingDisplay');
		toggleSPButton(opportunityID);
	}
}

function verifyOpportunityForm() {
	stageObj = document.getElementById('stageID');
	oppReasonIdObj = document.getElementById('oppReasonID');

	if (typeof(SelAll_) != 'undefined') {
		SelAll_('opportunityForm','UG_')
	}
	
	if (oppReasonIdObj != null) {			/* 2012-10-15 PPB Case 430062 only relevant if there is a oppReason on the form */
		if (stageObj.value == STAGE_LOST && oppReasonIdObj.value == 0) {
		alert(phr.oppPleaseSelectAReason);
		return false;
	}
	}
	
	return true;
}

function pageOnLoad() {
	stageOnChange();	//this is to set the visibility of the reason dropdown as the page loads
}

function stageOnChange() {
	/* 	WAB 2015-06-29 removed complicated code for dealing with visibility 
		but which did not deal with removing the 'required' attributes of hidden fields 
		call new function in fnlFormValidation.js
	*/
	setVisibilityForControlRow ($('oppReasonID'),($('stageID').value == STAGE_LOST)?true:false);

}


function addCrossSellProduct(productGroupId,productModelId,minSeats,maxSeats) {
	populateProductModels(productGroupId,{onCompleteFunction: function () {productModelsPopulated(productModelId,minSeats,maxSeats)}});
}

function productModelsPopulated(productModelId,minSeats,maxSeats) {
	
	$('frmProductModel').value = productModelId;
	
	populateTerm(productModelId,{onCompleteFunction: function () {termPopulated(productModelId,minSeats,maxSeats)}});
}

function termPopulated(productModelId,minSeats,maxSeats) {
	
	$('frmProductSeats').value = minSeats;
	
	$('frmProductSeats').onchange();
}


function editContact (params) {
	jQuery.fancybox.open({	
		"href": params,
		"type": "iframe",
		"helpers": {
	    	"overlay": {
				"locked": true 
			}
		},
		"scrollOutside": true});
}

function displayRelatedFiles() {
	var page = '/webservices/callWebService.cfc'

	jQuery.ajax({
		type: "GET",
		url: page,
		data: {method:'callWebService',webServiceName:'opportunityWS',methodName:'getRelatedFiles',returnFormat:'plain',_cf_nodebug:true,opportunityID:opportunityID,uploadUUID:uploadUUID},
		dataType: "html",
		cache: false,
		success: function(data,textStatus,jqXHR) {
			document.getElementById("relatedFiles").innerHTML = data;
		}
	})

}


/*
 * AHL Product picker select/search functions
 */

function populateProductPickerSearchAndSelect(opportunityID){
	jQuery.ajax(
			{type:'get',
				url:'/webservices/callwebservice.cfc?wsdl&method=callWebService&webserviceName=relayOpportunityWS&methodName=getOppAllProducts&returnFormat=json',
				data: {
					oppID: opportunityID
				},
				dataType:'json',
				evalScripts: true,
				debug : false,
				success: function(data,textStatus,jqXHR) {
					if(data.ISOK){
						var divObj = jQuery('#frmProduct');
						var selectObj = '<option></option>';

						jQuery.each(data.PRODUCTLIST, function(id, productGroup) {
							selectObj += '<optgroup label="'+productGroup.GROUP+'">';

							jQuery.each(productGroup.PRODUCTLIST, function() {
								selectObj += '<option value="'+this.PRODUCTID+'">'+this.DESCRIPTION+' ('+productGroup.GROUP+': '+this.SKU+')</option>';
							});

							selectObj += '</optgroup>';
						});
						divObj.append(selectObj);
						divObj.select2({placeholder: phr.oppPleaseSelectAProduct});
						jQuery("#frmProduct").on("change", function() {
							populateProductDetails(this.value);
						});

					}
					else
					{
						console.log('Error: '+data.ERRMSG+' - '+data.ERRDETAIL);
					}
				},
				error: function(data,textStatus,jqXHR) {
					console.log('Error: webservice call failed due to: '+textStatus);
				  }
			});
}

