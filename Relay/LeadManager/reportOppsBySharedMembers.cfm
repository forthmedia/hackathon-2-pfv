<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
2012-10-04	WAB		Case 430963 Remove Excel Header 
 --->

<cfinclude template="../templates/tableFromQueryHeaderInclude.cfm">
<cfparam name="openAsExcel" type="boolean" default="false">

	<cf_head>
		<cf_title>Pipeline Report</cf_title>
	</cf_head>
	
	
	<cfinclude template="LeadManagerTopHead.cfm">

<!--- 2012-07-23 PPB P-SMA001 commented out
<cfif application.com.settings.getSetting("leadManager.countryScopeOpportunityRecords")>	<!--- 2011-06-01 PPB REL106 use countryScopeOpportunityRecords setting --->
	<cfinclude template="/templates/qryGetCountries.cfm">
</cfif>
--->

<cfparam name="sortOrder" default="opportunity_Name">
<cfparam name="numRowsPerPage" default="50">
<cfparam name="startRow" default="1">
<cfparam name="frm_vendoraccountmanagerpersonid" type="numeric" default="0">

<cfparam name="checkBoxFilterName" type="string" default="">
<cfparam name="checkBoxFilterLabel" type="string" default="">

<cfquery name="getSharedMembers" datasource="#application.SiteDataSource#">
SELECT Country, ISOCode, opportunity_Name, opportunityID, Status, Shared_With, Account_Manager
from vOppBySharedMembers
	WHERE 1=1		
	<!--- 2012-07-23 PPB P-SMA001 commented out 
	<cfif application.com.settings.getSetting("leadManager.countryScopeOpportunityRecords")>	<!--- 2011-06-01 PPB REL106 use countryScopeOpportunityRecords setting --->
	and countryid  in ( <cf_queryparam value="#countryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	</cfif>
 	--->	
	#application.com.rights.getRightsFilterWhereClause(entityType="opportunity",alias="vOppBySharedMembers").whereClause#		<!--- 2012-07-23 PPB P-SMA001 note: countryScopeOpportunityRecords setting checked inside getRightsFilterWhereClause() --->

	<cfif isDefined("frm_vendoraccountmanagerpersonid") and frm_vendoraccountmanagerpersonid neq "0">
		and opportunityid in (select opportunityID from opportunity where vendoraccountmanagerpersonid =  <cf_queryparam value="#frm_vendoraccountmanagerpersonid#" CFSQLTYPE="CF_SQL_INTEGER" > )
	</cfif>
	<cfinclude template="../templates/tableFromQuery-QueryInclude.cfm">
	order by <cf_queryObjectName value="#sortOrder#"> 
</cfquery>

<cfoutput>
<table width="100%" cellpadding="3">
	<tr>
		<td><strong>Opportunities by shared users</strong></td>
		<td align="right">&nbsp;</td>
	</tr>
	<tr><td colspan="2">Opportunities that have been shared with other users</td></tr>
</table>
</cfoutput>

<cf_translate>
<CF_tableFromQueryObject 
	queryObject="#getSharedMembers#"
	queryName="getSharedMembers"
	sortOrder = "#sortOrder#"
	numRowsPerPage="#numRowsPerPage#"
	startRow="#startRow#"
	
	keyColumnList="opportunity_name"
	keyColumnURLList="opportunityEdit.cfm?opportunityid="
	keyColumnKeyList="opportunityid"
	
	showTheseColumns="ACCOUNT_MANAGER,ISOCODE,OPPORTUNITY_NAME,SHARED_WITH,STATUS"
 	hideTheseColumns="opportunityid,Country"
	columnTranslation="true"
	
	FilterSelectFieldList="Country,opportunity_Name,Status,Shared_With,Account_Manager"
	FilterSelectFieldList2="Country,opportunity_Name,Status,Shared_With,Account_Manager"
	GroupByColumns=""
	radioFilterLabel=""
	radioFilterDefault=""
	radioFilterName=""
	radioFilterValues=""
	checkBoxFilterName=""
	checkBoxFilterLabel=""
	allowColumnSorting="yes"
	showCellColumnHeadings="yes"
>
</cf_translate>



