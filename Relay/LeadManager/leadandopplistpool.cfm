<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			LeadandOppListPool.cfm	
Author:				AJC
Date started:		2008-07-01
	
Description:		This is a page displays and allows assignment of Pooled leads to partners
					Original requirement from P-SOP001 (Sophos)

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->

<!--- 
WAB 2009/06/26 Removed As Part of RACE Security, done automatically
<cfinclude template="/leadManager/application .cfm">

<cfinclude template="/leadManager/opportunityParams.cfm">
<cfif fileexists("#application.paths.code#\cftemplates\opportunityINI.cfm")>
	<cfinclude template="/code/cftemplates/opportunityINI.cfm">
</cfif>
--->

<!---used to set the ADD NEW reocrd for Opportunity--->
<cfparam name="showAdd" default="true">

<!--- <cfparam name="url.startRow" default="1"> --->

<cfparam name="sortOrder" default="account">
<cfparam name="numRowsPerPage" default="50">
<cfparam name="filterBy" default="">
<!--- The keyColumn list group of params below control which columns show URL links
		where they link to and what key should be used in the URL --->
<cfparam name="keyColumnList" type="string" default="detail"><!--- this can contain a list of columns that can be edited --->
<cfparam name="keyColumnURLList" type="string" default="#cgi.SCRIPT_NAME#?#request.query_string#&frmTask=edit&opportunityid="><!--- this can contain a list of matching columns that contain the URL of the editor template --->
<cfparam name="keyColumnKeyList" type="string" default="opportunity_ID"><!--- this can contain a list of matching key columns e.g. primary key --->
<cfparam name="keyColumnOpenInWindowList" type="string" default="no">
<cfparam name="OpenWinSettingsList" type="string" default="no">
<cfparam name="dateFormat" type="string" default="last_Updated,Expected_Close_Date"><!--- this list should contain those fields you want in the date format dd-mmm-yy --->
<cfparam name="entityID" type="numeric" default="0">
<cfparam name="partnerLocationID" type="numeric" default="0">
<cfparam name="leadOppPartnerShowTheseColumns" type="string" default="DETAIL,LAST_UPDATED,STAGE">
<cfparam name="columnTranslation" default="false">
<cfparam name="columnTranslationPrefix" default="">

<cfparam name="createOrder" default="false">
<cfparam name="pricingSupport" default="false">
<cfparam name="showRebate" default="false">
<cfparam name="showBudget" default="false">
<cfparam name="showAdd" default="false">
		
<cfif partnerLocationID eq "0">
	<cfquery name="getThisPartnerdetails" datasource="#application.sitedatasource#">
		select personID,organisationID,locationID from person 
		where personID = #request.relayCurrentUser.personid#
	</cfquery>
	<cfset partnerLocationID = getThisPartnerdetails.locationID>
	<cfif isDefined("showAllPartnerLeads") and showAllPartnerLeads eq "yes">
		<cfset partnerSalesPersonID = 0>
	<cfelse>
		<cfset partnerSalesPersonID = getThisPartnerdetails.personID>
	</cfif>
</cfif>

<CFIF isDefined("frmTask") and frmTask eq "edit">
	<cfinclude template="/leadManager/opportunityEdit.cfm">

<cfelse>
	<!--- <cfquery name="test" datasource="" --->
	<cfscript>
	// create an instance of the opportunity component
// call the opportunity component either directly or via a stub in userfiles/content/
if (fileexists(application.paths.code & "\cftemplates\relayOpportunity.cfc")) {
	componentPath = "code.CFTemplates.relayOpportunity";
	myopportunity = createObject("component",componentPath);}
else {myopportunity = createObject("component","relay.com.opportunity");}
	myopportunity.dataSource = application.siteDataSource;
	myopportunity.entityID = entityID;
	myopportunity.opportunityView = application.com.settings.getSetting("leadManager.opportunityView");
	myopportunity.partnerLocationID = partnerLocationID;
	myopportunity.partnerSalesPersonID = partnerSalesPersonID;
	// if form.sortorder is defined use it otherwise set it a default value
	if (isdefined("form.sortOrder") and form.sortOrder neq "") {
		myopportunity.sortOrder = form.sortOrder;} 
	else {myopportunity.sortOrder = "account";}
	myopportunity.listPartnerOpportunities();
	</cfscript>
	
	<CF_Translate>


	
	<cfset currencyFormatList = "CALCULATED_BUDGET">
	<cfset totalTheseColumnsList = "">
	
	<cfif createOrder>
		<cfset leadOppPartnerShowTheseColumns = #leadOppPartnerShowTheseColumns# & ",CREATE_ORDER">
		<cfset keyColumnList = #keyColumnList# & ",CREATE_ORDER">
		<cfset keyColumnURLList = #keyColumnURLList# & ",/orders/addOrderFromOpp.cfm?opportunityID=">
		<cfset keyColumnKeyList = #keyColumnKeyList# & ",opportunity_ID">
		<cfset keyColumnOpenInWindowList = #keyColumnOpenInWindowList# & ",yes">
		<cfset OpenWinSettingsList = #OpenWinSettingsList# & ";width=400,height=150,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=0,resizable=1">
	</cfif>
	
	<cfif pricingSupport>
		<cfset leadOppPartnerShowTheseColumns = #leadOppPartnerShowTheseColumns# & ",PRICING_SUPPORT">
		<cfset keyColumnList = #keyColumnList# & ",PRICING_SUPPORT">
		<cfset keyColumnURLList = #keyColumnURLList# & ",/relayTagTemplates/opportunityPricingSupport.cfm?opportunityID=">
		<cfset keyColumnKeyList = #keyColumnKeyList# & ",opportunity_ID">
		<cfset keyColumnOpenInWindowList = #keyColumnOpenInWindowList# & ",yes">
		<cfset OpenWinSettingsList = #OpenWinSettingsList# & ";width=800,height=500,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1">
	</cfif>
	
	<cfif showBudget>
		<cfset leadOppPartnerShowTheseColumns = #leadOppPartnerShowTheseColumns# & ",CALCULATED_BUDGET">
		<cfset totalTheseColumnsList = #totalTheseColumnsList# & ",calculated_budget">
	</cfif>
	
	<cfif showRebate>
		<cfset leadOppPartnerShowTheseColumns = #leadOppPartnerShowTheseColumns# & ",REBATE">
		<cfset currencyFormatList = #currencyFormatList# & ",rebate">
		<cfset totalTheseColumnsList = #totalTheseColumnsList# & ",rebate">
	</cfif>
	
<!---
Added by Gawain

2007-08-21 : commented by gad, not working for the required results

	<cfif showAdd>
		<cfform method="post" name="FormY"  action="/leadManager/opportunityEdit.cfm">
			<cfset request.relayFormDisplayStyle ="HTML-Form" >
			<cf_relayFormDisplay>
				<CF_relayFormElement  relayFormElementType="button" fieldname="add_new" currentValue="phr_Ext_Add_new" label="" spanCols="No" valueAlign="left" class="button" onclick="document.FormY.submit()"   />
			
			</cf_relayformdisplay>
		</cfform>		
	</cfif> 
--->	
	<cfif myopportunity.qListPartnerOpportunities.recordcount gt 0>
	<CF_tableFromQueryObject 
		queryObject="#myopportunity.qListPartnerOpportunities#"
		sortOrder = "#sortOrder#"
		numRowsPerPage="#numRowsPerPage#"
		
		useInclude = "false"
		
		keyColumnList="#keyColumnList#"
		keyColumnURLList="#keyColumnURLList#"
		keyColumnKeyList="#keyColumnKeyList#"
		keyColumnOpenInWindowList="#keyColumnOpenInWindowList#"
		OpenWinSettingsList="#OpenWinSettingsList#"
		
		showTheseColumns="#leadOppPartnerShowTheseColumns#"
		hideTheseColumns="entityID,vendorAccountManagerPersonID,opportunity_id,partnerSalesPersonID,stageID"
		dateFormat="#dateFormat#"
		allowColumnSorting="yes"
		currencyFormat="#currencyFormatList#"
		columnTranslation="#columnTranslation#"
		columnTranslationPrefix="#columnTranslationPrefix#"
		
		totalTheseColumns="#totalTheseColumnsList#"
		>
		</cfif>
		</CF_Translate>

</CFIF> 

