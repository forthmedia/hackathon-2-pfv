<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			pipelineReport.cfm
Author:				SWJ
Date started:		Apr-2002

Description:		To ptovide a summary of the data in the pipeline

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2007-03-28 SWJ Added in the optypeID filter
2007-06-10	SWJ Changed the way that Sach had set up oppType so that it looped through each oppType to show them as separate tables
2009/02/24	NJH	Bug Fix All Sites Issue 1884 - create keyColumnURLList parameters based on the oppTypeID
2010-07-13	NAS 	CLS002 - added record rights so they will appear in the reports.
2010/08/04	NJH		P-PAN002 - added rollup reporting
2011/06/01 	PPB 	REL106 use countryScopeOpportunityRecords setting
2011/11/28	WAB		LID 3672 Added a special Excel Link so that correct report can be downloaded
					Note that the code relies on the fact that these tfqos are not filtered
2012/03/12 	IH	 	Case 426033 Remove duplicate content-Disposition header and set filenames for Excel
2015/11/27	SB		Bootstrap removed tables
Possible enhancements:
 --->

<cfinclude template="../templates/tableFromQueryHeaderInclude.cfm">
<cfparam name="openAsExcel" type="boolean" default="false">
<cfparam name="sortOrder" default="stageID">
<cfparam name="numRowsPerPage" default="20">
<cfparam name="frm_vendoraccountmanagerpersonid" type="numeric" default="0">
<cfparam name="pipelinesumtotalTheseColumns" type="string" default="This_Subtotal">
<cfparam name="pipelinesumcurrencyFormat" type="string" default="This_Subtotal">
<cfparam name="pipelineSumShowOppStageIDs" type="string" default="No">
<cfparam name="pipelineSumShowTheseOppStageIDs" type="string" default="1,2,3,4,5,7,9,10">

<cfset opportunityView = application.com.settings.getSetting("leadManager.opportunityView")>

<cfset thisQuarter = quarter(now())>
<cf_DateRange type="quarter" value="current" year="#year(now())#">
<cf_CalcWeek CheckDate="#now()#" BeginDay="2" EndDay="6">

<!--- NJH 2016/03/24 - filter by expected close dates --->
<cfset form.dateFilter = "expectedCloseDate">

<cfif not structKeyExists(form,"frmFromDate")>
	<cfset form.frmToDate = request.requestTimeODBC>
	<cfset form.frmFromDate=createODBCDate(dateFormat(StartWeek,"yyyy-mm-dd"))>
</cfif>

<!--- 2007-06-10 SWJ First get oppType --->
<cfquery name="qGetOppTypes" datasource="#application.SiteDataSource#">
	select oppTypeID, oppType from oppType order by oppType
</cfquery>

<!--- Get the user's base currency --->
<cfquery name="current_currency" datasource="#application.SiteDataSource#">
	select countrycurrency from country c where isocode = '#ListFirst(request.relaycurrentuser.countryisocode)#'
</cfquery>

<cfset basecurrency = #ListFirst(current_currency.countrycurrency)#>
<cfset application.com.request.setDocumentH1(text="Summary Pipeline Report")>

<cf_title>Lead|Opportunity List</cf_title>


<script>
function openAsExcel (oppType) {
	conjunction = (window.location.href.indexOf('?') == -1)? '?':'&' ;
	window.location.href = window.location.href + conjunction + 'openAsExcel=true&openAsExcel_oppType=' + oppType
}
</script>

<cfloop query="qGetOppTypes">

	<cfquery name="getPipeline#oppTypeID#" datasource="#application.SiteDataSource#">
		select '#basecurrency#' as currency, status as deal_status, statusSortOrder, stage, stageID,
            sum(calculated_budget * case when fromcountrycurrency <>  <cf_queryparam value="#basecurrency#" CFSQLTYPE="CF_SQL_VARCHAR" >  then er.exchangerate else 1 end) as This_Subtotal,
		sum(overall_customer_Budget) as Subtotal, sum(calculated_budget_this_quarter) as Product_Budget
		from #opportunityView#  left join exchangerate er on  currency = er.fromcountrycurrency and er.tocountrycurrency =  <cf_queryparam value="#basecurrency#" CFSQLTYPE="CF_SQL_VARCHAR" >  and (getdate() >= er.startdate and (er.enddate is NULL or getdate() <= er.enddate))
		where 1=1
		<cfif isDefined("frm_vendoraccountmanagerpersonid") and frm_vendoraccountmanagerpersonid neq "0">
		<!--- and vendorAccountManagerPersonID = #frm_vendoraccountmanagerpersonid# --->
		<!--- 2010-07-13	Start 	NAS 	CLS002 - added record rights so they will appear in the reports. --->
			and (vendorAccountManagerPersonID =  <cf_queryparam value="#frm_vendorAccountManagerPersonID#" CFSQLTYPE="CF_SQL_INTEGER" >
				or opportunity_id in (SELECT recordid FROM recordRights
					WHERE UserGroupID = <Cf_queryparam value="#request.relayCurrentUser.usergroupid#" cfsqltype="cf_sql_integer">
					AND Entity = 'opportunity')
				)
		</cfif>
		<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
			and oppTypeID =  <cf_queryparam value="#oppTypeID#" CFSQLTYPE="CF_SQL_INTEGER" >
			#application.com.rights.getRightsFilterWhereClause(entityType="opportunity",alias="#opportunityView#").whereClause#		<!--- 2012-07-19 PPB P-SMA001 note: setting checked inside getRightsFilterWhereClause() --->
		group by status, statusSortOrder, stage, stageID
		order by stageID
	</cfquery>
</cfloop>
<cfoutput>

<div id="pipelineReportTable">
	<p>Week #htmleditformat(week(now()))# (Week commencing #dateFormat(StartWeek,"dd-mmm-yy")#)</p>
</div>
</cfoutput>

<cfparam name="keyColumnList" type="string" default="deal_status"><!--- this can contain a list of columns that can be edited --->
<cfparam name="keyColumnKeyList" type="string" default=""><!--- this can contain a list of matching key columns e.g. primary key --->

<cfset oppTypeDefined=isDefined("openAsExcel_OppType")>

<cfloop query="qGetOppTypes">

	<cfif !openAsExcel or (openAsExcel and ((oppTypeDefined and openAsExcel_OppType eq oppTypeID) or !oppTypeDefined))>
		<cfquery name="getPipeline#oppTypeID#_currencies" datasource="#application.SiteDataSource#">
			select distinct vl.currency, er.fromcountrycurrency
			from #opportunityView#  vl left join exchangerate er on vl.currency = er.fromcountrycurrency
					and er.tocountrycurrency =  <cf_queryparam value="#basecurrency#" CFSQLTYPE="CF_SQL_VARCHAR" >  and (getdate() >= er.startdate and (er.enddate is NULL or getdate() <= er.enddate))
			where 1=1
			and er.fromcountrycurrency is null
			and currency <>  <cf_queryparam value="#basecurrency#" CFSQLTYPE="CF_SQL_VARCHAR" >
			<cfif isDefined("frm_vendoraccountmanagerpersonid") and frm_vendoraccountmanagerpersonid neq "0">
				and vendorAccountManagerPersonID =  <cf_queryparam value="#frm_vendoraccountmanagerpersonid#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfif>
			<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
			<cfif oppTypeDefined>
				and oppTypeID =  <cf_queryparam value="#oppTypeID#" CFSQLTYPE="CF_SQL_INTEGER" >
			</cfif>
		</cfquery>

		<cfif evaluate('getPipeline#oppTypeID#_currencies.recordcount') gt 0>
			<div id="pipelineReportTable1">
				<cfoutput><p>The above report cannot be displayed as the following currency exchange rates do not exist:</p></cfoutput>
				<!--- if there are currencies that are not converted to the current users currency show them and stop the report --->
				<ul>
					<cfloop query="getPipeline#oppTypeID#_currencies">
							<cfoutput><li>#evaluate('getPipeline#htmleditformat(qGetOppTypes.oppTypeID)#_currencies.currency')# to #htmleditformat(listfirst(current_currency.countrycurrency))#</li></cfoutput>
					</cfloop>
				</ul>
			</div>
			<cfbreak>
	</cfif>
		<!--- this can contain a list of matching columns that contain the URL of the editor template --->
		<cfif isdefined("frm_vendoraccountmanagerpersonid")>
			<cfparam name="keyColumnURLList#qGetOppTypes.oppTypeID#" type="string" default="LeadAndOppList.cfm?filterSelect=status&frmOppTypeID=#qGetOppTypes.oppTypeID#&frm_vendoraccountmanagerpersonid=#frm_vendoraccountmanagerpersonid#&FilterSelectValues=">
		<cfelse>
			<cfparam name="keyColumnURLList#qGetOppTypes.oppTypeID#" type="string" default="LeadAndOppList.cfm?filterSelect=status&frmOppTypeID=#qGetOppTypes.oppTypeID#&FilterSelectValues=">
		</cfif>

		<!--- 2007-06-10 SWJ added the loop to show pipeline per oppType --->
		<cfoutput>
			<h2>
				<a href="javascript:openAsExcel('#oppTypeID#')"><img src="/images/icons/page_excel.png"></a> Pipeline for #htmleditformat(qGetOppTypes.oppType)#
			</h2>
		</cfoutput>

		<cfset thisQueryObject = evaluate("getPipeline#qGetOppTypes.oppTypeID#")>
		<cfset thiskeyColumnURLList = evaluate("keyColumnURLList#qGetOppTypes.oppTypeID#")>  <!--- NJH 2009/02/24 Bug Fix All Sites Issue 1884 --->

		<cfif !oppTypeDefined>
			<cfset excelFileName="Pipeline.xls">
		<cfelse>
			<cfset excelFileName="PipeLine_#qGetOppTypes.oppType#.xls">
		</cfif>

		<CF_tableFromQueryObject
			queryObject="#thisQueryObject#"
			queryName="getPipeline#qGetOppTypes.oppTypeID#"
			HidePageControls="yes"

			sortOrder = "#sortOrder#"
			numRowsPerPage="#numRowsPerPage#"

			keyColumnList="deal_status"
			keyColumnURLList="#thiskeyColumnURLList#"
			keyColumnKeyList="deal_status"

			showTheseColumns="DEAL_STATUS,STAGE,THIS_SUBTOTAL"

			columnTranslation="true"

			totalTheseColumns="This_Subtotal"
			currencyFormat="This_Subtotal"
			rollUpCurrency="true"
			allowColumnSorting="yes"
			openAsExcel = "#openAsExcel#"
			excelFileName = "#excelFileName#"
		>
		<cfif openAsExcel and !oppTypeDefined>
			<cfbreak>
		</cfif>
	</cfif>

</cfloop>