<!--- �Relayware. All Rights Reserved 2014 --->
	<!--- 2012/03/15 PPB AVD001 included from opportunityEdit --->
	<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="frmFileUpload" currentValue="" label="" spanCols="yes">
		<cfoutput>
		<!--- 2014-10-28	RPW	CORE-666 OPP Edit Screen needs multiple XML Changes --->
		<cf_relatedFile action="variables" entity="#opportunityID#" entitytype="opportunity" uploadUUID="#uploadUUID#">

			<p id="relatedFileCountMessage">phr_relFile_filecountMessagePartOne #relatedFileVariables.filelist.recordCount# phr_relFile_filecountMessagePartTwo.</p>

			<ul>
				<cfloop query="relatedFileVariables.filelist">
					<!--- 2014-10-28	RPW	CORE-666 OPP Edit Screen needs multiple XML Changes --->
					<cfscript>
						if (opportunityID) {
							variables.displayFileName = relatedFileVariables.filelist.FileName[relatedFileVariables.filelist.currentRow];
						} else {
							variables.displayFileName = relatedFileVariables.filelist.UploadedFileName[relatedFileVariables.filelist.currentRow];
						}
						variables.filePresent = FileExists(relatedFileVariables.filelist.FileHandle[relatedFileVariables.filelist.currentRow]);
					</cfscript>
					<cfif variables.filePresent>
					<li><A HREF="javascript:void(window.open('#replace(webhandle,"%","%25","ALL")#','UploadFileViewer','width=400,height=400,status,resizable,toolbar=1'))">#htmleditformat(Description)# (#htmleditformat(variables.displayFileName)#)</A></li> <!--- 2015-10-26	ACPK	PROD2015-142 Encode percentage signs in webhandle --->
					</cfif>
				</cfloop>
			</ul>

		<!--- 2014-10-28	RPW	CORE-666 OPP Edit Screen needs multiple XML Changes --->
		<cfset encryptedUrlVariables = application.com.security.encryptQueryString("?entityID=#opportunityID#&entityType=opportunity&replaceallowed=false&uploadUUID=#uploadUUID#")>
		<p><a href="javascript:void(window.open('/screen/entityRelatedFilePopup.cfm#encryptedUrlVariables#','UploadFiles','width=600,height=600,status,resizable,toolbar=0'))" onMouseOver="window.status='';return true" onMouseOut="window.status='';return true">phr_opp_UploadFilesToThisOpportunity</a></p>
		</cfoutput>
	</cf_relayFormElementDisplay>
