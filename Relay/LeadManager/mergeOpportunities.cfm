<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File name:		mergeOpportunitites.cfm
Author:			SWJ
Date created:	2004-01-30

Objective - to merge two or more opportunities.
				Steps involved are:
				a) receive a list of entityID's to merge.
				b) specifiy which is the one to keep.
				c) Do the merge.

Date Tested:
Tested by:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed

Enhancement still to do:



--->

<CFPARAM NAME="frmopportunityIDs" TYPE="string" DEFAULT="0">

<cfset opportunityView = application.com.settings.getSetting("leadManager.opportunityView")>

<CFQUERY NAME="getOppDetails" datasource="#application.siteDataSource#">
	SELECT opportunity_ID,
				Account,
				Stage,
				detail,
				expectedCloseDate as expected_Close_Date,
				overall_customer_budget,
				last_Updated,
				Account_Manager,
				ISOCode,
				region
 			FROM #opportunityView#
			WHERE opportunity_ID  in ( <cf_queryparam value="#frmopportunityIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	order by detail
</CFQUERY>


<!--- *********************************************************************
	  we're doing the merge --->
<cfif isDefined("form.frmOppToKeep")>
	<CFQUERY NAME="updateOppProducts" datasource="#application.siteDataSource#">
		update opportunityProduct
		set opportunityID =  <cf_queryparam value="#form.frmOppToKeep#" CFSQLTYPE="CF_SQL_INTEGER" >
		where opportunityID  in ( <cf_queryparam value="#form.frmopportunityIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	</CFQUERY>

	<cfset oppsToDelete = ListDeleteAt(form["frmopportunityIDs"],listFind(form["frmopportunityIDs"],form["frmOppToKeep"]))>
	<CFQUERY NAME="archiveOpp" datasource="#application.siteDataSource#">
		update opportunity
		set stageID = (select OpportunityStageID from oppStage where OpportunityStage = 'delete')
		where opportunityID  in ( <cf_queryparam value="#oppsToDelete#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	</CFQUERY>
<!--- <cfoutput>oppsToDelete=#oppsToDelete#</cfoutput> --->
	<cflocation url="/leadManager/opportunityEdit.cfm?opportunityid=#form.frmOppToKeep#"addToken="false">
</CFIF>

<cf_head>
	<cf_title>Merge Opportunities</cf_title>

<SCRIPT type="text/javascript">
function doMerge() {
	if (jQuery('input[name="frmOppToKeep"]:checked').length == 0) alert("You must use the radio button to specify which opportunity to keep.");
	else document.oppMergeForm.submit();
}
</script>
</cf_head>


<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
	<TR>
		<TD ALIGN="LEFT" CLASS="Submenu">Merge Opportunities</TD>
		<TD COLSPAN="2" ALIGN="right" CLASS="Submenu">
			<A HREF="javascript:history.go(-1);" Class="Submenu">Back&nbsp;&nbsp;</A>
		</TD>
	</TR>
</TABLE>
<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
<form method="post" name="oppMergeForm" id="oppMergeForm">
	<cfoutput><CF_INPUT type="hidden" name="frmopportunityIDs" value="#frmopportunityIDs#"></cfoutput>
	<tr>
		<TD COLSPAN="6" CLASS="CreamBackground">
			<P>Choose which opportunity you want to keep after the merge by clicking the radio button next to the opportunity. <BR>
			If you want to give the resulting organisation a new name, type it in the box at the bottom of the screen.<BR></P>

			<P>The attributes (e.g. account manager, ID etc.) of the opportunity that you choose to keep,
			will be remembered.  The other opportunity records will be deleted.  All product details will be linked to the remaining
			opportunity and will require you to update them once the merge has been done. </P>
		</TD>
	</tr>

	<TR>
		<TH>Keep this one</th>
		<TH>Account</th>
		<TH>Detail (ID)</TH>
		<TH>Stage</TH>
		<TH>Expected Close Date</TH>
		<th>Last Updated</th>
	</tr>


	<CFOUTPUT query="getOppDetails">
	<TR>
		<TD ALIGN="center" VALIGN="top"><CF_INPUT TYPE="radio" NAME="frmOppToKeep" VALUE="#opportunity_ID#"></TD>
		<TD VALIGN="top">#htmleditformat(Account)# (ISOCode)</TD>
		<TD VALIGN="top"><strong>#detail#</strong> (#htmleditformat(opportunity_ID)#)</TD>
		<TD VALIGN="top">#htmleditformat(Stage)#</TD>
		<TD VALIGN="top">#dateFormat(expected_Close_Date,"dd-mmm-yy")#</TD>
		<td valign="top">#htmleditformat(last_Updated)#</td>
	</tr>
	</cfoutput>

	<tr><td><input type="button" name="PerformMerge" value="Perform Merge" onClick="doMerge()"></td></tr>
</form>
</TABLE>




