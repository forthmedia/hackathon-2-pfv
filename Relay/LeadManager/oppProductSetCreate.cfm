<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			oppProductSetCreate.cfm
Author:				NM
Date started:		02 August 2005

Description:			

- Creation of Product Sets for lead and opportunity. A product set is a grouping of products.

Amendment History:

Date (DD-MMM-YYYY)	Initials	Code	What was changed

2006-01-25 WAB 					Remove form.userType     
2006-06-29      WAB			restrictKeystrokes function altered to handle firefox
2011-11-28		NYB		LHID8224 added translations for "Please select a Product" and "You must specify a quantity""
2011-12-07		NYB		LHID8224b changed the way translations are done in js

Possible enhancements:

--->

<cfset productCatalogueCampaignID = application.com.settings.getSetting("leadManager.products.productCatalogueCampaignID")>

<cfoutput>#productCatalogueCampaignID#</cfoutput>
 
<cfparam name="MakeForecastShipDateMandatory" type="boolean" default="false">
<cfparam name="form.specialPricingStatusMethod" default="">
<cfparam name="form.frmtask" default="">
<cfparam name="url.probability" type="string" default="0">
<cfparam name="url.countryID" type="string" default="0">
<cfparam name="form.probability" type="string" default="0">
<cfparam name="form.frmProductDeletionList" type="string" default="">
<cfparam name="form.frmSPManualEdit" type="string" default="">
<cfparam name="phraseprefix" type="string" default="">

<cf_translate>

<cfif not IsDefined("opportunityID")>
	Cannot load oppProductList without an opportunityID
	<CF_ABORT>
</cfif>

<cfif isDefined("form.specialPricingStatusMethod") and form.specialPricingStatusMethod eq "SP_Calculate" and request.relaycurrentUser.isInternal>
	phr_sys_LoadingMessage
	<cfparam name="desiredDiscount" default="">
	<cfparam name="desiredBudget" default="">
	
	<cfquery Name="getSPExpiryDate" datasource="#application.siteDataSource#">
	SELECT SPExpiryDate	FROM opportunity where opportunityID =  <cf_queryparam value="#opportunityID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</cfquery>
	
	<cfinvoke component="relay.com.oppSpecialPricing" method="updateSpecialPricing" returnvariable="message">
		<cfinvokeargument name="dataSource" value="#application.siteDataSource#"/>
		<cfinvokeargument name="opportunityID" value="#opportunityID#"/>
		<cfinvokeargument name="specialPricingStatusMethod" value="#form.specialPricingStatusMethod#"/>
		<cfinvokeargument name="personID" value="#request.relayCurrentUser.personid#"/>
		<cfinvokeargument name="desiredDiscount" value="#desiredDiscount#"/>
		<cfinvokeargument name="desiredBudget" value="#desiredBudget#"/>
		<cfinvokeargument name="SPExpiryDate" value="#getSPExpiryDate.SPExpiryDate#"/>
	</cfinvoke>

	<cfset message = message>
<cfelse>

	<cfif isDefined("form.specialPricingStatusMethod") and form.specialPricingStatusMethod eq "SP_Calculate" and request.relaycurrentuser.isInternal>
		phr_sys_LoadingMessage
		<cfparam name="desiredDiscount" default="">
		<cfparam name="desiredBudget" default="">
		
		<cfquery Name="getSPExpiryDate" datasource="#application.siteDataSource#">
		SELECT SPExpiryDate	FROM opportunity where opportunityID =  <cf_queryparam value="#opportunityID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfquery>
		
		<cfinvoke component="relay.com.oppSpecialPricing" method="updateSpecialPricing" returnvariable="message">
			<cfinvokeargument name="dataSource" value="#application.siteDataSource#"/>
			<cfinvokeargument name="opportunityID" value="#opportunityID#"/>
			<cfinvokeargument name="specialPricingStatusMethod" value="#form.specialPricingStatusMethod#"/>
			<cfinvokeargument name="personID" value="#request.relayCurrentUser.personid#"/>
			<cfinvokeargument name="desiredDiscount" value="#desiredDiscount#"/>
			<cfinvokeargument name="desiredBudget" value="#desiredBudget#"/>
			<cfinvokeargument name="SPExpiryDate" value="#getSPExpiryDate.SPExpiryDate#"/>
		</cfinvoke>
	
		<cfset message = message>
	</cfif>

<cfparam name="url.opportunityID" type="string" default="0">

<cfscript>
// create an instance of the opportunity component
if (fileexists(application.paths.code & "\cftemplates\relayOpportunity.cfc")) {
	componentPath = "code.CFTemplates.relayOpportunity";
	myopportunity = createObject("component",componentPath);}
else {myopportunity = createObject("component","relay.com.opportunity");}
myopportunity.dataSource = application.siteDataSource;
myopportunity.opportunityID = opportunityID;
myopportunity.productCatalogueCampaignID = productCatalogueCampaignID;
myopportunity.countryID = url["countryID"];
myopportunity.sortOrder = form["sortOrder"];
</cfscript>

<cfif isdefined("form.frmTask") and form.frmtask eq "addproduct">

<!--- this checks to see if a product needs adding --->
	<cfscript>
	myopportunity.quantity = FORM.quantity;
	myopportunity.productID = FORM.productID;
	myopportunity.createdBy = request.relayCurrentUser.usergroupid;
	myopportunity.lastUpdatedBy = request.relayCurrentUser.usergroupid;
//	if ( form["forecastShipDate"] neq "" )
//		myopportunity.forecastShipDate = CreateDate( left( form["forecastShipDate"], 4 ), mid( form["forecastShipDate"], 6, 2 ), right( form["forecastShipDate"], 2 ) );
//	else
//		myopportunity.forecastShipDate = "";
	myopportunity.forecastShipDate = form["forecastShipDate"] ;
	myopportunity.probability = form["probability"];
	myopportunity.specialPrice = form["specialPrice"];
	myopportunity.stockLocation = form["frmStockLocation"];
	myopportunity.SPManualEdit = form["frmSPManualEdit"];
	myopportunity.addOppProduct();
	if(myopportunity.specialPrice neq ""){
	myopportunity.SuperAMAssign();
	}
	// trigger process action if special price has been requested
	//if ( form["frmSpecialPriceReason"] neq "" )
	//{
	//	myopportunity.OppProductSpecialPriceReason = form["frmSpecialPriceReason"];
	//	myopportunity.OppProductSpecialPriceRequest();
	//}
	</cfscript>
<!--- --------------------------------------------------------------------- --->
<!--- nasty client-side redirect to stop form post on frame refresh --->
<!--- --------------------------------------------------------------------- --->

	<cflocation url="#cgi["script_name"]#?#request.query_string#" addToken="false">
	<CF_ABORT>

<!--- --------------------------------------------------------------------- --->
</cfif>

<!--- <cfif isdefined("form.frmTask") and form.frmtask eq "removeProduct">
	<cfloop index="deleteID" list="#form["frmProductDeletionList"]#">
		<cfscript>
		myopportunity.oppProductID = deleteID;
		myopportunity.removeOppProduct();
		myopportunity.oppProductID = "";
		</cfscript>
	</cfloop>
</cfif> --->

<cfif isdefined("form.frmTask") and form.frmtask eq "editProduct">

	<cfscript>
	myopportunity.quantity = form["frmQuantityEdit"];
	myopportunity.oppProductID = form["frmOppProductIDEdit"];
	myopportunity.lastUpdatedBy = request.relayCurrentUser.usergroupid;
//	if ( form["frmForecastShipDateEdit"] neq "" )
//		myopportunity.forecastShipDate = CreateDate( mid( form["frmForecastShipDateEdit"], 7, 4 ), mid( form["frmForecastShipDateEdit"], 4, 2 ), left( form["frmForecastShipDateEdit"], 2 ) );
//	else
//		myopportunity.forecastShipDate = "";
	myopportunity.forecastShipDate =form["frmForecastShipDateEdit"] ; 
	myopportunity.probability = form["frmProbabilityEdit"];
	myopportunity.specialPrice = form["frmSpecialPriceEdit"];
	myopportunity.stockLocation = form["frmStockLocationEdit"];
	myopportunity.SPManualEdit = form["frmSPManualEdit"];
	myopportunity.updateOppProduct();
	if(myopportunity.specialPrice neq ""){
	myopportunity.SuperAMAssign();
	}
	// trigger process action if special price has been requested
	//if ( form["frmSpecialPriceReason"] neq "" )
	//{
	//	myopportunity.OppProductSpecialPriceReason = form["frmSpecialPriceReason"];
	//	myopportunity.OppProductSpecialPriceRequest();
	//}
	myopportunity.oppProductID = "";
	</cfscript>
	
</cfif>

<cfscript>
	myopportunity.getOppProducts();
	myopportunity.get();
// probability selects
	myopportunity.OppProductProbabilitySelectName = "frmProbabilityEdit";
	myopportunity.OppProductProbabilitySelectDefault = url["probability"];
	myopportunity.OppProductProbabilitySelectOnchange = "eventOppProductEditOnChange(this);";
	myopportunity.OppProductProbability();
	OppProductProbabilitySelectEdit = myopportunity.OppProductProbabilitySelect;

	myopportunity.OppProductProbabilitySelectName = "Probability";
	myopportunity.OppProductProbabilitySelectDefault = url["probability"];
	myopportunity.OppProductProbabilitySelectOnchange = "";
	myopportunity.OppProductProbability();
	OppProductProbabilitySelectAdd = myopportunity.OppProductProbabilitySelect;
</cfscript>
<cfif myopportunity.OppTypeID is 2>
	<cfset showAddProduct=false>
	<cfset showDuplicate=false>
	<cfset useOppProductShipDate=false>
	<cfset useOppProductProbability=false>
	<cfset showIllustrativePrice=true>
	<cfif myopportunity.stageID is 12>
		<cfset showEditOppProduct=true>
		<cfset showCalcOppProduct=true>
	<cfelse>
		<cfset showEditOppProduct=false>
		<cfset showCalcOppProduct=false>
	</cfif>
<cfelse>
	<cfset showAddProduct=true>
	<cfset showDuplicate=true>
	<cfset useOppProductShipDate=true>
	<cfset useOppProductProbability=true>
	<cfset showIllustrativePrice=false>
	<cfset showEditOppProduct=true>
	<cfset showCalcOppProduct=true>
</cfif>

<cfinclude template="/templates/relayFormJavaScripts.cfm">
<cfif showIllustrativePrice>
	<cfscript>
		toCurrency = application.com.relayEcommerce.getCurrencies(request.relaycurrentuser.countryID);
	</cfscript>
</cfif>

<cf_translate encode="javascript">
<SCRIPT type="text/javascript">
	function addproduct() {
		var errorMessage = "";
		if ( document.forms["oppProductForm"]["productGroupID"].selectedIndex == 0 || document.forms["oppProductForm"]["thisProductID"].selectedIndex == 0 )
		{
			errorMessage = "phr_opp_PleaseSelectAProduct";
		}
		else if ( !(document.oppProductForm.quantity.value > 0) )
		{
			errorMessage = "phr_opp_SpecifyAQuantity";
		}
		else if ( document.oppProductForm.quantity.value > 0 )
		{
//			errorMessage = verifyInteger( document.oppProductForm.quantity.value, "number", 2 );
		}

		<cfif MakeForecastShipDateMandatory>
			if ( document.forms["oppProductForm"]["forecastShipDate"].value == "" )
			{
				errorMessage = "phr_opp_shipDateIsRequired";
			}
		</cfif>

		if ( errorMessage == "" )
		{
			document.oppProductForm.productID.value = document.oppProductForm.thisProductID.value;
			document.oppProductForm.frmTask.value = "addproduct";
			document.oppProductForm.submit();
			// SWJ commented out the two lines below so that the approval screen is not popped up here.
			// it needs to be done in the opportunity edit place.
			//if ( document.forms["oppProductForm"]["specialPrice"].value == "" ) document.oppProductForm.submit()
			//else specialPriceReasonWindow( document.forms["oppProductForm"]["thisProductID"][document.forms["oppProductForm"]["thisProductID"].selectedIndex].text );
		} else {
			alert( errorMessage );
		}
	}
	<!--- 2005/06/07 AJC START:CR_ATI010 - Product Sets --->
	function addproductset() {
		<cfoutput>window.open('oppProductSet.cfm?opportunityid=#jsStringFormat(myopportunity.opportunityID)#','oppProductSet','menubar=no,toolbar=no,resizable=yes,width=300,height=500');</cfoutput>
	}
	<!--- 2005/06/07 AJC END:CR_ATI010 - Product Sets --->
	
<!--- ICS 26.10.04 - addmultipleproducts added - copy of addProducts with some tweaks --->
	function addmultipleproducts() {
		var errorMessage = "";
		if ( document.forms["oppProductForm"]["productGroupID"].selectedIndex == 0 || document.forms["oppProductForm"]["thisProductID"].selectedIndex == 0 )
		{
			errorMessage = "phr_opp_PleaseSelectAProduct";
		}

		if ( errorMessage == "" )
		{
			productID = document.oppProductForm.thisProductID.value;
			opportunityID = document.oppProductForm.opportunityID.value;
			openWin( 'oppProductAddMultipleProducts.cfm?oppProductID=' + productID + '&opportunityID=' + opportunityID + '&probability=25' ,'PopUp', 'left=' + parseInt(screen.availWidth * 0.2)  + ', width=450, top=' + parseInt(screen.availHeight * 0.1)  + ', height=' + parseInt(screen.availHeight * 0.7) + ', toolbar=0, location=0, directories=0, status=1, menuBar=0, scrollBars=1, resizable=0'); 
		} else {
			alert( errorMessage );
		}
	}
	
/*
	function removeProduct(oppProductID) {
		if (oppProductID > 0) {
			document.oppProductForm.oppProductID.value = oppProductID;
			document.oppProductForm.frmTask.value = "removeProduct";
			document.oppProductForm.submit();
		} else {
		alert("You cannot remove this product");
		}
	}
*/
function saveSpecialPricing(action)
{
	//alert(action);
  document.frmReasonForm.specialPricingStatusMethod.value = action;
  var doThis = "yes";
  //alert(document.frmReasonForm.specialPricingStatusMethod.value);
  if (action == "SP_Calculate")
  	{
	document.frmReasonForm.frmComments.value = "Budget changed to " + document.frmReasonForm.newBudget.value
	}
  if (doThis == "yes" )
  	document.frmReasonForm.submit();
}


function calcMyDiscount() {
	document.frmReasonForm.SP_SaveBudget.disabled = false;
	document.frmReasonForm.newBudget.value = document.frmReasonForm.desiredBudget.value;
	//document.frmReasonForm.desiredDiscount.disabled = true;
	document.frmReasonForm.desiredDiscount.value = '';
	var captionElement = document.getElementById("resultText");
	captionElement.innerHTML = "Resulting percentage discount";
	if (document.frmReasonForm.fullPrice.value > 0)
	{
	document.frmReasonForm.newBudget.value =  
		(eval(Math.round(100 - ((((document.frmReasonForm.newBudget.value / document.frmReasonForm.fullPrice.value) * 100)*100)/100)))).toFixed(2);
}
	else
	{
		alert("Project full price cannot be reduced below 0.");
	}
}
  
function calcMyBudget() {
	document.frmReasonForm.newBudget.value = 
		(eval(Math.round( document.frmReasonForm.fullPrice.value * ((100-document.frmReasonForm.desiredDiscount.value) / 100) ))).toFixed(2);
	document.frmReasonForm.SP_SaveBudget.disabled = false;
	//document.frmReasonForm.desiredBudget.disabled = true;
	document.frmReasonForm.desiredBudget.value = '';
	var captionElement = document.getElementById("resultText");
	captionElement.innerHTML = "Resulting budget value";
}

function resetCalculator(){
	document.frmReasonForm.SP_SaveBudget.disabled = true;
	document.frmReasonForm.desiredBudget.disabled = false;
	document.frmReasonForm.desiredDiscount.disabled = false;
	document.frmReasonForm.calcDiscount.disabled = true;
	document.frmReasonForm.calcBudget.disabled=true;
	document.frmReasonForm.desiredBudget.value = '';
	document.frmReasonForm.desiredDiscount.value = '';
	document.frmReasonForm.newBudget.value = '';
	eventOppProductEditClear();
}

function CheckIfDesiredBudgetEmpty(){
	if (document.frmReasonForm.desiredBudget.value == "")
		document.frmReasonForm.calcDiscount.disabled=true
	else
		{
		document.frmReasonForm.calcDiscount.disabled=false;
		//document.frmReasonForm.desiredDiscount.disabled = true;
		}
}
		
function CheckIfDesiredDiscountEmpty(){
	if (document.frmReasonForm.desiredDiscount.value == "")
		document.frmReasonForm.calcBudget.disabled=true
	else
		document.frmReasonForm.calcBudget.disabled=false
}

function CheckIfFrmCommentsEmpty(){
	if (document.frmReasonForm.frmComments.value == "")
		document.frmReasonForm.specilPriceRequest.disabled=true
	else
		document.frmReasonForm.specilPriceRequest.disabled=false
}

function calcLineItemDiscount(thisObject) {
	var myForm = thisObject.form
	myForm.frmSpecialPriceEdit.value =   myForm.frmUnitPriceEdit.value * ((100-myForm.reduceByPerc.value) / 100);
}

function refreshParentLocation(thisMethod) {
   if (thisMethod != '') self.parent.location.href = window.parent.location;
}

//alert(window.parent.location)
</script>
</cf_translate>

<cfinvoke component="relay.com.oppSpecialPricing" method="getOppPricing" returnvariable="qOppPricing">
	<cfinvokeargument name="dataSource" value="#application.siteDataSource#"/>
	<cfinvokeargument name="opportunityID" value="#opportunityID#"/>
</cfinvoke>

<cfif IsQuery(myopportunity.getOppProducts)> 
<table border="0" cellspacing="0" cellpadding="0" align="left" bgcolor="White" class="withBorder" width="100%">
<tr><td>
	<table border="0" cellspacing="1" cellpadding="3" align="left" bgcolor="White" class="withBorder">

	<tr><td colspan="10" valign="top" align="right">
	<cfif useOppFunctions>
		<CFINCLUDE template="oppProductViewPopup.cfm">
	<cfelse>
		&nbsp;
	</cfif>
	</td></tr>

	<tr>
	<cfif useOppFunctions>
	<th>	
	</cfif>
		<cfoutput>
<form name="oppProductForm" method="post" enctype="multipart/form-data">
	<input type="hidden" name="productID" value="0">
	<input type="hidden" name="oppProductID" value="0">
	<input type="hidden" name="frmTask" value="0">
	<input type="hidden" name="frmSPManualEdit" value="">
	<CFIF IsDefined("sortOrder")>
		<CF_INPUT Name="sortOrder" Type="Hidden" Value="#sortOrder#">
	</CFIF>
		<!--- GCC No value needed in this instance but preserved for compatability--->
		<input type="hidden" name="frmAllCheckIDs" value="">
		<input type="hidden" name="frmAction" value="">

<!--- ==============================================================================
       oppProduct Table Headings         
=============================================================================== --->
		
		<CF_INPUT type="hidden" name="opportunityID" value="#opportunityID#">
		</cfoutput>
	<cfif useOppFunctions>
	</th>
	</cfif>	
	<cfoutput>
				<th>
					<cfif myopportunity.getOppProducts.recordCount neq 0>
					<cfswitch expression="#sortOrder#">
						<cfcase value="SKU asc">
							<a href="javaScript:reSort( 'SKU DESC' );" style="text-decoration: none;" title="Sort Z-A">phr_oppProductSKU</a>
							<img src="/images/misc/iconSortDesc.gif" border="0" alt="Sort Z-A">
						</cfcase>
						<cfcase value="SKU desc">
							<a href="javaScript:reSort( 'SKU ASC' );" style="text-decoration: none;" title="Sort A-Z">phr_oppProductSKU</a>
							<img src="/images/misc/iconSortAsc.gif" border="0" alt="Sort A-Z">
						</cfcase>
						<cfdefaultcase>
							<a href="javaScript:reSort( 'SKU ASC' );" style="text-decoration: none;" title="Sort A-Z">phr_oppProductSKU</a>
						</cfdefaultcase>
					</cfswitch>
					<cfelse>&nbsp;</cfif>
				</th>
				<th nowrap>phr_oppProductquantity</th>
				<cfif useOppProductShipDate>
					<th nowrap>
						<cfswitch expression="#sortOrder#">
							<cfcase value="forecastShipDate asc">
								<a href="javaScript:reSort( 'forecastShipDate DESC' );" style="text-decoration: none;" title="Sort Z-A">phr_oppProductShipDate</a>
								<img src="/images/misc/iconSortDesc.gif" border="0" alt="Sort Z-A">
							</cfcase>
							<cfcase value="forecastShipDate desc">
								<a href="javaScript:reSort( 'forecastShipDate ASC' );" style="text-decoration: none;" title="Sort A-Z">phr_oppProductShipDate</a>
								<img src="/images/misc/iconSortAsc.gif" border="0" alt="Sort A-Z">
							</cfcase>
							<cfdefaultcase>
								<a href="javaScript:reSort( 'forecastShipDate ASC' );" style="text-decoration: none;" title="Sort A-Z">phr_oppProductShipDate</a>
							</cfdefaultcase>
						</cfswitch>
					</th>
				</cfif>
				<cfif useOppProductProbability>
					<th nowrap>
						<cfswitch expression="#sortOrder#">
							<cfcase value="probability asc">
								<a href="javaScript:reSort( 'probability DESC' );" style="text-decoration: none;" title="Sort Z-A">phr_oppProductProbability</a>
								<img src="/images/misc/iconSortDesc.gif" border="0" alt="Sort Z-A">
							</cfcase>
							<cfcase value="probability desc">
								<a href="javaScript:reSort( 'probability ASC' );" style="text-decoration: none;" title="Sort A-Z">phr_oppProductProbability</a>
								<img src="/images/misc/iconSortAsc.gif" border="0" alt="Sort A-Z">
							</cfcase>
							<cfdefaultcase>
								<a href="javaScript:reSort( 'probability ASC' );" style="text-decoration: none;" title="Sort A-Z">phr_oppProductProbability</a>
							</cfdefaultcase>
						</cfswitch>
					</th>
				</cfif>
				<cfif myopportunity.getOppProducts.recordCount neq 0>
				<th nowrap>phr_oppProductUnitPrice</th>
				</cfif>					
				<cfif useOppProductPercentDiscount><th>phr_oppProductPercentDiscount</th></cfif>
				<cfif useOppProductStockLocation><th>phr_oppProductStockLocation</th></cfif>
				<cfif useOppProductSpecialPricing><th>phr_oppProductSpecialPrice</th></cfif>
				<cfif myopportunity.getOppProducts.recordCount neq 0>
					<th nowrap>phr_oppProductSubTotal</th>
					<cfif showIllustrativePrice><th>phr_#htmleditformat(phraseprefix)#oppIllustrativePrice *</th></cfif>
					<cfif showEditOppProduct><th nowrap>phr_oppProductEdit</th></cfif>
					<!--- <th nowrap><a href="javascript:void(eventDeleteOnClick());">phr_oppProductRemoveProduct</a></th> --->
				<cfelse>
					<th colspan="4" nowrap>&nbsp;</th>
				</cfif>
			</tr>
		</cfoutput>

	<cfif myopportunity.getOppProducts.recordCount gt 0>
<!--- ==============================================================================
       Existing oppProduct lines         
=============================================================================== --->
		<cfoutput query="myopportunity.getOppProducts">
			<TR <CFIF CurrentRow MOD 2 IS NOT 0>class="oddrow"<CFELSE>class="evenRow"</CFIF>> 
				<!--- <td>#description#</td> --->
				<cfif useOppFunctions>
				<td><INPUT TYPE="checkbox" NAME="frmProductCheck" VALUE="#oppProductID#" <cfif isDefined("frmProductCheck")><cfif ListContains(frmProductCheck,oppProductID)>checked</cfif></cfif>></td>
				</cfif>
				<td>#htmleditformat(sku)#</td>
				<td>#htmleditformat(quantity)#</td>
				<cfif useOppProductShipDate><td nowrap>#iif( IsDate( forecastShipDate ), 'DateFormat( forecastShipDate, "dd-mmm-yyyy" )', de( "" ) )#</td></cfif>
				<cfif useOppProductProbability><td>#htmleditformat(Probability)#</td></cfif>
				<td>
					<cfif showIllustrativePrice>
						#decimalformat(unitPrice)# (#htmleditformat(trim(myopportunity.Currency))#)
					<cfelse>
						#LSCurrencyFormat(unitPrice,"local")#
					</cfif>
				</td>
				<cfif useOppProductSpecialPricing>
				<!--- promotional or special price --->
						<cfset displayPrice = "">
						<cfset informationMessage = "">
						<cfset specialPriceUnapproved = false>
						<cfset informationImage = "/images/MISC/iconInformation.gif">
						<cfif productPromotionID neq "">
							<cfset displayPrice = promotionalPrice>
							<cfset informationMessage = "Promotional Price">
						<cfelseif specialPrice neq "">
							<cfset displayPrice = specialPrice>
							<cfif specialPriceApproverPersonID neq "">
								<cfset informationMessage = "Approved Special Price">
							<cfelse>
								<cfset specialPriceUnapproved = true>
								<cfset informationMessage = "Unapproved Special Price">
								<cfset informationImage = "/images/MISC/iconInformationRed.gif">
							</cfif>
						</cfif>
				</cfif>
				<cfif useOppProductPercentDiscount>
					<td>
					<!--- JC: Added unitprice trap to prevent div by zero --->
					<cfif displayPrice neq "" AND unitprice NEQ 0>
						<cfset percentDiscount = DecimalFormat( round(100-((displayPrice / unitprice)*100)))>#percentDiscount#%
					<cfelse>&nbsp;
					</cfif>
					</td>
				</cfif>
				<cfif useOppProductStockLocation><td>#StockLocation#&nbsp;</td></cfif>
				<cfif useOppProductSpecialPricing>
					<td align="right">
						
						<!--- <a title="" style="text-decoration: none; font-weight: normal; color: black; cursor: hand;">#specialPrice#</a> --->
						<!--- <cfif informationMessage neq "">
<!--- ********************************************************************* --->
<!--- need to check that browser has approval rights --->
<!--- ********************************************************************* --->
							<cfif permissionSpecialPriceApproval and specialPriceUnapproved><a href="javascript:void(specialPriceApprovalWindow(this,'#JSStringFormat( myOpportunity.getOppProducts.oppProductID )#'));"></cfif>
							<img src="#informationImage#" alt="#informationMessage#" width="14" height="14" border="0">
							<cfif permissionSpecialPriceApproval and specialPriceUnapproved></a></cfif>
						</cfif> --->
						<cfif displayPrice neq "">
							<cfif showIllustrativePrice>
								#decimalformat(displayPrice)# <strong>(#htmleditformat(trim(myopportunity.Currency))#)</strong>
							<cfelse>
								#LSCurrencyFormat(displayPrice,"local")#
							</cfif>
						<cfelse>
							&nbsp;
						</cfif>
						<!--- <img src="/images/MISC/iconWarning.gif" alt="" width="14" height="14" border="0"> --->
					</td>
				</cfif>
				<td align="right">
					<cfif showIllustrativePrice>
						#decimalformat(subtotal)# <strong>(#htmleditformat(trim(myopportunity.Currency))#)
					<cfelse>
						#LSCurrencyFormat(subtotal,"local")#
					</cfif>
				</td>
				<cfif showIllustrativePrice>
					<cfscript>
						res = application.com.currency.convert(fromValue="#trim(subtotal)#", fromCurrency="#trim(myopportunity.Currency)#", toCurrency="#trim(toCurrency.countrycurrency)#");
					</cfscript>
					<td align="right">#decimalformat(res.tovalue)# <strong>(#htmleditformat(toCurrency.countrycurrency)#)</strong></td>
				</cfif>
				<cfif showEditOppProduct>
					<td align="center"><CF_INPUT type="radio" name="frmEditOppProduct" value="#myopportunity.getOppProducts.oppproductid#" onClick="eventOppProductOnClick(this);"></td>
				</cfif>
				<!--- <td align="center"><input type="checkbox" name="frmProductDeletionList" value="#myopportunity.getOppProducts.oppproductid#" onDblClick="if(this.checked)eventDeleteOnClick();"></td> --->
			</tr>
		</cfoutput>
	</cfif>
	<cfif myopportunity.getOppProducts.recordCount neq 0>
		<tr><th height="10" colspan="10">&nbsp;</th></tr>
	</cfif>
	<cfif showIllustrativePrice>
	<cfoutput>
		<tr><td colspan="10">* phr_#htmleditformat(phraseprefix)#illustrativedescription</td></tr>
	</cfoutput>
	</cfif>
	<cfif myopportunity.getOppProducts.recordCount gt 4>
<!--- ==============================================================================
       oppProduct Table Headings         
=============================================================================== --->
		<cfoutput>
			<tr>
				<!--- <th nowrap>phr_oppProductDescription</th> ---> 
				
				<th colspan="2" nowrap>phr_oppProductSKU</th>
				<th nowrap>phr_oppProductquantity</th>
				<cfif useOppProductShipDate><th nowrap>phr_oppProductShipDate</th></cfif>
				<cfif useOppProductProbability><th nowrap>phr_oppProductProbability</th></cfif>
				<cfif useOppProductSpecialPricing><th>phr_oppProductSpecialPrice</th></cfif>
				<cfif useOppProductStockLocation><th>phr_oppProductStockLocation</th></cfif>
				<th>SP Calc</th>
				<th colspan="2" nowrap>&nbsp;</th>
			</tr>
		</cfoutput>
	</cfif>

<!--- ==============================================================================
       Edit oppProduct Row         
=============================================================================== --->
	<cfif showEditOppProduct>
		<cfif myopportunity.getOppProducts.recordCount neq 0>
			<tr><input type="hidden" name="frmProductIDEdit"><input type="hidden" name="frmOppProductIDEdit">
				<td <cfif useOppFunctions>colspan="2"</cfif> valign="top">
					<input type="Text" name="frmDescriptionEdit" style="border: none; background-color: #D5D5CA;">
				</td>			
				<cfif useOppProductQuantityEdit>
				<td valign="top">
					<input type="text" name="frmQuantityEdit" size="5" onKeyPress="restrictKeyStrokes(event,keybNumeric)" onChange="eventOppProductEditOnChange(this);">
	 			</td>
				<cfelse>
					<input type="hidden" name="frmQuantityEdit">	
				</cfif> 
				
				<cfif useOppProductShipDate>
				<td valign="top">
					<cf_relayDateField	
						thisFormName="oppProductForm"
						fieldName="frmForecastShipDateEdit"
						anchorName="anchorX1"
						onChange="eventOppProductEditOnChange(this)"
						helpText="phr_dateFieldHelpText"
					>
	
	<!--- 				<input type="text" name="frmForecastShipDateEdit" size="10" onChange="eventOppProductEditOnChange(this);" readonly title="Click on the icon below to choose a date">&nbsp;<br><A HREF="javascript:void(showCalendar());" TITLE="Click to choose date" onMouseOver="window.status='Pop Calendar';return true;" onMouseOut="window.status='';return true;"><IMG SRC="<cfoutput></cfoutput>/images/misc/IconCalendar.gif" ALIGN="ABSMIDDLE" border="0"></A> --->
				</td>
				<cfelse>
					<input type="hidden" name="frmForecastShipDateEdit">&nbsp;
				</cfif>
				
				<td valign="top">
				<cfif useOppProductProbability><cfoutput>#htmleditformat(OppProductProbabilitySelectEdit)#</cfoutput><cfelse><input type="hidden" name="frmProbabilityEdit" value="">&nbsp;</cfif>
				</td>
				<td valign="top" nowrap>
					<input type="hidden" name="frmUnitPriceEdit">
					<input type="text" name="reduceByPerc" size="3" maxlength="3" onKeyPress="restrictKeyStrokes(event,keybDecimal)" onKeyUp="if (this.value == '') {this.form.calcDiscount.disabled=true} else {this.form.calcDiscount.disabled=false}">
					<cfoutput>%<CF_INPUT type="button" name="calcDiscount" value="phr_#phraseprefix#Lead_calculate" disabled onClick="calcLineItemDiscount(this)" title="Reduce special pricing by this percentage" ></cfoutput>
					&nbsp;
				</td>			
				<td valign="top">
				<cfif isDefined ("qValidStockLocations") and useOppProductStockLocation and isQuery(qValidStockLocations)>
					<SELECT NAME="frmStockLocationEdit">
						<!--- this is defined in opportunityINI.cfm --->
						<cfoutput query="qValidStockLocations">
							<option value="#optionValue#">#htmleditformat(optionDescription)#</option>
						</cfoutput>
					</SELECT>
					
				<cfelse>
					<input type="hidden" name="frmStockLocationEdit">&nbsp;
				</cfif>
				</td>
				<td valign="top">
				<cfif useOppProductSpecialPricing><input type="text" name="frmSpecialPriceEdit" size="8" onChange="eventOppProductEditOnChange(this);" onKeyPress="restrictKeyStrokes(event,keybDecimal)" value=""><cfelse><input type="hidden" name="frmSpecialPriceEdit" value="">&nbsp;</cfif>
				</td>
				<td colspan="2" valign="top" align="left">
					<cfif usebuttons>
						<cfoutput><CF_INPUT type="Button" value="phr_#phraseprefix#oppProductSaveProduct" onclick="void(eventOppProductSave());" class="OrdersFormbutton"></cfoutput>
					<cfelse>
						<a href="javascript:void(eventOppProductSave());" title="Save Edit">phr_oppProductSaveProduct</a>
					</cfif>
					<cfif showDuplicate>
						<br>
						<a href="javascript:void(eventOppProductDuplicate());" title="Click here to create a duplicate line item">Create Duplicate</a>&nbsp;&raquo;&nbsp;<br><a href="javascript:void(eventOppProductAddDuplicate());" title="Save Duplicate line item">Save Duplicate</a>
					</cfif>
				</td>
			</tr>
	
		</cfif>
	</cfif>
<!--- ==============================================================================
       Add new oppProduct Row         
=============================================================================== --->
	<cfoutput>
	<cfif showAddProduct>
		<TR>
		
		<td colspan="10" class="label">
			<TABLE border="0" CELLSPACING="1" CELLPADDING="3" ALIGN="left" BGCOLOR="White">

			<tr>
				<th>&nbsp;</th>
				<th nowrap>phr_oppProductquantity</th>
				<th nowrap><cfif useOppProductShipDate>phr_oppProductShipDate<cfelse>&nbsp;</cfif></th>
				<th nowrap><cfif useOppProductProbability>phr_oppProductProbability<cfelse>&nbsp;</cfif></th>
				<th><cfif useOppProductSpecialPricing>phr_oppProductSpecialPrice<cfelse>&nbsp;</cfif></th>
				<th><cfif useOppProductStockLocation>phr_oppProductStockLocation<cfelse>&nbsp;</cfif></th>
				<th>&nbsp;</th>
			</tr>

				
				<tr>
				
				
			<td valign="top" width="1">
			<!--- <div id="productSelectorDiv1"> --->
			<cfswitch expression="#productSelector#">
				<cfcase value="singleDropDown">
					<cfscript>
						myopportunity.getproductList();
					</cfscript>
					<SELECT NAME="thisProductID"> 
						<cfloop query="myopportunity.qproductGroupID">
							<option value="#ID#">#left(itemtext,30)#</option>
						</cfloop>
					</SELECT>
				</cfcase>
				
				<cfcase value="linkedDropDown">
					<cfscript>
						myopportunity.getProductAndGroupList();
					</cfscript>
					<cf_twoselectsrelated
						query="myopportunity.qProductAndGroupList"
						name1="productGroupID"
						name2="thisProductID"
						display1="productGroupDescription"
						display2="productDescription"
						value1="productGroupID"
						value2="productID"
						forcewidth1="30"
						forcewidth2="40"
						size1="1"
						size2="auto"
						autoselectfirst="Yes"
						emptytext1="(Select Product Group)"
						emptytext2="(Optional Product)"
						forcelength2 = "6"
						formname = "oppProductForm">	
				</cfcase>
				<cfdefaultcase>
					<cfscript>
						myopportunity.getproductList();
					</cfscript>
					<SELECT NAME="thisProductID">
						<cfloop query="myopportunity.qproductGroupID">
							<option value="#ID#">#left(itemtext,30)#</option>
						</cfloop>
					</SELECT>				
				</cfdefaultcase>
			</cfswitch>
			<!--- </div> --->
			</td>
			<td valign="top"><input name="quantity" size="5" onKeyPress="restrictKeyStrokes(event,keybNumeric)"></td>
			<td valign="top">
			<cfif useOppProductShipDate>
				<cf_relayDateField	
					currentValue=""
					thisFormName="oppProductForm"
					fieldName="forecastShipDate"
					anchorName="anchorX"
					helpText="phr_dateFieldHelpText"
				>
			
<!--- 			<input type="text" name="forecastShipDate" size="10" readonly title="Click on the icon below to shoose a date">&nbsp;<br><A HREF="javascript:show_calendar('oppProductForm.forecastShipDate',null,'','DD-MM-YYYY',null,'AllowWeekends=No;CurrentDate=Today')" TITLE="Click to choose date" onMouseOver="window.status='Pop Calendar';return true;" onMouseOut="window.status='';return true;"><IMG SRC="<cfoutput></cfoutput>/images/misc/IconCalendar.gif" ALIGN="ABSMIDDLE" border="0"></A></td> --->
			<cfelse>
				<input type="hidden" name="forecastShipDate">&nbsp;
			</cfif>
			</td>
			<td valign="top">
			<cfif useOppProductProbability><cfoutput>#htmleditformat(OppProductProbabilitySelectAdd)#</cfoutput><cfelse><CF_INPUT type="hidden" name="Probability" value="#url["probability"]#">&nbsp;</cfif>
			</td>
			<cfif useOppProductSpecialPricing><td valign="top"><input name="specialPrice" size="5" onKeyPress="restrictKeyStrokes(event,keybDecimal)" class="text-align:right;"></td><cfelse><input type="hidden" name="specialPrice"></cfif>
			<cfif isDefined ("qValidStockLocations") and useOppProductStockLocation and isQuery(qValidStockLocations)>
				<td valign="top">
				<SELECT NAME="frmStockLocation">
					<!--- this is defined in opportunityINI.cfm --->
					<cfloop query="qValidStockLocations">
						<option value="#optionValue#">#htmleditformat(optionDescription)#</option>
					</cfloop>
				</SELECT>
				</td>
			<cfelse>
				<input type="hidden" name="frmStockLocation">
			</cfif>
			<td colspan="4" valign="top"><a href="javascript:addproduct()">phr_oppProductAddProduct</a>&nbsp;&nbsp;&nbsp;<a href="javascript:addproductset()">phr_oppProductAddProductSet</a><br>
			<a href="javascript:addmultipleproducts()">phr_sys_oppProductAddMultipleProducts</a></td>			
		</tr>
				</table>
			</td>		
		</tr>
		</cfif>
	</form>
	
<cfif 1 eq 1>	
	<form action="" method="post" name="frmReasonForm" id="frmReasonForm">
	<input type="hidden" name="specialPricingStatusMethod" value="">
	<input type="hidden" name="frmComments" value="">
	<CF_INPUT type="hidden" name="opportunityID" value="#opportunityID#">
	<tr>
		<td colspan="10" align="center">
			<TABLE border="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White">
				<cfif showCalcOppProduct>
					<tr>
						<th colspan="3" align="left">phr_SpecialPriceCalculator</th>
					</tr>
				</cfif>
				<tr bgcolor="white"><!--- Project full price (Total disti sell in list price) --->
					<td align="right">phr_#htmleditformat(phraseprefix)#ProjectFullPrice</td>
					<td>
					<cfif showIllustrativePrice>
						#decimalformat(qOppPricing.fullPrice)# <strong>(#htmleditformat(trim(myopportunity.Currency))#)</strong>
					<cfelse>
						#LSCurrencyFormat(qOppPricing.fullPrice,"local")#
					</cfif>
					</td>
					<td>&nbsp;</td>
				</tr>
				<tr bgcolor="white">
					<td align="right">phr_#htmleditformat(phraseprefix)#CurrentCalculatedBudget</td>
					<td>
					<cfif showIllustrativePrice>
						#decimalformat(qOppPricing.discountedSpecialPrice)# <strong>(#htmleditformat(trim(myopportunity.Currency))#)</strong>
					<cfelse>
						#LSCurrencyFormat(qOppPricing.discountedSpecialPrice,"local")#
					</cfif>
					</td>
						<CF_INPUT type="hidden" name="fullPrice" value="#qOppPricing.fullPrice#">
					</td>
					<td>&nbsp;</td>
				</tr>
				<cfif showillustrativeprice>
					<cfscript>
						//toCurrency = application.com.relayEcommerce.getCurrencies(request.relaycurrentuser.countryID);
						illustrativetotal = application.com.currency.convert(fromValue="#trim(qOppPricing.discountedSpecialPrice)#", fromCurrency="#trim(myopportunity.Currency)#", toCurrency="#trim(toCurrency.countrycurrency)#");
					</cfscript>
					<tr bgcolor="white">
					<td align="right">phr_#htmleditformat(phraseprefix)#CurrentCalculatedIllustrativeBudget</td>
					<td>#decimalformat(illustrativetotal.tovalue)# <strong>(#htmleditformat(toCurrency.countrycurrency)#)</strong></td>
					</td>
					<td>&nbsp;</td>
				</tr>
				</cfif>
				<cfif showCalcOppProduct>
					<tr bgcolor="white">
						<td align="right">** Reduce to this budget</td></td>
						<td><input type="text" name="desiredBudget" id="desiredBudget" size="10" onkeyup="CheckIfDesiredBudgetEmpty()"  onKeyPress="restrictKeyStrokes(event,keybDecimal)"> <strong>(#htmleditformat(trim(myopportunity.Currency))#)</strong></td>
						<td align="left"><input type="button" name="calcDiscount" value="Calculate Discount" disabled onClick="calcMyDiscount()">&nbsp;</td>
					</tr>
		
					<tr bgcolor="white">
						<td align="right">*** Reduce by this percentage</td>
						<td><input type="text" name="desiredDiscount" id="desiredDiscount" size="10" onkeyup="CheckIfDesiredDiscountEmpty()"  onKeyPress="restrictKeyStrokes(event,keybDecimal)"> <strong>(#htmleditformat(trim(myopportunity.Currency))#)</strong></td>
						<td align="left"><input type="button" name="calcBudget" value="Calculate Budget" disabled onClick="calcMyBudget()">&nbsp;</td>
					</tr>
		
					<tr bgcolor="white">
						<td align="right"><div id="resultText"> </div></td>
						<td><input type="text" name="newBudget" id="newBudget" disabled size="10"></td>
						<td align="left"><input type="button" name="SP_SaveBudget" value="Apply this change" disabled onClick="saveSpecialPricing('SP_Calculate')">&nbsp;<input type="button" value="Reset" onClick="resetCalculator()"></td>
					</tr>
					<tr>
						<td colspan="3">
							**  phr_#htmleditformat(phraseprefix)#Opp_ReduceByBudgetHelp<br /><br />
							*** phr_#htmleditformat(phraseprefix)#Opp_ReduceByPercentHelp
						</td>
					</tr>
				</cfif>
			</table>
		</td>
	</tr>
	</table>
		</td>
	</tr>
</table>
	</form>
</cfif>

	</cfoutput>
<cfelse>
	<cfoutput>You must pass a query object to #htmleditformat(thisTag)#</cfoutput>
</cfif>
<!---  --->
<!--- --------------------------------------------------------------------- --->
<!--- create JavaScript object to hold OppProducts result set client-side --->
<!--- --------------------------------------------------------------------- --->
<cf_translate encode="javascript">
<SCRIPT type="text/javascript">
<!--
var resultSetOppProducts = new Object;
var validStockLocationObj = new Object;
var dirtyOppProductsEdit = false;
var touchedSpecialPrice = false;
<cfoutput>
<cfloop query="myopportunity.getOppProducts">
	resultSetOppProducts["#JSStringFormat( myopportunity.getOppProducts.oppproductid )#"] = new Array(
	"#JSStringFormat( myopportunity.getOppProducts.oppproductid )#", //[0]
	"#JSStringFormat( myopportunity.getOppProducts.description )#", //[1]
	"#JSStringFormat( myopportunity.getOppProducts.quantity )#", //[2]
	"#iif( IsDate( myopportunity.getOppProducts.forecastshipdate ), 'DateFormat( myopportunity.getOppProducts.forecastshipdate, "dd/mm/yyyy" )', de( "" ) )#", //[3]
	"#JSStringFormat( myopportunity.getOppProducts.probability )#", //[4]
	"#JSStringFormat( myopportunity.getOppProducts.specialprice )#", //[5]
	"#JSStringFormat( myopportunity.getOppProducts.sku )#", //[6]
	"#JSStringFormat( myopportunity.getOppProducts.subtotal )#", //[7]
	"#JSStringFormat( myopportunity.getOppProducts.unitprice )#", //[8]
	"#JSStringFormat( myopportunity.getOppProducts.promotionalPrice )#", //[9]
	"#JSStringFormat( myopportunity.getOppProducts.productPromotionID )#", //[10]
	"#JSStringFormat( myopportunity.getOppProducts.specialPriceApproverPersonID )#", //[11]
	"#JSStringFormat( myopportunity.getOppProducts.productid )#", //[12]
	"#JSStringFormat( myopportunity.getOppProducts.stockLocation )#", //[13]
	"#JSStringFormat( myopportunity.getOppProducts.SPManualEdit )#" //[14]
	);
</cfloop>

<cfif useOppProductStockLocation and isQuery(qValidStockLocations)>
	<cfloop query="qValidStockLocations">
		<cfif qValidStockLocations.optionValue neq "">
		validStockLocationObj["#JSStringFormat( qValidStockLocations.optionValue )#"] = new Array("#JSStringFormat( qValidStockLocations.optionDescription )#");
		</cfif>
	</cfloop>
</cfif>
</cfoutput>

function eventOppProductEditClear()
{
	with ( document.forms["oppProductForm"] )
    {
		elements["frmProductIDEdit"].value = "";
		elements["frmOppProductIDEdit"].value = "";
		elements["frmUnitPriceEdit"].value = "";
		elements["frmDescriptionEdit"].value = "";
		elements["frmQuantityEdit"].value = "";
		elements["frmForecastShipDateEdit"].value = "";
		elements["frmSPManualEdit"].value = "";
		elements["frmSpecialPriceEdit"].value = "";
		elements["reduceByPerc"].value = "";
		elements["calcDiscount"].disabled = true;
	}
}

function eventOppProductOnClick( callerObject )
{
  var allowNewOppProductID = true;
  var radioButtonIndex;
  if ( dirtyOppProductsEdit && document.forms["oppProductForm"]["frmOppProductIDEdit"].value != "" )
  {
    allowNewOppProductID = confirm( "phr_opp_DoYouWishToForgetTheChanges" + document.forms["oppProductForm"]["frmDescriptionEdit"].value );
    if ( !allowNewOppProductID )
    {
// reset the radio button back to the original one
      for ( radioButtonIndex = 0; radioButtonIndex < document.forms["oppProductForm"]["frmEditOppProduct"].length; radioButtonIndex++ )
      {
        if ( document.forms["oppProductForm"]["frmEditOppProduct"][radioButtonIndex].value == document.forms["oppProductForm"]["frmOppProductIDEdit"].value )
        {
          document.forms["oppProductForm"]["frmEditOppProduct"][radioButtonIndex].checked = true;
          break;
        }
      }
    }
  }
  if ( allowNewOppProductID )
  {
    dirtyOppProductsEdit = false;
	touchedSpecialPrice = false;
    resetStyles();
    with ( document.forms["oppProductForm"] )
    {
      elements["frmProductIDEdit"].value = resultSetOppProducts[callerObject.value][12];
      elements["frmOppProductIDEdit"].value = resultSetOppProducts[callerObject.value][0];
	  elements["frmUnitPriceEdit"].value = resultSetOppProducts[callerObject.value][8];
      elements["frmDescriptionEdit"].value = resultSetOppProducts[callerObject.value][6].substring(0, 19); //GCC set to be SKU for display: description not editable
      elements["frmQuantityEdit"].value = resultSetOppProducts[callerObject.value][2];
      elements["frmForecastShipDateEdit"].value = resultSetOppProducts[callerObject.value][3];
	  elements["frmSPManualEdit"].value = 1;
	  <cfif isDefined ("qValidStockLocations") and useOppProductStockLocation and isQuery(qValidStockLocations)> 
	  for ( optionIndex = 0; optionIndex < elements["frmStockLocationEdit"].length; optionIndex++ )
		{
			if ( elements["frmStockLocationEdit"][optionIndex].value.toLowerCase() == resultSetOppProducts[callerObject.value][13].toLowerCase() )
			{
				elements["frmStockLocationEdit"].selectedIndex = optionIndex;
				break;
			}
		}
		</cfif>
      elements["frmProbabilityEdit"].value = ( resultSetOppProducts[callerObject.value][4] != "" ) ? resultSetOppProducts[callerObject.value][4] : "<cfoutput>#url["probability"]#</cfoutput>" ;
// promotional price or approved special price
      if ( resultSetOppProducts[callerObject.value][10] == "" && resultSetOppProducts[callerObject.value][11] == "" )
      {
        elements["frmSpecialPriceEdit"].value = resultSetOppProducts[callerObject.value][5];
        elements["frmSpecialPriceEdit"].readOnly = false;
      }
      else
      {
        elements["frmSpecialPriceEdit"].value = ( resultSetOppProducts[callerObject.value][10] != "" ) ? resultSetOppProducts[callerObject.value][9] : resultSetOppProducts[callerObject.value][5];
        elements["frmSpecialPriceEdit"].readOnly = true;
        elements["frmSpecialPriceEdit"].style.color = "silver";
      }
    }
	<cfif useOppProductQuantityEdit>
    	document.forms["oppProductForm"]["frmQuantityEdit"].focus();
	</cfif>
  }
}

function eventOppProductEditOnChange( callerObject )
{
  if ( callerObject.name == "frmSpecialPriceEdit" && callerObject.value != "" ) touchedSpecialPrice = true;
  callerObject.style["color"] = "red";
  dirtyOppProductsEdit = true;
  document.forms["oppProductForm"].elements["frmSPManualEdit"].value = 1;
  return true;
}

function resetStyles()
{
  normalColour = "black";
  with ( document.forms["oppProductForm"] )
  {
    elements["frmQuantityEdit"].style["color"] = normalColour;
    elements["frmForecastShipDateEdit"].style["color"] = normalColour;
    elements["frmProbabilityEdit"].style["color"] = normalColour;
    elements["frmSpecialPriceEdit"].style["color"] = normalColour;
  }
  return true;
}

function eventOppProductDuplicate( callerObject )
{
  var errorMessage = "";
  with ( document.forms["oppProductForm"] )
  {
    if ( elements["frmOppProductIDEdit"].value == "" ) errorMessage = "phr_opp_SelectAProductViaRadioButtons";

    if ( errorMessage != "" ) alert( errorMessage )
    else
    {
		dirtyOppProductsEdit = true;
		if( confirm( "phr_opp_DoYouWishToCopyThisRecord" ) )
		{
		}
		else
		{
			elements["frmQuantityEdit"].value = "";
			elements["frmForecastShipDateEdit"].value = "";
			elements["frmProbabilityEdit"].selectedIndex = 0;
			elements["frmSpecialPriceEdit"].value = "";
		}

//		elements["frmTask"].value = "addproduct";
/*
		elements["productID"].value = elements["frmProductIDEdit"].value;
		elements["quantity"].value = ""; //elements["frmQuantityEdit"].value;
		elements["forecastShipDate"].value = ""; //elements["frmForecastShipDateEdit"].value;
		elements["Probability"].selectedIndex = 0; //elements["frmProbabilityEdit"].selectedIndex;
		elements["specialPrice"].value = ""; //elements["frmSpecialPriceEdit"].value;
*/
/*
		elements["productGroupID"].selectedIndex = 1;
		elements["thisProductID"].selectedIndex = 1;
		elements["thisProductID"].value = elements["frmOppProductIDEdit"].value;
*/	
//		submit();
	}
  }
}

function eventOppProductAddDuplicate( callerObject )
{
/*
	with ( document.forms["oppProductForm"] )
	{
		elements["frmTask"].value = "addproduct";

		elements["productID"].value = elements["frmProductIDEdit"].value;
		elements["quantity"].value = elements["frmQuantityEdit"].value;
		elements["forecastShipDate"].value = elements["frmForecastShipDateEdit"].value;
		elements["Probability"].selectedIndex = elements["frmProbabilityEdit"].selectedIndex;
		elements["specialPrice"].value = elements["frmSpecialPriceEdit"].value;
	}
*/
  var errorMessage = "";
  with ( document.forms["oppProductForm"] )
  {
    if ( elements["frmOppProductIDEdit"].value == "" ) errorMessage = "phr_opp_SelectAProductViaRadioButtons";

    if ( errorMessage != "" ) alert( errorMessage )
    else
    {
		eventOppProductSave( true );
	}
  }
}

function eventOppProductSave( fromDuplication )
{
  errorMessage = "";
  with ( document.forms["oppProductForm"] )
  {
    if ( elements["frmOppProductIDEdit"].value == "" ) errorMessage = "phr_opp_SelectAProductViaRadioButtons";
    if ( elements["frmQuantityEdit"].value != "" && elements["frmQuantityEdit"].value.search( /^[0-9]*$/ ) != 0 )  errorMessage = "phr_opp_QuantityMustBeNumeric";
    if ( elements["frmSpecialPriceEdit"].value != "" && isNaN( Number( elements["frmSpecialPriceEdit"].value.replace( /,/g, "." ) ) ) )  errorMessage = "phr_opp_SpecialPriceMustBeNumeric";
	if ( fromDuplication && elements["frmQuantityEdit"].value.search( /^[0-9]+$/ ) != 0 ) errorMessage = "phr_opp_QuantityMustBeNumeric";
// || isNaN( Number( elements["quantity"].value ) ) ) 

	<cfif MakeForecastShipDateMandatory>
		if ( elements["frmForecastShipDateEdit"].value == "" )
		{
			errorMessage = "phr_opp_shipDateIsRequired";
		}
	</cfif>

    if ( errorMessage != "" ) alert( errorMessage )
    else
    {
      if ( ! fromDuplication )
	  	elements["frmTask"].value = "editProduct";
	else
	{
		elements["frmTask"].value = "addproduct";

		elements["productID"].value = elements["frmProductIDEdit"].value;
		elements["quantity"].value = elements["frmQuantityEdit"].value;
		elements["forecastShipDate"].value = elements["frmForecastShipDateEdit"].value;
		elements["Probability"].selectedIndex = elements["frmProbabilityEdit"].selectedIndex;
		elements["specialPrice"].value = elements["frmSpecialPriceEdit"].value;
	}
	  // SWJ commented these out.
      if ( elements["frmSpecialPriceEdit"].value != "" ) elements["frmSpecialPriceEdit"].value = parseFloat( elements["frmSpecialPriceEdit"].value.replace( /,/g, "." ) );
      //if ( touchedSpecialPrice ) specialPriceReasonWindow( elements["frmDescriptionEdit"].value )
	  //else 
	  submit();
    }
  }
  return true;
}

//function eventDeleteOnClick( callerObject )
//{
// var errorMessage = "";
//  var checkboxIndex;
//  with ( document.forms["oppProductForm"] )
//  {
//    if ( !(elements["frmProductDeletionList"].length) && ! elements["frmProductDeletionList"].checked ) errorMessage = "Please select at least one item for deletion.";
//    if ( elements["frmProductDeletionList"].length )
//    {
//      errorMessage = "Please select at least one item for deletion.";
//      for ( checkboxIndex = 0; checkboxIndex < elements["frmProductDeletionList"].length; checkboxIndex++ )
//      if ( elements["frmProductDeletionList"][checkboxIndex].checked )
//      {
//        errorMessage = "";
//        break;
//      }
//    }
//    if ( errorMessage != "" ) alert( errorMessage )
//    {
//      elements["frmTask"].value = "removeProduct";
//      submit();
//    }
//  }
//  return true;
//} 


function specialPriceReasonWindow( productDescription )
{
  var windowFeatutres = "";
  var targetUrl = "";
  windowFeatutres += "directories=no,";
  windowFeatutres += "hotkeys=no,";
  windowFeatutres += "location=no,";
  windowFeatutres += "menubar=no,";
  windowFeatutres += "resizable=no,";
  windowFeatutres += "scrollbars=yes,";
  windowFeatutres += "status=no,";
  windowFeatutres += "toolbar=no,";
  windowFeatutres += "fullscreen=no,";
  targetUrl = "oppProductSpecialPrice.cfm?description=";
  targetUrl += escape( productDescription );
  targetUrl += "&entityID=";
  targetUrl += escape( "<cfoutput>#JSStringFormat( url.opportunityID )#</cfoutput>" );
  newSpecialPriceReasonWindow = window.open( targetUrl, "approval", windowFeatutres, true );
  return true;
}

function reSort(sortOrder) {
	var form = document.oppProductForm;
	form.sortOrder.value = sortOrder;
	form.submit();
}


function specialPriceApprovalWindow( callerObject, oppProductID )
{
  var windowFeatutres = "";
  var targetUrl = "";
  callerObject.blur();
  windowFeatutres += "directories=no,";
  windowFeatutres += "hotkeys=no,";
  windowFeatutres += "location=no,";
  windowFeatutres += "menubar=no,";
  windowFeatutres += "resizable=no,";
  windowFeatutres += "scrollbars=yes,";
  windowFeatutres += "status=no,";
  windowFeatutres += "toolbar=no,";
  windowFeatutres += "fullscreen=no,";
  targetUrl += "oppProductApproval.cfm?countryID=";
  targetUrl += escape( "<cfoutput>#JSStringFormat( url["countryID"] )#</cfoutput>" );
  targetUrl += "&opportunityID=";
  targetUrl += escape( "<cfoutput>#JSStringFormat( url["opportunityID"] )#</cfoutput>" );
  targetUrl += "&oppProductID=";
  targetUrl += escape( oppProductID );
  newSpecialPriceReasonWindow = window.open( targetUrl, "approval", windowFeatutres, true );
  return true;
}

function specialPriceReasonCallBack( specialPriceReason )
{
  with ( document.forms["oppProductForm"] )
  {
    elements["frmSpecialPriceReason"].value = specialPriceReason;
    submit();
  }
}

function specialPriceApprovalCallBack( specialPriceReason )
{
  document.location.replace( document.location.href );
}

function showCalendar( callerObject )
{
  dirtyOppProductsEdit = true;
  eventOppProductEditOnChange( document.forms["oppProductForm"]["frmForecastShipDateEdit"] );
  show_calendar( 'oppProductForm.frmForecastShipDateEdit', null, '', 'DD-MM-YYYY', null, 'AllowWeekends=No;CurrentDate=Today' );
}

function toggleVisibility( divName, callerObject )
{
  document.all[divName].style.display = ( document.all[divName].style.display == "" ) ? "none" : "";
  if ( callerObject )
  {
    callerObject.value = ( document.all[divName].style.display == "" ) ? String.fromCharCode( 171 ) : String.fromCharCode( 187 );
    callerObject.blur();
  }
  return true;
}
//-->
</script>
</cf_translate>
<!--- --------------------------------------------------------------------- --->
<!--- 
<cfdump var="#myopportunity.getOppProducts#">
 --->
</cfif>

<cfif request.relaycurrentuser.isInternal>
	
	
</cfif>
</cf_translate>
<!--- 
<cfdump var="#myopportunity.getOppProducts#">
<cfdump var="#myopportunity.qProductAndGroupList#">
 --->
<!--- 
<cfdump var="#myopportunity.qproductGroupID#">
 --->

<!--- 
<cfcatch type="any">
<cfdump var="#cfcatch#">
</cfcatch>
</cftry>
 --->



