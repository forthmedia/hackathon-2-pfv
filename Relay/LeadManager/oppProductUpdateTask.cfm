<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		oppProductUpdateTask.cfm
Author:			GCC
Date started:		2004-09-20
	
Description:		This file handles requests from oppProductList to capture / validate
user entered values and uses them to single/bulk update opportunity product data.

Usage:			

Amendment History:

Date (DD-MMM-YYYY)	Initials 	Code	What was changed

04-MAR-2005			AJC			CR_ATI003_currentprices		Added function to update oppProduct prices to the latest price in the product Table.
20-JAN-2009			NJH			Bug Fix ATI Issue 1624		Passed opportunityID to the deleteOppProducts function

Enhancements still to do:

 --->
 <cf_translate>

<cfinclude template="/templates/relayFormJavaScripts.cfm">
<cf_head>
<script>
 function checkForm(frmAction) {
	var form = document.userData
	var msg = ""
	
	if (frmAction.value == 'integer') {
		if (isNaN(form.newInteger.value)) {
			msg += "\n\nYou must enter a number.";
		} 
	}

	if (msg != "") {
			alert(msg);
		} else {
			form.submit();
		}
}
</script>
</cf_head>


<cfif structkeyExists(Form,"Update")>
	<cfswitch expression=#frmAction#>	
		<cfcase value="probability">
			<cfscript>
				application.com.oppSpecialPricing.updateProbability(frmProductCheck, newProbability);
			</cfscript> 
		</cfcase>
		<cfcase value="delete">
			<cfscript>
				application.com.oppSpecialPricing.deleteOppProducts(oppProductID=frmProductCheck,opportunityID=opportunityID); //NJH 2009-01-20 Bug Fix ATI Issue 1624 - added opportunityID
			</cfscript> 
		</cfcase>
		<cfcase value="integer">
			<cfscript>
				application.com.oppSpecialPricing.updateInteger(frmProductCheck, newInteger);
			</cfscript> 	
		</cfcase>
		<cfcase value="Date">
			<cfscript>
				//application.com.oppSpecialPricing.updateForecastShipDate(frmProductCheck, CreateDate( mid( form["newDate"], 7, 4 ),mid( form["newDate"], 4, 2 ), left( form["newDate"], 2 ) ));
				application.com.oppSpecialPricing.updateForecastShipDate(frmProductCheck, form["newDate"]);
			</cfscript> 	
		</cfcase>
		<cfcase value="latest">
			<cfscript>
				application.com.oppSpecialPricing.updateOppPricing(frmProductCheck);
			</cfscript> 
		</cfcase>
	</cfswitch>
	<form name="returnForm" action="oppProductList.cfm" target="productList">
	<cfoutput>
		<CF_INPUT type="hidden" name="opportunityID" value="#opportunityID#">
		<CF_INPUT type="hidden" name="frmProductCheck" value="#frmProductCheck#">
	</cfoutput>
	</form>
	<script>
	var form = document.returnForm
	form.action='oppProductList.cfm';
	form.submit();
	self.close();
	</script>
<cfelse>

	<cfif structkeyExists(Form,"frmAction")>
		<table border="0" cellspacing="0" cellpadding="3" align="left" bgcolor="White" class="withBorder" width="100%" height="100%">
			<tr>
				<td align="center" valign="top">
					<form name="userData" method="post" target="_self" enctype="multipart/form-data">
						<cfswitch expression=#frmAction#>
							<cfcase value="probability">
									<cfquery name="getCurrentProbability" datasource="#application.siteDataSource#">
										select distinct probability from oppStage order by probability
									</cfquery>
								phr_SelectNewValue:
								<select name="newProbability">
										<cfoutput query="getCurrentProbability">
											<option value="#probability#">#htmleditformat(probability)#</option>
										</cfoutput>
								</select>
							</cfcase>
							<cfcase value="delete">
								phr_oppClickUpdateToConfirm
							</cfcase>
							<cfcase value="integer">
								phr_SelectNewValue:<input name="newInteger" type="Text" size="5">
							</cfcase>			
							<cfcase value="Date">
								<cf_relayDateField	
									currentValue=""
									thisFormName="userData"
									fieldName="newDate"
									anchorName="anchorX1"
									helpText="phr_dateFieldHelpText"
								>
							</cfcase>
							<cfcase value="latest">
								phr_oppClickUpdateToConfirmlatest
							</cfcase>
						</cfswitch>
						<cfoutput>
							<CF_INPUT type="hidden" name="opportunityID" value="#opportunityID#">
							<CF_INPUT type="hidden" name="frmProductCheck" value="#frmProductCheck#">
							<CF_INPUT type="hidden" name="frmAction" value="#frmAction#">
						</cfoutput>
						<input type="hidden" name="Update" value="True">
						<input type="button" name="btnUpdate" value="phr_Update" onClick="checkForm(frmAction)">
					</form>
				</td>
			</tr>
		</table>
	</cfif>

</cfif>

</cf_translate>