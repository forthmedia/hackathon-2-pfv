<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
2012-10-04	WAB		Case 430963 Remove Excel Header 
 --->
<cfinclude template="../templates/tableFromQueryHeaderInclude.cfm">
<cfparam name="openAsExcel" type="boolean" default="false">

<cf_head>
	<cf_title>Lead|Opportunity List</cf_title>
</cf_head>


<cfinclude template="LeadManagerTopHead.cfm">


<!--- 2012-07-23 PPB P-SMA001 commented out 
<cfif application.com.settings.getSetting("leadManager.countryScopeOpportunityRecords")>	<!--- 2011-06-01 PPB REL106 use countryScopeOpportunityRecords setting --->
	<cfinclude template="/templates/qryGetCountries.cfm">
</cfif>
--->

<cfparam name="startrow" default="1">
<cfparam name="sortOrder" default="country">
<cfparam name="numRowsPerPage" default="250">
<!--- The keyColumn list group of params below control which columns show URL links
		where they link to and what key should be used in the URL --->
<cfparam name="keyColumnList" type="string" default=""><!--- this can contain a list of columns that can be edited --->
<cfparam name="keyColumnURLList" type="string" default=""><!--- this can contain a list of matching columns that contain the URL of the editor template --->
<cfparam name="keyColumnKeyList" type="string" default=""><!--- this can contain a list of matching key columns e.g. primary key --->

<cfparam name="radioFilterLabel" type="string" default=""><!--- this can contain a list of columns that can be edited --->
<cfparam name="radioFilterDefault" type="string" default=""><!--- this can contain a list of matching columns that contain the URL of the editor template --->
<cfparam name="radioFilterName" type="string" default=""><!--- this can contain a list of matching key columns e.g. primary key --->
<cfparam name="radioFilterValues" type="string" default=""><!--- this should contain a list of values for each radio button delimited by a minus sign --->

<cfparam name="dateFormat" type="string" default=""><!--- this list should contain those fields you want in the date format dd-mmm-yy --->
<cfparam name="entityID" type="numeric" default="0">
<cfparam name="frm_vendoraccountmanagerpersonid" type="numeric" default="0">

<cfparam name="checkBoxFilterName" type="string" default="">
<cfparam name="checkBoxFilterLabel" type="string" default="">

<cfparam name="pipelineCountryShowTheseColumns" type="string" default="Project_Stage,Opportunity_Stage,calculated_budget_this_quarter,calculated_budget_next_quarter,calculated_budget_quarter_plus2">
<cfparam name="pipelineCountryOppStageIDs" type="string" default="1,2,3,4,5">

<cfset thisQuarter = quarter(now())>
<cf_DateRange type="quarter" value="current" year="#year(now())#">
<cf_CalcWeek CheckDate="#now()#" BeginDay="2" EndDay="6">


<cfquery name="getPipeline" datasource="#application.SiteDataSource#">
select 	CountryID, Country, Project_Stage, calculated_budget_this_quarter, 
		calculated_budget_next_quarter, calculated_budget_quarter_plus2,Opportunity_Stage
from	vLeadCountryPipeline
where 	OpportunityStageID  in ( <cf_queryparam value="#pipelineCountryOppStageIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	<cfinclude template="../templates/tableFromQuery-QueryInclude.cfm">

	order by country
</cfquery>

<cfoutput>
<table width="100%" cellpadding="3">
	<tr>
		<td><strong>Q#htmleditformat(thisQuarter)# #htmleditformat(year(now()))# Country Pipeline Report</strong></td>
		<td align="right">Week #htmleditformat(week(now()))# (Week commencing #dateFormat(StartWeek,"dd-mmm-yy")#)</td>
	</tr>
</table>
</cfoutput>

<cf_translate>
<CF_tableFromQueryObject 
	queryObject="#getPipeline#"
	queryName="getPipeline"
	sortOrder = "#sortOrder#"
	numRowsPerPage="#numRowsPerPage#"
	startrow="#startrow#"
	
	keyColumnList="#keyColumnList#"
	keyColumnURLList="#keyColumnURLList#"
	keyColumnKeyList="#keyColumnKeyList#"
	showTheseColumns="#pipelineCountryShowTheseColumns#"
	hideTheseColumns="CountryID,Country"
	dateFormat="#dateFormat#"
	columnTranslation="true"
	FilterSelectFieldList=""
	totalTheseColumns="calculated_budget_this_quarter,calculated_budget_next_quarter,calculated_budget_quarter_plus2"
	GroupByColumns="country"
	radioFilterLabel="#radioFilterLabel#"
	radioFilterDefault="#radioFilterDefault#"
	radioFilterName="#radioFilterName#"
	radioFilterValues="#radioFilterValues#"
	checkBoxFilterName="#checkBoxFilterName#"
	checkBoxFilterLabel="#checkBoxFilterLabel#"
	allowColumnSorting="no"
	currencyFormat="calculated_budget_this_quarter,calculated_budget_next_quarter,calculated_budget_quarter_plus2"
>
</cf_translate>


