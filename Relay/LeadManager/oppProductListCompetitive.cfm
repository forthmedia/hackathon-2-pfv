<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		oppProductListCompetitive.cfm
Author:			AJC
Date started:	2009-11-09

Description:	Adds competitor products to a oppProduct line item request

Amendment History:

Date (DD-MMM-YYYY)	Initials	Code	What was changed 
2011-12-05 NYB LHID8224 translated headings
2014-01-31	WAB 	removed references to et cfm, can always just use / or /?

Possible enhancements:

--->

<cfif request.relaycurrentUser.isInternal>
	
	<cf_head>
		<cf_title>Opp ProductList</cf_title>
	<cfoutput>
		<cfif isdefined("request.partnerFacingStylesheet")>
			<LINK REL="stylesheet" HREF="/code/styles/#htmleditformat(request.partnerFacingStylesheet)#.css">
		</cfif>
	</cfoutput>
	<!-- meta anti cache--> 
		<META HTTP-EQUIV="Expires" CONTENT="Mon, 06 Jan 1990 00:00:01 GMT"> 
		<META HTTP-EQUIV="Pragma" CONTENT="no-cache"> 
		<META HTTP-EQUIV="cache-control" VALUE="no-cache, no-store, must-revalidate">
	</cf_head>
	
</cfif>
<cfparam name="url.opportunityID" default="0">
<cfparam name="form.opportunityID" default="0">
<cfparam name="variables.opportunityID" default="0">
<cfparam name="url.oppProductID" default="0">
<cfparam name="form.oppProductID" default="0">
<cfparam name="variables.oppProductID" default="0">
<cfparam name="url.frmTask" default="">
<cfparam name="form.frmTask" default="">
<cfparam name="variables.frmTask" default="">
<cfparam name="url.frmOppTypeID" default="">
<cfparam name="form.frmOppTypeID" default="">
<cfparam name="variables.frmOppTypeID" default="">

<cfparam name="variables.ProductCompetitorID" default="0">
<cfparam name="variables.ProductCompetitorProductTypeID" default="0">
<cfparam name="variables.UserProductCompetitor" default="">
<cfparam name="variables.Model" default="">
<cfparam name="variables.UnitCost" default="">
<cfparam name="variables.Notes" default="">
<cfparam name="variables.Description" default="">
<cfparam name="variables.oppProductCompetitorID" default="">

<cfparam name="variables.compURL" default="">

<cfparam name="urlRedirect" default="">

<cfif url.opportunityID gt 0>
	<cfset variables.opportunityID = url.opportunityID>
<cfelseif form.opportunityID gt 0>
	<cfset variables.opportunityID = form.opportunityID>
</cfif>


<cfif form.frmTask neq "">
	<cfset variables.frmTask = form.frmTask>
<cfelseif url.frmTask neq "">
	<cfset variables.frmTask = url.frmTask>
</cfif>

<cfif form.oppProductID gt 0>
	<cfset variables.oppProductID = form.oppProductID>
<cfelseif url.oppProductID gt 0>
	<cfset variables.oppProductID = url.oppProductID>
</cfif>

<cfif form.frmOppTypeID gt 0>
	<cfset variables.frmOppTypeID = form.frmOppTypeID>
<cfelseif url.frmOppTypeID gt 0>
	<cfset variables.frmOppTypeID = url.frmOppTypeID>
</cfif>

<cfif request.relaycurrentUser.isInternal>
	<cfparam name="variables.backURL" default="">
	<cfparam name="variables.keyColumnURLList" default="">
<cfelse>
	<cfif isdefined('frmOppTypeID')>
		<cfparam name="variables.backURL" default="/?eid=#url.eid#&entityID=#url.entityID#&OpportunityID=#variables.opportunityID#&frmOppTypeID=#frmOppTypeID#">
	<cfelse>
		<cfparam name="variables.backURL" default="/?eid=#url.eid#&entityID=#url.entityID#&OpportunityID=#variables.opportunityID#">
	</cfif>
	<cfif isdefined('frmOppTypeID')>
		<cfparam name="variables.keyColumnURLList" default="/?eid=#url.eid#&entityID=#url.entityID#&frmOppTypeID=#frmOppTypeID#&frmTask=editFormCompetitive&OpportunityID=#variables.opportunityID#&OppProductID=#variables.OppProductID#&OppProductCompetitorID=,/?eid=#url.eid#&frmOppTypeID=#frmOppTypeID#&entityID=#url.entityID#&frmTask=deleteCompetitive&OpportunityID=#variables.opportunityID#&OppProductID=#variables.OppProductID#&OppProductCompetitorID=">	
	<cfelse>
	<cfparam name="variables.keyColumnURLList" default="/?eid=#url.eid#&entityID=#url.entityID#&frmTask=editFormCompetitive&OpportunityID=#variables.opportunityID#&OppProductID=#variables.OppProductID#&OppProductCompetitorID=,/?eid=#url.eid#&entityID=#url.entityID#&frmTask=deleteCompetitive&OpportunityID=#variables.opportunityID#&OppProductID=#variables.OppProductID#&OppProductCompetitorID=">	
</cfif>
</cfif>
<cfif variables.frmTask is "addCompetitive">
	
	<cfscript>
		// create an instance of the opportunity component
		if (not isdefined("myopportunity"))
		{
if (fileexists(application.paths.code & "\cftemplates\relayOpportunity.cfc")) {
	componentPath = "code.CFTemplates.relayOpportunity";
	myopportunity = createObject("component",componentPath);}
else {myopportunity = createObject("component","relay.com.opportunity");}
			myopportunity.dataSource = application.siteDataSource;
		}
		myopportunity.opportunityID = variables.opportunityID;
		myopportunity.oppProductID = variables.oppProductID;
		myopportunity.ProductCompetitorID = form.frmProductCompetitorID;
		myopportunity.productCompetitorProductTypeID = form.frmproductCompetitorProductTypeID;
		myopportunity.UserProductCompetitor = form.frmUserProductCompetitor;
		myopportunity.Model = form.frmModel;
		myopportunity.UnitCost = form.frmUnitCost;
		myopportunity.Notes = form.frmNotes;
		myopportunity.Description = form.frmDescription;
		myopportunity.addOppProductCompetitor();
	</cfscript>
	
<cfelseif variables.frmTask is "editCompetitive">
	<cfscript>
		// create an instance of the opportunity component
		if (not isdefined("myopportunity"))
		{
if (fileexists(application.paths.code & "\cftemplates\relayOpportunity.cfc")) {
	componentPath = "code.CFTemplates.relayOpportunity";
	myopportunity = createObject("component",componentPath);}
else {myopportunity = createObject("component","relay.com.opportunity");}
			myopportunity.dataSource = application.siteDataSource;
		}
		myopportunity.opportunityID = variables.opportunityID;
		myopportunity.oppProductID = variables.oppProductID;
		myopportunity.oppProductCompetitorID = form.frmOppProductCompetitorID;
		myopportunity.productCompetitorID = form.frmProductCompetitorID;
		myopportunity.productCompetitorProductTypeID = form.frmproductCompetitorProductTypeID;
		myopportunity.UserProductCompetitor = form.frmUserProductCompetitor;
		myopportunity.Model = form.frmModel;
		myopportunity.UnitCost = form.frmUnitCost;
		myopportunity.Notes = form.frmNotes;
		myopportunity.Description = form.frmDescription;
		myopportunity.updateOppProductCompetitor();
	</cfscript>

<cfelseif variables.frmTask is "editFormCompetitive">
	<cfscript>
		// create an instance of the opportunity component
		if (not isdefined("myopportunity"))
		{
if (fileexists(application.paths.code & "\cftemplates\relayOpportunity.cfc")) {
	componentPath = "code.CFTemplates.relayOpportunity";
	myopportunity = createObject("component",componentPath);}
else {myopportunity = createObject("component","relay.com.opportunity");}
			myopportunity.dataSource = application.siteDataSource;
		}
		myopportunity.opportunityID = variables.opportunityID;
		myopportunity.oppProductID = variables.oppProductID;
		myopportunity.oppProductCompetitorID = url.oppProductCompetitorID;
		qryGetOppProductCompetitors = myopportunity.getOppProductCompetitors();
	
		variables.ProductCompetitorID=qryGetOppProductCompetitors.ProductCompetitorID;
		variables.productCompetitorProductTypeID = qryGetOppProductCompetitors.productCompetitorProductTypeID;
		variables.UserProductCompetitor=qryGetOppProductCompetitors.userProductCompetitor;
		variables.Model=qryGetOppProductCompetitors.Model;
		variables.UnitCost=qryGetOppProductCompetitors.UnitCost;
		variables.Notes=qryGetOppProductCompetitors.Notes;
		variables.Description=qryGetOppProductCompetitors.Description;
		variables.oppProductCompetitorID=qryGetOppProductCompetitors.oppProductCompetitorID;
	</cfscript>
<cfelseif variables.frmTask is "deleteCompetitive">
	<cfscript>
		// create an instance of the opportunity component
		if (not isdefined("myopportunity"))
		{
if (fileexists(application.paths.code & "\cftemplates\relayOpportunity.cfc")) {
	componentPath = "code.CFTemplates.relayOpportunity";
	myopportunity = createObject("component",componentPath);}
else {myopportunity = createObject("component","relay.com.opportunity");}
			myopportunity.dataSource = application.siteDataSource;
		}
		myopportunity.opportunityID = variables.opportunityID;
		myopportunity.oppProductCompetitorID = url.oppProductCompetitorID;
		myopportunity.deleteOppProductCompetitor();
	</cfscript>
</cfif>

<cfif variables.frmTask neq "" and variables.frmTask neq "editFormCompetitive">
		<!--- START: 2009/11/11	AJC P-LEX039 Competitive products --->
	<cfif request.relayCurrentUser.isInternal>
		<cfset urlRedirect="oppProductListCompetitive.cfm?frmOppTypeID=#variables.frmOppTypeID#&opportunityID=#variables.opportunityID#&oppProductID=#variables.oppProductID#">
	</cfif>
	
	<cfif urlRedirect neq "">
		<cflocation url="#urlRedirect#" addtoken="no">
		<CF_ABORT>
	</cfif>
</cfif>

<cfscript>
	// create an instance of the opportunity component
	if (not isdefined("myopportunity"))
	{
if (fileexists(application.paths.code & "\cftemplates\relayOpportunity.cfc")) {
	componentPath = "code.CFTemplates.relayOpportunity";
	myopportunity = createObject("component",componentPath);}
else {myopportunity = createObject("component","relay.com.opportunity");}
	myopportunity.dataSource = application.siteDataSource;
	}
	myopportunity.opportunityID = variables.opportunityID;
	myopportunity.oppProductID = variables.oppProductID;
	myopportunity.getProductCompetitors();
	myopportunity.getproductCompetitorProductType();
	myopportunity.oppProductCompetitorID = "";
	myopportunity.getOppProductCompetitors();
</cfscript>
<cf_translate>
<cfif variables.oppProductID eq 0>
	phr_oppProductCompetitorProducts_noOppProductIDDefined
<cfelse>
	<cfif request.relaycurrentUser.isInternal>
		<cfif isdefined('frmOppTypeID')>
			<cfset variables.backURL = "oppProductList.cfm?OpportunityID=#variables.opportunityID#&frmOppTypeID=#frmOppTypeID#">
		<cfelse>
			<cfset variables.backURL = "oppProductList.cfm?OpportunityID=#variables.opportunityID#">
		</cfif>
	</cfif>
	<cfform action="#variables.backURL#" method="post" name="frmOppProduct">
		<table width="100%" border="0" cellspacing="1" cellpadding="3" bgcolor="White">
			<tr>
				<td>phr_oppProductAddCompetitor</td>
				<td align="right"><input type="submit" name="frmBackToOppPrroduct" value="phr_oppProductCompetitorBack">
				<cfoutput><CF_INPUT type="hidden" name="opportunityID" value="#variables.opportunityID#"></cfoutput></td>
			</tr>
		</table>
	</cfform>
	<cfif request.relaycurrentUser.isInternal>
		<cfif isdefined('frmOppTypeID')>
			<cfset variables.compURL = "oppProductList.cfm?OpportunityID=#variables.opportunityID#&frmOppTypeID=#frmOppTypeID#">
		<cfelse>
			<cfset variables.compURL = "oppProductList.cfm?OpportunityID=#variables.opportunityID#">
		</cfif>
		<cfif isdefined('frmOppTypeID')>
			<cfset variables.keyColumnURLList="oppProductListCompetitive.cfm?frmTask=editFormCompetitive&frmOppTypeID=#frmOppTypeID#&OpportunityID=#variables.opportunityID#&OppProductID=#variables.OppProductID#&OppProductCompetitorID=,oppProductListCompetitive.cfm?frmTask=deleteCompetitive&frmOppTypeID=#frmOppTypeID#&OpportunityID=#variables.opportunityID#&OppProductID=#variables.OppProductID#&OppProductCompetitorID=">
		<cfelse>
		<cfset variables.keyColumnURLList="oppProductListCompetitive.cfm?frmTask=editFormCompetitive&OpportunityID=#variables.opportunityID#&OppProductID=#variables.OppProductID#&OppProductCompetitorID=,oppProductListCompetitive.cfm?frmTask=deleteCompetitive&OpportunityID=#variables.opportunityID#&OppProductID=#variables.OppProductID#&OppProductCompetitorID=">
	</cfif>
	</cfif>
	<cfform action="#variables.compURL#" method="post" name="frmCompetitiveProduct">
		<table border="0" cellspacing="1" cellpadding="3" align="center" bgcolor="White">
			<tr>
				<th align="left">phr_opp_product_Competitor</th>
				<th align="left">phr_opp_product_Type</th>
				<th align="left">phr_opp_product_Model</th>
				<th align="left">phr_opp_product_UnitCost</th>
			</tr>
			<tr bgcolor="white">
				<td valign="top">
					<select name="frmProductCompetitorID">
						<option value="0"<cfif variables.ProductCompetitorID eq 0> selected</cfif>>(phr_oppProductCompetitorSelect)</option>
						<cfoutput query="myopportunity.qProductCompetitors"><option value="#ProductCompetitorID#"<cfif variables.ProductCompetitorID eq ProductCompetitorID> selected</cfif>>#htmleditformat(CompetitorName)#</option></cfoutput>
					</select>
				</td>
				<td valign="top">
					<select name="frmproductCompetitorProductTypeID">
						<option value="0"<cfif variables.productCompetitorProductTypeID eq 0> selected</cfif>>(phr_oppProductCompetitorPtoductTypeSelect)</option>
						<cfoutput query="myopportunity.qproductCompetitorProductType"><option value="#productCompetitorProductTypeID#"<cfif variables.productCompetitorProductTypeID eq productCompetitorProductTypeID> selected</cfif>>#htmleditformat(ProductType)#</option></cfoutput>
					</select>
				</td>
				<td valign="top">
					<cf_translate encode="javascript">
					<cfinput name="frmModel" type="text" value="#variables.Model#" maxlength="255" required="true" message="phr_opp_product_PleaseEnterAModel">
					</cf_translate>
				</td>
				<td valign="top">
					<cf_translate encode="javascript">
					<cfinput name="frmUnitCost" type="text" value="#variables.UnitCost#" maxlength="255" required="true" message="phr_opp_product_PleaseEnterAUnitPrice" validate="float">
					</cf_translate>
				</td>
			</tr>
			<tr>
				<td>phr_oppProductCompetitorManual</td>
				<th align="left">phr_opp_product_Notes</th>
				<th align="left">phr_opp_product_Description</th>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>
					<cfinput name="frmUserProductCompetitor" type="text" value="#variables.UserProductCompetitor#" maxlength="40">
				</td>
				<td valign="top">
					<cfoutput><cftextarea name="frmNotes" id="frmNotes">#htmleditformat(variables.Notes)#</cftextarea></cfoutput>
				</td>
				<td valign="top">
					<cfoutput><cftextarea name="frmDescription" id="frmDescription">#htmleditformat(variables.Description)#</cftextarea></cfoutput>
				</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td colspan="3">&nbsp;
					<cfoutput><CF_INPUT type="hidden" name="opportunityID" value="#variables.opportunityID#">
					<CF_INPUT type="hidden" name="oppProductID" value="#variables.oppProductID#">
					<cfif isdefined("frmOppTypeID")>
						<CF_INPUT type="hidden" name="frmOppTypeID" value="#frmOppTypeID#">
					<cfelseif isdefined("OppTypeID")>
						<CF_INPUT type="hidden" name="frmOppTypeID" value="#OppTypeID#">
					</cfif>
					</cfoutput>
				</td>
				<td>
					<cfif variables.frmTask is "editFormCompetitive">
						<cfoutput><CF_INPUT type="hidden" name="frmOppProductCompetitorID" value="#variables.oppProductCompetitorID#"></cfoutput>
						<input type="submit" name="editProduct" value="phr_oppProductCompetitorEditSubmit">
						<input type="hidden" name="frmTask" value="editCompetitive">
					<cfelse>
						<input type="submit" name="addProduct" value="phr_oppProductCompetitorAddSubmit">
						<input type="hidden" name="frmTask" value="addCompetitive">
					</cfif>
				</td>
			</tr>
		</table>
	</cfform>
	phr_oppProductCurrentCompetitor
	
	<cf_tablefromqueryobject 
		queryObject="#myopportunity.qOppProductCompetitors#"
		queryName="myopportunity.qOppProductCompetitors"
		sortOrder=""
		
		keyColumnList="editlink,removelink"
		keyColumnURLList="#keyColumnURLList#"
		keyColumnKeyList="OppProductCompetitorID,OppProductCompetitorID"
		
		hideTheseColumns="OppProductCompetitorID"
		showTheseColumns="ProductCompetitorName,producttype,Model,unitcost,Notes,Description,editlink,removelink"

		columnTranslation="true"
		useInclude=0
		HidePageControls="true"
		allowColumnSorting="no"
	>
</cfif>
</cf_translate>
<cfif request.relaycurrentUser.isInternal>
	
	
</cfif>
