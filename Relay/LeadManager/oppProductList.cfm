<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Description
File name:		oppProductList.cfm
Author:			SWJ
Date started:		May-03
		
--->

<!---  Amendment History:

Date (DD-MMM-YYYY)	Initials	Code	What was changed

29-Sep-2004			ICS			AddingOfMultipleProducts-nospacesbecausemyspacebardoesn'twork

23-Aug-2004			GCC			Removed if...else arround update code to enable 
product list to display following an update. oppSpecialPricing.cfc updated with 
getOppPricing variable initialisation to resolve a conflict between a query and 
function of the same name and to correct percentage discount calculation. 
Modified function reloadParent() to strip off any characters after and including
 # (strip anchor off before doing parent.location).
 
14-Nov-2003			KAP			Clone opportunity product added
15-DEC-2004			JC			Added unitprice trap to prevent div by zero

02-MAR-2005			AJC			CR_ATI002_superAM	Added Super Account manager functionality - IF a user is a 
SuperAM then if requesting a special price on a lead that they are not the account manager on then
they are automatically added to the Viewing Rights for the lead. IF they add a normal product then
nothing will happen.

04-MAR-2005			AJC			CR_ATI003_currentprices		Added function to update oppProduct prices to the latest price in the product Table.

11-MAY-2005			SWJ/AJC		VIP		Added OppType functionality

07-JUN-2005			AJC			CR_ATI010 Product Set Functionality

15-AUG-2005			GCC			CR_ATI025 Product Status Visibility

WAB 2006-01-25 Remove form.userType     

20-Apr-2006			GCC			CR_ATI032 Price Difference
2006-06-29      WAB			restrictKeystrokes function altered to handle firefox
2008-11-20			NJH		All Sites Issue 1372 bug fixing.. changed the attribute name of thisFormname to formname for the relayDateField customtag.
							Also added a hack (which had to be done in another place I noticed) to stop form post on frame refresh.. not really sure
							why this had to be put in here, but it seems to work. 
2009-02-23			NJH 	Bug Fix Issue T-10 838- added id's for descriptionEdit and QuantityEdit to give a handle for the input fields
2009-02-27			NJH		Bug Fix All Sites Issue 1894 - products show for all types, unless set up otherwise in the INI
2009-09-17			NJH		LID 2275  - only do a cflocation and abort if on internal site. Also added hidden fields (NYB)

2009-11-01			AJC		P-LEX039 - Competitive products and Partner Price
2010-02-25			NAS			P-LEX039 - Demo checkbox addition for 'Deal Registration'
2010-06-09			NAS		LID 3174 - SPQ 100% Discount Broken
2010-07-07 			NYB		Lexmark LHID 3636 - 
2010-07-13 NYB Lexmark LHID 3636
2011-02-01 			PPB 	LID5382 (and LID5383) frmUnitPriceEdit form field was defined twice; duplicate seemed to cause an intermittent problem cos the field was 'undefined' in javascript function
2011-02-04			PPB		LID5587	- moved definition of form frmReasonForm inside cfoutput cos javascript references to it weren't being recognised (presumably an enableCFoutputOnly is in effect)
2011-05-26 			PPB 	REL106 added EditLeadEID as a setting
2011-11-28			NYB		LHID8224 changed the text in the js popups to use translatable phrases
2011-12-07			NYB		LHID8224b changed the way translations are done in js
2014-01-31	WAB 	removed references to et cfm, can always just use / or /?

Possible enhancements:


SWITCHES Available: (work in progress list) (AJC 2009-11-30)
useOppProductCompetitive = "yes/no" Allows users to add competitive product(s) against a opp product line item

--->

<cfparam name="MakeForecastShipDateMandatory" type="boolean" default="false">
<cfparam name="form.specialPricingStatusMethod" default="">
<cfparam name="form.frmtask" default="">
<cfparam name="url.probability" type="string" default="0">
<cfparam name="url.countryID" type="string" default="0">
<cfparam name="form.probability" type="string" default="0">
<cfparam name="form.frmProductDeletionList" type="string" default="">
<cfparam name="form.frmSPManualEdit" type="string" default="">
<cfparam name="phraseprefix" type="string" default="">
<!--- 2006-08-13 SWJ Added these params here so that the don't have to be in opportunityINI.cfm --->
<cfparam name="useOppFunctions" type="boolean" default="false">
<cfparam name="useOppProductPercentDiscount" type="boolean" default="false">
<cfparam name="useOppProductQuantityEdit" type="boolean" default="false">
<cfparam name="useButtons" type="boolean" default="false">
<cfparam name="currencyMask" type="string" default="_________.__">		<!--- PPB 2010-05-21 I've used this mask cos its the one used in tableFromQueryObject --->
<!--- START: 2009-11-06	AJC	P-LEX039 - Competitive pricing - Added default for page redirect --->
<cfparam name="useOppProductCompetitive" default="false">
<cfparam name="useOppProductPartnerPrice" default="false">
<cfparam name="useOppProductApprovedPrice" default="false">
<cfparam name="urlRedirect" type="string" default="">
<cfparam name="showOppProductInclude" type="boolean" default="true">
<cfparam name="useOppProductStockLocation" type="boolean" default="false">
<cfparam name="useOppProductSpecialPricing" type="boolean" default="false">
<cfparam name="productSelector" default="">

<!--- END: 2009-11-06	AJC	P-LEX039 - Competitive pricing - Added default for page redirect --->
<cfparam name="currencyMask" type="string" default="_________.__">		<!--- PPB 2010-05-21 I've used this mask cos its the one used in tableFromQueryObject --->
<!--- 
<cfset MakeForecastShipDateMandatory = false />
 --->

<!--- ********************************************************************* --->
<!--- get current users user groups --->
<!--- ********************************************************************* --->
<cfquery name="qUserGroupMembership" datasource="#application.siteDataSource#">
	SELECT g.Name
	FROM RightsGroup r
	LEFT OUTER JOIN UserGroup g ON g.UserGroupID = r.UserGroupID
	WHERE r.PersonID =  <cf_queryparam value="#ListFirst( cookie["user"], "-" )#" CFSQLTYPE="CF_SQL_INTEGER" > 
</cfquery>
<cfset permissionSpecialPriceApproval = ListFindNoCase( ValueList( qUserGroupMembership.name ), "Approvals" ) neq 0>
<cfset permissionSpecialPriceApproval = true> <!--- testing - let anyone in --->
<!--- ********************************************************************* --->

<cf_translate>

<cfif request.relaycurrentUser.isInternal>
	
	<cf_head>
		<cf_title>Opp ProductList</cf_title>
	<cfoutput>
		<cfif isdefined("request.partnerFacingStylesheet")>
			<LINK REL="stylesheet" HREF="/code/styles/#request.partnerFacingStylesheet#.css">
		</cfif>
	</cfoutput>
	<!-- meta anti cache--> 
		<META HTTP-EQUIV="Expires" CONTENT="Mon, 06 Jan 1990 00:00:01 GMT"> 
		<META HTTP-EQUIV="Pragma" CONTENT="no-cache"> 
		<META HTTP-EQUIV="cache-control" VALUE="no-cache, no-store, must-revalidate">
	</cf_head>
</cfif>

<SCRIPT type="text/javascript">
function reloadParent() { 
	var stringToParse = "" + window.parent.location + "";
	var myArray = stringToParse.split("#");
	if (myArray.length != 0) { //detected a #
		self.parent.location.href = myArray[0];
	} else {
		self.parent.location.href = window.parent.location;
	}
}
</script>

<cfparam name="form.sortOrder" type="string" default="SKU ASC">

<cfif request.relaycurrentUser.isInternal>
	<cfoutput>
	<cf_body onLoad="if ('#htmleditformat(form.specialPricingStatusMethod)#' != '' || '#htmleditformat(form.frmtask)#' != '') reloadParent();">
	</cfoutput>
<cfelse>
	<cfif form.sortOrder eq "account">
		<cfset form.sortorder = "SKU ASC">
	</cfif>
</cfif>

<cfif not IsDefined("opportunityID")>
	Cannot load oppProductList without an opportunityID
	<CF_ABORT>
</cfif>

<cfif isDefined("form.specialPricingStatusMethod") and form.specialPricingStatusMethod eq "SP_Calculate" and request.relaycurrentUser.isInternal>
	<CFOUTPUT>
	phr_sys_LoadingMessage
	</CFOUTPUT>

	<cfparam name="desiredDiscount" default="">
	<cfparam name="desiredBudget" default="">
	
	<cfquery Name="getSPExpiryDate" datasource="#application.siteDataSource#">
		SELECT SPExpiryDate	FROM opportunity where opportunityID =  <cf_queryparam value="#opportunityID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</cfquery>
	
	<cfinvoke component="relay.com.oppSpecialPricing" method="updateSpecialPricing" returnvariable="message">
		<cfinvokeargument name="dataSource" value="#application.siteDataSource#"/>
		<cfinvokeargument name="opportunityID" value="#opportunityID#"/>
		<cfinvokeargument name="specialPricingStatusMethod" value="#form.specialPricingStatusMethod#"/>
		<cfinvokeargument name="personID" value="#request.relayCurrentUser.personid#"/>
		<cfinvokeargument name="desiredDiscount" value="#desiredDiscount#"/>
		<cfinvokeargument name="desiredBudget" value="#desiredBudget#"/>
		<cfinvokeargument name="SPExpiryDate" value="#getSPExpiryDate.SPExpiryDate#"/>
	</cfinvoke>

	<cfset message = message>
	
	<!--- --------------------------------------------------------------------- --->
	<!--- nasty client-side redirect to stop form post on frame refresh  - added by NJH 2008-11-20  All Sites Issue 1372 bug fixing - copied from below... it works, but not pretty....--->
	<!--- --------------------------------------------------------------------- --->
	<!--- LID 2275 NJH 2009-09-17 - only do this for internal site --->
	<cfif request.relayCurrentUser.isInternal>
		<cflocation url="#cgi["script_name"]#?#request.query_string#" addtoken="false">
		<CF_ABORT>
	</cfif>
<cfelse>
	<cfif isDefined("form.specialPricingStatusMethod") and form.specialPricingStatusMethod eq "SP_Calculate" and request.relaycurrentUser.isInternal>
		<CFOUTPUT>
			phr_sys_LoadingMessage
		</CFOUTPUT>
	
		<cfparam name="desiredDiscount" default="">
		<cfparam name="desiredBudget" default="">
		
		<cfquery Name="getSPExpiryDate" datasource="#application.siteDataSource#">
			SELECT SPExpiryDate	FROM opportunity where opportunityID =  <cf_queryparam value="#opportunityID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfquery>
		
		<cfinvoke component="relay.com.oppSpecialPricing" method="updateSpecialPricing" returnvariable="message">
			<cfinvokeargument name="dataSource" value="#application.siteDataSource#"/>
			<cfinvokeargument name="opportunityID" value="#opportunityID#"/>
			<cfinvokeargument name="specialPricingStatusMethod" value="#form.specialPricingStatusMethod#"/>
			<cfinvokeargument name="personID" value="#request.relayCurrentUser.personid#"/>
			<cfinvokeargument name="desiredDiscount" value="#desiredDiscount#"/>
			<cfinvokeargument name="desiredBudget" value="#desiredBudget#"/>
			<cfinvokeargument name="SPExpiryDate" value="#getSPExpiryDate.SPExpiryDate#"/>
		</cfinvoke>
	
		<cfset message = message>
	</cfif>

	<cfparam name="url.opportunityID" type="string" default="0">
	<!--- START: 2009-12-11	AJC P-LEX039 Competitive products --->
	<cfparam name="form.opportunityID" default="0">
	
	<cfif form.opportunityID neq 0>
		<cfset url.opportunityID = form.opportunityID>
	</cfif>
	<!--- END: 2009-12-11	AJC P-LEX039 Competitive products --->
	<cfscript>
		// create an instance of the opportunity component
		if (fileexists(application.paths.code & "\cftemplates\relayOpportunity.cfc"))
		{
		componentPath = "code.CFTemplates.relayOpportunity";
		myopportunity = createObject("component",componentPath);
		}
		else {
		myopportunity = createObject("component","relay.com.opportunity");
		}
		myopportunity.dataSource = application.siteDataSource;
		myopportunity.opportunityID = opportunityID;	
		myopportunity.get(); //NYB 2010-07-07 Lexmark LHID 3636: moved from line 430
		//NYB 2010-07-07 Lexmark LHID 3636: added if's around the following:
		if(isDefined("url.countryID") and url.countryID neq 0) {
			myopportunity.countryID = url["countryID"];
		}
		if(isDefined("form.sortOrder")) {
			myopportunity.sortOrder = form["sortOrder"];
		}
		myopportunity.productCatalogueCampaignID = application.com.settings.getSetting("leadManager.products.productCatalogueCampaignID");
	</cfscript>
		


<!--- START: 2009-12-10	AJC P-LEX039 Competitive products --->
<cfparam name="form.oppProductID" default="0">
<cfparam name="url.oppProductID" default="0">
<cfparam name="variables.oppProductID" default="0">
<cfparam name="url.frmTask" default="">
<cfparam name="form.frmTask" default="">
<cfparam name="variables.frmTask" default="">

<cfif url.frmTask neq "">
	<cfset variables.frmTask = url.frmTask>
<cfelseif form.frmTask neq "">
	<cfset variables.frmTask = form.frmTask>
</cfif>

<cfif isdefined("variables.frmTask") and 
		(variables.frmtask eq "addCompetitive"
			or
				variables.frmtask eq "editCompetitive"
					or
						variables.frmtask eq "editFormCompetitive"
							or
								variables.frmtask eq "deleteCompetitive"
									or
										variables.frmtask eq "viewCompetitive")>
	<cfif url.oppProductID gt 0>
		<cfset variables.oppProductID = url.oppProductID>
	<cfelseif form.oppProductID gt 0>
		<cfset variables.oppProductID = form.oppProductID>
	</cfif>

	<cfinclude template="\leadManager\oppProductListCompetitive.cfm">							
	<cfset showOppProductInclude = false>
</cfif>
<!--- END: 2009-12-10	AJC P-LEX039 Competitive products --->

<cfif isdefined("form.frmTask") and form.frmtask eq "addproduct">
<!--- this checks to see if a product needs adding --->
	<cfscript>
	myopportunity.quantity = FORM.quantity;
	//myopportunity.Status = form["status"];
	myopportunity.productID = FORM.productID;
	myopportunity.createdBy = request.relayCurrentUser.usergroupid;
	myopportunity.lastUpdatedBy = request.relayCurrentUser.usergroupid;
//	if ( form["forecastShipDate"] neq "" )
//		myopportunity.forecastShipDate = CreateDate( left( form["forecastShipDate"], 4 ), mid( form["forecastShipDate"], 6, 2 ), right( form["forecastShipDate"], 2 ) );
//	else
//		myopportunity.forecastShipDate = "";
	myopportunity.forecastShipDate = form["forecastShipDate"] ;
	myopportunity.probability = form["probability"];
	// Start 2010-02-25			NAS			P-LEX039 - Demo checkbox addition for 'Deal Registration'
	if(isdefined("form.frmDemo"))
	{
		myopportunity.Demo = 1;
	}
	//  End
	myopportunity.specialPrice = form["specialPrice"];
	myopportunity.stockLocation = form["frmStockLocation"];
	myopportunity.SPManualEdit = form["frmSPManualEdit"];

	// START: 2009-11-11	AJC P-LEX039 Competitive products
	myopportunity.approvedPrice = form["approvedPrice"];
	if(isdefined("form.frmUnitPrice"))
	{
		myopportunity.unitPrice = form["frmUnitPrice"];
	}
	qryAddOppProduct = myopportunity.addOppProduct();
	// END: 2009-11-11	AJC P-LEX039 Competitive products
	if(myopportunity.specialPrice neq ""){
	myopportunity.SuperAMAssign();
	}
	// trigger process action if special price has been requested
	//if ( form["frmSpecialPriceReason"] neq "" )
	//{
	//	myopportunity.OppProductSpecialPriceReason = form["frmSpecialPriceReason"];
	//	myopportunity.OppProductSpecialPriceRequest();
	//}
	</cfscript>
<!--- --------------------------------------------------------------------- --->
<!--- nasty client-side redirect to stop form post on frame refresh --->
<!--- --------------------------------------------------------------------- --->
	<!--- LID 2275 NJH 2009-09-17 - only do this for internal site --->
<!--- START: 2009-11-11	AJC P-LEX039 Competitive products --->
	<cfif request.relayCurrentUser.isInternal>
		<cfif useOppProductCompetitive>	
			<cfif isdefined("frmOppTypeID")>
				<cfset urlRedirect="oppProductListCompetitive.cfm?opportunityID=#form.opportunityID#&OppProductID=#qryAddOppProduct.OppProductID#&frmOppTypeID=#frmOppTypeID#">
			<cfelse>
				<cfset urlRedirect="oppProductListCompetitive.cfm?opportunityID=#form.opportunityID#&OppProductID=#qryAddOppProduct.OppProductID#">
			</cfif>
		<cfelse>
		<cfset urlRedirect="#cgi["script_name"]#?#request.query_string#">
	</cfif>
	</cfif>
	<cfif urlRedirect neq "">
		<cflocation url="#urlRedirect#" addtoken="no">
	<CF_ABORT>
	<cfelseif useOppProductCompetitive>
		<cfset variables.OppProductID = qryAddOppProduct.OppProductID>
		
		<cfinclude template="\leadManager\oppProductListCompetitive.cfm">
		<cfset showOppProductInclude = false>
	</cfif>
<!--- END: 2009-11-11	AJC P-LEX039 Competitive products --->	
<!--- --------------------------------------------------------------------- --->
</cfif>
	
<!--- <cfif isdefined("form.frmTask") and form.frmtask eq "removeProduct">
	<cfloop index="deleteID" list="#form["frmProductDeletionList"]#">
		<cfscript>
		myopportunity.oppProductID = deleteID;
		myopportunity.removeOppProduct();
		myopportunity.oppProductID = "";
		</cfscript>
	</cfloop>
</cfif> --->

<cfif isdefined("form.frmTask") and form.frmtask eq "editProduct"> 
	<cfscript>
	myopportunity.quantity = form["frmQuantityEdit"];
	myopportunity.oppProductID = form["frmOppProductIDEdit"];
	myopportunity.lastUpdatedBy = request.relayCurrentUser.usergroupid;
//	if ( form["frmForecastShipDateEdit"] neq "" )
//		myopportunity.forecastShipDate = CreateDate( mid( form["frmForecastShipDateEdit"], 7, 4 ), mid( form["frmForecastShipDateEdit"], 4, 2 ), left( form["frmForecastShipDateEdit"], 2 ) );
//	else
//		myopportunity.forecastShipDate = "";
	myopportunity.forecastShipDate =form["frmForecastShipDateEdit"] ; 
	myopportunity.probability = form["frmProbabilityEdit"];
	// P-LEX039 Start
	if(isdefined("form.frmDemoEdit"))
	{
		myopportunity.Demo = 1;
	}
	// P-LEX039 End
	myopportunity.specialPrice = form["frmSpecialPriceEdit"];
	myopportunity.stockLocation = form["frmStockLocationEdit"];
	myopportunity.SPManualEdit = form["frmSPManualEdit"];
	// START: P-LEX039
	myopportunity.unitPrice = form["frmUnitPriceEdit"];
	myopportunity.approvedPrice = form["frmApprovedPriceEdit"];
	// END: P-LEX039
	myopportunity.updateOppProduct();
	if(myopportunity.specialPrice neq ""){
	myopportunity.SuperAMAssign();
	}
	// trigger process action if special price has been requested
	//if ( form["frmSpecialPriceReason"] neq "" )
	//{
	//	myopportunity.OppProductSpecialPriceReason = form["frmSpecialPriceReason"];
	//	myopportunity.OppProductSpecialPriceRequest();
	//}
	myopportunity.oppProductID = "";
	</cfscript>
	
	<!--- --------------------------------------------------------------------- --->
	<!--- nasty client-side redirect to stop form post on frame refresh - 8.1 Bug Fix NJH 2009-02-16 --->
	<!--- --------------------------------------------------------------------- --->
	<!--- LID 2275 NJH 2009-09-17 - only do this for internal site --->
<!--- START: 2009-11-11	AJC P-LEX039 Competitive products --->
	<cfif request.relayCurrentUser.isInternal>
		<cfset urlRedirect="#cgi["script_name"]#?#request.query_string#">
	</cfif>
	
	<!--- ><cfif useOppProductCompetitive>	
		<cfset urlRedirect="oppProductListCompetitive.cfm?opportunityID=#form.opportunityID#">
	</cfif> --->
	
	<cfif urlRedirect neq "">
		<cflocation url="#urlRedirect#" addtoken="no">
		<CF_ABORT>
	</cfif>
<!--- END: 2009-11-11	AJC P-LEX039 Competitive products --->	
</cfif>
	
<!--- START: 2009-11-11	AJC P-LEX039 Competitive products --->
<cfif showOppProductInclude>
<!--- END: 2009-11-11	AJC P-LEX039 Competitive products --->
<cfscript>
	myopportunity.getOppProducts();
	myopportunity.get();
// probability selects
	myopportunity.OppProductProbabilitySelectName = "frmProbabilityEdit";
	myopportunity.OppProductProbabilitySelectDefault = url["probability"];
	myopportunity.OppProductProbabilitySelectOnchange = "eventOppProductEditOnChange(this);";
	myopportunity.OppProductProbability();
	OppProductProbabilitySelectEdit = myopportunity.OppProductProbabilitySelect;

	myopportunity.OppProductProbabilitySelectName = "Probability";
	myopportunity.OppProductProbabilitySelectDefault = url["probability"];
	myopportunity.OppProductProbabilitySelectOnchange = "";
	myopportunity.OppProductProbability();
	OppProductProbabilitySelectAdd = myopportunity.OppProductProbabilitySelect;

	myopportunity.currencySign = application.com.currency.getCurrencyByISOCode(myopportunity.currency).currencysign;
</cfscript>

<cfif myopportunity.OppTypeID is 2>
	<!--- NJH 2009-02-27 Bug Fix All Sites Issue 1894 - products should show for all types, unless set up otherwise in the INI 
	<cfset showAddProduct=false>
	<cfset showDuplicate=false>
	<cfset useOppProductShipDate=false>
	<cfset useOppProductProbability=false>
	<cfset showIllustrativePrice=true>--->
	<cfif myopportunity.stageID is 12>
		<cfset showEditOppProduct=true>
		<cfset showCalcOppProduct=true>
	<cfelse>
		<cfset showEditOppProduct=false>
		<cfset showCalcOppProduct=false>
	</cfif>
</cfif>

<!--- NJH 2009-02-27 Bug Fix All Sites Issue 1894 - param'd these variables. They were currently in an else statement if oppTypeId was not 2 --->
	<cfparam name="showDuplicate" default="true">
	<cfparam name="useOppProductShipDate" default="true">
	<cfparam name="useOppProductProbability" default="true">
	<cfparam name="showIllustrativePrice" default="false">
	<cfparam name="showEditOppProduct" default="true">
	<cfparam name="showCalcOppProduct" default="true">
<cfparam name="showAddProduct" default="true"> 
<cfparam name="useProductStatus" default="false">

<cfinclude template="/templates/relayFormJavaScripts.cfm">
<cfif showIllustrativePrice>
	<cfscript>
		toCurrency = application.com.relayEcommerce.getCurrencies(request.relaycurrentuser.countryID);
	</cfscript>
</cfif>

<cf_translate encode="javascript">
<SCRIPT type="text/javascript">
	function addproduct() {
		
		var errorMessage = "";
		if ( document.forms["oppProductForm"]["productGroupID"].selectedIndex == 0 || document.forms["oppProductForm"]["thisProductID"].selectedIndex == 0 )
		{
			errorMessage = "phr_opp_PleaseSelectAProduct";
		}
		else if ( !(document.oppProductForm.quantity.value > 0) )
		{
			errorMessage = "phr_opp_SpecifyAQuantity";
		}
		else if ( document.oppProductForm.quantity.value > 0 )
		{
//			errorMessage = verifyInteger( document.oppProductForm.quantity.value, "number", 2 );
		}

		<cfif MakeForecastShipDateMandatory>
			if ( document.forms["oppProductForm"]["forecastShipDate"].value == "" )
			{
				errorMessage = "Phr_oppProductShipDate_Mandatory";
			}
		</cfif>

		if ( errorMessage == "" )
		{
			document.oppProductForm.productID.value = document.oppProductForm.thisProductID.value;
			document.oppProductForm.frmTask.value = "addproduct";
			document.oppProductForm.submit();
			
			// SWJ commented out the two lines below so that the approval screen is not popped up here.
			// it needs to be done in the opportunity edit place.
			//if ( document.forms["oppProductForm"]["specialPrice"].value == "" ) document.oppProductForm.submit()
			//else specialPriceReasonWindow( document.forms["oppProductForm"]["thisProductID"][document.forms["oppProductForm"]["thisProductID"].selectedIndex].text );
		} else {
			alert( errorMessage );
		}
	}
	<!--- 2005-06-07 AJC START:CR_ATI010 - Product Sets --->
	function addproductset() {
		<cfoutput>window.open('oppProductSet.cfm?opportunityid=#myopportunity.opportunityID#<cfif isdefined('frmOppTypeID')>&frmOppTypeID=#frmOppTypeID#</cfif><cfif isdefined('OppTypeID')>&frmOppTypeID=#OppTypeID#</cfif>','oppProductSet','menubar=no,toolbar=no,resizable=yes,width=300,height=500');</cfoutput>
	}
	<!--- 2005-06-07 AJC END:CR_ATI010 - Product Sets --->
	
<!--- ICS 26.10.04 - addmultipleproducts added - copy of addProducts with some tweaks --->
	function addmultipleproducts() {
		var errorMessage = "";
		if ( document.forms["oppProductForm"]["productGroupID"].selectedIndex == 0 || document.forms["oppProductForm"]["thisProductID"].selectedIndex == 0 )
		{
			errorMessage = "phr_opp_PleaseSelectAProduct";
		}

		if ( errorMessage == "" )
		{
			productID = document.oppProductForm.thisProductID.value;
			opportunityID = document.oppProductForm.opportunityID.value;
			openWin( 'oppProductAddMultipleProducts.cfm?oppProductID=' + productID + '&opportunityID=' + opportunityID + '&probability=25' ,'PopUp', 'left=' + parseInt(screen.availWidth * 0.2)  + ', width=450, top=' + parseInt(screen.availHeight * 0.1)  + ', height=' + parseInt(screen.availHeight * 0.7) + ', toolbar=0, location=0, directories=0, status=1, menuBar=0, scrollBars=1, resizable=0'); 
		} else {
			alert( errorMessage );
		}
	}
	
/*
	function removeProduct(oppProductID) {
		if (oppProductID > 0) {
			document.oppProductForm.oppProductID.value = oppProductID;
			document.oppProductForm.frmTask.value = "removeProduct";
			document.oppProductForm.submit();
		} else {
		alert("You cannot remove this product");
		}
	}
*/
function saveSpecialPricing(action)
{
	//alert(action);
  document.frmReasonForm.specialPricingStatusMethod.value = action;
  var doThis = "yes";
  //alert(document.frmReasonForm.specialPricingStatusMethod.value);
  if (action == "SP_Calculate")
  	{
	document.frmReasonForm.frmComments.value = "Budget changed to " + document.frmReasonForm.newBudget.value
	}
  if (doThis == "yes" )
  	document.frmReasonForm.submit();
}


function calcMyDiscount() {
	document.frmReasonForm.SP_SaveBudget.disabled = false;
	document.frmReasonForm.newBudget.value = document.frmReasonForm.desiredBudget.value;
	//document.frmReasonForm.desiredDiscount.disabled = true;
	document.frmReasonForm.desiredDiscount.value = '';
	var captionElement = document.getElementById("resultText");
	captionElement.innerHTML = "Resulting percentage discount";
	if (document.frmReasonForm.fullPrice.value > 0)
	{
	document.frmReasonForm.newBudget.value =  
		(eval(Math.round(100 - ((((document.frmReasonForm.newBudget.value / document.frmReasonForm.fullPrice.value) * 100)*100)/100)))).toFixed(2);
}
	else
	{
		alert("Project full price cannot be reduced below 0.");
	}
}
  
function calcMyBudget() {
	document.frmReasonForm.newBudget.value = 
		(eval(Math.round( document.frmReasonForm.fullPrice.value * ((100-document.frmReasonForm.desiredDiscount.value) / 100) ))).toFixed(2);
	document.frmReasonForm.SP_SaveBudget.disabled = false;
	//document.frmReasonForm.desiredBudget.disabled = true;
	document.frmReasonForm.desiredBudget.value = '';
	var captionElement = document.getElementById("resultText");
	captionElement.innerHTML = "Resulting budget value";
}

function resetCalculator(){
	document.frmReasonForm.SP_SaveBudget.disabled = true;
	document.frmReasonForm.desiredBudget.disabled = false;
	document.frmReasonForm.desiredDiscount.disabled = false;
	document.frmReasonForm.calcDiscount.disabled = true;
	document.frmReasonForm.calcBudget.disabled=true;
	document.frmReasonForm.desiredBudget.value = '';
	document.frmReasonForm.desiredDiscount.value = '';
	document.frmReasonForm.newBudget.value = '';
	eventOppProductEditClear();
}

function CheckIfDesiredBudgetEmpty(){
	if (document.frmReasonForm.desiredBudget.value == "")
		document.frmReasonForm.calcDiscount.disabled=true
	else
		{
		document.frmReasonForm.calcDiscount.disabled=false;
		//document.frmReasonForm.desiredDiscount.disabled = true;
		}
}
		
function CheckIfDesiredDiscountEmpty(){
	if (document.frmReasonForm.desiredDiscount.value == "")
		document.frmReasonForm.calcBudget.disabled=true
	else
		document.frmReasonForm.calcBudget.disabled=false
}

function CheckIfFrmCommentsEmpty(){
	if (document.frmReasonForm.frmComments.value == "")
		document.frmReasonForm.specilPriceRequest.disabled=true
	else
		document.frmReasonForm.specilPriceRequest.disabled=false
}

function calcLineItemDiscount(thisObject) {
	var myForm = thisObject.form
	
			// 2010-06-09	START		NAS		LID 3174 - SPQ 100% Discount Broken - add over 100% alert & use the unitprice if the discount = 100%
			if (myForm.reduceByPerc.value > 100)
			{
				alert("The discount % cannot be greater than 100%");
			}
			else if (myForm.reduceByPerc.value == 100)
			{
				myForm.frmSpecialPriceEdit.value =   0
			}
			else
			{
				myForm.frmSpecialPriceEdit.value =   (myForm.frmUnitPriceEdit.value * ((100-myForm.reduceByPerc.value) / 100)).toFixed(2);	//2011-02-01 			PPB 	LID5382	

			}
		}
			// 2010-06-09	STOP		NAS		LID 3174 - SPQ 100% Discount Broken 	
		
function refreshParentLocation(thisMethod) {
   if (thisMethod != '') self.parent.location.href = window.parent.location;
}

//alert(window.parent.location)
</script>
</cf_translate>

<cfinvoke component="relay.com.oppSpecialPricing" method="getOppPricing" returnvariable="qOppPricing">
	<cfinvokeargument name="dataSource" value="#application.siteDataSource#"/>
	<cfinvokeargument name="opportunityID" value="#opportunityID#"/>
</cfinvoke>

		
<cfif IsQuery(myopportunity.getOppProducts)> 
<table border="0" cellspacing="0" cellpadding="0" align="left" bgcolor="White" class="withBorder" width="100%">
<tr><td>
	<table border="1" cellspacing="1" cellpadding="3" align="left" bgcolor="White" class="withBorder">

	<tr><td colspan="10" valign="top" align="right">

	<cfif useOppFunctions and request.relayCurrentUser.isInternal>
		<CFINCLUDE template="oppProductViewPopup.cfm">
	<cfelse>
		&nbsp;
	</cfif>

	</td></tr>
	<tr>
<!--- START: 2009-11-11	AJC P-LEX039 Competitive products --->
		<td>
<!--- END: 2009-11-11	AJC P-LEX039 Competitive products --->			
				
<!--- 	<cfif useOppFunctions>
	<th>	
	</cfif> --->
	

<!--- 2007-12-06 - GCC - If on portal and Request.EditLeadEID is defined submit to that rahter than the original page because it may have been called from add contact form process--->
<cfparam name="formAction" default="">
<!--- 2011-05-26 PPB REL106 added EditLeadEID as a setting --->
<cfset EditLeadEID = application.com.settings.getSetting("leadManager.EditLeadEID")>
<cfif request.relaycurrentuser.isinternal eq "false" and EditLeadEID neq "">
	<cfif isdefined('frmOppTypeID')>
		<cfset formAction = "/?eid=#EditLeadEID#&entityID=#entityID#&opportunityID=#opportunityID#&frmOppTypeID=#frmOppTypeID#">
	<cfelseif isdefined('OppTypeID')>
		<cfset formAction = "/?eid=#EditLeadEID#&entityID=#entityID#&opportunityID=#opportunityID#&frmOppTypeID=#OppTypeID#">
	<cfelse>
		<cfset formAction = "/?eid=#EditLeadEID#&entityID=#entityID#&opportunityID=#opportunityID#">
	</cfif>
</cfif>
			
<!--- START: 2009-11-30 AJC P-LEX039 Needed for AJX bind --->
<cfform name="oppProductForm" action="#formAction#" method="post" enctype="multipart/form-data">
<table border="0" cellspacing="0" cellpadding="0" align="left" bgcolor="White" width="100%">
<!--- END: 2009-11-30 AJC P-LEX039 Needed for AJX bind --->	
	<!--- 2007-11-20 - GCC - trialling form destination --->
	<!--- 
	<cfif request.relaycurrentuser.isInternal>
		<input type="hidden" name="formAction" value="">
	<cfelse>
		<input type="hidden" name="formAction" value="/?eid=2306">
	</cfif>
	 --->
		<cfoutput>
<!--- START: 2009-11-11	AJC P-LEX039 Competitive products --->
<!--- <form name="oppProductForm" method="post" enctype="multipart/form-data"> --->
<!--- END: 2009-11-11	AJC P-LEX039 Competitive products --->
	<input type="hidden" name="productID" value="0">
	<input type="hidden" name="oppProductID" value="0">
	<cfif isdefined('frmOppTypeID')>
		<CF_INPUT type="hidden" name="frmopptypeID" value="#frmopptypeID#">
	</cfif>
	<input type="hidden" name="frmTask" value="0">
	<input type="hidden" name="frmSPManualEdit" value="">
	<CFIF IsDefined("sortOrder")>
		<CF_INPUT Name="sortOrder" Type="Hidden" Value="#sortOrder#">
	</CFIF>
		<!--- GCC No value needed in this instance but preserved for compatability--->
		<input type="hidden" name="frmAllCheckIDs" value="">
		<input type="hidden" name="frmAction" value="">

<!--- ==============================================================================
       oppProduct Table Headings         
=============================================================================== --->
		
		<CF_INPUT type="hidden" name="opportunityID" value="#opportunityID#">
	<!--- START: NYB 2009-09-16 - LHID2275 - added: --->

	<cfif isDefined("frmNext") and frmNext eq "returnLeadScreen">
	<!--- if we are being called from addContactForm and thisSupresses the relayware/relayTagTemplates//addContactForm from showing again --->
	<input type="hidden" name="showForm" value="no">
	<input type="hidden" name="showIntroText" value="no">
	<input type="hidden" name="frmNext" value="returnLeadScreen">
	<CF_INPUT type="hidden" name="thisOrgID" value="#myopportunity.entityID#">
	</cfif>
		<!--- END: NYB 2009-09-16 - LHID2275 --->
		</cfoutput>
<!--- 	<cfif useOppFunctions>
	</th>
	</cfif>	 --->
		
	<cfif myopportunity.getOppProducts.recordCount gt 0>
	<cfoutput>
		
		<cfset IllustrativePriceToCurrency = application.com.settings.getSetting("leadManager.IllustrativePriceToCurrency")>
		
<!--- START: 2009-11-11	AJC P-LEX039 Competitive products --->
			<tr>
<!--- END: 2009-11-11	AJC P-LEX039 Competitive products --->	
				<cfif useOppFunctions and request.relayCurrentUser.isInternal><th>&nbsp;</th></cfif>
				<th>
					<cfswitch expression="#sortOrder#">
						<cfcase value="SKU asc">
							<a href="javaScript:reSort( 'SKU DESC' );" style="text-decoration: none;" title="Sort Z-A">phr_oppProductSKU</a>
							<img src="/images/misc/iconSortDesc.gif" border="0" alt="Sort Z-A">
						</cfcase>
						<cfcase value="SKU desc">
							<a href="javaScript:reSort( 'SKU ASC' );" style="text-decoration: none;" title="Sort A-Z">phr_oppProductSKU</a>
							<img src="/images/misc/iconSortAsc.gif" border="0" alt="Sort A-Z">
						</cfcase>
						<cfdefaultcase>
							<a href="javaScript:reSort( 'SKU ASC' );" style="text-decoration: none;" title="Sort A-Z">phr_oppProductSKU</a>
						</cfdefaultcase>
					</cfswitch>
				</th>
				<th nowrap>phr_oppProductquantity</th>
				<cfif useProductStatus>
				<th nowrap>phr_oppProductStatus</th>
				</cfif>
				<cfif useOppProductShipDate>
					<th nowrap>
						<cfswitch expression="#sortOrder#">
							<cfcase value="forecastShipDate asc">
								<a href="javaScript:reSort( 'forecastShipDate DESC' );" style="text-decoration: none;" title="Sort Z-A">phr_oppProductShipDate</a>
								<img src="/images/misc/iconSortDesc.gif" border="0" alt="Sort Z-A">
							</cfcase>
							<cfcase value="forecastShipDate desc">
								<a href="javaScript:reSort( 'forecastShipDate ASC' );" style="text-decoration: none;" title="Sort A-Z">phr_oppProductShipDate</a>
								<img src="/images/misc/iconSortAsc.gif" border="0" alt="Sort A-Z">
							</cfcase>
							<cfdefaultcase>
								<a href="javaScript:reSort( 'forecastShipDate ASC' );" style="text-decoration: none;" title="Sort A-Z">phr_oppProductShipDate</a>
							</cfdefaultcase>
						</cfswitch>
					</th>
				</cfif>
				<cfif useOppProductProbability>
					<th nowrap>
						<cfswitch expression="#sortOrder#">
							<cfcase value="probability asc">
								<a href="javaScript:reSort( 'probability DESC' );" style="text-decoration: none;" title="Sort Z-A">phr_oppProductProbability</a>
								<img src="/images/misc/iconSortDesc.gif" border="0" alt="Sort Z-A">
							</cfcase>
							<cfcase value="probability desc">
								<a href="javaScript:reSort( 'probability ASC' );" style="text-decoration: none;" title="Sort A-Z">phr_oppProductProbability</a>
								<img src="/images/misc/iconSortAsc.gif" border="0" alt="Sort A-Z">
							</cfcase>
							<cfdefaultcase>
								<a href="javaScript:reSort( 'probability ASC' );" style="text-decoration: none;" title="Sort A-Z">phr_oppProductProbability</a>
							</cfdefaultcase>
						</cfswitch>
					</th>
				</cfif>
				<!--- START: 2009-11-11	AJC P-LEX039 Competitive products --->
				<cfif useOppProductPartnerPrice>
				<th nowrap>phr_oppProductPartnerPrice</th>
				</cfif>
				<!--- END: 2009-11-11	AJC P-LEX039 Competitive products --->
				<cfif myopportunity.getOppProducts.recordCount neq 0>
				<th nowrap>phr_oppProductUnitPrice</th>
				</cfif>					
				<!--- Start 2010-02-25			NAS			P-LEX039 - Demo checkbox addition for 'Deal Registration' --->
				<cfif isdefined("OppType") and OppType EQ "Deal Reg">
					<th nowrap>phr_demo</th>
				</cfif>
				<!--- End	 2010-02-25			NAS			P-LEX039 - Demo checkbox addition for 'Deal Registration' --->				
				<cfif useOppProductPercentDiscount>
					<th>phr_oppProductPercentDiscount</th>
					<!--- 2006-04-20 GCC CR_ATI032 - Show Price difference as well as discount --->
					<th>phr_oppProductPriceDiscount</th>
				</cfif>
				<cfif useOppProductStockLocation><th>phr_oppProductStockLocation</th></cfif>
				<cfif useOppProductSpecialPricing><th>phr_oppProductSpecialPrice</th></cfif>
				<cfif useOppProductApprovedPrice><th>phr_oppProductApprovedPrice</th></cfif>
				
				<cfif myopportunity.getOppProducts.recordCount neq 0>
					<th nowrap>phr_oppProductSubTotal <cfif structkeyexists(request,"IllustrativePriceFromCurrency")>(#htmleditformat(request.IllustrativePriceFromCurrency)#)</cfif></th>
					<!--- CR_ATI031 - GCC - 2005-12-19 - euros --->
					<cfif structkeyExists(request,"illustrativePriceMethod")>						
						<cfswitch expression="#request.illustrativePriceMethod#">
							<cfcase value="2">
								<th>phr_oppProductSubTotal (#htmleditformat(IllustrativePriceToCurrency)#) phr_IllUSTRATIVE</th>
							</cfcase>
						</cfswitch>						
					</cfif>
					<cfif showIllustrativePrice><th>phr_#htmleditformat(phraseprefix)#oppIllustrativePrice *</th></cfif>
					<!--- START: 2009-11-11	AJC P-LEX039 Competitive products --->
					<cfif useOppProductCompetitive><th nowrap>phr_oppProductCompetitorEdit</th></cfif>
					<!--- END: 2009-11-11	AJC P-LEX039 Competitive products --->
					<cfif showEditOppProduct><th nowrap>phr_oppProductEdit</th></cfif>
					<!--- <th nowrap><a href="javascript:void(eventDeleteOnClick());">phr_oppProductRemoveProduct</a></th> --->
				<cfelse>
					<th colspan="4" nowrap>&nbsp;</th>
				</cfif>
				
			</tr>
		</cfoutput>

				<!--- cfif myopportunity.getOppProducts.recordCount gt 0> --->
<!--- ==============================================================================
       Existing oppProduct lines         
=============================================================================== --->
		<cfoutput query="myopportunity.getOppProducts">
				<tr <cfif CurrentRow MOD 2 IS NOT 0>class="oddrow"<cfelse>class="evenRow"</cfif>> 
				<!--- <td>#description#</td> --->
				<cfif useOppFunctions and request.relayCurrentUser.isInternal>
				<td><INPUT TYPE="checkbox" NAME="frmProductCheck" VALUE="#oppProductID#" <cfif isDefined("frmProductCheck")><cfif ListContains(frmProductCheck,oppProductID)>checked</cfif></cfif>></td>
				</cfif>
				<td>#htmleditformat(sku)#</td>
				<td>#htmleditformat(quantity)#</td>
				<cfif useProductStatus>
				<td>#htmleditformat(status)#</td>
				</cfif>
				<cfif useOppProductShipDate><td nowrap>#iif( IsDate( forecastShipDate ), 'DateFormat( forecastShipDate, "dd-mmm-yyyy" )', de( "" ) )#</td></cfif>
				<cfif useOppProductProbability><td>#htmleditformat(Probability)#</td></cfif>
				<!--- START: 2009-11-11	AJC P-LEX039 Competitive products --->
					
				<cfif useOppProductPartnerPrice>
					<td>
						<cfif showIllustrativePrice>
							#decimalformat(originalUnitPrice)# (#htmleditformat(trim(myopportunity.Currency))#)
						<cfelse>
							#LSCurrencyFormat(originalUnitPrice,"local")#
						</cfif>
					</td>
				</cfif>
					
				<!--- END: 2009-11-11	AJC P-LEX039 Competitive products --->
				<td>
					<cfif showIllustrativePrice>
						#decimalformat(unitPrice)# (#htmleditformat(trim(myopportunity.Currency))#)
					<cfelse>
						#htmleditformat(myopportunity.CurrencySign)##lsNumberFormat(unitPrice,currencyMask)#   
					</cfif>
				</td>
				<!--- Start 2010-02-25			NAS			P-LEX039 - Demo checkbox addition for 'Deal Registration' --->
					
				<cfif isdefined("OppType") and OppType EQ "Deal Reg">
					<td><cfif myopportunity.getOppProducts.demo EQ 1 >Yes<cfelse>No</cfif></td>
				</cfif>
					
				<!--- End	 2010-02-25			NAS			P-LEX039 - Demo checkbox addition for 'Deal Registration' --->
					
				<cfif useOppProductSpecialPricing>
				<!--- promotional or special price --->
						<cfset displayPrice = "">
						<cfset informationMessage = "">
						<cfset specialPriceUnapproved = false>
						<cfset informationImage = "/images/MISC/iconInformation.gif">
						<cfif productPromotionID neq "">
							<cfset displayPrice = promotionalPrice>
							<cfset informationMessage = "Promotional Price">
						<cfelseif specialPrice neq "">
							<cfset displayPrice = specialPrice>
							<cfif specialPriceApproverPersonID neq "">
								<cfset informationMessage = "Approved Special Price">
							<cfelse>
								<cfset specialPriceUnapproved = true>
								<cfset informationMessage = "Unapproved Special Price">
								<cfset informationImage = "/images/MISC/iconInformationRed.gif">
							</cfif>
						</cfif>
				</cfif>
										
				<cfif useOppProductPercentDiscount>
					<td>
					<!--- JC: Added unitprice trap to prevent div by zero --->
					<!--- NJH 2007-04-05 added isDefined as when useOppProductSpecialPricing is false, then display price isn't defined --->
					<cfif isDefined("displayPrice") and displayPrice neq "" AND isDefined("unitprice") and unitprice NEQ 0>
						<cfset percentDiscount = DecimalFormat( round(100-((displayPrice / unitprice)*100)))>#percentDiscount#%
					<cfelse>&nbsp;
					</cfif>
					</td>
					<!--- 2006-04-20 GCC CR_ATI032 - Show Price difference as well as discount --->
					<td>
						<cfif PriceDifference neq 0>
							#myopportunity.CurrencySign##lsNumberFormat(PriceDifference,currencyMask)#   
							<!--- #LSCurrencyFormat(PriceDifference,"local")# --->
						</cfif>
					</td>
				</cfif>
										
				<cfif useOppProductStockLocation><td>#StockLocation#&nbsp;</td></cfif>
										
				<cfif useOppProductSpecialPricing>
					<td align="right">
						<!--- <a title="" style="text-decoration: none; font-weight: normal; color: black; cursor: hand;">#specialPrice#</a> --->
							<!--- cfif informationMessage neq "">
<!--- ********************************************************************* --->
<!--- need to check that browser has approval rights --->
<!--- ********************************************************************* --->
								cfif permissionSpecialPriceApproval and specialPriceUnapproved><a href="javascript:void(specialPriceApprovalWindow(this,'#JSStringFormat( myOpportunity.getOppProducts.oppProductID )#'));">/cfif>
							<img src="#informationImage#" alt="#informationMessage#" width="14" height="14" border="0">
								cfif permissionSpecialPriceApproval and specialPriceUnapproved></a>/cfif>
							/cfif> --->
						<cfif displayPrice neq "">
							<cfif showIllustrativePrice>
								#decimalformat(displayPrice)# <strong>(#htmleditformat(trim(myopportunity.Currency))#)</strong>
							<cfelse>
								#htmleditformat(myopportunity.CurrencySign)##lsNumberFormat(displayPrice,currencyMask)#   
								<!--- #LSCurrencyFormat(displayPrice,"local")# --->
							</cfif>
						<cfelse>
							&nbsp;
						</cfif>
						<!--- <img src="/images/MISC/iconWarning.gif" alt="" width="14" height="14" border="0"> --->
					</td>
				</cfif>
					
				<!--- START: 2009-11-11	AJC P-LEX039 Approved price --->
				<cfif useOppProductApprovedPrice>
					<td align="right">
						<cfif ApprovedPrice neq "">
							<cfif showIllustrativePrice>
								#decimalformat(ApprovedPrice)# <strong>(#htmleditformat(trim(myopportunity.Currency))#)</strong>
							<cfelse>
								#htmleditformat(myopportunity.CurrencySign)##lsNumberFormat(ApprovedPrice,currencyMask)#   
								<!--- #LSCurrencyFormat(ApprovedPrice,"local")# --->
							</cfif>
						<cfelse>
							&nbsp;
						</cfif>
						<!--- <img src="/images/MISC/iconWarning.gif" alt="" width="14" height="14" border="0"> --->
					</td>
				</cfif>
					
				<!--- END: 2009-11-11	AJC P-LEX039 Approved price --->
				<td align="right">
					
					<cfif showIllustrativePrice>
						#decimalformat(subtotal)# <strong>(#htmleditformat(trim(myopportunity.Currency))#)
					<cfelse>
						#htmleditformat(myopportunity.CurrencySign)##lsNumberFormat(subtotal,currencyMask)#   
						<!--- #LSCurrencyFormat(subtotal,"local")# --->
					</cfif>
					
				</td>
					
				<!--- CR_ATI031 - GCC - 2005-12-19 - euros --->
				<cfif structkeyExists(request,"illustrativePriceMethod")>						
					<cfswitch expression="#request.illustrativePriceMethod#">
						<cfcase value="2">
							<cfscript>
								IllustrativePrice = application.com.currency.convert(
									method = #request.IllustrativePriceMethod#,
									fromValue = subtotal,
									fromCurrency = request.IllustrativePriceFromCurrency,
									toCurrency = IllustrativePriceToCurrency,
									date = myopportunity.lastUpdated
									);
							</cfscript>
							<cfset IllustrativePriceOutput = IllustrativePrice.toSign & IllustrativePrice.toValue>
							<td align="right">#htmleditformat(IllustrativePriceOutput)#</td>
						</cfcase>
					</cfswitch>						
				</cfif>
					
				<cfif showIllustrativePrice>
					<cfscript>
						res = application.com.currency.convert(fromValue="#trim(subtotal)#", fromCurrency="#trim(myopportunity.Currency)#", toCurrency="#trim(toCurrency.countrycurrency)#");
					</cfscript>
					<td align="right">#decimalformat(res.tovalue)# <strong>(#htmleditformat(toCurrency.countrycurrency)#)</strong></td>
				</cfif>
					
				<!--- START: 2009-11-11	AJC P-LEX039 Competitive products --->
				<cfif useOppProductCompetitive>
					<cfif request.relayCurrentUser.isInternal>
					<td align="center"><a href="oppProductListCompetitive.cfm?opportunityID=#opportunityID#&oppProductID=#oppProductID#<cfif isdefined('frmOppTypeID')>&frmOppTypeID=#htmleditformat(frmOppTypeID)#</cfif>"><img src="/images/icons/application_form_edit.png"></a></td>
					<cfelse>
						<cfif EditLeadEID neq "">	<!--- 2011-05-26 PPB I added defence on the existence of this variable even though it the original request.EditLeadEID wasn't --->
							<td align="center"><a href="/?eid=#EditLeadEID#&entityID=#entityID#&frmTask=viewCompetitive&opportunityID=#opportunityID#&oppProductID=#oppProductID#<cfif isdefined('frmOppTypeID')>&frmOppTypeID=#htmleditformat(frmOppTypeID)#</cfif>"><img src="/images/icons/application_form_edit.png"></a></td>
						</cfif> 
					</cfif>
				</cfif>
				<!--- END: 2009-11-11	AJC P-LEX039 Competitive products --->
					
				<cfif showEditOppProduct>
					<td align="center"><CF_INPUT type="radio" name="frmEditOppProduct" value="#myopportunity.getOppProducts.oppproductid#" onClick="eventOppProductOnClick(this);"></td>
				</cfif>
					
				<!--- <td align="center"><input type="checkbox" name="frmProductDeletionList" value="#myopportunity.getOppProducts.oppproductid#" onDblClick="if(this.checked)eventDeleteOnClick();"></td> --->
			</tr>
		</cfoutput>
	</cfif><!--- START: 2009-11-11	AJC P-LEX039 Competitive products --->
		
	<cfif myopportunity.getOppProducts.recordCount neq 0>
		<tr><th height="10" colspan="14">&nbsp;</th></tr>
	</cfif><!--- END: 2009-11-11	AJC P-LEX039 Competitive products --->
			
	<cfif showIllustrativePrice>
		<cfoutput>
			<tr><td colspan="10">* phr_#htmleditformat(phraseprefix)#illustrativedescription</td></tr>
		</cfoutput>
	</cfif>
						
	<!--- START: 2009-11-11	AJC P-LEX039 Competitive products --->
	<cfif myopportunity.getOppProducts.recordCount gt 1>
	<tr><td colspan="10">phr_oppProductsEditTitle</td></tr>
	<!--- END: 2009-11-11	AJC P-LEX039 Competitive products --->
<!--- ==============================================================================
       oppProduct Table Headings         
=============================================================================== --->
		<cfoutput>
			<tr>
				<!--- <th nowrap>phr_oppProductDescription</th> ---> 
				<cfif useOppFunctions and request.relayCurrentUser.isInternal><th>&nbsp;</th></cfif>
				<th nowrap>phr_oppProductSKU</th>
				<th nowrap>phr_oppProductquantity</th>
				<cfif useOppProductShipDate><th nowrap>phr_oppProductShipDate</th></cfif>
				<cfif useOppProductProbability><th nowrap>phr_oppProductProbability</th></cfif>
				<cfif useOppProductSpecialPricing><th>phr_oppProductSpecialPrice</th></cfif>
				<!--- Start 2010-02-25			NAS			P-LEX039 - Demo checkbox addition for 'Deal Registration' --->
				<cfif isdefined("OppType") and OppType EQ "Deal Reg">
					<th>&nbsp;</th>
					<th nowrap>phr_demo</th>
				</cfif>
				<!--- End	 2010-02-25			NAS			P-LEX039 - Demo checkbox addition for 'Deal Registration' --->
				<!--- START: 2009-11-11	AJC P-LEX039 Approved price --->
				<cfif useOppProductApprovedPrice and request.relayCurrentUser.isInternal><th>phr_oppProductApprovedPrice</th></cfif>
				<!--- END: 2009-11-11	AJC P-LEX039 Approved price --->
				
				<!--- START: 2009-11-11	AJC P-LEX039 --->
				<cfif useOppProductPartnerPrice><th nowrap>&nbsp;</td></cfif>
				<!--- END: 2009-11-11	AJC P-LEX039 --->
				<cfif useOppProductStockLocation><th>phr_oppProductStockLocation</th></cfif>
					<th>phr_opp_product_SPCalc</th>
				<cfif useOppProductSpecialPricing><th>phr_oppProductSpecialPrice</th></cfif>
				<cfif useOppProductStockLocation><th>phr_oppProductStockLocation</th></cfif>
				
				<th nowrap>phr_oppProductUnitPrice</th>
				<!--- START: 2009-11-11	AJC P-LEX039 Approved price --->
				<cfif useOppProductApprovedPrice and request.relayCurrentUser.isInternal><th>phr_oppProductApprovedPrice</th></cfif>
				<!--- END: 2009-11-11	AJC P-LEX039 Approved price --->
				<th colspan="3" nowrap>&nbsp;</th>
			</tr>
		</cfoutput>
	</cfif>

	
<!--- ==============================================================================
       Edit oppProduct Row         
=============================================================================== --->
	<cfif showEditOppProduct>
		<cfif myopportunity.getOppProducts.recordCount neq 0>
			<tr><input type="hidden" name="frmProductIDEdit"><input type="hidden" name="frmOppProductIDEdit">
				<td <cfif useOppFunctions and request.relayCurrentUser.isInternal>colspan="2"</cfif> valign="top">
					<!--- NJH 2009-02-23 Bug Fix Issue T-10 838- added id's for descriptionEdit and QuantityEdit to give a handle for the input fields --->
					<input type="Text" name="frmDescriptionEdit" id="oppProdDescEdit">
				</td>			
				<cfif useOppProductQuantityEdit>
				<td valign="top">
					<input type="text" id="oppProdQuantEdit" name="frmQuantityEdit" size="5" onKeyPress="restrictKeyStrokes(event,keybNumeric)" onChange="eventOppProductEditOnChange(this);">
	 			</td>
				<cfelse>
					<input type="hidden" name="frmQuantityEdit">	
				</cfif> 
				
				<cfif useOppProductShipDate>
				<td valign="top">
					<!--- NJH 2008-11-20 All Sites Issue 1372 bug fixing changed thisFormName to formName --->
					<cf_relayDateField	
						formName="oppProductForm"
						fieldName="frmForecastShipDateEdit"
						anchorName="anchorX1"
						onChange="eventOppProductEditOnChange(this)"
						helpText="phr_dateFieldHelpText"
					>
	
	<!--- 				<input type="text" name="frmForecastShipDateEdit" size="10" onChange="eventOppProductEditOnChange(this);" readonly title="Click on the icon below to choose a date">&nbsp;<br><A HREF="javascript:void(showCalendar());" TITLE="Click to choose date" onMouseOver="window.status='Pop Calendar';return true;" onMouseOut="window.status='';return true;"><IMG SRC="<cfoutput></cfoutput>/images/misc/IconCalendar.gif" ALIGN="ABSMIDDLE" border="0"></A> --->
				</td>
				<cfelse>
					<input type="hidden" name="frmForecastShipDateEdit">&nbsp;
				</cfif>

				<td valign="top">
				<cfif useOppProductProbability><cfoutput>#OppProductProbabilitySelectEdit#</cfoutput><cfelse><input type="hidden" name="frmProbabilityEdit" value="">&nbsp;</cfif>
				</td>
				<td valign="top" nowrap>
					<!--- <input type="hidden" name="frmUnitPriceEdit"> ---> <!--- 2011-02-01 PPB LID5382 frmUnitPriceEdit form field is defined below; duplicate seemed to cause an intermittent problem --->
					<input type="text" name="reduceByPerc" size="3" maxlength="3" onKeyPress="restrictKeyStrokes(event,keybDecimal)" onKeyUp="if (this.value == '') {this.form.calcDiscount.disabled=true} else {this.form.calcDiscount.disabled=false}">
					<cfoutput>%<CF_INPUT type="button" name="calcDiscount" value="phr_#phraseprefix#Lead_calculate" disabled onClick="calcLineItemDiscount(this)" title="Reduce special pricing by this percentage" ></cfoutput>
					&nbsp;
				</td>			
				<!--- Start 2010-02-25			NAS			P-LEX039 - Demo checkbox addition for 'Deal Registration' --->	
				<cfif isdefined("OppType") and OppType EQ "Deal Reg">
				<td valign="top">
						<INPUT TYPE="checkbox" NAME="frmDemoEdit" VALUE="">
					</td>
				</cfif>	
				<!--- End	 2010-02-25			NAS			P-LEX039 - Demo checkbox addition for 'Deal Registration' --->
				<!--- START: 2009-12-17	AJC P-LEX039 Approved price --->
						<!--- /cfif> --->
				<cfif useOppProductApprovedPrice and request.relayCurrentUser.isInternal>
					<td valign="top">
						<input type="text" name="frmApprovedPriceEdit" size="8" onChange="eventOppProductEditOnChange(this);" onKeyPress="restrictKeyStrokes(event,keybDecimal)" value="">
					</td>
				<cfelse>
					<input type="hidden" name="frmApprovedPriceEdit" value="">
				</cfif>
				<!--- END: 2009-12-17	AJC P-LEX039 Approved price --->
				<!--- START: 2009-11-11	AJC P-LEX039 --->
				<cfif useOppProductPartnerPrice>
				<td valign="top">
						<input type="text" id="oppProdPriceEdit" name="frmUnitPriceEdit" size="5" onKeyPress="restrictKeyStrokes(event,keybNumeric)" onChange="eventOppProductEditOnChange(this);"></td>
				<cfelse>
					<input type="hidden" name="frmUnitPriceEdit">
				</cfif>
				<!--- START: 2009-11-11	AJC P-LEX039 --->	
				<cfif isDefined ("qValidStockLocations") and useOppProductStockLocation and isQuery(qValidStockLocations)>
				<td valign="top">
					<SELECT NAME="frmStockLocationEdit">
						<!--- this is defined in opportunityINI.cfm --->
						<cfoutput query="qValidStockLocations">
							<option value="#optionValue#">#htmleditformat(optionDescription)#</option>
						</cfoutput>
					</SELECT>
				<cfelse>
					<input type="hidden" name="frmStockLocationEdit">&nbsp;
				</td>
				</cfif>
						
				<td valign="top">
						
				<cfif useOppProductSpecialPricing><input type="text" name="frmSpecialPriceEdit" size="8" onChange="eventOppProductEditOnChange(this);" onKeyPress="restrictKeyStrokes(event,keybDecimal)" value=""><cfelse><input type="hidden" name="frmSpecialPriceEdit" value="">&nbsp;</cfif>
				</td>
				<td colspan="2" valign="top" align="left">
					<cfif usebuttons>
						<cfoutput><CF_INPUT type="Button" value="phr_#phraseprefix#oppProductSaveProduct" onclick="void(eventOppProductSave());" class="OrdersFormbutton"></cfoutput>
					<cfelse>
						<a href="javascript:void(eventOppProductSave());" title="Save Edit">phr_oppProductSaveProduct</a>
					</cfif>
					<!--- NJH LID 8064 - option to add duplicate removed as used only for Allied Telesyn and the javascript has long been disabled.
					<cfif showDuplicate>
						<br>
						<a href="javascript:void(eventOppProductDuplicate());" title="Click here to create a duplicate line item">Create Duplicate</a>&nbsp;&raquo;&nbsp;<br><a href="javascript:void(eventOppProductAddDuplicate());" title="Save Duplicate line item">Save Duplicate</a> 
						<cfif request.relayCurrentUser.isInternal>
							<br><a href="javascript:getHelpID(272)" title="Click here to access help files on adding in products">Help</a>
						</cfif>
					</cfif> --->
				</td>
			</tr>
		</cfif>
	</cfif>
	
<!--- ==============================================================================
       Add new oppProduct Row         
=============================================================================== --->
	<cfoutput>
	<cfif showAddProduct>
		<!--- START: 2009-11-11	AJC P-LEX039 --->
		<tr><td colspan="10">phr_oppProductsAddTitle</td></tr>
		<!--- END: 2009-11-11	AJC P-LEX039 --->
		<TR>
		<td colspan="12" class="label">
			<TABLE border="0" CELLSPACING="1" CELLPADDING="3" ALIGN="left" BGCOLOR="White">
			<tr>
				<th>&nbsp;</th>
				<th nowrap>phr_oppProductquantity</th>
				<th nowrap><cfif useOppProductShipDate>phr_oppProductShipDate<cfelse>&nbsp;</cfif></th>
				<th nowrap><cfif useOppProductProbability>phr_oppProductProbability<cfelse>&nbsp;</cfif></th>
				<!--- START: 2009-11-11	AJC P-LEX039 --->
				<cfif useOppProductPartnerPrice><th nowrap>phr_oppProductUnitPrice</td></cfif>
				<!--- END: 2009-11-11	AJC P-LEX039 --->
				<!--- Start 2010-02-25			NAS			P-LEX039 - Demo checkbox addition for 'Deal Registration' --->
				<cfif isdefined("OppType") and OppType EQ "Deal Reg">
					<th nowrap>phr_demo</th>
				</cfif>	
				<!--- End	 2010-02-25			NAS			P-LEX039 - Demo checkbox addition for 'Deal Registration' --->
				<th><cfif useOppProductSpecialPricing>phr_oppProductSpecialPrice<cfelse>&nbsp;</cfif></th>
				<!--- START: 2009-12-17	AJC P-LEX039 Approved price --->
				<cfif useOppProductApprovedPrice and request.relayCurrentUser.isInternal>
					<th>phr_oppProductApprovedPrice</th>
				</cfif>
				<!--- END: 2009-12-17	AJC P-LEX039 Approved price --->
				<th><cfif useOppProductStockLocation>phr_oppProductStockLocation<cfelse>&nbsp;</cfif></th>
				<th>&nbsp;</th>
			</tr>
				<tr>
			<td valign="top" width="1">
			<!--- <div id="productSelectorDiv1"> --->
			<cfswitch expression="#productSelector#">
				<cfcase value="singleDropDown">
					<cfscript>
						myopportunity.getproductList();
					</cfscript>
					<SELECT NAME="thisProductID"> 
						<cfloop query="myopportunity.qproductGroupID">
							<option value="#ID#">#left(itemtext,30)#</option>
						</cfloop>
					</SELECT>
				</cfcase>
				
				<cfcase value="linkedDropDown">
						<!--- START: 2009-11-30 AJC P-LEX039 --->
					<cfscript>
							// myopportunity.getProductAndGroupList();
							myopportunity.getProductGroupList();
								//2010-07-13 NYB Lexmark LHID 3636 - added:
								partnerSalesPersonCountryID = application.com.RelayPLO.getPersonDetails(PersonID = myopportunity.partnerSalesPersonID).countryID;								
					</cfscript>
						
						<!--- <cf_twoselectsrelated
						query="myopportunity.qProductAndGroupList"
						name1="productGroupID"
						name2="thisProductID"
						display1="productGroupDescription"
						display2="productDescription"
						value1="productGroupID"
						value2="productID"
						forcewidth1="30"
						forcewidth2="40"
						size1="1"
						size2="auto"
						autoselectfirst="Yes"
						emptytext1="(Select Product Group)"
						emptytext2="(Optional Product)"
						forcelength2 = "6"
							formname = "oppProductForm"> --->
						<!--- END: 2009-11-30 AJC P-LEX039 --->
						<cfselect name="productGroupID" selected="" query="myopportunity.qproductGroupList" value="productGroupID" display="productGroupDescription">
						</cfselect>
								<!--- 2010-07-13 NYB Lexmark LHID 3636 - myopportunity.countryid with partnerSalesPersonCountryID in cfc call --->
								<cfselect name="thisProductID" bind="cfc:webservices.relayOpportunityWS.getProductGroupProducts(productGroupID={productGroupID},productCatalogueCampaignID=#application.com.settings.getSetting('leadManager.products.productCatalogueCampaignID')#,countryID=#partnerSalesPersonCountryID#)" bindOnLoad="false" selected="" value="productID" display="productDescription">
							<option value="">(Optional Product)</option>
						</cfselect>
						
				</cfcase>
				<cfdefaultcase>
					<cfscript>
						myopportunity.getproductList();
					</cfscript>
					<SELECT NAME="thisProductID">
						<cfloop query="myopportunity.qproductGroupID">
							<option value="#ID#">#left(itemtext,30)#</option>
						</cfloop>
					</SELECT>				
				</cfdefaultcase>
			</cfswitch>
			<!--- </div> --->
			</td>
			<td valign="top"><input name="quantity" size="5" onKeyPress="restrictKeyStrokes(event,keybNumeric)"></td>
			<td valign="top">
			<cfif useOppProductShipDate>
				<!--- NJH 2008-11-20 All Sites Issue 1372 bug fixing - changed thisFormName to formName --->
				<cf_relayDateField	
					currentValue=""
					formName="oppProductForm"
					fieldName="forecastShipDate"
					anchorName="anchorX"
					helpText="phr_dateFieldHelpText"
				>
			
<!--- 			<input type="text" name="forecastShipDate" size="10" readonly title="Click on the icon below to shoose a date">&nbsp;<br><A HREF="javascript:show_calendar('oppProductForm.forecastShipDate',null,'','DD-MM-YYYY',null,'AllowWeekends=No;CurrentDate=Today')" TITLE="Click to choose date" onMouseOver="window.status='Pop Calendar';return true;" onMouseOut="window.status='';return true;"><IMG SRC="<cfoutput></cfoutput>/images/misc/IconCalendar.gif" ALIGN="ABSMIDDLE" border="0"></A></td> --->
			<cfelse>
				<input type="hidden" name="forecastShipDate">&nbsp;
			</cfif>
			</td>
			<td valign="top">
			<cfif useOppProductProbability><cfoutput>#OppProductProbabilitySelectAdd#</cfoutput><cfelse><CF_INPUT type="hidden" name="Probability" value="#url["probability"]#">&nbsp;</cfif>
			</td>
			<!--- START: 2009-11-11	AJC P-LEX039 --->
			<cfif useOppProductPartnerPrice>
						<td valign="top"><cfinput onchange="if (this.value != parseFloat(this.value)) alert('Error: Decimal input only i.e. No commas and . for a decimal point.');" name="frmUnitPrice" type="text" value="" bind="cfc:webservices.relayOpportunityWS.getProduct(productID={thisProductID})"></td>
			</cfif>
			<!--- Start 2010-02-25			NAS			P-LEX039 - Demo checkbox addition for 'Deal Registration' --->
			<cfif isdefined("OppType") and OppType EQ "Deal Reg">
				<td valign="top">
					<INPUT TYPE="checkbox" NAME="frmDemo" VALUE="">
				</td>
			</cfif>
			<!--- End	 2010-02-25			NAS			P-LEX039 - Demo checkbox addition for 'Deal Registration' --->
			<!--- START: 2009-11-11	AJC P-LEX039 --->
			<cfif useOppProductSpecialPricing><td valign="top"><input name="specialPrice" size="5" onKeyPress="restrictKeyStrokes(event,keybDecimal)" class="text-align:right;"></td><cfelse><input type="hidden" name="specialPrice"></cfif>
				<!--- START: 2009-12-17	AJC P-LEX039 Approved price --->
				<cfif useOppProductApprovedPrice and request.relayCurrentUser.isInternal>
					<td valign="top"><input name="approvedPrice" size="5" onKeyPress="restrictKeyStrokes(event,keybDecimal)" class="text-align:right;"></td>
				<cfelse>
					<input type="hidden" name="approvedPrice">
				</cfif>
				<!--- START: 2009-12-17	AJC P-LEX039 Approved price --->
			<cfif isDefined ("qValidStockLocations") and useOppProductStockLocation and isQuery(qValidStockLocations)>
				<td valign="top">
				<SELECT NAME="frmStockLocation">
					<!--- this is defined in opportunityINI.cfm --->
					<cfloop query="qValidStockLocations">
						<option value="#optionValue#">#htmleditformat(optionDescription)#</option>
					</cfloop>
				</SELECT>
				</td>
			<cfelse>
				<input type="hidden" name="frmStockLocation">
			</cfif>
			<td colspan="4" valign="top">
				<a href="javascript:addproduct()">phr_oppProductAddProduct</a>&nbsp;&nbsp;&nbsp;
				<!--- NJH 2009-01-30 - added useOppFunctions --->
				<cfif request.relayCurrentUser.isInternal and useOppFunctions>
					<a href="javascript:addproductset()">phr_oppProductAddProductSet</a><br>
					<a href="javascript:addmultipleproducts()">phr_sys_oppProductAddMultipleProducts</a>
				</cfif>
			</td>			
		</tr>
				</table>
			</td>		
		</tr>
		</cfif>
			
		</cfoutput>
	<!--- START: 2009-11-30 AJC P-LEX039 Needed for AJX bind --->
	</table>
	</cfform>
	
			<!--- END: 2009-11-30 AJC P-LEX039 Needed for AJX bind --->
	
<cfif request.relayCurrentUser.isInternal>	
	<cfoutput>																<!--- 2011-02-04 PPB LID5587 - moved definition of form frmReasonForm inside cfoutput cos javascript references to it weren't being recognised --->
	<form action="" method="post" name="frmReasonForm" id="frmReasonForm">
	<input type="hidden" name="specialPricingStatusMethod" value="">
	<input type="hidden" name="frmComments" value="">
	<CF_INPUT type="hidden" name="opportunityID" value="#opportunityID#">
	<tr>
		<td colspan="10" align="center">
			<TABLE border="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White">
				<cfif showCalcOppProduct>
					<tr>
						<th colspan="3" align="left">phr_SpecialPriceCalculator</th>
					</tr>
				</cfif>
				<tr bgcolor="white"><!--- Project full price (Total disti sell in list price) --->
					<td align="right">phr_#htmleditformat(phraseprefix)#ProjectFullPrice</td>
					<td>
					<cfif showIllustrativePrice>
						#decimalformat(qOppPricing.fullPrice)# <strong>(#htmleditformat(trim(myopportunity.Currency))#)</strong>
					<cfelse>
						#htmleditformat(myopportunity.CurrencySign)##lsNumberFormat(qOppPricing.fullPrice,currencyMask)#   
						<!--- #LSCurrencyFormat(qOppPricing.fullPrice,"local")# --->
					</cfif>
					</td>
					<td>&nbsp;</td>
				</tr>
				<tr bgcolor="white">
					<td align="right">phr_#htmleditformat(phraseprefix)#CurrentCalculatedBudget</td>
					<td>
					<cfif showIllustrativePrice>
						#decimalformat(qOppPricing.discountedSpecialPrice)# <strong>(#htmleditformat(trim(myopportunity.Currency))#)</strong>
					<cfelse>
						#htmleditformat(myopportunity.CurrencySign)##lsNumberFormat(qOppPricing.discountedSpecialPrice,currencyMask)#   
						<!--- #LSCurrencyFormat(qOppPricing.discountedSpecialPrice,"local")# --->
					</cfif>
					<cfif qOppPricing.fullPrice gt 0>
					<cfset percentageDiscount = (1-(qOppPricing.discountedSpecialPrice/qOppPricing.fullPrice))*100>
						<cfif percentageDiscount neq 0>
							&nbsp;( phr_Discount #decimalformat(percentageDiscount)#%)
						</cfif>
					</cfif>
					</td>
						<CF_INPUT type="hidden" name="fullPrice" value="#qOppPricing.fullPrice#">
					<td>&nbsp;</td>
				</tr>
				<cfif showillustrativeprice>
					<cfscript>
						//toCurrency = application.com.relayEcommerce.getCurrencies(request.relaycurrentuser.countryID);
						illustrativetotal = application.com.currency.convert(fromValue="#trim(qOppPricing.discountedSpecialPrice)#", fromCurrency="#trim(myopportunity.Currency)#", toCurrency="#trim(toCurrency.countrycurrency)#");
					</cfscript>
					<tr bgcolor="white">
					<td align="right">phr_#htmleditformat(phraseprefix)#CurrentCalculatedIllustrativeBudget</td>
					<td>#decimalformat(illustrativetotal.tovalue)# <strong>(#htmleditformat(toCurrency.countrycurrency)#)</strong></td>
					<td>&nbsp;</td>
				</tr>
				</cfif>
				<cfif showCalcOppProduct>
					<tr bgcolor="white">
						<td align="right">** Reduce to this budget</td></td>
						<td><input type="text" name="desiredBudget" id="desiredBudget" size="10" onkeyup="CheckIfDesiredBudgetEmpty()"  onKeyPress="restrictKeyStrokes(event,keybDecimal)"> <strong>(#htmleditformat(trim(myopportunity.Currency))#)</strong></td>
						<td align="left"><input type="button" name="calcDiscount" value="Calculate Discount" disabled onClick="calcMyDiscount()">&nbsp;</td>
					</tr>
		
					<tr bgcolor="white">
						<td align="right">*** Reduce by this percentage</td>
						<td><input type="text" name="desiredDiscount" id="desiredDiscount" size="10" onkeyup="CheckIfDesiredDiscountEmpty()"  onKeyPress="restrictKeyStrokes(event,keybDecimal)"> <strong>(#htmleditformat(trim(myopportunity.Currency))#)</strong></td>
						<td align="left"><input type="button" name="calcBudget" value="Calculate Budget" disabled onClick="calcMyBudget()">&nbsp;</td>
					</tr>
		
					<tr bgcolor="white">
						<td align="right"><div id="resultText"> </div></td>
						<td><input type="text" name="newBudget" id="newBudget" disabled size="10"></td>
						<td align="left"><input type="button" name="SP_SaveBudget" value="Apply this change" disabled onClick="saveSpecialPricing('SP_Calculate')">&nbsp;<input type="button" value="Reset" onClick="resetCalculator()"></td>
					</tr>
					<tr>
						<td colspan="3">
							**  phr_#htmleditformat(phraseprefix)#Opp_ReduceByBudgetHelp<br /><br />
							*** phr_#htmleditformat(phraseprefix)#Opp_ReduceByPercentHelp
						</td>
					</tr>
				</cfif>
			</table>
		</td>
	</tr>
	</form>
	</cfoutput>
	</table>
</cfif>
	</td>
	</tr>
	</table>
<cfelse>
	<cfoutput>You must pass a query object to #htmleditformat(thisTag)#</cfoutput>
</cfif>
<!---  --->

<!--- --------------------------------------------------------------------- --->
<!--- create JavaScript object to hold OppProducts result set client-side --->
<!--- --------------------------------------------------------------------- --->
<cfoutput>
<cf_translate encode="javascript">	
<SCRIPT type="text/javascript">
<!--
var resultSetOppProducts = new Object;
var validStockLocationObj = new Object;
var dirtyOppProductsEdit = false;
var touchedSpecialPrice = false;

<cfloop query="myopportunity.getOppProducts">
	resultSetOppProducts["#JSStringFormat( myopportunity.getOppProducts.oppproductid )#"] = new Array(
	"#JSStringFormat( myopportunity.getOppProducts.oppproductid )#", //[0]
	"#JSStringFormat( myopportunity.getOppProducts.description )#", //[1]
	"#JSStringFormat( myopportunity.getOppProducts.quantity )#", //[2]
	"#iif( IsDate( myopportunity.getOppProducts.forecastshipdate ), 'DateFormat( myopportunity.getOppProducts.forecastshipdate, "dd/mm/yyyy" )', de( "" ) )#", //[3]
	"#JSStringFormat( myopportunity.getOppProducts.probability )#", //[4]
	"#JSStringFormat( myopportunity.getOppProducts.specialprice )#", //[5]
	"#JSStringFormat( myopportunity.getOppProducts.sku )#", //[6]
	"#JSStringFormat( myopportunity.getOppProducts.subtotal )#", //[7]
	"#JSStringFormat( myopportunity.getOppProducts.unitprice )#", //[8]
	"#JSStringFormat( myopportunity.getOppProducts.promotionalPrice )#", //[9]
	"#JSStringFormat( myopportunity.getOppProducts.productPromotionID )#", //[10]
	"#JSStringFormat( myopportunity.getOppProducts.specialPriceApproverPersonID )#", //[11]
	"#JSStringFormat( myopportunity.getOppProducts.productid )#", //[12]
	"#JSStringFormat( myopportunity.getOppProducts.stockLocation )#", //[13]
	"#JSStringFormat( myopportunity.getOppProducts.SPManualEdit )#", //[14]
	"#JSStringFormat( myopportunity.getOppProducts.approvedprice )#", //[15]
	// 2010-02-25			NAS			P-LEX039 - Demo checkbox addition for 'Deal Registration'
	"#JSStringFormat( myopportunity.getOppProducts.demo )#" //[16]
	);
</cfloop>

<cfif useOppProductStockLocation and isQuery(qValidStockLocations)>
	<cfloop query="qValidStockLocations">
		<cfif qValidStockLocations.optionValue neq "">
		validStockLocationObj["#JSStringFormat( qValidStockLocations.optionValue )#"] = new Array("#JSStringFormat( qValidStockLocations.optionDescription )#");
		</cfif>
	</cfloop>
</cfif>


function eventOppProductEditClear()
{
	with ( document.forms["oppProductForm"] )
    {
		elements["frmProductIDEdit"].value = "";
		elements["frmOppProductIDEdit"].value = "";
		elements["frmUnitPriceEdit"].value = "";
		elements["frmDescriptionEdit"].value = "";
		elements["frmQuantityEdit"].value = "";
		elements["frmForecastShipDateEdit"].value = "";
		elements["frmSPManualEdit"].value = "";
		elements["frmSpecialPriceEdit"].value = "";
		elements["frmApprovedPriceEdit"].value = "";
		elements["reduceByPerc"].value = "";
		elements["calcDiscount"].disabled = true;
		// 2010-02-25			NAS			P-LEX039 - Demo checkbox addition for 'Deal Registration'
		elements["frmDemoEdit"].value = "";
		
	}
}

function eventOppProductOnClick( callerObject )
{
  var allowNewOppProductID = true;
  var radioButtonIndex;
  if ( dirtyOppProductsEdit && document.forms["oppProductForm"]["frmOppProductIDEdit"].value != "" )
  {
    allowNewOppProductID = confirm( "phr_opp_DoYouWishToForgetTheChanges" + document.forms["oppProductForm"]["frmDescriptionEdit"].value );
    if ( !allowNewOppProductID )
    {
// reset the radio button back to the original one
      for ( radioButtonIndex = 0; radioButtonIndex < document.forms["oppProductForm"]["frmEditOppProduct"].length; radioButtonIndex++ )
      {
        if ( document.forms["oppProductForm"]["frmEditOppProduct"][radioButtonIndex].value == document.forms["oppProductForm"]["frmOppProductIDEdit"].value )
        {
          document.forms["oppProductForm"]["frmEditOppProduct"][radioButtonIndex].checked = true;
          break;
        }
      }
    }
  }
  if ( allowNewOppProductID )
  {
    dirtyOppProductsEdit = false;
	touchedSpecialPrice = false;
    resetStyles();
    with ( document.forms["oppProductForm"] )
    {
      elements["frmProductIDEdit"].value = resultSetOppProducts[callerObject.value][12];
      elements["frmOppProductIDEdit"].value = resultSetOppProducts[callerObject.value][0];
	  elements["frmUnitPriceEdit"].value = resultSetOppProducts[callerObject.value][8];
      elements["frmDescriptionEdit"].value = resultSetOppProducts[callerObject.value][6].substring(0, 19); //GCC set to be SKU for display: description not editable
      elements["frmQuantityEdit"].value = resultSetOppProducts[callerObject.value][2];
      elements["frmForecastShipDateEdit"].value = resultSetOppProducts[callerObject.value][3];
	  elements["frmSPManualEdit"].value = 1;
	  elements["frmApprovedPriceEdit"].value = resultSetOppProducts[callerObject.value][15];
	  // 2010-02-25			NAS			P-LEX039 - Demo checkbox addition for 'Deal Registration'
	  if (resultSetOppProducts[callerObject.value][16] == 1)
	  {
	  	elements["frmDemoEdit"].checked = true;
	  }
	  // End
	 
	  <cfif isDefined ("qValidStockLocations") and useOppProductStockLocation and isQuery(qValidStockLocations)> 
	  for ( optionIndex = 0; optionIndex < elements["frmStockLocationEdit"].length; optionIndex++ )
		{
			if ( elements["frmStockLocationEdit"][optionIndex].value.toLowerCase() == resultSetOppProducts[callerObject.value][13].toLowerCase() )
			{
				elements["frmStockLocationEdit"].selectedIndex = optionIndex;
				break;
			}
		}
		</cfif>
      elements["frmProbabilityEdit"].value = ( resultSetOppProducts[callerObject.value][4] != "" ) ? resultSetOppProducts[callerObject.value][4] : "<cfoutput>#url["probability"]#</cfoutput>" ;
// promotional price or approved special price
      if ( resultSetOppProducts[callerObject.value][10] == "" && resultSetOppProducts[callerObject.value][11] == "" )
      {
        elements["frmSpecialPriceEdit"].value = resultSetOppProducts[callerObject.value][5];
        elements["frmSpecialPriceEdit"].readOnly = false;
      }
      else
      {
        elements["frmSpecialPriceEdit"].value = ( resultSetOppProducts[callerObject.value][10] != "" ) ? resultSetOppProducts[callerObject.value][9] : resultSetOppProducts[callerObject.value][5];
        elements["frmSpecialPriceEdit"].readOnly = true;
        elements["frmSpecialPriceEdit"].style.color = "silver";
      }
    }
	<cfif useOppProductQuantityEdit>
    	document.forms["oppProductForm"]["frmQuantityEdit"].focus();
	</cfif>
  }
}

function eventOppProductEditOnChange( callerObject )
{
  if ( callerObject.name == "frmSpecialPriceEdit" && callerObject.value != "" ) touchedSpecialPrice = true;
  callerObject.style["color"] = "red";
  dirtyOppProductsEdit = true;
  document.forms["oppProductForm"].elements["frmSPManualEdit"].value = 1;
  return true;
}

function resetStyles()
{
  normalColour = "black";
  with ( document.forms["oppProductForm"] )
  {
    elements["frmQuantityEdit"].style["color"] = normalColour;
    elements["frmForecastShipDateEdit"].style["color"] = normalColour;
    elements["frmProbabilityEdit"].style["color"] = normalColour;
    elements["frmSpecialPriceEdit"].style["color"] = normalColour;
	elements["frmApprovedPriceEdit"].style["color"] = normalColour;
  }
  return true;
}

function eventOppProductDuplicate( callerObject )
{
  var errorMessage = "";
  with ( document.forms["oppProductForm"] )
  {
    if ( elements["frmOppProductIDEdit"].value == "" ) errorMessage = "phr_opp_SelectAProductViaRadioButtons";

    if ( errorMessage != "" ) alert( errorMessage )
    else
    {
		dirtyOppProductsEdit = true;
		if( confirm( "phr_opp_DoYouWishToCopyThisRecord" ) )
		{
		}
		else
		{
			elements["frmQuantityEdit"].value = "";
			elements["frmForecastShipDateEdit"].value = "";
			elements["frmProbabilityEdit"].selectedIndex = 0;
			elements["frmSpecialPriceEdit"].value = "";
			elements["frmApprovedPriceEdit"].value = "";
		}

//		elements["frmTask"].value = "addproduct";
/*
		elements["productID"].value = elements["frmProductIDEdit"].value;
		elements["quantity"].value = ""; //elements["frmQuantityEdit"].value;
		elements["forecastShipDate"].value = ""; //elements["frmForecastShipDateEdit"].value;
		elements["Probability"].selectedIndex = 0; //elements["frmProbabilityEdit"].selectedIndex;
		elements["specialPrice"].value = ""; //elements["frmSpecialPriceEdit"].value;
		elements["approvedPrice"].value = ""; //elements["frmApprovedPriceEdit"].value;
*/
/*
		elements["productGroupID"].selectedIndex = 1;
		elements["thisProductID"].selectedIndex = 1;
		elements["thisProductID"].value = elements["frmOppProductIDEdit"].value;
*/	
//		submit();
	}
  }
}

function eventOppProductAddDuplicate( callerObject )
{
/*
	with ( document.forms["oppProductForm"] )
	{
		elements["frmTask"].value = "addproduct";

		elements["productID"].value = elements["frmProductIDEdit"].value;
		elements["quantity"].value = elements["frmQuantityEdit"].value;
		elements["forecastShipDate"].value = elements["frmForecastShipDateEdit"].value;
		elements["Probability"].selectedIndex = elements["frmProbabilityEdit"].selectedIndex;
		elements["specialPrice"].value = elements["frmSpecialPriceEdit"].value;
		elements["approvedPrice"].value = elements["frmApprovedPriceEdit"].value;
	}
*/
  var errorMessage = "";
  with ( document.forms["oppProductForm"] )
  {
    if ( elements["frmOppProductIDEdit"].value == "" ) errorMessage = "phr_opp_SelectAProductViaRadioButtons";

    if ( errorMessage != "" ) alert( errorMessage )
    else
    {
		eventOppProductSave( true );
	}
  }
}

function eventOppProductSave( fromDuplication )
{
  errorMessage = "";
  with ( document.forms["oppProductForm"] )
  {
    if ( elements["frmOppProductIDEdit"].value == "" ) errorMessage = "phr_opp_SelectAProductViaRadioButtons";
    if ( elements["frmQuantityEdit"].value != "" && elements["frmQuantityEdit"].value.search( /^[0-9]*$/ ) != 0 )  errorMessage = "phr_opp_QuantityMustBeNumeric";
    if ( elements["frmSpecialPriceEdit"].value != "" && isNaN( Number( elements["frmSpecialPriceEdit"].value.replace( /,/g, "." ) ) ) )  errorMessage = "phr_opp_SpecialPriceMustBeNumeric";
	if ( elements["frmApprovedPriceEdit"].value != "" && isNaN( Number( elements["frmApprovedPriceEdit"].value.replace( /,/g, "." ) ) ) )  errorMessage = "phr_opp_ApprovedPriceMustBeNumeric";
	if ( fromDuplication && elements["frmQuantityEdit"].value.search( /^[0-9]+$/ ) != 0 ) errorMessage = "phr_opp_QuantityMustBeNumeric";
// || isNaN( Number( elements["quantity"].value ) ) ) 

	<cfif MakeForecastShipDateMandatory>
		if ( elements["frmForecastShipDateEdit"].value == "" )
		{
			errorMessage = "phr_opp_shipDateIsRequired";
		}
	</cfif>

    if ( errorMessage != "" ) alert( errorMessage )
    else
    {
      if ( ! fromDuplication )
	  	elements["frmTask"].value = "editProduct";
	else
	{
		elements["frmTask"].value = "addproduct";

		elements["productID"].value = elements["frmProductIDEdit"].value;
		elements["quantity"].value = elements["frmQuantityEdit"].value;
		elements["forecastShipDate"].value = elements["frmForecastShipDateEdit"].value;
		elements["Probability"].selectedIndex = elements["frmProbabilityEdit"].selectedIndex;
		elements["specialPrice"].value = elements["frmSpecialPriceEdit"].value;
		elements["approvalPrice"].value = elements["frmApprovedPriceEdit"].value;
	}
	  // SWJ commented these out.
      if ( elements["frmSpecialPriceEdit"].value != "" ) elements["frmSpecialPriceEdit"].value = parseFloat( elements["frmSpecialPriceEdit"].value.replace( /,/g, "." ) );
      if ( elements["frmApprovedPriceEdit"].value != "" ) elements["frmApprovedPriceEdit"].value = parseFloat( elements["frmApprovedPriceEdit"].value.replace( /,/g, "." ) );
      //if ( touchedSpecialPrice ) specialPriceReasonWindow( elements["frmDescriptionEdit"].value )
	  //else 
	  submit();
    }
  }
  return true;
}

//function eventDeleteOnClick( callerObject )
//{
// var errorMessage = "";
//  var checkboxIndex;
//  with ( document.forms["oppProductForm"] )
//  {
//    if ( !(elements["frmProductDeletionList"].length) && ! elements["frmProductDeletionList"].checked ) errorMessage = "Please select at least one item for deletion.";
//    if ( elements["frmProductDeletionList"].length )
//    {
//      errorMessage = "Please select at least one item for deletion.";
//      for ( checkboxIndex = 0; checkboxIndex < elements["frmProductDeletionList"].length; checkboxIndex++ )
//      if ( elements["frmProductDeletionList"][checkboxIndex].checked )
//      {
//        errorMessage = "";
//        break;
//      }
//    }
//    if ( errorMessage != "" ) alert( errorMessage )
//    {
//      elements["frmTask"].value = "removeProduct";
//      submit();
//    }
//  }
//  return true;
//} 


function specialPriceReasonWindow( productDescription )
{
  var windowFeatutres = "";
  var targetUrl = "";
  windowFeatutres += "directories=no,";
  windowFeatutres += "hotkeys=no,";
  windowFeatutres += "location=no,";
  windowFeatutres += "menubar=no,";
  windowFeatutres += "resizable=no,";
  windowFeatutres += "scrollbars=yes,";
  windowFeatutres += "status=no,";
  windowFeatutres += "toolbar=no,";
  windowFeatutres += "fullscreen=no,";
  targetUrl = "oppProductSpecialPrice.cfm?description=";
  targetUrl += escape( productDescription );
  targetUrl += "&entityID=";
  targetUrl += escape( "<cfoutput>#JSStringFormat( url.opportunityID )#</cfoutput>" );
  newSpecialPriceReasonWindow = window.open( targetUrl, "approval", windowFeatutres, true );
  return true;
}

function reSort(sortOrder) {
	var form = document.oppProductForm;
	form.sortOrder.value = sortOrder;
	form.submit();
}


function specialPriceApprovalWindow( callerObject, oppProductID )
{
  var windowFeatutres = "";
  var targetUrl = "";
  callerObject.blur();
  windowFeatutres += "directories=no,";
  windowFeatutres += "hotkeys=no,";
  windowFeatutres += "location=no,";
  windowFeatutres += "menubar=no,";
  windowFeatutres += "resizable=no,";
  windowFeatutres += "scrollbars=yes,";
  windowFeatutres += "status=no,";
  windowFeatutres += "toolbar=no,";
  windowFeatutres += "fullscreen=no,";
  targetUrl += "oppProductApproval.cfm?countryID=";
  targetUrl += escape( "<cfoutput>#JSStringFormat( url["countryID"] )#</cfoutput>" );
  targetUrl += "&opportunityID=";
  targetUrl += escape( "<cfoutput>#JSStringFormat( url["opportunityID"] )#</cfoutput>" );
  targetUrl += "&oppProductID=";
  targetUrl += escape( oppProductID );
  newSpecialPriceReasonWindow = window.open( targetUrl, "approval", windowFeatutres, true );
  return true;
}

function specialPriceReasonCallBack( specialPriceReason )
{
  with ( document.forms["oppProductForm"] )
  {
    elements["frmSpecialPriceReason"].value = specialPriceReason;
    submit();
  }
}

function specialPriceApprovalCallBack( specialPriceReason )
{
  document.location.replace( document.location.href );
}

function showCalendar( callerObject )
{
  dirtyOppProductsEdit = true;
  eventOppProductEditOnChange( document.forms["oppProductForm"]["frmForecastShipDateEdit"] );
  show_calendar( 'oppProductForm.frmForecastShipDateEdit', null, '', 'DD-MM-YYYY', null, 'AllowWeekends=No;CurrentDate=Today' );
}

function toggleVisibility( divName, callerObject )
{
  document.all[divName].style.display = ( document.all[divName].style.display == "" ) ? "none" : "";
  if ( callerObject )
  {
    callerObject.value = ( document.all[divName].style.display == "" ) ? String.fromCharCode( 171 ) : String.fromCharCode( 187 );
    callerObject.blur();
  }
  return true;
}
//-->
</script>
</cf_translate>
</cfoutput>
<!--- --------------------------------------------------------------------- --->

<!--- START: 2009-11-11	AJC P-LEX039 Competitive products --->
	 </cfif>
	 
<!--- END: 2009-11-11	AJC P-LEX039 Competitive products --->
</cfif>

<cfif request.relaycurrentUser.isInternal>
	
	
</cfif>
</cf_translate>

