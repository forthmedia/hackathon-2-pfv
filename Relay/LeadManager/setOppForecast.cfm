<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File name:		setOppForecast.cfm
Author:			YMA
Date created:	2013-02-06

	Objective - Set checked opportunities to Forecast or Not Forecast.
				Steps involved are:
				a) Receive a list of entityID's to update
				b) Check if we are setting or unsetting forecast
				c) Update the records

Date Tested:
Tested by:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed

Enhancement still to do:

--->

<CFPARAM NAME="frmopportunityIDs" TYPE="string" DEFAULT="0">
<CFPARAM NAME="action" TYPE="string">

<cfif isDefined('action') and (action eq "add" or action eq "remove")>
	<CFQUERY NAME="getOppDetails" datasource="#application.siteDataSource#">
		select detail, opportunityID from opportunity 
		where opportunityID  in ( <cf_queryparam value="#frmopportunityIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
		order by detail
	</CFQUERY>
	
	<!--- *********************************************************************
		  we're updating the forecast status --->
	<CFQUERY NAME="setForecast" datasource="#application.siteDataSource#">
		update opportunity 
		set forecast =  <cfif action eq "add">1<cfelse>0</cfif>
		where opportunityID  in ( <cf_queryparam value="#form.frmopportunityIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	</CFQUERY>
	
	<cf_head>
		<cf_title>Set Opportunity Forecast Status</cf_title>
	</cf_head>
	
	<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
		<TR>
			<CFOUTPUT><TD>The forecast status has been <cfif action eq "add">added<cfelse>removed</cfif> for the following opportunities:<BR>
			<ol>
			<cfloop query="getOppDetails" ><li>#detail# (#htmleditformat(opportunityID)#)</li></cfloop>
			</ol>
			</TD></CFOUTPUT>
		</TR>
	</TABLE>
<CFELSE>
	<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
		<TR>
			<TD>You must pass in action as 'add' or 'remove'</TD>
		</TR>
	</TABLE>
</CFIF>


