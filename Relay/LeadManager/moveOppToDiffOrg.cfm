<!--- �Relayware. All Rights Reserved 2014 --->
<!---

File name:		moveOppToDiffOrg.cfm
Author:			SWJ
Date created:	2004-01-29

	Objective - to move one or more entities from one to a new organisation record.
				Steps involved are:
				a) receive a list of entityID's to move
				b) search for the org to move them to
				c) choose the one and update the database record

Date Tested:
Tested by:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2013-02-22			RMB			433882 - ADDED IsNumeric cfif to search against organisationID

Enhancement still to do:



--->

<CFPARAM NAME="frmopportunityIDs" TYPE="string" DEFAULT="0">

<CFQUERY NAME="getOppDetails" datasource="#application.siteDataSource#">
	select detail, opportunityID from opportunity 
	where opportunityID  in ( <cf_queryparam value="#frmopportunityIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	order by detail
</CFQUERY>

<!--- *********************************************************************
	  we're searching for the organisation to link them to --->
<cfif isDefined('form.frmSearch') eq "yes">

	<!--- 2013-02-22 - RMB - 433882 - ADDED IsNumeric cfif to search against organisationID --->
	<CFQUERY NAME="getOrganisation" datasource="#application.siteDataSource#">
		select organisationName+' ('+ISOCode+', '+postalCode+', '+l.sitename+')' as displayValue, 
		convert(varchar(15),o.organisationID)+'-'+convert(varchar(15),l.locationID) as IDValue 
		from organisation o inner join location l on
		o.organisationID = l.organisationID
		inner join country c on
		l.countryID = c.countryID
		where
			1=1
			<cfif NOT IsNumeric(form.frmSearchString)>
				AND organisationName  like  <cf_queryparam value="#form.frmSearchString#%" CFSQLTYPE="CF_SQL_VARCHAR" >
			<cfelse>
				AND o.organisationID  =  <cf_queryparam value="#form.frmSearchString#" CFSQLTYPE="CF_SQL_INTEGER" >			
			</cfif>
	</CFQUERY>
	
</CFIF>

<!--- *********************************************************************
	  we're doing the update --->
<cfif isDefined('form.frmMove') and form.frmMove eq "move">
	<CFQUERY NAME="updatePerson" datasource="#application.siteDataSource#">
		update opportunity 
		set entityID =  <cf_queryparam value="#ListFirst(form.frmSelectedOrgID,"-")#" CFSQLTYPE="CF_SQL_INTEGER" > 
		where opportunityID  in ( <cf_queryparam value="#form.frmopportunityIDs#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	</CFQUERY>
	<CFQUERY NAME="getOrgDetails" datasource="#application.siteDataSource#">
		select organisationName+' ('+ISOCode+')' +' ID: ' + cast(organisationID as varchar(25)) as displayValue 
		from organisation o 
		inner join country c on	o.countryID = c.countryID 
		where o.organisationID =  <cf_queryparam value="#ListFirst(form.frmSelectedOrgID,'-')#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</CFQUERY>

	<CFSET message = "#valuelist(getOppDetails.detail,", ")# moved to organisationID #ListFirst(form.frmSelectedOrgID,'-')#">
</CFIF>

<cf_head>
	<cf_title>Transfer Opportunity</cf_title>
</cf_head>

<cfset application.com.request.setTopHead(PageTitle='Transfer an opportunity to a different organisation')>

<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" BGCOLOR="White" CLASS="withBorder">
	<cfif not isDefined('form.frmMove')>
		<TR>
			<TD>The following opportunities will be moved:<BR>
			<ol>
			<CFOUTPUT><cfloop query="getOppDetails" ><li>#detail# (#htmleditformat(opportunityID)#)</li></cfloop></CFOUTPUT>
			</ol>
			</TD>
		</TR>
	</cfif>
	<!--- *********************************************************************
	  we don't have any opportunityIDs so we first need to search for them --->
	<CFIF frmopportunityIDs eq "0">
		<cfform action="moveOppToDiffOrg.cfm" method="POST" name="personForm">
			<TR>
				<TD VALIGN="top" CLASS="label">opportunityID</TD>
				<TD><CFINPUT TYPE="Text" NAME="frmopportunityIDs" MESSAGE="You must put a valid opportunityID" REQUIRED="Yes" SIZE="15"></TD>
			</TR>
			<TR>
				<TD>&nbsp;</TD>
				<TD><INPUT TYPE="submit" VALUE="Search"></TD>
			</TR>
		</CFFORM>
		
	<!--- *********************************************************************
	  we've got opportunityID's but the getOrganisations has not been executed 
	  so we need the user to put in an orgName --->
	<CFELSEIF frmopportunityIDs neq "0" and not isDefined('message')>
		<cfform action="moveOppToDiffOrg.cfm" method="POST" name="orgSearchForm">
			<TR>
				<TD VALIGN="top"><HR WIDTH="100%" SIZE="1" COLOR="steelblue"></TD>
			</TR>
			<TR>
				<TD NOWRAP>Search for where to move them:&nbsp;<CFINPUT TYPE="Text" NAME="frmSearchString" MESSAGE="You must put a valid opportunityID" REQUIRED="Yes" SIZE="15">
				<INPUT TYPE="submit" NAME="frmSearch" VALUE="Search"></TD>
			</TR>
			<TR>
				<TD></TD>
			</TR>
			<CFOUTPUT><CF_INPUT TYPE="hidden" NAME="frmopportunityIDs" VALUE="#frmopportunityIDs#"></CFOUTPUT>
		</CFFORM>
	</CFIF>
		<!--- *********************************************************************
	  we've got opportunityID's but the getOrganisations has not been executed 
	  so we need the user to put in an orgName --->
	<CFIF frmopportunityIDs neq "0" and isDefined('getOrganisation.recordCount') and getOrganisation.recordCount gt 0 and not isDefined('form.frmMove')>
		<cfform action="moveOppToDiffOrg.cfm" method="POST" name="orgSearchForm2">
			<TR>
				<TD VALIGN="top"><HR WIDTH="100%" SIZE="1" COLOR="steelblue"></TD>
			</TR>
			<TR>
				<TD>Select which organisation to move them to:<BR>
				<CFSELECT NAME="frmSelectedOrgID"
			          SIZE="15"
			          MESSAGE="You must select an organisation"
			          QUERY="getOrganisation"
			          VALUE="IDValue"
			          DISPLAY="displayValue"
			          REQUIRED="Yes"></CFSELECT>
				</TD>
			</TR>
			<TR>
				<TD ALIGN="right"><INPUT TYPE="submit" NAME="frmMove" VALUE="move">&nbsp;&nbsp;&nbsp;</TD>
			</TR>
			<CFOUTPUT><CF_INPUT TYPE="hidden" NAME="frmopportunityIDs" VALUE="#frmopportunityIDs#"></CFOUTPUT>
		</CFFORM>
	</CFIF>
	
	<CFIF isDefined('message')>
		<TR>
			<TD VALIGN="top"><HR WIDTH="100%" SIZE="1" COLOR="steelblue"></TD>
		</TR>
		<TR><CFOUTPUT>
			<TD CLASS="messageText">The following opportunities :<BR>
				<ol>
					<cfloop query="getOppDetails" ><li>#detail# (#htmleditformat(opportunityID)#)</li></cfloop>
				</ol>
				have been linked to #htmleditformat(getOrgDetails.displayValue)#
			</TD>
			</CFOUTPUT>
		</TR>
		<tr><td><input type=button value="Close Window" onClick="javascript:window.opener.location.reload(true); if (window.opener.progressWindow) { window.opener.progressWindow.close() } ;self.close();"></td></tr>
	</CFIF>
	
</TABLE>



