<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			oppProductSpecialPrice.cfm
Author:				KAP
Date started:		2003-08-20
	
Description:			

called by oppProductList.cfm as a pop up for special price reasons

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
20-Aug-2003			KAP			Initial version

Possible enhancements:


 --->

<cfparam name="url.description" type="string" default="Unknown product">
<cfparam name="url.entityID" type="string" default="0">

<cf_translate>


<cf_head>
<cf_title>Phr_Sys_oppProductSpecialPricePopUpTitle</cf_title>
<meta http-equiv="expires" content="wed, 01 jan 2003 00:00:00 gmt" />
<meta http-equiv="cache-control" content="no-cache, must-revalidate" />
<meta http-equiv="pragma" content="no-cache" />

<SCRIPT type="text/javascript">
<!--
function eventWindowsOnLoad( callerObject )
{
  var thisScreenWidth = 1024;
  var thisScreenHeight = thisScreenWidth * 0.618034;
  callerObject.resizeTo( thisScreenWidth, thisScreenHeight );
  callerObject.moveTo( ( screen.AvailWidth - thisScreenWidth ) / 2, ( screen.AvailHeight - thisScreenHeight ) / 2 );
  callerObject.focus();
}

function eventFormOnSubmit( callerObject )
{
  var errorMessage = "";
  if ( callerObject.elements["frmSpecialPriceReason"].value == "" || callerObject.elements["frmSpecialPriceReason"].value.search( /^[ ]+$/ ) == 0 )
  {
    errorMessage = "Phr_Sys_oppProductSpecialPriceInvalidReason";
  }
  if ( errorMessage != "" )
  {
    alert( errorMessage );
  }
  else
  {
    window.opener.specialPriceApprovalCallBack( callerObject.elements["frmSpecialPriceReason"].value );
    self.close();
  }
  return false;
}

function eventFormOnCancel( callerObject )
{
  self.close();
  return true;
}
//-->
</script>

</cf_head>

<cf_body onLoad="eventWindowsOnLoad(this);">

<table border="0" cellspacing="0" cellpadding="0" class="withBorder">
<tr><td>

	<table border="0" cellspacing="1" cellpadding="3" align="center" class="withBorder">
		<form action="" onSubmit="return eventFormOnSubmit(this);">
		<tr><th colspan="3">Phr_Sys_oppProductSpecialPricing</th></tr>
		<tr><td colspan="3" class="label"><br>Phr_Sys_oppProductSpecialPriceRequest<br><br></td></tr>
		<tr><td colspan="3" align="center"><br><strong><cfoutput>#url["description"]#</cfoutput></strong><br><br></td></tr>
		<tr><td colspan="3" class="label"><br>Phr_Sys_oppProductSpecialPriceEnterReason<br><br></td></tr>
		<tr><td class="label"><br>Phr_Sys_oppProductSpecialPriceReason<br><br></td><td colspan="2" align="center"><textarea cols="60" rows="3" name="frmSpecialPriceReason"></textarea></td></tr>
		<tr><td colspan="3" class="label">&nbsp;</td></tr>
		<tr><td colspan="3" align="center"><br><input type="submit" name="frmSubmit" value="Phr_Continue">&nbsp;<input type="button" name="frmCancel" value="Phr_Cancel" onClick="eventFormOnCancel();"><br><br></td></tr>
		</form>
	</table>
	
	<table border="0" cellspacing="1" cellpadding="3" align="center" class="withBorder">
		<tr>
			<td>
				<cf_relatedFile action="put,list" entity="#url["entityID"]#" entitytype="opportunity">
			</td>
		</tr>
	</table>

</td></tr>
</table>




</cf_translate>

