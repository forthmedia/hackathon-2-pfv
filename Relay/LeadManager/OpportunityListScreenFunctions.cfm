<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			OpportunityListScreenFunctions.cfm
Author:				KAP
Date started:		06-Nov-2003

Description:

Called by LeadandOppList.cfm via tableFromQueryObject custom tag

Example of inclusion of extra logic in the tableFromQueryObject custom tag

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
05-Nov-2003			KAP			Initial version
27/08/2014			AXA			added title text to display tooltip for approval icons and added alt text for accessibility.
Possible enhancements:

--->

<!--- --------------------------------------------------------------------- --->
<!--- optional function called by tableFromQueryObject custom tag --->
<!--- --------------------------------------------------------------------- --->
<cffunction name="udfTableCallBack" access="private" returntype="string" output="no">
	<cfargument name="udfColumnName" type="string" required="yes" />
	<cfargument name="udfColumnValue" type="string" required="yes" />
	<cfargument name="udfColumnType" type="string" required="yes" /> <!--- 0=unknown, 1=number, 2=currency, 3=date, 4=string --->

	<cfset udfReturnValue = "" />

	<cfswitch expression="#udfColumnName#">

<!---
	<cfcase value="account_manager">
		<cfif udfColumnValue eq "">
			<cfset udfReturnValue = udfReturnValue & '<font color="red">&lt;none&gt;</font>' />
		</cfif>
	</cfcase>
--->


<!---
	<cfcase value="calculated_budget_this_quarter">
		<!--- NOTE: the calculated_budget_this_quarter is the holder column and then the unapproved_special_price.current row is the logic check. --->
		<cfif attributes.queryObject["unapproved_special_price"][attributes.queryObject.CurrentRow] gt "0">
			<cfset udfReturnValue = udfReturnValue & '<img src="/images/MISC/iconInformationRed.gif" alt="This opportunity contains unaproved special pricing." width="14" height="14" border="0">' />
		</cfif>
	</cfcase>

	<cfcase value="calculated_budget">
		<!--- NOTE: the calculated_budget_this_quarter is the holder column and then the unapproved_special_price.current row is the logic check. --->
		<cfif attributes.queryObject["unapproved_special_price"][attributes.queryObject.CurrentRow] gt "0">
			<cfset udfReturnValue = udfReturnValue & '<img src="/images/MISC/iconInformationRed.gif" alt="This opportunity contains unaproved special pricing." width="14" height="14" border="0">' />
		</cfif>
	</cfcase>
--->
	<cfcase value="calculated_budget">

		<!--- NOTE: the pricing_status is the holder column and then the oppPricingStatusID.current row is the logic check. --->
		<cfif listfind('3,4,5,6', attributes.queryObject["oppPricingStatusID"][attributes.queryObject.CurrentRow]) neq 0>

			<cfset udfReturnValue = udfReturnValue & ' <img src="/images/MISC/iconInformationRed.gif" alt="Required approval level." title="Required approval level: #attributes.queryObject["SPRequiredApprovalLevel"][attributes.queryObject.CurrentRow]#
Current approval level: #attributes.queryObject["SPCurrentApprovalLevel"][attributes.queryObject.CurrentRow]#." width="14" height="14" border="0">' />
		<cfelseif attributes.queryObject["oppPricingStatusID"][attributes.queryObject.CurrentRow] eq "8">
			<cfset udfReturnValue = udfReturnValue & ' <img src="/images/MISC/iconInformationApproved.gif" alt="Special pricing full approved." title="Special pricing fully approved to level: #attributes.queryObject["SPCurrentApprovalLevel"][attributes.queryObject.CurrentRow]#" width="14" height="14" border="0">' />
		</cfif>
	</cfcase>


<!--- ---------------------------------------------------------------------
	<cfcase value="isocode">
		<cfswitch expression="#udfColumnValue#">

		<cfcase value="gb">
			<cfset udfReturnValue = udfReturnValue & '<img src="/images/flags/18x12/en-uk.gif" width="18" height="12" align="middle">' />
		</cfcase>

		<cfcase value="es">
			<cfset udfReturnValue = udfReturnValue & '<img src="/images/flags/18x12/es.gif" width="18" height="12" align="middle">' />
		</cfcase>

		<cfcase value="us">
			<cfset udfReturnValue = udfReturnValue & '<img src="/images/flags/18x12/en.gif" width="18" height="12" align="middle">' />
		</cfcase>

		<cfcase value="de">
			<cfset udfReturnValue = udfReturnValue & '<img src="/images/flags/18x12/de.gif" width="18" height="12" align="middle">' />
		</cfcase>

		<cfdefaultcase>
		</cfdefaultcase>

		</cfswitch>
	</cfcase>--->
<!--- --------------------------------------------------------------------- --->

	<cfdefaultcase>
		<cfreturn>
	</cfdefaultcase>

	</cfswitch>

	<cfreturn udfReturnValue />
</cffunction>
<!--- --------------------------------------------------------------------- --->

