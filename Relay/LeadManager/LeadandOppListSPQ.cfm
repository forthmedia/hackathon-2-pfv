<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			leadandOppListSPQ.cfm
Author:				??
Date started:		??/??/??

Description:

Usage:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2008-07-22			AJC			Added top header
2008-07-22			AJC			Bugs - ATI SPQ access not working - Have added a requesttime out to 120 seconds
2007-10-22			NJH			Bugs -ATI Issue 835. Added a date filter to filter the the initial data set. It appears that it once existed but was then removed.
2010/08/04			NJH			P-PAN002 - added rollup reporting
2010/08/26			NJH			8.3 - opened new tabs for account and opp details
2011/06/01 			PPB 		REL106 use countryScopeOpportunityRecords setting
2012/03/12 			IH	 		Case 426033 Remove duplicate content-Disposition header and set default filename for Excel
Possible enhancements:


 --->

<!--- START: 2008-07-22 AJC Bugs - ATI SPQ access not working - Have added a requesttime out to 120 seconds --->
<cfsetting enablecfoutputonly="no" requesttimeout="120">
<!--- END: 2008-07-22 AJC Bugs - ATI SPQ access not working - Have added a requesttime out to 120 seconds --->

<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">
<cfparam name="openAsExcel" type="boolean" default="false">
<cfset excelFileName="opportunities.xls">

<!--- 2012-07-26 PPB P-SMA001 commented out
<cfif application.com.settings.getSetting("leadManager.countryScopeOpportunityRecords")>
	<cfinclude template="/templates/qryGetCountries.cfm">
</cfif>
--->

<cfparam name="sortOrder" default="account">
<cfparam name="numRowsPerPage" default="50">
<cfparam name="startRow" default="1">
<!--- The keyColumn list group of params below control which columns show URL links
		where they link to and what key should be used in the URL --->
<cfparam name="keyColumnList" type="string" default="detail,account,PRICING_STATUS,SPApprovalNumber"><!--- this can contain a list of columns that can be edited --->
<cfparam name="keyColumnUrlList" type="string" default=" , , ,specialPricePrint.cfm?openAsWord=true&entityID=">
<cfparam name="keyColumnOnClickList" type="string" default="javascript:openNewTab('Opportunity ##opportunity_ID##'*comma'Opportunity ##opportunity_ID##'*comma'/leadManager/opportunityEdit.cfm?opportunityid=##opportunity_ID##'*comma{iconClass:'leadmanager'});return false,javascript:openNewTab('Account Details ##entityID##'*comma'Account Details'*comma'/data/dataframe.cfm?frmsrchOrgID=##entityID##'*comma{iconClass:'accounts'});return false,javascript:openNewTab('Special Pricing ##opportunity_ID##'*comma'Special Pricing ##opportunity_ID##'*comma'/leadManager/specialPriceEdit.cfm?entityID=##opportunity_ID##'*comma{iconClass:'leadmanager'});return false, "><!--- this can contain a list of matching columns that contain the URL of the editor template --->
<cfparam name="keyColumnKeyList" type="string" default="opportunity_ID,entityID,opportunity_ID,opportunity_ID"><!--- this can contain a list of matching key columns e.g. primary key --->

<cfparam name="radioFilterLabel" type="string" default=""><!--- this can contain a list of columns that can be edited --->
<cfparam name="radioFilterDefault" type="string" default=""><!--- this can contain a list of matching columns that contain the URL of the editor template --->
<cfparam name="radioFilterName" type="string" default=""><!--- this can contain a list of matching key columns e.g. primary key --->
<cfparam name="radioFilterValues" type="string" default=""><!--- this should contain a list of values for each radio button delimited by a minus sign --->

<cfparam name="dateFormat" type="string" default="last_Updated,Expected_Close_Date"><!--- this list should contain those fields you want in the date format dd-mmm-yy --->
<cfparam name="entityID" type="numeric" default="0">
<cfparam name="frm_vendoraccountmanagerpersonid" type="numeric" default="0">

<cfparam name="checkBoxFilterName" type="string" default="showSPComplete">
<cfparam name="checkBoxFilterLabel" type="string" default="Phr_Sys_Opp_ShowSPQComplete">

<cfparam name="alphabeticalIndexColumn" type="string" default="Account">

<cfparam name="SPQReport" type="string" default="yes">

<cfparam name="ShowTheseOppStageIDs" type="string" default="#application.com.settings.getSetting('leadManager.leadAndOppListSPQ.SPQShowTheseOppStageIDs')#">

<cfparam name="SPQleadAndOppListFilterSelectColumnList" type="string" default="Pricing_Status,Account,Month_Expected_Close,Quarter_Expected_Close,Status,Country,Account_Manager">
<cfparam name="SPQleadAndOppListShowColumns" type="string" default="ACCOUNT,DETAIL,PRICING_STATUS,ACCOUNT_MANAGER,COUNTRY,CALCULATED_BUDGET,SPApprovalNumber">
<cfparam name="SPQsearchColumnList" type="string" default="opportunity_id,account,product">
<cfparam name="SPQsearchColumnDisplayList" type="string" default="Phr_opportunityID,Account,Product">

<!--- PPB 2010/06/01 LID3514 (Panda) locale was Spanish; I change it to English just for the formatting of FRMWHERECLAUSEC & FRMWHERECLAUSED before calling DateQueryWhereClause (which expects English format) --->
<cfset oldlocal = GetLocale()>
<cfset SetLocale("English (UK)")>

<!--- NJH 2008/10/16 Bug Fix ATI Issue 835 filter the initial record set to bring back a years worth of data --->
<!--- NJH 2008/10/16 Bug Fix ATI Issue 835 start --->
<cfparam name="FORM.FRMWHERECLAUSEC" default="#left(monthAsString(month(dateadd("m",-6,now()))),3)#-#year(dateadd("m",-6,now()))#">
<cfparam name="FORM.FRMWHERECLAUSED" default="#left(monthAsString(month(dateadd("m",6,now()))),3)#-#year(dateadd("m",6,now()))#">

<cfset SetLocale(oldlocal)>	<!--- you must set it back again before including DateQueryWhereClause so DateQueryWhereClause saves the right locale as oldLocale --->


<!--- NJH 2008/10/16 introduced the datequerywhereclause again to filter the records.
	I set these variables here for backwards compatibility, but have commented them out as ATI currently the only ones using SPQ's
	and it introduces issues when exporting to excel ...  --->
<!--- <cfif isDefined("FORM.FRMWHERECLAUSEA") and not isDefined("FORM.FRMWHERECLAUSE1")>
	<cfset FORM.FRMWHERECLAUSE1 = FORM.FRMWHERECLAUSEA>
</cfif>
<cfif not isDefined("FORM.FRMWHERECLAUSE3")>
	<cfset FORM.FRMWHERECLAUSE3 = FORM.FRMWHERECLAUSEC>
</cfif>
<cfif not isDefined("FORM.FRMWHERECLAUSE4")>
	<cfset FORM.FRMWHERECLAUSE4 = FORM.FRMWHERECLAUSED>
</cfif> --->

<cfinclude template="/templates/DateQueryWhereClause.cfm">
<!--- NJH 2008/10/16 Bug Fix ATI Issue 835 end --->

<cfscript>
// call the opportunity component either directly or via a stub in userfiles/content/
if (fileexists(application.paths.code & "\cftemplates\relayOpportunity.cfc")) {
	componentPath = "code.CFTemplates.relayOpportunity";
	myopportunity = createObject("component",componentPath);}
else {myopportunity = createObject("component","relay.com.opportunity");}
myopportunity.dataSource = application.siteDataSource;
myopportunity.entityID = entityID;
myopportunity.opportunityView = application.com.settings.getSetting("leadManager.opportunityView");
/* 2012-07-26 PPB P-SMA001 commented out object.countryIDList is now redundant
if (application.com.settings.getSetting("leadManager.countryScopeOpportunityRecords")) {
	myopportunity.countryIDList = CountryList;}
 */
// myopportunity.liveStatusList = liveStatusList;
// frm_vendoraccountmanagerpersonid will be zero unless roleScopeSPQRecords is set to 1
// in the opportunityINI.cfm file.  This in turns fires logic in leadManager/application .cfm
myopportunity.vendorAccountManagerPersonID = frm_vendoraccountmanagerpersonid;
// if form.sortorder is defined use it otherwise set it a default value
if (isdefined("form.sortOrder") and form.sortOrder neq "") {
	myopportunity.sortOrder = form.sortOrder;}
else {myopportunity.sortOrder = "account";}
if (isdefined("FilterSelect") and FilterSelect neq ""
	and isdefined("FilterSelectValues") and FilterSelectValues neq "") {
	myopportunity.FilterSelect = FilterSelect;
	myopportunity.FilterSelectValues = FilterSelectValues;
	}
if (isdefined("FilterSelect2") and FilterSelect2 neq ""
	and isdefined("FilterSelectValues2") and FilterSelectValues2 neq "") {
	myopportunity.FilterSelect2 = FilterSelect2;
	myopportunity.FilterSelectValues2 = FilterSelectValues2;
	}
if (isdefined("radioFilterName") and radioFilterName neq "") {
	if (isDefined("form.radioFilterName")){
		myopportunity.radioFilterName = radioFilterName;
		myopportunity.radioFilterValue = form.radioFilterName;}
	else {
		myopportunity.radioFilterName = radioFilterName;
		myopportunity.radioFilterName = radioFilterDefault;}
	}
if (isdefined("FORM.showSPComplete") and FORM.showSPComplete eq "1") {
	myopportunity.showSPComplete = "1";
	}
	else { myopportunity.showComplete = "0"; }
// NJH 2008/10/16 ATI Issue 835 changed from FORM.FRMWHERECLAUSE1 to FORM.FRMWHERECLAUSEA, etc. for each frmWhereClause variable
if (isdefined("FORM.FRMWHERECLAUSEA")) {
	myopportunity.FRMWHERECLAUSE1 = FORM.FRMWHERECLAUSEA;
	}
if (isdefined("FORM.FRMWHERECLAUSEB")) {
	myopportunity.FRMWHERECLAUSE2 = FORM.FRMWHERECLAUSEB;
	}
if (isdefined("FORM.FRMWHERECLAUSEC")) {
	myopportunity.FRMWHERECLAUSE3 = FORM.FRMWHERECLAUSEC;
	}
if (isdefined("FORM.FRMWHERECLAUSED")) {
	myopportunity.FRMWHERECLAUSE4 = FORM.FRMWHERECLAUSED;
	}
if (SPQReport eq "yes") {
	myopportunity.SPQReport = "yes";
	}

myopportunity.alphabeticalIndexColumn = alphabeticalIndexColumn;
myopportunity.alphabeticalIndex = form["frmAlphabeticalIndex"];
myopportunity.ShowTheseOppStageIDs = ShowTheseOppStageIDs;
myopportunity.oppTypeID = "1,2,3";
myopportunity.listOpportunitiesInternal();
opportunityList = myOpportunity.qlistOpportunitiesInternal;
</cfscript>

<cfinclude template="opportunityFunctions.cfm">
<cfinclude template="OpportunityListScreenFunctions.cfm">

<cfparam name="IllustrativePriceColumns" default="">
<cfif structkeyexists(request,"UseIllustrativePrice")>
	<cfset IllustrativePriceColumns = "CALCULATED_BUDGET">
</cfif>

<CF_tableFromQueryObject
	queryObject="#myopportunity.qlistOpportunitiesInternal#"
	sortOrder = "#sortOrder#"
	numRowsPerPage="#numRowsPerPage#"

	keyColumnList="#keyColumnList#"
	keyColumnURLList="#keyColumnURLList#"
	keyColumnKeyList="#keyColumnKeyList#"
	keyColumnOnClickList="#keyColumnOnClickList#"

	hideTheseColumns="OVERALL_CUSTOMER_BUDGET,EXPECTED_CLOSE_DATE,LAST_UPDATED,oppPricingStatusID,SPRequiredApprovalLevel,SPCurrentApprovalLevel,PROBABILITY"
	showTheseColumns="#SPQleadAndOppListShowColumns#"
	dateFormat="#dateFormat#"
	columnTranslation="true"


	FilterSelectFieldList="#SPQleadAndOppListFilterSelectColumnList#"
	FilterSelectFieldList2="#SPQleadAndOppListFilterSelectColumnList#"

	<!--- IllustrativePriceFromCurrency="USD"
	IllustrativePriceToCurrency="EUR"
	IllustrativePriceDateColumn="LAST_UPDATED"
	IllustrativePriceColumns="#IllustrativePriceColumns#" --->

	radioFilterLabel="#radioFilterLabel#"
	radioFilterDefault="#radioFilterDefault#"
	radioFilterName="#radioFilterName#"
	radioFilterValues="#radioFilterValues#"

	checkBoxFilterName="#checkBoxFilterName#"
	checkBoxFilterLabel="#checkBoxFilterLabel#"

	allowColumnSorting="yes"
	currencyFormat="#application.com.settings.getSetting('leadManager.leadAndOppListSPQ.currencyFormatColumnList')#"
	<!--- PriceFromCurrencyColumn="currency" --->
	rollUpCurrency="true"

	startRow="#startRow#"

	searchColumnList="#SPQsearchColumnList#"
	searchColumnDataTypeList="1,0,0"
	searchColumnDisplayList="#SPQsearchColumnDisplayList#"

	rowIdentityColumnName="opportunity_id"
	functionListQuery="#comTableFunction.qFunctionList#"

	alphabeticalIndexColumn="#alphabeticalIndexColumn#"

	useInclude="false"

	openAsExcel = "#openAsExcel#"
	excelFileName="#excelFileName#"
>