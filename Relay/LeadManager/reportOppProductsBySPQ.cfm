<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		reportOppProductsBySPQ.cfm
Author:
Date started:

Description:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
23-Oct-2008			NJH			Fix Issue 1244 - changed the vOppProductsBySPQ view, and replaced all references to unitPrice with specialPrice.
27-May-2010			PPB			added campaignID filter and removed it from vOppProductsBySPQ
30-Jul-2010			NJH			P-PAN002 - added rollUpCurrency for rollUpReporting
2011/06/01 			PPB 		REL106 use countryScopeOpportunityRecords setting
2012-10-04	WAB		Case 430963 Remove Excel Header

Possible enhancements:


 --->

<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">
<cfparam name="openAsExcel" type="boolean" default="false">

<cf_title>Lead|Opportunity List</cf_title>


<cfparam name="sortOrder" default="COUNTRY_DESCRIPTION">
<cfparam name="numRowsPerPage" default="1000">
<cfparam name="startRow" default="1">
<!--- The keyColumn list group of params below control which columns show URL links
		where they link to and what key should be used in the URL --->
<cfparam name="keyColumnList" type="string" default="PROJECT"><!--- this can contain a list of columns that can be edited --->
<cfparam name="keyColumnURLList" type="string" default="opportunityEdit.cfm?opportunityid="><!--- this can contain a list of matching columns that contain the URL of the editor template --->
<cfparam name="keyColumnKeyList" type="string" default="OPPORTUNITYID"><!--- this can contain a list of matching key columns e.g. primary key --->

<cfparam name="radioFilterLabel" type="string" default=""><!--- this can contain a list of columns that can be edited --->
<cfparam name="radioFilterDefault" type="string" default=""><!--- this can contain a list of matching columns that contain the URL of the editor template --->
<cfparam name="radioFilterName" type="string" default=""><!--- this can contain a list of matching key columns e.g. primary key --->
<cfparam name="radioFilterValues" type="string" default=""><!--- this should contain a list of values for each radio button delimited by a minus sign --->

<cfparam name="dateFormat" type="string" default="FORECAST_SHIP_DATE"><!--- this list should contain those fields you want in the date format dd-mmm-yy --->
<cfparam name="entityID" type="numeric" default="0">
<!--- <cfparam name="frm_vendoraccountmanagerpersonid" type="numeric" default="0"> --->

<!--- <cfparam name="checkBoxFilterName" type="string" default="showSPComplete">
<cfparam name="checkBoxFilterLabel" type="string" default="Phr_Sys_Opp_ShowSPQComplete"> --->

<!--- <cfparam name="alphabeticalIndexColumns" type="string" default="SKU"> --->

<!--- 2012-07-23 PPB P-SMA001 commented out
<cfif application.com.settings.getSetting("leadManager.countryScopeOpportunityRecords")>	<!--- 2011/06/01 PPB REL106 use countryScopeOpportunityRecords setting --->
	<cfinclude template="/templates/qryGetCountries.cfm">
</cfif>
--->

<cfparam name="SPQReport" type="string" default="yes">

<cfparam name="SPQleadAndOppListFilterSelectColumnListSPQ" type="string" default="SKU,COUNTRY_DESCRIPTION">
<cfparam name="SPQleadAndOppListShowColumnsSPQ" type="string" default="SKU,COUNTRY_DESCRIPTION,LIST_PRICE,SPQ_PRICE,TOTAL_SPQ_PRICE,QUANTITY,DISCOUNT_PERCENTAGE,PROJECT,ACCOUNT_MANAGER,FORECAST_SHIP_DATE,SPQ_APPROVED_ID">
<!--- <cfparam name="SPQsearchColumnList" type="string" default="opportunity_id,account,product">
<cfparam name="SPQsearchColumnDisplayList" type="string" default="Phr_opportunityID,Account,Product"> --->


<!--- 2012-07-23 PPB P-SMA001 commented out
<CFINCLUDE TEMPLATE="../templates/qrygetcountries.cfm">
--->

<!--- NJH 2008/10/23 ATI Bug Fix Issue 1244 - SpecialPrice AS SPQ_PRICE was unitPrice AS SPQ_PRICE --->
<CFQUERY NAME="GetSPQProductData" datasource="#application.SiteDataSource#">
	SELECT
		SKU, COUNTRY_DESCRIPTION, ListPrice AS LIST_PRICE,
		SpecialPrice AS SPQ_PRICE, TOTAL_SPQ_PRICE, AverageDiscount AS DISCOUNT_PERCENTAGE,SKUQuantity as QUANTITY,
		OPPORTUNITYID, PROJECT, ACCOUNT_MANAGER, FORECAST_SHIP_DATE, SPQ_APPROVED_ID,currency
	FROM vOppProductsBySPQ
	WHERE 1=1

	<!--- 2012-07-23 PPB P-SMA001 commented out
	<cfif application.com.settings.getSetting("leadManager.countryScopeOpportunityRecords")>
		and countryid  in ( <cf_queryparam value="#countryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
	</cfif>
	 --->
	#application.com.rights.getRightsFilterWhereClause(entityType="opportunityProduct",alias="vOppProductsBySPQ").whereClause#		<!--- 2012-07-23 PPB P-SMA001 note: countryScopeOpportunityRecords setting checked inside getRightsFilterWhereClause() --->

	and campaignID =  <cf_queryparam value="#application.com.settings.getSetting("leadManager.products.productCatalogueCampaignID")#" CFSQLTYPE="CF_SQL_INTEGER" >
	<cfinclude template="../templates/tableFromQuery-QueryInclude.cfm">
	ORDER BY <cf_queryObjectName value="#sortOrder#">
</CFQUERY>

<CF_tableFromQueryObject
	queryObject="#GetSPQProductData#"
	sortOrder = "#sortOrder#"
	openAsExcel = "#openAsExcel#"
	numRowsPerPage="#numRowsPerPage#"

	keyColumnList="#keyColumnList#"
	keyColumnURLList="#keyColumnURLList#"
	keyColumnKeyList="#keyColumnKeyList#"

	hideTheseColumns="OPPORTUNITYID"
	showTheseColumns="#SPQleadAndOppListShowColumnsSPQ#"
	dateFormat="#dateFormat#"
	numberFormat="QUANTITY,DISCOUNT_PERCENTAGE"
	columnTranslation="false"

	currencyFormat="SPQ_PRICE,LIST_PRICE,TOTAL_SPQ_PRICE"
	totalTheseColumns="DISCOUNT_PERCENTAGE,TOTAL_SPQ_PRICE,QUANTITY"
	averageTheseColumns="DISCOUNT_PERCENTAGE"
	GroupByColumns="COUNTRY_DESCRIPTION"
	FilterSelectFieldList="#SPQleadAndOppListFilterSelectColumnListSPQ#"
	FilterSelectFieldList2="#SPQleadAndOppListFilterSelectColumnListSPQ#"
<!--- 	FilterSelectFieldList2="#SPQleadAndOppListFilterSelectColumnListSPQ#" --->

	radioFilterLabel="#radioFilterLabel#"
	radioFilterDefault="#radioFilterDefault#"
	radioFilterName="#radioFilterName#"
	radioFilterValues="#radioFilterValues#"

	checkBoxFilterName=""
	checkBoxFilterLabel=""

	allowColumnSorting="yes"
	startRow="#startRow#"

	rollUpCurrency="true"

<!--- 	functionListQuery="" --->

	<!--- alphabeticalIndexColumn="#alphabeticalIndexColumns#" --->

	useInclude="false"
>

<cfif isdefined('FilterSelect') and filterselect eq "SKU">
	<CFQUERY NAME="GetSPQProductDataAnalysis" datasource="#application.SiteDataSource#">
		SELECT
			sku,
			listprice,
			min(specialPrice) as minimumSPQPrice, <!--- NJH 2008/10/23 ATI Bug Fix Issue 1244- was min(unitPrice) as minimumSPQPrice --->
			sum(total_spq_price)/sum(skuQuantity) as averageSPQPrice,
			max(specialPrice) as maximumSPQPrice <!--- NJH 2008/10/23 ATI Bug Fix Issue 1244 -was max(unitPrice) as maximumSPQPrice --->
		FROM
			vOppProductsBySPQ
		WHERE 1=1
		<cfinclude template="../templates/tableFromQuery-QueryInclude.cfm">
		group by sku,listprice
	</CFQUERY>
	<cfoutput query="GetSPQProductDataAnalysis">
	<p>
	<table border="0" cellspacing="1" cellpadding="3" class="withBorder">
			<tr>
			<th>phr_SKU</th>
			<th>phr_LIST_PRICE</th>
			<th>phr_MINIMUM_SPQ_PRICE</th>
			<th>phr_AVERAGE_SPQ_PRICE</th>
			<th>phr_MAXIMUM_SPQ_PRICE</th>
		</tr>
		<tr class="oddrow">
			<td>#htmleditformat(sku)#</td>
			<td>#htmleditformat(LSCurrencyFormat(listprice))#</td>
			<td>#htmleditformat(LSCurrencyFormat(minimumSPQPrice))#</td>
			<td>#htmleditformat(LSCurrencyFormat(averageSPQPrice))#</td>
			<td>#htmleditformat(LSCurrencyFormat(maximumSPQPrice))#</td>
		</tr>
	</table>
	</cfoutput>
</cfif>