<!--- �Relayware. All Rights Reserved 2014 --->

<!---
2014-06-10 REH Case 440415 added headings so it will read Contact Name not Opportunity Name
--->

	<cfset request.document.addHTMLDocumentTags = false>
	<cfif not application.com.security.confirmFieldsHaveBeenEncrypted(fieldNames="orgid")>
	      <CF_ABORT>
	</cfif>

	<cfparam name="startRow" default="1">
	<cfparam name="numRowsPerPage" default="100">
	<cfparam name="sortOrder" default="FirstName,LastName">

	<cf_translate>

		<cfset qryContacts = application.com.opportunity.getOpportunityEndCustomerContacts(organisationID=#session.customerOrgID#,sortOrder=#sortOrder#)>

		<cfif isdefined("url.debug")>
			<cfoutput><p>qryContacts<br/><cfdump var="#qryContacts#"></p></cfoutput>
		</cfif>

		<cfset urlString = "#application.com.security.encryptQueryString('mode=PortalAdd&frmOrganisationID=#session.customerOrgID#&personID=0')#">
		<div align="right">
			<CF_INPUT type="button" onClick="javascript:editContact('#urlString#');" value="phr_addNew"/>
		</div>


		<cfoutput>
		<script>
			function editContact (params) {
				jQuery.fancybox.open({
					"href": "/templates/EditEndCustomer.cfm?" + params,
					"type": "iframe",
					"helpers": {
				    	"overlay": {
							"locked": true
						}
					},
					"scrollOutside": true});
			}
		</script>
		</cfoutput>


		<cfif qryContacts.recordCount gt 0>
			<CF_tableFromQueryObject
				queryObject="#qryContacts#"
				sortOrder = "#sortOrder#"
				startRow = "#startRow#"
				openAsExcel = "false"
				numRowsPerPage="#numRowsPerPage#"
				totalTheseColumns=""
				GroupByColumns=""
				ColumnTranslation="true"
			    ColumnTranslationPrefix="phr_opp_"
				showTheseColumns="Name,JobDesc,Email,PhoneNumber,EditButton"
				ColumnHeadingList="ContactName,JobDesc,Email,PhoneNumber,EditButton"
				FilterSelectFieldList=""
				FilterSelectFieldList2=""
				hideTheseColumns=""
				numberFormat=""
				currencyformat=""
				dateFormat=""
				showCellColumnHeadings="no"
				keyColumnURLList=" "
				keyColumnOnClickList="editContact('##application.com.security.encryptQueryString('mode=PortalEdit&frmOrganisationID=##OrganisationID##&personID=##personID##')##')"
				keyColumnKeyList=""
				keyColumnTargetList=""
				keyColumnList="EditButton"
				keyColumnOpenInWindowList="no"
				OpenWinSettingsList=""
				useInclude = "false"
				allowColumnSorting = "true"
				KeyColumnLinkDisplayList="button"
			>
		<cfelse>
			phr_opp_NoContactsToList
		</cfif>

	</cf_translate>

<!---
						 javascript:openWin('/templates/EditEndCustomer.cfm?mode=PortalEdit&frmOrganisationID=##OrganisationID##&personID=##personID##'*comma'winname'*comma'toolbar=no*commawidth=480*commaheight=260*commalocation=no')"
 --->
<!---
				keyColumnOnClickList="javascript:window.open('/templates/EditEndCustomer.cfm?mode=PortalEdit&frmOrganisationID=##OrganisationID##&personID=##personID##','winname','toolbar=no,width=480,height=260,location=no')"
 --->

<script type="text/javascript">
	function refreshPage(){
		jQuery('#Contacts').load("/templates/customerContacts.cfm?inIFrame=1");
	}
</script>