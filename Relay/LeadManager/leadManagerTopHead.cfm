<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			leadManagerTopHead.cfm	
Author:				SWJ
Date started:		2001-01-25
	
Description:		This 

Usage:		 
<cf_leadManagertopHead
	pageTitle="edit sctren"
	templateType = "edit"
	thisDir="leadManager">


Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2005-05-10			AJC			Add in dropdown if opptype is 2 for sumbitting quotes/saving
2007-11-02			SSS			Switch of add new lead for opportunityedit for people who are not internal
2008-07-07			NYF			tweaked thisDir to have a default and include in all calls to /leadManager pages
2008-07-28			NYF			Bug 625 - Add New Lead wasn't being displayed on the internal app as the entityid wasn't defined
2008/12/19			NJH			LID 1518 Added attributes.querystring to ensure it is passed in and used by 'openasExcel=true'
2011/05/26 			PPB 		REL106 added AddLeadEID as a setting
2011-11-24			NYB			LHID8224 replaced Save & Back with translatable phrases
2012-03-13 			PPB 		AVD001 add attributes to hide various menu options for opp V2 until they are needed
2012-11-01			IH			Case 431656 set entityid = frmScreenEntityID if frmEntityID and frmcurrententityid is not passed
Possible enhancements:

 --->

<cfparam name="current" type="string" default="index.cfm">
<cfparam name="attributes.templateType" type="string" default="list">
<cfparam name="attributes.pageTitle" type="string" default="">
<cfparam name="attributes.pagesBack" type="string" default="1">
<cfparam name="attributes.thisDir" type="string" default="/leadManager">
<cfparam name="attributes.queryString" type="string" default="">
<!--- START: 2012-03-13 PPB AVD001 having included this into opportunityEditV2 (for Related Files) several links didn't work so these are hidden using the attributes below until required --->
<cfparam name="attributes.showBack" type="string" default="true">
<cfparam name="attributes.showPrint" type="string" default="true">
<cfparam name="attributes.showActions" type="string" default="true">
<cfparam name="attributes.showNotes" type="string" default="true">
<cfparam name="attributes.showSave" type="string" default="true">
<!--- END: 2012-03-13 PPB AVD001 --->

<!--- 2008-07-28 NYF Bug 625 ->  --->
<cfif (isdefined("frmentityid") or isdefined("frmcurrententityid") or isDefined("frmScreenEntityID")) and not (isdefined("entityid")) and isdefined("frmentitytypeid") and frmentitytypeid eq 2>
	<cfif isdefined("frmentityid")>
		<cfset entityid = frmentityid>
	<cfelseif isDefined("frmcurrententityid")>
		<cfset entityid = frmcurrententityid>
	<cfelseif isDefined("frmScreenEntityID")>
		<cfset entityid = frmScreenEntityID>
	</cfif>
</cfif>
<!--- <- Bug 625 --->

<!--- WAb 2010/10/20  into settings --->
<cfset addLeadOnInternal = application.com.settings.getSetting("leadManager.addLeadOnInternal")>
<cfif attributes.pageTitle is "" and isDefined("pageTitle") and pageTitle is not "">
	<cfset attributes.pageTitle = pageTitle>
</cfif>

<CF_RelayNavMenu pageTitle="#attributes.pageTitle#" thisDir="#attributes.thisDir#">
	<!--- <cfif attributes.showBack>		<!--- 2012-03-13 PPB AVD001 hidden for opp v2 for now --->
		<CF_RelayNavMenuItem MenuItemText="phr_sys_Back" CFTemplate="JavaScript:history.go(-#attributes.pagesBack#);">
	</cfif> --->

	<CFIF request.relayCurrentUser.isInternal and not listFind(cgi.script_name,"moveOppToDiffOrg.cfm","/") >  <!--- WAB 2006-01-25 Remove form.userType     <cfif CompareNoCase(usertype,'internal') eq 0>--->
		<cfif isDefined("entityID") and isNumeric(entityID)>    <!--- WAB 2008/05/14 removed attributes from attributes.entityid, everywhere else using plain entityid--->
			<CF_RelayNavMenuItem MenuItemText="List Opportunities" CFTemplate="#attributes.thisdir#/leadandOppList.cfm?entityID=#entityID#">
		<cfelse>
			<CF_RelayNavMenuItem MenuItemText="List All Opportunities" CFTemplate="#attributes.thisdir#/leadandOppList.cfm">
		</cfif>
		
		<CFIF findNoCase("allocateLeads.cfm",SCRIPT_NAME,1)>
			<CF_RelayNavMenuItem MenuItemText="Auto Allocate" CFTemplate="#attributes.thisdir#/allocateLeads.cfm?prid=autoAllocateLeads">
		</CFIF> 
		
		<cfif attributes.templateType neq "edit">
			<cfif isDefined("entityID") and isNumeric(entityID)>
				<CF_RelayNavMenuItem MenuItemText="Pipeline (Summary)" CFTemplate="#attributes.thisdir#/pipelineReport.cfm?entityID=#entityID#">
			<cfelse>
				<CF_RelayNavMenuItem MenuItemText="Pipeline (Summary)" CFTemplate="#attributes.thisdir#/pipelineReport.cfm">
			</cfif>
	
			<cfif isDefined("entityID") and isNumeric(entityID)>
				<CF_RelayNavMenuItem MenuItemText="Pipeline" CFTemplate="#attributes.thisdir#/pipelineReportDetailed.cfm?entityID=#entityID#">
			<cfelse>
				<cfif application.com.settings.getSetting("leadManager.products.oppProductsEditor") eq "none">
					<CF_RelayNavMenuItem MenuItemText="Pipeline" CFTemplate="#attributes.thisdir#/reportOppPipelineNoProduct.cfm">
				<cfelse>
					<CF_RelayNavMenuItem MenuItemText="Pipeline" CFTemplate="#attributes.thisdir#/reportOppPipelineByProduct.cfm">
				</cfif>
				
			</cfif>
			<!--- NJH 2008/06/05 Bug Fix - Lexmark Lead & Opp Pilot 4 : changed it from request.query_string to queryString--->
			<!--- NYF 2008/06/16 Bug Fix - NABU Bug 451 : changed it back to request.query_string from queryString because it wasn't working --->
			<!--- GCC 2008/06/20 They think its all working - it is now by using queryString AFTER open as excel - check for defined for safety --->
			<!--- WAB 2008/12/02 added a & between openAsExcel and querystring 
			GCC  2008/12/19 Why? What was this for? Where is it in release notes?--->
			<cfif attributes.queryString neq "">
				<CF_RelayNavMenuItem MenuItemText="Excel" CFTemplate="#cgi["SCRIPT_NAME"]#?openAsExcel=true&#attributes.querystring#">
			<!--- backwards compatible for where this is not being called correctly as a customTag which is a lot of places in lead manager! --->
			<cfelseif isdefined("queryString")>
				<CF_RelayNavMenuItem MenuItemText="Excel" CFTemplate="#cgi["SCRIPT_NAME"]#?openAsExcel=true&#querystring#">
			<cfelse>
				<CF_RelayNavMenuItem MenuItemText="Excel" CFTemplate="#cgi["SCRIPT_NAME"]#?openAsExcel=true&#request.query_string#">
			</cfif>			
		</cfif>
		
		<cfif attributes.showPrint>			<!--- 2012-03-13 PPB AVD001 hidden for opp v2 for now --->
			<CF_RelayNavMenuItem MenuItemText="Print" CFTemplate="javascript: print()">
		</cfif>

		<cfif attributes.templateType eq "edit">
			<cfif isDefined("opportunityID") and opportunityID neq 0>
				<CF_RelayNavMenuItem MenuItemText="Phr_Sys_Files" CFTemplate="/data/entityFiles.cfm?frmEntityType=opportunity&frmEntityID=#opportunityID#">
				<cfif attributes.showActions>		<!--- 2012-03-13 PPB AVD001 hidden for opp v2 for now --->	
					<CFIF isDefined("attributes.hasActions") and attributes.hasActions eq 0>
						<CF_RelayNavMenuItem MenuItemText="Phr_Sys_AddActionFor" CFTemplate="JavaScript:addActionRecord('opportunity',#caller.opportunityID#);">
		<!--- <A HREF="javaScript:addActionRecord('#attributes.entityType#',#attributes.entityID#)" TITLE="Phr_Sys_AddActionFor #attributes.helpText#" CLASS="#attributes.class#"><IMG SRC="/IMAGES/MISC/IconFlagGrey.gif" BORDER="0"></a><BR> --->
					<cfelse>
						<CF_RelayNavMenuItem MenuItemText="Phr_Sys_ShowActionsFor" CFTemplate="JavaScript:showActionRecord('opportunity',#caller.opportunityID#);">
		<!--- <A href="javaScript:showActionRecord('#attributes.entityType#',#attributes.entityID#)" TITLE="Phr_Sys_ShowActionsFor #attributes.helpText#" CLASS="#attributes.class#"><IMG SRC="/IMAGES/MISC/IconFlag.gif" BORDER="0"></a> --->
					</CFIF>
				</cfif>

				<cfif attributes.showNotes>		<!--- 2012-03-13 PPB AVD001 hidden for opp v2 for now --->
					<CF_RelayNavMenuItem MenuItemText="View Notes(#caller.qryNoteCount#)" CFTemplate="JavaScript:addNote(#caller.opportunityID#);">
				</cfif>
			</cfif>
		</cfif>
	</cfif>
	
	<cfset additionalParams = ""> <!--- NJH 2008/07/18 Bug Fix T-10 Issue 755 --->
		<!--- NJH 2008/07/18 Bug Fix T-10 Issue 755 append oppTypeID to the opportunityEdit set of parameters if it's defined --->
		<cfif isDefined("frmOppTypeID")>
			<cfset additionalParams = "&oppTypeID=#frmOppTypeID#">
		</cfif>
	
	
	<!--- NJH 2011/07/26 set the entityID based on frmEntityID and frmEntityTypeID if they are defined... these are set in screens and allows us to add a lead for an organisation when viewing a person --->
	<cfif isDefined("frmEntityID") and isDefined("frmEntityTypeID") and listFind("0,1,2",frmEntityTypeID) and not isDefined("entityID")>
		<cfquery name="getOrganisationID" datasource="#application.siteDataSource#">
			select organisationID from #application.entitytype[frmEntityTypeID].tablename# where #application.entitytype[frmEntityTypeID].uniqueKey# =  <cf_queryparam value="#frmEntityID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfquery>
		<cfset entityID = getOrganisationID.organisationID>
	</cfif>
	
	<!--- SKP 2010/10/20 LH4489 extra open bracket removed --->
	<!--- NJH 2008/12/15 CR-TNDNAB529 added check for request .Add LeadOnInternal --->
	<cfif isDefined("entityID") and isNumeric(entityID) and entityID neq 0 and request.relaycurrentuser.isInternal and addLeadOnInternal> <!--- WAB 2006-01-25 Remove form.userType     CompareNoCase(usertype,'internal') eq 0--->
		<CF_RelayNavMenuItem MenuItemText="Add New Opportunity" CFTemplate="#attributes.thisdir#/opportunityEdit.cfm?entityID=#entityID##additionalParams#">
	<cfelseif isDefined("attributes.entityID") and isNumeric(attributes.entityID) and not request.relaycurrentuser.isInternal and application.com.settings.getSetting("leadManager.addLeadOnExternal") >  <!--- NJH 2007/11/09 added 'and not isInternal' so that's it not opened in relayware--->
		<!--- 2011/05/26 PPB REL106 added AddLeadEID as a setting --->		
		<cfset AddLeadEID = application.com.settings.getSetting("leadManager.AddLeadEID")>
		<cfif AddLeadEID eq "">
			<cfset AddLeadEID = request.currentElement.ID>
		</cfif>
<!--- 		
		<cfif structkeyexists(request,"AddLeadEID")>
			<cfset AddLeadEID = request.AddLeadEID>
		<cfelse>
			<cfset AddLeadEID = request.currentElement.ID>
		</cfif>
 --->		
		<CF_RelayNavMenuItem MenuItemText="Add New Opportunity" CFTemplate="#cgi["SCRIPT_NAME"]#?eid=#AddLeadEID#&entityID=#attributes.entityID#">  <!--- NJH 2007/11/09 was opportunityEdit.cfm?entityID=#entityID#. This opened oppEdit.cfm outside of the portal --->
	<cfelseif request.relayCurrentUser.isInternal eq "true" and addLeadOnInternal>
		<!--- ==============================================================================
		SWJ  25-06-2008  StartOpp should only show if the user is internal, they are not in the 3 pane view and addLeadOnInternal is true
		=============================================================================== --->
		<!--- NJH 2009/02/25 Bug Fix All Sites Issue 1891 - startOpp.cfm was originally driven off addLeadOnInternal. This is now being used above as well
			and we now look for another variable as this method of starting opportunities is for opportunities through distributors, where nothing
			about the reseller or the end customer is known. Simon has suggested that this switch may be based around the oppTypeID, but the rules are not clear at this point,
			so I think this will suffice for now. Sony are currently the only ones using startOpp.cfm. It may need a coat of looking at some point in the future if 
			this doesn't meet the requirements for any business rules that may have been set.
		 --->
		<cfif isDefined("request.startOppForDisti") and request.startOppForDisti>
			<CF_RelayNavMenuItem MenuItemText="Add New Opportunity" CFTemplate="/leadManager/startOpp.cfm">
		</cfif>
	</cfif>
	
	<CFIF findNoCase("leadEdit.cfm",SCRIPT_NAME,1) >
		<CF_RelayNavMenuItem MenuItemText="Profile List" CFTemplate="/profileManager/profileList.cfm">
	</CFIF> 
	
	<cfif  attributes.showSave>		<!--- 2012-03-13 PPB AVD001 hidden for opp v2 for now --->	
		<CFIF attributes.templateType eq "edit" >
		<CF_RelayNavMenuItem MenuItemText="phr_sys_Save" CFTemplate="javascript:eventFormOnSubmit();">
	</CFIF> 
	</cfif>
</CF_RelayNavMenu>
