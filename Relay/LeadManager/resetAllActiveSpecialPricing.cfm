<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
2010-01-29			NJH			P-ATI007 - changed checkApproverLevelRequired call so that we could take advantage of over-riding the function
 --->

<cfset antiCache = RandRange(1,1000000)>
<cfsetting enablecfoutputonly="no" requesttimeout="6000">





<cfif not structkeyexists(session,"opportunityIDlastDone")>
	<cfset session.opportunityIDlastDone = 99999999999999999999>
</cfif>

<cfquery name="getOppsForReset" datasource="#application.sitedatasource#">
select top 1 opportunityID as entityID 
from opportunity 
WHERE     (stageID IN (1, 2, 3, 4)) 
AND (SPRequiredApprovalLevel IS NOT NULL) 
AND (SPRequiredApprovalLevel > SPCurrentApprovalLevel)
and opportunityID <  <cf_queryparam value="#session.opportunityIDlastDone#" CFSQLTYPE="CF_SQL_INTEGER" > 
order by opportunityID desc
</cfquery>

<cfif getOppsForReset.recordcount eq 0>

FINISHED

<cfelse>

<cfset entityID = getOppsForReset.entityID>
<cfset session.opportunityIDlastDone = entityID>

<cfquery name="initialiseOppsForReset" datasource="#application.sitedatasource#">
	update opportunity
		set SPApproverPersonIDs = NULL
	where opportunityid =  <cf_queryparam value="#entityid#" CFSQLTYPE="CF_SQL_INTEGER" > 
</cfquery>

	<cfinvoke component="relay.com.oppSpecialPricing" method="getCountryApprovers" returnvariable="SPApprovers">
		<cfinvokeargument name="dataSource" value="#application.siteDataSource#"/>
		<cfinvokeargument name="opportunityID" value="#entityID#"/>
	</cfinvoke>
	
	<cfquery name="getOppDetails" datasource="#application.siteDataSource#">
		select * from vLeadListingCalculatedBudget where opportunity_ID =  <cf_queryparam value="#entityID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</cfquery>
	
	<cfparam name="SPapproverLevel" default="0">
	<cfinvoke component="relay.com.oppSpecialPricing" method="checkApprover" returnvariable="SPApproverLevel">
		<cfinvokeargument name="dataSource" value="#application.siteDataSource#"/>
		<cfinvokeargument name="personID" value="#request.relayCurrentUser.personid#"/>
		<cfinvokeargument name="countryID" value="#getOppDetails.countryID#"/>
		<cfinvokeargument name="opportunityID" value="#entityID#"/>
	</cfinvoke>
	
	<!--- NJH 2010-01-29 P-ATI007 - changed call to that of below so that I could take advantage of over-riding functions. --->
	<!--- <cfinvoke component="relay.com.oppSpecialPricing" method="checkApproverLevelRequired" returnvariable="SPApproverLevelRequired">
		<cfinvokeargument name="dataSource" value="#application.siteDataSource#"/>
		<cfinvokeargument name="opportunityID" value="#entityID#"/>
	</cfinvoke> --->
	
	<cfset SPApproverLevelRequired = application.com.oppSpecialPricing.checkApproverLevelRequired(dataSource=application.siteDataSource,opportunityID=entityID)>
	
	<cfif listLen(SPApproverLevel) gt 1>
		<!--- if this person holds more than one approver levels you need to decide which to use --->
		<cfif listfind(SPapproverLevel, listfirst(SPApproverLevelRequired,"|")) gt 0>
			<!--- first try to use the SPapproverLevelRequired --->
			<cfset thisApproverLevel = listfind(SPapproverLevel, listfirst(SPApproverLevelRequired,"|"))>
		<cfelse>
			<!--- otherwise use the first in the list --->
			<cfset thisApproverLevel = listFirst(SPApproverLevel)>
		</cfif>
	<cfelse>
		<cfset thisApproverLevel = SPApproverLevel>
	</cfif>
	
	<SCRIPT type="text/javascript">
	<!--
	function saveSpecialPricing(action)
	{
		//alert(action);
	  document.frmReasonForm.specialPricingStatusMethod.value = action;
	  var doThis = "yes";
	  //alert(document.frmReasonForm.specialPricingStatusMethod.value);
	  if (action == "SP_AddComment" && document.frmReasonForm.frmComments.value == "")
	  	{
		alert ("Please add in your comments");
		doThis = "no";
		}
	// START ALEX
	  if (action == "SP_InfoResponse" && document.frmReasonForm.frmComments.value == "")
	  	{
		alert ("Please add in your further information");
		doThis = "no";
		}
	// END ALEX
	  if (action == "SP_Calculate")
	  	{
		document.frmReasonForm.frmComments.value = "Budget changed to " + document.frmReasonForm.newBudget.value
		}
	  if (action == "SP_Request" && document.frmReasonForm.frmComments.value == "")
	  	{
		alert ("You must provide a reson for the special pricing");
		doThis = "no";
		}
	  if (action == "SP_FullyApprove" && document.frmReasonForm.frmComments.value == "")
	  	{
		alert ("Please add in your comments");
		doThis = "no";
		}
	  if (action == "SP_MoreInfo" && document.frmReasonForm.frmComments.value == "")
	  	{
		alert ("Please describe the information you are looking for in the comments field.");
		doThis = "no";
		}
	  if (action == "SP_Reject" && document.frmReasonForm.frmComments.value == "")
	  	{
		alert ("Please describe why you are rejecting this request in the comments field.");
		doThis = "no";
		}
	  if (action == "SP_Complete")
	    {
		var messageStr = "Are you sure? \n"
	        messageStr += "If you click yes you will not be able to change the \n"
	        messageStr += "Shipped Quantity again for any line item.\n\n" ; 
		var proceed = confirm(messageStr); 
		if (proceed == false)
			doThis = "no" 
		}
	  if (doThis == "yes" )
	  	document.frmReasonForm.submit();
	}
	
	function CheckIfFrmCommentsEmpty(){
		if (document.frmReasonForm.frmComments.value == "")
			document.frmReasonForm.specialPriceRequest.disabled=true
		else
			document.frmReasonForm.specialPriceRequest.disabled=false
	}
	
	function updateProductsShipped(){
		document.frmReasonForm.processProductsShipped.value = "y";
		document.frmReasonForm.submit();
	}
	//-->
	</script>
	<cfoutput>
	<form action="specialPriceEdit.cfm?entityid=#entityid#" method="post" name="frmReasonForm" id="frmReasonForm">
		<input type="hidden" name="specialPricingStatusMethod" value="">
		<CF_INPUT type="hidden" name="preservedOppPricingStatus" value="#getOppDetails.Pricing_Status#">
		<CF_INPUT type="hidden" name="entityID" value="#entityID#">
		<input type="hidden" name="processProductsShipped" value="">
		<CF_INPUT type="hidden" name="SPApproverLevel" value="#thisApproverLevel#">
		<CF_INPUT type="hidden" name="SPApproverLevelRequired" value="#listFirst(SPApproverLevelRequired,"|")#">
		<input type="hidden" name="pagesBack" value="2">
		<input type="hidden" name="autoReset" value="yes">
	<input type="hidden" name="frmComments" value="System approval level threshold recalculation">
	</cfoutput>
	</form>
	<script>
		saveSpecialPricing('SP_Request');
	</script>
</cfif>



