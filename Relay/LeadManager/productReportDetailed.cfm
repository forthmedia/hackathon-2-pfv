<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
FILE:  
relay\LeadManager\productReportDetailed.cfm

AMENDMENT HISTORY:  
2009-07-24			NYB			ATI LHID 2470 - added a timeout of 30 minutes if opened in Excel, and NOLOCK on query
2009-08-25	NYB 	P-ATI002 - restrict results to a year ago
2009-11-03	NJH		P-ATI002 - added Special_Product column
2010-07-30	NJH		P-PAN002 - changed to use rollUp reporting
2011-06-01 	PPB 	REL106 use countryScopeOpportunityRecords setting 
2012-10-04	WAB		Case 430963 Remove Excel Header 

--->

<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">
<cfparam name="openAsExcel" type="boolean" default="false">

<!--- START:  NYB 2009-07-24 ATI LHID 2470 - added a timeout of 30 minutes --->
<cfsetting requestTimeOut = "1800">
<!--- END:  2009-07-24 ATI LHID 2470 --->

	<cf_head>
		<cf_title>Pipeline Report</cf_title>
	</cf_head>
	
	
	<cfinclude template="LeadManagerTopHead.cfm"> 

<!--- 2012-07-23 PPB P-SMA001 commented out
<cfif application.com.settings.getSetting("leadManager.countryScopeOpportunityRecords")>	<!--- 2011-06-01 PPB REL106 use countryScopeOpportunityRecords setting --->
	<cfinclude template="/templates/qryGetCountries.cfm">
</cfif>
--->

<cfparam name="startrow" default="1">
<cfparam name="sortOrder" default="OPPORTUNITY_STATUS">
<cfparam name="numRowsPerPage" default="50">
<!--- The keyColumn list group of params below control which columns show URL links
		where they link to and what key should be used in the URL --->
<cfparam name="keyColumnList" type="string" default="detail,account"><!--- this can contain a list of columns that can be edited --->
<cfparam name="keyColumnURLList" type="string" default="opportunityEdit.cfm?opportunityid=,../data/dataframe.cfm?frmsrchOrgID="><!--- this can contain a list of matching columns that contain the URL of the editor template --->
<cfparam name="keyColumnKeyList" type="string" default="opportunity_ID,entityID"><!--- this can contain a list of matching key columns e.g. primary key --->

<cfparam name="radioFilterLabel" type="string" default=""><!--- this can contain a list of columns that can be edited --->
<cfparam name="radioFilterDefault" type="string" default=""><!--- this can contain a list of matching columns that contain the URL of the editor template --->
<cfparam name="radioFilterName" type="string" default=""><!--- this can contain a list of matching key columns e.g. primary key --->
<cfparam name="radioFilterValues" type="string" default=""><!--- this should contain a list of values for each radio button delimited by a minus sign --->

<cfparam name="dateFormat" type="string" default=""><!--- this list should contain those fields you want in the date format dd-mmm-yy --->
<cfparam name="entityID" type="numeric" default="0"> 
<cfparam name="frm_vendoraccountmanagerpersonid" type="numeric" default="0">

<cfparam name="productFilterSelectFieldList" type="string" default="SKU,Country,Account_Manager,Month_Due,Product_Group,Special_Product">

<cfparam name="checkBoxFilterName" type="string" default="">
<cfparam name="checkBoxFilterLabel" type="string" default="Phr_Sys_Opp_ShowComplete">
<cfparam name="form.showcomplete" default="0">

<cfparam name="alphabeticalIndexColumn" type="string" default="Account">

<cfscript>
// create a structure containing the fields below which we want to be reported when the form is filtered
passThruVars = StructNew();
StructInsert(passthruVars, "showComplete", form.showComplete);
</cfscript>

<!--- <cfinclude template="shipDateQueryWhereClause.cfm">  NJH 2007-05-29 --->

<!--- START: NYB 2009-08-25 P-ATI002 - added: --->
<cfset usePartialRange="true">
<cfset PartialRangeStartDate = DateAdd("yyyy", -1, now())>
<cfset tableName="vOppProductReportDetailed">
<cfset dateField="FORECAST_SHIP_DATE">
<!--- END: NYB 2009-08-25 P-ATI002 --->

<cfset phrasePrefix="ship">
<cfinclude template="/relay/templates/DateQueryWhereClause.cfm">

<!--- <cfparam name="form.FRMWHERECLAUSEC" default="#left(monthAsString(month(now())),3)#-#year(now())#">
<cfparam name="form.FRMWHERECLAUSED" default="#left(monthAsString(month(dateadd("m",6,now()))),3)#-#year(dateadd("m",6,now()))#"> --->

<!--- NJH 2009-11-03 P-ATI002 - added special product column and added a select around the whole query --->
<cfquery name="getProductReport" datasource="#application.SiteDataSource#">
select * from (
select SKU, QUANTITY, OPP_VALUE, ACCOUNT, DETAIL, ENTITYID, ACCOUNT_MANAGER, COUNTRY, Product_Group,
OPPORTUNITY_STATUS, PRODUCT_PROBABILITY, FORECAST_SHIP_DATE, OPPORTUNITY_ID, Month_due,Countryid,
LEFT( UPPER( #alphabeticalIndexColumn# ), 1 ) as alphabeticalIndex, LAST_UPDATED,
<cfif structKeyExists(request,"specialPricingProductORGroupIDsRequiringAdditionalApproval") and len(request.specialPricingProductORGroupIDsRequiringAdditionalApproval) gt 0 and structKeyExists(request,"ProductORGroup")>
	isNull((select 'Yes' from product where 
	<cfif request.ProductORGroup eq "G">
		productGroupId
	<cfelse>
		productId
	</cfif>
	 in (#request.specialPricingProductORGroupIDsRequiringAdditionalApproval#) and productID = v.productID),'No')
<cfelse>
	'No'
</cfif>
	as Special_Product
from vOppProductReportDetailed v WITH (NOLOCK) <!--- NYB 2009-07-24 ATI LHID 2470 - added NOLOCK --->
Where 1=1
<!--- 2012-07-23 PPB P-SMA001 commented out 
<cfif application.com.settings.getSetting("leadManager.countryScopeOpportunityRecords")>	
and countryid  in ( <cf_queryparam value="#countryList#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> )
</cfif>
 --->
#application.com.rights.getRightsFilterWhereClause(entityType="opportunity",alias="v").whereClause#		<!--- 2012-07-23 PPB P-SMA001 note: countryScopeOpportunityRecords setting checked inside getRightsFilterWhereClause() --->
	
<!--- temp comment out - vendorAccountManagerPersonID not defined in vOppProductReportDetailed
	<cfif isDefined("frm_vendoraccountmanagerpersonid") and frm_vendoraccountmanagerpersonid neq "0">
		and vendorAccountManagerPersonID = #frm_vendoraccountmanagerpersonid#
	</cfif> --->
	
	<!--- <cfif isDefined("FRMWHERECLAUSEA") and FRMWHERECLAUSEA neq ""> 
		AND rtrim(datename(q, forecast_Ship_Date)) = #right(listfirst(FRMWHERECLAUSEA,"-"),1)#
			and datepart(yy, forecast_Ship_Date) = #listLast(FRMWHERECLAUSEA,"-")#
	</cfif>
	
	<cfif isDefined("FRMWHERECLAUSEB") and FRMWHERECLAUSEB neq ""> 
		AND left(datename(m, forecast_Ship_Date),3) = '#listfirst(FRMWHERECLAUSEB,"-")#'
			and datepart(yy, forecast_Ship_Date) = #listLast(FRMWHERECLAUSEB,"-")#
	</cfif>
	
	<cfif isDefined("FRMWHERECLAUSEC") and FRMWHERECLAUSEC neq "" and isDefined("FRMWHERECLAUSED") and FRMWHERECLAUSED eq ""> 
		AND forecast_Ship_Date >= '01-#listfirst(FRMWHERECLAUSEC,"-")#-#listLast(FRMWHERECLAUSEC,"-")#'
	</cfif>
	
	<cfif isDefined("FRMWHERECLAUSED") and FRMWHERECLAUSED neq "" and isDefined("FRMWHERECLAUSEC") and FRMWHERECLAUSEC eq ""> 
		AND forecast_Ship_Date < dateadd(m,1,'01-#listfirst(FRMWHERECLAUSED,"-")#-#listLast(FRMWHERECLAUSED,"-")#')
	</cfif>
	
	<cfif isDefined("FRMWHERECLAUSEC") and FRMWHERECLAUSEC neq "" and isDefined("FRMWHERECLAUSED") and FRMWHERECLAUSED neq ""> 
		AND forecast_Ship_Date >= '01-#listfirst(FRMWHERECLAUSEC,"-")#-#listLast(FRMWHERECLAUSEC,"-")#'
		AND	forecast_Ship_Date < dateadd(m,1,'01-#listfirst(FRMWHERECLAUSED,"-")#-#listLast(FRMWHERECLAUSED,"-")#')
	</cfif>  --->
	
	<!--- 2004-07-30 SWJ moved this filter to the view so the view only shows 'live' data
	<cfif isDefined("showComplete") and showComplete neq 0>
			AND opp_StatusID in (5,9,10)
	<cfelseif isDefined("showComplete") and showComplete eq 0>
		and opp_StatusID in (1,2,3,4,7)
	</cfif> --->			
) base where 1=1					
	<cfinclude template="../templates/tableFromQuery-QueryInclude.cfm">
	order by <cf_queryObjectName value="#sortOrder#"> 
</cfquery>

<cfoutput>
<table width="100%" cellpadding="3">
	<tr>
		<td><strong>Product Pipeline Report</strong></td>
		<td align="right"></td>
	</tr>
	<tr><td colspan="2">
		</td></tr>
</table>
</cfoutput>

<cfinclude template="opportunityListScreenFunctions.cfm">

<cf_translate>

<cfparam name="IllustrativePriceColumns" default="">
<cfif structkeyexists(request,"UseIllustrativePrice")>
	<cfset IllustrativePriceColumns = "OPP_VALUE">
</cfif>

<!--- NJH 2009-11-03 P-ATI002 - added special_product to showTheseColumns --->
<CF_tableFromQueryObject 
	showCellColumnHeadings="yes"
	openAsExcel="#openAsExcel#"
	queryObject="#getProductReport#"
	queryName="getProductReport"
	sortOrder = "#sortOrder#"
	numRowsPerPage="#numRowsPerPage#"
	startrow="#startrow#"
	
	passThroughVariablesStructure="#passThruVars#"
	
	keyColumnList="#keyColumnList#"
	keyColumnURLList="#keyColumnURLList#"
	keyColumnKeyList="#keyColumnKeyList#"
	
	hideTheseColumns="opportunity_ID,ENTITYID,month_due,Countryid,Product_Group"
	showTheseColumns="SKU,QUANTITY,OPP_VALUE,ACCOUNT,DETAIL,ACCOUNT_MANAGER,COUNTRY,OPPORTUNITY_STATUS,PRODUCT_PROBABILITY,FORECAST_SHIP_DATE,SPECIAL_PRODUCT"
	dateFormat="FORECAST_SHIP_DATE"
	<!--- columnTranslation="#translateReportColumns#" --->
	
	FilterSelectFieldList="#productFilterSelectFieldList#"
	FilterSelectFieldList2="#productFilterSelectFieldList#"
	
	<!--- IllustrativePriceFromCurrency="USD" 
	IllustrativePriceToCurrency="EUR"
	IllustrativePriceDateColumn="LAST_UPDATED"
	IllustrativePriceColumns="#IllustrativePriceColumns#"--->
	
	GroupByColumns="OPPORTUNITY_STATUS"
	totalTheseColumns="OPP_Value,QUANTITY"
	
	radioFilterLabel="#radioFilterLabel#"
	radioFilterDefault="#radioFilterDefault#"
	radioFilterName="#radioFilterName#"
	radioFilterValues="#radioFilterValues#"
	checkBoxFilterName="#checkBoxFilterName#"
	checkBoxFilterLabel="#checkBoxFilterLabel#"
	allowColumnSorting="no"
	currencyFormat="opp_Value"
	numberFormat="QUANTITY"
	queryWhereClauseStructure="#queryWhereClause#"
	rollUpCurrency="true"
>
<!--- 
<CF_tableFromQueryObject 
	queryObject="#getProductReport#"
	queryName="getProductReport"
	sortOrder = "#sortOrder#"
	numRowsPerPage="#numRowsPerPage#"
	startrow="#startrow#"
	
	passThroughVariablesStructure="#passThruVars#"
	
	keyColumnList="#keyColumnList#"
	keyColumnURLList="#keyColumnURLList#"
	keyColumnKeyList="#keyColumnKeyList#"
	
	hideTheseColumns="opportunity_ID,ENTITYID,month_due,Countryid,Product_Group"
	showTheseColumns="SKU,QUANTITY,OPP_VALUE,ACCOUNT,DETAIL,ACCOUNT_MANAGER,COUNTRY,OPPORTUNITY_STATUS,PRODUCT_PROBABILITY,FORECAST_SHIP_DATE"
	dateFormat="FORECAST_SHIP_DATE"
	columnTranslation="#translateReportColumns#"
	
	FilterSelectFieldList="#productFilterSelectFieldList#"
	FilterSelectFieldList2="#productFilterSelectFieldList#"
	
	GroupByColumns="OPPORTUNITY_STATUS"
	totalTheseColumns="OPP_Value"

	radioFilterLabel="#radioFilterLabel#"
	radioFilterDefault="#radioFilterDefault#"
	radioFilterName="#radioFilterName#"
	radioFilterValues="#radioFilterValues#"
	checkBoxFilterName="#checkBoxFilterName#"
	checkBoxFilterLabel="#checkBoxFilterLabel#"
	allowColumnSorting="no"
	currencyFormat="opp_Value"
	queryWhereClauseStructure="#queryWhereClause#"
>
--->
</cf_translate>
<!--- alphabeticalIndexColumn="#alphabeticalIndexColumn#" --->
<!--- startRow="#iif( isdefined( "startRow" ), "startRow", 1 )#" --->



