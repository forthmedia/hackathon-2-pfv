<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			oppProductSetReviewSchedule.cfm	
Author:				AJC
Date started:		2005-06-10
	
Description:		Product Set Review Schedule functions for opportunities	

Orginal Code:		CR_ATI010

Amendment History:

Date (DD-MMM-YYYY)	Initials	Code	What was changed

Possible enhancements:
 --->
<cfparam name="quantity" default="1">
<cfparam name="productSetID" default="">
<cfparam name="opportunityID" default="">


	<cf_head>
		<cf_title>Review Schedule</cf_title>
	</cf_head>
	
	<cfif isDefined("bSubmit")>
		<table border="0" cellspacing="1" cellpadding="3" align="left" bgcolor="White" class="withBorder" width="300">
			<tr>
				<td>
					<cfif productSetID EQ "" OR opportunityID EQ "">
						Error:<br>
						Either you have not specified a productSetID or an opportunityID.
					<cfelse>
						<cfloop from="1" to="#quantity#" index="i">
							<cfif isDefined("txtforecastShipDate#i#")>
								<cfscript>
									application.com.oppProductSets.addProductSetWithDatesToOpportunity(opportunityID,productSetID,Evaluate("txtforecastShipDate#i#"));
								</cfscript>
							</cfif>
						</cfloop>
						<cfoutput>#htmleditformat(quantity)# x Product Set added to Opportunity.</cfoutput><br>
						<SCRIPT type="text/javascript">opener.location.reload(false);</script>
						<a href="javascript:window.close()">close</a>
					</cfif>
				</td>
			</tr>
		</table>
	<cfelse>
		<cfscript>
			// if the start shipping date is invalid or null, set to today's date
			if(txtForecastShipDate is "" or not isDate(txtForecastShipDate))
			{
				txtForecastShipDate=createodbcdate(now());
			}
		</cfscript>
		<cfoutput>
			<form name="frmProductSetShipmentDates" action="" method="post">
				<CF_INPUT type="hidden" name="productSetID" value="#listFirst(cboProductSetID,'~')#">
				<CF_INPUT type="hidden" name="opportunityID" value="#opportunityID#">
				<CF_INPUT type="hidden" name="quantity" value="#quantity#">
				<table border="0" cellspacing="1" cellpadding="3" align="left" bgcolor="White" class="withBorder" width="300">
					<tr>
						<th colspan="2">Product Set #listFirst(cboProductSetID,'~')#</th>
					</tr>
					<tr>
						<th colspan="2">Quantity to add = #htmleditformat(quantity)#</th>
					</tr>
					<cfif isDefined("txtFrequency") AND txtFrequency neq "">
					<tr>
						<th colspan="2">Frequency = #htmleditformat(txtFrequency)#</th>
					</tr>
					<cfelse>
						<cfset txtFrequency = "Daily">
					</cfif>
					<tr>
						<th colspan="2">First Shipment = #dateformat(txtforecastShipDate,"MMM YYYY")#</th>
					</tr>
					<cfloop index="i" from="1" to="#quantity#" step="1">
						<cfswitch expression="#txtFrequency#">
							<cfcase value="Daily">
								<cfif isDefined("direction") and direction eq "start">
									<cfset shipmentdate = DateAdd("d", i-1, txtforecastShipDate)>
								<cfelse>
									<cfset shipmentdate = DateAdd("d", -(quantity-i), txtforecastShipDate)>
								</cfif>
							</cfcase>
							<cfcase value="Weekly">
								<cfif isDefined("direction") and direction eq "start">
									<cfset shipmentdate = DateAdd("ww", i-1, txtforecastShipDate)>
								<cfelse>
									<cfset shipmentdate = DateAdd("ww", -(quantity-i), txtforecastShipDate)>
								</cfif>
							</cfcase>
							<cfcase value="Monthly">
								<cfif isDefined("direction") and direction eq "start">
									<cfset shipmentdate = DateAdd("m", i-1, txtforecastShipDate)>
								<cfelse>
									<cfset shipmentdate = DateAdd("m", -(quantity-i), txtforecastShipDate)>
								</cfif>
							</cfcase>
							<cfcase value="Yearly">
								<cfif isDefined("direction") and direction eq "start">
									<cfset shipmentdate = DateAdd("yyyy", i-1, txtforecastShipDate)>
								<cfelse>
									<cfset shipmentdate = DateAdd("yyyy", -(quantity-i), txtforecastShipDate)>
								</cfif>
							</cfcase>
							<cfdefaultcase>
								<cfset shipmentdate = DateAdd("d", 0, txtforecastShipDate)>
							</cfdefaultcase>
						</cfswitch>
						<tr>
							<td>Shipment #i#</td><td>
							<cf_relayDateField	
								thisFormName="frmProductSetShipmentDates"
								fieldName="txtforecastShipDate#i#"
								anchorName="anchor#i#"
								currentValue="#shipmentdate#"
								helpText="phr_dateFieldHelpText">
						</tr>
					</cfloop>
					<tr>
						<td colspan="2" align="right"><input type="submit" name="bSubmit" value="Save Schedule"></td>
					</tr>
				</table>
			</form>
		</cfoutput>
	</cfif>
	

