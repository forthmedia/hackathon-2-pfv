/**
 * AttributeManager
 * 
 * @author Richard.Tingle
 * @date 01/02/16
 **/
component accessors=true output=false persistent=false {


	public any function init(){
		this.editorBackingBean=new singleSignOn.common.editorBackingBean();
	}

	/**
	 * Filters out attributes which should never be available for iteractions with SSO providers
	 * (e.g. password fields)
	 **/
	private Array function sanitiseAttributes(required Array input){
		
		ArrayDelete(input,"Password"); //its the salted hashed password which shouldn't have any value, but we're still not going to give it out
		ArrayDelete(input,"PasswordDate");
		ArrayDelete(input,"active");
		return input;
	}

	public Array function getPotentialAttributesForEntity(required numeric entityTypeID) output="false" {
		
		var availableFields=structkeyarray(application.com.rights.getEntityFieldRights(entityTypeID=entityTypeID,returnColumns="field",haveRightsForMethod="view"));
		availableFields=this.editorBackingBean.eliminateProfilesWithoutTextIDs(availableFields);

		ArrayAppend(availableFields,structkeyarray(getAttributeExtrasStructure(entityTypeID)),true );
		arraySort(availableFields,"textnocase");
		return sanitiseAttributes(availableFields);
	}
	
	/**
	 * Typically relevant to autocreation. Returns the minimum set of data required for an entity of this type
	 **/
	public Array function getRequiredAttributesForEntity(required numeric entityTypeID) output="false" {
		
		if (entityTypeID eq application.entityTypeID.person ){
			//because person has quite a few false positives (where the CF provides these required fields for you) this is just a hard coded list of the real ones
			
			requiredAttributes=[
				"Email",
				"FirstName"
			];
		}else{
			var entityTableName=application.entitytype[entityTypeID].TABLENAME;
			var metadata=application.com.relayEntity.getEntityMetaData(entityTypeID=entityTypeID)[entityTableName];
			
			//loop over the metadata and grab the non nullable fields
			
			var requiredAttributes=ArrayNew(1);
			
			for(var key in metadata){
				if (not metadata[key].ISNULLABLE){
					ArrayAppend(requiredAttributes,key);
				}
			}
			arraySort(requiredAttributes,"textnocase");
		}
		return sanitiseAttributes(requiredAttributes);
		
	}
	
	


	/**
	 * Takes a total list of attributes for an entity type, filters them between the ones on the main table and attaches how to get the non standard ones
	 **/
	public struct function filterStandardAndNonStandardAttributes(required array attributeArray, required numeric entityTypeID) output="false" {
		var returnStruct={primaryAttributes=arrayNew(1),nonstandardAttributes=structNew()};
		var availableNonStandardAttributes=getAttributeExtrasStructure(entityTypeID);
		
		for(var i=1; i <= ArrayLen(attributeArray); i++){
			var requestedAttribute=attributeArray[i];
			if (structKeyExists(availableNonStandardAttributes,requestedAttribute)){
				returnStruct.nonstandardAttributes[requestedAttribute]=availableNonStandardAttributes[requestedAttribute];
			}else{
				arrayAppend(returnStruct.primaryAttributes,requestedAttribute);
			}
		}

		return returnStruct;
	}
	
	/**
	 * These are the extra "unusual" person, organisation and location level fields that aren't held directly on the table
	 * e.g. countryISO code and language iso code
	 **/
	private struct function getAttributeExtrasStructure(required numeric  entityTypeID){
		var extraAttributes=structNew();
		if (entityTypeID EQ application.entityTypeID.person){
			extraAttributes["LanguageISOCode"]={joiningTable="language", foreignKey="language", primaryKey="language", requiredField="ISOCode"};
		}else if (entityTypeID EQ application.entityTypeID.location){
			extraAttributes["CountryISOCode"]={joiningTable="country", foreignKey="countryID", primaryKey="countryID", requiredField="ISOCode"};
		}else{
			extraAttributes["OrganisationCountryISOCode"]={joiningTable="country", foreignKey="countryID", primaryKey="countryID", requiredField="ISOCode"};
			extraAttributes["OrganisationType"]={joiningTable="OrganisationType", foreignKey="OrganisationTypeID", primaryKey="OrganisationTypeID", requiredField="TypeTextID"};
		}	
		
		return extraAttributes;
	}


}