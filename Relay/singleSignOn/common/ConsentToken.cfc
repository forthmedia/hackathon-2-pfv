<!---
  --- OAuth2Code
  --- ----------
  ---	This is a token that is passed around when the "gaining user concent to share data" form is being displayed. 
  ---	This avoids a hostile but authorised 3rd party from being able to skip user consent by including a form parameter
  ---	acceptSendDataToThirdParty along with their redirect to our authorisation page
  --- author: Richard.Tingle
  --- date:   15/07/15
  --->
<cfcomponent extends="com.tokens.oneTimeUseToken" accessors="true" output="false" persistent="false">
	<cffunction name="init" access="public" output="false" returntype="any">
		<cfargument name="tokenTypeTextID" type="string" default="consentToken" />
		<cfargument name="validity_seconds" type="numeric" default=3600 />
		<cfset super.init(argumentCollection=arguments)>

		<cfreturn this>
	</cffunction>

	<cffunction name="setClientIdentifier" access="public" output="false" returntype="void">
		<cfargument name="clientID" type="string" required=true />
		<cfscript>
			set("clientIdentifier",clientID);
		</cfscript>
	</cffunction>

	<cffunction name="getClientIdentifier" access="public" output="false" returntype="string">
		<cfscript>
			return get("ClientIdentifier");
		</cfscript>
	</cffunction>


</cfcomponent>