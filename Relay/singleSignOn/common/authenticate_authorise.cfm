
<!---clientName and consentToken are passed to this page, clientName can be used for display is desired, consentToken must be returned in the form scope --->
<cfparam name="clientName" default="#attributes.clientName#">
<cfparam name="consentToken" default="#attributes.consentToken#">
	
<cf_displayBorder>
	<cfoutput>
	<h2 style="text-align:center">#clientName# phr_oauth_wouldLikeToAccessYourData</h2>
	
		<form role="form" method="post" name="editClient">
			<div class="text-center form-vertical">
				<div class="form-group">
					<input type="hidden" name="consentTokenString" value="#consentToken.getTokenValue()#">
					<button type="submit" class="btn btn-default" name="acceptSendDataToThirdParty">phr_Sys_Yes</button>
					<button type="submit" class="btn btn-default" name="rejectSendDataToThirdParty">phr_Sys_No</button>
				</div>
			
			</div>
			
		</form>
	

	
	</cfoutput>
</cf_displayBorder>