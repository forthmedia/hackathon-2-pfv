<cfset application.com.request.setDocumentH1(text="phr_SSO_sitewideSettings")>

<cfif structKeyExists(url,"siteDefId") and not (structKeyExists(form,"mainMenu"))>
	<cf_include template="/singleSignOn/common/editor/LoginBehaviourManagement_oneSite.cfm">	
<cfelse>	
	<CF_tableFromQueryObject queryObject="#application.com.singleSignOnSiteControl.getAllSitesSSOBehaviour()#" 
		showTheseColumns="Title,defaultAllowRegularLogin,defaultAllowSingleSignOn,autostartSingleSignOn"
		ColumnHeadingList="relayware_site,sso_allowStandardLogin,sso_allowSSO,sso_beginSSOAutomatically"
		ColumnTranslationPrefix="phr_"
		HidePageControls="no"
		useInclude = "false"
		numRowsPerPage="400"
		keyColumnList="title"
		booleanFormat="defaultAllowRegularLogin,defaultAllowSingleSignOn,autostartSingleSignOn"
		keyColumnURLList="#request.script_name#?siteDefId="
		keyColumnKeyList="siteDefId"
		columnTranslation="true"
		allowColumnSorting="false">
	
</cfif>