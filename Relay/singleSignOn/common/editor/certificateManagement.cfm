
<cfif structKeyExists(url,"createCertificate")>
	
	<cfinclude template="certificateManagement_createCertificate.cfm">
	
<cfelse>
	<cfscript>
		certificateManager=new singleSignOn.SAML.IDP.CertificateManager();
		if (structKeyExists(form,"deleteCertificate")){
			//a UI prevents deletion button being available this is just a validation
			if (certificateManager.certificateCanBeDeleted(form.certificateAlias)){
				certificateManager.deleteCertificate(form.certificateAlias);
			}
		}
		
	</cfscript>
	
	
	
	
	<div class="internalBodyPadding">
	
		<p>phr_saml_certificate_explanation</p>
	
		
		<div class="entry">
			<form action="##" id="create" method="get" >
				<input type="hidden" name="createCertificate">
				<cf_input type="submit" id="createScope" value="Create New Certificate" />
			</form>
				
		</div>
	
		<h2>phr_saml_Manage_Existing_Certificates</h2>
	
		<table id="existingClients" class="display responsiveTable table table-striped table-bordered table-condensed">
			<thead>
				<tr>
					<th>phr_saml_Certificate_Alias</th>
		            <th>phr_Sys_Expires</th>
		            <th>phr_delete</th>
				</tr>
			</thead>
		    <tbody>
				<cfset evenRow=false>
				<cfloop array='#certificateManager.getAllAliases()#' index='certificateAlias'>
					<cfset evenRow=not evenRow>
					<tr class="<cfoutput>#evenRow?'evenRow':'oddRow'#</cfoutput>">
						<td><cfoutput>#HTMLEditFormat(certificateAlias)#</cfoutput></td>
						<td><cfoutput>#HTMLEditFormat(DateFormat(certificateManager.getCertificateExpiry(certificateAlias),"dd-mmm-yyyy"))#</cfoutput></td>
	
						<td  align="center" valign="top">
							<cfif certificateManager.certificateCanBeDeleted(certificateAlias)>
								<form action="" id="deleteClient" method="post" >	
									<cf_input type="hidden" name="certificateAlias" value="#HTMLEditFormat(certificateAlias)#">
									<cf_input type="hidden" name="deleteCertificate" value="true">
									<input type="image" src="/images/MISC/iconDeleteCross.gif" onclick="return confirm('phr_areYouSureYouWantToDelete')"  height="16"/>
								</form>
							<cfelse>
								<div title="Note that certificates cannot be deleted while service providers are still assigned to it">
									phr_na
								</div>
							</cfif>
			
						</td>
					</tr>
				</cfloop>
		    </tbody>
		</table>
		
		<p>phr_saml_used_certificates_cant_be_deleted</p>
	
	
	
	
	</div>
</cfif>
