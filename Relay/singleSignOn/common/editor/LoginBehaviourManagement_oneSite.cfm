<cf_checkFieldEncryption fieldnames="siteDefId">

<cfset request.relayFormDisplayStyle="html-div">
<cfset application.com.request.setTopHead(pageTitle = "OAuth2 Authorisation Server Client Settings",topHeadCfm="/relay//singleSignOn/common/editor/LoginBehaviourManagementTopNav.cfm")>

<cfscript>
	
	if(StructKeyExists(form,"defaultAllowRegularLogin")){
		application.com.singleSignOnSiteControl.updateSiteSSOBehaviour(siteDefId,defaultAllowRegularLogin,defaultAllowSingleSignOn,autostartSingleSignOn);
		application.com.relayui.setMessage(message="phr_loginBehaviorUpdated");
	}
	
	siteBehaviour=application.com.singleSignOnSiteControl.getSiteSSOBehaviour(siteDefId);

</cfscript>

<form id="siteBehaviour" action="" method="post">
<cfoutput>	
<cf_relayFormDisplay relayFormDisplayStyle="html-div">
	
	<CF_relayFormElementDisplay  relayFormElementType="text" currentValue="#siteBehaviour.Title#" fieldName="name" label="phr_relayware_site" disabled="true">
	<CF_relayFormElementDisplay relayFormElementType="radio" list="1#application.delim1#phr_yes,0#application.delim1#phr_no" currentValue="#siteBehaviour.defaultAllowRegularLogin#" fieldName="defaultAllowRegularLogin" label="phr_sso_allowStandardLogin"  >
	<CF_relayFormElementDisplay relayFormElementType="radio" list="1#application.delim1#phr_yes,0#application.delim1#phr_no" currentValue="#siteBehaviour.defaultAllowSingleSignOn#" fieldName="defaultAllowSingleSignOn" label="phr_sso_allowSSO"  >
	<CF_relayFormElementDisplay relayFormElementType="radio" list="1#application.delim1#phr_yes,0#application.delim1#phr_no" currentValue="#siteBehaviour.autostartSingleSignOn#" fieldName="autostartSingleSignOn" label="phr_sso_beginSSOAutomatically" NoteText="phr_SAML_OnlyWithOneMethod">
	
</cf_relayFormDisplay>
</cfoutput>
</form>

<script>
	
	function FormSubmit() {
	    triggerFormSubmit(jQuery('#siteBehaviour'));
	}
</script>	