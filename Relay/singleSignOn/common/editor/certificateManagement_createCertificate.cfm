

<cfscript>

	certificateManager=new singleSignOn.SAML.IDP.CertificateManager();
	
	creationMessage="";
	if (structKeyExists(form,"certificatealias") and structKeyExists(form,"certificateValidity")){
		
		if(not certificateManager.certificateExists(form.certificatealias)) {
			certificateManager.createCertificate(form.certificatealias, form.certificateValidity);
			creationMessage="phr_certificate_created";
			location("./certificateManagement.cfm", false, 307);
		}else{
			creationMessage="phr_certificate_exists";
		}
	}


</cfscript>

	

<div class="internalBodyPadding">
	
	<h1>phr_saml_SAML_Identity_Provider - phr_saml_Certificate_Management</h1>
	<br>
	<form role="form" method="post">

		  <div class="form-group">
		    <label for="certificatealias">phr_saml_Certificate_Alias</label>
		    <input type="text" class="form-control" name="certificatealias" id="certificatealias" placeholder="Alias Name" required="required">
		  </div>
		  <div class="form-group">
		    <label for="certificateValidity">phr_saml_Certificate_Validity</label>
		    <input type="number" min="1" max="10" step="1" class="form-control" name="certificateValidity" id="certificateValidity" placeholder="Validity (years)" required="required" value="10">
		  </div>
		  <cfif creationMessage NEQ "">
			  <p>
				<cfoutput>#creationMessage#</cfoutput>
			  </p>
		  </cfif>
	  <button type="submit" class="btn btn-primary">phr_Save</button>
		
	</form>


</div>





