<!--- 
@author MRE
@date 2016/12/30
Template for launching a registration with data parsed from an SSO request
Recieves the data as an encryped URL parameter - stays secure while avoiding using the 
--->


<cfset request.hiderightborder="true">
<cfset request.BorderSections="top">
<cf_DisplayBorder>

<!--- Decrypt mapping data to send as parameters to registration form --->
<cfset ssoPlugin=new singleSignOn.OAuth2.client.OAuth2AuthenticationFilterPlugin()>
<cfif structKeyExists(cookie,"mappingToken")>
	<cfset mappingStruct=ssoPlugin.decryptState(cookie.mappingToken)>
	<cf_addPartnerRegistrationDataForm
		showCols="#mappingStruct.showCols#"
		populationDetails="#mappingStruct.formFields#"
		mandatoryCols="#application.com.settings.getSetting("plo.mandatoryCols")#"
		autoPopulate=true
		autoPopulateSettings="#mappingStruct.autoPopulateSettings#"
		defaultCountryID="#mappingStruct.countryID#"
		frmNext="returnScreenid"
		>
</cfif>
