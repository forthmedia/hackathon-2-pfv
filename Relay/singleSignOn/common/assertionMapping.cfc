/**
* @author Martin Elgie
* @date 2016/12/07 (ymd)
* Despite having the same name this is the object used by OAuth2 client side only.
* SAML SP equivalent is a java object.
*/

component accessors=true output=false persistent=false {

	public void function init(String relaywareField="", boolean useAssertion=false, String foreignValue="") {
		this.relaywareField=arguments.relaywareField;
		this.useAssertion=arguments.useAssertion;
		this.foreignValue=arguments.foreignValue;
	}

	public String function getRelaywareField() {
		return this.relaywareField;
	}

	public boolean function isUseAssertion() {
		return this.useAssertion;
	}

	public String function getForeignValue() {
		return this.foreignValue;
	}

}