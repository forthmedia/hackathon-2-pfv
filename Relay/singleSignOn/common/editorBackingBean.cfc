<!---
  --- editorBackingBean
  --- -----------------
  ---
  --- author: Richard.Tingle
  --- date:   21/07/15
  --- Functions for OAuth2 SSO
  --- MRE: This is mostly OAuth2 specific so probably shouldn't be in common, but attributes are used by SAML too
  --- TODO: Move and rename as something like OAuth2Manager, refactor out attirbute method
  --->
<cfcomponent accessors="true" output="false" persistent="false">


    <cffunction name="createNewClient" access="public" output="false" returntype="string">
        <cfargument name="clientName" type="string" required="true" />
        <cfargument name="redirectURL" type="string" required="true" />
        <cfscript>
            var oauth2client=createObject("component","singleSignOn.OAuth2.AuthorisationServer.OAuth2Client");

            if (not redirectURL.toLowerCase().startsWith("http")){
                redirectURL="http://" & redirectURL;
            }

            oauth2client.init(clientName=clientName,redirectURL=redirectURL);
            oauth2client.autogenerateClient(); //the client secret will be overritten later but we need the client ID
            oauth2client.persistToDatabase();

        </cfscript>

        <cfreturn oauth2client.getClientIdentifier()/>
    </cffunction>

    <cffunction name="createNewServer" access="public" output="false" returntype="string">
        <cfscript>
            var oauth2server=createObject("component","singleSignOn.OAuth2.client.OAuth2Server");
            oauth2server.init();
        </cfscript>

        <cfreturn oauth2client.getClientIdentifier()/>
    </cffunction>

    <cffunction name="deleteClient" access="public" output="false" returntype="void">
        <cfargument name="clientID" type="string" required="true"/>
        <cfscript>
            var oauth2client=retrieveClient(clientID);
            oauth2client.deleteFromDatabase();
        </cfscript>

        <cfreturn />
    </cffunction>

    <cffunction name="deleteServer" access="public" output="false" returntype="void">
        <cfargument name="clientID" type="string" required="true"/>
        <cfscript>
            var oauth2Server=retrieveServer(clientID);
            oauth2Server.deleteFromDatabase();
        </cfscript>

        <cfreturn />
    </cffunction>

	<cffunction name="deactivateClient" access="public" output="false" returntype="void">
		<cfargument name="clientID" type="string" required="true"/>
			<cfscript>
				var oauth2client=retrieveClient(clientID);
				oauth2client.setActive(false);
				oauth2client.persistToDatabase();
			</cfscript>

		<cfreturn />
	</cffunction>

	<cffunction name="activateClient" access="public" output="false" returntype="void">
		<cfargument name="clientID" type="string" required="true"/>

			<cfscript>
				var oauth2client=retrieveClient(clientID);
				oauth2client.setActive(true);
				oauth2client.persistToDatabase();
			</cfscript>
		<cfreturn />
	</cffunction>

    <cffunction name="deactivateServer" access="public" output="false" returntype="void">
        <cfargument name="clientID" type="string" required="true"/>
        <cfscript>
            var oauth2server=retrieveServer(clientID);
            oauth2server.setActive(false);
            oauth2server.persistToDatabase();
        </cfscript>

        <cfreturn />
    </cffunction>

    <cffunction name="activateServer" access="public" output="false" returntype="void">
        <cfargument name="clientID" type="string" required="true"/>

        <cfscript>
            var oauth2server=retrieveServer(clientID);
            oauth2server.setActive(true);
            oauth2server.persistToDatabase();
        </cfscript>
        <cfreturn />
    </cffunction>

    <cffunction name="retrieveClient">
        <cfargument name="clientID" type="string" required="true"/>
        <cfscript>
            oauth2client=createObject("component","singleSignOn.OAuth2.AuthorisationServer.OAuth2Client").init(clientIdentifier=clientID);
            oauth2client.pullFromDatabase();
            return oauth2client;
        </cfscript>

    </cffunction>

    <cffunction name="retrieveServer">
        <cfargument name="clientID" type="string" required="true"/>
        <cfscript>
            oauth2server=createObject("component","singleSignOn.OAuth2.client.OAuth2Server").init(clientId=clientID);
            oauth2server.pullFromDatabase();
            return oauth2server;
        </cfscript>

    </cffunction>

	<cffunction name="createSetSecretLink" access="public" output="false" returntype="String">
		<cfargument name="clientID" type="string" required="true"/>
			<cfscript>
				var protocal="";
				var oAuth2clientGenerationToken="";
				var tokenValue="";

				var oAuth2client =createObject("component","singleSignOn.OAuth2.AuthorisationServer.OAuth2Client");
				oAuth2client.init(clientIdentifier=clientID);
				oAuth2client.pullFromDatabase();
				oAuth2client.persistToDatabase();

				oAuth2clientGenerationToken =createObject("component","singleSignOn.OAuth2.AuthorisationServer.OAuth2ClientGenerationToken");
				
				//here we're calling static methods only!
				oAuth2clientGenerationToken.invalidateAllTokensForClient(oAuth2client);
				
				//now we get an instance
				oAuth2clientGenerationToken.init();
				oAuth2clientGenerationToken.setClientID(oAuth2client.getClientIdentifier());
				oAuth2clientGenerationToken.autogenerateToken();
				oAuth2clientGenerationToken.persistToDatabase();
				tokenValue=oAuth2clientGenerationToken.getTokenValue();

				if(cgi.server_port_secure){
					protocal="https://";
				}else{
					protocal="http://";
				}


				return protocal & cgi.HTTP_HOST & "/singleSignOn/OAuth2/authorisationServer/generateClient.cfm?clientToken=" &tokenValue;
			</cfscript>

		<cfreturn />
	</cffunction>

	<cffunction name="getAllClients" access="public" output="false" returntype="array">
		<cfset var clientList="">
		<cfset var clientArray="">
		<cfset var clientObjectArray= arrayNew(1)>
		<CFQUERY NAME="clientIdentifers" datasource="#application.siteDataSource#">
			SELECT
			    clientIdentifier
			FROM
				OAuth2Client;
		</CFQUERY>

		<CFLOOP QUERY="clientIdentifers">
			<cfscript>
				clientList=listAppend(clientList,clientIdentifers.clientIdentifier);
			</cfscript>
		</CFLOOP>



		<cfscript>
			clientArray=listToarray(clientList);

			for(i=1;i<=arrayLen(clientArray);i++){
				clientObjectArray[i]=createObject("component","relay.singleSignOn.OAuth2.AuthorisationServer.OAuth2Client").init(clientIdentifier=clientArray[i]);
				clientObjectArray[i].pullFromDatabase();
			}


			return clientObjectArray;
		</cfscript>

	</cffunction>

    <cffunction name="getAllServers" access="public" output="false" returntype="array">
        <cfset var serverArray=arraynew(1)>
        <cfset var serverObjectArray=arrayNew(1)>
        <CFQUERY NAME="serversQuery" datasource="#application.siteDataSource#">
			SELECT
			    clientIdentifier
			FROM
				OAuth2Server;
		</CFQUERY>

        <CFLOOP QUERY="serversQuery">
            <cfscript>
				arrayappend(serverArray,clientIdentifier);
            </cfscript>
        </CFLOOP>


        <cfscript>
            for (i = 1; i <= arrayLen(serverArray); i++) {
                serverObjectArray[i] = createObject("component", "relay.singleSignOn.OAuth2.client.OAuth2Server").init(clientId = serverArray[i]);
                serverObjectArray[i].pullFromDatabase();
            }

            return serverObjectArray;
        </cfscript>

    </cffunction>

    <cffunction name="getValidServers" access="public" output="true" returntype="array" 
    		hint="Returns currenly active sites set to be used on this site">
	    <cfscript>
		    var serverArray=getAllServers();
		    var validArray=arrayNew(1);
		    for (entry in serverArray) {
		    	if (entry.isActive() AND entry.getauthorisedSites() eq request.currentsite.sitedefid) {
		    		validArray.add(entry);
		    	}
		    }
		    return validArray;
	    </cfscript>
    </cffunction>

	<cffunction name="eliminateProfilesWithoutTextIDs" access="public" output="false" returntype="array">
		<cfargument name="fieldList" type="array" required="true"/>
		<cfscript>
			var arrayLength=arrayLen(fieldList);
			//loop backwards to avoid the indexes changing while deleting issue
			for(var i=arrayLength;i>0;i--){
				if (isnumeric(fieldList[i])){
					ArrayDeleteAt(fieldList, i);
				}
			}

			return fieldList;

		</cfscript>
	</cffunction>
	
	<cffunction name="validateList" access="public" output="false" returntype="struct" hint="Returns a struct containing isOk which details if all the desiredFields are allowed and an array disallowedFields which will contain all extra fields">
		<cfargument name="desiredFieldList" type="array" required="true" hint="the desired fields"/>
		<cfargument name="allAvailableFields" type="array" required="true" hint="the list of fields that must contain the desired fields" />
		<cfscript>
			return validateScopeProfiles(desiredFieldList,allAvailableFields);

		</cfscript>
	</cffunction>
	

	<cffunction name="validateScopeProfiles" access="public" output="false" returntype="struct" hint="Returns a struct containing isOk which details if all the desiredFields are allowed and an array disallowedFields which will contain all extra fields">
		<cfargument name="desiredFieldList" type="array" required="true" hint="the desired fields"/>
		<cfargument name="allAvailableFields" type="array" required="true" hint="the list of fields that must contain the desired fields" />
		<cfscript>
			var returnStruct={isOk=true,disallowedFields=arrayNew(1), allowedFields=arrayNew(1)};

			for(field in desiredFieldList){
				if (NOT arrayContains(allAvailableFields,field)){
					returnStruct.isOk=false;
					arrayAppend(returnStruct.disallowedFields,field);
				}else{
					arrayAppend(returnStruct.allowedFields,field);

				}
			}


			return returnStruct;

		</cfscript>
	</cffunction>

	<cffunction name="convertArrayToOrderedStruct" access="public" output="false" returntype="struct">
		<cfargument name="arrayToConvert" type="array" required="true" hint="the desired fields"/>
		
		<cfscript>
			orderedStruct = createObject("java", "java.util.LinkedHashMap").init();

		    for(i=1;i<=arrayLen(arrayToConvert);i=i+1){
	
		    	orderedStruct[arrayToConvert[i]]=arrayToConvert[i];
		    }
		    return orderedStruct;
		
		</cfscript>
		
		
	</cffunction>
	
	<cffunction name="deleteArrayFromArray" access="public" output="false" returntype="array">
		<cfargument name="startArray" type="array" required="true"/>
		<cfargument name="arrayToDelete" type="array" required="true"/>
		<cfscript>
			newArray=ArrayNew(1);

		    for(i=1;i<=arrayLen(startArray);i=i+1){
		    	if (NOT ArrayContains(arrayToDelete,startArray[i])){
		    		arrayAppend(newArray,startArray[i]);
		    	}
		    }
		    return newArray;
		
		</cfscript>
		
		
	</cffunction>


</cfcomponent>