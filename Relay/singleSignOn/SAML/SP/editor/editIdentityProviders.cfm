
<cfset application.com.request.setTopHead(pageTitle = "phr_saml_SAMLIdentityProviders_title",addItemTemplate="editIdentityProvider.cfm",topHeadCfm="/relay/singleSignOn/SAML/SP/editor/SAMLSPTopNav.cfm")>

<cfparam name="sortOrder" default="identityProviderName">
<cfset IDPs=application.com.SAML.getAllIdentityProviders(sortOrder=sortOrder)>

<CF_tableFromQueryObject queryObject="#IDPs#" 
	showTheseColumns="identityProviderName,identityProviderEntityID,authnRequestURL"
	ColumnHeadingList="saml_identityProviderName,saml_identityProviderEntityID,saml_authnRequestURL"
	ColumnTranslationPrefix="phr_"
	HidePageControls="no"
	useInclude = "false"
	numRowsPerPage="400"
	keyColumnList="identityProviderName"
	keyColumnURLList="editIdentityProvider.cfm?IdentityProvideID="
	keyColumnKeyList="SAMLIdentityProviderID"
	columnTranslation="true">