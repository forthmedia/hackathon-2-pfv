<!--- This outputs an XML document for the user to save on their system; the SP metadata --->

<cfsetting showDebugOutput="No"> <!--- Just so debugging doesn't mess up the file on Dev sites --->

<cfparam name="form.siteDomain" type="string">

<cfscript>
	certificateManager = new singleSignOn.SAML.IDP.CertificateManager();
	password = certificateManager.getCertificateStorePassword();
    //certificateAlias = identityProvider.getCertificateToUse();
    certificationKeyStorePath = Replace(certificateManager.getCertificateStoreLocation(), "\", "\\", "all");

	metadataGenerator=application.javaloader.create("singleSignOn.samlserviceprovider.ServiceProviderMetadataGenerator").init();
	metadata=metadataGenerator.buildMetadata(application.com.saml.getRelaywareAsSPConfigForADomain(form.siteDomain), certificationKeyStorePath, certificateAlias, password);

</cfscript>



<cfheader name="Content-Disposition" value="attachment; filename=SPmetadata.xml" />
<!--- RJT  Must be all on one line! --->
<cfcontent type="application/xml;charset=utf-8" reset="true"><cfoutput>#metadata#</cfoutput><cfabort>


