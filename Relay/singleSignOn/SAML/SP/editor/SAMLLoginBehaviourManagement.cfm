
<!---This is a specialisation of the common LoginBehaviourManagement just so we can show a saml specific nav bar --->

<cfset application.com.request.setTopHead(pageTitle = "phr_SSO_LoginBehaviourManagement",topHeadCfm="/relay/singleSignOn/SAML/SP/editor/SAMLSPTopNav.cfm", showSave=isDefined("siteDefId"))>

<cf_include template="/singleSignOn/common/editor/LoginBehaviourManagement.cfm">
