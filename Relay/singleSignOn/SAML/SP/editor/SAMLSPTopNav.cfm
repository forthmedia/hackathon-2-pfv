<cfparam name="showSave" type="boolean" default="false">
<cfparam name="showDelete" type="boolean" default="false">
<cfparam name="addItemTemplate" type="string" default="">
<cfparam name="addItemTemplateText" type="string" default="phr_Add">


<CF_RelayNavMenu pageTitle="SAML">
	<cfif showSave>
		<cf_relayNavMenuItem MenuItemText="phr_Save" CFTemplate="##" onclick="FormSubmit(this);">
	</cfif>
	<cfif showDelete>
		<cf_relayNavMenuItem MenuItemText="phr_Delete" CFTemplate="##" onclick="deleteItem(); return false;">
	</cfif>
	<cfif addItemTemplate NEQ "">
		<cf_relayNavMenuItem MenuItemText="#addItemTemplateText#" CFTemplate="#application.com.security.encryptURL(addItemTemplate)#" onclick="">
	</cfif>
	<CF_RelayNavMenuItem MenuItemText="phr_SSO_SAMLIdentityProviders" CFTemplate="editIdentityProviders.cfm">
	<CF_RelayNavMenuItem MenuItemText="phr_SSO_SAMLSPMetadata" CFTemplate="metadataServiceProvider.cfm">
	<CF_RelayNavMenuItem MenuItemText="phr_saml_Certificate_Management" CFTemplate="certificateManagement.cfm">
</CF_RelayNavMenu>


