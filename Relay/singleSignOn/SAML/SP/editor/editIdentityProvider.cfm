
<cfset application.com.request.setTopHead(pageTitle = "phr_saml_SAMLIdentityProviders_title", topHeadCfm="/relay/singleSignOn/SAML/SP/editor/SAMLSPTopNav.cfm", showSave="true", showDelete="true")>

<cf_includeJavascriptOnce template = "/singleSignOn/SAML/js/processSAMLIDPMetaData.js">
<cf_includeJavascriptOnce template = "/singleSignOn/SAML/js/editIdentityProvider.js">
<cf_includejavascriptonce template = "/javascript/rwFormValidation.js">
<cf_includeJavascriptOnce template ="/javascript/prototypeSpinner.js"> <!--- For the loading button spinners --->

<cfif structKeyExists(url,"identityProvideID")>
	<cf_checkfieldEncryption fieldnames="identityProvideID">
	<cfset newRecord=false>
<cfelse>
	<cfset newRecord=true>
</cfif>

<cfset savingData=structKeyExists(form,"identityProviderName")>
<cfset deleting=structKeyExists(form,"deleteItem")>
<cfset loadingData=not savingData and not newRecord>

<cfscript>
	idpManager=application.javaloader.create("singleSignOn.samlserviceprovider.idps.IDPManager").init();
	certificateManager = new singleSignOn.SAML.IDP.CertificateManager();

	if (not newRecord){
		identityProvider=idpManager.getIDPByID(url.identityProvideID);
	}else{
		identityProvider=application.javaloader.create("singleSignOn.samlserviceprovider.idps.IdentityProvider").init();
	}


	samlAttributeManager=new  singleSignOn.common.AttributeManager();
	personFieldsArray=samlAttributeManager.getPotentialAttributesForEntity(entityTypeID=application.entityTypeID.person);
	personFields=ArrayToList(personFieldsArray);
	locationFields=ArrayToList(samlAttributeManager.getPotentialAttributesForEntity(entityTypeID=application.entityTypeID.location));
	personFieldsWithBlank=" ,"&personFields;
	essentialPersonFieldsArray=samlAttributeManager.getRequiredAttributesForEntity(application.entityTypeID.person);

	if(deleting){
		idpManager.delete(identityProvider);
		location("editIdentityProviders.cfm", false, 303);
		abort;
	}

	if (savingData){
		//because an empty multiselect is simply missing we param form.startingUsergroups
		param name="form.startingUsergroups" default="";
		param name="form.startingUsergroups" default="";
		param name="form.usergroupsPermittedToControl" default="";
	        param name="form.startingUsergroupCountries" default="";
	    param name="form.AuthorisedSitesToAuthenticate" default="";
	    param name="form.autoCreate_selectedLocationID" default="0"; //can be empty when there is an empty organisation selected for autocreation
	    
		//a last protection, should never happen
		if (listFind(form.usergroupsPermittedToControl,4) NEQ 0){
			throw (message="Attempted to authorise IDP to control RW Admin. #form.usergroupsPermittedToControl# #listContains(form.usergroupsPermittedToControl,4)#");
		}
		

		//2 form fields are effectively one piece of data; the authorised site. We combine the two
		if (form.internalOnlyIDP){
			form.AuthorisedSitesToAuthenticate=application.com.relayCurrentSite.getInternalSite().SITEDEFID;
		}
	
		identityProvider.setIdentityProviderName(form.identityProviderName);
		identityProvider.setIdentityProviderIdentifier(form.identityProviderEntityID);
		identityProvider.setAuthnRequestURL(form.authnRequestURL);
		identityProvider.setNameIDField_relayware(form.nameIDField_Relayware);
		identityProvider.setNameIDField_Foreign(form.nameIDField_Foreign);
		identityProvider.setRawIDPMetadata(form.rawMetadata);
		identityProvider.setActive(form.active);
		identityProvider.setAllowAutoCreate(form.allowAutoCreate);
		identityProvider.setAutoCreate_SelectLocOrg(form.autoCreate_SelectLocOrg);
		identityProvider.setAutoCreate_selectedOrganisationID(form.autoCreate_selectedOrganisationID);
		identityProvider.setAutoCreate_selectedLocationID(form.autoCreate_selectedLocationID);
		identityProvider.setUsergroupsAtPersonCreation_fromStringList(form.startingUsergroups);
		identityProvider.setUsergroupCountriesAtPersonCreation_fromStringList(form.startingUsergroupCountries);
		identityProvider.setAutoCreate_detectedLoc_ForeignField(form.autoCreate_detectedLoc_ForeignField);
		identityProvider.setAutoCreate_detectedLoc_RelaywareField(form.autoCreate_detectedLoc_RelaywareField);
		identityProvider.setPermittedToSetUsergroups(form.PermittedToSetUsergroups);
		identityProvider.setAssertionAttributeToControlUsergroups(form.assertionAttributeToControlUsergroups);
		identityProvider.setUsergroupsPermittedToControl_fromStringList(form.usergroupsPermittedToControl);
		identityProvider.setPermittedToControlCountryRights(form.PermittedToControlCountryRights);
		identityProvider.setAssertionAttributeToControlCountryRights(form.AssertionAttributeToControlCountryRights);
		identityProvider.setAuthorisedSitesToAuthenticate_fromStringList(javacast("string",form.AuthorisedSitesToAuthenticate));
		identityProvider.setOnCreationPortalPersonApprovalStatus(form.onCreationPortalPersonApprovalStatus);
		identityProvider.setCertificateToUse(form.certificateToUse);
		identityProvider.setPermittedToInitiateSAML(form.permittedToInitiateSAML);

		//deal with any mappings
		identityProvider.clearMappings(); //clear all existing mappings and add new ones
		mappings=DeserializeJSON(mappingTable_collapsed).collapsedData;

		for(i=1;i<=arrayLen(mappings);i++){
			mapping=mappings[i];
			identityProvider.addMapping(mapping.mappingFieldRelayware,mapping.currentMappingFromAssertion, mapping.mappingValue);

		}

		shouldSave=true;
		//new records not allowed if they have the same unique identifier
		if (newRecord){
			if (idpManager.identityProviderExistsByEntityID(form.identityProviderEntityID)){
				shouldSave=false;
				application.com.relayui.setMessage(message="phr_saml_cantAddDuplicateIDP", type="error", closeAfter="10");
			}
		}

		if (shouldSave){
			idpManager.persistIdentityManager(identityProvider);

			application.com.relayui.setMessage(message="phr_saml_identity_provider_saved", closeAfter="5");

			//if its a new record then we do a post then redirect to get us back in the normal flow
			if (newRecord){
				location("editIdentityProviders.cfm", false, 303);
				abort;
			}
		}
	}

</cfscript>

<script>
	<!--- Not in js file as uses translation --->
	function deleteItem(){
		var shouldDelete=confirm('phr_SAML_IDP_areYouSureYouWantToDelete');
		if (shouldDelete){
			jQuery("#deletionForm").submit();
		}
	}
</script>


<CFQUERY NAME="organsationsToSelect" datasource="#application.siteDataSource#">
	select OrganisationID as dataValue, OrganisationName as displayValue from organisation
	where OrganisationID !=1 <!---not allowed to be RW --->
	and Active = 1
	order by OrganisationName
</CFQUERY>

<!--- This is the only piece of data thats not directly held in the IDP, calculate it once --->
<cfset internalOnlyIDP = identityProvider.getAuthorisedSitesToAuthenticate_asStringList() EQ application.com.relayCurrentSite.getInternalSite().SITEDEFID>


<!--- A hidden form which is launched to delete the IDP  --->
<form id="deletionForm" method="post">
	<input name="deleteItem" type="hidden"/>
</form>


<form>
	<!---Because of the inconsistent way browsers implement <input type="file" ...> we "fake it" to allow us to have whatever styling we like --->
	  <div class="form-group">
	  	 <input id="metadataUploadButton" type="button" class="btn" value="phr_SAML_PopulatedFromMetaData" onclick="document.getElementById('metadataUpload').click();" />
	  </div>

	  <div hidden="hidden" style="display: none;">
			<input id="metadataUpload" type="file"/>
	  </div>

</form>


<div id="samlmetadataError" class="errorblock" style="display: none;">
	<a class="close" onClick="jQuery('#samlmetadataError').hide(600,'linear');"></a>
	<span id="samlmetadataError_message"></span>
</div>


<form id="identityProvider" action="" method="post">
	<cf_relayFormDisplay>
		<div id="mainControls" class="grey-box-top">
			<H2>phr_SAML_IDPDetails</H2>
			<CF_relayFormElementDisplay relayFormElementType="text" currentValue="#identityProvider.getIdentityProviderName()#" required="true" fieldName="identityProviderName" label="phr_SAML_identityProviderName">

			<CF_relayFormElementDisplay relayFormElementType="text" disabled="true" currentValue="#identityProvider.getIdentityProviderIdentifier()#" fieldName="identityProviderEntityID_display" label="phr_saml_identityProviderEntityID">
			<CF_relayFormElementDisplay relayFormElementType="hidden" currentValue="#identityProvider.getIdentityProviderIdentifier()#" required="true" fieldName="identityProviderEntityID" label="phr_saml_identityProviderEntityID">


			<CF_relayFormElementDisplay relayFormElementType="text" disabled="true" currentValue="#identityProvider.getAuthnRequestURL()#" fieldName="authnRequestURL_display" label="phr_saml_authnRequestURL">
			<CF_relayFormElementDisplay relayFormElementType="hidden" currentValue="#identityProvider.getAuthnRequestURL()#" required="true" fieldName="authnRequestURL" label="phr_saml_authnRequestURL">


			<CF_relayFormElementDisplay relayFormElementType="hidden" currentValue="#identityProvider.getRawIDPMetadata()#" hidden="true" fieldName="rawMetadata" label="RawMetadata">

			<CF_relayFormElementDisplay relayFormElementType="select" currentValue="#identityProvider.getNameIDField_relayware()#" list="#personFields#" fieldName="nameIDField_Relayware" label="phr_SAML_uniqueIdentifier_RWSide">
			<CF_relayFormElementDisplay relayFormElementType="text" placeholder="phr_SAML_LeaveBlankToUseNameID" currentValue="#identityProvider.getNameIDField_foreign()#" fieldName="nameIDField_Foreign" maxlength="256" label="phr_SAML_uniqueIdentifier_Assertion" NoteText="phr_SAML_LeaveBlankToUseNameID">

			<CF_relayFormElementDisplay relayFormElementType="radio" currentValue="#internalOnlyIDP#" fieldName="internalOnlyIDP" label="phr_SAML_ServiceProviderForSites" list="1#application.delim1#phr_PartnerCloud,0#application.delim1#phr_portal">

			<div class="portalOptions"
				<cfif internalOnlyIDP>style="display: none;"</cfif>
				>
			<CF_relayFormElementDisplay relayFormElementType="select" multiple="true" currentValue="#identityProvider.getAuthorisedSitesToAuthenticate_asStringList()#" fieldName="authorisedSitesToAuthenticate" label="phr_SAML_authorisedSites"  query="#application.com.relayCurrentSite.getSiteDefs(internal=false)#" value="siteDefID" display="title">
			</div>
		
				
		
			<CF_relayFormElementDisplay relayFormElementType="radio" currentValue="#identityProvider.isActive()#" fieldName="active" label="phr_sys_active" list="1#application.delim1#phr_yes,0#application.delim1#phr_no">
		</div>
		<CF_relayFormElementDisplay relayFormElementType="radio" currentValue="#identityProvider.getAllowAutoCreate()#" fieldName="allowAutoCreate" label="phr_saml_allowAutoCreate" list="1#application.delim1#phr_yes,0#application.delim1#phr_no">


		<div id="autocreateOptions" class="grey-box-top" <cfif not identityProvider.getAllowAutoCreate()>style="display: none;"</cfif>>

			<H2>phr_SAML_autocreationOptions</H2>
			<CF_relayFormElementDisplay relayFormElementType="radio" currentValue="#identityProvider.getAutoCreate_SelectLocOrg()#" fieldName="autoCreate_SelectLocOrg" label="phr_saml_creationOrganisationSelectionMethod" list="1#application.delim1#phr_SAML_SelectLocOrg,0#application.delim1#phr_SAML_DetectLocOrg">

			<div id="selectLocOrgDiv"
				<cfif not identityProvider.getAutoCreate_SelectLocOrg()>style="display: none;"</cfif>
				>
				<CF_relayFormElementDisplay relayFormElementType="select" currentValue="#identityProvider.getAutoCreate_selectedOrganisationID()#" fieldName="autoCreate_selectedOrganisationID" label="phr_saml_selectedOrganisation" query="#organsationsToSelect#">
				<CF_relayFormElementDisplay relayFormElementType="select" currentValue="#identityProvider.getAutoCreate_selectedLocationID()#" fieldName="autoCreate_selectedLocationID" bindOnLoad="true" size="1" bindFunction="cfc:webservices.callWebService.callWebservice(webServiceName='samlWS',methodName='getLocationsAtOrganisation',organisationID={autoCreate_selectedOrganisationID})" label="phr_saml_selectedLocation">
			</div>
			<div id="detectLocOrgDiv"
				<cfif identityProvider.getAutoCreate_SelectLocOrg()>style="display: none;"</cfif>
				>
				<CF_relayFormElementDisplay relayFormElementType="select" currentValue="#identityProvider.getAutoCreate_detectedLoc_RelaywareField()#" list="#locationFields#" fieldName="autoCreate_detectedLoc_RelaywareField" label="phr_SAML_Location_uniqueIdentifier_RWSide">
				<CF_relayFormElementDisplay relayFormElementType="text" currentValue="#identityProvider.getAutoCreate_detectedLoc_ForeignField()#" fieldName="autoCreate_detectedLoc_ForeignField" label="phr_SAML_location_uniqueIdentifier_Assertion">
			</div>

			<!--- Just before submission the mappingTable data is collapsed into mappingTable_collapsed--->
			<!--- Put a max length on or else it seems to end up as max length 50--->
			<CF_relayFormElementDisplay currentValue="" maxlength="4000" relayFormElementType="hidden" fieldName="mappingTable_collapsed" label="">

			<table id="mappingTable" class="table table-hover table-striped table-bordered">

				<tr>
					<th>phr_SAML_RelaywareField</th><th>phr_SAML_MappingMethod</th><th>phr_SAML_Value</th><th>phr_sys_Delete</th>
				</tr>
				<!--- The essential items --->
				<cfloop array="#essentialPersonFieldsArray#" index="essentialItem">

					<cfset essentialMapping=identityProvider.getMappingForRelaywareField(essentialItem)>

					<cfif isNull(essentialMapping)>
						<cf_SAMLMappingTableRow
							currentField="#essentialItem#"
							availableFields="#essentialItem#"
							currentMapping=""
							lockMappingType="true"
							deletable="false"
						>
					<cfelse>
						<cf_SAMLMappingTableRow
							currentField="#essentialItem#"
							availableFields="#essentialItem#"
							currentMapping="#essentialMapping.getForeignValue()#"
							lockMappingType="true"
							deletable="false"
						>
					</cfif>


				</cfloop>

				<!--- The existing non essential items --->
				<cfloop array="#identityProvider.getMappings()#" index="mapping">
					<cfif not ArrayContains(essentialPersonFieldsArray, mapping.getRelaywareField())>
						<cf_SAMLMappingTableRow
							currentField="#mapping.getRelaywareField()#"
							availableFields="#personFieldsWithBlank#"
							currentMappingFromAssertion="#mapping.isUseAssertion()#"
							currentMapping="#mapping.getForeignValue()#"
						>

					</cfif>


				</cfloop>

				<!--- A blank row for new items--->
				<cf_SAMLMappingTableRow
					currentField=""
					availableFields="#personFieldsWithBlank#"
					currentMappingFromAssertion="1"
					currentMapping=""
				>



			</table>

			<div class="partnerCloudOptions"
				<cfif not internalOnlyIDP>style="display: none;"</cfif>
				>
				<CF_relayFormElementDisplay relayFormElementType="select" multiple="true" currentValue="#arrayToList(identityProvider.getUsergroupsAtPersonCreation())#" fieldName="startingUsergroups" label="phr_SAML_StartingUsergroups"  query="#application.com.relayUserGroup.getUserGroupsForSelect(userGroupIDsToExclude=4, userGroupTypesToExclude='personal')#" value="usergroupid" display="name">
				<CF_relayFormElementDisplay relayFormElementType="select" multiple="true" currentValue="#arrayToList(identityProvider.getUsergroupCountriesAtPersonCreation())#" fieldName="startingUsergroupCountries" label="phr_SAML_StartingUsergroupCountries" query="#application.com.RelayCountries.getCountries()#" value="countryID" display="CountryDescription">
			</div>

			<div class="portalOptions"
				<cfif internalOnlyIDP>style="display: none;"</cfif>
				>
				<CF_relayFormElementDisplay relayFormElementType="select" currentValue="#identityProvider.getOnCreationPortalPersonApprovalStatus()#" fieldName="onCreationPortalPersonApprovalStatus" label="phr_SAML_onCreationPortalApprovalStatus" query="#application.com.flag.getFlagGroupFlags('PerApprovalStatus')#" value="FLAGTEXTID" display="TRANSLATEDNAME" >
			</div>
			
		</div>

		<div class="partnerCloudOptions"
			<cfif not internalOnlyIDP>style="display: none;"</cfif>
			>
		<CF_relayFormElementDisplay relayFormElementType="radio" currentValue="#identityProvider.isPermittedToSetUsergroups()#" fieldName="PermittedToSetUsergroups" label="phr_saml_allowAddUsergroups" list="1#application.delim1#phr_yes,0#application.delim1#phr_no">


		<div id="usergroupControl" class="grey-box-top"
			<cfif not identityProvider.isPermittedToSetUsergroups()>style="display: none;"</cfif>
		>
			<H2>phr_SAML_usergroupControl</H2>
			<CF_relayFormElementDisplay relayFormElementType="text" currentValue="#identityProvider.getAssertionAttributeToControlUsergroups()#" fieldName="assertionAttributeToControlUsergroups" label="phr_SAML_assertionAttributeToControlUsergroups">
			<CF_relayFormElementDisplay relayFormElementType="select" multiple="true" currentValue="#arrayToList(identityProvider.getUsergroupsPermittedToControl())#" fieldName="usergroupsPermittedToControl" label="phr_SAML_ControlledUsergroups"  query="#application.com.relayUserGroup.getUserGroupsForSelect(userGroupIDsToExclude=4, userGroupTypesToExclude='personal')#" value="usergroupid" display="name">
				
		</div>
			
		<CF_relayFormElementDisplay relayFormElementType="radio" currentValue="#identityProvider.isPermittedToControlCountryRights()#" fieldName="PermittedToControlCountryRights" label="phr_saml_allowControlLanguageRights" list="1#application.delim1#phr_yes,0#application.delim1#phr_no">
			
		<div id="countryControl" class="grey-box-top"
			<cfif not identityProvider.isPermittedToControlcountryRights()>style="display: none;"</cfif>
		>
			<H2>phr_SAML_countryControl</H2>
			<CF_relayFormElementDisplay relayFormElementType="text" currentValue="#identityProvider.getAssertionAttributeToControlCountryRights()#" fieldName="assertionAttributeToControlCountryRights" label="phr_SAML_assertionAttributeToControlCountryRights">

		</div>
		</div>

		<!--- This should be replaced by a CF_relayFormElementDisplay when there is time RJT --->

        <div class="form-group">
            <label for="certificateToUse">phr_SAML_certificateToUse</label>
			<select id="certificateToUse" name="certificateToUse" class="form-control">
				<cfoutput><option <cfif identityProvider.getCertificateToUse() eq ""> selected="selected" </cfif> value=""></option></cfoutput>
				<cfloop array="#certificateManager.getAllAliases()#" index= 'certificateAlias'>
					<cfoutput><option value="#certificateAlias#" <cfif identityProvider.getCertificateToUse() eq certificateAlias> selected="selected" </cfif> >#certificateAlias#</option></cfoutput>
				</cfloop>
			</select>
		</div>
        <CF_relayFormElementDisplay relayFormElementType="radio" currentValue="#identityProvider.isPermittedToInitiateSAML()#" fieldName="permittedToInitiateSAML" label="phr_SAML_isPermittedToInitiateSAML" list="1#application.delim1#phr_yes,0#application.delim1#phr_no">

	</cf_relayFormDisplay>
</form>

