<cfset application.com.request.setTopHead(pageTitle = "phr_saml_SAMLIdentityProviders_title", topHeadCfm="/relay/singleSignOn/SAML/SP/editor/SAMLSPTopNav.cfm", showSave="false", showDelete="false")>


<cfscript>
	certificateManager=new singleSignOn.SAML.IDP.CertificateManager();


	application.com.saml.checkAndWarnIfIsSecureMisconfigured();

</cfscript>

<cfif structKeyExists(form,"siteDomain")>
	<!--- For the metadata display --->
	<cfinclude template="metadataDisplay.cfm" >
</cfif>


<div class="internalBodyPadding">

	<form role="form" method="post">


			<div class="form-group">
				<label for="certificatealias">phr_saml_certificate_to_use</label>
			<select id="certificatealias" name="certificatealias" class="form-control">
			<cfoutput><option value=""></option></cfoutput>
			<cfloop array="#certificateManager.getAllAliases()#" index='certificateAlias'>
				<cfoutput><option value="#certificateAlias#">#certificateAlias#</option></cfoutput>

			</cfloop>
			</select>
			</div>

		  <div class="form-group">
		    <label for="siteDomain">phr_portal</label>
		    <select id="siteDomain" name="siteDomain"  class="form-control">
				<cfloop query="#application.com.relayelementtree.getSitesAndTopElements()#">
					<cfoutput><option value="#siteDomain#">#siteDomain#</option></cfoutput>
				</cfloop>
				<cfoutput><option value="#request.currentsite.DOMAIN#">#request.currentsite.DOMAIN#</option></cfoutput> <!--- Put in internal as well --->
			</select>
		  </div>
		  <button type="submit" class="btn btn-primary">phr_Sys_Download</button>
		
	</form>

</div>
