<!--- This is the end point identity providers forward people to with a valid assertion. The actually validation of the assertion happens in an authentication filter --->
<cfparam name="form.relayState" default="">




<cfif request.relayCurrentUser.isLoggedIn>

	<cfif form.relayState.startsWith("eid")>
		<cfset form.relayState="?" & form.relayState>
	</cfif>

	<cfif not form.relayState.startsWith("/")>
		<cfset form.relayState="/" & form.relayState>
	</cfif>
	
	<cflocation url="#form.relayState#" AddToken = false >
		

<cfelse>
	<cfif request.currentSite.isInternal>
		<cf_head>
			<cf_title><cfoutput>#request.CurrentSite.Title#</cfoutput> - Login</cf_title>
			<cf_includeCssOnce template = "/styles/login.css">
		</cf_head>
		
		<div id="outerDiv">
			<div id="loginDiv" class="row">
				<div id="loginLeftCol" class="col-xs-12 col-sm-6">
					<div id="loginOuterDiv">
						<div id="loginInputDiv" style="text-align: center;">
							<cfoutput>
								phr_SAML_singleSignOnFailed <cfif structKeyExists(request,"SAMLError")>(#request.SAMLError#)</cfif>
							</cfoutput>
						</div>
					</div>
				</div>
			<div id="loginRightCol" class="col-sm-6 hidden-xs">
				<img src="/images/login/partnerUp.png">
			</div>
		</div>
		<footer id="loginFooter" role="footer">
			<ul>
				<li id="footerLogo"><a href="http://www.relayware.com"><img src="/images/login/logo.png"></a></li>
			</ul>
		</footer>
	<cfelse>
		<cf_displayBorder>
			<cfoutput>
				phr_SAML_singleSignOnFailed <cfif structKeyExists(request,"SAMLError")>(#request.SAMLError#)</cfif>
			</cfoutput>
		</cf_displayBorder>
	</cfif>
</cfif>
