/**
 * SAMLAuthenticationFilterPlugin
 *
 * @author Richard.Tingle
 * @date 03/05/16
 **/
component accessors=true output=false persistent=false {


	public struct function runFilter(required numeric personID, required boolean isInternal, required numeric level) output="false" {
		 var result = {PersonID = personID, Level = Level, filterUsed=false};

		 if (isDefined("form") and structKeyExists (form,"samlResponse") and Level LT 2){
		 	param name="form.relayState" default="";

		 	var idpManager=application.javaloader.create("singleSignOn.samlserviceprovider.idps.IDPManager").init();
			var certificateProvider=application.javaloader.create("singleSignOn.samlserviceprovider.idps.IDPManagerBasedCertificateProvider").init(idpManager);
			var validator=application.javaloader.create("singleSignOn.samlserviceprovider.SAMLAssertionValidator")
					.init(certificateProvider, 
							application.com.settings.getSetting("SAML.SP.allowedTimeMismatch_minutes"), 
							application.com.settings.getSetting("SAML.SP.allowUnsignedResponses"));

			var base64DecodedResponseString=toString(ToBinary(form.SamlResponse), "utf-8");

			var requestIds = ArrayNew(1);

			lock type="Exclusive" scope="Session" timeout="20" {
				if(StructKeyExists(Session, "authnRequestIds")){
					requestIds = Session.authnRequestIds;
				}
			}

			var validationReport=validator.validate(base64DecodedResponseString, requestIds);

			if (validationReport.isValid()){
				var assertionInterpretor=application.javaloader.create("singleSignOn.samlserviceprovider.SAMLAssertionInterpretor").init();

				var responseStringPreprocessor=application.javaloader.create("singleSignOn.samlserviceprovider.ResponseStringPreprocessor").init();


				var assertionContentReport=assertionInterpretor.interpret(responseStringPreprocessor.stringToResponse(base64DecodedResponseString));

				var issuer=assertionContentReport.getIssuer();

				var identityProvider=idpManager.getIDPByEntityID(issuer);

				if(not identityProvider.isActive()){
					request.SAMLError=application.com.ErrorHandler.recordRelayError_Warning(Severity="INFORMATION", message="The identity provider #issuer# is not currently active and cannot be used for authenticaton", Type="SAML");
					return  result;	
				}
				
				//check the identity provider is authorised to authenticate this site
				if (listFind(identityProvider.getAuthorisedSitesToAuthenticate_asStringList(),request.currentSite.siteDefID) EQ 0 ){
					request.SAMLError=application.com.ErrorHandler.recordRelayError_Warning(Severity="INFORMATION", message="The identity provider #issuer# was not authorised to auhtenticate the current site; sitedefid: #request.currentSite.siteDefID#. As a result authentication failed ", Type="SAML");
					return  result;	
				}
				
				var fieldUsedAsNameID_foreign=identityProvider.getNameIDField_Foreign(); //should be "" to indicate the nameID is used
				var fieldUsedAsNameID_relayware=identityProvider.getNameIDField_Relayware();

				var nameID="";
				if (fieldUsedAsNameID_foreign EQ ""){
					nameID=assertionContentReport.getNameID();
				}else{
					if (assertionContentReport.hasAttribute(fieldUsedAsNameID_foreign)){
						nameID=assertionContentReport.getAttributeSingle(fieldUsedAsNameID_foreign);
					}else{
						request.SAMLError=application.com.ErrorHandler.recordRelayError_Warning(Severity="INFORMATION", message="#fieldUsedAsNameID_foreign# has been set to be used instead of the NameID, however it is not present in the assertion", Type="SAML");
						return  result;
					}
				}

				//can't use relayCurrent user as too early in the request
				var personQuery=new Query();
				personQuery.setDatasource(application.siteDatasource);
				personQuery.addParam(name="nameID",value=nameID,cfsqltype="CF_SQL_VARCHAR");

				var queryResponse = personQuery.execute(sql="select personID from vPerson where #application.com.security.queryObjectName(fieldUsedAsNameID_relayware)# = :nameID");


				var prefix=queryResponse.getPrefix();

				if (prefix.recordCount GT 1){
					request.SAMLError=application.com.ErrorHandler.recordRelayError_Warning(Severity="INFORMATION", message="The nameID match #nameID# for #fieldUsedAsNameID_relayware# matched #prefix.recordCount# people so the SSO failed", Type="SAML");
				}else if (prefix.recordCount EQ 1){
					var queryResult=queryResponse.getResult();
					result.personID=queryResult.personID;
					result.filterUsed=true;
					result.level=2;
					result.source="SAML:#issuer#"; //this just adds an entry to the usage table to register that this is how the person got logged in
				}else{
					var creationReport=autocreatePersonIfPossible(assertionContentReport,identityProvider);

					if (creationReport.created){
						result.personID=creationReport.personID;
						result.filterUsed=true;
						result.level=2;
					}

				}

				//if we have a person set up we then start to handle usrgroups (which existing people can (concievably))
				if (result.level EQ 2){
					if (request.currentSite.isInternal){
					manageUsergroupsIfPermitted(assertionContentReport,identityProvider,result.personID);
					manageCountryRightsIfPermitted(assertionContentReport,identityProvider,result.personID);
					}

					//this doesn't actually prevent access (something later does that), it just adds a helpful error message
					if (not application.com.login.isPersonAValidUserOnTheCurrentSite(result.personID).isValidUser){
						request.SAMLError=application.com.ErrorHandler.recordRelayError_Warning(Severity="INFORMATION", message="Although SAML identified or created a person (person ID #result.personID#) that person was not a valid user on the site they are attempting to log in to", Type="SAML");
					}

				}


			}else{
				request.SAMLError=application.com.ErrorHandler.recordRelayError_Warning(Severity="INFORMATION", message="SAML Assertion failed validation : #validationReport.getMessage()#", Type="SAML");
			}
		 }

		 return result;
	}

	private void function manageCountryRightsIfPermitted(required any assertionContentReport, required any identityProvider, required numeric personID){
		if (identityProvider.isPermittedToControlCountryRights()){
			var countryRightsToSetAttribute=identityProvider.getAssertionAttributeToControlCountryRights();

			if (assertionContentReport.hasAttribute(countryRightsToSetAttribute)){
				var countryRightsIsoCodes=assertionContentReport.getAttributeSingle(countryRightsToSetAttribute);
				var countryRightsIDs=application.com.relayCountries.convertISOCodesToCountryIDs(countryRightsIsoCodes);

				application.com.relayCountries.updateInternalUserCountryRights(internalpersonID=personID,countryIDList=countryRightsIDs);

			}else{
				request.SAMLError=application.com.ErrorHandler.recordRelayError_Warning(Severity="INFORMATION", message="Wasn't able to update countryRights as the attribute (#countryRightsToSetAttribute#) was missing", Type="SAML");
			}
		}
	}


	private void function manageUsergroupsIfPermitted(required any assertionContentReport, required any identityProvider, required numeric personID){
		if (identityProvider.isPermittedToSetUsergroups()){
			var managedUsergroups=identityProvider.getUsergroupsPermittedToControl();

			var usergroupsToSetAttribute=identityProvider.getAssertionAttributeToControlUsergroups();

			if (assertionContentReport.hasAttribute(usergroupsToSetAttribute)){
				var usergroupsToSet_mayBeNames=assertionContentReport.getAttributeSingle(usergroupsToSetAttribute);

				var usergroupIDsToSet="";
				//check if the usergroups are already IDs
				if (len(usergroupsToSet_mayBeNames) gt 0 and isNumeric(ListGetAt(usergroupsToSet_mayBeNames, 1)) ){
					usergroupIDsToSet=usergroupsToSet_mayBeNames;
				}else{
					usergroupIDsToSet=application.com.relayUserGroup.convertUsergroupNamesToUsergroupIDs(usergroupsToSet_mayBeNames);
				}

				application.com.relayUserGroup.updatePersonsUserGroups(personID=personID,userGroupIDList=usergroupIDsToSet,userGroupIDListToManage=ArrayToList(identityProvider.getUsergroupsPermittedToControl()));

			}else{
				request.SAMLError=application.com.ErrorHandler.recordRelayError_Warning(Severity="INFORMATION", message="Wasn't able to update usergroups as the attribute (#usergroupsToSetAttribute#) was missing", Type="SAML");
			}


		}
	}

	private struct function autocreatePersonIfPossible(required any assertionContentReport, required any identityProvider){
		var result={created=false, personID=0};


		if (not identityProvider.getAllowAutoCreate()){
			//we're not even allowed to autocreate
			request.SAMLError=application.com.ErrorHandler.recordRelayError_Warning(Severity="INFORMATION", message="Autocreation of person is not permitted for this Identity Provider and no person matched the nameID", Type="SAML");


			return result;
		}

		var locationID=0;
		if (identityProvider.getAutoCreate_SelectLocOrg()){
			//we'll have a single org and loc where all autocreated people go, nice and easy
			locationID=identityProvider.getAutoCreate_selectedLocationID();
		}else{
			var locationDetection_RelayareSide_parameter=identityProvider.getAutoCreate_detectedLoc_RelaywareField();
			var locationDetection_ForeignSide_parameter=identityProvider.getAutoCreate_detectedLoc_ForeignField();

			if (assertionContentReport.hasAttribute(locationDetection_ForeignSide_parameter)){

				var matchCondition=application.com.security.queryObjectName(locationDetection_RelayareSide_parameter) & " = " & application.com.security.queryparam(CFSQLType="CF_SQL_VARCHAR", value=assertionContentReport.getAttributeSingle(locationDetection_ForeignSide_parameter) );
				var relayEntityReport=application.com.relayEntity.getEntity(EntityTypeID=application.entityTypeID.location, whereClause=matchCondition, testLoggedInUserRights="false", fieldList="locationID");

				if (relayEntityReport.recordCount EQ 1){
					locationID=relayEntityReport.recordSet.locationID;
				}else{
					request.SAMLError=application.com.ErrorHandler.recordRelayError_Warning(Severity="INFORMATION", message="SAML autocreate expected 1 matching location but found #relayEntityReport.recordCount#. Match condition was #matchCondition#", Type="SAML");
					return result;
				}

			}else{
				request.SAMLError=application.com.ErrorHandler.recordRelayError_Warning(Severity="INFORMATION", message="SAML could not create a person as the assertion did not have a '#locationDetection_ForeignSide_parameter#' attribute", Type="SAML");
				return result;
			}


		}

		//we now know where we are putting people, we just need their actual data from the assertion

		var mappings=identityProvider.getMappings();

		var personDetails=structNew();

		personDetails.locationID=locationID;
		
		//only create user as an internal user if this is internal
		if (request.currentSite.isInternal){
		personDetails.loginExpires= DateAdd("yyyy",50,request.requestTimeODBC);
		}
			

		//we map the nameID by default. It may end up being overrided by an explicit mapping
		var nameID=identityProvider.getNameIDField_Foreign() EQ "" 
									? 
									assertionContentReport.getNameID()
									:
									assertionContentReport.getAttributeSingle(identityProvider.getNameIDField_Foreign());
		personDetails[identityProvider.getNameIDField_Relayware()]=nameID;
		
		for (mapping in mappings){
			if (mapping.isUseAssertion()){
				var attributeName=mapping.getForeignValue();
				
				if (attributeName EQ "nameID"){ //we can map fields to the nameID
					personDetails[mapping.getRelaywareField()]=assertionContentReport.getNameID();
				}else if (assertionContentReport.hasAttribute(attributeName)){
					personDetails[mapping.getRelaywareField()]=assertionContentReport.getAttributeSingle(attributeName);
				}else{
					request.SAMLError=application.com.ErrorHandler.recordRelayError_Warning(Severity="INFORMATION", message="SAML could not create a person as the assertion did not have a '#attributeName#' attribute, used for mapping the data of newly created people.", Type="SAML");
					return result;
				}

			}else{
				personDetails[mapping.getRelaywareField()]=mapping.getForeignValue(); //its a hard coded value
			}
		}

		var personCreationReport=application.com.relayEntity.insertPerson(personDetails=personDetails,setPassword="true",testLoggedInUserRights=false);

		if (personCreationReport.isOk){
			result.created=true;
			result.personID=personCreationReport.entityID;

			//only create user as an internal user if this is internal
			if (request.currentSite.isInternal){
			
			//apply any at creation usergroups. Note that the IDP may also be allowed to manage usergroups on an ongoing basis. That isn't handled here
			application.com.login.setPersonAsInternalUser(personID=result.personID, loginExpiresDate=DateAdd("yyyy",50,request.requestTimeODBC), countryIDList=ArrayToList(identityProvider.getUsergroupCountriesAtPersonCreation()));
				

			var usergroupsToSet=identityProvider.getUsergroupsAtPersonCreation();
				

			for(var i=1;i<=arrayLen(usergroupsToSet);i++){
				application.com.relayUserGroup.addPersonToUserGroup(personID=result.personID,userGroup=usergroupsToSet[i]);
			}
				

			}else{
				//if on the portal it autoapproves the person
				application.com.flag.setbooleanflag(entityid=personCreationReport.entityID, flagtextid=identityProvider.getOnCreationPortalPersonApprovalStatus(), deleteOtherRadiosInGroup="true");
			}

			return result;

		}else{
			var errorStruct={};

			errorStruct.personDetails=personDetails;
			errorStruct.personCreationReport=personCreationReport;
			request.SAMLError=application.com.ErrorHandler.recordRelayError_Warning(Severity="INFORMATION", message="SAML Autocreation failed. See WarningStructure for details", WarningStructure=errorStruct);
			return result;
		}



	}

}