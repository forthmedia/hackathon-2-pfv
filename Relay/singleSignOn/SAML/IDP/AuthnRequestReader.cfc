/**
 * AuthnRequestReader
 * 
 * @author Richard.Tingle
 * @date 23/11/15
 * 
 * Change Date	Initials	Message
 * 2016/04/20	RJT			Changed XML search to use local name only as namespace doesn't have to be samlp (in fact it is often saml2p)
 * 
 **/
component accessors=true output=false persistent=false {


	public struct function extractDataFromRequest(required string authnRequestString) output="false" {

		var SAMLRequest=xmlparse(authnRequestString);
		var results={requestIssuer="",authnrequestMessageID="", requestedProtocal="", issueInstantISOString="", issueInstant="" };
		
		
		var xmlNode ="";


		/* Check that saml and samlp matches and also the version */
		/* Get Issuer as spUniqueID */
		xmlNode = xmlSearch(SAMLRequest, '/*[local-name()="AuthnRequest"]/*[local-name()="Issuer"]'); /* this comment is just to correct a syntax highlighting bug*/
		if (ArrayLen(xmlNode) gt 0) {
			results.requestIssuer = xmlNode[1]['xmlText'];
		}
		xmlNode = xmlSearch(SAMLRequest, '/*[local-name()="AuthnRequest"]/@ID'); /* this comment is just to correct a syntax highlighting bug*/
		if (ArrayLen(xmlNode) gt 0) {
			results.authnrequestMessageID = xmlNode[1]['xmlValue'];
		}
		xmlNode = xmlSearch(SAMLRequest, '/*[local-name()="AuthnRequest"]/@ProtocolBinding'); /* this comment is just to correct a syntax highlighting bug*/
		if (ArrayLen(xmlNode) gt 0) {
			results.requestedProtocal = xmlNode[1]['xmlValue'];
		}
		xmlNode = xmlSearch(SAMLRequest, '/*[local-name()="AuthnRequest"]/@IssueInstant'); /* this comment is just to correct a syntax highlighting bug*/
		if (ArrayLen(xmlNode) gt 0) {
			results.issueInstantISOString = xmlNode[1]['xmlValue'];
		}

		return results;

	}
	

	
}