/**
 * ServiceProviderManager
 * 
 * @author Richard.Tingle
 * @date 18/11/15
 **/
component accessors=true output=false persistent=false {


	public array function getAllServiceProviders() output="false" {
		/* TODO: Implement Method */
		var allServiceProviderIDs=application.com.relayEntity.getEntity(entityTypeID=application.entityTypeID.SAMLServiceProvider,fieldList="ServiceProviderIdentifier");
		var allServiceProviders=arrayNew(1);
		
		for(var i=1;i<=allServiceProviderIDs.recordCount;i=i+1){
			allServiceProviders[i]=new SamlServiceProvider(allServiceProviderIDs.recordSet.ServiceProviderIdentifier[i]);
			allServiceProviders[i].pullFromDatabase();
		}
		
		return allServiceProviders;
	}
	
	public query function getValidValuesForServiceProvider() validvalues="true"{
		
		var serviceProviders=getAllServiceProviders();
		
		
		
		var validValuesQuery=queryNew("dataValue,displayValue","Varchar,Varchar");
		
		for(var i=1;i<=arrayLen(serviceProviders); i=i+1){
			QueryAddRow(validValuesQuery);
			validValuesQuery["dataValue"][i]=serviceProviders[i].getServiceProviderIdentifier();
			validValuesQuery["displayValue"][i]=serviceProviders[i].getServiceProviderName(); 

		}
		


		
		return validValuesQuery;
	}

}