<cfset application.com.request.setTopHead(pageTitle="phr_saml_SAML_Identity_Provider - phr_saml_metadata", queryString = "",topHeadCfm="/relay/singleSignOn/SAML/IDP/editor/SAMLIDPTopNav.cfm")>

<cfscript>
	certificateManager=new singleSignOn.SAML.IDP.CertificateManager();
	if (structKeyExists(form,"deleteCertificate")){
		
		if (certificateManager.certificateCanBeDeleted(form.certificateAlias)){
			certificateManager.deleteCertificate(form.certificateAlias);
		}else{
				
		}
		
	}
	
	
	application.com.saml.checkAndWarnIfIsSecureMisconfigured();
	
</cfscript>

<cfif structKeyExists(form,"certificatealias")>
	<!--- For the metadata display --->
	<cfset siteName=form.portal>
	<cfset certificateBase64String= certificateManager.getBase64Certificate(form.certificatealias)>
	<cfset certificateExpiryDate_isoFormat = certificateManager.getCertificateExpiry_IsoTimeString(form.certificatealias)>
	<cfinclude template="metadataDisplay.cfm" >
</cfif>


<div class="internalBodyPadding">

	<form role="form" method="post">

		  <div class="form-group">
		    <label for="certificatealias">phr_saml_certificate_to_use</label>
		    <select id="certificatealias" name="certificatealias" class="form-control">
				<cfloop array="#certificateManager.getAllAliases()#" index='certificateAlias'>
					<cfoutput><option value="#certificateAlias#">#certificateAlias#</option></cfoutput>
				
				</cfloop>
			</select>
		  </div>
		  <div class="form-group">
		    <label for="portal">phr_portal</label>
		    <select id="portal" name="portal"  class="form-control">
				<cfloop query="#application.com.relayelementtree.getSitesAndTopElements()#">
					<cfoutput><option value="#siteDomain#">#siteDomain#</option>
					<!-- #application.com.relaycurrentSite.isSiteSecure(siteDomain)# --></cfoutput>
				</cfloop>
			</select>
		  </div>
		  <button type="submit" class="btn btn-primary">phr_Sys_Download</button>
		
	</form>

</div>
