
<cfset application.com.request.setTopHead(pageTitle="phr_saml_SAML_Identity_Provider - phr_saml_Service_Provider_Settings")>

<cfset serviceProviderManager=createObject("component","Relay.singleSignOn.SAML.IDP.ServiceProviderManager")>

<!---Process deletions and activates/deactivates --->


<cfif structKeyExists(form,"deleteClient")>
	

	<cfset serviceProvider=new singleSignOn.SAML.IDP.SAMLServiceProvider(form.deleteClientID)>
	
	<cfset serviceProvider.delete()>
</cfif>



<div class="internalBodyPadding">
	
	<p>phr_saml_service_Provider_explanation</p>

	
	<div class="entry">
		<form action="" id="create" method="post" >
			<cf_input type="hidden" name="ServiceProviderToEdit" value="">
			<cf_input type="submit" id="createScope" value="phr_saml_create_service_provider_explanation" />
		</form>
			
	</div>
	<h2>Manage Existing Client Applications</h2>
	<table id="existingClients" class="display responsiveTable table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>phr_saml_Service_Provider_Identifier</th>
	            <th>phr_saml_Service_Provider_name</th>
	            <th>phr_saml_Assertion_Consumer_URL</th>
	            <th>phr_saml_certificate_to_use</th>
				<th>phr_Edit</th>
				<th>phr_delete</th>
			</tr>
		</thead>
	    <tbody>
			<cfset allclients=serviceProviderManager.getAllServiceProviders()>
			<cfset evenRow=false>
			<cfloop array='#allclients#' index='clientObject'>
				<cfset evenRow=not evenRow>
				<tr class="<cfoutput>#evenRow?'evenRow':'oddRow'#</cfoutput>">
					<td><cfoutput>#HTMLEditFormat(clientObject.getServiceProviderIdentifier())#</cfoutput></td>
					<td><cfoutput>#HTMLEditFormat(clientObject.getServiceProviderName())#</cfoutput></td>
					<td><cfoutput>#HTMLEditFormat(clientObject.getAssertionConsumerURL())#</cfoutput></td>
					<td><cfoutput>#HTMLEditFormat(clientObject.getCertificateToUse())#</cfoutput></td>
					<td align="center" valign="top">
						<form action="" id="toggleActivation" method="post" >
							<cf_input type="hidden" name="ServiceProviderToEdit" value="#clientObject.getServiceProviderIdentifier()#">
							<input type="image" src="/images/MISC/iconProfile.gif" height="16"/>
							
						</form>
					</td>
					<td  align="center" valign="top">
						<form action="" id="deleteClient" method="post" >


							<cf_input type="hidden" name="deleteClientID" value="#clientObject.getServiceProviderIdentifier()#">
							<cf_input type="hidden" name="deleteClient" value="true">
							<input type="image" src="/images/MISC/iconDeleteCross.gif" onclick="return confirm('phr_areYouSureYouWantToDelete')"  height="16"/>
						</form>
					</td>
					
				</tr>
			</cfloop>
	    </tbody>
	</table>

</div>



