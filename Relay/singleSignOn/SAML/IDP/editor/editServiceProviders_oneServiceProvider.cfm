<cfparam name="form.ServiceProviderToEdit">
<cfparam name="newServiceProvider" default="#len(form.ServiceProviderToEdit) EQ 0 #">
 

<cfset application.com.request.setTopHead(pageTitle="SAML Identity Provider - Service Provider Settings")>

<cfscript>
	function convertArrayToOrderedStruct(arrayToConvert) {
	    orderedStruct = createObject("java", "java.util.LinkedHashMap").init();

	    for(i=1;i<=arrayLen(arrayToConvert);i=i+1){

	    	orderedStruct[arrayToConvert[i]]=arrayToConvert[i];
	    }
	    return orderedStruct;
	}
	function deleteArrayFromArray(startArray,arrayToDelete) {
	    newArray=ArrayNew(1);

	    for(i=1;i<=arrayLen(startArray);i=i+1){
	    	if (NOT ArrayContains(arrayToDelete,startArray[i])){
	    		arrayAppend(newArray,startArray[i]);
	    	}
	    }
	    return newArray;
	}

	samlAttributeManager=new  singleSignOn.common.AttributeManager();

	certificateManager=new singleSignOn.SAML.IDP.CertificateManager();

	editorBackingBean=new singleSignOn.common.editorBackingBean();

	serviceProvider=new singleSignOn.SAML.IDP.SAMLServiceProvider(form.ServiceProviderToEdit);

	if (form.ServiceProviderToEdit NEQ ""){
		serviceProvider.pullFromDatabase();
	}

	personFields=samlAttributeManager.getPotentialAttributesForEntity(entityTypeID=application.entityTypeID.person);
	
	locationFields=samlAttributeManager.getPotentialAttributesForEntity(entityTypeID=application.entityTypeID.location);
	
	organisationFields=samlAttributeManager.getPotentialAttributesForEntity(entityTypeID=application.entityTypeID.organisation);	


	//the save behaviour on the form submit
	if (structKeyExists(form,"serviceProviderName")){
		//only fields currently selected or fields the user has access to are allowed to be added

		//selects are blank if nothing is selected
		param name="form.selectedPersonFields" default=""; 
		param name="form.selectedLocationFields" default=""; 
		param name="form.selectedOrganisationFields" default=""; 

		approvedPersonFields=arrayNew(1);
		approvedPersonFields.addAll(personFields);
		approvedPersonFields.addAll(serviceProvider.getPersonAttributes());

		serviceProvider.setPersonAttributes(editorBackingBean.validateScopeProfiles(listToArray(form.selectedPersonFields),approvedPersonFields).allowedFields);


		serviceProvider.setServiceProviderName(form.serviceProviderName);
		serviceProvider.setAssertionConsumerURL(form.assertionConsumerURL);
		serviceProvider.setNameIDField(arrayToList(editorBackingBean.validateScopeProfiles(listToArray(form.nameIDField),approvedPersonFields).allowedFields));


		//only fields currently selected or fields the user has access to are allowed to be added
		approvedLocationFields=arrayNew(1);
		approvedLocationFields.addAll(locationFields);
		approvedLocationFields.addAll(serviceProvider.getLocationAttributes());

		serviceProvider.setLocationAttributes(editorBackingBean.validateScopeProfiles(listToArray(form.selectedLocationFields),approvedLocationFields).allowedFields);

		//only fields currently selected or fields the user has access to are allowed to be added
		approvedOrganisationFields=arrayNew(1);
		approvedOrganisationFields.addAll(organisationFields);
		approvedOrganisationFields.addAll(serviceProvider.getOrganisationAttributes());

		serviceProvider.setOrganisationAttributes(editorBackingBean.validateScopeProfiles(listToArray(form.selectedOrganisationFields),approvedOrganisationFields).allowedFields);

		serviceProvider.setCertificateToUse(form.certificateToUse);
		serviceProvider.pushToDatabase();
		
		form.ServiceProviderToEdit=serviceProvider.getServiceProviderIdentifier(); //relevant when we've just autocreated a serviceprovider identifier
		newServiceProvider=false; //its in the database and no longer new
		
		application.com.relayui.setMessage(message="phr_saml_service_provider_saved");
	}


</cfscript>

<cfoutput>





	<div class="internalBodyPadding">
		<cfif arrayLen(certificateManager.getAllAliases()) EQ 0>
			<p>phr_saml_No_Certificate_explanation</p>

		<cfelse>
			<cf_includeJavascriptOnce template = "/singleSignOn/SAML/js/processSAMLMetaData.js">

			<cfif newServiceProvider>
				<p>The below Service Provider information can be populated manually or some fields can be autofilled by uploading the Service Provider Metadata XML</p>

				<form>
					<!---Because of the inconsistent way browsers implement <input type="file" ...> we "fake it" to allow us to have whatever styling we like --->
					  <div class="form-group">
					  	 <input id="metadataUploadButton" type="button" class="btn" value="Populated From Meta Data" onclick="document.getElementById('metadataUpload').click();" />
					  </div>

					  <div hidden="hidden" style="display: none;">
							<input id="metadataUpload" type="file"/>
					  </div>

				</form>
			<cfelse>
				<br>
			</cfif>
			<form method="post" role="form">

				  <div class="form-group">
				    <label for="ServiceProviderToEdit">phr_saml_Service_Provider_Identifier</label>
				    <cfif newServiceProvider>
					  <input type="text" class="form-control" name="ServiceProviderToEdit" id="ServiceProviderToEdit" placeholder="Leave Blank To Autogenerate (Autogeneration only appropriate for IDP initiated SSO)">

					<cfelse>
						<input type="hidden" class="form-control" name="ServiceProviderToEdit" id="hiddenServiceProviderToEdit" value="#form.ServiceProviderToEdit#">
						<input type="text" disabled="disabled" class="form-control"  id="ServiceProviderToEdit" value="#form.ServiceProviderToEdit#">

					</cfif>
				  </div>
				  <div class="form-group">
				    <label class="required" for="serviceProviderName">phr_saml_Service_Provider_name</label>
				    <input type="text" class="form-control" name="serviceProviderName" id="serviceProviderName" placeholder="Service Provider Identifier" required="required" value="#serviceProvider.getServiceProviderName()#">
				  </div>
				  <div class="form-group">
				    <label class="required" for="assertionConsumerURL">phr_saml_Assertion_Consumer_URL</label>
				    <input type="text" class="form-control" name="assertionConsumerURL" id="assertionConsumerURL" placeholder="Where a partner is sent after being logged in on Relayware" required="required"  value="#serviceProvider.getAssertionConsumerURL()#">
				  </div>

				   <div class="form-group">
				    <label for="nameIDField">phr_saml_Name_ID</label>
				    <select id="nameIDField" name="nameIDField" class="form-control" value="#serviceProvider.getNameIDField()#">
						<cfloop array="#personFields#" index="potentialNameID">
							<cfoutput><option  <cfif serviceProvider.getNameIDField() EQ potentialNameID>  selected="selected" </cfif> value="#potentialNameID#">#potentialNameID#</option></cfoutput>
						</cfloop>
					</select>
				  </div>
				  <div class="form-group">
				    <label for="certificateToUse">phr_saml_certificate_to_use</label>
				    <select id="certificateToUse" name="certificateToUse"  class="form-control">
						<cfloop array="#certificateManager.getAllAliases()#" index="certificateAlias">

							<cfoutput><option  <cfif serviceProvider.getCertificateToUse() EQ certificateAlias> selected="selected" </cfif> value="#certificateAlias#">#certificateAlias#</option></cfoutput>
						</cfloop>
					</select>
				  </div>

				<div class="form-group">
					
					
				    <label for="selectedPersonFields">phr_saml_person_Fields_In_Assertion</label>

				    <CF_TwoSelectsComboStruct
				    NAMENotSelected="UnselectedPersonFields"
				    NAMESelected="selectedPersonFields"
				    SIZE="12"
				    WIDTH="400px"
					FORCEWIDTH="400"
				    structSelected="#convertArrayToOrderedStruct(serviceProvider.getPersonAttributes())#"
					structNotSelected="#convertArrayToOrderedStruct(deleteArrayFromArray(personFields,serviceProvider.getPersonAttributes()))#"
				    CaptionNotSelected="<FONT SIZE=-1><B>phr_saml_Person_Attributes_Potential</B></FONT>"
				    CaptionSelected="<FONT SIZE=-1><B>phr_saml_Person_Attributes_Current</B></FONT>"
					UP="No"
					DOWN="No"
					FORMNAME="edit"
					>
				 </div>
				
				<!--- location Fields--->
				<div class="form-group">
					 <label for="selectedLocationFields">phr_saml_Location_Fields_In_Assertion</label>
					<CF_TwoSelectsComboStruct
					    NAMENotSelected="UnselectedLocationFields"
					    NAMESelected="selectedLocationFields"
					    SIZE="12"
					    WIDTH="400px"
						FORCEWIDTH="400"
					    structSelected="#convertArrayToOrderedStruct(serviceProvider.getLocationAttributes())#"
						structNotSelected="#convertArrayToOrderedStruct(deleteArrayFromArray(LocationFields,serviceProvider.getLocationAttributes()))#"
					    CaptionNotSelected="<FONT SIZE=-1><B>saml_location_Attributes_Potential</B></FONT>"
					    CaptionSelected="<FONT SIZE=-1><B>saml_location_Attributes_Current</B></FONT>"
						UP="No"
						DOWN="No"
						FORMNAME="edit"

						>
				</div>

				<!--- organisation Fields--->
				<div class="form-group">
					 <label for="selectedOrganisationFields">phr_saml_Organisation_Fields_In_Assertion</label>
					<CF_TwoSelectsComboStruct
						    NAME="UnselectedOrganisationFields"
						    NAME2="selectedOrganisationFields"
						    SIZE="12"
						    WIDTH="400px"
							FORCEWIDTH="400"
						    structSelected="#convertArrayToOrderedStruct(serviceProvider.getOrganisationAttributes())#"
							structNotSelected="#convertArrayToOrderedStruct(deleteArrayFromArray(OrganisationFields,serviceProvider.getOrganisationAttributes()))#"
						    Caption="<FONT SIZE=-1><B>saml_organisation_Attributes_Potential</B></FONT>"
						    Caption2="<FONT SIZE=-1><B>saml_organisation_Attributes_Current</B></FONT>"
							UP="No"
							DOWN="No"
							FORMNAME="edit"
							>
				</div>
					




				<button type="submit" class="btn btn-primary">Save</button>
			</form>
		</cfif>

	</div>
</cfoutput>

