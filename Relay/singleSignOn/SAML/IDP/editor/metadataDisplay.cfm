<cfsetting showDebugOutput="No"> <!--- Just so debugging doesn't mess up the file on Dev sites --->


<cfparam name="siteName" type="string"> <!--- Requires something like customer.portal.relayware.com --->
<cfparam name="certificateBase64String" type="string">
<cfparam name="certificateExpiryDate_isoFormat" type="String">

<cfset siteName=lcase(siteName)> <!---BF-294 RJT standardise to lower case for consistency --->

<cfif application.com.relaycurrentSite.isSiteSecure(siteName)>
	<cfset portalDomainURL="https://#siteName#">
<cfelse>
	<cfset portalDomainURL="http://#siteName#">
</cfif>



<cfoutput>
<cfxml variable="metadataXML">
    <md:EntityDescriptor	validUntil="#certificateExpiryDate_isoFormat#"
							entityID="#siteName#"
							xmlns:ds="http://www.w3.org/2000/09/xmldsig##"
							xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion"
							xmlns:md="urn:oasis:names:tc:SAML:2.0:metadata">
		<md:IDPSSODescriptor protocolSupportEnumeration="urn:oasis:names:tc:SAML:2.0:protocol">
				<md:KeyDescriptor use="signing">
					<ds:KeyInfo>
						<ds:X509Data>
							<ds:X509Certificate>#certificateBase64String#</ds:X509Certificate>
						</ds:X509Data>
					</ds:KeyInfo>
				</md:KeyDescriptor>
				
				<md:SingleSignOnService 
					Location="#portalDomainURL#/singleSignOn/SAML/IDP/endpoints/idpPost.cfm"
					Binding="urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST"/>
				
		</md:IDPSSODescriptor>
	</md:EntityDescriptor>
</cfxml>
</cfoutput>
 
<cfheader name="Content-Disposition" value="attachment; filename=IDPmetadata.xml" />
<!--- Must all be on one line RJT --->
<cfcontent type="application/xml;charset=utf-8" reset="true"><cfoutput>#toString(metadataXML)#</cfoutput><cfabort>


