/**
 * AssertionCreator
 * 
 * @author Richard.Tingle
 * @date 09/11/15
 **/
component accessors=true output=false persistent=false {

    
    public any function init(any signingCredential){
        this.signingCredential=signingCredential;
        return this;
    }
    
    public any function createAssersion(String nameID,String issuer,struct assertionAttributes,String audianceURI,numeric allowedTimeMismatch_minutes, string recipient){
        

        var DateTimeZoneClass=createObject("java", "org.joda.time.DateTimeZone");
		this.now=createObject("java", "org.joda.time.DateTime").init(DateTimeZoneClass.UTC);

		var AssertionClass=createObject("java", "org.opensaml.saml2.core.Assertion");
		var UUIDClass=createObject("java", "java.util.UUID");
        // Create the assertion
        assertionBuilder = getSAMLBuilder().getBuilder(AssertionClass.DEFAULT_ELEMENT_NAME);
        var assertion =  assertionBuilder.buildObject();
        assertion.setIssuer(createIssuer(issuer));
        assertion.setIssueInstant(this.now);
            
        var SAMLVersionClass=createObject("java", "org.opensaml.common.SAMLVersion");    
                 
        assertion.setVersion(SAMLVersionClass.VERSION_20);
        assertion.setSubject(createSubject(
                                createNameID(nameID),
                                createSubjectConfirmation(createSubjectConfirmationData(allowedTimeMismatch_minutes,recipient))    
                            ));
        assertion.getAuthnStatements().add(createAuthnStatement(allowedTimeMismatch_minutes));
        
        if (!assertionAttributes.isEmpty()){
        	assertion.getAttributeStatements().add(createAttributeStatement(assertionAttributes));
        }
        assertion.setConditions(createConditions(allowedTimeMismatch_minutes,audianceURI));
        assertion.setID("id" & UUIDClass.randomUUID().toString());
        
        
        //the signature
        var ConfigurationClass=createObject("java", "org.opensaml.Configuration");
        var SignatureClass=createObject("java", "org.opensaml.xml.signature.Signature");
        var signature =  ConfigurationClass.getBuilderFactory().getBuilder(SignatureClass.DEFAULT_ELEMENT_NAME)
                .buildObject(SignatureClass.DEFAULT_ELEMENT_NAME);
 
        signature.setSigningCredential(this.signingCredential);
        
        // This is also the default if a null SecurityConfiguration is specified
        secConfig = ConfigurationClass.getGlobalSecurityConfiguration();
        // If null this would result in the default KeyInfoGenerator being used
        var keyInfoGeneratorProfile = "XMLSignature";
        
        var SecurityHelperClass=createObject("java", "org.opensaml.xml.security.SecurityHelper");
        SecurityHelperClass.prepareSignatureParams(signature, this.signingCredential, secConfig,  JavaCast("null", ""));
        
        
        assertion.setSignature(signature);
        
        
        //must be marshalled before being signed
        ConfigurationClass.getMarshallerFactory().getMarshaller(assertion).marshall(assertion);

        var SignerClass=createObject("java", "org.opensaml.xml.signature.Signer");
        SignerClass.signObject(signature);
        
        return assertion;
    }
    
    private any function createIssuer(String issuerString){
    	var IssuerClass=createObject("java", "org.opensaml.saml2.core.Issuer");
        var issuerBuilder = getSAMLBuilder().getBuilder(IssuerClass.DEFAULT_ELEMENT_NAME);
        var issuer =  issuerBuilder.buildObject();
        issuer.setValue(issuerString);
        return issuer;
    }
    
    private any function createSubject(any nameID, any subjectConfirmation){
    	var SubjectClass=createObject("java", "org.opensaml.saml2.core.Subject");
        subjectBuilder =  getSAMLBuilder().getBuilder(SubjectClass.DEFAULT_ELEMENT_NAME);
        subject = subjectBuilder.buildObject();
        subject.setNameID( nameID);
        subject.getSubjectConfirmations().add(subjectConfirmation);
        
        return subject;
    }
    
    private any function createNameID(String nameIDText){
        var NameIDClass=createObject("java", "org.opensaml.saml2.core.NameID");
		var nameIdBuilder = getSAMLBuilder().getBuilder(NameIDClass.DEFAULT_ELEMENT_NAME);
        var nameId =  nameIdBuilder.buildObject();
        nameId.setValue(ToString(nameIDText));
        nameId.setFormat(NameIDClass.UNSPECIFIED);
        
        return nameId;
    }
    
    private any function createSubjectConfirmation(any subjectConfirmationData){
    	var SubjectConfirmationClass=createObject("java", "org.opensaml.saml2.core.SubjectConfirmation");
        var subjectConfirmationBuilder =  getSAMLBuilder().getBuilder(SubjectConfirmationClass.DEFAULT_ELEMENT_NAME);
        var subjectConfirmation =  subjectConfirmationBuilder.buildObject();
        subjectConfirmation.setSubjectConfirmationData(subjectConfirmationData);
        subjectConfirmation.setMethod(SubjectConfirmationClass.METHOD_BEARER);
        
        return subjectConfirmation;
    } 
    
    private any function createSubjectConfirmationData(numeric allowedTimeMismatch_minutes, string recipient){
    	var SubjectConfirmationDataClass=createObject("java", "org.opensaml.saml2.core.SubjectConfirmationData");
        var confirmationMethodBuilder = getSAMLBuilder().getBuilder(SubjectConfirmationDataClass.DEFAULT_ELEMENT_NAME);
        var confirmationMethod =  confirmationMethodBuilder.buildObject();

        confirmationMethod.setNotBefore(this.now.minusMinutes(allowedTimeMismatch_minutes));
        confirmationMethod.setNotOnOrAfter(this.now.plusMinutes(allowedTimeMismatch_minutes));
        confirmationMethod.setRecipient(recipient);
        return confirmationMethod;
    } 
    
    private any function createAuthnStatement(numeric allowedTimeMismatch_minutes){
    	var AuthnStatementClass=createObject("java", "org.opensaml.saml2.core.AuthnStatement");
    	var AuthnContextClass=createObject("java", "org.opensaml.saml2.core.AuthnContext");
    	var AuthnContextClassRefClass=createObject("java", "org.opensaml.saml2.core.AuthnContextClassRef");
    	

    	
        var authStatementBuilder =createObject("java", "org.opensaml.saml2.core.impl.AuthnStatementBuilder").init();
        var authnContextBuilder =createObject("java", "org.opensaml.saml2.core.impl.AuthnContextBuilder").init();
        var authnContextClassRefBuilder =createObject("java", "org.opensaml.saml2.core.impl.AuthnContextClassRefBuilder").init();

        var authnContextClassRef=authnContextClassRefBuilder.buildObject();
        authnContextClassRef.setAuthnContextClassRef("urn:oasis:names:tc:SAML:2.0:ac:classes:Password"); //we state that the user used their username and password
        
        var authnContext=authnContextBuilder.buildObject();
        authnContext.setAuthnContextClassRef(authnContextClassRef);
        
        var authnStatement = authStatementBuilder.buildObject();
        authnStatement.setAuthnInstant(this.now);
        authnStatement.setAuthnContext(authnContext);
        authnStatement.setSessionNotOnOrAfter(this.now.plusMinutes(allowedTimeMismatch_minutes));
        return authnStatement;
    }
    
    private any function createAttributeStatement(struct assertionAttributes){

        var attrStatementBuilder =  createObject("java", "org.opensaml.saml2.core.impl.AttributeStatementBuilder").init();
        var attrStatement =  attrStatementBuilder.buildObject();


        
        var keySet = assertionAttributes.keySet();
        var iterator=keySet.iterator();
        while(iterator.hasNext()){
            key=iterator.next();

            
            
            var attrFirstName = buildStringAttribute(key,assertionAttributes.get(key));

            
            
            attrStatement.getAttributes().add(attrFirstName);
        }
        
        return attrStatement;
    }
    

   public any function buildStringAttribute(String name, String value){
		var attrBuilder =createObject("java", "org.opensaml.saml2.core.impl.AttributeBuilder").init();
		var attrFirstName =  attrBuilder.buildObject();
		attrFirstName.setName(name);
		
		
		var XSStringClass=createObject("java", "org.opensaml.xml.schema.XSString");
		var AttributeValueClass=createObject("java", "org.opensaml.saml2.core.AttributeValue");
		 // Set custom Attributes
		var stringBuilder = getSAMLBuilder().getBuilder(XSStringClass.TYPE_NAME);
		var attrValueFirstName =  stringBuilder.buildObject(AttributeValueClass.DEFAULT_ELEMENT_NAME, XSStringClass.TYPE_NAME);
		attrValueFirstName.setValue(JavaCast("string", value));
		
		attrFirstName.getAttributeValues().add(attrValueFirstName);
		return attrFirstName;
   }
    
   

    public any function getSAMLBuilder(){

            if(NOT structKeyExists(this,"builderFactory")){
                // OpenSAML 2.3
                var BootstrapClass=createObject("java","org.opensaml.DefaultBootstrap");
                BootstrapClass.bootstrap();
                var configurationClass=createObject("java", "org.opensaml.Configuration");
             	this.builderFactory = configurationClass.getBuilderFactory();
            }

            return this.builderFactory;
    }
    
    private any function createConditions(numeric allowedTimeMismatch_minutes, String audianceURI){

        var audienceRestrictionBuilder=createObject("java", "org.opensaml.saml2.core.impl.AudienceRestrictionBuilder").init();
        

        var audienceBuilder=createObject("java", "org.opensaml.saml2.core.impl.AudienceBuilder").init();
        
        var audienceRestriction=audienceRestrictionBuilder.buildObject();
        var audiance=audienceBuilder.buildObject();
        
        audiance.setAudienceURI(audianceURI);
        audienceRestriction.getAudiences().add(audiance);


        var conditionsBuilder  =createObject("java", "org.opensaml.saml2.core.impl.ConditionsBuilder").init();
        var conditions = conditionsBuilder.buildObject();
        conditions.setNotBefore(this.now.minusMinutes(allowedTimeMismatch_minutes));
        conditions.setNotOnOrAfter(this.now.plusMinutes(allowedTimeMismatch_minutes));
        conditions.getConditions().add(audienceRestriction);
        
        return conditions;
    }
   
}