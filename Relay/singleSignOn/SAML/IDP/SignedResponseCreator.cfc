/**
 * SignedResponseCreator
 * 
 * @author Richard.Tingle
 * @date 09/11/15
 **/
component accessors=true output=false persistent=false {
	
    
    public any function init(required String issuer,required  String certificateStore, required String storePassword,required String certificateAlias){
        this.issuer=lcase(issuer); //BF-294 RJT standardise to lower case for consistency
        this.certificateStore=certificateStore;
        this.certificateAlias=certificateAlias;
        this.storePassword=storePassword;
        
        if (this.issuer eq ""){
        	throw (message="Issuer can't be empty");
        }
        if (this.certificateAlias eq ""){
        	throw (message="certificateAlias can't be empty");
        }
        
        var DateTimeZoneClass=createObject("java", "org.joda.time.DateTimeZone");
		this.now=createObject("java", "org.joda.time.DateTime").init(DateTimeZoneClass.UTC);
        
        return this;
    }
    
    private void function intializeCredentials(){
        if (NOT structKeyExists(this,"signingCredential")){


            var password = this.storePassword.toCharArray();

            
            // Get Default Instance of KeyStore
			var KeyStoreClass=createObject("java", "java.security.KeyStore");
			

            var ks = KeyStoreClass.getInstance("pkcs12");

			if (not FileExists(this.certificateStore)){
				/*
				* because the line createObject("java", "java.io.FileInputStream").init(this.certificateStore) throws a
				* bizarre error (Java class must not be abstrat") if the file can't be found we check first and throw a 
				* sensible error instead 
				*/ 
				throw (message = " The file being used as the SAML cerrtificate store(" & this.certificateStore & ") does not exist" );
			}

            var fis = createObject("java", "java.io.FileInputStream").init(this.certificateStore);

            ks.load(fis, password);


            fis.close();


         // Get Private Key Entry From Certificate


            var pkEntry =  ks.getEntry(this.certificateAlias,  createObject("java", "java.security.KeyStore$PasswordProtection").init(
                     this.storePassword.toCharArray()));



           var pk = pkEntry.getPrivateKey();

           var certificate =  pkEntry.getCertificate();
           var credential =createObject("java", "org.opensaml.xml.security.x509.BasicX509Credential").init();
           credential.setEntityCertificate(certificate);
           credential.setPrivateKey(pk);
           this.signingCredential = credential;

        }
 
   }
    
    
    public String function createSignedResponse(String requestID,String nameID, String assertionConsumerURI, String audianceURI, numeric allowedTimeMismatch_minutes, struct assertionAttributes){
        intializeCredentials();
       
       	var DefaultBootstrapClass=createObject("java", "org.opensaml.DefaultBootstrap");
       
        DefaultBootstrapClass.bootstrap();

		var ConfigurationClass=createObject("java", "org.opensaml.Configuration");
		var SignatureClass=createObject("java", "org.opensaml.xml.signature.Signature");

        var signature =  ConfigurationClass.getBuilderFactory().getBuilder(SignatureClass.DEFAULT_ELEMENT_NAME)
                 .buildObject(SignatureClass.DEFAULT_ELEMENT_NAME);

        signature.setSigningCredential(this.signingCredential);

        // This is also the default if a null SecurityConfiguration is specified
        var secConfig = ConfigurationClass.getGlobalSecurityConfiguration();
        // If null this would result in the default KeyInfoGenerator being used
        var keyInfoGeneratorProfile = "XMLSignature";

        
        var SecurityHelperClass=createObject("java", "org.opensaml.xml.security.SecurityHelper");
        
        SecurityHelperClass.prepareSignatureParams(signature, this.signingCredential, secConfig, JavaCast("null", ""));

		var IssuerClass=createObject("java", "org.opensaml.saml2.core.Issuer");
		var ResponseClass=createObject("java", "org.opensaml.saml2.core.Response");
		var StatusClass=createObject("java", "org.opensaml.saml2.core.Status");
		
        var issuerBuilder=ConfigurationClass.getBuilderFactory().getBuilder(IssuerClass.DEFAULT_ELEMENT_NAME); 
        var responseBuilder=ConfigurationClass.getBuilderFactory().getBuilder(ResponseClass.DEFAULT_ELEMENT_NAME);
        var statusBuilder=ConfigurationClass.getBuilderFactory().getBuilder(StatusClass.DEFAULT_ELEMENT_NAME);

		var UUIDClass=createObject("java", "java.util.UUID");

        var response=responseBuilder.buildObject();
        response.setID("id" & UUIDClass.randomUUID().toString());
        response.setIssueInstant(this.now);
        if (structkeyExists(arguments,"requestID") AND arguments.requestID NEQ ""){
        	response.setInResponseTo(requestID);
        }
        
        var issuerElement=issuerBuilder.buildObject();
        issuerElement.setValue(this.issuer);
        response.setIssuer(issuerElement);
        
        var StatusCodeClass=createObject("java", "org.opensaml.saml2.core.StatusCode"); 
        
		var statusBuilder=createObject("java", "org.opensaml.saml2.core.impl.StatusBuilder").init();
		var statusCodeBuilder=createObject("java", "org.opensaml.saml2.core.impl.StatusCodeBuilder").init();
        var stat=statusBuilder.buildObject();
        var statCode=statusCodeBuilder.buildObject();
        statCode.setValue(StatusCodeClass.SUCCESS_URI);
        stat.setStatusCode(statCode);

		var SAMLVersionClass=createObject("java", "org.opensaml.common.SAMLVersion");

        response.setStatus(stat);
        response.setVersion(SAMLVersionClass.VERSION_20);
        response.setDestination(assertionConsumerURI);
        
        var ac=new AssertionCreator(this.signingCredential);

        var assertion=ac.createAssersion(nameID,this.issuer, assertionAttributes, audianceURI, allowedTimeMismatch_minutes,assertionConsumerURI);

        
        //response.getAssertions().add(SAMLWriter.getSamlAssertion());
        response.getAssertions().add(assertion);
        response.setSignature(signature);
 

 
      
         ConfigurationClass.getMarshallerFactory().getMarshaller(response).marshall(response);
      
     
 
        var SignerClass=createObject("java", "org.opensaml.xml.signature.Signer");
  
        SignerClass.signObject(signature);

		var XMLHelperClass=createObject("java", "org.opensaml.xml.util.XMLHelper");

        var marshaller = createObject("java", "org.opensaml.saml2.core.impl.ResponseMarshaller").init();
        var plain = marshaller.marshall(response);
        var samlResponse = XMLHelperClass.nodeToString(plain);

        return samlResponse;
    }
    

}