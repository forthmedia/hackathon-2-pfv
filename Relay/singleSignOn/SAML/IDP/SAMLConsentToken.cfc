/**
 * SAMLConsentToken
 * 
 * @author Richard.Tingle
 * @date 24/11/15
 **/
component extends="singleSignOn.common.consentToken" accessors=true output=false persistent=false {

	public any function init(required string tokenTypeTextID="samlConsentToken",required numeric validity_seconds= 3600){
		return super.init(argumentCollection=arguments);
	}
	public void function setRelayState(required string relayState) output="false" {
		return set("relayState", arguments.relayState);
	}
	
	public string function getRelayState() output="false" {
		return get("relayState");
	}
	
	public void function setInResponseTo(required string inResponseTo) output="false" {
		return set("inResponseTo", arguments.inResponseTo);
	}
	
	public string function getInResponseTo() output="false" {
		return get("inResponseTo");
	}
	

}