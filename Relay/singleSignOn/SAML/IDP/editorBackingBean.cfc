<!---
  --- editorBackingBean
  --- -----------------
  ---
  --- author: Richard.Tingle
  --- date:   21/07/15
  --->
<cfcomponent accessors="true" output="false" persistent="false">


	<cffunction name="createNewClient" access="public" output="false" returntype="string">
		<cfargument name="clientName" type="string" required="true" />
		<cfargument name="redirectURL" type="string" required="true" />
			<cfscript>
				var oauth2client=createObject("component","OAuth2Client");

				if (not redirectURL.toLowerCase().startsWith("http")){
					redirectURL="http://" & redirectURL;
				}

				oauth2client.init(clientName=clientName,redirectURL=redirectURL);
				oauth2client.autogenerateClient(); //the client secret will be overritten later but we need the client ID
				oauth2client.persistToDatabase();

			</cfscript>

		<cfreturn oauth2client.getClientIdentifier()/>
	</cffunction>

	<cffunction name="deleteClient" access="public" output="false" returntype="void">
		<cfargument name="clientID" type="string" required="true"/>
		<cfscript>
				var oauth2client=retrieveClient(clientID);
				oauth2client.deleteFromDatabase();
		</cfscript>

		<cfreturn />
	</cffunction>

	<cffunction name="deactivateClient" access="public" output="false" returntype="void">
		<cfargument name="clientID" type="string" required="true"/>
			<cfscript>
				var oauth2client=retrieveClient(clientID);
				oauth2client.setActive(false);
				oauth2client.persistToDatabase();
			</cfscript>

		<cfreturn />
	</cffunction>

	<cffunction name="activateClient" access="public" output="false" returntype="void">
		<cfargument name="clientID" type="string" required="true"/>

			<cfscript>
				var oauth2client=retrieveClient(clientID);
				oauth2client.setActive(true);
				oauth2client.persistToDatabase();
			</cfscript>
		<cfreturn />
	</cffunction>

	<cffunction name="retrieveClient">
		<cfargument name="clientID" type="string" required="true"/>
			<cfscript>
				oauth2client=createObject("component","OAuth2Client").init(clientIdentifier=clientID);
				oauth2client.pullFromDatabase();
				return oauth2client;
			</cfscript>

	</cffunction>


	<cffunction name="getAllServiceProviders" access="public" output="false" returntype="array">
		<cfset var clientList="">
		<cfset var clientArray="">
		<cfset var clientObjectArray= arrayNew(1)>
		<CFQUERY NAME="serviceProviderIdentifiers" datasource="#application.siteDataSource#">
			SELECT
			    ServiceProviderIdentifier
			FROM
				SAMLServiceProvider;
		</CFQUERY>

		<CFLOOP QUERY="serviceProviderIdentifiers">
			<cfscript>
				clientList=listAppend(clientList,serviceProviderIdentifiers.ServiceProviderIdentifier);
			</cfscript>
		</CFLOOP>



		<cfscript>
			clientArray=listToarray(clientList);

			for(i=1;i<=arrayLen(clientArray);i++){
				clientObjectArray[i]=createObject("component","relay.singleSignOn.OAuth2.AuthorisationServer.OAuth2Client").init(clientIdentifier=clientArray[i]);
				clientObjectArray[i].pullFromDatabase();
			}


			return clientObjectArray;
		</cfscript>

	</cffunction>

	<cffunction name="eliminateProfilesWithoutTextIDs" access="public" output="false" returntype="array">
		<cfargument name="fieldList" type="array" required="true"/>
		<cfscript>
			var arrayLength=arrayLen(fieldList);
			//loop backwards to avoid the indexes changing while deleting issue
			for(var i=arrayLength;i>0;i--){
				if (isnumeric(fieldList[i])){
					ArrayDeleteAt(fieldList, i);
				}
			}

			return fieldList;

		</cfscript>
	</cffunction>
	
	<cffunction name="validateList" access="public" output="false" returntype="struct" hint="Returns a struct containing isOk which details if all the desiredFields are allowed and an array disallowedFields which will contain all extra fields">
		<cfargument name="desiredFieldList" type="array" required="true" hint="the desired fields"/>
		<cfargument name="allAvailableFields" type="array" required="true" hint="the list of fields that must contain the desired fields" />
		<cfscript>
			return validateScopeProfiles(desiredFieldList,allAvailableFields);

		</cfscript>
	</cffunction>
	

	<cffunction name="validateScopeProfiles" access="public" output="false" returntype="struct" hint="Returns a struct containing isOk which details if all the desiredFields are allowed and an array disallowedFields which will contain all extra fields">
		<cfargument name="desiredFieldList" type="array" required="true" hint="the desired fields"/>
		<cfargument name="allAvailableFields" type="array" required="true" hint="the list of fields that must contain the desired fields" />
		<cfscript>
			var returnStruct={isOk=true,disallowedFields=arrayNew(1), allowedFields=arrayNew(1)};

			for(field in desiredFieldList){
				if (NOT arrayContains(allAvailableFields,field)){
					returnStruct.isOk=false;
					arrayAppend(returnStruct.disallowedFields,field);
				}else{
					arrayAppend(returnStruct.allowedFields,field);

				}
			}


			return returnStruct;

		</cfscript>
	</cffunction>

	<cffunction name="convertArrayToOrderedStruct" access="public" output="false" returntype="struct">
		<cfargument name="arrayToConvert" type="array" required="true" hint="the desired fields"/>
		
		<cfscript>
			orderedStruct = createObject("java", "java.util.LinkedHashMap").init();

		    for(i=1;i<=arrayLen(arrayToConvert);i=i+1){
	
		    	orderedStruct[arrayToConvert[i]]=arrayToConvert[i];
		    }
		    return orderedStruct;
		
		</cfscript>
		
		
	</cffunction>
	
	<cffunction name="deleteArrayFromArray" access="public" output="false" returntype="array">
		<cfargument name="startArray" type="array" required="true"/>
		<cfargument name="arrayToDelete" type="array" required="true"/>
		<cfscript>
			newArray=ArrayNew(1);

		    for(i=1;i<=arrayLen(startArray);i=i+1){
		    	if (NOT ArrayContains(arrayToDelete,startArray[i])){
		    		arrayAppend(newArray,startArray[i]);
		    	}
		    }
		    return newArray;
		
		</cfscript>
		
		
	</cffunction>


</cfcomponent>