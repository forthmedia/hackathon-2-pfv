<!---
  --- SAMLRestartToken
  --- ----------
  ---	This token allows the RelayState and SAMLRequest XML (that came to us in a form post) to be squirreled away
  ---	till after the user is logged in
  --- author: Richard.Tingle
  --- date:   27/11/15
  --->
<cfcomponent extends="com.tokens.oneTimeUseToken" accessors="true" output="false" persistent="false">
	<cffunction name="init" access="public" output="false" returntype="any">
		<cfargument name="tokenTypeTextID" type="string" default="SAMLRestartToken" />
		<cfargument name="validity_seconds" type="numeric" default=3600 />
		<cfset super.init(argumentCollection=arguments)>

		<cfreturn this>
	</cffunction>

	<cffunction name="setRelayState" access="public" output="false" returntype="void">
		<cfargument name="relayState" type="string" required=true />
		<cfscript>
			set("relayState",relayState);
		</cfscript>
	</cffunction>

	<cffunction name="getRelayState" access="public" output="false" returntype="string">
		<cfscript>
			return get("relayState");
		</cfscript>
	</cffunction>
	
	<cffunction name="setSAMLRequest" access="public" output="false" returntype="void">
		<cfargument name="SAMLRequest" type="string" required=true />
		<cfscript>
			set("SAMLRequest",SAMLRequest);
		</cfscript>
	</cffunction>

	<cffunction name="getSAMLRequest" access="public" output="false" returntype="string">
		<cfscript>
			return get("SAMLRequest");
		</cfscript>
	</cffunction>




</cfcomponent>