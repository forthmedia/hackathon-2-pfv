/**
 * CertificateManager
 * 
 * @author Richard.Tingle
 * @date 13/11/15
 **/
component accessors=true output=false persistent=false {


    public void function init(String storePath=application.paths.userfiles & "\Certificates\SAMLCertificates.pfx", String storePassword="dtfhcvsae5fbzafbnse") {
        this.storePath = createObject("java","java.io.File").init(storePath);
        this.storePassword = storePassword;
    }
    
    public String function getCertificateStoreLocation(){
    	return this.storePath;
    }
    
    public String function getCertificateStorePassword(){
    	return this.storePassword;
    }
    
    private any function getKeyStore(){
        
        if (not StructKeyExists(this,"keyStore")){
            var SecurityClass=createObject("java","java.security.Security");
            var KeyStoreClass=createObject("java","java.security.KeyStore");
            
            
            SecurityClass.addProvider(createObject("java","org.bouncycastle.jce.provider.BouncyCastleProvider").init());
            
            this.keyStore = KeyStoreClass.getInstance("PKCS12");

            if (this.storePath.exists()){
                var fileInputStream =createObject("java","java.io.FileInputStream").init(this.storePath);
                try {
                	this.keyStore.load(fileInputStream, this.storePassword.toCharArray());
                } finally {
                	//close the stream no matter what
                	fileInputStream.close();
                }
            }else{
                //ensure that everything exists ready for committing back
                var folder=this.storePath.getParentFile();
                folder.mkdirs();
                
                this.keyStore.load(javacast("null",""), this.storePassword.toCharArray());
            }
        }
        
        return this.keyStore;
    }
    
    
    public boolean function certificateExists(string alias){
        arguments.alias=sanitiseAliasName(arguments.alias);
        return ArrayFindNoCase(getAllAliases(),arguments.alias) NEQ 0;
    }
    
    public void function createCertificate(string alias, numeric yearsValidity){
        arguments.alias=sanitiseAliasName(arguments.alias);
        
        var outStore =getKeyStore();
        
        var KeyPairGeneratorClass=createObject("java","java.security.KeyPairGenerator");
        
        var keyPairGenerator = KeyPairGeneratorClass.getInstance("RSA");
        
        keyPairGenerator.initialize(2048);
        var keyPair = keyPairGenerator.generateKeyPair();
        var publicKey = keyPair.getPublic();
        var privateKey = keyPair.getPrivate();
        var selfCert = createCertificateFromKeys("CN=Relayware", "CN=Relayware",
            publicKey, privateKey,yearsValidity);

        // Note: if you just want to store this certificate then write the
        // contents of selfCert.getEncoded() to file

        var outChain =arrayNew(1);
        outChain[1]=selfCert;
		

        outStore.setKeyEntry(arguments.alias, privateKey, this.storePassword.toCharArray(),javacast("java.security.cert.Certificate[]",outChain));
        
        persistKeystore();
    }
    
    /**
     * Certificates can only be deleted if no client is using them
     **/
    public boolean function certificateCanBeDeleted(string alias){
    	arguments.alias=sanitiseAliasName(arguments.alias);
    	var sanitisedAlias=application.com.security.queryParam(value=arguments.alias, cfsqltype="CF_SQL_VARCHAR");
		

		var entityDetails=application.com.relayEntity.getEntity(entityTypeID=application.entityTypeID.SAMLServiceProvider,whereClause="certificateToUse=#sanitisedAlias#", fieldList="certificateToUse");
		
    	
    	return entityDetails.recordCount EQ 0;
    }
    
    public void function deleteCertificate(string alias){
    	arguments.alias=sanitiseAliasName(arguments.alias);
    	getKeyStore().deleteEntry(arguments.alias);
    	persistKeystore();
    	
    }
    
    public string function getBase64Certificate(string alias){


        return ToBase64(getKeyStore().getCertificate(sanitiseAliasName(alias)).getEncoded());
    }
    
    
    private void function persistKeystore(){
        if ( getAllAliases().isEmpty()){
        	//empty keystores cause problems (can't be loaded), just delete the file itself
        	
        	//paranoia checks, should never come up but the idea of deleting C:// always alarms me
        	if (NOT this.storePath.isDirectory()){
        		FileDelete(this.storePath);
        	}
        	
        }else{
	        var outputStream = createObject("java","java.io.FileOutputStream").init(this.storePath);
	        getKeyStore().store(outputStream, this.storePassword.toCharArray());
	        outputStream.flush();
	        outputStream.close();
        }
    }
    
    public any function getAllAliases(){
        var aliases=getKeyStore().aliases();
        
        var aliasList=ArrayNew(1);
        //createObject("java","java.util.ArrayList");
        
        while(aliases.hasMoreElements()){
            aliasList.add(aliases.nextElement());
        }
        
        return aliasList;        
    }
    
    public any function getCertificateExpiry(String alias){
		//returns a java.util.Date;
        try {
            var cert=getKeyStore().getCertificate(sanitiseAliasName(alias));
            return cert.getNotAfter();
        } catch (any ex) {
            throw ( message="No such certificate as " & alias);
        }
    }
    
	public string function getCertificateExpiry_IsoTimeString(String alias){
        
        datetime = dateConvert( "local2utc", getCertificateExpiry(sanitiseAliasName(alias)));
        
        // When formatting the time, make sure to use "HH" so that the
        // time is formatted using 24-hour time.
        return(
            dateFormat( datetime, "yyyy-mm-dd" ) &
            "T" &
            timeFormat( datetime, "HH:mm:ss" ) &
            "Z"
        );
    }
	
	
	
    
    private any function createCertificateFromKeys(String dn, String issuer,any publicKey, any privateKey, numeric yearsOfValidity){
        //takes String dn, String issuer,PublicKey publicKey, PrivateKey privateKey, int yearsOfValidity
        //returns a java.security.cert.X509Certificate
        var certGenerator = createObject("java","org.bouncycastle.x509.X509V3CertificateGenerator").init();
        
        var random=createObject("java","java.util.Random").init();
        var MathClass=createObject("java","java.lang.Math");
        var BigIntegerClass=createObject("java","java.math.BigInteger");
        var CalendarClass=createObject("java","java.util.Calendar");
        
        certGenerator.setSerialNumber(BigIntegerClass.valueOf(MathClass.abs(random.nextLong())));
        certGenerator.setIssuerDN(createObject("java","org.bouncycastle.asn1.x509.X509Name").init(dn));
        certGenerator.setSubjectDN(createObject("java","org.bouncycastle.asn1.x509.X509Name").init(dn));
        certGenerator.setIssuerDN(createObject("java","org.bouncycastle.asn1.x509.X509Name").init(issuer)); 
        certGenerator.setNotBefore(CalendarClass.getInstance().getTime());
        
        
        var expiry=CalendarClass.getInstance();
        expiry.add(CalendarClass.YEAR, yearsOfValidity);
        
        certGenerator.setNotAfter(expiry.getTime());
        certGenerator.setPublicKey(publicKey);
        certGenerator.setSignatureAlgorithm("SHA1WithRSAEncryption");
        var certificate = certGenerator.generate(privateKey, "BC");
        return certificate;
    }
    
    /**
    * We can't have control characters in our alias names, we sanities them out
    */
    private string function sanitiseAliasName(String aliasName){
    	var sanitiedString=Replace(aliasName,'"',"dquot");
    	sanitiedString=Replace(sanitiedString,"'","quot");
    	sanitiedString=Replace(sanitiedString,"<","GT");
    	sanitiedString=Replace(sanitiedString,">","LT");
    	sanitiedString=Replace(sanitiedString,"&","AND");
    	sanitiedString=Replace(sanitiedString,"$","DOLLAR");
    	
    	return sanitiedString;
    }
    

}