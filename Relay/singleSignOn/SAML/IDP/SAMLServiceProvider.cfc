/**
 * SAMLServiceProvider2
 * 
 * @author Richard.Tingle
 * @date 18/11/15
 * 
 * Amendment History:
 *
 * Date (YYYY-MM-DD)	Initials 	What was changed
 * 2015-01-20			RJT			BF-312 Attributes now use the friendly name (or else you get really horrible "computed column 1" for flag groups	
 * 2015-02-01			RJT			Modifications to allow (on a case by case basis) foreign key lookups, was required for cyberark
 * 
 **/
component accessors=true output=false persistent=false {
	public any function init(String serviceProviderIdentifier=""){
		
		this.attributeManager=new singleSignOn.common.AttributeManager();
		
		this.serviceProviderId=""; //relayware ID
		this.serviceProviderName="";
		this.serviceProviderIdentifier=arguments.serviceProviderIdentifier;
		this.assertionConsumerURL="";
		this.nameIDField="PersonID"; 
		this.personAttributes="";
		this.locationAttributes="";
		this.organisationAttributes="";
		this.checkedIfExistsInDatabase=false;
		this.existsInDatabase=false;
		this.certificateToUse="";
		this.certificateWarningSent=false;
		if (serviceProviderIdentifier NEQ ""){
			this.serviceProviderIdentifier=arguments.serviceProviderIdentifier;
		}else{
			this.serviceProviderIdentifier=randomString();
		}
		return this;		
	}
	
	public void function setServiceProviderName(required String serviceProviderName){
		this.ServiceProviderName=arguments.serviceProviderName;
	}
	public void function setAssertionConsumerURL(required String assertionConsumerURL){
		
		this.assertionConsumerURL=arguments.assertionConsumerURL;
		
		if (not this.assertionConsumerURL.startsWith("http")){
			this.assertionConsumerURL="http://" &this.assertionConsumerURL;
		}
	}
	public void function setNameIDField(required String nameIDField){
		this.nameIDField=arguments.nameIDField;
	}
	public void function setPersonAttributes(required any personAttributes){
		if (isArray(arguments.personAttributes)){
			arguments.personAttributes=arrayToList(arguments.personAttributes);
		}
		
		this.personAttributes=arguments.personAttributes;
	}
	public void function setLocationAttributes(required any locationAttributes){
		if (isArray(arguments.locationAttributes)){
			arguments.locationAttributes=arrayToList(arguments.locationAttributes);
		}
		this.locationAttributes=arguments.locationAttributes;
	}
	public void function setOrganisationAttributes(required any organisationAttributes){
		if (isArray(arguments.organisationAttributes)){
			arguments.organisationAttributes=arrayToList(arguments.organisationAttributes);
		}
		this.organisationAttributes=arguments.organisationAttributes;
	}
	
	public string function setCertificateToUse(required string certificateToUse){
		if (this.certificateToUse NEQ arguments.certificateToUse){
			this.certificateWarningSent=false; //reset warning
		}
	    this.certificateToUse=arguments.certificateToUse;
	}
	public string function getCertificateToUse(){
		return this.certificateToUse;
	}
	
	public string function getServiceProviderName(){
		return this.ServiceProviderName;
	}
	public string function getServiceProviderIdentifier(){
		return this.serviceProviderIdentifier;
	}
	public string function getAssertionConsumerURL(){
		return this.assertionConsumerURL;
	}
	public string function getNameIDField(){
		return this.nameIDField;
	}
	public array function getPersonAttributes(){
		return listToArray(this.personAttributes);
	}
	public array function getLocationAttributes(){
		return listToArray(this.locationAttributes);
	}
	public array function getOrganisationAttributes(){
		return listToArray(this.organisationAttributes);
	}
	

	
	
	
	public string function randomString(numeric length=56){
		
			var permittedcharactersStart="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
			var permittedcharacters="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890";
			
			//secure random is a crytographically safe random number generator
			var secureRand = CreateObject("java", "java.security.SecureRandom").init();
			var i=1;
			var character="";
			var randomString="";
			
			//saml requires start with letter
			randomString=Mid( permittedcharactersStart,secureRand.nextInt(len(permittedcharactersStart))+1, 1 );
			for(i=2;i<=length;i++){
				character=Mid( permittedcharacters,secureRand.nextInt(len(permittedcharacters))+1, 1 );
				randomString=randomString & character;
			}

			return randomString;
	}
	
	public boolean function isAuthorisedToAccessDataForUser(required numeric personID){
		var sanitisedPersonID=application.com.security.queryParam(value=arguments.personID, cfsqltype="CF_SQL_INTEGER");
		var sanitisedServiceProviderId=application.com.security.queryParam(value=this.serviceProviderId, cfsqltype="CF_SQL_INTEGER");
		
		var result=application.com.relayEntity.getEntity(EntityTypeID=application.entityTypeID.SAMLServiceProviderPersonPermission,whereclaus="personID=sanitisedPersonID and ServiceProviderId=sanitisedServiceProviderId", fieldList="personID" );
		
		return result.recordCount>0;
		
	}
	public void function setAuthorisedToAccessDataForUser(required numeric personID){
		var entityDetailsStruct=StructNew();
		entityDetailsStruct.personID=arguments.personID;
		entityDetailsStruct.ServiceProviderId=this.serviceProviderId;	

		var returnValue=application.com.relayEntity.insertEntity(entityTypeID=application.entityTypeID.SAMLServiceProviderPersonPermission,entityDetails=entityDetailsStruct);
		
		if (not returnValue.isOk){
			if (structKeyExists(returnValue,"ERRORID")){
				throw (message="ErrorID " & returnValue.ERRORID &" " &returnValue.message);
			}else{
				throw (message=returnValue.message);
			}
		}
	
	}
	
	
	
	/**
	 * Attempts to pull data from the database, if it finds a record returns true, otherwise returns false
	 **/
	public boolean function pullFromDatabase(){
		checkedIfExistsInDatabase=true;
		
		var sanitisedServiceProviderName=application.com.security.queryParam(value=this.ServiceProviderIdentifier, cfsqltype="CF_SQL_VARCHAR");
		

		var entityDetails=application.com.relayEntity.getEntity(entityTypeID=application.entityTypeID.SAMLServiceProvider,whereClause="ServiceProviderIdentifier=#sanitisedServiceProviderName#", fieldList="ServiceProviderId,ServiceProviderName,assertionConsumerURL,nameIDField,personAttributes,locationAttributes,organisationAttributes,certificateToUse,certificateWarningSent");
		if (entityDetails.RecordCount EQ 0){
			//thats fine, load nothing
			return false;
		}else if (entityDetails.RecordCount EQ 1){
			this.ServiceProviderId=entityDetails.recordSet.ServiceProviderId;
			this.ServiceProviderName=entityDetails.recordSet.ServiceProviderName;
			this.assertionConsumerURL=entityDetails.recordSet.assertionConsumerURL;
			this.nameIDField=entityDetails.recordSet.nameIDField;
			this.personAttributes=entityDetails.recordSet.personAttributes;
			this.locationAttributes=entityDetails.recordSet.locationAttributes;
			this.organisationAttributes=entityDetails.recordSet.organisationAttributes;
			this.certificateWarningSent=entityDetails.recordSet.certificateWarningSent;
			this.certificateToUse=entityDetails.recordSet.certificateToUse;
			this.existsInDatabase=true;
			return true;
		}else{
			throw(message = "Was expecting 0 or 1 record for serviceProviderIdentifier #serviceProviderIdentifier# but instead recieved #entityDetails.RecordCount#");
		}
		
	}
	public void function pushToDatabase(){
		
		var entityDetailsStruct=StructNew();
			entityDetailsStruct.ServiceProviderName=this.ServiceProviderName;
			entityDetailsStruct.serviceProviderIdentifier=this.serviceProviderIdentifier;
			entityDetailsStruct.assertionConsumerURL=this.assertionConsumerURL;
			entityDetailsStruct.nameIDField=this.nameIDField;
			entityDetailsStruct.personAttributes=this.personAttributes;
			entityDetailsStruct.locationAttributes=this.locationAttributes;
			entityDetailsStruct.organisationAttributes=this.organisationAttributes;
			entityDetailsStruct.certificateWarningSent=this.certificateWarningSent;
			entityDetailsStruct.certificateToUse=this.certificateToUse;
		if (existsInDatabase(pullOnExists=false)){
			var returnValue=application.com.relayEntity.updateEntityDetails(entityTypeID=application.entityTypeID.SAMLServiceProvider, entityID=this.ServiceProviderId,entityDetails=entityDetailsStruct);
			if (not returnValue.isOk){
				if (structKeyExists(returnValue,"ERRORID")){
					throw (message="ErrorID" & returnValue.ERRORID &" " &returnValue.returnValue.message);
				}else{
					throw (message=returnValue.message);
				}
			}
		}else{
			var returnValue=application.com.relayEntity.insertEntity(entityTypeID=application.entityTypeID.SAMLServiceProvider,entityDetails=entityDetailsStruct);
			if (not returnValue.isOk){
				if (structKeyExists(returnValue,"ERRORID")){
					throw (message="ErrorID " & returnValue.ERRORID &" " &returnValue.message);
				}else{
					throw (message=returnValue.message);
				}
			}

		}
		
	}
	
	public void function delete(){
		
		if (existsInDatabase()){
			var deletedReport =application.com.relayEntity.deleteEntity(entityTypeID=application.entityTypeID.SAMLServiceProvider,entityID=this.ServiceProviderId);
			if (NOT deletedReport.ISOK){
				if (structKeyExists(deletedReport,"ERRORID")){
					throw (message="ErrorID " & deletedReport.ERRORID &" " &deletedReport.message);
				}else{
					throw (message=deletedReport.message);
				}
			}
			
		}
	}
	
	public boolean function existsInDatabase(boolean pullOnExists=true){
		if (not this.checkedIfExistsInDatabase){
			if (pullOnExists){
				pullFromDatabase();
			}else{
				//we need to check without pulling
				var sanitisedServiceProviderName=application.com.security.queryParam(value=this.ServiceProviderIdentifier, cfsqltype="CF_SQL_VARCHAR");
				return 0 NEQ application.com.relayEntity.getEntity(entityTypeID=application.entityTypeID.SAMLServiceProvider,whereClause="ServiceProviderIdentifier=#sanitisedServiceProviderName#", fieldList="ServiceProviderId").recordCount;
			}
		}
		return this.existsInDatabase;
	}
	
	public any function getNameIDForAPerson(required numeric personID){
		if (CompareNoCase(getNameIDField(),"email") EQ 0 ){
			//just as an efficiency saving
			return request.relaycurrentuser.person.email;
		}else{
			var entityDetails=application.com.relayEntity.getEntity(entityTypeID=application.entityTypeID.person,entityID=arguments.personID, fieldList=getNameIDField(), testLoggedInUserRights=false);
			return entityDetails.recordSet[getNameIDField()];
		}
		

	}
	public struct function getAttributesForAPerson(required numeric personID){
		
		var ids=application.com.relayEntity.getEntity(entityTypeID=application.entityTypeID.person,entityID=arguments.personID, fieldList="OrganisationID,LocationID", testLoggedInUserRights=false).recordSet;
		var organisationID=ids["OrganisationID"];
		var locationID=ids["LocationID"];
		
		
			
		if (len(this.personAttributes)>0){
			var personAttributes=getAttributesForEntity(attributeList=this.personAttributes,entityID=arguments.personID,entityTypeId=application.entityTypeID.person);
		}else{
			var personAttributes=structNew();
		}
		if (len(this.locationAttributes)>0){
			var locationAttributes=getAttributesForEntity(attributeList=this.locationAttributes,entityID=locationID,entityTypeId=application.entityTypeID.location);
		
		}else{
			var locationAttributes=structNew();
		}
		if (len(this.organisationAttributes)>0){
			var organisationAttributes=getAttributesForEntity(attributeList=this.organisationAttributes,entityID=organisationID,entityTypeId=application.entityTypeID.organisation);
		}else{
			var organisationAttributes=structNew();
		}
		dedupingStructInsert(personAttributes,locationAttributes,"loc_");
		dedupingStructInsert(personAttributes,organisationAttributes,"org_");
		
		return personAttributes;
	
	}
	
	/**
	 * Provides the attributes associated with an entity (e.g. location) for a particular entityId (e.g. the locationId)
	 **/
	private struct function getAttributesForEntity(required string attributeList,required numeric entityID, required numeric entityTypeId){
		var filteredAttributes=this.attributeManager.filterStandardAndNonStandardAttributes(listToArray(attributeList), entityTypeId);
		
		var returnedAttributes=structNew();
		if (arrayLen(filteredAttributes.primaryAttributes)>0){
			returnedAttributes=queryRowToStruct(application.com.relayEntity.getEntity(entityTypeID=entityTypeId,entityID=entityID, fieldList=arrayToList(filteredAttributes.primaryAttributes), testLoggedInUserRights=false, useFriendlyName=true).recordset);
		}
		
		var entityTable=application.entitytype[entityTypeId].tablename;
		var entityUniqueKeyColumn=application.entitytype[entityTypeId].uniqueKey;
		
		//obtain any non standard attributes
		for(var key in filteredAttributes.nonstandardAttributes){
			
			var nonStandardAttribute=filteredAttributes.nonstandardAttributes[key];
			
			
			var nonStandardParameterQuery= new query();
			nonStandardParameterQuery.setDatasource(application.siteDataSource); 
			nonStandardParameterQuery.addParam(name="entityID",value=entityID,cfsqltype="cf_sql_numeric");
			
			var nonStandardResult=nonStandardParameterQuery.execute(sql="select #application.com.security.queryObjectName(nonStandardAttribute.requiredField)# as attributeValue " &
									  "from #application.com.security.queryObjectName(nonStandardAttribute.joiningTable)# joinTable " &
									  "join #application.com.security.queryObjectName(entityTable)# entityTable " &
									  "on joinTable.#application.com.security.queryObjectName(nonStandardAttribute.primaryKey)# = entityTable.#application.com.security.queryObjectName(nonStandardAttribute.foreignKey)# " &
									  "where entityTable.#application.com.security.queryObjectName(entityUniqueKeyColumn)# = :entityID" );
			
			var metaData=nonStandardResult.getPrefix();
			var result=nonStandardResult.getResult();
			
			if (metaData.recordCount EQ 1){
				returnedAttributes[key]=result.attributeValue;
			}else{
				application.com.errorHandler.recordRelayError_Warning(Message="SAML attribute lookup (#key#) returned #metaData.recordCount# records", WarningStructure={metadata=metaData});
			}

			
			
		}
		
		return returnedAttributes;
		
	}
	
	private struct function queryRowToStruct(required inputQuery){
		
		var columns = ListToArray( arguments.inputQuery.ColumnList );
		var noOfColumns=ArrayLen(columns);
		var structure=structNew();
		
		for(var i=1;i<=noOfColumns;i=i+1){
			structure[columns[i]]=inputQuery[columns[i]][1];
		}
		
		return structure;

	}
	
	/**
	 * adds 2 structs together while deduping the second struct if duplicates exist
	 **/
	private void function dedupingStructInsert(required struct structToAddTo, required struct structToAdd, required String dedupingPrefix){
		
		for(var key in structToAdd){
			if (structKeyExists(structToAddTo,key)){
				StructInsert(structToAddTo, dedupingPrefix & key,structToAdd[key] ,false);
			}else{
				StructInsert(structToAddTo, key,structToAdd[key] ,false);
			}
			
		}
		
	}
	
	/**
	 * Warns support about a certificate expirey if it hasn't already
	 **/
	public void function sendWarningAboutCertificateExpireyIfNeccissary(){
		if (not this.certificateWarningSent){
			var mailerService=new mail();
			mailerService.setFrom("noreply@relayware.com");
			mailerService.setTo(application.com.settings.getSetting("emailAddresses.supportEmailAddress")); 
			mailerService.setSubject("SAML Certificate Expiry warning"); 
			mailerService.setType("html"); 
			mailerService.send(body="The SAML Certificate #getCertificateToUse()# used to allow Single Sign On for #this.serviceProviderName# on site #cgi.SERVER_NAME# is about to expire. This certificate is still actively being used!"); 
			this.certificateWarningSent=true;
			pushToDatabase();
		}
	}
	
}