 

<!--- 
This is a page to be hit by third parties looking to be authenticated by Relayware

The below is to work around the fact that all the post data would be lost during a login even

--->

<cfparam name="form.relayState" default="">



<cfif request.relayCurrentUser.isLoggedIn>
	<cfscript>
		
		//we unwind any restartTokens
		if (StructKeyExists(url,'samlRestartToken')){
			samlRestartToken=new singleSignOn.SAML.IDP.SAMLRestartToken(token=url.samlRestartToken);
			samlRestartToken.pullFromDatabase();
			
			/*because the "obtaining permission" behaviour posts back to itself we can be hit twice with the same
			* samlRestartToken, it is obviously invalid the second time (it has its own way of maintaining state)
			* so the second time we're hit with the invalid token we ignore it 
			*/
			if (samlRestartToken.isValidToken()){
				form.relayState=samlRestartToken.getRelayState();
				form.SAMLRequest=samlRestartToken.getSAMLRequest();
			}
			
			
		}

		/*
		* when it doesn't exist is when we've had to obtain consent to send data, under that scenario the consent token 
		* has kept that information safe for us
		* 
		*/
		
		if (StructKeyExists(form,'SAMLRequest')){
			samlRequestString = CharsetEncode(BinaryDecode(form.SAMLRequest,"Base64") ,"utf-8");
			authnRequestReader=new singleSignOn.SAML.IDP.AuthnRequestReader();
			requestDetails=authnRequestReader.extractDataFromRequest(samlRequestString);
		}else{
			if (StructKeyExists(form,'consentTokenString')){
				requestDetails=structNew();
				requestDetails.relayState="UNUSED";
				requestDetails.requestIssuer="UNUSED";
				requestDetails.authnrequestMessageID="UNUSED";
			}else{
				/*
				* theres no way to successfully continue (we're probably here as a result of clicking "back")
				* so we'll send the user to the home page
				*/
				location("/", false, 307);
				abort;	
			}
			
		}
		
	
		
	 </cfscript>
	 
		<cfparam name="form.relayState" default="">
		<cfparam name="form.relayState" default="">
		<cfmodule template="/RelayTagTemplates/ssoSamlIdpInitiated.cfm" relayState="#form.relayState#" ServiceProvider="#requestDetails.requestIssuer#"  inResponseTo="#requestDetails.authnrequestMessageID#">
	
	 
<cfelse>
	<!---Send user to the login page with instruction to return here once they are authenticated  --->
	
	<cfscript>
		//we create a SAML restart token, this 
		samlRestartToken=new singleSignOn.SAML.IDP.SAMLRestartToken();
		samlRestartToken.setRelayState(form.RelayState);
		samlRestartToken.setSAMLRequest(form.SAMLRequest);
		samlRestartToken.persistToDatabase();
	</cfscript>

	
	<cfset urlRequested=urlEncodedFormat(CGI.Script_Name & "?" & "samlRestartToken=#samlRestartToken.getTokenValue()#" )>
	
	<cfset httpPrefix=FindNoCase("https",cgi.SERVER_PROTOCOL) NEQ 0?"https://":"http://">
	<cfset loginPageSetting=application.com.settings.getSetting("saml.IDP.loginPage")>
	<cfset loginEID=loginPageSetting EQ ""?request.currentsite.elementTreeDef.LoginPage:loginPageSetting>
	
	<cflocation url="#httpPrefix##cgi.HTTP_HOST#/?eid=#loginEID#&urlRequested=#urlRequested#" AddToken = false >

</cfif>


