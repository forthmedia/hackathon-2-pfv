function handleFileSelect(evt) {

    var files = evt.target.files; // FileList object
    var f = files[0];

    var reader = new FileReader();

    // Closure to capture the file information.
    reader.onload = (function(theFile) {
        return function(e) {
            var xmlString = e.target.result;

            var formData="xmlString=".concat(xmlString);

            jQuery.ajax({url: '/webservices/callwebservice.cfc?wsdl&method=callWebService&webserviceName=samlMetadataProcessingWS&methodName=processSPMetadata&returnFormat=plain',
            	type: "POST",
            	data : formData,
            	success: function(result){
            		resultJQ=jQuery.parseJSON(result);
            		if(resultJQ.ISOK){
            			jQuery("#assertionConsumerURL").val(resultJQ.ASSERTIONCONSUMERURL);
                        jQuery("#ServiceProviderToEdit").val(resultJQ.ENTITYID);
                        jQuery("#serviceProviderName").val(resultJQ.ENTITYNAME);
            		}else{
            			alert(resultJQ.MESSAGE);
            			console.log(resultJQ);
            		}

            }});


        };
    })(f);
    reader.readAsText(f);
}

jQuery(document).ready(function(jQuery) {
	
	
	var uploadButton=jQuery('#metadataUpload');
	
	//metadataUpload only exists if we haven't already entered the data
	if (uploadButton.length !=0){
		uploadButton.on('change', handleFileSelect);
	}
	
    
});