function handleFileSelect(evt) {

    var files = evt.target.files; // FileList object
    var f = files[0];
    var reader = new FileReader();

    // Closure to capture the file information.
    reader.onload = (function(theFile) {
        return function(e) {
        	$('metadataUploadButton').spinner({position:'center'}); //dependancy on prototype, starts the loading animation
        	var xmlString = e.target.result;
            var formData="xmlString_encoded=".concat(encodeURIComponent(xmlString)); //base64 encode to avoid any possibility of corruption (the certificate often got corrupted)
            jQuery.ajax({url: '/webservices/callwebservice.cfc?wsdl&method=callWebService&webserviceName=samlMetadataProcessingWS&methodName=processIDPMetadata&returnFormat=plain',
            	type: "POST",
            	data : formData,
            	success: function(result){
            		resultJQ=jQuery.parseJSON(result);
            		if(resultJQ.ISOK){
            			jQuery("#identityProviderEntityID").val(resultJQ.IDENTITYPROVIDERENTITYID);
            			jQuery("#identityProviderEntityID_display").val(resultJQ.IDENTITYPROVIDERENTITYID);
                        jQuery("#authnRequestURL").val(resultJQ.AUTHNREQUESTURL);
                        jQuery("#authnRequestURL_display").val(resultJQ.AUTHNREQUESTURL);
                        jQuery("#rawMetadata").val(xmlString);
            		}else{            			
            			jQuery("#samlmetadataError").show(300,'linear');
            			jQuery("#samlmetadataError_message").html(resultJQ.MESSAGE);
            			
            		}
            		$('metadataUploadButton').removeSpinner();		
            		
            },
            error: function(result){
            	$('samlmetadataError').removeSpinner();
            	
            }
            
            });


        };
    })(f);
    reader.readAsText(f);
}

jQuery(document).ready(function(jQuery) {
	
	
	var uploadButton=jQuery('#metadataUpload');
	
	//metadataUpload only exists if we haven't already entered the data
	if (uploadButton.length !=0){
		uploadButton.on('change', handleFileSelect);
	}
	
    
});