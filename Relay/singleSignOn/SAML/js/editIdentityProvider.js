	function FormSubmit() {
		updateRequiredAttributesForMappingTable(); //things are required only if visible
		collapseTableToField(); //collapses the mappingTable down into the 
	    triggerFormSubmit(jQuery('#identityProvider'));
	}
	
	function updateRequiredAttributesForMappingTable() {
		var mappingTable=jQuery('#mappingTable');
		var mappingRows=mappingTable.find('tr');

		 
		 for(var i=0; i<=mappingRows.length; i++){
		 	var mappingRow=jQuery(mappingRows.get(i));
		 	var mappingFieldRelayware=mappingRow.find('[name="mappingFieldRelayware"]');
		 	
		 	if (jQuery('#allowAutoCreate:checked').val()==="1" && mappingFieldRelayware.val() != null && mappingFieldRelayware.val() !==""){
		 		//mark the mapping as required
		 		mappingRow.find('[name="mappingValue"]').prop('required',true);
		 	}else{
		 		//mark the mapping as not required
		 		mappingRow.find('[name="mappingValue"]').removeAttr('required');	
		 	}
		 	
		 }
		
		
	}
	


	function collapseTableToField(){
		//this collapses the entire mappings datable down to json
		var collapsedData={"collapsedData":[]
			};
	
		var allTableDataRows=jQuery('#mappingTable').find('tr');
		
		for (i = 0; i < allTableDataRows.length; ++i) {
		    var dataRow=allTableDataRows[i];
		    
		    var mappingFieldRelayware=jQuery(dataRow).find('[name=mappingFieldRelayware]');
		    var currentMappingFromAssertion=jQuery(dataRow).find('[name=currentMappingFromAssertion]');
		    var mappingValue=jQuery(dataRow).find('[name=mappingValue]');
		    
		    if (mappingFieldRelayware.val() != null && mappingFieldRelayware.val() !="" ){		    
			    //append the new mapping
				collapsedData.collapsedData.push({
											mappingFieldRelayware:mappingFieldRelayware.val(),
											currentMappingFromAssertion:currentMappingFromAssertion.val(),
											mappingValue:mappingValue.val()
											});
		    }
		}

		jQuery("#mappingTable_collapsed").val(JSON.stringify(collapsedData));
	}

	function toPartnerCloudMode(){
		jQuery(".partnerCloudOptions").show();
		jQuery(".portalOptions").hide();
	}
	function toPortalMode(){
		jQuery(".partnerCloudOptions").hide();
		jQuery(".portalOptions").show();
	}
	
	
	
	/*Manage the show hide behaviour*/
	jQuery(document).ready(function(jQuery) {

		jQuery('[name="internalOnlyIDP"]').on('change', function(){
			if (jQuery('#internalOnlyIDP:checked').val() ==="1"){
				toPartnerCloudMode();
			}else{
				toPortalMode();
			}
		});
		
		jQuery('[name="PermittedToSetUsergroups"]').on('change', function(){
			if (jQuery('#PermittedToSetUsergroups:checked').val() ==="1"){
				jQuery('#usergroupControl').show();
			}else{
				jQuery('#usergroupControl').hide();
			}
		});
		
		jQuery('[name="PermittedToControlCountryRights"]').on('change', function(){
			if (jQuery('#PermittedToControlCountryRights:checked').val() ==="1"){
				jQuery('#countryControl').show();
			}else{
				jQuery('#countryControl').hide();
			}
		});
		
		jQuery('[name="allowAutoCreate"]').on('change', function(){
			if (jQuery('#allowAutoCreate:checked').val() ==="1"){
				jQuery('#autocreateOptions').show();
			}else{
				jQuery('#autocreateOptions').hide();
			}
		});
	    
	    jQuery('[name="autoCreate_SelectLocOrg"]').on('change', function(){
			if (jQuery('#autoCreate_SelectLocOrg:checked').val() ==="1"){
				jQuery('#selectLocOrgDiv').show();
				jQuery('#detectLocOrgDiv').hide();
			}else{
				jQuery('#selectLocOrgDiv').hide();
				jQuery('#detectLocOrgDiv').show();
			}
		});
	    
	});
	

