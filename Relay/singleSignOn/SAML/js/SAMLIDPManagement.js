function ensureSufficientTableSpace(changedSelect) {
	var tableRow=changedSelect.closest("tr");
	var table=changedSelect.closest("table");

	
	var containingTable = tableRow.closest("table");
	var rowsInTable=containingTable.rows.length;
	var thisRowIndex=tableRow.sectionRowIndex;
	
	
	//must be before the callback in case of this being a deletion
	var tableRowParent=jQuery(tableRow).parent();
	
	if ((thisRowIndex+1)===rowsInTable){ //i.e. this is the last row in table
		console.log('add');
		jQuery.get('/webservices/callwebservice.cfc?wsdl&method=callWebService&webserviceName=samlWS&methodName=getNewMappingRow&returnFormat=plain', function(data) {
			tableRowParent.append(jQuery(data));  
		});
		
		
	}
	
}

