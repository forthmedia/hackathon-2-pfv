
<cfscript>
	helpBackingBean=new HelpBackingBean();
	headers=GetHttpRequestData().headers;
	plainTextJsonReturn="";

	if (StructKeyExists(headers,"authorization")){
		if (StructKeyExists(url,"endpoint") OR  StructKeyExists(form,"endpoint")){
			 passedParameters=structNew();
			 StructAppend(passedParameters,form);
			 StructAppend(passedParameters,url);


			 scopeManager=new singleSignOn.OAuth2.AuthorisationServer.ScopeManager(validateClient=false, searchCustomEndpoints=false, scopeSearchLocations_dotNotation=["singleSignOn.LaunchPadSSO.endpoints"]);
			 dataGenerator=new singleSignOn.OAuth2.AuthorisationServer.DataGenerator(requirehttps="false",scopeManager=scopeManager,validateClient=false );
			 dataReturn=dataGenerator.processDataRequest(headers.authorization,passedParameters.endpoint,passedParameters);
			 
			 if (dataReturn.isOk){
			 	plainTextJsonReturn=dataReturn.json;
			 	
			 }else{
			 	if (structKeyExists(dataReturn, "explanation")){
			 		plainTextJsonReturn='{ "success":"false", "message": "#dataReturn.explanation# - Full explanation shown because fullErrors option has been selected in settings" }';
			 	}else{
			 		plainTextJsonReturn='{ "success":"false", "message": "Invalid request" }';
			 	}
			 }
			 
			 
		}else{
			plainTextJsonReturn='{ "success":"false", "message": "endpoint not sent as a url parameter or form parameter" }';
		}
	}else{
		plainTextJsonReturn='{ "success":"false", "message": "Authorisation not sent as a header" }';
	}
	encryptedTextJsonReturn=application.com.encryption.EncryptStringAES(StringToEncrypt=plainTextJsonReturn,Passphrase=helpBackingBean.getLaunchPadPassPhrase());

	
</cfscript>

<!--- <cfcontent reset="true"> doesn't clear things like scripts so we have to save content then set the cfcontent'--->
<cfcontent type="text/plain" variable="#toBinary(toBase64(encryptedTextJsonReturn))#" /><cfabort/>


