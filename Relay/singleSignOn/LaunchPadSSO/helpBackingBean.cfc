/**
 * helpBackingBean
 *
 * @author Richard.Tingle
 * @date 10/09/15
 **/
component accessors=true output=false persistent=false {


	public any function init(string launchPadDomain=application.com.settings.getSetting("launchPad.domain"), string launchPadPassPhrase="E790B493BC6A48DE") output="false" {
		this.launchPadDomain=arguments.launchPadDomain;
		this.launchPadPassPhrase=launchPadPassPhrase;

		return this;
	}


	public string function getSSOLink(required numeric personID){
		var siteVersion  = application.com.relayCurrentSite.getSiteVersion();
		var emailAddress=application.com.relayEntity.getEntity(entityID=personID, entityTypeID=application.EntityTypeID.person, fieldList="email").recordSet.email;
		return application.com.login.createSSOLink(email=emailAddress,type=siteVersion.major&"."&siteVersion.minor,passPhrase=this.launchPadPassPhrase,lvm=10,sourceID="RW",siteURL=this.launchPadDomain);


	}
	


	public string function getEncryptedOAuthPacket(required numeric personID){
		var token=issueTokenForLaunchPad(personID);
		var returnStruct=structNew();
		returnStruct.OAuthToken=token.getTokenValue();

		returnStruct.dataEndpoint=request.currentSite.protocolAndDomain & "/singleSignOn/LaunchPadSSO/dataEndpoint.cfm";
		returnStruct.availableScopes=token.getScope();
		
		var returnJSON= serializeJSON(returnStruct);
		var returnEncrypted=application.com.encryption.EncryptStringAES(StringToEncrypt=returnJSON,Passphrase=this.getLaunchPadPassPhrase());

		return stringToHex(returnEncrypted);
	}
	private function stringToHex( String stringValue ){
        var binaryValue = stringToBinary( stringValue );
        var hexValue = binaryEncode( binaryValue, "hex" );
        return( lcase( hexValue ) );
    }
    private function stringToBinary( String stringValue ){
        var base64Value = toBase64( stringValue );
        var binaryValue = toBinary( base64Value );
        return( binaryValue );
    }

	/**
	 * Attempts to ensure that the requested person exists on launch pad (and is not duplicated). If it is not able to it will
	 * return false and log the failure under type Launch Pad Failure
	 **/
	public boolean function ensurePersonExistsOnLaunchPad(required numeric personID) output="false" {

		var sessionID=getSessionID();
		var emailAddress=application.com.relayEntity.getEntity(entityID=personID, entityTypeID=application.EntityTypeID.person, fieldList="email").recordSet.email;
		if (emailAddress EQ ""){
			throw (message = "Person ID " & personID & " either has no email address, doesn't exist, or we don't have permissions to see them", type = "Launch Pad Failure" );
		}

		var numberOfPeople=getNumberOfPeopleOnLaunchPadWithEmailAddress(emailAddress);

		if (numberOfPeople EQ 0){
			//they don't exist, we need to create them, return if we are successfull (is logged if we aren't')
			return createPersonOnLaunchPad(personID);
		}else if (numberOfPeople>1){
			//they're duplicated, we can't complete the SSO to a specific person, fall back to the generic user and log an error
			errorStruct=structNew();
			errorStruct.emailAddress=emailAddress;
			errorStruct.personID=arguments.personID;
			errorStruct.message("Could not complete launchPad SSO due to duplicated email address on launchpad side, fell back to generic user");
			application.com.errorHandler.recordRelayError_Warning(Severity="ERROR", Message=errorStruct.message, WarningStructure=errorStruct, type = "Launch Pad Failure" );
			return false;
		}else{
			return true; //the person already exists on the target system
		}


	}

	public string function getLaunchPadPassPhrase() output="false"{
		return this.launchPadPassPhrase;
	}
	
	/**
	 * Returns a clientless token that launchpad can use to obtain extra information about the user
	 **/
	public any function issueTokenForLaunchPad(required numeric personID){
		var token=new singleSignOn.OAuth2.AuthorisationServer.OAuth2Token(validity_seconds=300);
		token.setScope("relayware_metadata user_permissions usergroups launchpad_user_data");
		token.setClientID("CLIENTLESS");
		token.setPersonID(arguments.personID);
		token.persistToDatabase();
		return token;
	}


}
