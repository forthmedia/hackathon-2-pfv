/**
 * basic_user_data
 *
 * @author Richard.Tingle
 * @date 15/09/15
 **/
component implements="Relay.singleSignOn.OAuth2.AuthorisationServer.EndpointTemplate" accessors=true output=false persistent=false {

	public any function init(){
		return this;
	}

	public struct function processRequest(required numeric personID, required struct endPointArguments){
		var returnStruct=structNew();
		returnStruct.relaywareVersionNumber=application.com.relayCurrentSite.getSiteFullVersionString(); 
		return returnStruct;
	}

	/**
	 * Return the minimum fields that the end point expects to function correctly. Expected to return a
	 * struct of key - key description pairs
	 **/
	public struct function getRequiredFields(){
		return structNew(); //no required fields
	}


	/**
	 * Used in UI documentation
	 **/
	public string function getDescription(){
		return "Metadata pertaining to the Relayware instance itself";
	}
	
	public string function getScopeName(){
		return this.scopeName;
	}

	public void function setScopeName(required string scopeName){
		this.scopeName=arguments.scopeName;
	}

}