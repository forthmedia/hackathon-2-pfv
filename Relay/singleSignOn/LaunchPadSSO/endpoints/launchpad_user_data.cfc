/**
 * basic_user_data
 *
 * @author Richard.Tingle
 * @date 15/09/15
 **/
component extends="Relay.singleSignOn.OAuth2.AuthorisationServer.PersonOrgLocDataEndpoint" accessors=true output=false persistent=false {
	public struct function init(){
		super.init('firstname,email,lastname,language','SiteName,crmLocID,Address1,Address2,Address3,Address4,Address5,PostalCode,CountryDescription,CountryID','OrganisationName,OrganisationTypeID,OrgURL');
		return this;
	}

	public any function queryData(required string personID){
		returnStruct=super.queryData(personID);
		
		if (structkeyExists(returnStruct,"CountryID")){
			//add the iso code for compatibibilty
			returnStruct.countryISOCode=application.countryiso[returnStruct.CountryID];
		}
		
		return returnStruct;
	}

}