/**
 * basic_user_data
 *
 * @author Richard.Tingle
 * @date 15/09/15
 **/
component implements="Relay.singleSignOn.OAuth2.AuthorisationServer.EndpointTemplate" accessors=true output=false persistent=false {

	public any function init(){
		return this;
	}
	

	public struct function processRequest(required numeric personID, required struct endPointArguments){
		var groupsQuery=new Query();
		groupsQuery.setDatasource(application.siteDataSource); 
		groupsQuery.setName("groupsQuery");
		groupsQuery.addParam(name="personID",value=arguments.personID,cfsqltype="cf_sql_integer"); 
		var result=groupsQuery.execute(sql="select usergroup.UserGroupID, usergroup.Name,usergroup.UserGroupType from usergroup join RightsGroup on usergroup.UserGroupID=RightsGroup.UserGroupID where RightsGroup.PersonID=:personID ").getResult(); 
		var returnStruct=structNew();
		
		for (row = 1 ; row LTE result.RecordCount ; row = (row + 1)){
        	var userGroupData=structNew();
        	userGroupData["Name"]=result["Name"][row];
        	userGroupData["UserGroupType"]=result["UserGroupType"][row];
        	userGroupData["UserGroupID"]=result["UserGroupID"][row];
			returnStruct[result["Name"][row]]=userGroupData;
        }
        
        
        return returnStruct;
	}

	/**
	 * Return the minimum fields that the end point expects to function correctly. Expected to return a
	 * struct of key - key description pairs
	 **/
	public struct function getRequiredFields(){
		return structNew(); //no required fields
	}


	/**
	 * Used in UI documentation
	 **/
	public string function getDescription(){
		return "Usergroups of the user";
	}
	
	public string function getScopeName(){
		return this.scopeName;
	}

	public void function setScopeName(required string scopeName){
		this.scopeName=arguments.scopeName;
	}

}