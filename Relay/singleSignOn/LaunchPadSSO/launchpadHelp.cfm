<cfscript>
	//allow a token to specifiy where we go on launchpad
	if (structKeyExists(url,"launchpadURLToken")){
		//pull the token out of the db and get the url
		
		token=new com.tokens.token(tokenTypeTextID="launchpadURLToken", token=url.launchpadURLToken);
		token.pullFromDatabase();
		launchPadDestinationPage=token.get("url");
	}

</cfscript>


<cfparam name="launchPadDestinationPage" default="?eid=HelpLandingPage">


<cfset helpBackingBean=new helpBackingBean()>
<cfset ssoLink=helpBackingBean.getSSOLink(request.relayCurrentUser.personID)>
<cfset oauthEncryptedPacket=helpBackingBean.getEncryptedOAuthPacket(request.relayCurrentUser.personID)>
<cfset helpURL=application.com.settings.getSetting("launchPad.domain")>


<cflocation url="#helpUrl##launchPadDestinationPage#&hl=#ssoLink#&OAuthPacket=#oauthEncryptedPacket#" addtoken="false">