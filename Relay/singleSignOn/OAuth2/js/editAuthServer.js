/**
 * Created by martin.elgie on 16/11/2016.
 */

function FormSubmit() {
    updateRequiredAttributesForMappingTable();
    collapseTableToField();
    triggerFormSubmit(jQuery('#oAuthServer'));
}

function toPartnerCloudMode(){
    jQuery(".partnerCloudOptions").show();
    jQuery(".portalOptions").hide();
    jQuery(".internalRedirectURL").show();
    jQuery(".externalRedirectURL").hide();
}

function toPortalMode(){
    jQuery(".partnerCloudOptions").hide();
    jQuery(".portalOptions").show();
    jQuery(".internalRedirectURL").hide();
    jQuery(".externalRedirectURL").show();
}
function deleteItem() {
    var shouldDelete = confirm(phr.OAuth2_server_areYouSureYouWantToDelete);
    if (shouldDelete) {
        triggerFormSubmit(jQuery('#deletionForm'));
    }
}

function showAutocreateOptions() {
    jQuery(".autocreateOptions").show();
}

function hideAutocreateOptions() {
    jQuery(".autocreateOptions").hide();
}

function checkAutoRegButton() {
    if ((jQuery('#internalOnlyAuth:checked').val() !="1") && 
        (jQuery('#input_autocreate:checked').val() ==="1")) {
        jQuery(".autoRegOptions").show();
    } else {
        jQuery(".autoRegOptions").hide();
    }
}

function updateRequiredAttributesForMappingTable() {
    var mappingTable=jQuery('#mappingTable');
    var mappingRows=mappingTable.find('tr');

     
     for(var i=0; i<=mappingRows.length; i++){
        var mappingRow=jQuery(mappingRows.get(i));
        var mappingFieldRelayware=mappingRow.find('[name="mappingFieldRelayware"]');
        
        if (jQuery('#allowAutoCreate:checked').val()==="1" && mappingFieldRelayware.val() != null && mappingFieldRelayware.val() !==""){
            //mark the mapping as required
            mappingRow.find('[name="mappingValue"]').prop('required',true);
        }else{
            //mark the mapping as not required
            mappingRow.find('[name="mappingValue"]').removeAttr('required');    
        }
        
     }
    
    
}

function collapseTableToField(){
    //this collapses the entire mappings datable down to json
    var collapsedData={"collapsedData":[]
        };

    var allTableDataRows=jQuery('#mappingTable').find('tr');
    
    for (i = 0; i < allTableDataRows.length; ++i) {
        var dataRow=allTableDataRows[i];
        
        var mappingFieldRelayware=jQuery(dataRow).find('[name=mappingFieldRelayware]');
        var currentMappingFromAssertion=jQuery(dataRow).find('[name=currentMappingFromAssertion]');
        var mappingValue=jQuery(dataRow).find('[name=mappingValue]');
        
        if (mappingFieldRelayware.val() != null && mappingFieldRelayware.val() !="" ){          
            //append the new mapping
            collapsedData.collapsedData.push({
                                        mappingFieldRelayware:mappingFieldRelayware.val(),
                                        currentMappingFromAssertion:currentMappingFromAssertion.val(),
                                        mappingValue:mappingValue.val()
                                        });
        }
    }

    jQuery("#mappingTable_collapsed").val(JSON.stringify(collapsedData));
}

/*Manage the show hide behaviour*/
jQuery(document).ready(function(jQuery) {

    jQuery('[name="internalOnlyAuth"]').on('change', function(){
        if (jQuery('#internalOnlyAuth:checked').val() ==="1"){
            toPartnerCloudMode();
        }else{
            toPortalMode();
        }
        checkAutoRegButton();
    });

    jQuery('[name="input_authorised_sites"]').on('change', function(){
        jQuery('#input_authorised_sites > option').each(function() {
            jQuery(".internalUrl" + this.value).hide();
        })
        jQuery('#input_authorised_sites > option:selected').each(function() {
            jQuery(".internalUrl" + this.value).show();
        })
        if (jQuery('#input_authorised_sites > option:selected').size() > 0 ||
            jQuery('#internalOnlyAuth:checked').val() ==="1") {
            jQuery(".noExternalSite").hide();
        } else {
            jQuery(".noExternalSite").show();
        }

    });

    jQuery('[name="input_autocreate"]').on('change', function(){
        checkAutoRegButton();
        if (jQuery('#input_autocreate:checked').val() ==="1") {
            showAutocreateOptions();
        } else {
            hideAutocreateOptions();
        }
    });

    jQuery('[name="autoCreate_SelectLocOrg"]').on('change', function(){
        checkAutoRegButton();
        if (jQuery('#autoCreate_SelectLocOrg:checked').val() ==="1") {
            jQuery("#selectLocOrgDiv").show();
            jQuery("#detectLocOrgDiv").hide();
        } else {
            jQuery("#detectLocOrgDiv").show();
            jQuery("#selectLocOrgDiv").hide();
        }
    });

    jQuery('[name="PermittedToSetUsergroups"]').on('change', function(){
        if (jQuery('#PermittedToSetUsergroups:checked').val() ==="1"){
            jQuery('#usergroupControl').show();
        }else{
            jQuery('#usergroupControl').hide();
        }
    });
    
    jQuery('[name="PermittedToControlCountryRights"]').on('change', function(){
        if (jQuery('#PermittedToControlCountryRights:checked').val() ==="1"){
            jQuery('#countryControl').show();
        }else{
            jQuery('#countryControl').hide();
        }
    });
});