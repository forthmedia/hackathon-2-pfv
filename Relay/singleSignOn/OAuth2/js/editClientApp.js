/**
*	Created by Martin Elgie 2016/12/14 (ymd)
*/

function FormSubmit() {
    triggerFormSubmit(jQuery('#editClient'));
}

function saveScope() {
    triggerFormSubmit(jQuery('#edit'));
}

function deleteItem() {
    var shouldDelete = confirm(phr.OAuth2_client_areYouSureYouWantToDelete);
    if (shouldDelete) {
        triggerFormSubmit(jQuery('#deletionForm'));
    }
}