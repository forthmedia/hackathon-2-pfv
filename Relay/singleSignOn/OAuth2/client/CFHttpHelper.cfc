<!---
  Created by martin.elgie on 16/11/2016.
--->
<cfcomponent>
    <cffunction name="init" access="public" output="false" returntype="any">
        <cfset this.requireHttps=application.com.settings.getSetting("OAuth.Client.requireHTTPS")>
        <cfif not isBoolean(this.requireHTTPS)><cfset this.requireHTTPS=true></cfif>
        <cfset this.callbackEndpoint="/singleSignOn/OAuth2/client/endpoints/requestcallback.cfm">
        <cfset this.protocol=((findNoCase("https", cgi.server_protocol) NEQ 0) OR this.requireHttps) ?"https://":"http://">
        <cfset this.callbackURL=callbackUrlFromDomain(cgi.server_name)>
        <cfset this.registrationPage=this.protocol & cgi.server_name & "/singleSignOn/common/ssoLaunchedRegistration.cfm">
        <cfreturn this>
    </cffunction>

    <cffunction name="getPotentialCallbackUrls" access="public" output="false" returntype="struct"
                hint="Generate the URLs for all potential sites SSO can be used with">
        <cfset var resultStruct=structNew()>
        <cfloop query="#application.com.relayCurrentSite.getSiteDefs(external=true, internal= false)#">
                <cfset var domainArray=listToArray(#domains#)>
                <cfloop array="#domainArray#" index="local.oneDomain">
                    <cfset resultStruct[application.siteDefIndex[local.oneDomain].siteDefId]=callbackUrlFromDomain(local.oneDomain)>
                </cfloop>
        </cfloop>
        <cfreturn resultStruct>
    </cffunction>

    <cffunction name="callbackUrlFromDomain" access="private" output="false" returntype="string" 
                hint="Assigns current protocol and appends endpoint to the input domain">
        <cfargument name="domain" type="String" required="true">
        <cfif arguments.domain.endsWith("/")>
            <cfset arguments.domain=Left(arguments.domain, len(arguments.domain)-1)>
        </cfif>
        <cfreturn this.protocol & arguments.domain & this.callbackEndpoint>
    </cffunction>

    <cffunction name="getUrlRequested" access="public" output="false" returntype="string" 
                hint="Extract the request url, if it exists">
        <cfscript>
            var requested="";
            if (structKeyExists(url,"urlRequested")) {
                requested="/?" & url.urlRequested;
                if (requested.endsWith("&")) {
                    requested=Left(requested, len(requested)-1);
                }
            }
            return requested;
        </cfscript>
    </cffunction>

    <cffunction name="checkHttps" access="public" output="false" returntype="boolean"
                hint="Check the url for https, return false if required and not present">
        <cfargument name="urlInput" type="String" required="true">
        <cfreturn arguments.urlInput eq "" OR arguments.urlInput.toLowerCase().startsWith("https") OR NOT this.requireHttps>;
    </cffunction>

    <cffunction name="getParam" access="public" output="false" returntype="String"
            hint="Checks if the url (struct) argument contains the given key. Returns key value if true, blank string if not.">
        <cfargument name="inputStruct" type="struct" required="true">
        <cfargument name="parameters" type="array" required="true">
        <cfscript>
            var value = "";
            for (local.name in arguments.parameters) {
                if (structKeyExists(inputStruct, name)) {
                    value = inputStruct[name];
                    break;
                }
            }
            return value;
        </cfscript>
    </cffunction>

    <cffunction name="SendRequest" access="public" output="false" returntype="struct">
        <cfargument name="method" type="string" required="false" />
        <cfargument name="headers" type="struct" required="false" default=#structNew()# />
        <cfargument name="URI" type="string" required="false" />
        <cfargument name="formFields" type="struct" required="false" default=#structNew()# />
        <cfargument name="urlFields" type="struct" required="false" default=#structNew()# />

        <cfscript>
            var response = {};
        </cfscript>

        <cfhttp Method = "#arguments.method#"  Charset="utf-8" Url="#arguments.URI#" result="response" timeout="120" Throwonerror="true">

            <cfloop collection=#arguments.headers# item="local.header">
                <cfhttpparam type="header" name=#local.header# value="#arguments.headers[header]#">
            </cfloop>
            <cfloop collection=#arguments.urlFields# item="local.urlField">
                <cfhttpparam type="url" name=#local.urlField# value="#arguments.urlFields[urlField]#">
            </cfloop>
            <cfloop collection=#arguments.formFields# item="local.formField">
                <cfhttpparam type="Formfield" name=#local.formField# value="#arguments.formFields[formField]#">
            </cfloop>
        </cfhttp>

        <cfif structKeyExists(arguments.formFields, "client_id")>
            <cfset arguments.formFields.client_id="REDACTED - CLIENT SECRET SHOULD NOT BE IN LOGS">
        </cfif>
        <cfset this.lastRequest=arguments>
        <cfset this.lastResponse=response>
        <cfreturn response>
    </cffunction>
</cfcomponent>
