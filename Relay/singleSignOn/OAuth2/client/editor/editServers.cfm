<!---
  Created by martin.elgie on 01/11/2016.
--->

<cfif (structKeyExists(form,"SERVERTOEDIT") or 
		structKeyExists(url,"SERVERTOEDIT")) and not 
		structKeyExists(form,"deleteServer")>
    <cfinclude template="editServers_oneServer.cfm">
<cfelse>
    <cfinclude template="editServers_allServers.cfm">
</cfif>