<cfset application.com.request.setTopHead(pageTitle = "phr_OAuth2_serversTitle",topHeadCfm="/relay/singleSignOn/OAuth2/client/editor/OAuthClientTopNav.cfm", showAdd="true", showEdit="false")>

<cf_includejavascriptonce template = "/javascript/rwFormValidation.js">

<cfset backingBean=createObject("component","singleSignOn.common.editorBackingBean")>

<cfif structKeyExists(form,"deleteServer")>
    <cfset backingBean.deleteServer(serverID)>
    <cfset application.com.relayui.setMessage(message="phr_server_deleted", closeAfter="5")>
<cfelseif structKeyExists(url,"serverToDelete")>
    <cfset backingBean.deleteServer(url.serverToDelete)>
    <cfset application.com.relayui.setMessage(message="phr_server_deleted", closeAfter="5")>
</cfif>
<cfoutput>
    <cfset application.com.request.setDocumentH1(text="phr_OAuth2_manageServers")>
    <p>#htmlEditFormat("phr_OAuth2_editAllScreenText")#</p>
</cfoutput>

<CFQUERY NAME="serversQuery" datasource="#application.siteDataSource#">
    SELECT
        clientIdentifier, serverName, authoriseURL, 
        active,
        '<span class="deleteLink">Delete</span>' as deleteCol
    FROM
        OAuth2Server;
</CFQUERY>

<CF_tableFromQueryObject queryObject="#serversQuery#" 
    showTheseColumns="serverName,authoriseURL,active,deleteCol"
    ColumnHeadingList="OAuth2_serverName,OAuth2_authorise_url,active,delete"
    ColumnTranslationPrefix="phr_"
    HidePageControls="no"
    useInclude = "false"
    numRowsPerPage="400"
    booleanFormat="active"
    keyColumnList="serverName,deleteCol"
    keyColumnURLList="editServers.cfm?SERVERTOEDIT=,editServers.cfm?serverToDelete="
    keyColumnKeyList="clientIdentifier,clientIdentifier"
    columnTranslation="true">

