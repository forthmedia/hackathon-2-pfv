<cfparam name="showSave" type="boolean" default="false">
<cfparam name="showDelete" type="boolean" default="false">
<cfparam name="showAdd" type="boolean" default="false">
<cfparam name="showEdit" type="boolean" default="true">
<cfparam name="headerAttributes.pageTitle" type="string" default="">

<CF_RelayNavMenu>
    <cfif showAdd>
        <CF_RelayNavMenuItem MenuItemText="phr_add_server" CFTemplate="singleSignOn/OAuth2/client/editor/editServers_oneServer.cfm" target="mainSub" runInNewtab="true" tabName="tab_edit_server" tabTitle="phr_sys_nav_OAuth2_Authorisation_Servers">
    </cfif>
    <cfif showSave>
        <cf_relayNavMenuItem MenuItemText="phr_Save" CFTemplate="##" onclick="FormSubmit(this);">
    </cfif>
    <cfif showDelete>
        <cf_relayNavMenuItem MenuItemText="phr_Delete" CFTemplate="##" onclick="deleteItem(this); return false;">
    </cfif>
    <cfif showEdit>
        <CF_RelayNavMenuItem MenuItemText=#showSave ? "phr_back" : "phr_edit_servers"# CFTemplate="editServers.cfm">
    </cfif>

</CF_RelayNavMenu>


