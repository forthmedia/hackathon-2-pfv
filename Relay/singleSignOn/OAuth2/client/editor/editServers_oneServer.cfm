<cfset application.com.request.setTopHead(topHeadCfm="/relay/singleSignOn/OAuth2/client/editor/OAuthClientTopNav.cfm", showDelete="true", showSave="true")>

<cf_includeJavascriptOnce template = "/singleSignOn/OAuth2/js/editAuthServer.js" translate="true">
<cf_includejavascriptonce template = "/javascript/rwFormValidation.js">

<cfparam name="form.SERVERTOEDIT" default="">
<cfparam name="url.SERVERTOEDIT" default="">
<cfparam name="form.newServer" default="#len(form.SERVERTOEDIT) EQ 0 AND len(url.SERVERTOEDIT) EQ 0#">
<cfparam name="form.input_authorised_sites" default="#application.com.relayCurrentSite.getInternalSite().SITEDEFID#">

<cfset saving=structKeyExists(form,"input_server_name")/>
<cfset deleting=structKeyExists(form,"deleteCurrentServer")/>

<CFQUERY NAME="organsationsToSelect">
    select OrganisationID as dataValue, OrganisationName as displayValue from organisation
    where OrganisationID !=1 <!---not allowed to be RW --->
    and Active = 1
    order by OrganisationName
</CFQUERY>

<cfscript>
    editorBackingBean=new singleSignOn.common.editorBackingBean();

    // New blank server object
    oAuthServer=new singleSignOn.OAuth2.client.OAuth2Server();

    // Object already exists so retrieve the saved copy
    if (!saving) {
        if (form.SERVERTOEDIT NEQ ""){
            oAuthServer=editorBackingBean.retrieveServer(form.SERVERTOEDIT);
        }else if(url.SERVERTOEDIT NEQ "") {
            oAuthServer=editorBackingBean.retrieveServer(url.SERVERTOEDIT);
        }

    } else {
        if (form.internalOnlyAuth){
            form.input_authorised_sites=application.com.relayCurrentSite.getInternalSite().SITEDEFID;
        }

        oAuthServer.setServerName(form.input_server_name);
        oAuthServer.setClientID(form.input_client_id_hidden);
        oAuthServer.setClientSecret(form.input_client_secret_hidden);
        oAuthServer.setAuthoriseURL(form.input_auth_URI);
        oAuthServer.setTokenURL(form.input_token_URI);
        oAuthServer.setDataURL(form.input_data_URI);
        oAuthServer.setScope(form.input_scope);
        oAuthServer.setTokenPrefix(form.input_token_prefix);
        oAuthServer.setServerUID(form.input_server_unique_id);
        oAuthServer.setRelaywareUID(form.input_relayware_unique_id);
        oAuthServer.setAuthorisedSites(form.input_authorised_sites);
        oAuthServer.setActive(form.input_active);

        if (form.input_autocreate) {
            // Empty multiselects are missing form fields, so set defaults to be safe
            param name="form.usergroupsPermittedToControl" default="";
            param name="form.startingUsergroupCountries" default="";
            param name="form.startingUsergroups" default="";

            oAuthServer.setAutoCreate(form.input_autocreate);
            oAuthServer.setAutoregister(form.input_autoregister);
            oAuthServer.setAutoCreate_SelectLocOrg(form.autoCreate_SelectLocOrg);
            oAuthServer.setAutoCreate_selectedOrganisationID(form.autoCreate_selectedOrganisationID);
            oAuthServer.setAutoCreate_selectedLocationID(autoCreate_selectedLocationID);
            oAuthServer.setAutoCreate_detectedLoc_RelaywareField(autoCreate_detectedLoc_RelaywareField);
            oAuthServer.setAutoCreate_detectedLoc_ForeignField(autoCreate_detectedLoc_ForeignField);

            oAuthServer.setOnCreationPortalPersonApprovalStatus(form.onCreationPortalPersonApprovalStatus);
            oAuthServer.setUsergroupCountriesAtPersonCreation(form.startingUsergroupCountries);
            oAuthServer.setUsergroupsAtPersonCreation(form.startingUsergroups);
            oAuthServer.setCanSetUsergroups(form.PermittedToSetUsergroups);
            oAuthServer.setPermittedToControlCountryRights(form.PermittedToControlCountryRights);
            oAuthServer.setAssertionAttributeToControlUsergroups(form.assertionAttributeToControlUsergroups);
            oAuthServer.setUsergroupsPermittedToControl(form.usergroupsPermittedToControl);
            oAuthServer.setAssertionAttributeToControlCountryRights(form.assertionAttributeToControlCountryRights);


            // Handle mappings
            oAuthServer.clearMappings(); //clear all existing mappings and add new ones
            mappings=DeserializeJSON(mappingTable_collapsed).collapsedData;

        }

        if (oAuthServer.isValidForSave(editorBackingBean)) {
            // Save to database
            oAuthServer.persistToDatabase();
            application.com.relayui.setMessage(message="phr_OAuth2_auth_server_saved", closeAfter="5");
        } else {
            application.com.relayui.setMessage(message="phr_OAuth2_auth_server_not_saved", closeAfter="5", type="warning");
        }

        // Because of the foreign key we need to create the server DB entry before saving mappings
        if (form.input_autocreate) {
            for(i=1;i<=arrayLen(mappings);i++){
                mapping=mappings[i];
                oAuthServer.addMapping(mapping.mappingFieldRelayware,mapping.currentMappingFromAssertion, mapping.mappingValue);
            }
        }
    }

    attributeManager=new singleSignOn.common.AttributeManager();
    personFieldsArray=attributeManager.getPotentialAttributesForEntity(entityTypeID=application.entityTypeID.person);
    personFields=ArrayToList(personFieldsArray);
    personFieldsWithBlank=" ,"&personFields;
    locationFields=ArrayToList(attributeManager.getPotentialAttributesForEntity(entityTypeID=application.entityTypeID.location));
    essentialPersonFieldsArray=attributeManager.getRequiredAttributesForEntity(application.entityTypeID.person);
    internalOnlyAuth=oAuthServer.getAuthorisedSites() EQ application.com.relayCurrentSite.getInternalSite().SITEDEFID;
</cfscript>

<!--- A hidden form which is launched to delete the server entry  --->
<form id="deletionForm" method="post">
    <cf_input type="hidden" name="serverID" value=#oAuthServer.getClientID()#>
    <cf_input type="hidden" name="deleteServer" value="true">
</form>

<cfif form.newServer>
        <cfset application.com.request.setDocumentH1(text="phr_OAuth2_create_server")>
        <cfoutput>
            <p>#htmlEditFormat("phr_OAuth2_editNew")#</p>
        </cfoutput>
<cfelse>
        <cfset application.com.request.setDocumentH1(text="phr_OAuth2_edit_server")>
        <cfoutput>
            <p>#htmlEditFormat("phr_OAuth2_editExisting")#</p>
        </cfoutput>
</cfif>


<cfset httpHelper = new singleSignOn.OAuth2.client.CFHttpHelper()>
<cfset redirectUrlStruct=httpHelper.getPotentialCallbackUrls()>
<cfif oAuthServer.getAuthorisedSites() eq "">
    <cfoutput>
            <p>#htmlEditFormat("phr_OAuth2_noRedirectURL ")#
                <span class="oAuthItalic">
                    #htmlEditFormat("'phr_OAuth2_siteType'")#
                </span></p>
    </cfoutput>
<cfelse>
    <cfoutput>
            <p>#htmlEditFormat("phr_OAuth2_redirectionURLis:")#</p>
    </cfoutput>
    <cfif oAuthServer.getAuthorisedSites() eq application.com.relayCurrentSite.getInternalSite().SITEDEFID>
        <cfoutput>
            #htmlEditFormat(httpHelper.protocol & application.com.relayCurrentSite.getInternalSite().maindomain & httpHelper.callbackEndpoint)#
        </cfoutput>
        <br>
    <cfelse>
        <cfset local.sites=listToArray(#oAuthServer.getAuthorisedSites()#)>
        <cfloop array="#local.sites#" index="site">
            <cfoutput>
                #htmlEditFormat(httpHelper.protocol & application.sitedefbyid[site].maindomain & httpHelper.callbackEndpoint)#
            </cfoutput>
            <br>            
        </cfloop>
    </cfif>
    <br>
</cfif>

<form id="oAuthServer" method="post">
        <div id="editServersOneServer" class="grey-box-top">
        <CF_relayFormElementDisplay relayFormElementType="text" required="true" maxlength="256" class="form-control" currentValue="#oAuthServer.getServerName()#" fieldName="input_server_name" label="phr_OAuth2_serverName" noteText="phr_OAuth2_serverNameHint">

        <cf_input type="hidden" class="form-control" id="input_client_id" name="SERVERTOEDIT" currentValue="#oAuthServer.getClientId()#">
        <CF_relayFormElementDisplay relayFormElementType="text" required="true" maxlength="512" class="form-control" currentValue="#oAuthServer.getClientId()#" fieldName="input_client_id_hidden" label="phr_OAuth2_client_id" noteText="phr_OAuth2_clientIDHint">

        <cf_input type="hidden" class="form-control" id="input_client_secret" name="CLIENTSECRET" currentValue="#oAuthServer.getClientSecret()#">
        <CF_relayFormElementDisplay relayFormElementType="text" maxLength="512" required="true" class="form-control" currentValue="#oAuthServer.getClientSecret()#" fieldName="input_client_secret_hidden" label="phr_OAuth2_client_secret" noteText="phr_OAuth2_clientSecretHint">

        <CF_relayFormElementDisplay relayFormElementType="text" required="true" maxlength="512" class="form-control" currentValue="#oAuthServer.getAuthoriseURL()#" fieldName="input_auth_URI" label="phr_OAuth2_authorise_url" noteText="phr_OAuth2_authHint">

        <CF_relayFormElementDisplay relayFormElementType="text" required="true" maxlength="512" class="form-control" currentValue="#oAuthServer.getTokenURL()#" label="phr_OAuth2_token_url" fieldName="input_token_URI" noteText="phr_OAuth2_tokenHint">

        <CF_relayFormElementDisplay relayFormElementType="text" maxlength="512" class="form-control" currentValue="#oAuthServer.getDataURL()#" fieldName="input_data_URI" label="phr_OAuth2_data_url" noteText="phr_OAuth2_dataHint">

        <CF_relayFormElementDisplay relayFormElementType="text" maxlength="256" class="form-control" currentValue="#oAuthServer.getScope()#" fieldName="input_scope" label="phr_OAuth2_scope" noteText="phr_OAuth2_scopeHint">

        <CF_relayFormElementDisplay relayFormElementType="text" maxlength="256" class="form-control" currentValue="#oAuthServer.getTokenPrefix()#" fieldName="input_token_prefix" label="phr_OAuth2_token_Prefix" noteText="phr_OAuth2_prefixHint">

        <CF_relayFormElementDisplay relayFormElementType="text" required="true" maxlength="512" class="form-control" currentValue="#oAuthServer.getServerUID()#" fieldName="input_server_unique_id" label="phr_OAuth2_server_uid" noteText="phr_OAuth2_serverUidHint">

        <CF_relayFormElementDisplay relayFormElementType="select" currentValue="#oAuthServer.getRelaywareUID()#" list="#personFields#" fieldName="input_relayware_unique_id" label="phr_OAuth2_relayware_uid" required="true" noteText="phr_OAuth2_relayUidHint">

        <CF_relayFormElementDisplay relayFormElementType="radio" currentValue="#internalOnlyAuth#" fieldName="internalOnlyAuth" label="phr_OAuth2_siteType" list="1#application.delim1#phr_PartnerCloud,0#application.delim1#phr_portal" noteText="phr_OAuth2_serverTypeHint">

                <div class="portalOptions"<cfif internalOnlyAuth>style="display: none;"</cfif>>
        <CF_relayFormElementDisplay relayFormElementType="select" multiple="true" currentValue="#oAuthServer.getAuthorisedSites()#" fieldName="input_authorised_sites" label="phr_OAuth2_authorisedSites"  query="#application.com.relayCurrentSite.getSiteDefs(internal = false)#" value="siteDefID" display="title" noteText="phr_OAuth2_portalListHint">
        </div>
        <cfoutput>
            <div class="oAuthBoldItalic">
            <p>#htmlEditFormat("phr_OAuth2_redirectionURLis:")#</p>
            </div>
        </cfoutput>

        <!--- JS hidden/shown fields to display redirection URLs for selected sites --->
        <div class="internalRedirectURL" <cfif not internalOnlyAuth>style="display: none;"</cfif>>
            <cfoutput>
                <p>#htmlEditFormat(httpHelper.protocol & application.com.relayCurrentSite.getInternalSite().maindomain & httpHelper.callbackEndpoint)#</p>
            </cfoutput>
        </div>
        <div class="externalRedirectURL" <cfif internalOnlyAuth>style="display: none;"</cfif>>
            <cfloop collection=#redirectUrlStruct# item="key">
                <cfoutput>
                    <div class="internalUrl#key#">
                        <p>#htmlEditFormat(redirectUrlStruct[key])#</p>
                    </div>
                </cfoutput>
            </cfloop>
            <div class="noExternalSite" <cfif input_authorised_sites eq "">style="display: none"</cfif>>
                <cfoutput><p>#htmlEditFormat("phr_OAuth2_selectSiteForUrl")#</p></cfoutput>
            </div>
        </div>

        <CF_relayFormElementDisplay relayFormElementType="radio" currentValue="#oAuthServer.isActive()#" fieldName="input_active" label="phr_OAuth2_setStatus" list="1#application.delim1#phr_active,0#application.delim1#phr_inactive" noteText="phr_OAuth2_activeHint">
        </div>


        <CF_relayFormElementDisplay relayFormElementType="radio" currentValue="#oAuthServer.isAutocreate()#" fieldName="input_autocreate" label="phr_OAuth2_autocreate" list="1#application.delim1#phr_yes,0#application.delim1#phr_no" noteText="phr_OAuth2_autocreateHint">

        <!--- Start of autocreate user options --->
        <cf_relayFormDisplay>
        <div class="autocreateOptions"<cfif !oAuthServer.isAutocreate()>style="display: none;"</cfif>>
        <div class="grey-box-top">
                    <cfoutput>
            <h2>#htmlEditFormat("phr_OAuth2_autocreationOptions")#</h2>
        </cfoutput>

        <CF_relayFormElementDisplay relayFormElementType="radio" currentValue="#oAuthServer.getAutoCreate_SelectLocOrg()#" fieldName="autoCreate_SelectLocOrg" label="phr_OAuth2_creationOrganisationSelectionMethod" list="1#application.delim1#phr_OAuth2_SelectLocOrg,0#application.delim1#phr_OAuth2_DetectLocOrg">

        <div id="selectLocOrgDiv"
            <cfif not oAuthServer.getAutoCreate_SelectLocOrg()>style="display: none;"</cfif>>
            <CF_relayFormElementDisplay relayFormElementType="select" currentValue="#oAuthServer.getAutoCreate_selectedOrganisationID()#" fieldName="autoCreate_selectedOrganisationID" label="phr_OAuth2_selectedOrganisation" query="#organsationsToSelect#">
            <CF_relayFormElementDisplay relayFormElementType="select" currentValue="#oAuthServer.getAutoCreate_selectedLocationID()#" fieldName="autoCreate_selectedLocationID" bindOnLoad="true" size="1" bindFunction="cfc:webservices.callWebService.callWebservice(webServiceName='samlWS',methodName='getLocationsAtOrganisation',organisationID={autoCreate_selectedOrganisationID})" label="phr_OAuth2_selectedLocation">
        </div>
        <div id="detectLocOrgDiv"
            <cfif oAuthServer.getAutoCreate_SelectLocOrg()>style="display: none;"</cfif>>
            <CF_relayFormElementDisplay relayFormElementType="select" currentValue="#oAuthServer.getAutoCreate_detectedLoc_RelaywareField()#" list="#locationFields#" fieldName="autoCreate_detectedLoc_RelaywareField" label="phr_OAuth2_Location_uniqueIdentifier_RWSide">
            <CF_relayFormElementDisplay relayFormElementType="text" currentValue="#oAuthServer.getAutoCreate_detectedLoc_ForeignField()#" fieldName="autoCreate_detectedLoc_ForeignField" label="phr_OAuth2_location_uniqueIdentifier_Assertion">
        </div>

        <cfset mappingsArray=oAuthServer.getMappings()>
        <CF_relayFormElementDisplay currentValue="" maxlength="4000" relayFormElementType="hidden" fieldName="mappingTable_collapsed" label="">
        <table id="mappingTable" class="table table-hover table-striped table-bordered">

            <tr>
                <cfoutput>
                    <th>#htmlEditFormat("phr_SAML_RelaywareField")#</th>
                    <th>#htmlEditFormat("phr_SAML_MappingMethod")#</th>
                    <th>#htmlEditFormat("phr_SAML_Value")#</th>
                    <th>#htmlEditFormat("phr_sys_Delete")#</th>
                </cfoutput>
            </tr>
            <!--- The essential items --->
            <cfloop array="#essentialPersonFieldsArray#" index="essentialItem">

                <cfset essentialMapping=oAuthServer.getMappingForRelaywareField(essentialItem, mappingsArray)>

                <cfif isNull(essentialMapping)>
                    <cf_SAMLMappingTableRow
                        currentField="#essentialItem#"
                        availableFields="#essentialItem#"
                        currentMapping=""
                        lockMappingType="true"
                        deletable="false"
                    >
                <cfelse>
                    <cf_SAMLMappingTableRow
                        currentField="#essentialItem#"
                        availableFields="#essentialItem#"
                        currentMapping="#essentialMapping#"
                        lockMappingType="true"
                        deletable="false"
                    >
                </cfif>


            </cfloop>

            <!--- The existing non essential items --->
            <cfloop array="#oAuthServer.getMappings()#" index="mapping">
                <cfif not ArrayContains(essentialPersonFieldsArray, mapping.getRelaywareField())>
                    <cf_SAMLMappingTableRow
                        currentField="#mapping.getRelaywareField()#"
                        availableFields="#personFieldsWithBlank#"
                        currentMappingFromAssertion="#mapping.isUseAssertion()#"
                        currentMapping="#mapping.getForeignValue()#"
                    >

                </cfif>


            </cfloop>

            <!--- A blank row for new items--->
            <cf_SAMLMappingTableRow
                currentField=""
                availableFields="#personFieldsWithBlank#"
                currentMappingFromAssertion="1"
                currentMapping=""
            >

        </table>

        <div class="partnerCloudOptions"
            <cfif not internalOnlyAuth>style="display: none;"</cfif>>
            <CF_relayFormElementDisplay relayFormElementType="select" multiple="true" currentValue="#oAuthServer.getUsergroupsAtPersonCreation()#" fieldName="startingUsergroups" label="phr_OAuth2_StartingUsergroups"  query="#application.com.relayUserGroup.getUserGroupsForSelect(userGroupIDsToExclude=4, userGroupTypesToExclude='personal')#" value="usergroupid" display="name">
            <CF_relayFormElementDisplay relayFormElementType="select" multiple="true" currentValue="#oAuthServer.getUsergroupCountriesAtPersonCreation()#" fieldName="startingUsergroupCountries" label="phr_OAuth2_StartingUsergroupCountries" query="#application.com.RelayCountries.getCountries()#" value="countryID" display="CountryDescription">
        </div>

        <div class="portalOptions"
            <cfif internalOnlyAuth>style="display: none;"</cfif>>
            <CF_relayFormElementDisplay relayFormElementType="select" currentValue="#oAuthServer.getOnCreationPortalPersonApprovalStatus(asString=false)#" fieldName="onCreationPortalPersonApprovalStatus" label="phr_OAuth2_onCreationPortalApprovalStatus" query="#application.com.flag.getFlagGroupFlags('PerApprovalStatus')#" value="FLAGID" display="TRANSLATEDNAME" >
        </div>

        <!--- Start of Internal Only usergroup/country permissions settings --->
        <div class="partnerCloudOptions"
            <cfif not internalOnlyAuth>style="display: none;"</cfif>
            >
        <CF_relayFormElementDisplay relayFormElementType="radio" currentValue="#oAuthServer.isPermittedToSetUsergroups()#" fieldName="PermittedToSetUsergroups" label="phr_OAuth2_allowAddUsergroups" list="1#application.delim1#phr_yes,0#application.delim1#phr_no">


        <div id="usergroupControl" class="grey-box-top"
            <cfif not oAuthServer.isPermittedToSetUsergroups()>style="display: none;"</cfif>>
            <cfoutput>
                <h2>#htmlEditFormat("phr_OAuth2_usergroupControl")#</h2>
            </cfoutput>
            <CF_relayFormElementDisplay relayFormElementType="text" currentValue="#oAuthServer.getAssertionAttributeToControlUsergroups()#" fieldName="assertionAttributeToControlUsergroups" label="phr_OAuth2_assertionAttributeToControlUsergroups">

            <CF_relayFormElementDisplay 
            relayFormElementType="select" 
            multiple="true" 
            currentValue="#oAuthServer.getUsergroupsPermittedToControl()#" 
            fieldName="usergroupsPermittedToControl" 
            label="phr_OAuth2_ControlledUsergroups"  
            query="#application.com.relayUserGroup.getUserGroupsForSelect(userGroupIDsToExclude=4, userGroupTypesToExclude='personal')#" 
            value="usergroupid" 
            display="name">
                
        </div>
            
        <CF_relayFormElementDisplay relayFormElementType="radio" currentValue="#oAuthServer.isPermittedToControlCountryRights()#" fieldName="PermittedToControlCountryRights" label="phr_OAuth2_allowControlLanguageRights" list="1#application.delim1#phr_yes,0#application.delim1#phr_no">
            
        <div id="countryControl" class="grey-box-top"
            <cfif not oAuthServer.isPermittedToControlcountryRights()>style="display: none;"</cfif>>
            <cfoutput>
                <H2>#htmlEditFormat("phr_OAuth2_countryControl")#</H2>
            </cfoutput>
            <CF_relayFormElementDisplay relayFormElementType="text" currentValue="#oAuthServer.getAssertionAttributeToControlCountryRights()#" fieldName="assertionAttributeToControlCountryRights" label="phr_OAuth2_assertionAttributeToControlCountryRights">

        </div>
        </div>
        <!--- End of Internal Only usergroup/country permissions settings --->


        <div class="autoRegOptions"<cfif internalOnlyAuth>style="display: none;"</cfif>>
        <CF_relayFormElementDisplay relayFormElementType="radio" currentValue="#oAuthServer.isAutoregister()#" fieldName="input_autoregister" label="phr_OAuth2_registerUser" list="1#application.delim1#phr_yes,0#application.delim1#phr_no" noteText="phr_OAuth2_autoregisterHint">
        </div>

        </div>
        <!--- End of autocreate user options --->
        </div>
        </cf_relayFormDisplay>
</form>
