/*
  Created by martin.elgie on 11/11/2016.
  Handles the authentication of users logging in with OAuth2
  Call functions as required to handle various authorisation and login flows
*/

component accessors=true output=false persistent=false {

    public void function init() {
        this.httpHelper = new singleSignOn.OAuth2.client.CFHttpHelper();
        this.serverHelper = new singleSignOn.common.editorBackingBean();
    }

    /**
    * @hint There is currently no requirement for supporting Authorisation Server
    * initiatied login for OAuth2, so running this filter won't log in a user
    */
    public struct function runFilter(required numeric personID, required boolean isInternal, required numeric level) output="false" {
        var result = {
            PersonID = personID, 
            Level = Level, 
            filterUsed=false};

        if (structKeyExists(url,"eid") and url.eid eq "SSOLogin") {
            clientID=this.httpHelper.getParam(url, ["clientID"]);
            if (clientID neq "") {
                authServer=this.serverHelper.retrieveServer(clientID);
            }
        }

        return result;
    }

    /**
    * @hint Returns the ID of the user with the specified field value, or an empty string if no matches or more than 1 are found
    */
    public String function getPersonIDFromUniqueField(required struct authServer, required String identifier) output="false" {
        var fieldName=authServer.getRelaywareUID();
        var personQuery=new Query();
        personQuery.addParam(name="serverValue",value=identifier,cfsqltype="CF_SQL_VARCHAR");
        var queryResponse = personQuery.execute(sql="SELECT personID FROM vPerson WHERE #fieldName# = :serverValue");


        var prefix=queryResponse.getPrefix();

        if (prefix.recordCount neq 1){
            return "";
        } else {
            var queryResult = queryResponse.getResult();
            return queryResult.personID;
        }
    }

    /**
    * Attempt to log the user in and return them to the page defined by state, set in OAuth2AuthLaunchRequestPost.cfm
    */
    public Struct function logInUserFromPersonID(required String personID, required String authoriser, string state="") {
        var loginInfo={
            personID=personID,
            filterUser=true,
            level=2,
            source="OAuth2:#authoriser#",
            doReinitialisation=true};
        var errorStruct=structNew();

        var validStateToken=false;
        if (arguments.state neq "") { // If the user tried to log into a portal page, redirect them there
            var stateStruct=decryptState(state);
            var validStateToken = structKeyExists(stateStruct,"token") and stateStruct.token eq cookie.OAuth2StateToken;
        }

        if (validStateToken) {
            var currentUser=new com.relayCurrentUser();
            if (request.currentSite.isInternal) {
                session.relaycurrentuserstructure.isValidRelayInternalUser = 1;
            }
            currentUser.setLoggedInPrivate(argumentCollection=loginInfo);
            if (session.relayCurrentUserStructure.isLoggedIn) {
                if (structKeyExists(stateStruct,"beginWorkflow") AND stateStruct.beginWorkflow neq "") {
                    location(url="/proc.cfm?prid=#stateStruct.beginWorkflow#", addToken=false);
                } else {
                    location(url=stateStruct.url, addToken=false);   
                }
            } else {
                errorStruct.explanation="User could not be logged in";
            }
        } else {
            errorStruct.explanation="An invalid state was returned.";
        }
        return errorStruct;
    }

    public Struct function requestAccessToken(required struct authServer) {
        var response="";
        var fields=structnew();
        fields.grant_type="authorization_code";
        fields.code=#code#;
        fields.redirect_uri=#this.httpHelper.callbackURL#;
        fields.client_id=#authServer.getClientID()#;
        fields.client_secret=#authServer.getClientSecret()#;
        var response=this.httpHelper.sendRequest(
            method="POST",
            headers=structnew(),
            URI=#authServer.getTokenURL()#,
            formFields=fields);

        var responseData=deserializeJSON(response.fileContent);
        var returnValue={accessToken=getAccessToken(responseData)};

        // Check for UID data being returned along with the access token
        var uid=this.httpHelper.getParam(responseData, [authServer.getServerUID()]);
        if (uid neq "") {
            returnValue.uid = uid;
            returnValue.responseData=responseData;
        }

        return returnValue;
    }

    public Struct function requestScopeData(required struct authServer, required String accessToken) {
        var header = structNew();
        var prefix=authServer.getTokenPrefix() eq "" ? "bearer" : authServer.getTokenPrefix();
        header.Authorization="#prefix# #accessToken#";
        var requestFields=structNew();
        var urlFields=structNew();
        urlFields.endpoint=authServer.getScope();
        var dataRequest=this.httpHelper.sendRequest(
            method="GET",
            headers=header,
            URI=#authServer.getDataURL()#,
            formFields=requestFields,
            urlFields=urlFields);

        return deserializeJSON(dataRequest.fileContent);
    }

    public boolean function checkForParameters(required struct URL, required array parameters) {
        var containsAll=true;
        for (word in parameters) {
            containsAll=containsAll and structKeyExists(URL, word);
        }
        return containsAll;
    }

    /**
     * @hint Returns the encrypted token for a state being passed to the auth server
     */
    public String function encryptState(required Struct state) {
        var jsonString = serializeJSON(arguments.state);
        return application.com.encryption.encryptMagicNumber(jsonString);
    }

    /**
     * @hint Returns the decrypted token for a state being passed to the auth server
     */
    public Struct function decryptState(required String jsonString) {
        var state = application.com.encryption.decryptMagicNumber(arguments.jsonString);
        return deserializeJSON(state);
    }

    public String function getAccessToken(required Struct response) {
        return this.httpHelper.getParam(arguments.response, ["oauth_token","access_token"]);
    }

    public String function getTokenSecret(required Struct response) {
        var secretField="oauth_token_secret";
            if (structKeyExists(arguments.response, secretField)) {
                return arguments.response[secretField];
            }
    }

    public String function getNoRequestAuthParameters(required Struct serverIn, required String state) {
        var value="?response_type=code";
        value&="&state=#arguments.state#";
        value&="&redirect_uri=#this.httpHelper.callbackURL#";
        value&="&client_id=#arguments.serverIn.getClientID()#";
        value&="&scope=#arguments.serverIn.getScope()#";
        return value;
    }

    public Struct function autocreatePersonIfPossible(required any dataStruct, required any authServer) {
        var result={created=false, personID=0, reason="", creationReport=structNew()};

        if (not arguments.authServer.isAutocreate()) {
            result.reason="Autocreation of users is not enabled for this authorisation server";
            return result;
        }

        var locationID=getPersonLocationID(arguments.authServer, arguments.dataStruct);
        if (locationId eq 0) {
            result.reason="OAuth2Error autocreate could not match the person location";
            return result;
        }

        var mappings=authServer.getMappings();

        var personDetails = {locationID=locationID};
        
        //only create user as an internal user if this is internal
        if (request.currentSite.isInternal){
            personDetails.loginExpires= DateAdd("yyyy",50,request.requestTimeODBC);
        }
        
        for (mapping in mappings){
            if (mapping.isUseAssertion()){
                var attributeName=mapping.getForeignValue();
                
                if (this.httpHelper.getParam(arguments.dataStruct, [attributeName]) neq ""){
                    personDetails[mapping.getRelaywareField()]=this.httpHelper.getParam(arguments.dataStruct, [attributeName]);
                }else{
                    result.reason="OAuth2 could not create a person as the assertion did not have a '#attributeName#' attribute, used for mapping the data of newly created people.";
                    return result;
                }

            }else{
                personDetails[mapping.getRelaywareField()]=mapping.getForeignValue(); //its a hard coded value
            }
        }

        var personCreationReport=application.com.relayEntity.insertPerson(personDetails=personDetails,setPassword="true",testLoggedInUserRights=false);

        if (personCreationReport.isOk){
            result.created=true;
            result.personID=personCreationReport.entityID;
            // Only create user as an internal user if this is internal
            if (request.currentSite.isInternal){
                // Apply any at creation and on assertion usergroups
                application.com.login.setPersonAsInternalUser(
                    personID=result.personID, 
                    loginExpiresDate=DateAdd("yyyy",50,request.requestTimeODBC), 
                    countryIDList=arguments.authServer.getUsergroupCountriesAtPersonCreation());


            var usergroupsToSet=listToArray(arguments.authServer.getUsergroupsAtPersonCreation());
            if (arguments.authServer.isPermittedToSetUsergroups()) {
                var usergroupsListField = arguments.authServer.getAssertionAttributeToControlUsergroups();
                if (structKeyExists(arguments.dataStruct,usergroupsListField)) {
                    var groupsArray=application.com.relayUserGroup.getUsergroupsIdsFromNames(arguments.dataStruct[usergroupsListField]);
                    var permittedArray=listToArray(arguments.authServer.getUsergroupsPermittedToControl());
                    
                    for (group in groupsArray) {
                        if (arrayContains(permittedArray,group)) {
                            arrayAppend(usergroupsToSet,group);
                        }
                    }
                }
            }
                

            for(var i=1;i<=arrayLen(usergroupsToSet);i++){
                application.com.relayUserGroup.addPersonToUserGroup(personID=result.personID,userGroup=usergroupsToSet[i]);
            }

            if (arguments.authServer.isPermittedToControlCountryRights()) { 
                var countryListField = arguments.authServer.getAssertionAttributeToControlCountryRights();
                if (structKeyExists(arguments.dataStruct,countryListField)) {
                    var countriesList=application.com.relayCountries.getCountryIdListFromISOList(arguments.dataStruct[countryListField]);
                    if (countriesList neq "") {
                        application.com.relayCountries.updateInternalUserCountryRights(internalPersonID=result.personID,countryIDList=countriesList);
                    }
                }
            }


                

            }else{
                //if on the portal it autoapproves the person
                application.com.flag.setbooleanflag(entityid=personCreationReport.entityID, flagtextid=authServer.getOnCreationPortalPersonApprovalStatus(), deleteOtherRadiosInGroup="true");
            }

            return result;

        }else{
            result.creationReport.personDetails=personDetails;
            result.creationReport.personCreationReport=personCreationReport;
            result.reason="An error occured while attempting to create this user.";
            return result;
        }
    } 
    /** End of autocreate person function **/

    /** Get registration mapping fields based on assertion data **/
    public struct function getRegistrationMappings(required any dataStruct, required any authServer) {
        var mappings={reason="",formFields=structNew(),showCols=""};
        var serverMappings=authServer.getMappings();

        // Location/Organisation fields are set on configuration so map all that associated data first
        var locationID=getPersonLocationID(arguments.authServer, arguments.dataStruct);
        if (locationID neq 0) {
            var queryService = new query();
            queryService.setSQL("
                SELECT
                    vlocation.CountryID,
                    Address1,Address2,Address3,Address4,Address5,PostalCode,
                    vlocation.OrganisationID,OrganisationName,AKA
                FROM
                    vlocation
                INNER JOIN
                    vOrganisation
                ON 
                    vOrganisation.OrganisationID=vlocation.OrganisationID
                WHERE
                    LocationID=:locationID
            ");
            queryService.addParam(name="locationID", value=#locationID#, CFSQLTYPE="CF_SQL_INTEGER");
            var queryResult=queryService.execute();
            mappings.countryID=queryResult.getResult().CountryID;
            mappings.formFields.insAddress1=queryResult.getResult().Address1;
            mappings.formFields.insAddress2=queryResult.getResult().Address2;
            mappings.formFields.insAddress3=queryResult.getResult().Address3;
            mappings.formFields.insAddress4=queryResult.getResult().Address4;
            mappings.formFields.insAddress5=queryResult.getResult().Address5;
            mappings.formFields.insPostalCode=queryResult.getResult().PostalCode;
            mappings.formFields.insOrganisationID=queryResult.getResult().OrganisationID;
            mappings.formFields.insSiteName=queryResult.getResult().OrganisationName;
            mappings.formFields.insaka=queryResult.getResult().AKA;

            mappings.formFields.lockDetails.lockAddress=true;

        } else {
            mappings.countryID=0;
        }

        mappings.formFields.insCountryID=mappings.countryID;

        var regFieldsArray = getStandardRegistrationFields();
        for (var map in serverMappings) {
            var attributeValue="";
            if (map.isUseAssertion()) {
                var attributeName=map.getForeignValue();
                if (this.httpHelper.getParam(arguments.dataStruct, [attributeName]) neq ""){
                    attributeValue=arguments.dataStruct[map.getForeignValue()];
                }
            } else {
                attributeValue=map.getForeignValue();
            }
            if (!arrayFindNoCase(regFieldsArray,map.getRelaywareField(false))) {
                mappings.autoPopulateSettings.postMappings[map.getRelaywareField()]=attributeValue;
            }
            mappings.formFields["ins"&map.getRelaywareField()]=attributeValue;
        }

        mappings.autoPopulateSettings.approvalStatus=arguments.authServer.getOnCreationPortalPersonApprovalStatus();
        mappings.autoPopulateSettings.authoriser=authServer.getServerName();

        // Show the standard tag version fields
        mappings.showCols="insSalutation,insFirstName,insLastName,insJobDesc,insEmail,insOfficePhone,insMobilePhone,insSiteName,insaka,insVATnumber,insAddress1,insAddress2,insAddress3,insAddress4,insAddress5,insPostalCode,insCountryID,insLocEmail";
        return mappings;
    }
    /** End of getting mapping fields for registration**/

    public void function beginRegistration(struct mappings) {
        cookie.mappingToken=encryptState(arguments.mappings);
        location(url="#this.httpHelper.registrationPage#", addToken=false);
    }

    private numeric function getPersonLocationID(required any authServer, required struct dataStruct) {
        var locationID=0;
        if (arguments.authServer.getAutoCreate_SelectLocOrg()){
            locationID=arguments.authServer.getAutoCreate_selectedLocationID();
        }else{
            var locationDetection_RelayareSide_parameter=arguments.authServer.getAutoCreate_detectedLoc_RelaywareField();
            var locationDetection_ForeignSide_parameter=arguments.authServer.getAutoCreate_detectedLoc_ForeignField();

            if (this.httpHelper.getParam(arguments.dataStruct, [locationDetection_ForeignSide_parameter]) neq "") {

                var matchCondition=application.com.security.queryObjectName(locationDetection_RelayareSide_parameter) & " = " & application.com.security.queryparam(CFSQLType="CF_SQL_VARCHAR", value=this.httpHelper.getParam(arguments.dataStruct, [locationDetection_ForeignSide_parameter]) );
                var relayEntityReport=application.com.relayEntity.getEntity(EntityTypeID=application.entityTypeID.location, whereClause=matchCondition, testLoggedInUserRights="false", fieldList="locationID");

                if (relayEntityReport.recordCount EQ 1){
                    locationID=relayEntityReport.recordSet.locationID;
                }
            }
        }

        return locationID;
    }

    public Struct function appendDebugDataToStruct(required Struct errorStruct) {
        var returnStruct=arguments.errorStruct;
        if (structKeyExists(this.httpHelper,"lastRequest")) {
            returnStruct.lastRequest=this.httpHelper.lastRequest;
        }
        if (structKeyExists(this.httpHelper,"lastResponse")) {
            returnStruct.lastResponse=this.httpHelper.lastResponse;
        }
        return returnStruct;
    }

    public String function generateRandomToken() {
        return application.com.encryption.generateSecretKeyAESHex();
    }

    // private Array function getUsergroupsIdsFromNames(required String groupNames) {
    //     var idArray = arrayNew(1);
    //     if (arguments.groupNames neq "") {
    //         var queryService = new query();
    //         queryService.setSQL("
    //             SELECT
    //                 UsergroupID
    //             FROM
    //                 usergroup
    //             WHERE
    //                 Name 
    //             IN (:namesList)
    //         ");
    //         queryService.addParam(name="namesList", value=#arguments.groupNames#, CFSQLTYPE="CF_SQL_VARCHAR", list="true");
    //         var queryResult=queryService.execute();
    //         for (var row=1; row LTE queryResult.getResult().recordcount; row++) {
    //             arrayAppend(idArray,queryResult.getResult().UsergroupID[row]);
    //         }
    //     }
    //     return idArray;
    // }

    // private String function getCountryIdListFromISOList(required String countryList) {
    //     var idArray=arrayNew(1);
    //     if (arguments.countryList neq "") {
    //         var queryService = new query();
    //         queryService.setSQL("
    //             SELECT
    //                 CountryID
    //             FROM
    //                 country
    //             WHERE
    //                 ISOCode 
    //             IN (:codeList)
    //         ");
    //         queryService.addParam(name="codeList", value=#arguments.countryList#, CFSQLTYPE="CF_SQL_VARCHAR", list="true");
    //         var queryResult=queryService.execute();
    //         for (var row=1; row LTE queryResult.getResult().recordcount; row++) {
    //             arrayAppend(idArray, queryResult.getResult().CountryID[row]);
    //         }
    //     }
    //     return arrayToList(idArray);
    // }

    /**
    * Returns the standard form fields for registration - "ins" prefix is on the actual reg fields
    **/
    private Array function getStandardRegistrationFields(boolean includeIns=false) {
        var regArray = ["FirstName","LastName",
            "CountryISO", "OrgURL", "insOfficePhone", "insOfficePhoneExt",
            "Email","MobilePhone","Language","VATnumber",
            "Address1","Address2","Address3","Address4","Address5",
            "Address6","Address7","Address8","Address9","Address10",
            "PostalCode","Telephone","Fax","LocEmail","Salutation", 
            "JobDesc","UserName","Sex","DOBYear","DOBMonth","DOBDay","DOB",
            "aka","OrganisationTypeID","R2LocId","R2PerId","FaxPhone",
            "SpecificURL","Notes"];
        if (arguments.includeIns) {
            var insArray=arrayNew(1);
            for (field in regArray) {
                arrayAppend(insArray,"ins"&field);
            }
            return insArray;
        }
        return regArray;
    }
}