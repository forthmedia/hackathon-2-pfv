<!---
  Created by martin.elgie on 16/11/2016.
  Endpoint for an authorisation requests - extracts parameters for requesting an access token
--->
<cfscript>
    filter = new singleSignOn.OAuth2.client.OAuth2AuthenticationFilterPlugin();
    errorStruct=structNew();
    errorId=0;

    // Retrieve the server with ID saved in user cookie
    serverHelper = new singleSignOn.common.editorBackingBean();
    if (structKeyExists(cookie, "OAuth2LastID")) {
        serverID = cookie.OAuth2LastID;
        oAuthServer=serverHelper.retrieveServer(serverID);
        errorStruct.requestedServer=oAuthServer.getServerName();
    } else {
        errorStruct.explanation="No login has been initiated! No Authorisation Server ID was found in user cookie data. Only client initiated logins are permitted, this URL should only be reached after an OAuth2 login flow has been initiated.";
        errorStruct.Url=url;
        errorId=setError(errorStruct);
    }

    if (noErrors()) {
        // Identify the type of authorisation flow to follow here
        standardFlow=filter.checkForParameters(url,["code", "state"]);  
        if (not standardFlow) {
            errorStruct.explanation="The URL does not contain one or more required paramaters (code, state)";
            errorStruct.Url=url;
            errorId=setError(errorStruct);
        }
    }

    if (noErrors() and standardFlow) { // Relayware standard flow
        errorStruct.requestedServer=oAuthServer.getServerName();
        errorStruct.authorisationFlow="OAuth2 Authorisation Flow";

        accessTokenResponse = filter.requestAccessToken(oAuthServer);
        accessToken = accessTokenResponse.accessToken;
        if (accessToken eq "") {
            errorStruct=filter.appendDebugDataToStruct(errorStruct);
            errorStruct.explanation="Could not retrieve an access token from the Authentication Server.";
            errorId=setError(errorStruct);
        }

        if (noErrors()) {
            // It's possible for the requested scope data to be returned alongside the token
            if (structKeyExists(accessTokenResponse,"uid")) {
                identifier = accessTokenResponse.uid;
            } else {
                // If it's not with the token, use the token to request the scope data
                scopeData=filter.requestScopeData(oAuthServer, accessToken);
                if (!structKeyExists(scopeData, #oAuthServer.getServerUID()#)) {
                    errorStruct=filter.appendDebugDataToStruct(errorStruct);
                    errorStruct.identifier=oAuthServer.getServerUID();
                    errorStruct.explanation="Could not find the specified field in the data returned by the Authorisation Server. Please check UID fields and access token prefix.";
                    errorId=setError(errorStruct);
                } else {
                    identifier=scopeData[#oAuthServer.getServerUID()#];
                }
            }
        }
        
        if (noErrors()) {
            // Using the person's UID field get their ID and log them in
            personID=filter.getPersonIDFromUniqueField(oAuthServer, identifier);
            if (personId neq "") {
                errorStruct.loginAttempt=filter.logInUserFromPersonID(personID, oAuthServer.getServerName(), url.state);
            // If they don't exist and it's enabled, autocreate
            } else if (oAuthServer.isAutocreate()) {
                personResult={created=false};
                if (oAuthServer.isAutoregister() AND !request.currentSite.isInternal) { // Begin autoregister instead of enabled
                    registrationMappings=filter.getRegistrationMappings(scopeData, oAuthServer);
                    personResult.registering=true;
                    cookie.OAuth2State=url.state;
                    filter.beginRegistration(registrationMappings);
                } else {
                    personResult=filter.autocreatePersonIfPossible(scopeData, oAuthServer);
                    personResult.registering=false;
                }
                if (personResult.created) {
                    errorStruct.loginAttempt=filter.logInUserFromPersonID(personResult.personID, oAuthServer.getServerName(), url.state);
                } else if (!personResult.registering) {
                    errorStruct.explanation=personResult.reason;
                    if (structKeyExists(personResult,"creationReport")) {
                        errorStruct.creationReport=personResult.creationReport;
                    }
                    errorId=setError(errorStruct);
                }
            } else {
                errorStruct.explanation="The person could not be matched to the user details provided, please check UIDs and ensure user exists on this site or enable autocreation.";
                errorId=setError(errorStruct);
            }
        }
        if (structKeyExists(errorStruct,"explanation") OR structKeyExists(errorStruct,"loginAttempt") AND structKeyExists(errorStruct.loginAttempt,"explanation")  ) {
                        errorId=setError(errorStruct);
        }
        
    } // End of standard flow

    if (not noErrors()) {
        writeOutput("<p>phr_OAuth2_SSOError</p>");
        if (application.com.settings.getSetting("OAuth.client.fullErrors")) {
            writeDump(#errorStruct#);
        } else {
            writeOutput("Error ID: #errorId#");
        }

    } else {
        writeOutput("No errors but unable to login");
    }

    /**
    * Returns true if no error code has been set
    */
    private boolean function noErrors() {
        return not isDefined("errorId") or errorId eq 0;
    }

    private any function setError(Struct errorStruct) {
        return application.com.errorHandler.recordRelayError_Warning(
            type="OAuth2AuthenticationError",
            Severity="error",
            WarningStructure=arguments.errorStruct);
    }

</cfscript>