/**
 * Created by martin.elgie on 03/11/2016.
 */
component accessors="true" output="false" persistent="false"{

    public any function init(String serverName, String clientId, String tokenURL) {
        this.serverName="";
        this.clientId="";
        this.clientSecret="";
        this.created="";
        this.tokenURL="";
        this.authoriseURL="";
        this.dataURL="";
        this.scope="";
        this.tokenPrefix="bearer";
        this.serverUid="";
        this.relaywareUid="";
        this.authorisedSites="";
        this.active=false;

        this.autocreate=false;
        this.autoregister=false;
        this.selectLocOrg=true;
        this.autocreate_selectOrgId="";
        this.autocreate_selectLocId="";
        this.detectedLoc_RelaywareField="";
        this.detectedLoc_ForeignField="";

        this.onCreationPortalPersonApprovalStatus="0";
        this.usergroupCountriesAtPersonCreation="";
        this.usergroupsAtPersonCreation="";
        this.canSetUsergroups=false;
        this.canSetCountryRights=false;
        this.assertionAttributeToControlUsergroups ="";
        this.usergroupsPermittedToControl="";
        this.assertionAttributeToControlCountryRights="";

        if (structkeyexists(arguments, "serverName")) {
            this.serverName = serverName;
        }
        if (structkeyexists(arguments, "clientId")) {
            this.clientId = clientId;
        }
        if (structkeyexists(arguments, "tokenURL")) {
            this.tokenURL = tokenURL;
        }

        this.isValidServer = false;
        this.active = true;

        return this;
    }

    public boolean function isPermittedToSetUsergroups() {
        return this.canSetUsergroups;
    }

    public void function setCanSetUsergroups(boolean allow) {
        this.canSetUserGroups=arguments.allow;
    }

    public String function getAssertionAttributeToControlUsergroups() {
        return this.assertionAttributeToControlUsergroups;
    }

    public void function setAssertionAttributeToControlUsergroups(String attribute) {
        this.assertionAttributeToControlUsergroups=arguments.attribute;
    }

    public String function getUsergroupsPermittedToControl() {
        return this.usergroupsPermittedToControl;
    }

    public void function setUsergroupsPermittedToControl(String usergroups) {
        this.usergroupsPermittedToControl=arguments.usergroups;
    }

    public boolean function isPermittedToControlCountryRights() {
        return this.canSetCountryRights;
    }

    public void function setPermittedToControlCountryRights(boolean allow) {
        this.canSetCountryRights=arguments.allow;
    }

    public String function getAssertionAttributeToControlCountryRights() {
        return this.assertionAttributeToControlCountryRights;
    }

    public void function setAssertionAttributeToControlCountryRights(String attribute) {
        this.assertionAttributeToControlCountryRights=arguments.attribute;
    }

    public void function clearMappings() {
        var queryService = new query();
        queryService.setSQL("
            DELETE FROM
                OAuth2ServerMapping
            WHERE
                OAuth2ServerId=:clientID
            ");
        queryService.addParam(name="clientID", value=#this.clientId#, CFSQLTYPE="CF_SQL_VARCHAR");
        queryService.execute();
    }

    public void function addMapping(String relaywareField, boolean useAssertion, String value) {
        var queryService = new query();
        queryService.setSQL("
            INSERT INTO
                OAuth2ServerMapping
                (
                    OAuth2ServerId,
                    relaywareField,
                    useAssertion,
                    foreignValue
                )
            VALUES
                (
                    :serverID,
                    :field,
                    :assertion,
                    :value
                )
            ");
        queryService.addParam(name="serverID", value=this.clientId, CFSQLTYPE="CF_SQL_VARCHAR");
        queryService.addParam(name="field", value=arguments.relaywareField, CFSQLTYPE="CF_SQL_VARCHAR");
        queryService.addParam(name="assertion", value=arguments.useAssertion, CFSQLTYPE="CF_SQL_BIT");
        queryService.addParam(name="value", value=arguments.value, CFSQLTYPE="CF_SQL_VARCHAR");
        queryService.execute();
    }

    public array function getMappings() {
        var resultArray = arrayNew(1);
        var mappingQuery = new query();
        mappingQuery.setSQL("
            SELECT 
                relaywareField, useAssertion, foreignValue 
            FROM
                OAuth2ServerMapping
            WHERE
                OAuth2ServerID=:clientID
            ");
        mappingQuery.addParam(name="clientID", value=#this.clientId#, CFSQLTYPE="CF_SQL_VARCHAR");
        var queryResult=mappingQuery.execute().getResult();
        for (row=1; row LTE queryResult.recordcount; row++) {
            var mapping = new singleSignOn.common.assertionMapping(
                queryResult.relaywareField[row],
                queryResult.useAssertion[row],
                queryResult.foreignValue[row]);
            arrayAppend(resultArray,mapping);
        }
        return resultArray;
    }

    public String function getMappingForRelaywareField(required String relaywareField, required array mappings) {
        var assertionField="";
        for (var i=1; i LTE arrayLen(arguments.mappings); i++) {
            if (mappings[i].relaywareField EQ arguments.relaywareField) {
                assertionField=mappings[i].foreignValue;
                break;
            }
        }
        return assertionField;
    }

    public boolean function isAutocreate() {
        return this.autocreate;
    }

    public void function setAutocreate(boolean autocreate) {
        this.autocreate=arguments.autocreate;
    }

    public boolean function getAutoCreate_SelectLocOrg() {
        return this.selectLocOrg;
    }

    public void function setAutoCreate_SelectLocOrg(boolean select) {
        this.selectLocOrg=arguments.select;
    }

    public String function getAutoCreate_selectedOrganisationID() {
        return this.autocreate_selectOrgId;
    }

    public void function setAutoCreate_selectedOrganisationID(String id) {
        this.autocreate_selectOrgId=arguments.id;
    }

    public String function getAutoCreate_selectedLocationID() {
        return this.autocreate_selectLocId;
    }

    public void function setAutoCreate_selectedLocationID(String id) {
        this.autocreate_selectLocId=arguments.id;
    }

    public String function getAutoCreate_detectedLoc_RelaywareField() {
        return this.detectedLoc_RelaywareField;
    }

    public void function setAutoCreate_detectedLoc_RelaywareField(String field) {
        this.detectedLoc_RelaywareField=arguments.field;
    }

    public String function getAutoCreate_detectedLoc_ForeignField() {
        return this.detectedLoc_ForeignField;
    }

    public void function setAutoCreate_detectedLoc_ForeignField(String field) {
        this.detectedLoc_ForeignField=arguments.field;
    }

    public String function getUsergroupsAtPersonCreation() {
        return this.usergroupsAtPersonCreation;
    }

    public void function setUsergroupsAtPersonCreation(String usergroups) {
        this.usergroupsAtPersonCreation=arguments.usergroups;
    }

    public String function getUsergroupCountriesAtPersonCreation() {
        return this.usergroupCountriesAtPersonCreation;
    }

    public void function setUsergroupCountriesAtPersonCreation(String countries) {
        this.usergroupCountriesAtPersonCreation=arguments.countries;
    }

    /**
    * Approval status is stored as an integer on the db, this returns the text name
    */
    public String function getOnCreationPortalPersonApprovalStatus(boolean asString=true) {
        if (this.onCreationPortalPersonApprovalStatus eq "0") {
            return "0";
        }
        return asString ? 
        application.com.flag.getFlagStructure(this.onCreationPortalPersonApprovalStatus).apiName 
        : this.onCreationPortalPersonApprovalStatus;
    }

    public void function setOnCreationPortalPersonApprovalStatus(String status) {
        this.onCreationPortalPersonApprovalStatus=arguments.status;
    }

    public boolean function isAutoregister() {
        return this.autoregister;
    }

    public void function setAutoregister(boolean autoregister) {
        this.autoregister=arguments.autoregister;
    }

    public String function getTokenPrefix() {
        return this.tokenPrefix;
    }

    public void function setTokenPrefix(String prefix) {
        this.tokenPrefix=arguments.prefix;
    }

    public String function getClientId() {
        return this.clientId;
    }

    public void function setClientID(String id) {
        this.clientId = arguments.id;
    }

    public void function setClientSecret(String secret) {
        this.clientsecret = secret;
    }

    public String function getServerName() {
        return this.serverName;
    }

    public boolean function currentlyExistsInDatabase() {
        var queryService = new query();
        queryService.setSQL("
            SELECT
                1
            FROM
                OAuth2Server
            WHERE
                clientIdentifier = :clientId
                ");
        queryService.addParam(name="clientId", value=#this.clientId#, CFSQLTYPE="CF_SQL_VARCHAR");
        var result = queryService.execute();
        return result.getResult().recordcount >= 1;
    }

    private String function hashSecret(required String secret) {
        var hashed=hash(Trim(arguments.secret) & Trim(lcase(this.clientId)),"SHA-256");

        return hashed;
    }

    public boolean function isValidOAuthServer() {
        return this.isValidServer && this.active;
    }

    public void function setServerName(required String name) {
        this.serverName = arguments.name;
    }

    public String function getClientSecret() {
        return this.clientSecret;
    }

    public String function getAuthoriseURL() {
        return this.authoriseURL;
    }

    public void function setAuthoriseURL(required String url) {
        if (arguments.url neq "" and not arguments.url.toLowerCase().startsWith("http")) {
            arguments.url="http://" & arguments.url;
        }
        this.authoriseURL = arguments.url;
    }

    public String function getTokenURL() {
        return this.tokenURL;
    }

    public void function setTokenURL(required String url) {
        if (arguments.url neq "" and not arguments.url.toLowerCase().startsWith("http")) {
            arguments.url="http://" & arguments.url;
        }
        this.tokenURL = arguments.url;
    }

    public String function getDataURL() {
        return this.dataURL;
    }

    public void function setDataURL(required String url) {
        if (arguments.url neq "" and not arguments.url.toLowerCase().startsWith("http")) {
            arguments.url="http://" & arguments.url;
        }
        this.dataURL = arguments.url;
    }

    public String function getServerUID() {
        return this.serverUid;
    }

    public String function getScope() {
        return this.scope;
    }

    public void function setScope(required String scope) {
        this.scope=arguments.scope;
    }

    public void function setServerUID(required String uid) {
        this.serverUid = arguments.uid;
    }

    public void function setRelaywareUID(required String uid) {
        this.relaywareUid = arguments.uid;
    }

    public String function getRelaywareUID() {
        return this.relaywareUid;
    }

    public boolean function isActive() {
        return this.active;
    }

    public void function setActive(required boolean active) {
        this.active = arguments.active;
    }

    public String function getAuthorisedSites() {
        return this.authorisedSites;
    }

    public void function setAuthorisedSites(required String sites) {
        this.authorisedSites = arguments.sites;
    }

    public void function deleteFromDatabase() {
        var queryService = new query();
        queryService.setSQL("
            DELETE FROM
                OAuth2Server
            WHERE
                clientIdentifier=:clientID
            ");
        queryService.addParam(name="clientID", value=#this.clientId#, CFSQLTYPE="CF_SQL_VARCHAR");
        queryService.execute();
    }

    public boolean function isValidForSave(required any serverManager) {
        var serverArray=serverManager.getAllServers();
        var valid=true;
        for (var i=1; i LTE arrayLen(serverArray); i++) {
            var compareServer=serverArray[i];
            // Only allow the same server name when it's this client (based on ID)
            var isDuplicate = this.getServerName() eq compareServer.getServerName() 
                        && this.getClientId() neq compareServer.getClientId();
            if (isDuplicate) {
                compareServer.deleteFromDatabase();
            }
        }
        // Currently deleting duplicate name servers so this check will always return true
        return valid;
    }

    public void function persistToDatabase() {
        var queryService = new query();
        if (currentlyExistsInDatabase()) {
            queryService.setSQL("
            UPDATE
                OAuth2Server
            SET
                serverName=:name,
                hashedClientSecret=:secret,
                authoriseURL=:authUrl,
                tokenURL=:token,
                dataURL=:data,
                scope=:scope,
                tokenPrefix=:tokenPrefix,
                serverUID=:serverField,
                relaywareUID=:relaywareField,
                authorisedSites=:authorisedSites,
                active=:isActive,

                autocreate=:autocreate,
                autoregister=:autoregister,
                selectLocOrg=:selectLocOrg,
                autocreate_selectOrgId=:autocreate_selectOrgId,
                autocreate_selectLocId=:autocreate_selectLocId,
                detectedLoc_RelaywareField=:detectedLoc_RelaywareField,
                detectedLoc_ForeignField=:detectedLoc_ForeignField,

                onCreationPortalPersonApprovalStatus=:onCreationPortalPersonApprovalStatus,
                usergroupCountriesAtPersonCreation=:usergroupCountriesAtPersonCreation, 
                usergroupsAtPersonCreation=:usergroupsAtPersonCreation,
                permittedToSetUsergroups=:permittedToSetUsergroups, 
                permittedToSetCountryRights=:permittedToSetCountryRights,
                assertionAttributeToControlUsergroups=:assertionAttributeToControlUsergroups,
                usergroupsPermittedToControl=:usergroupsPermittedToControl, 
                assertionAttributeToControlCountryRights=:assertionAttributeToControlCountryRights
            WHERE
                clientIdentifier = :clientId
                ");
        } else {
            queryService.setSQL("
            INSERT INTO
                OAuth2Server
                (
                serverName,
                clientIdentifier,
                hashedClientSecret,
                authoriseURL,
                tokenURL,
                dataURL,
                scope,
                tokenPrefix,
                serverUID,
                relaywareUID,
                authorisedSites,
                created,
                active,

                autocreate, 
                autoregister, 
                selectLocOrg, 
                autocreate_selectOrgId,
                autocreate_selectLocId, 
                detectedLoc_RelaywareField,
                detectedLoc_ForeignField, 

                onCreationPortalPersonApprovalStatus,
                usergroupCountriesAtPersonCreation, 
                usergroupsAtPersonCreation,
                permittedToSetUsergroups, 
                permittedToSetCountryRights,
                assertionAttributeToControlUsergroups,
                usergroupsPermittedToControl, 
                assertionAttributeToControlCountryRights
                )
                VALUES (
                :name,
                :clientId,
                :secret,
                :authUrl,
                :token,
                :data,
                :scope,
                :tokenPrefix,
                :serverField,
                :relaywareField,
                :authorisedSites,
                :createdDate,
                :isActive,

                :autocreate, 
                :autoregister, 
                :selectLocOrg, 
                :autocreate_selectOrgId,
                :autocreate_selectLocId, 
                :detectedLoc_RelaywareField,
                :detectedLoc_ForeignField, 

                :onCreationPortalPersonApprovalStatus,
                :usergroupCountriesAtPersonCreation, 
                :usergroupsAtPersonCreation,
                :permittedToSetUsergroups, 
                :permittedToSetCountryRights,
                :assertionAttributeToControlUsergroups,
                :usergroupsPermittedToControl, 
                :assertionAttributeToControlCountryRights
                )");
            queryService.addParam(name="createdDate", value=#this.created#, CFSQLTYPE="CF_SQL_DATE");
        }
        queryService.addParam(name="name", value=#this.serverName#, CFSQLTYPE="CF_SQL_VARCHAR");
        queryService.addParam(name="clientId", value=#this.clientId#, CFSQLTYPE="CF_SQL_VARCHAR");
        queryService.addParam(name="secret", value=#this.clientSecret#, CFSQLTYPE="CF_SQL_VARCHAR");
        queryService.addParam(name="authUrl", value=#this.authoriseURL#, CFSQLTYPE="CF_SQL_VARCHAR");
        queryService.addParam(name="token", value=#this.tokenURL#, CFSQLTYPE="CF_SQL_VARCHAR");
        queryService.addParam(name="data", value=#this.dataURL#, CFSQLTYPE="CF_SQL_VARCHAR");
        queryService.addParam(name="scope", value=#this.scope#, CFSQLTYPE="CF_SQL_VARCHAR");
        queryService.addParam(name="tokenPrefix", value=#this.tokenPrefix#, CFSQLTYPE="CF_SQL_VARCHAR");
        queryService.addParam(name="serverField", value=#this.serverUid#, CFSQLTYPE="CF_SQL_VARCHAR");
        queryService.addParam(name="relaywareField", value=#this.relaywareUid#, CFSQLTYPE="CF_SQL_VARCHAR");
        queryService.addParam(name="authorisedSites", value=#this.authorisedSites#, CFSQLTYPE="CF_SQL_VARCHAR");
        queryService.addParam(name="isActive", value=#this.active#, CFSQLTYPE="CF_SQL_BIT");


        queryService.addParam(name="autocreate", value=#this.autocreate#, CFSQLTYPE="CF_SQL_BIT");
        queryService.addParam(name="autoregister", value=#this.autoregister#, CFSQLTYPE="CF_SQL_BIT");
        queryService.addParam(name="selectLocOrg", value=#this.selectLocOrg#, CFSQLTYPE="CF_SQL_BIT");
        queryService.addParam(name="autocreate_selectOrgId", value=#this.autocreate_selectOrgId#, CFSQLTYPE="CF_SQL_VARCHAR");
        queryService.addParam(name="autocreate_selectLocId", value=#this.autocreate_selectLocId#, CFSQLTYPE="CF_SQL_VARCHAR");
        queryService.addParam(name="detectedLoc_RelaywareField", value=#this.detectedLoc_RelaywareField#, CFSQLTYPE="CF_SQL_VARCHAR");
        queryService.addParam(name="detectedLoc_ForeignField", value=#this.detectedLoc_ForeignField#, CFSQLTYPE="CF_SQL_VARCHAR");
        
        queryService.addParam(name="onCreationPortalPersonApprovalStatus", value=#this.onCreationPortalPersonApprovalStatus#, CFSQLTYPE="CF_SQL_INTEGER");
        queryService.addParam(name="usergroupCountriesAtPersonCreation", value=#this.usergroupCountriesAtPersonCreation#, CFSQLTYPE="CF_SQL_VARCHAR");
        queryService.addParam(name="usergroupsAtPersonCreation", value=#this.usergroupsAtPersonCreation#, CFSQLTYPE="CF_SQL_VARCHAR");
        queryService.addParam(name="permittedToSetUsergroups", value=#this.canSetUsergroups#, CFSQLTYPE="CF_SQL_BIT");
        queryService.addParam(name="permittedToSetCountryRights", value=#this.canSetCountryRights#, CFSQLTYPE="CF_SQL_BIT");
        queryService.addParam(name="assertionAttributeToControlUsergroups", value=#this.assertionAttributeToControlUsergroups#, CFSQLTYPE="CF_SQL_VARCHAR");
        queryService.addParam(name="usergroupsPermittedToControl", value=#this.usergroupsPermittedToControl#, CFSQLTYPE="CF_SQL_VARCHAR");
        queryService.addParam(name="assertionAttributeToControlCountryRights", value=#this.assertionAttributeToControlCountryRights#, CFSQLTYPE="CF_SQL_VARCHAR");
        queryService.execute();
    }

    public boolean function pullFromDatabase() {
        var isOk=true;
        var queryService = new query();
        queryService.setSQL("
            SELECT
                serverName, clientIdentifier, hashedClientSecret,
                authoriseURL, tokenURL, dataURL, scope, tokenPrefix,
                serverUID, relaywareUID, authorisedSites, created, active,
                autocreate, autoregister, selectLocOrg, autocreate_selectOrgId,
                autocreate_selectLocId, detectedLoc_RelaywareField,
                detectedLoc_ForeignField, onCreationPortalPersonApprovalStatus,
                usergroupCountriesAtPersonCreation, usergroupsAtPersonCreation,
                permittedToSetUsergroups, permittedToSetCountryRights,
                assertionAttributeToControlUsergroups,
                usergroupsPermittedToControl, assertionAttributeToControlCountryRights
            FROM
                OAuth2Server
            WHERE
                clientIdentifier=:clientID
        ");
        queryService.addParam(name="clientID", value=#this.clientId#, CFSQLTYPE="CF_SQL_VARCHAR");
        var queryResult=queryService.execute();

        // Check just one entry has been returned
        if (queryResult.getResult().recordcount EQ 1) {
            isOk=true;
            this.isValidServer=true;
        } else {
            isOk=false;
            this.isValidServer=false;
            errorStruct={RecordCount=queryResult.getResult().recordcount,
                        ClientID=#this.clientId#,
                        ServerName=#this.serverName#};
            if (queryResult.getResult().recordcount GT 1) {
                errorStruct.message="Multiple servers found with this ID";
            } else {
                errorStruct.message="No server found with this ID";
            }
            errorId=application.com.errorHandler.recordRelayError_Warning(
                type="OAuth2AuthenticationError",
                Severity="error",
                WarningStructure=errorStruct);

            writeOutput("phr_OAuth2_SSOError ID: #errorId#");
        }

        // Return false if the database pull failed, otherwise update this server
        if (isOk) {
            this.serverName = queryResult.getResult().serverName;
            this.clientId = queryResult.getResult().clientIdentifier;
            this.clientSecret = queryResult.getResult().hashedClientSecret;
            this.authoriseURL = queryResult.getResult().authoriseURL;
            this.tokenURL = queryResult.getResult().tokenURL;
            this.dataURL = queryResult.getResult().dataURL;
            this.scope = queryResult.getResult().scope;
            this.tokenPrefix = queryResult.getResult().tokenPrefix;
            this.serverUid = queryResult.getResult().serverUID;
            this.relaywareUid = queryResult.getResult().relaywareUID;
            this.authorisedSites = queryResult.getResult().authorisedSites;
            this.created = queryResult.getResult().created;
            this.active = queryResult.getResult().active;

            this.autocreate=queryResult.getResult().autocreate;
            this.autoregister=queryResult.getResult().autoregister;
            this.selectLocOrg=queryResult.getResult().selectLocOrg;
            this.autocreate_selectOrgId=queryResult.getResult().autocreate_selectOrgId;
            this.autocreate_selectLocId=queryResult.getResult().autocreate_selectLocId;
            this.detectedLoc_RelaywareField=queryResult.getResult().detectedLoc_RelaywareField;
            this.detectedLoc_ForeignField=queryResult.getResult().detectedLoc_ForeignField;

            this.onCreationPortalPersonApprovalStatus=queryResult.getResult().onCreationPortalPersonApprovalStatus;
            this.usergroupCountriesAtPersonCreation=queryResult.getResult().usergroupCountriesAtPersonCreation;
            this.usergroupsAtPersonCreation=queryResult.getResult().usergroupsAtPersonCreation;
            this.canSetUsergroups=queryResult.getResult().permittedToSetUsergroups;
            this.canSetCountryRights=queryResult.getResult().permittedToSetCountryRights;
            this.assertionAttributeToControlUsergroups=queryResult.getResult().assertionAttributeToControlUsergroups;
            this.usergroupsPermittedToControl=queryResult.getResult().usergroupsPermittedToControl;
            this.assertionAttributeToControlCountryRights=queryResult.getResult().assertionAttributeToControlCountryRights;
        }
        return isOk;

    }
}
