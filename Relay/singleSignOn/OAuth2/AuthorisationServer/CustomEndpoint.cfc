/**
 * CustomEndpoint
 *
 * @author Richard.Tingle
 * @date 21/09/15
 **/
component extends="PersonOrgLocDataEndpoint" accessors=true output=false persistent=false {


	public string function getScopeName() output="false" {
		return this.scopeName;
	}

	public any function init(required string scopeName, required string personFields, required string locationFields, required string organisationFields) output="false" {
		/* TODO: Implement Method */
		super.init(personFields,locationFields,organisationFields);
		this.scopeName = arguments.scopeName;

		return this;
	}
}