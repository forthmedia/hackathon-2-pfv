<cfcomponent accessors="true" output="false" persistent="false">
	

	<cffunction name="init" access="public" output="false" returntype="any">
		<cfargument name="clientName" type="string" required="false" />
		<cfargument name="clientIdentifier" type="string" required="false" />
		<cfargument name="redirectURL" type="string" required="false" />
		
		<cfscript>
			this.redirectURL="";
			this.clientName="";
			this.clientIdentifier="";
			this.clientSecret="";
			this.created="";
			this.active=false;
			
			
			
			
			if (structKeyExists(arguments,"clientName")){
				this.clientName=clientName;
			}
			if (structKeyExists(arguments,"clientIdentifier")){
				this.clientIdentifier=clientIdentifier;
			}
			if (structKeyExists(arguments,"redirectURL")){
				setRedirectURL(arguments.redirectURL);
			}
			
			this.isValidClient=false; //is not valid until we have pulled from the database and found it
			this.active=true; //active unless we hear otherwise
		</cfscript>
		<cfreturn this>
	</cffunction>
	
	<cffunction name="autogenerateClient" hint="Creates a new token of the specifed type and returns it's details. This returns the client secret in raw form, the only time it is available" access="public" output="false" returntype="string">

		<cfscript>
			clientSecretRaw=randomString();
			if (!structKeyExists(this,"clientIdentifier") OR this.clientIdentifier EQ ""){
				this.clientIdentifier=randomString();
				this.created=now();
			}
			this.clientSecret=hashSecret(clientSecretRaw);
			
			return clientSecretRaw;
		</cfscript>
	</cffunction>
	
	<cffunction name="hashSecret" hint="Creates a new token of the specifed type and returns it's details. This returns the client secret in raw form, the only time it is available" access="private" output="false" returntype="string">
		<cfargument name="secret" type="string" required="true" />
		
		<cfscript>
			var hashed=hash(Trim(secret) & Trim(lcase(this.clientIdentifier)),"SHA-256");
			
		</cfscript>
		<cfreturn hashed>
		
	</cffunction>
	
	<cffunction name="validateClientSecret" hint="Creates a new token of the specifed type and returns it's details" access="public" output="false" returntype="boolean">
		<cfargument name="clientSecret" type="string" required="true" />

		<cfscript>
			var possibleHashedClientSecret=hashSecret(clientSecret);
			

		</cfscript>
		
		<cfreturn compare(possibleHashedClientSecret,this.clientSecret) EQ 0>

	</cffunction>
	
		
		
	<cffunction name="randomString" hint="Creates a secure random alphanumeric string" access="private" output="false" returntype="string">
		<cfargument name="length" type="numeric" default=56 />
		

		<cfscript>
			var permittedcharacters="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890";
			
			//secure random is a crytographically safe random number generator
			var secureRand = CreateObject("java", "java.security.SecureRandom").init();
			var i=1;
			var character="";
			var randomString="";
			for(i=1;i<=length;i++){
				character=Mid( permittedcharacters,secureRand.nextInt(len(permittedcharacters))+1, 1 );
				randomString=randomString & character;
			}

			return randomString;
		</cfscript>
		
		
		
	</cffunction>
	
	<cffunction name="isAuthorisedToAccessDataForUser" access="public"  output="false" returntype="boolean">
		<cfargument name="personID" type="numeric" required=true />
		<cfif isValidOAuthClient()>
			<CFQUERY NAME="getAuthentication" datasource="#application.siteDataSource#">
			
				 select 1 from OAuth2ClientPersonPermission 
				 where clientIdentifier=<cf_queryparam value="#this.clientIdentifier#" CFSQLTYPE="CF_SQL_VARCHAR" >
				 and personID=<cf_queryparam value="#arguments.personID#" CFSQLTYPE="CF_SQL_Integer" >;			 
			</CFQUERY>
			
			<cfreturn getAuthentication.recordCount GT 0>
			
		<cfelse>
			<cfreturn false>
		</cfif>
		
	</cffunction>
	
		
	<cffunction name="authoriseToAccessDataForUser" access="public"  output="false" returntype="void">
		<cfargument name="personID" type="numeric" required=true />
		<cfif isValidOAuthClient()>
			<CFQUERY NAME="createAuthentication" datasource="#application.siteDataSource#">
			
				 insert into OAuth2ClientPersonPermission (clientIdentifier,personID)
				 values(
				 	<cf_queryparam value="#this.clientIdentifier#" CFSQLTYPE="CF_SQL_VARCHAR" >,
				    <cf_queryparam value="#arguments.personID#" CFSQLTYPE="CF_SQL_Integer" >
				    )
				 
			</CFQUERY>
		</cfif>

		
	</cffunction>

		
	<cffunction name="persistToDatabase" access="public"  output="false" returntype="void">
		<cfif currentlyExistsInDatabase()>
			<CFQUERY NAME="pushOAuth2Client" datasource="#application.siteDataSource#">
			
				UPDATE OAuth2Client
				SET 
					clientName=<cf_queryparam value="#this.clientName#" CFSQLTYPE="CF_SQL_VARCHAR" >, 
					hashedClientSecret=<cf_queryparam value="#this.clientSecret#" CFSQLTYPE="CF_SQL_VARCHAR" >,
					active=<cf_queryparam value="#this.active#" CFSQLTYPE="CF_SQL_BIT" >,
					redirectURL=<cf_queryparam value="#this.redirectURL#" CFSQLTYPE="CF_SQL_VARCHAR" >
				WHERE 
					clientIdentifier=<cf_queryparam value="#this.clientIdentifier#" CFSQLTYPE="CF_SQL_VARCHAR" > ;
				
			</CFQUERY>

		<cfelse>
			<CFQUERY NAME="pushOAuth2Client" datasource="#application.siteDataSource#">
			
				 insert into OAuth2Client (clientName,clientIdentifier,hashedClientSecret, created,redirectURL,active)
				 values(
				 
				    <cf_queryparam value="#this.clientName#" CFSQLTYPE="CF_SQL_VARCHAR" >, 
				    <cf_queryparam value="#this.clientIdentifier#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
				    <cf_queryparam value="#this.clientSecret#" CFSQLTYPE="CF_SQL_VARCHAR" > ,
				    <cf_queryparam value="#CreateODBCDateTime(this.created)#" CFSQLTYPE="CF_SQL_timestamp" >, 
				    <cf_queryparam value="#this.redirectURL#" CFSQLTYPE="CF_SQL_VARCHAR" >,
				    <cf_queryparam value="#this.active#" CFSQLTYPE="CF_SQL_BIT" > 
				    )
				 
			</CFQUERY>
		</cfif>
		
		<cfif structKeyExists(this,"permittedScopesUpdated") AND this.permittedScopesUpdated>
			<!--- delete the old permitted scopes, create the new ones--->
			
			<CFQUERY NAME="removeOldData" datasource="#application.siteDataSource#">
			
				delete from OAuth2ScopePermission
				where clientIdentifier=<cf_queryparam value="#this.getClientIdentifier()#" CFSQLTYPE="CF_SQL_VARCHAR" >
			</CFQUERY>
			
			<cfset permittedScopes=this.getPermittedScopes()>
			
			<cfif arrayLen(permittedScopes) NEQ 0>
				<CFQUERY NAME="createNewData" datasource="#application.siteDataSource#">
				
					insert into OAuth2ScopePermission
					
					values
					<cfloop  
						index="i"
						from="1"
						to="#arrayLen(permittedScopes)#">
						    (
						    <cf_queryparam value="#this.getClientIdentifier()#" CFSQLTYPE="CF_SQL_VARCHAR" >,
						    <cf_queryparam value="#permittedScopes[i]#" CFSQLTYPE="CF_SQL_VARCHAR" >		
						    )	
						<cfif i NEQ arrayLen(permittedScopes)>
							, <!--- Not the last one --->
						</cfif>   	    	
					</cfloop>
					
				</CFQUERY>
			</cfif>
		</cfif>
		
		<cfset this.isValidClient=true> <!--- now in DB so is valid --->
		
	</cffunction>
	
	<cffunction name="isValidOAuthClient" access="public"  output="false" returntype="boolean" hint="returns if the client was pulled from database and is valid">
		

		<cfreturn (this.isValidClient and this.active)>
	</cffunction>
	
	
	<cffunction name="isCorrectRedirectURL" access="public"  output="false" returntype="boolean" hint="returns if the client was pulled from database and is valid">
		<cfargument name="redirectURL" type="string" required=true />
		<cfreturn compare(redirectURL,this.redirectURL) EQ 0>
	</cffunction>
	
	<cffunction name="authorisedForScope" access="public"  output="false" returntype="boolean">
		<cfargument name="scope" type="string" required=true />
		
		<cfset permittedScopes=this.getPermittedScopes()>
		<cfreturn ArrayContains(permittedScopes,arguments.scope) >
	</cffunction>
	
	<cffunction name="getClientIdentifier" access="public" output="false" returntype="string">
		<cfreturn this.clientIdentifier>
	</cffunction>
	
	<cffunction name="pullFromDatabase" access="public"  output="false" returntype="boolean" hint="returns if successfully pulled from database">
		<cfset var isOk=true>

		<CFQUERY NAME="clientData" datasource="#application.siteDataSource#">
			SELECT 
			    clientName,clientIdentifier,hashedClientSecret,redirectURL, created, active
			FROM 
				OAuth2Client
			WHERE 
				clientIdentifier=<cf_queryparam value="#this.clientIdentifier#" CFSQLTYPE="CF_SQL_VARCHAR" > ;
		</CFQUERY>
		
		<cfscript>
			local.returnStruct=structNew();
			
			
			if (clientData.recordCount != 1){
				isOk=false;
				this.isValidClient=false;
				
				
			}else{
				isOk=true;
				this.isValidClient=true;
			}
	
		</cfscript>

		
		<cfif isOk>
			<CFLOOP QUERY="clientData">
				<cfscript>
					this.redirectURL=clientData.redirectURL;
					this.clientName=clientData.clientName;
					this.clientIdentifier=clientData.clientIdentifier;
					this.clientSecret=clientData.hashedClientSecret;
					this.created=clientData.created;
					this.active=clientData.active;
				</cfscript>
			</CFLOOP>
		<cfelse>	
			<cfreturn false>
		</cfif>
		<cfreturn true>
		
	</cffunction>
	
	<cffunction name="getPermittedScopes" access="public" output="false" returntype="array">
		<cfif not currentlyExistsInDatabase()>
			<cfset this.permittedScopes=arrayNew(1)>
		<cfelseif (NOT structKeyExists(this,"permittedScopes"))>
			<CFQUERY NAME="getPermittedScopes" datasource="#application.siteDataSource#">
			
				select scope from OAuth2ScopePermission
				WHERE clientIdentifier=<cf_queryparam value="#this.getClientIdentifier()#" CFSQLTYPE="CF_SQL_VARCHAR" >
			</CFQUERY>
			
			<cfset this.permittedScopes=arrayNew(1)>
			
			<CFLOOP QUERY="getPermittedScopes">
				<cfscript>
					arrayAppend(this.permittedScopes,getPermittedScopes.scope);
				</cfscript>
			</CFLOOP>
		</cfif>
		
		
		<cfreturn this.permittedScopes>
		
		
		
		
	</cffunction>
	
	<cffunction name="setPermittedScopes" access="public" output="false" returntype="string">
		<cfargument name="permittedScopes" type="array" required=true />
		<cfset this.permittedScopes=arguments.permittedScopes>
		<cfset this.permittedScopesUpdated=true>
	</cffunction>
	
	<cffunction name="getClientName" access="public" output="false" returntype="string">
		
		<cfreturn this.clientName>
	</cffunction>
	
	<cffunction name="setClientName" access="public" output="false" returntype="string">
		<cfargument name="clientName" type="string" required=true />
		<cfset this.clientName=arguments.clientName>
	</cffunction>
	
	
	<cffunction name="isActive" access="public" output="false" returntype="string">
		
		<cfreturn this.active>
	</cffunction>
	
	<cffunction name="getRedirectURL" access="public" output="false" returntype="string">
		
		<cfreturn this.redirectURL>
	</cffunction>
	
	<cffunction name="hasSecureRedirectURL" access="public" output="false" returntype="string">
		
		<cfreturn this.getRedirectURL().toLowerCase().startsWith("https")>
	</cffunction>
	
	
	<cffunction name="setRedirectURL" access="public" output="false" returntype="void">
		<cfargument name="redirectURL" type="string" required=true />
		
		<cfscript>
		
			if (not arguments.redirectURL.toLowerCase().startsWith("http")){
					arguments.redirectURL="http://" & redirectURL;
			}
			
			this.redirectURL=arguments.redirectURL;
		</cfscript>

	</cffunction>
	
	<cffunction name="setActive" access="public" output="false" returntype="void">
		<cfargument name="active" type="string" required=true />
		<cfset this.active=arguments.active>
	</cffunction>
	
	<cffunction name="deleteFromDatabase" access="public" output="false" returntype="boolean">
		<CFQUERY NAME="oAuth2Client" datasource="#application.siteDataSource#" result="oAuth2ClientData">
			DELETE FROM 
				OAuth2Client
			WHERE 
				clientIdentifier =  <cf_queryparam value="#this.clientIdentifier#" CFSQLTYPE="CF_SQL_VARCHAR" > 
		</CFQUERY>
		
		<cfreturn oAuth2ClientData.recordCount GTE 1>
	</cffunction>
	
	<cffunction name="currentlyExistsInDatabase" hint="Checks if a token with this type and value currently exists, largly used to ensure uniqueness" access="public" output="false" returntype="boolean">
		<CFQUERY NAME="oAuth2Client" datasource="#application.siteDataSource#" result="oAuth2ClientData">
			SELECT 
			    1
			FROM 
				OAuth2Client
			WHERE 
				clientIdentifier =  <cf_queryparam value="#this.clientIdentifier#" CFSQLTYPE="CF_SQL_VARCHAR" > 
		</CFQUERY>
		
		<cfreturn oAuth2ClientData.recordCount GTE 1>
	</cffunction>

	
</cfcomponent>