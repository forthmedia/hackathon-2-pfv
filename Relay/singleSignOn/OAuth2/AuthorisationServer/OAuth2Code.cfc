<!---
  --- OAuth2Code
  --- ----------
  ---
  --- author: Richard.Tingle
  --- date:   15/07/15
  --->
<cfcomponent extends="com.tokens.oneTimeUseToken" accessors="true" output="false" persistent="false">
	<cffunction name="init" access="public" output="false" returntype="any">
		<cfargument name="tokenTypeTextID" type="string" default="OAuth2Code" />
		<cfargument name="validity_seconds" type="numeric" default=300 />

		<cfreturn super.init(argumentCollection=arguments)>
	</cffunction>




	<cffunction name="setScope" access="public" output="false" returntype="void">
		<cfargument name="scope" type="string" required=true />
		<cfscript>
			set("scope",scope);
		</cfscript>
	</cffunction>

	<cffunction name="getScope" access="public" output="false" returntype="string">
		<cfscript>
			return get("scope");
		</cfscript>
	</cffunction>



	<cffunction name="setClientID" access="public" output="false" returntype="void">
		<cfargument name="clientID" type="string" required=true />
		<cfscript>
			set("clientID",clientID);
		</cfscript>
	</cffunction>

	<cffunction name="getClientID" access="public" output="false" returntype="string">
		<cfscript>
			return get("clientID");
		</cfscript>
	</cffunction>



	<cffunction name="setRedirectURL" access="public" output="false" returntype="void">
		<cfargument name="redirectURL" type="string" required=true />
		<cfscript>
			set("redirectURL",redirectURL);
		</cfscript>
	</cffunction>

	<cffunction name="getRedirectURL" access="public" output="false" returntype="string">
		<cfscript>
			return get("redirectURL");
		</cfscript>
	</cffunction>

</cfcomponent>