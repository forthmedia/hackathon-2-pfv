<!---
  --- OAuth2Code
  --- ----------
  --- 
  --- author: Richard.Tingle
  --- date:   15/07/15
  --->
<cfcomponent extends="com.tokens.oneTimeUseToken" accessors="true" output="false" persistent="false">
	<cfset this.defaultTokenTypeTextID="OAuth2ClientGenerationToken">
	
	<cffunction name="init" access="public" output="false" returntype="any">
		<cfargument name="tokenTypeTextID" type="string" default="#this.defaultTokenTypeTextID#" />
		<cfargument name="validity_seconds" type="numeric" default=7776000 /> <!---7776000 seconds =3 months ---->

		<cfreturn super.init(argumentCollection=arguments)>
	</cffunction>
	

	<cffunction name="setClientID" access="public" output="false" returntype="void">
		<cfargument name="clientID" type="string" required=true />
		<cfscript>
			set("clientID",clientID);
		</cfscript>
	</cffunction>
	
	<cffunction name="getClientID" access="public" output="false" returntype="string">
		<cfscript>
			return get("clientID");
		</cfscript>
	</cffunction>
	
	<!--- STATIC METHODS --->
	<cffunction name="invalidateAllTokensForClient" access="public" output="false" returntype="void" hint="This is a STATIC method, it doesn't need an instance">
		<cfargument name="client" type="OAuth2Client" required="false" hint="can pass a client or a clientID">
		<cfargument name="clientID" type="any" default="#client.getClientIdentifier()#" hint="can pass a client or a clientID">
		<cfscript>
			//first we get all the tokenIDs for this client, then we invalidate them. This should return at most 1 record so performance shouldn't be an 
			//issue and the number of OAuth2ClientGenerationToken should be no more than the number of clients
			var tokenIDQuery=new Query();
			tokenIDQuery.setDataSource(application.siteDataSource);
			
			tokenIDQuery.addParam(name="clientID",value="%#clientID#%",cfsqltype="cf_sql_varchar"); 
			tokenIDQuery.addParam(name="tokenType",value=this.defaultTokenTypeTextID,cfsqltype="cf_sql_varchar"); 
			
			result = tokenIDQuery.execute(sql="select token from token where tokenType=:tokenType and expiryDate>GETDATE() and metadata LIKE :clientID");
			resultSet=result.getResult();
			for (var i= 1 ; i LTE result.getPrefix().RecordCount ; i = (i + 1)){
				var tokenString=resultSet["token"][i];	 
				
				var token=new OAuth2ClientGenerationToken(token=tokenString);
				token.invalidateToken();
			}
		</cfscript>
	</cffunction>

</cfcomponent>