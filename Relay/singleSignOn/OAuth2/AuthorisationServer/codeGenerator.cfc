<!---
  --- codeGenerator
  --- -------------
  --- 
  --- author: Richard.Tingle
  --- date:   15/07/15
  --->
<cfcomponent accessors="true" output="false" persistent="false">

	<cffunction name="processAuthorisationRequest" hint="Take the parameters passed to the authorisation endpoint and return a code (if everything checks out)" access="public" output="false" returntype="struct">
		<cfargument name="response_type" type="string" required=true />
		<cfargument name="clientID" type="string" required=true />
		<cfargument name="redirectURL" type="string" required=true />
		<cfargument name="scope" type="string" required=true />
		<cfargument name="state" type="string" required=true />
		
		<cfscript>
			var returnStruct=structNew();
			var code="";
			var validationReport=validateRequest(response_type,client_id,redirect_uri,scope);
			
			
			if (validationReport.isOk){
				returnStruct.isOk=true;
				code=createObject("component","OAuth2Code").init(validity_seconds=application.com.settings.getSetting("OAuth.AuthenticationServer.codeValidityTime") );
				code.autogenerateToken();
				code.setPersonID(request.relayCurrentUser.personID);
				code.setClientID(clientID);		
				code.setRedirectURL(redirectURL);	
				code.setScope(scope);	
				code.persistToDatabase();
				returnStruct.code=code.getTokenValue();
			}else{
				returnStruct.isOk=false;
				returnStruct.code="";
				
				if (application.com.settings.getSetting("OAuth.AuthenticationServer.fullErrors")){
					returnStruct.explanation=validationReport.unsecureReason;
				}
				
				errorStruct=structNew();
				errorStruct.clientID=arguments.clientID;
				errorStruct.redirectURL=arguments.redirectURL;
				errorStruct.scope=arguments.scope;
				errorStruct.state=arguments.state;
				errorStruct.explanation=validationReport.unsecureReason;
			
				returnStruct.errorid=application.com.errorHandler.recordRelayError_Warning(type="OAuthAuthenticationError",Severity="error",WarningStructure=errorStruct);
				
				
				
			}
		
			return returnStruct;
		</cfscript>
		
	</cffunction>


	<cffunction name="validateRequest" hint="returns if a code is permitted to be issued" access="private" output="false" returntype="struct">
		<cfargument name="response_type" type="string" required=true />
		<cfargument name="client_id" type="string" required=true />
		<cfargument name="redirect_uri" type="string" required=true />
		<cfargument name="scope" type="string" required=true />
		
		<cfscript>
			var returnStruct=StructNew();
			var scopes = ListToArray(scope," ");   
			var oAuth2client="";
			if (comparenocase(response_type,"code") NEQ 0){
				returnStruct.isOk=false;  //only the code flow is supported
				returnStruct.unsecureReason="only the code flow is supported";
				return returnStruct;
			}
			oAuth2client =createObject("component","OAuth2Client");
			oAuth2Client.init(clientIdentifier=client_id);
			oAuth2Client.pullFromDatabase();
			
			if (not oAuth2Client.isValidOAuthClient()){
				returnStruct.isOk=false;  //not a known client
				returnStruct.unsecureReason="not a known client";
				return returnStruct;
			}
			if (not oAuth2Client.isCorrectRedirectURL(redirect_uri)){
				returnStruct.isOk=false;  //not a known redirect uri
				returnStruct.unsecureReason="not a known redirect uri";
				return returnStruct;
			}
			
			
			for(i=1; i <= ArrayLen(scopes); i++){
				if (not oAuth2Client.authorisedForScope(scopes[i])){					
					returnStruct.isOk=false; //not authorised for the requested scope
					returnStruct.unsecureReason="not authorised for the requested scope";
					return returnStruct;
				}
			}

			returnStruct.isOk=true; 
			returnStruct.unsecureReason="success";
			return returnStruct;
		</cfscript>
		

		
	</cffunction>

</cfcomponent>