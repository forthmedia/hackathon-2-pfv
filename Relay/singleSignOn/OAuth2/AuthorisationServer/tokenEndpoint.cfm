<!---
This is an endpoint that expects to have form data posted to it; 
Expects 
grant_type=authorization_code
code=AUTH_CODE
redirect_uri=REDIRECT_URI
client_id=CLIENT_ID
client_secret=CLIENT_SECRET

---->
<cfheader name="Content-Type" value="application/json; charset=utf-8">
<cfif StructKeyExists(form,"grant_type") and
	  StructKeyExists(form,"code") and 
	  StructKeyExists(form,"redirect_uri") and
	  StructKeyExists(form,"client_id") and
	  StructKeyExists(form,"client_secret")>
	  
	  <cfset tokenGenerator=new tokenGenerator()>
	  <cfset tokenReturn=tokenGenerator.processTokenRequest(form.grant_type,form.client_id,form.code,form.redirect_uri,form.client_secret)>

	  <cfif tokenReturn.isOk>
	  		<cfscript>
				endpointResponse=structNew();
				endpointResponse["access_token"]=tokenReturn.token.getTokenValue();
				endpointResponse["expires_in"]=tokenReturn.token.getExpiresIn_seconds();
				endpointResponse["scope"]=tokenReturn.token.getScope();
				endpointResponse["token_type"]="Bearer";
			</cfscript>
	  
	      <cfsavecontent variable="jsonReturn"><cfoutput>#serializeJSON(endpointResponse)#</cfoutput></cfsavecontent>
	  <cfelse>	  
	  
	  	  <cfif structKeyExists(tokenReturn, "explanation")>
		  	
		  	<cfparam name="tokenReturn.errorID" default="Unknown">
		  	
		  	<!--- <cfcontent reset="true"> doesn't clear things like scripts so we have to save content then set the cfcontent'--->
		  	<cfsavecontent variable="jsonReturn">{ "success":"false", "message": "<cfoutput>#tokenReturn.explanation# - ErrorID: #tokenReturn.errorID#</cfoutput> - Full explanation shown because fullErrors option has been selected in settings"}</cfsavecontent>
		  <cfelse>	
			<cfsavecontent variable="jsonReturn">{ "success":"false", "message": "Invalid request. ErrorID: <cfoutput>#tokenReturn.errorID#"</cfoutput> }</cfsavecontent>
		  </cfif>
 	  
	  	  
	  	  
	  </cfif>
	  <cfcontent type="application/json" variable="#toBinary(toBase64(jsonReturn))#" /><cfabort/>
<cfelse>	 
	   <!--- <cfcontent reset="true"> doesn't clear things like scripts so we have to save content then set the cfcontent'--->
       <cfsavecontent variable="jsonReturn">{ "success":"false", "message": "All required fields were not passed" }</cfsavecontent>
   	   <cfcontent type="application/json" variable="#toBinary(toBase64(jsonReturn))#" /><cfabort/>

</cfif>