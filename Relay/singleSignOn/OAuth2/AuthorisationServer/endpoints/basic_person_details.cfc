/**
 * basic_user_data
 *
 * @author Richard.Tingle
 * @date 15/09/15
 **/
component extends="Relay.singleSignOn.OAuth2.AuthorisationServer.PersonOrgLocDataEndpoint" accessors=true output=false persistent=false {
	public struct function init(){
		super.init('firstname,email,lastname,officePhone,mobilePhone,FaxPhone,crmPerID','SiteName,crmLocID,Address1,Address2,Address3,Address4,Address5,PostalCode,CountryDescription,locEmail','OrganisationName,OrgURL,crmOrgID');
		return this;
	}


}