<!---
  --- OAuth2Code
  --- ----------
  ---
  --- author: Richard.Tingle
  --- date:   15/07/15
  --->
<cfcomponent extends="com.tokens.token" accessors="true" output="false" persistent="false">
	<cffunction name="init" access="public" output="false" returntype="any">
		<cfargument name="tokenTypeTextID" type="string" default="OAuth2Token" />
		<cfargument name="validity_seconds" type="numeric" default=3000 />

		<cfreturn super.init(argumentCollection=arguments)>
	</cffunction>

	<cffunction name="setClientID" access="public" output="false" returntype="void">
		<cfargument name="clientID" type="string" required=true />
		<cfscript>
			set("clientID",clientID);
		</cfscript>
	</cffunction>

	<cffunction name="getClientID" access="public" output="false" returntype="string">
		<cfscript>
			return get("clientID");
		</cfscript>
	</cffunction>



	<cffunction name="setScope" access="public" output="false" returntype="void">
		<cfargument name="scope" type="string" required=true />
		<cfscript>
			set("scope",scope);
		</cfscript>
	</cffunction>

	<cffunction name="getScope" access="public" output="false" returntype="string">
		<cfscript>
			return get("scope");
		</cfscript>
	</cffunction>

	<cffunction name="permitsScope" access="public" output="false" returntype="boolean">
		<cfargument name="requestedScope" type="string" required=true />
		<cfscript>
			var scopes=ListToArray(getScope(), " "); //scope is a space deliminated list per the OAuth standard

			for(i=1;i<=Arraylen(scopes);i++){
				if (scopes[i]==requestedScope){
					return true;
				}
			}
			return false;
		</cfscript>
	</cffunction>


	<cffunction name="getExpiresIn_seconds" access="public" output="false" returntype="string">
		<cfscript>
			return DateDiff("s",now(), getExpiryDate());
		</cfscript>
	</cffunction>


</cfcomponent>