<!--- A page that is hit with a 1 time use code that refers to a prototype client, hitting this page generates the clientSecret and
reports it once, and once only --->

<head>
<style media="screen" type="text/css">

table { margin: 1em; }
td, th { padding: .3em; border: 1px #ccc solid; }

</style>


</head>


<cfset fullExplanations=application.com.settings.getSetting("OAuth.AuthenticationServer.fullErrors")>

<cfif structKeyExists(url,"clientToken")>
	<cfscript>
		//just used to make sure some valid scopes haven't been deleted in the mean time (thats a UI issue only as they could never actually be used)
		editorBackingBean=new singleSignOn.common.editorBackingBean();
		scopeManager=new ScopeManager();
		allScopes=scopeManager.getAllEndpoints(namesOnly=true);
		
		
		token=createObject("component","OAuth2ClientGenerationToken").init(token=url.clientToken);
		
		token.pullFromDatabase();
	</cfscript>
	
	<cfif token.isValidToken()>
		<cfscript>
			oauth2client=createObject("component","OAuth2Client").init(clientIdentifier=token.getClientID());
			oauth2client.pullFromDatabase();
		</cfscript>
		
		<cfif oauth2client.isValidOAuthClient()>
			<cfset clientSecret=oauth2client.autogenerateClient()>
			<cfset oauth2client.persistToDatabase()>
			<h1><strong>This screen will show only once</strong></h1>
		
			<p>
				Client secret is considered secure information, store securely
			</p>
			<br>
			
			<table>
			  <tr>
			    <td><strong>clientID:</strong></td>
			    <td><cfoutput>#oauth2client.getClientIdentifier()#</cfoutput></td> 
			  </tr>
			  <tr>
			    <td><strong>clientSecret:</strong></td>
			    <td><cfoutput>#clientSecret#</cfoutput></td> 
			  </tr>
			  <tr>
			    <td><strong>redirectionURL:</strong></td>
			    <td><cfoutput>#HTMLEditFormat(oauth2client.getRedirectURL())#</cfoutput></td> 
			  </tr>
			  <tr>
			    <td><strong>permittedScope(s):</strong></td>
			    <td><cfoutput>#HTMLEditFormat(arrayToList(editorBackingBean.validateList(oauth2client.getPermittedScopes(namesOnly=true),allScopes).allowedFields," "))#</cfoutput></td> 
			  </tr>
			</table>
			
		<cfelse>
			<p>Unknown, expired or invalid client or token</p>
			
			<cfif fullExplanations>
				<p>oauth2client is not valid</p>
			</cfif>
			
		</cfif>
		
	<cfelse>
		<p>Unknown, expired or invalid client or token</p>
	
	</cfif>
	
	
<cfelse>
	<p>Unknown, expired or invalid client or token</p>
	
</cfif>

