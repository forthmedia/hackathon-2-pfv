<!---
  --- POLdataEndpoint
  --- ---------------
  ---
  --- An abstract class provides a base for classes that are just pulling out
  --- data from people, locations and organisations
  ---
  --- author: Richard.Tingle
  --- date:   15/09/15
  --->
<cfcomponent implements="Relay.singleSignOn.OAuth2.AuthorisationServer.EndpointTemplate" hint="An abstract class provides a base for classes that are just pulling out data from people, locations and organisations" accessors="true" output="false" persistent="false">


	<cffunction name="init" access="public" output="false" returntype="any">

		<cfargument name="personFieldList" type="any" required="true" hint="a list or array" />
		<cfargument name="locationFieldList" type="any" required="true"  hint="a list or array"  />
		<cfargument name="organisationFieldList" type="any" required="true"  hint="a list or array"  />

		<cfscript>
			setPersonFields(arguments.personFieldList);
			setLocationFields(arguments.locationFieldList);
			setOrganisationFields(arguments.organisationFieldList);

		</cfscript>

	</cffunction>


	<cffunction name="setPersonFields" access="public" output="false" returntype="void">
		<cfargument name="personFields" type="any" required="true"  />
		<cfscript>

			if (isArray(arguments.personFields)){
				this.personFields=arguments.personFields;
			}else{
				this.personFields=listToArray(arguments.personFields);
			}
			
			ArrayDelete(this.personFields,"Password"); //its the salted hashed password which shouldn't have ayn value, but we're still not going to give it out
			ArrayDelete(this.personFields,"PasswordDate");
			
		</cfscript>
	</cffunction>

	<cffunction name="setLocationFields" access="public" output="false" returntype="void">
		<cfargument name="locationFields" type="any" required="true"  />
		<cfscript>
			if (isArray(arguments.locationFields)){
				this.locationFields=arguments.locationFields;
			}else{
				this.locationFields=listToArray(arguments.locationFields);
			}
		</cfscript>
	</cffunction>

	<cffunction name="setOrganisationFields" access="public" output="false" returntype="void">
		<cfargument name="organisationFields" type="any" required="true"  />
		<cfscript>
			if (isArray(arguments.organisationFields)){
				this.organisationFields=arguments.organisationFields;
			}else{
				this.organisationFields=listToArray(arguments.organisationFields);
			}
		</cfscript>
	</cffunction>

	<cffunction name="getPersonFields" access="public" output="false" returntype="array">

		<cfscript>
			return this.personFields;
		</cfscript>
	</cffunction>

	<cffunction name="getLocationFields" access="public" output="false" returntype="array">

		<cfscript>
			return this.locationFields;
		</cfscript>
	</cffunction>

	<cffunction name="getOrganisationFields" access="public" output="false" returntype="array">

		<cfscript>
			return this.organisationFields;
		</cfscript>
	</cffunction>

	<cffunction name="processRequest" access="public" output="false" returntype="struct">
		<cfargument name="personID" type="numeric" required="true"  />
		<cfargument name="endPointArguments" type="struct" required="true"  />

		<cfscript>
			return queryData(personID);
		</cfscript>
	</cffunction>

	<cffunction name="getRequiredFields" access="public" output="false" returntype="struct">
		<cfscript>
			return StructNew(); //we have no required fields
		</cfscript>
	</cffunction>

	<cffunction name="getDescription" access="public" output="false" returntype="string">
		<cfscript>
			return 
			"A collection of data related to the Person, their location and their organisation <br>" &
			"<strong>Person</strong>:  #arrayToList(getPersonFields())# <br>" &
			"<strong>Location</strong>:  #arrayToList(getLocationFields())# <br>" &
			"<strong>Organisation</strong>:  #arrayToList(getOrganisationFields())#";

			
		</cfscript>
	</cffunction>

	<cffunction name="queryData" access="private" output="false" returntype="any">
		<cfargument name="personID" type="string" required="true" />


		<cfscript>
			var personDataArray="";
			var idArray="";
			var locationID="";
			var organisationID="";
			var locationDataArray="";
			var organisationDataArray="";
			var overallDataStruct=structNew();

			// grab required IDs
			var locationID =application.com.relayEntity.getEntity(EntityID=arguments.personID,EntityTypeID=application.entityTypeID.person, fieldList="locationID", testLoggedInUserRights=false).recordSet.locationID;
			var organisationID =application.com.relayEntity.getEntity(EntityID=locationID,EntityTypeID=application.entityTypeID.location, fieldList="organisationID", testLoggedInUserRights=false).recordSet.organisationID;




			//now grab the requested data that will be returned
			var personLevelData=application.com.relayEntity.getEntity(EntityID=arguments.personID, EntityTypeID=Application.entityTypeID.person, fieldList=arrayToList(this.personFields), testLoggedInUserRights=false);
			var locationLevelData=application.com.relayEntity.getEntity(EntityID=locationID, EntityTypeID=Application.entityTypeID.location, fieldList=arrayToList(this.locationFields), testLoggedInUserRights=false);
			var organisationLevelData=application.com.relayEntity.getEntity(EntityID=organisationID, EntityTypeID=Application.entityTypeID.organisation, fieldList=arrayToList(this.organisationFields), testLoggedInUserRights=false);

			appendQueryToStruct(overallDataStruct,personLevelData.recordSet);
			appendQueryToStruct(overallDataStruct,locationLevelData.recordSet);
			appendQueryToStruct(overallDataStruct,organisationLevelData.recordSet);


			return overallDataStruct;

		</cfscript>

		<cfreturn overallDataStruct>

	</cffunction>

	<cffunction name="appendQueryToStruct" access="private" output="false" returntype="struct" >
		<cfargument name="currentStruct" type="struct" required="true" />
		<cfargument name="query" type="any" required="true" />

		<cfscript>
			
			if (not IsSimpleValue(query)){ //seems like relayEntity returns "" when no records  are requested, I do not approve - RJT
				var columns=query.getColumnNames();
				for(i=1; i <= ArrayLen(columns); i++){
					var columnName=columns[i];
					var data=query[columnName];
					currentStruct[columnName]=data;
				}
			}
			return currentStruct;
		</cfscript>


	</cffunction>

	<cffunction name="santiseFieldList" access="private" output="false" returntype="any" hint="As cf_queryparam cannot be used for dynamic columns we have to roll our own, not as good as cf_queryparam but the values we're sanitising aren't user enterable anyway (except through very high level,internal user permissions)">
		<cfargument name="fieldList" type="string" required="true" />


			<cfset var fieldArray=ListToArray(fieldList)>

			<cfloop
			    index = "i"
			    from = "1"
			    to = "#arrayLen(fieldArray)#"
			    step = "1" >

				   <CFPARAM name="#fieldArray[i]#" type="variableName" default="N0_VALUE_SENT">   <!--- throws an exception on semicolons etc, encoded or otherwise--->
				   <cfset fieldArray[i]=rereplacenocase(fieldArray[i],'[^a-z0-9_]','','all')>
			</cfloop>

			<cfreturn ArrayToList(fieldArray)>

	</cffunction>


	<cffunction name="QueryToStruct" access="public" returntype="any" output="false"
	    hint="Converts an entire query or the given record to a struct. This might return a structure (single record) or an array of structures.">

	    <!--- Define arguments. --->
	    <cfargument name="Query" type="query" required="true" />
	    <cfargument name="Row" type="numeric" required="false" default="0" />

	    <cfscript>
	        // Define the local scope.
	        var LOCAL = StructNew();
	        // Determine the indexes that we will need to loop over.
	        // To do so, check to see if we are working with a given row,
	        // or the whole record set.
	        if (ARGUMENTS.Row){
	            // We are only looping over one row.
	            LOCAL.FromIndex = ARGUMENTS.Row;
	            LOCAL.ToIndex = ARGUMENTS.Row;
	        } else {
	            // We are looping over the entire query.
	            LOCAL.FromIndex = 1;
	            LOCAL.ToIndex = ARGUMENTS.Query.RecordCount;
	        }
	        // Get the list of columns as an array and the column count.
	        LOCAL.Columns = ListToArray( ARGUMENTS.Query.ColumnList );
	        LOCAL.ColumnCount = ArrayLen( LOCAL.Columns );
	        // Create an array to keep all the objects.
	        LOCAL.DataArray = ArrayNew( 1 );
	        // Loop over the rows to create a structure for each row.
	        for (LOCAL.RowIndex = LOCAL.FromIndex ; LOCAL.RowIndex LTE LOCAL.ToIndex ; LOCAL.RowIndex = (LOCAL.RowIndex + 1)){
	            // Create a new structure for this row.
	            ArrayAppend( LOCAL.DataArray, StructNew() );
	            // Get the index of the current data array object.
	            LOCAL.DataArrayIndex = ArrayLen( LOCAL.DataArray );
	            // Loop over the columns to set the structure values.
	            for (LOCAL.ColumnIndex = 1 ; LOCAL.ColumnIndex LTE LOCAL.ColumnCount ; LOCAL.ColumnIndex = (LOCAL.ColumnIndex + 1)){
	                // Get the column value.
	                LOCAL.ColumnName = LOCAL.Columns[ LOCAL.ColumnIndex ];
	                // Set column value into the structure.
	                LOCAL.DataArray[ LOCAL.DataArrayIndex ][ LOCAL.ColumnName ] = ARGUMENTS.Query[ LOCAL.ColumnName ][ LOCAL.RowIndex ];
	            }
	        }
	        // At this point, we have an array of structure objects that
	        // represent the rows in the query over the indexes that we
	        // wanted to convert. If we did not want to convert a specific
	        // record, return the array. If we wanted to convert a single
	        // row, then return the just that STRUCTURE, not the array.
	        if (ARGUMENTS.Row){
	            // Return the first array item.
	            return( LOCAL.DataArray[ 1 ] );
	        } else {
	            // Return the entire array.
	            return( LOCAL.DataArray );
	        }
	    </cfscript>
	</cffunction>

	<cffunction name="getScopeName"  access="public" returntype="string">
		<cfreturn this.scopeName>
	</cffunction>

	<cffunction name="setScopeName"  access="public" returntype="void">
		<cfargument name="scopeName" type="string" required="true" />
		<cfset this.scopeName=arguments.scopeName>
	</cffunction>



</cfcomponent>