
<cfcomponent accessors="true" output="false" persistent="false">

	<cffunction name="init" access="public" output="false" returntype="any">
		<cfargument name="requirehttps" type="boolean" default="#application.com.settings.getSetting('OAuth.AuthenticationServer.requireHTTPS')#" hint="OAuth assumed an https connection allow http only for testing or where annother way of securing the pipeline is used"/>
		<cfargument name="scopeManager" type="any" default="#new ScopeManager()#">
		<cfargument name="validateClient" type="any" default="true">
		<cfscript>
			this.requirehttps=arguments.requirehttps;
			this.scopeManager=arguments.scopeManager;
			this.validateClient=arguments.validateClient;
			
			return this;
		</cfscript>
	</cffunction>


	<cffunction name="processDataRequest" access="public" output="false" returntype="struct">
		<cfargument name="authorisation" type="string" required="true" />
		<cfargument name="endpointName" type="string" required="true"  />
		<cfargument name="recievedArguments" type="struct" required="true"  />

		<cfscript>

			var returnStruct=structNew();
			var validationReport=validateRequest(authorisation,endpointName);
			var relaywarePersonID=0;


			if (validationReport.isOk){
				returnStruct.isOk=true;


				tokenObject=validationReport.token;
				relaywarePersonID=tokenObject.getPersonID();

				var endPoint=this.scopeManager.getEndpoint(endpointName);

				for(requestedArgument in endPoint.getRequiredFields()){
					if (not structKeyExists(recievedArguments,requestedArgument)){
						var reasonText="Parameter " & requestedArgument & "was required for use of endpoint " & endpointName & " but was not passed";
						if (application.com.settings.getSetting("OAuth.AuthenticationServer.fullErrors")){
							returnStruct.explanation=reasonText;
						}

						errorStruct=structNew();
						errorStruct.endpointName=arguments.endpointName;
						errorStruct.authorisation=arguments.authorisation;
						errorStruct.explanation=reasonText;
						returnStruct.data="";
						returnStruct.errorID=application.com.errorHandler.recordRelayError_Warning(type="OAuthAuthenticationError",Severity="error",WarningStructure=errorStruct);
						return returnStruct;
					}
				}




				returnStruct.data=endPoint.processRequest(relaywarePersonID,recievedArguments);
				//returnStruct.data=queryData(relaywarePersonID,'firstname,email,lastname,officePhone,mobilePhone,FaxPhone,crmPerID','SiteName,crmLocID,Address1,Address2,Address3,Address4,Address5,PostalCode,CountryDescription,locEmail','OrganisationName,OrgURL,crmOrgID');

				//returnStruct.data.ResellerStatus=application.com.flags.getFlag(FlagID="ResellerType", EntityID=);

				//=application.com.relayEntity.getEntity(ENTITYID=relaywarePersonID, ENTITYTYPEID =application.entityTypeID.person, fieldList='firstname,email,lastname,officePhone,mobilePhone,FaxPhone,crmPerID');
				//returnStruct.data=application.com.relayEntity.getEntity(ENTITYID=3, ENTITYTYPEID =application.entityTypeID.person, fieldList='firstname');
			}else{
				returnStruct.isOk=false;
				returnStruct.token="";

				if (application.com.settings.getSetting("OAuth.AuthenticationServer.fullErrors")){
					returnStruct.explanation=validationReport.unsecureReason;
				}

				errorStruct=structNew();
				errorStruct.endpointName=arguments.endpointName;
				errorStruct.authorisation=arguments.authorisation;
				errorStruct.explanation=validationReport.unsecureReason;
				returnStruct.data="";
				returnStruct.errorID=application.com.errorHandler.recordRelayError_Warning(type="OAuthAuthenticationError",Severity="error",WarningStructure=errorStruct);

			}

			if (returnStruct.isOk){
				returnStruct.json=safeSerialiseToJSON(returnStruct.data);
			}else{
				returnStruct.json="";
			}

			return returnStruct;

		</cfscript>
	</cffunction>



	<cffunction name="validateRequest" hint="returns if a token permits data to be issued and unsecureReason as the explanation" access="private" output="false" returntype="struct">
		<cfargument name="authorisation" type="string" required="true" />
		<cfargument name="endpointName" type="string" required="true"  />


		<cfscript>
			var tokenType="bearer";
			var token="";
			var oAuth2Token="";
			var returnStruct =StructNew();
			if (!authorisation.toLowerCase().startsWith(tokenType & " ")){
				returnStruct.isOK=false;
				returnStruct.unsecureReason="Authorisation did not begin with a valid token type";
				return returnStruct;
			}
			token=authorisation.substring(tokenType.length()+1); //remove token type prefix

			var oAuth2Token =createObject("component","OAuth2Token");
			oAuth2Token.init(token=token);
			oAuth2Token.pullFromDatabase();

			if (not oAuth2Token.isValidToken()){
				returnStruct.isOK=false;
				returnStruct.unsecureReason="token is unknown or expired";
				return returnStruct;
			}


			if (this.validateClient){
				
			    oAuth2Client=createObject("component","OAuth2Client");
			    oAuth2Client.init(clientIdentifier=oAuth2Token.getClientID());
			    oAuth2Client.pullFromDatabase();
			    if (not oAuth2Client.isValidOAuthClient()){
			    	returnStruct.isOK=false;
			    	returnStruct.unsecureReason="client associated with token is invalid";
				return returnStruct;
			    }

			    if (not oAuth2Client.authorisedForScope(endpointName)){
				returnStruct.isOK=false;
				returnStruct.unsecureReason="requested scope " & endpointName & " is not authorised for this client" ;
				return returnStruct;
			    }

			}

			if (not oAuth2Token.permitsScope(endpointName)){
				returnStruct.isOK=false;
				returnStruct.unsecureReason="requested scope " & endpointName & " was not present in the valid scopes (note: the scope must both be in the client applications approved scope and have been requested by the client when the token was generated)" ;
				return returnStruct;
			}



			
			if (not this.scopeManager.endpointExists(endpointName)){
				returnStruct.isOK=false;
				returnStruct.unsecureReason="Although you were authorised to access " & endpointName & " it does not appear to exist, it may have been deleted" ;
				return returnStruct;
			}
			
			var isSecure=CGI.SERVER_PROTOCOL.toLowerCase().startswith("https");
			if (this.requirehttps AND Not isSecure){
				
				returnStruct.isOK=false;
				returnStruct.unsecureReason="OAuth2 requires secure endpoints, this endpoint is not secure";
				return returnStruct;
			}
			
			
			
			returnStruct.isOK=true;
			returnStruct.unsecureReason="success";
			returnStruct.token=oAuth2Token;

			return returnStruct;

		</cfscript>

	</cffunction>


	<cffunction name="safeSerialiseToJSON" access="private" output="false" returntype="any">
		<cfargument name="structure" type="struct" required="true" />


			<!---Because serializeJSON is buggy and converts postcode strings like "90458" to numbers 90458.0
			//we have to trick it into behaving correctly
			--->
			<cfset var stringHoldingValue="vcb9hkilkaas12ad45gdhdfsdgzasdvbnlawdddfsdg"> <!--- just some noise --->
			<cfset var modifiedStructure=StructCopy(structure)>
			<cfset recursiveNoiseAdd(modifiedStructure,stringHoldingValue)>
			
			<!---
			<cfset var jsonString="">

			<cfscript>
				  for (itemKey in modifiedStructure){
				     modifiedStructure[itemKey]=stringHoldingValue & modifiedStructure[itemKey];
				  }

			</cfscript>
			--->

			<cfset jsonString=SerializeJSON(modifiedStructure)>
			<cfset jsonString=Replace(jsonString, stringHoldingValue, "","all")>

			<cfreturn jsonString>

	</cffunction>
	<cffunction name="recursiveNoiseAdd" access="private" output="false" returntype="void" hint="puts noise in front of everything so that (for example) zip codes aren't converted to numbers (possibly loosing their 0 at the front)">
		<cfargument name="structure" type="struct" required="true" />
		<cfargument name="noise" type="string" required="true" />
		<cfscript>
			for (itemKey in structure){
				item=structure[itemKey];
				if (isStruct(item)){
					recursiveNoiseAdd(item,noise);
				}else{
					structure[itemKey]=noise & structure[itemKey];
				}
			}
		
		</cfscript>
	</cffunction>


</cfcomponent>