/**
 * A base CFC that defines the required functions of an OAuth2 end point,
 * the OAuth2 dataendpoint will search in Relay/singleSignOn/OAuth2/AuthorisationServer/endpoints
 * and singleSignOn/OAuth2/AuthorisationServer/endpoints for cfcs that extend this cfc.
 * They then represent the scopes that are provided by the resource server.
 * They are required to have a blank constructor
 *
 * @author Richard.Tingle
 * @date 15/09/15
 **/
interface{


	/**
	 * process the request from the 3rd party. At this point the request is authenticated
	 * to use this scope. A struct should be returned that will be serialised to JSON.
	 *  Note the arguments that the server recieves are passed to this function in arguments, required
	 * fields can be specifed in child classes (using the getRequiredFields() and error
	 * messages will be automatically generated. Other errors should be handled by the class itself.
	 **/
	public struct function processRequest(required numeric personID, required struct endPointArguments);

	/**
	 * Return the minimum fields that the end point expects to function correctly. Expected to return a
	 * struct of key - key description pairs
	 **/
	public struct function getRequiredFields();


	/**
	 * Used in UI documentation
	 **/
	public string function getDescription();


	/**
	 * Used automatically for functions pulled from files to provide them with cfc name = scope name,
	 * just implement as return this.scopeName (I miss default implementations and abstract classes)
	 **/
	public string function getScopeName();

	/**
	 * Used automatically for functions pulled from files to provide them with cfc name = scope name,
	 * just implement as this.scopeName=scopeName (I miss default implementations and abstract classes)
	 **/
	public void function setScopeName(required string scopeName);
	

}