
<cfcomponent accessors="true" output="false" persistent="false">

	<cffunction name="init" access="public" output="false" returntype="any">
		<cfargument name="requirehttps" type="boolean" default="#application.com.settings.getSetting('OAuth.AuthenticationServer.requireHTTPS')#" hint="OAuth assumed an https connection allow http only for testing or where annother way of securing the pipeline is used"/>
		<cfset this.requirehttps=requirehttps>
		<cfreturn this>
		
	</cffunction>

	<cffunction name="processTokenRequest" hint="Take the parameters passed to the authorisation endpoint and return a code (if everything checks out)" access="public" output="false" returntype="struct">
		<cfargument name="grant_type" type="string" required="true" />
		<cfargument name="clientID" type="string" required="true"  />
		<cfargument name="code" type="string" required="true" />
		<cfargument name="redirect_uri" type="string" required="true"  />
		<cfargument name="client_secret" type="string" required="true"  />
		

		<cfscript>
			var returnStruct=structNew();
			
			var validationReport=validateRequest(grant_type,client_id,code,redirect_uri,client_secret);
			
			if (validationReport.isOk){
				returnStruct.isOk=true;
				

				
				token=createObject("component","OAuth2Token").init(validity_seconds=application.com.settings.getSetting("OAuth.AuthenticationServer.tokenValidityTime") );
				token.autogenerateToken();
				
				token.setPersonID(validationReport.codeObject.getPersonID());
				token.setScope(validationReport.codeObject.getScope());
				token.setClientID(clientID);		
				token.persistToDatabase();
				returnStruct.token=token;
			}else{
				returnStruct.isOk=false;
				returnStruct.access_token="";
				
				if (application.com.settings.getSetting("OAuth.AuthenticationServer.fullErrors")){
					returnStruct.explanation=validationReport.unsecureReason;
				}
				
				errorStruct=structNew();
				errorStruct.grant_type=arguments.grant_type;
				errorStruct.clientID=arguments.clientID;
				errorStruct.code=arguments.code;
				errorStruct.redirect_uri=arguments.redirect_uri;
				errorStruct.explanation=validationReport.unsecureReason;
				
				if (structKeyExists(form,"CLIENT_SECRET")){
					form.CLIENT_SECRET="REDACTED - CLIENT SECRET SHOULD NOT BE IN LOGS";
				}
				
				returnStruct.errorID=application.com.errorHandler.recordRelayError_Warning(type="OAuthAuthenticationError",Severity="error",WarningStructure=errorStruct);
				
			}
		
		
		
			return returnStruct;
		</cfscript>
	</cffunction>


	<cffunction name="validateRequest" hint="returns if a code is permitted to be issued as isOK and unsecureReason as the explanation" access="private" output="false" returntype="struct">
		<cfargument name="grant_type" type="string" required="true"  />
		<cfargument name="clientID" type="string" required="true"  />
		<cfargument name="code" type="string" required="true"  />
		<cfargument name="redirect_uri" type="string" required="true"  />
		<cfargument name="client_secret" type="string" required="true"  />
		

		<cfscript>  
			var oAuth2client="";
			var returnStruct=StructNew();
			if (comparenocase(grant_type,"authorization_code") NEQ 0){
				returnStruct.isOK=false;
				returnStruct.unsecureReason="grant_type was " & grant_type & "but should be authorization_code";
				return returnStruct; //only the code flow is supported
			}
			oAuth2client =createObject("component","OAuth2Client");
			oAuth2Client.init(clientIdentifier=client_id);
			oAuth2Client.pullFromDatabase();
			if (not oAuth2Client.isValidOAuthClient()){
				
				returnStruct.isOK=false;
				returnStruct.unsecureReason="oAuth2Client not valid";
				return returnStruct; //not a known client
			}
			if (not oAuth2Client.isCorrectRedirectURL(redirect_uri)){
				returnStruct.isOK=false;
				returnStruct.unsecureReason="redirectURL incorrect";
				return returnStruct; //not a known redirect uri
			}
			
			if (not oAuth2Client.validateClientSecret(client_secret)){
				returnStruct.isOK=false;
				returnStruct.unsecureReason="incorrect client secret";
				return returnStruct; //not a known redirect uri
			}
			


			codeObject=createObject("component","OAuth2Code");
			codeObject.init(token=code);
			codeObject.pullFromDatabase();
			if (not codeObject.isValidToken()){
				returnStruct.isOK=false;
				returnStruct.unsecureReason="Unknown, already used or expired code";
				return returnStruct; //not a known redirect uri
			}
			
			if (not codeObject.getClientID() EQ clientID){
				returnStruct.isOK=false;
				returnStruct.unsecureReason="Code was not requested for this client";
				return returnStruct; //not a known redirect uri
			}
				
			var isSecure=CGI.SERVER_PROTOCOL.toLowerCase().startswith("https");
			if (this.requireHTTPS AND Not isSecure){
				
				returnStruct.isOK=false;
				returnStruct.unsecureReason="OAuth2 requires secure endpoints, this endpoint is not secure";
				return returnStruct; //not a known redirect uri
			}
			
			returnStruct.isOK=true;
			returnStruct.unsecureReason="Success";
			returnStruct.codeObject=codeObject;
			return returnStruct; 
			
		</cfscript>
		
	</cffunction>

</cfcomponent>