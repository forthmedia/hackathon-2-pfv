
<cfscript>
	headers=GetHttpRequestData().headers;
</cfscript>

<cfif StructKeyExists(headers,"authorization")>>
	<cfif StructKeyExists(url,"endpoint") OR  StructKeyExists(form,"endpoint")>

		  <cfscript>
			  passedParameters=structNew();
			  StructAppend(passedParameters,form);
			  StructAppend(passedParameters,url);

			  dataGenerator=new dataGenerator();
			  dataReturn=dataGenerator.processDataRequest(headers.authorization,passedParameters.endpoint,passedParameters);
		  </cfscript>

		  <cfif dataReturn.isOk>

			  <cfsavecontent variable="jsonReturn"><cfoutput>#dataReturn.json#</cfoutput></cfsavecontent>
		  <cfelse>

			  <cfparam name="dataReturn.errorID" default="Unknown">
		  	  <cfif structKeyExists(dataReturn, "explanation")>
			  	  <cfsavecontent variable="jsonReturn">{ "success":"false", "message": "<cfoutput>#dataReturn.explanation# - ErrorID: #dataReturn.errorID# - Full explanation shown because fullErrors option has been selected in settings</cfoutput>" }</cfsavecontent>
			  <cfelse>
			  	  <cfsavecontent variable="jsonReturn">{ "success":"false", "message": "Invalid request -<cfoutput>ErrorID: #dataReturn.errorID# </cfoutput>" }</cfsavecontent>
			  </cfif>

		  </cfif>
		  <cfcontent type="application/json" variable="#toBinary(toBase64(jsonReturn))#" /><cfabort/>
	<cfelse>
	  	<!--- <cfcontent reset="true"> doesn't clear things like scripts so we have to save content then set the cfcontent'--->
       <cfsavecontent variable="jsonReturn">{ "success":"false", "message": "endpoint not sent as a url parameter or form parameter" }</cfsavecontent>
   	   <cfcontent type="application/json" variable="#toBinary(toBase64(jsonReturn))#" /><cfabort/>

	</cfif>
<cfelse>

	   <!--- <cfcontent reset="true"> doesn't clear things like scripts so we have to save content then set the cfcontent'--->
       <cfsavecontent variable="jsonReturn">{ "success":"false", "message": "Authorisation not sent as a header" }</cfsavecontent>
   	   <cfcontent type="application/json" variable="#toBinary(toBase64(jsonReturn))#" /><cfabort/>

</cfif>


