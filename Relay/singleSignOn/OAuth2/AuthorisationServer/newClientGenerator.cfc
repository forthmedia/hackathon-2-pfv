<!---
  --- newClientGenerator
  --- ------------------
  --- 
  --- author: Richard.Tingle
  --- date:   16/07/15
  --->
<cfcomponent accessors="true" output="false" persistent="false">


	<cffunction name="createNewClient" access="public" output="false" returntype="struct">
		<cfargument name="clientName" type="String" required="true" />
		<cfargument name="redirectURL" type="String" required="true" />
		
		<cfscript>
			var clientDetails=StructNew();
			var oauth2client=createObject("component","OAuth2Client");
			oauth2client.init(clientName);
			oauth2client.setRedirectURL(arguments.redirectURL);
			clientDetails.plainTextSecret=oauth2client.autogenerateClient();
			oauth2client.persistToDatabase();
			clientDetails.clientName=arguments.clientName;
			clientDetails.clientIdentifier=oauth2client.getClientIdentifier();
			
			
		
		</cfscript>
		
		<cfreturn clientDetails/>
	</cffunction>
</cfcomponent>