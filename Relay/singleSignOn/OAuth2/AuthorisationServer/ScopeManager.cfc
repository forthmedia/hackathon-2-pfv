/**
 * scopeManager
 *
 * @author Richard.Tingle
 * @date 21/09/15
 **/
component accessors=true output=false persistent=false {


	public any function init(boolean searchCustomEndpoints=true, array scopeSearchLocations_dotNotation=["singleSignOn.OAuth2.AuthorisationServer.endpoints","code.singleSignOn.OAuth2.AuthorisationServer.endpoints","code.CFTemplates.Modules.singleSignOn.OAuth2.AuthorisationServer.endpoints"]){
		this.scopeSearchLocations_dotNotation=arguments.scopeSearchLocations_dotNotation;
		this.searchCustomEndpoints=searchCustomEndpoints;
		return this;
	}

	public void function createCustomScope(required string scopeName, required string personFields hint="a comma deliminated list of fields", required string locationFields, required string organisationFields) output="false" {
		var newScope=new CustomScope(scopeName,personFields,locationFields,organisationFields);
		persistScope(newScope);
	}

	public void function deleteCustomScope(required string scopeName) output="false" {
		var deleteScopeQuery=new query();
		deleteScopeQuery.addParam(name="scope",value=scopeName,cfsqltype="cf_sql_varchar");
		var result = deleteScopeQuery.execute
					(sql="
						DELETE FROM OAuth2CustomScope
						where scope=:scope
					");
		//delete all "hanging" permissions (this can't be a constraint because all scopes don't exist in the DB) - RJT
		var deleteScopePermissionsQuery=new query();
		deleteScopeQuery.addParam(name="scope",value=scopeName,cfsqltype="cf_sql_varchar");
		var result = deleteScopeQuery.execute
					(sql="
						DELETE FROM OAuth2ScopePermission	
						where scope=:scope
					");	
					
	}

	/**
	 * Returns if the requested scope exists, either built in or created through the UI
	 **/
	public boolean function endpointExists(required string scopeName){
		var customEndpointReturn=retrieveCustomEndpoint(scopeName);
		
		if (this.searchCustomEndpoints AND customEndpointReturn.exists){
			return true;
		}else{
			
			var endpointSearchLocations =getEndpointSearchLocations_dotNotation();

			for(location in endpointSearchLocations){
				var proposedCFC=location&'.'&endpoint;
				if (builtInEndPointExists(proposedCFC)){
					return true;
				}
			}
			
			
			return false;
		}
	}


	/**
	 * If it exists retrieves either a custom or build in endpoint
	 **/
	public EndpointTemplate function getEndpoint(required string scopeName){
		var customEndpointReturn=retrieveCustomEndpoint(scopeName);
		
		if (this.searchCustomEndpoints AND customEndpointReturn.exists){
			return customEndpointReturn.customEndpoint;
		}else{
			return getBuildInEndpoint(scopeName);
		}
	}

	/**
	 * Returns a struct containing .exists and .customEndpoint, CustomEndpoint will only exist
	 * if exists is true
	 **/
	public struct function retrieveCustomEndpoint(required string scopeName){
		var retrieveScopeQuery=new query();
		retrieveScopeQuery.addParam(name="scope",value=scopeName, cfsqltype="cf_sql_varchar");

		var result = retrieveScopeQuery.execute
					(sql="
					select scope,personFields,locationFields,organisationFields
					from OAuth2CustomScope
					where scope=:scope
					");
		var returnedEndpoints=queryToCustomEndpointList(result.getResult());

		if (arrayLen(returnedEndpoints) EQ 1){
			return {exists=true,CustomEndpoint=returnedEndpoints[1]};
		}else{
			return {exists=false};
		}

	}

	/**
	 * Returns all available endpoints(scopes) whether they are custom, core or userdefined
	 **/
	public array function getAllEndpoints(namesOnly=false){
		var allEndpoints=arrayNew(1);
		IF (this.searchCustomEndpoints){arrayAppend(allEndpoints,getAllCustomEndpoints(),true);}
		arrayAppend(allEndpoints,getAllBuildInEndpoints(),true);
		if (namesOnly){
			endpointNames=arrayNew(1);
			for(scope in allEndpoints){
				arrayAppend(endpointNames,scope.getScopeName());
			}
			return endpointNames;
		}else{
			return allEndpoints;
		}
		
	}

	/**
	 * Returns all endpoints(scopes) created through the UI, scopes created in coldfusion
	 * (be they core or custom) are not returned here
	 **/
	public Array function getAllCustomEndpoints(){
		var retrieveScopeQuery=new query();
		var result = retrieveScopeQuery.execute
					(sql="
					select scope,personFields,locationFields,organisationFields
					from OAuth2CustomScope
					order by scope
					");
		return queryToCustomEndpointList(result.getResult());
	}

	/**
	 * Gets built in endpoints by their scope name
	 **/
	public EndpointTemplate function getBuildInEndpoint(required string endpoint){
			var endpointSearchLocations =getEndpointSearchLocations_dotNotation();

			for(location in endpointSearchLocations){
				var proposedCFC=location&'.'&endpoint;
				if (builtInEndPointExists(proposedCFC)){
					var endpointCFC=createObject("component", proposedCFC).init();
					endpointCFC.setScopeName(endpoint);

					return endpointCFC;
				}
			}
			throw (message = "Endpoint #endpoint# not found in any of the search locations", type = "no such component");
	}

	
	public boolean function builtInEndPointExists(required string endpointPackageName){

		try{
				test=createObject("component", endpointPackageName);
				return true;
			}catch (coldfusion.runtime.CfJspPage$NoSuchTemplateException exception){
				return false;
			}


	}


	/**
	 * Returns all endpoints(scopes) created in coldfusion
	 * (be they core or custom). Only the names are returned so its an array of strings
	 **/
	public Array function getAllBuildInEndpoints(){
		var endpoints=ArrayNew(1);
		var searchLocations=getEndpointSearchLocations_slashNotation();
		for(var i=1; i<=arrayLen(searchLocations); i=i+1){
			var searchLocation=searchLocations[i];
			varCFCsInLocation=application.com.applicationVariables.getCFCs(relativepath=searchLocation);

			for(key in varCFCsInLocation){
				var endpointName=varCFCsInLocation[key].name;
				ArrayAppend(endpoints,getBuildInEndpoint(endpointName));
			}
		}
		return endpoints;
	}


	/**
	 * Takes a query to retrieve the scopename, personFields, locationFields and organisationFields
	 * and returns an array of CustomEndpoints
	 **/
	private Array function queryToCustomEndpointList(any endpointQuery){
	 	var endPoints=ArrayNew(1);

	 	// Loop over the records in the query.
    	for (var i= 1 ; i <= endpointQuery.RecordCount ;i=i+1){
    		endPoints[i]=new CustomEndpoint(endpointQuery["scope"][i],endpointQuery["personFields"][i],endpointQuery["locationFields"][i], endpointQuery["organisationFields"][i]);
    	}

        return endPoints;
	}

	public void function persistScope(required CustomEndpoint scopeToPersist){
		var persistScopeQuery=new query();

		persistScopeQuery.addParam(name="scope",value=scopeToPersist.getScopeName(),cfsqltype="cf_sql_varchar");
		persistScopeQuery.addParam(name="personFields",value=arrayToList(scopeToPersist.getPersonFields()),cfsqltype="cf_sql_varchar");
		persistScopeQuery.addParam(name="locationFields",value=arrayToList(scopeToPersist.getLocationFields()),cfsqltype="cf_sql_varchar");
		persistScopeQuery.addParam(name="organisationFields",value=arrayToList(scopeToPersist.getOrganisationFields()),cfsqltype="cf_sql_varchar");

		var result = persistScopeQuery.execute
					(sql="
					if exists(select 1 from OAuth2CustomScope where scope = :scope)
						BEGIN
							update OAuth2CustomScope
							set personFields=:personFields,
							locationFields=:locationFields,
							organisationFields=:organisationFields
							where scope=:scope

						END
					ELSE
						BEGIN
							insert into OAuth2CustomScope(scope,personFields,locationFields,organisationFields)
							VALUES(:scope,:personFields,:locationFields,:organisationFields);
						END

					");
	}

	/**
	 * Returns a list of locations to search when looking for endpoints.
	 *
	 * If it the Relay location should omit the relay, if in the code location should start code.
	 **/
	public Array function getEndpointSearchLocations_dotNotation(){
		return this.scopeSearchLocations_dotNotation;
	}


	/**
	 * Returns a list of locations to search when looking for endpoints. Will start relay/ or code/
	 **/
	private Array function getEndpointSearchLocations_slashNotation(){
		var slashArray=ArrayNew(1);
		var dotArray=getEndpointSearchLocations_dotNotation();

		for(var i=1; i<=arrayLen(dotArray); i=i+1){
			var slashLocation=replace(dotArray[i],".", "\","all");
			if (not slashLocation.tolowercase().startsWith("code")){
				slashLocation="relay\"&slashLocation;
			}
			slashArray[i]=slashLocation;
		}

		return slashArray;
	}

}