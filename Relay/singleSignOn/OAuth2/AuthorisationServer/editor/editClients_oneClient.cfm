<!--- �Relayware. All Rights Reserved --->
<!--- 	Code to put a drop down box on the selection list screen

		RT 23/10/15

Amendment History
2015/12/07 	SB	Bootstrap
			 --->

<cf_includejavascriptonce template = "/javascript/rwFormValidation.js">
<cf_includeJavascriptOnce template = "/singleSignOn/OAuth2/js/editClientApp.js" translate="true">

<cfset application.com.request.setTopHead(pageTitle = "OAuth2 Authorisation Server Client Settings",topHeadCfm="/relay/singleSignOn/OAuth2/AuthorisationServer/editor/OAuthServerTopNav.cfm", showSave="true", showDelete="true")>

<cfparam name="url.CLIENTTOEDIT" default="">
<cfparam name="form.CLIENTTOEDIT" default="">
<cfparam name="form.createLink" default="false">
<cfparam name="url.showSecretLink" default="">
<cfparam name="form.newClient" default="#len(form.CLIENTTOEDIT) EQ 0 AND len(url.CLIENTTOEDIT) EQ 0 AND len(url.showSecretLink) EQ 0#">
<cfscript>
	requireHTTPS=application.com.settings.getSetting("OAuth.AuthenticationServer.requireHTTPS");
	editorBackingBean=new singleSignOn.common.editorBackingBean();
	scopeManager=new singleSignOn.OAuth2.AuthorisationServer.ScopeManager();
	allScopes=scopeManager.getAllEndpoints(namesOnly=true);

	if(structKeyExists(form,"createNew")){
		clientID=backingBean.createNewClient(clientName,RedirectURI);
	}

	if (form.CLIENTTOEDIT NEQ ""){
		oAuthClient=editorBackingBean.retrieveClient(form.CLIENTTOEDIT);
	}else if (url.CLIENTTOEDIT NEQ "") {
		oAuthClient=editorBackingBean.retrieveClient(url.CLIENTTOEDIT);
	}else if (url.showSecretLink NEQ "") {
		oAuthClient=editorBackingBean.retrieveClient(url.showSecretLink);
		form.createLink=true;
	}else{
		 //this is a completely new object, if we just arrived at this page it just gives the code below
		 //something to pull nothing out of, if we're saving we'll fill it up and save
		oAuthClient=new singleSignOn.OAuth2.AuthorisationServer.OAuth2Client();

		if (structKeyExists(form,"input_client_name")){
			oAuthClient.autogenerateClient();
		}

	}

	//we're recieving a save event, save it all
	if (structKeyExists(form,"input_client_name")){
		param name="form.selectedScopesFields" default="";
		
		oAuthClient.setClientName(form.input_client_name);
		oAuthClient.setRedirectURL(form.input_Redirect_URI);

		oAuthClient.setPermittedScopes(listToArray(form.selectedScopesFields));

		oAuthClient.persistToDatabase();

        application.com.relayui.setMessage(message="phr_OAuth2_auth_server_saved", closeAfter="5");
	}
</cfscript>

<form id="deletionForm" method="post">
    <cf_input type="hidden" name="clientID" value=#oAuthClient.getClientIdentifier()#>
    <cf_input type="hidden" name="deleteClient" value="true">
</form>

<cfif oAuthClient.isValidOAuthClient()>
	<cfoutput>
		<p>#htmlEditFormat("phr_OAuth2_clientSecretText")#</p>
	</cfoutput>
	<cfif structKeyExists(form,"createLink") AND form.createLink>
		<cfoutput>
			<p>#htmlEditFormat("phr_OAuth2_clientSecretDescription:")#</p>
			#HTMLEditFormat(editorBackingBean.createSetSecretLink(oAuthClient.getClientIdentifier()))#
		</cfoutput>
		</p>
	<cfelse>
		<form action="" id="secretClient" method="post" >
			<cf_input type="hidden" name="CLIENTTOEDIT" value="#oAuthClient.getClientIdentifier()#">
			<cf_input type="hidden" name="createLink" value="true">
				<button type="submit" class="btn btn-primary"><cfoutput>#htmlEditFormat("phr_OAuth2_createClientSecretLink")#</cfoutput></button>
		</form>
	</cfif>	
	<br>	
</cfif>

<cfif form.newClient>
        <cfset application.com.request.setDocumentH1(text="phr_OAuth2_create_client")>
        <cfoutput>
            <p>#htmlEditFormat("phr_OAuth2_editNewClient")#</p>
        </cfoutput>
<cfelse>
        <cfset application.com.request.setDocumentH1(text="phr_OAuth2_edit_client")>
        <cfoutput>
            <p>#htmlEditFormat("phr_OAuth2_editExistingClient")#</p>
        </cfoutput>
</cfif>

<div id="editClientsOneClient">
	<cfoutput>
		<form role="form" method="post" name="editClient" id="editClient">
	    	<CF_input type="hidden" id="input_client_id" name="CLIENTTOEDIT" value="#oAuthClient.getClientIdentifier()#">
		  	<CF_relayFormElementDisplay relayFormElementType="text" readOnly="true" maxlength="256" fieldName="input_client_id_hidden" currentValue="#oAuthClient.getClientIdentifier()#" label="phr_OAuth2_client_id">
		  	<CF_relayFormElementDisplay relayFormElementType="text" required="true" maxlength="256" fieldName="input_client_name" currentValue="#oAuthClient.getClientName()#" label="phr_OAuth2_clientName">
		  	<CF_relayFormElementDisplay relayFormElementType="text" required="true" maxlength="512" fieldName="input_Redirect_URI" currentValue="#oAuthClient.getRedirectURL()#" label="phr_OAuth2_redirectionURL">
		  	<CF_relayFormElementDisplay relayFormElementType="select" multiple="true" currentValue="#arrayToList(oAuthClient.getPermittedScopes())#" list="#arrayToList(allScopes)#" fieldName="selectedScopesFields" label="phr_OAuth2_scopesUsedByClient" required="true" noteText="phr_OAuth2_clientScopesHint">
		</form>

	</cfoutput>
</div>