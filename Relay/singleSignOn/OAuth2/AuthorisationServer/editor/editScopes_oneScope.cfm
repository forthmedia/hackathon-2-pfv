<cfparam name="form.SCOPETOEDIT" default="">
<cfparam name="form.newScope" default="#len(form.SCOPETOEDIT) EQ 0 #">

<cf_includejavascriptonce template = "/javascript/rwFormValidation.js">
<cf_includeJavascriptOnce template = "/singleSignOn/OAuth2/js/editClientApp.js" translate="true">

<cfset application.com.request.setTopHead(queryString = "",topHeadCfm="/relay/singleSignOn/OAuth2/AuthorisationServer/editor/OAuthServerTopNav.cfm", hideScopes="false", saveScopes="true")>
<cfset application.com.request.setDocumentH1(text="phr_OAuth2_oneScopeEditTitle")>

<cfscript>
	editorBackingBean=new singleSignOn.common.editorBackingBean();
	scopeManager=new singleSignOn.OAuth2.AuthorisationServer.ScopeManager();


	if (NOT form.newScope){
		form.SCOPETOEDIT=rereplace(form.SCOPETOEDIT," ","_","all"); //spaces are not permitted in scopes, correct to underscopes
		scopeResponse=scopeManager.retrieveCustomEndpoint(form.SCOPETOEDIT);
		if (scopeResponse.exists){
			scope=scopeResponse.CustomEndpoint;
		}else{
			//person fields etc are part of the standard save
			scope=new singleSignOn.OAuth2.AuthorisationServer.CustomEndpoint(form.SCOPETOEDIT, "", "", "");
		}

	}else{
		//person fields etc are part of the standard save
		scope=new singleSignOn.OAuth2.AuthorisationServer.CustomEndpoint(form.SCOPETOEDIT, "", "", "");
	}




	function convertArrayToOrderedStruct(arrayToConvert) {
	    orderedStruct = createObject("java", "java.util.LinkedHashMap").init();

	    for(i=1;i<=arrayLen(arrayToConvert);i=i+1){

	    	orderedStruct[arrayToConvert[i]]=arrayToConvert[i];
	    }
	    return orderedStruct;
	}

	function deleteArrayFromArray(startArray,arrayToDelete) {
	    newArray=ArrayNew(1);

	    for(i=1;i<=arrayLen(startArray);i=i+1){
	    	if (NOT ArrayContains(arrayToDelete,startArray[i])){
	    		arrayAppend(newArray,startArray[i]);
	    	}
	    }
	    return newArray;
	}

	personFields=structkeyarray(application.com.rights.getEntityFieldRights(entityTypeID=application.entityTypeID.person,returnColumns="field",haveRightsForMethod="view"));
	ArrayDelete(personFields,"Password"); //its the salted hashed password which shouldn't have ayn value, but wqe're still not going to give it out (this is validated at the endpoint itself as well)
	ArrayDelete(personFields,"PasswordDate");
	personFields=editorBackingBean.eliminateProfilesWithoutTextIDs(personFields);
	arraySort(personFields,"text");


	locationFields=structkeyarray(application.com.rights.getEntityFieldRights(entityTypeID=application.entityTypeID.location,returnColumns="field",haveRightsForMethod="view"));
	locationFields=editorBackingBean.eliminateProfilesWithoutTextIDs(locationFields);
	arraySort(locationFields,"text");


	organisationFields=structkeyarray(application.com.rights.getEntityFieldRights(entityTypeID=application.entityTypeID.organisation,returnColumns="field",haveRightsForMethod="view"));
	organisationFields=editorBackingBean.eliminateProfilesWithoutTextIDs(organisationFields);
	arraySort(organisationFields,"text");



	//the save behaviour on the form submit
	if (structKeyExists(form,"selectedPersonFields") || structKeyExists(form,"selectedLocationFields") || structKeyExists(form,"selectedOrganisationFields") ){
		
		//as a multiselect with no selection doesn't exist
		param name="form.selectedPersonFields" default="";
		param name="form.selectedLocationFields" default="";
		param name="form.selectedOrganisationFields" default="";
		
		//only fields currently selected or fields the user has access to are allowed to be added
		approvedPersonFields=arrayNew(1);
		approvedPersonFields.addAll(personFields);
		approvedPersonFields.addAll(scope.getPersonFields());

		scope.setPersonFields(editorBackingBean.validateScopeProfiles(listToArray(form.selectedPersonFields),approvedPersonFields).allowedFields);

		//only fields currently selected or fields the user has access to are allowed to be added
		approvedLocationFields=arrayNew(1);
		approvedLocationFields.addAll(locationFields);
		approvedLocationFields.addAll(scope.getLocationFields());

		scope.setLocationFields(editorBackingBean.validateScopeProfiles(listToArray(form.selectedLocationFields),approvedLocationFields).allowedFields);

		//only fields currently selected or fields the user has access to are allowed to be added
		approvedOrganisationFields=arrayNew(1);
		approvedOrganisationFields.addAll(organisationFields);
		approvedOrganisationFields.addAll(scope.getOrganisationFields());

		scope.setOrganisationFields(editorBackingBean.validateScopeProfiles(listToArray(form.selectedOrganisationFields),approvedOrganisationFields).allowedFields);

		if (len(form.SCOPETOEDIT)>0){ //only save if the scopename was filled in
			scopeManager.persistScope(scope); //put the scope into the database
		}
	}

	//validity checks
	personFieldValidity=editorBackingBean.validateScopeProfiles(scope.getPersonFields(),personFields);
	locationFieldValidity=editorBackingBean.validateScopeProfiles(scope.getLocationFields(),locationFields);
	organisationFieldValidity=editorBackingBean.validateScopeProfiles(scope.getOrganisationFields(),organisationFields);



</cfscript>



<h2>Edit Scope</h2>
<p>This page can be used to adjust the Person, Location and Organisation level fields returned by this scope</p>
<form name = "edit" action="" id="edit" method="post">
	<div class="entry">
		 <strong>Scope name:  </strong>
		<cfif form.newScope>
			<cf_input type="text" name="scopeToEdit" maxlength="255" required="required" value="#form.SCOPETOEDIT#">
		<cfelse>
			<cf_input type="hidden" name="scopeToEdit" value="#form.SCOPETOEDIT#" >
			<cf_input type="text" disabled="disabled" name="scopeToEditDisplay" value="#form.SCOPETOEDIT#" >
		</cfif>

	</div>
	<!--- Person Fields--->

	<div class="form-group">
		<label for="selectedPersonFields">phr_oauth_personFieldsInScope</label>
		<CF_TwoSelectsComboStruct
		    NAMENotSelected="UnselectedPersonFields"
		    NAMESelected="selectedPersonFields"
		    SIZE="12"
		    WIDTH="400px"
			FORCEWIDTH="400"
		    structSelected="#convertArrayToOrderedStruct(scope.getPersonFields())#"
			structNotSelected="#convertArrayToOrderedStruct(deleteArrayFromArray(personFields,scope.getPersonFields()))#"
		    CaptionNotSelected="<FONT SIZE=-1><B>Potential Person Fields/Profiles:</B></FONT>"
		    CaptionSelected="<FONT SIZE=-1><B>Current Person Fields/Profiles:</B></FONT>"
			UP="No"
			DOWN="No"
			FORMNAME="edit"
			>
	</div>

	<cfif NOT personFieldValidity.isOk>
		<p class="warning"> Warning, the field(s) <cfoutput>#HTMLEditFormat(ArrayToList(personFieldValidity.disallowedFields))#</cfoutput> are not valid fields or you do not have rights to them</p>
	</cfif>

	<div class="form-group">
		<!--- location Fields--->
		<label for="selectedLocationFields">phr_oauth_locationFieldsInScope</label>
		
		<CF_TwoSelectsComboStruct
			    NAMENotSelected="UnselectedLocationFields"
			    NAMESelected="selectedLocationFields"
			    SIZE="12"
			    WIDTH="400px"
				FORCEWIDTH="400"
			    structSelected="#convertArrayToOrderedStruct(scope.getLocationFields())#"
				structNotSelected="#convertArrayToOrderedStruct(deleteArrayFromArray(LocationFields,scope.getLocationFields()))#"
			    CaptionNotSelected="<FONT SIZE=-1><B>Potential Location Fields/Profiles:</B></FONT>"
			    CaptionSelected="<FONT SIZE=-1><B>Current Location Fields/Profiles:</B></FONT>"
				UP="No"
				DOWN="No"
				FORMNAME="edit"
				>
	</div>
	<cfif NOT locationFieldValidity.isOk>
		<p class="warning"> Warning, the field(s) <cfoutput>#HTMLEditFormat(ArrayToList(locationFieldValidity.disallowedFields))#</cfoutput> are not valid fields or you do not have rights to them</p>
	</cfif>

	<!--- organisation Fields--->
	<div class="form-group">
		<label for="selectedOrganisationFields">phr_oauth_organisationFieldsInScope</label>
		<CF_TwoSelectsComboStruct
			    NAME="UnselectedOrganisationFields"
			    NAME2="selectedOrganisationFields"
			    SIZE="12"
			    WIDTH="400px"
				FORCEWIDTH="400"
			    structSelected="#convertArrayToOrderedStruct(scope.getOrganisationFields())#"
				structNotSelected="#convertArrayToOrderedStruct(deleteArrayFromArray(OrganisationFields,scope.getOrganisationFields()))#"
			    Caption="<FONT SIZE=-1><B>Potential Organisation Fields/Profiles:</B></FONT>"
			    Caption2="<FONT SIZE=-1><B>Current Organisation Fields/Profiles:</B></FONT>"
				UP="No"
				DOWN="No"
				FORMNAME="edit"
				>
	</div>
	<cfif NOT organisationFieldValidity.isOk>
		<p class="warning"> Warning, the field(s) <cfoutput>#HTMLEditFormat(ArrayToList(organisationFieldValidity.disallowedFields))#</cfoutput> are not valid fields or you do not have rights to them</p>
	</cfif>
</form>