<cfset application.com.request.setTopHead(queryString = "",topHeadCfm="/relay/singleSignOn/OAuth2/AuthorisationServer/editor/OAuthServerTopNav.cfm", hideScopes="true")>

<cfscript>
	scopeManager=new singleSignOn.OAuth2.AuthorisationServer.ScopeManager();
	if (structKeyExists(form,"scopeToDelete")){
		scopeManager.deleteCustomScope(form.scopeToDelete);
	}
	allScopes=scopeManager.getAllEndpoints();

</cfscript>
<cfset application.com.request.setDocumentH1(text="phr_OAuth2_scopeEditTitle")>
<cfoutput>
    <p>#htmlEditFormat("phr_OAuth2_scopeEditDescription")#</p>
</cfoutput>

<form id="editScope" method="post">
	<cf_input type="hidden" name="scopeToEdit" value="">
</form>
<cfoutput>
	<h2>#htmlEditFormat("phr_OAuth2_manageScopes")#</h2>
</cfoutput>

<table id="existingClients" class="display responsiveTable table table-striped table-hover">
	<thead>
		<tr>
			<cfoutput>
				<th>#htmlEditFormat("phr_OAuth2_scopeName")#</th>
				<th>#htmlEditFormat("phr_description")#</th>
				<th>#htmlEditFormat("phr_edit")#</th>
				<th>#htmlEditFormat("phr_delete")#</th>
			</cfoutput>
		</tr>
	</thead>
    <tbody>
		<cfset evenRow=false>
		<cfloop array='#allScopes#' index='scopeObject'>
			<cfset evenRow=not evenRow>
			<tr class="<cfoutput>#evenRow?'evenRow':'oddRow'#</cfoutput>">
				<td><cfoutput>#HTMLEditFormat(scopeObject.getScopeName())#</cfoutput></td>
				<td><cfoutput>#application.com.security.sanitiseHTML(scopeObject.getDescription())#'</cfoutput></td> <!--- Description may contain simple HTML, deliberately not htmledit formatted--->
				<td align="center" valign="top">
					<cfoutput>
						<cfif IsInstanceOf(scopeObject, "singleSignOn.OAuth2.AuthorisationServer.CustomEndpoint")>
							<form action="" id="edit" method="post" >
								<cf_input type="hidden" name="scopeToEdit" value="#scopeObject.getScopeName()#"/>
								<input type="image" src="/images/MISC/iconProfile.gif" height="16"/>
							</form>
						<cfelse>
							<cfoutput>N/A</cfoutput>
						</cfif>

					</cfoutput>
				</td>
				<td align="center" valign="top">
					<cfoutput>
						<cfif IsInstanceOf(scopeObject, "singleSignOn.OAuth2.AuthorisationServer.CustomEndpoint")>
							<form action="" id="delete" method="post" >
								<cf_input type="hidden" name="scopeToDelete" value="#scopeObject.getScopeName()#"/>
								<input type="image" src="/images/MISC/iconDeleteCross.gif" onclick="return confirm('Are you sure you want to delete this scope?')"  height="16"/>
							</form>
						<cfelse>
							<cfoutput>N/A</cfoutput>
						</cfif>

					</cfoutput>
				</td>
			</tr>
		</cfloop>
    </tbody>
</table>