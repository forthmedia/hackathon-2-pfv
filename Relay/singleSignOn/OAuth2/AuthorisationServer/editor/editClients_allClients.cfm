<!--- 2015/12/02 SB Bootstrap --->

<cfset application.com.request.setTopHead(pageTitle = "OAuth2 Authorisation Server Client Settings",topHeadCfm="/relay/singleSignOn/OAuth2/AuthorisationServer/editor/OAuthServerTopNav.cfm", showEdit="false", showAdd="true")>



<cfset backingBean=createObject("component","singleSignOn.common.editorBackingBean")>


<cfif structKeyExists(form,"deleteClient")>
	<cfset backingBean.deleteClient(clientID)>
	<cfset application.com.relayui.setMessage(message="phr_OAuth2_client_deleted", closeAfter="5")>
<cfelseif structKeyExists(url,"serverToDelete")>
	<cfset backingBean.deleteClient(url.serverToDelete)>
	<cfset application.com.relayui.setMessage(message="phr_OAuth2_client_deleted", closeAfter="5")>
</cfif>
<cfoutput>
    <cfset application.com.request.setDocumentH1(text="phr_OAuth2_manageClientsTitle")>
	<p>#htmlEditFormat("phr_OAuth2_editAllClientsScreenText")#</p>
</cfoutput>

<cfoutput>
	<h2>#htmlEditFormat("phr_OAuth2_manageClients")#</h2>
</cfoutput>

<CFQUERY NAME="clientsQuery" datasource="#application.siteDataSource#">
    SELECT
        clientIdentifier, clientName, redirectURL, 
        '<i class="fa fa-unlock-alt" style="color: green;"></i>' as secretLink,
        active,
        '<span class="deleteLink">Delete</span>' as deleteCol
    FROM
        OAuth2Client;
</CFQUERY>

<CF_tableFromQueryObject queryObject="#clientsQuery#" 
    showTheseColumns="clientName,redirectURL,secretLink,active,deleteCol"
    ColumnHeadingList="OAuth2_clientName,OAuth2_redirectionURL,OAuth2_secretLink,active,delete"
    ColumnTranslationPrefix="phr_"
    HidePageControls="no"
    useInclude = "false"
    numRowsPerPage="400"
    booleanFormat="active"
    keyColumnList="clientName,secretLink,deleteCol"
    keyColumnURLList="editClients.cfm?CLIENTTOEDIT=,editClients.cfm?showSecretLink=,editClients.cfm?serverToDelete=,"
    keyColumnKeyList="clientIdentifier,clientIdentifier,clientIdentifier"
    columnTranslation="true">