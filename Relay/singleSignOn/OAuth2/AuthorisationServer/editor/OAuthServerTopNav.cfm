<cfparam name="showSave" type="boolean" default="false">
<cfparam name="showDelete" type="boolean" default="false">
<cfparam name="showAdd" type="boolean" default="false">
<cfparam name="showEdit" type="boolean" default="true">
<cfparam name="hideScopes" type="boolean" default="false">
<cfparam name="saveScopes" type="boolean" default="false">
<cfparam name="headerAttributes.pageTitle" type="string" default="">


<CF_RelayNavMenu pageTitle="#headerAttributes.pageTitle#" thisDir="test/editScopes.cfm">

	<cfif showAdd>
        <CF_RelayNavMenuItem MenuItemText="phr_add_client" CFTemplate="singleSignOn/OAuth2/AuthorisationServer/editor/editClients_oneClient.cfm" target="mainSub" runInNewtab="true" tabName="tab_edit_server" tabTitle="phr_sys_nav_OAuth2_Client_Applications">
    </cfif>
    <cfif saveScopes>
        <cf_relayNavMenuItem MenuItemText="phr_Save" CFTemplate="##" onclick="saveScope(this);">
    </cfif>
    <cfif showSave>
        <cf_relayNavMenuItem MenuItemText="phr_Save" CFTemplate="##" onclick="FormSubmit(this);">
    </cfif>
    <cfif showDelete>
        <cf_relayNavMenuItem MenuItemText="phr_Delete" CFTemplate="##" onclick="deleteItem(this); return false;">
    </cfif>
    <cfif showEdit>
        <CF_RelayNavMenuItem MenuItemText=#showSave ? "phr_back" : "phr_edit_clients"# CFTemplate="editClients.cfm">
    </cfif>
    <cfif not hideScopes>
		<CF_RelayNavMenuItem MenuItemText="Edit endpoints (scopes)" CFTemplate="editScopes.cfm">
    </cfif>
    <cfif hideScopes>
        <CF_RelayNavMenuItem MenuItemText="Create scope" CFTemplate="editScopes_oneScope.cfm">
    </cfif>

</CF_RelayNavMenu>


