<cfset application.com.request.setTopHead(queryString = "",topHeadCfm="/relay/singleSignOn/OAuth2/AuthorisationServer/editor/OAuthServerTopNav.cfm")>

<cfif (structKeyExists(form,"CLIENTTOEDIT") or 
		structKeyExists(url,"showSecretLink") or 
		structKeyExists(url,"CLIENTTOEDIT") 
		and not 
		(structKeyExists(form,"deleteClient") or 
		structKeyExists(url,"serverToDelete")))>
	<cfinclude template="editClients_oneClient.cfm">
<cfelse>
	<cfinclude template="editClients_allClients.cfm">
</cfif>


