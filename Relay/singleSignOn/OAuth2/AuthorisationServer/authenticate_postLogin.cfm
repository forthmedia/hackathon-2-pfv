<!--- 
This is a page to be hit by third parties looking to be authenticated by Relayware, it contains no actual UI elements.
It either cfincludes UI elements to obtain user consent, or redirects to the 3rd party.

Note the user must be logged in by the time they hit this page. This can happen in any way but should happen automatically if 
authenticate.cfm is hit first off
--->


<cfif structKeyExists(form,"rejectSendDataToThirdParty")>
	<cflocation url="/" AddToken = false >
</cfif>

<cfset realClient=false><!---start false, may become true--->
<cfset authorisedToAccessData=false><!---start false, may become true--->

<cfscript>
	requireHTTPS=application.com.settings.getSetting("OAuth.AuthenticationServer.requireHTTPS");
	authorisedToAccessData=false; 
	if (structKeyExists(url,"client_ID")){
		//we find if the current user has authorised this client
		oauth2client=createObject("component","OAuth2Client").init(clientIdentifier=client_ID);
		oauth2client.pullFromDatabase();
		if (oauth2client.isValidOAuthClient()){
			realClient=true;
			if (oauth2client.isAuthorisedToAccessDataForUser(request.relayCurrentUser.personID)){
				authorisedToAccessData=true;
			}else if (structKeyExists(form,"acceptSendDataToThirdParty")){
				//we have recieved a command to authorise the client for this user, we need to make sure the consent token checks out
				//this prevents form scope manipulation being used to 
				consentToken=createObject("component","singleSignOn.common.ConsentToken").init(token=form.consentTokenString);
				consentToken.pullFromDatabase();
				if (consentToken.isValidToken() AND consentToken.getPersonID() eq request.relayCurrentUser.personID AND consentToken.getPersonID()){
					oauth2client.authoriseToAccessDataForUser(request.relayCurrentUser.personID);
					oauth2client.persistToDatabase();
					authorisedToAccessData=true;
				}else{
					//we'll end up back on the consent page, log the issue
					errorStruct=structNew();
					errorStruct.requestedPerson=request.relayCurrentUser.personID;
					errorStruct.authorisedPerson=consentToken.getPersonID();
					errorStruct.requestedClientID=oauth2client.getClientIdentifier();					
					errorStruct.authorisedClientID=consentToken.getClientIdentifier();
					errorStruct.explanation="acceptSendDataToThirdParty was present in the form scope but the consent token was not valid or for the right user or for the right client. This could be a case of the user going back to an old page or could be a (failed) hacking attempt.";
				
					application.com.errorHandler.recordRelayError_Warning(type="OAuthAuthenticationError",Severity="error",WarningStructure=errorStruct);
					
				
				}

				
			}
		}
		
		
	}

</cfscript>

<cfif realClient AND requireHTTPS AND NOT oauth2client.hasSecureRedirectURL()>
	<p>HTTPS Only (note the client's authorised redirect URL is used for this test not the URL parameter)</p>
<cfelseif authorisedToAccessData>

	<cfscript>
	
		
		if (
			structkeyExists(url,"response_type") AND 
			structkeyExists(url,"client_id") AND  
			structkeyExists(url,"redirect_uri") AND  		
			structkeyExists(url,"scope") AND 
			structkeyExists(url,"state") 
		){
			
			codeGenerator=createObject("component","codeGenerator");
			codereturn=codeGenerator.processAuthorisationRequest(url.response_type,url.client_id,url.redirect_uri,url.scope,url.state);
			if (Find("?",url.redirect_uri) NEQ 0){
				redirectWithCode=url.redirect_uri & "&code=" & codereturn.code & "&state=" & urlEncodedFormat(url.state);
			}else{
				redirectWithCode=url.redirect_uri & "?code=" & codereturn.code & "&state=" & urlEncodedFormat(url.state);
			}
			
		}else{
			codereturn.explanation="Not all required parameters were passed on the URL";
			codereturn.isOk=false;
			errorStruct=structNew();
			errorStruct.urlParameters=url;
			errorStruct.explanation=codereturn.explanation;
			codereturn.errorid=application.com.errorHandler.recordRelayError_Warning(type="OAuthAuthenticationError",Severity="error",WarningStructure=errorStruct);
		}
		
	</cfscript>
	
	<cfif codereturn.isOk>
		<cfif not structKeyExists(url,"test")>
			<cfheader statuscode="307" statustext="Moved Temporarily">
			<cfheader name="Location" value="#redirectWithCode#">
		</cfif>
		<!---fallback link if redirect fails or test mode is on --->
		<cfoutput><a href="#redirectWithCode#">#redirectWithCode#</a></cfoutput>
		
	<cfelse>
		<cfoutput>
			<p>Single Sign On Error.</p>
			<cfif application.com.settings.getSetting("OAuth.AuthenticationServer.fullErrors") AND structKeyExists(codereturn,"explanation")>
				<p>#codereturn.explanation# - Full explanation shown because fullErrors option has been selected in settings</p>
			</cfif>
		</cfoutput>
	
	</cfif>

<cfelse>
	<cfif realClient>
		<cfscript>
			consentToken=createObject("component","singleSignOn.common.ConsentToken").init(validity_seconds=application.com.settings.getSetting("OAuth.AuthenticationServer.codeValidityTime"));
			consentToken.autogenerateToken();
			consentToken.setPersonID(request.relayCurrentUser.personID);
			consentToken.setClientIdentifier(oauth2client.getClientIdentifier());		
			consentToken.persistToDatabase();
		</cfscript>

		<cfset agreeToDataTransferPage=application.com.settings.getSetting("OAuth.AuthenticationServer.authorisePage")>
		<cfset clientName=oauth2client.getClientName()>
		<cfif agreeToDataTransferPage eq "">
			<cfmodule template="/singleSignOn/common/authenticate_authorise.cfm" clientName="#clientName#" consentToken="#consentToken#">
		<cfelse>
			<cfmodule template="#agreeToDataTransferPage#" clientName="#clientName#" consentToken="#consentToken#">
		</cfif>
		
		
	<cfelse>
		<p>Single Sign On Error.</p> 
		<cfif application.com.settings.getSetting("OAuth.AuthenticationServer.fullErrors")>
			<p>No such Client or Client Deactivated - Full explanation shown because fullErrors option has been selected in settings</p>
		</cfif>
		<cfscript>
			errorStruct=structNew();
			errorStruct.message="No such client (or no ClientID on URL)";
			errorStruct.urlParameters=url;
			
			codereturn.errorid=application.com.errorHandler.recordRelayError_Warning(type="OAuthAuthenticationError",Severity="error",WarningStructure=errorStruct);
		</cfscript>		
	</cfif>
	

	
</cfif>

<cfif isDefined("codereturn.errorid")>
	<p>Error ID: <cfoutput>#codereturn.errorid#</cfoutput></p>
</cfif>	