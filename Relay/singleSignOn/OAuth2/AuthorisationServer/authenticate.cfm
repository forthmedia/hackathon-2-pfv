<!--- 
This is a page to be hit by third parties looking to be authenticated by Relayware, it contains no actual UI elements.
It either cfincludes UI elements to obtain user consent, or redirects to the 3rd party
--->

<cfif request.relayCurrentUser.isLoggedIn>
	<cfinclude template="authenticate_postLogin.cfm">
<cfelse>
	<!---Send user to the login page with instruction to return here once they are authenticated  --->

	
	<cfset urlRequested=urlEncodedFormat(CGI.Script_Name & "?" & CGI.query_String)>
	
	<cfset httpPrefix=FindNoCase("https",cgi.SERVER_PROTOCOL) NEQ 0?"https://":"http://">
	<cfset loginPageSetting=application.com.settings.getSetting("OAuth.AuthenticationServer.loginPage")>
	<cfset loginEID=loginPageSetting EQ ""?request.currentsite.elementTreeDef.LoginPage:loginPageSetting>
	
	<cflocation url="#httpPrefix##cgi.HTTP_HOST#/?eid=#loginEID#&urlRequested=#urlRequested#" AddToken = false >

</cfif>