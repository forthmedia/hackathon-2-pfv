<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			cfm
Author:				MDC
Date started:		2004-11-03
	
Description:			

creates functionList query for tableFromQueryObject custom tag

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
03-Nov-2004			MDC			Initial version

Possible enhancements:


 --->

<cfscript>
comTableFunction = CreateObject( "component", "relay.com.tableFunction" );

comTableFunction.functionName = "Phr_eLearningReportFunctions";
comTableFunction.securityLevel = "";
comTableFunction.url = "";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();

comTableFunction.functionName = "--------------------";
comTableFunction.securityLevel = "";
comTableFunction.url = "";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();

comTableFunction.functionName = "Phr_eLearning_FulfillRecords";
comTableFunction.securityLevel = "";
comTableFunction.url = "";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();

comTableFunction.functionName = "&nbsp;phr_eLearning_Fulfill_Checked_Records";
comTableFunction.securityLevel = "";
comTableFunction.url = "javascript:document.frmBogusForm.submit();";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();

</cfscript>