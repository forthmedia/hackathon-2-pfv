<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			moduleSets.cfm
Author:				NJH
Date started:		2008/08/20
Description:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2012-04-16	 		Englex 		P-REL109 - Add training program
2013-04-09			STCR 		CASE 432194 Translation of ModuleSet Title
2016-05-18			WAB			BF-714 TrainingProgram drop down. Remove reference to get ExtendedMetaData.sql and replace with func:...getTrainingProgram()
2016-05-18			WAB			BF-716 TrainingProgram drop down. Does not make sense to filter by session.selectedTrainingProgramID
Possible enhancements:

 --->

<!--- START: 2012-04-16 - Englex - P-REL109 - Add training program --->
<cfif application.com.settings.getSetting("elearning.useTrainingProgram")>
	<cfinclude template="trainingProgramCheck.cfm">
</cfif>
<!--- END: 2012-04-16 - Englex - P-REL109 - Add training program --->

<cf_title>Module Sets</cf_title>

<cfparam name="getData" type="string" default="foo">
<cfparam name="sortOrder" default="Description">
<cfparam name="variables.showTheseColumns" default="ModuleSetID,Description,Active" />
<cfparam name="numRowsPerPage" type="numeric" default="100">
<cfparam name="startRow" type="numeric" default="1">

<!--- save the content for the xml to define the editor --->
<cfsavecontent variable="xmlSource">
	<cfoutput>
	<editors>
		<editor id="232" name="thisEditor" entity="trngModuleSet" title="Module Editor">
			<field name="moduleSetID" label="phr_elearning_ModuleSetID" description="This records unique ID." control="html"></field>
			<!--- <field name="Description" label="phr_elearning_ModuleSetDescription" description="The module set description"></field> --->
			<!--- STCR 2013-04-09 CASE 432194 Translation of ModuleSet Title --->
			<field name="" CONTROL="TRANSLATION" required="true" label="phr_elearning_ModuleSetDescription" description="The module set description"  Parameters="phraseTextID=Description"></field>
			<!--- START: 2012-04-16 - Englex - P-REL109 - Add training program --->
			<cfif application.com.settings.getSetting("elearning.useTrainingProgram")>
				<field name="TrainingProgramID" label="phr_elearning_trainingProgram" description="The training program the specialisation is valid for." control="select" query="func:com.relayElearning.getTrainingProgram(forDropDown=1)" ></field>
			</cfif>
			<!--- END: 2012-04-16 - Englex - P-REL109 - Add training program --->
			<field name="Active" label="phr_elearning_ModuleSetActive" description="Is the module set active?" control="YorN"></field>
			<field name="" control="includeTemplate" template="/eLearning/moduleSetModules.cfm" spanCols="true" valueAlign="middle"/>
			<group label="System Fields" name="systemFields">
				<field name="sysCreated"></field>
				<field name="sysLastUpdated"></field>
				<field name="LastUpdatedbyPerson" label="phr_editor_lastUpdated" description="" control="hidden"></field>
			</group>
		</editor>
	</editors>
	</cfoutput>
</cfsavecontent>


<cfif not structKeyExists(URL,"editor")>
	<cfset getData = application.com.relayElearning.GetModuleSetsData(sortOrder=#sortOrder#)>
</cfif>

<!--- START: 2012-05-01 - Englex - P-REL109 - Add training program --->
<cfif application.com.settings.getSetting("elearning.useTrainingProgram")>
	<cfset variables.showTheseColumns = "ModuleSetID,Description,Training_Program,Active"/>
</cfif>
<!--- END: 2012-05-01 - Englex - P-REL109 - Add training program --->
<cf_listAndEditor
	xmlSource="#xmlSource#"
	cfmlCallerName="moduleSets"
	keyColumnList="Description"
	keyColumnKeyList="moduleSetID"
	showSaveAndReturn = "#iif((structKeyExists(URL,'hideBackButton') and url.hideBackButton is true),false,true)#"
	booleanFormat="Active"
	FilterSelectFieldList="moduleSetID,Active"
	FilterSelectFieldList2="moduleSetID,Active"
	numRowsPerPage="#numRowsPerPage#"
	hideBackButton="#iif(structKeyExists(URL,'hideBackButton') and url.hideBackButton is true,true,false)#"
	showTheseColumns="#variables.showTheseColumns#"
	startRow="#startRow#"
	useInclude="false"
	sortOrder="#sortOrder#"
	topHead=""
	queryData="#getData#"
>