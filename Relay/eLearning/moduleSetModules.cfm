<!--- �Relayware. All Rights Reserved 2014 --->
<!---
files:  eLearning\moduleSetModules.cfm
NYB	2008-11-07	P-SOP009 - added "'phr_'+AICCTitlePhraseTextID as TitlePhr" to getModulesNotInModuleSet and getModulesInModuleSet queries, and CF_TwoSelectsComboList
SSS 2009-02-06 Elearning error when AICCTitlePhraseTextID is null make it blank other wise CF_TwoSelectsComboList will fall over.
NJH 2009/02/24 Bug Fix All Sites Issue 1880 - display the module titles for both combo lists and use the new translation method.
SSS 2009/09/28 LHID 2594 - checking for a variable that never existed if you wish to clear a module set.
MS  15-FEB-2011		LID:5470 	Updated Query so that the modulte title gets translated and then broken down if length greater then 50
IH	2012-07-02 Case 429102 switch to TwoSelectsComboQuery to prevent empty list element issues when title is blank
YMA 2012/11/12 CASE:431767 Deal with Null ModuleCode
2012/11/19	NJH		CASE 432093 - only delete from table where modules don't exist.. don't do a delete and then an insert of all new records.
--->

<cfif not Isdefined("AddNew")>

	<!--- Start SSS 2009/09/28 LHID 2594 changed where the form var SelectedModuleIDs is checked as if you want to empty a module set this var is not there so the delete
	does not take place --->
	<cfif structKeyExists(form,"Update") or structKeyExists(form,"Added")>
		<cftransaction>
			<cftry>
				<cfquery name="deleteExistingModulesFromModuleSetModule" datasource="#application.siteDataSource#">
					update trngModuleSetModule
						set lastUpdated=getDate(),
							lastUpdatedBy = <cfqueryparam cfsqltype="cf_sql_integer" value="#request.relayCurrentUser.userGroupID#">
					where moduleSetID = <cfqueryparam cfsqltype="cf_sql_integer" value="#form.moduleSetID#">
						<cfif structKeyExists(form,"SelectedModuleIDs") and form.SelectedModuleIDs neq "">
							and moduleID not in (<cfqueryparam cfsqltype="cf_sql_integer" value="#form.selectedModuleIDs#" list="true">)
						</cfif>
					
					delete from trngModuleSetModule where moduleSetID = <cfqueryparam cfsqltype="cf_sql_integer" value="#form.moduleSetID#">
					<cfif structKeyExists(form,"SelectedModuleIDs") and form.SelectedModuleIDs neq "">
						and moduleID not in (<cfqueryparam cfsqltype="cf_sql_integer" value="#form.selectedModuleIDs#" list="true">)
					</cfif>
				</cfquery>
				
				<cfif structKeyExists(form,"SelectedModuleIDs") and form.SelectedModuleIDs neq "">
					<cfquery name="insertNewModulesIntoModuleSetModule" datasource="#application.siteDataSource#">
						insert into trngModuleSetModule (moduleSetID,moduleID,createdBy,created,lastUpdatedBy,lastUpdated)
						<cfloop list="#form.SelectedModuleIDs#" index="moduleID">
							select 
								<cfqueryparam cfsqltype="cf_sql_integer" value="#form.moduleSetID#">,
								<cf_queryparam cfsqltype="cf_sql_integer" value="#moduleID#">,
								<cfqueryparam cfsqltype="cf_sql_integer" value="#request.relayCurrentUser.userGroupID#">,
								GetDate(),
								<cfqueryparam cfsqltype="cf_sql_integer" value="#request.relayCurrentUser.userGroupID#">,
								GetDate()
							where not exists (select 1 from trngModuleSetModule where moduleId=<cf_queryparam cfsqltype="cf_sql_integer" value="#moduleID#"> and moduleSetID=<cf_queryparam cfsqltype="cf_sql_integer" value="#form.moduleSetID#">)
								<cfif moduleID neq listlast(form.SelectedModuleIDs)>
									union all
								</cfif>
						</cfloop>
					</cfquery>
				</cfif>
				
				<cftransaction action="commit"/>
				<cfcatch type="Database">
					<cfdump var="#cfcatch#">
					<cftransaction action="rollback"/>
					<cfoutput>There was an error updating the modules in the module set</cfoutput>
				</cfcatch>
			</cftry>
		</cftransaction>
	</cfif>
	<!--- END SSS 2009/09/28 LHID 2594 --->
	<!--- SSS 2009-02-06 added is null to the title phrase textID so that two select combo
	getes passed the right data
	NJH 2009/02/15 - changed the phrases to use the new translation method
	--->
	
	<!--- YMA 2012/11/12 CASE:431767 - added isnull to module code select.  This was forcing the rest of the default tranlsation to return null when modulecode was null. --->
	<cfsavecontent variable="select_sql">
		<cfoutput>
			trngModule.moduleID, 
			isnull(moduleCode,'')+': '+ CASE WHEN len(title_defaultTranslation) > 100
			THEN left(title_defaultTranslation,30) +  ' ... ' + right(title_defaultTranslation,30)
			ELSE title_defaultTranslation END AS Title,
			title_defaultTranslation
		</cfoutput>
	</cfsavecontent>
	
	<!--- 	START - MS - 15-Feb-2011 LID:5470
			Updated Query so that the modulte title gets translated and then broken 
			down if length greater then 50--->
	<cfquery name="getModulesNotInModuleSet" datasource="#application.siteDataSource#">
		SELECT #preserveSingleQuotes(select_sql)#
		FROM trngModule 
		WHERE moduleID NOT IN
			(select moduleID from trngModuleSetModule where moduleSetID = <cfqueryparam cfsqltype="cf_sql_integer" value="#form.moduleSetID#">)
		and availability <> 0
		<!--- START: 2012-04-16 - Englex - P-REL109 - Add training program --->
		<cfif application.com.settings.getSetting("elearning.useTrainingProgram")>
			<cfset msTrainingProgramID = application.com.relayElearning.GetModuleSetsData(modulesetID=form.modulesetID).trainingprogramID>
			<cfif msTrainingProgramID neq 0 and msTrainingProgramID neq "">
				and trainingProgramID = <cf_queryparam value="#msTrainingProgramID#" CFSQLType="CF_SQL_INTEGER" />
			</cfif>
		</cfif>
		<!--- END: 2012-04-16 - Englex - P-REL109 - Add training program --->
		order by moduleCode,title_defaultTranslation
	</cfquery>
	
	<!--- NJH 2009/02/24 Bug Fix All Sites Issue 1880 --->
	<cfquery name="getModulesInModuleSet" datasource="#application.siteDataSource#">
		SELECT #preserveSingleQuotes(select_sql)#
		FROM trngModule
		WHERE moduleID IN
			(select moduleID from trngModuleSetModule where moduleSetID = <cfqueryparam cfsqltype="cf_sql_integer" value="#form.moduleSetID#">)
		and availability <> 0
		<!--- order by AICCTitlePhraseTextID --->
		order by moduleCode,title_defaultTranslation
	</cfquery>
	<!--- END - MS - 15-Feb-2011 --->
					
	<!--- 2012-07-02 IH Case 429102 switch to TwoSelectsComboQuery to prevent empty list element issues when title is blank --->
	<CF_TwoSelectsComboQuery
		    NAME="AllModules"
		    NAME2="SelectedModuleIDs"
		    SIZE="20"
		    WIDTH="380"
		    FORCEWIDTH="380"				    
			QUERY1="getModulesInModuleSet"
			QUERY2="getModulesNotInModuleSet"
			VALUE="moduleID"
			DISPLAY="Title"
			TITLE="title_defaultTranslation"
		    CAPTION="<FONT SIZE=-1><B>phr_elearning_PotentialModules:</B></FONT>"
		    CAPTION2="<FONT SIZE=-1><B>phr_elearning_CurrentModules:</B></FONT>"
			UP="No" 
			DOWN="No"
			FORMNAME="editorForm">
					
		<!--- <cfset form.XMLEDITORFIELDLIST = listAppend(form.XMLEDITORFIELDLIST,"SelectedCountryIDs")> --->
		
<cfelse>
	<cf_includejavascriptonce template = "/javascript/twoselectscombo.js">
</cfif>
