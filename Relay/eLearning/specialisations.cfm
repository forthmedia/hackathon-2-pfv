<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			Specialisations.cfm
Author:				NJH
Date started:		2008/09/08
Description:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2009-01-28			NYB			P-SOP006 - added Title, changed Description to use descriptionPhraseiD, changed Code to Title in list, added as Title to getData query
2010-10-27			NYB			Sophos issue
2011-03-02			NYB 		LHID5772: fixed back link
2012-04-16	 		Englex 		P-REL109 - Add training program
2014-10-02			RPW			CORE-705 Error while trying to "Set up modules" in Training area
2014-10-03			RPW			CORE-705 Error while trying to "Set up modules" in Training area - Moved session.selectedTrainingProgramID to trainingProgramCheck.cfm
2016-05-18			WAB			BF-714 TrainingProgram drop down. Remove reference to getExtendedMetaData.sql and replace with func:...getTrainingProgram()
2016-05-18			WAB			BF-716 TrainingProgram drop down. Does not make sense to filter by session.selectedTrainingProgramID

Possible enhancements:

 --->

<!--- START: 2012-04-16 - Englex - P-REL109 - Add training program --->
<cfif application.com.settings.getSetting("elearning.useTrainingProgram")>
	<cfinclude template="trainingProgramCheck.cfm">
</cfif>
<!--- END: 2012-04-16 - Englex - P-REL109 - Add training program --->

<cfparam name="getData" type="string" default="foo" />
<cfparam name="sortOrder" default="code" />
<cfparam name="url.active" default=1>
<cfparam name="variables.showTheseColumns" default="Code,Title,Description,Number_Of_Rules,Number_Of_Rules_To_Pass,Country" />

<cfscript>
 	// this changes the active status of a certification
 	if(structKeyExists(URL, "frmRowIdentity")){
		for(x = 1; x lte listLen(URL.frmRowIdentity); x = x + 1){
			application.com.relayElearning.inactivateSpecialisation(specialisationID=listGetAt(URL.frmRowIdentity,x));
		}
	}
 </cfscript>

<!--- save the content for the xml to define the editor --->

<cfsavecontent variable="xmlSource">
	<cfoutput>
	<editors>
		<editor id="232" name="thisEditor" entity="Specialisation" title="phr_elearning_specialisationEditor">
			<field name="SpecialisationID" label="phr_elearning_specialisationID" description="This records unique ID." control="html"></field>
			<cfif isdefined("add") and add eq "yes">
				<field name="Code" label="phr_elearning_specialisationCode" description="The specialisation code, should not contain spaces."></field>
			<cfelse>
				<field name="Code" label="phr_elearning_specialisationCode" description="The specialisation code, should not contain spaces." control="hidden"></field>
			</cfif>
			<field name="" CONTROL="TRANSLATION" label="phr_elearning_specialisationTitlePhrase" Parameters="phraseTextID=Title" required="true"></field>
			<field name="" CONTROL="TRANSLATION" label="phr_elearning_specialisationDescriptionPhrase" description=""  Parameters="phraseTextID=Description,displayAs=TextArea"></field>
			<field name="CountryID" label="phr_elearning_specialisationCountry" description="The country the specialisation is valid for." control="select" query="select countryID as value,countryDescription as display, 1 as orderIndex from country where isoCode is null union select  ''as countryID,'------------------------------------------' as display, 2 as orderIndex union select countryID as value,countryDescription as display, 3 as orderIndex from country where isoCode is not null order by orderIndex,countryDescription"></field>
			<!--- START: 2012-04-16 - Englex - P-REL109 - Add training program --->
			<cfif application.com.settings.getSetting("elearning.useTrainingProgram")>
			<field name="TrainingProgramID" label="phr_elearning_trainingProgram" description="The training program the specialisation is valid for." control="select" query="func:com.relayElearning.getTrainingProgram(forDropDown=1)"></field>
			</cfif>
			<group label="System Fields" name="systemFields">
				<field name="sysCreated"></field>
				<field name="sysLastUpdated"></field>
				<field name="LastUpdatedbyPerson" label="phr_editor_lastUpdated" description="" control="hidden"></field>
			</group>
			<cfif not structKeyExists(url,"add") or structKeyExists(form,"added")>
				<field name="" control="includeTemplate" template="/eLearning/specialisationsInclude.cfm" spanCols="true" valueAlign="middle"/>
			</cfif>
		</editor>
	</editors>
	</cfoutput>
</cfsavecontent>


<cfif not structKeyExists(URL,"editor")>
	<!--- LID 2688 NJH 2009/09/25 added country scope to function arguments. --->
	<cfset getData = application.com.relayElearning.GetSpecialisationData(sortOrder=sortOrder,countryScope=true,active=url.active)>
</cfif>

<cfset functionType="Specialisation">
<cfinclude template="/elearning/elearningFunctions.cfm">


<cfif application.com.settings.getSetting("elearning.useTrainingProgram")>
	<cfset variables.showTheseColumns = "Code,Title,Description,Number_Of_Rules,Number_Of_Rules_To_Pass,Country,Training_Program"/>
</cfif>

<cf_listAndEditor
	xmlSource="#xmlSource#"
	cfmlCallerName="specialisations"
	keyColumnList="Title"
	keyColumnKeyList="specialisationID"
	showSaveAndReturn = "true"
	showTheseColumns="#variables.showTheseColumns#"
	useInclude="false"
	sortOrder="#sortOrder#"
	queryData="#getData#"
	rowIdentityColumnName="specialisationID"
    functionListQuery="#comTableFunction.qFunctionList#"
	backform="/elearning/specialisations.cfm"
	FilterSelectFieldList="Code,Title,Country"
	booleanFormat="active"
	columnTranslation="true"
	columnTranslationPrefix="phr_report_specialisation_"
>