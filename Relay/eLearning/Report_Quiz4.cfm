<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			Report_Quiz4.cfm	
Author:				A Developer
Date started:		The Olden Days
	
Description:		Quizzes by Student

Amendment History:

Date (DD-MM-YYYY)	Initials 	What was changed
2008-09-16			AJC			Change the report to use TFQO and also added filters & export to excel
								Added Country to the query, pretty much changed the whole file
2009-02-27			NJH			Bug Fix All Sites Issue 1899 - made column headings translatable
2009-08-03			NYB  		SNY047					
2009-09-30			NYB			LHID2604
2009-10-23			NAS			LID - 2481/2499 Code entered to Order Translatable Columns
16-March-2011		MS			P-SNY107 Adding IF clause to see if a custom report exists and load that for sony
2012/03/04			IH			Case 426266 add query caching 
2012-10-04	WAB		Case 430963 Remove Excel Header 
					
Possible enhancements:


 --->
<cfsetting requesttimeout="120" enablecfoutputonly="no">
<cfif fileExists("#application.paths.code#/cftemplates/report_Quiz4.cfm")> <!--- WAB 19/2/2007 added so that site without opportunity.ini will load.  In reality this file probably shouldn't be loaded in application.com --->
	<cfinclude template="/code/cftemplates/report_Quiz4.cfm">
<cfelse>

<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">
<cfparam name="openAsExcel" type="boolean" default="false">

<cf_title>Report: Quizzes By Student</cf_title>
		
<cfif not openAsExcel>
		<cfparam name="current" type="string" default="index.cfm">
		<cfparam name="attributes.templateType" type="string" default="edit">
		<cfparam name="attributes.pageTitle" type="string" default="">
		<cfparam name="attributes.pagesBack" type="string" default="1">
		<cfparam name="attributes.thisDir" type="string" default="">

		<CF_RelayNavMenu pageTitle="#attributes.pageTitle#" thisDir="#attributes.thisDir#">
			<CF_RelayNavMenuItem MenuItemText="Excel" CFTemplate="#cgi["SCRIPT_NAME"]#?#queryString#&openAsExcel=true">
		</CF_RelayNavMenu>
</cfif>

<!--- NYB 2009-08-03 SNY047 & LHID2604- replaced default value of "" with "QuizName" - to get sorting to work --->
<cfparam name="sortOrder" default="QuizName">
<cfparam name="numRowsPerPage" default="50">
<cfparam name="startRow" default="1">
<cfparam name="dateFormat" type="string" default="">

<cfparam name="keyColumnList" type="string" default=""><!--- this can contain a list of columns that can be edited --->
<cfparam name="keyColumnURLList" type="string" default=""><!--- this can contain a list of matching columns that contain the URL of the editor template --->
<cfparam name="keyColumnKeyList" type="string" default=""><!--- this can contain a list of matching key columns e.g. primary key --->

<cfparam name="checkBoxFilterName" type="string" default="">
<cfparam name="checkBoxFilterLabel" type="string" default="">

<cfparam name="FilterSelect1" type="string" default="">

<cfset titlePhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "quizdetail", phraseTextID = "title")>
<cfset titlePhraseQuerySnippets.join = replaceNoCase(titlePhraseQuerySnippets.join,"quizDetailID","qd.quizDetailID")>

<!--- 2012/03/04	IH	Case 426266 add query caching --->
<CFQUERY name="getReportData" DATASOURCE="#application.SiteDataSource#" cachedwithin="#createTimeSpan(0,0,15,0)#">
	select * from 
	(SELECT distinct o.OrganisationName as organisation, p.FirstName+' '+ p.LastName as fullname, 
		case when qt.quizDetailID = 0 then f.name else qd.title_defaultTranslation end as QuizName,
		cast(qt.Score as decimal(12,2)) as score, qt.Passmark, c.countrydescription as country, qd.active as Quiz_Active
	FROM quizTaken qt with(nolock) INNER JOIN
		Person p with(nolock) ON qt.PersonID = p.PersonID INNER JOIN
		organisation o with(nolock) ON p.OrganisationID = o.OrganisationID INNER JOIN
		country c with(nolock) ON o.countryID = c.countryID
		left join quizDetail qd on qd.quizDetailID = qt.quizDetailID
		left join
				(trngUserModuleProgress tump with (noLock)
					inner join trngCoursewareData d with (noLock) on d.personID = tump.personID and d.moduleID = tump.moduleID
					inner join trngModule m with (noLock) on tump.moduleID = m.moduleID
					inner join files f with (noLock) on f.fileID = m.fileID)
		on qt.userModuleProgressID = tump.userModuleProgressID
	) as basequery
	where 1 = 1
	<cfinclude template = "/templates/tableFromQuery-QueryInclude.cfm">			
	order by <cf_queryObjectName value="#sortOrder#">
</CFQUERY>
	
<CF_tableFromQueryObject 
	queryObject="#getReportData#"
	queryName="getReportData"
	sortOrder = "#sortOrder#"
	openAsExcel = "#openAsExcel#"
	numRowsPerPage="#numRowsPerPage#"
	startRow="#startRow#"
	
	keyColumnList="#keyColumnList#"
	keyColumnURLList="#keyColumnURLList#"
	keyColumnKeyList="#keyColumnKeyList#"
	
 	hideTheseColumns=""
	ShowTheseColumns="QuizName,Fullname,Organisation,Country,Passmark,Score,Quiz_Active"
	FilterSelectFieldList="Country,Organisation,Fullname,Quizname,Quiz_Active"
	FilterSelectFieldList2=""
	GroupByColumns=""
	radioFilterLabel=""
	radioFilterDefault=""
	radioFilterName=""
	radioFilterValues=""
	checkBoxFilterName=""
	checkBoxFilterLabel=""
	allowColumnSorting="yes"
	columnTranslation="true"
	ColumnTranslationPrefix="phr_elearning_"
	booleanFormat="Quiz_Active"
>

</cfif><!--- end of if to check if a custom userfile exists --->
