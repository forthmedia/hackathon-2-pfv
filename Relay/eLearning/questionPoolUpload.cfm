<!--- �Relayware. All Rights Reserved 2014 --->
<!---
Author:				STCR
Date started:		2012-05-14

Description:		Load Quiz Questions and Answers
					If the file contains any errors (number of columns is invalid, for example), the file is rejected.
					Otherwise, the contents are loaded into a temporary table where it is matched and then loaded.

Amendment History:

Date (YYYY-MM-DD)	Initials 	What was changed
2012-05-16			STCR		Replace html form fields with CF_relayFormElement and CF_relayFormElementDisplay
2012-05-17			STCR		Error/Success Reports. Do not match on QuestionRef outside this QuestionPool.
2012-08-24			STCR		Matching_Answer_Ref column and Matching QuestionType support
2012-11-21			IH			Case 429346 added sourceCollectionMethod and sourceDirectory to fileToDB call
2013-04-15			STCR		CASE 432328 implement QuestionSortOrder
2013-04-30			STCR		CASE 434975 catch for bad CSV file where column heading is too wide - eg because all the column headings have been pasted into the first cell
2013-11-25			IH			Case 438085 "All" and "Any" country should be the same
2014-01-23 			NYB 		Case 438455 added additional text and reference to an Excel Template file
2016-05-19          DAN         Case 449464 - specify ISO-8859-1 character encoding for bulk questions upload as we are not using UTF-8

Possible enhancements:
Add more clauses to friendly error message catch when new error scenarios are identified.

 --->

<cfparam name="url.QuestionPoolID" type="numeric" default="0" />
<cfparam name="form.QuestionPoolID" type="numeric" default="#url.QuestionPoolID#" />
<cfparam name="variables.QuestionPoolID" type="numeric" default="#form.QuestionPoolID#" />
<cfif isDefined("form.showBulkReports")>
	<cfset variables.showBulkReports = true />
<cfelse>
	<cfset variables.showBulkReports = false />
</cfif>

<cfset attributes.pageTitle="phr_eLearning_Bulk_UploadQuestions" />

<cfset variables.checkQuestionPool = application.com.relayQuiz.getQuizQuestionPoolList(QuestionPoolID=variables.QuestionPoolID) />

<cfsetting requesttimeout="600">
<cfif request.relayCurrentUser.isInternal and application.com.login.checkInternalPermissions(securityTask='eLearningTask',securityLevel='level2')>

	<cfif isDefined("form.filCSVFile")>
		<cfif variables.checkQuestionPool.recordCount eq 1>
			<cfset variables.dirPath = "#application.userFilesAbsolutePath#\dataTransfers\import\QuizQuestion" />

			<cfif not directoryExists(variables.dirPath)>
				<cfset application.com.globalFunctions.CreateDirectoryRecursive(fullPath=variables.dirPath) />
			</cfif>
			<cffile action="upload" filefield="filCSVFile" destination="#variables.dirPath#\QuizQuestionPool-#variables.QuestionPoolID#-#request.relaycurrentuser.personID#.csv" nameconflict="OVERWRITE">

			<cfset fieldNames = arrayNew(1)>
			<cfset tmp = arrayAppend(fieldNames,"CountryDescription")>
			<cfset tmp = arrayAppend(fieldNames,"LanguageName")>
			<cfset tmp = arrayAppend(fieldNames,"QuestionRef")>
			<cfset tmp = arrayAppend(fieldNames,"QuestionTypeDescription")>
			<cfset tmp = arrayAppend(fieldNames,"QuestionText")>
			<cfset tmp = arrayAppend(fieldNames,"QuestionSortOrder")>
			<cfset tmp = arrayAppend(fieldNames,"AnswerRef")>
			<cfset tmp = arrayAppend(fieldNames,"AnswerText")>
			<cfset tmp = arrayAppend(fieldNames,"AnswerScore")>
			<!--- <cfset tmp = arrayAppend(fieldNames,"SortOrder")> ---> <!--- 2013-04-15 STCR CASE 432328 Use new AnswerSortOrder column instead, although see further down for effort to maintain backward compatibility for customers using previous csv template --->
			<cfset tmp = arrayAppend(fieldNames,"AnswerSortOrder")>
			<cfset tmp = arrayAppend(fieldNames,"MatchingAnswerRef")>
			<!--- 2013-04-15 STCR CASE 432328 - since CASE 429346 change to sourceCollectionMethod for utf-8 support, the fieldNames array is ignored by fileToDB,
				however we are keeping it here because it is being still being used for a nested [[fieldList]] phrase within phr_eLearning_Bulk_CSVIncorrectColumns --->
			<cfset mergeStruct = {fieldList = arrayToList(fieldNames)}>
			<!--- READ CSV --->
			<!--- Trap known/expected errors and return friendlier error messages. --->
			<cftry>
				<cfset returnStruct= application.com.dataTransfers.fileToDB(rethrowOnError=true,dir="#variables.dirPath#\",filename="#cffile.serverFile#",Delim=",",Tablename="QuizQuestionPoolBulkUpload",ColumnNames="true",fieldNames=fieldNames,deleteQryWhereClause="createdBy=#request.relaycurrentuser.personID#",tableOptions="delete",validateFile="true", setCreated="true",sourceCollectionMethod="http", sourceDirectory=cffile.SERVERDIRECTORY, charset="iso-8859-1")>
				<!--- 2013-04-15 STCR - note that the 2012-11-21 change to use: sourceCollectionMethod="http", sourceDirectory=cffile.SERVERDIRECTORY - fixes utf-8 support but also causes fileToDB to ignore the fieldNames array and attempts to match the csv column headings against db column names instead. --->
				<cfcatch>
				<!--- This is an example of how we could handle different errors more gracefully if fileToDB did not trap them. --->
				<cfif structKeyExists(cfcatch,"Detail") and (findNoCase("Error converting data type",cfcatch.Detail) gt 0
													or 	findNoCase("Conversion failed when converting",cfcatch.Detail) gt 0 )>
					<cf_translate>
						<h2> phr_eLearning_Bulk_ErrorFileNotUploaded </h2>
						<p> phr_eLearning_Bulk_ErrorConvertingDataType </p>
					</cf_translate>
					<cfset application.com.errorHandler.recordRelayError_Warning(type="QuizQuestionPool Bulk Upload fileToDB",Severity="error",caughtError=cfcatch)>
					<cf_abort>
				<cfelseif (structKeyExists(cfcatch,"Message") and (
															findNoCase("parameter of the Left function",cfcatch.Message) gt 0
														or 	findNoCase("parameter of the Mid function",cfcatch.Message) gt 0
														or 	findNoCase("parameter of the Right function",cfcatch.Message) gt 0
															))
						or (structKeyExists(cfcatch,"detail") and (
															findNoCase("invalid column name",cfcatch.detail)
														or  findNoCase("The identifier that starts with",cfcatch.detail)  <!--- The identifier that starts with 'EXTREMELY_LONG_COLUMN_NAME_..._IN_CSV_FILE' is too long. Maximum length is 128. --->
															)) >

					<cf_translate mergeStruct="#mergeStruct#">
						<h2> phr_eLearning_Bulk_IncorrectFileFormat </h2>
						<p> phr_eLearning_Bulk_CSVIncorrectColumns </p>
					</cf_translate>
					<cfset application.com.errorHandler.recordRelayError_Warning(type="QuizQuestionPool Bulk Upload fileToDB fn_removeSpecialCharsFromTextQualifiedString",Severity="error",caughtError=cfcatch)>
					<cf_abort>
				<cfelse>
					<cfrethrow />
				</cfif>
			</cfcatch>
			</cftry>

			<cfoutput>
			<cfif returnStruct.fileLoaded>
				<cf_translate><p>phr_eLearning_Bulk_UploadComplete #returnStruct.numRowsInserted# phr_eLearning_Bulk_RowsProcessed</p></cf_translate>

				<!--- First prep the temp table with the correct keys we need for later --->
				<cfquery name="setQuestionPoolID" datasource="#application.siteDataSource#">
					UPDATE QuizQuestionPoolBulkUpload
					SET QuestionPoolID = <cf_queryparam cfsqltype="cf_sql_integer" value="#variables.QuestionPoolID#">
					WHERE createdBy = #request.relaycurrentuser.personID# AND QuestionPoolID IS NULL
				</cfquery>
				<!--- <cfif application.testsite eq 2 and cgi.SERVER_NAME contains "dev3"><cfdump var="#returnStruct#" label="fileToDB returnStruct" /></cfif> ---><!--- TODO remove debug --->
				<!--- 2013-04-15 STCR CASE 432328 backward compatibility with old SortOrder from 2012 csv template. This is possible since the change to using fileToDB in http mode ignores the fieldNames array, although it does mean that csv column headers must match db column names. --->
				<cfif structKeyExists(returnStruct,"importQuery") and structKeyExists(returnStruct.importQuery,"SortOrder") and not structKeyExists(returnStruct.importQuery,"AnswerSortOrder")>
					<cfquery name="setQuestionPoolID" datasource="#application.siteDataSource#">
						UPDATE QuizQuestionPoolBulkUpload
						SET AnswerSortOrder = SortOrder
						WHERE createdBy = #request.relaycurrentuser.personID# AND QuestionPoolID = <cf_queryparam cfsqltype="cf_sql_integer" value="#variables.QuestionPoolID#">
						AND AnswerSortOrder IS NULL AND SortOrder IS NOT NULL
					</cfquery>
				</cfif>
				<!---
				Translate user friendly question type names to system QuestionTypeTextID
				Multiple Choice -> checkbox
				Single Choice -> radio
				Fill in the Blank [not supported by Relayware]
				Short Answer [not supported by Relayware]
				Matching -> matching [Phase 2]
				 --->
				<!--- STCR - if there were many question types then we could do this next step in fewer queries by adding a 'QuestionTypeDescription' column to the QuizQuestionType table and updating using a join on that, however that would be less flexible if we chose later to allow multiple friendly names to map to the same QuestionType (eg "Single Choice" and "True/False") --->
				<cfquery name="setQuestionTypeTextID_checkbox" datasource="#application.siteDataSource#">
					UPDATE QuizQuestionPoolBulkUpload
					SET QuestionTypeTextID = 'checkbox', QuestionTypeID = (SELECT QuestionTypeID FROM QuizQuestionType WHERE QuestionTypeTextID='checkbox')
					WHERE createdBy = #request.relaycurrentuser.personID# AND QuestionPoolID = <cf_queryparam cfsqltype="cf_sql_integer" value="#variables.QuestionPoolID#">
					AND QuestionTypeDescription = 'Multiple Choice'
				</cfquery>
				<cfquery name="setQuestionTypeTextID_radio" datasource="#application.siteDataSource#">
					UPDATE QuizQuestionPoolBulkUpload
					SET QuestionTypeTextID = 'radio', QuestionTypeID = (SELECT QuestionTypeID FROM QuizQuestionType WHERE QuestionTypeTextID='radio')
					WHERE createdBy = #request.relaycurrentuser.personID# AND QuestionPoolID = <cf_queryparam cfsqltype="cf_sql_integer" value="#variables.QuestionPoolID#">
					AND QuestionTypeDescription = 'Single Choice'
				</cfquery>
				<!--- P-REL109 Phase 2 CASE 430084 --->
				<cfquery name="setQuestionTypeTextID_matching" datasource="#application.siteDataSource#">
					UPDATE QuizQuestionPoolBulkUpload
					SET QuestionTypeTextID = 'matching', QuestionTypeID = (SELECT QuestionTypeID FROM QuizQuestionType WHERE QuestionTypeTextID='matching')
					WHERE createdBy = #request.relaycurrentuser.personID# AND QuestionPoolID = <cf_queryparam cfsqltype="cf_sql_integer" value="#variables.QuestionPoolID#">
					AND QuestionTypeDescription = 'Matching'
				</cfquery>
				<!--- Ensure 'All' or 'Any' Country matches 'All Countries'. We can remove this step if training/documentation insists on 'All Countries'. --->
				<cfquery name="setCountryDescription_All" datasource="#application.siteDataSource#">
					UPDATE QuizQuestionPoolBulkUpload
					SET CountryDescription = 'All Countries'
					WHERE createdBy = #request.relaycurrentuser.personID#
					AND QuestionPoolID = <cf_queryparam cfsqltype="cf_sql_integer" value="#variables.QuestionPoolID#">
					<!--- 2014-01-23 NYB Case 438455 changed "All or Any" to just "All" --->
					AND CountryDescription = 'All'
				</cfquery>
				<!--- START: 2014-01-23 NYB Case 438455 added: --->
				<cfquery name="setCountryDescription_Any" datasource="#application.siteDataSource#">
					UPDATE QuizQuestionPoolBulkUpload
					SET CountryDescription = 'Any Country'
					WHERE createdBy = #request.relaycurrentuser.personID#
					AND QuestionPoolID = <cf_queryparam cfsqltype="cf_sql_integer" value="#variables.QuestionPoolID#">
					AND CountryDescription = 'Any'
				</cfquery>
				<!--- END: 2014-01-23 NYB Case 438455 --->
				<!--- Set as many keys as we can in one pass, ready for translation. Questions first, Answers later... --->
				<!--- Assumes that QuestionRef is unique within a given QuestionPool --->
				<!--- PJP 20/12/2012 CASE 432544: Added in check for ANY as country setting countryid to 0 --->
				<cfquery name="matchExistingQuestions" datasource="#application.siteDataSource#">
					UPDATE bu
					SET
						bu.LanguageID = L.LanguageID,
						<!--- 2014-01-23 NYB Case 438455 changed "All Countries" to "Any Country" --->
						bu.CountryID = CASE when bu.CountryDescription = 'Any Country' then 0 else c.CountryID	end,
						bu.QuestionID = qq.QuestionID
					FROM QuizQuestionPoolBulkUpload bu
					left join [Language] L on L.[Language] = bu.LanguageName
					left join Country c on c.CountryDescription = bu.CountryDescription
					left join QuizQuestion qq on qq.QuestionRef = bu.QuestionRef and qq.QuestionPoolID = bu.QuestionPoolID <!--- Only match against Questions in the same QuestionPool. The same QuestionRef could be used in a different QuestionPool --->
					WHERE bu.createdBy = #request.relaycurrentuser.personID# AND bu.QuestionPoolID = <cf_queryparam cfsqltype="cf_sql_integer" value="#variables.QuestionPoolID#">
				</cfquery>
				<!--- For insertions we want to get one row per unmatched QuestionRef, regardless of the number of country/language translations supplied --->
				<cfquery name="getQuestionsToInsert" datasource="#application.siteDataSource#">
					select distinct QuestionPoolID, QuestionRef, QuestionID, QuestionTypeID, QuestionSortOrder <!--- 2013-04-15 STCR CASE 432328 Question Sort Order --->
					from QuizQuestionPoolBulkUpload
					WHERE createdBy = #request.relaycurrentuser.personID# AND QuestionPoolID = <cf_queryparam cfsqltype="cf_sql_integer" value="#variables.QuestionPoolID#">
					AND QuestionID IS NULL
					AND QuestionTypeID IS NOT NULL
					AND isNull(QuestionRef,'') <> ''
				</cfquery>

				<!---
					Display a representation of the uploaded data to the user and provide a means to cancel or continue processing the uploaded data.
					Eg display a summary of total number of questions read and total number of answers read, or output a TFQO.
				 --->

				<cfloop query="getQuestionsToInsert">
					<cfset variables.newQuestionID = 0 />
					<cfif len(trim(getQuestionsToInsert.QuestionRef)) gt 0>
						<cfset variables.newQuestionID = application.com.relayquiz.insertQuestion(QuestionPoolID=getQuestionsToInsert.QuestionPoolID, QuestionTypeID=getQuestionsToInsert.QuestionTypeID, QuestionRef=getQuestionsToInsert.QuestionRef, SortOrder=IIf(isNumeric(getQuestionsToInsert.QuestionSortOrder),getQuestionsToInsert.QuestionSortOrder,0)) />
						<cfif variables.newQuestionID gt 0>
							<!--- Once we've inserted the new Question we want to update all other country/language rows for this Question in the temp table with the new ID ready for the full pass --->
							<cfquery name="updateNewQuestions" datasource="#application.siteDataSource#">
								UPDATE QuizQuestionPoolBulkUpload
								SET QuestionID =  <cf_queryparam value="#variables.newQuestionID#" CFSQLTYPE="cf_sql_integer" >
								WHERE createdBy = #request.relaycurrentuser.personID# AND QuestionPoolID = <cf_queryparam cfsqltype="cf_sql_integer" value="#variables.QuestionPoolID#">
								AND QuestionRef  =  <cf_queryparam value="#getQuestionsToInsert.QuestionRef#" CFSQLTYPE="CF_SQL_VARCHAR" >
							</cfquery>
							<!--- This way we are calling only one insert+update query per new QuizQuestion. As opposed to looping through the whole bulk table and doing a separate select query to check the QuestionID for each translation --->
						</cfif>
					</cfif>
				</cfloop>
				<!--- Now the Question IDs are all ready, we can get them and update the translations --->
				<cfquery name="getQuestionsToTranslate" datasource="#application.siteDataSource#">
					select distinct CountryID, LanguageID, QuestionID, QuestionText
					from QuizQuestionPoolBulkUpload
					WHERE createdBy = #request.relaycurrentuser.personID# AND QuestionPoolID = <cf_queryparam cfsqltype="cf_sql_integer" value="#variables.QuestionPoolID#">
					AND QuestionID IS NOT NULL
					AND CountryID IS NOT NULL
					AND LanguageID IS NOT NULL
				</cfquery>
				<cfloop query="getQuestionsToTranslate">
					<cfset application.com.relayTranslations.addNewPhraseAndTranslationV2(entityTypeID=application.entityTypeID.QuizQuestion, entityID=getQuestionsToTranslate.QuestionID, phrasetext=getQuestionsToTranslate.QuestionText, phraseTextID='Title', countryID=getQuestionsToTranslate.CountryID, languageID=getQuestionsToTranslate.LanguageID) />
				</cfloop>
				<!--- Questions processed, so now we can process the Answers, knowing that all the keys apart from the AnswerID have been populated --->
				<cfquery name="matchExistingAnswers" datasource="#application.siteDataSource#">
					UPDATE bu
					SET
						bu.AnswerID = qa.AnswerID
					FROM QuizQuestionPoolBulkUpload bu
					left join QuizAnswer qa on qa.AnswerRef = bu.AnswerRef AND qa.QuestionID = bu.QuestionID
					WHERE bu.createdBy = #request.relaycurrentuser.personID# AND bu.QuestionPoolID = <cf_queryparam cfsqltype="cf_sql_integer" value="#variables.QuestionPoolID#">
				</cfquery>
				<!--- 2012-08-24 STCR CASE 430084 --->
				<cfquery name="matchExistingMatchingAnswers" datasource="#application.siteDataSource#">
					UPDATE bu
					SET
						bu.MatchingAnswerID = qa.AnswerID
					FROM QuizQuestionPoolBulkUpload bu
					left join QuizAnswer qa on qa.AnswerRef = bu.MatchingAnswerRef AND qa.QuestionID = bu.QuestionID
					WHERE bu.createdBy = #request.relaycurrentuser.personID# AND bu.QuestionPoolID = <cf_queryparam cfsqltype="cf_sql_integer" value="#variables.QuestionPoolID#">
				</cfquery>
				<!--- For insertions we want to get one row per unmatched AnswerRef, regardless of the number of country/language translations supplied --->
				<cfquery name="getAnswersToInsert" datasource="#application.siteDataSource#">
					select distinct QuestionPoolID, QuestionID, AnswerRef, AnswerScore, AnswerSortOrder, MatchingAnswerRef <!--- 2012-08-24 STCR CASE 430084 add MatchingAnswerRef ---> <!--- 2013-04-15 STCR CASE 432328 Answers now using new AnswerSortOrder column instead of old sortOrder column --->
					from QuizQuestionPoolBulkUpload
					WHERE createdBy = #request.relaycurrentuser.personID# AND QuestionPoolID = <cf_queryparam cfsqltype="cf_sql_integer" value="#variables.QuestionPoolID#">
					AND AnswerID IS NULL
					AND isNull(AnswerRef,'') <> ''
					AND QuestionID IS NOT NULL
					AND AnswerScore IS NOT NULL
				</cfquery>
				<cfloop query="getAnswersToInsert">
					<cfset variables.newAnswerID = 0 />
					<cfif len(trim(getAnswersToInsert.AnswerRef)) gt 0>
						<cfset variables.newAnswerID = application.com.relayquiz.insertAnswer(QuestionID=getAnswersToInsert.QuestionID, Score=getAnswersToInsert.AnswerScore, AnswerRef=getAnswersToInsert.AnswerRef, MatchingAnswerRef=getAnswersToInsert.MatchingAnswerRef, SortOrder=IIf(isNumeric(getAnswersToInsert.AnswerSortOrder),getAnswersToInsert.AnswerSortOrder,0)) />
						<cfif variables.newAnswerID gt 0>
							<!--- Once we've inserted the new Answer we want to update all other country/language rows for this Answer in the temp table with the new ID ready for the full pass --->
							<cfquery name="updateNewAnswers" datasource="#application.siteDataSource#">
								UPDATE QuizQuestionPoolBulkUpload
								SET AnswerID =  <cf_queryparam value="#variables.newAnswerID#" CFSQLTYPE="cf_sql_integer" >
								WHERE createdBy = #request.relaycurrentuser.personID# AND QuestionPoolID = <cf_queryparam cfsqltype="cf_sql_integer" value="#variables.QuestionPoolID#">
								AND AnswerRef =  <cf_queryparam value="#getAnswersToInsert.AnswerRef#" CFSQLTYPE="CF_SQL_VARCHAR" >
								AND QuestionID  =  <cf_queryparam value="#getAnswersToInsert.QuestionID#" CFSQLTYPE="CF_SQL_VARCHAR" >
							</cfquery>
							<!--- 2012-08-24 STCR CASE 430084 --->
							<cfquery name="updateNewAnswers" datasource="#application.siteDataSource#">
								UPDATE QuizQuestionPoolBulkUpload
								SET MatchingAnswerID =  <cf_queryparam value="#variables.newAnswerID#" CFSQLTYPE="cf_sql_integer" >
								WHERE createdBy = #request.relaycurrentuser.personID# AND QuestionPoolID = <cf_queryparam cfsqltype="cf_sql_integer" value="#variables.QuestionPoolID#">
								AND MatchingAnswerRef =  <cf_queryparam value="#getAnswersToInsert.AnswerRef#" CFSQLTYPE="CF_SQL_VARCHAR" >
								AND QuestionID  =  <cf_queryparam value="#getAnswersToInsert.QuestionID#" CFSQLTYPE="CF_SQL_VARCHAR" >
							</cfquery>
							<!--- This way we are calling only one insert+update query per new QuizAnswer. As opposed to looping through the whole bulk table and doing a separate select query to check the QuestionID for each translation --->
						</cfif>
					</cfif>
				</cfloop>
				<!--- Set the QuizAnswer.MatchingAnswerID for each QuizQuestionPoolBulkUpload.AnswerID where MatchingAnswerID IS NOT NULL  --->
				<cfquery name="updateMatchingAnswerIDs" datasource="#application.siteDataSource#">
					UPDATE qa
					SET
						qa.MatchingAnswerID = bu.MatchingAnswerID
					FROM QuizAnswer qa
					left join QuizQuestionPoolBulkUpload bu on qa.MatchingAnswerRef = bu.MatchingAnswerRef AND qa.QuestionID = bu.QuestionID
					WHERE bu.createdBy = #request.relaycurrentuser.personID# AND bu.QuestionPoolID = <cf_queryparam cfsqltype="cf_sql_integer" value="#variables.QuestionPoolID#">
					AND bu.MatchingAnswerID IS NOT NULL
					AND isNull(bu.MatchingAnswerRef,'') <> ''
				</cfquery>
				<!--- Now the Answer IDs are all in place, we can update the translations --->
				<cfquery name="getAnswersToTranslate" datasource="#application.siteDataSource#">
					select distinct isnull(CountryID,0) as countryid, LanguageID, AnswerID, AnswerText
					from QuizQuestionPoolBulkUpload
					WHERE createdBy = #request.relaycurrentuser.personID# AND QuestionPoolID = <cf_queryparam cfsqltype="cf_sql_integer" value="#variables.QuestionPoolID#">
					AND AnswerID IS NOT NULL
					AND LanguageID IS NOT NULL
				</cfquery>
				<cfloop query="getAnswersToTranslate">
					<cfset application.com.relayTranslations.addNewPhraseAndTranslationV2(entityTypeID=application.entityTypeID.QuizAnswer, entityID=getAnswersToTranslate.AnswerID, phrasetext=getAnswersToTranslate.AnswerText, phraseTextID='Title', countryID=getAnswersToTranslate.CountryID, languageID=getAnswersToTranslate.LanguageID) />
				</cfloop>
				<cfset variables.showBulkReports = true /><!--- Render TFQOs --->
			<cfelse><!--- Unknown fileToDB error --->
				<cf_translate>phr_eLearning_Bulk_ErrorFileNotUploaded</cf_translate>
			</cfif>

			</cfoutput>

			<!--- Show user the data rejected by FileToDB --->
			<cfif structKeyExists(returnStruct,"badRecords") AND returnStruct.badRecords.RecordCount GT 0>
				<cf_translate mergeStruct="#mergeStruct#"><h2>phr_eLearning_Bulk_IncorrectFileFormat</h2>
				<p>phr_eLearning_Bulk_CSVIncorrectColumns</p></cf_translate>
				<cfoutput>
					<cfloop query="returnStruct.badRecords">
						<cf_translate><p>phr_eLearning_Bulk_RowNumber #returnStruct.badRecords.rowNum#. #returnStruct.badRecords.data#</p></cf_translate>
					</cfloop>
				</cfoutput>
			</cfif>
		<cfelse>
			<cf_translate>phr_eLearning_SelectQuestionPool</cf_translate>
		</cfif><!--- END check questionPool --->
	<!--- The file has not yet been uploaded, so show the file upload form. --->
	<cfelseif not variables.showBulkReports>

		<cf_translate>
		<cfoutput>
		<script language="JavaScript">
			function validateQuestionPool() {
				var qp = document.getElementById('questionPoolID');
				if(#variables.QuestionPoolID# == 0) {
					if(qp.options[qp.selectedIndex].value == 0) {
						alert('#JSStringFormat("phr_eLearning_SelectQuestionPool")#');
						return false;
					}
				}
				return true;
			}
			function validateFile() {
				filename = document.frmCSVUpload.filCSVFile.value;
				if (filename.length == 0) {
					alert('#JSStringFormat("phr_eLearning_Bulk_PleaseSelectAFile")#');
					return false;
				}
				fileExtension = filename.substr(filename.length-4);
				if ((fileExtension != '.txt') && (fileExtension != '.csv')) {
					alert('#JSStringFormat("phr_eLearning_Bulk_UploadFormatsMessage")#');
					return false;
				}
				return true;
			}

			function showLoader() {
				document.frmCSVUpload.submitform.disabled = true;
				document.getElementById("uploader_image").innerHTML = "phr_eLearning_Bulk_PleaseWait <br /><img src='/images/icons/loading.gif' />";
			}
			function submitForm() {
				canSubmitForm = validateQuestionPool() && validateFile();
				if (canSubmitForm) {
					document.frmCSVUpload.submit();
					showLoader();
				}
			}

		</script>
		</cfoutput>

		<!--- <h1>phr_eLearning_Bulk_UploadCSVFile</h1> ---><!--- replaced h1 tag with relayNavMenu pageTitle from eLearningTopHead.cfm --->
		<cfset request.relayFormDisplayStyle="HTML-table">
		<cf_relayFormDisplay>
		<cfform action="" method="post" enctype="multipart/form-data" name="frmCSVUpload" id="frmCSVUpload" onSubmit="return validateFile();" >


			<cfif variables.QuestionPoolID eq 0>
				<!--- 2012/11/22	YMA		CASE: 431961 No need to use 'forDropDown'. Instead I've added nullText attribute to relayFormElementDisplay.  Also updated display and value properties.--->
				<cfset variables.getQuestionPools = application.com.relayQuiz.getQuizQuestionPoolList(sortOrder="QuestionPoolRef") />
				<CF_relayFormElementDisplay relayFormElementType="select" fieldname="questionPoolID" id="questionPoolID" label="phr_eLearning_SelectQuestionPool" currentValue="#variables.QuestionPoolID#" query="#variables.getQuestionPools#" display="QuestionPoolRef" value="QuestionPoolID" nullText="phr_elearning_QuestionPool_none">
				<!--- <tr>
					<td>
						phr_eLearning_SelectQuestionPool
					</td>
					<td>
						<cf_relayFormElement relayFormElementType="select" fieldname="questionPoolID" label="phr_eLearning_QuestionPool" currentValue="#variables.QuestionPoolID#" query="#variables.getQuestionPools#" display="display" value="value" >
					</td>
				</tr> --->
			<cfelse>
				<cfset variables.getQuestionPools = application.com.relayQuiz.getQuizQuestionPoolList(QuestionPoolID=variables.QuestionPoolID) />
				<CF_relayFormElementDisplay relayFormElementType="HTML" currentValue="#variables.getQuestionPools.QuestionPoolRef#" fieldname="" label="phr_eLearning_QuestionPool">
				<cfif not isDefined("url.QuestionPoolID")><CF_relayFormElementDisplay relayFormElementType="hidden" label="" id="questionPoolID" currentvalue="#variables.QuestionPoolID#" fieldname="QuestionPoolID"></cfif>
				<!--- <tr>
					<td>phr_eLearning_QuestionPool</td>
					<td><cfoutput>#htmlEditFormat(variables.getQuestionPools.QuestionPoolRef)#</cfoutput><cfif not isDefined("url.QuestionPoolID")><input type="hidden" name="QuestionPoolID" value="#HTMLEditFormat(variables.QuestionPoolID)#" /></cfif></td>
				</tr> --->
			</cfif>
			<CF_relayFormElementDisplay relayFormElementType="file" fieldname="filCSVFile" id="filCSVFile" accept="text/csv" label="phr_eLearning_Bulk_PleaseSelectAFile" currentValue="">

				<!--- <tr>
					<td>phr_eLearning_Bulk_PleaseSelectAFile </td>
				 	<td><input type="file" name="filCSVFile" id="filCSVFile" accept="text/csv" /></td>
				</tr> --->
				<!--- <tr>
					<td>phr_eLearning_BulkUpload_FirstRowHasHeadings </td>
					<td><input type="checkbox" name="frmFirstRowColumnHeadings" id="frmFirstRowColumnHeadings" value="1" /></td>
				</tr> --->

				<!--- <input type="button" name="submitform" value="phr_eLearning_Bulk_UploadFileSubmit" onClick="submitForm();" /> --->

			<div class="row">
				<div class="col-xs-12">
					<div id="uploader_image">&nbsp;</div>
					<p>phr_eLearning_Bulk_OverwriteWarning</p>
					<!--- 2014-01-23 NYB Case 438455 added additional text and reference to an Excel Template file --->
					<p>Download and complete this Question Template <a href="/elearning/question_template.csv">template.csv</a> or use the <a href="/elearning/question_template.xltx">Excel Template file</a> then Save As a csv file.</p>
					<CF_relayFormElement relayFormElementType="button" class="btn btn-primary margin-bottom-15" fieldname="submitform" currentValue="phr_eLearning_Bulk_UploadFileSubmit" onClick="submitForm();" label="" spanCols="No" valueAlign="left">
				</div>
			</div>
		</cfform>
		</cf_relayFormDisplay>
		</cf_translate>
	</cfif>
	<!--- If we have just uploaded data or if we were sent here from the TFQO then we want to load the reports without reprocessing all the data --->
	<cfif variables.QuestionPoolID gt 0 and variables.showBulkReports>
		<!---
				Provide a summary (using TFQO) of the Question Pool contents after processing is complete. Report to display:
			a.	Questions and their related Answers as Loaded
			b.	Records that failed to load for any reason
			c.	Edit button by each of the successful records to link to the appropriate Edit Screen:
		--->
		<!--- load header --->


		<!--- params --->
		<cfparam name="sortOrder" default="QuestionRef,QuestionSortOrder,AnswerRef,AnswerSortOrder" />
		<cfparam name="numRowsPerPage" default="50" />
		<cfparam name="startRow" default="1" />
		<cfscript>
		   frmVars = StructNew();
		   StructInsert(frmVars, "QuestionPoolID", #variables.QuestionPoolID#);
		   StructInsert(frmVars, "showBulkReports", true);
		</cfscript>

		<cfquery name="getBadRows" datasource="#application.siteDataSource#">
			select * from
			(
				select * from QuizQuestionPoolBulkUpload bu
				where createdBy = #request.relaycurrentuser.personID#
				AND QuestionPoolID = <cf_queryparam cfsqltype="cf_sql_integer" value="#variables.QuestionPoolID#">
				AND (
				<!--- PJP CASE 432544:re added the join to countryid null  --->
				CountryId IS NULL
				OR LanguageID IS NULL
				OR QuestionID IS NULL
				OR QuestionTypeID IS NULL
				OR AnswerID IS NULL
				OR AnswerScore IS NULL
				)
				<!--- Could separate each of these out with specific phr_reason_xxx codes then union together and translate in the TFQO? --->
			)base where 1=1
			<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
			order by <cf_queryObjectName value="#sortOrder#">
		</cfquery>
		<cfif getBadRows.recordCount gt 0>
			<cfoutput>
			<cf_translate>
				<h2> phr_eLearning_Bulk_ErrorReportTitle </h2>
				<p>#getBadRows.recordCount# phr_eLearning_Bulk_RowsNotSaved </p>
			</cf_translate>
			</cfoutput>
			<CF_tableFromQueryObject
				id="badRowsReport"
				queryObject="#getBadRows#"
				queryName="getBadRows"
				sortOrder = "#sortOrder#"
				numRowsPerPage="#numRowsPerPage#"
				numberFormat="Score"
				dateTimeFormat=""
				startRow="#startRow#"
				showTheseColumns="CountryDescription,CountryID,LanguageName,LanguageID,QuestionRef,QuestionID,QuestionTypeDescription,QuestionTypeID,QuestionText,QuestionSortOrder,AnswerRef,AnswerID,AnswerText,AnswerScore,AnswerSortOrder,MatchingAnswerRef,MatchingAnswerID"
				columnHeadingList="Country,Matched_Country_ID,Language,Matched_Language_ID,Question_Ref,Matched_Question_ID,Question_Type,Matched_Question_Type_ID,Question_Text,Question_Sort_Order,Answer_Ref,Matched_Answer_ID,Answer_Text,Answer_Score,Answer_Sort_Order,Matching_Answer_Ref,Matched_Matching_Answer_ID"
				columnTranslation="False"
				allowColumnSorting="no"
				passThroughVariablesStructure = "#frmVars#"
				<!--- FilterSelectFieldList = ""
				FilterSelectFieldList2 = ""
				FilterSelectFieldList3 = "" --->
			>
		</cfif>
		<!--- TFQO showing saved Question / Answer rows from this Pool, with edit link on Question. Answers are linked off of the Question page, so we do not provide a separate direct link to edit the Answer. --->
		<cfquery name="getGoodRows" datasource="#application.siteDataSource#">
			select * from
			(
				select * from QuizQuestionPoolBulkUpload bu
				where createdBy = #request.relaycurrentuser.personID#
				AND QuestionPoolID = <cf_queryparam cfsqltype="cf_sql_integer" value="#variables.QuestionPoolID#">
				<!--- PJP CASE 432544: re added the join to countryid null  --->
				AND CountryID IS NOT NULL
				AND LanguageID IS NOT NULL
				AND QuestionID IS NOT NULL
				AND QuestionTypeID IS NOT NULL
				AND AnswerID IS NOT NULL
				AND AnswerScore IS NOT NULL
			)base where 1=1
			<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
			order by <cf_queryObjectName value="#sortOrder#">
		</cfquery>
		<cfif getGoodRows.recordCount gt 0>
			<cfoutput>
			<cf_translate>
				<h2> phr_eLearning_Bulk_SuccessReportTitle </h2>
				<p>#getGoodRows.recordCount# phr_eLearning_Bulk_RowsSaved </p>
			</cf_translate>
			</cfoutput>
			<CF_tableFromQueryObject
				id="goodRowsReport"
				queryObject="#getGoodRows#"
				queryName="getGoodRows"
				sortOrder = "#sortOrder#"
				numRowsPerPage="#numRowsPerPage#"
				numberFormat="Score"
				dateTimeFormat=""
				startRow="#startRow#"
				showTheseColumns="CountryDescription,CountryID,LanguageName,LanguageID,QuestionRef,QuestionID,QuestionTypeDescription,QuestionTypeID,QuestionText,QuestionSortOrder,AnswerRef,AnswerID,AnswerText,AnswerScore,AnswerSortOrder,MatchingAnswerRef,MatchingAnswerID"
				columnHeadingList="Country,Matched_Country_ID,Language,Matched_Language_ID,Question_Ref,Matched_Question_ID,Question_Type,Matched_Question_Type_ID,Question_Text,Question_Sort_Order,Answer_Ref,Matched_Answer_ID,Answer_Text,Answer_Score,Answer_Sort_Order,Matching_Answer_Ref,Matched_Matching_Answer_ID"
				keyColumnList="QuestionRef,QuestionID"
				keyColumnKeyList="QuestionID,QuestionID"
				keyColumnURLList="/eLearning/questions.cfm?editor=yes&questionPoolID=#variables.questionPoolID#&frmBackForm=questionPools.cfm&QuestionID=,/eLearning/questions.cfm?editor=yes&questionPoolID=#variables.questionPoolID#&showBackButton=false&frmBackForm=questionPools.cfm&QuestionID="
				keyColumnOpenInWindowList="true,true"
				OpenWinNameList="Tab_Question,Tab_Question"
				OpenWinSettingsList="iconClass:'script_edit',iconClass:'script_edit'"
				columnTranslation="False"
				allowColumnSorting="no"
				passThroughVariablesStructure = "#frmVars#"
				<!--- FilterSelectFieldList = ""
				FilterSelectFieldList2 = ""
				FilterSelectFieldList3 = "" --->
			>
		</cfif>
	</cfif>
<cfelse><!--- does not have correct securityTask --->
	<cf_translate>phr_element_YouDoNotHaveRightsToViewElement</cf_translate>
</cfif>
