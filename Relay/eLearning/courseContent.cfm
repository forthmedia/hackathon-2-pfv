<!--- �Relayware. All Rights Reserved 2014 --->

<cf_title>Courseware</cf_title>

<cfset FileTypeHeading = "ELearning_CourseWare">

<cfquery name="getfiletypegroupid" datasource="#application.siteDataSource#">
	SELECT filetypegroupid FROM filetypegroup
		where [heading] =  <cf_queryparam value="#FileTypeHeading#" CFSQLTYPE="CF_SQL_VARCHAR" > 
</cfquery>

<cfif getfiletypegroupid.recordCount gt 0>
	<cfset filetypegroupid = getfiletypegroupid.filetypegroupid>
<cfelse>
	<!--- if it's missing insert and try again --->
	<cfquery name="insertFiletypegroupid" datasource="#application.siteDataSource#">
		INSERT INTO filetypegroup([heading]) VALUES(<cf_queryparam value="#FileTypeHeading#" CFSQLTYPE="CF_SQL_VARCHAR" >)	
	</cfquery>
		
	<cfquery name="getfiletypegroupid" datasource="#application.siteDataSource#">
		SELECT filetypegroupid FROM filetypegroup
			where [heading] =  <cf_queryparam value="#FileTypeHeading#" CFSQLTYPE="CF_SQL_VARCHAR" > 
	</cfquery>
	<cfset filetypegroupid = getfiletypegroupid.filetypegroupid>
</cfif>

<cfset url.search=true>
<cfparam name="url.searchString" default="">
<cfset url.fileTypeGroupID = fileTypeGroupID>

<cfset application.com.request.setTopHead(topHead="elearningTopHead.cfm")>

<cfinclude template="/filemanagement/applicationFileManagerIni.cfm">
<cfinclude template="/filemanagement/fileSearch.cfm">