<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		questionPoolsInclude.cfm	
Author:			STCR  
Date started:	2012-05-03
	
Description:	Used to include Questions from the Question Pool editor. Adapted from certificationsInclude.cfm

Amendment History:

Date (YYYY-MM-DD)	Initials 	What was changed
2012-09-04			STCR		CASE 430069 Question Pools usability - enable 'Manage Questions' link after first save

Possible enhancements:


 --->


<cfif not Isdefined("AddNew")>

	<!--- <cfinclude template="/elearning/eligibilityRules.cfm"> --->

	<cfif structKeyExists(url,"questionPoolID")>
		<cfset variables.questionPoolID = url.questionPoolID>
	<cfelseif structKeyExists(form,"questionPoolID")>
		<cfset variables.questionPoolID = form.questionPoolID>
	<cfelse>
		Variable questionPoolID does not exist.
		<CF_ABORT>
	</cfif>

<!--- 2012-05-03 STCR replaced this with link to questions.cfm because there is already an iframe there for managing the Answers to the selected Question. Nesting another scrolling iframe here would be nasty. --->
<!--- 	<cfoutput>
		<iframe src="/elearning/questions.cfm?questionPoolID=#htmleditformat(variables.questionPoolID)#" width="100%" height="400px" scrolling="auto" frameborder=0 />
	</cfoutput> --->
	<cfif structKeyExists(URL,"editor") and not structKeyExists(url,"add") and isDefined("questionPoolID") and isNumeric(questionPoolID) and questionPoolID gt 0>
		<cfoutput><a href="questions.cfm?QuestionPoolID=#questionPoolID#" title="#htmlEditFormat(' phr_eLearning_Manage_Questions ')#">phr_eLearning_Manage_Questions</a></cfoutput>
	<cfelseif structKeyExists(variables,"questionPoolID") and isNumeric(variables.questionPoolID) and variables.questionPoolID gt 0>
		<!--- 2012-09-04 STCR CASE 430069 enable 'Manage Questions' link after first save. It would appear that RelayXMLEditor does populate variables.questionPoolID with the PK of the newly added record, so we can use it here. --->
		<cfoutput><a href="questions.cfm?QuestionPoolID=#variables.questionPoolID#" title="#htmlEditFormat(' phr_eLearning_Manage_Questions ')#">phr_eLearning_Manage_Questions</a></cfoutput>
	</cfif>
</cfif>
