<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Amendment History
NYB 2009-09-30 LHID2686
NJH 2015/03/04	Jira Fifteen-266 - select all columns so that they are available if sorting by them
 --->

<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">
<cfparam name="openAsExcel" type="boolean" default="false">

<cf_title>Training Report - By Course Summary</cf_title>

<cfparam name="sortOrder" default="Trainee,fulfilled desc,Country"><!--- NYB 2009-09-30 LHID2686 - changed userModuleFulfilled to fulfilled --->
<cfparam name="numRowsPerPage" default="50">
<cfparam name="startRow" default="1">
<cfparam name="checkBoxFilterName" type="string" default="">
<cfparam name="checkBoxFilterLabel" type="string" default="">
<cfparam name="form.fulfilled" default="All"> <!--- NYB 2009-09-30 LHID2686 - changed userModuleFulfilled to fulfilled --->
<cfset showTheseColumns="Trainee,Organisation_Name,Full_Postal_Address,Country,course_title,Module_Title,Score,Module_Status">

<cfif structKeyExists(form,"fulfilled") and form.fulfilled eq "All">
	<cfset showTheseColumns="#showTheseColumns#,fulfilled">
</cfif>

<cfif isDefined("frmRowIdentity") AND form.fulfilled eq "no">
	<cfquery name="updateFulfilledStatus" datasource="#application.siteDataSource#">
		UPDATE	trngUserModuleProgress
		SET		userModuleFulfilled = getdate(),
			lastUpdated=getDate(),
			lastUpdatedBy=#request.relayCurrentUser.userGroupID#,
			lastUpdatedByPerson=#request.relayCurrentUser.personId#
		WHERE	userModuleProgressID  IN ( <cf_queryparam value="#frmRowIdentity#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) AND
				userModuleFulfilled is null
	</cfquery>
	<cfoutput>#htmleditformat(ListLen(frmRowIdentity))# records fulfilled.</cfoutput>
</cfif>

<cfinclude template="reportTraining_UCMC_functions.cfm">

<cfquery name="getCourseLevels" datasource="#application.siteDataSource#">
	SELECT courseSeriesID, courseSeriesShortName
	FROM trngCourseSeries
</cfquery>

<cfset courseSeriesIDs = "">
<cfloop query="getCourseLevels">
	<cfif lCase(left(courseSeriesShortName,5)) eq "level" and trim(mid(courseSeriesShortName,6,len(courseSeriesShortName))) gte 2>
		<cfset courseSeriesIDs = listAppend(courseSeriesIDs,courseSeriesID)>
	</cfif>
</cfloop>

<cfquery name="gettrngUserModuleProgress" datasource="#application.siteDataSource#">
select distinct Trainee,Organisation_Name,Full_Postal_Address,Country,course_title,Module_Title,Score,Module_Status,fulfilled from (
SELECT		*, Trainee + '<br>' + Postal_Address AS Full_Postal_Address,
CASE WHEN userModuleFulfilled IS NULL THEN 'No' ELSE 'Yes' END AS fulfilled
FROM		vTrngUserModuleProgress
WHERE Module_StatusTextID='Passed'
) as baseQuery
<cfif structKeyExists(form,"fulfilled") and form.fulfilled neq "all">
	where fulfilled =  <cf_queryparam value="#form.fulfilled#" CFSQLTYPE="CF_SQL_VARCHAR" >
</cfif>

ORDER BY	<cf_queryObjectName value="#sortOrder#">
</cfquery>

<table width="100%" cellpadding="3">
	<tr>
		<td><strong>Training Report Certification</strong></td>
		<td align="right">&nbsp;</td>
	</tr>
	<!--- START: NYB 2009-09-30 LHID2686 --->
	<cfif structKeyExists(form,"fulfilled") and form.fulfilled eq "All">
	<tr><td colspan="2">Details of all courses</td></tr>
	</cfif>
	<!--- END: NYB 2009-09-30 LHID2686 --->
</table>

 <!--- START: NYB 2009-09-30 LHID2686 - removed ,Module_Status from numberFormat
		various other changes: --->
<cfif form.fulfilled eq "no">
	<CF_tableFromQueryObject
		queryObject="#getTrngUserModuleProgress#"
		queryName="getTrngUserModuleProgress"
		sortOrder = "#sortOrder#"
		numRowsPerPage="#numRowsPerPage#"
		startRow="#startRow#"
		openAsExcel="#openAsExcel#"
		hideTheseColumns=""
		showTheseColumns="#showTheseColumns#"
		columnTranslation="true"
		numberFormat="Attempts,Time_Spent,quiz_Passed,Score"
		dateformat=""

		rowIdentityColumnName="userModuleProgressID"
		functionListQuery="#comTableFunction.qFunctionList#"

		radioFilterLabel="Show Fulfilled:"
		radioFilterDefault="#form.fulfilled#"
		radioFilterName="fulfilled"
		radioFilterValues="No-Yes-All"

		allowColumnSorting="yes"
		showCellColumnHeadings="no"

		useinclude="false"
	>
<cfelse>
	<CF_tableFromQueryObject
		queryObject="#gettrngUserModuleProgress#"
		queryName="gettrngUserModuleProgress"
		sortOrder = "#sortOrder#"
		numRowsPerPage="#numRowsPerPage#"
		startRow="#startRow#"
		openAsExcel="#openAsExcel#"
		hideTheseColumns=""
		showTheseColumns="#showTheseColumns#"
		columnTranslation="true"
		numberFormat="Attempts,Time_Spent,quiz_Passed,Score"
		dateformat=""

		radioFilterLabel="Show Fulfilled:"
		radioFilterDefault="#form.fulfilled#"
		radioFilterName="fulfilled"
		radioFilterValues="No-Yes-All"

		allowColumnSorting="yes"
		showCellColumnHeadings="no"

		useinclude="false"
	>
</cfif>
 <!--- END: NYB 2009-09-30 LHID2686 --->