<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			quizSetup.cfm
Author:				SWJ
Date started:		2008-08-02
Description:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2009-10-23			NAS			LID - 2481/2499 Code entered to Order Translatable Columns
2010/09/28			NAS			LID4160: eLearning - Quizzes to only pick up modules on active courses
2010/10/04 			PPB			LID4237 paging not working
2010/11/17			NAS			LID4784: CR : Quiz - Setup Quiz Area - Add Column for Associated Module
2011/06/06			GCC			Added rowIdentityColumnName to stop the <tr id using associated module name which can contain HTML...
2012-07-06			WAB			Removed cf_translatequerycolumn and associated code.  Quiz_Name now comes pre-translated (from title_defaultTranslation)
2012-07-12			PJP			Case#429052 added new table inner join trngModuleCourse
2013-03-21			NYB 		Case 434096 made all the numeric fields required with a minimum value of 1
2015/08/14			NJH			Case 445556  - htmlEditFormat the quiz name fields
Possible enhancements:

 --->

<cf_title>Quiz Setup</cf_title>
<cfset application.com.request.setTopHead(pageTitle="Quizzes")>

<cfparam name="getData" type="string" default="foo">
<cfparam name="sortOrder" default="Quiz_Name">
<cfparam name="startRow" default="1">				<!--- PPB 2010/10/04 LID4237 paging not working --->
<cfparam name="numRowsPerPage" default="100">
<cfparam name="url.active" default=1>

<cfif structKeyExists(url,"quizDetailID") and not structKeyExists(form,"quizDetailID")>
	<cfset form.quizDetailID = url.quizDetailID>
</cfif>

<cfset xmlSource = "">

<cfif structKeyExists(url,"editor")>
	<!--- 2013-03-21 NYB Case 434096 added control,required & range to xml:--->
	<cfsavecontent variable="xmlSource">
		<cfoutput>
		<editors>
			<editor id="232" name="thisEditor" entity="quizDetail" title="Quiz Editor">
				<field name="QuizDetailID" label="Quiz ID" description="This records unique ID." control="html"></field>
				<field name="" CONTROL="TRANSLATION" label="Quiz Title" Parameters="phraseTextID=Title" required="true"></field>
				<field name="TimeAllowed" label="Time Allowed for Quiz" description="" control="numeric" required="true" range="1"></field>
				<field name="QuestionsPerPage" label="Questions Per Page" description="" control="numeric" required="true" range="1"></field>
				<field name="NumberOfAttemptsAllowed" label="Attempts Allowed" description="Attempts allowed" control="numeric" required="true" range="1"></field>
				<field name="passMark" label="Pass Mark" description="Pass Mark" control="numeric" required="true" range="1"></field>
				<field name="NumToChoose" label="Questions Per Quiz" description="The number of questions the quiz will show out of the number available" control="numeric" required="true" range="1"></field>
				<!--- if we want to translate these phrases as entities then use this code>
				<field name="" CONTROL="TRANSLATION" label="Header" Parameters="phraseTextID=Header,displayas=TextArea" required="false"></field>
				<field name="" CONTROL="TRANSLATION" label="Footer" Parameters="phraseTextID=Footer,displayas=TextArea" required="false"></field>
				--->
				<field name="" CONTROL="TRANSLATION" label="Quiz Header" Parameters="phraseTextID=header" description="Top text displayed in the final quiz screen, above quiz mark"></field>
				<field name="" CONTROL="TRANSLATION" label="Quiz Footer" Parameters="phraseTextID=footer" description="Bottom text displayed in the final quiz screen, below quiz mark"></field>
				<field name="active" label="Active" control="YorN"/>
				<group label="Questions per Question Pool" condition="form.quizDetailId neq 0">
					<field name="" control="includeTemplate" template="/eLearning/quizSetupInclude.cfm" label="Question Pools" spanCols="true" valueAlign="middle"/>
				</group>
				<group label="System Fields" name="systemFields">
					<field name="sysCreated"></field>
					<field name="sysLastUpdated"></field>
				</group>
			</editor>
		</editors>
		</cfoutput>
	</cfsavecontent>
<cfelse>
	<cfset getData = application.com.relayQuiz.getLiveQuizzes(sortOrder=sortOrder,active=url.active)>
</cfif>

<cf_listAndEditor
	xmlSource="#xmlSource#"
	cfmlCallerName="quizSetup"
	keyColumnList="Quiz_Name"
	keyColumnKeyList=" "
	keyColumnURLList = " "
	keyColumnOnClickList="javascript:openNewTab('##jsStringFormat(htmlEditFormat(Quiz_Name))##'*comma'##jsStringFormat(htmlEditFormat(Quiz_Name))##'*comma'/eLearning/quizSetup.cfm?editor=yes&hideBackButton=true&QuizDetailID=##jsStringFormat(QuizDetailID)##'*comma{reuseTab:true*commaiconClass:'elearning'});return false;"
	showTheseColumns="Quiz_Name,Associated_Module,Questions_To_Choose,Time_Allowed,Questions_Per_Page,Attempts_allowed"
	hideTheseColumns="CreatedBy,Created"
	hideBackButton="#iif(structKeyExists(URL,'hideBackButton') and url.hideBackButton is true,true,false)#"
	useInclude="false"
	sortOrder="#sortOrder#"
	queryData="#getData#"
	rowIdentityColumnName="quizDetailID"
	numRowsPerPage="#numRowsPerPage#"
	startRow="#startRow#"
	showSaveAndAddNew="true"
>