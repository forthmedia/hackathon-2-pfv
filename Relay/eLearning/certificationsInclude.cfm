<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		certificationsInclude.cfm	
Author:			NJH  
Date started:	09-09-2008
	
Description:	Used to include certification rules from the certification editor		

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
29-10-2008 			NYB 		P-SOP008
2012-04-17	 		Englex 		P-REL109 - Moved eligibilityRules back into cfif if training program activated
2013-04-26			STCR		CASE 434745 - prevent passing certificationID=0 to Certification Rule Editor
Possible enhancements:


 --->

<!--- NYB 2009-09-17 LHID2646 - moved eligibilityRules.cfm from inside cfif --->


<cfif not Isdefined("AddNew")>

	<cfinclude template="/elearning/eligibilityRules.cfm">

<!--- 	<cfif structKeyExists(url,"certificationID")>
		<cfset variables.certificationID = url.certificationID>
	<cfelseif structKeyExists(form,"certificationID")>
		<cfset variables.certificationID = form.certificationID>
	<cfelse>
		Variable certificationID does not exist.
		<CF_ABORT>
	</cfif> --->
	
	<cfif structKeyExists(form,"certificationID") and isNumeric(form.certificationID) and form.certificationID gt 0>
		<cfoutput>
		<iframe src="/elearning/certificationRules.cfm?certificationID=#htmleditformat(form.certificationID)#" width="100%" height="400px" scrolling="auto" frameborder=0></iframe>
		</cfoutput>
	</cfif>
</cfif>
