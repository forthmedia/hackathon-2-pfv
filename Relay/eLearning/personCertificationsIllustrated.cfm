<!--- �Relayware. All Rights Reserved 2014 --->
<cfparam name="personid" default="#request.relaycurrentuser.personid#">
<cfparam name="colorList" default="red,blue,orange,green,purple,yellow,pink,black,aqua">
<cfparam name="sortOrder" default="CertificationID">

<cfset CertificationData = application.com.relayElearning.GetCertificationSummaryData(entityID=personid,entityTypeID=0,sortOrder=sortOrder)>
<cfset Certifications = application.com.relayElearning.GetCertificationData(showActive="false",sortOrder=sortOrder)>

<cfset certColor = StructNew()>
<cfset certColorIndex = 0>

<cfloop query="Certifications">
	<cfset certColorIndex = certColorIndex+1>
	<cfset StructInsert(certColor,CertificationID,ListGetAt(colorList,certColorIndex))>
	<cfif certColorIndex gte ListLen(colorList)>
		<cfset certColorIndex = 0>
	</cfif>
</cfloop>

<cfset redBottom=25>
<cfset outlineBottom=50>

<cfif request.relaycurrentuser.BROWSER.TYPE eq "MSIE">
	<cfset redBottom=28>
	<cfset outlineBottom=53>
</cfif>


<cfoutput>
	<cfif isDefined("url.test")>
		
		
		<cf_head>
			<cf_title>Test Thermometers</cf_title>
		</cf_head>
			
	</cfif>
	<link href="/elearning/personCertificationsIllustratedSS.css" rel="stylesheet" type="text/css">		
</cfoutput>

<cfoutput query="CertificationData">
	<cfset segment=100/NUM_MODULES_TO_PASS>
	<cfset lineTop=0>
	<cfif len(PASS_DATE) eq 0>
		<cfset downInt = 100-Int(100*(MODULES_PASSED/NUM_MODULES_TO_PASS))>
	<cfelse>
		<cfset downInt=0>
	</cfif>
	<div id="widgetcontainer">
		<div id="containerb">
			<div id="imgcontainer">
				<cfloop index="i" from="1" to="#NUM_MODULES_TO_PASS-1#">
					<cfset lineTop=lineTop+segment>
					<div class="divline" style="top:#lineTop#px;"></div>
				</cfloop>
				<div id="empty"><img id="empty" src="/images/eLearning/thermometer_empty.gif"></div>
				<div id="coloredDiv" style="bottom:#redBottom#px;"><img id="colored" src="/images/eLearning/thermometer_#StructFind(certColor, CertificationID)#.gif" style="clip: rect(#downInt#px 32px 100px 0px)"></div>
				<div id="outlineDiv" style="bottom:#outlineBottom#px;"><img id="imgoutline" src="/images/eLearning/thermometer_outline.gif"></div>
			</div>
		</div>
		<div id="title">#htmleditformat(Title)#</div>
	</div>
</cfoutput>

<cfif isDefined("url.test")>
	<cfoutput>
		
		
	</cfoutput>
</cfif>
