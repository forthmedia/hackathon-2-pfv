<!--- 
File name:			courseModules.cfm	
Author:				AJC Englex
Date started:		01/05/2012
	
Description:		Based on moduleSetModules.cfm	

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed

2012-07-12	PJP	Case#429052 added new table trngModuleCourse, altered queries to use new table
2012/11/12 	YMA 	CASE:431767 Deal with Null ModuleCode
2012/11/19	NJH		CASE 432093 - only delete from table where modules don't exist.. don't do a delete and then an insert of all new records.
2013/02/21	NJH		Case 433922 - look at form.courseID rather than url.courseID. ALso add modules on an initial course add.
Possible enhancements:


 --->

<cfif not Isdefined("AddNew")>

<!--- 	<cfif structKeyExists(form,"courseID")>
		<cfset variables.courseID = form.courseID>
	<cfelseif structKeyExists(url,"courseID")>
		<cfset variables.courseID = url.courseID>
	<cfelse>
		Variable courseID does not exist.
		<CF_ABORT>
	</cfif> --->

	<cfif structKeyExists(form,"Update") or structKeyExists(form,"Added")>
		<cftransaction>
			<cftry>
				<!--- PJP 2012-07-11 Case 429052 added trngModuleCourse, first delete all records by courseid 
					NJH 2012/11/21 Case 432093 - alter query for modRegister purposes.
				---->
				<cfquery name="deleteExistingModulesFromModuleSetModule" datasource="#application.siteDataSource#">
					update trngModuleCourse
						set lastUpdated=getDate(),
							lastUpdatedBy=<cfqueryparam cfsqltype="cf_sql_integer" value="#request.relayCurrentUser.userGroupID#">,
							lastUpdatedByPerson=<cfqueryparam cfsqltype="cf_sql_integer" value="#request.relayCurrentUser.personID#">
					where courseID = <cfqueryparam cfsqltype="cf_sql_integer" value="#form.courseID#">
						<cfif structKeyExists(form,"SelectedModuleIDs")>
							and moduleID not in (<cf_queryparam cfsqltype="cf_sql_integer" value="#form.SelectedModuleIDs#" list="true">)
						</cfif>
						
					delete trngModuleCourse where courseID = <cfqueryparam cfsqltype="cf_sql_integer" value="#form.courseID#">
						<cfif structKeyExists(form,"SelectedModuleIDs")>
							and moduleID not in (<cf_queryparam cfsqltype="cf_sql_integer" value="#form.SelectedModuleIDs#" list="true">)
						</cfif>
				</cfquery>
				<!--- PJP 2012-07-11 Case 429052 added trngModuleCourse, Insert ---->
				<cfif structKeyExists(form,"SelectedModuleIDs")>
						<cfquery name="insertModulesIntoCourse" datasource="#application.siteDataSource#">
							insert into trngModuleCourse (moduleID,courseID,createdby,created,lastupdated,lastUpdatedBy,lastUpdatedByPerson)
					<cfloop list="#form.SelectedModuleIDs#" index="moduleID">
								select	
									<cf_queryparam cfsqltype="cf_sql_integer" value="#moduleID#">,
									<cf_queryparam cfsqltype="cf_sql_integer" value="#form.courseID#">,
									#request.relayCurrentUser.usergroupid#,
									GetDate(),
									GetDate(),
									#request.relayCurrentUser.usergroupid#,
									#request.relayCurrentUser.personID#
								where not exists (select 1 from trngModuleCourse where moduleId=<cf_queryparam cfsqltype="cf_sql_integer" value="#moduleID#"> and courseID=<cf_queryparam cfsqltype="cf_sql_integer" value="#form.courseID#">)
									<cfif moduleID neq listlast(form.SelectedModuleIDs)>
										union all
									</cfif>
							</cfloop>
						</cfquery>
				</cfif>
				<cftransaction action="commit"/>
				<cfcatch type="Database">
					<cftransaction action="rollback"/>
					<cfoutput>There was an error updating the modules in the module set</cfoutput>
				</cfcatch>
			</cftry>
		</cftransaction>
	</cfif>
	
	<!--- YMA 2012/11/12 CASE:431767 - added isnull to module code select.  This was forcing the rest of the default tranlsation to return null when modulecode was null. --->	
	<cfsavecontent variable="select_sql">
		<cfoutput>
			trngModule.moduleID, 
			isnull(moduleCode,'')+': '+ CASE WHEN len(title_defaultTranslation) > 100
			THEN left(title_defaultTranslation,30) +  ' ... ' + right(title_defaultTranslation,30)
			ELSE title_defaultTranslation END AS Title,
			title_defaultTranslation
		</cfoutput>
	</cfsavecontent>
	
	<!--- NJH 2013/11/05 - Case 437459 - no need to get modules not in course as this is done now via a webservice --->
	<cfquery name="getModulesNotInCourse" datasource="#application.siteDataSource#">
		select #preserveSingleQuotes(select_sql)# from trngModule where 1=0
		<!--- SELECT #preserveSingleQuotes(select_sql)#
		FROM trngModule
		WHERE trngModule.availability <> 0
		<!--- PJP 2012-07-11 Case 429052 added not exists to remove Modules not in course from trngModuleCourse ---->
		and not exists
			(select tmc.moduleid from trngModuleCourse tmc where tmc.courseID = #form.courseID# and tmc.moduleid = trngModule.moduleid)

		<!--- START: 2012-04-16 - Englex - P-REL109 - Add training program --->
		<cfif application.com.settings.getSetting("elearning.useTrainingProgram")>
			<cfset cTrainingProgramID = application.com.relayElearning.GetTrainingCoursesData(courseID=form.courseID).trainingprogramID>
			<cfif cTrainingProgramID neq 0 and cTrainingProgramID neq "">
				and trainingProgramID = <cf_queryparam value="#cTrainingProgramID#" CFSQLType="CF_SQL_INTEGER" />
			</cfif>
		</cfif>
		<!--- END: 2012-04-16 - Englex - P-REL109 - Add training program --->
		order by moduleCode,title_defaultTranslation --->
	</cfquery>
	
	<!--- NJH 2009/02/24 Bug Fix All Sites Issue 1880 --->
	<cfquery name="getModulesInCourse" datasource="#application.siteDataSource#">
		SELECT #preserveSingleQuotes(select_sql)#
		FROM trngModule inner join trngModuleCourse on trngModuleCourse.moduleID = trngModule.moduleID
		<!--- PJP 2012-07-11 Case 429052 changed form trngModule to new table trngModuleCourse ---->
		WHERE trngModuleCourse.courseID = <cf_queryparam cfsqltype="cf_sql_integer" value="#form.courseID#">
		and trngModule.availability <> 0
		order by moduleCode,title_defaultTranslation
	</cfquery>
	<!--- END - MS - 15-Feb-2011 --->
	
	<cfset cTrainingProgramID = 0>
	<cfif application.com.settings.getSetting("elearning.useTrainingProgram")>
		<cfset cTrainingProgramID = application.com.relayElearning.GetTrainingCoursesData(courseID=form.courseID).trainingprogramID>
	</cfif>
		
	<!--- NJH 2013/11/05 - Case 437459 - query to create the module filter - get the first letter of the module 
	2013/11/13 GCC changed to include modulecode as primary filter if it exists--->
	<!--- NJH 2013/11/21 Case 437964 - filter to show only available modules --->	
	<cfquery name="getFiltersForModuleListing" datasource="#application.siteDataSource#">
		select '' as value, 'phr_elearning_any_letter' as display
		union
		select distinct left(ltrim(rtrim(isnull(moduleCode,title_defaultTranslation))),1) as value, left(ltrim(rtrim(isnull(moduleCode,title_defaultTranslation))),1) as display from trngModule 
		where availability = 1
		<cfif cTrainingProgramID neq 0 and cTrainingProgramID neq "">
			and trainingProgramID = <cf_queryparam value="#cTrainingProgramID#" CFSQLType="CF_SQL_INTEGER" />
		</cfif>		
		order by value
	</cfquery>
	<cfset bindFunction = 'cfc:webservices.relayElearningWS.getModulesForSelect({SelectedModuleIDs_filter},{SelectedModuleIDs},#cTrainingProgramID#)'>
	<cfset filterQuery = getFiltersForModuleListing>
			
	<!--- NJH 2013/11/21 Case 437964 - show the filter label --->		
	<CF_TwoSelectsComboQuery
		    NAME="AllModules"
		    NAME2="SelectedModuleIDs"
		    SIZE="20"
		    WIDTH="400"
		    FORCEWIDTH="400"				    
			QUERY1="getModulesInCourse"
			QUERY2="getModulesNotInCourse"
			VALUE="moduleID"
			DISPLAY="Title"
			TITLE="title_defaultTranslation"
		    CAPTION="<FONT SIZE=-1><B>phr_elearning_PotentialModules:</B></FONT>"
		    CAPTION2="<FONT SIZE=-1><B>phr_elearning_CurrentModules:</B></FONT>"
			UP="No" 
			DOWN="No"
			FORMNAME="editorForm"
			bindFunction=#bindFunction#
			filterQuery=#filterQuery#
			filterLabel="phr_elearning_moduleFilterLabel"
			>
<cfelse>
	<cf_includejavascriptonce template = "/javascript/twoselectscombo.js">
</cfif>
