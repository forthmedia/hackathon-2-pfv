<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		questionPools.cfm
Author:			STCR
Date started:	2012-05-01

Description:	P-REL109 Taining Manager Enhancement Project Phase 1 - Question Pools

Amendment History:

Date (YYYY-MM-DD)	Initials 	What was changed
2012-07-24			IH			Case 429695	Pass initial sort order to CF_listAndEditor
2012-09-04			STCR		CASE 430069 Question Pools usability. Set QuestionPoolRef as mandatory.
Possible enhancements:


 --->

<cfparam name="getData" type="string" default="foo" />
<cfparam name="sortOrder" default="QuestionPoolRef ASC" />
<cfparam name="numRowsPerPage" type="numeric" default="100" />
<cfparam name="startRow" type="numeric" default="1" />

<cfscript>
 	if(structKeyExists(URL, "frmRowIdentity")){
		for(x = 1; x lte listLen(URL.frmRowIdentity); x = x + 1){
			application.com.relayQuiz.deleteQuestionPool(questionPoolID=listGetAt(URL.frmRowIdentity,x));
		}
	}
 </cfscript>

<!--- save the content for the xml to define the editor --->

<cfsavecontent variable="xmlSource">
	<cfoutput>
	<editors>
		<editor id="469" name="thisEditor" entity="QuizQuestionPool" title="phr_eLearning_QuestionPool_Title">
			<field name="QuestionPoolID" label="phr_eLearning_QuestionPoolID" description="Unique ID." control="html"></field>
			<field name="QuestionPoolRef" label="phr_eLearning_QuestionPoolRef" description="Question Pool short reference" required="true"></field>
			<field name="" control="includetemplate" template="/eLearning/questionPoolsInclude.cfm" label=""/>
		</editor>
	</editors>
	</cfoutput>
</cfsavecontent>

<cfif not structKeyExists(URL,"editor")>
	<cfset getData = application.com.relayQuiz.getQuizQuestionPoolList(sortOrder=sortOrder)>
</cfif>

<cfset functionType="QuestionPool" />
<cfinclude template="/elearning/elearningFunctions.cfm" />

<cf_listAndEditor
	xmlSource="#xmlSource#"
	cfmlCallerName="questionPools"
	keyColumnList="QuestionPoolRef,Manage_Questions"
	keyColumnKeyList="QuestionPoolID,QuestionPoolID"
	keyColumnURLList="questionPools.cfm?editor=yes&QuestionPoolID=,questions.cfm?QuestionPoolID="
	showTheseColumns="QuestionPoolRef,QuestionPoolID,Manage_Questions"
	FilterSelectFieldList=""
	FilterSelectFieldList2=""
	useInclude="false"
	queryData="#getData#"
	numRowsPerPage="#numRowsPerPage#"
	startRow="#startRow#"
	sortOrder="#sortOrder#"
	backForm="questionPools.cfm"
	rowIdentityColumnName="questionPoolID"
	functionListQuery="#comTableFunction.qFunctionList#"
	pageTitle="eLearning_QuestionPool_Title"
	columnTranslation="true"
	columnTranslationPrefix="phr_elearning_"
>