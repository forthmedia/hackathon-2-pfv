<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			Certifications.cfm
Author:				NJH
Date started:		2008/08/20
Description:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2008-10-22			NYB 		turned title & description into phrasetext fields
2008-10-22			NYB 		P-SOP007
2008-12-10			AJC			Add ability to Activate a certification
2010/11/08			WAB			Modified additionalSubmitText 'cause was failing. (added document. infront of editorForm)
2011-03-02			NYB 		LHID5772: fixed back link
2012-04-16	 		Englex 		P-REL109 - Add training program
2012/10/15			NJH			social CR - added share options
2013-09-11 			NYB 		Case 436307 - added ",incInactiveRules=false" to function call
2013-11-20 			NYB 		Case 437839 added maxLength
2014-01-15 			NYB 		Case 438542 remove dups from list of ticked certificates
2014-01-16 			PPB 		Case 438542 added timeout
2014-10-02			RPW			CORE-760 Error message when trying to view Certifications on  Training
2014-10-02			RPW			CORE-705 Error while trying to "Set up modules" in Training area
2014-10-03			RPW			CORE-760 Error message when trying to view Certifications on  Training - Moved session.selectedTrainingProgramID to trainingProgramCheck.cfm
2015-08-14			NJH			Case 445556  - htmlEditFormat the various name fields in the openNewTab calls
2015-12-20			PYW			P-TAT006 BRD 31. Add Validity field
2016-05-18			WAB			BF-714 TrainingProgram drop down. Remove reference to get ExtendedMetaData.sql and replace with func:...getTrainingProgram()
2016-05-18			WAB			BF-716 TrainingProgram drop down. Does not make sense to filter by session.selectedTrainingProgramID

Possible enhancements:

 --->

<!--- START: 2012-04-16 - Englex - P-REL109 - Add training program --->
<cfif application.com.settings.getSetting("elearning.useTrainingProgram")>
	<cfinclude template="trainingProgramCheck.cfm">
</cfif>
<!--- END: 2012-04-16 - Englex - P-REL109 - Add training program --->

<cf_title>Certifications</cf_title>

<cfparam name="getData" type="string" default="foo">
<cfparam name="sortOrder" default="CertificationCode,CertificationTitle">
<cfparam name="url.action" default="activate">
<cfparam name="url.active" default="1">
<cfparam name="variables.showTheseColumns" default="CertificationCode,CertificationTitle,certificationType,ModuleSet,ModuleTitle,QuizTitle,CoursewareTitle,DownloadFile,Country"/>
<cfparam name="numRowsPerPage" type="numeric" default="10">
<cfparam name="startRow" type="numeric" default="1">
<cfparam name="numRowsPerPage" type="numeric" default="10">
<cfparam name="startRow" type="numeric" default="1">

<cfsetting requesttimeout="900"> 	<!--- 2014-01-16 PPB Case 438542 added timeout (inactivating certifications can be time-consuming) --->

<cfscript>
 	// this changes the active status of a certification
 	if (structKeyExists(FORM, "frmRowIdentity")){
 	 	FORM.frmRowIdentity = application.com.globalfunctions.RemoveListDuplicates(FORM.frmRowIdentity);			// 2014-01-15 NYB Case 438542 if the user has ticked many rows for the same certificate, remove dups
	// START: 2008-12-10 AJC Add ability to Activate a certification
		if (url.action eq "deactivate")
		{
			for(x = 1; x lte listLen(FORM.frmRowIdentity); x = x + 1){
				application.com.relayElearning.inactivateCertification(certificationID=listGetAt(FORM.frmRowIdentity,x));
		}
	}
		else
		{
			for(x = 1; x lte listLen(FORM.frmRowIdentity); x = x + 1){
				application.com.relayElearning.activateCertification(certificationID=listGetAt(FORM.frmRowIdentity,x));
			}
		}
	}
	// END: 2008-12-10 AJC Add ability to Activate a certification
 </cfscript>

<!--- save the content for the xml to define the editor --->
<cfset SortOrderList="select 1 as value,1 as display">
<cfloop index='i' from='2' to='99'>
	<cfset SortOrderList="#SortOrderList# union select #i# as value,#i# as display">
</cfloop>

<cfsavecontent variable="xmlSource">
	<cfoutput>
	<editors>
		<editor id="232" name="thisEditor" entity="trngCertification" title="Certification Editor">
			<field name="certificationID" label="phr_elearning_certificationID" description="This records unique ID." control="html"></field>
			<field name="Code" label="phr_elearning_certificationCode" description="The certification code." required="true"></field>
			<field name="" CONTROL="TRANSLATION" label="phr_elearning_certificationTitlePhrase" Parameters="phraseTextID=Title" required="true"></field>
			<field name="" CONTROL="TRANSLATION" label="phr_elearning_certificationDescriptionPhrase" description=""  Parameters="phraseTextID=Description,displayAs=TextArea"></field>
			<!--- START: 2012-04-16 - Englex - P-REL109 - Add training program --->
			<cfif application.com.settings.getSetting("elearning.useTrainingProgram")>
			<field name="TrainingProgramID" label="phr_elearning_trainingProgram" description="The training program the specialisation is valid for." control="select" query="func:com.relayElearning.getTrainingProgram(forDropDown=1)"></field>
			</cfif>
			<!--- END: 2012-04-16 - Englex - P-REL109 - Add training program --->
			<field name="certificationTypeID" label="phr_elearning_certificationType" description="The type of certification" control="select"
					query="select certificationTypeID as value,description as display from trngCertificationType order by description"></field>
			<field name="Duration" label="phr_elearning_certificationDurationInMonths" description="The certification duration (in months)." control="numeric" maxLength="4"></field><!--- 2013-11-20 NYB Case 437839 added maxLength --->
			<!--- <field name="active" label="Active" description="Is the certification active?" control="YorN"></field> --->
			<!--- 2015-12-20 PYW P-TAT006 BRD 31. Add Validity field --->
			<field name="validity" label="phr_elearning_certificationValidity" description="The certification validity duration (in months)." control="numeric" maxLength="4"></field>
			<field name="CountryID" label="phr_elearning_certificationCountry" description="The country the certification is valid in." control="select" query="select countryID as value,countryDescription as display, 1 as orderIndex from country where isoCode is null and countryID in (#request.relayCurrentUser.regionListSomeRights#) union select  ''as countryID,'------------------------------------------' as display, 2 as orderIndex union select countryID as value,countryDescription as display, 3 as orderIndex from country where isoCode is not null and countryID in (#request.relayCurrentUser.countryList#) order by orderIndex,countryDescription"></field>
			<cfset shareCertification = true>
				<cfif not application.com.settings.getSetting("socialMedia.enableSocialMedia")>
					<cfset shareCertification = false>
				</cfif>
			<field name="share" label="phr_elearning_certificationShare" description="Allow social updates for this certification." control="YorN" <cfif not shareCertification>readonly="true"</cfif> <cfif not shareCertification>noteText="Social Media has not been enabled."</cfif>></field>
			<field name="SortOrder" label="phr_elearning_SortOrder" description="The display order of certifications." control="select" query="#SortOrderList#"></field>
			<field name="" control="includeTemplate" template="/eLearning/certificationsInclude.cfm" label="phr_elearning_PotentialPrerequisites" spanCols="true" valueAlign="middle"/>
            <group label="System Fields" name="systemFields">
				<field name="sysCreated"></field>
				<field name="sysLastUpdated"></field>
				<field name="LastUpdatedbyPerson" label="phr_editor_lastUpdated" description="" control="hidden"></field>
			</group>
		</editor>
	</editors>
	</cfoutput>
</cfsavecontent>


<cfif not structKeyExists(URL,"editor")>
	<cf_includeJavascriptOnce template = "/javascript/fileDownload.js">
	<!--- 2013-09-11 NYB Case 436307 - added ",incInactiveRules=false" to function call: --->
	<cfset getData = application.com.relayElearning.GetCertificationSummary(sortOrder=sortOrder,active=url.active,incInactiveRules=false)>
</cfif>

<cfinclude template="/elearning/elearningFunctions.cfm">
<!--- START: 2008-12-10 AJC Add ability to Activate a certification --->
<cfoutput>
<script>
	function submitAction(action)
	{
		document.frmBogusForm.action='#jsStringFormat(SCRIPT_NAME)#?action='+action;
		document.frmBogusForm.method='post';
		document.frmBogusForm.submit();
	}
</script>
</cfoutput>
<!--- END: 2008-12-10 AJC Add ability to Activate a certification --->

<!--- START: 2012-05-01 - Englex - P-REL109 - Add training program --->
<cfif application.com.settings.getSetting("elearning.useTrainingProgram")>
	<cfset variables.showTheseColumns = "CertificationCode,CertificationTitle,certificationType,ModuleSet,ModuleTitle,QuizTitle,CoursewareTitle,DownloadFile,Country,TrainingProgram"/>
</cfif>
<!--- END: 2012-05-01 - Englex - P-REL109 - Add training program --->

<cfquery name="qFileTypeGroupID" datasource="#application.siteDataSource#" cachedwithin="#createTimeSpan(0,0,1,0)#">
	SELECT filetypegroupid
	FROM filetypegroup
	WHERE [heading] =  <cf_queryparam value="ELearning_CourseWare" CFSQLTYPE="CF_SQL_VARCHAR" >
</cfquery>

<cfset fileTypeGroupID = qFileTypeGroupID.filetypegroupid>

<cf_listAndEditor
	xmlSource="#xmlSource#"
	cfmlCallerName="certifications"
	keyColumnList="CertificationTitle,Moduleset,ModuleTitle,QuizTitle,CoursewareTitle,DownloadFile"
	keyColumnKeyList=" , , , , , "
	keyColumnURLList=" , , , , , "
	keyColumnOnClickList="javascript:openNewTab('##jsStringFormat(htmlEditFormat(CertificationTitle))##'*comma'##jsStringFormat(htmlEditFormat(CertificationTitle))##'*comma'/eLearning/certifications.cfm?editor=yes&hideBackButton=true&certificationID=##jsStringFormat(certificationID)##'*comma{reuseTab:true*commaiconClass:'elearning'});return false;,javascript:openNewTab('##jsStringFormat(htmlEditFormat(Moduleset))##'*comma'##jsStringFormat(htmlEditFormat(Moduleset))##'*comma'/eLearning/moduleSets.cfm?editor=yes&hideBackButton=true&moduleSetID=##jsStringFormat(moduleSetID)##'*comma{reuseTab:true*commaiconClass:'elearning'});return false;,javascript:openNewTab('##jsStringFormat(htmlEditFormat(ModuleTitle))##'*comma'##jsStringFormat(htmlEditFormat(ModuleTitle))##'*comma'/eLearning/modules.cfm?editor=yes&hideBackButton=true&moduleID=##jsStringFormat(moduleID)##'*comma{reuseTab:true*commaiconClass:'elearning'});return false;,javascript:openNewTab('##jsStringFormat(htmlEditFormat(QuizTitle))##'*comma'##jsStringFormat(htmlEditFormat(QuizTitle))##'*comma'/eLearning/quizSetup.cfm?editor=yes&hideBackButton=true&QuizDetailID=##jsStringFormat(QuizDetailID)##'*comma{reuseTab:true*commaiconClass:'elearning'});return false;,javascript:openNewTab('##jsStringFormat(htmlEditFormat(CoursewareTitle))##'*comma'##jsStringFormat(htmlEditFormat(CoursewareTitle))##'*comma'/fileManagement/files.cfm?fileID=##jsStringFormat(courseWareID)##&editor=yes&showSaveAndAddNew=false'*comma{reuseTab:true*commaiconClass:'elearning'});return false;,javascript:getFile(##jsStringFormat(courseWareID)##);return false;"
	showSaveAndReturn = "#iif((structKeyExists(URL,'hideBackButton') and url.hideBackButton is true),false,true)#"
	showTheseColumns="#variables.showTheseColumns#"
	FilterSelectFieldList="CertificationCode,CertificationTitle,certificationType,ModuleSet,ModuleTitle,QuizTitle,CoursewareTitle,Country"
	FilterSelectFieldList2="CertificationCode,CertificationTitle,certificationType,ModuleSet,ModuleTitle,QuizTitle,CoursewareTitle,Country"
	groupByColumns=""
	<!--- includeFile = "/eLearning/certificationsInclude.cfm" --->
	useInclude="false"
	sortOrder="#sortOrder#"
	queryData="#getData#"
	numRowsPerPage="#numRowsPerPage#" <!--- NJH 2012/12/17 Case 432504 - added numRowsPerPage and startRow --->
	startRow="#startRow#"
	rowIdentityColumnName="certificationID"
    functionListQuery="#comTableFunction.qFunctionList#"
	columnTranslation="true"
	columnTranslationPrefix="phr_report_certification_"
	backform="/elearning/certifications.cfm"
	hideBackButton="#iif(structKeyExists(URL,'hideBackButton') and url.hideBackButton is true,true,false)#"
>