<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		specialisationRulesCompulsoryModulesUpdate.cfm	
Author:			PPB  
Date started:	05-08-2010
	
Description:   adds the specialisationRuleCompulsoryModule rows after a specialisationRule is added/edited	(ie what it says on the tin)		

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:

Notes:  url.specialisationRuleID is set in code by relayxmleditor to the new or the existing row id 

--->
<!--- 2013-07-01	YMA	Case 436018 specialisationRule from the url is not reliable.  Use form instead.  SelectedIDs no longer exists in form.  Now use modules. --->
<cftransaction>
	<cftry>
		<cfquery name="deleteExistingModules" datasource="#application.siteDataSource#">
			delete from specialisationRuleCompulsoryModule where specialisationRuleID = <cfqueryparam cfsqltype="cf_sql_integer" value="#form.specialisationRuleID#">
		</cfquery>	
		
		<cfif structKeyExists(form,"MODULES")>
			<cfloop list="#form.MODULES#" index="moduleID">
				<cfquery name="insertNewModules" datasource="#application.siteDataSource#">
					insert into specialisationRuleCompulsoryModule (specialisationRuleID,moduleID,createdBy,created,lastUpdatedBy,lastUpdated) 
					values 
						(<cfqueryparam cfsqltype="cf_sql_integer" value="#form.specialisationRuleID#">,
	 					 <cfqueryparam cfsqltype="cf_sql_integer" value="#moduleID#">,
						 <cfqueryparam cfsqltype="cf_sql_integer" value="#request.relayCurrentUser.userGroupID#">,
						 <cfqueryparam cfsqltype="cf_sql_timestamp" value="#request.requestTime#">,
						 <cfqueryparam cfsqltype="cf_sql_integer" value="#request.relayCurrentUser.userGroupID#">,
						 <cfqueryparam cfsqltype="cf_sql_timestamp" value="#request.requestTime#">
						)
				</cfquery>
			</cfloop>
		</cfif>
	
		<cftransaction action="commit"/>
		<cfcatch type="Database">
			<cftransaction action="rollback"/>
			<cfoutput>There was an error updating the modules for this specialisation rule</cfoutput>
		</cfcatch>
	</cftry>
</cftransaction>