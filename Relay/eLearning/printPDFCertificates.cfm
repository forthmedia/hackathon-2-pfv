<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		printPDFCertificates.cfm
Author:			AJC
Date started:	2012/07/17

Description:	P-REL109 Phase 2 - Code ported over from the Lenovo PS project P-LEN030 CR-018 Elearning PDF Certificate

Usage:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2015-10-13 		PPB    	P-KAS064 List Certificates For All Persons At Portal Administrators Organisation/Location; too many changes to mark individually - compare with svn if reqd
2014-05-30		SB		Table responsive structure.
2016/06/06		NJH		JIRA PROD2016-166 - bring in Pete's work to core.. changed slightly so only one parameter in relay tag.

Enhancements still to do:


 --->
<cf_param name="ListForAllPersonsAtTrainingContactsOrgOrLoc" label="List For All Persons at Training Contact's Organization or Location" description="Training Contacts can view and Print Certificates for others at organization or location" validValues="Organization,Location" default=""/>

<cfset moduleNamePhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "trngmodule", phraseTextID = "title",baseTableAlias="tm")>
<cfset courseNamePhraseQuerySnippets = application.com.relaytranslations.getPhrasePointerQuerySnippets(entityType = "trngCourse", phraseTextID = "title",baseTableAlias="tmc")>

<cfset ListForAllPersonsAtCurrentUsersOrgOrLoc = "">
<cfset showPersonName = false>
<cfset uniqueKey = "">
<cfset ListForAllPersonsAtTrainingContactsOrgOrLoc = ListForAllPersonsAtTrainingContactsOrgOrLoc eq "organization"?"organisation":ListForAllPersonsAtTrainingContactsOrgOrLoc>

<cfif ListForAllPersonsAtTrainingContactsOrgOrLoc neq "" and application.com.relayElearning.isPersonTrainingContact()>
	<cfset showPersonName = true>
	<cfset ListForAllPersonsAtCurrentUsersOrgOrLoc = ListForAllPersonsAtTrainingContactsOrgOrLoc>
	<cfset uniqueKey = application.com.relayEntity.getEntityType(entityTypeID=ListForAllPersonsAtCurrentUsersOrgOrLoc).uniqueKey>
</cfif>

<cfquery name="passedQuizes">
	select	distinct
				qt.QuizTakenID,
				qt.Completed,
                qt.personId,
				tm.modulecode,
                /* Case 448830 */
                tm.moduleID,
                tmc.courseId,/*DCC added for scorm quizzes*/
				qt.personId,
				<cfif showPersonName>
				p.fullname AS PersonName,
				</cfif>
				#preserveSingleQuotes(moduleNamePhraseQuerySnippets.select)# as module_title,
				#preserveSingleQuotes(courseNamePhraseQuerySnippets.select)# as course_title,
				qt.passmark,
				qt.score
	from        dbo.QuizTaken qt
			inner join dbo.trngUserModuleProgress tump on tump.userModuleProgressID=qt.userModuleProgressID
			inner join dbo.trngModule tm on tump.moduleID = tm.moduleID
			#preserveSingleQuotes(moduleNamePhraseQuerySnippets.join)#
			<cfif showPersonName>
			inner join person p WITH (noLock) ON p.personId = qt.personId
				<cfif ListForAllPersonsAtCurrentUsersOrgOrLoc neq "">
					and p.#uniqueKey# = <cf_queryparam cfsqltype="cf_sql_integer" value="#request.relaycurrentuser[uniqueKey]#">
				</cfif>
			</cfif>
			left join dbo.trngModuleCourse tmc on tmc.moduleID = tm.moduleID
			#preserveSingleQuotes(courseNamePhraseQuerySnippets.join)#
	where qt.quizStatus='pass'
		<cfif not showPersonName>
		and   qt.personid=<cf_queryparam cfsqltype="cf_sql_integer" value="#request.relaycurrentuser.personid#">
		</cfif>
</cfquery>

<cfquery name="passedCertifications">
	select * from (
	select
		v.CERTIFICATIONID,
		v.CERTIFICATIONTYPEID,
		v.CODE,
		v.DESCRIPTION,
		v.MODULES_PASSED,
		v.NUM_MODULES_TO_PASS,
		v.PERSONID,
		<cfif showPersonName>
		p.fullname AS PersonName,
		</cfif>
		v.PASS_DATE,
		v.PERSONCERTIFICATIONID,
		v.TITLE,
		v.STATUS
		from vTrngCertificationSummary v
		<cfif showPersonName>
		inner join person p WITH (noLock) ON p.personId = v.personId
			<cfif ListForAllPersonsAtCurrentUsersOrgOrLoc neq "">
				and p.#uniqueKey# = <cf_queryparam cfsqltype="cf_sql_integer" value="#request.relaycurrentuser[uniqueKey]#">
			</cfif>
		</cfif>
		where v.statusTextID = 'Passed'
		<cfif not showPersonName>
		and v.personID = <cf_queryparam cfsqltype="cf_sql_integer" value="#request.relaycurrentuser.personid#">
		</cfif>
	) as base where 1=1
	order by CODE,DESCRIPTION,PASS_DATE,STATUS
</cfquery>

<cf_includeJavascriptOnce template="/javascript/openWin.js">

<cf_head>
	<script>
		function openPDF(link) {
			openWin(link,'Certificate','width=600,height=500,toolbar=0,location=0,directories=0,status=1,menuBar=0,scrollBars=1,resizable=1');
		}
	</script>
</cf_head>
<!--id="printPdfCertificates">-->
<table data-role="table" data-mode="reflow" class="responsiveTable table table-striped table-bordered table-condensed">
	<thead>
	<tr>
		<cfif showPersonName>
			<th>phr_elearning_PersonName</th>
		</cfif>
		<th>phr_elearning_module_name</th>
			<th>phr_elearning_course_name</th>
			<th>phr_elearning_pass_marks</th>
			<th>phr_elearning_score</th>
			<th>phr_elearning_pass_date</th>
		<th></th>
	</tr>
	</thead>
	<tbody>
	<cfset rowClass="">
	<cfloop query="passedQuizes">
		<cfset rowClass=rowClass eq "oddRow"?"evenRow":"oddRow">

		<cfoutput>
			<tr class="#rowClass#">
				<cfif showPersonName>
					<td>#PersonName#</td>
				</cfif>
				<td class="module first">#module_title#</td>
				<td class="course tdCollapse">#course_title#</td>
				<td class="passmark tdCollapse">#trim(passmark)#</td>
				<td class="score tdCollapse">#Score#</td>
				<td class="dateCompleted tdCollapse">#LSDateFormat(Completed, "dd mmm yyyy")#</td>
				  <!--- Case 448830 --->
				<cfset pdfLink=application.com.security.encryptURL('/elearning/certificate.cfm?quiztakenid=#passedQuizes.QuizTakenID#&personID=#passedQuizes.personid#&courseID=#passedQuizes.courseID#')>
				<td class="pdfFile tdCollapse"><a href="##" onclick="Javascript:openPDF('#pdfLink#');return false;" class="btn btn-primary btn-sm printPDFLink">phr_elearning_Print</a></td>
			</tr>
		</cfoutput>
	</cfloop>
	</tbody>
</table>


<table>
	<tr><td>&nbsp;</td></tr>
</table>

<table data-role="table" data-mode="reflow" class="responsiveTable table table-striped table-bordered table-condensed withBorder">
	<thead>
	<tr>
		<cfif showPersonName>
			<th>phr_elearning_PersonName</th>
	    </cfif>
		<th>phr_elearning_certification_name</th>
		<th>phr_elearning_Number_of_Modules_Passed</th>
		<th width="14%">phr_elearning_pass_date</th>
		<th></th>
	</tr>
	</thead>
	<tbody>
	<cfset rowClass="">
	<cfloop query="passedCertifications">
		<cfset rowClass=rowClass eq "oddRow"?"evenRow":"oddRow">

		<cfoutput>
		<tr align="center" class="#rowClass#">
			<cfif showPersonName>
				<td>#PersonName#</td>
			</cfif>
				<td>#passedCertifications.TITLE#</td>
				<td>#passedCertifications.MODULES_PASSED#</td>
				<td>#LSDateFormat(passedCertifications.PASS_DATE, "dd mmm yyyy")#</td>
			<cfset pdfLink=application.com.security.encryptURL('/elearning/certificate.cfm?CertificationID=#passedCertifications.CERTIFICATIONID#&personID=#passedCertifications.personid#')>
				<td><a href="##" onclick="Javascript:openPDF('#pdfLink#');return false;">phr_elearning_Print</a></td>
		</tr>
		</cfoutput>
	</cfloop>
	<tbody>
</table>