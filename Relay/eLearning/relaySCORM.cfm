<!--- �Relayware. All Rights Reserved 2014 --->
<!---
2013-06-14	NYB Case 435360 moved function definition out of jquery 
2015-06-15 	WAB Implemented SCORMCommit, both supporting actual commit requests and doing an autocommit after x seconds
				Also a few modification to the javascript
--->


<cfparam name="session.elearning.scorm.SCORMSessionKey" type="string" default="#createuuid()#" />

<cfparam name="debug" default="false">
<cfif debug and request.relayCurrentUser.isInternal>
	<cfset debug = true>
<cfelse>
	<cfset debug = false>
</cfif>

<cf_head>        
		
        <script type="text/javascript">
			
            var dataStore = {
                'cmi' : {
                    '_version': {
                        'access': 'ro', 
                        'value': '1.0.0' 
						<!--- Could be useful to make this a setting - since the characters after the second dot are LMS-defined --->
                    },
                    'completion_status': {
                        'access': 'rw',
                        'value': 'unknown'
                    },
                    'completion_threshold': {
                        'access': 'ro',
                        'value': null <!--- will be null unless extracted from the manifest --->
                    },
                    'credit': {
                        'access': 'ro',
                        'value': 'credit'
                    },
                    'entry': {
                        'access': 'ro',
                        'value': ''
                    },
                    'exit': {
                        'access': 'wo',
                        'value': ''
                    },
                    'interactions': {
                        '_children': {
                            'access': 'ro',
                            'value': null
                        },
                        '_count': {
                            'access': 'ro',
                            'value': 0
                        }
                    },
                    'launch_data': {
                        'access': 'ro',
                        'value': ''
                    },
                    'learner_id': {
                        'access': 'ro',
                        'value': '404'
                    },
                    'learner_name': {
                        'access': 'ro',
                        'value': 'Unknown User'
                    },
                    'learner_preference': {
                        '_children': {
                            'access': 'ro',
                            'value': 'audio_level,language,delivery_speed,audio_captioning'
                        },
                        'audio_level': {
                            'access': 'rw',
                            'value': 0
                        },
                        'language': {
                            'access': 'rw',
                            'value': ''
                        },
                        'delivery_speed': {
                            'access': 'rw',
                            'value': 0
                        },
                        'audio_captioning': {
                            'access': 'rw',
                            'value': 0
                        }
                    },
                    'location': {
                        'access': 'rw',
                        'value': ''
                    },
                    'max_time_allowed': {
                        'access': 'ro',
                        'value': 0
                    },
                    'mode': {
                        'access': 'ro',
                        'value': 'normal'
                    },
                    'objectives': {
                        '_children': {
                            'access': 'ro',
                            'value': null
                        },
                        '_count': {
                            'access': 'ro',
                            'value': 0
                        }
                    },
                    'progress_measure': {
                        'access': 'rw',
                        'value': 0
                    },
                    'scaled_passing_score': { 
                        'access': 'ro',
                        'value': null 	<!--- RW not implementing IMS Simple Sequencing. REQ_74.3.3 --->
                    },
                    'score': {
                        '_children': {
                            'access': 'ro',
                            'value': 'scaled,raw,min,max'
                        },
                        'scaled': {
                            'access': 'rw',
                            'value': 0
                        },
                        'raw': {
                            'access': 'rw',
                            'value': 0
                        },
                        'min': {
                            'access': 'rw',
                            'value': 0
                        },
                        'max': {
                            'access': 'rw',
                            'value': 0
                        }
                    },
                    'session_time': {
                        'access': 'wo',
                        'value': 0
                    },
                    'success_status': {
                        'access': 'rw',
                        'value': 'unknown'
                    },
                    'suspend_data': {
                        'access': 'rw',
                        'value': ''
                    },
                    'time_limit_action': {
                        'access': 'ro',
                        'value': 'continue,no message'
                    },
                    'total_time': {
                        'access': 'ro',
                        'value': 'PT0S' /* REQ_89 */
                    }
                },
                'adl': {
                    'nav': {
                        'request': {
                            'access': 'rw',
                            'value': 'continue'
                        },
                        'request_valid': {
                            'continue': {
                                'access': 'ro',
                                'value': 'unknown'
                            },
                            'previous': {
                                'access': 'ro',
                                'value': 'unknown'
                            }
                        }
                    }
                }

            };

            var API_1484_11 = new function() {
            		var that = this;
                    this.commitOnSetValue = false;
                    this.autoCommitTimeSeconds = 2;
                    this.sessionKey = <cfoutput>'#session.elearning.scorm.SCORMSessionKey#'</cfoutput>;
                    this.lastError = 0;
                    this.debugToWindow = <cfoutput>#debug#</cfoutput>;
                    this.debugToConsole = <cfif application.testsite eq 2 and cgi.HTTP_USER_AGENT contains "Chrome" and not cgi.HTTP_USER_AGENT contains "mobile">true<cfelse>false</cfif>;
                    this.errorStrings = {
                        0: 'No Error',
                        101: 'General Exception',
                        102: 'General Initialization Failure',
                        103: 'Already Initialized',
                        104: 'Content Instance Terminated',
                        111: 'General Termination Failure',
                        112: 'Termination Before Initialization',
                        113: 'Termination After Termination',
                        122: 'Retrieve Data Before Initialization',
                        123: 'Retrieve Data After Termination',
                        132: 'Store Data Before Initialization',
                        133: 'Store Data After Termination',
                        142: 'Commit Before Initialization',
                        143: 'Commit After Termination',
                        201: 'General Argument Error',
                        301: 'General Get Error',
                        351: 'General Set Error',
                        391: 'General Commit Failure',
                        401: 'Undefined Data Model Element',
                        402: 'Unimplemented Data Model Element',
                        403: 'Data Model Element Value Not Initialized',
                        404: 'Data Model Element Is Read Only',
                        405: 'Data Model Element Is Write Only',
                        406: 'Data Model Element Type Mismatch',
                        407: 'Data Model Element Value Out Of Range',
                        408: 'Data Model Dependency Not Estabilished'
                    }

					this.dirtyKeys = {}; // used to keep track of items which need committing

                    this._webserviceCall = function(action, data, options) {
                        var page = '/webservices/callwebservice.cfc?wsdl&method=callWebService&returnformat=json&_cf_nodebug=true&webservicename=relaySCORMWS&methodname=';
						data ['SCORMSessionKey'] = this.sessionKey;
						page =  page += action;

                        <!--- 2012-09-03 STCR - synchronous calls are being used because we need to ensure that return values from initialize and getValue requests were in memory before the SCO asked for them... --->
						var defaultOptions = {sendAsync : false, method:'get'};
						options =  options || { };
						options = Object.extend (defaultOptions, options);
						
                        new Ajax.Request(page, {
                                method: options.method, 
                                parameters: data, 
                                asynchronous: options.sendAsync,
                                onComplete:  function (requestObject,JSON) {
                                    json = requestObject.responseText.evalJSON(true)
									that.logDebug ('JSON Received');
									if (this.debugToConsole) {console.log (json);};
                                    if (!json) {
                                        alert ('json not parsed');
                                        return false;
                                    } else {
                                        if (typeof json.SCORMSESSIONKEY != 'undefined') {
                                            that.sessionKey = json.SCORMSESSIONKEY;
                                            that.logDebug ('Session set: ' + that.sessionKey);
                                        }
                                        
                                        if (typeof json.CMI != 'undefined') {
                                            that.Parse(json.CMI, 'cmi');
                                        }
                                        
                                        if (typeof json.ADL != 'undefined') {
                                            that.Parse(json.ADL, 'adl');
                                        }
                                    }

                                    return true;
                                }
                        });   
                    }

                    this.Parse = function(jsonObject, prefix)
                    {
                        for (var key in jsonObject) {
                            this.SetValue(prefix + '.' + key.toLowerCase(), jsonObject[key], true);
                        }
                    }

                    this.Initialize = function() {
                        this.logDebug ('Initialize');
                        this._webserviceCall('SCORMInitialize', {});
                        return "true";
                    }

                    this.Terminate = function() {
                        this.logDebug('In Terminate Function');
                        this.logDebug('Make call synchronous call to Commit First');
						this.Commit({sendAsync : false});
                        this.logDebug('Make call to SCORM Terminate');
                        this._webserviceCall('SCORMTerminate', {});
                        <!--- TODO - now that we have moved the API instance onto the portal course page, we need to re-initialise 
							to default values after termination. 
							Otherwise the next SCO could pick up some of the previous SCO's values that are still held in memory. --->
                        return "true";
                    }

                    this.GetValue = function(key) {
                        // Break the key up through the dots
                        var splitKey = key.split('.');
                        var foundKey = dataStore;

                        for (var i = 0; i < splitKey.length; i++) {
                            if (foundKey.hasOwnProperty(splitKey[i]) == false) {
                                // Property is non existent (so not implemented)
                                this.lastError = 402;
								logDebug ('GetValue(' + key + ') {return: ""}');
                                return "";
                            } else {
                                foundKey = foundKey[splitKey[i]];
                            }
                        }

                        if (foundKey.access == 'rw' || foundKey.access == 'ro') {
                            this.logDebug('GetValue(' + key + ') {return: ' + foundKey.value + '}');
                            <!---
                            	 /*
									REQ_77 The LMS shall implement the cmi.success_status data model element. 
									REQ_77.1 The LMS shall implement the cmi.success_status data model element as read/write.
									REQ_77.2 The LMS shall implement the cmi.success_status data model element as a state 
									consisting of the following vocabulary tokens:  
									- passed 
									- failed 
									- unknown
									REQ_77.3 The LMS shall initialize the value of the cmi.success_status data model element to 
									the default value of unknown. 
									REQ_77.5.4 If no cmi.scaled_passing_score has been defined for the SCO, then the LMS shall 
									rely on the value set for the cmi.success_status data model element by the SCO and 
									return that value.  If no value was set by the SCO for the cmi.success_status data 
									model element then the LMS shall return unknown.
                            	 */
                            if(key =='cmi.success_status') {
                            	if(cmi.scaled_passing_score != null && cmi.score.scaled >= cmi.scaled_passing_score) {
                            		return 'passed';
                            	}
                            	else if(cmi.scaled_passing_score != null && cmi.score.scaled < cmi.scaled_passing_score) {
                            		return 'failed';
                            	}
                            }
							--->
							<!--- cmi.scaled_passing_score is read-only (cannot be written by setValue), and is populated from the sequencing xml in the manifest, which we are not supporting. --->
                            return foundKey.value;
                        } else {
                            if (foundKey.access == 'wo') {
                                // Property is write-only
                                this.lastError = 405;
                            } else {
                                // Unknown error happened
                                this.lastError = 301;
                            }

                            return "";
                        }
                    }

                    this.SetValue = function(key, value, isInitialize) {
                        if (typeof isInitialize == 'undefined') { 
                            isInitialize = false;
                        }
                        
                        <!--- <cfif application.testsite eq 2 and cgi.HTTP_USER_AGENT contains "Chrome" and not cgi.HTTP_USER_AGENT contains "mobile">console.log('SetValue(' + key + ', ' + value + ', ' + isInitialize + ')');</cfif> --->

                        // Break the key up through the dots
                        var splitKey = key.split('.');
                        var foundKey = dataStore;
                        var arraySet = false;

                        for (var i = 0; i < splitKey.length; i++) {
                            if (i == 1 && (splitKey[i] == 'objectives' || splitKey[i] == 'interactions')) {
                                <!--- <cfif application.testsite eq 2 and cgi.HTTP_USER_AGENT contains "Chrome" and not cgi.HTTP_USER_AGENT contains "mobile">console.log('Trying to set [' + splitKey[i] + ']');</cfif> --->
								this.logDebug('Trying to set [' + splitKey[i] + ']');
                                var currentCount = foundKey[splitKey[i]]._count.value;

                                // Increment the children count if the child doesn't exist
                                if (typeof foundKey[splitKey[i]][splitKey[i+1]] == 'undefined') {
                                    foundKey[splitKey[i]]._count.value += 1;
                                }

                                arraySet = true;
                            }

                            if (foundKey.hasOwnProperty(splitKey[i]) == false && arraySet == false) {
                                // Property is non existent (so not implemented)
                                this.lastError = 402;
                                return "false";
                            } else if (foundKey.hasOwnProperty(splitKey[i]) == false && arraySet == true) {
                                if (splitKey[i] == 'undefined') { 
                                    splitKey[i] = currentCount;
                                    key = key.replace('undefined', currentCount);
                                }

                                foundKey[splitKey[i]] = {
                                    'id': {
                                        'value': splitKey[i+1],
                                        'access': 'ro'
                                    },
                                    'value': {
                                        'access': 'rw',
                                        'value': ''
                                    }
                                };

                                foundKey = foundKey[splitKey[i]];
                                //foundKey = foundKey[splitKey[i]].value;
                                //break;
                            } else {
                                foundKey = foundKey[splitKey[i]];
                            }
                        }

                        if (arraySet == true) {
                            // We need to make some set values
                            foundKey.access = 'rw';
                            foundKey.value = '';
                        }

                        if (foundKey.access == 'rw' || foundKey.access == 'wo' || isInitialize == true) {
                            // Objects are passed in by reference, so this will work like magic
                            foundKey.value = value;
							
							// If we are initialising then we do not send values to the database
							// So only need to save if !isInitialize
							if (!isInitialize) {
								// if commitOnSetValue then save now, otherwise wait for commit or autocommit
								if (this.commitOnSetValue) {
	                                this._webserviceCall('SCORMSetValue', {'ElementName': key, 'ElementValue': value}, {sendAsynch : false});  <!--- 2012-09-03 STCR - ...for setValue requests we do not actually need to wait for a return value, so we could improve performance (and therefore ui response and usability) by setting to true to allow asynchronous calls. However when tested with the Advanced Runtime Golf SCO, not all requests would get through.  This is because of sequencing issues - we can't guarantee that the last value set will actually arrive at the db last'--->
								} else {
									// we are going to autocommit this value in autoCommitTimeSeconds by using a timeout
									// but we only want a single commit to occur at the end of batch, so cancel any existing timeout
									if (this.commitTimeOut) {
										clearTimeout (this.commitTimeOut);
									}
									var that = this
									// add this key to the collection of keys (don't use an arry 'cause only want one instance of any key)
									this.dirtyKeys[key] = '';  
									// set a new timeout to do the commit, save as a variable so can be cleared
									this.commitTimeOut = setTimeout (function () {
																					that.Commit ();
																					}
																		, this.autoCommitTimeSeconds * 1000);

								
								}
								this.logDebug('Set ' + key + ' to '+ value);
						
							}

                            return "true";
                        } else {
                            if (foundKey.access == 'ro') {
                                // Property is read-only
                                this.lastError = 404;
                            } else {
                                // Unknown error happened
                                this.lastError = 301;
                            }

                            return "false";
                        }
                    }

                    this.Commit = function(options) {

						var defaultOptions = {sendAsync : true, method: 'post'};
						options =  options || { };
						options = Object.extend (defaultOptions, options);
						
						 // make an array of the dirty keys.  If there are any dirty keys then persist to database
						var keysArray = $H(this.dirtyKeys).keys(); 
						if (keysArray.length) {
							this.dirtyKeys = {}; // reinitialise
							var argumentCollection =  Object.toJSON({elements : dataStore, keys : keysArray});
							if (this.commitTimeOut) {
								clearTimeout (this.commitTimeOut)
							}
	                        this._webserviceCall('SCORMCommit', {argumentCollection :argumentCollection}, options);
						} else {
							// this.logDebug ('No items to commit')
						}
                        return "true";
                    }

                    this.GetLastError = function() {
                    	this.logDebug ('GetLastError {return: ' + this.lastError + '}');

                        // Reset the last error again
                        var tempLastError = this.lastError;
                        this.lastError = 0;

                        return tempLastError;
                    }

                    this.GetErrorString = function(errorCode) {
						this.logDebug ('GetErrorString(' + errorCode + ')');

                        if (this.errorStrings.hasOwnProperty(errorCode) == true) {
							this.logDebug (this.errorStrings[errorCode]);
                            return this.errorStrings[errorCode];
                        } else {
							this.logDebug ('GetErrorString(' + errorCode + ') non existing');
                            return this.errorStrings[0];
                        }
                    }

                    this.GetDiagnostic = function(errorCode) {
                    	this.logDebug ('GetDiagnostic(' + errorCode + ')');
                        return "-- unknown error --";
                    }

					/* WAB 2015-06-17 added a general debugging function instead of code strewn with if statements */
					this.logDebug = function (message) {
						if (this.debugToConsole) { console.log (message);};
						if (this.debugToWindow) {$('diagnostics').innerHTML = $('diagnostics').innerHTML+'<br>' + message ;}
					}

                };

        </script>
</cf_head>