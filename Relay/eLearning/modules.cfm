		<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			modules.cfm
Author:				SWJ
Date started:			/xx/02
Description:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2008-08-26			NJH			Added credits to the xml editor
2008-10-22			NYB 		turned title & description into phrasetext fields
2009/02/10			SSS			Made the title required
2009-02-12			WAB			Added inline translation control
2009-02-23			NYB			removed "Associated_Quiz," from initial display list
2009/02/06			NJH			P-SNY047 - if we're using rewards as part of elearning, then create promotions that tie to modules. This will appear
								on the incentive statement as the module name. These promotions are created with userID of 4 so that they don't appear
								in the promotion edit list.
2009/10/15			NAS			LID 2750 changed On-line to Online as part of translating the module type
2009/10/30			NJH			LID 2819 - only show files in dropdown if they exist.
2010/08/06			NJH			RW8.3 - get values from settings
2010/09/09			AJC			P-LEN022 - Added filters and increased timeout
2010/09/15			WAB			P-LEN022 Implemented Database Level PhrasePointer Cache
2010/09/15			NAS			P-LEN022 add 'Active' to only retrieve Modules for Active Courses
2011/04/21			MS			P-LEN030 CSAT (CR-31)	Date Published added as an extra field to be recorded for modules
2011/09/06 			PPB 		LID7679 added countryID to module
2011/11/22 			PPB 		CR-LEN061 - upload an image files
2012-03-27			RMB			added mobileCompatible
2012-04-16	 		Englex 		P-REL109 - Add training program
2012-05-01			Englex		P-REL109 - Remove course dropdown - Selector now on Course screen
2012/06/18			IH			Case 428809 remove active filtering
2012-07-02 			IH 			Case 429102 link module code too in case if module title is blank
2012-07-16			STCR		P-REL109 Phase 2 - move courseware file selector above launcher filename field.
2012-07-16			STCR		P-REL109 Phase 2 - choose whether to use SCO Quiz for pass status
2012/07/26 			AJC			P-REL109 Phase 2 Module pre-requisites
2012/10/15			NJH			social CR - added share options to turn sharing off for a particular module
2012/11/14			YMA			CASE:432009 Changed listing to show module availability rather than course active.
2012/11/14			YMA			CASE:432009 Additional Request from VAB to include links to quiz and course edit pages from this report
2013/01/18			NJH			Case:433326 use getFilesApersonHasRights to get files for a person dropdown.
2013-05-21			YMA			Case 435364 Removed exception to if statement for arguments.aiccFileName neq "".  We'll only be showing sco diagnostics tools if a fileID is defined!
2013-08-20			IH			Case 436639 use fileType instead of fileTypeID to also retrieve Lenovo's customised courseware files
2013-08-20 			IH 			Case 436638 add cmSort to getData query to keep No Course Selected modules on top
2014-09-10			AXA 		Partner Cloud Phase 2 - remove AICCFilename field from form and set it implicitly by parsing the manifest file.
2014-09-15			AXA 		Partner Cloud Phase 2 - use jQuery to conditionally hide or show the AICCFilenamefield.
2014-10-02			RPW			CORE-705 Error while trying to "Set up modules" in Training area
2014-10-03			RPW			CORE-705 Error while trying to "Set up modules" in Training area - Moved session.selectedTrainingProgramID to trainingProgramCheck.cfm
2015-04-08			AHL			Case 444252 Course and Modules duplicated on Portal. Filtering Inactive course results.
2015-11-05          DCC         Case 444344 manifest/zip reader improvements
2015/08/14			NJH			Case 445556  - htmlEditFormat the various name fields in the openNewTab calls
2015-26-08			DCC			CASE 445511 Additional check for autounzip setting on media group - more poignant error message if disabled
2015-10-26			WAB			CASE 446337 Protect against there being no courseware files available
2015-10-26			DCC			Case 446337 changed getCoursewareFiles to use fileTypeGroup='ELearning_CourseWare' instead
2015/10/26			PYW			TATA-006. BRD 31. Add Module Expiry Date
2016-03-03			WAB			Moved attributes relating to recordRights manager from listAndEditor call to the XML node.
2016-05-12			atrunov     PROD2016-778 setting recordRights usage by default and configuring Elearning Visibility with multiple rules
2016-05-18			WAB			BF-714 TrainingProgram drop down. Remove reference to get ExtendedMetaData.sql and replace with func:...getTrainingProgram()
2016-05-18			WAB			BF-716 TrainingProgram drop down. Does not make sense to filter by session.selectedTrainingProgramID
2016-11-04			STCR 		PROD2016-2674 CASE 449896 translate previously-hardcoded column headings
2016-03-01			DCC			RT-317 toggle view course content based on isscorm radio selection as when scorm file will throw LMS error
Possible enhancements:

TODO: eLearning SWJ Module.cfm - add group by course, check visibility, check fileList
 --->

<!--- START: 2012-04-16 - Englex - P-REL109 - Add training program --->
<cfif application.com.settings.getSetting("elearning.useTrainingProgram")>
	<cfinclude template="trainingProgramCheck.cfm">
</cfif>
<!--- END: 2012-04-16 - Englex - P-REL109 - Add training program --->

<!--- 2012-11-19	YMA		CASE:432010 changed report to filter to available / unavailable modules rather than courses --->
<cfparam name="url.available" type="Boolean" default="1">

<cf_title>Modules</cf_title>
<cfsetting requesttimeout="600">

<cfquery name="getNumCourses" datasource="#application.siteDataSource#">
	select count(1) as courseCount from trngCourse where active=1
</cfquery>

<cfif getNumCourses.courseCount eq 0>
	<cfoutput>No courses have yet been set up. Please set up your courses before creating modules.</cfoutput>
<cfelse>

	<cfparam name="getData" type="string" default="foo">
	<cfparam name="sortOrder" default="Title_Of_Course">
	<cfparam name="groupBy" default="Title_Of_Course">
	<cfparam name="numRowsPerPage" type="numeric" default="100">
	<cfparam name="startRow" type="numeric" default="1">
	<cfparam name="showTheseColumns" type="string" default="Module_Code,Title_Of_Module,Title_Of_Course,Module_Type,Duration,Associated_Quiz,mobileCompatible,SortOrder">

	<cfset xmlSource = "">
	<cfset useIncentives = application.com.settings.getSetting("elearning.useIncentives")>
	<cfset singleUserGroupType = application.com.settings.getSetting("elearning.singleUserGroupType")>
	<cfset userGroupTypes = application.com.settings.getSetting("elearning.userGroupTypes")>

	<cfif structKeyExists(URL,"editor")>
		<!--- BEGIN 2015/11/03 PYW. TATA-006. BRD 31. Update Module Expiry Date --->
        <cfset expiryPeriod = application.com.settings.getSetting("elearning.moduleExpiryPeriod")>
		<cfif expiryPeriod NEQ "" and isNumeric(expiryPeriod) and expiryPeriod GT 0>
			<script>
			jQuery(document).ready(function() {
			    var currentExpiryDate = jQuery('#expiryDate').val();
			    if (currentExpiryDate == '')  {
                    setExpiryDate();
			    }
				jQuery('#publishedDate').change(function(){
					setExpiryDate();
				});
			});
			
			function setExpiryDate() {
			    var publishedDateFormat = jQuery('#publishedDate').attr('datemask');
			    var publishedDateParts = publishedDateFormat.split("/");
                var newPublishedDate = jQuery('#publishedDate').val().split("/");
                if (publishedDateParts[0] == 'dd') {
                    var newExpiryDate = new Date(newPublishedDate[2], newPublishedDate[1] - 1 + <cfoutput>#jsStringFormat(expiryPeriod)#</cfoutput>, newPublishedDate[0]);
                    jQuery('#expiryDate').val(newExpiryDate.getDate() + '/' + (newExpiryDate.getMonth() + 1) + '/' +  newExpiryDate.getFullYear());
                } else {
                    var newExpiryDate = new Date(newPublishedDate[2], newPublishedDate[0] - 1 + <cfoutput>#jsStringFormat(expiryPeriod)#</cfoutput>, newPublishedDate[1]);
                    jQuery('#expiryDate').val((newExpiryDate.getMonth() + 1) + '/' + newExpiryDate.getDate() + '/' +  newExpiryDate.getFullYear());
                }
			}

			</script>
		</cfif>
		<!--- END 2015/11/03 PYW. TATA-006. BRD 31. Update Module Expiry Date --->

		<cfset SortOrderList="select 1 as value,1 as display">
		<cfloop index='i' from='2' to='99'>
			<cfset SortOrderList="#SortOrderList# union select #i# as value,#i# as display">
		</cfloop>

		<!--- NJH 2013/01/18 Case 433326 - get files a person has rights to and that exist. Any files with a related url have a fileExists of 1 --->
		<!--- IH 2013-08-20 Case 436639 use fileType instead of fileTypeID to also retrieve Lenovo's customised courseware files --->
		<cfset getCoursewareFiles = application.com.filemanager.getFilesAPersonHasViewRightsTo(personID=request.relayCurrentUser.personID,checkFileExists=true,fileTypeGroup='ELearning_CourseWare')><!--- DCC removed filetype='Courseware' and added fileTypeGroup='ELearning_CourseWare' ---><!--- fileTypeID=application.fileTypeID.courseware --->
		
		<cfquery name="getCoursewareFiles" dbtype="query">
			select name, fileID from getCoursewareFiles where [fileExists]=1
		</cfquery>


	<!--- START 2014-09-10 AXA Partner Cloud Phase 2 - remove AICCFilename field from form and set it implicitly by parsing the manifest file.
			WAB 2015-05-13 Modified so that only updates if not already set. (The AICCFileName is
	 --->
	<cffunction name="updateLauncherFromScormManifest" returnType="struct" output='true'>
		<cfset var result = {isOK=true,message=""}>

		<cftry>
			<cfif not isDefined("moduleId")>
				<cfset moduleId = 0>
			</cfif>
			<!--- Get Zip file details and read manifest file and returns launcher filename --->
			<cfif structKeyExists(form,'isSCORM') AND form.isSCORM>


				<cfif isNumeric(form.moduleId) and form.moduleId neq 0>
					<cfset scormDetail = application.com.relayElearning.SCORMParseManifest(ModuleID=moduleId) />
					<cfif structKeyExists(scormDetail,"rw") and structKeyExists(scormDetail.rw,"launcher")  and len(trim(scormDetail.rw.launcher)) GT 0>
						<cfquery name="updateModuleLauncher" datasource="#application.siteDataSource#">
							update trngModule
								set [AICCFileName]=<cf_queryparam value="#scormDetail.rw.launcher#" cfsqltype="cf_sql_varchar">
							where 	moduleId = <cf_queryparam cfsqltype="cf_sql_integer" value="#moduleId#">
									and isNull(AICCFileName,'') = ''
						</cfquery>

					</cfif>
				</cfif>
			</cfif>
			<cfcatch>
				<cfset result.isOK = false>
				<cfset result.message = cfcatch.message>
			</cfcatch>
		</cftry>

		<cfreturn result>
	</cffunction>
	<!--- END 2014-09-10 AXA Partner Cloud Phase 2 - remove AICCFilename field from form and set it implicitly by parsing the manifest file. --->


		<cffunction name="setModuleLink" returnType="string">

			<cfset var viewModuleLink = "">

				<cfsavecontent variable="viewModuleLink">
				<cfoutput>
					<cfif arguments.moduleID neq 0>

						<cfif arguments.fileID neq "" and arguments.fileID neq 0><!--- 2013-05-21	YMA	Case 435364 Removed exception to if statement for arguments.aiccFileName neq "".  We'll only be showing sco diagnostics tools if a fileID is defined! DCC Case 444344 2015/08/05 removed application.com.fileManager.getFileAttributes(fileid=arguments.fileID).filename put back arguments.aiccFileName --->
							<cfset courseLauncherUrl = application.com.relayElearning.getCourseLauncherUrl(moduleID=arguments.moduleID,fileId=arguments.fileID,filename=arguments.aiccFileName)>
							<cfif courseLauncherUrl neq "">
								<cf_includeJavascriptOnce template="/javascript/openWin.js">
								<div class="infoblock">
									<!--- DCC RT-317 2016-03-01 Need to hide if is scorm as link launch will throw LMS error user should use diagnostics link below --->
									<div id="togglescorm" <cfif arguments.isScorm>style="display:none;"</cfif>>
									<a href="javascript:void(openWin('#courseLauncherUrl#','CoursewareContent#arguments.moduleID#','width=760,height=420,scrollbars=no,status=no,address=no'));" align="left">phr_elearning_ViewCourseContent</a>
									</div>
									<cfif arguments.isScorm>
									<br>phr_elearning_SCODiagnosticsDescription. <a href="javascript:void(openNewTab('SCO Diagnostics','SCODiagnostics','/eLearning/scoDiagnostics.cfm?moduleID=#arguments.moduleID#',{reuseTab:true,iconClass:'elearning'}));">phr_elearning_SCODiagnostics</a>
									</cfif>
								</div>
							<cfelse>
							<!--- DCC 2015/08/26 case 445511 need autounzipcheck else errormessage misleading --->
								<cfset zipcheckresult = application.com.relayelearning.checkzip(#moduleId#).autounzip>
								<div class="warningblock">
									<cfif zipcheckresult EQ 0>phr_elearning_scorm_unziperror<cfelse>phr_elearning_CourseContentFileDoesNotExist. phr_elearning_IfFileIsZipEnterLauncherFile. phr_elearning_IfUsingExternalFileAddHttpToFilename.</cfif>
								</div>
							</cfif>
						</cfif>
					</cfif>
				</cfoutput>
			</cfsavecontent>

			<cfreturn viewModuleLink>
		</cffunction>

		<cfset request.setModuleLink = setModuleLink>

		<!--- 2014/04/14	YMA	Case 435820 List All Linked Courses --->
		<cffunction name="relatedCourses" returnType="string">

			<cfset var relatedCourses = "">

			<cfsavecontent variable="relatedCourses">
				<cfoutput>
					<ul>
						<cfif arguments.moduleID neq 0>
							<cfquery name="getRelatedCourses" datasource="#application.siteDataSource#">
								select title_of_course,courseID from vTrngModules where moduleID =  <cf_queryparam value="#arguments.moduleID#" CFSQLTYPE="cf_sql_integer" >
							</cfquery>
							<cfif getRelatedCourses.recordcount gt 0>
								<cfloop query="getRelatedCourses">
									<script>
									openCourse#jsStringFormat(getRelatedCourses.courseID)# = function () {
										openNewTab('#jsStringFormat(title_of_course)#','#jsStringFormat(title_of_course)#','/eLearning/courses.cfm?editor=yes&hideBackButton=true&CourseID=#jsStringFormat(getRelatedCourses.courseID)#',{reuseTab:true,iconClass:'elearning'});
									}
									</script>
									<li><a href="##" onclick="javascript: openCourse#jsStringFormat(getRelatedCourses.courseID)#() ;return false;">#title_of_course#</a></li>
								</cfloop>
							<cfelse>
								<li>No related courses.</li>
							</cfif>
						<cfelse>
							<li>No related courses.</li>
						</cfif>
					</ul>
				</cfoutput>
			</cfsavecontent>

			<cfreturn relatedCourses>
		</cffunction>

		<cfset request.getRelatedCourses = relatedCourses>


		<cfif fileExists('#application.paths.code#/cftemplates/moduleEditorCustomCourseware.cfm')>
			<cfset defaultCoursewareTypeID = application.com.relayForms.getDefaultLookupListForField(fieldname='coursewareTypeID').lookupID>

			<!--- 2015/08/10	YMA	P-CYB002 CyberArk Phase 3 Skytap Integration
									Style toggle rows hidden to make seamless with form and set custom course launcher section hidden by default --->
			<style>
				#listAndEditorContainer tr#coursewareDetails_toggleRow {display: none;}
				#listAndEditorContainer tr#customCouseLauncher_toggleRow {display: none;}

				#listAndEditorContainer tbody#customCouseLauncher {display: none;}
			</style>
			<!--- 2015/08/10	YMA	P-CYB002 CyberArk Phase 3 Skytap Integration
									Script to toggle core course launcher section or custom course launcher section depending on Courseware File type --->
			<script>
				coursewareTypeIDChanged = function (coursewareTypeID) {
					<cfif defaultCoursewareTypeID neq "">
						<cfoutput>
						if(coursewareTypeID != #defaultCoursewareTypeID# && coursewareTypeID != ''){
						</cfoutput>
							jQuery('tbody#coursewareDetails').hide();
							jQuery('tbody#customCouseLauncher').show();
						}else{
							jQuery('tbody#coursewareDetails').show();
							jQuery('tbody#customCouseLauncher').hide();
						}
					</cfif>
				}

				jQuery(document).ready(function(){
					coursewareTypeIDChanged(jQuery('select[name="coursewareTypeID"]').val())
				});
			</script>
		</cfif>
		<!--- DCC RT-317 2016-03-01 setscorm on radio to hide reveal view course content link --->
		<script>
		//DCC RT-317 added 2016-03-01
		jQuery(document).ready(function() {
			jQuery("input[name='isSCORM']").change(function(){
			     if (jQuery(this).val() === '0') {
			       //not scorm show normal link
			       jQuery('#togglescorm').show();
			    } else if (jQuery(this).val() === '1') {
			       //is scorm hide link
			       jQuery('#togglescorm').hide();
			    }
			});
		});
		</script>
		<cfsavecontent variable="xmlSource">
			<!--- 2014-09-10 AXA Partner Cloud Phase 2 - remove AICCFilename field from form --->
			<cfoutput>
			<editors>
				<editor id="232" name="thisEditor" entity="trngModule" title="Module Editor">
					<field name="moduleID" label="phr_elearning_ModuleID" description="This records unique ID." control="html"></field>
					<field name="moduleCode" label="phr_elearning_ModuleCode" description="The module code"></field>
					<field name="" CONTROL="TRANSLATION" label="phr_elearning_ModuleTitle" Parameters="phraseTextID=Title" required="true"></field>
					<field name="" CONTROL="TRANSLATION" label="phr_elearning_ModuleDescription" description=""  Parameters="phraseTextID=Description,displayAs=TextArea"></field>
					<!--- 2015/08/10	YMA	P-CYB002 CyberArk Phase 3 Skytap Integration --->
					<cfif fileExists('#application.paths.code#/cftemplates/moduleEditorCustomCourseware.cfm')>
						<field name="coursewareTypeID" label="phr_eLearning_coursewareTypeID" type="select"  required="true" query="func:com.relayForms.getLookupList(fieldname='coursewareTypeID')"
						display="translatedItemText" value="LookupID" onchange="javascript:coursewareTypeIDChanged(jQuery(this).val())"/>
						<group name="customCouseLauncher" label="customCouseLauncher" onload="jQuery(this).hide()">
							<field name="moduleEditorCustomCourseware" label="" control="includeTemplate" template="/code/cftemplates/moduleEditorCustomCourseware.cfm"></field>
						</group>
					</cfif>
					<group name="coursewareDetails" label="coursewareDetails">
					<field name="isSCORM" label="phr_eLearning_isSCORM_Label" description="phr_eLearning_isSCORM_Description" control="YorN"></field>
					<cfif isdefined("add") and add is "yes"><field name="courseID" description="This records course ID." default="0" control="hidden"></field></cfif>
					<lineItem label="phr_elearning_Filename">
						<field name="fileID" label="" labelAlign="" description="The module courseware filename" control="select"
							query="select NULL as value, ' - phr_NoneSelected - ' as display, 1 as sortOrder
								union
								select fileID as value, name as display,2 as sortOrder
								from vFileDetails where fileID in (<cfif getCoursewareFiles.recordcount is not 0>#valueList(getCoursewareFiles.fileID)#<cfelse>0</cfif>) order by sortOrder, display"></field> <!--- NJH 2009/10/30 LID 2819 - changed query to sort by fileID for files that exist, SKP 2010/09/01 added filename/display to sort order,2015-10-26	WAB	CASE 446337 Protect against there being no courseware files available --->
						<field name="" control="HTML" value="func:request.setModuleLink()" label="" labelAlign=""/>
					</lineItem>

					<!--- 2015-29-04 DCC/WAB Case 444344 AICCFilename reinstated and included file to do manifest/zip reading--->
					<field name="AICCFilename" label="phr_elearning_moduleFilename" description="phr_elearning_IfFileIsZipEnterLauncherFile"></field>
					<field name="LauncherChoice" label="" control="includeTemplate" template="\elearning\modules_chooseLauncher.cfm"></field>
					</group>

					<field name="passDecidedBy" label="phr_eLearning_passDecidedBy_Label" description="phr_elearning_passDecidedBy_Description" control="select" validvalues="Quiz,Courseware"></field>

					<cfif fileExists(application.paths.code & "\cftemplates\modulesform.cfm")>
						<cfinclude template="\Code\cftemplates\modulesform.cfm">
					<cfelse>
						<field name="quizDetailID" label="phr_elearning_associatedQuiz" description="The quiz this module is associated with - filtered to quizzes that are not already associated with a module." control="select"
								query="select 0 as value, ' - phr_NoneSelected - ' as display, 1 as sortOrder from quizDetail
									UNION
								   select qd.quizDetailID as value, qd.title_defaultTranslation as display, 2 as sortOrder from quizDetail qd
									   left outer join trngModule m on m.quizDetailID = qd.quizDetailID
								   where m.quizDetailID is null and qd.active=1
								   	<cfif (isdefined("moduleID") and moduleID neq "" and moduleID neq 0)>
									   	or qd.quizDetailID = (select quizDetailID from trngModule where moduleId = #moduleID#)
									<cfelseif structKeyExists(form,"quizDetailID")>
										or qd.quizDetailID = #form.quizDetailID#
									</cfif>
								   ">
						</field>
					</cfif>


					<field name="CountryID" label="phr_elearning_moduleCountry" description="The country the module is valid in." control="select" query="select countryID as value,countryDescription as display, 1 as orderIndex from country where isoCode is null and countryID in (#request.relayCurrentUser.regionListSomeRights#) union select  ''as countryID,'------------------------------------------' as display, 2 as orderIndex union select countryID as value,countryDescription as display, 3 as orderIndex from country where isoCode is not null and countryID in (#request.relayCurrentUser.countryList#) order by orderIndex,countryDescription"></field>
					<cfif application.com.settings.getSetting("elearning.useTrainingProgram")>
					<field name="TrainingProgramID" label="phr_elearning_trainingProgram" description="The training program the specialisation is valid for." control="select" query="func:com.relayElearning.getTrainingProgram(forDropDown=1)"></field>
					</cfif>
					<field name="publishedDate" label="phr_eLearning_pubDate" description="" control="date" default="#request.requestTime#"></field> <!--- 2011/04/21	MS		CSAT (CR-31)	Date Published --->
					<!--- 2015/10/26 PYW TATA-006. BRD 31. Add Module Expiry Date --->
					<cfif IsDefined("expiryPeriod")>
					<field name="expiryDate" label="phr_module_ExpiryDate" description="" control="date"></field>
					</cfif>
					<field name="moduleDuration" label="phr_elearning_moduleDuration" description="The module duration"></field>
					<field name="credits" label="Credits" description="The amount of credits for this module" control="numeric"></field>
					<field name="AICCType" label="phr_elearning_ModuleType" description="The module type" control="select" validvalues="Online,Classroom,Webinar"> </field>
					<field name="screenTextID" label="phr_elearning_Screen" description="The module screen" control="select"
							query="select NULL as value, ' - phr_NoneSelected - ' as display, 1 as sortorder
									union
									select ScreenTextID as value, ScreenName as display, 2 as sortorder from Screens
									where entitytype='trngusermoduleprogress' order by sortorder"></field>
					<!--- 2011/11/22 PPB CR-LEN061 - upload an image file --->
					<field name="" control="file" acceptType="image" label="phr_eLearning_ModuleImage" parameters="filePrefix=Thumb_,fileExtension=png,displayHeight=90" description=""></field>
					<field name="availability" label="Availability" description="Is the course available" control="YorN"></field>
					<field name="mobileCompatible" label="phr_elearning_mobileCompatible" description="The display of mobile Compatible courses." control="YorN"></field>
					<cfset shareModule = true>
					<cfif not application.com.settings.getSetting("socialMedia.enableSocialMedia")>
						<cfset shareModule = false>
					</cfif>
					<field name="share" label="phr_elearning_moduleShare" description="Allow social updates for this module." control="YorN" <cfif not shareModule>readonly="true"</cfif> <cfif not shareModule>noteText="Social Media has not been enabled."</cfif>></field>
					<field name="SortOrder" label="phr_elearning_SortOrder" description="The display order of certifications." control="select" query="#SortOrderList#"></field>
					<cfif useIncentives>
					<field name="points" label="phr_elearning_Points" description="The amount of points earned for this module" control="numeric"></field>
					</cfif>
					<field name="" control="recordRights" useExtendedRights="true" label="phr_elearning_WhoShouldSeeThisModule" userGroupTypes = "#userGroupTypes#" permissionLevel="1" 		singleUserGroupTypeOnly = "#singleUserGroupType#"></field>
					<!--- 2014/04/14	YMA	Case 435820 List All Linked Courses --->
					<field name="" control="HTML" value="func:request.getRelatedCourses()" label="Courses that contain this module" />
					<group label="System Fields" name="systemFields">
						<field name="sysCreated"></field>
						<field name="sysLastUpdated"></field>
						<field name="LastUpdatedbyPerson" label="phr_editor_lastUpdated" description="" control="hidden"></field>
					</group>
				</editor>

			</editors>
			</cfoutput>
		</cfsavecontent>


	<cfelse>
		<cfset getData = application.com.relayElearning.GetTrainingModulesData(sortOrder=sortOrder,availableModules=url.available)>
		<!--- 2013-08-20 IH Case 436638 add cmSort to getData query to keep No Course Selected modules on top --->
		<cfquery name="getData" dbType="query">
			select *, 'text' as showTitle_Of_CourseAs from getData
			where courseID is null
			union
			select *, 'link' as showTitle_Of_CourseAs from getData
			where courseID is not null AND active = 1	<!--- 2015-04-08 AHL Case 444252 Course and Modules duplicated on Portal. Filtering Inactive course results. --->
			order by cmSort, #sortOrder#
		</cfquery>
	</cfif>

	<cfif application.com.settings.getSetting("elearning.useTrainingProgram")>
		<cfset variables.showTheseColumns = "Module_Code,Title_Of_Module,Title_Of_Course,Module_Type,Duration,Associated_Quiz,Training_Program,MobileCompatible,SortOrder"/>
		<cfset variables.columnHeadingList = "elearning_Module_Code,elearning_Title_Of_Module,elearning_Title_Of_Course,elearning_Module_Type,elearning_moduleDuration,elearning_modules_AssociatedQuiz,elearning_TrainingProgram,MobileCompatible,elearning_SortOrder"/>
		<!--- 2016-11-04 PROD2016-2674 Case 449896 - need to translate previously-hardcoded column headings. Using columnHeadingList instead of passing the elearning_ prefix to TFQO, in order to minimise phrase duplication. --->
	</cfif>

	<!--- NJH 2009/02/06 P-SNY047 --->
	<cfif useIncentives>
		<cfset showTheseColumns = listAppend(showTheseColumns,"Points")>
		<cfset columnHeadingList = listAppend(columnHeadingList,"eLearning_Module_Points")>
		
		<!--- if a record has just been added and we're using rewards as part of elearning, then set up a promotion for this module --->
		<cfif (not Isdefined("AddNew") and isDefined("Added")) or isDefined("Update") and Update eq "Update">
			<cfquery name="createUpdateModulePromotion" datasource="#application.siteDataSource#">
				if not exists (select * from rwPromotion where rwPromotionCode =  <cf_queryparam value="Module#form.moduleID#" CFSQLTYPE="CF_SQL_VARCHAR" >  and createdBy=4 and triggerPoint = 'adhoc')
					insert into rwPromotion (rwPromotionCode,rwPromotionDescription,triggerPoint,created,createdBy,lastUpdated,lastUpdatedBy)
					values (<cf_queryparam value="Module#form.moduleID#" CFSQLTYPE="CF_SQL_VARCHAR" >,<cf_queryparam value="phr_title_trngModule_#form.moduleID#" CFSQLTYPE="CF_SQL_VARCHAR" >,'adhoc',GetDate(),4,GetDate(),4)
			</cfquery>
		</cfif>
	</cfif>

	<!--- 2012/11/14	YMA		CASE:432009 Additional Request from VAB to include links to quiz and course edit pages from this report --->
	<!--- 2014-09-10 AXA Partner Cloud Phase 2 - added postSaveFunction --->

	<cf_listAndEditor
		xmlSource="#xmlSource#"
		cfmlCallerName="modules"
		columnTranslation="True"
		columnTranslationPrefix="phr_"
		keyColumnList="Module_Code,Title_Of_Module,Title_Of_Course,Associated_Quiz"
		keyColumnKeyList=" ,moduleID, , "
		keyColumnOnClickList="javascript:openNewTab('##jsStringFormat(htmlEditFormat(Title_Of_Module))##'*comma'##jsStringFormat(htmlEditFormat(Title_Of_Module))##'*comma'/eLearning/modules.cfm?editor=yes&hideBackButton=true&moduleID=##jsStringFormat(moduleID)##'*comma{reuseTab:true*commaiconClass:'elearning'});return false;,javascript:openNewTab('##jsStringFormat(htmlEditFormat(Title_Of_Module))##'*comma'##jsStringFormat(htmlEditFormat(Title_Of_Module))##'*comma'/eLearning/modules.cfm?editor=yes&hideBackButton=true&moduleID=##jsStringFormat(moduleID)##'*comma{reuseTab:true*commaiconClass:'elearning'});return false;,javascript:openNewTab('##jsStringFormat(htmlEditFormat(Title_Of_Course))##'*comma'##jsStringFormat(htmlEditFormat(Title_Of_Course))##'*comma'/eLearning/courses.cfm?editor=yes&hideBackButton=true&CourseID=##jsStringFormat(CourseID)##'*comma{reuseTab:true*commaiconClass:'elearning'});return false;,javascript:openNewTab('##jsStringFormat(htmlEditFormat(Associated_Quiz))##'*comma'##jsStringFormat(htmlEditFormat(Associated_Quiz))##'*comma'/eLearning/quizSetup.cfm?editor=yes&hideBackButton=true&QuizDetailID=##jsStringFormat(QuizDetailID)##'*comma{reuseTab:true*commaiconClass:'elearning'});return false;"
		keyColumnURLList=" , , , "
		booleanFormat="mobileCompatible"
		showTheseColumns="#showTheseColumns#"
		columnHeadingList="#variables.columnHeadingList#"
		FilterSelectFieldList="Module_Code,Title_Of_Module,Title_Of_Course,Module_Type"
		FilterSelectFieldList2="Module_Code,Title_Of_Module,Title_Of_Course,Module_Type"
		hideBackButton="#iif(structKeyExists(URL,'hideBackButton') and url.hideBackButton is true,true,false)#"
		groupByColumns="#groupBy#"
		useInclude="false"

		sortOrder="#sortOrder#"
		queryData="#getData#"
		numRowsPerPage="#numRowsPerPage#"
		startRow="#startRow#"
		postSaveFunction=#updateLauncherFromScormManifest#
	>

</cfif>