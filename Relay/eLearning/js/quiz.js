/* �Relayware. All Rights Reserved 2014 */

function addQuizDetailQuestionPool() {
	var numOfQuestionsToAdd = jQuery('#numOfQuestions').val();

	if (isNaN(numOfQuestionsToAdd)) {
		numOfQuestionsToAdd = 0;
	}
	
	var btnObj = jQuery('#addQuizDetailQuestionPoolBtn');
	
	/* set all the input value to required; can't set them to required initially as they then get picked up when submitting the quizDetail form */
	jQuery(btnObj).parents('tr').find(':input').attr('required',true);
	
	if ((+parseInt(jQuery('#sumOfQuestions').val())+ +numOfQuestionsToAdd) <= +parseInt(jQuery('#NumToChoose').val())) {
		jQuery('#numOfQuestions').attr('range','1,'+jQuery('#questionPoolId :selected').attr('numOfQuestionsInPool')); // set the numOfQuestions range
		var trObj = jQuery(btnObj).parents('tr').get(0);
		var trElements = getInputElements(trObj);
		setValidateForInputElements(trElements,true);
		
		if (doRelayValidationOnArrayOfElements(trObj,trElements,{showValidationErrorsInline:true})) {
			var page = domainAndRoot + '/webservices/callwebservice.cfc?wsdl&method=callWebService&webserviceName=relayElearningWS&methodName=insertQuizDetailQuestionPool&returnFormat=JSON'
			var parameters = serializeInputElements(trElements);
			parameters += '&quizDetailID='+jQuery('#QuizDetailID').val()
			
			var myAjax = new Ajax.Request(
				page, 
				{
					method: 'post', 
					parameters: parameters , 
					evalJSON: 'force',
					debug : false,
					onComplete: function (requestObject,JSON) {
						updateQuizDetailQuestionPoolDiv(jQuery('#QuizDetailID').val());
					}
				}
			)
		}
	} else {
		alert('Warning. Total Number of Questions Per Question Pool exceeded the Total Number of Questions Per Quiz')	
	}

	/* set the input value for the questionPools back to not required so that the main quizDetail form does not pick them up on submit */
	jQuery(btnObj).parents('tr').find(':input').removeAttr('required');
}


function updateQuizDetailQuestionPoolDiv (quizDetailId) {
	var page = domainAndRoot + '/webservices/callwebservice.cfc?wsdl&method=callWebService&webserviceName=relayElearningWS&methodName=getQuizDetailQuestionPoolDisplay&returnFormat=JSON'
	var parameters = {quizDetailId:quizDetailId};

	var myAjax = new Ajax.Request(
		page, 
		{
			method: 'get', 
			parameters: parameters , 
			evalJSON: 'force',
			debug : false,
			onComplete: function (requestObject,JSON) {
				json = requestObject.responseJSON;
				jQuery('#QuizDetailQuestionPoolDiv').html(json.CONTENT);
			}
		}
	)
}


function deleteQuizDetailQuestionPool(quizDetailQuestionPoolId) {

	var page = domainAndRoot + '/webservices/callwebservice.cfc?wsdl&method=callWebService&webserviceName=relayElearningWS&methodName=deleteQuizDetailQuestionPool&returnFormat=JSON'
	var parameters = {quizDetailQuestionPoolId:quizDetailQuestionPoolId};

	var myAjax = new Ajax.Request(
		page, 
		{
			method: 'post', 
			parameters: parameters , 
			evalJSON: 'force',
			debug : false,
			onComplete: function (requestObject,JSON) {
				updateQuizDetailQuestionPoolDiv(jQuery('#QuizDetailID').val());
			}
		}
	)	
}