/* �Relayware. All Rights Reserved 2014 */

function refreshModuleStatus(moduleID,status,userModuleProgressID) {
	if (moduleID == undefined)
		moduleID=0;
	if (status == undefined)
		status = '';
		
	if (div = $('module'+moduleID+'div')) {
	
		if (userModuleProgressID != undefined && status == '') {
			var page = '/webservices/callwebservice.cfc?wsdl&method=callWebService&returnformat=json&_cf_nodebug=false'
			var page = page + '&webservicename=relayElearningWS&methodname=getModuleStatus'    
			var parameters = {userModuleProgressID:userModuleProgressID}
			
			var myAjax = new Ajax.Request(
	            page, 
	            {
	                method: 'get', 
	                parameters: parameters,
	                evalJSON: 'force',
	                debug : false,
	                onComplete: function (requestObject,JSON) {
	                	div.className='inner '+ requestObject.responseText.evalJSON();
	                }                       
	            });
		} else if (status != '') {
			div.className='inner '+status;
		} 
	}
	else if (typeof (refreshModuleDiv) == 'function')
		refreshModuleDiv();
}


//2013-07-19 PPB Case 436072 lifted function from personCertifications.cfm for use by ElearningStudentProgress; probably we should return isOk and message rather than firing location.reload here
//nb. this can be used to reset the user module progress status even when there isn't a Quiz associated 
function resetQuiz(userModuleProgressID,quizTakenID) {
	if ($("savedMessage")!=null) { 				// ensure the recipient element exists; called "savedMessage" for compatiblity with existing code in personCertifications.cfm
		$("savedMessage").innerHTML = ""; 		//clear any previous message
	}
	//2013-07-25 PPB Case 436072 moved returnFormat:'json' out of parameters below
	page = '/webservices/callwebservice.cfc?wsdl&method=callWebService&webserviceName=relayElearningWS&methodName=resetQuizAttempt'
	var myAjax = new Ajax.Request(
		page, 
		{
			method: 'post', 
			parameters: {quizTakenID:quizTakenID,userModuleProgressID:userModuleProgressID,returnFormat:'json'}, 
			evalJSON: 'force',
			debug : false,
			onComplete: function (requestObject,JSON) {
							var json = requestObject.responseJSON
							var messageClass = 'success'

							if (!json.ISOK) {
								messageClass = 'error'
							}
							if ($("savedMessage")!=null) { 			// ensure the recipient element exists;			
								$('savedMessage').innerHTML = '<div class="'+messageClass+'block">'+json.MESSAGE+'</div>';
							}
							
							if (json.ISOK) {
								location.reload(false); // only reload if successfully, as that will mean that the module progress has changed.
							}
						}
		});
	
	return false;
}


 /* JS functions/functionality to be used in the trainingPathAssignment functionality. */
var trainingPathAssignment = (function () {
	
		var confirmDeletion = function(trainingPathId,trainingPath) {
			if (confirm("Are you sure you want to remove "+trainingPath+" from this user.") == true) {
				document.getElementById("frmUnAssignTrainingPathID").value = trainingPathId;
				document.forms["trainingPathUnAssignForm"].submit();
			} else {
				return false;
			}
		 };
		 
		 return { confirmDeletion: confirmDeletion};
		 
 })();
 
 
 /* JS functions/functionality to be used in the trainingPath functionality. Currently only used in the relaytag */
 var trainingPath = {
 	init: function() {
 		var min_width = 100;
		var $_trainingPathName = jQuery(".trainingPathName")
		
		$_trainingPathName.each(function(){
			var this_width = jQuery(this).outerWidth();
			if (this_width > min_width) min_width = this_width;
		})
		
		$_trainingPathName.css('min-width', min_width + 'px');
		
		jQuery('[name^="frmAssignTrainingPath"]').click(function() {
				var addPersonIDs = "";
				var removePersonIDs = "";
				var trainingPathId = jQuery(this).attr('data-trainingpathid');

				$('trainingPathResult'+trainingPathId).spinner();

				
				/* using jquery ui multi-select, so have to get value of checkboxes, rather than selected options! May need to revisit this */
				jQuery('#frmAssignPeopleToTrainingPath'+trainingPathId+' option:selected').each(function(){
					addPersonIDs = addPersonIDs + ',' + jQuery(this).val()
				});
				
				jQuery('#frmAssignPeopleToTrainingPath'+trainingPathId+' option:not(:selected)').each(function(){
					removePersonIDs = removePersonIDs + ',' + jQuery(this).val()
				});
				
				addPersonIDs = addPersonIDs.replace(/^,/, '');
				removePersonIDs = removePersonIDs.replace(/^,/, '');

				page = '/webservices/callWebservice.cfc?wsdl&method=callWebService&webservicename=relayElearningWS&methodName=addRemoveUsersToTrainingPath'

				var myAjax = new Ajax.Updater(
					"trainingPathResult"+trainingPathId,
					page,
					{
						method: 'get',
						parameters: {trainingPathId:trainingPathId,addPersonIDs:addPersonIDs,removePersonIDs:removePersonIDs,returnFormat:'plain'},
						evalScripts: true,
						debug : false,
						onComplete: function(){$('trainingPathResult'+trainingPathId).removeSpinner();}
					});
		});
 	}
 }
 
 
  /* JS functions/functionality to be used in the trainingPath functionality. Currently only used in the relaytag */
 var trainingModules = {
 	currentCourseID: 0,
 	trngModulesDataStruct:{},
 	$_currentRowObj:'',
 	
 	init: function(dataStruct) {
 
 		trngModulesDataStruct = dataStruct;
		jQuery('.show_modules a').click(function() {
			showModules(this);
		});
 		
		function showModules (obj) {

			$_currentRowObj = jQuery(obj).parents('tr');
			var totalColspan = $_currentRowObj.children().length;
			var currentRowID = $_currentRowObj.attr('id').split('=')[1]; // string is 'trngPathID_courseID=17_2'
			currentCourseID = currentRowID.split('_')[1];
		
			infoRow =  currentRowID + '_info'
			$_infoRowObj = jQuery('#'+infoRow);
		
			if ($_infoRowObj.length == 0) {
				var page = '/webservices/callwebservice.cfc?wsdl&method=callWebService&webserviceName=relayElearningWS&methodName=getCourseModulesWithProgress&returnFormat=json&courseID='+currentCourseID;
			
				jQuery.ajax({
					type: "GET",
					url: page,
					data: dataStruct,
					dataType: "json",
					cache: false,
					success: function(data,textStatus,jqXHR) {
						$_currentRowObj.after('<tr id="'+currentRowID+'_info"><td colspan="'+totalColspan+'" align="right">'+data.CONTENT+'</td></tr>');
					}
				});
		
			} else {
				$_infoRowObj.toggle()
			}
		
			return;
		}
	},
	
	refreshModuleDivInTrainingPath: function(courseID) {
		if (courseID == undefined) {
			courseID = currentCourseID;
		}

		page = '/webservices/callWebservice.cfc?wsdl&method=callWebService&webserviceName=relayElearningWS&methodName=getPersonCourseSummary&returnFormat=json'
		parameters = 'courseID='+courseID+'&personID='+trngModulesDataStruct.personId+'&showTheseColumns='+trngModulesDataStruct.showTheseColumns;

		var myAjax = new Ajax.Request(
			page,
			{
				method: 'get',
				parameters: parameters ,
				evalJSON: 'force',
				debug : false,
				onComplete: function (requestObject,JSON) {
					var json = requestObject.responseJSON
					
					$_currentRowObj.children('td').each(function() {
						var $_td = jQuery(this);
						var className = $_td.attr('class').toUpperCase();
						if ((json[className]) && (className == 'NUM_MODULES_COMPLETED' || className=='NUM_MODULES_ATTEMPTED')) {
							$_td.html(json[className]);
						}
					});
				}
			}
		)

		page = '/webservices/callWebservice.cfc?wsdl&method=callWebService&webserviceName=relayElearningWS&methodName=getCourseModulesWithProgress&courseID='+courseID;
			
		jQuery.ajax({
			type: "GET",
			url: page,
			data: jQuery.param(trngModulesDataStruct),
			dataType: "json",
			cache: false,
			success: function(data,textStatus,jqXHR) {
				jQuery('#personCertificationDataDiv').html(data.CONTENT);
			}
		});
	}
}