<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			.cfm
Author:				SWJ
Date started:			/xx/02

Description:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2009/04/24			NJH			Bug Fix T-10 Issue 1066 - added course code in editor as it was appearing in the listing screen
2009/06/24			NJH			Bug Fix Sony1 Support Issue 2396 - changed course description from textArea1000 to textArea250 as the field is only 250 characters
2009/06/29			SSS			Bug Fix Sony1 Support Issue 2396 - ROLLED CHANGE ABOVE OUT SONY DECIDED THAT THEY WOULD LIKE THE DATABASE TO CHANGE NOT THE FILE.
2009/10/23			NAS			LID - 2481/2499 Code entered to Order Translatable Columns
2010/08/06			NJH			RW8.3 - get values from settings
2010/09/15			NAS			P-LEN022 add 'Active' to only retrieve active/inactive courses
2010/09/15			WAB			P-LEN022 Implemented Database Level PhrasePointer Cache
2011/04/19			MS			P-LEN030 CSAT (CR-31)	Date Published added as an extra field to be recorded for courses
2011/11/22 			PPB 		CR-LEN061 - upload an image file
2012-03-27			RMB			added mobileCompatible
2012-04-16	 		Englex 		P-REL109 - Add training program
2013-07-10 			PPB 		Case 436015 added jsStringFormat to cope with apostrophes
2013-11-07 			PPB 		Case 437554 increased length of course description
2014-10-03			RPW			CORE-760 Error message when trying to view Certifications on  Training - Moved session.selectedTrainingProgramID to trainingProgramCheck.cfm
2015-06-09			AHL			Case 444625 allowing empty null fields and provide them with a default null phrase
2015/08/14			NJH			Case 445556  - htmlEditFormat the various name fields in the openNewTab calls
2015-11-24			IH			P-TAT006 BRD 31. Add showExpired
2016-03-03			WAB			Moved attributes relating to recordRights manager from listAndEditor call to the XML node.
2016-05-12			atrunov     PROD2016-778 setting recordRights usage by default and configuring Elearning Visibility with multiple rules
2016-05-18			WAB			BF-714 TrainingProgram drop down. Remove reference to get ExtendedMetaData.sql and replace with func:...getTrainingProgram()
2016-05-18			WAB			BF-716 TrainingProgram drop down. Does not make sense to filter session.selectedTrainingProgramID
Possible enhancements:


 --->

<!--- START: 2012-04-16 - Englex - P-REL109 - Add training program --->
<cfif application.com.settings.getSetting("elearning.useTrainingProgram")>
	<cfinclude template="trainingProgramCheck.cfm">
</cfif>
<!--- END: 2012-04-16 - Englex - P-REL109 - Add training program --->

<cf_title>Courses</cf_title>
<cfsetting requesttimeout="300">
<cfparam name="getData" type="string" default="foo">
<cfparam name="sortOrder" default="CourseTitle">
<cfparam name="numRowsPerPage" type="numeric" default="100">
<cfparam name="startRow" type="numeric" default="1">
<!--- 2010/09/15			NAS			P-LEN022 add 'Active' to only retrieve active/inactive courses --->
<cfparam name="url.active" type="Boolean" default="1">
<!--- START: 2012-05-01 - Englex - P-REL109 - Add training program --->
<cfparam name="variables.showTheseColumns" default="CourseCode,CourseTitle,ModuleTitle,QuizTitle,CoursewareTitle,DownloadFile,SolutionArea,mobileCompatible"/>
<!--- END: 2012-05-01 - Englex - P-REL109 - Add training program --->


<!--- NJH 2009/07/22 P-SNY047 - added sort order for courses similar to modules --->
<cfset SortOrderList="select 1 as value,1 as display">
<cfloop index='i' from='2' to='99'>
	<cfset SortOrderList="#SortOrderList# union select #i# as value,#i# as display">
</cfloop>

<!--- NJH 2010/08/06 Rw8.3 - get variables from settings --->
<cfset userGroupTypes = application.com.settings.getSetting("elearning.userGroupTypes") />
<cfset singleUserGroupType = application.com.settings.getSetting("elearning.singleUserGroupType") />

<!--- save the content for the xml to define the editor --->
<!--- NJH 2009/04/24 Bug Fix T-10 Issue 1066 - added course code in editor as it was appearing in the listing screen. --->
<!--- NJH 2009/07/21 P-SNY047 - added translations for courses --->
<cfsavecontent variable="xmlSource">
	<cfoutput>
	<editors>
		<editor id="232" name="thisEditor" entity="trngCourse" title="Course Editor">
			<field name="courseID" label="phr_eLearning_Course_ID" description="This records unique ID." control="html"></field>
			<field name="AICCCourseID" label="phr_eLearning_Course_Code" description=""></field>
			<field name="" CONTROL="TRANSLATION" label="phr_eLearning_Course_Title" Parameters="phraseTextID=Title" required="true"></field>
			<field name="" CONTROL="TRANSLATION" label="phr_eLearning_Course_Description" description=""  Parameters="phraseTextID=Description,displayAs=TextArea,maxChars=5000"></field>		<!--- 2013-11-07 PPB Case 437554 added maxChars --->
			<field name="publishedDate" label="phr_eLearning_pubDate" description="" control="date" default="#Now()#"></field> <!--- 2011/04/19	MS		CSAT (CR-31)	Date Published --->
			<!--- <field name="AICCCourseTitle" label="phr_eLearning_Course_Title" description=""></field>
			<field name="AICCCourseDescription" label="phr_eLearning_Course_Description" description="" control="textArea1000"></field> --->
			<field name="courseSeriesID" label="phr_elearning_Series" description="" control="select" query="select courseSeriesID as value, courseSeriesShortName as display from trngCourseSeries" NullText="phr_elearning_Series_NullText"/>	<!--- 2015-06-09 AHL Case 444625 --->
			<field name="solutionAreaId" label="phr_eLearning_Solution_Area" description="" control="select" query="select solutionAreaID as value, solutionAreaName as display from trngSolutionArea"/>
			<field name="userLevelId" label="phr_eLearning_User_Level" description="" control="select" query="select userLevelID as value, userLevelName as display from trngUserLevel"/>
			<field name="noModules" label="phr_eLearning_Number_Of_Modules" description=""></field>
			<!--- 2011/11/22 PPB CR-LEN061 - upload an image file --->
			<field name="" control="file" acceptType="image" label="phr_eLearning_CourseImage" parameters="filePrefix=Thumb_,fileExtension=png,displayHeight=90" description=""></field>
			<!--- NJH added SortOrder.  If missing in table, SQL to added in H:\Release Notes\Sony\P-SNY047 Sony Academy\P-SNY047 Release Scripts.sql --->
			<field name="SortOrder" label="phr_elearning_SortOrder" description="The display order of courses." control="select" query="#SortOrderList#" NullText="phr_elearning_SortOrder_NullText"></field>	<!--- 2015-06-09 AHL Case 444625 --->
			<!--- this should work, don't know why it's throwing an error:
			<field name="CreatedBy" label="" default="#request.relaycurrentuser.userGroupID#" description="" control="hidden"></field>
			<field name="Created" label="" default="#request.requestTime#" description="" control="hidden"></field>
			<field name="LastUpdatedBy" label="" default="#request.relaycurrentuser.userGroupID#" description="" control="hidden"></field>
			<field name="LastUpdated" label="" default="#request.requestTime#" description="" control="hidden"></field>
			--->
			<!--- START: 2012-04-16 - Englex - P-REL109 - Add training program --->
			<cfif application.com.settings.getSetting("elearning.useTrainingProgram")>
				<field name="TrainingProgramID" label="phr_elearning_trainingProgram" description="The training program the specialisation is valid for." control="select" query="func:com.relayElearning.getTrainingProgram(forDropDown=1)" NullText="phr_elearning_trainingProgram_NullText"></field>	<!--- 2015-06-09 AHL Case 444625 --->
			</cfif>
			<!--- END: 2012-04-16 - Englex - P-REL109 - Add training program --->
			<!--- 2010/09/15			NAS			P-LEN022 add 'Active' to only retrieve active/inactive courses --->
			<field name="Active" label="phr_elearning_Active" description="The display of Active courses." control="YorN"></field>
			<field name="mobileCompatible" label="phr_elearning_mobileCompatible" description="The display of mobile Compatible courses." control="YorN"></field>
			<field name="" control="recordRights" useExtendedRights="true"   label="phr_elearning_WhoShouldSeeThisCourse" 	userGroupTypes = "#userGroupTypes#" 	singleUserGroupTypeOnly = "#singleUserGroupType#"></field>
			<field name="" control="includeTemplate" template="/eLearning/courseModules.cfm" label="Course Modules"  NullText="phr_elearning_CourseModules_NullText"/>	<!--- 2015-06-09 AHL Case 444625 --->
			<group label="System Fields" name="systemFields">
				<field name="sysCreated"></field>
				<field name="sysLastUpdated"></field>
				<field name="LastUpdatedbyPerson" label="phr_editor_lastUpdated" description="" control="hidden"></field>
			</group>
		</editor>
	</editors>
	</cfoutput>
</cfsavecontent>

<cfif not isDefined("URL.editor")>
	<cf_includeJavascriptOnce template = "/javascript/fileDownload.js">

	<!--- 2010/09/15			NAS			P-LEN022 add 'Active' to only retrieve active/inactive courses --->
	<!--- 2015-11-24 IH P-TAT006 BRD 31. Add showExpired --->
	<cfset getData = application.com.relayElearning.GetCourseSummary(sortOrder=sortOrder,active=url.active,showExpired=false)>
</cfif>

<cfif application.com.settings.getSetting("elearning.useTrainingProgram")>
	<cfset variables.showTheseColumns = "CourseCode,CourseTitle,ModuleTitle,QuizTitle,CoursewareTitle,DownloadFile,SolutionArea,TrainingProgram,mobileCompatible"/>
</cfif>

<cfquery name="qFileTypeGroupID" datasource="#application.siteDataSource#" cachedwithin="#createTimeSpan(0,0,1,0)#">
	SELECT filetypegroupid
	FROM filetypegroup
	WHERE [heading] =  <cf_queryparam value="ELearning_CourseWare" CFSQLTYPE="CF_SQL_VARCHAR" >
</cfquery>

<cfset fileTypeGroupID = qFileTypeGroupID.filetypegroupid>


<cf_listAndEditor
	xmlSource="#xmlSource#"
	cfmlCallerName="courses"
	keyColumnList="CourseTitle,ModuleTitle,QuizTitle,CoursewareTitle,DownloadFile"
	keyColumnKeyList=" , , , , "
	keyColumnURLList=" , , , , "
	keyColumnOnClickList="javascript:openNewTab('##jsStringFormat(htmlEditFormat(CourseTitle))##'*comma'##jsStringFormat(htmlEditFormat(CourseTitle))##'*comma'/eLearning/courses.cfm?editor=yes&hideBackButton=true&CourseID=##jsStringFormat(CourseID)##'*comma{reuseTab:true*commaiconClass:'elearning'});return false;,javascript:openNewTab('##jsStringFormat(htmlEditFormat(ModuleTitle))##'*comma'##jsStringFormat(htmlEditFormat(ModuleTitle))##'*comma'/eLearning/modules.cfm?editor=yes&hideBackButton=true&ModuleID=##jsStringFormat(ModuleID)##'*comma{reuseTab:true*commaiconClass:'elearning'});return false;,javascript:openNewTab('##jsStringFormat(htmlEditFormat(QuizTitle))##'*comma'##jsStringFormat(htmlEditFormat(QuizTitle))##'*comma'/eLearning/quizSetup.cfm?editor=yes&hideBackButton=true&QuizDetailID=##jsStringFormat(QuizDetailID)##'*comma{reuseTab:true*commaiconClass:'elearning'});return false;,javascript:openNewTab('##jsStringFormat(htmlEditFormat(CoursewareTitle))##'*comma'##jsStringFormat(htmlEditFormat(CoursewareTitle))##'*comma'/fileManagement/files.cfm?fileID=##jsStringFormat(courseWareID)##&editor=yes&showSaveAndAddNew=false'*comma{reuseTab:true*commaiconClass:'elearning'});return false;,javascript:getFile(##jsStringFormat(courseWareID)##);return false;"
	showTheseColumns="#variables.showTheseColumns#"
	FilterSelectFieldList="CourseCode,CourseTitle,ModuleTitle,QuizTitle,CoursewareTitle,SolutionArea"
	FilterSelectFieldList2="CourseCode,CourseTitle,ModuleTitle,QuizTitle,CoursewareTitle,SolutionArea"
	useInclude="false"
	sortOrder="#sortOrder#"
	hideBackButton="#iif(structKeyExists(URL,'hideBackButton') and url.hideBackButton is true,true,false)#"
	queryData="#getData#"
	numRowsPerPage="#numRowsPerPage#"
	startRow="#startRow#"
	booleanFormat="mobileCompatible"
	columnTranslation="true"
	columnTranslationPrefix="phr_report_course_"
	treatEmtpyFieldAsNull="true"<!--- 2015-06-09 AHL Case 444625 allowing empty null fields --->
>