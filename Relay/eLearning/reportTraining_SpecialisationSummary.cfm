<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		reportTraining_SpecialisationSummary.cfm	
Author:			NJH  
Date started:	20-08-2008
	
Description:	Displays training Specialisations 

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->

<cfparam name="useFullRange" type="boolean" default="true">
<cfparam name="openAsExcel" type="boolean" default="false">
<cfparam name="sortOrder" default="Country,Organisation_name,Description">
<cfparam name="numRowsPerPage" default="50">
<cfparam name="startRow" default="1">

<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">

<cf_title>phr_elearning_SpecialisationSummary</cf_title>

<cfscript>
	argumentsStruct = structNew();
	argumentsStruct.sortOrder = sortOrder;
	argumentsStruct.returnOrgData = true;
	
	qryGetSpecialisationSummaryData = application.com.relayElearning.GetSpecialisationSummaryData(argumentCollection=argumentsStruct);
</cfscript>

<table width="100%" cellpadding="3">
	<tr>
		<td><strong>phr_elearning_SpecialisationSummary</strong></td>
		<td align="right">&nbsp;</td>
	</tr>
</table>

<CF_tableFromQueryObject 
	queryObject="#qryGetSpecialisationSummaryData#"
	queryName="qryGetSpecialisationSummaryData"
	sortOrder = "#sortOrder#"
	numRowsPerPage="#numRowsPerPage#"
	startRow="#startRow#"
	openAsExcel="#openAsExcel#"
 	hideTheseColumns="Country"
	showTheseColumns="Organisation_Name,Country,Code,Description,Status,Pass_Date,Num_Certifications_To_Pass,Num_Certifications_Passed"
	columnTranslation="false"
	dateformat="Registration_Date,Pass_Date"
	numberFormat="Num_Certifications_To_Pass,Num_Certifications_Passed"
	totalTheseColumns="Num_Certifications_To_Pass,Num_Certifications_Passed"
	FilterSelectFieldList="Code,Organisation_Name,Country,Description,Status"
	FilterSelectFieldList2="Code,Organisation_Name,Country,Description,Status"
	GroupByColumns="Country"
	radioFilterLabel=""
	radioFilterDefault=""
	radioFilterName=""
	radioFilterValues=""
	checkBoxFilterName=""
	checkBoxFilterLabel=""
	allowColumnSorting="yes"
	useInclude="false"
>