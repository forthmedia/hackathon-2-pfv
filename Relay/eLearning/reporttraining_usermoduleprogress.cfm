<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			reportTraining_UserModuleProgress.cfm	
Author:				?
Date started:		?
Description:		

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2008-11-03 			MDC 		Lenovo LighthouseID 1273 added in below as report was showing the first result in the list rather than certification data
2009/03/23			NJH			Bug Fix Lenovo Issue 1979 Added a param to hide/show the page controls.
2012-07-12			PJP			Case#429052 added new table inner join trngModuleCourse 
Possible enhancements:

 --->



<cf_title>phr_eLearningUserModuleProgressReport</cf_title>
<!--- 


<cf_head>
	<cf_title>phr_eLearningUserModuleProgressReport</cf_title>
</cf_head>

 --->
<cfparam name="frmEntityTypeID" type="numeric" default="2">

<cfif frmEntityTypeID eq 0>
	<cfparam name="frmCurrentEntityID" type="numeric" default="#request.relayCurrentUser.personID#">
<cfelseif frmEntityTypeID eq 2>
	<cfparam name="frmCurrentEntityID" type="numeric" default="#request.relayCurrentUser.organisationID#">
</cfif>

<cfparam name="foreignKeyFlagTextID" type="string" default="CertificationID">

<cfparam name="sortOrder" default="fullName,solution_area,title,completion_date">
<cfparam name="numRowsPerPage" default="50">
<cfparam name="startRow" default="1">
<cfparam name="hidePageControls" type="boolean" default="true">  <!--- NJH 2009/03/23 Bug Fix Lenovo Issue 1979 --->

<!--- NJH 2008/01/23 If the user is internal, show the personID. --->
<cfif request.relayCurrentUser.isInternal>
	<cfparam name="showTheseColumns" type="string" default="FullName,PersonID,Course_Series,Solution_Area,Course_Code,Course_Title,Completion_Date">
<cfelse>
	<!--- if it's a person, don't show their name --->
	<cfif frmEntityTypeID eq 0>
		<cfparam name="showTheseColumns" type="string" default="Course_Series,Solution_Area,Course_Code,Course_Title,Completion_Date">
	<cfelse>
		<cfparam name="showTheseColumns" type="string" default="FullName,Course_Series,Solution_Area,Course_Code,Course_Title,Completion_Date">
	</cfif>
</cfif>

<cfset groupBy = "">

<!--- if we're viewing the person details, display the foreign key from the incoming data --->
<cfif frmEntityTypeID eq 0>
	<cfset flagStructure = application.com.flag.getFlagStructure(foreignKeyFlagTextID)>
	<cfquery name="getForeignKey" datasource="#application.siteDataSource#">
		select data as foreignKey from #flagStructure.flagType.dataTableFullName#
		where entityID =  <cf_queryparam value="#frmCurrentEntityID#" CFSQLTYPE="CF_SQL_INTEGER" > 
<!--- 2008-11-03 MDC Lenovo LighthouseID 1273 added in below as report was showing the first result in the list rather than certification data --->
		and flagid =  <cf_queryparam value="#flagStructure.flagid#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</cfquery>
	
	<cfset groupBy="">
</cfif>

<!--- NJH 2009/07/22 P-SNY047 - course translation --->
<cfquery name="getUserCourses" datasource="#application.siteDataSource#">
	select * from (
	select 'phr_title_TrngModule_' + convert(VarChar,tm.moduleID) as title, tm.aiccType, 'phr_title_TrngCourse_' + CONVERT(VarChar, tc.courseId)<!--- tc.aiccCourseTitle ---> as Course_title, tc.aiccCourseID as Course_Code,
		p.firstname+ ' '+p.lastname as fullname, p.personID,
		tcs.courseSeriesTitle as course_series, tul.userLevelName as user_level, tsa.solutionAreaName as solution_area,
		tump.quizPassedDate as completion_date
	from trngUserModuleProgress tump with (noLock) 
		inner join trngModule tm with (noLock) on tm.moduleID = tump.moduleID 
		inner join trngModuleCourse tmc with (noLock) on tmc.moduleID = tm.moduleID <!--- PJP 2012-07-11 Case 429052 added inner join to trngModuleCourse ---->
		inner join trngCourse tc with (noLock) on tc.courseID = tmc.courseID 
		inner join trngUserLevel tul with (noLock)
		on tc.userLevelID = tul.userLevelID inner join trngSolutionArea tsa with (noLock)
		on tsa.solutionAreaID = tc.solutionAreaID inner join person p with (noLock)
		on p.personID = tump.personID left outer join trngCourseSeries tcs with (noLock)
		on tcs.courseSeriesID = tc.courseSeriesID
	) as base
	where 1=1
	<cfinclude template="../templates/tableFromQuery-QueryInclude.cfm">
		<cfif frmEntityTypeID eq 0>
		and personID =  <cf_queryparam value="#frmCurrentEntityID#" CFSQLTYPE="CF_SQL_INTEGER" > 	
		<cfelseif frmEntityTypeID eq 2>
		and personID in (select personID from person where organisationID =  <cf_queryparam value="#frmCurrentEntityID#" CFSQLTYPE="CF_SQL_INTEGER" > )
		</cfif>
	--group by Course_Series,Title,FullName,aiccType,Course_Title,personID,user_level,solution_area
	order by <cf_queryObjectName value="#sortOrder#">
</cfquery>

<h1>phr_elearning_UserModuleProgress</h1>
<TABLE WIDTH="100%" BORDER="0" CELLSPACING="1" CELLPADDING="3" ALIGN="center" CLASS="withBorder">
    <cfif frmEntityTypeID eq 0>	
	<tr>
		<td align="left">
			<cfoutput>
				<strong>#htmleditformat(application.com.flag.getFlagStructure(foreignKeyFlagTextID).translatedName)#:</strong> <cfif getForeignKey.recordcount EQ 0>No #htmleditformat(application.com.flag.getFlagStructure(foreignKeyFlagTextID).translatedName)# exists for this user.<cfelse>#htmleditformat(getForeignKey.foreignKey)#</cfif>
			</cfoutput>
		</td>
	</tr>	
    </cfif>

	<tr>
		<td align="left">

			<cfif getUserCourses.recordCount gt 0>
				<CF_tableFromQueryObject 
					queryObject="#getUserCourses#"
					queryName="getUserCourses"
					sortOrder = "#sortOrder#"
					<!--- numRowsPerPage="#numRowsPerPage#" --->
					startRow="#startRow#"
					<!--- openAsExcel="#openAsExcel#" --->
					hideTheseColumns=""
					showTheseColumns="#showTheseColumns#"
					columnTranslation="true"
					numberFormat=""
					dateformat="Completion_Date"
					allowColumnSorting="yes"
					showCellColumnHeadings="no"
					groupByColumns="#groupBy#"
					HidePageControls="#HidePageControls#" <!--- NJH 2009/03/23 Bug Fix Lenovo Issue 1979 --->
					useInclude = "false"
				>
			<cfelse>
				<cfif frmEntityTypeID eq 0>
					phr_elearning_NoCoursesForThisPerson.
				<cfelseif frmEntityTypeID eq 2>
					phr_elearning_NoCoursesForThisOrganisation.
				</cfif>
			</cfif>

			</td>
	</tr>
		
</TABLE>