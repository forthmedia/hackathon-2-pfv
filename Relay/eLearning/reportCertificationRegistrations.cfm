<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			reportCertificationRegistrations.cfm	
Author:				NJH
Date started:		2008/08/21
Description:		Display and edit person certifications.	

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:

 --->

<cf_title>Certification Registrations</cf_title>

<cfparam name="getData" type="string" default="foo">
<cfparam name="sortOrder" default="Organisation_Name,FullName,Description">
<cfparam name="personCertificationID" type="numeric" default="0">
<cfparam name="numRowsPerPage" default="50">
<cfparam name="startRow" default="1">

<cfparam name="returnOrgData" type="boolean" default="true">
<cfparam name="returnPersonData" type="boolean" default="true">
<cfparam name="showTheseColumns" type="string" default="Organisation_Name,FullName,Code,Description,Registration_Date,Expiry_Date,Status">
<cfparam name="FilterSelectFieldList" type="string" default="Organisation_Name,Code,Status">
<cfparam name="FilterSelectFieldList2" type="string" default="#FilterSelectFieldList#">
<cfparam name="keyColumnList" type="string" default="FullName">


<cfset request.relayFormDisplayStyle = "HTML-table">


<cfset url.CertificationID = 0>
<cfset url.personID = 0>

<!--- save the content for the xml to define the editor --->
<cfsavecontent variable="xmlSource">
	<cfoutput>
	<editors>
		<editor id="232" name="thisEditor" entity="trngPersonCertification" title="Person Certification Editor">
			<field name="personCertificationID" label="phr_elearning_personCertificationID" description="This records unique ID." control="html"></field>
			<field name="CertificationID" label="phr_elearning_certificationID" description="The certification ID of the certification the person registered for." control="HTML" display="display" value="value"
						query="select title_defaultTranslation as display, certificationId as value from trngCertification where certificationID = (select certificationID from trngPersonCertification where personCertificationID = #personCertificationID#)"></field>
			<field name="personID" label="phr_elearning_personID" description="The person ID of the person holding the certification." control="HTML" display="display" value="value"
						query="select firstname+' '+lastname as display, personId as value from person where personID = (select personID from trngPersonCertification where personCertificationID = #personCertificationID#)"></field>
			<field name="registrationDate" label="phr_elearning_registrationDate" description="The date the person registered for the certification." control="date"></field>
			<field name="cancellationDate" label="phr_elearning_cancellationDate" description="The date the registration was cancelled." control="date"></field>
			<field name="CreatedBy" label=""></field>
			<field name="Created" label=""></field>
			<field name="LastUpdatedBy" label=""></field>
			<field name="LastUpdated" label=""></field>
		</editor>	
	</editors>
	</cfoutput>
</cfsavecontent>


<cfif not isDefined("URL.editor")>	
	<cfset getData = application.com.relayElearning.GetPersonCertificationData(returnOrgData=returnOrgData,returnPersonData=returnPersonData,sortOrder=sortOrder)>
</cfif>

<cf_listAndEditor 
	xmlSource="#xmlSource#" 
	cfmlCallerName="/elearning/reportCertificationRegistrations"
	keyColumnList="#keyColumnList#"
	keyColumnKeyList="personCertificationID"
	showTheseColumns="#showTheseColumns#"
	dateFormat="Registration_Date,Expiry_date"
	showSaveAndReturn = "true"	
	FilterSelectFieldList="#FilterSelectFieldList#"
	FilterSelectFieldList2="#FilterSelectFieldList2#"
	useInclude="false"
	sortOrder="#sortOrder#"
	queryData="#getData#"
	numRowsPerPage="#numRowsPerPage#"
	startRow="#startRow#"
>