<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			Report_Quiz2.cfm	
Author:				A Developer
Date started:		The Olden Days
	
Description:		Quizzes Taken

Amendment History:

Date (DD-MM-YYYY)	Initials 	What was changed
2008-09-16			AJC			Change the report to use TFQO and also added filters & export to excel
								Pretty much changed the whole file
2009-02-27			NJH			Bug Fix All Sites Issue 1899 - made column headings translatable
2009-08-03			NYB  		SNY047					
2009-09-30			NYB			LHID2604
2009-10-23			NAS			LID - 2481/2499 Code entered to Order Translatable Columns
					
Possible enhancements:


 --->

<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">
<cfparam name="openAsExcel" type="boolean" default="false">

<cf_title>Report: Quizzes Taken</cf_title>

<cfif not openAsExcel>
		<cfparam name="current" type="string" default="index.cfm">
		<cfparam name="attributes.templateType" type="string" default="edit">
		<cfparam name="attributes.pageTitle" type="string" default="">
		<cfparam name="attributes.pagesBack" type="string" default="1">
		<cfparam name="attributes.thisDir" type="string" default="">

		<CF_RelayNavMenu pageTitle="#attributes.pageTitle#" thisDir="#attributes.thisDir#">
			<CF_RelayNavMenuItem MenuItemText="Excel" CFTemplate="#cgi["SCRIPT_NAME"]#?#queryString#&openAsExcel=true">
		</CF_RelayNavMenu>
</cfif>

<!--- NYB 2009-08-03 SNY047 & LHID2604 - replaced default value of "" with "QuizName" - to get sorting to work --->
<cfparam name="sortOrder" default="QuizName">
<cfparam name="numRowsPerPage" default="50">
<cfparam name="startRow" default="1">
<cfparam name="dateFormat" type="string" default="">

<cfparam name="keyColumnList" type="string" default=""><!--- this can contain a list of columns that can be edited --->
<cfparam name="keyColumnURLList" type="string" default=""><!--- this can contain a list of matching columns that contain the URL of the editor template --->
<cfparam name="keyColumnKeyList" type="string" default=""><!--- this can contain a list of matching key columns e.g. primary key --->

<cfparam name="checkBoxFilterName" type="string" default="">
<cfparam name="checkBoxFilterLabel" type="string" default="">

<cfparam name="FilterSelect1" type="string" default="">

<!--- 2009-02-27 NJH Bug Fix All Sites Issue 1899 - changes avgScore to averageScore --->
<CFQUERY name="getReportData" DATASOURCE="#application.SiteDataSource#">
	select * from (SELECT     
		qd.title_defaultTranslation as QuizName,
	cast(AVG(qt.Score) as decimal(12,2)) AS AverageScore, qt.Passmark, cast(MAX(qt.Score) as decimal(12,2)) AS MaxScore, cast(MIN(qt.Score) as decimal(12,2)) AS MinScore,
	qd.active as quiz_active
	FROM dbo.QuizDetail qd with(nolock) INNER JOIN
         dbo.QuizTaken qt with(nolock) ON qd.QuizDetailID = qt.QuizDetailID
	GROUP BY qd.quizDetailID, qt.Passmark,qd.title_defaultTranslation,qd.active
	union
		select f.name as quizName,
		cast(AVG(qt.Score) as decimal(12,2)) AS AverageScore, qt.Passmark, cast(MAX(qt.Score) as decimal(12,2)) AS MaxScore, cast(MIN(qt.Score) as decimal(12,2)) AS MinScore,
		1 as active
		FROM  dbo.QuizTaken qt with(nolock)
			inner join trngUserModuleProgress tump with (noLock) on qt.userModuleProgressID = tump.userModuleProgressID
			inner join trngCoursewareData d with (noLock) on tump.moduleId = d.moduleID
			inner join trngModule m with (noLock) on tump.moduleID = m.moduleID
			inner join files f with (noLock) on f.fileID = m.fileID
		where qt.quizDetailID = 0
		GROUP BY qt.Passmark,m.fileID,f.name
	) as basequery
	where 1 = 1
	<cfinclude template = "/templates/tableFromQuery-QueryInclude.cfm">
	order by <cf_queryObjectName value="#sortOrder#">
</CFQUERY>
	
<CF_tableFromQueryObject 
	queryObject="#getReportData#"
	queryName="getReportData"
	sortOrder = "#sortOrder#"
	openAsExcel = "#openAsExcel#"
	numRowsPerPage="#numRowsPerPage#"
	startRow="#startRow#"
	keyColumnList="#keyColumnList#"
	keyColumnURLList="#keyColumnURLList#"
	keyColumnKeyList="#keyColumnKeyList#"
	ShowTheseColumns="quizName,passMark,AverageScore,MinScore,MaxScore,Quiz_active"
	FilterSelectFieldList="QuizName,Quiz_active"
	allowColumnSorting="yes"
	columnTranslation="true"
	ColumnTranslationPrefix="phr_elearning_"
	booleanFormat="quiz_active"
>