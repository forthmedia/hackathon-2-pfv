<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			eLearningTopHead.cfm
Author:
Date started:
Description:

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2010/09/15			NAS			P-LEN022 add 'Active' to only retrieve Modules for Active Courses
2010/09/22			PPB			P-LEN022 add 'Active' to only retrieve Modules for Active Courses
2012-04-03			Englex      P-REL109 - add training Programs menu
2012-11-19			YMA			CASE:432010 changed report to filter to available / unavailable modules rather than courses
2012-12-10			IH			Case 432480 remove duplicate excel link from certifications.cfm
2016/01/06			NJH			JIRA BF-171  - show inactive training paths
Possible enhancements:

 --->
<CFPARAM NAME="current" TYPE="string" DEFAULT="trainingHome.cfm">

<cfparam name="attributes.templateType" type="string" default="list">
<cfparam name="pageTitle" type="string" default="">
<cfparam name="attributes.pagesBack" type="string" default="1">
<cfparam name="attributes.thisDir" type="string" default="">
<cfparam name="navInclude" type="string" default="">
<!--- 2010/09/15			NAS			P-LEN022 add 'Active' to only retrieve active/inactive courses --->
<!--- 2010/09/22			PPB			P-LEN022 also used for modules.cfm to only retrieve modules for active/inactive courses --->
<cfparam name="url.active" type="Boolean" default="1">
<cfif application.com.settings.getSetting("elearning.useTrainingProgram")
	and (findNoCase("modules.cfm",SCRIPT_NAME)
			or findNoCase("courses.cfm",SCRIPT_NAME)
				or findNoCase("moduleSets.cfm",SCRIPT_NAME)
					or findNoCase("certifications.cfm",SCRIPT_NAME)
						or findNoCase("specialisations.cfm",SCRIPT_NAME)
							or findNoCase("certification_summary.cfm",SCRIPT_NAME)
								or findNoCase("course_summary.cfm",SCRIPT_NAME))>
		<cfset navInclude="/elearning/trainingProgramSelector.cfm">
</cfif>

<cfif not structKeyExists(url,"editor")>
	<CF_RelayNavMenu pageTitle="#pageTitle#" thisDir="#attributes.thisDir#" navInclude="#navInclude#">

	<CFIF findNoCase("courses.cfm",SCRIPT_NAME)>
		<CF_RelayNavMenuItem MenuItemText="Add" CFTemplate="courses.cfm?editor=yes&add=yes">
		<CF_RelayNavMenuItem MenuItemText="Excel" CFTemplate="javascript:openAsExceljs()">
		<cfif url.active EQ 1>
			<CF_RelayNavMenuItem MenuItemText="Inactive Courses" CFTemplate="courses.cfm?active=0">
		<cfelse>
			<CF_RelayNavMenuItem MenuItemText="Active Courses" CFTemplate="courses.cfm?active=1">
		</cfif>
	</CFIF>

	<CFIF findNoCase("modules.cfm",SCRIPT_NAME)>
		<CF_RelayNavMenuItem MenuItemText="Add" CFTemplate="modules.cfm?editor=yes&add=yes">
		<CF_RelayNavMenuItem MenuItemText="Excel" CFTemplate="javascript:openAsExceljs()">
		<!--- 2010/09/22			PPB			P-LEN022 add 'Active' to only retrieve modules for active/inactive courses --->
		<!--- 2012-11-19	YMA		CASE:432010 changed report to filter to available / unavailable modules rather than courses --->
		<cfif url.available>
			<CF_RelayNavMenuItem MenuItemText="phr_report_modules_UnavailableModules" CFTemplate="modules.cfm?available=0">
		<cfelse>
			<CF_RelayNavMenuItem MenuItemText="phr_report_modules_AvailableModules" CFTemplate="modules.cfm?available=1">
		</cfif>
	</CFIF>

	<!--- NJH 2015/08/12 JIRA PROD2015-19 Suggest a training path for a user to follow. --->
	<CFIF findNoCase("trainingPaths.cfm",SCRIPT_NAME)>
		<CF_RelayNavMenuItem MenuItemText="Add" CFTemplate="trainingPaths.cfm?editor=yes&add=yes">
		<CF_RelayNavMenuItem MenuItemText="Excel" CFTemplate="javascript:openAsExceljs()">
		<cfif url.active>
			<CF_RelayNavMenuItem MenuItemText="phr_elearning_menu_showInActiveTrainingPaths" CFTemplate="trainingPaths.cfm?active=0">
		<cfelse>
			<CF_RelayNavMenuItem MenuItemText="phr_elearning_menu_showActiveTrainingPaths" CFTemplate="trainingPaths.cfm?active=1">
		</cfif>
	</CFIF>

	<!--- PJP 05/12/2012: CASE 432430: renamed .cfm file to correct one so that inactive/active tabs now appear --->
	<CFIF findNoCase("certifications.cfm",SCRIPT_NAME)>
		<CF_RelayNavMenuItem MenuItemText="Add" CFTemplate="certifications.cfm?editor=yes&add=yes">
		<CF_RelayNavMenuItem MenuItemText="Excel" CFTemplate="javascript:openAsExceljs()">
		<cfif url.active EQ 1>
			<CF_RelayNavMenuItem MenuItemText="Inactive Certifications" CFTemplate="certifications.cfm?active=0">
		<cfelse>
			<CF_RelayNavMenuItem MenuItemText="Active Certifications" CFTemplate="certifications.cfm?active=1">
		</cfif>
	</CFIF>

	<CFIF findNoCase("course_summary.cfm",SCRIPT_NAME)>
		<CF_RelayNavMenuItem MenuItemText="Excel" CFTemplate="javascript:openAsExceljs()">
		<cfif url.active EQ 1>
			<CF_RelayNavMenuItem MenuItemText="Inactive Courses" CFTemplate="course_summary.cfm?active=0">
		<cfelse>
			<CF_RelayNavMenuItem MenuItemText="Active Courses" CFTemplate="course_summary.cfm?active=1">
		</cfif>
	</CFIF>

	<!--- NJH 2008/08/19 Elearning 8.1 --->
	<CFIF findNoCase("moduleSets.cfm",SCRIPT_NAME)>
		<CF_RelayNavMenuItem MenuItemText="Add" CFTemplate="moduleSets.cfm?editor=yes&add=yes">
		<CF_RelayNavMenuItem MenuItemText="Excel" CFTemplate="javascript:openAsExceljs()">
	</CFIF>

	<CFIF findNoCase("courseContent",SCRIPT_NAME)>
		<cfset fileTypeGroupIDParamString = "">
		<cfif isDefined("FiletypeGroupID")>
			<cfset fileTypeGroupIDParamString= "FileTypeGroupID=#FiletypeGroupID#">
		</cfif>
		<CF_RelayNavMenuItem MenuItemText="Upload New Resource" CFTemplate="/filemanagement/Files.cfm?#fileTypeGroupIDParamString#&editor=yes&add=yes&hideBackButton=false&fileID=0" target="_self">
	</CFIF>

	<CFIF findNoCase("courseCertification.cfm",SCRIPT_NAME) >
		<CF_RelayNavMenuItem MenuItemText="Add" CFTemplate="courseCertification.cfm?editor=yes&add=yes">
		<CF_RelayNavMenuItem MenuItemText="Excel" CFTemplate="javascript:openAsExceljs()">
	</CFIF>

	<!--- NJH 2008/08/19 Elearning 8.1 --->
	<CFIF findNoCase("quizSetup.cfm",SCRIPT_NAME) >
		<CF_RelayNavMenuItem MenuItemText="Add" CFTemplate="quizSetup.cfm?editor=yes&add=yes">
		<CF_RelayNavMenuItem MenuItemText="Excel" CFTemplate="javascript:openAsExceljs()">
		<cfif url.active EQ 1>
			<CF_RelayNavMenuItem MenuItemText="Inactive Quizzes" CFTemplate="quizSetup.cfm?active=0">
		<cfelse>
			<CF_RelayNavMenuItem MenuItemText="Active Quizzes" CFTemplate="quizSetup.cfm?active=1">
		</cfif>
	</CFIF>

	<!--- NJH 2008/09/08 Elearning 8.1 --->
	<CFIF findNoCase("specialisations.cfm",SCRIPT_NAME) >
		<CF_RelayNavMenuItem MenuItemText="Add" CFTemplate="specialisations.cfm?editor=yes&add=yes">
		<CF_RelayNavMenuItem MenuItemText="Excel" CFTemplate="javascript:openAsExceljs()">
		<cfif url.active EQ 1>
			<CF_RelayNavMenuItem MenuItemText="Inactive Specializations" CFTemplate="specialisations.cfm?active=0">
		<cfelse>
			<CF_RelayNavMenuItem MenuItemText="Active Specializations" CFTemplate="specialisations.cfm?active=1">
		</cfif>
	</CFIF>

	<!--- START: 2012-04-03 - Englex - P-REL109 - add training Programs menu --->
	<CFIF findNoCase("trainingPrograms.cfm",SCRIPT_NAME) >
		<CF_RelayNavMenuItem MenuItemText="Add" CFTemplate="trainingPrograms.cfm?editor=yes&add=yes">
		<CF_RelayNavMenuItem MenuItemText="Excel" CFTemplate="javascript:openAsExceljs()">
	</CFIF>
	<!--- END: 2012-04-03 - Englex - P-REL109 - add training Programs menu --->

	<!--- BEGIN 2012-04-19 - STCR - P-REL109 - add Question Pool menu --->
	<CFIF findNoCase("questionPools.cfm",SCRIPT_NAME) >
		<CF_RelayNavMenuItem MenuItemText="Add" CFTemplate="questionPools.cfm?editor=yes&add=yes">
		<CF_RelayNavMenuItem MenuItemText="Excel" CFTemplate="javascript:openAsExceljs()">
	</CFIF>
	<!--- END 2012-04-19 - STCR - P-REL109 - add Question Pool menu --->

	<!--- BEGIN 2012-05-02 - STCR - P-REL109 - add Question editor menu --->
	<CFIF findNoCase("questions.cfm",SCRIPT_NAME) >
		<cfif isDefined("questionPoolID") and isNumeric(questionPoolID) and questionPoolID gt 0>
			<CF_RelayNavMenuItem MenuItemText="Add" CFTemplate="questions.cfm?editor=yes&add=yes&questionPoolID=#questionPoolID#">
		</cfif>
		<CF_RelayNavMenuItem MenuItemText="Excel" CFTemplate="javascript:openAsExceljs()">
		<cfif isDefined("questionPoolID") and isNumeric(questionPoolID) and questionPoolID gt 0>
			<CF_RelayNavMenuItem MenuItemText="phr_elearning_Bulk_UploadQuestions" CFTemplate="questionPoolUpload.cfm?questionPoolID=#questionPoolID#">
		</cfif>
		<cfif isDefined("questionPoolID") and isNumeric(questionPoolID) and questionPoolID gt 0>
			<CF_RelayNavMenuItem MenuItemText="phr_elearning_QuestionPool" CFTemplate="questionPools.cfm?editor=yes&questionPoolID=#questionPoolID#">
		</cfif>
	</CFIF>
	<!--- END 2012-04-19 - STCR - P-REL109 - add Question editor menu --->

	<!--- BEGIN 2012-05-16 - STCR - P-REL109 - Bulk Upload options --->
	<CFIF findNoCase("questionPoolUpload.cfm",SCRIPT_NAME) >
		<cfif isDefined("filCSVFile") and isDefined("questionPoolID") and isNumeric(questionPoolID) and questionPoolID gt 0>
			<CF_RelayNavMenuItem MenuItemText="phr_elearning_Bulk_UploadMore" CFTemplate="questionPoolUpload.cfm?questionPoolID=#questionPoolID#">
		</cfif>
		<cfif isDefined("filCSVFile") and isDefined("questionPoolID") and isNumeric(questionPoolID) and questionPoolID gt 0>
			<CF_RelayNavMenuItem MenuItemText="phr_elearning_Manage_Questions" CFTemplate="questions.cfm?questionPoolID=#questionPoolID#">
		</cfif>
		<cfif isDefined("filCSVFile") and isDefined("questionPoolID") and isNumeric(questionPoolID) and questionPoolID gt 0>
			<CF_RelayNavMenuItem MenuItemText="phr_elearning_QuestionPool" CFTemplate="questionPools.cfm?editor=yes&questionPoolID=#questionPoolID#">
		</cfif>
	</CFIF>
	<!--- END 2012-05-16 - STCR - P-REL109 - Bulk Upload options --->

	<CFIF listFindNoCase("reportTraining_CourseModule.cfm,reportTraining_Course.cfm,reportTraining_specialisationSummary.cfm,reportTraining_certificationSummary.cfm,reportTraining_certificationProgress.cfm,reportTraining_UserCourseQuiz.cfm,reportTraining_UserCourseModulesCompleted.cfm",listLast(SCRIPT_NAME,"/")) >
		<CF_RelayNavMenuItem MenuItemText="Excel" CFTemplate="javascript:openAsExceljs()">
	</CFIF>

	</CF_RelayNavMenu>
</cfif>

