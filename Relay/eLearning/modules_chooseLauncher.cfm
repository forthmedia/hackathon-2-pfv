<!--- Author DCC 2015-29-04 scormadditions.cfm -
used to present user with dropdown of possible launch files from their zip
package once they have saved it and set it as scorm CASE

Date		Initials	Changes
2015/07/05  DCC			Case 444344 moved dropdown creation to scorm and non scorm and other tweeks
						With some mods by WAB so that it could be included as a 'field' by XMLEditor
						eg. <field control="includeTemplate" template="\elearning\scormadditions.cfm"></field>
--->

<!--- Picking up the moduleID from the caller scope is a bit nasty (very specific to XMLEditor), but it seems to work,
	NJH 2016/02/02 changed from caller scope to form scope... --->
<cfset moduleID = form.moduleID>

<cfif isNumeric(ModuleID)>
	<cfif request.relayCurrentUser.isInternal>

		<cf_head>
			<script>
				function setfilelauncher(v){
					if (v === undefined ) {
				        return;
				    } else {
						jQuery("[ID='AICCFilename']").val(v.value);
					}
				}
			</script>
		</cf_head>

		<cfquery name="getdetails" datasource="#application.siteDataSource#">
			SELECT
			ft.secure as secure,
			f.fileid as fileid,
			tm.AICCFileName,
			tm.isScorm,
			tm.moduleId,
			ft.autounzip
			FROM files f
			inner join fileType ft with (noLock) on f.fileTypeID=ft.fileTypeID
			left outer join trngModule tm with (noLock) on f.fileid = tm.fileid
			WHERE tm.moduleid = <cf_queryparam value="#moduleID#" cfsqltype="cf_sql_integer">
		</cfquery>

		<cfif getdetails.recordCount AND getdetails.fileid NEQ "">
			<cfif getdetails.isScorm EQ 1>
				<cfset scormDetail = application.com.relayElearning.SCORMParseManifest(ModuleID=moduleId) />
				<cfif isdefined("scormdetail")>
					<cfif NOT (structKeyExists(scormDetail,"rw") and structKeyExists(scormDetail.rw,"launcher") and len(trim(scormDetail.rw.launcher)) GT 0)>
						<cfoutput>
							<div class='warningblock'><cfif getdetails.autounzip EQ 0>phr_elearning_scorm_unziperror<cfelse>phr_elearning_scorm_nomanifest</cfif></div></cfoutput>
					</cfif>
				</cfif>
			</cfif>

			<cfset getfile = application.com.fileManager.getFileAttributes(fileid=getdetails.fileid)>

			<cfif getfile.extension EQ "zip">

				<cfzip file="#getfile.physicallocation#" action="list" name="SCORMFiles" recurse="true" filter="*.html,*.htm">

				<!--- Show a dropdown of files in the zip --->
				<cfif SCORMFiles.recordcount NEQ 0>
					<cfoutput>
						phr_scorm_possiblescormlaunchfiles<br />
						<select name="files" id="AICCFilename2" onchange="setfilelauncher(this)">
							<option value="" >Please select a launch file</option>
							<cfloop query="#SCORMFiles#">
								<option value="#name#" <cfif isDefined("getdetails.AICCFilename") AND #getdetails.AICCFilename# EQ #name# >selected=selected</cfif>>#name#</option>
							</cfloop>
						</select>
					</cfoutput>
				</cfif>
			</cfif>
		</cfif>
	</cfif>
</cfif>