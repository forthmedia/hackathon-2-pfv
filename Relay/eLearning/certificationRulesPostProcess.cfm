<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
Filename:	certificationRulesPostProcess.cfm

Version History
Date		Initals		Details
2013-10-02	PPB			Case 437262 new file
2013-11-12 	PPB 		Case 437635 prevent sending email if the rule is not activated yet (many changes - release whole file)
2013-12-05 	PPB 		Case 438261 extended mechanism to trigger on other Rule changes and always pop-up "send emails?" question if updating/sending emails

What it does:
Fire off updatePersonCertifications if :
the active field was changed to active 
OR the active field was changed to inactive
OR the activation date was in the future and is now in range 
OR the activation date was in range and is now in the future 
OR ANY OTHER FORM CHANGES

NB some examples of when we wouldn't do an update/send email: 
if the only change the user makes is to change the activation date from a date in the future to another date in the future, there is no need to do the update/emails
if the only change the user makes is to change the activation date from a date in the future to today or in the past but the Rule remains inactive, there is no need to do the update/emails

 --->

<cfset updatePersonCertificationsRequired = false>

<cfset originalDateInRange = (form.activationDate_orig lte request.requestTime)>
<cfset dateInRange = (form.activationDate lte request.requestTime)>

<cfif  (form.active_orig eq 1 AND form.active eq 0 AND originalDateInRange) 
	OR (form.active_orig eq 0 AND form.active eq 1 AND dateInRange)
	OR (form.active_orig eq 1 AND form.active eq 1 AND dateInRange neq originalDateInRange)
	<!--- START 2013-12-05 PPB Case 438261 extended mechanism to trigger on other Rule changes --->
	OR (form.active eq 1 AND dateInRange AND form.certificationRuleTypeID_orig NEQ form.certificationRuleTypeID )		<!--- probably this never changes but added just in case --->
	OR (form.active eq 1 AND dateInRange AND form.numModulesToPass_orig NEQ form.numModulesToPass )
	OR (form.active eq 1 AND dateInRange AND form.studyPeriod_orig NEQ form.studyPeriod )
	OR (form.active eq 1 AND dateInRange AND form.ModuleSetID_orig NEQ form.ModuleSetID )
	<!--- END 2013-12-05 PPB Case 438261 --->
	>
	<cfset updatePersonCertificationsRequired = true>
</cfif>


<cftry>

	<cfset returnURL = "/elearning/certificationRules.cfm?certificationID=#certificationID#">		<!--- 2013-11-12 PPB Case 437635 I don't use backform because the url parameters can get added to it multiple times so this saves stripping them off --->
	
	<cfoutput>
	<cfif updatePersonCertificationsRequired>
		<script type="text/javascript">
			var sendEmails = false;
			//if ('#dateInRange#'=='YES') {		// 2013-12-05 PPB Case 438261 always pop up question and let the user decide whether to send emails
				sendEmails = confirm('phr_elearning_CertificationRuleChangedSendEmails');		
			//}									// 2013-12-05 PPB Case 438261
			
			location.href = '#returnURL#' + '&frmCertificationRuleTypeID=#form.CertificationRuleTypeID#&updatePersonCertifications=true&sendEmails=' + sendEmails
		</script>
	<cfelse>	
		<script type="text/javascript">
			location.href = '#returnURL#'
		</script>
	</cfif>
	</cfoutput>

	<cfcatch>
		<cfdump var="#cfcatch#">
	</cfcatch>
</cftry>