<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			Report_Quiz5.cfm	
Author:				A Developer
Date started:		The Olden Days
	
Description:		Quizzes by Partner

Amendment History:

Date (DD-MM-YYYY)	Initials 	What was changed
2008-09-16			AJC			Change the report to use TFQO and also added filters & export to excel
								Added Country to the query, pretty much changed the whole file
2009-02-27			NJH			Bug Fix All Sites Issue 1899 - made column headings translatable
2009-08-03			NYB  		SNY047					
2009-09-30			NYB			LHID2604
2009-10-23			NAS			LID - 2481/2499 Code entered to Order Translatable Columns
					
Possible enhancements:


 --->

<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">
<cfparam name="openAsExcel" type="boolean" default="false">

<cf_title>Report: Quizzes By Partner</cf_title>

<cfif not openAsExcel>		
		<cfparam name="current" type="string" default="index.cfm">
		<cfparam name="attributes.templateType" type="string" default="edit">
		<cfparam name="attributes.pageTitle" type="string" default="">
		<cfparam name="attributes.pagesBack" type="string" default="1">
		<cfparam name="attributes.thisDir" type="string" default="">

		<CF_RelayNavMenu pageTitle="#attributes.pageTitle#" thisDir="#attributes.thisDir#">
			<CF_RelayNavMenuItem MenuItemText="Excel" CFTemplate="#cgi["SCRIPT_NAME"]#?#queryString#&openAsExcel=true">
		</CF_RelayNavMenu>
</cfif>

<cfparam name="sortOrder" default="QuizName">
<cfparam name="numRowsPerPage" default="50">
<cfparam name="startRow" default="1">
<cfparam name="dateFormat" type="string" default="">

<cfparam name="keyColumnList" type="string" default=""><!--- this can contain a list of columns that can be edited --->
<cfparam name="keyColumnURLList" type="string" default=""><!--- this can contain a list of matching columns that contain the URL of the editor template --->
<cfparam name="keyColumnKeyList" type="string" default=""><!--- this can contain a list of matching key columns e.g. primary key --->

<cfparam name="checkBoxFilterName" type="string" default="">
<cfparam name="checkBoxFilterLabel" type="string" default="">

<cfparam name="FilterSelect1" type="string" default="">

<CFQUERY name="getReportData" DATASOURCE="#application.SiteDataSource#">
	select Organisation,QuizName,passmark,country,
		cast(avg(Score) as decimal(12,2)) as averageScore,quiz_active
	from 
		(SELECT distinct o.OrganisationName as Organisation, 
			case when qt.quizDetailID = 0 then f.name else qd.title_defaultTranslation end as QuizName,
			qt.passmark, cast(Score as decimal(12,2)) as score, c.countrydescription as country,
			qd.active as quiz_active
		FROM QuizTaken qt with(nolock) INNER JOIN
			Person p with(nolock) ON qt.PersonID = p.PersonID INNER JOIN
			organisation o with(nolock) ON p.OrganisationID = o.OrganisationID INNER JOIN
			country c with(nolock) ON o.countryID = c.countryID left join
			dbo.QuizDetail qd with(nolock) ON qd.QuizDetailID = qt.QuizDetailID left join
				(trngUserModuleProgress tump with (noLock)
					inner join trngCoursewareData d with (noLock) on d.personID = tump.personID and d.moduleID = tump.moduleID
					inner join trngModule m with (noLock) on tump.moduleID = m.moduleID
					inner join files f with (noLock) on f.fileID = m.fileID)
			on qt.userModuleProgressID = tump.userModuleProgressID
		) as baseQuery
	where 1 = 1
	<cfinclude template = "/templates/tableFromQuery-QueryInclude.cfm">
	GROUP BY Organisation,QuizName,passmark,country,quiz_active
	order by <cf_queryObjectName value="#sortOrder#">
</CFQUERY>
	
<CF_tableFromQueryObject 
	queryObject="#getReportData#"
	queryName="getReportData"
	sortOrder = "#sortOrder#"
	openAsExcel = "#openAsExcel#"
	numRowsPerPage="#numRowsPerPage#"
	startRow="#startRow#"
	keyColumnList="#keyColumnList#"
	keyColumnURLList="#keyColumnURLList#"
	keyColumnKeyList="#keyColumnKeyList#"
	ShowTheseColumns="QuizName,Organisation,Country,Passmark,AverageScore,Quiz_active"
	FilterSelectFieldList="Country,Organisation,Quizname,Quiz_active"
	allowColumnSorting="yes"
	columnTranslation="true"
	ColumnTranslationPrefix="phr_elearning_"
>