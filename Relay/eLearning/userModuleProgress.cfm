<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		userModuleProgress.cfm	
Author:			NJH  
Date started:	02-09-2008
	
Description:	User module functionality		

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->

<cf_translate>

<cfparam name="mode" type="string" default="view">
<cfparam name="personID" type="numeric" default="#request.relayCurrentUser.personID#">
<cfparam name="moduleID" type="numeric" default="0">

<cfif mode eq "add" and moduleID neq 0>
	<cfscript>
		userModuleProgressID = application.com.relayElearning.insertPersonModuleProgress(moduleID=moduleID,personID=personID);
	</cfscript>
	
	<cfif userModuleProgressID eq 0>
		phr_elearning_userModuleInProgress.
	<cfelse>
		phr_elearning_userModuleStarted.
	</cfif>
</cfif>

</cf_translate>
