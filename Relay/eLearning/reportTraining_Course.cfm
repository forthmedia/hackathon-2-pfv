<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
NYB	2009-09-24	LHID2667 - removed totalTimeCourse from showTheseColumns
NJH 2009/10/01 	LID 2684
--->
<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">
<cfparam name="openAsExcel" type="boolean" default="false">

<cf_title>Training Report - By Course Summary</cf_title>

<cfparam name="sortOrder" default="AICCCourseTitle">
<cfparam name="numRowsPerPage" default="50">
<cfparam name="startRow" default="1">

<cfparam name="checkBoxFilterName" type="string" default="">
<cfparam name="checkBoxFilterLabel" type="string" default="">

<cfquery name="getTrngCourseSummary" datasource="#application.siteDataSource#">
	SELECT * from vTrngCourseSummary
		WHERE 1=1
	<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
	order by <cf_queryObjectName value="#sortOrder#"> 
</cfquery>

<table width="100%" cellpadding="3">
	<tr>
		<td><strong>Training Report - By Course Summary</strong></td>
		<td align="right">&nbsp;</td>
	</tr>
	<tr><td colspan="2">Details of all courses</td></tr>
</table>

<!--- NJH 2009/10/01 LID 2684 removed AICCCourseTitle as the FilterSelectFieldList value --->
<CF_tableFromQueryObject 
	queryObject="#getTrngCourseSummary#"
	queryName="getTrngCourseSummary"
	sortOrder = "#sortOrder#"
	numRowsPerPage="#numRowsPerPage#"
	startRow="#startRow#"
	openAsExcel="#openAsExcel#"
 	hideTheseColumns=""
	showTheseColumns="AICCCourseTitle,totalCompletedModules,totalStartedModules,totalNonStartedModules"
	columnTranslation="true"
	dateformat=""
	
	FilterSelectFieldList=""
	GroupByColumns=""
	radioFilterLabel=""
	radioFilterDefault=""
	radioFilterName=""
	radioFilterValues=""
	checkBoxFilterName=""
	checkBoxFilterLabel=""
	allowColumnSorting="yes"
	showCellColumnHeadings="no"
>