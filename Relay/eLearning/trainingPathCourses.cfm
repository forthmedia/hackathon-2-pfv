<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			trainingPathCourses.cfm
Author:				AJC Englex
Date started:		01/05/2012

Description:		Assign Courses to TrainingPath

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2016/01/06			NJH		Jira BF-169 - filter out courses where modules are inactive


Possible enhancements:


 --->

<cfif not Isdefined("AddNew")>

	<cfif structKeyExists(form,"Update") or structKeyExists(form,"Added")>
		<cftransaction>
			<cftry>
				<cfquery name="deleteExistingCoursesFromCourseSetCourse" datasource="#application.siteDataSource#">
					update trngTrainingPathCourse
						set lastUpdated=getDate(),
							lastUpdatedBy=<cfqueryparam cfsqltype="cf_sql_integer" value="#request.relayCurrentUser.userGroupID#">,
							lastUpdatedByPerson=<cfqueryparam cfsqltype="cf_sql_integer" value="#request.relayCurrentUser.personID#">
					where trainingPathID = <cfqueryparam cfsqltype="cf_sql_integer" value="#form.ID#">
						<cfif structKeyExists(form,"SelectedCourseIDs")>
							and CourseID not in (<cf_queryparam cfsqltype="cf_sql_integer" value="#form.SelectedCourseIDs#" list="true">)
						</cfif>

					delete trngTrainingPathCourse where trainingPathID = <cfqueryparam cfsqltype="cf_sql_integer" value="#form.ID#">
						<cfif structKeyExists(form,"SelectedCourseIDs") and form.SelectedCourseIDs neq "">
							and CourseID not in (<cf_queryparam cfsqltype="cf_sql_integer" value="#form.SelectedCourseIDs#" list="true">)
						</cfif>
				</cfquery>
				<cfif structKeyExists(form,"SelectedCourseIDs") and form.SelectedCourseIDs neq "">
						<cfquery name="insertCoursesIntoCourse" datasource="#application.siteDataSource#">
							insert into trngTrainingPathCourse (CourseID,trainingPathID,createdby,created,lastupdated,lastUpdatedBy,lastUpdatedByPerson)
					<cfloop list="#form.SelectedCourseIDs#" index="CourseID">
								select
									<cf_queryparam cfsqltype="cf_sql_integer" value="#CourseID#">,
									<cf_queryparam cfsqltype="cf_sql_integer" value="#form.ID#">,
									<cf_queryparam cfsqltype="cf_sql_integer" value="#request.relayCurrentUser.usergroupid#">,
									GetDate(),
									GetDate(),
									<cf_queryparam cfsqltype="cf_sql_integer" value="#request.relayCurrentUser.usergroupid#">,
									<cf_queryparam cfsqltype="cf_sql_integer" value="#request.relayCurrentUser.personId#">
								where not exists (select 1 from trngTrainingPathCourse where CourseId=<cf_queryparam cfsqltype="cf_sql_integer" value="#CourseID#"> and trainingPathID=<cf_queryparam cfsqltype="cf_sql_integer" value="#form.ID#">)
									<cfif CourseID neq listlast(form.SelectedCourseIDs)>
										union all
									</cfif>
							</cfloop>
						</cfquery>
				</cfif>
				<cftransaction action="commit"/>
				<cfcatch type="Database">
					<cftransaction action="rollback"/>
					<cfoutput>There was an error updating the Courses in the Course set</cfoutput>
				</cfcatch>
			</cftry>
		</cftransaction>
	</cfif>

	<cfsavecontent variable="select_sql">
		<cfoutput>
			c.AICCCourseID,
			c.CourseID,
			isnull(AICCCourseID,'')+': '+ CASE WHEN len(c.title_defaultTranslation) > 100
			THEN left(c.title_defaultTranslation,30) +  ' ... ' + right(c.title_defaultTranslation,30)
			ELSE c.title_defaultTranslation END AS Title,
			c.title_defaultTranslation
		</cfoutput>
	</cfsavecontent>

	<cfquery name="getCoursesNotInCourse" datasource="#application.siteDataSource#">
		select #preserveSingleQuotes(select_sql)# from trngCourse c where 1=0
	</cfquery>

	<cfquery name="getCoursesInCourse" datasource="#application.siteDataSource#">
		SELECT distinct #preserveSingleQuotes(select_sql)#
		FROM trngCourse c
			inner join trngTrainingPathCourse on trngTrainingPathCourse.CourseID = c.CourseID
			inner join trngModuleCourse tmc on tmc.courseID = c.courseID
			inner join trngModule m on tmc.moduleID = m.moduleID
		WHERE trngTrainingPathCourse.TrainingPathID = <cf_queryparam cfsqltype="cf_sql_integer" value="#form.ID#">
			and c.active = 1
			--and trngModule.availability = 1
		order by AICCCourseID,c.title_defaultTranslation
	</cfquery>

	<cfquery name="getFiltersForCourseListing" datasource="#application.siteDataSource#">
		select '' as value, 'phr_elearning_any_letter' as display
		union
		select distinct left(ltrim(rtrim(isnull(AICCCourseID,c.title_defaultTranslation))),1) as value, left(ltrim(rtrim(isnull(AICCCourseID,c.title_defaultTranslation))),1) as display
		from trngCourse c
			inner join trngModuleCourse tmc on tmc.courseID = c.courseID
			inner join trngModule m on tmc.moduleID = m.moduleID
		where c.active = 1
			and m.availability = 1
		order by value
	</cfquery>
	<cfset bindFunction = 'cfc:webservices.relayElearningWS.getCoursesForSelect({SelectedCourseIDs_filter},{SelectedCourseIDs})'>
	<cfset filterQuery = getFiltersForCourseListing>

	<CF_TwoSelectsComboQuery
		    NAME="AllCourses"
		    NAME2="SelectedCourseIDs"
		    SIZE="20"
		    WIDTH="400"
		    FORCEWIDTH="400"
			QUERY1="getCoursesInCourse"
			QUERY2="getCoursesNotInCourse"
			VALUE="CourseID"
			DISPLAY="Title"
			TITLE="title_defaultTranslation"
		    CAPTION="<FONT SIZE=-1><B>phr_elearning_PotentialCourses:</B></FONT>"
		    CAPTION2="<FONT SIZE=-1><B>phr_elearning_CurrentCourses:</B></FONT>"
			UP="No"
			DOWN="No"
			FORMNAME="editorForm"
			bindFunction=#bindFunction#
			filterQuery=#filterQuery#
			filterLabel="phr_elearning_CourseFilterLabel"
			>
<cfelse>
	<cf_includejavascriptonce template = "/javascript/twoselectscombo.js">
</cfif>
