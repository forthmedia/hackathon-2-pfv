﻿<!--- ©Relayware. All Rights Reserved 2014 --->
<!---
File name:			trainingPathAssignment.cfm
Author:				YMA
Date started:		22/04/2014

Description:		Assign a user to follow a trainingPath path. This .cfm is called in a screen.

Date (DD-MMM-YYYY)	Initials 	What was changed
Amendment History:


Date (DD-MMM-YYYY)	Initials 	What was changed
Possible enhancements:


 --->

<cfparam name="sortOrder" default="trainingPath, CourseTitle">
<cfparam name="restrictToUser" default="true">

<cfset result = structNew()>

<cfif structKeyExists(form,"frmAssignTrainingPath")>
	<cfset availableTrainingPath = application.com.trainingPath.getTrainingPathList(sortOrder=sortOrder,restrictToUser=restrictToUser,personID=personID,showOnlyUnAssignedTrainingPath=true)>

	<cfif listfind(valueList(availabletrainingPath.trainingPathID), form.frmTrainingPath)>
		<cfset result = application.com.trainingPath.insertTrainingPathPerson(trainingPathID=form.frmTrainingPath,personID=form.personID)> <!--- inserting a person entity --->
	<cfelse>
		<cfset result.isOK = false>
		<cfset result.message = "Cannot insert TrainingPath ID #form.frmTrainingPath#.  User already has this TrainingPath assigned.">
	</cfif>
</cfif>


<cfif structKeyExists(form,"frmUnAssignTrainingPath")>
	<cfset result = application.com.trainingPath.deleteTrainingPathPerson(trainingPathID=form.frmUnAssignTrainingPathID,personID=form.personID)> <!--- deleting a person entity --->
</cfif>

<cfif structKeyExists(result,"message")>
	<cfset application.com.relayUI.setMessage(message=result.message,type=result.isOK?"success":"error")>
</cfif>

<cfset personID = request.relaycurrentuser.isinternal?frmPersonID:request.relaycurrentuser.personID>
<cfset getData = application.com.trainingPath.getTrainingPathList(sortOrder=sortOrder,restrictToUser=restrictToUser,personID=personID,showOnlyAssignedTrainingPath=true,returnCoursesAsCommaList=true)>

<!--- if the person is has elearning task level 1 they can assign training paths --->
<cfset showAssignUnAssignFunctionality = application.com.login.checkInternalPermissions("eLearningTask","Level1")>

<cfquery dbtype="query" name="getData">
	select *, <cfif showAssignUnAssignFunctionality>'<span class="deleteLink" title="delete">Delete</span>'<cfelse>''</cfif> as del
	from getData where ignoreAssignmentRules = 0
	union
	select *, '' as del
	from getData where ignoreAssignmentRules = 1
</cfquery>

<cfset availableTrainingPath = application.com.trainingPath.getTrainingPathList(sortOrder=sortOrder,restrictToUser=restrictToUser,personID=personID,showOnlyUnAssignedTrainingPath=true)>

<cfquery dbtype="query" name="availableTrainingPath">
	select distinct trainingPathId, trainingPath
	from availableTrainingPath
</cfquery>

<cf_includeJavascriptOnce template="/elearning/js/elearning.js">

<cf_modalDialog identifier=".trainingPath a.smallLink" type="ajax">

<CF_tableFromQueryObject
	queryObject="#getData#"
	queryName="getData"
 	hideTheseColumns=""
	showTheseColumns="trainingPath,CourseTitle,del"
	columnTranslation="true"
	ColumnTranslationPrefix="phr_report_trainingPath_"
	useInclude="false"
	keyColumnList="trainingPath,del"
	keyColumnURLList="/webservices/callWebservice.cfc?wsdl&method=callWebService&webservicename=relayElearningWS&methodName=getTrainingPathInfo&returnFormat=plain&TrainingPathId=,##"
	keyColumnKeyList="trainingPathId, "
	keyColumnOnClickList=" ,trainingPathAssignment.confirmDeletion(##jsStringFormat(trainingPathId)##*comma'##jsStringFormat(trainingPath)##');return false"
	keyColumnOpenInWindowList="no,no"
	HidePageControls="yes"
>

<cfif showAssignUnAssignFunctionality>
	<form name="trainingPathAssignForm" id="trainingPathAssignForm" method="post">
		<cf_relayFormDisplay>
			<cf_relayFormElementDisplay relayFormElementType="HTML" currentValue="" fieldname="" label="" spanCols="true" valueAlign="center">
				<cf_relayFormElement relayFormElementType="hidden" currentValue="#personID#" fieldname="personID" label="">

				<cf_relayFormElement relayFormElementType="select" nulltext="phr_ext_selectavalue" nullvalue="true" currentValue="" fieldname="frmTrainingPath" label="phr_elearning_trainingPath_TrainingPath" display="trainingPath" value="trainingPathID" query="#availableTrainingPath#">
				<cf_relayFormElement relayFormElementType="submit" class="btn btn-primary" currentValue="phr_elearning_trainingPath_assignTrainingPath" fieldname="frmAssignTrainingPath" label="">
			</cf_relayFormElementDisplay>
		</cf_relayFormDisplay>
	</form>

	<form name="trainingPathUnAssignForm" id="trainingPathUnAssignForm" method="post">
		<cf_relayFormElement relayFormElementType="hidden" currentValue="#personID#" fieldname="personID" label="">
		<cf_relayFormElement relayFormElementType="hidden" currentValue="" fieldname="frmUnAssignTrainingPathID" label="">
		<cf_relayFormElement relayFormElementType="hidden" currentValue="" fieldname="frmUnAssignTrainingPath" label="">
	</form>
</cfif>