<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			Report_Quiz3.cfm	
Author:				A Developer
Date started:		The Olden Days
	
Description:		Failures and Passes

Amendment History:

Date (DD-MM-YYYY)	Initials 	What was changed
2008-09-16			AJC			Change the report to use TFQO and also added filters & export to excel
								Pretty much changed the whole file
2009-02-27			NJH			Bug Fix All Sites Issue 1899 - made column headings translatable
2009-08-03			NYB  		SNY047					
2009-09-30			NYB			LHID2604
2009-10-23			NAS			LID - 2481/2499 Code entered to Order Translatable Columns
2010/01/14 			NJH			LID 2774 - added the equal sign to the query to get the number of passes
2012-10-04	WAB		Case 430963 Remove Excel Header 
					
Possible enhancements:


 --->

<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">
<cfparam name="openAsExcel" type="boolean" default="false">

<cf_title>Report: Failures and Passes</cf_title>

<cfif not openAsExcel>
		<cfparam name="current" type="string" default="index.cfm">
		<cfparam name="attributes.templateType" type="string" default="edit">
		<cfparam name="attributes.pageTitle" type="string" default="">
		<cfparam name="attributes.pagesBack" type="string" default="1">
		<cfparam name="attributes.thisDir" type="string" default="">

		<CF_RelayNavMenu pageTitle="#attributes.pageTitle#" thisDir="#attributes.thisDir#">
			<CF_RelayNavMenuItem MenuItemText="Excel" CFTemplate="#cgi["SCRIPT_NAME"]#?#queryString#&openAsExcel=true">
		</CF_RelayNavMenu>
</cfif>

<!--- NYB 2009-08-03 SNY047 & LHID2604- replaced default value of "" with "QuizName" - to get sorting to work --->
<cfparam name="sortOrder" default="QuizDetailID">
<cfparam name="numRowsPerPage" default="50">
<cfparam name="startRow" default="1">
<cfparam name="dateFormat" type="string" default="">

<cfparam name="keyColumnList" type="string" default=""><!--- this can contain a list of columns that can be edited --->
<cfparam name="keyColumnURLList" type="string" default=""><!--- this can contain a list of matching columns that contain the URL of the editor template --->
<cfparam name="keyColumnKeyList" type="string" default=""><!--- this can contain a list of matching key columns e.g. primary key --->

<cfparam name="checkBoxFilterName" type="string" default="">
<cfparam name="checkBoxFilterLabel" type="string" default="">

<cfparam name="FilterSelect1" type="string" default="">

<CFQUERY name="getReportData" DATASOURCE="#application.SiteDataSource#">
	select * from 
	(SELECT
		title_defaultTranslation as QuizName,
	 cast(AVG(qt.Score) as decimal(12,2)) AS AverageScore, qt.Passmark, qt.QuizDetailID,
      (SELECT COUNT(1) FROM quizTaken qt2 with(nolock) WHERE qt2.quizDetailID = qt.QuizDetailID AND qt2.quizStatus='Fail') AS failures,
      (SELECT COUNT(1) FROM quizTaken qt2 with(nolock) WHERE qt2.quizDetailID = qt.QuizDetailID AND  qt2.quizStatus='Pass') AS passes,
		qd.active as quiz_active
		FROM  dbo.QuizTaken qt with(nolock)inner JOIN
            dbo.QuizDetail qd with(nolock) ON qd.QuizDetailID = qt.QuizDetailID
		GROUP BY title_defaultTranslation, qt.Passmark, qt.QuizDetailID,qd.active
	union
	SELECT
		f.name as QuizName,
	 cast(AVG(qt.Score) as decimal(12,2)) AS AverageScore, qt.Passmark, 0,
      (SELECT COUNT(distinct qt2.quiztakenID) FROM quizTaken qt2 with(nolock) 
			inner join trngUserModuleProgress tump2 with (noLock) on qt2.userModuleProgressID = tump2.userModuleProgressID
			inner join trngCoursewareData d2 with (noLock) on tump2.moduleId = d2.moduleID
			inner join trngModule m2 with (noLock) on tump2.moduleId = m2.moduleID
		where qt2.quizStatus = 'Fail' and m2.fileID = m.fileID) AS failures,
      (SELECT COUNT(distinct qt2.quiztakenID) FROM quizTaken qt2 with(nolock) 
			inner join trngUserModuleProgress tump2 with (noLock) on qt2.userModuleProgressID = tump2.userModuleProgressID
			inner join trngCoursewareData d2 with (noLock) on tump2.moduleId = d2.moduleID
			inner join trngModule m2 with (noLock) on tump2.moduleId = m2.moduleID
		where qt2.quizStatus = 'Pass' and m2.fileID = m.fileID) AS passes,
			1 as quiz_active
		FROM  dbo.QuizTaken qt with(nolock)
			inner join trngUserModuleProgress tump with (noLock) on qt.userModuleProgressID = tump.userModuleProgressID
			inner join trngCoursewareData d with (noLock) on tump.moduleId = d.moduleID
			inner join trngModule m with (noLock) on tump.moduleId = m.moduleID
			inner join files f with (noLock) on f.fileID = m.fileID
		where qt.quizDetailID = 0
		GROUP BY qt.Passmark,m.fileID,f.name) x
	where 1 = 1
	<cfinclude template = "/templates/tableFromQuery-QueryInclude.cfm">
	order by <cf_queryObjectName value="#sortOrder#">
</CFQUERY>

<CF_tableFromQueryObject 
	queryObject="#getReportData#"
	queryName="getReportData"
	sortOrder = "#sortOrder#"
	openAsExcel = "#openAsExcel#"
	numRowsPerPage="#numRowsPerPage#"
	startRow="#startRow#"
	keyColumnList="#keyColumnList#"
	keyColumnURLList="#keyColumnURLList#"
	keyColumnKeyList="#keyColumnKeyList#"
	ShowTheseColumns="quizName,passMark,averageScore,failures,passes,quiz_active"
	FilterSelectFieldList="QuizName,quiz_active"
	FilterSelectFieldList2=""
	allowColumnSorting="yes"
	columnTranslation="true"
	ColumnTranslationPrefix="phr_elearning_"
	booleanFormat="quiz_active"
>