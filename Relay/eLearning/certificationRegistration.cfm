<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			relay\elearning\certificationRegistration.cfm
Author:				NJH
Date started:		2008/08/21
Description:		Register person certifications. This file expects to have a wrapper around it with a cfform. (ie a screen)

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2008-01-21			NYB			now shows checkbox as disabled if user has not met the prerequisites for it
2009-02-25			NYB			Sophos - changed statusTextID to status - as was throwing a "no column exists" error
								Sophos Bug 1397 -> added ,personid=personID to getUserEligibility call
2009-08-19			NYB			8.2 bug LHID 2533
2011-03-24			NYB			LHID 4017. Removed <cfoutput>#frmPersonID#</cfoutput> - suspect this was test code not removed.
2013-05-22			YMA			Case 435369 limit certifications by publish date in certification registration relaytag
2013-05-28			STCR		CASE 435369 Modify YMA's change, so that the Portal allows Content Editors to preview Certifications for future dates
2013-11-21			AXA			Case 437312 added sortorder
2015-04-15 			WAB 		CASE 444394 Add Labels to AvailableCertifications checkboxes
2016-01-05          VSN         Case 447179 - dont show the brackets if there is no description
2016-01-11          DAN         Case 447179 - further fix to the above as CASE statement is not supported by QofQ in CF
2016-03-11			WAB			Set relayformDisplay classes to "" (for stacked layout)
Possible enhancements:

 --->

<cfparam name="frmEntityID" default="0" >
<cfparam name="frmEntityTypeID" type="numeric" default="0">
<cfparam name="frmCurrentEntityID" type="numeric" default="0">

<cfparam name="formName" type="string" default="mainForm">
<cfparam name="certificationTypeID" type="numeric" default="0">
<cfparam name="sortOrder" type="string" default="Code,Description">									<!--- 2013-11-21 AXA Case 437312 added sortorder --->
<cf_param name="trainingContactFlagTextID" type="string" default="KeyContactTraining"/>

<!--- START:  NYB 2009-08-19 - 8.2 bug LHID 2533 - added: --->
<cfparam name="frmRegistrationDate" type="date" default="#now()#">
<!--- END:  NYB 2009-08-19 - 8.2 bug LHID 2533 --->

<cfset personID=0>
<cfset request.relayFormDisplayStyle = "HTML-div">

<!--- set the personID if we're showing certifications on a person level --->
<cfif structKeyExists(form,"frmRegisterPersonID")>
	<cfset personID = form.frmRegisterPersonID>
<cfelseif request.relayCurrentUser.isInternal and frmEntityTypeID eq 0 and frmCurrentEntityID neq 0>
	<cfset personID = frmCurrentEntityID>
<cfelseif request.relayCurrentUser.isInternal and frmEntityTypeID eq 0 and frmEntityID neq 0>
	<cfset personID = frmEntityID>
<cfelseif not request.relayCurrentUser.isInternal>
	<cfset personID = request.relayCurrentUser.personID>
</cfif>

<!---
	if we're showing registrations on a person level (ie. not on an org screen where we show all registrations at that org),
	then get available certifications to register for.
 --->
<cfif personID neq 0>

	<cfscript>
		argumentsCollection = structNew();
		argumentsCollection.showActive=true;
		if (isDefined("request.relaycurrentuser.content.date") and isDate(request.relaycurrentuser.content.date)) {
			argumentsCollection.fromActivationDate = request.relaycurrentuser.content.date;
		}
		else {
			argumentsCollection.fromActivationDate = request.requestTime;
		}
		argumentsCollection.countryIDList=request.relaycurrentuser.CountryList;
		argumentsCollection.sortOrder=sortOrder;										//2013-11-21 AXA Case 437312 added sortorder
		if (certificationTypeID neq 0) {
			argumentsCollection.certificationTypeID=certificationTypeID;
		}
		qryCertificationData = application.com.relayElearning.GetCertificationData(argumentCollection=argumentsCollection);
	</cfscript>

	<!--- add the person certification --->
	<cfif structKeyExists(form,"frmInsertCertification")>
		<cfset certificationRegisteredList="">
		<cfloop query="qryCertificationData">
			<cfif isDefined("frmCertificationID#certificationID#")>
				<cfscript>
					registered = application.com.relayElearning.insertPersonCertificationData(certificationID=certificationID,personID=personID,registerDate=frmRegistrationDate);
					if (registered != 0) {
						certificationRegisteredList = listAppend(certificationRegisteredList,certificationID);
					}
				</cfscript>
			</cfif>
		</cfloop>

		<cfif certificationRegisteredList neq "">
			<cfset message = "phr_elearning_TheFollowingCertsHaveBeenRegistered:<br>">
			<cfquery name="getCertificationCodes" dbType="query">
				<!--- 2016-01-11 VSN/DAN Case 447179 --->
                select title, description, code from qryCertificationData where certificationID in (#certificationRegisteredList#)
			</cfquery>
			<cfset message = message & "<ul>">
			<cfoutput query="getCertificationCodes">
                <!--- 2016-01-11 VSN/DAN Case 447179 --->
                <cfset qryDisplay = (description NEQ "" AND description NEQ " .... ") ? "#title# (#description#)" : "#title#">
				<cfset message = message & "<li>" & qryDisplay & "</li>">
			</cfoutput>
			<cfset message = message & "</ul>">
		</cfif>
	</cfif>

	<cfscript>
		qryPersonCertifications = application.com.relayElearning.GetPersonCertificationData(entityID=personID,entityTypeID=0);
		// NYB 2009-01-20 Sophos Bug 1397 - added query:
		qryEligibleCertifications = application.com.relayElearning.getEligibleCertifications();
	</cfscript>

	<cfquery name="getAvailableCertifications" dbType="query">
        <!--- 2016-01-11 VSN/DAN Case 447179 --->
        select title, description, certificationID as qryValue
		from qryCertificationData
		where countryID in (#request.relaycurrentuser.CountryList#,#request.relayCurrentUser.regionListSomeRights#)
		<!---	2012-07-24 PPB P-SMA001 getRightsFilterWhereClause() cannot be used in a QOQ (cos it uses an IN clause) so if more than simple country-scoping is required this should be moved into the main query
		where 1=1 #application.com.rights.getRightsFilterWhereClause(entityType="trngCertification",alias="qryCertificationData",regionList="#request.relayCurrentUser.regionListSomeRights#").whereClause#	<!--- 2012-07-24 PPB P-SMA001 --->
		--->
	</cfquery>

	<cf_relayFormDisplay id="certificationRegistration" class="">

		<!--- if we're a training partner, then register on behalf of someone else on the portal --->
		<cfif (application.com.relayElearning.getTrainingContact(organisationID=request.relayCurrentUser.person.organisationID) eq request.relayCurrentUser.personID) and not request.relayCurrentUser.isInternal>
			<cfquery name="getPeopleAtOrg" datasource="#application.siteDataSource#">
				select personID, firstname+' '+lastname as fullname from person where organisationID = #request.relayCurrentUser.organisationID# order by firstname, lastname
			</cfquery>
			<cfif not isDefined("frmRegisterPersonID")>
				<cfset frmRegisterPersonID = personID>
			</cfif>
			<cf_relayFormElementDisplay relayFormElementType="select" fieldName="frmRegisterPersonID" label="phr_elearning_registerCertificationFor" currentValue="#frmRegisterPersonID#" query="#getPeopleAtOrg#" display="Fullname" value="personID" onChange="javascript:#formName#.submit();">
		<cfelse>
			<cf_relayFormElementDisplay relayFormElementType="hidden" currentValue="#personID#" fieldname="frmRegisterPersonID" label="">
		</cfif>

		<cfif isDefined("message") and message neq "">
			<cf_relayFormElementDisplay relayFormElementType="Message" currentValue="#message#" fieldname="" label="">
		</cfif>

		<cfset validCertificationCount = 0>

		<cf_relayFormElementDisplay  inputWrapperOverrideClass="col-xs-12 col-sm-9 certificateRegistrationDiv" relayFormElementType="HTML" currentValue="" fieldname="" label="phr_elearning_availableCertifications">
			<cfif getAvailableCertifications.recordCount gt 0>
				<cfloop query="getAvailableCertifications">
                    <!--- 2016-01-11 VSN/DAN Case 447179 --->
                    <cfset qryDisplay = (description NEQ "" AND description NEQ " .... ") ? "#title# (#description#)" : "#title#">
					<cfset disabled = false>
					<cfset value="">
					<cfset certificationStatus="">

					<!--- only get those certifications as available certification where the person doesn't have them in the list valid (registered,passed) certifications --->
					<cfquery name="getValidPersonCertifications" dbType="query">
							<!--- NYB 2009-02-25 - Sophos - changed statusTextID to status - as was throwing a "no column exists" error --->
							select * from qryPersonCertifications where statusTextID in ('Registered','Passed','Pending')
							and certificationID = #qryValue#
					</cfquery>

					<cfset checked = "no">
					<cfif getValidPersonCertifications.recordCount gt 0>
						<cfset value=qryValue>
						<cfset disabled = true>
						<cfset checked = "yes">
						<!--- <cfset certificationStatus =" (#getValidPersonCertifications.status#)"> --->
						<cfset validCertificationCount = validCertificationCount+1>
						<!--- NYB 2008-01-21 Sophos Bug 1397 -> added: --->
					<cfelse>
							<!--- NYB 2009-02-26 Sophos Bug 1397 -> added ,personid=personID to getUserEligibility call --->
							<cfset disabled = IIf(application.com.relayElearning.getUserEligibility(CertificationID=qryValue,personid=personID), false, true)>
						<!--- <- NYB 2008-01-21 Sophos Bug 1397 --->
					</cfif>
					<!--- PJP 06/12/2012: CASE 432428: Make sure any html is rendered in the title, but also remove any possible secuirty breach code --->
					<!--- WAB 2015-04-15 CASE 444394 Add Label - no longer supplied by cf_input. Also needed checkbox div for correct layout --->
					<div class="checkbox">
					<cfoutput><label for="frmCertificationID#qryValue#">#application.com.security.sanitiseHTML(qryDisplay)##htmleditformat(certificationStatus)#</label></cfoutput>
						<cf_relayFormElement usecfform="false" label="#application.com.security.sanitiseHTML(qryDisplay)##htmleditformat(certificationStatus)#" relayFormElementType="checkbox" currentValue="#value#" value="#qryValue#" fieldname="frmCertificationID#qryValue#" disabled="#disabled#" checked="#checked#">
					</div>
				</cfloop>
			<cfelse>
				phr_elearning_NoCertificationsForWhichToRegister.
			</cfif>
		</cf_relayFormElementDisplay>

		<!--- START:  NYB 2009-08-19 - 8.2 bug LHID 2533 - removed:
		<cfset regDateElementType = "hidden">
		<cfif request.relayCurrentUser.isInternal>
			<cfset regDateElementType = "date">
		</cfif>
		 END:  NYB 2009-08-19 - 8.2 bug LHID 2533 --->

		<!--- START:  NYB 2009-08-19 - 8.2 bug LHID 2533 - replaced:
		<cf_relayFormElementDisplay relayFormElementType="#regDateElementType#" currentValue="#now()#" fieldname="frmRegistrationDate" label="phr_elearning_RegistrationDate" formName="#formName#">
		 NYB 2009-08-19 - 8.2 bug LHID 2533: --->
		<cfif request.relayCurrentUser.isInternal>
			<cf_relayFormElementDisplay relayFormElementType="date" currentValue="#frmRegistrationDate#" fieldname="frmRegistrationDate" label="phr_elearning_RegistrationDate" formName="#formName#">
		</cfif>
		<!--- END:  NYB 2009-08-19 - 8.2 bug LHID 2533 --->

		<cfset disableRegisterButton=false>
		<cfif validCertificationCount eq getAvailableCertifications.recordCount>
			<cfset disableRegisterButton=true>
		</cfif>

		<cf_relayFormElementDisplay relayFormElementType="HTML" currentValue="" fieldname="" label="" spanCols="true" valueAlign="center">
			<!--- <cf_relayFormElement relayFormElementType="submit" currentValue="phr_Cancel" fieldname="frmCancelRegistration" label=""> --->
			<cf_relayFormElement relayFormElementType="submit" currentValue="phr_Register" fieldname="frmInsertCertification" label="" disabled="#disableRegisterButton#">
		</cf_relayFormElementDisplay>

	</cf_relayFormDisplay>

</cfif>