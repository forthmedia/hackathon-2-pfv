﻿<!--- ©Relayware. All Rights Reserved 2014 --->
<!---
File name:			trainingProgramCheck.cfm
Author:				Englex Limited
Date started:		18/04/2012
Description:		Checks if the selected training program has changed

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
04-07-2014 AHL Case #441111: The case 440744 assumes that trainingmanagerID now is never stored as 0, but as an empty string
2014-10-03			RPW			CORE-760 Error message when trying to view Certifications on  Training
2014-10-03			RPW			CORE-705 Error while trying to "Set up modules" in Training area

Possible enhancements:

 --->

<cfparam name="session.selectedTrainingProgramID" default=""> <!--- 2014/07/04 AHL Case #441111 The case 440744 assumes that trainingmanagerID now is never stored as 0, but as an empty string --->

<cfif isdefined("form.cboTrainingProgramID")>
	<cfset session.selectedTrainingProgramID = form.cboTrainingProgramID>
</cfif>

<cfscript>
	//2014-10-03			RPW			CORE-760 Error message when trying to view Certifications on  Training
	//2014-10-03			RPW			CORE-705 Error while trying to "Set up modules" in Training area
	lock timeout = "1" scope = "Session" throwOnTimeout = "no" type = "exclusive" {
		if (!IsDefined("session.selectedTrainingProgramID") || !IsNumeric(session.selectedTrainingProgramID)) {
			session.selectedTrainingProgramID = 0;
		}
	}
</cfscript>
