<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			trainingProgramSelector.cfm
Author:				Englex Limited
Date started:		18/04/2012
Description:		Used to change the training program selected

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2015/11/30 SB Bootstrap
Possible enhancements:

 --->
<cfset getTrainingProgram = application.com.relayElearning.getTrainingProgram(forDropDown=1,nonePhrase="phr_elearning_trainingProgram_all")>

<cfoutput>
<div class="grey-box-top">
	<div class="row">
		<div class="form-group">
			<div class="col-xs-12 col-sm-12 col-md-6">
				<form name="selectTrainingProgram" method="post" action="#cgi.script_name#">
					<label for="cboTrainingProgramID">phr_elearning_selectTrainingProgram:</label>
					<select name="cboTrainingProgramID" id="cboTrainingProgramID" onchange="document.selectTrainingProgram.submit();" class="form-control">
						</cfoutput><cfoutput query="getTrainingProgram">
							<!--- 2013-07-02	YMA	Case 435926 Check to see if selectTrainingProgramID exists in session. --->
							<option value="#value#"<cfif structKeyExists(session,"selectedTrainingProgramID") and session.selectedTrainingProgramID eq value> selected</cfif>>#display#</option>
						</cfoutput>
					</select>
				</form>
			</div>
		</div>
	</div>
</div>