<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		eligibilityRules.cfm	
Author:			NYB  
Date started:	29-10-2008
	
Description:			

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2009-09-17 			NYB 		LHID2646 - created V2 - COMPLETELY different to V1
2010/11/08			WAB			Added code to delete eligibilty rules
								Also converted to TwoSelectsComboQuery since I was getting an error on the list version (because of a null in a query)
2011-02-10			NYB			LHID5643 - replaced TrngCertification with vTrngCertifications in queries to pick up new Title format
2012-04-17	 		Englex 		P-REL109 - Fixed issue where prerequisites and current were not translated
2012/11/21			NJH			Case 432093 - altered queries to lastUpdated's for modRegister tracking
 --->

	<cfoutput>
		<cfif structKeyExists(form,"Update") or structKeyExists(form,"ADDEDRECORD")>
			<cfif structKeyExists(form,"SelectedCertifications")>
				<cftransaction>
					<cftry>
						<cfquery name="removePrerequisites" datasource="#application.siteDataSource#">
							<cfif SelectedCertifications is not "">
							insert into eligibilityRule 
							(eligibilityRuleTypeID,requiredID,created,createdBy,appliedToID,appliedToTable,lastUpdated,lastUpdatedBy,lastUpdatedByPerson)
							select 1,certificationid,getdate(),<cfqueryparam cfsqltype="cf_sql_integer" value="#request.relaycurrentuser.userGroupID#">,
							<cfqueryparam cfsqltype="cf_sql_integer" value="#form.CertificationID#">,'trngCertification',getDate(),<cfqueryparam cfsqltype="cf_sql_integer" value="#request.relaycurrentuser.userGroupID#">,<cfqueryparam cfsqltype="cf_sql_integer" value="#request.relaycurrentuser.personID#">
							from trngCertification where certificationid  in ( <cf_queryparam value="#SelectedCertifications#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) 
							and certificationid not in 
							(select requiredID from eligibilityRule where appliedToTable='trngCertification' and appliedToID=<cfqueryparam cfsqltype="cf_sql_integer" value="#form.CertificationID#">)
							</cfif>
							
							delete from eligibilityRule
							where 
								eligibilityRuleTypeID = 1
								and	appliedToTable='trngCertification' 
								and appliedToID=<cfqueryparam cfsqltype="cf_sql_integer" value="#form.CertificationID#">
								<cfif SelectedCertifications is not "">
								and requiredid  not in ( <cf_queryparam value="#SelectedCertifications#" CFSQLTYPE="CF_SQL_INTEGER"  list="true"> ) 
								</cfif>
						</cfquery>
						<cftransaction action="commit"/>
						<cfcatch type="Database">
							<cftransaction action="rollback"/>
							<cfoutput>There was an error updating the prerequisites for this Certification.
								<pre>
									insert into eligibilityRule 
									(eligibilityRuleTypeID,requiredID,created,createdBy,appliedToID,appliedToTable,lastUpdated,lastUpdatedBy,lastUpdatedByPerson) 
									select 1,certificationid,getdate(),#htmleditformat(request.relaycurrentuser.USERGROUPID)#,
									#htmleditformat(form.CertificationID)#,'trngCertification',getDate(),#request.relaycurrentuser.userGroupID#,#request.relaycurrentuser.personID# 
									from trngCertification where certificationid in (#htmleditformat(SelectedCertifications)#)									
								</pre>
							</cfoutput>
							<cfdump var="#cfcatch#">
						</cfcatch>
					</cftry>
				</cftransaction>
			<cfelse>
				<cfquery name="removePrerequisites" datasource="#application.siteDataSource#">
					update eligibilityRule set 
						lastUpdated=getDate(),
						lastUpdatedBy=<cfqueryparam cfsqltype="cf_sql_integer" value="#request.relaycurrentuser.userGroupID#">,
						lastUpdatedByPerson=<cfqueryparam cfsqltype="cf_sql_integer" value="#request.relaycurrentuser.personID#">
					 where appliedToTable='trngCertification' and appliedToID=<cfqueryparam cfsqltype="cf_sql_integer" value="#form.CertificationID#">
					 
					delete from eligibilityRule where appliedToTable='trngCertification' and appliedToID=<cfqueryparam cfsqltype="cf_sql_integer" value="#form.CertificationID#">
				</cfquery>
			</cfif>
		</cfif>

		<!--- 2011-02-10 NYB LHID5643 replaced TrngCertification with vTrngCertifications in queries to pick up new Title format  --->
		<cfset cTrainingProgramID = application.com.relayelearning.getCertificationData(certificationID = form.CertificationID).trainingprogramID>
		
		<cfquery name="getCertificationsToApply" datasource="#application.siteDataSource#">
			select certificationID as value, title_defaultTranslation as display 
			from trngCertification
			<cfif structKeyExists(form,"certificationID") and form.certificationID neq "" and form.certificationID neq 0>
				where certificationid! =  <cf_queryparam value="#form.CertificationID#" CFSQLTYPE="CF_SQL_INTEGER" >  and certificationid not in 
				(select requiredID from eligibilityRule where appliedToTable='trngCertification' and appliedToID=<cfqueryparam cfsqltype="cf_sql_integer" value="#form.CertificationID#">)
			</cfif>
			<!--- START: 2012-04-16 - Englex - P-REL109 - if using training programs then filter the list --->
			<cfif application.com.settings.getSetting("elearning.useTrainingProgram")>
				<cfif cTrainingProgramID neq 0 and cTrainingProgramID neq "">
					and trainingprogramID =  <cf_queryparam value="#cTrainingProgramID#" CFSQLTYPE="cf_sql_integer" >
				</cfif>
			</cfif>
			<!--- END: 2012-04-16 - Englex - P-REL109 - if using training programs then filter the list --->
		</cfquery>

		<cfquery name="getCertificationsApplied" datasource="#application.siteDataSource#">
			select certificationID as value, title_defaultTranslation as display
			from trngCertification where 
			<cfif structKeyExists(form,"certificationID") and form.certificationID neq "" and form.certificationID neq 0>
				certificationid in 
				(select requiredID from eligibilityRule where appliedToTable='trngCertification' and appliedToID=<cfqueryparam cfsqltype="cf_sql_integer" value="#form.CertificationID#">)
			<cfelse>
				1=2
			</cfif>
		</cfquery>

		<cf_includejavascriptonce template = "/javascript/twoselectscombo.js">

			<CF_TwoSelectsComboQuery
		    NAME="AllCertifications"
		    NAME2="SelectedCertifications"
		    SIZE="10"
		    WIDTH="400"
	    	ForceWidth="100"
			Query2="getCertificationsToApply"
		    Query1="getCertificationsApplied"
			value = "value"
			display = "display"
		    CAPTION="<FONT SIZE=-1><B>phr_elearning_PotentialPrerequisites:</B></FONT>"
		    CAPTION2="<FONT SIZE=-1><B>phr_elearning_CurrentPrerequisites:</B></FONT>"
			UP="No"
			DOWN="No"
			FORMNAME="editorForm"
			DELIM="|"
			>		
				
	</cfoutput>