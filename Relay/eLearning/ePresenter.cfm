<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		ePresenter.cfm	
Author:			SWJ  
Date started:	11-09-2007
	
Description:	Version 1 of a presentation tool that uses CFPresentation and gets its data from 
				Relayware content editor		

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->


<A HREF="javascript:history.go(-1);" Class="Submenu">Back</A>

<cfinvoke component="relay.com.relayElementTree" method="getTreeChildNodes" returnvariable="qPages">
	<cfinvokeargument name="parentID" value="1958">
</cfinvoke>

<cfdump var="#qPages#">

<style>
	body {
	margin: 0;
	padding: 0;
	font-family: verdana, arial, helvetica, sans-serif;
	font-size: 76%;/* font sizing in ems, baby. if you want to change anything, just change this.*/
	/*funny thing happens at 75% and lower. opera goes to a nice small size, but moz and ie pc change almost not at all. seems 76% is as small as you can go and stay the same across browsers. poop.*/
	color: #000;
	background-color: #fff;
	background-image: url(/images/misc/blank.gif);/*these three lines replace the n4_upgrade.gif in n4.css. and if you use this code, make sure you point to an img on _your_ server, not mine.*/
	background-repeat: no-repeat;
	background-position: top left;
	}
	h1, h2, h3, h4, h5, h6, {color: #006;}

	h1 {
	font-size: 1.8em;
	font-weight: normal;
	margin-top: 0em;
	margin-bottom: 0em;/*both set to zero and padding in header div is used intead to deal with compound ie pc problems that are beyound summary in a simple comment.*/
	}

</style>

<!--- The following code determines the look of the slide presentation. ColdFusion displays the slide presentation directly in the browser because no destination is specified. The title appears above the presenter information. --->
<cfpresentation title="eLearning" showOutline="yes" showNotes="yes" primaryColor="##0000FF" 
				glowColor="##FF00FF" lightColor="##FFFF00">

<!--- The following code defines the presenter information. You can assign each presenter to one or more slides. --->
    <cfpresenter name="Mike Morgan" email="mike.morgan@foundation-network.com">

<!--- The following code defines the content for the first slide in the presentation. The duration of the slide determines how long the slide plays before proceeding to the next slide. The audio plays for the duration of the slide. --->

	<cfoutput query="qPages">
		<cfquery name="getDetail" datasource="#application.siteDataSource#">
			exec TranslateEntityPhrases @entityIDList =  <cf_queryparam value="#qPages.nodeID#" CFSQLTYPE="CF_SQL_INTEGER" > , @entityType = 'element', @PhraseTextIDList = 'detail,headline' 
		</cfquery>
		
		<cfpresentationslide title="#nodetext#" presenter="Mike Morgan">
			<cfif len(getDetail.phraseText) gt 1>
				<h1 style="color: ##006; font-family: verdana, arial, helvetica, sans-serif;">
				<img src="../test/CF8/assets/FNL-logo.jpg" alt="" width="70" height="87" border="0">
				#htmleditformat(NODETEXT)#</h1>
				#htmleditformat(getDetail.phraseText)#
			<cfelse>
				<strong>Need content</strong>
			</cfif>
	    </cfpresentationslide>
	</cfoutput>
<!--- 		<cfpresentationslide title="Slide 3" presenter="Aiden">
			<cfform  name="testForm">
				<a href="http://www.foundation-network.com">Link</a>
				Check box <cfinput name="id" type="checkbox">
			</cfform>
	    </cfpresentationslide>
<cfpresentationslide title="Slide 4" src="http://www.foundation-network.com">
			
	    </cfpresentationslide> --->
</cfpresentation> 
 


