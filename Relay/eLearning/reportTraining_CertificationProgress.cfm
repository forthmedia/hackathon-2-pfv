<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		reportTraining_CertificationProgress.cfm	
Author:			NJH  
Date started:	20-08-2008
	
Description:	Displays modules passed within Certifications for individuals with a range of filter criteria to provide:
				a) Modules passed per Person within Partner
				b) Credits earned per Person within Partner
				c) Modules passed per Certification



Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2008-11-11 	NYB	P-SOP010 - renamed Fullname to Name, replaced [Certification] Code & Descriptiion with [Certification] Title
2012-01-31 	NYB	Case 426399 noticed while working on this bug that this report was timing out every time.  Have add an increased page timeout and 15 minute cache to improve this
2012-09-20	IH	Case 430705 remove duplicate Excel link
2013-01-10 	YMA	Case 433060 enhance report by bringing back translated title rather then phrase in sql query

Possible enhancements:


 --->

<cfsetting requestTimeout="3600" />

<cfparam name="useFullRange" type="boolean" default="true">
<cfparam name="openAsExcel" type="boolean" default="false">
<cfparam name="sortOrder" default="Organisation,Name,Certification_Code">
<cfparam name="numRowsPerPage" default="50">
<cfparam name="startRow" default="1">
<!--- START: AJC 2008-11-28 SOP011 - Was displaying incorrect informating --->
<cfparam name="useMyTeams" type="boolean" default="false">
<!--- END: AJC 2008-11-28 SOP011 - Was displaying incorrect informating --->

<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">

<cf_title>phr_elearning_CertificationProgress</cf_title>

<!--- 2013-01-10 	YMA	Case 433060 --->
<cfquery name="getTrngCertificationProgress" datasource="#application.siteDataSource#" cachedwithin="#createTimeSpan(0,0,15,0)#">
	select * from (
		select distinct organisationName as organisation, o.countryID,c.countryDescription as country, firstname + ' ' + lastname as [Name],
	 		certificationCode as Certification_Code, certificationTitle as Certification, certificationDescription as certification_description, 
	 		v.moduleCode as module_code, tm.title_defaultTranslation as [Module],v.credits as credits_earned,
	 		(select max(score) from quizTaken qt with (noLock) inner join trngUserModuleProgress tump with (noLock) 
				on qt.userModuleProgressId = tump.userModuleProgressID 
			where tump.moduleID = v.moduleID and v.personID = tump.personID and v.userModuleFulfilled = tump.userModuleFulfilled) as quiz_score
			<cfif useMyTeams>
				,teamID
			</cfif>
	 	from vtrngPersonCertifications v inner join person p with (noLock) on v.personID = p.personID
			inner join trngmodule tm on v.moduleID = tm.moduleID
			inner join organisation o with (noLock) on p.organisationID = o.organisationID
			inner join country c with (noLock) on c.countryID = o.countryID
			<cfif useMyTeams>
				inner join MyTeamMember mtm with (noLock) ON p.personid = mtm.personid
				inner join MyTeam mt with (noLock) ON mtm.TeamMemberID = mt.Teamid 
			</cfif>
	) as base
	<!--- 	2012-07-25 PPB P-SMA001 commented out	
	where countryID in (#request.relayCurrentUser.countryList#)
	 --->
	where 1=1 #application.com.rights.getRightsFilterWhereClause(entityType="trngPersonCertification",alias="base").whereClause#		<!--- 2012-07-25 PPB P-SMA001 --->
	<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
	group by Organisation,CountryID,Country,Name,Certification_Code,Certification,certification_description,Module_Code,Module,Quiz_Score,Credits_Earned
	<cfif useMyTeams>
		,teamID
	</cfif>
	order by <cf_queryObjectName value="#sortOrder#"> 
</cfquery>

<table width="100%" cellpadding="3">
	<tr>
		<td><strong>phr_elearning_CertificationProgress</strong></td>
		<td align="right">&nbsp;</td>
	</tr>
</table>

<!--- NYB 2008-11-10 P-SOP010 - replaced Certification_Description with  --->
<CF_tableFromQueryObject 
	queryObject="#getTrngCertificationProgress#"
	queryName="getTrngCertificationProgress"
	sortOrder = "#sortOrder#"
	numRowsPerPage="#numRowsPerPage#"
	startRow="#startRow#"
	openAsExcel="#openAsExcel#"
 	hideTheseColumns=""
	showTheseColumns="Organisation,Country,Name,Certification_Code,Certification,Module_Code,Module,Quiz_Score,Credits_Earned"
	columnTranslation="false"
	dateformat=""
	numberFormat="Quiz_Score,Credits_Earned"
	useTeams="#useMyTeams#"
	useTeamsPersonIDColumn="TeamId"
	FilterSelectFieldList="Country,Organisation,Name,Certification_Code"
	FilterSelectFieldList2="Country,Organisation,Name,Certification_Code"
	GroupByColumns=""
	radioFilterLabel=""
	radioFilterDefault=""
	radioFilterName=""
	radioFilterValues=""
	checkBoxFilterName=""
	checkBoxFilterLabel=""
	allowColumnSorting="yes"
	showCellColumnHeadings="no"
>


