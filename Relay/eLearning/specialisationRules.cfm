<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		specialisationRules.cfm	
Author:			NJH  
Date started:	08-09-2008
	
Description:			

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2008-10-24			NYB 		turned title & description into phrasetext fields
2008-11-11			NYB 		replaced description with title as drop down contents
2010-10-20			NYB 		removed Created from <editors> - as it kept updating the Created field, as well as the LastUpdated field, everytime a rule was changed
2011-03-02			NYB 		LHID5772: fixed country rules.  Changed to use function instead of query 
								- because query was so big it was messy
								added support for this into relayxmleditorv2.cfm
2012-05-04	 		Englex 		P-REL109 - Add training program
2012-06-18			PPB			Case 428899 needed to explicitly specify "document." in additionalSubmitText
2013-12-13			AXA 		Case 438108 increased width of modules select boxes
Possible enhancements:


 --->

<cfparam name="specialisationID" type="numeric">

<cfif specialisationID neq 0>
	<!--- PPB 2010/08/16 P-SOP024 --->
	<cfset specialisationCountryID = application.com.relayElearning.getSpecialisationData(specialisationID=specialisationID).countryID>
	
	<!--- PPB 2010/08/16 for new rules save the certificationId so that we can make sure they appear in the certification dropdown after pressing save
	 	  this would not be necessary if specialisationRuleID were populated with its new value after the new rule is saved (in which case the 2nd half of the UNION query would pick it up); not sure why it isn't
	 --->
	<cfif StructKeyExists(form,"certificationID")>
		<cfset newCertificationID = form.certificationID>
	<cfelse>
		<cfset newCertificationID = 0>
	</cfif>
	
	<cfif structKeyExists(url,"editor") and url.editor eq "yes">
		<cfif structKeyExists(form,"specialisationRuleID")>
			<cfset specialisationRuleID = form.specialisationRuleID>
		</cfif>
		<cfset url.specialisationRuleID=specialisationRuleID>
		<cfset certificationid = application.com.relayElearning.GetSpecialisationRuleData(specialisationRuleID=url.specialisationRuleID)>
		<cfif certificationid.recordcount eq 1>
			<cfset certificationid = certificationid.certificationid>
		<cfelse>
			<cfset certificationid = 0>
		</cfif>
		
		
		<!--- save the content for the xml to define the editor --->
		<cfsavecontent variable="xmlSource">
			<cfoutput>
			<editors>
				<editor id="232" name="thisEditor" entity="SpecialisationRule" title="phr_elearning_SpecialisationRuleEditor">
					<field name="specialisationRuleID" label="phr_elearning_specialisationRuleID" description="This records unique ID." control="html"></field>
					<field name="specialisationID" label="phr_elearning_specialisation" description="The specialisation that this rule applies to" control="html" makeHidden="true" default="#specialisationID#"></field>				
					<field name="numOfCertifications" label="phr_elearning_numOfCertifications" description="The number of certifications required to achieve the specialisation rule"></field>
					<cfif fileexists("#application.paths.code#\cftemplates\customSpecialisationRules.cfm")> <!---450905 CW & RJT Added a hook for custom specialisation rules --->
						<cfinclude template="/code/cftemplates/customSpecialisationRules.cfm">
					</cfif>
					<field name="certificationID" label="phr_elearning_certification" description="The certification for the specialisation rule" control="select"  
								query="func:com.relayElearning.GetAssignableCertifications(countryID=#specialisationCountryID#,currentValue=#certificationid#,specialisationID=#specialisationID#)"></field>
					<!--- PPB 2010-08-05 P-SOP024 - add Compulsory Modules; we start with empty queries and populate via Ajax (strictly speakly the RHS select box doesn't need to be Ajax but it helps to make it populate after saving a new rule cos the new specialisationRuleID doesn't seem to be usable at the point where needed  --->
					<!--- 2013-07-01	YMA	Case 436018 Set isSelected on selected modules for TwoSelects validvalues. --->
					<!--- 2013-11-26	YMA	Case 438108 Display module code: module title in potential modules box. --->
					<!--- 2013-12-13	AXA Case 438108 increased width of modules select boxes--->
					<field name="modules" label="Compulsory Modules" description="The Compulsory Modules required for the specialisation rule" control="twoSelects" validValues="SELECT tm.moduleID AS value,isnull(tm.moduleCode,'')+': '+ CASE WHEN len(tm.title_defaultTranslation) > 100 THEN left(tm.title_defaultTranslation,30) +  ' ... ' + right(tm.title_defaultTranslation,30) ELSE tm.title_defaultTranslation END AS display,CASE WHEN sr.moduleID IS NOT NULL THEN 1 ELSE 0 END AS isSelected FROM trngModule tm LEFT JOIN SpecialisationRuleCompulsoryModule sr ON tm.moduleID = sr.moduleID AND sr.specialisationruleID = [[specialisationRule.specialisationRuleID]] WHERE tm.moduleID IN (SELECT moduleID FROM SpecialisationRuleCompulsoryModule WHERE SpecialisationRuleID = [[specialisationRule.specialisationRuleID]]) AND availability != 0" 
							default="" caption1="phr_elearning_PotentialModules" CAPTION2="phr_elearning_CurrentModules" allowSort="no" size="7" width="450"/>
					<field name="activationDate" label="Activation Date" description="The date the specialisation rule becomes active" control="date"></field>
					<field name="active" label="Active" description="Is the specialisation rule active?" control="YorN"></field>
					<field name="CreatedBy" label=""></field>
					<field name="Created" label="" description="" ></field>
					<field name="LastUpdatedBy" label=""></field>
					<field name="LastUpdated" label=""></field>
				</editor>	
			</editors>
			</cfoutput>
		</cfsavecontent>
	
		<cfif not isDefined("add")>
			<cfset add = "no">
		</cfif>
	
		<CF_RelayXMLEditor
			xmlSourceVar = "#xmlSource#"
			editorName = "thisEditor"
			showSaveAndReturn = "true"
			postSaveIncludeFile = "/elearning/specialisationRulesCompulsoryModulesUpdate.cfm"
			thisEmailAddress = "relayhelp@foundation-network.com"
			showSave="no" 
			add="#add#"
		>
		<!--- PPB 2010/08/17 P-SOP024 i have turned off the showSave button on add new rec cos if the user pressed it twice it crashes; the button is still available in edit mode --->
	
	
	
		
		<!--- PPB 2010/08/15 P-SOP024 START ---> 
		
		<cf_includeJavascriptOnce template = "/javascript/fnlajax.js">
		<cf_includeJavascriptOnce template = "/javascript/listener.js">
		
		<script type="text/javascript">
			//26-08-2016 ESZ Uncaught TypeError: Cannot read property 'length' of null
			var elSelectedIDs = document.getElementById('modules');
			var arrayLength = elSelectedIDs.length-1
			
			if (arrayLength == -1) {
				arrayLength = 0;
			}
			// no longer required since introducing populateSelectedModules using Ajax			
			var arrOriginalCompulsoryModuleId = new Array (arrayLength);
			var arrOriginalCompulsoryModuleText = new Array (arrayLength);
	
	
			if (document.editorForm != undefined) {
				//document.editorForm.certificationID.addEventListener( "change", function () { populateSelectableModules(document.editorForm.specialisationRuleID.value,document.editorForm.certificationID.value,{onCompleteFunction: function () {removeSelectedIDs()}})   }, false)
				addListener(document.editorForm.certificationID,"change", function () { populateSelectableModules($('SpecialisationRuleID').value,document.editorForm.certificationID.value,originalCertificationID,{onCompleteFunction: function () {resetSelectedIDs(document.editorForm.certificationID.value)}})   }, false)
			}
			
			// load the Potential CompulsoryModules into the LHS select box
			populateSelectableModules = function(specialisationRuleID,certificationID,originalCertificationID,options) {
				// specialisationRuleID will be '' for a New rule
				if (specialisationRuleID=='') {	
					specialisationRuleID = 0;
				}	
				//options.valueColumn="moduleID"
				populateSelectBoxViaAjax ('List_modules','/webservices/callWebService.cfc?webServiceName=relayElearningWS&methodname=getCertificationModules','callWebService','specialisationRuleID=' + specialisationRuleID + '&certificationID=' + certificationID + '&originalCertificationID=' + originalCertificationID,options)
			
			}
	
	
			// ajax in RHS version; load the CompulsoryModules into the RHS select box
			//populateSelectedModules = function(specialisationRuleID,options) {
				//options.valueColumn="moduleID"
			//	populateSelectBoxViaAjax ('SelectedIDs','/webservices/callWebService.cfc?webServiceName=relayElearningWS&methodname=getSpecialisationRuleCompulsoryModules','callWebService','specialisationRuleID=' + specialisationRuleID ,options)
			//}
	
	
			// save the 'original' list of compulsory modules so that if we change certification and change it back the list is reverted
			saveSelectedIDs = function()
			{
				for (i = 0; i<=elSelectedIDs.length - 1; i++) {
					arrOriginalCompulsoryModuleId[i] = elSelectedIDs.options[i].value;
					arrOriginalCompulsoryModuleText[i] = elSelectedIDs.options[i].text;
				}
			}
				
	
			// having changed a certification we need to clear the list of compulsory modules that had previously been selected
			resetSelectedIDs = function(newCertificationID)
			{
				 var elSel = document.getElementById('modules_select');
				 var i;
				 for (i = elSel.length - 1; i>=0; i--) {
				    elSel.remove(i);
				 }
				 
				if (newCertificationID == originalCertificationID) {
	
					//ajax in RHS version; experienced unreliablilty in IE
					//populateSelectedModules(document.editorForm.specialisationRuleID.value);
	
					for (i = 0; i<=arrOriginalCompulsoryModuleId.length - 1; i++) {
					    elSel.options[elSel.options.length] = new Option(arrOriginalCompulsoryModuleText[i], arrOriginalCompulsoryModuleId[i]);
					}
				}
			}
			
			//form load initialisation
			if (document.editorForm != undefined) {
				
				//save the  and originalCompulsoryModules so that if the user switches away from the certificate and then switches back we can re-populate the originals
				var originalCertificationID;
	
				if ($('SpecialisationRuleID').value == '') {
					originalCertificationID = 0;
				} else {
					originalCertificationID = document.editorForm.certificationID.value;
				}
	
				//fire this off on form load to prefil the Potential Modules list
				populateSelectableModules($('SpecialisationRuleID').value,document.editorForm.certificationID.value,originalCertificationID,{})
				
				//ajax in RHS version
				//populateSelectedModules(document.editorForm.specialisationRuleID.value);
				
				saveSelectedIDs();
			} 
			
		</script>
		<!--- PPB 2010/08/15 P-SOP024 END ---> 
	
	
	
	<cfelse>
		<!--- adding a specialisation rule --->
		<cfif structKeyExists(form,"frmAddSpecialisationRule")>
			<!--- PPB - i don't think this is ever run --->
			<cfset specialisationRuleId = application.com.relayElearning.insertSpecialisationRule(specialisationID=specialisationID,certificationID=frmCertificationID,numOfCertifications=frmNumOfCertifications)>
		</cfif>
	
		<cfscript>
			qryGetSpecialisationRules = application.com.relayElearning.GetSpecialisationRuleData(specialisationId=specialisationID);
		</cfscript>
		
		<cfset request.relayFormDisplayStyle = "HTML-table">
		
		<cf_relayFormDisplay>
	
		<cfif qryGetSpecialisationRules.recordCount gt 0>
				<tr>
					<th>phr_elearning_specialisationRule</th>
					<th>phr_elearning_certification / phr_elearning_compulsoryModules</th>
					<th>phr_elearning_numOfCertifications</th>
					<th>phr_elearning_active</th>
					<th>phr_elearning_activationDate</th>
					<th>&nbsp;</th>
				</tr>
				<cfloop query="qryGetSpecialisationRules">
					<cfscript>
						qryCertificationData = application.com.relayElearning.GetCertificationData(certificationID=certificationID);
						qryCompulsoryModules = application.com.relayElearning.GetSpecialisationRuleCompulsoryModules(specialisationRuleID=specialisationRuleID);
					</cfscript>
	
					<cfset compulsoryModules = "">
					<cfif qryCompulsoryModules.recordcount gt 0>
						<cfloop query="qryCompulsoryModules">
							<cfset compulsoryModules = compulsoryModules & "<br/>" & "&nbsp;&nbsp;" & #display# >
						</cfloop> 
					</cfif>
					<tr valign="top">
						<td><cf_relayFormElement relayFormElementType="HTML" currentValue="<a href='/elearning/specialisationRules.cfm?editor=yes&specialisationRuleID=#specialisationRuleID#&specialisationID=#specialisationID#'>Rule #htmleditformat(currentRow)#</a>" fieldname="" label=""></td>
						<!--- NYB 2008-10-22 P-SOP007 - replaced "qryCertificationData.description" with "qryCertificationData.title" --->
						<td><cf_relayFormElement relayFormElementType="HTML" currentValue="#qryCertificationData.title# #compulsoryModules#" fieldname="" label=""></td>
						<td><cf_relayFormElement relayFormElementType="HTML" currentValue="#numOfCertifications#" fieldname="" label=""></td>
						<cfif active>
							<cfset ruleActiveDisplay = "phr_Yes">
						<cfelse>
							<cfset ruleActiveDisplay = "phr_No">
						</cfif>
						<td><cf_relayFormElement relayFormElementType="HTML" currentValue="#ruleActiveDisplay#" fieldname="" label=""></td>
						<td><cf_relayFormElement relayFormElementType="HTML" currentValue="#DateFormat(activationDate, 'dd-mmm-yyyy')#" fieldname="" label=""></td>
						<td>&nbsp;</td>
					</tr>
				</cfloop>
		</cfif>
		
		<cfset formName="addSpecialisationRuleForm">
		<cfset getCertificationsRecordCount = application.com.relayElearning.GetAssignableCertifications(countryID=specialisationCountryID,returnType="RecordCount")>
		
		<form name="#formName#"  method="post">
				<cf_relayFormElement relayFormElementType="hidden" currentValue="#specialisationID#" fieldname="specialisationID" label="">
				<!--- START: PPB 2010/08/16 P-SOP024 - added country rules--->
				<!--- 	
				<cfscript>
					specialisationQry = application.com.relayElearning.getSpecialisationData(specialisationID=specialisationID);
					//certificationQry = application.com.relayElearning.GetCertificationData(countryID=specialisationQry.countryID);
				</cfscript>
						<!--- NYB 2008-10-22 P-SOP007 - replaced "description" with "title" --->
						<cfquery name="getFilteredCertifications" dbType="query">
							select distinct certificationID, title from certificationQry
							where active = 1
							<cfif qryGetSpecialisationRules.recordCount gt 0>
								and certificationID not in (#valueList(qryGetSpecialisationRules.certificationID)#)
							</cfif>
							
							order by title
						</cfquery>
				--->
				<!--- ppb this query shares a lot with the certification dropdown - if i had time i'd try to merge them --->
				<!--- END: PPB 2010/08/16 P-SOP024 - added country rules--->
				<cfif getCertificationsRecordCount gt 0>
					<tr>
						<td colspan="99" align="right" >
							<a class="submenu" href='/elearning/specialisationRules.cfm?editor=yes&add=yes&specialisationRuleID=0&specialisationID=<cfoutput>#htmleditformat(specialisationID)#</cfoutput>'><b>Add New Rule</b></a>
						</td>
					</tr>	
				<cfelse>
					<tr>
						<td colspan="99" align="center">phr_elearning_NoNewRulesToAddAsNoCertificationsExist.</td>
					</tr>
				</cfif>
			
			</form>
			</cf_relayFormDisplay>
			
		</cfif>
</cfif>

