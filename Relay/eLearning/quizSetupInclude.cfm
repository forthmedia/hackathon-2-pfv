<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		quizSetUpInclude.cfm
Author:			NJH
Date started:	2014/06/18

Description:	Used to include quiz question pools from within the quiz editor

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
Possible enhancements:


 --->

<cfif form.quizDetailID neq 0>
	<cf_includeJavascriptOnce template="/elearning/js/quiz.js">

	<div id="QuizDetailQuestionPoolDiv">
	<cfoutput>#application.com.relayQuiz.getQuizDetailQuestionPoolDisplay(quizDetailID=form.quizDetailID).content#</cfoutput>
	</div>
</cfif>