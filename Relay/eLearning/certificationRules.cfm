<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		eLearning\certificationRules.cfm	
Author:			NJH  
Date started:	21-08-2008
	
Description:			

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2008-11-20 			NYB 		P-SOP011 - add email merge fields
2009-05-20 			NYB 		Bug 2255: Updating Certification Rule changes the CreatedBy and Created columns - not just the LastUpdated & LastUpdatedBy
2012-04-16	 		Englex 		P-REL109 - Add training program
2012-11-19			GCC			432118 - was timing out sending passed records to pending when recordcount got to more than ~600 records. Extended timoeout form 1 minute to 10 minutes and removed looped subquery that was not required to improve performance
2013-04-11			STCR		432194 - trngModuleSet.description replaced by defaultTranslation. TODO - see about using entity phrase for valid values display col?
2013-04-29			STCR		Specify cfmlCallerName when calling RelayXMLEditor directly
2013-07-26			PPB			Case 436307 sort order of module sets and exclude inactive ones
2013-10-03 			PPB 		Case 437262 make edit of active/activationDate fire updatePersonCertifications and send emails
2013-11-12			PPB			Case 437635 prevent sending email if the rule is not activated yet
2013-12-02 			PPB 		Case 438029 replaced queries with call to function relayElearning.getPersonCertificationDetails
2015-12-29			PYW			P-TAT006 BRD 31. Add 'Retest' rule processing
15/03/2016          WAB/DAN     PROD2016-721 - fix for editing certification rules
2016-10-18 			WAB 		PROD2016-2546 Change certificationRuleOrder to be required and remove the Null Text - will default to first item N/A ( 0 )

Possible enhancements:


 --->
<!--- GCC 2012-11-19 432118 added 1 hour timeout  --->
<cfsetting requesttimeout="3600">

<cfparam name="sendEmails" default="true">			<!--- 2013-10-03 PPB Case 437262 --->
<cfparam name="certificationID" type="numeric">

<cfset SortOrderQuery="select 0 as value, 'N/A' as display">
<cfloop index='i' from='1' to='10'>
	<cfset SortOrderQuery="#SortOrderQuery# union select #i# as value,cast(#i# as varchar) as display">
</cfloop>
<cfset SortOrderQuery = SortOrderQuery&" order by value">

<cfif structKeyExists(url,"editor") and url.editor eq "yes">
	<cfset url.certificationRuleID=certificationRuleID>
	<cfset url.certificationRuleTypeID=0>
	<!--- save the content for the xml to define the editor --->
	<cfsavecontent variable="xmlSource">
		<cfoutput>
		<editors>
			<editor id="232" name="thisEditor" entity="trngCertificationRule" title="Certification Rule Editor">
				<field name="certificationRuleID" label="phr_elearning_certificationRuleID" description="This records unique ID." control="html"></field>
				<field name="certificationID" label="phr_elearning_certificationID" description="This records certification ID." control="html"></field>
				<field name="certificationRuleTypeID" label="phr_elearning_certificationRuleType" description="The type of certification rule" control="FKReadOnly"
						ForeignKeyEntity="trngCertificationRuleType" ForeignKey="certificationRuleTypeID" ForeignKeyValues="description"
							query="select certificationRuleTypeID as value,description as display from trngCertificationRuleType where certificationRuleTypeID = (select certificationRuleTypeID from trngCertificationRule where certificationRuleID=#certificationRuleID#)"></field>
				<field name="numModulesToPass" label="phr_elearning_numModulesToPass" description="The number of modules to pass for the certification rule"></field>
				<field name="studyPeriod" label="phr_elearning_studyPeriodInMonths" description="The study period for the certification rule"></field>
				<!--- START 2013-07-26 PPB Case 436307 sort order of module sets and exclude inactive ones --->
                <field name="ModuleSetID" label="phr_elearning_moduleSet" description="The Module Set for the certification rule" control="select" query="func:com.relayElearning.GetModuleSetsData(forDropDown=true,certificationID=[[certificationID]],certificationRuleID=[[certificationRuleID]])"></field>
				<field name="certificationRuleOrder" label="Rule Order" description="The order in which the rules are applied" control="select" query="#SortOrderQuery#" required="true" showNull="false"></field>
				<field name="activationDate" label="Activation Date" description="The date the certification rule becomes active" control="date"></field>				
				<field name="active" label="Active" description="Is the certification rule active?" control="YorN"></field>
				<!--- <field name="LastUpdatedBy" label="" default="#request.relaycurrentuser.userGroupID#" description="" control="hidden"></field>
				<field name="LastUpdated" label="" default="#request.requestTime#" description="" control="hidden"></field> ---><!--- 2013-05-08 STCR these hidden fields were throwing error in RelayEntity.cfc on save --->
			</editor>	
		</editors>
		</cfoutput>
	</cfsavecontent>
	
	<CF_RelayXMLEditor
		xmlSourceVar = "#xmlSource#"
		editorName = "thisEditor"
		showSaveAndReturn = "false"													<!--- 2013-11-12 PPB Case 437635 since "save" now returns don't show 2 links --->
		cfmlCallerName = "certificationRules"
		thisEmailAddress = "relayhelp@foundation-network.com"
		postSaveIncludeFile = "\eLearning\certificationRulesPostProcess.cfm"		<!--- 2013-10-03 PPB Case 437262 --->
	>
	
<cfelse>
		
	<!--- adding a certification rule --->
	<cfif structKeyExists(form,"frmAddCertificationRule")>
		<cfscript>
			application.com.relayElearning.insertCertificationRule(certificationID=CertificationID,moduleSetID=frmModuleSetID,numModulesToPass=frmNumModulesToPass,studyPeriod=frmStudyPeriod,certificationRuleTypeID=frmCertificationRuleTypeID,activationDate=frmActivationDate,certificationRuleOrder=frmCertificationRuleSortOrder);
		</cfscript>
	</cfif>	
		
	<cfif structKeyExists(form,"frmAddCertificationRule") OR (structKeyExists(url,"updatePersonCertifications"))>			<!--- 2013-10-03 PPB Case 437262 added updatePersonCertifications--->
		<cfquery name="getRuleType" datasource="#application.siteDataSource#">
			select * from trngCertificationRuleType where certificationRuleTypeID =  <cf_queryparam value="#frmCertificationRuleTypeID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfquery>
		
		<!--- if an update rule has been introduced, send an email to people who are currently certified with that certification to let them know that 
			they will need to do an update rule to maintain their certification --->
		
		<!--- 2015-12-29 PYW P-TAT006 BRD 31. Add Retest --->
		<cfif getRuleType.recordCount eq 1 and (getRuleType.ruleTypeTextID eq "Update" or getRuleType.ruleTypeTextID eq "Retest")>
	
			<!--- NYB 2008-11-06 P-SOP009 -> --->
 			<!--- 2011/11/23 WAB LID 8218 Convert to entityTranslations --->
 			<!--- 2015-/12/30 PYW P-TAT006 BRD 31. Add validity --->
			<cfquery name="getCertificationDetails" datasource="#application.siteDataSource#" maxrows="1">
				select certificationID,validity,
				'phr_title_trngcertification_' + convert(varchar,certificationid) as Title
				from trngCertification where CertificationID=<cfqueryparam cfsqltype="cf_sql_integer" value="#certificationID#"> 
			</cfquery>

			<cfif getRuleType.ruleTypeTextID eq "Retest" and not IsNumeric(getCertificationDetails.validity)>
				<cfset updatePersonCertData = false>
			<cfelse>
				<cfset updatePersonCertData = true>
			</cfif>

			<cfif updatePersonCertData>

			<cfset form.CertificationID = getCertificationDetails.CertificationID>
			<cfset form.Certification = application.com.relayTranslations.translatePhrase(phrase=getCertificationDetails.Title)/>
			<!--- <- NYB --->

			<!--- START 2013-12-02 PPB Case 438029 replaced query with function for sharing with CertificationActivationNotification.cfm
			<!--- GCC 2012-11-19 432118 reworked into one query  --->
			<cfquery name="getPeopleWithThisPassedCertification" datasource="#application.siteDataSource#">
				select personCertificationID, personID, statusTextID 
				,convert(varchar,tpc.registrationDate,106) as RegistrationDate
				,convert(varchar,tpc.passDate,106) as PassDate from 
				trngPersonCertification tpc with (noLock) inner join trngPersonCertificationStatus tpcs with (noLock)
					on tpc.statusID = tpcs.statusID and tpcs.statusTextID in ('Passed','Pending')
				where certificationID =  <cf_queryparam value="#certificationID#" CFSQLTYPE="CF_SQL_INTEGER" > 
			</cfquery>
			 --->
			<cfif getRuleType.ruleTypeTextID eq "Update">
				<cfset thisStatusTextId = "Passed,Pending">
				<cfset thisEmailTextID = "TrngCertificationUpdateRequired">
			<cfelse>
				<cfset thisStatusTextId = "Passed">
				<cfset thisEmailTextID = "TrngCertificationRetestRequired">
			</cfif>
			<cfset getPeopleWithThisPassedCertification=application.com.relayElearning.getPersonCertificationDetails(certificationID=certificationID,statusTextId=thisStatusTextId)>
			<!--- END 2013-12-02 PPB Case 438029 --->

			
			<!--- AJC 2008-11-20 P-SOP011 - added -> --->
				<cfscript>
				qryPendingStatusID = application.com.relayElearning.getPersonCertificationStatus(statusTextID='Pending');
				pendingStatusID = qryPendingStatusID.StatusID;
				</cfscript>
			<!--- <- P-SOP011 --->

			<!--- START 2013-11-12 PPB Case 437635 prevent sending email if the rule is not activated yet --->
			<cfif StructKeyExists(form,"frmActivationDate") and (frmActivationDate gt request.requestTime)>
				<cfset sendEmails = false>
			</cfif>								
			<!--- END 2013-11-12 PPB Case 437635 --->
			
			<cfloop query="getPeopleWithThisPassedCertification">
				<cfset form.RegistrationDate = getPeopleWithThisPassedCertification.RegistrationDate>
				<cfset form.PassDate = getPeopleWithThisPassedCertification.PassDate>
				<!--- <- P-SOP011 --->
				
				<!--- START 2013-10-03 PPB Case 437262 --->
				<cfif sendEmails>
					<cfset application.com.email.sendEmail(personID=personID,emailTextID=thisEmailTextID)>
				</cfif>
				<!--- END 2013-10-03 PPB Case 437262 --->
				
				<cfscript>
				if (getRuleType.ruleTypeTextID eq "Update")	{
					// set anyone who is currently 'passed' against this certification as 'pending'
					if (statusTextID eq "Passed") {
						argumentsStruct = structnew();
						argumentsStruct.frmStatusID = pendingStatusID;  //AJC 2008-11-20 P-SOP011 
						application.com.relayElearning.updatePersonCertificationData(personCertificationID=personCertificationID,argumentsStruct=argumentsStruct);
					}
				}
				</cfscript>
			</cfloop>
			
			<!--- if this rule is an update rule, then email anyone who is currently 'registered' against this certification informing them of the new rule--->
			<!--- START 2013-12-02 PPB Case 438029 replaced query with function for sharing with CertificationActivationNotification.cfm
			<cfquery name="getPeopleWithThisRegisteredCertification" datasource="#application.siteDataSource#">
				select personCertificationID, personID, convert(varchar,tpc.registrationDate,106) as RegistrationDate from trngPersonCertification tpc with (noLock) inner join trngPersonCertificationStatus tpcs with (noLock)
					on tpc.statusID = tpcs.statusID and tpcs.statusTextID = 'Registered'
				where certificationID =  <cf_queryparam value="#certificationID#" CFSQLTYPE="CF_SQL_INTEGER" > 
			</cfquery>
			--->
			<cfif getRuleType.ruleTypeTextID eq "Update">
				<cfset getPeopleWithThisRegisteredCertification=application.com.relayElearning.getPersonCertificationDetails(certificationID=certificationID,statusTextId='Registered')>
				<!--- END 2013-12-02 PPB Case 438029 --->
				
				<cfloop query="getPeopleWithThisRegisteredCertification">
					<cfset form.RegistrationDate = getPeopleWithThisRegisteredCertification.RegistrationDate>
					<!--- <- P-SOP011 --->
	
					<!--- START 2013-10-03 PPB Case 437262 --->
					<cfif sendEmails>
						<cfset application.com.email.sendEmail(personID=personID,emailTextID='TrngNewCertificationRuleAdded')>
					</cfif>
					<!--- END 2013-10-03 PPB Case 437262 --->
				</cfloop>
			</cfif>
		
			</cfif> <!--- updatePersonCertData --->
		</cfif>
	</cfif>
	
	<cfscript>
		qryGetCertificationRules = application.com.relayElearning.GetCertificationRuleData(activeModuleSets=true,certificationId=certificationID,sortOrder="certificationRuleOrder");
	</cfscript>
	
	<!--- <cfgrid name="reportsGrid" format="html" pageSize="10" stripeRows="true" 
	   bind="cfc:relay.com.relayElearning.qGetCertificationData({cfgridpage},{cfgridpagesize},{cfgridsortcolumn},{cfgridsortdirection})"> 
	  <cfgridcolumn name="Description" header="Name"> 
	  <cfgridcolumn name="certificationID"> 
	</cfgrid> ---> 

	
	<cfset request.relayFormDisplayStyle = "HTML-table">
	<cfset formName="addCertificationRuleForm">
	
	<cfform name="#formName#" method="post" >
	<cf_relayFormDisplay>
	
	<cfif qryGetCertificationRules.recordCount gt 0>
			<tr>
				<th>phr_elearning_CertificationRule</th>
				<th>phr_elearning_ModuleSet</th>
				<th>phr_elearning_numModulesToPass</th>
				<th>phr_elearning_studyPeriod</th>
				<th>phr_elearning_certificationRuleOrder</th>
				<th>phr_elearning_CertificationRuleType</th>
				<th>phr_elearning_active</th>
				<th>phr_elearning_activationDate</th>
			</tr>
			<cfloop query="qryGetCertificationRules">
				<tr>
					<td><cf_relayFormElement relayFormElementType="HTML" currentValue="<a href='/elearning/certificationRules.cfm?editor=yes&CertificationRuleID=#certificationRuleID#&certificationID=#CertificationID#'>Rule #htmleditformat(currentRow)#</a>" fieldname="" label=""></td>
					<td><cf_relayFormElement relayFormElementType="HTML" currentValue="#description#" fieldname="" label=""></td>
					<td><cf_relayFormElement relayFormElementType="HTML" currentValue="#numModulesToPass#" fieldname="" label=""></td>
					<td><cf_relayFormElement relayFormElementType="HTML" currentValue="#studyPeriod#" fieldname="" label=""></td>
					<td><cf_relayFormElement relayFormElementType="HTML" currentValue="#((certificationRuleOrder is 0)?'N/A':certificationRuleOrder)#" fieldname="" label=""></td>
					<td><cf_relayFormElement relayFormElementType="HTML" currentValue="#CertificationRuleType#" fieldname="" label=""></td>
					<cfif ruleActive>
						<cfset ruleActiveDisplay = "phr_Yes">
					<cfelse>
						<cfset ruleActiveDisplay = "phr_No">
					</cfif>
					<td><cf_relayFormElement relayFormElementType="HTML" currentValue="#ruleActiveDisplay#" fieldname="" label=""></td>
					<td><cf_relayFormElement relayFormElementType="HTML" currentValue="#DateFormat(activationDate, 'dd-mmm-yyyy')#" fieldname="" label=""></td>
				</tr>
			</cfloop>
	
	</cfif>
	<cf_relayFormElement relayFormElementType="hidden" currentValue="#CertificationID#" fieldname="CertificationID" label="">
		
		<cfscript>
			moduleSetQry = application.com.relayElearning.GetModuleSetsData(certificationID = #CertificationID#);
		</cfscript>
		
		<cfquery name="getFilteredModuleSets" dbType="query">
			select *, description + ' ('+cast(numModulesInModuleSet as varchar)+')' as display from moduleSetQry
			where active = 1
			<cfif qryGetCertificationRules.recordCount gt 0>
				and moduleSetID not in (#valueList(qryGetCertificationRules.moduleSetID)#)
			</cfif>
		</cfquery>
		
		<cfquery name="getCertificationRuleTypes" datasource="#application.siteDataSource#">
			select certificationRuleTypeID, description from trngCertificationRuleType
		</cfquery>
		
		<cfquery name="sortOrderQuery" datasource="#application.siteDataSource#">
			#preserveSingleQuotes(sortOrderQuery)#
		</cfquery>
		
		<tr><td colspan="8">&nbsp;</td></tr>
		<tr>
			<th colspan="8">phr_elearning_AddNewRule</th>
		</tr>
		
		<cfif getFilteredModuleSets.recordCount gt 0>
			<tr>
				<td>&nbsp;</td>
				<td><strong>phr_elearning_ModuleSet</strong></td>
				<td><strong>phr_elearning_numModulesToPass</strong></td>
				<td><strong>phr_elearning_studyPeriod</strong></td>
				<td><strong>phr_elearning_certificationRuleOrder</strong></td>
				<td><strong>phr_elearning_CertificationRuleType</strong></td>
				<td><strong>phr_elearning_ActivationDate</strong></td>
				<td>&nbsp;</td>
			</tr>
			
			<tr>
				<td>&nbsp;</td>
				<td><cf_relayFormElement relayFormElementType="select" currentValue="" fieldname="frmModuleSetID" label="" query="#getFilteredModuleSets#" display="display" value="moduleSetID" required="yes"></td>
				<td><cf_relayFormElement relayFormElementType="numeric" currentValue="" fieldname="frmNumModulesToPass" label="" required="yes" size="5"></td>
				<td><cf_relayFormElement relayFormElementType="numeric" currentValue="" fieldname="frmStudyPeriod" label="" required="yes" size="5"></td>
				<td><cf_relayFormElement relayFormElementType="select" currentValue="" fieldname="frmCertificationRuleSortOrder" label="" query="#sortOrderQuery#" display="display" value="value" required="yes"></td>
				<td><cf_relayFormElement relayFormElementType="select" currentValue="" fieldname="frmCertificationRuleTypeID" label="" query="#getCertificationRuleTypes#" display="description" value="certificationRuleTypeID" required="yes"></td>
				<td><cf_relayFormElement relayFormElementType="date" currentValue="#now()#" fieldname="frmActivationDate" label="" formName="#formName#"></td>
				<td><cf_relayFormElement relayFormElementType="submit" currentValue="phr_Add" fieldname="frmAddCertificationRule" label="" required="yes"></td>
			</tr>
			<tr>
				<td colspan="8"><!--- NJH 2013/05/31 - not sure what this translation is supposed to be and there doesn't appear to be one, so have commented this out for now
						phr_elearning_Update_Background ---></td>
			</tr>
		<cfelse>
			<tr>
				<td colspan="7">phr_elearning_NoNewRulesToAddAsNoModuleSetsExist.</td>
			</tr>
		</cfif>
	</cf_relayFormDisplay>
	</cfform>
	
</cfif>