<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			certificationFunctions.cfm
Author:				NJH
Date started:		2008/09/09

Description:

creates functionList query for tableFromQueryObject custom tag

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2008-12-10			AJC			Add ability to Activate a certification
2012-05-07	 		Englex 		P-REL109 - Add training program
Possible enhancements:


 --->

<cfparam name="functionType" type="string" default="Certification">

<cfset functionName = application.com.relayTranslations.translatePhrase(phrase="elearning_"&functionType&"Functions")>

<cfscript>
comTableFunction = CreateObject( "component", "relay.com.tableFunction" );

comTableFunction.functionName = functionName;
comTableFunction.securityLevel = "";
comTableFunction.url = "";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();

comTableFunction.functionName = "--------------------";
comTableFunction.securityLevel = "";
comTableFunction.url = "";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();

if (functionType eq 'Specialisation') {
	comTableFunction.functionName = "&nbsp;phr_elearning_InactivateSpecialisations";
	comTableFunction.securityLevel = "";
	comTableFunction.url = "javascript:if (confirm('phr_elearning_confirmInactivateSpecialisations')) {document.frmBogusForm.submit();} ;//";
	comTableFunction.windowFeatures = "";
	comTableFunction.functionListAddRow();
}

if (functionType eq 'Certification') {
	// START: 2008-12-10 AJC Add ability to Activate a certification
	comTableFunction.functionName = "&nbsp;phr_elearning_activateCertifications";
	comTableFunction.securityLevel = "";
	comTableFunction.url = "javascript:if (confirm('phr_elearning_ConfirmActivateCertifications')) {submitAction(action='activate');} ;//";
	comTableFunction.windowFeatures = "";
	comTableFunction.functionListAddRow();
	comTableFunction.functionName = "&nbsp;phr_elearning_InactivateCertifications";
	comTableFunction.securityLevel = "";
	comTableFunction.url = "javascript:if (confirm('phr_elearning_ConfirmInactivateCertifications')) {submitAction(action='deactivate');} ;//";
	comTableFunction.windowFeatures = "";
	comTableFunction.functionListAddRow();
	// END: 2008-12-10 AJC Add ability to Activate a certification
}

// START: 2012-05-07 Englex P-REL109 - Add training program
if (functionType eq 'TrainingProgram') {
	comTableFunction.functionName = "&nbsp;phr_elearning_delete_trainingProgram";
	comTableFunction.securityLevel = "";
	comTableFunction.url = "javascript:if (confirm('phr_elearning_confirm_delete_trainingProgram')) {document.frmBogusForm.submit();} ;//";
	comTableFunction.windowFeatures = "";
	comTableFunction.functionListAddRow();
}
if (functionType eq 'Question') {
	comTableFunction.functionName = "&nbsp;phr_elearning_delete_question";
	comTableFunction.securityLevel = "";
	comTableFunction.url = "javascript:if (confirm('phr_elearning_confirm_delete_question')) {document.frmBogusForm.submit();} ;//";
	comTableFunction.windowFeatures = "";
	comTableFunction.functionListAddRow();
}
if (functionType eq 'QuestionPool') {
	comTableFunction.functionName = "&nbsp;phr_elearning_delete_questionPool";
	comTableFunction.securityLevel = "";
	comTableFunction.url = "javascript:if (confirm('phr_elearning_confirm_delete_questionPool')) {document.frmBogusForm.submit();} ;//";
	comTableFunction.windowFeatures = "";
	comTableFunction.functionListAddRow();
}
// END: 2012-05-07 Englex P-REL109 - Add training program
</cfscript>


