<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			Report_Quiz1.cfm	
Author:				A Developer
Date started:		The Olden Days
	
Description:		Available Quizzes

Amendment History:

Date (DD-MM-YYYY)	Initials 	What was changed
2008-09-16			AJC			Change the report to use TFQO and also added filters & export to excel
								Pretty much changed the whole file
2009-02-27			NJH			Bug Fix All Sites Issue 1899 - made column headings translatable
2009-08-03			NYB  		SNY047					
2009-09-30			NYB			LHID2604
2009-10-23			NAS			LID - 2481/2499 Code entered to Order Translatable Columns
					
Possible enhancements:

 --->

<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">
<cfparam name="openAsExcel" type="boolean" default="false">

<cf_title>Report: Available Quizzes</cf_title>

<cfif not openAsExcel>
		<cfparam name="current" type="string" default="index.cfm">
		<cfparam name="attributes.templateType" type="string" default="edit">
		<cfparam name="attributes.pageTitle" type="string" default="">
		<cfparam name="attributes.pagesBack" type="string" default="1">
		<cfparam name="attributes.thisDir" type="string" default="">

		<CF_RelayNavMenu pageTitle="#attributes.pageTitle#" thisDir="#attributes.thisDir#">
			<CF_RelayNavMenuItem MenuItemText="Excel" CFTemplate="#cgi["SCRIPT_NAME"]#?#queryString#&openAsExcel=true">
		</CF_RelayNavMenu>
</cfif>

<cfparam name="sortOrder" default="QuizName">
<cfparam name="numRowsPerPage" default="18">
<cfparam name="startRow" default="1">
<cfparam name="dateFormat" type="string" default="">

<cfparam name="keyColumnList" type="string" default=""><!--- this can contain a list of columns that can be edited --->
<cfparam name="keyColumnURLList" type="string" default=""><!--- this can contain a list of matching columns that contain the URL of the editor template --->
<cfparam name="keyColumnKeyList" type="string" default=""><!--- this can contain a list of matching key columns e.g. primary key --->

<cfparam name="checkBoxFilterName" type="string" default="">
<cfparam name="checkBoxFilterLabel" type="string" default="">

<cfparam name="FilterSelect1" type="string" default="">

<CFQUERY name="getReportData" DATASOURCE="#application.SiteDataSource#">
	select * from (select qd.QuizDetailID,
		qd.title_defaultTranslation as QuizName,
		PossibleQuestions,NumToChoose,TimeAllowed,QuestionsPerPage,
		NumberOfAttemptsAllowed,qd.Created,
		qd.CreatedBy,QuizGroupID,Passmark,tm.moduleID 
		from quizDetail as qd with(nolock)
			inner join trngModule tm on tm.quizDetailID = qd.quizDetailID
		where qd.active=1
		) as base
	where 1 = 1
	<cfinclude template = "/templates/tableFromQuery-QueryInclude.cfm">			
	order by <cf_queryObjectName value="#sortOrder#">
</CFQUERY>
	
<CF_tableFromQueryObject 
	queryObject="#getReportData#"
	queryName="getReportData"
	sortOrder = "#sortOrder#"
	openAsExcel = "#openAsExcel#"
	numRowsPerPage="#numRowsPerPage#"
	startRow="#startRow#"
	
	keyColumnList="#keyColumnList#"
	keyColumnURLList="#keyColumnURLList#"
	keyColumnKeyList="#keyColumnKeyList#"
	
 	hideTheseColumns=""

	ShowTheseColumns="quizName,timeAllowed,questionsPerPage"
	<!--- ColumnHeadingList="Quiz Name, Time Allowed, Questions Per Page" --->
	FilterSelectFieldList="QuizName"
	FilterSelectFieldList2=""
	GroupByColumns=""
	radioFilterLabel=""
	radioFilterDefault=""
	radioFilterName=""
	radioFilterValues=""
	checkBoxFilterName=""
	checkBoxFilterLabel=""
	allowColumnSorting="yes"
	columnTranslation="true"
	ColumnTranslationPrefix="phr_elearning_"
>