<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		questions.cfm
Author:			AJC - Englex Limited
Date started:	2012-04-19

Description:	P-REL109 Training Manager Enhancement Project Phase 1

Amendment History:

Date (YYYY-MM-DD)	Initials 	What was changed
2012-07-20			STCR		P-REL109 Phase 2 - Do not make SCORM-only QuestionTypes available to RelayQuiz Questions
2012-09-04			STCR		CASE 430408 pass through questionPoolID to frmBogusForm for listing after deletion function
2012-09-07			STCR		Do not allow changing of QuestionType to/from 'matching' type, if there are already Answers defined for this Question
2013-04-12			STCR		CASE 432328 Implement Question sortOrder and re-order fields on listing.
2015/08/05			NJH			Jira 403 - make questionRef required so that the record can be edited..

Possible enhancements:
Close loophole that allows QuestionType to be changed to/from 'matching' type if the Edit Question form has not yet been reloaded.

 --->

<cfparam name="getData" type="string" default="foo">
<cfparam name="sortOrder" default="sortOrder,QuestionRef">
<cfparam name="numRowsPerPage" type="numeric" default="100">
<cfparam name="startRow" type="numeric" default="1">

<cfparam name="url.questionPoolID" type="numeric" default="0" />
<cfparam name="form.questionPoolID" type="numeric" default="#url.questionPoolID#" />
<cfparam name="variables.questionPoolID" type="numeric" default="#form.questionPoolID#" />

<cfparam name="frmBackForm" default="" /><!--- STCR 2012-05-17 P-REL109 showBackButton="false" no longer supported. If opening this page in a new tab then we do not want the backForm derived by RelayXMLEditor as it makes a mess of the frames --->

<cfif not structKeyExists(url,"add")><!--- 2012-09-07 STCR do not allow changing of QuestionType to/from 'matching' type, if there are already Answers defined for this Question  --->
	<cfparam name="url.QuestionID" type="numeric" default="0" />
	<cfparam name="form.QuestionID" type="numeric" default="#url.QuestionID#" />
	<cfparam name="variables.QuestionID" type="numeric" default="#form.QuestionID#" />
</cfif>

<cfset passThroughVars = structNew() /><!--- 2012-09-04 STCR CASE 430408 pass through questionPoolID to frmBogusForm for listing after deletion function --->
<cfif not structKeyExists(url,"editor")><cfset passThroughVars.questionPoolID = variables.questionPoolID /></cfif><!--- 2012-09-04 STCR CASE 430408 do not use passThrough if we are already on the editor screen, because the questionPoolID will already be there as a url param, and we do not want it duplicated into a list on form submission --->

<cfscript>
 	// this changes the active status of a certification
 	if(structKeyExists(URL, "frmRowIdentity")){
		for(x = 1; x lte listLen(URL.frmRowIdentity); x = x + 1){
			application.com.relayQuiz.deleteQuestion(questionID=listGetAt(URL.frmRowIdentity,x));
		}
	}
 </cfscript>
<cfif structKeyExists(variables,"QuestionID") and isNumeric(variables.QuestionID) and variables.QuestionID gt 0>
	<cfset variables.getQuizQuestion = application.com.RelayQuiz.getQuizQuestionList(QuestionID=variables.QuestionID) /><!--- 2012-09-07 STCR do not allow changing of QuestionType to/from 'matching' type, if there are already Answers defined for this Question  --->
	<cfset variables.qryGetAnswers = application.com.relayQuiz.GetAnswerList(questionID=variables.questionID) />
</cfif>
<!--- save the content for the xml to define the editor --->
<cfsavecontent variable="xmlSource">
	<cfoutput>
	<editors>
		<editor id="470" name="thisEditor" entity="QuizQuestion" title="phr_elearning_QuizQuestion_Title">
			<field name="QuestionID" label="phr_elearning_QuestionID" description="Unique Question ID." control="html"></field>
			<!--- <field name="QuestionPoolID" label="phr_elearning_QuestionPoolID" description="Unique Question Pool ID." control="hidden" default="#variables.questionPoolID#"></field> --->
			<field name="QuestionPoolID" label="phr_elearning_QuestionPoolRef" control="HTML" value="value" display="display" description="Unique Question Pool" query="SELECT QuestionPoolID AS value, QuestionPoolRef AS display FROM QuizQuestionPool WHERE QuestionPoolID=#variables.questionPoolID#"></field>
			<field name="QuestionRef" label="phr_elearning_QuestionRef" description="Question short reference" required="true"></field>
			<cfif isDefined("variables.getQuizQuestion.QuestionTypeTextID") and variables.getQuizQuestion.QuestionTypeTextID eq "matching" and isDefined("variables.qryGetAnswers.recordCount") and variables.qryGetAnswers.recordCount gt 0>
				<!--- If matching question already has answers then do not allow change of question type --->
				<field name="QuestionTypeID" label="phr_elearning_QuestionType" control="SELECT"
            	query = "select QuestionTypeTextID as display, QuestionTypeID as value from QuizQuestionType where relayQuizCompatible=1 and QuestionTypeTextID = 'matching'"></field>
			<cfelseif isDefined("variables.getQuizQuestion.QuestionTypeTextID") and listFind("radio,checkbox",variables.getQuizQuestion.QuestionTypeTextID) and isDefined("variables.qryGetAnswers.recordCount") and variables.qryGetAnswers.recordCount gt 0>
				<!--- If radio or checkbox question already has answers then do not allow change to matching question type, but do allow change between radio and checkbox --->
				<field name="QuestionTypeID" label="phr_elearning_QuestionType" control="SELECT"
            	query = "select QuestionTypeTextID as display, QuestionTypeID as value from QuizQuestionType where relayQuizCompatible=1 and QuestionTypeTextID in ('radio','checkbox') order by QuestionTypeID"></field>
			<cfelse>
				<!--- If Question has no Answers defined yet, or if adding a new Question, then allow choice of QuestionType --->
				<field name="QuestionTypeID" label="phr_elearning_QuestionType" control="SELECT"
            	query = "select QuestionTypeTextID as display, QuestionTypeID as value from QuizQuestionType where relayQuizCompatible=1 order by QuestionTypeID"></field> <!--- STCR 2012-07-20 P-REL109 Phase 2 - Do not make SCORM-only QuestionTypes available to RelayQuiz Questions --->
			</cfif>
			<field name="" CONTROL="TRANSLATION" label="phr_elearning_QuestionText" Parameters="phraseTextID=title" required="true"></field>
			<field name="sortOrder" control="SELECT" value="value" display="display" label="phr_eLearning_QuestionSortOrder" description="phr_eLearning_QuestionSortOrder" default="0"
				query="select numberValue-1 AS value, numberValue-1 AS display from numberList where numberValue &gt; 0"></field><!--- 0-99 dropdown --->
			<group label="System Fields" name="systemFields">
				<field name="sysCreated"></field>
				<field name="sysLastUpdated"></field>
			</group>
		</editor>
	</editors>
	</cfoutput>
</cfsavecontent>


<cfif not structKeyExists(URL,"editor")>
	<cfset getData = application.com.relayQuiz.getQuizQuestionList(questionPoolID=variables.questionPoolID) />
	<!--- removed a translateQueryColumn - questionText is now automatically translated.  TBD ought to get above function to do the sorting --->
	<cfquery dbtype="query" name="getData">
		SELECT *
		FROM	getData
		ORDER BY <cf_queryObjectName value="#sortOrder#">
	</cfquery>

</cfif>

<cfset functionType="Question">
<cfinclude template="/elearning/elearningFunctions.cfm">

<cf_listAndEditor
	xmlSource="#xmlSource#"
	cfmlCallerName="questions"
	keyColumnList="QuestionRef"
	keyColumnKeyList="QuestionID"
	keyColumnURLList="questions.cfm?editor=yes&questionPoolID=#variables.questionPoolID#&QuestionID="
	showTheseColumns="QuestionRef,QuestionTypeTextID,QuestionID,QuestionText,sortOrder"
	FilterSelectFieldList=""
	FilterSelectFieldList2=""
	useInclude="false"
	includeFile = "/eLearning/questionsInclude.cfm"
	sortOrder="QuestionRef"
	queryData="#getData#"
	numRowsPerPage="#numRowsPerPage#"
	startRow="#startRow#"
	columnTranslation="true"
	columnTranslationPrefix="phr_elearning_"
	backForm="#frmBackForm#"
	rowIdentityColumnName="questionID"
	functionListQuery="#comTableFunction.qFunctionList#"
	passThroughVariablesStructure="#passThroughVars#"
> <!--- 2012-09-04 STCR CASE 430408 pass through questionPoolID to frmBogusForm for listing after deletion function --->
