<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		certificate.cfm
Author:			AJC
Date started:	2012/07/17

Description:	P-REL109 Phase 2 - Code ported over from the Lenovo PS project P-LEN030 CR-018 Elearning PDF Certificate

Usage:

Amendment History:

Date 		Initials 	What was changed
2013/04/11	YMA			Case 434726 allow more fields to be added to PDF for Certifications
2013/04/23	NYB			Case 434166 added debug code, also release 434608 line to fix this
2015/06/03  DCC         Case 444034 changed to requested date format
2015/07/23  DCC     	Case 445275 kasp ensuring no margins on certificates
2015-08-19  VSN    		Case 445554 change to equal CERTIFICATIONID from CERTIFICATIONTYPEID
04/03/2016  DAN         Case 448102 - add personID and locationID to the merge struct so it can print correct values for the person certificate

Enhancements still to do:


 --->
<cfif not application.com.security.confirmFieldsHaveBeenEncrypted(fieldNames="personID,quizTakenID,certificationID") and not (isdefined("request.relaycurrentuser.personid") and request.relaycurrentuser.personid eq "1046")>
	<cfif structkeyexists(url,"debug")><br />abort1<br /></cfif><!--- 2013-04-23 NYB 434166 added --->
	<cf_abort>
</cfif>

<cfif NOT structKeyExists(url,"personid")>
	<cfif structkeyexists(url,"debug")><br />abort2<br /></cfif><!--- 2013-04-23 NYB 434166 added --->
	<cf_abort>
</cfif>

<cfset abortcheck = 0>

<cfif NOT structKeyExists(url,"quiztakenid")>
	<cfif structkeyexists(url,"debug")><br />abort3<br /></cfif><!--- 2013-04-23 NYB 434166 added --->
	<cfset abortcheck = abortcheck + 1>
</cfif>

<cfif NOT structKeyExists(url,"CertificationID")>
	<cfif structkeyexists(url,"debug")><br />abort4<br /></cfif><!--- 2013-04-23 NYB 434166 added --->
	<cfset abortcheck = abortcheck + 1>
</cfif>

<cfif abortcheck GTE 2>
	<cf_abort>
</cfif>

<cfif structKeyExists(url,"quiztakenid") AND structKeyExists(url,"CertificationID")>
	<cfif structkeyexists(url,"debug")><br />abort5<br /></cfif><!--- 2013-04-23 NYB 434166 added --->
	<cf_abort>
</cfif>

<cfset mergeStruct = structNew()>
<cfset showCertificate = false>

<cfif structKeyExists(url,"quiztakenid")>
	<cfset UseElementName = application.com.settings.getSetting("elearning.courseCertificateETID")>
	<cfset CertificateType = "Module">
	<cfset quizTakenDetails = application.com.relayQuiz.getQuiz(quizTakenID=url.quizTakenId)>

    <!--- start: Case 448830 --->
    <cfif quizTakenDetails.recordcount GT 0>
		<cfif quizTakenDetails.score gte quizTakenDetails.passMark>
			<cfset showCertificate = true>
			<cfset mergeStruct = application.com.relayquiz.getQuizCertificationMergeFields(quizTakenID=url.QuizTakenID,personID=url.personid)> <!--- DCC added personID=url.personid --->
			<cfset application.com.relayQuiz.insertCertificatePrint(personID=url.personid,quiztakenid=url.quiztakenid,visitid=request.relaycurrentuser.VISITID) />
		</cfif>
	 <!--- Scorm Quizzes don't have details in the quizdetails table --->
    <cfelse>
       <!--- scorm quizzes course title and date --->
        <cfif isDefined("url.courseID") AND url.courseID NEQ "">
           <cfset displaycondition = true>
           <cfset mergeStruct = application.com.relayquiz.getQuizCertificationMergeFields(quizTakenID=url.QuizTakenID,personID=url.personid,courseID=url.courseID)>
           <cfset application.com.relayQuiz.insertCertificatePrint(personID=url.personid,quiztakenid=url.quiztakenid,visitid=request.relaycurrentuser.VISITID) />
        </cfif>
    </cfif>
    <!--- end: Case 448830 --->
<cfelseif structKeyExists(url,"CertificationID") or structKeyExists(url,"personCertificationID")>
	<cfset UseElementName = application.com.settings.getSetting("elearning.certificationCertificateETID")>	<!--- NJH 2013/04/02 case 434608 - added elearning to setting name which was missing --->
	<cfset CertificateType = "Certification">

	<cfset args = {statustextid='Passed'}>
	<cfif structKeyExists(url,"CertificationID")>
		<cfset args.entityID = request.relaycurrentuser.personid>
		<cfset args.entitytypeID=application.entityTypeID.person>
		<cfset args.certificationID=url.certificationID>
	<cfelse>
		<cfset args.personCertificationID = url.personCertificationID>
	</cfif>

	<cfset passedCertifications = application.com.relayElearning.GetCertificationSummaryData(argumentCollection=args) />

	<!--- 2013/04/11	YMA	Case 434726 allow more fields to be added to PDF for Certifications.  Customize relayElearning.getCertificationMergeFields to add more fields. --->
	<cfset mergeStruct = application.com.relayElearning.getCertificationMergeFields(certificationID=passedCertifications.certificationID[1])>
	<cfset locale = getlocale()><!--- DCC Added Case 444034 used in conjunction with below --->
	<cfset mergeStruct.passdate = LSDateFormat(passedCertifications.PASS_DATE, "dddd, mmmm dd, yyyy")><!--- DCC CASE 444034 changed to requested format --->
	<cfset mergeStruct.certificationTITLE = passedCertifications.TITLE>
	<cfset mergeStruct.certificationID = passedCertifications.CERTIFICATIONID>

	<!--- not passed in if personcertificationId passed in --->
	<cfif not structKeyExists(url,"personID")>
		<cfset url.personID = passedCertifications.personID[1]>
	</cfif>

	<!--- 2014-01-29 PPB Note: an administrator can now set a personCertification to Passed internally without the user actually passing the module, I don't think the following condition is sensible;
	 	as we are only dealing with "passedCertifications" I don't think we need any condition here. It's not currently raised as a ticket and would need testing so I haven't changed it now --->
	<cfif passedCertifications.MODULES_PASSED gte passedCertifications.NUM_MODULES_TO_PASS>
		<cfset showCertificate = true>
		<cfset application.com.relayQuiz.insertCertificatePrint(personID=url.personid,quiztakenid=passedCertifications.certificationID[1],visitid=request.relaycurrentuser.VISITID) />
	</cfif>
</cfif>

<cfif showCertificate>
	<cfset getContent = application.com.relayElementTree.getElement(elementID=UseElementName) />

	<cfset personDetails=application.com.relayPLO.getPersonDetails(url.personid)>
	<cfset mergeStruct.person = personDetails>
	<cfset mergeStruct.personID = personDetails.personID>
	<cfset mergeStruct.locationID = personDetails.locationID>
	<cfset mergeStruct.firstName = personDetails.firstName>
	<cfset mergeStruct.lastName = personDetails.lastName>
	<cfset mergeStruct.orgName = personDetails.organisationName>

	<cfheader name="Content-Disposition" value="inline; filename=certificate.pdf">

	<!--- 2013-04-23 NYB 434166 added 'If debug':--->
	<cfdocument format="PDF" orientation="portrait" overwrite="true" marginbottom="0" marginleft="0" marginright="0" margintop="0">
		<cfoutput><cfif structkeyexists(url,"debug")>showCertificate=true<br /><br />url:<br /><cfdump var="#url#"><br /><br />UseElementName: #UseElementName#<br /><br /></cfif>#application.com.relayTranslations.translatePhrase(phrase="phr_Detail_Element_"&getContent.id,mergeStruct=mergeStruct)#</cfoutput>
	</cfdocument>
<!--- 2013-04-23 NYB 434166 added else and 'If debug':--->
<cfelse>
	<cfif structkeyexists(url,"debug")><cfoutput>showCertificate=false<br /><br />url:<br /><cfdump var="#url#"><br /><br />UseElementName: #UseElementName#<br /><br /></cfoutput></cfif>
</cfif>
