<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		certificationOverview.cfm	
Author:			NJH  
Date started:	26-08-2008
	
Description:	Provide an overview of the certification		

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->


<cf_translate>
	
<cfparam name="certificationID" type="numeric">

<cfset request.relayFormDisplayStyle = "HTML-table">

<cfscript>
	qryCertificationData = application.com.relayElearning.GetCertificationData(certificationID=certificationID);
	qryCertificationRuleData = application.com.relayElearning.GetCertificationRuleData(certificationID=certificationID);
</cfscript>


<cf_relayFormDisplay>
	<cf_relayFormElementDisplay relayFormElementType="HTML" currentValue="#qryCertificationData.description#" label="phr_elearning_certificationDescription">
	<cf_relayFormElementDisplay relayFormElementType="HTML" currentValue="#qryCertificationData.Code#" label="phr_elearning_certificationCode">
	<cf_relayFormElementDisplay relayFormElementType="HTML" currentValue="" label="phr_elearning_CertificationRules">
	<cf_relayFormElementDisplay relayFormElementType="HTML" currentValue="&nbsp;" label="">
	
	<cfloop query="qryCertificationRuleData">
		<cf_relayFormElementDisplay relayFormElementType="HTML" currentValue="#passRate#" label="&nbsp;&nbsp;phr_elearning_passRate">
		<cf_relayFormElementDisplay relayFormElementType="HTML" currentValue="#numModulesToPass#" label="&nbsp;&nbsp;phr_elearning_numModulesToPass">
		<cf_relayFormElementDisplay relayFormElementType="HTML" currentValue="" label="&nbsp;&nbsp;phr_elearning_moduleSets">
		
		<cfscript>
			qryModuleSetData = application.com.relayElearning.GetModuleSetsData(certificationRuleID = certificationRuleID);
		</cfscript>
		
		<cfloop query="qryModuleSetData">
			<cf_relayFormElementDisplay relayFormElementType="HTML" currentValue="#description#" label="&nbsp;&nbsp;&nbsp;&nbsp;phr_elearning_ModuleSetDescription">
		</cfloop>
		<cf_relayFormElementDisplay relayFormElementType="HTML" currentValue="&nbsp;" label="">
	</cfloop>
	<cf_relayFormElementDisplay relayFormElementType="HTML" currentValue="&nbsp;" label="">
</cf_relayFormDisplay>	
</cf_translate>