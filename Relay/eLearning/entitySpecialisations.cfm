<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			entitySpecialisations.cfm	
Author:				NJH
Date started:		2008/08/21
Description:		Display and edit person certifications. This file expects to have a wrapper around it with a cfform. (ie a screen)	

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2008-11-11 			NYB P-SOP010 - added translateColumns param
2009-02-03 			NYB SOP-bug1338 - replaced Code with Title, removed Num_Certifications_To_Pass
2009/09/25			NJH LHID 2283 - get value of frmCurrentEntityID from screen if it exists
2010-08-06			PPB P-SOP024 - added modules required/passed/list
2012-10-05			WAB CAse 431093 Remove Show_Certifications from sortOrder - not valid since added to query using QoQ
2015-11-27          ACPK        BF-12 Added doNotSortTheseColumns to prevent sorting by Show Certifications

Possible enhancements:

 --->


	
<cf_translate>

<cf_param name="sortOrder" default="Title" validvalues="Title,Description,Status,Pass_Date,Num_Certifications_Passed" displayAs="twoSelects"/>
<cfparam name="frmEntitySpecialisationID" default="0" type="numeric"/>
<cf_param name="showTheseColumns" type="string" default="Title,Description,Status,Pass_Date,Num_Certifications_Passed,Show_Certifications" validvalues="Title,Description,Status,Pass_Date,Num_Certifications_Passed,Show_Certifications" multiple="true" size="5"/>
<!--- 2008-11-11 NYB P-SOP010 - added --->
<cf_param name="translateColumns" type="boolean" default="false"/>

<cf_param name="validSpecialisationsOnly" type="boolean" default="true"/>
<cfparam name="keyColumnList" type="string" default="Show_Certifications"/>
<!--- 2009-02-03 NYB SOP-bug1338 - replaced Code with Title, removed Num_Certifications_To_Pass --->

<cfparam name="getData" type="string" default="foo">
<cfparam name="frmEntityID" type="numeric" default="0" >
<cfparam name="frmEntityTypeID" type="numeric" default="2">
<cfparam name="frmCurrentEntityID" type="numeric" default="0">

<cfif frmEntitySpecialisationID eq "">
	<cfset frmEntitySpecialisationID = "0">
</cfif>

<cfset organisationID=0>
<cfset request.relayFormDisplayStyle = "HTML-table">

<cfif isDefined("frmScreenEntityType")> <!--- LHID 2283 NJH 2009/09/25 --->
	<cfset frmEntityType = frmScreenEntityType>
	<cfset frmEntityTypeID = application.entityTypeID[frmEntityType]>
	<cfset frmCurrentEntityID = frmScreenEntityID>
</cfif>

<cfif structKeyExists(form,"frmOrganisationID")>
	<cfset organisationID = frmOrganisationID>
<cfelseif request.relayCurrentUser.isInternal and frmEntityTypeID eq 2>
	<cfset organisationID = frmCurrentEntityID>
<cfelseif not request.relayCurrentUser.isInternal>
	<cfset organisationID = request.relayCurrentUser.person.organisationID>
</cfif>

<cfscript>
	argumentsCollection = structNew();
	argumentsCollection.sortOrder=sortOrder;
	argumentsCollection.entityTypeID=frmEntityTypeID;
	argumentsCollection.validSpecialisationsOnly=validSpecialisationsOnly;
	argumentsCollection.entityID=organisationID;
	
	getData = application.com.relayElearning.GetSpecialisationSummaryData(argumentCollection=argumentsCollection);
</cfscript>

<cfquery name="getData" dbType="query">
	select *, '<img src="/images/icons/bullet_arrow_down.png">' as Show_Certifications from getData
</cfquery>

<cfif getData.recordCount gt 0>

	
	<cf_includeJavascriptOnce template = "/javascript/tableManipulation.js">
	
	<cfset columnToShowAjaxAt = listFindNoCase(getData.columnList,"entitySpecialisationID")>
	
	<script>
		<cfoutput>
		var columnToShowAjaxAt = #jsStringFormat(columnToShowAjaxAt)# </cfoutput>
		function $alt(element) {
		  if (arguments.length > 1) {
		    for (var i = 0, elements = [], length = arguments.length; i < length; i++)
		      elements.push($alt(arguments[i]));
		    return elements;
		  }
		  if (Object.isString(element))
		    element = document.getElementById(element);
		  return Element.extend(element);
		}
		
		function showCertifications (obj,id) {
			//alert(obj+","+id);
			currentRowObj = getParentOfParticularType (obj,'TR')
			currentTableObj = getParentOfParticularType (obj,'TABLE')

			totalColspan = 0
			for (i=0;i<currentRowObj.cells.length;i++) {
				totalColspan  += currentRowObj.cells[i].colSpan
			}
	
			infoRow =  id + '_info'
			infoRowObj = $alt(infoRow)
			

			if (!infoRowObj) {
				page = '/webservices/relayElearningWS.cfc?wsdl&method=getSpecialisationItems'
				page = page + '&entitySpecialisationID='+id
	
				// changed to post to ensure proper refresh
				var myAjax = new Ajax.Request(
					page, 
					{
						method: 'post', 
						parameters: '', 
						evalJSON: 'force',
						debug : false,
						onComplete: function (requestObject,JSON) {
							json = requestObject.responseJSON
							rowStructure = new Array (2)
							rowStructure.row = new Array (1) 
							rowStructure.cells = new Array (1) 
							rowStructure.cells[0] = new Array (1) 
							rowStructure.cells[0].content  = json.CONTENT 
							rowStructure.cells[0].colspan  = totalColspan //-rowStructure.cells[0].colspan
							rowStructure.cells[0].align  = 'right'
	
							regExp = new RegExp ('_info')
							deleteRowsByRegularExpression (currentTableObj,regExp)
							
							addRowToTable (currentTableObj,rowStructure,obj.parentNode.parentNode.rowIndex + 1,infoRow)
						
							// 2014-06-17	YMA	Re-initialize responsive table on AJAX content
							jQuery(currentTableObj).trigger('create');
							}
					});
			} 
			else
			{
				regExp = new RegExp (infoRow)
				deleteRowsByRegularExpression (currentTableObj,regExp)
			}
			
			return 
		}
		
		function getParentOfParticularType (obj,tagName) {	
			if (obj.parentNode.tagName == tagName) 	{
				return obj.parentNode
			} else {
				return getParentOfParticularType (obj.parentNode,tagName)
			}
		}
	</script>

<!--- 2008-11-11 NYB P-SOP010 - added #translateColumns# & ColumnTranslationPrefix --->
<!--- 2015-11-27    ACPK    BF-12 Added doNotSortTheseColumns to prevent sorting by Show Certifications --->
	<CF_tableFromQueryObject 
		queryObject="#getData#"
		queryName="getData"
	 	hideTheseColumns=""
		showTheseColumns="#showTheseColumns#"
		doNotSortTheseColumns="Show_Certifications"
		columnTranslation="#translateColumns#"
		ColumnTranslationPrefix="phr_specialisation_"
		dateformat="Pass_Date,Expired_Date"
		numberFormat="Num_Certifications_To_Pass,Num_Certifications_Passed,Num_Modules_To_Pass,Num_Modules_Passed"
		useInclude="false"
		keyColumnList="#keyColumnList#"
		keyColumnURLList=" "
		keyColumnKeyList="entitySpecialisationID"
		keyColumnOnClickList="javascript:showCertifications(this*comma##entitySpecialisationID##);return false"
		keyColumnOpenInWindowList="no"
		rowIdentityColumnName="entitySpecialisationID"
		HidePageControls="yes"
	>
	<div id="dummy"></div>
<cfelse>
	<div align="center">phr_elearning_noSpecialisationsHaveBeenAchieved</div>
</cfif>


</cf_translate>


