<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		eLearning\answers.cfm	
Author:			STCR  
Date started:	2012-05-03
	
Description: Manage Answers to a QuizQuestion, layout based on code from certificationRules.cfm

Amendment History:

Date (YYYY-MM-DD)	Initials 	What was changed
2012-08-01			STCR		Matching Questions
2012-08-24			AJC			'Delete' option
2013-02-22			PPB			Case 433209 show/edit AnswerRef against each answer
2013-04-29			STCR		Specify cfmlCallerName when calling RelayXMLEditor directly
2013-05-08			STCR		CASE 434612 / 433209 AnswerRef consistency across add/edit forms and question types
2016-05-18			WAB			During BF-714 Answer List drop down. Remove reference to getExtendedMetaData.sql and replace with func:...GetAnswerList()

Possible enhancements:

 --->


<cfparam name="url.QuestionID" type="numeric" default="0" />
<cfparam name="form.QuestionID" type="numeric" default="#url.QuestionID#" />
<cfparam name="variables.QuestionID" type="numeric" default="#form.QuestionID#" />
<cfparam name="frmMatchingAnswerID" type="numeric" default="0" />


<cfset variables.getQuizQuestion = application.com.RelayQuiz.getQuizQuestionList(QuestionID=variables.QuestionID) /><!--- STCR Phase 2 - need to know QuestionType now because we render the form differently for matching questions --->

<script type="text/javascript">

function confirmDelete(answer)
	{
		<cfoutput>var msg = '#JSStringFormat("phr_elearning_confirm_delete_answer ")#' + answer + '?';</cfoutput>

		var agree=confirm(msg);
		if (agree)
			return true ;
		else
			return false ;
	}

function matchingAnswerChangeHandler() {
	var LHSAnswerText = document.getElementById('frmLHSAnswerText');
	var matchingAnswerDropdown = document.getElementById('frmMatchingAnswerDropdown');
	var matchingAnswerNewText = document.getElementById('frmMatchingAnswerNewText');
	var matchingAnswerNewRef = document.getElementById('frmMatchingAnswerNewRef');
	var scoreCell = document.getElementById('MatchingPairScoreCell');
	var scoreField = document.getElementById('frmScore');
	var matchingAnswerID = matchingAnswerDropdown.options[matchingAnswerDropdown.selectedIndex].value;
	if(LHSAnswerText.value == '') {
		matchingAnswerDropdown.selectedIndex = 0;
		matchingAnswerDropdown.disabled = true;
		scoreField.value == '';
		scoreField.disabled = true;
	}
	else {
		matchingAnswerDropdown.disabled = false;
		scoreField.disabled = false;
	}
	if(matchingAnswerID != null && matchingAnswerID != '' && matchingAnswerID != '0') <!--- If existing RHS option selected --->
	{
		matchingAnswerNewText.style.visibility = 'hidden';
		matchingAnswerNewRef.style.visibility = 'hidden';
	}
	else <!--- If no RHS option selected then prompt user to input new RHS option --->
	{
		matchingAnswerNewText.style.visibility = 'visible';
		matchingAnswerNewRef.style.visibility = 'visible';
	}
}	

function validateNewAnswer() {
	<cfif variables.getQuizQuestion.QuestionTypeTextID neq "matching">
		return true;
	<cfelse>
		var alertMsg = '';
		<!--- Valid submissions:
			a) frmLHSAnswerText != '' && frmMatchingAnswerDropdown == 0 && frmMatchingAnswerNewText != '' && frmMatchingAnswerNewRef != ''
			b) frmLHSAnswerText != '' && frmMatchingAnswerDropdown != 0 && frmMatchingAnswerNewText == ''
			c) frmLHSAnswerText == '' && frmMatchingAnswerDropdown == 0 && frmMatchingAnswerNewText != '' && frmMatchingAnswerNewRef != ''
		 --->
		var LHSAnswerTextValue = document.getElementById('frmLHSAnswerText').value;
		var matchingAnswerNewTextValue = document.getElementById('frmMatchingAnswerNewText').value;
		var matchingAnswerNewRefValue = document.getElementById('frmMatchingAnswerNewRef').value;
		var matchingAnswerDropdown = document.getElementById('frmMatchingAnswerDropdown');
		var matchingAnswerID = matchingAnswerDropdown.options[matchingAnswerDropdown.selectedIndex].value;
		LHSAnswerTextValue = LHSAnswerTextValue.replace(/^\s+|\s+$/g,'');
		matchingAnswerNewTextValue = matchingAnswerNewTextValue.replace(/^\s+|\s+$/g,'');
		matchingAnswerNewRefValue = matchingAnswerNewRefValue.replace(/^\s+|\s+$/g,'');
		if(LHSAnswerTextValue != null && LHSAnswerTextValue != '' 
				&& (matchingAnswerID == null || matchingAnswerID == '' || matchingAnswerID == '0') 
				&& matchingAnswerNewTextValue != null && matchingAnswerNewTextValue != ''
				&& matchingAnswerNewRefValue != null && matchingAnswerNewRefValue != ''
				) {
			return true;
		}
		else if(LHSAnswerTextValue != null && LHSAnswerTextValue != '' 
				&& matchingAnswerID != null && matchingAnswerID != '' && matchingAnswerID != '0' 
				&& (matchingAnswerNewTextValue == null || matchingAnswerNewTextValue == '')
				) {
			return true;
		}
		else if( (LHSAnswerTextValue == null || LHSAnswerTextValue == '') 
				&& (matchingAnswerID == null || matchingAnswerID == '' || matchingAnswerID == '0') 
				&& matchingAnswerNewTextValue != null && matchingAnswerNewTextValue != ''
				&& matchingAnswerNewRefValue != null && matchingAnswerNewRefValue != ''
				) {
			return true;
		}
		else {
			<cfoutput>alertMsg = '#jsStringFormat("phr_elearning_RHS_Option_Required ")# \n#jsStringFormat("phr_elearning_RHS_AnswerRef_Required ")#';</cfoutput>
			alert(alertMsg);
			return false;
		}
	</cfif>
}

</script>

<cfif structKeyExists(url,"editor") and url.editor eq "yes">
	<cfset qryGetAnswers = application.com.relayQuiz.GetAnswerList(questionID=questionID) />

	<cfset url.answerID=answerID>
	<cfset url.certificationRuleTypeID=0>
	<!--- save the content for the xml to define the editor --->
	<cfsavecontent variable="xmlSource">
		<cfoutput>
		<editors>
			<editor id="232" name="thisEditor" entity="QuizAnswer" title="phr_eLearning_AnswerEditor">
				<field name="answerID" label="phr_elearning_answerID" description="Unique Answer ID." control="html"></field>
				<field name="questionID" label="phr_elearning_questionID" description="Unique Question ID." control="hidden" default="#variables.QuestionID#"></field>
				<field name="answerRef" label="phr_eLearning_AnswerRef" description="phr_eLearning_AnswerRef" size="12"></field>				<!--- 2013-02-22 PPB Case 433209 show/edit AnswerRef against each answer --->
				<field name="" control="TRANSLATION" label="phr_eLearning_AnswerText" Parameters="phraseTextID=Title" required="true"></field>
				<cfif variables.getQuizQuestion.QuestionTypeTextID eq "matching">
					<cfset variables.answerHasLHSMatches = false />
					<cfloop query="qryGetAnswers"><cfif qryGetAnswers.MatchingAnswerID eq url.AnswerID><cfset variables.answerHasLHSMatches = true /></cfif></cfloop>
					<cfif not variables.answerHasLHSMatches><!--- The same rhs option can be used multiple times, however we cannot use the same answer as both the lhs option of one answer and the rhs option to others. --->
						<field name="MatchingAnswerID" label="phr_elearning_MatchingAnswerColumnTitle" control="SELECT" 
						query = "func:com.relayQuiz.GetAnswerList(questionID=#questionID#,forDropDown=true,forMatchingQuestionType=true,excludeAnswerID=#answerID#)"></field>
						<!--- Hide score for RHS options --->
						<field name="Score" control="numeric" label="phr_eLearning_AnswerScore" description="phr_eLearning_AnswerScore" default="0" required="true"></field>
					</cfif>
				<cfelse>
					<field name="Score" control="numeric" label="phr_eLearning_AnswerScore" description="phr_eLearning_AnswerScore" default="0" required="true"></field>
				</cfif>			
				<field name="sortOrder" control="numeric" label="phr_eLearning_AnswerSortOrder" description="phr_eLearning_AnswerSortOrder" default="0"></field>
			</editor>	
		</editors>
		</cfoutput>
	</cfsavecontent>
	
	<cfif structKeyExists(form,"update")>
		<cfset application.com.relayEntity.updateEntityDetails(entityID=form.answerID,entityDetails=form,entityTypeID=application.entityTypeID.quizAnswer)>
	</cfif>
	
	<CF_RelayXMLEditor
		xmlSourceVar = "#xmlSource#"
		editorName = "thisEditor"
		showSaveAndReturn = "true"
		cfmlCallerName = "answers"
		thisEmailAddress = "relayhelp@foundation-network.com"
	>
	
<cfelse>
		
	<!--- adding an answer --->
	<cfif structKeyExists(form,"frmAddAnswer")>
		<cfif not isDefined("frmScore") or not isNumeric(frmScore)>
			<cfset frmScore = 0 />
		</cfif>
		<cfif not isDefined("frmSortOrder") or not isNumeric(frmSortOrder)>
			<cfset frmSortOrder = 0 />
		</cfif>
		<cfif not isDefined("frmAnswerRef")> <!--- 2013-05-08 STCR CASE 434612 / 433209 --->
			<cfset frmAnswerRef = "" />
		</cfif>
		<cfif structKeyExists(form,"frmMatchingAnswerDefaultTranslation") and not isDefined("frmMatchingAnswerRef")>
			<cfset frmMatchingAnswerRef = "" />
		</cfif>
		<cfscript>
			/* 2012-08-28 STCR usability - if a pair of matching options were submitted, then save the RHS first and use its AnswerID when saving the LHS */
			variables.newRHSAnswerID = 0;
			if(structKeyExists(form,"frmMatchingAnswerDefaultTranslation") and len(trim(form.frmMatchingAnswerDefaultTranslation)) gt 0) {
				variables.newRHSAnswerID = application.com.relayQuiz.insertAnswer(questionID=variables.questionID,defaultTranslation=frmMatchingAnswerDefaultTranslation, MatchingAnswerID=0, AnswerRef=frmMatchingAnswerRef, Score=0,sortOrder=frmSortOrder);
			}
			if(variables.newRHSAnswerID neq 0 and len(trim(frmDefaultTranslation)) gt 0) {
				application.com.relayQuiz.insertAnswer(questionID=variables.questionID,defaultTranslation=frmDefaultTranslation, MatchingAnswerID=variables.newRHSAnswerID, AnswerRef=frmAnswerRef, Score=frmScore,sortOrder=frmSortOrder);
			}
			else if(variables.newRHSAnswerID eq 0) {
				application.com.relayQuiz.insertAnswer(questionID=variables.questionID,defaultTranslation=frmDefaultTranslation, MatchingAnswerID=frmMatchingAnswerID, AnswerRef=frmAnswerRef, Score=frmScore,sortOrder=frmSortOrder);
			}
		</cfscript>
	<cfelseif structKeyExists(form,"frmDeleteAnswer")>
		<cfscript>
			application.com.relayQuiz.deleteAnswer(answerID=form.answerID);
		</cfscript>
	</cfif>
	<cfset qryGetAnswers = application.com.relayQuiz.GetAnswerList(questionID=questionID) /><!--- 2012-08-13 STCR CASE 430067 retrieve query after answers are added or edited, so that latest added Answer is not missing from the list --->
	
	<cfset request.relayFormDisplayStyle = "HTML-table">
	
	<cf_relayFormDisplay>
	
	<cfif qryGetAnswers.recordCount gt 0>
		<cfif variables.getQuizQuestion.QuestionTypeTextID eq "matching">
			<!--- 2012-08-28 STCR usability - get list of RHS options which do have LHS pairings, 
				so that later on we can determine which options are left unpaired and therefore need a display row of their own --->
			<cfset MatchingAnswerIDList = ValueList(qryGetAnswers.MatchingAnswerID) />
			<tr>
				<!--- <th>phr_elearning_Matching_Option</th> --->
				<th colspan="2">phr_elearning_LHS_Option</th>
				<th colspan="2">phr_elearning_RHS_Option</th>
				<th>phr_elearning_score</th>
				<th>phr_elearning_SortOrder</th>
				<th>&nbsp;</th>
			</tr>
			<cfset formName="DeleteAnswerForm">
			<cfloop query="qryGetAnswers">
				<!--- 2012-08-28 STCR usability - do not repeat RHS options on LHS. 
					"If this is a LHS option, or an unpaired RHS option..." --->
				<cfif isNumeric(qryGetAnswers.MatchingAnswerID) and qryGetAnswers.MatchingAnswerID gt 0 or ListFind(MatchingAnswerIDList,qryGetAnswers.AnswerID) eq 0>
					<cfform name="#formName#" action="" method="post">
					<tr>
						<!--- <td><cf_relayFormElement relayFormElementType="HTML" currentValue="<a href='/elearning/answers.cfm?editor=yes&answerID=#answerID#&questionID=#questionID#'>phr_eLearning_Matching_Option #htmleditformat(currentRow)#</a>" fieldname="" label=""></td> --->
						<td colspan="2"><cfif isNumeric(MatchingAnswerID) and MatchingAnswerID gt 0><cfoutput><a href='/elearning/answers.cfm?editor=yes&answerID=#answerID#&questionID=#questionID#'> phr_Title_QuizAnswer_#answerID# </a></cfoutput><cfelse>&nbsp;</cfif></td>
						<td colspan="2"><cfif isNumeric(MatchingAnswerID) and MatchingAnswerID gt 0><cfoutput><a href='/elearning/answers.cfm?editor=yes&answerID=#MatchingAnswerID#&questionID=#questionID#'> phr_Title_QuizAnswer_#MatchingAnswerID# </a></cfoutput><cfelse><cfoutput><a href='/elearning/answers.cfm?editor=yes&answerID=#answerID#&questionID=#questionID#'> phr_Title_QuizAnswer_#answerID# </a></cfoutput></cfif></td>
						<td><cfif isNumeric(qryGetAnswers.MatchingAnswerID) and qryGetAnswers.MatchingAnswerID gt 0><cf_relayFormElement relayFormElementType="HTML" currentValue="#score#" fieldname="" label=""></cfif></td><!--- Hide score for unpaired RHS options --->
						<td><cf_relayFormElement relayFormElementType="HTML" currentValue="#sortOrder#" fieldname="" label=""></td>
						<td><cf_relayFormElement relayFormElementType="submit" currentValue="phr_Delete" fieldname="frmDeleteAnswer" label="" required="yes" onClick="return confirmDelete('phr_Title_QuizAnswer_#answerID#')"></td>
					</tr>
					<cf_relayFormElement relayFormElementType="hidden" currentValue="#answerID#" fieldname="answerID" label="" required="yes">
					</cfform>
				</cfif>
			</cfloop>
		<cfelse><!--- radio,checkbox --->
			<tr>
				<th>phr_elearning_QuizAnswer</th>
				<th colspan="2">phr_elearning_AnswerText</th>
				<!---<th>phr_elearning_matchingAnswerID</th>--->
				<th>phr_elearning_answerRef</th>					<!--- 2013-02-22 PPB Case 433209 --->
				<th>phr_elearning_score</th>
				<th>phr_elearning_SortOrder</th>
				<th>&nbsp;</th>
			</tr>
			<cfset formName="DeleteAnswerForm">
			<cfloop query="qryGetAnswers">
				<cfform name="#formName#" action="" method="post">
					<tr>
						<td><cf_relayFormElement relayFormElementType="HTML" currentValue="<a href='/elearning/answers.cfm?editor=yes&answerID=#answerID#&questionID=#questionID#'>phr_eLearning_QuizAnswer #htmleditformat(currentRow)#</a>" fieldname="" label=""></td>
						<td colspan="2"><cfoutput>phr_Title_QuizAnswer_#answerID#</cfoutput></td>
						<!--- <td><cf_relayFormElement relayFormElementType="HTML" currentValue="#matchingAnswerID#" fieldname="" label=""></td> --->
						<td><cf_relayFormElement relayFormElementType="HTML" currentValue="#answerRef#" fieldname="" label=""></td>						<!--- 2013-02-22 PPB Case 433209 --->
						<td><cf_relayFormElement relayFormElementType="HTML" currentValue="#score#" fieldname="" label=""></td>
						<td><cf_relayFormElement relayFormElementType="HTML" currentValue="#sortOrder#" fieldname="" label=""></td>
						<td><cf_relayFormElement relayFormElementType="submit" currentValue="phr_Delete" fieldname="frmDeleteAnswer" label="" required="yes" onClick="return confirmDelete('phr_Title_QuizAnswer_#answerID#')"></td>
					</tr>
					<cf_relayFormElement relayFormElementType="hidden" currentValue="#answerID#" fieldname="answerID" label="" required="yes">
				</cfform>
			</cfloop>
		</cfif>
	
	</cfif>
	
	<cfset formName="addAnswerForm">
	
	<cfform name="#formName#" action="" method="post"  onsubmit="return validateNewAnswer();">
		<cf_relayFormElement relayFormElementType="hidden" currentValue="#questionID#" fieldname="questionID" label="">
		
		<cfif variables.getQuizQuestion.QuestionTypeTextID eq "matching">
			<tr><td colspan="7">phr_elearning_MatchingAnswerSetupIntro</td></tr>
			<tr>
				<th colspan="7">phr_elearning_AddNewOption</th>
			</tr>
		<cfelse>
			<tr><td colspan="7">&nbsp;</td></tr>
			<tr>
				<th colspan="7">phr_elearning_AddNewAnswer</th>
			</tr>
		</cfif>
		
			<tr>
				<cfif variables.getQuizQuestion.QuestionTypeTextID neq "matching">
					<td>&nbsp;</td>
					<th colspan="2">phr_elearning_AnswerText</th>
					<th>phr_elearning_answerRef</th>						<!--- 2013-02-22 PPB Case 433209 ---><!--- 2013-05-08 STCR CASE 434612 include AnswerRef on Add form since CASE 433209 already made AnswerRef mandatory on Edit form. --->
				<cfelse>
					<th>phr_elearning_LHS_Option</th>
					<th>phr_elearning_LHS_AnswerRef</th> <!--- 2013-05-08 STCR CASE 434612 --->
					<th>phr_elearning_RHS_Option</th>
					<th>phr_elearning_RHS_AnswerRef</th> <!--- 2013-05-08 STCR CASE 434612 --->
				</cfif>
				<th>phr_elearning_score</th>
				<th>phr_elearning_SortOrder</th>
				<th>&nbsp;</th>
			</tr>
			
			<tr>
				<cfif variables.getQuizQuestion.QuestionTypeTextID eq "matching">
					<td><cf_relayFormElement relayFormElementType="text" currentValue="" fieldname="frmDefaultTranslation" label="" required="no" size="40" maxlength="4000" id="frmLHSAnswerText" onchange="matchingAnswerChangeHandler();"></td>
					<!--- 2012-08-13 STCR CASE 430067 retrieve query after answers are added or edited, so that latest added Answer is not missing from the list --->
					<cfset qryGetAnswersForDropDown = application.com.relayQuiz.GetAnswerList(questionID=variables.questionID,forDropDown=true,forMatchingQuestionType=true,forAddForm=true) />
					<td><cf_relayFormElement relayFormElementType="text" currentValue="" fieldname="frmAnswerRef" label="phr_elearning_LHS_answerRef" required="no" size="15" maxlength="100"></td> <!--- 2013-05-08 STCR CASE 434612 --->
					<td>
						<cf_relayFormElement relayFormElementType="select" currentValue="" fieldname="frmMatchingAnswerID" label="" query="#qryGetAnswersForDropDown#" display="display" value="value" required="no" id="frmMatchingAnswerDropdown" disabled="true" onchange="matchingAnswerChangeHandler();">
						<cf_relayFormElement relayFormElementType="text" currentValue="" fieldname="frmMatchingAnswerDefaultTranslation" label="" required="no" size="40" maxlength="4000" id="frmMatchingAnswerNewText" style="visibility:visible;" onchange="matchingAnswerChangeHandler();">
					</td>
					<td id="MatchingAnswerRefCell" style="visibility:visible;"><cf_relayFormElement relayFormElementType="text" currentValue="" fieldname="frmMatchingAnswerRef" id="frmMatchingAnswerNewRef" style="visibility:visible;" label="phr_elearning_RHS_answerRef" required="no" size="15" maxlength="100"></td> <!--- 2013-05-08 STCR CASE 434612 --->
					<!--- <td><cf_relayFormElement relayFormElementType="numeric" currentValue="" fieldname="frmMatchingAnswerID" label="" required="no" size="5"></td> --->
					<td id="MatchingPairScoreCell" style="visibility:visible;"><cf_relayFormElement relayFormElementType="numeric" currentValue="" fieldname="frmScore" label="" required="no" size="5" id="frmScore" disabled="true"></td>
				<cfelse>
					<td>&nbsp;</td>
					<td colspan="2"><cf_relayFormElement relayFormElementType="text" currentValue="" fieldname="frmDefaultTranslation" label="phr_elearning_AnswerText" required="yes" size="40" maxlength="4000"></td>
					<td><cf_relayFormElement relayFormElementType="text" currentValue="" fieldname="frmAnswerRef" label="phr_elearning_answerRef" required="yes" size="15" maxlength="100"></td>					<!--- 2013-02-22 PPB Case 433209 ---><!--- 2013-05-08 STCR CASE 434612 --->
					<td><cf_relayFormElement relayFormElementType="numeric" currentValue="" fieldname="frmScore" label="phr_elearning_score" required="yes" size="5"></td>
				</cfif>
				<td><cf_relayFormElement relayFormElementType="numeric" currentValue="" fieldname="frmSortOrder" label="" required="no" size="5"></td>
				<td><cf_relayFormElement relayFormElementType="submit" currentValue="phr_Add" fieldname="frmAddAnswer" label="" required="yes"></td>
			</tr>
	
	</cfform>
	</cf_relayFormDisplay>
	
</cfif>