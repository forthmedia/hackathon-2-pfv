<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
NYB	2009-09-24	LHID2667 - removed AvgTimeModule,SumTimeModule from showTheseColumns and renamed AvgNumberModule to AvgNumberQuizAttempts
2012-07-12			PJP			Case#429382 Fixed Filter not working

 --->

<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">
<cfparam name="openAsExcel" type="boolean" default="false">

<cf_title>Training Report - By Course, By Module Summary</cf_title>

<cfparam name="sortOrder" default="AICCCourseTitleDefault">
<cfparam name="numRowsPerPage" default="50">
<cfparam name="startRow" default="1">

<cfparam name="checkBoxFilterName" type="string" default="">
<cfparam name="checkBoxFilterLabel" type="string" default="">

<cfquery name="getTrngCourseModuleSummary" datasource="#application.siteDataSource#">
	SELECT AICCCourseTitleDefault,cast(AvgQuizScore as decimal(12,2)) as AvgQuizScore,AvgNumberQuizAttempts 
	from vTrngCourseModuleSummary WHERE 1=1
	<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
	order by <cf_queryObjectName value="#sortOrder#"> 
</cfquery>


<table width="100%" cellpadding="3">
	<tr>
		<td><strong>Training Report - By Course, By Module Summary</strong></td>
		<td align="right">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">Details of all course and related modules</td>
	</tr>
</table>

<CF_tableFromQueryObject 
	queryObject="#getTrngCourseModuleSummary#"
	queryName="getTrngCourseModuleSummary"
	sortOrder = "#sortOrder#"
	numRowsPerPage="#numRowsPerPage#"
	startRow="#startRow#"
	openAsExcel="#openAsExcel#"
 	hideTheseColumns=""
	showTheseColumns="AICCCourseTitleDefault,AvgQuizScore,AvgNumberQuizAttempts"
	columnTranslation="true"
	dateformat=""
	
	FilterSelectFieldList="AICCCourseTitleDefault"
	GroupByColumns="AICCCourseTitleDefault"
	radioFilterLabel=""
	radioFilterDefault=""
	radioFilterName=""
	radioFilterValues=""
	checkBoxFilterName=""
	checkBoxFilterLabel=""
	allowColumnSorting="yes"
	showCellColumnHeadings="no"
>