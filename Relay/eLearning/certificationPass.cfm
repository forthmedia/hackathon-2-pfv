<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			CertificationRegistrations.cfm	
Author:				NYB
Date started:		2009/05/04
Description:		Pass person certifications

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed

Possible enhancements:

 --->

<cfif request.relayCurrentUser.isInternal>
	<cf_translate>
	
	<cfparam name="frmEntityID" default="0" >
	<cfparam name="frmEntityTypeID" type="numeric" default="0">
	<cfparam name="frmCurrentEntityID" type="numeric" default="0">
	
	<cfparam name="formName" type="string" default="mainForm">
	<cfparam name="certificationTypeID" type="numeric" default="0">
	
	<cfset personID=0>
	<cfset request.relayFormDisplayStyle = "HTML-table">
	
	<!--- set the personID if we're showing certifications on a person level --->
	<cfif frmEntityTypeID eq 0 and frmCurrentEntityID neq 0>
		<cfset personID = frmCurrentEntityID>
	<cfelseif frmEntityTypeID eq 0 and frmEntityID neq 0>
		<cfset personID = frmEntityID>
	</cfif>
	
	<cfif personID neq 0>
		<cfscript>
			argumentsCollection = structNew();
			argumentsCollection.showActive=true;
			if (certificationTypeID neq 0) {
				argumentsCollection.certificationTypeID=certificationTypeID;
			}
			qryCertificationData = application.com.relayElearning.GetCertificationData(argumentCollection=argumentsCollection);
		</cfscript>
		
		<!--- add the person certification --->
		<cfset certificationPassedList="">
		<cfset certificationFailedList="">
		<cfif structKeyExists(form,"frmInsertCertification")>
			<cfloop query="qryCertificationData">
				<cfset allRulesPassed="true">
				<cfif isDefined("frmCertificationID#certificationID#")>
<!---
<cfoutput><br/>1. Certificationid=#certificationID#;  Certification Row: #qryCertificationData.CurrentRow#</cfoutput>
--->
					<cfscript>
						registered = application.com.relayElearning.insertPersonCertificationData(certificationID=certificationID,personID=personID,registerDate=frmPassDate);
						qryCertificationModules = application.com.relayElearning.getModules(certificationID=certificationID,SortOrder="CertificationRuleID",incModulesUserDoesntHaveRightsTo="true");
					</cfscript>
<!---
<br/><cfdump var="#qryCertificationModules#">					
--->

					<cfoutput query="qryCertificationModules" group="certificationRuleID">

<!---
<br/>2. CertificationRuleID: #CertificationRuleID#
--->
						<cfif allRulesPassed>
<!---
<br/>2ba. CertificationRules: #qryCertificationModules.CurrentRow#
--->
							<cfset ruleMet="false">
							<cfset modulesProcessed = 0>
							<cfoutput>
								<cfif not ruleMet>
<!---
<br/>3. Modules: #qryCertificationModules.CurrentRow#
--->
									<cfset modulesProcessed=modulesProcessed+1>
									<cfset userModuleProgressID = application.com.relayElearning.insertPersonModuleProgress(moduleID=moduleID,personID=personID)>
									<cfset setModuleCompleteResultStruct = application.com.relayElearning.setModuleComplete(userModuleProgressID=userModuleProgressID,CompletedDate=frmPassDate,personID=personID,status="Passed",CertificationPassDate=frmPassDate)>						
<!---
<br/>4. modulesProcessed=#modulesProcessed#; numModulesToPass=#numModulesToPass# 
--->
									<cfif modulesProcessed gte numModulesToPass>
<!---
<br/>6. ruleMet 
--->
										<cfset ruleMet="true">
									</cfif>
<!---
<br/>3b. Modules: #qryCertificationModules.CurrentRow#; modulesProcessed=#modulesProcessed#; userModuleProgressID=#userModuleProgressID#;
--->
								</cfif>
							</cfoutput>
							<cfif not ruleMet>
								<cfset allRulesPassed="false">
<!---
<br/>5. rule NOT Met
--->
							</cfif>
<!---
<br/>2b. CertificationRules: #qryCertificationModules.CurrentRow#
--->
						</cfif>
					</cfoutput>
					<cfif allRulesPassed>
						<cfset certificationPassedList = listAppend(certificationPassedList,certificationID)>
					<cfelse>
						<cfset certificationFailedList = listAppend(certificationFailedList,certificationID)>
						<cfset ruleMet="true">
					</cfif>
<!---
<cfoutput><br/>1b. Certifications: #qryCertificationData.CurrentRow#</cfoutput>
--->
				</cfif>
			</cfloop>
			
			<!--- Catch additional failures, for eg, where the only moduleSet contained in the rule is Inactive --->
			<cfloop index="i" list="#certificationPassedList#">
				<cfset qryPersonCertifications = application.com.relayElearning.GetPersonCertificationData(entityID=personID,entityTypeID=0,certificationID=i)>
				<cfif lcase(qryPersonCertifications.StatusTextID) neq "passed">
					<cfset certificationPassedList = ListDeleteAt(certificationPassedList, ListFind(certificationPassedList,i))>
					<cfset certificationFailedList = ListAppend(certificationFailedList, i)>
				</cfif>
			</cfloop>
			<cfset message = "">
			<cfif certificationPassedList neq "">
				<cfset message = "phr_elearning_TheFollowingCertsHaveBeenPassed:<br>">
				<cfquery name="getCertificationCodes" dbType="query">
					select title from qryCertificationData where certificationID in (#certificationPassedList#)
				</cfquery>
				<cfset message = message & valueList(getCertificationCodes.title,"<br>")>
			</cfif>

			<!--- List the certifications that could not be passed.  The only reason this should ever happen 
					if because:
					- the numModuleToPass is greater then the number of modules assigned to that certification rule 
					- the only moduleSet contained in the rule is Inactive
			--->
			<cfif certificationFailedList neq "">
				<cfif certificationPassedList neq "">
					<cfset message = message & "<br><br>">
				</cfif>
				<cfset message = message & "phr_elearning_TheFollowingCertsCouldNotBePassed:<br>">
				<cfquery name="getCertificationCodes" dbType="query">
					select title from qryCertificationData where certificationID in (#certificationFailedList#)
				</cfquery>
				<cfset message = message & valueList(getCertificationCodes.title,"<br>")>
			</cfif>

		</cfif>
		
		<cfquery name="getAvailableCertifications" dbType="query">
			select title+' ('+description+')' as qryDisplay,certificationID as qryValue 
			from qryCertificationData
			where countryID in (#request.relaycurrentuser.CountryList#,#request.relayCurrentUser.regionListSomeRights#)
			<!---	2012-07-24 PPB P-SMA001 getRightsFilterWhereClause() cannot be used in a QOQ (cos it uses an IN clause) so if more than simple country-scoping is required this should be moved into the main query					 
			where 1=1 #application.com.rights.getRightsFilterWhereClause(entityType="trngCertification",alias="qryCertificationData",regionList="#request.relayCurrentUser.regionListSomeRights#").whereClause#	<!--- 2012-07-24 PPB P-SMA001 --->	
			--->
		</cfquery>
		
		<cf_relayFormDisplay>
			<cfif isDefined("message") and message neq "">
				<cf_relayFormElementDisplay relayFormElementType="Message" currentValue="#message#" fieldname="" label="">
			</cfif>
	
			<cfset validCertificationCount = 0>
			<cf_relayFormElementDisplay relayFormElementType="HTML" currentValue="" fieldname="" label="phr_elearning_availableCertifications">
				<cfif getAvailableCertifications.recordCount gt 0>
					<cfloop query="getAvailableCertifications">
						<cfset disabled = false>
						<cfset value="">
						<cfset certificationStatus="">
						
						<cfset qryPersonCertifications = application.com.relayElearning.GetPersonCertificationData(entityID=personID,entityTypeID=0,certificationID=qryValue)>
						<cfset qryEligibleCertifications = application.com.relayElearning.getEligibleCertifications(certificationID=qryValue,personID=personID)>
						<cfset disabled = false>
						<cfif (qryPersonCertifications.recordCount gt 0 and ListContainsNoCase(ValueList(qryPersonCertifications.status), "Passed"))>
							<cfset value=qryValue>
							<cfset disabled = true>
							<cfset validCertificationCount = validCertificationCount+1>
						</cfif> 
						
						<cfif qryEligibleCertifications.recordCount eq 0>
							<cfset disabled = true>
						</cfif>
						
						<cf_relayFormElement relayFormElementType="checkbox" currentValue="#value#" value="#qryValue#" fieldname="frmCertificationID#qryValue#" label="" disabled="#disabled#">&nbsp;<cfoutput>#htmleditformat(qryDisplay)##htmleditformat(certificationStatus)#</cfoutput><br>
					</cfloop>
				<cfelse>
					phr_elearning_NoCertificationsToPass.
				</cfif>
			</cf_relayFormElementDisplay>
			
			<cfset passDateElementType = "hidden">
			<cfif request.relayCurrentUser.isInternal>
				<cfset passDateElementType = "date">
			</cfif>
			
			<cf_relayFormElementDisplay relayFormElementType="#passDateElementType#" currentValue="#now()#" fieldname="frmPassDate" label="phr_elearning_PassedDate" formName="#formName#">
			
			<cfset disablePassedButton=false>
			<cfif validCertificationCount eq getAvailableCertifications.recordCount>
				<cfset disablePassedButton=true>
			</cfif>
			
			<cf_relayFormElementDisplay relayFormElementType="HTML" currentValue="" fieldname="" label="" spanCols="true" valueAlign="center">
				<cf_relayFormElement relayFormElementType="submit" currentValue="phr_elearning_PassCertifications" fieldname="frmInsertCertification" label="" disabled="#disablePassedButton#">
			</cf_relayFormElementDisplay>
		</cf_relayFormDisplay>
	</cfif>
	
	</cf_translate>

<cfelse>	
	<cfoutput><p>This form is not available externally.</p></cfoutput>
</cfif>

