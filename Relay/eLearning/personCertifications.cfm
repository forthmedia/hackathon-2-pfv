<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			personCertifications.cfm
Author:				NJH
Date started:		2008/08/21
Description:		Display and edit person certifications. This file expects to have a wrapper around it with a cfform. (ie a screen)

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2008-11-11 			NYB 	P-SOP010
2009-02-23 			NYB 	bug 1859 - changed sortOrder from Title to FullName
2009-06-03 			NYB		Sop010 - removed column sorting from TFQO; limited to show to determined Flags (or All) via expandFor param (in calling - RelayTag - file)
2009-10-02			NJH		LID 2723
2010-02-18 			PPB 	B-01696 - this is now also run from a second screen (Update Person Certifications) which allows the results to be updated
2010/10/14			NAS		LID4315: E-Learning - Certification - Quizzes - First question randomizes
2011-02-11 			NYB 	LHID5479 changed flag call for expandAll - as function returns data - not a boolean value - if flag isn't linked to an EntityType
							ie, won't work properly if not configured properly
2011/02/25			NJH		LID 5753 - add the date to the getCertification webservice to prevent any sort of caching..
2011-03-04 			NYB 	LHID3717 & 4828; removed expandFor and all associated support code
							because P-FNL069 change made to relay/relayElearningWS.cfc on 2009/07/06
							has broken the showModules dropdown link and rendered this unsupportable
2011-09-16 			NYB 	P-SNY106
2012/11/13			YMA	CASE:431933 change quiz popup to auto refresh the certifications list on closing so the user automatically see the certification progress.
2013-01-31 			PPB 	Case 433337 set keyColumnList to use "editLink" not "Title" (which wasn't working) to activate anchor
2013-02-08 			PPB 	Case 433337 email issues
2013-02-21			RMB		Case 433844 - Removed CFSET columnHeadingList
2013-04-19			STCR	CASE 432328 respect Question OrderMethod
2013-06-26			NYB		CASE 434909 - further changes to sortOrder to use a variant of the defaultSortOrder technique
							Remove some CF_PARAMs which are not required and add some others.  This template now cf_included from relayTag
2013-07-11 			PPB 	Case 436072 reverted the use of stringify()
2013-07-19			PPB		Case 436072 moved resetQuiz function to elearning.js to share it
2013-11-05 			PPB 	Case 436072 reverted the use of stringify() back again because it caused an issue for evault; there is explicit conversion using deserializeJSON() in updateCertificationItems()
2013-11-12 			PPB 	Case 437867 pass parameter allowUpdate through so certification status & passed date fields are available on edit form
2014-02-13 			PPB/WAB Case 438878 added a <cfform> + adjusted a TFQO setting
2014-02-20 			NYB 	Case 438849 translated Status column
2014-03-20 			NYB 	Case 438890 removed the form called "#formname#", this is handled by passThroughVariablesStructure so it doesn't need to be added and it's existance is preventing the TFQO form from working
							NB:  THIS WILL REQUIRE RUNNING UPDATED VERSION OF:  StoredProcedures updatePhrasePointerCacheForEntityType.sql AND Trigger trigger_phrase_updatePhrasePointerCache.sql
2015-02-25			RPW		FIFTEEN-177 - The POL 3 pane screen titled [Certification Registrations] has icon edit links which do not work.
2015-12-20			PYW		P-TAT006 BRD 31. Add person certification expiry date

Possible enhancements:

 --->


<cfparam name="getData" default="#queryNew('')#">
<!--- 2008-11-11 NYB P-SOP010 - changed from Description to Title --->
<!--- START: 2013-06-26 NYB Merge changes - changed from cf_params to cfparams --->
<cfparam name="allowUpdate" default="false"/><!--- 2010-02-18 PPB B-01696 this is set for the Update Person Certifications screen not the Person Certifications screen --->
<cfparam name="frmCertificationID" default="0"/>						<!--- 2010-02-18 PPB B-01696 need certification id to get modules so we can auto-pass modules if we set the certification to passed --->
<cfparam name="frmPersonCertificationID" default="0"/>
<!--- END: 2013-06-26 NYB Merge changes --->
<cfparam name="frmModulesToPass" default="0">						<!--- 2010-02-18 PPB B-01696 --->
<cfparam name="frmModulesPassed" default="0">						<!--- 2010-02-18 PPB B-01696 --->
<cfparam name="frmEntityID" type="numeric" default="0" >
<cfparam name="frmEntityTypeID" type="numeric" default="0">
<cfparam name="frmCurrentEntityID" type="numeric" default="0">
<cfparam name="mode" type="string" default="view">
<cfparam name="goToUrl" type="string" default="/elearning/certificationRegistrations.cfm?">
<cfparam name="formName" type="string" default="TFQOForm"><!--- 2013-05-16 NYB Case 434909 - renamed TFQOForm --->
<!--- START: 2013-06-26 NYB changed from cf_params to cfparams --->
<cfparam name="certificationTypeID" type="numeric" default="0"/>
<cfparam name="validCertificationsOnly" type="boolean" default="false"/>
<cfparam name="returnPersonData" type="boolean" default="true"/>
<!--- 2015-12-22 PYW P-TAT006 BRD 31. Add person certification expiry date --->
<cfparam name="showTheseColumns" type="string" default="Title,Status,Registration_Date,Pass_Date,Expired_Date,personCertificationExpiryDate,Num_Modules_To_Pass,Modules_Passed,Show_Modules"/><!--- STCR 2012-05-31 P-REL109 CASE 428567 --->
<cfparam name="GroupBy" default=""/>

<!--- END: 2013-06-26 NYB --->
<cf_param name="numRowsPerPage" type="numeric" default="500"/>  <!--- NJH 2009/10/02 LID 2723 --->
<!--- 2010/10/14			NAS		LID4315: E-Learning - Certification - Quizzes - First question randomizes --->
<cf_param name="OrderMethod" type="string" default="Random" validvalues="Alpha,Random,SortOrder"/>
<!--- START: 2013-06-26 NYB Merge changes - changed from cf_params to cfparams --->
<cfparam NAME="stringencyMode" DEFAULT="#application.com.settings.getSetting('elearning.quizzes.stringencyMode')#"/><!--- NYB 2011-09-15 P-SNY106 --->
<cfparam NAME="showIncorrectQuestionsOnCompletion" DEFAULT="false"/>
<!--- END: 2013-06-26 NYB Merge changes --->
<!--- START: 2013-06-26 NYB Merge changes - added: --->
<cf_param name="organisationDefaultsortOrder" default="Fullname,Code,Description,Registration_Date,Status"/>
<cf_param name="personDefaultsortOrder" default="Fullname,Code,Description,Registration_Date,Status"/>
<!--- END: 2013-06-26 NYB Merge changes --->

<cfset request.relayFormDisplayStyle = "HTML-table">

<cfsetting requesttimeout="180"> <!--- 2013-05-02 NYB Case 434909 3 minutes  --->

<cfif isDefined("frmOrganisationID") and isDefined("getScreen.entityType") and getScreen.entityType eq "organisation">
	<cfset personID = 0>
	<cfset frmEntityTypeID=2>
	<cfset frmCurrentEntityID = frmOrganisationID>
<cfelseif structKeyExists(form,"frmPersonID")>
	<cfset personID = frmPersonID>
<cfelseif request.relayCurrentUser.isInternal and frmEntityTypeID eq 0>
	<cfset personID = frmCurrentEntityID>
<cfelseif not request.relayCurrentUser.isInternal>
	<cfset personID = request.relayCurrentUser.personID>
<!--- 2008-11-11 NYB P-SOP010 - added this elseif --->
<cfelseif request.relayCurrentUser.isInternal and frmEntityTypeID eq 2>
	<cfset personID = 0>
</cfif>

<cfif not request.relayCurrentUser.isInternal>
	<cfif personId eq application.com.relayElearning.getTrainingContact(organisationID=request.relayCurrentUser.organisationID,personId=personId)>
		<cfset personID = 0>
		<cfset frmEntityTypeID=2>
		<cfset frmCurrentEntityID = request.relayCurrentUser.organisationID>
	</cfif>
</cfif>

<!--- START: 2013-05-16 NYB Case 434909 - added --->
<cfif frmEntityTypeID eq 2>
	<!--- 2013-06-26 NYB Case 434909 Continued - changed to default organisationDefaultsortOrder: --->
	<cfparam name="sortorder" default="#organisationDefaultsortOrder#">
<cfelse>
	<!--- 2013-06-26 NYB Case 434909 Continued - changed to default personDefaultSortOrder: --->
	<cfparam name="sortorder" default="#personDefaultSortOrder#">
</cfif>
<!--- END: 2013-05-16 NYB Case 434909 --->

<!--- edit the person certification. This function will only get called on the portal, as the internal uses the xml editor to add --->
<cfif structKeyExists(form,"frmEditCertification")>

	<!--- if set certification to Passed make sure enough modules have been passed else auto pass them (as per the old certificationPass.cfm) --->

	<cfset allRulesPassed="true">

	<!--- 2013-05-16 NYB Case 434909 - added "request.relayCurrentUser.isInternal and": --->
	<cfif request.relayCurrentUser.isInternal and frmStatusId eq application.com.relayElearning.getPersonCertificationStatus(statusTextID='Passed').statusID>
		<cfif frmModulesToPass gt frmModulesPassed>

			<cfif not isDate(frmPassDate)>
				<CFSET frmPassDate = request.requestTime>
			</cfif>

	<!--- 2010-02-18 PPB B-01696 START: the following section of code was lifted from certificationPass.cfm (which should be deprecated, but if not will need updating with changes) --->
	<!--- 2013-02-11 PPB I now question whether we should be trying to update the certificate here based on modules or whether we should let the scheduled task do it   --->

			<cfset qryCertificationModules = application.com.relayElearning.getModules(certificationID=frmCertificationID,SortOrder="CertificationRuleID",incModulesUserDoesntHaveRightsTo="true")>

			<cfoutput query="qryCertificationModules" group="certificationRuleID">
	<!---
	<br/>2. CertificationRuleID: #CertificationRuleID#
	--->
				<cfif allRulesPassed>
	<!---
	<br/>2ba. CertificationRules: #qryCertificationModules.CurrentRow#
	--->
					<cfset ruleMet="false">
					<cfset modulesProcessed = 0>
					<cfoutput>
						<cfif not ruleMet>
	<!---
	<br/>3. Modules: #qryCertificationModules.CurrentRow#
	--->
							<cfset modulesProcessed=modulesProcessed+1>
							<cfset userModuleProgressID = application.com.relayElearning.insertPersonModuleProgress(moduleID=moduleID,personID=personID)>
							<cfset setModuleCompleteResultStruct = application.com.relayElearning.setModuleComplete(userModuleProgressID=userModuleProgressID,CompletedDate=frmPassDate,personID=personID,status="Passed",CertificationPassDate=frmPassDate)>
	<!---
	<br/>4. modulesProcessed=#modulesProcessed#; numModulesToPass=#numModulesToPass#
	--->
							<cfif modulesProcessed gte numModulesToPass>
	<!---
	<br/>6. ruleMet
	--->
								<cfset ruleMet="true">
							</cfif>
	<!---
	<br/>3b. Modules: #qryCertificationModules.CurrentRow#; modulesProcessed=#modulesProcessed#; userModuleProgressID=#userModuleProgressID#;
	--->
						</cfif>
					</cfoutput>
					<cfif not ruleMet>
						<cfset allRulesPassed="false">
	<!---
	<br/>5. rule NOT Met
	--->
					</cfif>
	<!---
	<br/>2b. CertificationRules: #qryCertificationModules.CurrentRow#
	--->
				</cfif>
			</cfoutput>
<!--- 2010-02-18 PPB B-01696 END: the following section of code was lifted from certificationpass.cfm (which should be deprecated, but if not will need updating with changes) --->

		</cfif>
	</cfif>

	<cfif allRulesPassed>
		<cfset application.com.relayElearning.updatePersonCertificationData(personCertificationID=frmPersonCertificationID,argumentsStruct=form)>
		<cfset mode="view">
	<cfelse>
		<cfset application.com.relayUI.setMessage(message="phr_elearning_certificationNotAutoPassedMessage",type="info")>
		<!--- <div class="messageText">
			phr_elearning_certificatationNotAutoPassedMessage
		</div> --->

	</cfif>
</cfif>


<!--- NYB 2011-03-07 removed as expandFor no longer supported: --->
<!--- START:  NYB 2009-06-03 Sop010 - added ---
<cfif not request.relayCurrentUser.isInternal>
	<cfif expandFor neq "All">
		<cfloop index = "flagTextID" list = "#expandFor#">
			<!--- 2011-02-11 NYB LHID5479 start. Changed to use isFlagSetForPerson and test compare against personid and boolean - because function returns data - not a boolean value - if flag isn't linked to an EntityType --->
			<cfset x = application.com.flag.isFlagSetForPerson(flagID=flagTextID,personid=request.relayCurrentUser.personID)>
			<cfif evaluate(x) eq request.relayCurrentUser.personID or x eq "true">
			<!--- 2011-02-11 NYB LHID5479 end --->
				<cfset expandFor = "All">
				<cfbreak>
			</cfif>
		</cfloop>
	</cfif>

	<cfif expandFor eq "All">
		<cfset personID=0>
		<cfset frmEntityTypeID=2>
		<cfset frmCurrentEntityID = request.relayCurrentUser.organisationID>
	</cfif>
</cfif>
!--- END:  NYB 2009-06-03 Sop010 --->

<cfscript>
	//NYB 2009-06-03 Sop010 - removed multiple lines from here - many moved to above
	argumentsCollection = structNew();
	argumentsCollection.sortOrder=sortOrder;
	argumentsCollection.returnPersonData=returnPersonData;
	argumentsCollection.entityTypeID=frmEntityTypeID;
	argumentsCollection.validCertificationsOnly=validCertificationsOnly;
	if (certificationTypeID neq 0) {
		argumentsCollection.certificationTypeID=certificationTypeID;
	}
	if (personID neq 0) {
		argumentsCollection.entityID=personID;
		//2008-11-11 NYB P-SOP010 - removed:
		//groupBy = "Code";
	} else {
		argumentsCollection.entityID=frmCurrentEntityID;
		argumentsCollection.returnPersonData=returnPersonData;
		if (not listFindNoCase(showTheseColumns,"FullName")) {
		showTheseColumns = listPrepend(showTheseColumns,"FullName");
		}
		//2008-11-11 NYB P-SOP010 - added:
		if (not listFindNoCase(showTheseColumns,"FullName")) {
		argumentsCollection.SortOrder = listPrepend(sortOrder,"FullName");
		}
		groupBy = "Fullname";
	}
	argumentsCollection.SortOrder = application.com.globalFunctions.removeDuplicates(type="list",data=argumentsCollection.SortOrder);
</cfscript>

<!--- 2010-02-18 PPB B-01696 the GetCertificationSummaryData call caused a problem on staging when running UPDATE Person Certifications even though it was ok on dev; the problem was because the sortorder 'FullName' was being applied instead of 'Description';
	  we could force the sortOrder description here but the code above to set it to 'FullName' was written for a reason so to get over the issue we have configured a parameter sortOrder=Description on the screen definition --->

<!--- START: 2013-05-16 NYB Case 434909 - added --->
<cfif structkeyexists(url,"mode") and structkeyexists(form,"mode")>
	<cfset url.mode = form.mode>
</cfif>
<!--- END: 2013-05-16 NYB Case 434909 - added --->

<!--- editing a person certification --->
<cfif mode eq "edit">

	<cfscript>
		qryPersonCertificationDetails = application.com.relayElearning.getPersonCertificationData(personCertificationID=frmPersonCertificationID);
	</cfscript>

	<cfset formName = "#formName#">

	<!--- 2013-05-16 NYB Case 434909 - added form: --->
	<cfform name="#formName#"  method="post" >
	<cf_relayFormDisplay>
		<!--- <cfform name="#formName#"  method="post" > --->
			<cf_relayFormElementDisplay relayFormElementType="hidden" currentValue="#frmPersonCertificationID#" fieldname="frmPersonCertificationID" label="">
			<cf_relayFormElementDisplay relayFormElementType="hidden" currentValue="#frmCertificationID#" fieldname="frmCertificationID" label="" formName="#formName#">			<!--- 2010-02-18 PPB B-01696  --->
			<cf_relayFormElementDisplay relayFormElementType="hidden" currentValue="#frmModulesToPass#" fieldname="frmModulesToPass" label="" formName="#formName#">				<!--- 2010-02-18 PPB B-01696  --->
			<cf_relayFormElementDisplay relayFormElementType="hidden" currentValue="#frmModulesPassed#" fieldname="frmModulesPassed" label="" formName="#formName#">				<!--- 2010-02-18 PPB B-01696  --->
			<cf_relayFormElementDisplay relayFormElementType="hidden" currentValue="#personID#" fieldname="frmPersonID" label="">
				<!--- START: 2013-05-16 NYB Case 434909 - added --->
				<cf_relayFormElementDisplay relayFormElementType="hidden" currentValue="#frmEntityTypeID#" fieldname="frmEntityTypeID" label="">
				<cf_relayFormElementDisplay relayFormElementType="hidden" currentValue="#frmCurrentEntityID#" fieldname="frmCurrentEntityID" label="">
				<cf_relayFormElementDisplay relayFormElementType="hidden" currentValue="view" fieldname="mode" label="">
				<!--- END: 2013-05-16 NYB Case 434909 - added --->
			<cf_relayFormElementDisplay relayFormElementType="hidden" currentValue="#allowUpdate#" fieldname="allowUpdate" label="" formName="#formName#">							<!--- 2013-11-12 PPB Case 437867 pass allowUpdate through --->

			<cfif request.relayCurrentUser.isInternal>

				<cfquery name="getCertificationStatuses" datasource="#application.siteDataSource#">
					SELECT statusId, description FROM trngPersonCertificationStatus WHERE statusTextId <> 'Cancelled'
				</cfquery>

				<!--- qryPersonCertificationDetails contains status and StatusTextId not statusId, so i convert instead of updating the cfc and the view --->
				<cfset personCertificationStatusId = application.com.relayElearning.getPersonCertificationStatus(statusTextID='#qryPersonCertificationDetails.StatusTextId#').statusID>

				<cf_relayFormElementDisplay relayFormElementType="#iif(allowUpdate,DE('select'),DE('hidden'))#" currentValue="#personCertificationStatusId#" fieldName="frmStatusId" size="1" query="#getCertificationStatuses#" display="description" value="statusId" label="phr_elearning_CertificationStatus" formName="#formName#" >	<!--- 2010-02-18 PPB B-01696  --->

				<cf_relayFormElementDisplay relayFormElementType="date" currentValue="#qryPersonCertificationDetails.registration_Date#" fieldname="frmRegistrationDate" label="phr_elearning_registrationDate" formName="#formName#">

				<cf_relayFormElementDisplay relayFormElementType="#iif(allowUpdate,DE('date'),DE('hidden'))#" currentValue="#qryPersonCertificationDetails.pass_Date#" fieldname="frmPassDate" label="phr_elearning_passDate" formName="#formName#">	<!--- 2010-02-18 PPB B-01696  --->

				<cf_relayFormElementDisplay relayFormElementType="date" currentValue="#qryPersonCertificationDetails.cancellation_Date#" fieldname="frmCancellationDate" label="phr_elearning_cancellationDate" formName="#formName#">
			</cfif>
			<cf_relayFormElementDisplay relayFormElementType="HTML" currentValue="" fieldname="" label="">
				<cf_relayFormElement relayFormElementType="submit" currentValue="phr_Cancel" fieldname="frmCancelEditRegistration" label="">
				<cf_relayFormElement relayFormElementType="submit" currentValue="phr_Save" fieldname="frmEditCertification" label="">
			</cf_relayFormElementDisplay>
		<!--- </cfform> --->
	</cf_relayFormDisplay>
	<!--- 2013-05-16 NYB Case 434909 - added form: --->
	</cfform>

<cfelseif mode eq "view">

		<cfset getData = application.com.relayElearning.GetCertificationSummaryData(argumentCollection=argumentsCollection)>

		<!--- START: 2013-05-16 NYB Case 434909 - remove title THEN replace with TranslatedTitle, removed translate --->
		<cfset qryColumnList = getData.ColumnList>
		<!--- 2014-02-20 NYB Case 438849 - added to the list: --->
		<cfset qryColumnList = application.com.globalFunctions.ListMinusList(qryColumnList,"Title,Description,TitleTranslated,DescTranslated,Status,StatusTextID,StatusTranslated")>

		<cfset editLinkPhrase = "#application.com.relayTranslations.translatePhrase('phr_Edit')#">

		<!--- 2015-02-25	RPW	FIFTEEN-177 - Changed editLink to Edit --->
		<cfquery name="getData" dbType="query">
			select <cfloop from="1" to="#ListLen(qryColumnList)#" index="i">#ListGetAt(qryColumnList, i)#,</cfloop>
			TitleTranslated as [title],
			DescTranslated as [description],
			<!--- 2014-02-20 NYB Case 438849 - added: --->
			StatusTranslated as [status],
			'<img src="/images/icons/bullet_arrow_down.png">' as Show_Modules, '<span class="fa fa-pencil" id="orgListPencil"></span>' AS Edit
			 from getData
		</cfquery>

		<cfif request.relayCurrentUser.isInternal or getData.recordcount lte 1000>
			<cfparam NAME="disableShowAllLink" DEFAULT="false"/>
		<cfelse>
			<cfparam NAME="disableShowAllLink" DEFAULT="true"/>
		</cfif>
		<!--- END: 2013-05-16 NYB Case 434909 --->

		<!--- START: 2014-03-20 NYB Case 438890 removed, this is handled by passThroughVariablesStructure below
	    <cfoutput><form name="#formname#"></cfoutput>		<!--- 2014-02-13 PPB/WAB Case 438878 added form  --->
		    <cf_relayFormElement relayFormElementType="hidden" currentValue="0" fieldname="frmCertificationID" label="">
		    <cf_relayFormElement relayFormElementType="hidden" currentValue="0" fieldname="frmPersonCertificationID" label="">
		    <cf_relayFormElement relayFormElementType="hidden" currentValue="0" fieldname="frmModulesToPass" label="">
		    <cf_relayFormElement relayFormElementType="hidden" currentValue="0" fieldname="frmModulesPassed" label="">
		    <cf_relayFormElement relayFormElementType="hidden" currentValue="view" fieldname="mode" label="">
		    <cf_relayFormElement relayFormElementType="hidden" currentValue="0" fieldname="allowUpdate" label="">				<!--- 2013-11-12 PPB Case 437867 pass allowUpdate through --->
	    </form>
		END: 2014-03-20 NYB Case 438890 added jsStringFormat code: --->

		<cfif getData.recordCount gt 0>

			<cf_includeJavascriptOnce template = "/javascript/tableManipulation.js">
			<cf_includeJavascriptOnce template = "/javascript/protoTypeSpinner.js">

			<cfset columnToShowAjaxAt = listFindNoCase(getData.columnList,"personCertificationID")>

			<!--- 2008-11-11 NYB P-SOP010 - throughout script, changed $ to $alternative - as was conflicting was jquery --->
			<script>
				<cfoutput>
					var columnToShowAjaxAt = #jsStringFormat(columnToShowAjaxAt)#;
					var currentPersonCertificationID = 0;
				</cfoutput>

				function $alternative(element) {
				  if (arguments.length > 1) {
				    for (var i = 0, elements = [], length = arguments.length; i < length; i++)
				      elements.push($alternative(arguments[i]));
				    return elements;
				  }
				  if (Object.isString(element))
				    element = document.getElementById(element);
				  return Element.extend(element);
				}

				function showModules (obj,id) {
					currentRowObj = getParentRow (obj)
					currentTableObj = getParentTable  (obj)
					totalColspan = 0
					for (i=0;i<currentRowObj.cells.length;i++) {
						totalColspan  += currentRowObj.cells[i].colSpan
					}

					var timeNow = new Date();
					currentPersonCertificationID = id;

					infoRow =  id + '_info'
					infoRowObj = $alternative(infoRow)

					if (!infoRowObj) {
						obj.spinner({position:'right'})
						page = '/webservices/callwebservice.cfc?wsdl&method=callWebService&webserviceName=relayElearningWS&methodName=getCertificationItems&returnFormat=json&_cf_nodebug=true'
						parameters = 'personCertificationID='+id
						<cfoutput>parameters += '&allowUpdate='+ #jsStringFormat(allowUpdate)#</cfoutput>
						// 2010/10/14			NAS		LID4315: E-Learning - Certification - Quizzes - First question randomizes
						<cfoutput>parameters +='&OrderMethod=' + '#jsStringFormat(OrderMethod)#';</cfoutput>
						// 2011-09-16 			NYB 		P-SNY106
						<cfoutput>parameters +='&stringencyMode=#jsStringFormat(stringencyMode)#&showIncorrectQuestionsOnCompletion=#jsStringFormat(showIncorrectQuestionsOnCompletion)#';</cfoutput>
						<cfif not request.relayCurrentUser.isInternal>
							<cfoutput>parameters += '&eid='+ #jsStringFormat(request.currentElement.id)#</cfoutput>
						</cfif>

						// changed to post to ensure proper refresh - NJH 2013/05/23 - changed back to get as we now refresh automatically.. no updates done, so it's a get
						var myAjax = new Ajax.Request(
							page,
							{
								method: 'get',
								parameters: parameters,
								evalJSON: 'force',
								debug : false,
								onComplete: function (requestObject,JSON) {
									json = requestObject.responseJSON
									rowStructure = new Array (2)
									rowStructure.row = new Array (1)
									rowStructure.cells = new Array (1)
									rowStructure.cells[0] = new Array (1)
									rowStructure.cells[0].content  = json.CONTENT
									rowStructure.cells[0].colspan  = totalColspan //-rowStructure.cells[0].colspan
									rowStructure.cells[0].align  = 'right'
									rowStructure.cells[0].classname  = 'noDataTitle'; //Rows will be pulled to left while data-title attribute is missed in responsiveTable style

									regExp = new RegExp ('_info')
									obj.removeSpinner()
									deleteRowsByRegularExpression (currentTableObj,regExp)

									addRowToTable (currentTableObj,rowStructure,currentRowObj.rowIndex + 1,infoRow)

									// 2014-06-17	YMA	Re-initialize responsive table on AJAX content
									jQuery(currentTableObj).trigger('create');
									}
							});
					}
					else
					{
						regExp = new RegExp (infoRow)
						deleteRowsByRegularExpression (currentTableObj,regExp)
					}

					return
				}


				<cfif request.relayCurrentUser.isInternal>

				<!--- CASE 435326 - NJH 2013/05/23  - add ability to reset a quiz attempt - only show if an internal user --->
				<!--- 2013-07-17 PPB Case 436072 moved function resetQuiz() to elearning.js  --->

				function saveModules (id) {
					//  2010-02-18 PPB B-01696 new function

					$("savedMessage").innerHTML = ""; 		//clear any previous message

					page = '/webservices/relayElearningWS.cfc?wsdl&method=updateCertificationItems&returnFormat=JSON&_cf_nodebug=true'
					params = '&personCertificationID='+id

					//NB you can't loop the FORM elements because the dropdowns are created on the fly so don't exist in the FORM scope
					var selectElems = document.getElementsByTagName('select');
					var arrResults = new Array();

					for(i=0; i<selectElems.length; i++)
					{

						if(selectElems[i].id.substr(0,12) == 'moduleResult')			//all select boxes on the page are currently moduleResult but defend just in case another is added
						{
							arrResults[i] = [selectElems[i].id.substr(13,9), selectElems[i].value];	// selectElems[i][selectElems[i].selectedIndex].value   selectElems[selectElems[i].selectedIndex].value
						}
					}

					//2013-07-11 PPB Case 436072 reverted the use of stringify() because the string would need explicit conversion to an array at the other end in updateCertificationItems()
					//2013-11-05 PPB Case 436072 reverted the use of stringify() back again because it caused an issue for evault; there is explicit conversion using deserializeJSON() in updateCertificationItems()
					params = params + '&arrResults=' +  JSON.stringify(arrResults);// arrResults.toJSON();
					//params = params + '&arrResults=' +  arrResults.toJSON();

					var myAjax = new Ajax.Request(
						page,
						{
							method: 'post',
							parameters: params,
							evalJSON: 'force',
							debug : false,
							onComplete: function (requestObject,JSON) {
											json = requestObject.responseJSON
													if (json.ISOK) {
														$('savedMessage').innerHTML = '<div class="successblock">phr_elearning_ChangesSaved</div>'
														location.reload(false)
														/*
														the code below would update just the num_modules_passed cell for the certification that has just had its modules updated, as a module may be relevant to more than one certification, we would need to update num_modules_passed for those rows too so I have reverted to reload the page to refresh the display for all

														if (json.CELLUPDATES) {
															for (cell in json.CELLUPDATES) {
																updateTableCell (currentTableObj,currentRowObj,cell,json.CELLUPDATES[cell])
															}
														}
														*/

													}
										}
						});

					return
				}
				</cfif>

				function addUserModule(obj,personCertificationID,moduleID,personID) {
					page = '/webservices/relayElearningWS.cfc?wsdl&method=insertUserModuleProgress&returnFormat=JSON&_cf_nodebug=true'
					parameters = 'moduleID='+moduleID
					parameters +='&personID='+personID

					var myAjax = new Ajax.Request(
						page,
						{
								method: 'post',
								parameters: parameters,
								evalJSON: 'force',
								debug : false,
								onComplete: function (requestObject,JSON) {
									json = requestObject.responseJSON
									alert(json.CONTENT)
								}
						}
					)

					refreshModuleDiv(personCertificationID);

				}

				<!--- NJH 2012/11/22 Case 431933 - refresh the module div as people take modules and quizzes.. also update the certification row that the user is on --->
				function refreshModuleDiv(personCertificationID) {
					// if function being called from pop-up containing quiz or courseware
					if (personCertificationID== undefined) {
						personCertificationID = currentPersonCertificationID;

						page = '/webservices/relayElearningWS.cfc?wsdl&method=getPersonCertificationSummary&returnFormat=JSON&_cf_nodebug=true'
						parameters = 'personCertificationID='+personCertificationID

						var myAjax = new Ajax.Request(
							page,
							{
								method: 'get',
								parameters: parameters ,
								evalJSON: 'force',
								debug : false,
								onComplete: function (requestObject,JSON) {
									json = requestObject.responseJSON
									for (i=0;i<currentRowObj.cells.length;i++) {
										className = currentRowObj.cells[i].className.toUpperCase();

										if ((json[className]) && (className == 'STATUS' || className == 'MODULES_PASSED' || className == 'PASS_DATE')) {
											currentRowObj.cells[i].innerHTML = json[className]
										}
									}
								}
							}
						)
					}

					page = '/webservices/relayElearningWS.cfc?wsdl&method=getCertificationItems&returnFormat=JSON&_cf_nodebug=true'
					parameters = 'action=update&OrderMethod=<cfoutput>#jsStringFormat(OrderMethod)#</cfoutput>&personCertificationID='+personCertificationID
					<!--- 2013-04-19 STCR CASE 432328 respect Question OrderMethod --->
					<cfif not request.relayCurrentUser.isInternal>
						<cfoutput>page = page + '&eid='+ #jsStringFormat(request.currentElement.id)#</cfoutput>
					</cfif>
					div = 'personCertificationDataDiv'
					var myAjax = new Ajax.Updater(
						div,
						page,
						{
							method: 'get',
							parameters: parameters ,
							evalJSON: 'force',
							debug : false
						});
				}


				jQuery(document).ready(function(){
					jQuery(".takeQuizRL").fancybox({
						   helpers: {
					         overlay: {
					            locked: true
					         }
					      },
						autoSize: false,
						width: '90%',
						height: '90%',
						type: 'iframe'
					});
				});
			</script>

			<cfif request.relayCurrentUser.isInternal>
				<!--- START 2013-01-31 PPB Case 433337 "Title" -> "editLink" (note: to use the title in this ver you would need to set it to "TitleTranslated") --->
				<!--- 2015-02-25	RPW	FIFTEEN-177 - Changed editLink to Edit --->
				<cfset keyColumnList = "Edit,Show_Modules">
				<!--- RMB - 2013-02-21 - 433844 - Removed CFSET columnHeadingList - cfset columnHeadingList = columnHeadingList & ", " --->
				<!--- 2015-02-25	RPW	FIFTEEN-177 - Changed editLink to Edit --->
				<cfset showTheseColumns = showTheseColumns & ",Edit">
				<!--- END 2013-01-31 PPB Case 433337 --->
			<cfelse>
				 <cfset keyColumnList = " ,Show_Modules">
			</cfif>


			<cfoutput>
				<!--- START: 2013-05-16 NYB Case 434909 - made capitals, added action --->
				<script>
					function editPersonCertification(certificationID,personCertificationID,modulesToPass,modulesPassed) {
 						document.#jsStringFormat(formName)#.FRMCERTIFICATIONID.value = certificationID;					//  2010-02-18 PPB B-01696
						document.#jsStringFormat(formName)#.FRMPERSONCERTIFICATIONID.value = personCertificationID;
						document.#jsStringFormat(formName)#.FRMMODULESTOPASS.value = modulesToPass;						//  2010-02-18 PPB B-01696
						document.#jsStringFormat(formName)#.FRMMODULESPASSED.value = modulesPassed;						//  2010-02-18 PPB B-01696
						document.#jsStringFormat(formName)#.MODE.value = 'edit';
						document.#jsStringFormat(formName)#.ALLOWUPDATE.value = #allowUpdate#;							//  2013-11-12 PPB Case 437867 pass allowUpdate through
						document.#jsStringFormat(formName)#.action = "/elearning/personCertifications.cfm";
						document.#jsStringFormat(formName)#.submit();
					}
				</script>
				<!--- END: 2013-05-16 NYB Case 434909 --->
			</cfoutput>

			<cf_includeJavascriptOnce template="/elearning/js/elearning.js">

			<!--- 2010-02-18 PPB B-01696 - added Num_Modules_To_Pass and Modules_Passed parameters to editPersonCertification--->
			<!--- 2013-05-16 NYB Case 434909 - added: --->
			<cfset passThroughVariablesStructure = {mode="#mode#",frmCurrentEntityID=#frmCurrentEntityID#,frmentityTypeid=#frmentityTypeid#,sortorder="#sortOrder#",frmCertificationID=frmCertificationID,frmPersonCertificationID=frmPersonCertificationID,frmModulesToPass=frmModulesToPass,frmModulesPassed=frmModulesPassed,allowUpdate=allowUpdate}>			<!--- 2013-11-12 PPB Case 437867 pass allowUpdate through --->

			<CF_tableFromQueryObject
				formName="#formName#"  <!--- 2013-05-16 NYB Case 434909 added  --->
				queryObject="#getData#"
				queryName="getData"
				groupByColumns="#groupBy#"
			 	hideTheseColumns="#groupBy#"
				showTheseColumns="#showTheseColumns#"
				columnTranslation="true"
				ColumnTranslationPrefix="phr_certification_"
				dateformat="Registration_Date,Pass_Date,Expired_Date,Expiry_Date,personCertificationExpiryDate"
				dateFormatMask="long,long,long,long"<!--- 2016-06-02 ESZ Case 449146 added  --->
				numberFormat="Credits_Obtained,Num_Modules_To_Pass,Modules_Passed"
				useInclude="false"
				keyColumnList="#keyColumnList#"
				keyColumnURLList="##,##"					<!--- 2014-02-13 PPB/WAB Case 438878 changed spaces to hashes(escaped)   --->
				keyColumnKeyList="personCertificationID,personCertificationID"
				keyColumnOnClickList="javascript:editPersonCertification(##certificationID##*comma##personCertificationID##*comma##Num_Modules_To_Pass##*comma##Modules_Passed##);return false;,javascript:showModules(this*comma##personCertificationID##);return false"
				keyColumnOpenInWindowList="no,no"
				rowIdentityColumnName="personCertificationID"
				HidePageControls="no"  <!--- 2013-05-16 NYB Case 434909 made no  --->
				sortOrder="#sortOrder#"
				allowColumnSorting="no"
				numRowsPerPage = "#numRowsPerPage#"
				disableShowAllLink="#disableShowAllLink#"  <!--- 2013-05-16 NYB Case 434909 added  --->
				passThroughVariablesStructure="#passThroughVariablesStructure#"  <!--- 2013-05-16 NYB Case 434909 added  --->
			>
			<div id="dummy"></div>
		<cfelse>
			<div align="center">phr_elearning_noCertificationsHaveBeenRegistered</div>
		</cfif>


</cfif>