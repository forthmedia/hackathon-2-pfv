<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		questionsInclude.cfm	
Author:			STCR  
Date started:	2012-05-01
	
Description:	Used to include Answer Pool from the Question editor. Adapted from certificationsInclude.cfm

Amendment History:

Date (YYYY-MM-DD)	Initials 	What was changed
2013-04-25			STCR		CASE 434613 - Prevent passing QuestionID=0 to Answer Editor

Possible enhancements:


 --->


<cfif not Isdefined("AddNew")>
	<!--- <cfif structKeyExists(url,"questionID")>
		<cfset variables.questionID = url.questionID>
	<cfelseif structKeyExists(form,"questionID")>
		<cfset variables.questionID = form.questionID>
	<cfelse>
		Variable questionID does not exist.
		<CF_ABORT>
	</cfif> --->
	<!--- 2013-04-25 STCR CASE 434613 The above code was no longer behaving as expected - presumably a subtle change to RelayXMLEditor --->
	<cfparam name="url.QuestionID" type="numeric" default="0" />
	<cfparam name="form.QuestionID" type="numeric" default="#url.QuestionID#" />
	<cfparam name="variables.QuestionID" type="numeric" default="#form.QuestionID#" />
	
	<cfif variables.QuestionID neq 0>
		<cfoutput>
		<iframe src="/elearning/answers.cfm?questionID=#htmleditformat(variables.questionID)#" width="100%" height="400px" scrolling="auto" frameborder="0"></iframe>
		</cfoutput>
	</cfif>
</cfif>