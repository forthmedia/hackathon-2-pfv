<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			trainingPaths.cfm
Author:				YMA
Date started:		17/04/2014

Description:		Suggest a trainingPath path for a user to follow.

Date (DD-MMM-YYYY)	Initials 	What was changed
Amendment History:
2016/01/06		NJH		JIRA BF-171  - show inactive training paths

Possible enhancements:


 --->
<cf_title>Training Paths</cf_title>
<cfparam name="getData" type="string" default="foo">
<cfparam name="sortOrder" default="trainingPath, CourseTitle">
<cfparam name="numRowsPerPage" type="numeric" default="100">
<cfparam name="startRow" type="numeric" default="1">
<cfparam name="variables.showTheseColumns" default="trainingPath,CourseTitle"/>
<cfparam name="url.active" type="boolean" default="true">

<cfsavecontent variable="xmlSource">
	<cfoutput>
	<editors>
		<editor id="232" name="thisEditor" entity="trngTrainingPath" title="phr_elearning_trainingPath_Editor">
			<field name="ID" label="phr_trainingPath_ID" description="This records unique ID." control="html"></field>
			<field name="trainingPathTextID" label="phr_trainingPath_textID" description=""></field>
			<field name="" CONTROL="TRANSLATION" label="phr_trainingPath_title" Parameters="phraseTextID=Title" required="true"></field>
			<field name="" control="includeTemplate" template="/eLearning/trainingPathCourses.cfm" label="phr_trainingPath_courses"></field>
			<field name="" label="phr_trainingPath_restrictToCountries" description="" control="countryScope" width="150"></field>
			<field name="" label="phr_trainingPath_restrictToUserGroups" description="" control="recordRights" recordRightsCaption1="Potential Members" recordRightsCaption2="Current Members" suppressUserGroups="false" userGroupTypes="External,Other"></field>
			<field name="ignoreAssignmentRules" label="phr_trainingPath_ignoreAssignmentRules" description="" control="select" displayAs="radio" validvalues="select 0 as value, 'Display to individually assigned people in the above visibility groups' as display union select 1 as value, 'Display to everyone in the above visibility groups' as display"/>
			<field name="active" label="phr_trainingPath_active" control="yorn"/>
			<group label="System Fields" name="systemFields">
				<field name="sysCreated"></field>
				<field name="sysLastUpdated"></field>
			</group>
		</editor>
	</editors>
	</cfoutput>
</cfsavecontent>

<cfif not structKeyExists(URL,"editor")>
	<cfset getData = application.com.trainingPath.getTrainingPathList(sortOrder=sortOrder,returnCoursesAsCommaList=true,active=url.active)>
</cfif>

<cf_listAndEditor
	xmlSource="#xmlSource#"
	cfmlCallerName="TrainingPaths"
	keyColumnList="TrainingPath"
	keyColumnKeyList=" "
	keyColumnURLList=" "
	keyColumnOnClickList="javascript:openNewTab('##jsStringFormat(htmlEditFormat(TrainingPath))##'*comma'##jsStringFormat(htmlEditFormat(TrainingPath))##'*comma'/eLearning/TrainingPaths.cfm?editor=yes&hideBackButton=true&ID=##jsStringFormat(TrainingPathID)##'*comma{reuseTab:true*commaiconClass:'elearning'});return false;"
	showTheseColumns="#variables.showTheseColumns#"
	FilterSelectFieldList="trainingPath"
	useInclude="false"
	sortOrder="#sortOrder#"
	hideBackButton="#iif(structKeyExists(URL,'hideBackButton') and url.hideBackButton is true,true,false)#"
	queryData="#getData#"
	numRowsPerPage="#numRowsPerPage#"
	startRow="#startRow#"
	columnTranslation="true"
	booleanFormat="ignoreAssignmentRules"
	columnTranslationPrefix="phr_report_trainingPath_"
>