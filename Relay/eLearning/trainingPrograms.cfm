<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:		trainingPrograms.cfm
Author:			AJC - Englex Limited
Date started:	2012/04/17

Description:	P-REL109 Training Manager Enhancement Project Phase 1 - Training Program management

Amendment History:

Date (YYYY/MM/DD)	Initials 	What was changed
2012-07-06 WAB Removed redundant cf_ translatequerycolumn, column now arrives pre-translated
2012-07-12			PJP			Case#429184  Make sortorder work on columns
Possible enhancements:


 --->

<cfparam name="getData" type="string" default="foo">
<cfparam name="sortOrder" default="trainingProgramID ASC">
<cfparam name="numRowsPerPage" type="numeric" default="100">
<cfparam name="startRow" type="numeric" default="1">

<!--- save the content for the xml to define the editor --->

<cfscript>
 	// this changes the active status of a certification
 	if(structKeyExists(URL, "frmRowIdentity")){
		for(x = 1; x lte listLen(URL.frmRowIdentity); x = x + 1){
			application.com.relayElearning.deleteTrainingProgram(trainingProgramID=listGetAt(URL.frmRowIdentity,x));
		}
	}
 </cfscript>

<cfsavecontent variable="xmlSource">
	<cfoutput>
	<editors>
		<editor id="469" name="thisEditor" entity="trngTrainingProgram" title="phr_trngTrainingProgram_Title">
			<field name="TrainingProgramID" label="phr_elearning_TrainingProgramID" description="Unique ID." control="html"></field>
			<field name="TrainingProgramTextID" label="phr_elearning_TrainingProgramtextID" description="Unique Text ID" required="true"></field>
			<field name="" CONTROL="TRANSLATION" label="phr_elearning_TrainingProgram" Parameters="phraseTextID=title" required="true"></field>
		</editor>
	</editors>
	</cfoutput>
</cfsavecontent>

<!--- 2012-07-12			PJP			Case#429184  Make sortorder work on columns --->
<cfif not structKeyExists(url,"editor")>
	<cfset getData = application.com.relayElearning.getTrainingProgram(sortOrder=sortOrder)>
</cfif>

<cfset functionType="TrainingProgram">
<cfinclude template="/elearning/elearningFunctions.cfm">

<cf_listAndEditor
	xmlSource="#xmlSource#"
	cfmlCallerName="trainingPrograms"
	keyColumnList="trainingProgram"
	keyColumnKeyList="trainingProgramID"
	showTheseColumns="trainingProgram,trainingProgramTextID"
	FilterSelectFieldList=""
	FilterSelectFieldList2=""
	useInclude="false"
	sortOrder="#sortOrder#"
	queryData="#getData#"
	numRowsPerPage="#numRowsPerPage#"
	startRow="#startRow#"
	columnTranslation="true"
	columnTranslationPrefix="phr_elearning_"
	rowIdentityColumnName="trainingProgramID"
	functionListQuery="#comTableFunction.qFunctionList#"
>