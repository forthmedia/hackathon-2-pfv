<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		courseCertification.cfm	
Author:			SWJ  
Date started:	03-07-2008
	
Description:	Interface for managing person's registrations for a certification		

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->

<cf_title>Course Certifications</cf_title>
<!---


 
<cf_head>
	<cf_title>Course Certifications</cf_title>
</cf_head>

 --->

<cfparam name="getData" type="string" default="foo">
<cfparam name="sortOrder" default="Title_Of_Course">

<!--- save the content for the xml to define the editor --->
<!--- NJH 2009/07/22 P-SNY047 course translation --->
<cfsavecontent variable="xmlSource">
	<cfoutput>
	<editors>                                  
		<editor id="232" name="thisEditor" entity="trngCourseCertification" title="Course Certification Editor">
			<field name="trngCourseCertificationID" label="phr_elearning_CourseCertificationID"	description="This records unique ID." control="html"></field>
			<field name="courseID" label="phr_elearning_Course" description="The course ID of the course this module belongs to." 
					control="select" query="select courseID as value, 'phr_title_TrngCourse_' + CONVERT(VarChar, courseId) as display from trngCourse"></field>
			<field name="personID" label="phr_elearning_Student" description="Student Registering." control="select" 
					query="select personid as value, fullname as display from vPeople where personid = #request.relayCurrentUser.personid#"></field>
			<field name="registeredDate" label="phr_elearning_registeredDate" control="date" description=""></field>
			<field name="completedDate" label="phr_elearning_completedDate" control="date" description=""></field>
			<field name="targetCompletionDate" label="phr_elearning_targetCompletionDate" control="date" description=""></field>
			<field name="dateCertificationExpires" label="phr_elearning_dateCertificationExpires" control="date" description=""></field>
		</editor>
	</editors>
	</cfoutput>
</cfsavecontent>


<cfif not isDefined("URL.editor")>
	<cfquery name="getData" datasource="#application.siteDataSource#">
		select * from vTrngCourseCertification
	</cfquery>
</cfif>

<cf_listAndEditor 
	xmlSource="#xmlSource#" 
	cfmlCallerName="courseCertification"
	keyColumnList="Title_Of_Course"
	keyColumnKeyList="trngCourseCertificationID"
	
	FilterSelectFieldList=""
	FilterSelectFieldList2=""
	
	showTheseColumns="Title_of_Course,FullName,registeredDate,completedDate,targetCompletionDate,dateCertificationExpires"
				
	useInclude="false"
	sortOrder="#sortOrder#"
	queryData="#getData#"
>