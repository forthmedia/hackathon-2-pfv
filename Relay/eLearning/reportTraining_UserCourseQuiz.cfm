<!--- �Relayware. All Rights Reserved 2014 --->
<!--- Amendment History
2008-06-12 			NYF 		for CR-LEN548, added a date filter
2008-07-08	AJC Issue 673: E-Learning Report Download Error - Used new js method for opentoexcel
2009/05/27 SSS LH 2279 moved the excel link to the profiletophead
2009/06/03  SSS LH 2279 changed the query to only get the columns that we need
2009/09/28 SSS LH 1960 Query needed tweaking as a name of a column in the view has changed
2010/07/13 AJC P-SNY103 Change timetaken to ##m ##s format as per quiz top 10
--->

<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">
<cfparam name="openAsExcel" type="boolean" default="false">

<cf_title>Training Report - By Course Summary</cf_title>

<cfparam name="sortOrder" default="Trainee,Country">
<cfparam name="numRowsPerPage" default="50">
<cfparam name="startRow" default="1">

<cfparam name="checkBoxFilterName" type="string" default="">
<cfparam name="checkBoxFilterLabel" type="string" default="">

<!--- NYF 2008-06-12 CR-LEN548 - added a date filter -> --->
<!--- finds the most recent Completion Date as filter defaults to the month prior to this  --->
<!--- NJH 2008/07/25 T-10 Bug Fix Issue 846. If date is null, get the current date --->
<cfquery name="getMaxCompletionDate" datasource="#application.siteDataSource#">
	SELECT  isNull(max(completion_date),GetDate()) as MaxDate FROM vTrngUserModuleProgress
	<!---	2012-07-25 PPB P-SMA001 commented out 
	Where countryid in (#request.relaycurrentuser.countrylist#)
	 --->
	where 1=1 #application.com.rights.getRightsFilterWhereClause(entityType="trngUserModuleProgress",alias="vTrngUserModuleProgress").whereClause#		<!--- 2012-07-25 PPB P-SMA001 --->
</cfquery>

<cfparam name="useFullRange" default="false">
<cfparam name="usePartialRange" default="true">
<cfset PartialRangeEndDate = getMaxCompletionDate.MaxDate>
<cfset PartialRangeStartDate = dateadd("m",-1,PartialRangeEndDate)>
<cfset useDateRange ="true">
<cfset tablename ="vTrngUserModuleProgress">
<cfset dateField="completion_date">
<cfset dateRangeFieldName="completion_date">

<cfinclude template="/templates/DateQueryWhereClause.cfm"> 

<CFScript>
	myList = StructKeyList(queryWhereClause);
	if (myList contains "A") {
		StructDelete(queryWhereClause, "A");
   	} 	
</CFScript>
<!--- <- NYF  --->
<!--- START LH 2279 SSS 2009/06/03 changed the query so it only returns what we need --->
<!--- START LH 1960 SSS 2009/09/28 Query needed tweaking as a name of a column in the view has changed --->

<!--- 2011-02-18 NYF replaced: -> ---
<cfquery name="getUserModuleProgress" datasource="#application.siteDataSource#">
	SELECT  Trainee, organisation_name, Country,solution_Area, series_title, 
			course_Code, course_title, Module_Title, Attempts, Time_Spent, quiz_Passed, 
			completion_date, Score, Module_Status
	FROM    vTrngUserModuleProgress
	Where 1=1
	and countryid in (#request.relaycurrentuser.countrylist#)
	<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
		order by <cf_queryObjectName value="#sortOrder#"> 
</cfquery>
!--- <- NYF with: -> --->
<cfsavecontent variable = "includeWhereClause_sql">
<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
</cfsavecontent>
<cfset getUserModuleProgress = application.com.relayElearning.getUserCourseModuleProgress(CountryIDList=request.relaycurrentuser.countrylist,AdditionalWhereClause=includeWhereClause_sql,SortOrder=SortOrder)>
<!--- <- NYF with: -> --->

<!--- START LH 1960 SSS 2009/09/28 Query needed tweaking as a name of a column in the view has changed --->
<!--- END LH 2279 SSS 2009/06/03 changed the query so it only returns what we need --->
<table width="100%" cellpadding="3">
	<tr>
		<td><strong>Trainee Report - By Course, Quiz</strong></td>
		<td align="right">&nbsp;</td>
	</tr>
	<tr><td colspan="2">Details of all courses</td></tr>
</table>

<!--- NYF 2008-06-12 CR-LEN548 - added queryWhereClauseStructure & passThroughVariablesStructure & queryWhereClauseLabel in the attribute list --->
<CF_tableFromQueryObject 
	queryObject="#getUserModuleProgress#"
	queryName="getUserModuleProgress"
	sortOrder = "#sortOrder#"
	numRowsPerPage="#numRowsPerPage#"
	startRow="#startRow#"
	openAsExcel="#openAsExcel#"
 	hideTheseColumns=""
	showTheseColumns="Trainee,organisation_name,Country,solution_Area,series_title,course_Code,course_title,Module_Title,Attempts,Time_Spent,quiz_Passed,completion_date,Score,Module_Status"
	columnTranslation="true"
	dateformat="completion_date"
	FilterSelectFieldList="Trainee,organisation_name,Country,solution_Area,series_title,Module_Status,quiz_Passed,course_Code,course_title"
	FilterSelectFieldList2="Trainee,organisation_name,Country,solution_Area,series_title,Module_Status,quiz_Passed,course_Code,course_title"
	GroupByColumns=""
	radioFilterLabel=""
	radioFilterDefault=""
	radioFilterName=""
	radioFilterValues=""
	checkBoxFilterName=""
	checkBoxFilterLabel=""
	allowColumnSorting="yes"
	showCellColumnHeadings="no"
	queryWhereClauseStructure="#queryWhereClause#"
	selectbyday="false"
	passThroughVariablesStructure="#passThruVars#"			
	queryWhereClauseLabel="Phr_sys_report_CompletionDateFilter" 	
>