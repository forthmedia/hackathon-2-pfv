<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		scoDiagnostics.cfm	
Author:			NJH  
Date started:	11-09-2012
	
Description:	A SCO diagnostics page to help debug SCOs		

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->
<cfparam name="moduleID" default=0>

<cfquery name="getCoursewareGroup" datasource="#application.siteDataSource#">
	select fileTypeGroupID from fileTypeGroup where heading='ELearning_CourseWare'
</cfquery>

<!--- <cfset coursewareFiles = application.com.filemanager.getFilesAPersonHasRightsTo(filetypegroupid=getCoursewareGroup.fileTypeGroupID,personID=request.relayCurrentUser.personID)>

<cfparam name="frmCoursewareID" default=0>
<cfparam name="frmLaunchFile" default=""> --->

<cfset request.displayStyle="HTML-table">


<cfform name="viewCoursware">
	<cf_relayFormDisplay>
		<!--- <cf_relayFormElementDisplay relayFormElementType="select" fieldname="frmCoursewareID" label="Courseware" currentValue=#frmCoursewareID# query="#coursewareFiles#" value="fileid" display="name">
		<cf_relayFormElementDisplay relayFormElementType="text" fieldname="frmLaunchFile" label="Launch File" currentValue=#frmLaunchFile# required="true">
		<cf_relayFormElementDisplay relayFormElementType="submit" fieldname="frmSubmit" label="" currentValue="Run Diagnostics">
		
		<cfif structKeyExists(form,"frmCoursewareID") and form.frmLaunchFile neq "">
			<cfset fileDetails = application.com.filemanager.getFilesAPersonHasRightsTo(fileid=frmCoursewareID,personID=request.relayCurrentUser.personID)>
			
			<cfset fileUrl = application.com.security.encryptURL(url="/fileManagement/fileDisplay.cfm?filePath=/content/courseware/#frmCoursewareID#/#frmLaunchFile#&fileName=#frmLaunchFile#")> --->
			<cfset courseLauncherUrl = application.com.relayElearning.getCourseLauncherUrl(moduleID=moduleID)>
			<cfset session.elearning.scorm.currentModuleID = moduleID>
			<cfset session.elearning.scorm.currentUserModuleProgressID = 1>
			<cfset debug = true>
			<cfinclude template="/elearning/relayScorm.cfm">
			<tr>
				<td colspan="2">		
					<cfoutput><iframe src="#courseLauncherUrl#" name="scoDiagnostics" width="650px;" height="800px;"></iframe></cfoutput><div id="diagnostics" style="float:right;width:300px;text-align:left;height:700px;overflow: auto;"></div>
				</td>
			</tr>
		<!--- </cfif> --->
	</cf_relayFormDisplay>
</cfform>
