<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		specificationsInclude.cfm	
Author:			NJH  
Date started:	09-09-2008
	
Description:	Used to include specification rules from the specification editor		

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:


 --->

<cfif not Isdefined("AddNew")>

	<!--- <cfif structKeyExists(url,"specialisationID")>
		<cfset variables.specialisationID = url.specialisationID>
	<cfelseif structKeyExists(form,"specialisationID")>
		<cfset variables.specialisationID = form.specialisationID>
	<cfelse>
		Variable specialisationID does not exist.
		<CF_ABORT>
	</cfif> --->
	
	<cfoutput>
		<iframe src="/elearning/specialisationRules.cfm?specialisationID=#htmleditformat(form.specialisationID)#" width="100%" height="400px" scrolling="auto" frameborder=0></iframe>
	</cfoutput>
</cfif>