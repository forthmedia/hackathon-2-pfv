<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:		reportTraining_CertificationSummary.cfm	
Author:			NJH  
Date started:	20-08-2008
	
Description:	Displays training Certifications for individuals with a range of filter criteria to provide:		
				a) Certifications achieved per Partner
				b) Certifications underway per Partner
				c) Certifications achieved per Country/Region
				d) Certifications underway per Country/Region
				e) Certifications that have Expired
				f) Certifications about to Expire
				g) Certifications that have expired


Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2008-11-11 	NYB	P-SOP010 - renamed Fullname to Name, replaced [Certification] Code & Descriptiion with [Certification] Title
2009-05-20	NYB	Sophos - Sophos Support LID 2232/Bugzilla bug 2155 - was throwing an error when trying to open in Excel - or was timing out
2009-05-26	NYB	Sophos - Sophos Support 2266 - report does not behave as expected.  Client expects selecting a team to return all client
						data for CAMs that are in that team, however original report returns only data for users in a team - thereby
						making it useless for viewing external users data - as teams can only contain internal users
2012-08-09 	PPB P-SMA001 fix 

Possible enhancements:


 --->

<!--- NYB 2009-05-20	NYB	Sophos - LID 2232 - added to stop timeout error --->
<cfsetting requesttimeout="1200">

<cfparam name="useFullRange" type="boolean" default="true">
<cfparam name="openAsExcel" type="boolean" default="false">
<cfparam name="sortOrder" default="Country,Organisation_name,Fullname">
<cfparam name="numRowsPerPage" default="50">
<cfparam name="startRow" default="1">
<cfparam name="useMyTeams" type="boolean" default="true">

<!--- START:  NYB 2009-05-26 Sophos Support 2266 - added (replaced cfscript contents from below *1): --->
<cfparam name="AccountManagerFlagTextID" type="string" default="AccManager">
<cfparam name="restrictToUserCountryList" type="boolean" default="true">
<cfif (not ((structKeyExists(form,"frmTeam") and form.frmTeam neq "") or (structKeyExists(url,"frmTeam") and url.frmTeam neq "")))>
	<cfset useMyTeams="true">
</cfif>
<!--- END:  2009-05-26 Sophos Support 2266 --->

<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">

<cf_title>phr_elearning_CertificationSummary</cf_title>

<!--- NYB 2009-05-26 Sophos Support 2266 - removed cfscript setting argumentsStruct 
and cfquery name="qryGetCertificationSummaryData" dbtype= " query
--->

<!--- START:  NYB 2009-05-26 Sophos Support 2266 
	- 	report was originally using query returned by relayElearning.GetCertificationSummaryData, however this 
		doesn't return the data I need for bug 2266 changes --->
<cfquery name="qryGetCertificationSummaryData" datasource="#application.siteDataSource#">
	select OrganisationID,Organisation_Name,Country,Code,Status,Registration_Date,Pass_Date,Num_Modules_To_Pass,Modules_Passed,
	PersonID,fullname as [Name], Title as Certification 
	from (
		select 
			v.*,o.OrganisationID
			,o.organisationName as organisation_name,c.countryDescription as country
			,p.firstname, p.lastname, p.firstName + ' ' +p.lastname as fullname
			<cfif useMyTeams and isdefined("frmTeam") and frmTeam NEQ "">
				,teamID
			</cfif>
			from vTrngCertificationSummary v
			inner join person p with (noLock) on p.personID = v.personID
			inner join organisation o with (noLock) on o.organisationID = p.organisationID
			inner join country c with (noLock) on o.countryID = c.countryID 
			<!--- 2012-07-25 PPB P-SMA001 commented out   
			<cfif restrictToUserCountryList> 
				and c.countryID in (#request.relayCurrentUser.countryList#,#request.relayCurrentUser.regionListSomeRights#)
			</cfif>
			--->
			<cfif useMyTeams and isdefined("frmTeam") and frmTeam NEQ "">
				inner join integerFlagData ifd with (noLock) on o.organisationID = ifd.entityID and ifd.Flagid =  <cf_queryparam value="#application.com.flag.getFlagStructure(AccountManagerFlagTextID).Flagid#" CFSQLTYPE="CF_SQL_INTEGER" >  
				inner join MyTeamMember mtm with (noLock) ON ifd.data = mtm.personid
				inner join MyTeam mt with (noLock) ON mtm.TeamMemberID = mt.Teamid 
				where mt.createdBy = #request.relayCurrentUser.personID#
			<cfelse>
				where 1=1 
			</cfif>
			<!--- 2012-08-09 PPB P-SMA001 used alias="o" because there is no v.countryId; done as a where clause instead of tacking on to above join for ease of custom extension; --->
			<cfif restrictToUserCountryList> 
				#application.com.rights.getRightsFilterWhereClause(entityType="trngCertification",alias="o",regionList=request.relayCurrentUser.regionListSomeRights).whereClause#		<!--- 2012-07-25 PPB P-SMA001 --->
			</cfif>
	) as base where 1=1
	<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
	group by OrganisationID,Organisation_Name,Country,PersonID,fullname,Code,Title,Status,Registration_Date,Pass_Date,Num_Modules_To_Pass,Modules_Passed
	order by <cf_queryObjectName value="#sortOrder#">
</cfquery>
<!--- END:  2009-05-26 Sophos Support 2266 --->

<table width="100%" cellpadding="3">
	<tr>
		<td><strong>phr_elearning_CertificationSummary</strong></td>
		<td align="right">&nbsp;</td>
	</tr>
</table>

<CF_tableFromQueryObject 
	queryObject="#qryGetCertificationSummaryData#"
	queryName="qryGetCertificationSummaryData"
	sortOrder = "#sortOrder#"
	numRowsPerPage="#numRowsPerPage#"
	startRow="#startRow#"
	openAsExcel="#openAsExcel#"
 	hideTheseColumns="Country"
	showTheseColumns="Organisation_Name,Country,Name,Code,Certification,Status,Registration_Date,Pass_Date,Num_Modules_To_Pass,Modules_Passed"
	columnTranslation="false"
	dateformat="Registration_Date,Pass_Date"
	numberFormat="Credits_Obtained,Num_Modules_To_Pass,Modules_Passed"
	totalTheseColumns="Credits_Obtained,Num_Modules_To_Pass,Modules_Passed"
	FilterSelectFieldList="Organisation_Name,Country,Code,Status"
	FilterSelectFieldList2="Organisation_Name,Country,Code,Status"
	GroupByColumns="Country"
	radioFilterLabel=""
	radioFilterDefault=""
	radioFilterName=""
	radioFilterValues=""
	checkBoxFilterName=""
	checkBoxFilterLabel=""

	keyColumnList="Organisation_Name,Name"
	keyColumnURLList="/data/dataframe.cfm?frmsrchOrgID=,/data/dataframe.cfm?frmsrchpersonID="
	keyColumnKeyList="organisationID,personID"

	allowColumnSorting="yes"
	useInclude="false"
	useTeams="#useMyTeams#"
	useTeamsPersonIDColumn="TeamId"
	translateTheseColumns="Certification"
>