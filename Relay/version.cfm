<!--- �Relayware. All Rights Reserved 2014 --->
<!--- WAB/RJT 2015-12-07 	Changed from a string to a structure
							Values are inserted during the build process.  
							No longer uses tokens (because they look naff on dev sites, so replacement actually looks for strings like 'xxxxx = ""' - so don't change too much!
 --->
<cfset relaywareVersionNumber =
	{
		  year = "" 				<!--- e.g. in 2015.1.10.2  year=2015 --->
	   ,  major = "" 				<!--- e.g. in 2015.1.10.2  major=1 --->
	   , minor = ""				<!--- e.g. in 2015.1.10.2  minor=10 --->
	   , maintenance = ""	<!--- e.g. in 2015.1.10.2  maintenance=2 --->
	   , build = ""				
	   , revision = ""		<!--- The SVN revision number, find in beanstalk --->
	   , branch = ""			<!--- Leave as is on tags --->
	}>
	
