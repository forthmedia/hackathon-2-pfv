<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
WAB 2010/06  Beginnings of a file where configuration tests can be kept
			Called by \admin\testConfig.cfc
WAB 2011/04/19 LID 6316/6310 Added tests for max file upload size and of configured mimeTypes
WAB 2016-04-19	CASE 449163 mimeType Test needs to be case insensitive
--->

	<!--- 
		TODO
		Add test for cf scriptpath
	--->

<cfcomponent cache="false" index="3">

	<cffunction name="Mapping" hint="Check if the Instance mapping has been set up" index="3">
		<cfargument name="applicationScope" default="#application#">
		<cfset var result = applicationScope.com.clustermanagement.hasInstanceMappingBeenSetUpCorrectly(applicationScope = applicationScope)>

		<cfreturn result>
	
	</cffunction>

	<cffunction name="MissingTemplateHandler" hint="Check whether IIS Missing Template Handler has been set up" index="1">
		<cfargument name="applicationScope" default="#application#">	
		
		<cfset var result = {isOK=true,message="OK"}>

		<cfif isDefined("request.currentSite")><!--- when called remotely there is no currentSite, --->
			<cfset urlToTest = "#request.currentSite.protocolAndDomain#/robots.txt">			
			<cfhttp url=#urlToTest# redirect="yes">
		
			<cfif cfhttp.statuscode is not "200 OK">
				<cfset result.isok = false>
				<cfset result.message = "Request to #urlToTest#.Status Code: #cfhttp.statuscode#.  Make sure that this missing template handler is set up and that it is used for local requests.  If the site is HTTPS, then this might indicate a certificate error">
			</cfif>
		<cfelse>
			<!--- not sure what happens if called remotely on a cluster with no IIS --->
			<cfset result = applicationScope.com.iisAdmin.isMissingTemplateHandlerSetUp()>
		</cfif>

		<cfreturn result>
	
	</cffunction>

	<cffunction name="AsynchronousGateway" hint="Check whether AsynchronousGateway is Working">
		<cfargument name="applicationScope" default="#application#">
		<cfset var result = applicationScope.com.asynchronousGatewayUtils.isAsynchronousGatewaySetUpCorrectly(applicationScope = applicationScope)>

		<cfreturn result>	
	
	</cffunction>
	
	
	<cffunction name="SiteWideErrorHandler" hint = "Check Site-Wide Error Handler Mapping and Setting">
		<cfargument name="applicationScope" default="#application#">
		<cfargument name="cfpassword" default="">
		<cfset result ={isOK= true, message=""}>
	
		<!--- Check Existing --->
		<cfset result  = applicationScope.com.errorHandler.setSitewideErrorHandlerMappingAndSetting(cfpassword = cfpassword)>
		<cfreturn result>
	
	</cffunction>

	<cffunction name="maxFileUploadSize" hint = "Checks That IIS is configured to handle maximum file size">
		<cfargument name="applicationScope" default="#application#">
		<cfset var result ={isOK= true, message=""}>
	
		<cfset var maxUploadSizeSetting_MB =  applicationScope.com.settings.getSetting('files.maxUploadSize_MB')>
		<cfset var maxUploadSizeIIS_MB =  applicationScope.com.iisadmin.getMaxFileSize() /1024/1024>
		<cfset var maxUploadSizeIIS_MB_Rounded =  round(maxUploadSizeIIS_MB * 100)/100>

		<!--- 2011/11/30 OLDA LID6316 Message added.  --->
		<cfset result.message  = "IIS Setting = #maxUploadSizeIIS_MB_Rounded# MB<BR>Relayware Setting = #maxUploadSizeSetting_MB# MB. Please call Relayware Customer Support to discusss an increase in your file size limits ">

		<cfif maxUploadSizeSetting_MB gt maxUploadSizeIIS_MB>
			<cfset result.isOK = false>
		</cfif>

		<cfreturn result>
	
	</cffunction>

	<cffunction name="checkMimeTypes" hint = "Checks That IIS has mimeTypes configured for all file types that can be uploaded.">
		<cfargument name="applicationScope" default="#application#">
		<cfset var result ={isOK= true, message=""}>

		<cfset extensions = applicationScope.com.settings.getSetting('files.allowedfileextensions')>
		
		<cfset IISMimeTypesXMLArray =  applicationScope.com.iisadmin.getMimeTypes()>
		
		<cfloop list = #extensions# index = "extension">
			<cfset x = xmlsearch (IISMimeTypesXMLArray[1],"//mimeMap[translate(@fileExtension,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='.#lcase(extension)#']")>
			<cfif not arrayLen(x)>
				<cfset result.isok = false>
				<cfset result.message = result.message & ".#extension# not supported <BR>">
			</cfif>
		
		</cfloop>
		
		<cfreturn result>
	
	</cffunction>




	
</cfcomponent>
