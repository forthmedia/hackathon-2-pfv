<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
**************************************************************
THIS FILE CONTAINS A UTF-8 CHARACTER SO MUST BE SAVED IN UTF-8
**************************************************************

WAB 2012-12-11
systemTests\datasource.cfc

Plug in for the system tests to check that the datasource has been configured correctly

 --->
<cfcomponent>
<cfprocessingdirective pageencoding="utf-8">

	<cffunction name="checkDataSourcenVarchar" hint = "Checks nVarChar/High Ascii Support">
		<cfargument name="applicationScope" default="#application#">
		<cfset var result ={isOK= true, message="OK"}>


			<cfset var character = chr(39057)>
			
			<cfif len(character) is not 1>
				<cfset result.isOK = false>
				<cfset result.message = "The test file appears to be corrupted #character# #len(character)#">
			
			<cfelse>
				<CFQUERY NAME="create" DATASOURCE="#applicationScope.SiteDataSource#">
				select N'#character#' as character
				into ##aTemporaryTableForNvarcharTest

				select * from ##aTemporaryTableForNvarcharTest
				
				where 
				character = <cfqueryparam value="#character#" cfsqltype="cf_sql_varchar">
				
				drop table ##aTemporaryTableForNvarcharTest
				</CFQUERY>
	
				<cfif create.recordCount is 0>
					<cfset result.isOK = false>
					<cfset result.message = "No Nvarchar Support">
				</cfif>

			</cfif>

		
		<cfreturn result>
	</cffunction>	

	<cffunction name="checkDataSourceCLOB" hint = "Checks CLOB Support">
		<cfargument name="applicationScope" default="#application#">
		<cfset var result ={isOK= true, message="OK"}>


			<cfset var longString = repeatString ('A',65000)>

			<CFQUERY NAME="create" DATASOURCE="#application.SiteDataSource#">
			create table ##aTemporaryTableForCLOBTest (nVarcharMaxField nvarchar(max))

			insert into ##aTemporaryTableForCLOBTest (nVarcharMaxField)
			values (<cf_queryparam value="#longString#" CFSQLTYPE="CF_SQL_VARCHAR" >)
			</CFQUERY>

			<CFQUERY NAME="read" DATASOURCE="#applicationScope.SiteDataSource#">
			select * from ##aTemporaryTableForCLOBTest
			
			drop table ##aTemporaryTableForCLOBTest
			</CFQUERY>

			<cfif len(read.nVarcharMaxField) is not len(longString)>
				<cfset result.isOK = false>
				<cfset result.message = "No CLOB Support">
			</cfif>
		
		<cfreturn result>

	</cffunction>

</cfcomponent>