<cfset qryBanners = application.com.banner.getBanner(bannerIds = listBanner)>
<cfif qryBanners.recordCount>
	
  <!--- LIBRARIES --->
  <cf_includecssonce template="/styles/banner.css" />
  <cf_includeJavascriptOnce template="javascript/banner.js">


  <!--- Start Carousel --->
  <cfoutput><div id="rwbanner" class="carousel slide" data-ride="carousel" style="height: #validatedBannerHeight# !important"></cfoutput>
  
    <!-- Indicators -->
    <cfif qryBanners.recordCount gt 1>
      <ol class="carousel-indicators">
        <cfset bisActive = "true"/>
        <cfloop index = "slideCount" from = "0" to = "#(qryBanners.recordCount - 1)#"> 
          <cfoutput>
            <li data-target="##rwbanner" data-slide-to="#slideCount#" #(bisActive)?"class=""active""":""#></li>
          </cfoutput>
          <cfset bisActive="false"/>
        </cfloop>
      </ol>
    </cfif>


    <!--- Carousel Inner --->
    <div class="carousel-inner">				
      <cfset bisActive = "true"/>
      <cfloop query='qryBanners'>
        <cfoutput>
          <cfset backgroundImageURL = '#application.com.fileManager.getSecuredFile(FileID=qryBanners.backgroundFileID).relativeURL#'>					
          <div id="bannerID#qryBanners.bannerID#" class="item#(bisActive)?" active":""# #ListChangeDelims(qryBanners.cssClasses,' ')#" 
                style="background-image: url('#URLDecode(backgroundImageURL)#')">
            <div class="carousel-caption">
              <div class="container">
                <h1>#qryBanners.heading#</h1>
                <div class="captionText">						
                  <p>#qryBanners.paragraph#</p>
                </div>
                <a href="#qryBanners.targetUrl#" class="btn btn-primary pull-left">#qryBanners.button#</a>
              </div>
            </div>
            <style>
              ##bannerID#qryBanners.bannerID# h1 {
                color: #qryBanners.headingFontColor#;
              }
              ##bannerID#qryBanners.bannerID# p {
                color: #qryBanners.paragraphFontColor#;
              }
            </style>    
          </div>            
        </cfoutput>
        <cfset bisActive="false"/>
      </cfloop> 
    </div>

    <!--- Controls --->
    <cfif qryBanners.recordCount gt 1>
      <a class="left carousel-control" href="#rwbanner" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
      </a>
      <a class="right carousel-control" href="#rwbanner" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
      </a>
    </cfif>

  </div> <!--- End Carousel ---> 

  <cfif #fullWidthBanner# eq "Yes">
    <cfoutput>
      <script>
	     jQuery(document).ready(function(){
           jQuery('##headerContainer').after(jQuery('##rwbanner'));
         });
      </script>
	</cfoutput>	
  </cfif>

</cfif>