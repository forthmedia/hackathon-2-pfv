
<cf_includeJavascriptOnce template="/javascript/extExtension.js"/>
<cf_includeJavascriptOnce template="/code/javascript/jscolor/jscolor.js"/>
<cf_includeJavascriptOnce template = "/javascript/jquery.minicolors.js">
<cf_includecssonce template = "/styles/minicolors.css">
<cf_includecssonce template = "/styles/banner.css">
<cf_includeJavascriptOnce template = "/javascript/jquery.hideseek.js">

<script>
function verifyDeletion(bannerid) {
	//#request.script_name#?
    if (confirm("Are you sure that you want to delete the banner " + bannerid) == true) {
		window.location.href = ("?delete=yes&bannerID="+bannerid)
    }
	return false;
}
</script>	

<cf_param name="getData" type="string" default="">
<cf_param name="sortOrder" type="string" default="title">
<cf_param name="numRowsPerPage" type="numeric" default="100">
<cf_param name="startRow" type="numeric" default="1">
<cf_param name="showTheseColumns" type="string" default="title,bannerID,targetUrl,Delete"/>
<cf_param name="keyColumnList" type="string" default="title,delete"/>
<cf_param name="keyColumnKeyList" type="string" default="bannerID,bannerID"/>
<cf_param name="keyColumnURLList" type="string" default=" , "/>

<cfif !StructKeyExists(url,"editor")>
	<cfparam name="headerAttributes.pageTitle" type="string" default="">
	<CF_RelayNavMenu pageTitle="#headerAttributes.pageTitle#" thisDir="/banner/banner">
		<CF_RelayNavMenuItem MenuItemText="phr_banner_add" CFTemplate="editBanner.cfm?editor=yes&add=yes">
	</CF_RelayNavMenu>
</cfif>

<cfif !StructKeyExists(url,"editor") and StructKeyExists(url,"delete")>
	<cfquery name="qryDeleteData" datasource="#application.siteDataSource#">
		DELETE
			[dbo].[recordrights]
		WHERE entity like 'banner' and recordid = <cfqueryparam cfsqltype="cf_sql_integer" value='#URL.bannerID#' />
	</cfquery>
	
	<cfquery name="qryDeleteData" datasource="#application.siteDataSource#">
		DELETE
			[dbo].[banner]
		WHERE
			bannerID = <cfqueryparam cfsqltype="cf_sql_integer" value='#URL.bannerID#' />
	</cfquery>
</cfif>

<cfset getData = application.com.banner.getBannersQuery(sortOrder=sortOrder)/>
<cfset xmlSource =application.com.banner.getEditorXML() />
	

<!--- ---------------------------------------------------------------------------
	A Sarabi 08/10/2016
	Banner Items live search box
--------------------------------------------------------------------------- --->

<cfif !StructKeyExists(url,"editor")>
	<cfoutput>
        <div class="grey-box-top">
          <label for="bannersearchbox">Search using titles, banner IDs or URLs:</label>
          <input type="text" id="bannersearchbox" name="search" placeholder="Search..." data-list="##tfqo_aReport tbody"  data-toggle="hideseek"/>
        </div>
	</cfoutput>
</cfif>

<!--- Call to List and editor --->
<cf_listAndEditor
  xmlSource="#xmlSource#"
  cfmlCallerName="banner"
  keyColumnList="#keyColumnList#"
  keyColumnKeyList="#keyColumnKeyList#"
  keyColumnOnClickList="javascript:openNewTab('##jsStringFormat(htmlEditFormat(title))##'*comma'##jsStringFormat(htmlEditFormat(title))##'*comma'#request.script_name#?editor=yes&hideBackButton=true&bannerID=##jsStringFormat(bannerID)##'*comma{reuseTab:true*commaiconClass:'banner'});return false;,javascript:verifyDeletion(##jsStringFormat(bannerID)##);return false;"
  showTheseColumns="#showTheseColumns#"
  hideBackButton="#iif(structKeyExists(URL,'hideBackButton') and url.hideBackButton is true,true,false)#"
  useInclude="false"
  sortOrder="#sortOrder#"
  doNotSortTheseColumns="delete,targetUrl"
  queryData="#getData#"
  numRowsPerPage="#numRowsPerPage#"
  startRow="#startRow#"
  showSaveAndAddNew="true"
  postSaveFunction=#bannerPostSaveFunction#
  columnTranslation="true"
  ColumnTranslationPrefix="phr_Banner_Builder_">

<cffunction name="bannerPostSaveFunction" returnType="struct" output='true'>
	<cfset var result = {isOK=true,message=""}/>
	<cfreturn result/>
</cffunction>





