<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			ReportSODLocationLookup.cfm	
Author:				NJH
Date started:		2008/12/16
	
Description:	Produces a report of sod location lookups		

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2009-01-26			NYB			ShowCols changed to SODShowCols - as ShowCols can be set in RelayINI for the Add Contanct Form 
								- which will overwrite this entry
2012-10-04	WAB		Case 430963 Remove Excel Header 

Possible enhancements:


 --->
 
<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">

<cfparam name="openAsExcel" type="boolean" default="false"> 
<cfparam name="sortOrder" type="string" default="ThirdPartyForeignKey">
<cfparam name="SODShowCols" type="string" default="ThirdPartyForeignKey,LocationID,OrganisationID,OrganisationName,MatchMethod,Created">
<cfparam name="numRowsPerPage" default = "100">

 
	<cf_head>
		<cf_title>SOD Location Lookup Report</cf_title>
	</cf_head>
	
	
	<CF_RelayNavMenu>
		<CF_RelayNavMenuItem MenuItemText="Excel" CFTemplate="#cgi["SCRIPT_NAME"]#?#queryString#&openAsExcel=true">
	</CF_RelayNavMenu>

<cfset sodLoadLocationLookupQry = application.com.relaySOD.getSodLoadLocationLookupData(showEntityTypeIDData=2,sortOrder=sortOrder)>

<cf_tableFromQueryObject 
	queryObject="#sodLoadLocationLookupQry#"
	queryName="sodLoadLocationLookupQry"
	sortOrder = "#sortOrder#"
	dateFormat ="created"
	showTheseColumns="#SODShowCols#"
	openAsExcel="#openAsExcel#"
	numRowsPerPage="#numRowsPerPage#"
	useInclude="false"
>

<cfif not openAsExcel>
	
	
	
</cfif>

