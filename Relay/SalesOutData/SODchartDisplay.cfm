<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			SODchartDisplay.cfm	
Author:				NJH
Date started:		2006-07-29
	
Purpose:	A graphical display of SOD product sales by month.

Usage: 			

Amendment History:


Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:

--->
<cfparam name="orgID" type="numeric">
<cfparam name="numOfMonths" type="numeric" default="3">
<cfparam name="productCode" type="string" default="All">

<cfset chartTitle="Product Sales in Last "&#numOfMonths#&" Months">
<cfif productCode neq "All">
	<cfset chartTitle= #productCode#&" Sales in Last "&#numOfMonths#&" Months">
</cfif>




<cf_head>
	<cf_title>SOD Chart Display</cf_title>
</cf_head>


<cfinclude template="/styles/mediumChartStyles.cfm">
<cfif orgID neq "">
	<cfset application.com.relayDashboard.generateCFChart(chartTitle=#chartTitle#,chartName=#chartTitle#,chartDataMethod="getSODproductSalesByMonth", organisationID=#orgID#, numOfMonths=#numOfMonths#,productCode=#productCode#)>
<cfelse>
	<cfset application.com.relayDashboard.generateCFChart(chartTitle=#chartTitle#,chartName=#chartTitle#,chartDataMethod="getSODproductSalesByMonth",numOfMonths=#numOfMonths#,productCode=#productCode#)>
</cfif>



