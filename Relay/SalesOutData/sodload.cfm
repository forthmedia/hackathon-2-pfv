<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			SODload.cfm	
Author:				NJH
Date started:		2007-07-02
	
Description:			

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed

Possible enhancements:
need to alter columns 
	DistributorCODE/ResellerID > DistributorResellerID
	SKU/SKUPartNumber > SKUPartNumber

 --->




<cf_head>
	<cf_title>SOD Load</cf_title>
</cf_head>



<!--- 

1. loop through SOD data.
2. create a rwtransaction when this file is run
3. if company account (rwcompanyAccount)doesn't exist, create it otherwise update it.

 --->


<cf_translate>

<cfsetting requesttimeout="6000" enablecfoutputonly="No">

<cfparam name="numOfClaimsToProcess" type="numeric" default="500">

<cf_includeJavascriptOnce template="/javascript/openWin.js">

<cfif fileexists("#application.paths.code#\cftemplates\incentiveINI.cfm")>
	<cfinclude template="/code/cftemplates/incentiveINI.cfm">
</cfif>

<cfset request.relayFormDisplayStyle="HTML-table">


<cfif isDefined("frmSourceID")>
	<cfscript>
		SourceDetailsQry = application.com.relaySOD.getSourceDetails(sourceID=frmSourceID);
	</cfscript>
	
	<!--- ensure that all the necessary columns have data in them. Otherwise, reject the load.
		This forces the user to clean up the data.
		Not particularly sure why this is done as we could just have the columns be not null --->
	<cfset isValid = application.com.relaySOD.validateDataLoadTable(sourceID=frmSourceID)>
</cfif>


<!--- if a datasource hasn't been defined or if it has but it's invalid --->
<cfif not isDefined("frmSourceID") or isDefined("frmChangeSource") <!--- or (isDefined("frmSourceID") and not isValid) --->>

	<cfscript>
		sourceQry = application.com.relaySOD.getSODSources();
	</cfscript>
	
	<cfform  method="post" name="getSourceForm">
		<cf_relayFormDisplay>
			<cf_relayFormElementDisplay relayFormElementType="select" fieldName="frmSourceID" label="phr_sod_sodSource" currentValue="" query="#sourceQry#" display="source" value="sourceID" nullText="phr_sod_selectASource" nullValue="">
			<cf_relayFormElementDisplay relayFormElementType="submit" fieldname="submitBtn" label="" currentValue="submit">
		</cf_relayFormDisplay>
	</cfform>
	
	<!--- if the dataload table contains nulls --->
	<cfif isDefined("frmSourceID") and not isValid>
		<cfoutput>
			Your dataload table contains nulls in key fields #htmleditformat(SourceDetailsQry.location)#, #htmleditformat(SourceDetailsQry.product)# or #htmleditformat(SourceDetailsQry.quantity)#.
		</cfoutput>
	</cfif>
	
<cfelse>

	<cfscript>
		SourceDetailsQry = application.com.relaySOD.getSourceDetails(sourceID=frmSourceID);
	</cfscript>
	
	<!--- this is the 'table' to run the queries against. It can either be a view or a table
		A view is used if we need to add columns that may not exist in the table, such as a valid date column --->
	<cfif not SourceDetailsQry.useView>
		<cfset queryDataTable = SourceDetailsQry.dataSource>
	<cfelse>
		<cfset queryDataTable = "v#SourceDetailsQry.dataSource#">
	</cfif>
	
	<!--- this is the original table that we need to add control columns to --->
	<cfset tableName = SourceDetailsQry.dataSource>
	<cfset batchTimeStamp = createODBCdateTime(request.requestTime)>

	<CFIF Not IsDefined("tableName")>
		You must pass tableName to SODLoadLicence.cfm.  It should contain the name of the table you are wanting to process
		<cfexit method="EXITTEMPLATE">
	</CFIF>
	
	<cfset request.relayFormDisplayStyle="HTML-table">
	<cfform name="SODloadActions"  method="post">
		<cf_relayFormDisplay>
			<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="" label="phr_SOD_Datasource" currentValue="#tableName#">
			<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="" label="phr_SOD_ServerID" currentValue="#request.relaywareServerID#">
			<cf_relayFormElementDisplay relayFormElementType="submit" fieldname="frmChangeSource" currentValue="phr_SOD_changeSource" label="">
		</cf_relayFormDisplay>
	
		<cf_relayFormDisplay>
			<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="" label="" currentValue="<h2>phr_SOD_SODFunctions</h2>" spanCols="yes">
			<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="" label="phr_SOD_matchSOD" currentValue="">
				<cf_relayFormElement relayFormElementType="submit" fieldname="frmMatchSOD" currentValue="phr_SOD_matchSOD">
			</cf_relayFormElementDisplay>
			<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="" label="phr_SOD_loadSOD" currentValue="">
				<cf_relayFormElement relayFormElementType="submit" fieldname="frmLoadSOD" currentValue="phr_SOD_loadSOD">
			</cf_relayFormElementDisplay>
			<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="" label="phr_SOD_createTransactionsFromSODClaims" currentValue="">
				<cf_relayFormElement relayFormElementType="submit" fieldname="frmProcessSODClaims" currentValue="phr_sod_CreateTransactions">
			</cf_relayFormElementDisplay>
			<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="" label="phr_SOD_reprocessRecords" currentValue="">
				<cf_relayFormElement relayFormElementType="submit" fieldname="frmReprocessSODMaster" currentValue="phr_SOD_reprocessRecords">
			</cf_relayFormElementDisplay>
			<!--- <cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="" label="phr_SOD_viewUnMatchedTransReport" currentValue="">
				<cf_relayFormElement relayFormElementType="button" fieldname="frmViewReport" currentValue="phr_SOD_View" onClick="javascript:openWin('/salesOutData/SODunmatchedTransactionsReport.cfm','Unmatched','width=950,height=600,toolbar=0,location=0,directories=0,status=1,menuBar=0,scrollBars=1,resizable=1');">
			</cf_relayFormElementDisplay> --->
			<cf_relayFormElementDisplay relayFormElementType="hidden" fieldname="frmSourceID" currentValue="#frmSourceID#" label="">
		</cf_relayFormDisplay>
	</cfform>
	
	<!--- add necessary columns to SODloadTable if they don't already exist --->
	<!--- update the control columns in the dataload table --->
	<cfif isDefined("frmMatchSOD")>
	
		<cfscript>
			application.com.relaySOD.addControlColumns(dataloadTable=tableName);
			application.com.relaySOD.updateControlColumns(tableName=queryDataTable,sourceID=frmSourceID);
		</cfscript>
		
		<cfquery name="getTotalTransactions" datasource="#application.siteDataSource#">
			select count(*) as getNumberTransactions from #tableName#
		</cfquery>
		<cfoutput>
			There are #htmleditformat(getTotalTransactions.getNumberTransactions)# total Transactions in the table #htmleditformat(tableName)#<br/><br />
		</cfoutput>
		
		<!--- output the location matching results --->
		<cfquery name="getNumbLocs" datasource="#application.siteDataSource#">
			select count(distinct #SourceDetailsQry.location#) as numRecs from #tableName#
		</cfquery>
		
		<cfquery name="getNonMatched" datasource="#application.siteDataSource#">
			select count(distinct #SourceDetailsQry.location#) as recs from #tableName# where locationID is null
		</cfquery>
		
		<strong>Location Matching: </strong><cfoutput>There are #htmleditformat(getNumbLocs.numRecs)# resellers that matched and #htmleditformat(getNonMatched.recs)# that did not.<br><br></cfoutput>
		
		<!--- output the product matching results --->
		<cfquery name="getNumProducts" datasource="#application.siteDataSource#">
			select count(distinct #SourceDetailsQry.product#) as numRecs from #tableName#
		</cfquery>
		
		<cfquery name="getNonMatched" datasource="#application.siteDataSource#">
			select count(distinct #SourceDetailsQry.product#) as recs from #tableName# where productID is null
		</cfquery>
		<strong>Product Matching: </strong>Out of <cfoutput>#htmleditformat(getNumbLocs.numRecs)#</cfoutput> distinct product codes, there are <cfoutput>#htmleditformat(getNonMatched.recs)#</cfoutput> product codes that did not match.<br><br>
		
		<!--- output the num of records with null points --->
		<cfquery name="getNonMatched" datasource="#application.siteDataSource#">
			select count(*) as recs from #tableName# where points is null
		</cfquery>
		<strong>Points: </strong>There are <cfoutput>#htmleditformat(getNonMatched.recs)#</cfoutput> records with null points value<br><br>
		
		<!--- output the account matching results --->
		<cfquery name="getNonMatched" datasource="#application.siteDataSource#">
			select count(*) as recs from #tableName# where RWCompanyAccountID is null
		</cfquery>
		<strong>Company Account Matching: </strong>There are <cfoutput>#htmleditformat(getNonMatched.recs)#</cfoutput> records with a null RWCompanyAccountID value.<br><br>
	
		<cfquery name="getNonMatchedTransactions" datasource="#application.siteDataSource#">
			select count(*) as recs from #tableName# where rwPersonAccountID is null
		</cfquery>
		<cfquery name="getNonMatchedAccounts" datasource="#application.siteDataSource#">
			select count(distinct #SourceDetailsQry.location#) as recs from #tableName# where rwPersonAccountID is null
		</cfquery>
		
		<strong>Person Account Matching: </strong><cfoutput>There are #htmleditformat(getNonMatchedAccounts.recs)# records with a null RWPersonAccountID value and these have #htmleditformat(getNonMatchedTransactions.recs)# transactions.<br><br></cfoutput>
		
		<!--- output the records that are ready for processing --->
		<cfquery name="getMatchedRecords" datasource="#application.siteDataSource#">
			select count(*) as recs from #tableName# where matchdate is not null
		</cfquery>
		
		<strong>Matched Records: </strong><cfoutput>There are #htmleditformat(getMatchedRecords.recs)# records ready for processing.<br><br></cfoutput>
		
		
	<!--- insert records from dataload table into the master table --->
	<!--- create SOD claims from available records in the SOD master table--->
	<cfelseif isDefined("frmLoadSOD")>
	
		<cftransaction action = "begin">
			<cftry>		
				<cfscript>
					application.com.relaySOD.moveToSODMaster(dataloadTable=queryDataTable,sourceID=frmSourceID,batchDate=batchTimeStamp);
					application.com.relaySOD.moveMatchedRecordsToSODClaim(batchDate=batchTimeStamp,reprocessedRecords=false);
					application.com.relaySOD.moveUnMatchedRecordsToSODFailure(batchDate=batchTimeStamp);
				</cfscript>
				<cftransaction action="commit"/>
			
				<cfcatch type="Database"> 
					<cfoutput> 
					     <h3>Error loading SOD data into SOD tables</h3> 
					     <p>Message :#htmleditformat(cfcatch.message)#</p> 
					     <p>Type :#htmleditformat(cfcatch.type)#</p>
						 <p>Detail :#htmleditformat(cfcatch.detail)#</p>
					     <p><i>Rolling back the attempted transaction</i></p> 
					     <cftransaction action="rollback"/>
					</cfoutput> 
				</cfcatch>
			</cftry>

		</cftransaction>
		
		<!--- truncate dataload table if successful --->
		<!--- need to check if all rows are in master table and that the correct number of rows are in the sodFailure and sodClaim tables --->
		<cfscript>
			loadSuccessful = application.com.relaySOD.verifySuccessfulLoad(sourceID=frmSourceID,batchDate=batchTimeStamp);
		</cfscript>
		
		<cfif loadSuccessful>
			<!--- <cfquery name="truncateDataLoadTable" datasource="#application.siteDataSource#">
				truncate table #tableName#
			</cfquery> --->
			
			Table was loaded successfully.
		<cfelse>
			Table was not loaded successfully.
		</cfif>
		
		<cfquery name="getRowsJustLoaded" datasource="#application.siteDataSource#">
			select count(*) as recs from SODMaster where created =  <cf_queryparam value="#batchTimeStamp#" CFSQLTYPE="CF_SQL_TIMESTAMP" >  and sourceID =  <cf_queryparam value="#frmSourceID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfquery>
		
		<cfquery name="getNumMatchedRecords" datasource="#application.siteDataSource#">
			select count(*) as recs from sodClaim where batchDate =  <cf_queryparam value="#batchTimeStamp#" CFSQLTYPE="CF_SQL_TIMESTAMP" >  and sourceID =  <cf_queryparam value="#frmSourceID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfquery>
		
		<cfquery name="getNumUnMatchedRecords" datasource="#application.siteDataSource#">
			select count(*) as recs from sodFailure where batchDate =  <cf_queryparam value="#batchTimeStamp#" CFSQLTYPE="CF_SQL_TIMESTAMP" >  and sourceID =  <cf_queryparam value="#frmSourceID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfquery>
		
		<cfoutput>
		<p>A total of #htmleditformat(getRowsJustLoaded.recs)# records moved to SODMaster.</p>
		<p>#htmleditformat(getNumMatchedRecords.recs)# matched records moved to SODclaims.</p>
		<p>#htmleditformat(getNumUnMatchedRecords.recs)# unmatched records moved to SODfailure table</p>
		</cfoutput>
		
	<!--- creating the claims from the SODclaim table --->
	<cfelseif isDefined("frmProcessSODClaims")>
	
		<cfscript>
			application.com.relaySOD.processSODclaims(sourceID=frmSourceID,rwPromotionID=4);
		</cfscript>
		
		<!--- get the count of records from SODmaster that have just been processed --->
		<cfquery name="getNumOfClaimsProcessed" datasource="#application.siteDataSource#">
			select count(*) as processedClaimsCount from SODMaster
				where processDate =  <cf_queryparam value="#request.requestTime#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
		</cfquery>
		
		<cfoutput>#htmleditformat(getNumOfClaimsProcessed.processedClaimsCount)# claims processed.<br></cfoutput>
		
	<cfelseif isDefined("frmReprocessSODMaster")>
	
		<!--- match and move matched records to SOD claims --->
		<cftransaction action = "begin">
			<cftry>
				<cfscript>
					application.com.relaySOD.updateControlColumns(tableName='SODMaster',sourceID=frmSourceID);
					application.com.relaySOD.moveMatchedRecordsToSODClaim();
				</cfscript>
				
				<cftransaction action="commit"/>
			
				<cfcatch type="Database"> 
					<cfoutput> 
					     <h3>Error matching and moving records.</h3> 
					     <p>Message :#htmleditformat(cfcatch.message)#</p> 
					     <p>Type :#htmleditformat(cfcatch.type)#</p>
						 <p>Detail :#htmleditformat(cfcatch.detail)#</p>
					     <p><i>Rolling back the attempted transaction</i></p> 
					     <cftransaction action="rollback"/>
					</cfoutput> 
				</cfcatch>
			</cftry>

		</cftransaction>
		
		<cfquery name="getMatchedRows" datasource="#application.siteDataSource#">
			select count(*) as rowsMatched from SODMaster where matchDate =  <cf_queryparam value="#request.requestTime#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
		</cfquery>
		
		<cfquery name="getRecordsMovedFromSodFailure" datasource="#application.siteDataSource#">
			select count(*) as rowsMoved from SODmaster sm left join SODFailure sf
				on sm.batchRowID = sf.batchRowID and
				sm.created = sf.batchDate and
				sm.sourceID = sf.sourceID
			where sf.batchDate is null and
				sm.matchDate =  <cf_queryparam value="#request.requestTime#" CFSQLTYPE="CF_SQL_TIMESTAMP" >  and 
				sm.processDate is null
		</cfquery>
		
		<cfquery name="getRecordsMovedToSODClaim" datasource="#application.siteDataSource#">
			select count(*) as rowsMoved from SODmaster sm inner join SODclaim sc
				on sm.batchRowID = sc.batchRowID and
				sm.created = sc.batchDate and
				sm.sourceID = sc.sourceID
			where sm.matchDate =  <cf_queryparam value="#request.requestTime#" CFSQLTYPE="CF_SQL_TIMESTAMP" > 
				and processDate is null
		</cfquery>
		
		<cfoutput>
			#htmleditformat(getMatchedRows.rowsMatched)# records in SODMaster have just matched.<br>
			#htmleditformat(getRecordsMovedFromSodFailure.rowsMoved)# records were moved from SODFailure.<br>
			#htmleditformat(getRecordsMovedToSODClaim.rowsMoved)# records were moved to SODClaim.<br>
		</cfoutput>
	
	</cfif>

</cfif>

</cf_translate>


