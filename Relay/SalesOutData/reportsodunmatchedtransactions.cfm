<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			reportSODunmatchedTransactions.cfm	
Author:				NJH
Date started:		2007/09/14
	
Description:	Produces a report of SOD transactions that haven't matched (on products and locations, RWCompanyAccounts and RWPersonAccounts)
				 to allow us to create Rewards transactions in Relayware		

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2012-10-04	WAB		Case 430963 Remove Excel Header 

Possible enhancements:


 --->

<cfparam name="sortOrder" type="string" default="loaded,invoice_Date,invoice_number">
<cfparam name="showCols" type="string" default="Datasource,LocationID,OrganisationID,Invoice_Number,Invoice_Date,Product_Code,Points,Company_Account_Matched,Person_Account_Matched,Loaded_By,Loaded">
<cfparam name="hideCols" type="string" default="quantity,sourceID,order_Value">
<cfparam name="numRowsPerPage" type="numeric" default=1000>
<cfparam name="openAsExcel" type="boolean" default="false">

<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">

<cfset tableName="vUnmatchedSODTransactions">
<cfset dateField="invoice_Date">
<cfset dateRangeFieldName="invoice_Date">
<cfparam name="useFullRange" default="true">

<cfinclude template="/templates/DateQueryWhereClause.cfm"> 

<cfquery name="getUnmatchedTransactions" datasource="#application.siteDataSource#">
	select * from vUnmatchedSODTransactions
	where 1=1
	<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
	order by <cf_queryObjectName value="#sortOrder#">
</cfquery>


	<cf_head>
		<cf_title>SOD Unmatched Transactions report</cf_title>
	</cf_head>
	<cfinclude template="SODTopHead.cfm">
	

<cf_tableFromQueryObject 
	queryObject="#getUnmatchedTransactions#"
	queryName="getUnmatchedTransactions"
	sortOrder = "#sortOrder#"
	numberFormat = "points"
	numRowsPerPage = "#numRowsPerPage#"
	
	showTheseColumns="#showCols#"
	hideTheseColumns="#hideCols#"
	
	FilterSelectFieldList="datasource"
	FilterSelectFieldList2="Company_Account_Matched,Person_Account_Matched"
	dateFormat="loaded,invoice_Date"
	
	>

<!--- 
	keyColumnList="productCode"
	keyColumnURLList="/products/editProduct.cfm?campaignID=4&productID="
	keyColumnKeyList="productID"
	keyColumnOpenInWindowList="yes"
	functionListQuery="#comTableFunction.qFunctionList#" --->
<cfif not openAsExcel>
	
	
</cfif>

