<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			SODproductTask.cfm	
Author:				NJH
Date started:		2006-07-29
	
Purpose:	Move or remove products and/or herirarchies from a SOD group.

Usage: 			

Amendment History:


Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:

--->
<cf_translate>

<cfif not StructKeyExists(form, "frmSODgroupID") and not StructKeyExists(url, "frmSODgroupID")>
	frmSODgroupID must be passed as a parameter
	<CF_ABORT>
</cfif>

<cfparam name="frmEditHierarchyID" default="-1">
<cfparam name="frmProduct" default="">
<cfparam name="frmProductHierarchyTask" default="">

<cfoutput>
	<SCRIPT type="text/javascript">
		function reloadParentAndClose(){
			opener.location.href="SODhierarchyManagement.cfm?frmSODgroupID=#jsStringFormat(frmSODgroupID)#";
			self.close();
		}
	</script>
</cfoutput>

<cfif isDefined("frmMove")>
	<cfif frmProductHierarchyTask eq "editHierarchy">
		<cfset application.com.SOD.moveSODhierarchy(groupID=#frmSODgroupID#, hierarchyID=#currentHierarchyID#, level1=#frmSODlevel1Txt#, level2=#frmSODlevel2Txt#, level3=#frmSODlevel3Txt#, level4=#frmSODlevel4Txt#, level5=#frmSODlevel5Txt#)>
		<SCRIPT type="text/javascript">
			reloadParentAndClose();
		</script>
	<cfelseif frmProductHierarchyTask eq "moveProduct">
		<cfset hierarchyID = application.com.SOD.addSODHierarchy(groupID=#frmSODgroupID#, level1=#frmSODlevel1Txt#,level2=#frmSODlevel2Txt#, level3=#frmSODlevel3Txt#, level4=#frmSODlevel4Txt#, level5=#frmSODlevel5Txt#)>
		<cfset application.com.SOD.addSODproduct(productCode=#frmProduct#,hierarchyID=#hierarchyID#)>
		<cfset application.com.SOD.deleteSODproduct(productCode=#frmProduct#,hierarchyID=#currentHierarchyID#)>
		<SCRIPT type="text/javascript">
			reloadParentAndClose();
		</script>
	</cfif>
<cfelse>
	<cfset groupHierarchies = application.com.SOD.getSODhierarchies(groupID = #frmSODgroupID#)>
	
	<cfquery name="hierarchiesMinusCurrentHierarchy" dbtype="query">
		select level1, level2, level3, level4, level5, productHierarchyID, productHierarchy, productHierarchyDisplay from groupHierarchies
			where productHierarchyID <> #frmEditHierarchyID#
	</cfquery>
	
	<cfquery name="getCurrentHierarchy" datasource="#application.sitedatasource#">
		select level1 + '-' + level2 + '-' + level3 + '-' + level4 + '-' + level5 as
		levels from SODhierarchy where productHierarchyID =  <cf_queryparam value="#frmEditHierarchyID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</cfquery>



	
	
	<cf_head>
		<cf_title>SOD Product Task</cf_title>
		
		<SCRIPT type="text/javascript">
		
			function validateHierarchyLevels(){
				if (productForm.frmSODlevel1Txt.value == "") {
					alert("phr_sys_SOD_PleaseEnterAvalueForLevel1");
					productForm.frmSODlevel1Txt.focus();
					return false;
				} else if (productForm.frmSODlevel2Txt.value == "") {
					alert("phr_sys_SOD_PleaseEnterAvalueForLevel2");
					productForm.frmSODlevel2Txt.focus();
					return false;
				} else if (productForm.frmSODlevel3Txt.value == "") {
					alert("phr_sys_SOD_PleaseEnterAvalueForLevel3");
					productForm.frmSODlevel3Txt.focus();
					return false;
				} else if (productForm.frmSODlevel4Txt.value == "") {
					alert("phr_sys_SOD_PleaseEnterAvalueForLevel4");
					productForm.frmSODlevel4Txt.focus();
					return false;
				} else if (productForm.frmSODlevel5Txt.value == "") {
					alert("phr_sys_SOD_PleaseEnterAvalueForLevel5");
					productForm.frmSODlevel5Txt.focus();
					return false;
				}
				return true;
			}
		
			function changeTextValues(productHierarchyObj) {
				var hierarchy = productHierarchyObj.value.split('|');
				for (var i=0;i<hierarchy.length;i++) {
					switch(i) {
						case 0: productForm.frmSODlevel1Txt.value = hierarchy[i];break;
						case 1: productForm.frmSODlevel2Txt.value = hierarchy[i];break;
						case 2: productForm.frmSODlevel3Txt.value = hierarchy[i];break;
						case 3: productForm.frmSODlevel4Txt.value = hierarchy[i];break;
						case 4: productForm.frmSODlevel5Txt.value = hierarchy[i];break;
					}
				}
			}
		
		</script>
	</cf_head>
	
	<cfset hierarchySize=10>
	<cfset request.relayFormDisplayStyle="HTML-table">
	<cfform action="SODhierarchyTask.cfm" method="post" name="productForm">
		<cf_relayFormDisplay>
		
			<cfif frmProductHierarchyTask eq "moveProduct" or frmProductHierarchyTask eq "editHierarchy">
				<cfif frmProductHierarchyTask eq "moveProduct">
					<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="" currentValue="#frmProduct#" label="phr_sys_SOD_ProductsToMove">
				</cfif>
				<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="" currentValue="#getCurrentHierarchy.levels#" label="phr_sys_SOD_currentCategory">
				
				<cf_relayFormElementDisplay relayFormElementType="hidden" fieldname="frmSODgroupID" currentValue="#frmSODgroupID#" label="">
				<cf_relayFormElementDisplay relayFormElementType="hidden" fieldname="currentHierarchyID" currentValue="#frmEditHierarchyID#" label="">
				<cf_relayFormElementDisplay relayFormElementType="hidden" fieldname="frmProduct" currentValue="#frmProduct#" label="">
				<cf_relayFormElementDisplay relayFormElementType="hidden" fieldname="frmProductHierarchyTask" currentValue="#frmProductHierarchyTask#" label="">
				
				<cf_relayFormElementDisplay relayFormElementType="HTML" currentValue="" fieldname="" label="phr_sys_SOD_destinationCategory">
					<cfif hierarchiesMinusCurrentHierarchy.recordCount gt 0>
						<cf_relayFormElement relayFormElementType="select" fieldname="frmSODhierarchyLevels" currentValue="" label="phr_sys_SOD_Categories" query="#hierarchiesMinusCurrentHierarchy#" display="productHierarchyDisplay" value="productHierarchy" onChange="changeTextValues(frmSODhierarchyLevels);"><br><br>
					</cfif>
					<cf_relayFormElement relayFormElementType="text" fieldname="frmSODlevel1Txt" currentValue="#IIF(hierarchiesMinusCurrentHierarchy.recordCount gt 0, DE(hierarchiesMinusCurrentHierarchy.level1[1]), DE(''))#" label="phr_sys_SOD_SODhierarchyLevel1" size="#hierarchySize#" maxLength="#hierarchySize#">
					<cf_relayFormElement relayFormElementType="text" fieldname="frmSODlevel2Txt" currentValue="#IIF(hierarchiesMinusCurrentHierarchy.recordCount gt 0, DE(hierarchiesMinusCurrentHierarchy.level2[1]), DE(''))#" label="phr_sys_SOD_SODhierarchyLevel2" size="#hierarchySize#" maxLength="#hierarchySize#">
					<cf_relayFormElement relayFormElementType="text" fieldname="frmSODlevel3Txt" currentValue="#IIF(hierarchiesMinusCurrentHierarchy.recordCount gt 0, DE(hierarchiesMinusCurrentHierarchy.level3[1]), DE(''))#" label="phr_sys_SOD_SODhierarchyLevel3" size="#hierarchySize#" maxLength="#hierarchySize#">
					<cf_relayFormElement relayFormElementType="text" fieldname="frmSODlevel4Txt" currentValue="#IIF(hierarchiesMinusCurrentHierarchy.recordCount gt 0, DE(hierarchiesMinusCurrentHierarchy.level4[1]), DE(''))#" label="phr_sys_SOD_SODhierarchyLevel4" size="#hierarchySize#" maxLength="#hierarchySize#">
					<cf_relayFormElement relayFormElementType="text" fieldname="frmSODlevel5Txt" currentValue="#IIF(hierarchiesMinusCurrentHierarchy.recordCount gt 0, DE(hierarchiesMinusCurrentHierarchy.level5[1]), DE(''))#" label="phr_sys_SOD_SODhierarchyLevel5" size="#hierarchySize#" maxLength="#hierarchySize#">
				</cf_relayFormElementDisplay>
			</cfif>
				
			<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="" currentValue="" label="" valueAlign="center" spanCols="yes">
				<cfif frmProductHierarchyTask eq "moveProduct" or frmProductHierarchyTask eq "editHierarchy">
					<cf_relayFormElement relayFormElementType="button" fieldname="frmClose" currentValue="phr_sys_SOD_Close" label="" onClick="javascript:self.close();">
					<cf_relayFormElement relayFormElementType="submit" fieldname="frmMove" currentValue="phr_sys_SOD_Move" label="" onClick="return validateHierarchyLevels();" valueAlign="left">
				<cfelse>
					<cf_relayFormElement relayFormElementType="button" fieldname="frmClose" currentValue="phr_sys_SOD_Close" label="" onClick="javascript:if(!opener.closed){opener.location.reload(true)};self.close();">
				</cfif>
			</cf_relayFormElementDisplay>
			
		</cf_relayFormDisplay>
	 </cfform>
	
	
</cfif>
</cf_translate>
