<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			SODmatchLocations.cfm	
Author:				NJH
Date started:		2006-07-29
	
Purpose:	

Usage: 			

Amendment History:


Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:

--->
<cf_translate>

<!--- setting up some variables if they're not defined yet --->
<cfif not isDefined("frmCountryChanged")>
	<cfset frmCountryChanged = false>
</cfif>
<cfif not isDefined("frmOrderByChanged")>
	<cfset frmOrderByChanged = false>
</cfif>
<cfif not isDefined("frmSearchMode")>
	<cfset frmSearchMode = "Tight">
</cfif>

<cfif not isDefined("orgID")>

	<!--- org is not defined, so we're attempting to match locations in SODinitialLoad with organisations in relayware. --->

	<cfset mode="findInCM">

	<cfif isDefined("frmCreateMatch")>
		<cfset application.com.relaySOD.createMatch(thirdPartyForeignKey=#frmSODlocations#, locationID=#frmMatch#)>
	</cfif>
	<!--- countries now setup in application.SODCountryData by userfiles/code/cftemplates/SODload.cfm which is run daily.This is JIC... --->
	<cfif not structKeyExists(application, "SODCountryData") or structisempty(application.SODCountryData)>
		<cfscript>application.com.relaySOD.setApplicationSODCountryData();</cfscript>
	</cfif>
	
	<cfif not isDefined("frmOrderBy")>
		<cfset frmOrderBy="CompanyName">
	</cfif>
	
	<cfif not isDefined("frmCountry")>
		<cfset frmCountry = request.relayCurrentUser.CountryID>
	</cfif>
	
	<cfquery name="getUnMatchedLocations" datasource="#application.siteDataSource#">
		select distinct thirdPartyForeignKey as companyID, companyName + ' - ' + isnull(postalCode,' ') as companyName, sum(totalPrice) as totalPrice, sum(Quantity) as Quantity 
		from SODinitialLoad 
		where thirdPartyForeignKey not in
			(select thirdPartyForeignKey from SODloadLocationLookup)
		and countryID =  <cf_queryparam value="#frmCountry#" CFSQLTYPE="CF_SQL_INTEGER" > 
		group by thirdPartyForeignKey, companyName + ' - ' + isnull(postalCode,' '), country
		order by
		<cfif isDefined("frmOrderBy")>
			<cfif frmOrderBy eq "CompanyName">
				companyName asc
			<cfelse>
				sum(#frmOrderBy#) desc
			</cfif>
		</cfif>
	</cfquery>
	
	<cf_querySim>
		orderBy
		display,value
		phr_sys_SOD_OrderByName|CompanyName
		phr_sys_SOD_OrderByValue|TotalPrice
		phr_sys_SOD_OrderByVolume|Quantity
	</cf_querySim>
	
<cfelse>

	<!--- orgID is defined... we've come from an organisation's Sales Out Data screen --->
	<cfset mode="findInSOD">
	
	<cfif isDefined("frmCreateMatch")>
		<!--- creating a match --->
		<cfset application.com.relaySOD.createMatch(thirdPartyForeignKey=#frmMatch#, locationID=#frmSODlocations#)>
	</cfif>
	<!--- only run once per session --->
	<cfif not structKeyExists(session, "SODCountryData")>
		<cfset userCountries = application.com.relaySOD.getUnMatchedOrganisationLocations(organisationID=#orgID#)>
	</cfif>
	
	<cfquery name="getUnMatchedLocations" datasource="#application.siteDataSource#">
		select distinct locationID as companyID, SiteName 
				+ case WHEN len(ltrim(Address1)) = 0 then '' else ', ' + Address1 end 
				+ case WHEN len(ltrim(Address4)) = 0 then '' else ', ' + Address4 end 
				+ case WHEN len(ltrim(Address5)) = 0 then '' else ', ' + Address5 end
				+ case WHEN len(ltrim(postalCode)) = 0 then '' else ', ' + postalCode end
				+ case WHEN len(ltrim(locationID)) = 0 then '' else ', ID:' + cast(locationID as nvarchar(50)) end
			as companyName
		from location 
		where organisationID =  <cf_queryparam value="#orgID#" CFSQLTYPE="CF_SQL_INTEGER" >  
		and locationID not in (select locationID from SODloadLocationLookup) 
		<!--- 2012-07-20 PPB P-SMA001		 and countryID in (#request.relayCurrentUser.countryList#) --->
		#application.com.rights.getRightsFilterWhereClause(entityType="location").whereClause#	<!--- 2012-07-20 PPB P-SMA001 --->
	</cfquery>
	
	<cfquery name="getCountryIDfromOrgID" datasource="#application.SiteDataSource#">
		select o.countryID, o.organisationName, c.countryDescription from organisation o inner join country c 
			on o.countryID = c.countryID
			where o.organisationID =  <cf_queryparam value="#orgID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	</cfquery>
	
	<cfif not isDefined("frmCountry")>
		<cfset frmCountry = getCountryIDfromOrgID.countryID>
	</cfif>
	
	<cfif not isDefined("frmCountryDesc")>
		<cfset frmCountryDesc = getCountryIDfromOrgID.countryDescription>
	</cfif>
	
	<cfif not isDefined("frmOrganisation")>
		<cfset frmOrganisation = getCountryIDfromOrgID.organisationName>
	</cfif>
	
	<cfif not isDefined("frmSearchLocations")>
		<cfset frmSearchLocations = left(getCountryIDfromOrgID.organisationName,3)>
	</cfif>

</cfif>

<!--- setting or resetting the location variables, and the search text variable
	If we're doing a loose search, get first word of company name. Otherwise, get first 3 letters. --->
<cfif getUnMatchedLocations.recordCount gt 0>
	<cfif not isDefined("frmSODlocations") or frmOrderByChanged>
		<cfset frmSODlocations = getUnMatchedLocations.companyID[1]>
		<cfif frmSearchMode eq "Tight">
			<cfset frmSearchLocations = left(getUnMatchedLocations.companyName[1],3)>
		<cfelse>
			<cfset frmSearchLocations = Left(getUnMatchedLocations.companyName[1],Find(' ', getUnMatchedLocations.companyName[1]))>
		</cfif>
	</cfif>
	<cfparam name="frmSearchLocations" default="">
	<cfif frmSearchMode eq "Tight" and frmSearchLocations neq left(getUnMatchedLocations.companyName[1],3) and frmCountryChanged>
		<cfset frmSearchLocations = left(getUnMatchedLocations.companyName[1],3)>
	<cfelseif frmSearchMode neq "Tight">
		<cfif Find(' ', getUnMatchedLocations.companyName[1]) gt 0>
			<cfif frmSearchLocations neq Left(getUnMatchedLocations.companyName[1],Find(' ', getUnMatchedLocations.companyName[1])) and frmCountryChanged>
				<cfset frmSearchLocations = Left(getUnMatchedLocations.companyName[1],Find(' ', getUnMatchedLocations.companyName[1]))>
			</cfif>
		<cfelse>
			<cfif frmSearchLocations neq getUnMatchedLocations.companyName[1] and frmCountryChanged>
				<cfset frmSearchLocations = getUnMatchedLocations.companyName[1]>
			</cfif>
		</cfif>
	</cfif>
</cfif>

<cf_querySim>
	searchMode
	display,value
	phr_sys_SOD_TightMatch|Tight
	phr_sys_SOD_LooseMatch|Loose
</cf_querySim>




<cf_head>
	<cf_title>SOD Match Locations</cf_title>
	
	<SCRIPT type="text/javascript">
		function closeWin() {
			if(!opener.closed){
				opener.location.reload(true);
			}
			self.close();
		}
		
		function changeSearchString(searchMode, location, locationID) {

			if (searchMode == 'Tight') {
				document.getElementById("frmSearchLocations").value = location.substring(0,3);
			} else {
				if (location.indexOf(' ') != -1) {
					document.getElementById("frmSearchLocations").value = location.substring(0,location.indexOf(' '));
				} else {
					document.getElementById("frmSearchLocations").value = location;
				}
			}
			
			// if we're outputing results and the location has changed, change the hidden variable as well in the results form.
			<cfif isDefined("frmSearch")>
				document.resultsForm.frmSODlocations.value = locationID;
			</cfif>
			
		}
	</script>
	
</cf_head>

<cfset request.relayFormDisplayStyle="HTML-table">

<cf_relayFormDisplay>
	<cfform action="/SalesOutData/SODmatchLocations.cfm" method="post" name="mainForm">
		<cf_relayFormElement relayFormElementType="hidden" fieldname="frmCountryChanged" currentValue="false" label="">
		<cf_relayFormElement relayFormElementType="hidden" fieldname="frmOrderByChanged" currentValue="false" label="">
		
		<cfswitch expression="#mode#">
			<cfcase value="findInSOD">
				<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="frmCountryDesc" currentValue="#frmCountryDesc#" label="phr_sys_SOD_Country">
				<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="frmOrganisation" currentValue="#frmOrganisation#" label="phr_sys_SOD_OrganisationName">
				<cf_relayFormElement relayFormElementType="hidden" fieldname="orgID" currentValue="#orgID#" label="">
				<cf_relayFormElement relayFormElementType="hidden" fieldname="frmCountry" currentValue="#frmCountry#" label="">
			</cfcase>
			<cfdefaultcase>
				<cfif not isDefined("orgID")>
					<!--- application variable setup by userfiles/code/cftemplates/SODload.cfm which is run daily. this is done to eliminate the need to get a count for each country over and over which was taking forever to run. --->
					<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="frmResults" currentValue="" label="phr_sys_SOD_Country">
						<cfif structKeyExists(application,"SODCountryData")>
							<cfset CDSorted = structSort(application.SODCountryData,"textnocase","desc","country")>
							<select name="frmCountry" onChange="frmCountryChanged.value=true;mainForm.submit();">
								<cfloop from="1" to="#arrayLen(CDSorted)#" index="x">
									<cfset CID = CDSorted[x]>
									<cfoutput><option value="#CID#"<cfif frmCountry eq CID> selected</cfif>>#application.SODCountryData[CID].Country# (#application.SODCountryData[CID].CountryCount#)</option></cfoutput>
								</cfloop>
								<!--- <cfoutput query="userCountries">
									<cfset thisCountryCnt = application.com.relaySOD.getCountryCount(countryID)>
									<option value="#countryID#">#Country# (#thisCountryCnt#)</option>
								</cfoutput> --->
							</select>
						<cfelse>
							phr_applicationCountryData_Variable_not_setup
						</cfif>
					</cf_relayFormElementDisplay>
				<cfelse>
					<cf_relayFormElementDisplay relayFormElementType="select" fieldname="frmCountry" currentValue="#frmCountry#" label="phr_sys_SOD_Country" query="#userCountries#" display="Country" value="CountryID" onChange="frmCountryChanged.value=true;mainForm.submit();">
				</cfif>
			</cfdefaultcase>
		</cfswitch>
		
		<cfif getUnMatchedLocations.recordCount gt 0>
			<!--- if we have locations that are unmatched --->
			<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="" currentValue="" label="phr_sys_SOD_unmatchedLocations">
				<cf_relayFormElement relayFormElementType="select" fieldname="frmSODlocations" currentValue="#frmSODlocations#" label="" query="#getUnMatchedLocations#" display="companyName" value="companyID" onChange="changeSearchString('#frmSearchMode#',frmSODlocations[selectedIndex].text,frmSODlocations[selectedIndex].value);">
				
				<cfif mode eq "findInCM">
					<cf_relayFormElement relayFormElementType="radio" fieldname="frmOrderBy" currentValue="#frmOrderBy#" label="" query="#orderBy#" display="display" value="value" onClick="frmOrderByChanged.value=true;mainForm.submit();">
				</cfif>
				
			</cf_relayFormElementDisplay>
			<cf_relayFormElementDisplay relayFormElementType="text" fieldname="frmSearchLocations" currentValue="#frmSearchLocations#" label="phr_sys_SOD_SearchForLocation" size="50">
			<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="" currentValue="" label="phr_sys_SOD_search">
				<cf_relayFormElement relayFormElementType="radio" fieldname="frmSearchMode" currentValue="#frmSearchMode#" label="" query="#searchMode#" display="display" value="value">
				<cf_relayFormElement relayFormElementType="submit" fieldname="frmSearch" currentValue="phr_sys_SOD_search" label="">
			</cf_relayFormElementDisplay>
		<cfelse>
			<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="" currentValue="There are no unmatched locations" label="">
		</cfif>
		
		<cfif mode eq "findInSOD">
			<cf_relayFormElementDisplay relayFormElementType="button" fieldname="frmCloseWin" currentValue="phr_sys_SOD_ReturnToSalesOutData" label="" spanCols="yes" valueAlign="right" onClick="closeWin();">
		</cfif>
		
	</cfform>
	
	<cfif isDefined("frmSearch")>
		<!--- we've done a search... display the results --->
		<cfform action="/SalesOutData/SODmatchLocations.cfm" method="post" name="resultsForm">
			<cf_relayFormElement relayFormElementType="hidden" fieldname="frmSODlocations" currentValue="#frmSODlocations#" label="">
			<cf_relayFormElement relayFormElementType="hidden" fieldname="frmSearchMode" currentValue="#frmSearchMode#" label="">
			<cf_relayFormElement relayFormElementType="hidden" fieldname="frmCountry" currentValue="#frmCountry#" label="">
			
			<cfswitch expression="#mode#">
				<cfcase value="findInSOD">
					<cf_relayFormElement relayFormElementType="hidden" fieldname="orgID" currentValue="#orgID#" label="">
					<cfset possibleMatches = application.com.relaySOD.getPossibleOrganisationMatchesInSOD(searchOrgName=#frmSearchLocations#, countryID=#frmCountry#, searchMode=#frmSearchMode#)>
				</cfcase>
				<!--- default case is 'findInCM' --->
				<cfdefaultcase>
					<cfset possibleMatches = application.com.relaySOD.getPossibleSODlocationMatchesInCM(searchOrgName=#frmSearchLocations#, countryID=#frmCountry#, searchMode=#frmSearchMode#)>
				</cfdefaultcase>
			</cfswitch>
		
			<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="frmResults" currentValue="" label="phr_sys_SOD_possibleLocations">
			<cfif possibleMatches.recordCount gt 0>
				<cfoutput query="possibleMatches" group="organisationName">
					<tr>
						<td valign="top">#htmleditformat(organisationName)#</td>
						<td>
							<table>
							<cfoutput>
								<cf_querySim>
									locationMatch
									display,value
									&nbsp;|#htmleditformat(locationID)#
								</cf_querySim>
							<tr>
								<cfif possibleMatches.currentRow eq 1>
									<td><cf_relayFormElement relayFormElementType="radio" fieldname="frmMatch" currentValue="#locationID#" label="" query="#locationMatch#" display="display" value="value"></td>
								<cfelse>
									<td><cf_relayFormElement relayFormElementType="radio" fieldname="frmMatch" currentValue="" label="" query="#locationMatch#" display="display" value="value"></td>
								</cfif>
								<td>#htmleditformat(sitename)# (#htmleditformat(locationID)#) - <cfif address1 neq "">#htmleditformat(address1)#, </cfif><cfif city neq "">#htmleditformat(city)#, </cfif>#htmleditformat(postalCode)#</td>
							</tr>
							</cfoutput>
							</table>
						</td>
					</tr>
				</cfoutput>
				<cfif mode eq "findInCM">
					<cf_relayFormElement relayFormElementType="hidden" fieldname="frmOrderBy" currentValue="#frmOrderBy#" label="">
				</cfif>
				<cf_relayFormElementDisplay relayFormElementType="submit" fieldname="frmCreateMatch" currentValue="phr_sys_SOD_CreateMatch" label="">
			<cfelse>
				phr_sys_SOD_NoLocationsWereFound
			</cfif>
			</cf_relayFormElementDisplay>
		</cfform>
	</cfif>
	
</cf_relayFormDisplay>





</cf_translate>
