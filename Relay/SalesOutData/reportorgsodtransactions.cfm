<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			orgSODTransactions.cfm	
Author:				NJH
Date started:		2007/09/14
	
Description:	Produces a report of an organisations transactions		

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
10-Oct-2008			NJH			Trend 1.1.3 Issue 1201 - Showed the current month values in the dropdown in the initial run of the report.
								The dropdown was showing the current month, although the data was being pulled back for the current month

04-Mar-2010 		PPB			P-TND096 show distributor on report
YMA 2013/01/23 Case:432560 enforce default month field to use english locale when prodessing month

Possible enhancements:


 --->
<!--- 


<cf_head>
	<cf_title>Organisation SOD Transactions</cf_title>
</cf_head>


 --->
<cfif fileexists("#application.paths.code#\cftemplates\SODINI.cfm")>
	<cfinclude template="/code/cftemplates/SODINI.cfm">
</cfif>
<cfparam name="frmCurrentEntityID" default="6497">
<cfparam name="sortOrder" type="string" default="datasource,invoiceMonth,invoice_Number">
<cfparam name="showpoints" default="true">		<!--- this setting is in the sodini.cfm file show points will show points per transaction in the report--->
<cfparam name="showDisti" default="false">		<!--- PPB 2010/03/04 P-TND096 - this setting is read from sodini.cfm  --->


<!--- NJH 2008/05/20 P-TND066 1.1.3 Default the report to the current month--->
<!--- NJH 2008/10/10 Bug Fix Trend 1.1.3 Issue 1201 - changed to form.variables --->
<!--- YMA 2013/01/23 Case:432560 enforce default month field to use english locale when prodessing month --->
<cfparam name="form.FRMWHERECLAUSEC" default="#left(monthAsString(datepart("m",now()),'en'),3)#-#datepart("yyyy",now())#">
<cfparam name="form.FRMWHERECLAUSED" default="#left(monthAsString(datepart("m",now()),'en'),3)#-#datepart("yyyy",now())#">
<cfparam name="numRowsPerPage" default = "100"> 

<!--- PPB 2010/03/04 P-TND096 re-jigged build of the showCols variable to allow the additional flag  --->
<cfif not IsDefined("showCols")>	
	<cfset showCols = "Datasource,Country,Reseller_Name,Third_Party_Foreign_Key,Invoice_Number,Invoice_Date,Product_Code,Quantity,No_Of_Users,">
	<cfif showpoints>
		<cfset showCols =  showCols & "Points,">
	</cfif>
	<cfset showCols =  showCols & "Order_Type,Order_Value,EndUser_Company_Name,">
	<cfif showDisti>
		<cfset showCols =  showCols & "Distributor_Code,Distributor_Name,">
	</cfif>
	<cfset showCols =  showCols & "Site_Name,LocationID">
</cfif>


<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">

<!---  NJH 2008/05/27 Added new functionality to TFQO whereby a locale is passed. Setting the locale in this way to get the
EURO symbol forced the date to be displayed in the French Locale as well, which was undesired behaviour.
<cfset t = Setlocale("French (Standard)")> 
--->  <!--- SOD data in Euros --->

<cfset tableName="SodMaster sm with (noLock) inner join location l with (noLock) on sm.locationID = l.locationID and l.organisationID = #frmCurrentEntityID#">
<cfset dateField="invoiceDate">
<cfset dateRangeFieldName="invoice_Date">
<!--- NJH 2008/10/10 Bug Fix Trend 1.1.3 Issue 1201 - changed to from true to false --->
<cfparam name="useFullRange" default="false">

<cfinclude template="/templates/DateQueryWhereClause.cfm"> 

<cfscript>
// create a structure containing the fields below which we want to be reported when the form is filtered
	if (not isDefined("passThruVars")) {
		passThruVars = StructNew();
	}
	StructInsert(passthruVars, "frmCurrentEntityID", frmCurrentEntityID);
	StructInsert(passthruVars, "ThisScreenTextID", ThisScreenTextID);
	StructInsert(passthruVars, "frmEntityType", frmEntityType);
	
</cfscript>

<!--- PPB 2010/03/04 P-TND096 added showDisti conditions in query --->
<!--- PPB 2010/03/19 P-TND096 done again with knowledge that NavisionKey comes from TrendNavisionKeys not the org.R1OrgID as previously informed  --->
<cfquery name="getOrgTransactions" datasource="#application.siteDataSource#" cachedwithin="#createTimeSpan(0,0,5,0)#">
	SELECT * FROM
	(	SELECT CASE WHEN sourceID = 1 THEN 'Box' WHEN sourceID = 2 THEN 'License' END AS datasource,
			country,
			resellerName as Reseller_Name,
			thirdPartyForeignKey as Third_Party_Foreign_Key,
			invoiceNumber as Invoice_Number,
			invoiceDate as invoice_Date,
			datename(month,invoiceDate) AS invoiceMonth,
			productCode as Product_code,
			quantity,
			NoOfUsers as No_of_users,
			orderType as order_type,
			orderValue as order_value,
			endUserCompanyName as EndUser_Company_name,
			l.siteName as Site_name,
			l.locationID,
			<cfif showDisti>sm.R1DistiID as Distributor_Code, DistiOrg.OrganisationName as Distributor_Name,</cfif>
			points -- NJH 2008/05/20 P-TND066 1.1.3
		FROM sodMaster sm WITH (NOLOCK) 
		INNER JOIN location l WITH (NOLOCK) ON l.locationID = sm.locationID  
		<cfif showDisti>
			LEFT OUTER JOIN TrendNavisionKeys tnk ON tnk.No = sm.R1DistiID
			LEFT OUTER JOIN Location DistiLoc ON tnk.LocationID = DistiLoc.LocationID   
			LEFT OUTER JOIN Organisation DistiOrg ON DistiOrg.OrganisationID = DistiLoc.OrganisationID   
		</cfif>
		WHERE l.organisationID =  <cf_queryparam value="#frmCurrentEntityID#" CFSQLTYPE="CF_SQL_INTEGER" > 
	) base
	WHERE 1 = 1
	<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
	ORDER BY <cf_queryObjectName value="#sortOrder#">
</cfquery>

<!--- <cfif getOrgTransactions.recordCount> --->
	<!--- WAB Case 431103, changed currencyLocale attribute to be currencyISO --->
	<cf_tableFromQueryObject 
		queryObject="#getOrgTransactions#"
		queryName="getOrgTransactions"
		sortOrder = "#sortOrder#"
		
		dateFormat="invoice_Date"
		currencyFormat="order_Value"
		currencyISO="EUR"
		
		showTheseColumns="#showCols#"
		
		FilterSelectFieldList="Datasource"
		FilterSelectFieldList2="Site_name"
		
		queryWhereClauseStructure="#queryWhereClause#" 
		passThroughVariablesStructure="#passThruVars#"
		numRowsPerPage=#numRowsPerPage#
	>
<!--- <cfelse>
	No transactions found for this organisation.
</cfif> --->

<!--- 

 --->

