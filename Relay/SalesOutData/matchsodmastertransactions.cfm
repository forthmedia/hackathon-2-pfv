<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			matchSODMasterTransactions.cfm	
Author:				NJH
Date started:		2007-09-18
	
Description:		For each SOD source, go through the SODMaster table and attempt to match any unmatched transactions.

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
13-06-2008			NJH			Added two parameters batchdate and sourceID to allow this page to run against a subset of data
								as there is too much data in the DB to run the update query against everything.
10-10-2008			NJH			Added a date range, so that transactions having an invoice date between certain dates could be matched


Possible enhancements:


 --->




<cf_head>
	<cf_title>Match SOD Master Transactions</cf_title>
</cf_head>


<cfsetting RequestTimeout = "600">

<cfparam name="batchDate" type="date" default="#request.requestTime#">
<cfparam name="sourceID" type="numeric" default=0>
<!--- NJH 2008-10-10 matching transactions in a date range --->
<cfparam name="startInvoiceDate" type="date" default="#year(now())#-#month(now())#-01">
<cfparam name="endInvoiceDate" type="date" default="#year(now())#-#month(now())#-#DaysInMonth(now())#">

<!--- NJH 2008-06-13 - if someone has passed a date through, then use it; otherwise don't because the current time won't 
match any records as the batchDate is quite specific. --->
<cfif batchDate neq request.requestTime>
	<cfset batchDate = createODBCDateTime(batchDate)>
<cfelse>
	<cfset batchDate = "">
</cfif>

<!--- NJH 2008-10-10 if we're matching data in a date range, ensure that we're only matching about a month's worth of data, as otherwise it
	gets to be too much --->
<cfif batchDate eq "" and (dateDiff("d",startInvoiceDate,endInvoiceDate) gt 31 or dateDiff("d",startInvoiceDate,endInvoiceDate) lt 0)>
	<cfoutput>Please ensure that the start date is less than the end date and that the range is less than or equal to a month (ie. 31 days).</cfoutput>
	<CF_ABORT>
</cfif>

<cfset sodSourceQry = application.com.relaySOD.getSODSources()>

<!--- if the sourceID is 0, we loop through all sources --->
<cfif sourceID eq 0>
	<cfset sourceIDList = valueList(sodSourceQry.sourceID)>
<cfelse>
	<cfset sourceIDList = sourceID>
</cfif>

<cfset count=0>

<cfloop list="#sourceIDList#" index="sourceID">
	<cfset count = count +1>
	<cfscript>
		source = application.com.relaySOD.getSourceDetails(sourceID=sourceID).description;
	</cfscript>
	<cfoutput>
		#count#. Matching <strong>#source#</strong> data...<br>
		<cfif batchDate neq "">
			for transactions with a batchDate of '#batchDate#'<br>
		<cfelse>
			for transactions with an invoice date greater than or equal to '#startInvoiceDate#' and less than or equal to '#endInvoiceDate#'<br>
		</cfif>	
	</cfoutput>
	

	<!--- match and move matched records to SOD claims --->
		<cftransaction action = "begin">
			<cftry>
				<cfscript>
					argumentsCollection = structNew();
					argumentsCollection.tableName='SodMaster';
					argumentsCollection.sourceID=sourceID;
					// if batch date is entered, use that otherwise use the date range
					if (batchDate neq "") {
						argumentsCollection.batchDate=batchDate;
					} else {
						argumentsCollection.startInvoiceDate=createODBCdateTime(startInvoiceDate);
						argumentsCollection.endInvoiceDate=createODBCdateTime(endInvoiceDate);
					}
					application.com.relaySOD.updateControlColumns(argumentCollection=argumentsCollection);
					/*if (batchDate neq "") {
						application.com.relaySOD.updateControlColumns(tableName='SODMaster',sourceID=sourceID,batchDate=batchDate);
					} else {
					application.com.relaySOD.updateControlColumns(tableName='SODMaster',sourceID=sourceID);
					}*/
					application.com.relaySOD.moveMatchedRecordsToSODClaim();
				</cfscript>
				
				<cftransaction action="commit"/>
			
				<cfcatch type="Database"> 
					<cfoutput> 
					     <h3>Error matching and moving records.</h3> 
					     <p>Message :#htmleditformat(cfcatch.message)#</p> 
					     <p>Type :#htmleditformat(cfcatch.type)#</p>
						 <p>Detail :#htmleditformat(cfcatch.detail)#</p>
					     <p><i>Rolling back the attempted transaction</i></p> 
					     <cftransaction action="rollback"/>
					</cfoutput> 
				</cfcatch>
			</cftry>

		</cftransaction>
		
		<cfquery name="getMatchedRows" datasource="#application.siteDataSource#">
			select count(*) as rowsMatched from SODMaster where matchDate = #createODBCDateTime(request.requestTime)#
				and sourceID =  <cf_queryparam value="#sourceID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfquery>
		
		<cfquery name="getRecordsMovedFromSodFailure" datasource="#application.siteDataSource#">
			select count(*) as rowsMoved from SODmaster sm left join SODFailure sf
				on sm.batchRowID = sf.batchRowID and
				sm.created = sf.batchDate and
				sm.sourceID = sf.sourceID
			where sf.batchDate is null and
				sm.matchDate = #createODBCDateTime(request.requestTime)# and 
				sm.processDate is null and
				sm.sourceID =  <cf_queryparam value="#sourceID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfquery>
		
		<cfquery name="getRecordsMovedToSODClaim" datasource="#application.siteDataSource#">
			select count(*) as rowsMoved from SODmaster sm inner join SODclaim sc
				on sm.batchRowID = sc.batchRowID and
				sm.created = sc.batchDate and
				sm.sourceID = sc.sourceID
			where sm.matchDate = #createODBCDateTime(request.requestTime)#
				and processDate is null
				and sm.sourceID =  <cf_queryparam value="#sourceID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfquery>
		
		<cfoutput>
			<br><strong>Results:</strong><br>
			&nbsp;#htmleditformat(getMatchedRows.rowsMatched)# records in SODMaster have just matched.<br>
			&nbsp;#htmleditformat(getRecordsMovedFromSodFailure.rowsMoved)# records were moved from SODFailure.<br>
			&nbsp;#htmleditformat(getRecordsMovedToSODClaim.rowsMoved)# records were moved to SODClaim.<br><br>
		</cfoutput>
		
</cfloop>



