<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			scorecardVarianceReportFunctions.cfm
Author:				SWJ
Date started:		2007-05-02
	
Description:			

creates functionList query for tableFromQueryObject custom tag

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
02-May-20037		SWJ			Initial version

Possible enhancements:


 --->

<cfscript>
comTableFunction = CreateObject( "component", "relay.com.tableFunction" );

comTableFunction.functionName = "Sales Out Data Functions";
comTableFunction.securityLevel = "";
comTableFunction.url = "";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();

comTableFunction.functionName = "--------------------";
comTableFunction.securityLevel = "";
comTableFunction.url = "";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();

comTableFunction.functionName = "&nbsp;Approve Changes";
comTableFunction.securityLevel = "";
comTableFunction.url = "javascript:document.frmBogusForm.submit();";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();

comTableFunction.functionName = "Phr_Sys_Tagging";
comTableFunction.securityLevel = "";
comTableFunction.url = "";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();

/*comTableFunction.functionName = "&nbsp;Phr_Sys_TagAllRecordsOnPage";
comTableFunction.securityLevel = "";
comTableFunction.url = "javascript:void(checkboxSetAll(true));//";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();

comTableFunction.functionName = "&nbsp;Phr_Sys_UntagAllRecordsOnPage";
comTableFunction.securityLevel = "";
comTableFunction.url = "javascript:void(checkboxSetAll(false));//";
comTableFunction.windowFeatures = "";
comTableFunction.functionListAddRow();*/
</cfscript>

