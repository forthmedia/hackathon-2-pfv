<!--- �Relayware. All Rights Reserved 2014 --->
<!---
File name:			SODhierarchyManagement.cfm	
Author:				NJH
Date started:		2006-07-29
	
Purpose:	Manage Sales Out Data groups, hierarchies and products

Usage: 			

Amendment History:


Date (DD-MMM-YYYY)	Initials 	What was changed


Possible enhancements:

--->



<!--- adding a product to a hierarchy --->
<cfif isDefined("frmAddProduct")>
	<cfset hierarchyID = application.com.SOD.addSODhierarchy(groupID=#frmSODgroupID#, level1=#frmSODlevel1Txt#, level2=#frmSODlevel2Txt#, level3=#frmSODlevel3Txt#, level4=#frmSODlevel4Txt#, level5=#frmSODlevel5Txt#)>
					
	<cfloop list="#frmAvailableSODproducts#" index="availableProduct">
		<cfset application.com.SOD.addSODproduct(productCode=#availableProduct#, hierarchyID=#hierarchyID#)>
	</cfloop>
</cfif>

<!--- removing a hierarchy or a product from a hierarchy --->
<cfif isDefined("frmProductHierarchyTask")>
	<cfif frmProductHierarchyTask eq "removeProduct">
		<cfset application.com.SOD.deleteSODproduct(productCode=#frmProduct#,hierarchyID=#frmEditHierarchyID#)>
	<cfelseif frmProductHierarchyTask eq "removeHierarchy">
		<cfset application.com.SOD.deleteSODhierarchy(hierarchyID=#frmEditHierarchyID#)>
	</cfif>
</cfif>

<!---  adding a new user group--->
<cfif isDefined("frmSubmitNewGroup") and isDefined("frmNewGroupName")>
	<cfif frmNewGroupName neq "">
		<cfset frmSODgroupID = application.com.SOD.addSODgroup(groupName=#frmNewGroupName#)>
		<cfif frmSODgroupID eq -1>
			<cfset errorMessage = "phr_sys_SOD_GroupNameExists">
		</cfif>
	</cfif>
	
<!--- editing a group name --->
<cfelseif isDefined("frmSubmitEditGroup") and isDefined("frmNewGroupName")>
	<cfif frmNewGroupName neq "">
		<cfset tempGroupID = application.com.SOD.updateSODgroup(newGroupName=#frmNewGroupName#, groupID=#frmSODgroupID#)>
		<cfif tempGroupID eq -1>
			<cfset errorMessage = "phr_sys_SOD_GroupNameExists">
		</cfif>
	</cfif>
</cfif>

<!--- Getting SOD groups that aren't system SOD groups --->
<cfquery name="getSODhierarchyGroups" datasource="#application.sitedatasource#">
	select productHierarchyGroupName, productHierarchyGroupID from SODhierarchyGroup
	where groupTypeID <> 1
	order by productHierarchyGroupName
</cfquery>

<!--- may be a record count of 0 here - allow for it --->
<cfif not isDefined("frmSODgroupID")>
	<cfif getSODhierarchyGroups.recordCount gt 0>
		<cfset frmSODgroupID = getSODhierarchyGroups.productHierarchyGroupID[1]>
	<cfelse>
		<cfset frmCreateNewGroup = "true">
	</cfif>	
</cfif>

<!--- Not in group edit mode --->
<cfif not isDefined("frmCreateNewGroup") and not isDefined("frmEditGroup") and not isDefined("errorMessage")>	
	<cfset groupHierarchies = application.com.SOD.getSODhierarchies(groupID = #frmSODgroupID#)>
	
	<cfquery name="getHierarchiesForFilter" dbtype="query">
		select productHierarchyID, productHierarchy, productHierarchyDisplay, 2 as hierarchyOrder from groupHierarchies
		UNION
		select -1 as productHierarchyID, 'phr_showAll' as productHierarchy, 'phr_showAll' as productHierarchyDisplay, 1 as hierarchyOrder from groupHierarchies
		order by hierarchyOrder, productHierarchy
	</cfquery>

	<cfif not isDefined("frmSODhierarchy")>
		<cfif getHierarchiesForFilter.recordCount gt 0>
			<cfset frmSODhierarchy = getHierarchiesForFilter.productHierarchyID[1]>
		<cfelse>
			<cfset frmSODhierarchy = -1>
		</cfif>
	</cfif>
	
	<cfquery name="getAvailableProducts" datasource="#application.sitedatasource#">
		select distinct productCode from SODproductHierarchy
		where productCode not in 
			(select distinct productCode from SODproductHierarchy sph inner join SODhierarchy sh on
				sph.productHierarchyID = sh.productHierarchyID and productHierarchyGroupID =  <cf_queryparam value="#frmSODgroupID#" CFSQLTYPE="CF_SQL_INTEGER" > )
		order by productCode
	</cfquery>

	<cfset SODproducts = application.com.SOD.getSODproducts(groupID=#frmSODgroupID#,hierarchyID=#frmSODhierarchy#)>
	
	<cfif not isDefined("frmSODproduct")>
		<cfset frmSODproduct = SODproducts.productCode[1]>
	</cfif>
</cfif>

<cf_translate>

<cf_head>
	<cf_title>SOD Hierarchy Management</cf_title>
	
	<cf_includejavascriptonce template = "/javascript/openwin.js">	
		
	<SCRIPT type="text/javascript">
		
		function moveAvailableProducts(){
			if (mainForm.frmSODlevel1Txt.value == "") {
				alert("phr_sys_SOD_PleaseEnterAvalueForLevel1");
				mainForm.frmSODlevel1Txt.focus();
				return false;
			} else if (mainForm.frmSODlevel2Txt.value == "") {
				alert("phr_sys_SOD_PleaseEnterAvalueForLevel2");
				mainForm.frmSODlevel2Txt.focus();
				return false;
			} else if (mainForm.frmSODlevel3Txt.value == "") {
				alert("phr_sys_SOD_PleaseEnterAvalueForLevel3");
				mainForm.frmSODlevel3Txt.focus();
				return false;
			} else if (mainForm.frmSODlevel4Txt.value == "") {
				alert("phr_sys_SOD_PleaseEnterAvalueForLevel4");
				mainForm.frmSODlevel4Txt.focus();
				return false;
			} else if (mainForm.frmSODlevel5Txt.value == "") {
				alert("phr_sys_SOD_PleaseEnterAvalueForLevel5");
				mainForm.frmSODlevel5Txt.focus();
				return false;
			}
			return true;
		}
			
		function changeTextValues(productHierarchyObj) {
			var hierarchy = productHierarchyObj.value.split('|');
			for (var i=0;i<hierarchy.length;i++) {
				switch(i) {
					case 0: mainForm.frmSODlevel1Txt.value = hierarchy[i];break;
					case 1: mainForm.frmSODlevel2Txt.value = hierarchy[i];break;
					case 2: mainForm.frmSODlevel3Txt.value = hierarchy[i];break;
					case 3: mainForm.frmSODlevel4Txt.value = hierarchy[i];break;
					case 4: mainForm.frmSODlevel5Txt.value = hierarchy[i];break;
				}
			}
		}
		
		
		function submitForm(task,hierarchyID,product) {
			mainForm.target = "";
			mainForm.action = "SODHierarchyManagement.cfm";
			mainForm.frmEditHierarchyID.value = hierarchyID;
			mainForm.frmProduct.value = product;
			mainForm.frmProductHierarchyTask.value = task;
			
			if (task == "removeProduct") {
				if (confirm('phr_sys_SOD_AreYouSureYouWantToDeleteTheProduct') == false) {
					return;
				}
			} else if (task == "removeHierarchy") {
				if (confirm('phr_sys_SOD_AreYouSureYouWantToDeleteTheCategory') == false) {
					return;
				}
			} else if (task == "editHierarchy" || task == "moveProduct") {
				openWin('','newWindow','width=600,height=150,toolbar=no,titlebar=no,resizable=no,status=no,menubar=no,fullscreen=no');
				mainForm.target = "newWindow";
				mainForm.action = "SODhierarchyTask.cfm"
			}
			mainForm.submit();
		}
	</script>
</cf_head>



<cfset hierarchySize=10>
<cfset request.relayFormDisplayStyle = "HTML-table">
<cfform action="SODHierarchyManagement.cfm" method="post" name="mainForm">
<cf_relayFormDisplay>

	<!--- Not in group edit mode --->
	<cfif not isDefined("frmCreateNewGroup") and not isDefined("frmEditGroup") and not isDefined("errorMessage")>
		<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="" currentValue="" label="" spanCols="yes" valueAlign="right">
			<cf_relayFormElement relayFormElementType="submit" fieldname="frmEditGroup" currentValue="phr_sys_SOD_EditGroup" label="" >
			<cf_relayFormElement relayFormElementType="submit" fieldname="frmCreateNewGroup" currentValue="phr_sys_SOD_CreateNewGroup" label="" >
		</cf_relayFormElementDisplay>
		<cf_relayFormElementDisplay relayFormElementType="select" fieldname="frmSODgroupID" currentValue="#frmSODgroupID#" label="phr_sys_SOD_SODgroup" query="#getSODhierarchyGroups#" display="productHierarchyGroupName" value="productHierarchyGroupID" onChange="javascript:mainForm.submit();">
	</cfif>
	
	<!--- there was an error in trying to save the new group name. --->
	<cfif isDefined("errorMessage")>
		<cf_relayFormElementDisplay relayFormElementType="Message" fieldname="" currentValue="#errorMessage#" label="">
	</cfif>
	
	<!--- if we're in group edit mode --->
	<cfif isDefined("frmCreateNewGroup") or isDefined("frmEditGroup") or isDefined("errorMessage")>
	
		<cfif isDefined("frmSODgroupID")>
			<cfquery name="getGroupName" datasource="#application.sitedatasource#">
				select productHierarchyGroupName from SODhierarchyGroup where productHierarchyGroupID =  <cf_queryparam value="#frmSODgroupID#" CFSQLTYPE="CF_SQL_INTEGER" > 
			</cfquery>
			<cf_relayFormElement relayFormElementType="hidden" fieldname="frmSODgroupID" currentValue="#frmSODgroupID#" label="">
		</cfif>
		
		<cfset groupName = #IIF(isDefined("frmEditGroup") and getGroupName.recordCount gt 0,"getGroupName.productHierarchyGroupName", DE(""))#>
		<cf_relayFormElementDisplay relayFormElementType="text" fieldname="frmNewGroupName" currentValue="#groupName#" label="phr_sys_SOD_newGroupName">
		<cfset fieldname = #IIF(isDefined("frmCreateNewGroup"),DE("frmSubmitNewGroup"), DE("frmSubmitEditGroup"))#>

		<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="" currentValue="" label="">
			<cf_relayFormElement relayFormElementType="button" fieldname="frmCancel" currentValue="phr_Cancel" label="" onClick="javacript: document.location.href=this.document.location.href">
			<cf_relayFormElement relayFormElementType="submit" fieldname="#fieldname#" currentValue="phr_Save" label="">
		</cf_relayFormElementDisplay>
	</cfif>
</cf_relayFormDisplay>
<!--- </cfform> --->
<!--- Not in group edit mode --->
<cfif not isDefined("frmCreateNewGroup") and not isDefined("frmEditGroup") and not isDefined("errorMessage")>
	<!--- <cfform action="SODHierarchyManagement.cfm" method="post" name="mainForm" target="" onSubmit="return moveAvailableProducts();"> --->
	<!--- if there aren't any available products to move to hierarchies, don't show this add section --->
	<cfif getAvailableProducts.recordCount gt 0>
		<cf_relayFormDisplay cellPadding=1>
			<!--- <cf_relayFormElement relayFormElementType="hidden" fieldname="frmSODgroupID" currentValue="#frmSODgroupID#"> --->
			
			<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="" label="" currentValue="" spanCols="yes">
				<cf_relayFormDisplay>
					<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="" label="phr_sys_SOD_AddProductsToGroup" currentValue="&nbsp;">
					<tr>
						<td>
							<cf_relayFormDisplay class="" cellPadding=0>
								<cf_relayFormElementDisplay relayFormElementType="select" fieldname="frmAvailableSODproducts" currentValue="" label="phr_sys_SOD_chooseAvailableProducts" query="#getAvailableProducts#" display="productCode" value="productCode" multiple="yes" size="10" style="width:200px;" onClick="javascript:frmAddProduct.disabled=false;" required="no">
							</cf_relayFormDisplay>
						</td>
						<td valign="top">
							<cf_relayFormDisplay class="" cellPadding=0>
								<cfif groupHierarchies.recordCount gt 0>
									<cf_relayFormElementDisplay relayFormElementType="select" fieldname="frmSODhierarchyLevels" currentValue="#frmSODhierarchy#" label="phr_sys_SOD_chooseExistingCategory" query="#groupHierarchies#" display="productHierarchyDisplay" value="productHierarchy" onChange="changeTextValues(frmSODhierarchyLevels);">
								</cfif>
								<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="" currentValue="" label="phr_sys_SOD_CreateNewCategory">
									<table>
										<tr>
											<th>level 1</th><th>level 2</th><th>level 3</th><th>level 4</th><th>level 5</th>
										</tr>
										<tr>
											<td><cf_relayFormElement relayFormElementType="text" fieldname="frmSODlevel1Txt" currentValue="#SODproducts.level1[1]#" label="phr_sys_SOD_SODhierarchyLevel1" size="#hierarchySize#" maxLength="#hierarchySize#"></td>
											<td><cf_relayFormElement relayFormElementType="text" fieldname="frmSODlevel2Txt" currentValue="#SODproducts.level2[1]#" label="phr_sys_SOD_SODhierarchyLevel2" size="#hierarchySize#" maxLength="#hierarchySize#"></td>
											<td><cf_relayFormElement relayFormElementType="text" fieldname="frmSODlevel3Txt" currentValue="#SODproducts.level3[1]#" label="phr_sys_SOD_SODhierarchyLevel3" size="#hierarchySize#" maxLength="#hierarchySize#"></td>
											<td><cf_relayFormElement relayFormElementType="text" fieldname="frmSODlevel4Txt" currentValue="#SODproducts.level4[1]#" label="phr_sys_SOD_SODhierarchyLevel4" size="#hierarchySize#" maxLength="#hierarchySize#"></td>
											<td><cf_relayFormElement relayFormElementType="text" fieldname="frmSODlevel5Txt" currentValue="#SODproducts.level5[1]#" label="phr_sys_SOD_SODhierarchyLevel5" size="#hierarchySize#" maxLength="#hierarchySize#"></td>
										</tr>
									</table>
								</cf_relayFormElementDisplay>
								<cf_relayFormElementDisplay relayFormElementType="submit" fieldname="frmAddProduct" label="" currentValue="phr_sys_SOD_AddProducts" disabled="true" valueAlign="left" spanCols="no">
							</cf_relayFormDisplay>
						</td>
					</tr>
				</cf_relayFormDisplay>
			</cf_relayFormElementDisplay>
		</cf_relayFormDisplay>
	</cfif>
		
	<!--- Displaying the products in the hierarchies --->
	<cfif SODproducts.recordCount gt 0>
		<cf_relayFormDisplay>
			<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="" label="" currentValue="&nbsp;" spanCols="yes">
			<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="" label="phr_sys_SOD_ManageProductsInGroup" currentValue="&nbsp;">
			<cfif getHierarchiesForFilter.recordCount gt 0>
				<cf_relayFormElementDisplay relayFormElementType="select" fieldname="frmSODhierarchy" currentValue="#frmSODhierarchy#" label="phr_sys_SOD_CategoryFilter" query="#getHierarchiesForFilter#" display="productHierarchyDisplay" value="productHierarchyID" onChange="javascript:mainForm.submit();">
			</cfif>
	
			<cf_relayFormElementDisplay relayFormElementType="HTML" fieldname="" label="" currentValue="" valueAlign="left">
				<cf_relayFormDisplay>
						<cf_relayFormElement relayFormElementType="hidden" fieldName="frmEditHierarchyID" currentValue="">
						<cf_relayFormElement relayFormElementType="hidden" fieldName="frmProduct" currentValue="">
						<cf_relayFormElement relayFormElementType="hidden" fieldName="frmProductHierarchyTask" currentValue="">
					
					<tr>
						<th colspan="3">
							phr_sys_SOD_category
						</th>
						<th colspan="3">
							phr_sys_SOD_product
						</th>
					</tr>
					<cfset hierarchyRowClass="oddRow">
					<cfoutput query="SODproducts" group="productHierarchy">
					<cfset hierarchyRowClass  = #IIF(hierarchyRowClass eq "evenRow", DE("oddRow"), DE("evenRow"))#>
					<tr class="#hierarchyRowClass#">
						<td valign="top" width="20">
							<a href="javascript: submitForm('editHierarchy','#productHierarchyID#','')"><img src="/images/misc/iconEditPencil.gif" border="0" alt="phr_sys_SOD_editCategory"/></a></td>
						<td valign="top" width="20">
							<a href="javascript:submitForm('removeHierarchy','#productHierarchyID#','')"><img src="/images/misc/iconDeleteCross.gif" border="0" alt="phr_sys_SOD_deleteCategory"/></a></td>
						<td valign="top">
							#htmleditformat(productHierarchy)#
						</td>
						<td>
							<table border=0 cellpadding=0 cellspacing=0 width="100%">
								<cfset productRowClass=#hierarchyRowClass#>
								<cfoutput>
									<tr class="#productRowClass#">
										<td valign="top" width="20"><a href="javascript:submitForm('moveProduct','#productHierarchyID#','#ProductCode#')"><img src="/images/misc/iconEditPencil.gif" border="0" alt="phr_sys_SOD_moveProduct"/></a></td>
										<td valign="top" width="20"><a href="javascript:submitForm('removeProduct','#productHierarchyID#','#ProductCode#');"><img src="/images/misc/iconDeleteCross.gif" border="0" alt="phr_sys_SOD_deleteProduct"/></a></td>
										<td valign="top" align="left">#htmleditformat(ProductCode)#</td>
									</tr>
									<cfset productRowClass  = #IIF(productRowClass eq "evenRow", DE("oddRow"), DE("evenRow"))#>
								</cfoutput>
							</table>
							
						</td>
					</tr>
					</cfoutput>
					</cf_relayFormDisplay>
	<!---  			<cfoutput>
					<iframe src="SODproductManagement.cfm?frmSODgroupID=#frmSODgroupID#&frmSODhierarchy=#frmSODhierarchy#" name="SODproductList" id="SODproductList" width="520" title="productList" height="280">
					</iframe>
				</cfoutput> --->
			</cf_relayFormElementDisplay>
		</cf_relayFormDisplay>
	</cfif>
</cfif>
</cfform>





</cf_translate>
