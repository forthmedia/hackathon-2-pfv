<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			reportSODunmatchedLocations.cfm	
Author:				NJH
Date started:		2007/09/14
	
Description:	Produces a report of sod locations that haven't matched to locations in Relayware		

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2012-10-04	WAB		Case 430963 Remove Excel Header 

Possible enhancements:


 --->
 
<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">

<cfparam name="openAsExcel" type="boolean" default="false"> 
<cfparam name="sortOrder" type="string" default="datasource,country">
<cfparam name="showCols" type="string" default="Datasource,Third_Party_Foreign_Key,Reseller_Name,Country,Invoice_Date">
<cfparam name="numRowsPerPage" default = "100">

<cfset tableName="vUnmatchedSODLocations">
<cfset dateField="Invoice_Date">
<cfset dateRangeFieldName="Invoice_Date">
<cfparam name="useFullRange" default="true">

<cfinclude template="/templates/DateQueryWhereClause.cfm">

<cfscript>
// create a structure containing the fields below which we want to be reported when the form is filtered
if (not isDefined("passThruVars")) { 
	passThruVars = StructNew();
}
//StructInsert(passthruVars, "useFullRange", useFullRange);
</cfscript>
 
	<cf_head>
		<cf_title>SOD Unmatched Locations report</cf_title>
	</cf_head>
	
	
	<cfinclude template="SODTopHead.cfm">

<cfset dateCols = "Invoice_Date">

<cfquery name="getUnmatchedLocations" datasource="#application.siteDataSource#">
	select * from vUnmatchedSODLocations
	where 1=1
	<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">
	order by <cf_queryObjectName value="#sortOrder#">
</cfquery>

<cf_tableFromQueryObject 
	queryObject="#getUnmatchedLocations#"
	queryName="getUnmatchedLocations"
	sortOrder = "#sortOrder#"
	dateFormat ="#dateCols#"
	showTheseColumns="#showCols#"
	openAsExcel="#openAsExcel#"
	numRowsPerPage="#numRowsPerPage#"
	
	FilterSelectFieldList="datasource"
	FilterSelectFieldList2="Country"
	
	queryWhereClauseStructure="#queryWhereClause#"
>

<cfif not openAsExcel>
	
	
</cfif>

