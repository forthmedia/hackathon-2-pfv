<!--- �Relayware. All Rights Reserved 2014 --->
<!--- 
File name:			salesOutDisplay.cfm	
Author:				SSS
Date started:		2009/10/30
	
Description:		Display sales out data.

Amendment History:

Date (DD-MMM-YYYY)	Initials 	What was changed
2009-jan-30			SSS			Changed the template so that thisOrganisationID is being passed around and not 			
								FRMCURRENTENTITYID
2012-10-04	WAB		Case 430963 Remove Excel Header 

Possible enhancements:


 --->

<cfinclude template="/templates/tableFromQueryHeaderInclude.cfm">

<cfinclude template="/styles/mediumChartStyles.cfm">
 
<cfparam name="openAsExcel" type="boolean" default="false">
<cfparam name="SODgroupID" type="numeric" default="1">

<cfquery name="getSODgroups" datasource="#application.siteDataSource#">
	select productHierarchyGroupID, productHierarchyGroupName from SODhierarchyGroup
</cfquery>
<!--- SSS 2009-01-30 if thisOrganisationID is set do not reset it ---> 
<cfif not isdefined("thisOrganisationID")>
	<cfif isdefined('FRMCURRENTENTITYID')>
		<cfset thisOrganisationID = FRMCURRENTENTITYID>
		<!--- horrible hack for screen inclussion --->
		<cfif not openasexcel>
			</form>
		</cfif>
	<cfelse>
		<cfparam name="thisOrganisationID" default="#request.relaycurrentuser.organisationID#">
	</cfif>
</cfif>

<cfquery name="doesOrganisationHaveUnMatchedLocations" datasource="#application.siteDataSource#">
	select count(locationID) as locationcount from location 
	where 
		organisationID =  <cf_queryparam value="#thisOrganisationID#" CFSQLTYPE="CF_SQL_INTEGER" >   
	and 
		locationID not in 
		(select locationID from SODloadLocationLookup) 
	and 
		location.countryID = (select countryID from organisation where organisationID =  <cf_queryparam value="#thisOrganisationID#" CFSQLTYPE="CF_SQL_INTEGER" > )
</cfquery>

<cfquery name="doesOrganisationHaveSalesOutData" datasource="#application.siteDataSource#">
	select count(locationID) as locationcount from location 
	where 
		organisationID =  <cf_queryparam value="#thisOrganisationID#" CFSQLTYPE="CF_SQL_INTEGER" >   
	and 
		locationID in 
		(select locationID from SODloadLocationLookup) 
	and 
		location.countryID = (select countryID from organisation where organisationID =  <cf_queryparam value="#thisOrganisationID#" CFSQLTYPE="CF_SQL_INTEGER" > )
</cfquery>

<cfscript>
	passThruVars = StructNew();
</cfscript>

<cfif not openAsExcel>

	<cfscript>
	// create a structure containing the fields below which we want to be reported when the form is filtered
	// SSS 2009-01-30 thisOrganisationID pass this round instead of frmcourrentEntityID because that may not exist
	StructInsert(passthruVars, "frmEntityType", form.frmEntityType);
	StructInsert(passthruVars, "thisOrganisationID", thisOrganisationID);
	StructInsert(passthruVars, "thisScreenTextID", form.thisScreenTextID);
	StructInsert(passthruVars, "SODgroupID", SODgroupID);
	</cfscript>

	
	<cf_head>
		<cf_title>Sales Out Data analysis</cf_title>
		<cf_includejavascriptonce template = "/javascript/openwin.js">	
	</cf_head>
	
	
	
	<cfif doesOrganisationHaveUnMatchedLocations.locationcount neq 0>
		<!--- this organisation has several locations that are don't have sales out data --->
		<cfoutput>
			#htmleditformat(doesOrganisationHaveUnMatchedLocations.locationcount)# locations for this organisation do not have sales out data. Click 
			<a href="javascript:void(openWin('/SalesOutData/SODmatchLocations.cfm?orgID=#thisOrganisationID#','','width=600,toolbar=no,titlebar=no,status=no,menubar=no,scrollbars=yes,resizable=yes'))">here </a>
			to find possible location matches in unmatched records.
		</cfoutput>
	</cfif>
	
		<!--- don't show anything if there is no data --->
	<cfif not doesOrganisationHaveSalesOutData.locationcount eq 0>		
		<!--- if this organisation has sales out data, display it --->

		<cfset request.relayFormDisplayStyle="HTML-table">
		<cfform action="/data/entityFrameScreens.cfm?&MESSAGE=" method="post" name="groupForm">
			<cf_relayFormDisplay>
				<cf_relayFormElementDisplay relayFormElementType="HTML" fieldName="" currentValue="" label="phr_OpenInExcel" labelAlign="right">
					<cfoutput>
						<a href="/code/cftemplates/salesoutdisplay.cfm?#queryString#&openAsExcel=true" target="blank" title="phr_OpenInExcel">phr_Excel</a>
					</cfoutput>
				</cf_relayFormElementDisplay>
				<cf_relayFormElementDisplay relayFormElementType="HTML" fieldName="" currentValue="" label="phr_sys_SOD_ChartAnalysis" labelAlign="right">
					<cfoutput>
						<a href="javascript: void(openWin('/SalesOutData/SODchartDisplay.cfm?orgID=#thisOrganisationID#','','width=#request.chartWidth#,height=#request.chartHeight#,toolbar=no,titlebar=no,resizable=no,status=no,menubar=no,fullscreen=no'))" title="phr_sys_SOD_ChartAnalysis">phr_sys_SOD_ViewChart</a>
					</cfoutput>
				</cf_relayFormElementDisplay>
				<cf_relayFormElementDisplay relayFormElementType="select" fieldName="SODgroupID" currentValue="#SODgroupID#" label="phr_sys_SOD_ChooseAproductSet" query="#getSODgroups#" display="productHierarchyGroupName" value="productHierarchyGroupID" onChange="groupForm.submit();" labelALign="right">
				<cf_relayFormElement relayFormElementType="hidden" fieldName="thisScreenTextID" currentValue="#thisScreenTextID#" label="">
<!--- SSS 2009-01-30  thisOrganisationID pass this round instead of frmcourrentEntityID because that may never exist --->
				<cf_relayFormElement relayFormElementType="hidden" fieldName="thisOrganisationID" currentValue="#thisOrganisationID#" label="">
				<cf_relayFormElement relayFormElementType="hidden" fieldName="frmEntityType" currentValue="#frmEntityType#" label="">
			</cf_relayFormDisplay>
		</cfform>

		<!--- open as excel link and product group picker --->
	</cfif>
</cfif>	
	
	<cfparam name="sortOrder" default="Product_Code">
	<cfparam name="numRowsPerPage" default="100">
	<cfparam name="startRow" default="1">
	
	<cfquery name="getData" datasource="#application.siteDatasource#">
		select * from (
			SELECT     SODload.ProductCode as Product_code, SODload.UnitPrice as unit_price, SODload.TotalPrice as total_price, 
			SODload.YearNumber as sale_year,
			cast(SODload.YearNumber as varchar(4)) + '-' + cast(SODload.QuarterNumber as varchar(2)) as quarter,
			cast(SODload.YearNumber  as varchar(4)) + '-' + cast(SODload.MonthNumber as varchar(2)) as sale_month,
			SODhierarchy.level1, 
			SODhierarchy.level2, 
			SODhierarchy.level3, 
		    SODhierarchy.level4, 
			SODhierarchy.level5
		FROM         SODproductHierarchy INNER JOIN
		                      SODhierarchy ON SODproductHierarchy.productHierarchyID = SODhierarchy.productHierarchyID INNER JOIN
		                      SODload ON SODproductHierarchy.ProductCode = SODload.ProductCode INNER JOIN
		                      SODloadLocationLookup ON SODload.ThirdPartyForeignKey = SODloadLocationLookup.ThirdPartyForeignKey INNER JOIN
							  Location ON SODloadLocationLookup.LocationID = Location.LocationID INNER JOIN
							  organisation ON Location.OrganisationID = organisation.OrganisationID
		WHERE     organisation.OrganisationID =  <cf_queryparam value="#thisOrganisationID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		<cfif isDefined("SODgroupID")>
				AND SODhierarchy.productHierarchyGroupID =  <cf_queryparam value="#SODgroupID#" CFSQLTYPE="CF_SQL_INTEGER" > 
		</cfif>
		) a
		where 1=1	
		<cfinclude template="/templates/tableFromQuery-QueryInclude.cfm">	
		order by <cf_queryObjectName value="#sortOrder#">
	</cfquery> 
	
	<CF_tableFromQueryObject 
		queryObject="#getData#"
		sortOrder = "#sortOrder#"
		startRow = "#startRow#"
		openAsExcel = "#openAsExcel#"
		numRowsPerPage="#numRowsPerPage#"
		totalTheseColumns="total_price"
		GroupByColumns="product_code"
		showTheseColumns="sale_year,quarter,sale_month,Product_Code,level1,level2,level3,level4,level5,unit_price,total_price"
		FilterSelectFieldList="sale_year,quarter,sale_Month,level1,level2,level3,level4,level5,Product_Code"
		FilterSelectFieldList2="sale_year,quarter,sale_Month,level1,level2,level3,level4,level5,Product_Code"
		hideTheseColumns=""
		numberFormat="year"
		currencyformat="unit_price,total_price"
		showCellColumnHeadings="no" 
		passThroughVariablesStructure="#passThruVars#"
		keyColumnURLList="/SalesOutData/SODchartDisplay.cfm?orgID=#thisOrganisationID#&productCode="
		keyColumnKeyList="product_code"
		keyColumnList="product_code"
		keyColumnOpenInWindowList="yes"
		OpenWinSettingsList="width=#request.chartWidth+40#,height=#request.chartHeight+40#,toolbar=no,titlebar=no,resizable=no,status=no,menubar=no,fullscreen=no"
	>
<cfif not openAsExcel>




</cfif>


